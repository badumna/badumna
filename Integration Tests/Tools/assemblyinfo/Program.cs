﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.IO;

namespace AssemblyInfo
{
    public class Program
    {
        private class Command
        {
            public Action<Assembly> Action;
            public string Help;
        }

        private static Dictionary<string, Command> Commands = new Dictionary<string, Command>
        {
            { "obfuscation", new Command { Action = ObfuscatedPercentage, Help = "prints % of names that are obfuscated" } },
            { "fullname", new Command { Action = a => Console.WriteLine(a.FullName), Help = "prints the full name of the assembly" } },
            { "configuration", new Command { Action = AssemblyConfiguration, Help = "prints the configuration attribute of the assembly" } },
            { "title", new Command { Action = AssemblyTitle, Help = "prints the title attribute of the assembly" } },
            { "company", new Command { Action = AssemblyCompany, Help = "prints the company attribute of the assembly" } }
        };

        private static bool IsObfuscatedName(string name)
        {
            return !string.IsNullOrEmpty(name) && ((int)name[0] >= 0x1700 || name.Length <= 2);
        }

        private static void ObfuscatedPercentage(Assembly assembly)
        {
            BindingFlags allDeclaredMembers =
                BindingFlags.DeclaredOnly |
                BindingFlags.Public |
                BindingFlags.NonPublic |
                BindingFlags.Instance |
                BindingFlags.Static;

            BindingFlags nonPublicMembers =
                BindingFlags.DeclaredOnly |
                BindingFlags.NonPublic |
                BindingFlags.Instance |
                BindingFlags.Static;

            var types = assembly.GetTypes();
            var publicTypes = from type in types where type.IsVisible select type;
            var nonPublicTypes = from type in types where !type.IsVisible select type;

            var publicTypeNonPublicMembers = from type in publicTypes from member in type.GetMembers(nonPublicMembers) select member.Name;
            var nonPublicTypeMembers = from type in nonPublicTypes from member in type.GetMembers(allDeclaredMembers) select member.Name;
            var nonPublicMemberNames = Enumerable.Concat(publicTypeNonPublicMembers, nonPublicTypeMembers);

            int total = nonPublicMemberNames.Count();
            int obfuscated = nonPublicMemberNames.Count(n => IsObfuscatedName(n));

            Console.WriteLine("{0:0.0}", 100.0 * obfuscated / total);
        }

        private static void AssemblyConfiguration(Assembly assembly)
        {
            object[] configAttributes = assembly.GetCustomAttributes(typeof(AssemblyConfigurationAttribute), true);
            if (configAttributes.Length != 1)
            {
                Console.WriteLine("Zero or many AssemblyConfiguration attributes found!");
                Environment.Exit(3);
            }

            Console.WriteLine(((AssemblyConfigurationAttribute)configAttributes[0]).Configuration);
        }

        private static void AssemblyTitle(Assembly assembly)
        {
            object[] configAttributes = assembly.GetCustomAttributes(typeof(AssemblyTitleAttribute), true);
            if (configAttributes.Length != 1)
            {
                Console.WriteLine("Zero or many AssemblyTitle attributes found!");
                Environment.Exit(3);
            }

            Console.WriteLine(((AssemblyTitleAttribute)configAttributes[0]).Title);
        }

        private static void AssemblyCompany(Assembly assembly)
        {
            object[] configAttributes = assembly.GetCustomAttributes(typeof(AssemblyCompanyAttribute), true);
            if (configAttributes.Length != 1)
            {
                Console.WriteLine("Zero or many AssemblyCompany attributes found!");
                Environment.Exit(3);
            }

            Console.WriteLine(((AssemblyCompanyAttribute)configAttributes[0]).Company);
        }

        public static int Main(string[] args)
        {
            string assemblyFileName = null;
            string mode = null;

            if (args.Length == 2)
            {
                mode = args[0].ToLowerInvariant();
                assemblyFileName = Path.GetFullPath(args[1]);
            }

            if (mode == null || !Commands.ContainsKey(mode))
            {
                Console.WriteLine("Usage: AssemblyInfo <mode> <assembly.dll>");
                Console.WriteLine();
                Console.WriteLine("       where <mode> is one of");

                foreach (KeyValuePair<string, Command> command in Commands)
                {
                    Console.WriteLine("            {0,-20}{1}", command.Key, command.Value.Help);
                }

                return 1;
            }

            AppDomain.CurrentDomain.AssemblyResolve +=
                delegate (object sender, ResolveEventArgs resolveArgs)
                {
                    try
                    {
                        return Assembly.LoadFrom(Path.Combine(Path.GetDirectoryName(assemblyFileName), new AssemblyName(resolveArgs.Name).Name + ".dll"));
                    }
                    catch (Exception)
                    {
                        Console.WriteLine("Failed to load referenced assembly: {0}", resolveArgs.Name);
                        return null;
                    }
                };

            Assembly assembly;
            try
            {
                assembly = Assembly.LoadFrom(assemblyFileName);
            }
            catch (Exception e)
            {
                Console.WriteLine("Could not open '{0}': {1}", assemblyFileName, e.Message);
                return 2;
            }

            try
            {
                Commands[mode].Action(assembly);
                return 0;
            }
            catch (Exception e)
            {
                Console.WriteLine("Unexpected error: {0}", e);
                return 3;
            }
        }
    }
}
