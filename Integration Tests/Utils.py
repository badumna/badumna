
import os
import re
import glob
import itertools
import subprocess
from zipfile import ZipFile
from filecmp import dircmp
from fnmatch import fnmatch

class Utils:

    def unzip_file(self, filename, destination):
        print "Unzipping '{0}' to '{1}'".format(filename, destination)
        zip = ZipFile(filename)
        zip.extractall(destination)

        
        
    def directories_match(self, directory1, directory2):
        def assert_dir_matches(dc):
            if len(dc.left_only) > 0:
                raise AssertionError("Found files not present in both directories under '{0}': {1}".format(dc.left, dc.left_only))

            if len(dc.right_only) > 0:
                raise AssertionError("Found files not present in both directories under '{0}': {1}".format(dc.right, dc.right_only))
                
            if len(dc.diff_files) > 0:
                raise AssertionError("Found files that differ under '{0}': {1}".format(dc.left, dc.diff_files))
            
            if len(dc.funny_files) > 0:
                raise AssertionError("Found 'funny' files under '{0}': {1}".format(dc.left, dc.funny_files))
            
            for subdir in dc.subdirs.itervalues():
                assert_dir_matches(subdir)

        assert_dir_matches(dircmp(directory1, directory2, ignore=[]))

        
    def _recurse_dircmp(self, dc, selector):
        return itertools.chain(selector(dc), *(self._recurse_dircmp(s, selector) for s in dc.subdirs.itervalues()))
    
    def _gather_files(self, directory1, directory2, property_selector):
        dc = dircmp(directory1, directory2, ignore=[])
        
        def selector(dc):
            return (os.path.relpath(os.path.join(dc.left, f), directory1) for f in property_selector(dc))
        
        return self._recurse_dircmp(dc, selector)
    
    def in_left_only(self, directory1, directory2):
        return list(self._gather_files(directory1, directory2, lambda dc: dc.left_only))
    
    def different_only(self, directory1, directory2):
        return list(self._gather_files(directory1, directory2, lambda dc: dc.diff_files))
    

    def find_files(self, directory, pattern):
        """Return a list of paths to files matching 'pattern' in 'directory' or one of its subdirectories."""
        
        result = []
        
        for root, dirs, files in os.walk(directory):
            for file in files:
                if fnmatch(file, pattern):
                    result.append(os.path.join(root, file))
                    
        return result

    def get_assembly_info(self, element, assembly):
        return subprocess.check_output(['Tools/AssemblyInfo', element, assembly]).strip()
        
    def assemblies_have_correct_attributes(self, version, test_dir):
        exclusions = [
            'System.Web.Abstractions.dll',
            'System.Web.Mvc.dll',
            'System.Web.Routing.dll',
            'Mono.Options.dll',
            'Mono.Security.dll',
            'ICSharpCode.SharpZipLib.dll',
            'System.Data.SQLite.dll',
            'sqlite3.dll',
            'App_global.asax.dll',
            'App_GlobalResources.dll',
            'App_GlobalResources.resources.dll',
            'App_Web_*.dll',
            'unzip.exe',
            
            # We can't easily sign or set the version number on the following assemblies because we release the source code for them.
            'WebSignupDemo.dll',
            'Statistics.dll',
            'StatisticsServer.exe',
        ]
                      
        name_checks = [
            ( 'signing key', re.compile(r'PublicKeyToken=138f8bd6a8dc98c9') ),
            ( 'version', re.compile(r'Version={0}.\d+'.format(version)) ),
        ]

        assemblies = self.find_files(test_dir, '*.dll')
        assemblies.extend(self.find_files(test_dir, '*.exe'))

        failures = []
        for assembly in assemblies:
            basename = os.path.basename(assembly)
            if any(fnmatch(basename, x) for x in exclusions):
                continue
            
            fullname = self.get_assembly_info('fullname', assembly)
            issues = [ check[0] for check in name_checks if check[1].search(fullname) is None ]
            
            config = self.get_assembly_info('configuration', assembly)
            if  config != '':
                issues.append('configuration ({0})'.format(config))
            
            if len(issues) > 0:
                failures.append("'{0}' ({1}) has wrong {2}.".format(os.path.relpath(assembly, test_dir), fullname, ' and '.join(issues)))
                
        assert len(failures) == 0, '\n'.join(failures)






