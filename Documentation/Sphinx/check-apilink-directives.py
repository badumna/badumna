#!/usr/bin/env python
import os, sys
import re
import fileinput
import glob
import itertools

err = 0

os.chdir(os.path.dirname(os.path.abspath(__file__)))

def check(ref, filename, line, content):
    global err
    if not os.path.exists(os.path.join("..", "API", "output", "html", ref + ".htm")):
        err += 1
        print >> sys.stderr, "ERROR: {f}:{l} {text} ({id})".format(f=filename, l=line, text=content, id=ref)

patterns = map(re.compile, [
    '^\.\. \|.*\| apilink::.* (?P<ref>[^ ]+)$',
    ':netapi:`(([^`]*) )*(?P<ref>[^` ]+)`',
])

def get_inputs():
    for path, dirnames, filenames in os.walk("."):
        for filename in filenames:
            if filename.endswith(".rst") or filename.endswith(".txt"):
                #print filename
                yield os.path.join(path, filename)

for line in fileinput.input(get_inputs()):
    line = line.strip()
    for pattern in patterns:
        for match in pattern.finditer(line):
            check(match.groupdict()['ref'], filename=fileinput.filename(), line=fileinput.filelineno(), content=line)

sys.exit(err)
