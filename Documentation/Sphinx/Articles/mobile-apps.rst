.. _mobile-apps:

-------------------------------------------------------
Developing Mobile Applications Using Badumna
-------------------------------------------------------

   
Badumna supports game development for iOS and Android platforms.
This section discusses the mobile applications that can be developed and the methods that can be used to develop the applications using Badumna.


====================
Mobile applications
====================
Badumna provides support for a variety of mobile games and applications:

.. image:: ../images/ipad.png
   :width: 200px
   :align: right

   
**1. Casual games:** Badumna can be configured for developing casual multiplayer games.
Casual games typically involve 2 to 8 players in each game session, with multiple sessions taking place simultaneously.
These games are often fast moving and thus require a quick response for a smooth user experience.
Badumna utilises a patent-pending protocol that offers the ability to send state updates at a high frequency.
Whereas most systems send acknowledgements in order to preserve reliability, our protocol is unique in that it minimises the need for
acknowledgements through the use of a smart transport mechanism (thus conserving bandwidth) without sacrificing reliability.

Badumna's casual gaming mode can be used to convert your existing single-player games to multiplayer, taking advantage of Badumna's cross-platform publishing capability.
Moreover, this functionality is provided without having to worry about managing any servers or writing any server code.
 
.. image:: ../images/mmog.png
   :width: 250px
   :align: right

**2. MMOs/Virtual worlds:** MMO (massively multiplayer online) applications typically have large worlds with thousands of players online at any one time.
These games often provide persistent worlds that evolve as a result of actions from the players.
MMO backend servers rely on *interest management* to deliver updates to all the users in the game.
Interest management is the task of identifying those remote players whose avatars are close to a local player's avatar as the avatar moves.
Interest management functionality is quite critical when developing MMOs or virtual worlds because sending state updates to the thousands of users in a scene or shard is simply not possible.
Not only does Badumna automate this functionality but it provides this functionality in a fully decentralised manner without relying on central servers.
This means that you can scale your game to millions of users and build shardless worlds without having to worry about managing massive server farms.

 
**3. Location-based games:** Location-based gaming is a new genre that is rapidly gaining popularity since the advent of GPS-capable smart phones and tablets.
These games incorporate players' real world locations in the game play, offering interesting functionality that is a mix between 
the real world and virtual world.
The interest management functionality offered by Badumna can be used to synchronise both a player's real location and their
avatar's virtual position with other players. This is possible because a player's real-world location consists of spatial data 
that can be indexed and queried using Badumna's decentralized spatial indexing algorithms.
Publishers can therefore design fully synchronous location-based games and reap the benefits of scalability and cost-reduction associated with Badumna's decentralised architecture.

.. image:: ../images/lbs-1.png
   :width: 100px
   :align: right
   
**4. Location-based services:** Badumna can be used for non-gaming applications such as location-based services.
Location based services require that a user's real-world location be synchronised with relevant remote users.
As with location-based games, this feature is automatically supported by Badumna's distributed spatial indexing technology.
Traditionally, location-based services have used central servers to index and synchronise the location information of all users.
These applications can have millions of users accessing the service at any one time.
Publishers are therefore forced to offer only asynchronous multiuser functionality since they are not able to index and synchronise the spatial information of all the global users in real-time.
Badumna solves this problem, providing real-time synchronisation of the global users.
This allows publishers to design more compelling location-based services without having to worry about scalability.


====================
Development methods
====================

Badumna can be used in a variety of ways to develop mobile applications:

**1. Unity:** If you are using Unity as your development platform for mobile applications, Badumna has built-in support to develop mobile applications.
Simply download Badumna's Unity package available on the website and follow the instructions in the :ref:`unity-tutorials` section.

**2. Xamarin.iOS/Xamarin.Android:** Xamarin is a cross platform framework that allows you to develop native apps using C# and .NET.
Xamarin.iOS and Xamarin.Android provide the ability to develop native apps for iOS and Android platforms respectively.
If you are using this framework, please contact us for the appropriate .NET libraries.

**3. Custom engines:** If you have a custom C++ engine that you use for your game development, you can use Badumna with the C++ bindings. 
You will need to download Badumna's C++ package and follow the instructions in the :ref:`cplusplus` section for further information.

======================
Cross platform support
======================

Badumna can be used to design applications that can work across multiple platforms.
For example, you can design a game where Android users can interact and play with iOS users and be part of the same ecosystem.
Badumna also supports Windows, MacOS, and Linux operating systems.
You can therefore develop a game across all these platforms and have desktop users interact with mobile users.

Moreover, Badumna's decentralised architecture offers visibility across the entire network of users.
This is especially useful if you are a publisher of multiple titles.
You can offer cross game functionality between your games or even provide an in-game monetisation platform that can be accessed by all your users.

==============
3G v/s Wifi
==============
When designing games for mobile devices, it is important to keep the device and network in mind.
3G networks can have variable data rates as the speed depends on the number of users sharing the cell tower. 
If a large number of users are connected to a cell tower, then the average speed available to each user would be low.
Therefore, if you are developing an application targeting 3G networks, you need to restrict the network traffic such that the application is able to deal with slower data rates.
If you are developing a "Wifi only" application, you can assume much faster data rates (similar to home networks) and design your application accordingly.
Badumna will optimise the network traffic as part of its delivery protocol by sending only the information that has changed and use techniques 
such as dead reckoning to conserve network traffic.
Applications can add an additional layer of application-specific dead reckoning to further optimise the network traffic and improve user experience 
(refer to :ref:`smoothing` for more details).

.. include:: common-subs.txt
