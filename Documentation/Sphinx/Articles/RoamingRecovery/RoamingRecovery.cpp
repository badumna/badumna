#include <iostream>
#include <windows.h>

#include "Badumna/Core/RuntimeInitializer.h"
#include "Badumna/Core/NetworkFacade.h"
#include "Badumna/Configuration/Options.h"
#include "Badumna/Replication/SpatialEntityStateSegment.h"

namespace NetworkRoamingRecovery
{
    using namespace Badumna;

    enum EntitySegment
    {
        EntitySegment_Counter = StateSegment_FirstAvailableSegment
    };

    class Original : public ISpatialOriginal
    {
    public:
        Original() :
            guid_(BadumnaId::None()),
            position_(),
            radius_(1),
            area_of_interest_radius_(10),
            counter_(0)
        {
        }

        BadumnaId Guid() const { return guid_; }
        void SetGuid(BadumnaId const &id) { guid_ = id; }

        Vector3 Position() const { return position_; }
        void SetPosition(Vector3 const &position) { position_ = position; }

        float Radius() const { return radius_; }
        void SetRadius(float val) { radius_ = val; }

        float AreaOfInterestRadius() const { return area_of_interest_radius_; }
        void SetAreaOfInterestRadius(float val) { area_of_interest_radius_ = val; }

        int Counter() const { return counter_; }
        void IncCounter() { ++counter_; }

        void HandleEvent(InputStream *stream) { }

        void Serialize(BooleanArray const &required_parts, OutputStream *stream)
        {
            if (required_parts[EntitySegment_Counter])
            {
                *stream << counter_;
            }
        }

    private:
        BadumnaId guid_;
        Vector3 position_;
        float radius_;
        float area_of_interest_radius_;
        int counter_;
    };

    class Replica : public ISpatialReplica
    {
    public:
        Replica() :
            guid_(BadumnaId::None()),
            position_(),
            radius_(0),
            area_of_interest_radius_(0),
            counter_(0)
        {
        }

        BadumnaId Guid() const { return guid_; }
        void SetGuid(BadumnaId const &id) { guid_ = id; }

        Vector3 Position() const { return position_; }
        void SetPosition(Vector3 const &position) { position_ = position; }

        float Radius() const { return radius_; }
        void SetRadius(float val) { radius_ = val; }

        float AreaOfInterestRadius() const { return area_of_interest_radius_; }
        void SetAreaOfInterestRadius(float val) { area_of_interest_radius_ = val; }

        int Counter() const { return counter_; }
        void IncCounter() { ++counter_; }

        void HandleEvent(InputStream *stream) { }

        void Deserialize(BooleanArray const &included_part, InputStream *stream, int estimated_milliseconds_since_departure)
        {
            if (included_part[EntitySegment_Counter])
            {
                int new_counter;
                *stream >> new_counter;

                if (new_counter != counter_)
                {
                    std::wcout << L"Entity " << guid_.ToString().CStr() << L" counter = " << new_counter << std::endl;
                }
            }
        }

    private:
        BadumnaId guid_;
        Vector3 position_;
        float radius_;
        float area_of_interest_radius_;
        int counter_;
    };

    class Program
    {
    public:
        Program();
        void Run();
        void Quit();

    private:
        BadumnaRuntimeInitializer initializer_;

        std::auto_ptr<NetworkFacade> facade_;
        bool reinitialize_required_;
        HANDLE should_quit_;

        std::auto_ptr<NetworkScene> scene_;

        Original original_;
        std::vector<Replica*> replicas_;

        void Initialize();
        void Shutdown();
        void OnAddressChanged();

        ISpatialReplica *CreateReplica(NetworkScene const &, BadumnaId const &id, uint32_t);
        void RemoveReplica(NetworkScene const &, ISpatialReplica const &replica);
    };

    Program::Program() :
        initializer_(),
        facade_(),
        reinitialize_required_(false),
        should_quit_(),
        scene_(),
        original_(),
        replicas_()
    {
    }

    void Program::Run()
    {
        should_quit_ = CreateEvent(NULL, TRUE, FALSE, NULL);

        Initialize();

        int timer = 1;
        while (WaitForSingleObject(should_quit_, 0) == WAIT_TIMEOUT)
        {
            if (timer % 10 == 0)
            {
                original_.IncCounter();
                facade_->FlagForUpdate(original_, EntitySegment_Counter);
            }

            facade_->ProcessNetworkState();

            if (reinitialize_required_)
            {
                Shutdown();
                Initialize();
            }

            Sleep(100);
            ++timer;
        }

        Shutdown();
    }

    void Program::Quit()
    {
        SetEvent(should_quit_);
    }

    void Program::Initialize()
    {
        std::wcout << L"Initialize" << std::endl;

        // NOTE: Set the seed peer address as appropriate.
        // NOTE: The AddressChangedEvent notification will only be be triggered if at least one working STUN server is configured.
        Options options;
        options.GetConnectivityModule().AddSeedPeer("www.example.com:21251");
        options.GetConnectivityModule().SetApplicationName("network-roaming-recovery");

        while (true)
        {
            facade_.reset(NetworkFacade::Create(options));

            reinitialize_required_ = false;
            facade_->SetAddressChangedEventDelegate(ConnectivityStatusDelegate(&Program::OnAddressChanged, *this));

            if (facade_->Login())
            {
                break;
            }

            std::wcout << L"Login failed, retrying..." << std::endl;

            facade_->Shutdown();

            if (WaitForSingleObject(should_quit_, 0) != WAIT_TIMEOUT)
            {
                return;
            }
        }

        scene_.reset(facade_->JoinScene(
            "test",
            CreateSpatialReplicaDelegate(&Program::CreateReplica, *this),
            RemoveSpatialReplicaDelegate(&Program::RemoveReplica, *this)));

        scene_->RegisterEntity(&original_, 1);
    }

    void Program::OnAddressChanged()
    {
        std::wcout << L"Address change detected." << std::endl;
        reinitialize_required_ = true;
    }

    ISpatialReplica *Program::CreateReplica(NetworkScene const &, BadumnaId const &id, uint32_t)
    {
        std::wcout << L"Create replica " << id.ToString().CStr() << std::endl;
        Replica *replica = new Replica();
        replica->SetGuid(id);
        replicas_.push_back(replica);
        return replica;
    }
    
    void Program::RemoveReplica(NetworkScene const &, ISpatialReplica const &replica)
    {
        std::wcout << L"Remove replica " << replica.Guid().ToString().CStr() << std::endl;

        std::vector<Replica*>::iterator iter = std::find(replicas_.begin(), replicas_.end(), &replica);
        if (iter != replicas_.end())
        {
            delete *iter;
            replicas_.erase(iter);
        }
    }

    void Program::Shutdown()
    {
        std::wcout << L"Shutdown" << std::endl;
        scene_->Leave();
        scene_.reset(0);
        facade_->Shutdown();

        for (std::vector<Replica*>::iterator it = replicas_.begin(); it != replicas_.end(); ++it)
        {
            delete *it;
        }

        replicas_.clear();
    }
}

static NetworkRoamingRecovery::Program program;

static BOOL WINAPI ctrl_handler(DWORD type)
{
    if (type == CTRL_C_EVENT)
    {
        program.Quit();
        return TRUE;
    }

    return FALSE;
}

int main(int argc, char **)
{
    SetConsoleCtrlHandler(&ctrl_handler, TRUE);
    program.Run();
    return 0;
}
