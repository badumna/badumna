﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;

using Badumna;
using Badumna.DataTypes;
using Badumna.SpatialEntities;
using Badumna.Utilities;

namespace NetworkRoamingRecovery
{
    class Program
    {
        static void Main(string[] args)
        {
            Program program = new Program();
            program.Run();
        }

        private INetworkFacade networkFacade;

        private NetworkScene scene;

        private Entity original;

        private List<Entity> replicas = new List<Entity>();

        private ManualResetEvent shouldQuit = new ManualResetEvent(false);

        private bool reinitializeRequired;

        private void Run()
        {
            Console.CancelKeyPress += delegate(object sender, ConsoleCancelEventArgs args)
            {
                args.Cancel = args.SpecialKey == ConsoleSpecialKey.ControlC;
                shouldQuit.Set();
            };

            this.original = new Entity();

            this.Initialize();

            int timer = 1;
            while (!shouldQuit.WaitOne(TimeSpan.Zero))
            {
                if (timer % 10 == 0)
                {
                    this.original.Counter++;
                    this.networkFacade.FlagForUpdate(this.original, (int)EntitySegments.Counter);
                }

                this.networkFacade.ProcessNetworkState();

                if (this.reinitializeRequired)
                {
                    this.Shutdown();
                    this.Initialize();
                }

                Thread.Sleep(100);
                timer++;
            }

            this.Shutdown();
        }

        private void Initialize()
        {
            Console.WriteLine("Initialize");

            // NOTE: Set the seed peer address as appropriate.
            // NOTE: The AddressChangedEvent notification will only be be triggered if at least one working STUN server is configured.
            Options options = new Options();
            options.Connectivity.SeedPeers.Add("www.example.com:21251");
            options.Connectivity.ApplicationName = "network-roaming-recovery";

            while (true)
            {
                this.networkFacade = NetworkFacade.Create(options);

                this.reinitializeRequired = false;
                this.networkFacade.AddressChangedEvent += delegate
                {
                    Console.WriteLine("Address change detected.");
                    this.reinitializeRequired = true;
                };

                if (this.networkFacade.Login())
                {
                    break;
                }

                Console.WriteLine("Login failed, retrying...");

                this.networkFacade.Shutdown();

                if (this.shouldQuit.WaitOne(TimeSpan.FromSeconds(10)))
                {
                    return;
                }
            }

            CreateSpatialReplica createReplica = delegate(NetworkScene s, BadumnaId entityId, uint entityType)
            {
                Console.WriteLine("Create replica {0}", entityId);
                Entity replica = new Entity();
                this.replicas.Add(replica);
                return replica;
            };

            RemoveSpatialReplica removeReplica = delegate(NetworkScene s, IReplicableEntity replica)
            {
                var entity = (Entity)replica;
                Console.WriteLine("Remove replica {0}", entity.Guid);
                this.replicas.Remove(entity);
            };

            this.networkFacade.RegisterEntityDetails(10, 1);
            this.scene = this.networkFacade.JoinScene("test", createReplica, removeReplica);
            this.scene.RegisterEntity(this.original, 1);
        }

        private void Shutdown()
        {
            Console.WriteLine("Shutdown");
            this.scene.Leave();
            this.scene = null;
            this.networkFacade.Shutdown();
            this.replicas.Clear();
        }
    }

    enum EntitySegments
    {
        Counter = SpatialEntityStateSegment.FirstAvailableSegment
    }

    class Entity : ISpatialOriginal, ISpatialReplica
    {
        public BadumnaId Guid { get; set; }

        public Vector3 Position { get; set; }

        public float Radius { get; set; }

        public float AreaOfInterestRadius { get; set; }

        public byte Counter { get; set; }

        public Entity()
        {
            this.Radius = 1;
            this.AreaOfInterestRadius = 10;
        }

        public void Serialize(BooleanArray requiredParts, Stream stream)
        {
            if (requiredParts[(int)EntitySegments.Counter])
            {
                stream.WriteByte(this.Counter);
            }
        }

        public void Deserialize(BooleanArray includedParts, Stream stream, int estimatedMillisecondsSinceDeparture)
        {
            if (includedParts[(int)EntitySegments.Counter])
            {
                byte counter = (byte)stream.ReadByte();
                if (counter != this.Counter)
                {
                    Console.WriteLine("Entity {0} counter = {1}", this.Guid, counter);
                }

                this.Counter = counter;
            }
        }

        public void HandleEvent(Stream stream)
        {
        }
    }
}
