.. _recovery-after-network-failure:

--------------------------------------------
Recovering after connecting to a new network
--------------------------------------------

========
Overview
========

For certain applications it is important to support the ability to roam from network to network while the application is running.
A typical example is an application on a mobile device where the device may switch between cellular and wifi networks as the user moves around.  
Supporting network roaming currently requires a new :netapi:`NetworkFacade 9C132730` to be constructed whenever the public network address of the machine changes.
This document describes how to implement support for network roaming.


===========================================
The :netapi:`AddressChangedEvent 866BD4F4`
===========================================

If you have defined at least one working STUN server when constructing your facade then Badumna will notify your application if the peer's public network address changes.
This notification is provided by the :netapi:`AddressChangedEvent 866BD4F4` on the network facade (this event is invoked on the application thread during the next call to :netapi:`ProcessNetworkState 3F60847F`).
Once this event has been triggered, communications (e.g. entity updates, chat messages, etc.) sent on the existing network facade may no longer work.
The existing facade must be shut down, and a new facade constructed.
The new facade will connect to the Badumna network with the new public network address and communications can resume.


=========================
Switching to a new facade
=========================

When handling the :netapi:`AddressChangedEvent 866BD4F4` a few steps must be taken to get up and running again:

   1. Shut down the existing facade by calling one of the :netapi:`Shutdown F7EDB177` overloads.
   2. Remove references to objects created by the existing network facade, such as :netapi:`NetworkScene C39EEE11` and :netapi:`IChatChannel 494A0B14` instances.
      These objects can be re-created once the new network facade is active.
   3. Instantiate a new network facade and call a :netapi:`Login D3712FEA` overload as usual.
      Note that the network may be unavailable for a short period of time while the operating system switches to the new network.
      In this case, the :netapi:`Login D3712FEA` call may fail because the facade hasn't found a public address.
      If the login call fails then this facade should be shut down and a new facade created before trying to log in again (see the example below for details).
   4. Rejoin any scenes that were previously joined, re-register original entities, reconnect to chat channels, etc.


=======
Example
=======

The following example code demonstrates how to implement network roaming support in a simple console application.
Before running the example update the seed peer address to point to your seed peer, and ensure the seed peer and example application names match
(in general roaming will only work if you have a seed peer because after one peer changes networks it will no longer be able to find another by broadcast).
Compile the program and run an instance on two separate machines.
You should see a replica appear on each peer with a sequence number that increments continuously.
Disconnect one peer from its current network and connect it to a new network.
After a brief delay you should see the peers reconnect and the replicas continue counting.


.. literalinclude:: RoamingRecovery.cs
    


