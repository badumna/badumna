--------------------------------------------------------------------------
Badumna benefits
--------------------------------------------------------------------------

====================
Instant scalability
====================

Badumna uses a revolutionary approach that is based on a decentralised architecture, rather than the traditional client-server architecture. Client-server architectures are prone to single point of failures and are hard to scale no matter how you configure your server cluster. Badumna's patent pending technology utilises a decentralised approach that utilises the end-user network that can grow to a large number of users without stressing any single node in the network. 
This means that applications can scale to truly massive player counts without relying on a single server that needs to be up and running all the time.


======================
Improved performance
======================

Badumna reduces the round trip time of messages significantly. Badumna is therefore able to provide a smoother user experience making it ideal for real time action games.

===================
New game designs
===================

Badumna enables game designers to create games that would not be commercially feasible with a conventional client-server architecture.
Designers have avoided some options because their implementation would result in high operating costs and the risk of significant performance degradation for players.
Badumna creates exciting new possibilities such as single world 'unsharded' environments instead of splitting players into isolated servers; 
adding synchronous interactions to large social games that had asynchronous functionality in the past; 
or creating large-scale multiplayer games for mobile devices.


========================
Reduced operating costs
========================

Hosting a multiplayer application can be very expensive due to the high operational costs associated in managing a server farm with very high bandwidth demands. Badumna is capable of reducing this cost significantly (up to 80%) as it utilises the end-user network to deliver most of the functionality.

For example, consider a hypothetical MMO with 80,000 active users, where the average number of concurrent users is about 8,000, with peaks of up to 10,000. Let's assume that each individual server within a server cluster can manage a maximum of 400 concurrent users (the actual maximum concurrent users per server depends on type of game and the servers used).

The simplified example below shows that if Badumna reduces the server requirements by 75% then the publisher's annual operating costs could be reduced by up to $168,000.

Badumna's instant scalability and 'Pay As You Grow' pricing model ensures that you do not invest in server infrastructure that might never be used making it an ideal choice when the popularity of the application is unpredictable, such as Free To Play games.

.. figure:: ../images/savings-chart.png
   :width: 550px
   :alt: Badumna savings
   :align: center
   :figwidth: 550px 
