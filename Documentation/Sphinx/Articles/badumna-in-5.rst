--------------------
Badumna in 5 Minutes
--------------------

================
Badumna overview
================

Badumna is a network engine designed for multi-user applications such as online games and virtual worlds.
The uniqueness of Badumna lies in its ability to provide a highly scalable networking framework for application developers.
An application that uses Badumna can scale to truly massive player counts using minimal operator-owned infrastructure and network resources.

This approach has many benefits, including a major reduction in operating costs for online games and improved game performance
because the network automatically scales as the user base grows and the direct peer-to-peer connections provide lower network latency.  
The approach also creates new game design options that are not commercially feasible with a client-server architecture. 
Imagine the possibilities of an MMO where all of the users can interact with each other, as opposed to just those players on the same one server. 
Or the ability to make social games that are truly social, by adding real-time synchronous interactions between millions of players without a massive new server farm. 
Badumna also lets you create a new style of location-based mobile games, where Badumna automatically discovers players in your local area and establishes real-time connections between them.

=====================
How does Badumna work
=====================

Decentralisation is the key to providing a scalable platform for online games.
Badumna forms a secure, structured, and managed peer-to-peer network of all the users in the system.
Services such as state synchronisation and chat are offered on this network making them extremely scalable. 

Badumna forms a secondary network of trusted nodes (operator controlled machines) that are used for services such as authentication, third-party arbitration and HTTP tunnelling.
Badumna provides a fully distributed look-up service to access these services thereby eliminating the single point of failure and making them scalable.

Badumna was designed with simplicity in mind. 
Game state synchronisation, which is the main task for an online game, is provided as an automated feature by Badumna. 
Developers do not have to worry about any server side code for synchronising state information. 
Badumna manages all that in the network and exchanges object updates to and from relevant remote objects.

==================================
Hey, hasn't P2P been tried before?
==================================
Experienced game developers might remember P2P approaches being used in games many years ago, 
or have recently seen companies offering P2P as part of a networking solution. 
These approaches mean simply using clients as servers, or sending game content updates P2P. 
Badumna is fundamentally different.
Badumna uses a managed P2P network for real-time state synchronisation, automatically discovering peers within a region of interest 
and establishing real-time connections. 
This approach creates unrivalled scalability, performance and cost benefits.  

==================
Test drive Badumna
==================

To test Badumna's basic features, download a fully compiled example from this link depending on your target platform.

Link to Windows version: `Windows download 
<http://www.scalify.com/badumna/LiveDemo/LiveDemo.zip>`_

Link to Mac version: `Mac download 
<http://www.scalify.com/badumna/LiveDemo/LiveDemo.dmg>`_ 

Once you have downloaded the example, execute 2 copies of the program either on the same machine or different machines.
You will be able to test Badumna's state synchronisation and proximity chat functionality.
All three versions have been configured to work with each other. 
You should therefore be able to start the Windows version and interact with other users
who are using the web version or the Mac version as long as you join the same room.
Please note that for running the Windows example you will need .NET framework(ver 2.0 or later) installed on your machine and 
for running the Mac example you will need Mono 2.6 installed on your Mac.

If you wish to learn more about Badumna, please feel free to browse the detailed :ref:`user manual <chapter:how-to>`
or `download
<http://www.scalify.com/store.php>`_
the entire product suite.
