-------------------
Technology Overview
-------------------

========
Overview
========

Badumna Network Suite is a complete technology solution for building highly scalable multiuser applications.
The product suite comprises of a network core that resides on the client, a set of server applications, and 
monitoring tools. The technology has been designed in such a manner that it hides all the complexities of
managing game state synchronisation and back-end networking details from the game developer. Game developers
do not have to write a single line of server code to develop multi-user applications. Badumna transparently
manages all the state synchronisation in the network.

Moreover, Badumna's decentralised architecture not only provides an easy to use product but a highly scalable
underlying framework. Badumna's design allows developers to scale their applications to millions of users without 
having to worry about managing expensive server clusters. Badumna does not rely on any one server that needs to 
be up and running all the time, allowing you to achieve an uptime of 100%. Badumna's unique architecture makes it
the only product in the market that can provide such high reliability.

To learn more about the technology, please feel free to read about the different modules that form the product
suite.

============
Badumna core
============

This is Badumna's core module and it resides with the game client providing most of the functionality.
Badumna has a very novel approach in terms of offering multiuser functionality. Traditional network engines 
rely on client-server architecture to offer game state synchronisation. This approach has several drawbacks.
It is very hard to scale to a large number of users. When a game grows in 
popularity, operators are forced to divide the game into shards and add more servers to their server cluster.
Not only is this a very expensive exercise but managing such a growing cluster of servers reliably is a very 
arduous task. Operators fail to achieve the high reliability required for their applications resulting in 
increased downtime and a loss of revenue.

Very scalable platform
======================

Badumna, on the other hand uses a decentralised architecture. Such an architecture removes the
bottlenecks associated with client-server architecture and allows the application to scale to unlimited number
of users. Badumna uses sophisticated multidimensional structures to form this network that comprises of all the
users. 
Badumna uses a cooperative approach which means that every single user contributes to the game functionality in
some manner. 
Game functionality is offered on this network by using advanced distributed indexing techniques. This technique
ensures that users are not relying on any single server for any specific functionality. This approach also allows
the game to scale to a large number of users without relying on expensive server clusters. Badumna
manages all the load transparently in the network.

Robust and secure transport
===========================
Badumna provides a robust and secure transport layer that is responsible for all the underlying communication
between the users. The transport layer provides a mechanism to transmit UDP packets in a reliable manner thereby
providing support for real-time bandwidth intensive applications. The transport layer also provides all the routing
functionality within Badumna. The routing is done over a structured peer-to-peer network that is robust and secure.
Individual nodes in the network can go offline at any time and it doesn't affect the application functionality in
any manner. Dynamic scheduling and flow control techniques provide the support for handling flash crowds.
Badumna also encrypts every message before sending, making it a safe and secure protocol.

User-friendly API
=================
Badumna's core functionality is accessed via a user-friendly API. The API can be used by application programmers
to access the various functions that are supported by Badumna. The primary aim is to provide a simple and
flexible set of operations to the application developers and hide all the complex functionality from them. 
The distributed nature of Badumna allows it to provide very interesting features such as distributed state
synchronisation, distributed controllers
(for hosting NPCs in the network), and distributed validation (performing third-party arbitration in the network). 
Please refer to the <link>user manual</link> for more details. The
identification of this set defines the application requirements of the networking subsystem, facilitating its
use in many different multiuser applications. If you want to know more about the Badumna API, then you can read
the complete |Badumna|.

===================
Server applications
===================
The server applications extend Badumna's core functionality making it suitable for 
multi-user applications ranging from casual games to more complex AAA titles. All server applications are
highly scalable and capable of handling thousands of connections simultaneously. Badumna offers a novel distributed
lookup service that removes a key bottleneck with client-server applications. Using distributed lookup, 
developers do not have to hard code the hostname or IP address of server applications in their client. Clients
get notified of the server details at runtime as part of Badumna's discovery process.

Dei Server
==========
Dei Server provides user authentication and ensures that the network is secure from external attacks.
Apart from authenticating users when they first login to the network, it also issues a range of keys and tokens
that are used by the user for the duration of their session. The keys and tokens provided by Dei Server
enables the users to encrypt the data and gives them permission to participate and join the network, making
the communication channel extremely secure. Tokens are configured to expire periodically.
This setup allows Dei Server to ban users if needed by disabling their accounts which would ensure their tokens do not
get renewed, prohibiting them from joining the network.
User certificates are digitally signed by Dei Server ensuring that users do not tamper with their identity
providing vital identity protection in the system.
All communication with Dei Server is held over a secure SSL channel. Its multi-threaded architecture allows it to
support millions of connections.

.. figure:: ../images/dei.jpg
   :width: 550px
   :alt: security server
   :align: center
   :figwidth: 550px
   
   Figure 1: Dei Server   
   
Dei Server comes with a Dei admin client tool-kit which gives operators a web-based mechanism to manage Dei Server
accounts. The tool-kit comes with complete source code so that developers can add functionality as per their
application needs.

Operators can choose to extend Dei Server functionality in a number of ways. We provide a database abstraction
layer that can be used to interface Dei Server with other custom database applications. We also provide complete
source code for the Dei Server. Developers can use the source code and customise it to suite their application
requirements.

If you wish to learn more about Dei Server - you can read the `user manual
<http://www.scalify.com/pdf/user_manual_142.pdf>`_  or checkout the |Dei API|.


HTTP Server
===========
HTTP server provides a http tunnel for all users that cannot connect to the Badumna network. 
This usually happens when a user is behind a corporate proxy or a firewall that blocks UDP traffic. 
The HTTP server acts as a gateway and provides a tunnel so that Badumna clients can still send and receive
relevant data using HTTP protocol. This application ensures that we are able to connect any user in the 
network irrespective of their network setup.

.. figure:: ../images/http.jpg
   :width: 550px
   :alt: HTTP tunnel
   :align: center
   :figwidth: 550px
   
   Figure 2: HTTP Tunnel  

Overload Server
===============
The Overload server application reduces congestion in the network by providing off-loading services to any
users that are overloaded due to excessive network traffic. It uses a dynamic rate control algorithm to monitor
the upstream and downstream capacity of all the users in the network. Connections experiencing higher than
normal load get off-loaded to the overload server keeping the network congestion free especially during 
flash crowds. The diagram below shows the different steps taken by a node to use the overload service.

.. figure:: ../images/overload-switching.jpg
   :width: 550px
   :alt: overload server
   :align: center
   :figwidth: 550px
   
   Figure 3: Overload Server  

Arbitration server
==================
Arbitration server provides a mechanism to support third-party validation operations such as combat or
e-commerce transactions. It provides a simple interface to define complex game logic. Providing a byte
stream interface, it allows for operations to be highly optimised resulting in a very efficient communications
channel. Arbitration server has built-in support for serialisation so that information is always processed
in the right order. It also uses Badumna's reliable channel to send and receive data making in a reliable
method to execute game logic.

====================
Administration tools
====================
A variety of easy to use tools are provided for managing and monitoring the application. These tools are 
intended for administrators and other developers that may need to run and administer servers during development 
or after launching an application.

Control Center
==============
Control Center is a web-based central administration facility. It provides the ability to install server
applications on remote machines and control them (start/stop/restart). The web interface means that you can 
access the functionality from anywhere as long as you have access to a web browser. 
You can also monitor all your server applications and view different performance graphs. You can even get
notified when a server application goes down.

.. figure:: ../images/control-center.png
   :width: 550px
   :alt: control center
   :align: center
   :figwidth: 550px
   
   Figure 4: Control Center

   
Statistics server
=================
Statistics server records vital game statistics specific to the users such as number of user online in each scene, 
average session time for each user, total number of unique visitors in a given period. Statistics server functionality 
can be accessed out of the box as it has been integrated with the Control Center. Source code for this application is 
provided so that you can extend the functionality to suit your needs.
By changing the data access layer and storing the user statistics to your custom database you have the ability to execute custom 
queries as required.


=================
Network Structure
=================
The figure below shows Badumna's network structure. It is a ring based structure with different services being offered
on each ring. When a new peer joins the network, Badumna will place the peer in an appropriate ring depending on the 
characteristics of the peer and other parameters of the network. Different services are offered on each ring depending
on the position of the ring. For example, the outermost ring may offer services such as chat and service discovery.
The next ring may provide services such as interest management (entity discovery) and object replication.
The inner rings that are usually reserved for trusted peers (operator controlled nodes) provide services such as 
authentication, arbitration, and HTTP tunnelling. Control Center, which is the central administration tool communicates
with the trusted nodes to monitor the network and record its status.

.. figure:: ../images/network-structure.png
   :width: 550px
   :alt: control center
   :align: center
   :figwidth: 550px
   
   Figure 5: Network Structure
   
.. include:: ../apilink-directives.txt
   

