--------------------------------------------------------------------------
Distributed Validation - Developing Cheat-proof Applications Using Badumna
--------------------------------------------------------------------------

============
Introduction
============

Badumna is a technology platform that provides the ability to develop highly scalable multiuser applications.
It achieves this goal by using a hybrid architecture - a combination of client-server and decentralised architectures. 
This model ensures that the technology can scale to support unlimited number of users with minimal resources.
The use of decentralised architecture means that a lot of messages are exchanged within the end-user network without
going through a central authoritative server. This could allow certain malicious users to modify their client and send
incorrect messages to other users of the application. Such an activity would go undetected in a decentralised setup and lead to 
certain malicious users gaining unfair advantage over other regular users in the network. This paper describes the distributed
validation module in Badumna. This module was designed to prevent such malicious users from cheating in a Badumna powered application.

======================
Distributed validation
======================

Badumna uses the concept of validators to support distributed validation. Every entity in Badumna is assigned a validator by a central
trusted authority (such as an Arbitration Server, see...Arbitration servers for more details).  A validator is another entity in the
network typically hosted on a remote machine. A validator is selected such that it is not interested in the original
entity and could be part of a different scene. Validator selection can be customised by the developer in order to improve the level of security 
and also to optimise the network. Validator selection is discussed in more details in section 3.

Once the validator is assigned, the trusted authority will send the initial state of the entity to both the entity and the validator.
This update is digitally signed by the trusted authority hence it cannot be tampered and is recognised as a valid initial state by 
the entity and its validator. The entity will then start sending all its user input to its validator. Upon receiving the input, the 
validator will compute a new state and dispatch it to all interested entities (Fig 1). The validator will ensure that the user input
conforms to the rules of the game. The validator will also ensure that the user is not sending any illegal input or if the user is
sending input at a rate faster than it is allowed in the game. If the validator detects any illegal user input, it will inform the
trusted authority. Periodically, the validator will send the entity state to the entity itself and also to the trusted authority.
The validator will sign the state before sending so that it cannot be tampered. Upon receiving this state update, the local entity will
make any corrections to its state as it relies on client-side prediction to update its local state. This correction ensures that 
its state is accurately synchronised with the rest of the users in the network.

.. figure:: ../images/Distributed-validation-Diagram.png
   :width: 550px
   :alt: distributed validation
   :align: center
   
   **Fig 1: Distributed validation**

If a validator for a given entity goes offline then a new validator is assigned by the trusted authority. This is done in a seamless
manner so that the interested entities do not lose any updates. Once a new validator is assigned, the trusted authority will send
the last received update from the previous validator to the new validator. This state is used as a starting state by the new
validator and the local entity sends its user input from that point in time to the new validator. 

.. figure:: ../images/Validator-selection.png
   :width: 550px
   :alt: distributed validation
   :align: center
   
   **Figure 2: Validator backup**   

Badumna performs further optimisations to ensure that validator migration is handled in a smooth manner.
Typically, when a validator is assigned to a given entity, there is a back-up validator that is also assigned by the trusted authority.
During the normal user session, the validator will exchange information with the backup validator. This includes information such as
list of interested entities and any entity specific information set by the game designer. This ensures that when there is a transition,
the backup validator is ready with all the information that it needs to start processing the state updates with minimal disruption (Fig 2).

The validator processes the input and generate updates in exactly the same manner as it is done by a server in a client-server
architecture. This ensures that application designers are getting the same level of security as a client-server system with
the added benefit of a decentralised architecture.

===================
Validator selection
===================

Badumna provides the application developers with a very flexible mechanism to select validators. Validators can be changed
dynamically at any time during the user session without any interruption to the service. Badumna offers three different options for 
selecting a
validator for a given entity - 

1. the local entity can be assigned to be the validator for itself, 
2. a regular peer in the network can be selected to act as a validator, 
3. or a trusted peer (controlled by the operator) can be assigned to be the validator for the local entity. 

Selecting the local entity as its own validator is the least secure but it is the most efficient in terms of
conserving network traffic. Selecting a trusted peer on the other hand is the most secure but it introduces additional traffic 
to centrally controlled servers.
Depending on the level of security desired, application developers can choose an
appropriate option for validation. They can change the validation option at any time in the game and can do so as many times as they
wish. For example, game developers can choose a validation option depending on the position of a player in a game. If some parts of
the game are meant for chatting and meeting other users and there is no incentive for cheating in this space, then you could choose the first
option for validation. On the other hand, if you have certain areas in the game where you are trading virtual currency with other 
users, then you may want to use the third option. 

================
Reputation model
================

Badumna's validation model can be extended with the use of a reputation scheme. A reputation scheme uses a number of 
application specific parameters to compute a reputation value for each user. The reputation value for a user can change over a 
period of time. Game developers wanting to optimise their game for network traffic can use such a reputation scheme to select the
validation option for a given user. Users' having a very high reputation index can be assigned option 1 for a longer duration of 
time (as they are less likely to cheat) compared to users that have a low reputation index. 

=======
Summary
=======

This article describes the distributed validation feature that is included in Badumna 2.0. :ref:`distributed-validation` section in the reference manual
explains the validation framework in detail and provides two example games that demonstrate how to use distributed validation in your game. 
We expect to improve the distributed validation interface in future updates as we receive feedback from the user community. 

 
