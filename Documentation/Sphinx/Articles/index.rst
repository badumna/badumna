  
Table of Contents
=================


.. toctree::
   :maxdepth: 2
   
   Badumna in 5 Minutes <badumna-in-5>
   Developing Mobile Applications Using Badumna <mobile-apps>
   Technology Overview <technology>
   porting-guide
   Distributed Validation <dist-validation>
   Badumna Benefits <why-choose.rst>
   Network Roaming Recovery <RoamingRecovery/network-roaming-recovery>
