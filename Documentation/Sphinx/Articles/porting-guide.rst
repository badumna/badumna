﻿
----------------------------------------
Porting from a previous Badumna version
----------------------------------------

.. _porting-to-2.3:

2.x to 2.3 Porting Guide
~~~~~~~~~~~~~~~~~~~~~~~~

Badumna 2.3 makes some breaking changes to the API in a few narrow areas.
These changes were made to support new features, eliminate some problems that were commonly encountered with the previous API, and to improve the overall consistency of the API.
Most projects will only require a small number of changes to work with Badumna 2.3.

For projects using a 1.x version of Badumna, you will first need to follow the :ref:`porting-to-2.0` section.

Critical Changes in Badumna 2.3
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Logging out of the Network Facade
=================================

The ``NetworkFacade.Logout()`` method has been removed, as the functionality was redundant.

**Changes Required:**

  #. If you used ``NetworkFacade.Logout()`` you should now call ``NetworkFacade.Shutdown()`` instead.
  #. To log in again, simply create a new ``NetworkFacade`` instance.

NetworkFacade Instance singleton
================================

The ``NetworkFacade.Instance`` property was deprecated in Badumna 2.0, and has now been removed.

Chat
====

The API for using private / proximity chat has been improved to support multiple proximity chat channels and verified character identities (when using Dei). We took this opportunity to simplify the API as well.

**Changes required:**

 #. Specifying a character name for chat must be done at login time. This is done using |NetworkFacade.Login(IIdentityProvider)| or |NetworkFacade.Login(string)|.

 #. ``NetworkFacade.CreateChatService`` has been removed - you now create / access the singleton ``IChatSession`` instance associated with the logged-in character via the |NetworkFacade.ChatSession| property.
 #. ``IChatService`` has been renamed to ``IChatSession``
 #. The following methods previously on ``IChatService`` have changed in ``IChatSession``:
     - |OpenPrivateChannels| and |SubscribeToProximityChannel| no longer take a display name (the currently logged-in character's name is always used)
     - |AcceptInvitation| and |SubscribeToProximityChannel| now return an |IChatChannel| instance (rather than ``void``).
 #. The following methods previously on ``IChatService`` are not present in ``IChatSession``:
     - ``SendChannelMessage``: see |IChatChannel.SendMessage(string)|
     - ``UnsubscribeFromChatChannel``: see |IChatChannel.Unsubscribe|
     - ``SubscribeToChatChannel``: no longer required - use ``AcceptInvitation`` or ``SubscribeToProximityChannel``
 #. ``ChatPresenceHandler`` and ``ChatMessageHandler`` delegates now receive an |IChatChannel| instance rather than a ``ChatChannelId``.
 #. ``ChatChannelId.Proximity`` and ``ChatChannelId.GetProximityChannelId()`` have been removed, as there is no longer any need to construct ``ChatChannelId`` instances in client code.

**Behaviour Changes:**

 #. Proximity chat channels no longer send self (feedback) messages, which is consistent with Private chat. Displaying chat messages sent by the current user should be handled in client code if required.

Dei
===

The API for authenticating with Dei has changed, primarily to support verified characters (which is a new feature of Badumna 2.3). A user may have multiple characters associated with their account, and logs in to Badumna as one of these characters. Character names are currently used in private chat. When using Dei, this ensures that character names are unique, and Badumna will automatically verify the identity of other chat participants.

**Changes required:**

  #. ``ITokenProvider`` has been renamed to ``IIdentityProvider`` to better reflect its purpose. Similarly, ``Dei.DeiTokenProvider`` has been renamed to ``Dei.IdentityProvider``.
  #. Login-related methods (essentially all interaction with Dei) have moved to the new |Dei.Session| class. This class differs from the old ``DeiTokenProvider`` class in a number of ways:
      - It now implements ``IDisposable``, you should ensure you either use it in a ``using`` block or explicitly call its ``Dispose()`` method once it is no longer needed.
      - The constructor does not take a ``sslConnection`` argument, but rather has a settable ``UseSslConnection`` property (it still defaults to true).
      - A |LoginProgressNotification| delegate (if used) is passed in at construction time, instead of being passed to the ``Authenticate`` method.
  #. Login via Dei (using the new |Dei.Session| class) has changed to require the following steps:

      - Authentication (|Dei.Session.Authenticate|)
      - Identity (character) selection (|Dei.Session.SelectIdentity|)
      - Pass the |IIdentityProvider| (obtained via the outref argument of ``SelectIdentity``) to |NetworkFacade.Login(IIdentityProvider)|.

Please see the :ref:`dei-character-management` section and the |Dei.Session| API docs for more details on the new functionality.

Non-critical Changes in Badumna 2.3
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Unverified Identity Provider
============================

A new ``IIdentityProvider`` implementation (|UnverifiedIdentityProvider|) has been added to allow a consistent Login API (|NetworkFacade.Login(IIdentityProvider)|) to be used whether you are using Dei or not.

Keypair Generation
==================

When logging in to Badumna, the client generates an RSA key pair to use for communication (regardless of whether Dei is used). On mobile platforms, this can take some time. In Badumna 2.3, it's now possible to store this key and re-use it for subsequent connections. See :ref:`load-time` for details.

.. _porting-to-2.0:

1.x to 2.0 Porting Guide
~~~~~~~~~~~~~~~~~~~~~~~~

Critical Changes in Badumna 2.0
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Configuration Options
=====================

Configuration options are no longer implicitly loaded from the "NetworkConfig.xml" file.
Instead an instance of the |Options| class must be created and configured with the desired values.
The |Options| class can be configured programmatically by setting its properties, or can it load option values from an XML file.
The |Options| class replaces the old ConfigurationOptions class.

**Change required:**

    *If you previously used NetworkConfig.xml*

    Use the following code snippet to create an |Options| instance from your existing NetworkConfig.xml file.
    See `Creating the Network Facade`_ for further instructions.
    
    .. sourcecode:: c#

        string optionsPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "NetworkConfig.xml");
        Options options = new Options(optionsPath);


**OR:**

    *If you previously used ConfigurationOptions*

    Change your code to construct an instance of |Options| instead of ConfigurationOptions.
    The table below shows the mapping between the properties of the old ConfigurationOptions class and the new |Options| class.
    Note that the old NetworkFacade.ConfigureFrom(...) methods have been removed.
    See `Creating the Network Facade`_ for further instructions.
    
    
    ==========================================      =======================================================================================
    ConfigurationsOptions                           Options
    ==========================================      =======================================================================================
    **AcceptOverload**                              |Overload.IsServer|
    **ArbitrationServers**                          |Arbitration.Servers| (now an IList<|ArbitrationServerDetails|>)
    **BroadcastPort**                               |Connectivity.BroadcastPort|, |Connectivity.IsBroadcastEnabled|
    **DiscoverySource**                             |Connectivity.SeedPeers| (now a List<string>)
    **DiscoveryType**                               n/a, the old NameServer discovery type is no longer supported
    **IsArbitrationEnabled**                        n/a, arbitration is now enabled automatically if |Arbitration.Servers| is not empty
    **IsHostedService**                             |Connectivity.IsHostedService|
    **IsLanTestModeEnabled**                        |Connectivity.ConfigureForLan()|
    **IsOverloadEnabled**                           |Overload.IsClientEnabled|
    **IsOverloadServiceDiscoveryEnabled**           n/a, service discovery is used automatically if |Overload.ServerAddress| is empty
    **IsPersistenceEnabled**                        n/a, the previously deprecated Persistence system has been removed
    **IsPortForwardingEnabled**                     |Connectivity.IsPortForwardingEnabled|
    **IsServiceDiscoveryEnabled**                   n/a, service discovery is enabled automatically if required
    **MaximumPort**                                 |Connectivity.EndPortRange|
    **MinumumPort**                                 |Connectivity.StartPortRange|
    **OverloadPeer**                                |Overload.ServerAddress|
    **PersistenceDirectory**                        n/a, the previously deprecated Persistence system has been removed
    **PersistenceServer**                           n/a, the previously deprecated Persistence system has been removed
    **StunServers**                                 |Connectivity.StunServers|
    **TunnelMode**                                  |Connectivity.TunnelMode|
    **TunnelUris**                                  |Connectivity.TunnelUris| (now an IList<string>)
    ==========================================      =======================================================================================


Creating the Network Facade
===========================

The network facade is no longer implicitly created on the first access to ``NetworkFacade.Instance``.
It must now be explicitly created by a call to |NetworkFacade.Create(Options)|.
If ``NetworkFacade.Instance`` is accessed prior to calling |NetworkFacade.Create(Options)| it will return ``null``.

**Change required:**

    Insert a call to |NetworkFacade.Create(Options)|, passing in your instance of the |Options| class.
    This call must be made before any access to ``NetworkFacade.Instance``.



Setting the Application Name
============================

Because the network facade must now be created explicitly by calling |NetworkFacade.Create(Options)| it is no longer necessary to have separate INetworkFacade.Initialize(...) methods.
These methods have been removed and initialization is now done during the |NetworkFacade.Create(Options)| call.
Previously the application name was set when calling the old Initialize method.
For consistency, the application name setting has now been moved to the |Options| class.

**Changes required:**

    1. Remove the call to NetworkFacade.Instance.Initialize(...)
    2. If an application name was specified, set it on the |Options| instance's |Connectivity.ApplicationName| property prior to calling |NetworkFacade.Create(Options)|, e.g.,
       
       .. sourcecode:: c#
       
           options.Connectivity.ApplicationName = "com.example.app-name";


Updating Peer Harnesses
=======================

As a result of the changes to the way configuration options are set, the constructors for |PeerHarness| have changed.

**Changes required:**

    1. Update code referencing the old PeerHarness() constructor to use the new |PeerHarness(Options)| constructor.
       The |Options| instance passed to the constructor specifies the default options to use for the peer (they can be overridden by the standard Badumna command line options).
    2. Update code referencing the old PeerHarness(INetworkFacade,IDeiConfigManager) constructor to use the new |PeerHarness(NetworkFacadeFactory,Options,IDeiConfigManager)| constructor.
       |NetworkFacadeFactory| is a delegate that takes an |Options| instance and returns an |INetworkFacade|.
       This delegate will be called by the |PeerHarness| when it needs to create the network facade.
       Normally |NetworkFacade.Create| should be passed for this unless you have special requirements.
       The |Options| instance specifies the default options as indicated for the |PeerHarness(Options)| constructor above.
       The |IDeiConfigManager| parameter is unchanged from the 1.x series.


Granting Permissions for Service Accounts
=========================================

A small change is required if you're using Dei and running service peers that are located using distributed lookup (e.g. overload or arbitration servers).
Peers that announce themselves through the distributed lookup system now require Announce permission.
This ensures that malicious peers cannot intercept traffic by announcing themselves as service peers.

**Change required:**

    *Grant Announce permission to the user account(s) used by your service peers.
    This can be done in Control Center by clicking the "Show status" link next to the running Dei service, logging in to Dei, and editing the appropriate user.
    See section 9.9.1 "Starting Dei Server" in the user manual for further details.*


Non-critical Changes in Badumna 2.0
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The ``NetworkFacade.Instance`` property has been deprecated and will be removed in a future version.
Instead of using the ``NetworkFacade.Instance`` property, the application should maintain a reference to the network facade returned by the new |NetworkFacade.Create(Options)| method.
The network facade should be passed to other objects that require it.

**Changes required:**

    1. Keep a reference to the network facade object returned by |NetworkFacade.Create(Options)|.
    2. Eliminate all references to ``NetworkFacade.Instance``, instead use the network facade object from step 1
       (this may require, e.g., passing this object to the constructors of other classes that require it).


.. include:: ../apilink-directives.txt

