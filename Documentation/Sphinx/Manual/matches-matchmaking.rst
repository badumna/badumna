.. include:: ../apilink-directives.txt

.. _match-matchmaking:

Badumna Matches: Matchmaking
============================

If you want to make mini-games with small number of players, you can use Badumna Cloud's matchmaking service.
Matchmaking is ideal to invite a small sub-set of players to join a match based on a set of criteria.

Creating a match
~~~~~~~~~~~~~~~~

To create a match, first create a ``MatchmakingCriteria`` instance and configure it as required.
You can optionally specify an application-defined match category, give your match a name, and set you match to be private or public.
This information will be sent to the Badumna Cloud matchmaking service to allow other players to find and join your match.
See the ``MatchmakingCriteria`` :netapi:`API docs 907D3C4E` for more information.

You can then create a match using the :netapi:`NetworkFacade.Match.CreateMatch B0E8884C` method.

.. sourcecode:: c#
    
    var criteria = new MatchmakingCriteria
        {
            Category = 0 // Matches any category.
        };

    this.match = this.network.Match.CreateMatch
        criteria, 
        4, // Max players
        this.playerName,
        this.CreateReplicaDelegate,
        this.RemoveReplicaDelegate);

The third and fourth arguments are delegates for creating and removing replicas of entities from other members of the match, which will be explained in  :ref:`match-entity-replication`.

        
Joining an existing match
~~~~~~~~~~~~~~~~~~~~~~~~~

To joining an existing match, you must first search for available matches.
Create a ``MatchmakingCriteria`` instance to filter for the kind of match you want to join,
and then get a list of all available matches using :netapi:`NetworkFacade.Match.FindMatches 74BAAF2A`.

.. sourcecode:: c#

    var criteria = new MatchmakingCriteria
        {
            Category = 0 // Matches any category.
        };

    this.network.Match.FindMatches(criteria, this.HandleMatchQuery);

When the find matches query has completed, ``HandleMatchQuery`` will be invoked with the results.
It will return an empty list if there are no matches found.
Otherwise you can join one of the existing matches by calling :netapi:`NetworkFacade.Match.JoinMatch C4651F61`:

.. sourcecode:: c#

    private var HandleMatchQuery(IEnumerable<MatchmakingResult> results)
    {
        foreach (var result in results)
        {
            this.match = this.network.Match.JoinMatch(
                result,
                this.playerName,
                this.CreateReplicaDelegate,
                this.RemoveReplicaDelegate);
            return;
        }
    }

Understanding match status
~~~~~~~~~~~~~~~~~~~~~~~~~~

A match has a designated host.
If the host leaves the match, one of the other members will automatically take over as host.
A member's status will be either: Initializing, Hosting, Connected, Connecting or Closed.
The current state of a match is available via its ``Status`` property.
When the match status is changed, the ``StatusChanged`` event will be raised.
Subscribe to that event to handle match closure, or becoming host:

.. sourcecode:: c#

    this.match.StateChanged += this.OnMatchStatusChange;

    private void OnMatchStatusChange(MatchStatus status, string message)
    {
        if (status == MatchStatus.Closed)
        {
            this.match = null;
        }
    }

See the ``MatchStatus`` :netapi:`API docs 7D9FFA` to get more information for each individual status.

Leave a match
~~~~~~~~~~~~~~

To leave a match, call :netapi:`Match.Leave 4032638D`.

.. sourcecode:: c#

    this.match.Leave();
    
Any regular entities registered with the match will be removed.

Try it out
~~~~~~~~~~~

See matchmaking in action:

- :ref:`Unity Matchmaking Tutorial<unity-match-tutorial1>`

