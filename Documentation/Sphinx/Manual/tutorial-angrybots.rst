.. _angrybots-tutorial:

AngryBots tutorial
~~~~~~~~~~~~~~~~~~

The complete demo project is available for download from the `Unity Asset Store <https://www.assetstore.unity3d.com/#/content/11356>`_ .
This tutorial explains how to convert the single player AngryBots demo into a multiplayer game using Badumna Cloud.

You will need a Badumna Cloud account and a valid application identifier to test this game.
If you do not have a Badumna Cloud account, you can sign up for a free account using the Badumna Cloud wizard window that is displayed when you open the project (see figure below).
Once you sign-up you will be provided with an application identifier.
Alternatively, you can set the application identifier with your own id created on `cloud.badumna.com <https://cloud.badumna.com>`_ (see :ref:`chapter:get-started`).

.. _fig:BadumnaCloudSignup-AngryBots:

.. figure:: ../images/sign-up.png
   :align: center
   
   Badumna Cloud Signup Window

Once you have an application identifier, click the "Setup" button in the signup window. 
Enter your application identifier and then click "Save".
Make sure you load the scene:

:menuselection:`File --> Open Scene`

You are now ready to build the game and test the multiplayer functionality.

.. admonition:: Tutorial pre-requisites

   It is recommended to read the guide to :ref:`badumna-matches` before reading this tutorial.

We will now explain the different scripts added to the demo in order to implement multiplayer functionality.
This will provide an understanding on how to add multiplayer functionality to an existing game using Badumna Cloud.

This tutorial will cover 

1. Match basic functionality (i.e. creating, joining or finding matches)
2. Match basic replication
3. Match RPC for Match controller and entity.
4. Match Chat system.
5. Match Hosted Entity

GameManager.cs
--------------

:ref:`GameManager <gamemanager-angrybots>` consists of helper methods use for keep tracking the list of Players and NPCs in the game.

BadumnaCloud.cs
---------------

:ref:`BadumnaCloud <badumnacloud-angrybots>` is responsible for managing the Badumna network.
This involves initializing, logging in and shutting down the network. 

Initializing and shutting down network
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Begin asynchronous Badumna network initialization process when the script is loaded, see ``Awake`` method.

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/AngryBots/BadumnaCloud.cs.badumna_initialization.rst

You will need to shut down the network before quitting the game.

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/AngryBots/BadumnaCloud.cs.shutting_down_badumna.rst

Login to the network
^^^^^^^^^^^^^^^^^^^^

Only login to Badumna if the network initialization process is complete.

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/AngryBots/BadumnaCloud.cs.login_badumna.rst

Regularly update Badumna
^^^^^^^^^^^^^^^^^^^^^^^^
You need to regularly update Badumna by calling ProcessNetworkState.
This is done in the Update method.

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/AngryBots/BadumnaCloud.cs.FixedUpdate_method.rst

BaseBadumna.cs
--------------

:ref:`BaseBadumna <basebadumna-angrybots>` is a base class for any classes that require to call any Badumna API.
All the classes derived from BaseBadumna will automatically find the BadumnaCloud instance in the game when is loaded.

Player.cs
---------

List of player's properties that need to be synchronized,

1. Player's position
2. Player's orientation
3. Player's name
4. Actions: firing

Player class is a replicable entity that used for local and remote players, the ``Local`` field is added to distinguish between those two types.
All properties that tagged with ``Replicable`` attribute will be automatically updated by Badumna regularly.
The ``Get`` method is called by Badumna regularly to obtain the local player's current properties 
and the ``Set`` method will be called on the relevant remote player to set the property values accordingly.
Learn more about replication in the :ref:`match-entity-replication`

The following code list all the replicable player properties.

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/AngryBots/Player.cs.replicable_properties.rst

HostedEntity.cs
---------------

:ref:`HostedEntity <hostedentity-angrybots>` is a base class for any hosted entities type in the game.
In this case there are only two types of hosted entities; Enemy.cs and Prop.cs.

Enemy.cs
--------

Similar to Player class, Enemy class is a replicable entity that used for local and remote hosted entities, the ``Local`` field is used to distinguish between those two types.
This class is derived from HostedEntity class.

List of enemy's properties that need to be synchronized.

1. Enemy's position
2. Enemy's orientation

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/AngryBots/Enemy.cs.replicable_properties.rst


List of enemy's RPC methods.

1. AnimateDeath()
2. ExplodeEffect()

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/AngryBots/Enemy.cs.replica_RPC_method.rst

Prop.cs
-------

Prop class is also derived from HostedEntity class and used to replicate the game's prop such as terminal hack door in this case.

List of prop's RPC methods.

1. AnimateDeactivate()

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/AngryBots/Prop.cs.RPC_method.rst

Menu.cs
-------

:ref:`Menu <menu-angrybots>` class is used to draw the match GUI based on the current state of the game.
For example, it should draw the Lobby UI when the current game state is ``GameState.Lobby`` and so on.
There is no specific Badumna API call on this class, the rest of the codes should be self explained.

Game.cs
-------

Earlier if the ``Menu`` class is used for drawing an appropriate GUI, the :ref:`Game <game-angrybots>` class is responsible for controlling whatever action is passed from the ``Menu`` class.
It is used for managing basic Badumna match functionality such as creating, finding and joining matches.
Please refer to :ref:`badumna-matches` to learn more.

Creating a new match
^^^^^^^^^^^^^^^^^^^^

Call ``BadumnaCloud.Match.CreateMatch<MatchController>`` to create a new match.

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/AngryBots/Game.cs.CreateMatch_method.rst

Please notes that we will explain about ``MatchController`` next.

Finding matches
^^^^^^^^^^^^^^^

Call ``this.BadumnaCloud.Match.FindMatches`` to find available matches.

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/AngryBots/Game.cs.FindMatch_method.rst


Joining an existing match
^^^^^^^^^^^^^^^^^^^^^^^^^

Call ``this.BadumnaCloud.Match.JoinMatch<MatchController>`` to join into an existing match.

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/AngryBots/Game.cs.JoinMatch_method.rst

MatchController.cs
------------------

:ref:`MatchController <matchcontroller-angrybots>` class is being replicate to all the members in the match.
Use this class to send RPC from client to host and vice versa.

List of MatchController RPC methods.

1. StartMatch()
2. EndMatch()
3. KickMember(string)
4. AddScore(string)

In this case, there is no replicable property is required. 

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/AngryBots/MatchController.cs.RPC_methods.rst


PlayerManager.cs
----------------

:ref:`PlayerManager <playermanager-angrybots>` class is responsible for registering and unregistering local player, registering and unregistering hosted entities, and creating and removing replica into and from the match.

Register local player
^^^^^^^^^^^^^^^^^^^^^

Registering local player with the match.

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/AngryBots/PlayerManager.cs.RegisterEntity_method.rst

Unregister local player
^^^^^^^^^^^^^^^^^^^^^^^

Unregister local player from the match.

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/AngryBots/PlayerManager.cs.UnregisterEntity_method.rst

Register hosted entity
^^^^^^^^^^^^^^^^^^^^^^

Registering the enemy (NPC) and props into the match only if the peer is a host.

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/AngryBots/PlayerManager.cs.RegisterEnemy_method.rst
.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/AngryBots/PlayerManager.cs.RegisterProps_method.rst


Unregister hosted entity
^^^^^^^^^^^^^^^^^^^^^^^^

Unregister hosted entity from the match, can be done through ``UnregisterHostedEntity`` to unregister all hosted entities or `UnregisterEntity` to unregister a specific entity.

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/AngryBots/PlayerManager.cs.UnregisterHostedEntity_method.rst

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/AngryBots/PlayerManager.cs.UnregisterEnemy_method.rst


Creating and removing replicas delegates
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The following methods are the creation and removal of replica delegates.

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/AngryBots/PlayerManager.cs.replica_delegates.rst

Chat.cs
-------

:ref:`Chat <chat-angrybots>` class is used to display the Chat GUI on the screen and handle sending and receiving chat messages in the match.

Receive Chat Message
^^^^^^^^^^^^^^^^^^^^

In order to receive match chat messages, you should subscribe to ``Match.ChatMessageReceived`` event.
Passed ``HandleChatMessage`` delegate to ``Match.ChatMessageReceived`` event.

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/AngryBots/Chat.cs.Awake_method.rst

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/AngryBots/Chat.cs.HandleChatMessage_method.rst

Send Chat Message
^^^^^^^^^^^^^^^^^

Call ``Match.Chat`` for sending the chat messages.

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/AngryBots/Chat.cs.SendChatMessage_method.rst


js Scripts
----------

In order to fully integrate Badumna with the existing AngryBots demo, a couple of original JavaScript scripts need to be updated.
Refer to this :ref:`page <angrybots-sources>` to see the list of all JavaScript scripts that need modification.
