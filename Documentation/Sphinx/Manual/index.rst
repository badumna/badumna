
.. toctree::
   :hidden:
   
   Badumna Matches <matches-overview>
   Badumna Scenes <scenes-overview>
   Friends <connect-with-friends>
   Trusted Servers <arbitration>
   Management <management>
   Deadreckoning <smoothing>
   Setting Up Pro <setting-up-pro>
   Setting Up Cloud <setting-up-cloud>

   Using Badumna with Unity <unity-getting-started>
   Optimisation <optimise>
   Residential network setting <router>
   
   Unity Tutorials <unity-tutorials>   

   Badumna C++ <cpp>

   Known Issues <knownissues>
   Change History <changes>

.. _chapter:how-to:

Badumna Guide
=============

This guide provides a brief introduction to the features offered by Badumna, and shows how to use them in your game.
Please note that the features marked with [Pro] are not supported by Badumna Cloud. 
They are intended for `Badumna Pro <http://www.scalify.com/features.php>`_ only.

.. tip::

   If you're using the C++ API then start by reading :ref:`cplusplus` which explains the C++ package and its contents.
   Unity developers may want to read :ref:`chapter:unity` which explains the contents of the Badumna-Unity SDK and all the relevant platform settings.


   
Badumna offers two distinct ways to implement online multiplayer functionality - **Matches** and **Scenes**. 

Badumna Matches
----------------

:ref:`badumna-matches` are ideal for casual games where you want to match a fixed number of players (based on certain criteria) and connect them in a match.
In these types of game, you will typically have lots of individual matches in progress with each match having a small number of players. 
If you are developing such a game then you should use :ref:`badumna-matches`, which support:

  - Matchmaking
  - Entity replication
  - Dead reckoning
  - Remote procedure calls
  - In-match chat
  - Hosted NPCs

Create a massively scalable open world
--------------------------------------

:ref:`badumna-scenes` are ideal for games such as MMOs that have large worlds with each world potentially hosting thousands of players at any one time.
Each player however, interacts with only what they can see in their region of interest.
If your game consists of one or more such worlds then you should use :ref:`badumna-scenes`, which offer:

  - Entity replication and interest management
  - Dead reckoning
  - Remote procedure calls
  - Proximity chat
  - Private chat and presence [Pro]
  - Distributed NPCs [Pro]
  - Distributed validation [Pro]

.. admonition:: Using both methods in a game

   You can use both :ref:`badumna-scenes` and :ref:`badumna-matches` in a game.
   For example, you can use Scenes to design a large virtual world and within the virtual world you can use Matches to offer mini games where groups of players can compete with each other.


Optimising a Badumna powered application
----------------------------------------

These are couple of tips for optimising your game to improve efficiency and performance especially when you are developing games on mobile platforms.

 - :ref:`Reducing load time<load-time>`
 - :ref:`Recovering from a network failure<network-recovery>`


Connect with friends [Pro]
--------------------------

To let players :ref:`connect with friends <connect-with-friends>`, be notified when they're online and exchange private messages, you can run a secure Badumna network with verified player identities, and take advantage of the presence and private chat features:

 - :ref:`Verified identities [Pro]<verified-identities>`
 - :ref:`Presence [Pro]<private-chat>`
 - :ref:`Private chat [Pro]<private-chat>`

Trusted servers [Pro]
---------------------

Badumna lets you connect to :ref:`trusted servers <trusted-servers>` with reliable messaging, which can be used to provide:

 - Persistent data storage [Pro]
 - Game logic arbitration [Pro]
 
Manage your network [Pro]
-------------------------

:ref:`Managing your network <management>` is easy with Badumna's Control Center.
Badumna Pro also supports additional central services such as a statistics server for tracking players in the game, an overload server to help players with low bandwidth, and an HTTP tunnel server to help players behind corporate firewalls that block UDP:

 - :ref:`Control center [Pro]<control-center>`
 - :ref:`Seed peer [Pro]<seed-peer>`
 - :ref:`Statistics server [Pro]<statistics-server>`
 - :ref:`Overload servers [Pro]<overload>`
 - :ref:`HTTP tunnel server [Pro]<tunnel>`

Game development on a residential network [Pro]
-----------------------------------------------

Some useful information on how to :ref:`setup your residential network<chapter-router>` when developing games with Badumna.
 
 - :ref:`Universal Plug and Play (UPnP)<upnp>`
 - :ref:`Port forwarding<port-forwarding>`
 - :ref:`LAN test mode<lan-test>`
 - :ref:`Default port number<portnumbers>`

Known issues in Badumna
-----------------------

A couple of known issues in Badumna are highlighted in this :ref:`section<known-issues>`.

Change History
--------------

 - :ref:`Badumna 3.2<badumna-3.2>`
 - :ref:`Badumna 3.1.1<badumna-3.1.1>`
 - :ref:`Badumna 3.1<badumna-3.1>`
 - :ref:`Badumna 3.0<badumna-3.0>`
 - :ref:`Badumna 2.3.3<badumna-2.3.3>`
 - :ref:`Badumna 2.3.2<badumna-2.3.2>`
 - :ref:`Badumna 2.3.1<badumna-2.3.1>`
 - :ref:`Badumna 2.3<badumna-2.3>`
 - :ref:`Badumna 2.0<badumna-2.0>`
 - :ref:`Badumna 1.4.3<badumna-1.4.3>`
 - :ref:`Badumna 1.4.2<badumna-1.4.2>`
 - :ref:`Badumna 1.4.1<badumna-1.4.1>`
 - :ref:`Badumna 1.4<badumna-1.4>`
