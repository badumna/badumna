.. _unity-scene-tutorial4:

Unity tutorial - Verified Identity
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. admonition:: Pre built package available (Badumna Pro)

   The instructions below will guide you in creating a multiplayer game using Badumna.
   If you would prefer to see the result immediately, the SDK contains a completed package:
   
   ``Demo\Scene-Demo4-VerifiedIdentity(Pro).unitypackage``
   
   You only need to configure Badumna network and select the correct assembly in the completed demo.

This tutorial builds on the simple game from tutorial 3.
We'll add verified identity, where each player will have to login through Dei server (authentication server) before joining the the game.

.. include:: tutorial-requirements-unity-badumna-pro.txt

**1. Open the project created in tutorial 3.**

Alternatively, you can use the completed tutorial 3 solution included in the Badumna Pro Unity SDK.

**2. Edit the Network.cs:**

We are going to use ``NetworkFacade.Login(IIdentityProvider)`` method to login to Badumna with verified identity provider obtained from Dei server.
Also use the character name instead of the user name for the player name.
Refer to the highlighted lines in the following Start method codes.

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Scene-Demo4-VerifiedIdentity(Pro)/Network.cs.Start_method_(Dei).rst

**3. Edit the NetworkConfiguration.cs:**

Added a public field for DeiServer address.

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Scene-Demo4-VerifiedIdentity(Pro)/NetworkConfiguration.cs.Demo_Scene-4_Fields.rst


**4. Edit the LoginScreen.cs:**

Add the following using statement at the top of LoginScreen.cs.

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Scene-Demo4-VerifiedIdentity(Pro)/LoginScreen.cs.using_statements.rst

Add the following private fields for storing the user password and the Dei related stuff.

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Scene-Demo4-VerifiedIdentity(Pro)/LoginScreen.cs.dei_fields.rst

Also add the following properties,

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Scene-Demo4-VerifiedIdentity(Pro)/LoginScreen.cs.dei_properties.rst

Edit the Start method and add the highlighted lines as shown below:

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Scene-Demo4-VerifiedIdentity(Pro)/LoginScreen.cs.Start_method.rst

Replace the previous OnGUI method with the following block of codes:

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Scene-Demo4-VerifiedIdentity(Pro)/LoginScreen.cs.OnGUI_method.rst

Add the password field on the login window, by adding the highlighted lines into LoginWindow method.

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Scene-Demo4-VerifiedIdentity(Pro)/LoginScreen.cs.LoginWindow_method.rst

Then, replace the DoLogin method with the following codes:

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Scene-Demo4-VerifiedIdentity(Pro)/LoginScreen.cs.DoLogin_method.rst

Finally, add a couple of new methods that are used for managing user character through Dei authentication server.

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Scene-Demo4-VerifiedIdentity(Pro)/LoginScreen.cs.Character_management_methods.rst

**5. Build and run the game**

.. include:: build-and-run-unity.txt

- Configure the dei server address from NetworkConfiguration in Game Manager Inspector window.

.. include:: build-and-run-unity-badumna-pro.txt

Run the Dei server.
We have included a pre-configured version of Dei Server to be used for testing.
To start this Dei Server simply execute ``DeiServer.exe`` which is located in ``Demo/Binaries`` folder.

Build and run a couple of instances of the game to try out dei authentication.
You will have to enter a valid username and password at the login screen.
Refer to the file ``DeiAccounts.s3db.readme`` for a list of accounts that have been created for testing.
