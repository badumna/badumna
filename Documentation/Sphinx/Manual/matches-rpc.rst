.. include:: ../apilink-directives.txt

.. _match-rpc:

Badumna Matches: Remote Procedure Calls (RPCs)
==============================================

.. include:: rpc-intro.txt

Defining RPCs
-------------

A method on a match entity can be marked as being remotely callable with the |Replicable| attribute.
RPC methods can have up to six parameters of any type, and should have a return type of ``void``.

.. sourcecode:: c#

    public class Avatar
    {
        [Replicable]
        public void DoSomething(int number, string word)
        {
            // ...
        }
    }

.. include:: rpc-signature-registration.txt

Making RPCs
-----------

To call a method on all replicas of an original entity in a match, pass a delegate for the method *on the original entity instance* and desired parameters to the match's ``CallMethodOnReplicas`` method:

.. sourcecode:: c#

    this.match.CallMethodOnReplicas(this.localAvatar.DoSomething, 7, "foo");

To call a method on the original entity of a replica in a match, use the match's ``CallMethodOnOriginal`` method, and pass a delegate for the method *on the replical entity instance:*

.. sourcecode:: c#

    this.match.CallMethodOnOriginal(replica.DoSomething, 7, "foo");

.. tip::

    If you need to know who sent an RPC, add a parameter of type :netapi:`MemberIdentity 6201A21D` to the RPC method.
    Use the :netapi:`MemberIdentity BAFD586B` property on the :netapi:`Match ACB1590` instance when sending the RPC.


Try it out
----------

See match-based RPCs in action:

- :ref:`Unity Match RPC Tutorial<unity-match-tutorial4>`
