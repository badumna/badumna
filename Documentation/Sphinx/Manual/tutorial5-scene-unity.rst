.. _unity-scene-tutorial5:

Unity tutorial - Arbitration Server
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. admonition:: Pre built package available (Badumna Pro)

   The instructions below will guide you in creating a multiplayer game using Badumna.
   If you would prefer to see the result immediately, the SDK contains a completed package:
   
   ``Demo\Scene-Demo5-ArbitrationServer(Pro).unitypackage``
   
   You only need to configure Badumna network and select the correct assembly in the completed demo.

This tutorial builds on the previous tutorial 4.
We'll add arbitration server that will be used to obtain friend list from the server.

.. include:: tutorial-requirements-unity-badumna-pro.txt

.. spelling::
    friendList

**1. Open the project created in tutorial 4.**

Alternatively, you can use the completed tutorial 5 solution included in the Badumna Pro Unity SDK.

**2. Copy DemoArbitrationEvent source codes**

Copy DemoArbitrationEvent source codes; FriendListEventSet.cs, FriendListReply.cs and FriendListRequest.cs from ``Demo\Source\DemoArbitrationEvents`` into your unity project directory, as this codes will be used later on this tutorial.

**3. Edit the NetworkConfiguration.cs:**

Added public fields for storing Arbitration server settings.

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Scene-Demo5-ArbitrationServer(Pro)/NetworkConfiguration.cs.Demo_scene-5_fields.rst

And, added the highlighted line into GenerateNetworkConfiguration method.

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Scene-Demo5-ArbitrationServer(Pro)/NetworkConfiguration.cs.GenerateNetworkConfiguration_method_(Arbitration_server).rst

**4. Add FriendList.cs:**

Add the following class into your unity project.

.. literalinclude:: ../../../Source/Unity/Unity/Scripts/Generated/Scene-Demo5-ArbitrationServer(Pro)/FriendList.cs
    :language: c#

.. _friendlist-scene-demo-5:

**5. Edit GameManager.cs:**

Add friendList field into GameManager class:

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Scene-Demo5-ArbitrationServer(Pro)/GameManager.cs.Demo_scene-5_fields.rst

Add the highlighted line below into Initialize method

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Scene-Demo5-ArbitrationServer(Pro)/GameManager.cs.Initialize_method_(ArbitrationServer).rst

And the highlighted lines below under Shutdown method

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Scene-Demo5-ArbitrationServer(Pro)/GameManager.cs.Shutdown_method_(ArbitrationServer).rst

**6. Edit Scene.cs:**

Add the highlighted line below under JoinScene method, requesting a friend list from server as soon as the player join the scene.

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Scene-Demo5-ArbitrationServer(Pro)/Scene.cs.JoinScene_method_(ArbitrationServer).rst


**7. Build and run the game**

.. include:: build-and-run-unity.txt

- Configure the dei server address from NetworkConfiguration in Game Manager Inspector window.
- Configure the friend server address from NetworkConfiguration in Game Manager Inspector window.

.. include:: build-and-run-unity-badumna-pro.txt

Steps required:

- Run the Dei server.
  See the previous tutorial on how to start the Dei server.
- Run the  FriendListArbitrationServer included in the Badumna Pro Unity SDK.
  * Start the FriendListArbitrationServer using **unity-demo** as the application name.
  * The name of the arbitrator used must match the arbitrator name specified in the configuration of the Arbitration server application. In this case, ‘friendserver’ is used as the name of the arbitrator.
  * Make sure that the FriendListArbitrationServer uses dei server.
- Build and run a couple of instances of the game.

.. hint::

  Use the following command line to start the FriendListArbitration server,

    .. container:: commandline
   
      ``FriendListArbitrationServer.exe --dei-config-file=FriendListArbitrationServer.deiconfig --application-name=unity-demo``