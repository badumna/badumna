.. _gamemanager-template-cloud:

GameManger template script - Badumna Cloud
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. literalinclude:: ../../../Source/Unity/Unity/Scripts/Generated/BadumnaCloud-Scene/GameManager.cs
    :language: c#