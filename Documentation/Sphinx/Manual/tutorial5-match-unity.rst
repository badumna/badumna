
.. _unity-match-tutorial5:

Unity tutorial - Match Chat
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. admonition:: Pre built package available (BadumnaCloud)

   This tutorial shows how to add Badumna match chat functionality step by step.
   If you would prefer to see the result immediately, the SDK contains a completed package:
   
   ``Unity Packages\Match-Demo5-Chat.unitypackage``
   
   You only need to specify your cloud application identifier and select the correct assembly in the completed demo.

.. admonition:: Pre built package available (Badumna Pro)

   This tutorial shows you how to add Badumna match functionality from scratch.
   If you would prefer to see the result immediately, the SDK contains a completed package:
   
   ``Demo\Match-Demo5-Chat(Pro).unitypackage``
   
   You only need to configure Badumna network and select the correct assembly in the completed demo.

This tutorial builds on the simple game from tutorial 4.
We'll add match chat, where each player will receive chat messages sent by other players in the match.

.. include:: tutorial-requirements-unity.txt

**1. Open the project created in tutorial 4.**

You can either open the project that you completed following the tutorial :ref:`Unity tutorial - Match Remote Procedure Call<unity-match-tutorial4>` or
you can open the completed tutorial solution included in the BadumnaCloud or Badumna Pro Unity SDK.


**2. Edit Matchmaking.cs:**

Add the highlighted code to ``HandleMatchStart`` method inside the Matchmaking class to initialize the chat system when the match starts.

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Match-Demo5-Chat/Matchmaking.cs.HandleMatchStart_method_(Chat).rst

Add the following highlighted line to ``HandleMatchEnd`` method to remove the chat system when the match is ended.

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Match-Demo5-Chat/Matchmaking.cs.HandleMatchEnd_method_(Chat).rst

**3. Edit Chat.cs:**

The BadumnaCloud package includes a :ref:`Chat template script<chat-match-template>`, which has a useful stubs method in it.

Add the a field to the script referencing the Match object:

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Match-Demo5-Chat/Chat.cs.Demo_Match-4_Field.rst

You'll also need these fields which are used for displaying chat messages:

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Match-Demo5-Chat/Chat.cs.Demo_3_Fields.rst

Edit the ``Start`` method and replace its body with the following block of code.
You need to subscribe to the ``ChatMessageReceived`` event from the match object. 

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Match-Demo5-Chat/Chat.cs.Start_method.rst

Unsubscribe from the ``ChatMessageReceived`` event ``OnDisable``.

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Match-Demo5-Chat/Chat.cs.OnDisable_method.rst

Edit the ``SendMessage`` method to send a text message through the match object:

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Match-Demo5-Chat/Chat.cs.SendMessage_method.rst

The ``HandleChatMessage`` method needs to store the received messages and scroll the chat window down to show it:

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Match-Demo5-Chat/Chat.cs.HandleMessage_method.rst

We will add methods for creating the window and responding to user input:

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Match-Demo5-Chat/Chat.cs.OnGUI_method.rst

**4. Build and run the game**

.. include:: build-and-run-unity.txt

Build and run a couple of instances of the game to try out match chat.

.. admonition:: Badumna Pro only !!!

  .. include:: build-and-run-unity-match-badumna-pro.txt