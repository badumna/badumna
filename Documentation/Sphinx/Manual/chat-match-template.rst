.. _chat-match-template:

Chat template script
~~~~~~~~~~~~~~~~~~~~

.. literalinclude:: ../../../Source/Unity/Unity/Scripts/Generated/BadumnaCloud-Match/Chat.cs
    :language: c#