
.. _unity-match-tutorial2:

Unity tutorial - Match controller
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. admonition:: Pre built package available (BadumnaCloud)

   This tutorial shows how to add Badumna match controller functionality step by step.
   If you would prefer to see the result immediately, the SDK contains a completed package:
   
   ``Unity Packages\Match-Demo2-Controller.unitypackage``
   
   You only need to specify your cloud application identifier and select the correct assembly in the completed demo.

.. admonition:: Pre built package available (Badumna Pro)

   This tutorial shows you how to add Badumna match functionality from scratch.
   If you would prefer to see the result immediately, the SDK contains a completed package:
   
   ``Demo\Match-Demo2-Controller(Pro).unitypackage``
   
   You only need to configure Badumna network and select the correct assembly in the completed demo.

In this Unity tutorial you will learn how to add a match controller to your game.
A match controller is useful to control global game state such as starting/stopping matches, maintaining team scores, rankings, etc.  
This demo will show how to use match controllers to start a match when all the members are ready and then stop the match after a fixed duration.
This tutorial builds on the previous tutorial - :ref:`Unity tutorial - Matchmaking<unity-match-tutorial1>`. 


.. include:: tutorial-requirements-unity.txt

**1. Open the Unity project from the previous tutorial**

You can either open the project that you completed following the tutorial :ref:`Unity tutorial - Matchmaking<unity-match-tutorial1>` or
you can open the completed tutorial solution included in the BadumnaCloud or Badumna Pro Unity SDK.

**2. Edit MatchController.cs:**

Add a couple of fields and properties to the ``MatchController`` class.

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Match-Demo2-Controller/MatchController.cs.Demo_Match-1_Fields.rst

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Match-Demo2-Controller/MatchController.cs.Demo_Match-1_Properties.rst

Add the following RPC methods that will be used to indicate whether a match is about to start or end and when a new match member is ready.

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Match-Demo2-Controller/MatchController.cs.Match_controller_RPCs.rst

Add the highlighted line to the ``RemoveMember`` callback to ensure that the match state is consistent and can only be started if all the members are ready.

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Match-Demo2-Controller/MatchController.cs.RemoveMember_method_(Controller).rst

**3. Edit Matchmaking.cs:**

Add the highlighted code to the ``OnMatchStatusChange`` callback to subscribe to the ``OnMatchStarted`` and ``OnMatchEnded`` events.

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Match-Demo2-Controller/Matchmaking.cs.OnMatchStatusChange_method_(Controller).rst

Add the callback methods used for handling those events.

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Match-Demo2-Controller/Matchmaking.cs.HandleMatchStart_method.rst

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Match-Demo2-Controller/Matchmaking.cs.HandleMatchEnd_method.rst

Finally, add the following highlighted code to the ``MatchmakingWindow`` method for displaying 'Start' and 'Ready' buttons on the host and client respectively.
Add a 'Restart match' button if the peer is currently the host and also a 'Back to lobby' button to all the clients when the match is finished.

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Match-Demo2-Controller/Matchmaking.cs.MatchmakingWindow_method_(Controller).rst

**4. Build and run the game**

.. include:: build-and-run-unity.txt

Build and run a couple of instances of the game to try out the match controller RPC functionality.

.. admonition:: Badumna Pro only !!!

  .. include:: build-and-run-unity-match-badumna-pro.txt


