
.. include:: ../apilink-directives.txt

.. _smoothing:

Appendix II: Smoothing
======================

Badumna allows you to smooth replicated property values by **interpolating** between recent updates.
It also supports **extrapolation** of replicated property values based on recent updates (dead reckoning).

Smoothing is supported for any replicable property on an entity or match controller of the type ``float``, ``double`` or ``Badumna.DataTypes.Vector3``, including ``IReplicableEntity.Position``.

This allows you, for example, to render smooth animation even though updates are periodic and can be delayed or dropped.

To use smoothing, add the ``[Smoothing]`` attribute to your replicable property, and specify the number of milliseconds of interpolation and extrapolation you want to use:

.. sourcecode:: c#

    [Replicable]
    [Smoothing(Interpolation = 200, Extrapolation = 0)]
    public Vector3 Position { get; set; }

If you do not specify any interpolation or extrapolation values along with the smoothing attribute then Badumna will use default values of 200 and 0 for interpolation and extrapolation respectively.

For more information see |SmoothingAttribute|.

Bandwidth conservation
----------------------

Smoothing can also be used to save bandwidth in certain scenarios.
For instance, when the rate of change of an entity's property is constant for periods of time, then replicating the derivative (rate of change) along with the property means updates only need to be sent when the derivative changes.
For example, when an entity often moves at a constant velocity for a period, it is possible to save bandwidth by replicating position and velocity only when the velocity changes.

To enable this behaviour, your entity needs to implement a property specifying the rate of change of the property to be smoothed.
The name of this rate of change property should then be specified in the original property's smoothing attribute:

.. sourcecode:: c#

    [Replicable]
    [Smoothing(Interpolation = 200, Extrapolation = 0, RateOfChangeProperty = "Velocity")]
    public Vector3 Position { get; set; }

    public Vector3 Velocity { get; set }

