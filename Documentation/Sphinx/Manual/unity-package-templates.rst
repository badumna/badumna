.. toctree::
   :hidden:

   gamemanager-template
   gamemanager-template-pro
   matchmaking-template
   network-template
   network-template-pro
   network-configuration-template
   player-template
   proximitychat-template
   scene-template

.. _unity-templates:

Badumna Scene Template Scripts - Unity
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**BadumnaCloud templates**

 - :ref:`GameManager <gamemanager-template-cloud>`

 - :ref:`Network <network-template-cloud>`

 - :ref:`Scene <scene-template>`

 - :ref:`Player <player-template>`

 - :ref:`Chat <proximitychat-template>`


**Badumna Pro templates**

 - :ref:`GameManager <gamemanager-template-pro>`

 - :ref:`Network <network-template-pro>`

 - :ref:`NetworkConfiguration <network-configuration-template>`

 - :ref:`Scene <scene-template>`

 - :ref:`Player <player-template>`
 
 - :ref:`Chat <proximitychat-template>`