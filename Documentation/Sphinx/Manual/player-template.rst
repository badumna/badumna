.. _player-template:

Player template script
~~~~~~~~~~~~~~~~~~~~~~

.. literalinclude:: ../../../Source/Unity/Unity/Scripts/Generated/BadumnaCloud-Scene/Player.cs
    :language: c#
