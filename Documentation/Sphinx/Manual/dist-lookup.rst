:orphan:

.. _section-distributed-lookup:

Distributed lookup service
==========================

Distributed Service Discovery also known as distributed lookup is a
unique Badumna feature for supporting server based applications. Instead
of requiring the IP addresses and port numbers of centralised servers to be
known to all clients at start up, Distributed Service Discovery allows
clients to locate these services at runtime in a completely
decentralized manner. The benefits of using Distributed Service
Discovery include:

#. **Flexibility:** Extra instances of centralised services can be added
   at runtime. Clients can locate and start using them in a matter of
   minutes.

#. **Reliability:** Usually a central server is employed to store the
   address information of all the servers in the game network and supply
   that information to the clients at start-up. However, the central
   server introduces an additional single point of failure to the
   already complex gaming system. Distributed Service Discovery
   eliminates the single point of failure thereby increasing the fault
   tolerance of the system significantly.

The Distributed Service Discovery feature can be enabled in Badumna
based games by configuring both the server and client appropriately.
Currently Overload Servers and Arbitration Servers can be configured to
use distributed lookup. Distributed Service Discovery will be used by
default if no server address is provided when you add a service. 
We will now explain how to configure distributed lookup for Arbitration Servers and Overload
Servers.

Configuring an Arbitration Server
---------------------------------

In order to enable distributed lookup for an Arbitration Server, you
need to configure the server and the client. The server needs to include
the following XML snipped in its configuration file. Please note that we
have assumed that the arbitration server is called *combatzonea*.

.. sourcecode:: c#

      <Module Name="Arbitration">
         <Server>
             <Name>combatzonea</Name>
         </Server>
      </Module>

The client side configuration file should contain the same configuration
snippet as the one above. This will ensure that the client requests the
address of *combatzonea* arbitration server during runtime.
Alternatively this configuration can be done programatically as follows:

.. sourcecode:: c#

      Options options = new Options();
      options.Arbitration.Servers.Add(new ArbitrationServerDetails("combatzonea"));

After logging in to the network, the Arbitration server must be announced on the network by calling  |NetworkFacade.AnnounceService|, 
specifying ServerType.Arbitration as the service type. 
If the server is implemented using PeerHarness, then |PeerHarness.RegisterService| should be called instead of |NetworkFacade.AnnounceService|.


.. warning::
   **Running multiple Arbitration Servers in the network**
   
   If Arbitration Servers needs to connect to each other via distributed lookup, 
   they should make sure their own ArbitrationServerDetails (without their address) is the first entry in the arbitration options.
      

Configuring an Overload Server
------------------------------

To use Service Discovery for an Overload Server, the following
configuration should be used on the server:

.. sourcecode:: c#

      <Module Name="Overload">
         <AcceptOverload>enabled</AcceptOverload>
      </Module>

The client configuration should include the following:

.. sourcecode:: c#

      <Module Name="Overload">
         <EnableOverload>enabled</EnableOverload>
      </Module>

Service Discovery will be used because no server address is specified.
To instead perform this configuration programatically on the client, use
the following code:

.. sourcecode:: c#

      Options options = new Options();
      options.Overload.IsClientEnabled = true;
      options.Overload.ServerAddress = null;


Note that *null* is the default value for the ServerAddress property so
it does not have to be set explicitly as is done here.

As mentioned at the beginning of this section, the major benefit of distributed lookup is the flexibility
and reliability it brings to the system. 
Consider a situation where you have a single Overload Server deployed with distributed lookup enabled.
In order to add a second Overload Server to the network to increase the overall capacity,
you simply start another Overload Server with the exact same configuration. 
The newly added Overload Server will be discovered by all the peers within minutes of the server going online and peers will
randomly connect to one of the Overload servers to access its functionality. 
If one of the Overload Server fails, peers connected to the affected Overload server will
automatically reconnect to the other server. 
Once the failed Overload server recovers, peers will start connecting to the restarted server again to
achieve server load balancing.


.. warning::
   **Special account required when running a secure network**
   
   For security reasons, when running a secure network using Dei, Badumna services (e.g. Overload and Arbitration servers) must use a
   special account that has the right permissions. 
   Clients can use normal accounts to lookup such announced services. 
   Please refer to :ref:`section:dei-user-accounts` for more information on Dei account types.

.. warning::
   **Clients cannot mix distributed and direct lookup for a service.**

   If a client is using distributed lookup for a service type (arbitration or overload), it cannot specify some servers using a fixed
   address for that same service type. Clients can use different lookup methods between service types, e.g. direct connection for arbitration
   and distributed lookup for overload.

.. include:: ../apilink-directives.txt