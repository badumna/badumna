.. include:: ../apilink-directives.txt

.. toctree::
   :hidden:
   
   Secure Identities <dei>
   VerifiedIdentities <verified-identities>
   Private Chat <private-chat>

.. _connect-with-friends:

Connect with Friends [Pro]
==========================

.. include:: pro-tipbox.txt

You can choose whether players have to log-in with an identity to play your game.
The advantage of requiring user authentication is that players' verified identities can then be used to let them connect with each other.

Verified identities
-------------------

With Badumna Pro, you can require all users to log in with a verified identity using Dei Server, Badumna's authentication server.
Dei Server lets users sign in using a password, and only users who have signed in will be able to join the Badumna network.
Dei Server can let each users have multiple characters tied to a single account.

Learn more in :ref:`verified-identities`.

Presence and private chat
-------------------------

Using verified identities in your game will allow players to connect with their friends using their known identities.
Users can

 - be notified when their friends come online, and
 - exchange private messages using their verified identities.

Learn more in :ref:`private-chat`.
