.. toctree::
   :hidden:

   gamemanager-match-template
   gamemanager-match-template-pro
   network-match-template
   network-match-template-pro
   network-configuration-match-template
   matchmaking-template
   matchcontroller-match-template
   player-match-template
   chat-match-template
   npc-match-template   

   
.. _unity-match-templates:

Badumna Match Template Scripts - Unity
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**BadumnaCloud templates**

 - :ref:`GameManager <gamemanager-match-template-cloud>`

 - :ref:`Network <network-match-template-cloud>`
  
 - :ref:`Matchmaking <matchmaking-match-template>`

 - :ref:`MatchController <matchcontroller-match-template>`

 - :ref:`Player <player-match-template>`

 - :ref:`Chat <chat-match-template>`

 - :ref:`NPC <npc-match-template>`


**Badumna Pro templates**

 - :ref:`GameManager <gamemanager-match-template-pro>`

 - :ref:`Network <network-match-template-pro>`
 
 - :ref:`NetworkConfiguration <network-configuration-match-template>`
 
 - :ref:`Matchmaking <matchmaking-match-template>`

 - :ref:`MatchController <matchcontroller-match-template>`

 - :ref:`Player <player-match-template>`

 - :ref:`Chat <chat-match-template>`

 - :ref:`NPC <npc-match-template>`