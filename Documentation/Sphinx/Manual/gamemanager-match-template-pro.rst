.. _gamemanager-match-template-pro:

GameManager template script - Badumna Pro
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. literalinclude:: ../../../Source/Unity/Unity/Scripts/Generated/BadumnaPro-Match/GameManager.cs
    :language: c#