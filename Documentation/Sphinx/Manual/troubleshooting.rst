.. This orphan directive is here to make the build server's check task pass while the Troubleshooting guide isn't linked in to the toctree.
.. It can be removed as soon as the link is added back to how-to.rst

:orphan:

.. _chapter:cloud-troubleshooting:

How-To Guide: Troubleshooting
==============================

- **My client is unable to connect to the network.**
  There may be several reasons why your game client may not connect to the network:

  1. The UDP ports used by Badumna Cloud are blocked. 
     Badumna Cloud uses a UDP port range from 21300 to 21399 for its communication.
     Please ensure that these port numbers are not blocked in your client configuration.

  2. A firewall is blocking network traffic.
     Check and make sure there is no firewall running and blocking the application.
     If you have a firewall running, you will have to include your game in its exception list.

- **Why do you have three different versions of Badumna DLL?**
  The three different DLLs target different platforms.
  Each DLL has been optimised for the different platforms (iOS, Android, Desktop).
  When you build your game, you have to ensure that you select the correct version depending on your target platform.
  If you are using Unity for your game development, there is a helper script that can select the correct version for you. 
  
