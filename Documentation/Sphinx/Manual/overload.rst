.. include:: ../apilink-directives.txt

.. _overload:

Overload Server [Pro]
=====================

.. include:: pro-tipbox.txt

Overload Server is a Badumna peer with special functionality associated
with it. It provides load balancing and a fall back mechanism for normal
Badumna peers to share their load in the event of starvation (a Badumna
node experiencing high outgoing network traffic and unable to handle the
load). This can happen due to a flash crowd event, e.g. lots of people
gathering in a small space. In such a scenario, if a normal Badumna peer
is unable to handle the network activity by itself then it will offload
some of its network load to an Overload Server (if available). Hence,
the Overload Server needs to be a Badumna peer running on an operator
controlled machine that can support such load balancing functionality.
Instructions to setup an Overload Server for your application are as
follows:

#. Install the Overload Server application on an appropriate machine
   (Overload Server is available as part of Badumna Network Suite
   installation package).

#. Start the Overload Server. The Overload Server should be run from the
   command line, specifying an application name, seed peer, and
   optionally the port to listen on. Alternatively options can be loaded
   from a configuration file in the same way as when starting a seed
   peer (see :ref:`section:seed-peer`).

   .. container:: commandline

       ``OverloadPeer.exe --application-name=my-application  --badumna-config-seedpeer=seedpeer.example.com:21251``

   If Dei is being used to run a secure network, the Dei configuration
   can also be specified using the command line options

   -  ``--dei-config-file`` , or

   -  ``--dei-config-string``

   See :ref:`configuring-a-seed-peer-to-use-dei` for information on
   Dei configurations.

#. Specify the details of the Overload Server in your client Badumna
   configuration. Add the following to the network configuration code of
   your client:

   .. sourcecode:: c#

       Options options = new Options();
       options.Overload.ServerAddress = "overloadserver.example.com:21252";
       options.Overload.IsClientEnabled = true;

   Alternatively, if you are using a configuration file then add the
   following to that file:

   .. sourcecode:: xml

       <Module Name="Overload">
           <EnableOverload>enabled</EnableOverload>
           <OverloadPeer>overloadserver.example.com:21252</OverloadPeer>
       </Module>

   where “overloadserver.example.com” is the hostname of your machine
   running the Overload Server and “21252” is the port number.

   Alternatively, you can use distributed lookup to locate overload
   servers on the network (see :ref:`section-distributed-lookup`).
   Distributed lookup is used if no server address is specified:

   .. sourcecode:: c#

       Options options = new Options();
       options.Overload.ServerAddress = null;
       options.Overload.IsClientEnabled = true;

   Or for a configuration file:

   .. sourcecode:: xml

       <Module Name="Overload">
           <EnableOverload>enabled</EnableOverload>
       </Module>

Your Overload Server is now configured and is ready for use.

.. tip::
   **Multiple overload servers**
   
   If you require multiple overload servers, you must use distributed
   lookup (:ref:`section-distributed-lookup`).

