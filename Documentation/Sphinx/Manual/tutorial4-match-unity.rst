
.. _unity-match-tutorial4:

Unity tutorial - Match Remote Procedure Calls
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. admonition:: Pre built package available (BadumnaCloud)

   This tutorial shows how to use entity RPCs step by step.
   If you would prefer to see the result immediately, the SDK contains a completed package:
   
   ``Unity Packages\Match-Demo4-RPC.unitypackage``
   
   You only need to specify your cloud application identifier and select the correct assembly in the completed demo.

.. admonition:: Pre built package available (Badumna Pro)

   This tutorial shows you how to add Badumna match functionality from scratch.
   If you would prefer to see the result immediately, the SDK contains a completed package:
   
   ``Demo\Match-Demo4-RPC(Pro).unitypackage``
   
   You only need to configure Badumna network and select the correct assembly in the completed demo.

This tutorial builds on the simple game from tutorial 3. We'll add RPC calls to our demo, where a player can create an explosion effect in the game and send this event to all replicas.

.. include:: tutorial-requirements-unity.txt

.. spelling::
    CallMethodOnReplicas
    CallMethodOnOriginal

**1. Open the project from the previous tutorial**

You can either open the project that you completed following the tutorial :ref:`Unity tutorial - Match Entity Replication<unity-match-tutorial3>` or
you can open the completed tutorial solution included in the BadumnaCloud or Badumna Pro Unity SDK.


**2. Edit the GameManager.cs:**

We're going to add "ExplosionPrefab" field to the GameManager class that will be used for storing the explosion prefab:

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Match-Demo4-RPC/GameManager.cs.Demo_Scene-2_Fields.rst

**3. Attach ExplosionPrefab into GameManager:**

Select the GameManager object in the Hierarchy window and drag the ExplosionPrefab from the Prefabs folder in the Project window to the ExplosionPrefab field in the GameManager Inspector window.

**4. Edit the Player.cs:**

Add the following block of code into your Player class.

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Match-Demo4-RPC/Player.cs.Update_method.rst

The Update method above is used to detect the user input/touch and decide whether it should play the explosion effect by calling SendExplosionEffect method that will be explained shortly.

Add the following two methods below into your Player class.

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Match-Demo4-RPC/Player.cs.RPC_methods.rst

The ``ExplosionEffect`` method is an RPC method that will be replicated by Badumna whenever the Match.CallMethodOnReplicas or Match.CallMethodOnOriginal is called.
``SendExplosionEffect`` is a helper method which will call Match.CallMethodOnReplicas to replicate the ExplosionEffect to all replicas and also call ExplosionEffect method to display the Explosion locally.

**5. Build and run the game**

.. include:: build-and-run-unity.txt

Build and run a couple of instances of the game to try out RPC calls.

.. admonition:: Badumna Pro only !!!

  .. include:: build-and-run-unity-match-badumna-pro.txt