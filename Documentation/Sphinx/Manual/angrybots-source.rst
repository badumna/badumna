.. _angrybots-sources:

AngryBots Scripts
~~~~~~~~~~~~~~~~~

 - C#
    
    + :ref:`GameManager <gamemanager-angrybots>`
    + :ref:`BadumnaCloud <badumnacloud-angrybots>`
    + :ref:`BaseBadumna <basebadumna-angrybots>`
    + :ref:`Player <player-angrybots>`
    + :ref:`HostedEntity <hostedentity-angrybots>`
    + :ref:`Enemy <enemy-angrybots>`
    + :ref:`Prop <prop-angrybots>`
    + :ref:`Game <game-angrybots>`
    + :ref:`Menu <menu-angrybots>`
    + :ref:`MatchController <matchcontroller-angrybots>`
    + :ref:`PlayerManager <playermanager-angrybots>`
    + :ref:`Chat <chat-angrybots>`

 - JavaScript

    + :ref:`AIScript <ai-angrybots>`
    + :ref:`BuzzerKamikazeControllerAndAi <buzzerkamikazecontrollerandai-angrybots>`
    + :ref:`DisableOutsideRadius <disableoutsideradius-angrybots>`
    + :ref:`MechAttackMoveController <mechattackmovecontroller-angrybots>`
    + :ref:`PatrolRoute <patrolroute-angrybots>`
    + :ref:`SpiderAttackMoveController <spiderattackmovecontroller-angrybots>`
    + :ref:`SpiderReturnMoveController <spiderreturnmovecontroller-angrybots>`
    + :ref:`Spawner <spawner-angrybots>`
    + :ref:`DestroyObject <destroyobject-angrybots>`
    + :ref:`EnemyArea <enemyarea-angrybots>`
    + :ref:`AutoFire <autofire-angrybots>`
    + :ref:`Health <health-angrybots>`

.. _gamemanager-angrybots:

GameManager.cs
~~~~~~~~~~~~~~

.. literalinclude:: ../../../Source/Unity/Unity/Scripts/Generated/AngryBots/GameManager.cs
    :language: c#   

.. _badumnacloud-angrybots:

BadumnaCloud.cs
~~~~~~~~~~~~~~~

.. literalinclude:: ../../../Source/Unity/Unity/Scripts/Generated/AngryBots/BadumnaCloud.cs
    :language: c#

.. _basebadumna-angrybots:

BaseBadumna.cs
~~~~~~~~~~~~~~

.. literalinclude:: ../../../Source/Unity/Unity/Scripts/Generated/AngryBots/BaseBadumna.cs
    :language: c#

.. _player-angrybots:

Player.cs
~~~~~~~~~

.. literalinclude:: ../../../Source/Unity/Unity/Scripts/Generated/AngryBots/Player.cs
    :language: c#

.. _hostedentity-angrybots:

HostedEntity.cs
~~~~~~~~~~~~~~~

.. literalinclude:: ../../../Source/Unity/Unity/Scripts/Generated/AngryBots/HostedEntity.cs
    :language: c#

.. _enemy-angrybots:

Enemy.cs
~~~~~~~~

.. literalinclude:: ../../../Source/Unity/Unity/Scripts/Generated/AngryBots/Enemy.cs
    :language: c#

.. _prop-angrybots:

Prop.cs
~~~~~~~~

.. literalinclude:: ../../../Source/Unity/Unity/Scripts/Generated/AngryBots/Prop.cs
    :language: c#

.. _game-angrybots:

Game.cs
~~~~~~~

.. literalinclude:: ../../../Source/Unity/Unity/Scripts/Generated/AngryBots/Game.cs
    :language: c#

.. _menu-angrybots:

Menu.cs
~~~~~~~

.. literalinclude:: ../../../Source/Unity/Unity/Scripts/Generated/AngryBots/Menu.cs
    :language: c#

.. _matchcontroller-angrybots:

MatchController.cs
~~~~~~~~~~~~~~~~~~

.. literalinclude:: ../../../Source/Unity/Unity/Scripts/Generated/AngryBots/MatchController.cs
    :language: c#

.. _playermanager-angrybots:

PlayerManager.cs
~~~~~~~~~~~~~~~~

.. literalinclude:: ../../../Source/Unity/Unity/Scripts/Generated/AngryBots/PlayerManager.cs
    :language: c#


.. _chat-angrybots:

Chat.cs
~~~~~~~

.. literalinclude:: ../../../Source/Unity/Unity/Scripts/Generated/AngryBots/Chat.cs
    :language: c#

.. _ai-angrybots:

AIScript.js
~~~~~~~~~~~

.. literalinclude:: ../../../Source/Unity/Unity/Scripts/Generated/AngryBots/AIScript.js
    :language: c#

.. _buzzerkamikazecontrollerandai-angrybots:

BuzzerKamikazeControllerAndAi.js
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. literalinclude:: ../../../Source/Unity/Unity/Scripts/Generated/AngryBots/BuzzerKamikazeControllerAndAi.js
    :language: c#

.. _disableoutsideradius-angrybots:

DisableOutsideRadius.js
~~~~~~~~~~~~~~~~~~~~~~~

.. literalinclude:: ../../../Source/Unity/Unity/Scripts/Generated/AngryBots/DisableOutsideRadius.js
    :language: c#

.. _mechattackmovecontroller-angrybots:

MechAttackMoveController.js
~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. literalinclude:: ../../../Source/Unity/Unity/Scripts/Generated/AngryBots/MechAttackMoveController.js
    :language: c#

.. _patrolroute-angrybots:

PatrolRoute.js
~~~~~~~~~~~~~~

.. literalinclude:: ../../../Source/Unity/Unity/Scripts/Generated/AngryBots/PatrolRoute.js
    :language: c#

.. _spiderattackmovecontroller-angrybots:

SpiderAttackMoveController.js
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. literalinclude:: ../../../Source/Unity/Unity/Scripts/Generated/AngryBots/SpiderAttackMoveController.js
    :language: c#

.. _spiderreturnmovecontroller-angrybots:

SpiderReturnMoveController.js
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. literalinclude:: ../../../Source/Unity/Unity/Scripts/Generated/AngryBots/SpiderReturnMoveController.js
    :language: c#

.. _spawner-angrybots:

Spawner.js
~~~~~~~~~~

.. literalinclude:: ../../../Source/Unity/Unity/Scripts/Generated/AngryBots/Spawner.js
    :language: c#

.. _destroyobject-angrybots:

DestroyObject.js
~~~~~~~~~~~~~~~~

.. literalinclude:: ../../../Source/Unity/Unity/Scripts/Generated/AngryBots/DestroyObject.js
    :language: c#

.. _enemyarea-angrybots:

EnemyArea.js
~~~~~~~~~~~~

.. literalinclude:: ../../../Source/Unity/Unity/Scripts/Generated/AngryBots/EnemyArea.js
    :language: c#

.. _autofire-angrybots:

AutoFire.js
~~~~~~~~~~~

.. literalinclude:: ../../../Source/Unity/Unity/Scripts/Generated/AngryBots/AutoFire.js
    :language: c#

.. _health-angrybots:

Health.js
~~~~~~~~~

.. literalinclude:: ../../../Source/Unity/Unity/Scripts/Generated/AngryBots/Health.js
    :language: c#