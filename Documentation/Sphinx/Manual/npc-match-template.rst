.. _npc-match-template:

NPC template script
~~~~~~~~~~~~~~~~~~~

.. literalinclude:: ../../../Source/Unity/Unity/Scripts/Generated/BadumnaCloud-Match/NPC.cs
    :language: c#