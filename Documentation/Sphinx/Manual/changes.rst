.. _changes:

Change History
==============

.. _badumna-3.2:

Badumna 3.2
-----------

Badumna Pro
~~~~~~~~~~~

Badumna 3.2 brings Badumna Pro (formerly the Badumna Network Suite) up-to-date with Badumna Cloud, adding support for autoreplication and matches
(note that autoreplication is not yet available in C++ as the .NET support for autoreplication depends on reflection).
See the change log entries for earlier versions of Badumna v3 for full details.


Dei
~~~

Dei now supports characters.  Each user may create one or more characters, and select a particular character for the session.
Additionally DeiServer now always uses an AccountManager assembly, and the default DeiAccountManager.dll is now distributed alongside DeiServer.


StatisticsServer
~~~~~~~~~~~~~~~~

An HTTP endpoint has been added to the StatisticsServer (disabled by default).
Use the `--http-serve` command-line option to enable it.
The format of StatisticsDetail messages has been altered to support distinguishing a large number of concurrent sessions when Dei is not in use.
This change means that previous versions of Badumna cannot send statistics messages to StatisticsServer v3.0+, and vice versa.


C++ Build Simplification
~~~~~~~~~~~~~~~~~~~~~~~~

The C++ build now only supports building a static library.
Supporting both static and dynamic libraries caused confusion and breakages in the build system without providing a significant benefit to users.
If for some reason a statically compiled library is not appropriate for your use, please `contact us <http://www.scalify.com/contact.php>`_.

The output of the make-based build has been renamed from ``libbadumna.a`` to ``libbadumna-cpp.a``, i.e you now link against the library using the compiler flag ``-lbadumna-cpp``.
Using the previous name (``-lbadumna``) on Windows in some circumstances caused the compiler to try linking against ``Badumna.dll`` (the .NET assembly).


Other improvements
~~~~~~~~~~~~~~~~~~

- Badumna Cloud now supports C++.
- ``NetworkFacade.RegisterEntityDetails`` is no longer an optional call if you're using scenes.
  Additionally the method has been changed to take a ``float`` for the maximum speed parameter, instead a vector.


.. _badumna-3.1.1:

Badumna 3.1.1
-------------

Badumna 3.1.1 is a maintenance release that fixes the following bugs:

- Fixed a race involving ``Badumna.Match.FindMatches``.
- Fixed an issue in the Control Center where a package name could not be reused even after the original package had been removed.


.. _badumna-3.1:

Badumna 3.1
-----------

Matches
~~~~~~~

Badumna 3.1 introduces matches.
Matches are designed for smaller games, where all players are always in communication with each other.
Matches also support a host peer that can perform game logic and create non-player entities.
If the host leaves the game then the control and hosted entities seamlessly migrate to another player in the match.
A matchmaking service is also provided to help form matches based on specified criteria.


Other improvements
~~~~~~~~~~~~~~~~~~

- Smoothing for autoreplicated entities now supports using a specified velocity, and smoothing is available for any property of type ``float``, ``double`` or ``Vector3``.
- There's now a single ``Replicable`` attribute for use in validation and autoreplication, in the Badumna namespace.
- Changed ``NetworkFacade.IsFullyConnected`` to return true only when at least one open peer is connected.
- The logging system can now be configured programmatically (this is only useful if you have a trace build).
- Errors that affect the functioning of Badumna are now reported via exceptions from ``ProcessNetworkState``.  If you see these exceptions please report them to us.


.. _badumna-3.0:

Badumna 3.0
-----------

Badumna Cloud
~~~~~~~~~~~~~

Badumna 3.0 introduces Badumna Cloud for Unity and .NET projects.
Badumna Cloud eliminates the need to run and maintain your own centralised service peers.


Autoreplication
~~~~~~~~~~~~~~~

Replicated entities now no longer need to implement either of the ``ISpatialEntity`` interfaces (``ISpatialOriginal`` and ``ISpatialReplica``).
Rather than implementing ``Serialize`` and ``Deserialize`` methods, properties can now be replicated by tagging them with the ``Replicable`` attribute.
Replicated properties can have smoothing applied automatically to the replica.
Remote Procedure Calls (RPCs) can also be defined by applying the ``Replicable`` attribute to entity methods.

Because entities no longer necessarily have a ``Guid``, the proximity chat and streaming APIs have been updated to accept entities directly.


Other improvements
~~~~~~~~~~~~~~~~~~

Badumna 3.0 also includes the following changes and improvements:

- Added ``NetworkFacade.BeginCreate`` and ``NetworkFacade.EndCreate`` to simplify asynchronous invocation of ``NetworkFacade.Create``.
- A ConnectivityException is now thrown from ``NetworkFacade.Create`` if the peer cannot perform basic networking tasks (e.g. if UDP is blocked).
- Added support for manually specifying keypairs when not using Dei.  This can decrease startup time if the keypair is generated ahead of time.
- A matchmaking system for finding other players based on certain criteria was introduced.


.. _badumna-2.3.3:

Badumna 2.3.3
-------------

General
~~~~~~~

- Added a workaround to prevent a crash when using Badumna with Unity 4 on Windows 8.

StatisticsServer
~~~~~~~~~~~~~~~~

The StatisticsServer in Badumna 2.3.3 includes fixes for SQL injection vulnerabilities, so you should upgrade as soon as possible.

.. _badumna-2.3.2:

Badumna 2.3.2
-------------

Private Chat
~~~~~~~~~~~~~
Optimised private chat module to reduce network traffic and improve overall performance.

Other improvements
~~~~~~~~~~~~~~~~~~~
Badumna 2.3.2 contains the following changes and improvements

- Fixed an issue with Dei Server running on Linux.
- Fixed a minor issue with Control Center.
- Fixed an issue with WebSignUp demo.
- Fixed an issue in replication module.
- Fixed a minor issue with Badumna scene change and interest management.

.. _badumna-2.3.1:

Badumna 2.3.1
-------------

Dei CustomAccountManager
~~~~~~~~~~~~~~~~~~~~~~~~

The default Dei account manager class (``SqlAccountManager``) is now distributed as a separate ``DeiAccountManager.dll`` within the Badumna package. The source code and project used to create this DLL is included in the release to make creating custom implementations easier.

See :ref:`section:custom-datastorage` for more details.

Other Changes
~~~~~~~~~~~~~

Badumna 2.3.1 also contains the following changes and improvements:

- The |NetworkFacade.GetCharacterForArbitrationSession| method has been added, allowing arbitration servers to know the current Character of the client making a request.

.. _badumna-2.3:

Badumna 2.3
-----------

Support for new platforms
~~~~~~~~~~~~~~~~~~~~~~~~~

Support has been added for the following platforms:

- Flash (using ActionScript or Unity)
- Android (using Mono for Android or Unity)
- iOS (using MonoTouch, Unity, or C++)


Improved Chat API
~~~~~~~~~~~~~~~~~

The chat API has been substantially improved.
See the `Porting Guide <http://www.scalify.com/documentation/Articles/porting-guide.html>`_ for details.


Support for Multiple Characters
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The Dei system now has support for multiple characters per user.
See :ref:`dei-character-management` for details.


Other Changes
~~~~~~~~~~~~~

A number of other improvements have also been made, including the following:

- Multiple instances of ``NetworkFacade`` can now be created in a single application domain.  
  This allows client applications to handle a change of network address while running by hooking the ``AddressChangedEvent`` and re-initializing the network stack.
- The ``NetworkFacade.Instance`` and ``NetworkFacade.IsInstantiated`` properties which were deprecated in the previous release have now been removed.
- The ``INetworkFacade.Logout()`` method has been removed, as it did not properly log out.  To 'logout', use the ``INetworkFacade.Shutdown`` method (a new facade must be created to log back in).
- ``AddressChangedEvent`` is now invoked in the application thread.
- A new API was added to improve performance for scenes with a small number of entities: ``INetworkFacade.JoinMiniScene(...)``.
  When joining a scene using this API, interest relationships are maintained between all entities all the time.
- A new API was added to get a ``BadumnaId`` from an arbitration session id: ``INetworkFacade.GetBadumnaIdForArbitrationSession(int sessionId)``.
- Fixed a potential SQL injection attack in the default Dei SqlAccountManager.
- Tunnel now accepts commandline configuration options for Badumna settings such as ``--badumna-config-port``, ``--application-name``, etc.
- Altered the Badumna C++ exception callback to report the stack trace instead of the root method name.
- Badumna C++ now logs more informative error / assertion messages if the BadumnaRuntime hasn't been initialized or if Badumna.dll or Dei.dll is missing.

.. _badumna-2.0:

Badumna 2.0
-----------

Configuration options and NetworkFacade.Instance
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The configuration system has been updated to be more explicit in its
behaviour and to reduce the chance of common configuration problems.

#. The file “NetworkConfig.xml” is no longer implicitly loaded.

#. The NetworkFacade must be explicitly configured before it can be
   used. This is done by a call to |NetworkFacade.Create(Options)|.

#. Configuration options can be set programmatically on an instance of
   the |Options| class, or they can be loaded from an XML file.

#. The application name must now be set on the |Options| instance. The
   old INetworkFacade.Initialize(...) methods have been
   removed—initialization is now done during the call to
   |NetworkFacade.Create(Options)|.

#. ``NetworkFacade.Instance`` has been deprecated. Instead, developers
   should maintain a reference to the |INetworkFacade| returned by the
   call to |NetworkFacade.Create(Options)|. ``NetworkFacade.Instance``
   will be removed in an upcoming version.

To replicate the old behaviour where “NetworkConfig.xml” was implicitly
loaded, use code like the following:

.. sourcecode:: c#

    string optionsPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "NetworkConfig.xml");
    Options options = new Options(optionsPath);
    options.Connectivity.ApplicationName = "com.example.app-name";
    NetworkFacade.Create(options);

.. _badumna-1.4.3:

Badumna 1.4.3
-------------

Performance Optimization
~~~~~~~~~~~~~~~~~~~~~~~~

Badumna 1.4.3 removed an identified CPU hotspot.

Control Center
~~~~~~~~~~~~~~

#. Fixed the running ControlCenter on Linux problem (i.e. ControlCenter
   crashed when it run under mono on Linux machine).

#. Fixed the Germ running as a background service on Linux problem.

#. Fixed the Germ get machine status from MacOSX problem.

.. _badumna-1.4.2:

Badumna 1.4.2
-------------

Connectivity
~~~~~~~~~~~~

#. A new configuration option called **HostedService** has been added
   into the Connectivity module. Please refer to Section
   :ref:`arb-server` for details.

#. A new event named |AddressChangedEvent|, which will be invoked when
   the public IP address of the local peer has changed, has been
   exported by the NetworkFacade.

Chat
~~~~

A private chat related bug has been fixed. Please refer to Section :ref:`private-chat` for an updated example on private chat.

Dei
~~~

Badumna 1.4.2 has a workaround for the ssl connection between DeiServer
and Unity2.6 client. Now Unity2.6 client can connect to Dei Server using
ssl-connection.

Arbitration
~~~~~~~~~~~

|ArbitrationEventSet.Serialize| will now throw
InvalidOperationException if an arbitration event that overrides
SerializedLength exceeds the specified length when serialized (see
:ref:`section:arbitration`).

Buddy List Demo
~~~~~~~~~~~~~~~

There is a bug fix in the BuddyListRequest class, which removes the
SerializedLength override.

.. _badumna-1.4.1:

Badumna 1.4.1
-------------

Connectivity
~~~~~~~~~~~~

A few connectivity bugs have been fixed in Badumna 1.4.1 to better
handle peers behind Symmetric NAT.

UPnP
~~~~

A port mapping removal bug in the UPnP module has been fixed.

Overload
~~~~~~~~

A bug which causes duplicated chat message when using overload server
has been fixed.

Peer shutdown
~~~~~~~~~~~~~

A bug related to the shutdown operation has been fixed to allow correct
shutdown when running on some versions of Mono.

.. _badumna-1.4:

Badumna 1.4
-----------

Local spatial replicas
~~~~~~~~~~~~~~~~~~~~~~

In Badumna 1.3, each peer held replicas of all registered local spatial
original entities. This has been changed in version 1.4. In Badumna 1.4,
each peer only hold replicas for remote spatial entities. In Badumna
1.4, each peer only holds replicas for remote spatial entities. This
means that the network status returned by *GetNetworkStatus()* may
report fewer number of replicas compared to version 1.3. This change is
completely transparent to existing games and requires no change to the
game code.

Initializing Badumna
~~~~~~~~~~~~~~~~~~~~

In Badumna 1.4, we have added a new method to initialize Badumna.
*public void Initialize(string appName)* has been added to the
NetworkFacade. This allows game developers to specify a unique
application name that will become the name for the Badumna network for
that game. Badumna peers from different games will then be unable to
communicate with each other. The older method to initialize Badumna is
still supported in this version and can be used by developers.

UPnP port forwarding
~~~~~~~~~~~~~~~~~~~~

The default value for UPnP port forwarding is set to *enabled*. You can
disable the UPnP port forwarding in the NetworkConfig.xml or by setting
*IsPortForwardingEnabled* to *false* in *ConfigurationOptions*.

GetNetworkStatus()
~~~~~~~~~~~~~~~~~~

The *GetNetworkStatus()* method has been updated and it now contains
information on whether the UPnP port forwarding succeed or not.

Presence information
~~~~~~~~~~~~~~~~~~~~

*void ChangePresence(ChatChannelId channel, ChatStatus status)* has been
removed from IChatService. Badumna 1.4 no longer support setting
different chat presences for different types of chat (proximity or
private chat). Each player will have a uniform presence status for both
proximity and private chat. The same presence status will be displayed
to all other users in private chat.

HTTP tunnel
~~~~~~~~~~~

In version 1.3, the support for HTTP tunnelling was not automated.
Developers had to provide an interface for the end users to manually
select HTTP tunnelling in order to connect using the tunnel. In version
1.4, the support for HTTP tunnelling has been automated. If you have a
HTTP tunnel server running, you just need to specify that in the network
configuration. The client can automatically use the HTTP tunnel server
if it is unable to connect to the network using UDP. Please refer to
:ref:`tunnel` for more details on how to configure the HTTP
tunnel.

API calling order
~~~~~~~~~~~~~~~~~

As a result of the automatic tunnelling feature,
*NetworkFacade.ConfigureFrom* can only be called before
the *NetworkFacade.Instance* object is accessed. Hence, for most games, you
will need to call *NetworkFacade.ConfigureForm* before you call
*NetworkFacade.Instance.Initialize()* method.

If you are a Unity user, there are two required changes that you will
have to make. The first change is in the Update function within
Badumna’s NetworkInitialization.cs script. You will notice that we now
require an extra check for NetworkFacade.Instance.IsLoggedIn before we call
ProcessNetworkState. Please refer to one of the Unity demos in :ref:`unity-tutorials` for more details.

The second change required is to move the network configuration set-up
from the NetworkInitialization constructor into Unity’s Awake function.
Please refer to the Unity demos in :ref:`unity-tutorials` for more
details.

You will also notice in the Unity demos that the other Badumna classes
such as LocalAvatar and RemoteAvatar now perform their initialization
within the Awake function as opposed to their constructor. This change
is optional but is considered to be optimal as these classes derive from
MonoBehaviour. Refer to the Unity demos in :ref:`unity-tutorials` for
more details.

Arbitration Server
~~~~~~~~~~~~~~~~~~

The Arbitration Server in version 1.4 is not backwards compatible with
version 1.3. Therefore, if you have a game that uses the Arbitration
Server, you will have to make a small change for it to work in the new
version. We have added an extra function in version 1.4 that makes the
Arbitration Server more reliable and robust under adverse network
conditions. Please refer to :ref:`arb-server` for more
details on how to use this new function.

Decentralized Service Discovery Support
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

In Badumna 1.3, the addresses of centralized servers, such as
Arbitration and Overload server had to be specified in the client
configuration. The Service Discovery feature introduced in Badumna 1.4
allows clients to locate such servers during runtime dynamically in a
completely decentralized manner. To use this feature the only change
required is in the client configuration and it is completely transparent
to the existing game code. Please refer to
:ref:`section-distributed-lookup` for more details.

Dei server
~~~~~~~~~~

Version 1.3 came with two different versions of the Dei server, one that
supported MySql database and the other that supported SqLite database.
In version 1.4 we have combined the two Dei server packages into one
package. You specify the database in the configuration options for the
Dei server. Apart from MySql and SQLite, it also supports SqlServer. We
have also included a simple web application along with the source code
that allows you to add and manage users to the Dei server database. You
can customise this program according to your applications requirements.
There are no changes required in the client in terms of how you access
the Dei server. In version 1.3, Dei server used an auto-generated
certificate to secure the connection between the client and the server.
However, in version 1.4, you have the option of using your custom
certificate that is signed by a trusted authority. Please refer to
:ref:`dei` for more details.

Distributed non-player objects
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Version 1.4 provides a new method to support non-player characters (NPC)
that can be hosted on client machines. A method called *public string
StartController<T>(string sceneName, uint max)* has been added to
NetworkFacade. This method allows you to run dynamic objects on clients
machines. The NPCs automatically migrate from one client to another if a
client goes offline. If you are developing a game that requires hundreds
of NPCs in the game then you can use this functionality. Please refer to
:ref:`distributed-controller` for more details about this
feature.

Unity and API Examples
~~~~~~~~~~~~~~~~~~~~~~

In Badumna 1.4, the API examples have been rearranged based on
functionality and several new examples have been added. New examples
include demonstrations for private chat, database access, and
arbitration servers. Unity examples have also been rearranged to
follow the same style as the API examples. The Unity examples in 1.3
used Unity tags to identify the different type of characters. However,
tags have been removed in 1.4. The demos now use a simple enumeration
list (Enumeration.cs) to store the list of player types.

We have also added a comprehensive example on how to access a database
from multiple arbitration servers. The example demonstrates how to
selectively lock parts of the database so that you can access unlocked
parts of the database from other remote servers. Please refer to
:ref:`arb-server` for more details.

Buddy list
~~~~~~~~~~

Badumna 1.4, includes support for Buddy lists. There is a Windows API
example and a Unity example that demonstrates how to support Buddy lists
when the information is stored in a database and accessed by Arbitration
Server.

Control Center
~~~~~~~~~~~~~~

Control Center now works on Windows, Mac and Linux operating systems
(previous version only supported Windows). The Control Center interface
has been redesigned to improve usability. It also comes with a user
authentication system. You therefore require a valid username and
password to access the Control Center making it more secure. The
connection between the Control Center and the Germ host is now secured
by the use of SSL. When you start the Control Center, it will generate a
certificate for you. Please refer to :ref:`control-center` for more
details.

Seed Peer
~~~~~~~~~

Badumna 1.4 provides the ability to support multiple Seed Peers for a
application. Multiple Seed Peers provide more reliability to the
network. Having multiple Seed Peers ensures that if a Seed Peer goes
down for a certain period, the network is still operational and the
application continues to function as normal.

Default port numbers
~~~~~~~~~~~~~~~~~~~~

Default port numbers for the different services such as Seed Peer and
Dei server have been changed. This means that when you start these
services in default mode, then you will have to change the port numbers
in your client network configuration. Please refer to
:ref:`portnumbers` for the list of port numbers that Badumna
utilises for the different services.

Bug fixes
~~~~~~~~~

Apart from the new features, Badumna 1.4 includes a number of bug fixes.
Some of them are listed here:

#. **Timing:** A bug in Environment.TickCount in the Mac version of Mono
   1.2.5 led to a noticeable lag on Macs.

#. **Replication:** Bug in the replication module caused flashing of
   entities under some rare conditions.

#. **Replication:** Multiple scenes do not work correctly when there are
   multiple original entities registered on the same peer.

#. **Replication:** Resent updates are not always correctly delivered.

#. **Connection table inconsistency:** Connection table can become
   inconsistent when there are many peers on the same LAN.

#. **UPnP:** The UPnP module does not work in the Unity Web Player.

#. **UPnP:** The UPnP module does not work with some routers.

#. **Chat:** Chat messages will be received multiple times when there
   are multiple original entities on the same peer.

#. **Chat Presence:** The user presence state will not be updated when
   the remote user crashes.

#. **DHT:** Some objects stored on the DHT will never expire.

#. **Transport:** The rate limiter can cause excessive CPU load when
   there are large number of connections.

#. **Configuration:** The verbosity level *Information* can not be set
   when using ConfigurationOptions.

.. include:: ../apilink-directives.txt
