.. include:: ../apilink-directives.txt

.. _scene-RPC:

Badumna Scenes: Remote Procedure Calls (RPCs)
=============================================

.. include:: rpc-intro.txt

Defining RPCs
-------------

A method on a replicable entity can be marked as being remotely callable with the |Replicable| attribute.
RPC methods can have up to six parameters of any type, and should have a return type of ``void``.

.. sourcecode:: c#

    public class Avatar : IReplicableEntity
    {
        public Vector3 Position { get; set; }

        [Replicable]
        public void DoSomething(int number, string word)
        {
            // ...
        }
    }

.. include:: rpc-signature-registration.txt

Making RPCs
-----------

To call a method on all replicas of an original entity in a scene, pass a delegate for the method *on the original entity instance* and desired parameters to the scene's ``CallMethodOnReplicas`` method:

.. sourcecode:: c#

    this.scene.CallMethodOnReplicas(this.localAvatar.DoSomething, 7, "foo");

To call a method on the original entity of a replica in a scene, use the scene's ``CallMethodOnOriginal`` method, and pass a delegate for the method *on the replical entity instance:*

.. sourcecode:: c#

    this.scene.CallMethodOnOriginal(replica.DoSomething, 7, "foo");

Try it out
----------

See RPCs in action:

- :ref:`Unity RPC Tutorial<unity-scene-tutorial2>`