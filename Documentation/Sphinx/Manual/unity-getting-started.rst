.. _chapter:unity:

.. spelling::
    myseedpeer

Using Badumna with Unity
==========================

Badumna has built-in support for `Unity <http://www.unity3d.com>`_. Badumna's Unity SDK provides ready-to-use scripts to enable multiplayer
functionality. The following sections provide simple guides to use the Badumna Unity SDK.

Badumna has explicit support for the following targets when using Unity:

- Windows
- OSX
- iOS
- Android

.. tip::
   **Unity required**
   
   You will require a copy of Unity installed on your machine.
   This unity packages are built using Unity 3.5.

Getting started with the SDKs
-----------------------------

You can download the `Badumna Pro-Unity SDK <http://www.scalify.com/store.php>`_ from the Scalify website or `Badumna Cloud-Unity SDK <https://cloud.badumna.com/download>`_ from the Cloud Badumna website. 
Each includes full source code of the demo.


The tutorials for the included demos can be found :ref:`here <unity-tutorials>`. 

SDK Contents
------------

The SDK is delivered as a Zip archive with the following structure:

Badumna Pro - Unity SDK
~~~~~~~~~~~~~~~~~~~~~~~

- *Assemblies:* Contains pre-built .Net assemblies required to use Badumna with Unity:

  - *Android:* Use these when targeting Android
  - *Desktop:* Use these assemblies when targeting Windows, OSX, Linux or the Web Player.
  - *iOS:* For iPhone, iPad, and iPod

- *Demo:* This folder contains the demo project described in this chapter.

- *Documentation:*

  - *Manual:* Contains the Badumna User Manual.
  - *ApiReference:* Contains documentation for the entire Badumna application programming interface.

- *ManagementTools:* Contains applications used for managing centralised services, as described in :ref:`Managing your network <management>`.

Badumna Cloud - Unity SDK
~~~~~~~~~~~~~~~~~~~~~~~~~
- *Assemblies:* Contains pre-built .Net assemblies required to use Badumna with Unity:

  - *Android:* Use these when targeting Android
  - *Desktop:* Use these assemblies when targeting Windows, OSX, Linux or the Web Player.
  - *iOS:* For iPhone, iPad, and iPod

- *Unity Packages:* This folder contains the demo project described in this chapter.

- *Documentation:*

  - *Manual:* Contains the Badumna User Manual.
  - *ApiReference:* Contains documentation for the entire Badumna application programming interface.


Targeting different platforms
------------------------------

Unity supports a wide variety of platforms, and Badumna does as well.

This section will present information that is specific to each supported platform when using Unity and Badumna.
There is not much more to be aware of, other than ensuring that you reference the correct Assemblies (.Net DLL's) in your Unity project.
Unity and Badumna will take care of the rest!
See :ref:`unity-platform-settings` for more detail information about setting up Badumna with Unity.

Windows, OSX, and Linux
~~~~~~~~~~~~~~~~~~~~~~~
For targeting any desktop, be it on Windows, OSX or Linux, no special preparation is
necessary as long as you have Unity installed.

iOS
~~~

For iOS Unity development you will need:

 - Unity with iOS Pro license
 - Xcode
 - An Apple Developer license

Before you can test the demo on your iOS device, you have to setup and register the device for development purposes.
Please refer to Unity's documentation on `Getting Started with iOS Development <http://docs.unity3d.com/Documentation/Manual/iphone-GettingStarted.html>`_ 
for further information.

Badumna iOS assemblies can be found in the ``Assemblies\iOS`` folder of Badumna's Unity SDK.

You may also want to read the article `Developing mobile apps <http://www.scalify.com/documentation/Articles/mobile-apps.php>`_ for useful tips on developing mobile applications using Badumna.

Android
~~~~~~~

For Android Unity development you will need:

 - Unity with Android Pro license
 - The `Android SDK <http://developer.android.com/sdk/index.html>`_.

    - Unity will currently only recognise an Android SDK that is installed to the path ``C:\android-sdk-windows``.
      A workaround for this is to copy the SDK folder to this location, but this can cause problems if you later update the SDK. 
      A better solution is to create a symbolic link. 
      To do this, download the `Junction <http://technet.microsoft.com/en-us/sysinternals/bb896768.aspx>`_ command-line tool from Microsoft Technet. 
      Open a command prompt and type the following:

            ``junction -s c:\android-sdk-windows c:\progra~1\android\android-sdk``

 - Edit your system environment variables to:

    - Remove any quote characters ``"`` from the JAVA_HOME variable.
    - Add ``C:\andoid-sdk-windows\platform-tools`` to the global system PATH variable.

Badumna assemblies for Android can be found in the ``Assemblies\Android`` folder of Badumna's Unity SDK.

You may also want to read the article `Developing mobile apps <http://www.scalify.com/documentation/Articles/mobile-apps.php>`_ for useful tips on developing mobile applications using Badumna.