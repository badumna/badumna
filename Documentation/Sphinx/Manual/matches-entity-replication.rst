
.. _match-entity-replication:

Badumna Matches: Entity Replication
============================================

When you are in a match, you can register entities for replication to all other members of the match.
The entity will be controlled on your peer, and persist in the game until it is unregistered, or you leave the match.

To replicate an entity, you need to define a class to represent your entity, and register an instance of it with the match.
Replicas will be created on other member peers using the ``CreateReplica`` delegate passed when joining the match (see :ref:`match-matchmaking`).


Defining replicable entity
~~~~~~~~~~~~~~~~~~~~~~~~~~

Your replicable entity should be represented by a class, not a struct.
Any properties whose values need to be replicated should be marked with the ``[Replicable]`` attribute. 

.. sourcecode:: c#

    public class Avatar
    {
        [Replicable]
        public Vector3 Position { get; set; }
        
        // ...
    }

.. include:: type-replication-warning.txt
.. include:: smoothing-info.txt

Registering entities
~~~~~~~~~~~~~~~~~~~~

Register your entity with the match, using an unsigned integer ID to identify the entity type. 
This ID will be passed to the ``CreateReplica`` delegate that you supplied on match creation or joining, and used by your application to determine the type of the replica that needs to be created.

.. sourcecode:: c#

    this.match.RegisterEntity(this.localAvatar, entityType);

Replica creation and removal
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

When a new replica needs to be created, Badumna invokes the ``CreateReplica`` delegate you provided when creating or joining the match.
Your delegate should create and return an instance of the appropriate class.
Typically you will also want it to store a reference to the replica so it can be rendered.

.. sourcecode:: c#

    private IReplicableEntity CreateReplica(Match match, EntitySource source, uint entitytype)
    {
        var replica = new Avatar();
        this.replicas.Add(replica);
        return replica;
    }

When an entity is removed from the match, Badumna notifies your application by invoking the ``RemoveReplica`` delegate you provided when creating or joining match.
Any action to be taken here is entirely application-dependent.
    
.. sourcecode:: c#

    private void RemoveReplica(Match match, EntitySource source, object replica)
    {
        this.replicas.Remove((Avatar)replica);
    }
	
Unregistering entities
~~~~~~~~~~~~~~~~~~~~~~
	
To stop your entity from being replicated, unregister it from the match.

.. sourcecode:: c#

    this.match.UnregisterEntity(this.localAvatar);

When you leave the match, all entities will automatically be unregistered.
	
Try it out
~~~~~~~~~~~

See match-based entity replication in action:

- :ref:`Unity Match Entity Replication Tutorial<unity-match-tutorial3>`
