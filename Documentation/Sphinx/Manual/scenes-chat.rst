
.. _scene-proximity-chat:

Badumna Scenes: Proximity chat
==============================

Badumna's proximity chat allows an entity in a scene to send messages to all nearby entities.

To enable proximity chat, subscribe your entity to the proximity chat channel, passing a delegate for receiving proximity chat messages.
Your delegate will receive chat messages from any other entities nearby in the same scene.

You must register your entity with a scene before subscribing to proximity chat.

.. sourcecode:: c#

    this.proximityChatChannel = this.network.ChatSession.SubscribeToProximityChannel(
        this.localAvatar,
        this.HandleChatMessage);


    private void HandleChatMessage(IChatChannel channel, BadumnaId userId, string message)
    {
        // ...
    }

To send a message to all nearby entities, simply send it to the proximity channel:
    
.. sourcecode:: c#

    this.proximityChatChannel.SendMessage(chatMessage);

Try it out
----------

See proximity chat in action:

- :ref:`Unity Proximity Chat Tutorial<unity-scene-tutorial3>`