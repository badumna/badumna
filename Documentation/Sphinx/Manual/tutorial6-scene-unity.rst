.. _unity-scene-tutorial6:

Unity tutorial - Private Chat
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. admonition:: Pre built package available (Badumna Pro)

   The instructions below will guide you in creating a multiplayer game using Badumna.
   If you would prefer to see the result immediately, the SDK contains a completed package:
   
   ``Demo\Scene-Demo6-PrivateChat(Pro).unitypackage``
   
   You only need to configure Badumna network and select the correct assembly in the completed demo.

This tutorial builds on the previous tutorial 5.
We'll add private chat, where a player can privately talk to other players as along as they are friends.

.. include:: tutorial-requirements-unity-badumna-pro.txt

.. spelling::
    friendList

**1. Open the project created in tutorial 5.**

Alternatively, you can use the completed tutorial 6 solution included in the Badumna Pro Unity SDK.

**2. Edit FriendList.cs:**

Add the Friend class below to the top of FriendList.cs file.
This Friend class is used to store the information about the friend name, and chat channel used.

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Scene-Demo6-PrivateChat(Pro)/FriendList.cs.Friend_class.rst

Replace the existing friendList field with the following code:

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Scene-Demo6-PrivateChat(Pro)/FriendList.cs.friendList_field.rst

Add a new property called Friends storing list of player's friend.

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Scene-Demo6-PrivateChat(Pro)/FriendList.cs.Friends_property.rst

Replace the GUILayout.Label under FriendListWindow method with the highlighted line below.

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Scene-Demo6-PrivateChat(Pro)/FriendList.cs.FriendListWindow_method.rst

Replace the HandleServerMessage from the previous tutorial with the following code block:

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Scene-Demo6-PrivateChat(Pro)/FriendList.cs.HandleServerMessage_method.rst

**3. Edit the Chat.cs:**

Add the following field and property into Chat.cs:

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Scene-Demo6-PrivateChat(Pro)/Chat.cs.Demo_Scene-6_field-property.rst

Add couple of new methods specifically for handling the private chat system in the demo:

InitializePrivateChat will invite all of player's friends to the private channel.

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Scene-Demo6-PrivateChat(Pro)/Chat.cs.InitializePrivateChat_method.rst

HandleChannelInvitation will handle private chat invitation from another user and set the appropriate chat channel accordingly.

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Scene-Demo6-PrivateChat(Pro)/Chat.cs.HandleChannelInvitation_method.rst

HandlePresence will be called if friend's presence status is changed.

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Scene-Demo6-PrivateChat(Pro)/Chat.cs.HandlePresence_method.rst

Replace the previous SendMessage method with the following block of code:

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Scene-Demo6-PrivateChat(Pro)/Chat.cs.SendMessage_method.rst

Finally, add the highlighted lines below into Unsubscribe method.

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Scene-Demo6-PrivateChat(Pro)/Chat.cs.Unsubscribe_method.rst

**4. Build and run the game**

.. include:: build-and-run-unity.txt

- Configure the dei server address from NetworkConfiguration in Game Manager Inspector window.
- Configure the friend server address from NetworkConfiguration in Game Manager Inspector window.

.. include:: build-and-run-unity-badumna-pro.txt

Steps required:

- Run the Dei server.
  See the previous tutorial on how to start the Dei server.
- Run the  FriendListArbitrationServer included in the Badumna Pro Unity SDK.
  See the previous tutorial for starting FriendListArbitration server.    
- Build and run a couple of instances of the game.

.. hint:: 

  Use the following format to send a private chat message.
  
  ``@username chat message``
  
  e.g.

  ``@alice Hello, Alice!!!``
