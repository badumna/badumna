
.. _unity-match-tutorial7:

Unity tutorial - Match Advanced Demo
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. admonition:: Pre built package available (BadumnaCloud)

   The SDK contains a package showing the completed tutorial:
   
   ``Unity Packages\Match-Demo7-Advanced.unitypackage``
   
   You only need to specify your cloud application identifier and select the correct assembly in the completed demo.

.. admonition:: Pre built package available (Badumna Pro)

   This tutorial shows you how to add Badumna match functionality from scratch.
   If you would prefer to see the result immediately, the SDK contains a completed package:
   
   ``Demo\Match-Demo7-Advanced(Pro).unitypackage``
   
   You only need to configure Badumna network and select the correct assembly in the completed demo.

This tutorial builds on the previous tutorial.
The objective of this tutorial is to demonstrate how to develop a complete game using Badumna Cloud that uses matchmaking, NPCs, and match controllers for scoring.
In this demo each player has to collect marbles in a fixed time period.
The more marbles you collect, the more points you receive.

.. include:: tutorial-requirements-unity.txt

**1. Open the project created in the previous tutorial**

You can either open the project that you completed following the tutorial :ref:`Unity tutorial - Match Hosted Entities<unity-match-tutorial6>` or
you can open the completed tutorial solution included in the BadumnaCloud or Badumna Pro Unity SDK.


**2. Edit NPC.cs**

Add a public field to the NPC class.

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Match-Demo7-Advanced/NPC.cs.Demo_Match-6_Fields.rst

In order to make the NPC collectible by the player, add the following RPC methods. 
These are Unity methods that use colliders to detect whether a player has touched an NPC (marble in this case).

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Match-Demo7-Advanced/NPC.cs.EnableRenderer_method.rst

And two other helper methods,

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Match-Demo7-Advanced/NPC.cs.OnTriggerEnter_method.rst

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Match-Demo7-Advanced/NPC.cs.RespawnObject_method.rst

**3. Edit Player.cs**

Add the ``Replicable`` attribute to the ``CharacterName`` property as it is required by the NPC class to keep track of the game score.

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Match-Demo7-Advanced/Player.cs.CharacterName_property.rst

**4. Edit Matchmaking.cs**

Add the field below for storing the final game score.

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Match-Demo7-Advanced/Matchmaking.cs.Demo_Match-6_Fields.rst

Add the highlighted code to the ``CreateReplica`` method.

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Match-Demo7-Advanced/Matchmaking.cs.Replica_delegates_(Advance).rst

Adding the ``CharacterController`` to the remote player will ensure that ``OnTriggerEnter`` is called when any remote player hits the NPC.

Set the NPC match field when registering the hosted entity.

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Match-Demo7-Advanced/Matchmaking.cs.Hosted_entity_registration.rst

In this demo, we will add a spectator mode.
The idea is that if a player joins a match after it has already started they will spawn as a spectator and can only join the game in the next round.
In order to do that we will add a new ``AddMember`` method to the matchmaking class.

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Match-Demo7-Advanced/Matchmaking.cs.AddMember_method.rst

And subscribe this handler to the match's ``MemberAdded`` event when creating or joining a match.

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Match-Demo7-Advanced/Matchmaking.cs.CreateMatch_method.rst

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Match-Demo7-Advanced/Matchmaking.cs.JoinMatch_method_(Advance).rst

We will add a ``StateChangedHandler`` method for setting the client game state.

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Match-Demo7-Advanced/Matchmaking.cs.StateChangedHandler_method.rst

Hook the ``StateChangedHandler`` method up to the match controller's ``OnStateChanged`` event, by adding the highlighted line inside ``OnMatchStatusChange`` method.

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Match-Demo7-Advanced/Matchmaking.cs.OnMatchStatusChange_method_(Advance).rst


Then, modify the GUI to show the game scores during the game and show an info message when the player is in spectator mode. 

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Match-Demo7-Advanced/Matchmaking.cs.OnGUI_method.rst

Also display the final score when the match is complete.

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Match-Demo7-Advanced/Matchmaking.cs.MatchmakingWindow_method_(Advance).rst

Make sure you update the final score field as follows:

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Match-Demo7-Advanced/Matchmaking.cs.HandleMatchEnd_method_(Advance).rst


Add the following highlighted line inside the ``HandleMatchStart`` method to reset the game score.

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Match-Demo7-Advanced/Matchmaking.cs.HandleMatchStart_method_(Advance).rst


**5. Edit MatchController.cs:**

Create a ``StateChangedHandler`` delegate.

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Match-Demo7-Advanced/MatchController.cs.StateChangedHandler_delegate.rst

Add a field for storing player scores.

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Match-Demo7-Advanced/MatchController.cs.Demo_Match-6_Fields.rst

Add an ``OnStateChanged`` event.

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Match-Demo7-Advanced/MatchController.cs.Demo_Match-6_Event.rst

Add a ``GameScore`` property, used for displaying score in the screen.

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Match-Demo7-Advanced/MatchController.cs.GameScore_property.rst

Add a new RPC method for adding a score to a specific player.

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Match-Demo7-Advanced/MatchController.cs.AddScore_method.rst

Add the following method and use this method to generate a string containing the player scores.

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Match-Demo7-Advanced/MatchController.cs.GenerateGameScore_method.rst

Add a ``ResetGameScore`` method to reset score when the match is restarted.

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Match-Demo7-Advanced/MatchController.cs.ResetGameScore_method.rst

Add a ``SyncGameScoreTo`` method to synchronize the game score to new members of the match in case they joined in the middle of the game.

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Match-Demo7-Advanced/MatchController.cs.SyncGameScoreTo_method.rst

Update the player score list when a new member joins the match or a member leaves the match.

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Match-Demo7-Advanced/MatchController.cs.AddMember_method.rst

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Match-Demo7-Advanced/MatchController.cs.RemoveMember_method_(Advance).rst

Added two new RPC methods for setting the game state and synchronizing the game score respectively.

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Match-Demo7-Advanced/MatchController.cs.SetGameState_method.rst

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Match-Demo7-Advanced/MatchController.cs.SyncGameScore_method.rst

Finally, add an ``OnGUI`` method that will be used to display the game score during the match.

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Match-Demo7-Advanced/MatchController.cs.OnGUI_method.rst

**6. Edit Network.cs**

Before we can use the ``Dictionary`` type as an argument for the ``SyncGameScore`` RPC method in the ``MatchController`` class, we need to register the ``Dictionary`` type for replication, and register the signature of the ``SyncGameScore`` RPC method.

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Match-Demo7-Advanced/Network.cs.Start_method_(Advance).rst

**7. Build and run the game**

.. include:: build-and-run-unity.txt

Build and run a couple of instances of the game.

.. admonition:: Badumna Pro only !!!

  .. include:: build-and-run-unity-match-badumna-pro.txt