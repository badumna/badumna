
:orphan:

Appendix I: Replicable Types and RPC Signatures
===============================================

.. _replicable-types:

Replicable Types
----------------

Badumna can automatically replicate properties on entities or match controllers of the following types: ``bool``, ``byte``, ``short``, ``int``, ``long``, ``float``,  ``double``, ``decimal``, ``sbyte``, ``ushort``, ``uint``, ``ulong``, ``char`` and ``string``.

If you wish to use other types, you need to register them with Badumna's ``TypeRegistry``, providing delegates for writing and reading them from and to a stream (and a delegate for creating a copy of an instance of the type if the type is a mutable reference type).
In our example above the ``Colour`` property was of type ``Color`` (a value type), so that type must be registered with Badumna like this:

.. sourcecode:: c#

    this.network.TypeRegistry.RegisterValueType<Color>(
        (c, w) =>
        {
            w.Write(c.R);
            w.Write(c.G);
            w.Write(c.B);
            w.Write(c.A);
        },
        r => new Color(r.ReadByte(), r.ReadByte(), r.ReadByte(), r.ReadByte()));

See the ``TypeRegistry`` :netapi:`API docs 843BAEF` for more information on registering types.

.. _rpc-signatures:

RPC Signatures
--------------

Badumna automatically supports any RPC with up to three parameters of the types: ``int``, ``float``, ``bool``, ``string`` or ``Badumna.DataTypes.Vector3``.

If you wish to use an RPC with a different signature, you will need to register it with Badumna's RPC Manager:

For example, if you want to use an RPC method that takes three parameters of types ``float``, ``byte`` and ``Color``, you would register the signature like this:

.. sourcecode:: c#

    this.network.RPCManager.RegisterRPCSignature<float, byte, Color>();

RPC signature registration should be done immediately after initializing Badumna.

You can register RPCs with up to six parameters.
	
RPCs can use any types that are registered with Badumna's ``TypeRegistry``, including those basic types that are automatically supported, so in the above example we would also need to register ``Color`` as a replicable type (see :ref:`replicable-types`).