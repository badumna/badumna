.. _unity-platform-settings:

Unity Platform Settings
~~~~~~~~~~~~~~~~~~~~~~~

For reference, the Unity settings required for each platform are shown below - **you do not need to set these yourself** if you use the Build Targets dialog included in the ``BadumnaCloud`` and ``Badumna Pro`` template package, as it sets them for you.

Player settings (set via Edit > Project Settings > Player):

    - Web Player and Desktop:
        - Run In Background: enabled
        - Api Compatibility Level: .NET 2.0
    - iOS and Android:
        - Api Compatibility Level: .NET 2.0 Subset
        - Bundle Identifier: you will need to use your own    
    - iOS:
        - AOT compiler arguments: nimt-trampolines=4096

Assembly (these paths are relative to the SDK ``Assemblies`` folder):

    - Desktop:
        - ``Desktop\Badumna.dll``
    - iOS:
        - ``iOS\Badumna.Unity.iOS.dll``
    - Android:
        - ``Android\Badumna.Android.dll``
        
