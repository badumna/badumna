.. _bootcamp-tutorial:

BootCamp tutorial
~~~~~~~~~~~~~~~~~

The complete demo project is available for download from the `Unity Asset Store <https://www.assetstore.unity3d.com/#/content/8564>`_ .
This tutorial explains how to convert the single player BootCamp demo into a multiplayer game using Badumna Cloud.

You will need a Badumna Cloud account and a valid application identifier to test this game.
If you do not have a Badumna Cloud account, you can sign up for a free account using the Badumna Cloud wizard window that is displayed when you open the project (see figure below).
Once you sign-up you will be provided with an application identifier.
Alternatively, you can set the application identifier with your own id created on `cloud.badumna.com <https://cloud.badumna.com>`_ (see :ref:`chapter:get-started`).

.. _fig:BadumnaCloudSignup:
   
.. figure:: ../images/sign-up.png
   :align: center
   
   Badumna Cloud Signup Window

Once you have an application identifier, click the "Setup" button in the signup window. 
Enter your application identifier and then click "Save".
Open the BootCamp scene:

:menuselection:`File --> Open Scene`

You are now ready to build the game and test the multiplayer functionality.

We will now explain the different scripts added to the demo in order to implement multiplayer functionality.
This will provide an understanding on how to add multiplayer functionality to a typical third person shooter game using Badumna Cloud.


Player.cs
---------

List of player's properties that need to be synchronized,

1. Player's position
2. Player's orientation
3. Player's animation
4. Player's name
5. Current selected weapon
6. Actions : walking, crouching, aiming, jumping, reloading

Player class is used for representing a replicable entity that implements the ``IReplicableEntity`` interface.
As ``Player`` class is used for local and remote players, the ``Local`` field is added to distinguish between those two types.
All replicable properties will be automatically updated by Badumna regularly. 
The ``Get`` method is called by Badumna regularly to obtain the local player's current properties 
and the ``Set`` method will be called on the relevant remote player to set the property values accordingly.
Learn more about replication in the guide to :ref:`scene-replication`.

States such as **Walk**, **Crouch**, and **Aim** are controlled by the ``SoldierController.js`` script. 
The following code lists all the replicable player properties. 

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Bootcamp/Player.cs.replicable_properties.rst

Game.cs
-------
This class is responsible for managing the Badumna network.
This involves initializing and shutting down the network, joining and registering an entity with a Badumna scene, 
creating and removing remote players from the scene.

Initializing and shutting down network
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Begin asynchronous Badumna network initialization process when the script is loaded, see ``Awake`` method.

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Bootcamp/Game.cs.badumna_initialization.rst

Login to the network after the initialization process is complete and the player has entered his/her username.

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Bootcamp/Game.cs.badumna_login.rst

You will need to shut down the network before quitting the game.

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Bootcamp/Game.cs.shutting_down_badumna.rst

Joining and leaving Badumna scene
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Join and register the local player with the Badumna scene.

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Bootcamp/Game.cs.joining_scene.rst

Leave and unregister the local player from the Badumna scene.

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Bootcamp/Game.cs.leaving_scene.rst

Creating and removing replicas
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
``CreateReplica`` method will be called when a remote player enters the local player's area of interest.

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Bootcamp/Game.cs.CreateReplica_method.rst

On the other hand, ``RemoveReplica`` method is called when a remote player moves outside the local player's area of interest.

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Bootcamp/Game.cs.RemoveReplica_method.rst

Regularly update Badumna
^^^^^^^^^^^^^^^^^^^^^^^^
You need to regularly update Badumna by calling ProcessNetworkState.
This is done in the FixedUpdate method.

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Bootcamp/Game.cs.FixedUpdate_method.rst

ActorAnimator.cs
----------------
ActorAnimator class is very similar to ``SoldierAnimations.js``. 
This class is attached to the remote player object and is responsible for playing the character animation based on the remote player's current animation state.

Shooting players
----------------
This section will explain how the shooting functionality is implemented in this demo.
The following list of actions shows the steps required to notify the original that he is just being shot.

1. Each remote player has a target collider attached to it. 
   When a bullet hits the collider the ``HitSoldier`` or ``HitSoldierGrenade`` method from ``SoldierDamageControl.js`` script will be called depending on the type of weapon used.
2. These methods will then call either ``HitByRifle`` or ``HitByGrenade`` from ``Player.cs`` accordingly.
3. The replica needs to inform its original about the hit. This event is sent using an RPC method call from the replica back to the original.
4. Finally, when the RPC method is called on the original, the original can play the hit effect and update its life property.

HitSoldier and HitSoldierGrenade
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
HitSoldier will call ``Player.HitByRifle`` method if it is attached to remote player.

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Bootcamp/SoldierDamageControl.js.HitSoldier_method.rst

Similarly, HitSoldierGrenade will call ``Player.HitByGrenade`` method. 

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Bootcamp/SoldierDamageControl.js.HitSoldierGrenade_method.rst

HitByRifle and  HitByGrenade
^^^^^^^^^^^^^^^^^^^^^^^^^^^^
``HitByRifle`` method on a replica (remote player) calls the ``SimulateHitByRiffle`` RPC method to be called on the original.

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Bootcamp/Player.cs.HitByRifle_method.rst

Similarly, ``HitBygrenade`` method calls the ``SimulateHitByGrenade`` RPC method.

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Bootcamp/Player.cs.HitByGrenade_method.rst

Respawning a dead player
------------------------
When a player is dead, the ``SoldierDamageControl.js`` calls the ``Dead`` method and passes a new spawn position. 
Player class will then broadcast this information to all the replicas by calling the RPC ``AnimateDeath`` method and passing the new spawn point as its argument.

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Bootcamp/SoldierDamageControl.js.Update_method.rst

The ``Player.Dead`` method,

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Bootcamp/Player.cs.Dead_method.rst


Local and remote player RPC methods
-----------------------------------
List of local player RPC methods,

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Bootcamp/Player.cs.local_player_RPC_methods.rst

List of remote player RPC methods,

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Bootcamp/Player.cs.remote_player_RPC_methods.rst


We hope this tutorial has give you an understanding on how to add multiplayer functionality to third person shooter games.
Please feel free to report any issues with this demo in the `Badumna Cloud forum <http://www.scalify.com/forum/forumdisplay.php?6-Badumna-Cloud>`_.



