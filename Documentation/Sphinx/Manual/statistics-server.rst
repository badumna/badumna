.. include:: ../apilink-directives.txt

.. _statistics-server:

Statistics Server [Pro]
=======================

.. include:: pro-tipbox.txt

The Statistics Server is a simple yet powerful tool which gives you the
ability to track the online activity of a Badumna network in real-time.
Once deployed, the Statistics Server receives and collects update
messages from each Badumna client at regular intervals. These update
messages consist of a unique user ID and a string referred to as the
’payload’, containing some information of your choice. With this
information, the Statistics Server is able to collect useful statistics,
such as the live user activity as well as the number of sessions and
total time duration that a user has spent using the application in a
fixed time period.

The Control Center provides the interface to access the Statistics
Server information. It is possible to extend the functionality of the
Statistics Server to record additional information and execute custom
queries depending on your application requirements.
Please refer to :ref:`custom-presence-server` for more details on
customising the Statistics Server.

.. _section:starting-statistics-server:

Starting the Statistics Server
------------------------------

The Statistics Server is pre-configured to work out of the box. By
default, the Statistics Server uses a SQLite database to store
statistical data, but can be configured to use MySQL or Microsoft SQL
Server instead.

Starting from the Command Line
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The Statistics Server can be launched from the command line as follows:

.. container:: commandline
   
   ``StatisticsServer.exe``

By default the Statistics Server will listen for incoming update
messages from Badumna clients on port number 21256. Use the
--port-number option in order to change to a different port number if
21256 is not available.

.. container:: commandline
   
   ``StatisticsServer.exe --port-number=21256``

To change the message interval, use the ``--message-interval`` option as
follow:

.. container:: commandline
   
   ``StatisticsServer.exe --message-interval=300``

Please note that if you wish to access the Statistics Server information
from the Control Center, you have to start the Statistics Server from
the Control Center. The command-line option is useful if you have
customised the Statistics Server and are accessing it using a custom
designed interface. Refer to :ref:`custom-presence-server` for more
details on extending Statistics Server functionality.

Starting from Control Center
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Please refer to :ref:`subsection:statistic-server` section for more information.

Using a different database
~~~~~~~~~~~~~~~~~~~~~~~~~~

The Statistics Server currently supports the following databases:
Microsoft SQL Server, MySQL and SQLite.

The Statistics Server is configured to use SQLite by default, and since
SQLite is a “zero-configuration” database, it does not require any
database to be created prior to use. However, if you want to use
Microsoft SQL Server or MySQL instead, then you will need to create an
empty database called “statistics” or another name of your choice. The
Statistics Server will create the required tables in the database on its
first run. Please refer to your Database vendor’s documentation for
instructions on creating a new database.

Please note that if you wish to use MySQL you will also need to install
MySQL Connector/Net, available from `the MySQL web
site <http://dev.mysql.com/downloads/connector/net/>`_.

.. warning::
   **Using MySQL Connector/Net with Mono**
   
   On Windows, the MySQL Connector/Net installer will installed the required mysql.data.dll in the global assembly cache (GAC), and edits the machine.config file to indicate that the MySQL data provider is available in that assembly.

There is no Linux or Mac OS X installer, so if you are on one of those platforms you will need to either install mysql.data.dll into the GAC yourself using *gacutil* or move it into the same directory as the Statistics Server executable.
You will also need to edit the StatisticsServer.exe.config to indicate that the MySQL data provider is available in that assembly.
To do this, just uncomment the relevant part of the DbProviderFactories configuration, and check that the version and public key token match the version of MySQL Connector/Net that you are using.

The Statistics Server uses a *data provider* to connect to a database.
It ships with data providers for Microsoft SQL Server (version 7.0 or
later), and SQLite. The data provider for MySQL (MySql Connector/Net) is
freely available (see above).

The Statistics Server’s database configuration is set in the file
StatisticsServer.exe.config. This file contains an application setting
specifying which data provider to use, and the connection string to use
for your chosen data provider. Edit this file to use your chosen
database as follows:

#. Specify the correct data provider to use with your database:
   *System.Data.SQLite* for SQLite (shown), *System.Data.SqlClient* for
   Microsoft SQL Server, or *MySql.Data.MySqlClient* for MySql.

#. Specify the connection string to use to connect to your database.
   Default connection strings for each supported database are already
   specified, assuming that you created the database called
   “StatisticsStats” on the same machine as the one the Statistics
   Server is hosted on. If you are using MySQL you will need to specify
   the user name and password for the account that the Statistics Server
   will use to access the database. For more information on connection
   strings please refer to
   `connectionstrings.com <http://www.connectionstrings.com>`_ or your
   database vendor’s documentation.

.. sourcecode:: xml

    <?xml version="1.0" encoding="utf-8" ?>
    <configuration>

      <appSettings>
        <!-- Chosen Data Provider -->
        <add key="dataProvider"
                 value="System.Data.SQLite" />
      </appSettings>

      <!-- Connection strings -->
      <connectionStrings>
        <add name="System.Data.SQLite"
             connectionString="Data Source=statistics.s3db"/>
      </connectionStrings>

      <!-- Data providers -->
      <system.data>
        <DbProviderFactories>
          <remove invariant="System.Data.SQLite"/>
          <add name="SQLite Data Provider"
               invariant="System.Data.SQLite"
               description=".Net Framework Data Provider for SQLite"
               type="System.Data.SQLite.SQLiteFactory, System.Data.SQLite" />
        </DbProviderFactories>
      </system.data>

    </configuration>


.. _configure-client:

Configuring the Badumna Client
------------------------------

In order to configure your Badumna Client to send messages to the
Statistics Server you need to create a Statistics Tracker object. This
is done by calling the following NetworkFacade function:

.. sourcecode:: c#

    public StatisticsTracker CreateTracker(string serverAddress, int serverPort, TimeSpan interval, string initialPayload);

where is the IP address of the Statistics Server, is the port number
that the Statistics Server will be listening on, is the time duration in
between update messages in seconds, and is the initial string value for
the payload of the update message.

It is important that you set the value of (time duration in between
update messages) to the same value you set for when starting the
Statistics Server (see :ref:`section:starting-statistics-server`).
Using different values in the Statistics Server and in the Badumna
client may result in incorrect results being displayed by the Statistics
Server. The default value used in the Statistics Server for Message
interval is 5 minutes (300 seconds).

Once the StatisticsTracker object is created, Badumna has all the
information it needs to start sending updates to the Statistics Server.
You can control the functionality using the following member functions
that are available for the Statistics Tracker object. You can refer to
the StatisticsTracker :netapi:`API documentation 7245184D` online for more details.

.. sourcecode:: c#

    public void Start();
    public void Stop();
    public void SetPayload(string payload);
    public void SetUserID(long userId);

The StatisticsTracker will begin to send update messages once Start() is
called, and will stop sending messages after Stop() is called. Badumna
will send the messages at the interval set by the StatisticsTracker
object. The update message sent by Badumna consists of two parts: the
user id and the payload.

Payload is a message represented in string format. When you first call
Start(), Badumna will use the value set in *initialPayload* parameter
when creating the StatisticsTracker object. This value can be changed
at any time by calling the SetPayload() function on the StatisticsTracker
object.

The user id is a unique user id that is used to identify the user. If
you are forming a secure network (using a Dei Server) then Badumna will
send the user id that was used to authenticate with Dei Server. If you
are not using Dei Server, then you can manually set the value for used
id by calling SetUserId() on the StatisticsTracker object. This value
will be used by Badumna to send the update messages to the Statistics
Server.

The Payload information is sent so that the Statistics Server can
categorise the information using that value. Typically, you may want to
set the payload to the current scene name. This will allow you to
categorise the users based on the different scenes in the game. However,
you are free to assign any value to the payload. For example, if you
have a single-sharded game with only one scene, you can set the payload
depending on the position of the user. This would allow you to track
user activity in different parts of the game. If you wish to categorise
user activity based on country , you may want to set the payload to the
user’s country. This will allow you to track the popularity of your
application in different countries.

Monitoring
----------

Once a Statistics Server has been successfully deployed from the Control
Center it will appear in the ’Running Services’ list. Selecting ’Show
Status’ takes you to the User Activity page, as shown in
:ref:`fig:presence-scene`. The User Activity page displays statistical
information describing current user activity in the network, updated in
real-time.

At the top of this page is a table named ’User Statistics’. This table
shows the number of sessions that are currently running.

.. _fig:presence-scene:
   
.. figure:: ../images/presence-scene.jpg
   :align: center
   :width: 100%
   
   Statistics Server - Live User Activity

Under the ’User Statistics’ is a line graph marked ’Live User Activity’.
This line graph shows the activity of users during the last twenty
message intervals, and is updated in real-time. The first line on the
graph represents the total number of users connected to the network. All
subsequent lines represent groups of users by using the ’payload’ as a
category. For example, if the payload was set as the scene name that a
user was currently residing in, then the lines on the User Activity
graph would represent the total number of users in each network scene
during the last twenty message intervals.

Under the User Activity graph is a link marked ’Show Session History’.
Selecting this link takes you to the Session History page, as shown in
:ref:`fig:presence-session`. The Session History page displays
statistical information describing past user activity in the network.

At the top of this page is a navigation panel stating the currently
selected time period under review. Under the time period is a drop-down
box, allowing you to toggle between viewing historical information for
the duration of a day, a week, or a month. On each side of the time
period are links to select the previous or next time period.

Under the navigation panel is a table named ’Session Statistics’. This
table shows the total number of unique users who were connected to the
network for the selected time period, as well as the average session
time for all sessions started during that time.

.. _fig:presence-session:
   
.. figure:: ../images/presence-session.jpg
   :align: center
   :width: 100%
   
   Statistics Server - Session History

Under the ’Session Statistics’ are two bar graphs marked ’Number of
Sessions’ and ’Concurrent Users’. The first bar graph shows the number
of sessions that were started during the selected time period, and the
second bar graph shows the number of concurrent users for that period.
If the selected time period is a day, then each bar represents an hour.
If the selected time period is a week, then each bar represents a six
hour period. 

If the selected time period is a month, then each bar represents a day.


.. _custom-presence-server:

Customising the Statistics Server
---------------------------------

This section will provide details on how to customise the Statistics
Server so that you can extend the existing functionality, use your
custom database and design complex queries depending on your application
requirements. The complete source code for the Statistics Server is
available upon request. You will require a copy of Visual Studio
2010 or Visual C# 2010 Express.

When you open the StatisticsServer solution you will see three projects
in the Solution Explorer: Statistics, StatisticsServer and
StatisticsTester. The Statistics project contains commonly used classes
and constants. The StatisticsTester project contains code for an
application which is used to test the Statistics Server by creating
dummy traffic for the Statistics Server to listen to. The
StatisticsServer project contains the code for the Statistics Server
itself.

The Statistics Server works by first having a Listener object listening
at a defined port number. When a StatisticsDetails message has been
received it is given to the StatisticsManager as a
StatisticsDetailsRecord, which has a time stamp of when the message was
received. The StatisticsManager takes the StatisticsDetailsRecord object
and checks its session list for any SessionDetails record containing the
unique user ID stored in the message. If a session already exists that
uses the user ID, then the StatisticsManager replaces the old
StatisticsDetailsRecord stored in the SessionDetails with the new one.
Otherwise, a new SessionDetails object is created with the
StatisticsDetailsRecord and is placed into the session list.

At regular intervals the StatisticsManager will go through the session
list to look for any session which has not been updated for two message
intervals. A session that has missed two message intervals is considered
to have ended, in which case the StatisticsManager will call the
StatisticsDAL to record details of the user session. The StatisticsDAL
is also called every message interval by the StatisticsManager to save
user activity the whole network. The StatisticsDAL (or statistics data
access layer) is responsible for interfacing with the user-chosen
database, and writing/reading data to/from it.

Out of the box the StatisticsDAL creates two tables in the chosen
database. From the StatisticsDAL’s InitialiseDatabase() function the two
tables, named ’Sessions’ and ’Snapshots’, are created during start-up:

.. sourcecode:: c#

  
    private void InitialiseDatabase()
    {
        if (!this.TableExists("Sessions"))
        {
            this.Execute(this.Sql("CREATE TABLE Sessions (UserID INTEGER NOT NULL, TotalMins INTEGER NOT NULL, StartTime DATETIME NOT NULL)"));
        }
        if (!this.TableExists("Snapshots"))
        {
            this.Execute(this.Sql("CREATE TABLE Snapshots (Time DATETIME NOT NULL, Category TEXT NOT NULL, Count INTEGER NOT NULL)"));
        }
    }

The ’Sessions’ table is used to store information about an individual
user session. It stores the unique ID of the user, the total time
duration of the session, and the time that the session started. Each
time a user session has ended the StatisticsManager class would record
the session through the StatisticsDAL:

.. sourcecode:: c#

    public void RecordUserSession(long userId, long totalMins, DateTime startTime)
    {
        this.Enqueue(this.Sql(
            @"INSERT INTO Sessions (UserID, TotalMins, StartTime) VALUES (@id, @mins, @startTime)",
            "id", userId.ToString(),
            "mins", totalMins.ToString(),
            "startTime", startTime.ToString(this.dateTimeFormat)
        ));
    }

The ’Snapshots’ table is used to store information about user activity
for a particular point in time. It stores the time that the snapshot was
taken, the category (the ’payload’ of the update message, typically set
to the scene name), and the number of users belonging to that category.
At regular intervals, set to be the same regularity as that of
Statistics update messages being sent, the StatisticsManager class would
record a snapshot of the network through the StatisticsDAL:

.. sourcecode:: c#

   
    public void RecordSceneSnapshot(DateTime time, string[] categories, long[] counts)
    {
        if ((categories.Length > 0) && (counts.Length > 0) && (categories.Length == counts.Length))
        {
            for (int i = 0; i < categories.Length; ++i)
            {
                this.Enqueue(this.Sql(
                    "INSERT INTO Snapshots (Time, Category, Count) VALUES (@time, @category, @count)",
                    "time", time.ToString(this.dateTimeFormat),
                    "category", categories[i],
                    "count", counts[i].ToString()
                ));
            }
        }
    }


In order to store additional information, the InitialiseDatabase()
function in the StatisticsDAL class should first be expanded to include
your own custom tables to be created. After doing that, you should then
write a function, much like the RecordUserSession() and
RecordSceneSnapshot(), which will insert a new table entry into the
database.

The StatisticsManager class determines the recording behaviour of the
Statistics Server, specifically in the ProcessSessionList() function:

.. sourcecode:: c#

    private void ProcessSessionList()
    {
        DateTime previousTime = DateTime.Now;
        
        while (this.isRunning)
        {
            DateTime currentTime = DateTime.Now;
            double timeDelta = (currentTime - previousTime).TotalSeconds;
            
            if (timeDelta >= this.messageInterval)
            {
                this.UpdateSessionList();
                this.RecordSceneSnapshot();
                previousTime = currentTime.AddSeconds(this.messageInterval - timeDelta);
            }
            
            Thread.Sleep(Parameters.ProcessInterval);
        }
    }

While the loop is running the session list is updated (during which
sessions that are found to have ended are recorded), and scene snapshots
are recorded at regular intervals. You can then add code to the loop to
make the call to any additional functions you have added to the
StatisticsDAL.

All the functions in the StatisticsDAL which are used to query the
database accept the same two parameters, the start time and the range
type. This is because the Session History screen in the Control Center
allows the user to select data over a particular time period, either a
day, week, or month. The GetAverageSessionTime() function is as follows:

.. sourcecode:: c#

   
    public double GetAverageSessionTime(DateTime startTime, Parameters.RangeType rangeType)
    {
        double data = 0.0f;

        using (DbTransaction transaction = this.databaseConnection.BeginTransaction(IsolationLevel.Serializable))
        {
            DateTime endTime = startTime.AddDays(1);

            switch (rangeType)
            {
                default:
                case Parameters.RangeType.Day:
                    break;

                case Parameters.RangeType.Week:
                    endTime = startTime.AddDays(7);
                    break;

                case Parameters.RangeType.Month:
                    endTime = startTime.AddMonths(1);
                    break;
            }

            using (IDbCommand command = this.Sql("SELECT * FROM Sessions WHERE StartTime > @start AND StartTime <= @end",
                "start", startTime.ToString(this.dateTimeFormat),
                "end", endTime.ToString(this.dateTimeFormat)))
            {
                command.Transaction = transaction;

                using (IDataReader reader = command.ExecuteReader())
                {
                    long sum = 0;
                    long sessionCount = 0;

                    while (reader.Read())
                    {
                        long totalMins = (long)reader["TotalMins"];
                        sum += totalMins;
                        sessionCount++;
                    }

                    if (sessionCount > 0)
                    {
                        data = (double)sum / sessionCount;
                    }
                }
            }
        }

        return data;
    }
 

As shown, we are given the start time of the time period to query, and
we use the range type to work out the end time. Using the given start
time and the calculated end time the SQL query is made, after which we
go through all the returned entries in order to retrieve the ’TotalMins’
value to calculate the average session time with.

Note that in the default accessor functions we calculate the end time as
opposed to specifying it directly through the parameters, and this is
because that in some of the other accessor functions in StatisticsDAL we
use the range type is to determine the granularity of the information
returned. So when the range type is a day, then we would return an array
of twenty-four elements where each element represents an hour in the
day, while if the range type was a month, then we would return an array
of length equal to the number of days in the month in question. As your
additional accessor function won’t have to deal with the Control Center
directly, you won’t have to worry about this.