.. include:: ../apilink-directives.txt

.. _trusted-servers:

Trusted Servers [Pro]
=====================

.. include:: pro-tipbox.txt

In addition to the key peer-to-peer networking facilities offered by
Badumna, the network suite also provides the facility to host central
servers. These may be used for services such as reliably hosting
persistent data, and arbitrating parts of the application logic that
might be vulnerable to cheating if it were run on peer machines.

To support this, Badumna allows arbitration services to be implemented,
where a special peer running on a central server can register with the
network as an arbitration server, and other peers can connect to them as
arbitration clients, and communicate reliably.

.. tip::
   Germ Harnesses
   
   * The Badumna Network Suite includes *Control Center* which can be used to remotely start and stop central services (see :ref:`control-center`).
     To be controlled via the Control Center, applications need to be implemented using a *Germ Harness* which will host a process and listen to Control Center instructions for starting and stopping it *et cetera*.
   * There are two kinds of Germ Harness: *Process Harness* for hosting normal processes, and *Peer Harness* for hosting processes that will be peers in a Badumna Network.

   The arbitration servers in the demo example are implemented using Peer Harnesses.

   
.. _arb-server:

Arbitration Servers
~~~~~~~~~~~~~~~~~~~

In order to act as an arbitration server, a Badumna peer needs to
specify the name of the arbitration service it offers in its
configuration. Each Badumna peer can only offer a single arbitration
service. The configuration can be set programmatically as follows:

.. sourcecode:: c#

    Options options = new Options();
    options.Arbitration.Servers.Add(new ArbitrationServerDetails("ExampleArbitrationService", "arbitration.example.com:21260"));

Alternatively the configuration can be set in the Arbitration module in
an XML configuration file (see :ref:`configuration-pro`):

.. sourcecode:: xml

    <Module Name="Arbitration">
      <Server>
        <Name>ExampleArbitrationService</Name>
        <ServerAddress>arbitration.example.com:21260</ServerAddres>
      </Server>
    </Module>

An arbitration server works as an operator hosted service.
Hence the HostedService configuration option should be set to enabled in the Options object.

.. sourcecode:: c#

    Options options;
    options.ConnectivityModule.IsHostedService = true;

.. tip:
   **Registering Arbitration Servers using distributed lookup.**
   
   Badumna provides a unique functionality to register Arbitration Servers with just their service names.
   Please refer to :ref:`section-distributed-lookup` for more details.

   
Badumna API Usage
^^^^^^^^^^^^^^^^^

The functionality available to an arbitration server includes:


-  Register as an arbitrator with the network, and handle received
   messages:

   |NetworkFacade.RegisterArbitrationHandler(HandleClientMessage, TimeSpan, HandleClientDisconnect)|

   The peer then registers with the network, passing in a client message
   handler, a client disconnection handler, and a disconnection timeout.
   Connected clients are automatically assigned a session ID, and when
   they send a message to the server this ID is passed in to the client
   message handler along with the message itself, serialized as a byte
   array. The server will receive notification via the client
   disconnection handler if a client connection is lost (i.e. a message
   sent to the client fails to be delivered). The disconnection handler
   is also triggered if a client has not sent a message to the server
   for longer than the disconnection timeout period.

-  Send replies to clients:

   |NetworkFacade.SendServerArbitrationEvent(int, byte[])|

   The server can send messages back to the client, passing it the
   session ID, and the message again serialized as a byte array.

-  Get a |BadumnaId| associated with a client:

   |NetworkFacade.GetBadumnaIdForArbitrationSession(int)|

   The server can request a |BadumnaId| that is associated with the given session ID.
   This |BadumnaId| can be used, e.g., to send data to the client using the
   :ref:`streaming-protocol`.

.. warning::
   **Arbitration Callback Exception Safety**
   
   When running using a peer harness, the handlers passed to RegisterArbitrationHandler will be called by the peer harness during regular processing.
   For this reason they should not throw exceptions, as these exceptions will not be caught, and will cause the arbitration server to crash.

.. warning::
   **Synchronous message handling**

   Application message handlers will block Badumna's messaging threads, and so should return quickly.
   Slow message handling code should be run asynchronously as demonstrated in the demo arbitration server included in your package.
   
Arbitration Clients
~~~~~~~~~~~~~~~~~~~

Badumna clients that need to use an arbitration server, must specify the
arbitration service in their configuration. A Badumna client can connect
to multiple Arbitration Servers. The configuration can be set
programmatically as follows:

.. sourcecode:: c#

    Options options = new Options();
    options.Arbitration.Servers.Add(new ArbitrationServerDetails("CombatArbitrationService", "combatarbitration.example.com:21260"));
    options.Arbitration.Servers.Add(new ArbitrationServerDetails("TradeArbitrationService", "tradearbitration.example.com:21260"));

Alternatively the configuration can be set in the Arbitration module in
an XML configuration file (see :ref:`configuration-pro`):

.. sourcecode:: xml

    <Module Name="Arbitration">
      <Server>
        <Name>CombatArbitrationService</Name>
        <ServerAddress>combatarbitration.example.com:21260</ServerAddres>
      </Server>
      <Server>
        <Name>TradeArbitrationService</Name>
        <ServerAddress>tradearbitration.example.com:21260</ServerAddres>
      </Server>
    </Module>

To use the arbitration service, clients must get an arbitrator from the
network facade by name using the |GetArbitrator| method, and connect to it
using its |Connect| method:

.. sourcecode:: c#

    public interface INetworkFacade
    {
        ...
        Badumna.Arbitration.IArbitrator GetArbitrator(string name);
        ...
    }

    namespace Badumna.Arbitration
    {
        public interface Badumna.Arbitration.IArbitrator
        {
            ...
            void Connect(
                ArbitrationConnectionResultHandler connectionResultHandler,
                HandleConnectionFailure connectionFailedHandler,
                HandleServerMessage serverEventHandler);
            ...
        }
    }

The Connect method takes handlers for receiving the connection result,
connection failure notifications, and server messages. When
connected, the arbitrator’s |SendEvent| method can be used to send
messages to the arbitration server. Messages should be serialized to
byte arrays using the |ArbitrationEventSet| class.

.. sourcecode:: c#

    namespace Badumna.Arbitration
    {
        public interface Badumna.Arbitration.IArbitrator
        {
            ...
            void SendEvent(byte[] message);
            ...
        }
    }



Messages can only be sent when the arbitrator is connected. If the
connection to the arbitrator fails, applications can attempt to
reconnect by calling Connect again. Applications should not call
GetArbitrator twice with the same arbitrator name.

.. _section:arbitration:

Arbitration Events
~~~~~~~~~~~~~~~~~~

To facilitate message serialization, Badumna provides two classes:
|ArbitrationEvent| and |ArbitrationEventSet|. Applications should
subclass ArbitrationEvent to create application-specific event classes,
that can serialize themselves to a byte stream, and construct themselves
from data read from a byte stream. These classes should then be
registered with an ArbitrationEventSet, which can then be used to handle
serialization and deserialization of arbitration events.

.. tip::
   **Creating your own arbitration events**
   
   To create your own arbitration events, create a new class derived from
   ArbitrationEvent, that:

   -  has a constructor that takes a BinaryReader and deserializes the
      event from it,

   -  has a Serialize method that takes a BinaryWriter and serializes the
      event to it.

If your arbitration event will serialize to a fixed length (i.e. it
contains no variable length types such as strings or variable length
collections), you can also add an override for the SerializedLength
property, that returns the expected size of the serialized event.
Badumna can use this information to serialize the event more
efficiently.

Since arbitration messages need to be used by both client and server
applications, they will usually be defined in a separate project. When
arbitration events are registered with an ArbitrationEventSet, they will
be assigned unique IDs based upon the order in which they are
registered. The ArbitrationEventSet will encode this ID into the byte
array when serializing the event, and use it to decide how to
deserialize the byte array upon receipt of an event. For this reason it
is necessary that arbitration events are registered in the same order on
both server and client.

.. warning::
   **Arbitration Events and Exceptions**
   
   Arbitration Events will typically be deserialized in the call to Arbitration Server's client event handler, which is invoked during the call to NetworkFacade's |ProcessNetworkStatus()| method.
    
   The client event handler should not throw any exceptions (see warning above).
   It is therefore recommended that any exceptions thrown during ArbitrationEvent construction are caught in the constructor and a single custom exception is rethrown.
   This exception can then easily be caught in the client event handler where deserialization is attempted. 

   
Each Badumna package includes source code for an arbitration server, that can be used as a starter pack for your Arbitration server.
You can extend this project and customise it based on your application requirements.
Please refer to DemoArbitrationServer project in your specific package.


See arbitration servers in use in the Unity Demo.
