
.. include:: ../apilink-directives.txt

.. _private-chat:

Presence and Private Chat [Pro]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. include:: pro-tipbox.txt

To use private chat, users must announce themselves to the network.
Others who know a user's character name can invite the user to a private chat.
When a private chat invitation has been accepted, the inviter can send private messages directly to the invitee.
Private chat sessions are uni-directional, so both users need to establish sessions in order to have a two-way conversation.

Enabling private chat
---------------------

Open private channels to allow other users to try to chat, passing a delegate to handle chat invitations:

.. sourcecode:: c#

    this.network.ChatSession.OpenPrivateChannels(this.HandleChannelInvitation);

.. comment
    |IChatSession.OpenPrivateChannels(ChatInvitationHandler)|

Update your presence status information to the network:

.. sourcecode:: c#

    this.network.ChatSession.ChangePresence(ChatStatus.Online);

.. comment
    |IChatSession.ChangePresence(ChatStatus)|

   Chat status can be Online, Away, Chat, Do Not Disturb, or Extended Away.
   Please note, the Offline status is automatically set by Badumna when user goes offline.
   Your application should not call ChangePresence to change the chat status to Offline.

Handling chat invitations
-------------------------
   
In your chat invitation handler, you can accept invitations from other users (as desired):
   
.. sourcecode:: c#

    private Dictionary<string, IChatChannel> openChannels =
        new Dictionary<string, IChatChannel>();

    private void HandleChannelInvitation(ChatChannelId channelId, string username)
    {
        // Accept the invitation.
        this.openChannels[username] = this.network.ChatSession.AcceptInvitation(
            channelId, this.HandlePrivateMessage, this.HandlePresence);
    }
        
.. comment
    |IChatSession.AcceptInvitation(ChatChannelId, ChatMessageHandler, ChatPresenceHandler)|
   
The |ChatInvitationHandler| passed when opening private channels, can call |IChatSession.AcceptInvitation(ChatChannelId, ChatMessageHandler, ChatPresenceHandler)| to accept invitations.
It in turn passes delegates for handling messages from other users and updates to their presence information.

Inviting friends to chat
------------------------
   
You need to know your friend's character name to invite them to chat:

.. sourcecode:: c#

    this.network.ChatSession.InviteUserToPrivateChannel(friendName);

.. comment
   |IChatSession.InviteUserToPrivateChannel(string)|

Stopping chat
-------------
   
Unsubscribe from a channel to stop the private chat:

.. sourcecode:: c#

    this.openChannels[friendName].Unsubscribe();
    this.openChannels.Remove(friendName);

.. comment
    |IChatChannel.Unsubscribe()|

   It is best practice to check for existing chat sessions with a given user when receiving a private chat invitation, and closing them with this method before accepting the invitation.
   
.. comment   
   Code for private chat is shown in BadumnaDemo's :ref:`PrivateChatModule<PrivateChatModule.cs>` class.
