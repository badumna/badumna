.. _scene-template:

Scene template script
~~~~~~~~~~~~~~~~~~~~~

.. literalinclude:: ../../../Source/Unity/Unity/Scripts/Generated/BadumnaCloud-Scene/Scene.cs
    :language: c#