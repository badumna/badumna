
.. toctree::
   :hidden:
   
   Matchmaking <matches-matchmaking>
   Match-based Entity Replication <matches-entity-replication>
   Match-based Remote Procedure Calls <matches-rpc>
   Match-based Chat <matches-chat>
   Match-based Game Controllers <matches-controller>
   Match-based Hosted Entities <matches-hosted-entities>
   
.. _badumna-matches:

Badumna Matches
===============

If you are making a multiplayer game for a limited number of players (i.e. not an MMO), you can use Badumna Matches.
Badumna Cloud provides a matchmaking service that allows players to create or find and join matches.

All members of a match can communicate with each other so long as they remain in the match.
Members can create game entities which will be automatically replicated to other members.
Entity updates are sent directly between members thereby reducing the end-to-end latency significantly.

Each match has a designated host that can run global logic and NPCs.
If the host leaves the match, a new host is automatically assigned and takes ownership of global state and NPCs.

Badumna takes care of working out when game state (global state and entity state) has changed, automatically generating updates and sending them to all members - all with minimal application code required.

.. include:: initialization-blurb.txt

Matchmaking
-----------

The Badumna Cloud matchmaking service lets you create and search for matches:

- You can either create a new match, or join an existing one.
- The peer that creates a match will become the designated match host.
- If the match host leaves, another member will automatically take over as host.

Learn all about creating and joining matches in :ref:`match-matchmaking`.

Match Controller
----------------

Match controllers lets match members communicate with each other:

- Send messages via remote procedure call (RPC) between members, including to and from the designated host.
- The designated host can control and replicate global game state to match members.

Learn more in :ref:`match-controller`.

Entity Replication
------------------

Once you are connected to a match, you can register an entity with the match to have it replicated to all other members.

- Badumna will automatically detect and replicate entity state changes.
- No serialization or deserialization code is required - Badumna does it all.

Learn more about entity replication in :ref:`match-entity-replication`

Remote Procedure Calls
------------------------

When in a match, you can send custom messages between the different participants in several different ways.

- Badumna lets you call methods on remote entities.
- As easy as calling a local method.

Learn more in :ref:`match-RPC`

Chat
-----

Badumna Matches support chat between members:

- Send private chat messages to specific other members.
- Send public chat messages to all other members.

Learn more in :ref:`match-chat`.

Hosted entities
----------------

Badumna supports the ability to host non player characters (NPC) in a match. 
The NPCs are created on the match host.
Badumna supports host migration in the event the match host goes offline.
This will ensure that your NPCs will continue to exist in the match as long as you have players in a match.

Learn more in :ref:`hosted-entities`.
