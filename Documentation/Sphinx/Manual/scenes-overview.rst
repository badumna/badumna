
.. toctree::
   :hidden:
   
   Scene-based Basic Replication <scenes-entity-replication>
   Scene-based Remote Procedure Calls <scenes-rpc>
   Scene-based Proximity Chat <scenes-chat>
   Distributed Controller <distributed-controller>
   Distributed Validation <validation>

.. _badumna-scenes:

Badumna Scenes
===============

Badumna scenes allow game developers to design game worlds that are very large in size and can support thousands of players. 
Due to Badumna's decentralized architecture, you can design single-sharded worlds that are very large in size without having to worry about server-side code.
Badumna takes care of working out when game state has changed, automatically generating updates, and working out which clients need to receive updates for maximum efficiency - all with minimal application code required.

.. include:: initialization-blurb.txt

Entity Replication
------------------

- Badumna automatically determines which other peers need to receive updates from each replicable entity.
- Badumna lets you mark which properties of your entities need to be replicated, and automatically sends updates for you when they change.
- No serialization/deserialization code required - Badumna does it all.

.. _fig:InterestManagement-Cloud:
   
.. figure:: ../images/InterestManagementSimple.svg
   :align: center
   
   Local entity A will only receive updates from remote entity Y, as X & Z are outside its region of interest.

Learn more in :ref:`scene-replication`
   
Remote Procedure Calls
----------------------

- Badumna lets you call methods on remote entities.
- As easy as calling a local method.

Learn more in :ref:`scene-rpc`

Proximity Chat
--------------

- Badumna lets you chat with other nearby players.

Learn more in :ref:`scene-proximity-chat`

Distributed Controller
----------------------

- Badumna lets you run NPCs without using a central server.
- NPCs run on players' machines.
- NPCs migrate to new machines when current peer goes offline.

Learn more in :ref:`distributed-controller`

Distributed Validation
----------------------

- To prevent cheating, Badumna lets you have player characters run on other players' machines.
- Validators are automatically assigned and re-assigned as required.
- Badumna supports client-side prediction for instant responsiveness.

Learn more in :ref:`distributed-validation`

