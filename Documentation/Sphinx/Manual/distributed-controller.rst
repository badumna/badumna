.. _distributed-controller:

Badumna Scenes: Distributed NPCs [Pro]
======================================

.. include:: pro-tipbox.txt

Replicable entities can only remain in a scene as long as the peer that is running them remains joined to the scene.
If an NPC is tied to a particular user (e.g. a player character's pet etc.), then the NPC can be run as a replicable entity on that user's machine without problems.
If an NPC should remain in the game no matter if any given user leaves, then it cannot run on a user's machine as a replicable entity.
Developers can run dedicated peers to host NPCs as replicable entities, but that is not a particularly scalable solution. 

Badumna Pro offers the ability to let NPCs run on an arbitrary user's machine, and have the NPC migrate to another user's machine if the current one goes offline.
These NPCs are not implemented as normal replicable entities, but use Badumna's distributed controller feature.

.. _section:distributed-controller:

Distributed Controller based NPCs
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Badumna comes with an advanced feature called **Distributed Controller**
that supports executing game code reliably in a non-reliable distributed
environment such as the game network.

Many games have NPCs that have non-critical roles. Such NPCs are usually
designed to allow players to interact (e.g. fight/talk) with them and
gain experience points, gold, weapons, etc. Due to the non-critical
nature of these NPCs, as long as the majority of such NPCs keep
functioning correctly, it doesn’t affect the overall gaming experience.
To support a large number of such NPCs would typically require
significant server resources(in terms of CPU, memory and bandwidth
requirements) for the game operator. In Badumna, the Distributed
Controller is provided to host such NPCs within the game network without
the need for dedicated servers. The Distributed Controller therefore
offloads the CPU and bandwidth requirements that are imposed by such
NPCs. It has been designed to ensure that the majority of the NPCs will
work consistently even as players keep joining and leaving the game.

We would like to introduce certain concepts relevant to the use of distributed controllers. 

#. **NPC Migration:** A distributed controller NPC is hosted on a
   regular user computer. If a user decides to exit the game, their
   computer will be disconnected from the Badumna network immediately.
   In order to ensure that NPCs keep running irrespective of users
   joining and leaving the game network, the NPCs need to relocate to a
   different computer and resume working as quickly as possible. This
   automatic relocation of NPCs is termed as *NPC migration* in Badumna.

#. **Checkpoint/Recover:** To support NPC migration, NPCs need to save
   their current status, called *Checkpoint*, and *Recover* from this
   saved status on a different computer.

#. **Non-critical NPCs:** As mentioned above, the distributed controller
   should only be used to implement NPCs that are non-critical. Game
   developers should expect that some of the NPCs would occasionally
   stop working for a few seconds during migration or even disappear for
   an extended period of time during the game session. Badumna ensures
   that most NPCs will work consistently when there are enough peers in
   the network.

To use this feature, a custom controller class inherited from
*DistributedSceneController* in the *Badumna.Controllers* namespace
needs to be implemented. The following methods must be overridden in the
child controller class:

#. **TakeControlOfEntity** is called when the controller is created. It
   should create an *ISpatialOriginal* object, which is usually an NPC
   avatar. This object needs to be initialized and then returned. The
   *ISpatialOriginal* object is inactive when returned from the
   *TakeControlOfEntity* function. This means that the
   *ISpatialOriginal* object is created and stored locally by the
   Distributed Controller module, but it has not been registered to any
   scene yet. There will be no processing or communications overhead
   from such inactive *ISpatialOriginal* objects.

#. The **InstantiateRemoteEntity** and **RemoveEntity** methods are
   called when the NPC is notified to create a replica of a remote
   entity and when the NPC becomes no longer interested in the remote
   entity respectively.

#. **Checkpoint** and **Recover** are used to checkpoint the status of
   the NPC avatar and recover from the saved status respectively.

#. **Sleep** is called to notify the controller that the NPC avatar is
   going to be switched to inactive status.

#. **Wake** is used to notify the controller that the NPC avatar will
   become active.

#. **Process** is called regularly when the NPC avatar is active. You
   can invoke any game logic within this method. For example, it is
   common to execute the NPC’s behaviour code within this method  [1]_.

To start or stop the Distributed Controller based NPCs in the game, the
game code should call one of the following APIs:

#. |NetworkFacade.StartController<T>(string uniqueName)| will start a
   controller named *uniqueName*. There will only be one NPC character
   created if all computers call this StartController method using the
   same *uniqueName* parameter. This is because the NPC instances are
   identified by this string (*uniqueName*). The uniqueName parameter
   also must follow a defined pattern to include the Scene Name for
   which the NPC will join.

#. |string NetworkFacade.StartController<T>(string sceneName, string controllerName, ushort max)|
   will start a controller of type T with the name controllerName, and
   have the created NPC to join the scene specified by *sceneName*. The
   parameter *max* defines the maximum number of such NPCs to be
   created. If all computers call this method with the same *max*
   parameter, then up to *max* instances of the specified NPCs will be
   created in the game. The returned value of this method is the
   uniqueName of the created controller instance.

#. |NetworkFacade.StopController<T>(string uniqueName)| is used to
   stop the controller and its associated NPC identified by the
   *uniqueName* parameter.

Please note that a maximum of one NPC is created per game client. For
example, if you set the vale of *max* to 50, Badumna will start a
maximum of one NPC on each game client. Therefore, you will require more
than 50 online users (each will call StartController with same
parameters on the local machine) to have all 50 NPCs created. When the
number of online users drops to say 20, the total number of such NPCs
will also eventually drop to 20 within a minute.

It is possible to call StartController multiple times on the same
machine with different *controllerName* to start different types of
distributed controller based NPCs. Each peer can start up to 64
different types of NPCs.


.. [1]
   Unity provides other functions such as FixedUpdate to define NPC
   behaviour that can be used.

.. include:: ../apilink-directives.txt
