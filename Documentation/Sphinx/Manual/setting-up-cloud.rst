
.. include:: ../apilink-directives.txt

.. _setting-up-cloud:

Setting up a Badumna Cloud application
======================================

To use Badumna Cloud in your game, you must first create an application on `cloud.badumna.com <http://cloud.badumna.com>`_ (see :ref:`chapter:get-started`).

Your game should initialize Badumna and log in at start up, and shutdown Badumna before exiting.
While running, your game should also regularly update Badumna.

Initialization and Log In
-------------------------

Initialize Badumna with the ID for the Badumna Cloud application you created on `cloud.badumna.com <http://cloud.badumna.com>`_, and log in with a character name:

.. sourcecode:: c#

    this.network = NetworkFacade.Create("ABCDE"); // Use your app ID here.
    this.network.Login(characterName);

``NetworkFacde.Create`` will block until initialization is complete.
To initialize Badumna asynchronously, you can use ``NetworkFacade.BeginCreate`` instead:

.. sourcecode:: c#

    this.network = NetworkFacade.BeginCreate(
        "ABCDE", // Use your app ID here.
        result =>
        {
            this.network = NetworkFacade.EndCreate(result);
            this.network.Login(characterName);
        });
    
Regularly update Badumna
------------------------

Badumna needs to be triggered regularly to do its job, so ``ProcessNetworkState()`` should be called in your game's main update loop.

.. sourcecode:: c#

    this.network.ProcessNetworkState();

Shutdown Badumna
-----------------
On application exit, it is important to inform Badumna that the user is closing the application. 
This will enable Badumna to shutdown the network thread and also perform other finalization. 

.. sourcecode:: c#

   this.network.Shutdown();
