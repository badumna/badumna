
.. _match-chat:

Badumna Matches: Chat
=====================

When in a match, you can send chat messages privately to specific members, or publicly to all other members:


.. sourcecode:: c#

    // Send a chat message to everyone in the match.
    match.Chat("Hello, everyone!");
    
    // Send a chat message to a particular member of the match.
    match.Chat(member, "Hello, " + member.Name);

To handle incoming chat messages, subscribe to your match instance's ``ChatMessageReceived`` event.

.. sourcecode:: c#

    match.ChatMessageReceived += this.HandleChatMessage;
    
    private void HandleChatMessage(string message, MemberIdentity sender, ChatType type)
    {
        // ...
    }   
    
Try it out
~~~~~~~~~~~

See match-based chat in action:

- :ref:`Unity Match Chat Tutorial<unity-match-tutorial5>`