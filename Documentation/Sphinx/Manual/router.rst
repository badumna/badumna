.. _chapter-router:

Game development on a residential network
=========================================

Badumna is able to handle different types of Network Address Translation
(NAT) devices, including residential broadband routers. However, Badumna
networks require at least one peer to have an open connection (i.e. all
other peers should be able to communicate directly with it). The Seed
Peer fulfils the role of having an open connection. This appendix
explains how to set up a working development environment in a typical
residential network environment. Please note that the set up steps are
only required when you want to have your Badumna game development
environment in a residential network. End users do not need to set up
anything on their routers to play Badumna based games.

This appendix assumes the development machine is behind a residential
broadband router with build-in NAT and is assigned a private address. We
first introduce how to use UPnP and port forwarding to start a Seed Peer
with open connection. Then we will discuss the LAN test mode feature
which allows the connectivity issues to be bypassed during development.

.. _upnp:

Universal Plug and Play (UPnP)
------------------------------

UPnP is enabled by default in Badumna 1.4. It will automatically try to
set up a port forwarding entry on the NAT and make the Seed Peer appear
to have an open connection to all other peers. If everything works as
expected, when the *verbose* command line option is set, the Seed Peer
will print out its NetworkStatus information on the standard output and
that should contain the public address details as shown in
:ref:`fig:network-status`.

.. _fig:network-status:
   
.. figure:: ../images/network-status.jpg
   :align: center
   :width: 100%
   
   Badumna - Network Status output

In :ref:`fig:network-status`, the public address is marked as a *Full
cone* NAT type. Together with the *Open* NAT type, these two NAT types
are regarded as open connections.

.. _port-forwarding:

Port Forwarding
---------------

The majority of routers support UPnP (universal plug and play) feature.
However, this is not 100% guaranteed as there may be defects in the
router’s implementation of UPnP. It is also possible that UPnP is not
supported or has been disabled in the router.

Due to the different brands and models of routers, the exact procedure
of manually setting up port forwarding on the router may be different.
The 3rd party web site http://portforward.com maintains detailed steps
on how to set up port forwarding on hundreds of different router models.
Please refer to that web site for more details. The general idea is to
map an external port, say X, to the internal port, Y, that the Seed Peer
is going to use. This will ensure that all UDP traffic sent to the
external port X will be redirected to port Y on the Seed Peer’s machine.

.. _appendix:lantestmode:

Once the port forwarding is set up on the router, you can restart the
Seed Peer and check whether the reported public address is of type Full
Cone NAT.

.. _lan-test:

LAN Test Mode
-------------

It also possible to bypass the connectivity issue during the development
phase by using the LAN test mode. In the LAN test mode, Badumna assumes
all peers are running on PCs within the same LAN and all peers can
directly communicate with each other via the private IP addresses. Peers
will find out each other through subnet broadcasting. Hence the Seed
Peer is no longer required.

To enable LAN mode programmatically call the |ConfigureForLan()|
method. E.g.:

.. sourcecode:: c#

    Options options = new Options();
    options.Connectivity.ConfigureForLan();

To enable LAN mode in a configuration file manually set the options as
described in the |ConfigureForLan()| API documentation. E.g.:

.. sourcecode:: c#

    <?xml version="1.0" encoding="utf-16"?>
    <Modules>
      ...
      <Module Name="Connectivity" Enabled="true">
        ...
        <Stun />
        <PortForwarding Enabled="false" />
        <Broadcast Enabled="true">21250</Broadcast>
        ...
      </Module>
      ...
    </Modules>

Please note, when running in the LAN test mode:

#. All regular peers and services must be configured to run in LAN test
   mode.

#. Peers will not be able to communicate with external peers on the
   Internet.

#. This feature is for testing purpose only. LAN test mode must be
   disabled in the release version of your products.

.. include:: ../apilink-directives.txt

.. _portnumbers:

Default port numbers
--------------------

.. _tab:DefaultPortNumbers:

.. csv-table:: Default port numbers
   :header: "Service", "Port"
   
   HTTP Tunnel Server:, 21247 TCP
   Dei Server:, 21248 TCP
   Broadcast:, 21250 UDP
   Seed Peer:, 21251 UDP
   Overload Server:, 21252 UDP
   Germ:, 21253 TCP
   Control Center:, 21254 TCP
   DeiAdministrationTool:, 21255 TCP
   Statistics server:, 21256 UDP
   Arbitration servers:, 21260 - 21270 UDP
   Clients:, 21300 - 21399 UDP
