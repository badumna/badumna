
:orphan:

.. _chapter:get-started:
   
Getting Started with Badumna Cloud
==================================

Follow these three simple steps and you are on your way to making multiplayer games using Badumna Cloud.

**1. Sign-up for an account:**  
     First sign-up for a Badumna Cloud account at `cloud.badumna.com <https://cloud.badumna.com/cp/account/register>`_. 
     Login to your account and create a new application.
     Your application will be assigned a unique application identifier.
     Download the appropriate client SDK depending on your development platform. 

**2. Follow the How-to Guide:** 
     We will walk you through the process of using Badumna Cloud in your game. 
     You will get an idea of the typical workflow for using our SDK in your game.
     The :ref:`how-to guide<chapter:how-to>` has been structured such that you can learn about using each feature separately.

**3. Follow the tutorials:** 
     Follow our step-by-step guide to create your first Badumna Cloud game. 
     You can either follow the steps in the web tutorial or watch the video tutorial (where available).
     The tutorials demonstrate how to build a Badumna-enabled game from scratch.
   
     We have prepared tutorials for :ref:`Unity<unity-tutorials>` users and :ref:`C++<cplusplus>` users.
   
Feel free to learn more about Badumna Cloud by reading the `FAQs <https://cloud.badumna.com/support/faq>`_ or you can check out our `user forum <http://www.scalify.com/forum>`_ for more specific questions. 
   