.. _known-issues:

Known issues in Badumna 
========================

This section lists all the known issues that exist in the current
release of Badumna (ver |version|).

#. **Badumna currently does not support Unity size optimization for iOS Player.**
   Badumna will not work when iOS Stripping level is enabled in Unity.

#. **Tunnel server cannot run in the background on Linux and Mac**.
   Tunnel server cannot be started using mono-service2 or ’&’ directly.
   To run it in the background you will need the following command,

   .. container:: commandline
      
      ``mono Tunnel.exe > /dev/null &``

#. **Tunnel server may display an exception when a client disconnects.** When
   a tunnel client disconnects, the server may show an exception on its
   console. This exception does not affect the operation of the tunnel
   server and may be safely ignored.

.. include:: ../apilink-directives.txt
