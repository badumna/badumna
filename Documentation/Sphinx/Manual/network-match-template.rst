.. _network-match-template-cloud:

Network template script - Badumna Cloud
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. literalinclude:: ../../../Source/Unity/Unity/Scripts/Generated/BadumnaCloud-Match/Network.cs
    :language: c#