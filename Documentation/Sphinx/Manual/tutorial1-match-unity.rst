
.. _unity-match-tutorial1:

Unity tutorial - Matchmaking
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. admonition:: Pre built package available (BadumnaCloud)

   This tutorial shows you how to add Badumna match functionality from scratch.
   If you would prefer to see the result immediately, the SDK contains a completed package:
   
   ``Unity Packages\Match-Demo1-Matchmaking.unitypackage``
   
   You only need to specify your cloud application identifier and select the correct assembly in the completed demo.

.. admonition:: Pre built package available (Badumna Pro)

   This tutorial shows you how to add Badumna match functionality from scratch.
   If you would prefer to see the result immediately, the SDK contains a completed package:
   
   ``Demo\Match-Demo1-Matchmaking(Pro).unitypackage``
   
   You only need to configure Badumna network and select the correct assembly in the completed demo.

Sometimes you may not want to create a single virtual world that all players will share, but would rather create separate instances for smaller groups of players.
This is where you can use Badumna's match feature to allocate players to different matches based on a set of criteria.

This Unity tutorial will demonstrate Badumna's basic match functionality.
You will learn how to: 

- Create a new match
- Find existing matches
- Join an existing match

.. include:: tutorial-requirements-unity.txt

**1. Create a new project**

- Open Unity
- Create a new project.
- Import the BadumnaCloud-Match.unitypackage (located in the BadumnaCloud SDK's Unity Packages folder) or BadumnaPro-Match.unitypackage (located in the Badumna Pro SDK's Unity Package folder).
  
  This package includes the Badumna library and a set of script :ref:`templates <unity-match-templates>` that can be edited and used in your game.

- Import the DemoAssets Unity package (located in the SDK's Unity Packages folder).  
  
  This package includes the prefabs and other assets that we will use to build the game.
  All the assets in this package have been sourced freely from the Unity asset store.

- Open the demo scene (located in the project's Scene folder).

**2. Add a GameManager object**

- Create a new game object to act as the game manager:
  :menuselection:`Game Object Menu --> Create Empty`
  
- Rename the object 'GameManager'.

- Add the GameManager.cs template script to the GameManager object.
  
  In the Project window, expand the Script and Network folders.
  Select the GameManager script and drag it into the Inspector window of GameManager object.

**3. Edit GameManager.cs**

GameManager.cs takes care of loading the Network.cs, and exposing some of its core functionality.
You will need to add certain fields to the GameManager class.
Copy the following code to GameManager.cs where you find the comment "Add any custom fields here...":

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Match-Demo1-Matchmaking/GameManager.cs.Demo_Match-0_Fields.rst

and

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Match-Demo1-Matchmaking/GameManager.cs.Demo_Fields.rst

Edit the Initialize method to add the new scripts - see the highlighted code below:

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Match-Demo1-Matchmaking/GameManager.cs.Initialize_method_(matchmaking).rst

Edit the Shutdown method to reset the scripts when Badumna is shutdown and reset.

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Match-Demo1-Matchmaking/GameManager.cs.Shutdown_method.rst

**4. Edit Network.cs:**

The start method must get the player name from the login script. Copy the highlighted code below into your Network script (Network.cs).

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Match-Demo1-Matchmaking/Network.cs.Start_method_(Chat).rst

**5. Edit Matchmaking.cs:**

Add GameState enumeration at the top of Matchmaking.cs file.
This enumeration shows several different game states used in this demo. You can choose to define custom game states relevant to your game.

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Match-Demo1-Matchmaking/Matchmaking.cs.Game_state_enumeration.rst

Copy the following fields into the Matchmaking class.

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Match-Demo1-Matchmaking/Matchmaking.cs.Demo_Match-0_Fields.rst

Add these following three methods; ``Start``, ``OnGUI`` and ``OnDisable`` to the Matchmaking class.

Set the initial game state to ``GameState.Lobby``.
Also initialize the matchmaking window rectangle inside the ``Start`` method.

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Match-Demo1-Matchmaking/Matchmaking.cs.Start_method.rst

Display the matchmaking GUI after logging into Badumna network.

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Match-Demo1-Matchmaking/Matchmaking.cs.OnGUI_method.rst

Destroy the object and leave from the current match on ``OnDisable``.

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Match-Demo1-Matchmaking/Matchmaking.cs.OnDisable_method.rst

To make sure that the game state is updated correctly, add the following highlighted code to ``OnMatchStatusChange`` method.

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Match-Demo1-Matchmaking/Matchmaking.cs.OnMatchStatusChange_method_(Basic).rst

Set the game state to *FindingMatch* in the ``FindMatch`` method.

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Match-Demo1-Matchmaking/Matchmaking.cs.FindMatch_method.rst

Set the game state to *JoiningMatch* in the ``JoinMatch`` method.

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Match-Demo1-Matchmaking/Matchmaking.cs.JoinMatch_method.rst

We will now add a method for creating the window and displaying appropriate messages based on the current game state.

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Match-Demo1-Matchmaking/Matchmaking.cs.MatchmakingWindow_method.rst

**6. Configure the GameManager**

Finally, we need to specify which assets will be used for the user interface skin.
This can be done in the Unity editor by configuring the fields that have been added to the GameManager.

- Select the GameManager object in the Hierarchy window, so that its configurable fields are viewable in the Inspector window.
- Drag the mobile asset from the GUISkin folder in the Project window to the IOSSkin field in the Inspector window.

.. admonition:: BadumnaCloud only !!!

  - Set the Application Identifier to the ID for the application you created on `cloud.badumna.com <cloud.badumna.com>`_ (see :ref:`chapter:get-started`).

.. admonition:: Badumna Pro only !!! 
  
  - Drag the NetworkConfiguration.cs from the Script folder in the Project window to the GameManager Inspector window.
  - Set the matchmaking server address. 
  - Configure other badumna options according to your needs.

**7. Build and run the game**

.. include:: build-and-run-unity.txt

Build and run a couple of instances of the game to try out the basic match functionality.

.. admonition:: Badumna Pro only !!!

  .. include:: build-and-run-unity-match-badumna-pro.txt


