.. include:: ../apilink-directives.txt

.. _scene-replication:

Badumna Scenes: Entity Replication
==================================

Using Badumna Scenes, your game entities can be automatically replicated to other peers nearby in the virtual world.
To set up basic replication you need to:

- Define your replicable entity.
- Register entity details.
- Join a scene.
- Register your entity with the scene.

To end replication:

- Unregister your entity from the scene.
- Leave the scene.

Define your replicable entity
-----------------------------

Any class you want to use to represent a replicable entity has to implement the ``IReplicableEntity`` interface.
It requires a ``Position`` property, which is automatically replicated by Badumna.
Other properties which you want to be replicated should be marked with the ``[Replicable]`` attribute.

For example, the following ``Avatar`` class includes a replicable ``Colour`` property.

.. sourcecode:: c#

    public class Avatar : IReplicableEntity
    {
        public Vector3 Position { get; set; }

        [Replicable]
        public Color Colour { get; set; }
    }

.. include:: type-replication-warning.txt
.. include:: smoothing-info.txt

Register entity details
-----------------------

In order to optimize interest management, Badumna needs to know the maximum entity area of interest (AOI) radius that will be used, and the maximum speed that any entity will move at.
``RegisterEntityDetails`` must be called before any scene is joined.

.. sourcecode:: c#

    float maximumAreaOfInterest = 250f;
    float maximumEntitySpeed = 50f;
    this.network = this.network.RegisterEntityDetails(maximumAreaOfInterest, maximumEntitySpeed);
    
.. warning::
   **AOI and bandwidth consumption**
   
   Increasing entities' AOI will typically lead to increased bandwidth consumption, so care should be taken to choose AOI values that are large
   enough to provide a consistent experience to users, *but no larger*.
        
Join a scene
------------

When you join a scene, you need to provide delegates for creating and removing replicas that Badumna will call when other replicable entities enter or leave your area of interest.
The replica creation delegate should instantiate a class with the same replicable properties as the original entity, and keep a reference to the new replica so that your game can render the entity.
The replica removal delegate just needs to do any local clean up - typically just removing the reference that was held.

.. sourcecode:: c#

    this.scene = this.network.JoinScene("scene1", this.CreateReplica, this.RemoveReplica);

    private IReplicableEntity CreateReplica(NetworkScene scene, BadumnaId entityid, uint entitytype)
    {
        var replica = new Avatar();
        this.replicas.Add(replica);
        return replica;
    }

    private void RemoveReplica(NetworkScene scene, IReplicableEntity replica)
    {
        this.replicas.Remove((Avatar)replica);
    }
    
Register your entity with the scene
-----------------------------------

Register your entity with the scene, using an ID that will be passed to the ``CreateReplica`` delegate on other peers to indicate the type of the replica that needs to be created.
You must also pass the bounding radius of the entity and the radius of its area of interest.
Only entities whose bounding sphere intersects the sphere of interest of one of your original entities will be replicated to you.

.. sourcecode:: c#

    this.scene.RegisterEntity(this.localAvatar, entityType, radius, areaOfInterestRadius);

Unregister entity from the scene
--------------------------------

``UnregisterEntity`` method is the opposite of ``RegisterEntity`` method. 
You need to call this method in order to stop Badumna from replicating your entity in the scene.

.. sourcecode:: c#

    this.scene.UnregisterEntity(this.localAvatar);

This method is useful if you want to swap your current entity with a different entity in the middle of the game without leaving the scene.
To achieve this you will first have to unregister the old entity from the scene and then register the new entity with the scene.

Leave a Scene
--------------

Call ``Leave`` method to leave from the scene. All entities will automatically be unregistered from that scene.

.. sourcecode:: c#

    this.scene.Leave();

Try it out
----------

See replication in action:

- :ref:`Unity Basic Replication Tutorial<unity-scene-tutorial1>`
