.. _distributed-validation:

Badumna Scenes: distributed validation [Pro]
============================================

.. include:: pro-tipbox.txt

One of the major concerns in online gaming is cheating. There are
several ways in which players can attempt to cheat, and many of them
involve locally running a version of the game which has been modified
(‘hacked’) to give them an unfair advantage.

Online games running in a server-client architecture typically address
this problem by not trusting the clients, and game logic is run on the
server. In a fully distributed peer-to-peer network this option is not
available.

A hybrid architecture can offer protection against hacked clients by
allowing game logic to run on arbitrary peers within the network,
including trusted peers. For example, instead of having a player’s
character running on their local machine, it could run on a
disinterested peer, or even a centrally-controlled trusted peer.

Running a player’s character on a peer that is not controlled by the
player means that the player cannot hack their client to directly
manipulate their own character’s state. Running on a disinterested peer
reduces motivation of the controller of that peer to try and manipulate
game state. Running on a trusted peer is effectively like running on a
server, but with the added advantage that you could dynamically switch
between client-server mode and peer-to-peer mode.
Such a design immediately raises the bar for cheating, and can offer the security of a client-server architecture on-demand.

Badumna includes a validation system that allows game entities to run on other peers in the
network. The validation is considered an ‘experimental feature’ in this
version of Badumna. That means that while the core functionality has
been implemented and tested, the API may be subject to change in future
versions based on feedback. Future versions are intended to include
additional security features such as digital signing of updates, and
authorisations.

Source code for XNA and Unity based distributed validation demo applications are available upon request from `Scalify <http://www.scalify.com/contact.php>`_.

.. tip::

    **Mobile platforms**

    Note that Distributed Validation feature has been fully tested on mobile platforms.

Key concepts
------------

When using validated distribution for an entity, Badumna will assign a
peer in the network to act as the entity’s *validator*. The *validated
entity* will be hosted on the validator. The *originating peer* (e.g.
the player’s own peer) will send input to the validator, and the
validator will send updates in reply. Since this introduces latency as
in a client-server architecture, Badumna uses a latency hiding technique
known as *client-side prediction*.

Validator assignment
~~~~~~~~~~~~~~~~~~~~

A ‘master server’ is responsible for deciding which peers validated
entities will run on. A validated entity is instantiated on the local
machine and registered with Badumna. Badumna will contact the master
server to request a validator for the entity. Once a validator is
assigned, the originating peer will send a registration request to the
validator. The validator will send the initial state for the entity to
the originating peer. The originating peer sends input for the entity to
the validator and receives back state updates. If the validator goes
offline, Badumna will automatically assign a new validator. Any short
delay between the current validator going offline and a new one being
assigned is hidden from the player using the same technique used to hide
the built-in latency of hosting the entity on a validator: client-side
prediction.

.. tip::
   **Secure game state**
   
   Badumna currently allows validated entities to be registered with
   any initial state. In future it will be required to pass a digitally
   signed entity state, that has been signed by an authorized party such as
   a trusted central service, or an authorised validator.

Client-side prediction
~~~~~~~~~~~~~~~~~~~~~~

Since there will be some delay between the input being sent to the
validator and the originating peer receiving the new state, Badumna uses
client-side prediction to hide this latency. Client-side prediction is a
latency hiding technique commonly used in server-client games. In
client-side prediction, the originating peer will update the validated
entity locally, and buffer input used for the update. Upon receiving the
state update from the validator, the originating peer will restore the
entity’s state to the validated state, and then reapply buffered input
to correct its predicted state. It is important that both the same code
is used to predict client updates on the originating peer and make
authoritative updates on the validator.

.. tip::
   **Learn more about client side prediction:**

   Client-side prediction is described in Yahn Bernier's paper ``Latency Compensating Methods in Client/Server In-game Protocol Design and Optimization``, which is available on the  
   `Valve website`_, or as a `PDF`_ on Mark Claypool's website.

.. _Valve website: http://developer.valvesoftware.com/wiki/Latency_Compensating_Methods_in_Client/Server_In-game_Protocol_Design_and_Optimization

.. _PDF: http://web.cs.wpi.edu/~claypool/courses/4513-B03/papers/games/bernier.pdf

Replication
~~~~~~~~~~~

Replication of validated entities to other interested peers works in the
same way as the replication of all spatial entities. The only difference
is that updates are sent from the validator, rather than from the
originating peer. However, the validation framework introduces a simpler
API for implementing spatial entities, meaning developers do not need to
implement their own entity serialization and deserialization, as
explained in the following section.

Badumna API Usage
-----------------

Validated entities should derive from the Badumna class
(|ValidatedEntity|). Validation is controlled through the validation
facade, which is exposed through the network facade’s Validation
property.

Creating a validated entity
~~~~~~~~~~~~~~~~~~~~~~~~~~~

Badumna provides the |ValidatedEntity| class to be used as the base
class for all validated entities. It implements the ISpatialReplica
interface, so the same class can be used to represent the entity on the
originating peer, and its replicas on remote peers.

When creating a validated entity, it is necessary to:

-  Specify the entity’s radius and its area of interest radius.

-  Identify which properties are to be included in its replicable state
   with the |Replicable| attribute.

-  Implement an |Update| method.

A validated entity’s replicable state is composed of all the properties
that are replicated from the validator to the original and replicas. For
client-side prediction to function correctly, the replicable state must
include all properties that are used to calculate state updates. To
indicate that a property is part of the replicated state, it should be
marked with the |Replicable| attribute. The only types of property
that can form part of the replicable state are: int, float, bool,
string, and Badumna’s Vector3D.

The validated entity’s Update method must update the entity’s state as a
function of its existing replicable state, its input, and the time
interval for the update. This is because the update needs to be
calculated in the same way on the originating peer and the validator to
stop the predicted state from deviating from the true state.

The exception to this is when a validated entity needs to react to a
custom message. Custom messages sent from other peers will be received
by the entity hosted on the validator. Since the originating peer will
not be aware of the message, it cannot take it into account, and its
predicted state may not match the authoritative state calculated on the
validator. This error is automatically corrected the next time an update
is received from the validator.

Note that the Update method should not be called directly by application
code. It is called by Badumna during the call to
|NetworkFacade.ProcessNetworkState()|. When calling Update, Badumna
will indicate why Update is being called by passing the appropriate
|UpdateMode|. On the validator, Badumna will call Update once for each
time it receives input from the originating peer, setting the update
mode to Authoritative. On the originating peer, Badumna will call Update
once for each time |IValidationFacade.UpdateEntity| is called, setting
the update mode to Prediction. If an update has been received from the
validator since the last call to Update on the originating peer, then
Badumna will call Update for each buffered input since the input the
update corresponds to, setting the UpdateMode to Synchronisation.

The update function may trigger different actions depending on the
update mode, as long as these actions do not affect replicated state.
For example, if the user input triggers a spell being cast, the update
in Prediction mode may trigger a visual effect, but the update in
Authoritative mode may send a custom message to the target of the spell.

The ValidatedEntity class provides SaveState, RestoreState, Serialize
and Deserialize methods that are used by Badumna to store and
communicate the entity’s replicable state.

The validation facade
~~~~~~~~~~~~~~~~~~~~~

An application using distributed validation will need to perform the standard initialization and shutdown detailed in :ref:`setting-up-pro`, including configuring Badumna, creating a network facade, logging in, registering entity types and joining a scene.

To use validation, the following steps are required:

-  Add master validation server details to the Badumna options.

   |Options.Validation.Servers|

   Add the address of the master validation server to the list of
   servers in the validation configuration module. The name used is not
   important. Distributed lookup is not currently supported for the
   master validation server.

-  Configure the peer as a validator.

   |ConfigureValidator(AllocationDecisionMethod, CreateValidatedEntity, RemoveValidatedEntity)|

   If a peer is to act as a validator, it needs to be configured with
   three delegates: one for deciding whether to accept an allocation
   request from the master server, one for creating validated entities
   on receipt of a registration request from a peer, and one for
   handling validated entity removal. The validated entity factory
   method takes an integer indicating the type of entity to create,
   which should match the integer used by the CreateSpatialReplica
   delegate used when joining a scene.

-  Register a validated entity.

   |RegisterValidatedEntity(IValidatedEntity, UInt32, ISerializedEntityState)|

   After creating a validated entity, register it with Badumna. The
   registration call accepts an integer representing the type of the
   entity, which will be passed to the CreateValidatedEntity delegate on
   the validator, and the CreateSpatialReplica delegate on other peers.
   Optionally, an initial state can be passed when registering the
   entity. This state should be obtained from the entity by calling its
   |SaveState| method. The entity’s current replicable state is
   ignored, unless provided in this fashion, since in future any initial
   state will need to be digitally signed by an authorised party.

-  Register validated entities with a scene.

   | RegisterEntityWithScene(IValidatedEntity, NetworkScene)|

   Validated entities should be registered with a scene via this method
   on the facade, rather than methods on the scene object. The scene
   instance should be obtained in the normal way, by joining a scene.

-  Update validated entities.

   :netapi:`UpdateEntity(IValidatedEntity, TimeSpan, byte[]) E13A8739`

   Entities should be updated by passing a time interval and a byte
   array representing user input to the facade’s UpdateEntity method.
   The user input is passed as a byte array to allow applications to
   create the most efficient serialized user input representation
   possible for their game. Applications will typically implement their
   own user input class which can serialize itself to a byte array, and
   initialize itself from a byte array.

-  Leaving a scene.

   | UnregisterEntityFromScene(IValidatedEntity)|

   Validated entities should be unregistered from a scene via this
   method on the facade.

-  Unregistering a validated entity.

   |UnregisterValidatedEntity(IValidatedEntity)|

   To stop validation, validated entities should be unregistered from
   Badumna via this method on the facade.

-  Notifications

   |ValidationFailed| and |ValidatorOnline|

   Applications can receive notifications of validation failures and
   validation going live by subscribing to these two events. In
   addition, a validated entity’s |Status| property can be checked to
   see if the entity is live (currently being hosted by a validator),
   transitioning (trying to move to a new validator, and able to
   continue updating via client side prediction), or paused (has not yet
   been assigned a validator, or transitioning has failed). When a
   validator has gone offline, a new validator will automatically be
   assigned. Peers are pre-assigned a second validator to transition to.

XNA Demo
--------

Source code for the XNA distributed validation demo is available on request from `Scalify
<http://www.scalify.com/contact.php>`_.

This demo requires XNA Game Studio 4.0. 

The demo game allows players to control a simple avatar, moving around a
2D space. They can change their avatar’s colour, and play a simple sound
that can be heard by nearby players. The demo uses the simple
update/render game loop implemented by default in an XNA game. There are
three main area of functionality that need to be added to use
distributed validation: defining a user input class, defining a
validated entity class, and setting up the distributed validation.

Creating a user input class
~~~~~~~~~~~~~~~~~~~~~~~~~~~

Since user input for different games can vary widely, applications are
required to implement their own input class with as efficient a
serialization as possible. In the demo, the only inputs are cursor keys
for movement, a colour change key, and an action key. The movement of
the avatar depends upon whether or not the cursor keys are currently
down. The colour change and action execution depend upon key-down
events, i.e. holding their keys down does not generate multiple colour
changes or actions. The UserInput class in the demo only requires six
flags: four to indicate cursor key states, and two to indicate whether
key-down events occurred for the colour or action key. These flags are
represented as booleans, but for efficiency they are serialized as bits
in a single byte. 

.. sourcecode:: c#

    /// <inheritdoc/>
    public byte[] ToBytes()
    {
        byte data = 0;
        if (this.up)
        {
            data |= 1;
        }

        if (this.down)
        {
            data |= 2;
        }

        if (this.left)
        {
            data |= 4;
        }

        if (this.right)
        {
            data |= 8;
        }

        if (this.action)
        {
            data |= 16;
        }

        if (this.colour)
        {
            data |= 32;
        }

        return new byte[1] { data };
    }

.. sourcecode:: c#

    /// <summary>
    /// Initializes this instance with data deserialized from a byte array.
    /// </summary>
    /// <param name="bytes">The serialized data.</param>
    public void FromBytes(byte[] bytes)
    {
        Debug.Assert(bytes.Length == 1, "Serialized user input should be one byte long.");
        byte data = bytes[0];
        this.up = (data & 1) > 0;
        this.down = (data & 2) > 0;
        this.left = (data & 4) > 0;
        this.right = (data & 8) > 0;
        this.action = (data & 16) > 0;
        this.colour = (data & 32) > 0;
    }

Creating a validated entity class
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The Player class is used to create player avatars as validated entities.
It is derived from the Badumna class ValidatedEntity, and so inherits
Position, Radius and AreaOfInterestRadius properties. Since we want
avatars’ colour to be replicated, there is a property representing
colour that is marked with the Replicable attribute. Replicable
properties can only have the type int, float, bool, string or Badumna’s
Vector3, so the colour is represented as an integer, and the Player
class has a dictionary mapping integers to XNA colours.

The Player class’s Update method controls the behaviour of
the avatar.

.. sourcecode:: c#

    /// <summary>
    /// Update the player.
    /// </summary>
    /// <remarks>
    /// The player updates should depend upon current replicated state, user input, and time interval only.
    /// </remarks>
    /// <param name="interval">The time interval for this update.</param>
    /// <param name="userInput">Serialized user input.</param>
    /// <param name="mode">The update mode.</param>
    public override void Update(TimeSpan interval, byte[] userInput, UpdateMode mode)
    {
        UserInput input = new UserInput(userInput);
        if (userInput == null)
        {
            throw new ArgumentException("input must be non-null instance of UserInput.");
        }

        float x = this.Position.X;
        float y = this.Position.Y;
        if (input.Up && !input.Down)
        {
            y -= (float)interval.TotalSeconds * Player.MoveSpeed;
        }
        else if (input.Down && !input.Up)
        {
            y += (float)interval.TotalSeconds * Player.MoveSpeed;
        }

        if (input.Left && !input.Right)
        {
            x -= (float)interval.TotalSeconds * Player.MoveSpeed;
        }
        else if (input.Right && !input.Left)
        {
            x += (float)interval.TotalSeconds * Player.MoveSpeed;
        }

        this.Position = new BVector3(x, y, 0);

        if (input.Colour && !this.colourPressed)
        {
            this.Colour++;
            this.Colour = this.Colour % ColourTable.Count;
        }

        this.colourPressed = input.Colour;

        if (input.Action && !this.actionPressed)
        {
            if (mode == UpdateMode.Prediction)
            {
                this.TriggerAction();
            }
            else if (mode == UpdateMode.Authoritative)
            {
                this.NetworkFacade.SendCustomMessageToRemoteCopies(this, new MemoryStream());
            }
        }

        this.actionPressed = input.Action;
    }

A user input object is instantiated and initialized with the byte array
holding serialized user input. The avatar’s position is updated,
according to movement key input, and the player’s colour is updated
according to whether the colour change key was pressed. The action taken
in response to the action key being pressed depends upon the update
mode. If the update mode is prediction, the player fires off its action
event (the game subscribes to this event, and plays a sound when the
event is received). This means that the action will feel very responsive
to the player. If the update mode is authoritative, this means that this
is happening on the validator, and so the action is sent as a custom
message to all replicas. This means that replicas will only ‘see’ the
action if the validator decides it should happen as a result of the user
input. If the update mode is synchronisation, then nothing will happen.
This means that the sound will not be played multiple times during
synchronisation on the originating peer.

The player class is also used to represent replicas on remote peers, so
it handles the custom message and triggers the action event.

Setting up distributed validation
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

When the game class (Game1) is initialized, it triggers asynchronous
initialization of Badumna via the StartBadumna method. This method takes
care of typical start up requirements, including specifying Badumna
options, initializing Badumna, logging in, and registering entity
details. When specifying Badumna options, it is necessary to include the
address of the master server. This will need editing to point to the
machine where you are running your master server (any peer can act as
the master server - it just needs to be running at a known address and
port). The ConfigureValidator method is called to configure the peer as
a validator. The peer needs to join a scene as usual. The method then
subscribes to the ValidatorOnline and ValidationFailed events, so it can
display helpful debug information on screen. A player instance is
created and its initial state is saved. The player is then registered
with Badumna for validation, and registered with the scene that was
joined, so it can be seen by other players in the scene.

.. sourcecode:: c#

    /// <summary>
    /// Initialize Badumna and log in to the network.
    /// </summary>
    private void StartBadumna()
    {
        // Set up the Badumna options
        Options options = new Options();

        // Use LAN mode for testing
        options.Connectivity.ConfigureForLan();

        // To use on the Internet, comment out the ConfigureForLan() line above and
        // add a known seed peer. e.g.:
        ////options.Connectivity.SeedPeers.Add("seedpeer.example.com:21251");

        // Set the application name
        options.Connectivity.ApplicationName = "validation-demo";

        // Set the master validation server address.
        options.Validation.Servers.Add(new ValidationServerDetails("master", "example.com:21270"));

        // Initialize Badumna and log in.
        this.networkFacade = NetworkFacade.Create(options);
        this.status = "Logging in.";
        this.networkFacade.Login();
        this.status = "Logged in.";

        // Register entity types to be replicated.
        this.networkFacade.RegisterEntityDetails(
            Player.InterestRadius,
            new Badumna.DataTypes.Vector3(Player.MoveSpeed, Player.MoveSpeed, 0f).Magnitude);

        // Configure validation to accept validation requests.
        this.networkFacade.ValidationFacade.ConfigureValidator(
            () => true,
            this.CreateValidatedEntity,
            this.OnValidatedEntityRemoval);

        // Join a scene.
        this.scene = this.networkFacade.JoinScene("foo", this.CreateSpatialReplica, this.RemoveSpatialReplica);

        // Subscribe to validation events.
        this.networkFacade.ValidationFacade.ValidatorOnline += this.OnValidatorOnline;
        this.networkFacade.ValidationFacade.ValidationFailed += this.OnValidationFailure;

        // Create the player.
        this.player = new Player(this.networkFacade);
        this.player.ActionTriggered += () => { this.localBleep.Play(); };
        this.status = "Registering player.";

        // Set player's initial state
        this.player.Position = new Badumna.DataTypes.Vector3(200f, 200f, 0);
        this.player.Colour = 3;
        ISerializedEntityState initialState = this.player.SaveState();
        
        // Register the player for validation.
        this.networkFacade.ValidationFacade.RegisterValidatedEntity(this.player, this.playerEntityType, initialState);

        // Join the scene.
        this.networkFacade.ValidationFacade.RegisterEntityWithScene(this.player, this.scene);

        this.started = true;
    }

The game class’s Update method allows users to exit the game by hitting
the ‘Q’ key. Calling the network facade’s Shutdown method will
automatically take care of unregistering any validated entities. The
Update method queues any exit request for execution next loop to allow
the new ‘Quitting’ status to be drawn to the screen before shutting
down.

If initialization has completed (as indicated by the value of the
‘started’ field, the Update method determines what the user input for
that update should be by examining the keyboard state and comparing it
to the previous keyboard state. It triggers updating of the player by
calling the UpdateEntity method. This will schedule the calling of the
player’s Update method locally, and send the input to the assigned
validator for validation. As in all Badumna applications, the update
method also needs to call ProcessNetworkState on the network facade. It
is during this call that the validate entity’s Update method is actually
called, to ensure that all application-provided code is executed in the
same thread.

.. sourcecode:: c#

    protected override void Update(GameTime gameTime)
    {
        // Shut down and exit if exit is queued.
        if (this.exitQueued)
        {
            this.started = false;
            this.networkFacade.Shutdown(false);
            this.Exit();
        }

        // Allows the game to exit
        KeyboardState newKeyboardState = Keyboard.GetState();
        if (newKeyboardState.IsKeyDown(Keys.Q))
        {
            // Queue exit for next loop, to allow status update to be drawn first.
            this.status = "Quitting.";
            this.exitQueued = true;
        }

        if (this.started)
        {
            // Don't do anything if the player is not registered.
            if (this.player.Status != EntityStatus.Paused)
            {
                // Get user input.
                UserInput input = new UserInput(
                    newKeyboardState.IsKeyDown(Keys.Up),
                    newKeyboardState.IsKeyDown(Keys.Down),
                    newKeyboardState.IsKeyDown(Keys.Left),
                    newKeyboardState.IsKeyDown(Keys.Right),
                    newKeyboardState.IsKeyDown(Keys.Enter) && !this.oldKeyboardState.IsKeyDown(Keys.Enter),
                    newKeyboardState.IsKeyDown(Keys.C) && !this.oldKeyboardState.IsKeyDown(Keys.C));

                // Update the player.
                this.networkFacade.ValidationFacade.UpdateEntity(
                    this.player,
                    gameTime.ElapsedGameTime,
                    input.ToBytes());
            }

            // Trigger Badumna regular processing
            this.networkFacade.ProcessNetworkState();
        }

        this.oldKeyboardState = newKeyboardState;

        base.Update(gameTime);
    }

Running the demo
~~~~~~~~~~~~~~~~

Before running the demo, make sure you have edited the master server
address in the Game1.StartBadumna() method. The demo is configured to run
in LAN mode. If you wish to test it over the Internet, you will need to
edit out the call to ConfigureForLan, and provide a SeedPeer address.

Any peer running at a known location can be used as the master
validation server. To use an instance of SeedPeer, use the following
command-line options:

.. container:: commandline
   
   ``SeedPeer.exe --application-name=validation-demo --badumna-config-port=21270``

Since a peer cannot act as its own validator, you will need to have at
least two instances of the demo game application running to be able to
‘play’.

Unity Demo
----------

Source code for the Unity distributed validation demo is available on request from `Scalify
<http://www.scalify.com/contact.php>`_.

This Unity tutorial will demonstrate how to build a very simple game
using the distributed validation framework. It is recommended that you
first look at the :ref:`unity-scene-tutorial1` to understand the general demo architecture.

Create a new project
~~~~~~~~~~~~~~~~~~~~

Open the Unity editor and create a new project. If you successfully copy
the package into the *Standard Packages* directory you should be able to
see it under the list of available packages when you create a new
project. Include the ValidationDemo.unitypackage only and then create
the project.

Open the Unity scene file
~~~~~~~~~~~~~~~~~~~~~~~~~

Open ValidationDemo.unity file. Set the application to run in background
and *Api Compatibility Level* to .NET 2.0. Both of these options can be
found under Player setting.

NetworkInitialization.cs
~~~~~~~~~~~~~~~~~~~~~~~~

As explained in the :ref:`unity-scene-tutorial1`, ***NetworkInitialization.cs*** handles Badumna
Network initialization, joining and leaving a scene, registering and
unregistering the local entity and creating and removing remote entities
in the current scene. In this demo, NetworkInitialization.cs will have a
couple more additional functionalities including configuring the
validation facade, registering and unregistering the local entity into
and from validation facade.

Network Configuration
^^^^^^^^^^^^^^^^^^^^^

Beside the normal Badumna network configuration, in order to use the
distributed validation in this demo you have to set the master
validation server address as well.

.. _code:9.1:

Badumna network configuration
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. sourcecode:: c#

            
    // set the options programatically
    Options badumnaConfigOptions = new Options();

    // The type of discovery mechanism to use. 
    // Badumna requires the knowledge of one existing peer to connect with in 
    // order to join the network. The address of any other peer can be 
    // 'discovered' through SeedPeers(peers that are 'always' running).

    // Use LAN mode for testing
    badumnaConfigOptions.Connectivity.ConfigureForLan();

    // To use on the Internet, comment out the ConfigureForLan() line above 
    // and add a known seed peer.  e.g.:
    // badumnaConfigOptions.Connectivity.SeedPeers.Add("seedpeer.example.com:21251");

    // Disable/enable the use of broadcast
    badumnaConfigOptions.Connectivity.IsBroadcastEnabled = true;

    // The port used for discovery of peers via UDP broadcast.
    badumnaConfigOptions.Connectivity.BroadcastPort = 21250;

    // The broadcast port shouldn't be withtin the peer port range, 
    // otherwise badumna will throw an exception
    badumnaConfigOptions.Connectivity.StartPortRange = 21300;
    badumnaConfigOptions.Connectivity.EndPortRange = 21399;

    // set the application name
    badumnaConfigOptions.Connectivity.ApplicationName = "validation-demo";

    // set the validation server address
    badumnaConfigOptions.Validation.Servers.Add(new ValidationServerDetails("master", "example.com:21270"));

Configure distributed validation
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This section will explain how to configure the distributed validation
and subscribe to validation events. Call
NetworkFacade.ValidationFacade.ConfigureValidator(
allocationDecisionMethod, validatedEntityFactoryMethod,
onValidatedEntityRemoval) to configure the validator, where:

-  *allocationDecisionMethod* is a callback function that is called when
   there is a remote peer that is requesting this local peer to be its
   validator. If the callback function return true, that means this
   local peer agree to be its validator.

-  *validatedEntityFactoryMethod* is a callback function for
   instantiating validated entities on a validator. It will be called by
   Badumna when this local peer has agreed to validate a remote peer,
   and it receives an entity registration request.

-  *onValidatedEntityRemoval* is a callback function that is called on a
   validator to remove a validated entity. It will be called by Badumna
   when the validator receives an entity unregistration request, or the
   validated entity’s originating peer is no longer active (e.g. it has
   logged out from the Badumna network).

The validation facade has two events, ValidatorOnline event and
ValidationFailed event. Subscribe to these events to keep track the
status of the validator.

.. sourcecode:: c#

    // Configure validation to accept validation requests.
    NetworkInitialization.Facade.ValidationFacade.ConfigureValidator(
        () => true,
        this.CreateValidatedEntity,
        this.OnValidatedEntityRemoval);

.. sourcecode:: c#

    // Subscribe to validation events.
    facade.ValidationFacade.ValidatorOnline += this.OnValidatorOnline;
    facade.ValidationFacade.ValidationFailed += this.OnValidationFailure;

Register Local Entity with the validation facade
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

In this demo, the local entity will not be registered with the network
scene directly but instead it will be registered via the validation
facade. The steps that are required to register a local entity are as
follows:

#. Create the game object that will be used as a local player (see
   CreateLocalPlayer() function).

#. Create a new instance of the *Player* class. (*Note: Player class
   will be explained in more details in the next section.*)

#. Add CharacterController, CameraFollowerScript, PlayerController and
   AnimationController components into the local player game object.

#. Register the local player via the validation facade by calling
   RegisterValidatedEntity and RegisterEntityWithScene (see
   :ref:`code:9.4`).

.. _code:9.4:

CreateLocalPlayer() function
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^   
 
.. sourcecode:: c#

    private bool CreateLocalPlayer()
    {
        try
        {
            uint entityType = (uint)PlayerType.SmallLerpz;
            GameObject playerObject = (GameObject)GameObject.Instantiate(this.ListOfAvatars[(int)entityType], transform.position, transform.rotation);

            if (playerObject != null)
            {
                // set all the components required
                CharacterController controller = playerObject.AddComponent<CharacterController>();
                controller.radius = 0.4f;
                controller.center = new UnityEngine.Vector3(0, 1.1f, 0);

                playerObject.AddComponent(typeof(CameraFollowerScript));

                Player player = new Player(
                    NetworkInitialization.Facade,
                    new Badumna.DataTypes.Vector3(playerObject.transform.position.x, playerObject.transform.position.y, playerObject.transform.position.z),
                    controller);

                this.playerController = playerObject.AddComponent<PlayerController>();
                this.playerController.Player = player;
                this.playerController.Controller = controller;
                this.playerController.GetUserInput = true;
                this.playerController.PlayerId = Guid.NewGuid();

                playerObject.AddComponent<AnimationController>();

                ISerializedEntityState initialState = player.SaveState();

                // Register the player for validation.
                NetworkInitialization.Facade.ValidationFacade.RegisterValidatedEntity(player, (uint)PlayerType.SmallLerpz, initialState);

                // Join the player to a scene.
                NetworkInitialization.Facade.ValidationFacade.RegisterEntityWithScene(player, this.networkScene);
                this.isRegistered = true;

                return true;
            }
        }
        catch (Exception e)
        {
            Debug.LogError(e);
            return false;
        }

        return false;
    }

Create and Remove Entity
^^^^^^^^^^^^^^^^^^^^^^^^

Create and Remove Entity callback functions are very similar to the Create and Remove Entity callback functions in the :ref:`Unity Basic Replication demo <unity-scene-tutorial1>`.
Please refer to :ref:`unity-scene-tutorial1` for more information regarding these functions.

Validation event handlers
^^^^^^^^^^^^^^^^^^^^^^^^^

The *OnValidatorOnline* event handler will be called when the local peer
has successfully connected to a validator. The *OnValidationFailure*
event handler will be called when the local peer cannot reach the master
validator server or has no validator available. When a validation
failure is triggered you have to reset the local player state to the
last valid state otherwise the state might not be synchronized properly
the next time this peer connected to a new validator.

.. sourcecode:: c#

    private void OnValidatorOnline(object sender, ValidatorOnlineEventArgs e)
    {
        Debug.Log("Live validating.");
    }

    private void OnValidationFailure(object sender, ValidationFailureEventArgs e)
    {
        if (e.Reason == ValidationFailureReason.ServerUnreachable)
        {
            Debug.Log("Failure: server unreachable. Retrying...");
        }
        else
        {
            Debug.Log("Failure: no validator available. Retrying...");
        }

        if (this.playerController != null)
        {
            this.playerController.Player.NeedResettingPosition = true;
        }
    }

Create Validated Entity
^^^^^^^^^^^^^^^^^^^^^^^

Badumna will call the CreateValidatedEntity callback function previously
passed to ConfigureValidator when there is a remote peer that requested
this local peer to be its validator and the request has been accepted.
The steps required to create a validated entity are listed below.

#. Create an instance of validator prefab, where the validator prefab
   has a CharaterController component attach to it. As the validator
   will validate the movement of a local player on another peer,
   collision detection should be considered. In this case,
   CharacterController is required for calculating the collision.

#. Create the Player instance and store it in the validated entity list
   to keep track of the number of validated entities running on this
   local peer.

.. sourcecode:: c#

    private IValidatedEntity CreateValidatedEntity(uint entityType)
    {
        if ((PlayerType)entityType == PlayerType.SmallLerpz)
        {
            GameObject validatorObject = (GameObject)GameObject.Instantiate(this.ListOfAvatars[(int)PlayerType.Validator], transform.position, transform.rotation);
            CharacterController controller = validatorObject.GetComponent<CharacterController>();

            Player validatedEntity = new Player(
                NetworkInitialization.Facade,
                new Badumna.DataTypes.Vector3(validatorObject.transform.position.x, validatorObject.transform.position.y, validatorObject.transform.position.z),
                controller);

            this.validatedEntities.Add(validatedEntity, validatorObject);
            return validatedEntity;
        }

        return null;
    }

Remove Validated Entity
^^^^^^^^^^^^^^^^^^^^^^^

Validated entity removal is the opposite of validated entity creation.
Accordingly, the OnValidatedEntityRemoval function will be called when
the remote peer has unregistered its validated entity, or the remote
peer has logged out from the Badumna network.

.. sourcecode:: c#

    private void OnValidatedEntityRemoval(IValidatedEntity entity)
    {
        GameObject validatorObject;
        if (this.validatedEntities.TryGetValue(entity, out validatorObject))
        {
            Destroy(validatorObject);
            this.validatedEntities.Remove(entity);
        }
    }

Destroy the validator object before removing the validated entity from
the list.

PlayerController.cs
~~~~~~~~~~~~~~~~~~~

The PlayerController class is responsible for updating the local entity
based on user input or applying updates to a remote player object if
this PlayerController instance belongs to a remote entity. The
*getUserInput* flag is used to differentiate whether this
PlayerController is used by the local entity or remote entity.

Update
^^^^^^

If the PlayerController instance belongs to a local entity, the current
user input will be sent to the validator by calling the
*ValidationFacade.UpdateEntity* (see :ref:`code:9.9`) function. Also
call *ProcessNetworkState()* afterward (see :ref:`code:9.8`) to update
Badumna state. Otherwise, apply the position update to the remote player
object and adjust the orientation based on the last two position
updates.

.. _code:9.8:

Update() function
^^^^^^^^^^^^^^^^^^

.. sourcecode:: c#

    public void Update()
    {
        if (this.getUserInput)
        {
            this.UpdateEntity();

            if (NetworkInitialization.Facade.IsLoggedIn)
            {
                NetworkInitialization.Facade.ProcessNetworkState();
            }
        }
        else
        {
            // a replica.
            Vector3 newPosition = new Vector3(
                this.player.Position.X,
                this.player.Position.Y,
                this.player.Position.Z);

            Vector3 moveDirection = newPosition - this.lastPosition;
            this.lastPosition = newPosition;
            
            this.transform.position = newPosition;

            // update the orientation.
            moveDirection.y = 0;
            if (moveDirection != Vector3.zero)
            {
                this.transform.rotation = Quaternion.LookRotation(moveDirection);
            }
        }
    }

.. _code:9.9:

UpdateEntity() function
^^^^^^^^^^^^^^^^^^^^^^^^

.. sourcecode:: c#

    private void UpdateEntity()
    {
        Transform cameraTransform = Camera.main.transform;
        Vector3 forward = cameraTransform.TransformDirection(Vector3.forward);
        forward.y = 0.0f;
        forward = forward.normalized;

        int vertical = Input.GetAxis("Vertical") == 0 ? 0 : (int)Mathf.Sign(Input.GetAxis("Vertical"));
        int horizontal = Input.GetAxis("Horizontal") == 0 ? 0 : (int)Mathf.Sign(Input.GetAxis("Horizontal"));

        UserInput userInput = new UserInput(vertical == 1, vertical == -1, horizontal == -1, horizontal == 1, Input.GetButtonDown("Jump"), new Badumna.DataTypes.Vector3(forward.x, forward.y, forward.z));

        NetworkInitialization.Facade.ValidationFacade.UpdateEntity(
            this.player,
            TimeSpan.FromSeconds(Time.deltaTime),
            userInput.ToBytes());
    }

Player.cs
~~~~~~~~~

Player class is derived from the ValidatedEntity abstract class. It can
act as a local entity or as a remote entity since the ValidatedEntity
class implements IValidatedEntity in which implements both
ISpatialOriginal and ISpatialReplica. In contrast to the LocalAvatar and
RemoteAvatar classes from Demo1-BasicDemo, this class inherits methods
to automatically serialize and deserialize properties that are marked
with the Replicable attribute, so it is not necessary to implement these
methods (see :ref:`code:9.10`). This also means that FlagForUpdate
function will be called automatically by the ValidatedEntity class when
a property has been changed.

.. _code:9.10:

Replicable properties
^^^^^^^^^^^^^^^^^^^^^^

.. sourcecode:: c#

    [Replicable]
    public float MoveSpeed { get; set; }

    [Replicable]
    public bool Jumping { get; set; }

The behaviour of the player such as movement and jumping should be
calculated within the Update function. This Update function will be
called on the local peer and the validator peer. The behaviour of the
Update function will depend upon the UpdateModes. For example, it only
calls the character controller *Move* function when the UpdateMode is
prediction or authoritative. The others function such as
*UpdatedSmoothedMovementDirection*, *ApplyGravity*, etc are taken from
ThirdPersonController script and have been simplified for this demo
purposes.

.. sourcecode:: c#

    public override void Update(TimeSpan timeInterval, byte[] input, UpdateMode mode)
    {
        if (this.needResettingPosition && mode == UpdateMode.Prediction)
        {
            this.ResettingPosition();
        }

        UserInput userInput = new UserInput(input);

        if (userInput.Jump && mode != UpdateMode.Synchronisation)
        {
            this.lastJumpButtonTime = DateTime.Now;
        }

        this.UpdateSmoothedMovementDirection(userInput, timeInterval);

        this.ApplyGravity(userInput, timeInterval);

        this.ApplyJumping();

        Vector3 movement = (this.moveDirection * (float)this.MoveSpeed) + new Vector3(0, this.verticalSpeed, 0);
        movement *= (float)timeInterval.TotalSeconds;

        if (mode != UpdateMode.Synchronisation)
        {
            if (this.controller != null)
            {
                this.collisionFlags = this.controller.Move(new UE.Vector3(movement.X, movement.Y, movement.Z));
                this.Position = new Vector3(
                                    this.controller.transform.position.x,
                                    this.controller.transform.position.y,
                                    this.controller.transform.position.z);

                if (this.moveDirection.Magnitude > 0)
                {
                    this.controller.transform.rotation = UE.Quaternion.LookRotation(new UE.Vector3(this.moveDirection.X, this.moveDirection.Y, this.moveDirection.Z));
                }

                if (this.IsGrounded())
                {
                    this.Jumping = this.Jumping ? false : this.Jumping;
                }
            }
        }
        else
        {
            movement.Y = 0;
            this.Position += movement;
        }
    }

AnimationController.cs
~~~~~~~~~~~~~~~~~~~~~~

The AnimationController class is a simplified version of
ThirdPersonSimpleAnimation class and is used to play the animations of
the Lerpz character, both for local and remote players.

Build and run the game
~~~~~~~~~~~~~~~~~~~~~~

-  If you want to test the game on the Internet, make sure you have set
   the seed peer address in the Awake() function before building the
   game.

-  If you want to test the game on the Internet, make sure you have
   started a seed peer with “validation-demo” as the application name.

-  Make sure the master validation server address is set.

-  Make sure the *run in the background* option is set.

-  Make sure the *Api Compatibility Level* is set to .NET 2.0

You can use a seed peer or any regular peer as a master validation
server. In this tutorial we will use a seed peer as a validator server.
In order to do that the following steps are required.

#. If you want to test the game on LAN, make sure the seed peer started
   with ConfigureLan options. Otherwise start the seedpeer and make sure
   you have started the seed peer with “validation-demo” as the
   application name.

#. Set the validation server address with the address of the seedpeer.

#. Build and run the game. You need to start at least two instances to
   make this demo work.

If you have successfully set up the seed peer and started two instances
simultaneously, you should be able to see the two instances connected to each
other.

.. include:: ../apilink-directives.txt