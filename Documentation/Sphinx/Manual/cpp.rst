.. _cplusplus:

Badumna C++
===========

Introduction
------------

Badumna C++ provides a C++ interface to the Badumna library.
It works by using embedded Mono to host the Badumna .NET assembly.
Bindings are provided for the majority of Badumna's features.

Badumna C++ works with Windows, Linux, and OS X.
iOS is also supported but requires a suitable build of embedded Mono (please `contact us <http://www.scalify.com/contact.php>`_ for details).
The following sections explain how to use Badumna C++ in your project.
For more information on how Badumna itself works, please see :ref:`chapter:how-to`.

Where to get Badumna C++
------------------------

Badumna C++ is released as a separate SDK.
The Badumna Cloud C++ SDK can be downloaded from `cloud.badumna.com <https://cloud.badumna.com/>`_.
The Badumna Pro C++ SDK can be downloaded from `www.scalify.com/store.php <http://www.scalify.com/store.php>`_.

.. _section:compilingCpp:

Compiling Badumna C++
---------------------

The source code of Badumna C++ code is included in the SDK.
This makes it easy to use with different compilers, Standard Template Library implementations and third party libraries.

.. tip::
   **Embedded Mono for iOS**
   
   iOS requires a build of embedded Mono for the ARM platform, along with AOT-compiled libraries.
   Please `contact us <http://www.scalify.com/contact.php>`_ if you intend to use Badumna C++ on iOS.

For Windows users, Visual Studio project files are provided in the ``Source\MSVC`` sub-directory for both Visual Studio 2008 and Visual Studio 2010.

For both Windows and Linux users, a ``Makefile`` is included in the ``Source`` directory. Windows users will need to install MinGW to build via ``make``.
The default target builds Badumna C++ into a static library named ``libbadumna-cpp.a``. When compiling your own C++ project, this
means you should use the compiler flag ``-lbadumna-cpp``. All supported Makefile targets and variables are summarized in
tables :ref:`tab:CppMakefileTargets` and :ref:`tab:CppMakefileVars`.

.. _tab:CppMakefileTargets:

.. table:: Makefile Targets

   ==========   ===========================================
   **Target**   **Description**
   ----------   -------------------------------------------
   lib          Build the library
   apidoc       Build doxygen based API doc
   clean        Clean up all files generated during build
   install      Install the library and header files
   printvars    Print out important Makefile variables
   ==========   ===========================================

   
.. _tab:CppMakefileVars:

.. table:: Makefile variables

   ===============   =================================================
   **Variable**      **Description**
   ---------------   -------------------------------------------------
   release           Perform release build if defined
   verbose           Build in verbose mode if defined
   enable\_logging   Build with logging enabled
   MONO\_2\_PATH     The location of the Mono library
   INSTALL\_DIR      The location where Badumna C++ will be installed
   ===============   =================================================
   
  
For example, to build the Badumna C++ library in release mode, printing the commands executed, use the following command:

.. container:: commandline
   
   ``make release=1 verbose=1``

To install the library and public header files in ``/c/Badumna`` (the MinGW equivalent of ``C:\Badumna``):

.. container:: commandline
   
   ``make install INSTALL_DIR=/c/Badumna``

The only third party library that Badumna C++ depends on is the Mono library.
The included Visual Studio project files and the Makefile on Windows will use a customized cut down version of Mono 2.8.1 that is shipped as a part of the Badumna C++ SDK.
When compiling Badumna C++ on Linux you will need to specify the location of the Mono library using the MONO_2_PATH variable.

.. tip::
    Badumna C++ does not use C++ exceptions or RTTI, so they can be disabled.

Supported compilers and platforms
---------------------------------

Badumna C++ has been tested with the following compilers:

#. Visual Studio 2008 (VC++ 9) and Visual Studio 2010 (VC++ 10).

#. MinGW32 4.5.2.

#. GCC 4.4 and 4.5.

#. Clang 2.8 and 2.9.

Badumna C++ has been tested on the following OS platforms:

#. 32bit Windows XP/Vista/7.

#. 64bit Windows 7.

#. 64bit Linux (Ubuntu 12.04).

#. iOS 5 and 6.

Badumna C++ is also known to work on OS X.


C++ API Examples
----------------

A set of API examples is included in the Badumna C++ SDK.
These examples demonstrate how to use a number of Badumna's features.
The API examples make use of the `Qt framework <http://qt-project.org/>`_.
Qt is a cross platform library for developing GUI applications.
This framework is used for demonstration purposes only.
You are free to use any C++ based graphics engine when developing your application with Badumna C++.

API Examples
~~~~~~~~~~~~

The following C++ API examples are provided:

-  **Demo 0 - Initialization** shows how to initialize both the runtime environment and the Badumna engine.

   This console demo can be considered as the hello world application of Badumna C++.
   It initializes the runtime first, constructs a configuration options object and then uses it to initialize the Badumna network.
   The network status details will be printed out if everything works as expected.

-  **Match Demo** demonstrates the key features of the match system (see :ref:`badumna-matches`).

   The key logic of this demo can be found in the button click handlers of the *MainWindow* class and in the *MatchManager* class.

   .. tip::

       [Pro] To run this demo you will first need to start a seed peer with the following options:

       ``SeedPeer.exe --mm --application-name=match-demo -v``

       Once the seed peer has started, run the demo and enter the public address of the seed peer when prompted.

-  **Scene Demo 1 - Replication** demonstrates the basic scene-based entity replication feature (see :ref:`scene-replication`).

   The replication feature used in this application is implemented in the *ReplicationManager* class.
   The application joins a Badumna scene first with specified create and remove spatial replica delegates.
   It then registers an original entity to the obtained Badumna scene object.
   The area of interest radius and radius are set as entity properties.

-  **Scene Demo 2 - Proximity Chat** extends Scene Demo 1 by adding the proximity chat feature (see :ref:`scene-proximity-chat`).

   The *ReplicationManager* class in this example has a new member variable called *chat_session_*, it points to an *IChatSession* object returned from the *NetworkFacade*.
   Calling the *SendChannelMessage* method on this *chat_session_* member with a proximity chat channel sends the specified chat message to all nearby entities.

-  **Scene Demo 3 - Dead-Reckoning** shows how to use Badumna’s built-in dead-reckoning functionality to smooth replica movement.

   To implement a DeadReckonable entity, the original entity (DeadReckonableLocalEntity) should inherit from the *IDeadReckonableSpatialOriginal* class while the replica entity (DeadReckonableReplica) inherits from the *IDeadReckonableSpatialReplica* class.
   The actual position of the original entity is periodically calculated in the *UpdatePosition* method.
   On the remote replica side, each replica’s *AttemptMovement* method is regularly called with estimated current positions.

-  **Scene Demo 4 - Multiple Scenes** implements two different Badumna scenes in the same application. Entities will only be able to see others in the same scene.

   This demo is again extended from Scene Demo 1.
   The virtual space is divided into two Badumna scenes, separated by a vertical border line.
   Entities will only be able to see other entities on the same side (i.e. Badumna scene).
   The change scene code can be found in the *ChangeScene* method of the *ReplicationManager* class.

-  **Scene Demo 5 - Private Chat** :ref:`[Pro] <cpp-pro>` demonstrates the private chat feature (see :ref:`private-chat`). This includes the buddy list, buddy presence and private chat message delivery.

   Private chat related features are implemented in the *ChatManager* class.
   The *ChatManager* calls *OpenPrivateChannels* on the *IChatSession* object first to register a *ChatInvitationHandler* delegate which will be called on receiving chat invitations.
   Such invitations are used to form a private chat channel between two users.
   The invitation handler decides whether the incoming invitation should be accepted by calling the *AcceptInvitation* method.
   Calling the *AcceptInvitation* will also require the private chat message delegate and presence update delegate to be registered with the *IChatSession* object.
   These two delegates will be invoked accordingly on receiving incoming private chat messages and presence update events.

-  **Scene Demo 6 - Dei Server** :ref:`[Pro] <cpp-pro>` shows how to use Dei to authenticate users (see :ref:`verified-identities`).

   In the *MainWindow* class of this application, a *Dei.Session* object is first created.
   It calls the *Authenticate* with the specified Dei user-name and password to login to the Dei server, and then *SelectIdentity* with the desired character.
   The returned *IIdentityProvider* object is then passed to the *NetworkFacade*’s *Login* to login to Badumna networks.

   Please note that you need to run a Dei server to try this demo.
   A Dei server with a sample SQLite database is located in the *Examples/Binaries* directory.
   Start the server by running ``DeiServer.exe``.
   The *DeiAccounts.s3db.readme* file describes the accounts that are available.
   See :ref:`subsec:DeiServer` for more details about Dei server.

-  **Scene Demo 7 - Arbitration** :ref:`[Pro] <cpp-pro>` details how to use the arbitration feature (see :ref:`trusted-servers`).

   Arbitration related features are implemented in the *ArbitrationManager* class.
   When in server mode, the *ArbitrationManager* calls *RegisterArbitrationHandler* from the *NetworkFacade* to register an *ArbitrationClientMessageDelegate*, which is called to send a message to an arbitration client, and an *ArbitrationClientDisconnectDelegate*, which is called when an arbitration client disconnects or times out.
   When in client mode, the *ArbitrationManager* calls *GetArbitrator* from the *NetworkFacade* to receive a new *IArbitrator* object.
   It then calls *Connect* on the *IAbitrator* object in order to register an *ArbitrationConnectionDelegate*, which is called to return the result of an attempted connection with the Arbitration Server, an *ArbitrationConnectionFailureDelegate*, which is called when the Arbitration Client has been disconnected from the server, and an *ArbitrationServerMessageDelegate*, which is called on receiving a message from the Arbitration server.

   Please note that you will need to first run an instance of this demo in server mode before running another instance in client mode.
   The mode is chosen from a pop-up window that appears when the demo is executed.

-  **Scene Demo 8 - Distributed Controller** :ref:`[Pro] <cpp-pro>` employs the Distributed Controller feature (see :ref:`distributed-controller`) to implement a simple follower NPC that runs on user PCs.

   The NPC, which inherits from the *DistributedSceneController* class, is implemented in the *DistributedNPC* class. 
   A *CreateControllerDelegate* delegate is registered with the network facade before *Login* is called.


Running API Examples
~~~~~~~~~~~~~~~~~~~~

Pre-built Windows executables are available in the ``Examples\Binaries`` directory of the SDK.
Double click on those executables to launch and try the demonstrations.
You usually need to launch at least two instances of the same application (e.g. two instances of ``Scene-Demo1-Replication.exe``) to see how they interact with each other.

The API examples in the Badumna Pro SDK are configured to run in LAN mode by default (except Match Demo, which uses a seed peer).
This is the easiest way to setup a Badumna network for demonstration or testing purpose, as it does not require a Seed Peer and Internet connectivity.
LAN mode also supports multiple instances running across different PCs on the same LAN.
Please note that the pre-built executables will not work across the Internet due to the LAN mode restriction.
If you want to configure your application so that it can be tested across the Internet please refer to :ref:`setting-up-pro` or :ref:`setting-up-cloud`.

The API examples in the Badumna Cloud SDK all use the cloud, and require you to enter the id of your cloud application.
You can create one at https://cloud.badumna.com/.

Compiling API Examples
~~~~~~~~~~~~~~~~~~~~~~

You need to install and configure the Qt SDK to be able to build the API examples.
The Qt SDK is available from `qt-project.org <http://qt-project.org>`_.

Each example has a Visual Studio solution file located in its source directory.
Open this solution file in Visual Studio and build the solution as normal.

Please note that the project files depend on a Windows environment variable named **QT_MSVC_PATH** to figure out the location of the Qt header and libraries.
For example, if you are using Qt 4.7.3 installed at its default location and the compiler of your choice is Visual Studio 2008, then the above mentioned **QT_MSVC_PATH** environment variable should be defined to have the value C:\\QtSDK\\Desktop\\Qt\\4.7.3\\msvc2008.

.. warning::
   Remember to set the *QT\_MSVC\_PATH* environment variable if you want to build API Examples in Visual Studio,  otherwise the API examples will not compile properly.

To build with ``make`` (on either Windows or Linux), you may need to modify some variables (e.g QT_PATH, MONO_2_PATH) within the ``Makefile`` if the defaults are not correct for your system.

Runtime Dependencies
~~~~~~~~~~~~~~~~~~~~

The Mono library and Badumna assembly files (Badumna.dll and Dei.dll) should be placed in the same directory as the API example executables.
All Visual Studio project files as well as the Make-based build will copy these automatically as part of the build process.
See the ``Examples\Binaries`` directory for an example of the required layout.

.. tip::
   Don't forget to set the working directory correctly if you want use Visual Studio debugger with the API Examples.


Using Badumna C++
-----------------

The API of Badumna C++ is designed to closely match the API of the Badumna .NET assembly.
Full documentation of the C++ API is in the ``Documentation\API`` directory of the Badumna C++ SDK.
Open ``Documentation\API\index.html`` in your web browser to begin reading it.
The API documentation is also available online at :cppapi:`index.html`.

Initialize the Runtime
~~~~~~~~~~~~~~~~~~~~~~

The first thing you need to do in order to use the Badumna C++ library is to initialize the runtime.
This will initialize the Mono library runtime and load all required Badumna assemblies.
When the Badumna C++ library is built in debug mode it also checks that you are using compatible Badumna.dll and Dei.dll assemblies.

Both the runtime initialization and shutdown can be done by using the *BadumnaRuntimeInitializer* RAII class.
You may also choose to call *InitializeRuntime()* and *ShutdownRuntime()* manually instead.

Error Handling in Badumna C++
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

C++ exceptions are not used in Badumna C++. Instead, Badumna C++ has well defined error handling policies.
All possible errors are grouped into three categories and handled as follows:

-  **Invalid Operation Error:** This type of error is usually caused by an invalid operation in your application code. For example, trying to join a scene without calling *Login* first.

   Invalid Operation Error is not recoverable.
   The application code needs to be reviewed, fixed and rebuilt to meet the operational requirements of Badumna.

   How to register a customized Invalid Operation Error handler is explained in the system initialization part of the API documentation.
   The default handler will print out the details of the error and call *abort()*.

-  **Runtime Error:** Runtime error is a type of recoverable error. It is usually difficult to identify them by just reading the code. For example, in the streaming subsystem, the specified file path of the file that you want  send to the remote side must be a full path rather than just a file-name. A runtime error will be triggered if only the file-name is specified, without the path.

   For runtime errors, the involved method will return an error code to indicate the failure.
   It is up to the application to retry the failed operation with corrected parameters, or to terminate with the error code.

-  **Internal Logic Error:** This type of error is caused by bugs in Badumna C++ or the Badumna.dll assembly itself. It can also be caused by running Badumna C++ using an unsupported Badumna.dll assembly, e.g. Badumna.dll from a previous release.

   Internal Logic Errors are not recoverable.
   If you are using a valid combination of Badumna.dll and Badumna C++, then the only reason such an error can occur is because of a bug in Badumna.
   Please report such issues so that they can be fixed in our next maintenance release.

   How to register a customized Internal Logic Error Handler is explained in the system initialization part of the API documentation.
   The default handler will print out the details of the reported error and call *abort()*.

Object Ownership and Lifetime
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Object ownership is documented as part of the API documentation.
As a general guideline, unless otherwise stated, applications do not have ownership of Badumna generated objects.
This includes objects passed to the delegate callbacks as parameters and objects returned from Badumna.

It is the application’s responsibility to ensure that all objects registered with Badumna have sufficient life time.
For example, when using member methods as delegates, it is the application code’s responsibility to ensure that the object registered with the delegate is still valid when the delegate is invoked.

Multithreading
~~~~~~~~~~~~~~

Badumna C++ is not thread safe, as the underlying Badumna.dll assembly is not thread safe.
Applications should employ proper synchronization mechanisms when accessing Badumna C++ objects from multiple threads simultaneously.

When running in a multi-threading environment, each thread that needs to access Badumna C++ should be registered with the runtime and be unregistered before calling runtime shutdown.
The *ThreadGuard* class is a RAII class provided to do such registering and unregistering.

Packaging Your Game
-------------------

Windows and Linux
~~~~~~~~~~~~~~~~~

Other than the Badumna C++ itself, both the Mono library and Badumna assemblies (Badumna.dll and Dei.dll) need to be redistributed as a part of your Badumna C++ based game.

The relative location of where they should be placed depends on the path you specified when initializing the runtime.
By default, if you initialize the runtime using the default Mono path and Badumna.dll path, then the Mono library should be placed in a subdirectory named "mono" in the working directory.
Badumna.dll and Dei.dll assemblies should be placed next to the mono subdirectory.


License
-------

Badumna C++ is licensed under the license agreement included in the Badumna C++ SDK.

Badumna C++ uses the Mono library.
The shipped version is a cut down version of Mono 2.8.1 with all redundant files deleted.
All the included components are unmodified.
It is licensed under the LGPL license, which basically means that you can use it and redistribute it free of charge as long as you only dynamically link the library (mono-2.0.dll).
There is no requirement to have any part of your product to be open source.


Limitations
-----------

Badumna C++ comes with the following limitations:

-  Distributed Validation is not currently supported.

   Distributed Validation is a new concept introduced in Badumna 2.0 and its interface is still evolving.
   We plan to support Distributed Validation in Badumna C++ in a future release when the Distributed Validation interface is finalised.

-  Streaming in memory data is not supported.

   The current version of Badumna C++ only supports file streaming.
   This limitation of in-memory streaming can be worked around by writing the in memory data to a local file first and then sending that file using file streaming.

.. include:: ../apilink-directives.txt

-------------------------------------------------
 
.. _cpp-pro:

[Pro]: Feature only available in Badumna Pro.
