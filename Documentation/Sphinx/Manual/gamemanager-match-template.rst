.. _gamemanager-match-template-cloud:

GameManger template script - Badumna Cloud
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. literalinclude:: ../../../Source/Unity/Unity/Scripts/Generated/BadumnaCloud-Match/GameManager.cs
    :language: c#