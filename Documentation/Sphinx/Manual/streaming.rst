
:orphan:

.. include:: ../apilink-directives.txt

.. _streaming-protocol:

Streaming protocol
------------------

The Badumna streaming protocol provides a high performance yet easy to
use and reliable way of streaming content (data stream or file) between
Badumna peers. A peer can request to send content to a remote peer or it
can also request the remote side to start sending content. The following
two methods can be used to subscribe to requests:

#. |SubscribeToSendStreamRequests|

#. |SubscribeToReceiveStreamRequests|

An EventHandler delegate, which will be invoked on receiving incoming
requests, is specified when invoking the above methods. This handler is
described in more detail below.

A peer can call one of the following two methods to request to start
sending content to a specified remote peer. The data transmission is
asynchronous meaning that the begin send methods will return
immediately. The completion callback specified when invoking the send
methods will be called on completion.

#. |BeginSendReliableFile|

#. |BeginSendReliableStream|

A peer can also request the remote peer to start sending a file or other
content by calling one of the following two methods:

#. |BeginRequestReliableFile|

#. |BeginRequestReliableStream|

To learn more about the streaming protocol, you can refer to the
associated API documentation in the |Streaming| namespace. The
|StreamingManager| class provides all the API for streaming related
functions whereas the |IStreamController| interface controls the
streaming operation and provides information on its current status.

We will now take you through the steps that are required to support
streaming functionality in your application and demonstrate that using
sample code written in C#.

The first thing that a client has to do is to subscribe to Badumna’s
streaming service. This allows the client to receive files from other
clients. This step is typically done during application start up. To
subscribe to receive files you use the
|SubscribeToReceiveStreamRequests| method that is part of the
|StreamingManager| class as follows:

.. sourcecode:: c#

    public void Initialize()
    {
        // ...
    other initialization code ...

        this.networkFacade.Streaming.SubscribeToReceiveStreamRequests(
            "MyTag", this.HandleMyTagStreamEvent);

        // ...
    other initialization code ...
    }

This above method takes two arguments. The first argument is a string
used to identify the type of streaming request. Multiple calls may be
made to |SubscribeToReceiveStreamRequests| to associate different tags
with different handlers. When sending a file, the string passed to the
|BeginSendReliableStream| or |BeginSendReliableFile| method must
match one of the strings registered on the remote end. The second
argument is the handler that will get invoked when a request is received
from a remote client. In the example above we have used a handler called
HandleMyTagStreamEvent (defined below).

We will now explain how to send a file using the streaming interface.
You can send a file using the |BeginSendReliableFile| method as shown
below. We have defined a class called FileTransfer which will initiate a
transfer when it is constructed. It will also update details in a GUI as
the transfer progresses, via the |IStreamController| instance returned
from |BeginSendReliableFile|.

.. sourcecode:: c#

    public class FileTransfer
    {
        private IStreamController transferController;

        public FileTransfer(INetworkFacade networkFacade, string filename, BadumnaId destinationId, string username)
        {
            this.transferController = networkFacade.Streaming.BeginSendReliableFile("MyTag", filename, destinationId, username, this.Complete, null);
            this.transferController.InformationChanged +=
                this.UpdateProgress;    
        }
        
        private void UpdateProgress(object sender, EventArgs args)
        {
            // Update progress display on the GUI from data on
            // transferController.
        }

        private void Complete(IAsyncResult ar)
        {
            // Execute any actions required on completion such
            // as removing the progress display from the GUI.
        }
    }

The call to |BeginSendReliableFile| takes six arguments:

:string:          The unique string that should match what was used in the subscribe call at the destination client. 
:string:          The full path to the file to be sent.
:BadumnaId:       The :netapi:`BadumnaId 89D6264D` of an entity owned by the destination client.
:string:          The username of the client sending the file, this is not validated and is only an aid for the destination user.
:AsyncCallback:   The callback function that will be invoked when the streaming operation is complete.
:object:          Custom state passed to the callback.

The |BeginSendReliableFile| method will cause the
HandleMyTagStreamEvent method to be invoked on the destination client.
We now describe how to implement this method.

.. sourcecode:: c#

    public void HandleMyTagStreamEvent(object sender, StreamRequestEventArgs args)
    {
        // The StreamRequestEventArgs parameter provides access to
        // information about the stream, and methods to accept or
        // reject it.  For full details refer to the API
        // Documentation.

        // This is the name that we'll save the file to.  Here we're
        // using the original name of the file from the source client.
        string filename = args.StreamName;  

        // Make sure that the user is happy to accept the file and set
        // the value of 'agree' accordingly.
        bool agree = true;

        if {agree}
        {
            IStreamController transferController = args.AcceptFile(filename, this.Complete, null);
            transferController.InformationChanged +=
                this.TriggerPropertyChanged;
        }
        else
        {
            // The user does not want to receive the file
            args.Reject();
        }
    }

On receiving a stream request, the handler must call |AcceptFile| or
|AcceptStream| to accept the transfer, or call |Reject| to reject
the transfer. If the transfer is accepted then an |IStreamController|
is returned and can be used to monitor the transfer in the same way as
shown in the FileTransfer class above. In particular,
|IStreamController| has properties such as |BytesTotal| (total bytes
to be transferred), |BytesTransfered| (number of bytes transferred so
far), and |TransferRateKBps| (estimated transfer rate in KBps).


There are several other methods available in the streaming API but their
usage follows the pattern above. Refer to the |Badumna.Streaming|
section of the API documentation for full details.

.. tip::
   Please note that the API links used to explain custom messages and the streaming protocol refer to the .NET API.
   If you are using a different API please refer to the corresponding documentation for your SDK.

.. _centralised-services-concept:
