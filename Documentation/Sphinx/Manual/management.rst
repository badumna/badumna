.. include:: ../apilink-directives.txt

.. toctree::
   :hidden:
   
   Control Center <control-center>
   Seed Peer <seed-peer>
   Statistics Server <statistics-server>
   Overload Server <overload>
   HTTP Tunnel Server <tunnel>

.. _management:

Managing Your Game [Pro]
========================

.. include:: pro-tipbox.txt

In addition to Dei Server, Badumna's user authentication server, Badumna Pro also includes several other server applications to help you manage and support your game network.

Network discovery
-----------------

Run :ref:`Seed Peer <seed-peer>` to start a new Badumna network.

Track network activity
----------------------

:ref:`Statistics Server <statistics-server>` lets you track real-time online activity in your Badumna network.

Support users with limited bandwidth
------------------------------------

:ref:`Overload Server <overload>` will deliver messages on behalf of peers that have run out of bandwidth.

Support users behind restrictive firewalls
------------------------------------------

Some corporate firewalls block UDP traffic making many games inaccessible from corporate sites.
Badumna's :ref:`HTTP Tunnel Server <tunnel>` can be used as a proxy for peers behind such firewalls, allowing them to play your game too.

Manage your central services
----------------------------
 
To facilitate controlling the above central service, and also Dei Server and any arbitration servers for your game, Badumna Pro includes :ref:`control-center`, a web application for monitoring and controlling your central services.