
.. include:: ../apilink-directives.txt

.. _verified-identities:

Verified Identities [Pro]
~~~~~~~~~~~~~~~~~~~~~~~~~

.. include:: pro-tipbox.txt

Badumna networks can require all users to log in with a verified identity.
Verified identities are obtained from an instance of Dei Server, Badumna's authentication server.
More information about Dei Server is available in :ref:`dei`.

Using Dei Server
----------------

To help get started with Dei Server, the Demo/Binaries folder in the Badumna Pro download package includes a copy of DeiServer (DeiServer.exe) with a pre-built user database containing example user accounts (DeiAccounts.s3db).
Refer to the file DeiAccounts.s3db.readme in the same folder for information on the user accounts.

Dei Server can be started from the command line:

.. container:: commandline
   
   ``DeiServer.exe``

To learn more about using Dei Server see :ref:`dei`.
   
Remember that when using verified identities, all peers in the network must use a verified identity, including the SeedPeer, arbitration servers etc.
See :ref:`using-dei-in-badumna-clients` for information on configuring peers to use Dei.

Using verified identities
-------------------------

- Create a secure session with the address and port you are running Dei Server on, then authenticate the user's account:

    .. sourcecode:: c#
      
      var session = new Dei.Session(dei.example.com, 21247);
      LoginResult result = session.Authenticate(username, password);
   
- Select a character to join the network as:
   
   The authentication result will include a list of characters in the ``LoginResult.Characters`` property.
   Select the one you wish to log in as.

   .. sourcecode:: c#
   
      IIdentityProvider identity;
      session.SelectIdentity(character, out identity);
   
   When a valid character is passed to the call to |Dei.Session.SelectIdentity|, the session will populate (via an out ref) an IIdentityProvider with digitally signed credentials authorising login to the Badumna network.

- Creating characters
   
   The Dei session will let you create and delete characters associated with your account:
   
   .. sourcecode:: c#
      
      // Create a character with the name "UberPwnr";
      session.CreateCharacter("UberPwnr");
      // Delete a character
      session.DeleteCharacter(oldCharacter);
   
   
   By default, Dei Server only enforces that character names be unique.
   Developers may implement additional rules (such as limiting the number of characters a user can have) by implementing a custom |IAccountManager| for the Dei server (see :ref:`dei`).

- Log in to the Badumna network using the secure identity.

   .. sourcecode:: c#

      networkFacade.Login(identity);
   
   Any peer using a secure identity to log in to Badumna will only connect to other peers also using a secure identity.
   The character selected for a secure identity will be used for features such as private chat.

.. container:: demoCode
   
   BadumnaDemo's VerifiedIdentityModule.cs shows code for obtaining a secure identity.

.. tip::
 
   In order to test Verified Identities feature, you will have to start an instance of Dei Server. 
   A ready to use version of Dei Server is included in the Demo/Binaries folder (DeiServer.exe).
   A number of default accounts have also been created to use with this Dei Server (refer to DieAccounts.s3db.readme in Demo/Binaries folder for details). 
   If you are using a Seed Peer as part of the setup, you will have to start the Seed Peer using DeiConfig (refer to :ref:`configuring-a-seed-peer-to-use-dei`).


