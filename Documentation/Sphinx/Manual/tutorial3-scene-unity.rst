
.. _unity-scene-tutorial3:

Unity tutorial - Proximity Chat
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. admonition:: Pre built package available (BadumnaCloud)

   The instructions below will guide you in creating a multiplayer game using Badumna.
   If you would prefer to see the result immediately, the SDK contains a completed package:
   
   ``Unity Packages\Scene-Demo3-Chat.unitypackage``
   
   You only need to specify your cloud application identifier and select the correct assembly in the completed demo.

.. admonition:: Pre-built package available (Badumna Pro)

   The instructions below will guide you in creating a multiplayer game using Badumna from scratch.
   If you would prefer to see the result immediately, the SDK contains a completed package:
   
   ``Demo\Scene-Demo3-Chat(Pro).unitypackage``.
   
   You only need to configure Badumna network and select the correct assembly in the completed demo.

This tutorial builds on the simple game from tutorial 2.
We'll add proximity chat, where each player will receive chat messages sent by other players near them in the game.


.. include:: tutorial-requirements-unity.txt

**1. Open the project created in tutorial 2.**

Alternatively, you can use the completed tutorial 2 solution included in the BadumnaCloud or Badumna Pro Unity SDK.

**2. Edit the GameManager.cs:**

We're going to add a login script that will prompt users for a character name before joining Badumna.
Add fields for the login screen and proximity chat objects to the GameManager class:

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Scene-Demo3-Chat/GameManager.cs.Demo_Fields.rst

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Scene-Demo3-Chat/GameManager.cs.Demo_Scene-3_Fields.rst

Edit the Initialize method to adds the new scripts:

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Scene-Demo3-Chat/GameManager.cs.Initialize_method_(Chat).rst

If we shutdown and restart Badumna, we'll need the user to log in again, so edit the Shutdown method to reset these scripts:

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Scene-Demo3-Chat/GameManager.cs.Shutdown_method.rst

**3. Edit the Network.cs:**

The start method must get the player name from the login script. Copy the highlighted code below into your Network script.

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Scene-Demo3-Chat/Network.cs.Start_method_(Chat).rst
       
**4. Edit the Scene.cs:** 

Next we'll add a line of code to subscribe to and unsubscribe from proximity chat when entering and leaving a scene respectively:

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Scene-Demo3-Chat/Scene.cs.JoinScene_method_(Chat).rst
        
.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Scene-Demo3-Chat/Scene.cs.LeaveScene_method.rst

**4. Edit the Chat.cs:**

The BadumnaCloud package includes a :ref:`Chat template script<proximitychat-template>`, which already has code for subscribing and unsubscribing the local player to/from chat.
We need to fill in the code for sending messages, and displaying received messages.
The bulk of the code we'll add will be for the GUI window where users can enter messages and see received messages.

The new fields required are as follows:

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Scene-Demo3-Chat/Chat.cs.Demo_3_Fields.rst

Edit the SendMessage method to send a demo message:

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Scene-Demo3-Chat/Chat.cs.SendMessage_method.rst

The HandleChatMessage method needs to store the received message and scroll the chat window down to show it:

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Scene-Demo3-Chat/Chat.cs.HandleMessage_method.rst
 
The remaining edits all relate to the chat window. We'll calculate its size in the Start method:
 
.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Scene-Demo3-Chat/Chat.cs.Start_method.rst
 
Then we'll add methods for creating the window, and responding to user input:

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Scene-Demo3-Chat/Chat.cs.OnGUI_method.rst
         
**5. Build and run the game**

.. include:: build-and-run-unity.txt

Build and run a couple of instances of the game to try out proximity chat.

.. admonition:: Badumna Pro only !!!

  .. include:: build-and-run-unity-badumna-pro.txt