
.. _unity-match-tutorial3:

Unity tutorial - Match Entity Replication
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. admonition:: Pre built package available (BadumnaCloud)

   This tutorial shows how to add Badumna match entity replication step by step.
   If you would prefer to see the result immediately, the SDK contains a completed package:
   
   ``Unity Packages\Match-Demo3-Replication.unitypackage``
   
   You only need to specify your cloud application identifier and select the correct assembly in the completed demo.

.. admonition:: Pre built package available (Badumna Pro)

   This tutorial shows you how to add Badumna match functionality from scratch.
   If you would prefer to see the result immediately, the SDK contains a completed package:
   
   ``Demo\Match-Demo3-Replication(Pro).unitypackage``
   
   You only need to configure Badumna network and select the correct assembly in the completed demo.

In this Unity tutorial you will learn how to implement basic replication in your game.
This feature is useful to synchronise object properties such as an avatar's position, colour or a car's velocity, orientation, rotation, etc.
In this demo we will demonstrate how to synchronise a player's position, orientation, and animation.
This tutorial builds on the previous tutorial - :ref:`Unity tutorial - Match controller<unity-match-tutorial1>`.

.. include:: tutorial-requirements-unity.txt

.. spelling::
    animationController
    animationName

**1. Open the project from the previous tutorial**

You can either open the project that you completed following the tutorial :ref:`Unity tutorial - Match controller<unity-match-tutorial2>` or
you can open the completed tutorial solution included in the BadumnaCloud or Badumna Pro Unity SDK.

**2. Edit GameManager.cs:**

We'll just add a few public fields to the GameManager class that will allow us to specify a prefab for the player and an on-screen joystick (for mobile platforms).
Copy the following code to the GameManager.cs where you find the comment "Add any custom fields here...":

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Match-Demo3-Replication/GameManager.cs.Demo_Replication_Fields.rst

**3. Edit Matchmaking.cs:**

Add the following constant and field to the Matchmaking class.

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Match-Demo3-Replication/Matchmaking.cs.Demo_Match-2_Constant.rst

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Match-Demo3-Replication/Matchmaking.cs.Demo_Match-2_Fields.rst

Replace the body of the ``CreateLocalPlayer`` method with the following highlighted code to create a playable Lerpz character:
    
.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Match-Demo3-Replication/Matchmaking.cs.CreateLocalPlayer_method.rst

Add the following highlighted code to the replica creation and removal delegates for creating Lerpz characters for other players:
    
.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Match-Demo3-Replication/Matchmaking.cs.Replica_delegates.rst

Add two new methods to register and unregister the local player from the match.

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Match-Demo3-Replication/Matchmaking.cs.LocalPlayer_registration.rst

Call ``RegisterLocalPlayer`` method from ``HandleMatchStart`` and ``UnregisterLocalPlayer`` from ``HandleMatchEnd`` (see highlighted lines).

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Match-Demo3-Replication/Matchmaking.cs.HandleMatchStart_method_(Replication).rst

and

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Match-Demo3-Replication/Matchmaking.cs.HandleMatchEnd_method_(Replication).rst

**4. Edit Player.cs:**

The :ref:`Player template script <player-match-template>` holds standard player properties that are automatically replicated by Badumna. 
The script already includes ``Position`` and ``Orientation`` properties.

We want to replicate animation so we'll add an animationController. This code can be added anywhere in the Player class.

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Match-Demo3-Replication/Player.cs.Demo_1_Fields.rst

Add the Match field to the ``Player`` class.

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Match-Demo3-Replication/Player.cs.Match_field.rst

We'll add properties for ``CharacterName`` and ``AnimationName``.
Since ``AnimationName`` needs to be replicated, we'll add the ``Replicable`` attribute to it.
The ``AnimationName`` property wraps the ``animationName`` field of our ``animationController``, so the controller's current animation will always be replicated.

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Match-Demo3-Replication/Player.cs.AnimationName_Property.rst

The ``CharacterName`` name is stored on the ``Player`` itself and is not replicated.

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Match-Demo3-Replication/Player.cs.CharacterName_Property.rst

We'll need an ``Awake`` method that grabs an instance of ``ThirdPersonSimpleAnimation`` from the game object where the ``Player`` script is attached:

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Match-Demo3-Replication/Player.cs.Awake_method.rst

**5. Configure the GameManager**

Finally, we need to specify which assets we're using for the various game objects such as the player avatar prefab.
We can do this in the Unity editor by configuring the fields we've added to the GameManager with assets that were included in the demo assets package:

- Select the GameManager object in the Hierarchy window, so that its configurable fields are viewable in the Inspector window.
- Drag the Lerpz and Joystick prefabs from the Prefabs folder in the Project window to the Player Prefab and Joystick Prefab fields in the Inspector window respectively.
        
**6. Build and run the game**

.. include:: build-and-run-unity.txt

Build and run a couple of instances of the game to see replication in action.

.. admonition:: Badumna Pro only !!!

  .. include:: build-and-run-unity-match-badumna-pro.txt