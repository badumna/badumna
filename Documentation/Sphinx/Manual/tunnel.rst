.. _tunnel:

HTTP Tunnelling Service [Pro]
-----------------------------

.. include:: pro-tipbox.txt

Introduction
~~~~~~~~~~~~

The HTTP Tunnelling Service allows clients to connect to the
peer-to-peer network using a client-server HTTP connection. This enables
users behind restrictive firewalls to connect to the network. It can
also support clients on bandwidth-constrained devices. The client only
makes a connection to the tunnelling service and the tunnelling service
is responsible for sending the appropriate messages on the peer-to-peer
network. The machine which runs the tunnel server must have sufficient
network, CPU, and memory resources to support the connected clients.

Server Configuration
~~~~~~~~~~~~~~~~~~~~

The tunnel server is a process that can be run standalone or under the
control center. The tunnel server requires either Microsoft .NET
Framework v2.0 or later running on Microsoft Windows XP SP2 or later; or
Mono on any platform. Because the tunnel server acts as a proxy client,
it requires the same Badumna configuration to be specified as a normal
client.
The configuration is specified in the same way as for starting a seed peer (see :ref:`section:seed-peer`).
For example, the configuration could be stored in a file called “BadumnaOptions.xml”, and then loaded using the ``--badumna-config-file`` parameter as shown below.
The only other configuration information required for the tunnel server
is the “URL prefix” to listen on. The prefix is specified on the command line when in standalone mode, or in the tunnel
server’s configuration page in the control center. Some example prefixes
and their meanings are listed in :ref:`tab:HTTPPrefixExamples`. See
`UrlPrefix Strings <http://msdn.microsoft.com/en-us/library/aa364698.aspx>`_
on Microsoft’s site for a full description of URL prefix strings. For
example, to start the tunnel listening for requests on all interfaces on
port 8080 and load the Badumna configuration from BadumnaOptions.xml:

.. container:: commandline
   
   ``Tunnel.exe --badumna-config-file=BadumnaOptions.xml http://+:8080/``

Additional configuration may be required depending on the prefix used,
see below for details.

.. _tab:HTTPPrefixExamples:

.. table:: HTTP Prefix Examples

   =======================   ================================================================================================
   Prefix                    Meaning
   -----------------------   ------------------------------------------------------------------------------------------------
   http://+:8080/            Listen for requests on all interfaces on port 8080.  
   https://+:8080/           Listen for requests on all interfaces on port 8080 using SSL (a certificate must be configured). 
   http://localhost:8081/    Listen for requests to localhost on port 8081.
   =======================   ================================================================================================

Permissions
^^^^^^^^^^^

When running the tunnel server on Microsoft Windows using the Microsoft
.NET Framework, the user running the tunnel server must have permissions
to listen on the specified URL prefix. Also, to run tunnel server on
Windows machine you need to manually open the tunnel server port in
Windows firewall. This requirement does not apply when using Mono
because Mono does not use Microsoft’s HTTP Server API. Note that when
running the tunnel server on a Unix-based platform it may require root
privileges if configured to listen on a port below 1024.

On Windows Vista and later the netsh.exe program can be used to grant
permissions to listen on a given prefix. For Windows XP,
httpcfg.exe [2]_ must be used. To grant permission for “Everyone” to
listen on “http://+:8080/” use one of the following commands:

:Vista and later: netsh http add urlacl url=http://+:8080/ user=Everyone
:XP: httpcfg set urlacl /u http://+:8080/ /a "D:(A;;GX;;;WD)"

See `Configuring Namespace Reservations <http://msdn.microsoft.com/en-us/library/ms733768.aspx>`_
on Microsoft’s website for further details.

Client Configuration
~~~~~~~~~~~~~~~~~~~~

The client configuration consists of setting the tunnel mode and
specifying a list of URIs of tunnel servers. The configuration can be
specified either programmatically or in an XML configuration file. To
configure tunnelling programmatically, use code like:

.. sourcecode:: c#

    Options options = new Options();
    options.Connectivity.TunnelMode = TunnelMode.On;
    options.Connectivity.TunnelUris.Add("http://tunnel1.example.com:8085/");
    options.Connectivity.TunnelUris.Add("http://tunnel2.example.com:8085/");

The XML configuration takes the following form:

.. sourcecode:: xml

    <Module Name="Connectivity" Enabled="true">
      <Tunnel Mode="on">
        <Uri>http://tunnel1.example.com:8085/</Uri>
        <Uri>http://tunnel2.example.com:8085/</Uri>
      </Tunnel>
    </Module>

The tunnel mode can be set as described in :ref:`tab:TunnelModes`. By
default the tunnel mode is set to Auto. Note that if the tunnel mode is
set to On and no URIs are specified then the client will not be able to
connect at all. At least one tunnel URI must be specified for tunnelling
to be used. If multiple tunnel URIs are specified then Badumna will
randomly select one to connect to. If a connection cannot be formed then
the remaining tunnels will be tried in a random order until a connection
is successful or the list is exhausted.

Note that when in Auto mode Badumna will try all ports in the range it
is configured to use before falling back to tunnelled mode. This occurs
synchronously when |NetworkFacade.Create(Options)| is called. If a
large port range is specified it could take a long time so it is
recommended that the port range be restricted by the “MaxPortsToTry”
attribute when using Auto mode. e.g.:

.. sourcecode:: c#

    options.Connectivity.MaxPortsToTry = 3;

or:

.. sourcecode:: xml

    <Module Name="Connectivity">
        <PortRange MaxPortsToTry="3">21300,21399</PortRange>
    </Module>

Also note that when tunnelling is in use (i.e. tunnel mode is On or
tunnel mode is Auto and UDP is detected as blocked), a connection to the
tunnel server is not initiated until the Login(...) method is called.
This is a safeguard – the initial connection to the tunnel server must
send the user’s credential so that the tunnel server can validate the
user’s permission to use the network.

.. _tab:TunnelModes:

.. table:: Tunnel Modes

   =========    ===========================================================================================================================================
   Mode         Description
   ---------    -------------------------------------------------------------------------------------------------------------------------------------------
   Off          Tunnelling will not be used.  If a direct UDP connection cannot be established then the client will not be able to connect to the network.
   On           Tunnelling will always be used.  If a tunnel server cannot be contacted then the client will not be able to connect to the network.
   Auto         A direct UDP connection will be attempted.  If UDP traffic is blocked then a tunnelled connection will be attempted.
   =========    ===========================================================================================================================================


Programming Considerations
~~~~~~~~~~~~~~~~~~~~~~~~~~

When Badumna is using a tunnelled connection most API calls require a
message to be sent to the tunnel server. These calls all make
synchronous (blocking) requests to the tunnel server. If a tunnel
request fails then the API method will throw a TunnelRequestException
(this can occur if, e.g., the network connection to the tunnel server is
lost). This behaviour should be taken into account when deciding to
support tunnelled connections. As an explicit exception, the
NetworkFacade.FlagForUpdate(...) methods do not make a request to the
tunnel server and do not block.

Another restriction when using a tunnelled connection is that certain
API methods are not supported. These will throw NotSupportedExceptions
if called when a tunnel connection is in use (the API documentation for
each method indicates if it’s not supported under tunnelling).
NetworkFacade.IsTunnelled can be used to determine if the connection is
tunnelled. The unsupported methods are limited to methods intended to be
used for ‘server’-type behaviour such as the arbitration server. All
methods required by end-user clients are supported over tunnelled
connections.

.. [1]
   If running under mono, launch them from the command line, using the
   commands in the batch scripts preceded by *mono* or *mono-service*

.. [2]
   httpcfg.exe is part of the Optional Tools component of the `Windows
   XP SP2 Support
   Tools <http://go.microsoft.com/fwlink/?LinkID=84085>`_. Choose the
   custom installation and make sure the Optional Tools is selected.

.. include:: ../apilink-directives.txt
