.. _network-match-template-pro:

Network template script - Badumna Pro
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. literalinclude:: ../../../Source/Unity/Unity/Scripts/Generated/BadumnaPro-Match/Network.cs
    :language: c#