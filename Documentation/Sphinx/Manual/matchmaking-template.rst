.. _matchmaking-match-template:

Matchmaking template script
~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. literalinclude:: ../../../Source/Unity/Unity/Scripts/Generated/BadumnaCloud-Match/Matchmaking.cs
    :language: c#