.. _chapter-optimise:

.. spelling::

   keyPairXml

Optimising a Badumna powered application
=========================================

This section will discuss a few tips on optimising your game to improve efficiency and performance.

.. _load-time:

Reducing load time
------------------
You may find that your application's load time has increased as a result of integrating with Badumna.
This may especially be true if you are developing mobile apps.
The load time increases as a result of Badumna's Login API call.
During Login, Badumna generates a private/public key pair.
This key pair is used by Badumna to encrypt and secure all the messages that are sent from the peer.
It is the generation of this key pair that can be slow on certain mobile devices.
It is important that the key pair is generated on the device and not on a server as the private key can never be transmitted over the network for security reasons.

In order to reduce the load time, you can generate the key pair once (the first time the app is installed/started) and reuse it after that.
Badumna provides a mechanism to achieve this.
Before you call Badumna's Login(IIdentityProvider) method, you must generate a key pair by using the following API method:

|UnverifiedIdentityProvider.GenerateKeyPair|

This static method is used for generating a new RSA key pair and returns the key as an XML string format.
Store this keyPairXml string locally and reuse it to optimize the load time performance.
Please refer to the operating systems's developer reference manual for more information on how to save data on the device securely.

You can also generate a new RSA key pair using the following API:

|Dei.Session.GenerateKeyPair|

This API has exactly same functionality as the |UnverifiedIdentityProvider.GenerateKeyPair|, this was added for your convenience when using Dei.

Pass this generated key pair string when creating an instance of |UnverifiedIdentityProvider(string, string)| or pass it to a |Dei.Session(String,UInt16,String,LoginProgressNotification)| constructor as an additional argument when you connect to Dei and use it for logging in to Badumna. 
If you provide UnverifiedIdentityProvider or Dei.Session with keyPairXml string, Badumna will then use the provided key pair and not generate a new one.
This will reduce the load time of your application significantly.

.. _network-recovery:

Recovering from a network failure
---------------------------------
There are occasions when your application may lose network connectivity for a brief period.
This is especially relevant for mobile apps (user could be driving a car in a remote location or switch from 3G to Wifi or vice versa).
In these situations it is important that your application recovers automatically as soon as network connection is restored.
There are two possible scenarios possible and you need to cater to them so that your application is optimised.

#. The application loses network connection for a brief period and when it recovers it has the same IP address and port number.
   This can happen if a user minimises the app on a mobile phone (especially iOS) and then opens it again.
   In this scenario you do not need to restart Badumna.
   All you need to do is register the entity in the required scene and Badumna will do the rest.
   You will need to subscribe to the AddressChange event in order to ensure that the IP address hasn't changed (refer to the relevant operating system's reference manual for more details).

#. The application recovers from network failure but has a different IP address or port number.
   This can happen when a mobile device switches its network from 3G to Wifi or vice versa.
   In this case, you need to shutdown and restart Badumna for the application to work properly.
   Please refer to the article `Recovery After Network Failure <http://www.scalify.com/documentation/Articles/RoamingRecovery/network-roaming-recovery.html>`_ for more information on the different steps required.
   This article demonstrates how to shutdown and restart Badumna when such a network change event occurs with an example.


.. include:: ../apilink-directives.txt
