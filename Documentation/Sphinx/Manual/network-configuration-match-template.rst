.. _network-configuration-match-template:

NetworkConfiguration template script
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. literalinclude:: ../../../Source/Unity/Unity/Scripts/Generated/BadumnaPro-Match/NetworkConfiguration.cs
    :language: c#