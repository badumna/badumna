.. include:: ../apilink-directives.txt

.. _hosted-entities:

Badumna Matches: Hosted entities
================================

Match entities created on a particular match member will only remain in the game while that member is still in the match.
Sometimes you want an entity to remain in the match no matter who joins or leaves, e.g. an entity for an NPC.
You can achieve this by using a hosted entity.
Hosted entities run on whichever peer is the current match host, migrating to the next host when the current one leaves.

Defining hosted entities
------------------------

Hosted entities are defined in exactly the same way as match entities, using a class with replicable properties and remotely callable methods marked with the |Replicable| attribute.

.. sourcecode:: c#

    public class NPC
    {
        [Replicable]
        public Vector3 Position { get; set; }
        
        [Replicable]
        public void DoSomething(int duration)
        {
            // ...
        }
        
        // ...
    }
	
Hosted entities support :ref:`custom replicable types and RPC signatures<replicable-types>` and :ref:`smoothing` in the same way as regular match entities.
	
Registering hosted entities
---------------------------

Like match entities, hosted entities need to be registered with a match before they will be replicated.
Only the current host can register a hosted entity.
Use an unsigned integer ID to identify the type of the hosted entity. 
This ID will be passed to the ``CreateReplica`` delegate that you supplied on match creation or joining, and used by your application to determine the type of the replica that needs to be created.

.. sourcecode:: c#

    var hostedEntity = new NPC();
    this.match.RegisterHostedEntity(hostedEntity, entityType);
    this.hostedEntities.Add(hostedEntity);

When a hosted entity needs to be removed, the host unregisters it from the match by calling |Match.UnregisterHostedEntity|:
    
.. sourcecode:: c#

    this.match.UnregisterHostedEntity(hostedEntity);

Replica creation and removal
----------------------------

Badumna creates and removes replicas of hosted entities by invoking the ``CreateReplica`` delegate you provided when creating or joining the match.
It will pass |EntitySource.Host| as the source of the replica to distinguish it from a match entity.
Your delegate should create and return an instance of the appropriate class.
You will also need to store a reference to the replica so it can be updated if the current peer becomes the match host.

.. sourcecode:: c#

    private IReplicableEntity CreateReplica(Match match, EntitySource source, uint entitytype)
    {
        if (source == EntitySource.Host)
        {
            var replica = new NPC();
            this.npcs.Add(replica);
            return replica;
        }
        else
        {
            var replica = new Avatar();
            this.replicas.Add(replica);
            return replica;
        }       
    }

When a hosted entity is removed from the match, Badumna notifies your application by invoking the ``RemoveReplica`` delegate you provided when creating or joining match.
Any action to be taken here is entirely application-dependent.
    
.. sourcecode:: c#

    private void RemoveReplica(Match match, EntitySource source, object replica)
    {
        this.replicas.Remove((Avatar)replica);
    }

Updating hosted entities
------------------------

The current host is responsible for updating all hosted entities.
When the host sets a hosted entity's replicable property, the change will be replicated to all other members.
Clients should not set hosted entity's replicable properties as the changes will not be replicated.
The host can send RPCs to all replicas of a hosted entity by calling |Match.CallMethodOnReplicas|.

.. sourcecode:: c#

    if (this.match.State == MatchStatus.Hosting)
    {
        this.match.CallMethodOnReplicas(this.hostedEntities[0].DoSomthing, 3);
    }
    
Clients can send RPCs to the original hosted entity on the host by calling |Match.CallMethodOnOriginal|.

.. sourcecode:: c#

    if (this.match.State != MatchStatus.Hosting)
    {
        this.match.CallMethodOnOriginal(this.hostedEntities[0].DoSomthing, 3);
    }

Hosted Entity Migration
-----------------------

When the host leaves a match, another member is automatically assigned to be the new host.
When a member becomes host, it will automatically take over the replication of hosted entities.
Your game should update any hosted entities when it is hosting.

Hosted entities should only be registered with Badumna once.
A new host should not re-register hosted entities that have already been replicated to it.
	
Try it out
----------

See hosted entities in action:

- :ref:`Unity Hosted Entities Tutorial<unity-match-tutorial6>`