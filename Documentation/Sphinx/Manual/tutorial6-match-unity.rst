
.. _unity-match-tutorial6:

Unity tutorial - Match Hosted Entities
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. admonition:: Pre built package available (BadumnaCloud)

   This tutorial shows how to add hosted entities step by step.
   If you would prefer to see the result immediately, the SDK contains a completed package:
   
   ``Unity Packages\Match-Demo6-HostedEntity.unitypackage``
   
   You only need to specify your cloud application identifier and select the correct assembly in the completed demo.

.. admonition:: Pre built package available (Badumna Pro)

   This tutorial shows you how to add Badumna match functionality from scratch.
   If you would prefer to see the result immediately, the SDK contains a completed package:
   
   ``Demo\Match-Demo6-HostedEntity(Pro).unitypackage``
   
   You only need to configure Badumna network and select the correct assembly in the completed demo.

This tutorial builds on the previous tutorial 5. 
We will add hosted entities into the previous demo.

.. include:: tutorial-requirements-unity.txt

.. spelling::
    NPC
    
**1. Open the project created in tutorial 5.**

You can either open the project that you completed following the tutorial :ref:`Unity tutorial - Match Chat<unity-match-tutorial5>` or
you can open the completed tutorial solution included in the BadumnaCloud or Badumna Pro Unity SDK.


**2. Edit GameManager.cs:**

We are going to add "ObjectPrefab" field to the GameManager class that will be used for storing the prefab for a hosted entity, we will use a simple spherical object.

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Match-Demo6-HostedEntity/GameManager.cs.Demo_Match-5_Fields.rst

**3. Attach ObjectPrefab into GameManager:**

Select the GameManager object in the Hierarchy window and drag the ObjectPrefab from the Prefabs folder to the Project window to the ObjectPrefab field in the GameManager Inspector window.

**4. NPC.cs:**

The BadumnaCloud package includes an :ref:`NPC template script<npc-match-template>`, which has exactly the same properties as the :ref:`Player template script <player-match-template>`.
We do not need to edit NPC.cs.

**5. Edit Matchmaking.cs:**

Add the following constant and field to the ``Matchmaking`` class.

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Match-Demo6-HostedEntity/Matchmaking.cs.Demo_Match-5_Constant.rst

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Match-Demo6-HostedEntity/Matchmaking.cs.Demo_Match-5_Fields.rst

Add the following highlighted code to the replica creation and removal delegates:

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Match-Demo6-HostedEntity/Matchmaking.cs.Replica_delegates_(HostedEntity).rst

Add two new methods to register and unregister the hosted entity from the match.

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Match-Demo6-HostedEntity/Matchmaking.cs.Hosted_entity_registration.rst

Call ``RegisterHostedEntity`` from ``HandleMatchStart`` and ``UnregisterHostedEntity`` from ``HandleMatchEnd`` (see highlighted lines):

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Match-Demo6-HostedEntity/Matchmaking.cs.HandleMatchStart_method_(HostedEntity).rst

and,

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Match-Demo6-HostedEntity/Matchmaking.cs.HandleMatchEnd_method_(HostedEntity).rst

**6. Build and run the game**

.. include:: build-and-run-unity.txt

Build and run a couple of instances of the game to see hosted entities in action.

.. admonition:: Badumna Pro only !!!

  .. include:: build-and-run-unity-match-badumna-pro.txt