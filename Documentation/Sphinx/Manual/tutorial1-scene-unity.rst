
.. _unity-scene-tutorial1:

Unity tutorial - Basic replication
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


.. only:: web

  Watch the video or follow the text tutorial below.

  .. raw:: html

      <iframe class="video" width="640" height="360" src="//www.youtube.com/embed/0NPmIDhuRNU?rel=0" frameborder="0" allowfullscreen></iframe>

.. admonition:: Pre-built package available (BadumnaCloud)

 The instructions below will guide you in creating a multiplayer game using Badumna from scratch.
 If you would prefer to see the result immediately, the SDK contains a completed package:
 
 ``Unity Packages\Scene-Demo1-Replication.unitypackage``.
 
 You only need to specify your cloud application identifier and select the correct assembly in the completed demo.

.. admonition:: Pre-built package available (Badumna Pro)

   The instructions below will guide you in creating a multiplayer game using Badumna from scratch.
   If you would prefer to see the result immediately, the SDK contains a completed package:
   
   ``Demo\Scene-Demo1-Replication(Pro).unitypackage``.
   
   You only need to configure Badumna network and select the correct assembly in the completed demo.

.. include:: tutorial-requirements-unity.txt

.. spelling::
    animationController
    animationName

**1. Create a new project**

- Open Unity
- Create a new project.
- Import the BadumnaCloud-Scene.unity package (located in the BadumnaCloud SDK's Unity Packages folder) or BadumnaPro-Scene.unity package (located in the Badumna Pro SDK's Unity Packages folder).
  
  This package includes the Badumna library and a set of script :ref:`templates <unity-templates>` that can be edited and used in your game.

- Import the DemoAssets Unity package (located in the SDK's Unity Packages folder).
    
  This package includes the prefabs and other assets that we will use to build the game.
  All the assets in this package have been sourced freely from the Unity asset store.

- Open the demo scene (located in the project's Scene folder).
  
  The demo scene contains ground, invisible fences (to stop players falling off), light, and a camera.

**2. Add a GameManager object**

- Create a new game object to act as the game manager:

  :menuselection:`Game Object Menu --> Create Empty`
  
- Rename the object 'GameManager'.

- Add the GameManager.cs template script to the GameManager object:
  
  In the Project window, expand the Script and Network folders.
  Select the GameManager script and drag it into the Inspector window of GameManager object.

**3. Edit the GameManager.cs:**

The GameManager.cs takes care of loading the Network.cs script, and exposing some of its core functionality.
We'll just add a few public fields to the GameManager class that will allow us to specify a prefab for the player and an on-screen joystick (for mobile platforms).
Copy the following code to the GameManager.cs where you find the comment "Add any custom fields here...":

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Scene-Demo1-Replication/GameManager.cs.Demo_Replication_Fields.rst

**4. Edit the Player.cs:**

The :ref:`Player template script <player-template>` holds standard player properties that are automatically replicated by Badumna. 
The script already includes ``Position`` and ``Orientation`` properties.

We want to replicate animations, so we'll add an animationController. This code can be added anywhere in the Player class.

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Scene-Demo1-Replication/Player.cs.Demo_1_Fields.rst

Also add ``Scene`` field into the Player class.

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Scene-Demo1-Replication/Player.cs.Scene_field.rst

We'll add properties for ``CharacterName`` and ``AnimationName``.
Since ``AnimationName`` needs to be replicated, we'll add the ``Replicable`` attribute to it.
The ``AnimationName`` property wraps the ``animationName`` field of our ``animationController``, so the controller's current animation will always be replicated.

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Scene-Demo1-Replication/Player.cs.AnimationName_Property.rst

The ``CharacterName`` name is stored on the ``Player`` itself and is not replicated.

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Scene-Demo1-Replication/Player.cs.CharacterName_Property.rst

We'll need an ``Awake`` method which will be used to grab an instance of ``ThirdPersonSimpleAnimation`` from the game object the ``Player`` script is attached to:

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Scene-Demo1-Replication/Player.cs.Awake_method.rst

**5. Edit the Network.cs:**

Copy the following field into the Network.cs.

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Scene-Demo1-Replication/Network.cs.Demo_Scene-1_Field.rst

Added the highlighted lines into the ``Start`` method in Network class:

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Scene-Demo1-Replication/Network.cs.Start_method_(Replication).rst

Also, add the highlighted line below into the ``OnDisable`` method:

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Scene-Demo1-Replication/Network.cs.OnDisable_method.rst

**6. Edit the Scene.cs:**

Replace the body of the ``CreateLocalPlayer`` method with the following highlighted code to create a playable Lerpz character:
    
.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Scene-Demo1-Replication/Scene.cs.CreateLocalPlayer_method.rst

Add the following highlighted code to the replica creation and removal delegates for creating Lerpz characters for other players:
    
.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Scene-Demo1-Replication/Scene.cs.Replica_delegates.rst

Finally, add the highlighted line into ``JoinScene`` method

.. include:: ../../../Source/Unity/Unity/Scripts/Generated-Docs/Scene-Demo1-Replication/Scene.cs.JoinScene_method_(Replication).rst

**7. Configure the GameManager**

Finally, we need to specify which assets we're using for the various game objects such as the player avatar prefab and parts of the user interface.
We can do this in the Unity editor by configuring the fields we've added to the GameManager with assets that were included in the demo assets package:

- Select the GameManager object in the Hierarchy window, so that its configurable fields are viewable in the Inspector window.
- Drag the Lerpz and Joystick prefabs from the Prefabs folder in the Project window to the Player Prefab and Joystick Prefab fields in the Inspector window respectively.
- Drag the mobile asset from the GUISkin folder in the Project window to the IOSSkin field in the Inspector window.

.. admonition:: BadumnaCloud only !!!
 
  - Set the Application Identifier to the ID for the application you created on `cloud.badumna.com <cloud.badumna.com>`_ (see :ref:`chapter:get-started`).

.. admonition:: Badumna Pro only !!! 
  
  - Drag the NetworkConfiguration.cs from the Script folder in the Project window to the GameManager Inspector window.
  - Configure the badumna options according to your needs.
        
**8. Build and run the game**

.. include:: build-and-run-unity.txt

Build and run a couple of instances of the game to see replication in action.

.. admonition:: Badumna Pro only !!!

  .. include:: build-and-run-unity-badumna-pro.txt