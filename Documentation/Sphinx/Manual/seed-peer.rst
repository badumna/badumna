.. include:: ../apilink-directives.txt

.. _seed-peer:

Running a Seed Peer [Pro]
-------------------------
.. spelling::
    sp

.. include:: pro-tipbox.txt

There are a couple of key requirements governing the setting up of a
Badumna network:

#. To function correctly, every Badumna network needs at least one peer
   running on a machine with an *open connection*,
   where an open connection is one that:

   -  has a public IP address,

   -  has an open or full-cone NAT type,

   -  uses a port not blocked by your firewall.

#. To join a Badumna network, a peer needs to find an existing member of
   the network.

These requirements are easily satisfied by running a peer on a machine with an open connection, and configuring other peers to connect to this first peer (known as a *seed peer*).
This is the standard way to set up a Badumna network.
The :ref:`section:seed-peer` section below explains how to start a seed peer for your network,
and :ref:`configuration-pro` explains how to configure other peers to connect to the seed peer.

.. tip::
   **SeedPeer can tell you if you have an open connection.**
   
   See :ref:`check-open-connection`

If you only want to run your Badumna network within a local area network
(LAN) for development purposes, then it is possible to relax these
requirements. Badumna clients can be configured to run in *LAN mode*
where no peer with an open connection is required. Badumna clients can
also be configured to find each other on the local subnet using
broadcast, so no seed peer is required in this scenario. :ref:`badumna_on_lan`  explains how to
configure peers to run in LAN mode and use subnet broadcast.



.. warning::
   **Never mix regular peers and LAN mode peers.**
   
   When using the *LAN mode*, all peers running in the same network should be configured to use *LAN mode*. The network's behaviour becomes undefined if you try to mix regular peers and peers running in *LAN mode* together.

.. _section:seed-peer:

Starting a seed peer
~~~~~~~~~~~~~~~~~~~~

.. tip::
   **Ensure you have the Seed Peer application installed.**
   
   The SeedPeer application is located in the SeedPeer directory inside
   the ManagementTools directory.

Every Badumna enabled application requires a seed peer to allow peers to
find the network [1]_. The seed peer must be started on a machine with
an open connection.

Badumna networks should also have an *application name* to identify
them. Only peers using matching application names will be able to join
the same network. The seed peer must be configured with this application
name, which can be achieved by using the ``--application-name`` command
line option when launching SeedPeer, e.g.:

.. container:: commandline
   
   ``SeedPeer.exe --application-name=com.example.my-app``

where ’com.example.my-app’ should be replaced with the name for your
Badumna network. It is recommended that application names use the
reverse-domain-name style to ensure uniqueness.

By default, SeedPeer listens on port 21251. You can configure SeedPeer
to listen on a different port by using the ``--badumna-config-port``
command line option. This may be required if the default port number is
not available on the machine. e.g.:

.. container:: commandline
   
   ``SeedPeer.exe --badumna-config-port=12345``
   
.. tip::
   **Running under Mono**
   
   Badumna applications can run under Mono on Windows, Mac OS X or Linux.
   The commands listed in this manual show how to run applications on Windows using the Microsoft .NET framework.
   To launch Badumna Network Suite applications under Mono, simply prefix the command line input with ``mono``.
   For example, the instructions to launch a seed peer in verbose mode are as follows:

   .. container:: commandline

      ``SeedPeer.exe --verbose``

   To launch a seed peer in verbose mode using Mono, you would enter:

   .. container:: commandline
   
      ``mono SeedPeer.exe --verbose``
   


.. _check-open-connection:
   
Checking you have an open connection
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The seed peer should be run on a machine with an open connection.

To check that your machine has an open connection, run SeedPeer with the
``--verbose`` option:

.. container:: commandline
   
   ``SeedPeer.exe --verbose``

:ref:`fig:seedpeer-output` shows an example of the output of this command. The line
beginning ‘Public address’ shows the NAT type, followed by the public IP
address, followed by the port being used. If the NAT type shown is
‘Open’ or ’Full cone’, then the host machine has an open connection.

.. _fig:seedpeer-output:
   
.. figure:: ../images/seedpeer-output.png
   :align: center
   :width: 100%
   
   Verbose SeedPeer output

Starting multiple seed peers
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

You can increase reliability by running multiple seed peers for the same
network on different hosts. This helps to ensure that there is always a
seed peer for clients to connect to even if one host is unavailable.

Each seed peer in a network needs to be made aware of all other seed
peers to guarantee that a single network is formed. Configuring SeedPeer
with a single other seed peer can be done using the ``--badumna-config-seedpeer`` command line option, e.g.:

.. container:: commandline
   
   ``SeedPeer.exe --badumna-config-seedpeer=sp2.example.com:21251``

The second seed peer should also be launched with the address of the
first seed peer. The two seed peers can be launched in either order.

When using more than two seed peers the seed peer addresses must be
specified in a configuration file. To generate a configuration file
containing the default options used by SeedPeer run the following
command:

.. container:: commandline
   
   ``SeedPeer.exe --badumna-config-save=BadumnaOptions.xml``

After SeedPeer indicates that the options have been saved stop it by
pressing Ctrl-C. If you open the newly created BadumnaOptions.xml you will
notice that it contains the tag ``<Initializer />``. This tag specifies
the seed peers as a comma separated list in the format ADDRESS:PORT. As
an example, assume you want to start three seed peers in your network:
sp1.example.com, sp2.example.com, and sp3.example.com. Each of these
seed peers will be started on the default port, 21251. The Initializer
tag for sp1.example.com should be updated as follows:

.. sourcecode:: c#

    <?xml version="1.0" encoding="utf-8"?>
    <Modules>
     ...
     <Module Name="Connectivity" Enabled="true">
      ...
      <Initializer>sp2.example.com:21251,sp3.example.com:21251</Initializer>
      ...
     </Module>
     ...
    </Modules>

As you can see we have included the address and port number of the other
two seed peers in the body of the Initializer tag. The connectivity
configuration for the other two seed peers should be updated in a
similar manner.

Once the configuration file has been updated, SeedPeer can be started
with the new configuration using the following command:

.. _re-starting-a-seed-peer:

.. container:: commandline
   
   ``SeedPeer.exe --badumna-config-file=BadumnaOptions.xml``

Restarting a seed peer
^^^^^^^^^^^^^^^^^^^^^^

Typically, SeedPeer is the first application that you start in your game
network. When SeedPeer starts the only other peers it knows about are
the other seed peers specified in its configuration. If there is only
one seed peer for the network then SeedPeer will not initially know
about any other peers. As other peers are started and connect to the
seed peer it will keep track of some of these peers, allowing new peers
to be joined into the same network. If SeedPeer is now shutdown and
restarted after some delay there may be an existing network of peers
that is no longer connected to the seed peer. New peers will connect
with the seed peer and form a new network that is disconnected from the
first. To avoid this situation SeedPeer should be given the ``--rejoin``
option when it is restarted, e.g.:

.. container:: commandline
   
   ``SeedPeer.exe --rejoin``

This option causes SeedPeer to try to reconnect with the peers it was
talking to during its previous run.

Note that if multiple seed peers are running on a network and they are
not all shut down at the same time the rejoin option is not required.
The seed peer that is restarted will connect to the seed peers that
remained running and so will be connected to the existing network.

Starting a seed peer for a secure Badumna network
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

If you are running a secure Badumna network with Dei Server, you will need to specially configure your seed peer.
See :ref:`dei` for more information about secure networks.
Please refer to :ref:`configuring-a-seed-peer-to-use-dei` for full instructions on starting a seed peer for a secure Badumna network.

Other SeedPeer command line options
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

For a complete list of the command line options available for SeedPeer
run SeedPeer with the ``--help`` option, e.g.:

.. container:: commandline
   
   ``SeedPeer.exe --help``

.. [1]
   Unless Badumna is being used in LAN mode.
