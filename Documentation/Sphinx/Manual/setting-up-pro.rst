
.. toctree::
   :hidden:
   
   configuration-pro

.. include:: ../apilink-directives.txt

.. _setting-up-pro:

Setting up a Badumna Pro application
====================================

To use Badumna Pro in your game, you should first set up a Badumna network for your application.
Typically you do this by starting a Seed Peer, see :ref:`seed-peer`.

During development you can configure your peers to run in LAN mode for testing and you won't need a seed peer, but only peers on your LAN will be able to join.

Your game should initialize Badumna and log in at start up, and shutdown Badumna before exiting.
While running, your game should also regularly update Badumna.

Initialization and Log In
-------------------------

Your peers need to be configured to join your Badumna network, by passing appropriate options at initialization.

As a minimum, your need to set your application name (using a reverse domain name you own is a good practice), and either set a seed peer or configure for LAN mode:

.. sourcecode:: c#

  var options = new Options();
  options.Connectivity.ApplicationName = "com.example.my-app";
  options.Connectivity.SeedPeers.Add("sp1.example.com:21251");
  // Uncomment the following line to use LAN mode (no seed peer required)
  // options.Connectivity.ConfigureForLan();``

Then initialize Badumna with these options, and log in with a character name.
  
.. sourcecode:: c#

    this.network = NetworkFacade.Create(options);
    this.network.Login(characterName);

``NetworkFacde.Create`` will block until initialization is complete.
To initialize Badumna asynchronously, you can use ``NetworkFacade.BeginCreate`` instead:

.. sourcecode:: c#

    this.network = NetworkFacade.BeginCreate(
        options,
        result =>
        {
            this.network = NetworkFacade.EndCreate(result);
            this.network.Login(characterName);
        });
    
See :ref:`configuration-pro` for more information on configuring your peers.

Regularly update Badumna
------------------------

Badumna needs to be triggered regularly to do its job, so ``ProcessNetworkState()`` should be called in your game's main update loop.

.. sourcecode:: c#

    this.network.ProcessNetworkState();

Shutdown Badumna
-----------------
On application exit, it is important to inform Badumna that the user is closing the application. 
This will enable Badumna to shutdown the network thread and also perform other finalization. 

.. sourcecode:: c#

   this.network.Shutdown();
