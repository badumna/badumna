.. include:: ../apilink-directives.txt

.. _match-controller:

Badumna Matches: Match Controllers
==================================

Match controllers can be used to send messages between match members, and to replicate global match state.

Defining a match controller
------------------------------

The match controller can be any class that has methods and properties marked for replication with the ``Replicable`` attribute.

.. sourcecode:: c#

    public class MyController
    {
        [Replicable]
        public bool GameStarted { get; set; }
        
        [Replicable]
        public void SignalGameStart()
        {
            // ...
        }

        [Replicable]
        public void SignalPlayerIsReady(MemberIdentity member)
        {
            // ...
        }
    }
	
Mark properties as replicable to include them in the replicated global match state.
    
.. include:: type-replication-warning.txt

Mark methods as replicable to make them available via remote procedure call (RPC) for sending messages.

.. include:: rpc-signature-registration.txt

Registering a controller for a match
------------------------------------

You need to use the |Match<T>| generic class, and pass a match controller object when creating or joining the match.

.. sourcecode:: c#

    MyController controller = new MyController();
    Match<MyController> match = this.network.Match.CreateMatch(
        controller, 4, playerName, this.CreateReplica, this.RemoveReplica);

Sending messages
----------------

Clients can send messages to the host by making an RPC on one of the controller's replicable methods:

.. sourcecode:: c#

    if (Match.State != MatchStatus.Hosting)
    {
        this.match.CallMethodOnHost(
            this.match.Controller.SignalPlayerIsReady, this.match.MemberIdentity);
    }

Any member can broadcast a message to all clients, by calling ``CallMethodOnMembers`` and passing one of the controller's replicable methods:

.. sourcecode:: c#

    this.match.CallMethodOnMembers(this.match.Controller.SignalGameStart);

To send a message to a particular client, use ``CallMethodOnMember`` and pass the identity of the member you wish to send the message to:
    
.. sourcecode:: c#

    this.match.CallMethodOnMember(memberIdentity, this.match.Controller.SignalPlayerIsReady);

Replicating global state
------------------------

When the host sets replicable properties on the controller, they will be automatically replicated on clients:

.. sourcecode:: c#

    if (Match.State == MatchStatus.Hosting)
    {
        this.match.Controller.GameStarted = true;
    }

Other members should not directly set replicable controller properties, as these updates will not be replicated to other members.
	
Try it out
~~~~~~~~~~~

See match controllers in action:

- :ref:`Unity Match Controller Tutorial<unity-match-tutorial2>`