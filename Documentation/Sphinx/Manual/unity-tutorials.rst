
.. toctree::
   :hidden:
   
   Unity Scene-Tutorial 1 <tutorial1-scene-unity>
   Unity Scene-Tutorial 2 <tutorial2-scene-unity>
   Unity Scene-Tutorial 3 <tutorial3-scene-unity>
   Unity Scene-Tutorial 4 <tutorial4-scene-unity>
   Unity Scene-Tutorial 5 <tutorial5-scene-unity>
   Unity Scene-Tutorial 6 <tutorial6-scene-unity>
   Unity Match-Tutorial 1 <tutorial1-match-unity>
   Unity Match-Tutorial 2 <tutorial2-match-unity>
   Unity Match-Tutorial 3 <tutorial3-match-unity>
   Unity Match-Tutorial 4 <tutorial4-match-unity>
   Unity Match-Tutorial 5 <tutorial5-match-unity>
   Unity Match-Tutorial 6 <tutorial6-match-unity>
   Unity Match-Tutorial 7 <tutorial7-match-unity>
   Bootcamp Tutorial <tutorial-bootcamp>
   AngryBots Tutorial <tutorial-angrybots>
   Unity scene templates <unity-package-templates>
   Unity match templates <unity-package-match-templates>
   
   unity-package-templates
   unity-package-match-templates
   unity-platform-settings
   angrybots-source


.. _unity-tutorials:

Unity tutorials
~~~~~~~~~~~~~~~~~~

This section presents a step-by-step guide to getting started with Badumna and Unity.
Through a set of tutorials we will show how to create a basic multiplayer game and build on that to add more features.
Please note that the tutorials marked with "Pro only" are intended for Badumna Pro.
These features are not supported in Badumna Cloud.

- Badumna Scene Tutorials
   - :ref:`1 - Basic replication<unity-scene-tutorial1>` (estimated time - 9 minutes)
   - :ref:`2 - Remote Procedure Calls<unity-scene-tutorial2>` (estimated time - 6 minutes)
   - :ref:`3 - Proximity chat<unity-scene-tutorial3>` (estimated time - 5 minutes)
   - :ref:`4 - Verified identity<unity-scene-tutorial4>` (Pro only)
   - :ref:`5 - Arbitration server<unity-scene-tutorial5>` (Pro only)
   - :ref:`6 - Private chat<unity-scene-tutorial6>` (Pro only)

- Badumna Match Tutorials
   - :ref:`1 - Matchmaking<unity-match-tutorial1>` (estimated time - 9 minutes)
   - :ref:`2 - Match controller<unity-match-tutorial2>` (estimated time - 6 minutes)
   - :ref:`3 - Entity Replication<unity-match-tutorial3>` (estimated time - 6 minutes)
   - :ref:`4 - Remote Procedure Calls<unity-match-tutorial4>` (estimated time - 5 minutes)
   - :ref:`5 - Chat<unity-match-tutorial5>` (estimated time - 6 minutes)
   - :ref:`6 - Hosted entities<unity-match-tutorial6>` (estimated time - 6 minutes)
   - :ref:`7 - Advanced<unity-match-tutorial7>` (estimated time - 6 minutes)

- Advanced tutorials
   - :ref:`Tutorial - Bootcamp<bootcamp-tutorial>` (estimated time - 15 minutes)
   - :ref:`Tutorial - AngryBots<angrybots-tutorial>` (estimated time - 15 minutes)


After completing the tutorials, you will be able to make your own fully functional multiplayer game using Badumna Cloud and Unity.







