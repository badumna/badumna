.. _control-center:

Control Center [Pro]
====================

.. include:: pro-tipbox.txt

Control Center is a central administrative tool for managing all Badumna related services.
It is ideal for large-scale deployments which may involve multiple services deployed across several remote machines.
The Control Center provides game administrators with the ability to install services on remote machines and manage (start/stop/update/monitor) them via a web interface.
It uses a program called *Germ* to communicate with remote machines.
You have to install and start a Germ on a remote machine that you want to use for Badumna related services.
Once the Germ is running on the remote machine, you can use the Control Center to install and start new services on this remote machine.
You can also monitor the services that are running on this remote machine via the Control Center.
As the Control Center comes with a web interface, you can access it from any machine using a web browser.

Requirements
------------
You will require 4.0 .NET framework to run Control Center. 


Running Control Center on Windows Vista/7
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

On Windows Vista and Windows 7, Control Center needs to be run with administrator privileges the first time it is using a particular port, including the very first time it is run.

When running with administrator privileges, Control Center will add the port to the URL Access Control List.
After this has been done, Control Center will not need administrator privileges on subsequent runs.


.. tip::
   **Running Control Center with administrator privileges**
   
   To run Control Center with administrator privileges, launch *Command Prompt* with administrator privileges by right clicking on its Start Menu shortcut (:menuselection:`Start --> All Programs --> Accessories --> Command Prompt`) and selecting *Run as administrator*.
   You can then launch Control Center from the command line as described above.

Running Control Center on Mono
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Control Center has been tested with Mono 2.10.8 on Ubuntu 12.04 and Mono 2.10.9 on Mac OS X.
It is important that you have to remove the Microsoft.Web.Infrastructure.dll from the ControlCenter/bin directory when running Control Center on Mono.
Please refer to `ASP.NET MVC3 Support <http://www.mono-project.com/Release_Notes_Mono_2.10#ASP.NET_MVC3_Support>`_ for more detail information.

   
.. _section-introduction:

Initial Configuration
---------------------

Before you can use the Control Center, you need to understand how the Control Center operates.
For obvious security reasons, we want the Control Center to communicate with its Germs only (and not any other Germ processes from some other Control Center).
The Control Center uses a certificate based system to support this feature.
The first time you start the Control Center, it will auto generate a certificate (that includes a private key and a public key).
As part of the certificate generation, you will be asked to enter a pass phrase.
Please enter a unique pass phrase and store it in a secure place.
You cannot change this pass phrase once the certificate has been generated (refer to :ref:`section-certificate` if you forget your pass phrase).

As the Control Center can be accessed via a web browser, it can be accessed from any remote machine.
In order to make it secure, the Control Center comes with a user authentication system.
This ensures that you have the convenience of accessing the Control Center from anywhere you want but at the same time you can restrict who is allowed to access the Control Center.
The Control Center uses a SQLite database to store user information.
The default installation of Control Center includes all the necessary database files that are required for this operation and it is preconfigured to work with this database.
If you wish to use a different database application such as MySql for this purpose, then please refer to :ref:`section-database` for more details.

Finally, you can only manage a single application from a Control Center instance. 

Therefore, if you intend to deploy multiple applications and manage them via the Control Center, you will require one instance of Control Center for each application.

.. _section:start-cc:

Starting the Control Center
---------------------------

The Control Center application can be found in the ControlCenter
directory. To start the Control Center, run the launcher executable from
the command line:

.. container:: commandline
   
   ``ControlCenter.exe``

Command line options that can be specified are:

-p, --port  The port to listen on
-i, --ip    The IP address to host the application at.

The default port is 21254. The default IP address is *localhost*. To
make Control Center accessible remotely, you must specify your machine’s
internal IP address. For remote access, you will also need to configure
your router to perform port forwarding to forward HTTP requests and TCP
connections to the specified port.

For example, to start an instance of the Control Center at port number
1234 on the local host:

.. container:: commandline
   
   ``ControlCenter.exe --port=1234 --ip=127.0.0.1``

If you want to start a second instance of the Control Center for another
application, you can do so by using a different port number. The port
number is therefore important. It allows you to identify the Control
Center that you are using for a specific application.

As mentioned above, if this is the first time you are starting the
Control Center, it will auto-generate a certificate and will prompt you
to enter a pass phrase for the certificate. The Control Center will now
start and run in the background. If you restart the Control Center at
some point in the future, it will start automatically using the same
certificate. You will be asked to enter the pass phrase if you restart
the Control Center.

Accessing the Control Center
----------------------------

Once you have the Control Center running, you can access it from any
machine using a standard web browser. To access the Control Center you
can enter the IP address/hostname of the machine that is running the
Control Center followed by its port number. For example if you are
accessing the Control Center from the local machine, you can enter the
following address in the browser window: http://localhost:21254. This
will access the Control Center instance that is running on port 21254 of
the local machine. If you are accessing the Control Center from a remote
machine, you need to enter the complete IP address or hostname along
with the port number. Please note that when you start the Control Center
(as explained in :ref:`section:start-cc`) it will automatically start
a browser on the local machine for convenience.

Authentication and user accounts
--------------------------------

As the Control Center can be accessed from any remote machine, its
access is controlled by an authentication system. You need a valid
username and password. The default installation of Control Center comes
with an admin account that has been created. Hence, the first time you
access the Control Center, you need to use the following account
details:

:Username: admin
:Password: admin_pass

.. _fig:cc-7:
   
.. figure:: ../images/ControlCenter/cc-19.png
   :align: center
   :width: 100%
   
   Control Center - Application settings

.. _fig:cc-2:
   
.. figure:: ../images/ControlCenter/cc-03.png
   :align: center
   :width: 100%
   
   Control Center - Main page

After you login, it will ask you to enter a friendly name and a unique application name (see :ref:`fig:cc-7`).
You can enter a friendly name that will allow you to identify your application.
The application name is the same unique string which is used by the Badumna client to join the network.
Make sure that you use the same string in both places.
Please refer to :ref:`setting-up-pro` for details on how to specify the application name in a Badumna client.
Once you enter this information and click submit, you will go to the Control Center’s main page.

.. warning::
   **Application name can be changed later on from Application Setting configuration, while Friendly name can only be set once.**

At the top of the page, you will see the change password link (see
:ref:`fig:cc-2`). **Please change the password**. The admin account
will allow you to login to all the Control Centers that you may have
running on a particular machine.

.. warning::
   **Please change the default password to the admin account.**

When you login as admin, you can add users and give them permission to
access the Control Center. If you have multiple applications deployed
(being managed by multiple Control Centers) you can set the permissions
for a user so that they have access to only certain applications. In
order to manage users, click on the ‘Admin’ tab.

To add a new user, click on the ‘Create user’ link. You will see a
screen as shown in :ref:`fig:account_registration`. Enter the details
for the new user as requested and then click the ‘Register’ button. Once
you add a user, you need to give them permission to access the Control
Center. This is done by assigning the role. Each application is
identified by a role. You therefore need to create a role that
corresponds with the Control Center friendly name. You can create a role
by clicking on the ‘Manage or Create Role’ link. You will see a screen
similar to :ref:`fig:manage_role`. Enter the same name as the
friendly name and click ‘Add Role’ button. You will notice a new role
being added to the list. Now you can click on the ‘Manage’ link
corresponding to the new role that you just created. You will see a list
of all existing users including the user you just created. To give this
user permission to access the Control Center, enable the corresponding
radio-button as shown in Figure :ref:`fig:manage_single_role`. You
have now successfully created a new user and given them permission to
access the Control Center. In order to remove a user or change
permissions, you can click on ‘Manage user’ link. You will see a screen
similar to Figure :ref:`fig:manage_user`. To delete a user, click on
the corresponding ‘Delete’ link next to that user. When you login as a
normal user, you don’t have access to the ‘Admin’ tab.

.. _fig:account_registration:
   
.. figure:: ../images/ControlCenter/cc-04.png
   :align: center
   :width: 100%
   
   Control Center - Account registration

.. _fig:manage_role:
   
.. figure:: ../images/ControlCenter/cc-05.png
   :align: center
   :width: 100%
   
   Control Center - Manage role

.. _fig:manage_single_role:
   
.. figure:: ../images/ControlCenter/cc-06.png
   :align: center
   :width: 100%
   
   Control Center - Manage a specific role

.. _fig:manage_user:
   
.. figure:: ../images/ControlCenter/cc-07.png
   :align: center
   :width: 100%
   
   Control Center - Manage user

.. _section:germ-installation:  
 
Germ installation
-----------------

In order to access remote machines and start services you first need to
install the *Germ* package on the remote machine. It is important that
you perform this step only after you have started your Control Center.
This ensures that the public key information specific to the Control
Center is copied on the remote machine. To install and start a Germ on a
remote machine, perform the following steps:

#. Go to the Germ directory under Control Center directory. Copy this
   directory to the remote machine that you want to use as a Badumna
   server. Make sure you have created a certificate by running Control
   Center first (i.e. the publicKey.key is included in Germ directory).

#. To start the Germ on a Windows machine, use the following command [1]_

   .. container:: commandline
      
      ``LaunchGerm.exe``

The above command will start a Germ on that machine and it will listen
on the default port 21253. If for some reason the default port (21253)
is not available on that machine, you can start the Germ and have it
listen on a different port number using the following command:

.. container:: commandline
   
   ``LaunchGerm.exe --port=1000``

The above command will start the Germ process and it will listen on port
number 1000.

Germ also can be started to automatically connect to a Control Center by using the following command.

.. container:: commandline

   ``LaunchGerm.exe --connect-to=127.0.0.1:1000`` 

In this case the germ will start and try to connect to the Control Center running locally.
Note that the port number 1000 is not the port number that you specified when starting the Control Center but instead this is the port number of the Germs listener that can be set from the Control Center.
Please refer to :ref:`subsec:germs-listener` explaining how to configure the Germs listener.

.. warning::
   **Windows 7 and Vista require administrator privileges.**
   On Windows 7 and Vista, Germ must be started with administrator privileges.

Main information page
---------------------

We will now explain the main information page of the Control Center. To
go to the main information page, you can click on the first menu item
which should show your Control Center friendly name. In
:ref:`fig:cc-2`, this is the menu item labelled ‘MyApp’. As you can see
from the figure, the main page is divided into four sections: Running
Services, Germs Host, Global Settings and Available Services. ‘Running
Services’ lists all the services that are currently running as part of
your application. If you have started the Control Center for the first
time, there won’t be any services listed under this section. ‘Germs
Host’ provides a list of remote machines that you have access to. These
machines can be used to start new services specific to your application.
The ‘Global Settings’ section stores all the settings that are relevant
to the entire application. The ‘Available Services’ section lists all
the services that are available for the application and can be started
on remote machines. We will now explain these sections in detail.

Germs host
----------

This section lists all the machines that can be used to start Badumna
specific services. To add a machine to this list, click on the ‘Add
Germ’ link. You will see a screen as shown in :ref:`fig:cc-addgerm`.
Enter the hostname or IP address of the machine that you want to add.
Enter the port number that the Germ process on that machine is listening
on. This is the port number that you specified when you started the Germ
process on the machine (the default port is 21253). Click on the
‘Submit’ button. This will take you back to the main information page.
If the Control Center is able to establish a connection with the remote
machine (via the Germ process), it will show the details of the remote
machine in the list of ‘Germs Host’. Along with the host name, the
status of the machine is displayed (Online/Offline). This section also
displays the CPU load and the available physical memory on this machine.
If you click on the ‘View details’ link next to the Germ, you can get
further information about your remote machine such as the total number
of bytes sent and received per second. You will also see two graphs -
one plots the CPU usage and memory available and the other plots the
incoming and outgoing network traffic in kilobytes per second (see
:ref:`fig:cc-3`).

.. _fig:cc-addgerm:
   
.. figure:: ../images/ControlCenter/cc-08.png
   :align: center
   :width: 100%
   
   Control Center - Adding a Germ

.. _fig:cc-3:
   
.. figure:: ../images/ControlCenter/cc-09.png
   :align: center
   :width: 100%
   
   Control Center - Germ details

.. _subsec:germs-listener:

Germs listener Settings
-----------------------

Germs listener is a new functionality introduced in Control Center 2.3 where the germ can automatically connect to the Control Center.
If this feature is active, the user will not need to add the Germs into the Germs host list manually, the Control Center will automatically take care of it.
Go to **Germs listener Settings** under the *Admin* tab to enable or disable germs listener, by default germs listener is disabled.

The 'Port Number' section allows you to configure which port that the germs listener should listen on.
This is the port number that should be used when starting the Germ application with ``--connect-to`` command argument.

.. figure:: ../images/ControlCenter/cc-24.png
   :align: center
   :width: 100%

   Control Center - Germs listener Settings

Global Settings
---------------

The ‘Global Settings’ section allows you to configure parameters that are applied to the entire application.
It is divided into two modules: application settings and connectivity settings.
If you click on the ‘Configure’ link next to ‘Application Settings’, you will see there is only one parameter that you can configure: application name.
This string should match the one used in your client application (refer to :ref:`setting-up-pro` for more details about setting the application name in a Badumna peer .

The ‘Connectivity Settings’ allow you to configure all the parameters
associated with network connectivity. Click on the ‘Configure’ link next
to ‘Connectivity Settings’. There is only one parameter that you can
configure as part of the basic settings (broadcast option). If you want
your Badumna server processes to have broadcast enabled, then click on
the radio button next to ‘Is broadcast enabled’. You need to specify a
port number at which the relevant processes should broadcast. The
default port number is 21250. You can use that port number or change it
to a more appropriate port number if 21250 is not available.

Connectivity Settings also provides advanced configuration options.
These options are intended for advanced users only. It is recommended
that you do not access these options unless you are an advanced user.


Please refer to :ref:`section:advanced-connectivity` if you intend to
make changes to the advanced connectivity options.

.. _subsec:services:

Available services
------------------

This section lists all the services that are available for the application and are ready to be started.
You will observe that there are five standard services that are available for all applications:

 - :ref:`Seed Peer<seed-peer>`
 - :ref:`Dei Server<dei>`
 - :ref:`tunnel`
 - :ref:`overload`
 - :ref:`statistics-server`
 
Apart from these five services, you can add custom services that are specific to your application such as an Arbitration Server that manages combat functionality in your application.
The next section explains how to start the default services that are available.
Please make sure that you have added at least one Germ host to your Control Center before you start any services (refer to :ref:`section:germ-installation` on how to start a Germ on a remote machine).

.. _fig:cc-deiconfig:
   
.. figure:: ../images/ControlCenter/cc-10.png
   :align: center
   :width: 100%
   
   Control Center - Dei Server Config

.. _subsection:dei:  
 
Starting Dei Server
-------------------

If you are planning to use Dei for user authentication then you will
need to start a Dei Server (this is the account management service). If
you click on the ‘Start’ button next to Dei account management, it will
start Dei server with default settings. It is a good idea to click on
‘Configure’ and make sure the settings are correct before starting any
service. When you click on ‘Configure’ link next to Dei account
management, you will see a screen as shown in :ref:`fig:cc-deiconfig`.

The first option allows you to set the expiry time for the participation
key in minutes. The default expiry time is 1440 minutes (i.e. 1 day).
The second option allows you to set the port number for Dei server. The
default port number for Dei Server is 21248. If for some reason, your
remote machine does not allow using that port number, you can change it
here. The third option allows you to specify if you want to use an SSL
connection when communicating with Dei Server. Generally, SSL should
only be disabled for testing purposes. The last option provides details
about the Dei installation package.

You are now ready to start Dei Server. Click on the ‘Start’ link. The
first option allows you to give a custom name to your Dei Server (e.g
MyAppDeiServer). In the second option, you have to pick the machine from
the drop-down list that you want to start Dei Server on. If you had
enabled SSL connections, you will see two more options. These options
allow you to specify which certificate to use (please refer to
:ref:`dei` for more information on the different types of
certificates that Dei supports). If you are using an auto-generated
certificate then select ‘Autogenerate certificate’ and click ‘Submit’.
If you intend to use a custom certificate, then select ‘Custom
certificate’. You will have to enter a passphrase for your custom
certificate and then click ‘Submit’. If the Control Center is able to
start Dei Server, you will see a message at the bottom of the screen to
that effect. If for some reason, the Control Center is unable to start
Dei service, you will see a message as to why it was not able to start
Dei server on the Germ host. If the service has started, you will also
see an entry under the ‘Running Services’ section on the main page.


The entry will display the service name, the germ host that the service
is running on, the last time the machine was pinged and the response
received.

.. _dei-server-login:

Login to Dei Server through Control Center
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

When you start Dei Server from the Control Center, you can login to Dei
using an admin account to create new Dei accounts and edit existing
accounts. To do this click on the *Show Status* link corresponding to
your running Dei Server instance. You will see a page as shown in
:ref:`fig:cc-logindeiserver`.

The top part of the page shows the status of Dei Server (ping response
and time of last ping). The second half of the page provides Dei account
management functionality.

.. _fig:cc-logindeiserver:
   
.. figure:: ../images/ControlCenter/cc-20.png
   :align: center
   :width: 100%
   
   Control Center - Login to Dei Server

By default, you can login to Dei Server using the following credentials:

:Username: admin
:Password: admin\_password

If you successfully login you should see a screen as shown in
:ref:`fig:cc-loggedin-createuser`. Please change the default admin
password the first time you login to Dei. To change the password, you
can click on the "Change password" link next to **Dei Account
Management** (see :ref:`fig:cc-loggedin-createuser`). You can now
create new Dei accounts using this interface and assign appropriate
permissions to them  [2]_. Control Center provides three default account
types that can be used when creating new Dei accounts (i.e. *user, admin,
hosted service*). There is also an additional account type called
*custom*.

#. *User account type:* Selecting this account type will result in a Dei
   account with permission only. Use this account type to create Dei
   accounts for normal Badumna users.

#. *Admin account type:* Selecting this account type will result in a Dei
   account with six permissions - and . Usually, you will only need one
   admin account. However, you have the flexibility to create additional
   admin accounts if you have multiple people administering Dei Server.

#. *HostedService account type:* Selecting this account type will result
   in a Dei account with and permissions. Badumna services such as
   Overload and Arbitration Servers that use distributed lookup will
   require this account type in order to successfully announce their
   services.

#. *Custom account type:* This option allows you to create an account
   and decide on what permissions to be assigned to the account. Once
   you select this account type, you will notice that you are able to
   edit the permissions table.

You can also edit and remove existing Dei accounts by clicking on the
*Edit users* tab, as shown in :ref:`fig:cc-loggedin-editusers`.

.. _fig:cc-loggedin-createuser:
   
.. figure:: ../images/ControlCenter/cc-21.png
   :align: center
   :width: 100%
   
   Control Center - Create New Dei user account page

.. _fig:cc-loggedin-editusers:
   
.. figure:: ../images/ControlCenter/cc-22.png
   :align: center
   :width: 100%
   
   Control Center - Edit existing Dei user account page

.. _fig:cc-seedpeer:
   
.. figure:: ../images/ControlCenter/cc-11.png
   :align: center
   :width: 100%
   
   Control Center - Seed Peer Config

Starting a seed peer
--------------------

Click on the ‘Configure’ link next to ‘Seed Peer’. You will see a screen
as shown in :ref:`fig:cc-seedpeer` The first option allows you to
specify if you are using the Dei authentication system as part of this
application. If you are using Dei Server, then select this option by
clicking on the radio button. You will be asked to enter further details
about your Dei Server such as the IP address, username/password and
whether to use an SSL connection (please refer to
:ref:`configuring-a-seed-peer-to-use-dei` for more information related
to Dei set up).

The second option allows you to specify if you are starting a new
network or joining an existing game network. If this is a new
application that you are starting and currently there are no users in
the network then you should select this option by clicking on the radio
button. However, if this is an application that has been running for
some time and you want the Seed Peer to join the existing network then
you should leave this option unchecked.

The third option allows you to specify if the seed peer should also be a matchmaking server for your game.

The fourth option allows you to specify the listening port for the Seed
Peer. This port number must match the port number that you will specify
in the client configuration (see :ref:`configuration-pro` for more
information on client configuration).

The fifth option allows you to specify if you wish to monitor the
service. Monitoring a service allows you to be notified if the service
goes down for some reason. If you check the Monitor Service Performance
button, you will be asked to enter the monitoring frequency in seconds.
Please enter the frequency. Make sure that you do not enter a number
that is very small as it will increase the network traffic. Monitoring
the service performance every 5 minutes (300 seconds) is recommended.

The last option displays details of the Seed Peer package. You can now
click on the ‘Start’ link to start the Seed Peer. You will see a screen
as shown in :ref:`fig:start_service`. You can give your service a
custom name (e.g. SeedPeer-A). Select the machine you want to start the
service by selecting the Germ host from the drop down list. Now click on
the ‘Submit’ button. The Control Center will start the service and
display a message accordingly. You will also see the Seed Peer service
listed under the ‘Running Services’ section.

.. _fig:start_service:
   
.. figure:: ../images/ControlCenter/cc-12.png
   :align: center
   :width: 60%
   
   Control Center - Start service

.. _fig:cc-overload:
   
.. figure:: ../images/ControlCenter/cc-13.png
   :align: center
   :width: 100%
   
   Control Center - Overload Peer Config

Starting a Overload Peer
------------------------

To start an overload peer from the Control Center, you need to configure
it first by clicking on the ‘Configure’ button. You will see a screen as
shown in :ref:`fig:cc-overload`. The first option allows you to
specify if you are using Badumna’s distributed lookup option for service
discovery (see :ref:`section-distributed-lookup` for more details).
Check this box if you are using the distributed lookup option.

The second option allows you to specify if you are using a Dei
authentication service. If you are using Dei, then check the box, next
to ‘Use Dei Authentication Server’. You will be asked to enter further
details about your Dei Server such as the IP address, username/password
and whether to use a SSL connection (please refer to
:ref:`configuring-a-seed-peer-to-use-dei` for more information related
to Dei setup).

The next option in the overload peer configuration allows you to specify
the port number for the overload peer. The default port number is 21252.
You can change this to a different port number if 21252 is not available
on that machine.

Monitor Service Performance option is exactly the same as for a seed
peer. It gives you an option to monitor the performance of the overload
peer and make sure that it is running all the time.

The last option ‘Package details’ provides details on the Overload Peer
installation package. After completing the configuration, you can start
the Overload Peer service by clicking on the ‘Start’ link. You will then
see a pop-up window that will allow you to give your service a name
(e.g. Overload Peer - USA) and also select the machine to start the
service from the drop down list. You can now click on ‘Submit’ to start
the service. An appropriate message will be displayed and you will see
the service name included in the ‘Running Services’ section.

.. _fig:cc-tunnel:
   
.. figure:: ../images/ControlCenter/cc-14.png
   :align: center
   :width: 100%
   
   Control Center - HTTP Tunnel Config

.. _subsection:statistic-server: 

Starting a Statistic server
---------------------------

From the list of available services on the home screen of the Control
Center you should be able to see an entry for the Statistics Server. If
you select "Start", the Control Center will deploy the Statistics Server
with the default settings, as shown in :ref:`fig:presence-start`.
Selecting "Configure" will take you to a screen where you can change the
listening port and message interval, as shown in
:ref:`fig:presence-configure`.

.. _fig:presence-start:
   
.. figure:: ../images/presence-start.jpg
   :align: center
   :width: 60%
   
   Statistics Server - Starting the Server

.. _fig:presence-configure:
   
.. figure:: ../images/presence-configure.jpg
   :align: center
   :width: 60%
   
   Statistics Server - Configuring the Server

The listening port is the port number where the Statistics Server will
listen for regular updates from each of the Badumna clients in the
network. The default port number is 21256. Change the port number if
this is unavailable.

The message interval is the amount of time in between the regular update
messages from each Badumna client. This setting determines the frequency
with which the Statistics Server records a snapshot of the current
network activity into its database, as well as determining how long to
wait before closing a session for a user that has stopped sending
updates to the Statistics Server. The default update frequency is set to
300 seconds (5 minutes). This is the recommended value. Please note that
this value must be the same as the value configured in the client (see
:ref:`configure-client`).

Starting an HTTP tunnelling service
-----------------------------------

To start an HTTP tunnelling service from the Control Center, you first
configure the service by clicking on the ‘Configure’ link. You will see
a screen as shown in :ref:`fig:cc-tunnel`. Enter the HTTP prefix for
your tunnelling service (please refer to :ref:`tunnel` for more
details on setting the prefix). Package details provides details on the
installation package for the tunnelling server. Click on the ‘Start’
button. You can enter a custom name for your HTTP tunnelling service
(e.g. MyApp-Tunnel) and select the machine that you want to start the
service. You can then click on the ‘Submit’ button to start the service.
An appropriate message will be displayed and you will see the service
name included in the ‘Running Services’ section.

Monitoring service performance
------------------------------

Control Center allows you to monitor the performance of the services
that you are running as part of your game network. You can even obtain
detailed network status information for certain services such as Seed
Peer and Overload Peer. The Running Services section displays all the
services that are running in your network along with details of the
machine they are running on and the status of the last ping response. If
you would like to ping your service you can click on the ‘Ping’ link
next to it. The Control Center will ping that service and display the
response in the appropriate column. To obtain more details about a
service, you can click on the ‘Show status’ link next to the service.
The Control Center will display relevant information for that service.
For Badumna specific services such as Seed Peer and Overload Peer, the
Control Center will display connectivity information, the discovery
status. This page also displays a graph that plots the total network
traffic to and from the process. If the service is an overload peer, an
extra graph will be displayed on the screen showing the current number
of connected clients, number of receiving peers, and total incoming and
outgoing updates. Custom services (non-Badumna services) and Dei Server
do not show such details. The only information available for these
services is ping information (whether they are online and active).

.. _fig:cc-9:
   
.. figure:: ../images/ControlCenter/cc-15.png
   :align: center
   :width: 100%
   
   Control Center - Overload status details

.. _fig:cc-6:
   
.. figure:: ../images/ControlCenter/cc-16.png
   :align: center
   :width: 100%
   
   Control Center - Custom service

Starting services on Windows
----------------------------

Please note that when you are starting a service on a Windows machine
for the first time, you will receive a security alert from Windows and
you will have to manually unblock the program and permit the service to
accept connections ( :ref:`fig:unblock-mesg`). Hence if your remote
machine is running Windows, then you will have to unblock the program
manually for the first time. Alternatively, you can configure the
firewall settings on your remote windows machine so that it does not
complain when a new program is trying to access the Internet when
started.

.. _fig:unblock-mesg:
   
.. figure:: ../images/unblock-mesg.jpg
   :align: center
   :width: 45%
   
   Windows Security Alert

Custom services
---------------

Control Center allows you to start any custom services that are specific
to your application. In order to start a custom service you first need
to build the service. You do that by clicking on the ‘Custom Service
Builder’ menu at the top of the page. If this is the first time you have
clicked Custom Service Builder, you will not have any applications
listed under ‘Applications’. Click on the ‘Add Service’ link to add a
new service. You will be asked to enter a name for the service and
choose whether it’s an ‘Arbitration service’ or a ‘Non-Badumna service’.
If your new service is a customised Arbitration Server for your
application, then you should select the type as ‘Arbitration service’.
However if your service is some other application such as a Unity
headless server process then you should select ‘Non-Badumna service’.
Make sure you enter a relevant name for your service (e.g. Guest Book
Arbitrator). Once you click on the ‘Create’ link, your service will
appear in the list of applications in Custom Service Builder.

Before you can start the service, you need to build your service
installation package. Click on the ‘Configure’ link next to your service
name. You will see a screen as shown in :ref:`fig:cc-6`. If this is
the first time you are installing this particular package, then you
should check both the options — ‘Include all contents’ and ‘Include
package extraction tools’. The first option informs the Control Center
that all the files in the package need to be uploaded to the remote
machine as it is the first time the service is being installed. The
second option informs the Control Center that since this is the first
time you are building the service in the Control Center, it should
include all the extraction tools as part of the installation. However,
if you are installing an update to an already installed service, then
you can choose not to include all contents. The Control Center will only
upload the files that have been modified or newly added to this
particular update. Also, for subsequent updates you don’t have to check
the ‘Include package extraction tools’ option, as these tools have
already been included as part of the base installation. This reduces the
package size for the update and makes the installation process faster.

You now have to select the package contents. In order to add files to
the package, you should right-click on your service name as listed
there. There are two ways to add files in the package. You can include
all your package files inside a zip file and upload the zip file or you
can add the files one after the other. Once you have included all the
files that are necessary for the package, you need to tell the Control
Center the executable file that starts your custom service. Right click
on the executable file and select ‘Set as executable file’. You are now
ready to build the package. Click on the ‘Build’ button. You will be
asked to enter a short description of the service. The Control Center
will then build the custom service and you will see the service listed
in the list of ‘Available Services’ section on the main page.

You can click on the ‘Configure’ link next to your new service. You will
have the option to enter any command line arguments that are required to
start the service. You can also view the package details. You can now
start the service by clicking the ‘Start’ link on this page or you can
click the ‘Start’ link next to the custom service on the main page. You
will be asked to give the service a name (e.g. Guestbook - EU Users) and
select a remote machine to start the service. Click on the ‘Submit’
button. The Control Center will start the custom service and display a
message accordingly. You will also see the custom service listed under
the ‘Running Services’ section.

.. _fig:cc-5:
   
.. figure:: ../images/ControlCenter/cc-17.png
   :align: center
   :width: 100%
   
   Control Center - Email notification

Notification
------------

If you want to be notified via email if any of the remote machines or
services are down, you can configure that via the Notification menu
item. Click on the ‘Notification’ menu item on the main page and then
click on the ‘Notification Email’ link. You will see a screen as shown
in :ref:`fig:cc-5`. Check the box next to ‘Send Notification via
email’. Fill the rest of the fields with the following information:

:SMTP server: Enter your SMTP server (e.g. smtp.example.com).
:SMTP port:  Enter your SMTP port number (e.g. 25).
:Username:  Enter the username if that is required by your SMTP server.
:Password:  Enter a password if required by your SMTP server.
:From:  Enter a name that will appear in the from field (e.g. Control Center).
:To:  Enter the recipient email address. Control Center will send the email to this email address.


After you have entered all the details, click on the ‘Submit’ button.
You will now see the message confirming that notification via e-mail is
enabled. 

You have successfully configured your Control Center’s email
notification service.

.. _section-certificate:

Certificate update
------------------

The Control Center uses a certificate-based system to ensure that all
the server processes are connected in a secure network and can only talk
to their Control Center. When you start the Control Center for the first
time, it will generate a unique certificate for your application. You
are asked to enter a pass phrase during the certificate generation
process. It is important that you enter a unique pass phrase and store
it in a secure place. After the certificate is generated, the Control
Center inserts the corresponding public key in the Germ folder. Hence,
when you install Germs on remote machines, they have the public key
required to communicate with the Control Center. The pass phrase for
your certificate cannot be changed. To access the Control Center you
don’t require the pass phrase (just a valid username and password).
However, if you happen to restart the Control Center for some reason,
then it will ask you to enter the pass phrase.

If for some reason you forget the pass phrase then you have to perform
the following steps to start the Control Center and get it reconnected
with the application network.

#. Stop all running services on the remote machines manually. Then, stop
   all germs that host those running services as well.

#. Go to the Control Center folder and look for the Certificate folder.
   Inside this folder, you will find a file with this name -
   certificate.pfx. Delete this file.

#. Now restart the Control Center (refer to :ref:`section:start-cc`
   for more information on starting the Control Center). It will prompt
   you to enter a new passphrase as the old certificate file is not
   found. At this stage the Control Center will not be able to start the
   services. This is because the Control Center has a new certificate
   and is not able to communicate with any of the remote Germ processes.

#. Now you have to repeat the process of copying the Germ folder to all
   the remote machines and then restarting the Germ processes on all the
   remote machines. Please refer to :ref:`section:germ-installation`
   for more information on starting a Germ process. Make sure that you
   stop the existing Germ process before you start the new Germ process.

#. You can now start the services from the Control Center (please refer
   to :ref:`subsec:services` for more information on starting new
   services).

You have now setup the Control Center and reconnected to the existing
network with a new certificate.

.. _section-database:

Using a different database application
--------------------------------------

As you learnt in :ref:`section-introduction`,
to access the Control Center you require a valid username and password.
Control Center stores the user account information in a database. The
default installation of Control Center uses a SQLite database. The
Control Center installation includes the necessary database files that
are required. However, if you want to use a different database
application such as MySQL or Microsoft SQL Server for the Control Center
user account storage you can configure the Control Center accordingly.

We will provide instructions to configure the Control Center to use a
MySQL database application. We assume that you have already installed
MySQL on the machine that will host the database. You will need to
perform the following steps:

#. Install MySQL Connector on the machine that is running the Control
   Center. You can download this package from the
   `MySQL <http://dev.mysql.com/downloads/connector/net/>`_ website.
   This installation includes two dlls (MySql.data.dll and
   MySql.web.dll). Copy these two files into bin folder inside the
   Control Center folder.

#. The Control Center installation comes with a SQL dump file, called
   controlcenter_data.sql. Use this file to generate the MySQL database
   for the Control Center. This file can be found under *App_Data*
   directory inside ControlCenter directory. The command line used to
   generate the database from the SQL file is as follow.

   .. container:: commandline
      
      ``mysql -u user_id -p < controlcenter_data.sql``

#. Edit the web.config file that is located in the Control Center
   folder. You will find the connectionStrings module in the file:

.. sourcecode:: c#

       <connectionStrings>
           <remove name ="LocalSQLite"/>
           <add name="LocalSQLite" connectionString="Data Source=&quot;./App_Data/Provider.sqlite&quot;;Version=3;"/>
       </connectionStrings>

You will have to change this to the following:

.. sourcecode:: c#

       <connectionStrings>
           <remove name ="LocalMySqlServer"/>
           <add name="LocalMySqlServer" connectionString="Data Source=server_name;user id=username;password=password;database=control_center;" providerName="MySql.Data.MySqlClient"/>
       </connectionStrings>

where server_name is the hostname or IP address of the machine that
has MySQL installed, username is a valid username with admin
privileges to access the database and password is the corresponding
password for that username, and if you use controlcenter_data.sql to
generate the database then you should leave the database name to be
control_center.

#. There are three other sections that have to be updated in the
   web.config file. These sections are <profile>, <roleManager>, and
   <membership>. Go through these sections and replace :

   -  SQLiteRoleProvider with MySQLRoleProvider,

   -  SQLiteMembershipProvider with MySQLMembershipProvider,

   -  and SQLiteProfileProvider with MySQLProfileProvider.

You have now configured Control Center to work with a MySQL database.
You can start the Control Center and use it as described in this
section. The rest of the functionality should be the same.

You can use the same steps to configure the Control Center to work with
SQL server or any other SQL compliant database.

.. _section:advanced-connectivity:

Advanced connectivity options
-----------------------------

Connectivity settings are applied to all Badumna services in your game
network. There may be occasions when you may have to change the default
settings of certain parameters within Badumna’s connectivity module.
This section describes those advanced options. :ref:`fig:cc-4` shows a
screen dump of the advanced connectivity options.

.. _fig:cc-4:
   
.. figure:: ../images/ControlCenter/cc-18.png
   :align: center
   :width: 100%
   
   Control Center - Advanced connectivity options

Port forwarding is enabled by default in Badumna and it is recommended
that you keep that enabled. Port forwarding ensures that the service
peer obtains an open NAT connection if it happens to be behind a NAT
device. However, if under certain special circumstances you want to
disable port forwarding for your service, you can disable port
forwarding. Examples of times when you would disable port forwarding
could include during testing or when you are making a specific build for
users that are on a corporate proxy and have their UDP ports blocked.

The Stun server list is a list of servers that Badumna uses during its
Stun process. You can edit this list to add or remove stun servers if
you have concrete information about such servers in the Internet. Once
again, we don’t recommend that you modify this list unless you are
certain of what you are doing. Stun allows Badumna to detect the type of
NAT device a particular peer is connected through and hence apply
appropriate settings so that it is able to communicate with the rest of
the peers. If you happen to test your application on a private LAN
(without Internet connection) or if you are testing HTTP tunnelling
where UDP is blocked, you may choose to disable Stun by leaving the Stun
server list empty.

User administration
-------------------

If you login to the Control Center with the admin account, you will
notice an additional menu item at the top labelled Admin. This allows you
to manage access permissions in the Control Center. If you click on the
‘Admin’ tab, you will notice three links: Manage User, Create User, and
Manage or Create Role. The *Create User* link allows you to add new
users who can access the Control Center. The *Manage or Create Role*
link allows you to add new roles and manage existing roles. The concept
of role is important if you have multiple Control Centers that are
running for different games. Each Control Center will be referred to by
a role. The name of the role must match the friendly name of the
application. Hence if your Control Center friendly name is **MyApp**,
the corresponding role that identifies this Control Center will be
called **MyApp**. A user can be permitted to access a Control Center
running an application with a particular name by assigning the role with
that name to the user. You can assign roles to a user by using the
*Manage User* link.

Therefore, it is important that you create at least one role with the
friendly name of your application. When you add new users who can access
your Control Center, make sure that you include the role in their list
of roles. As an administrator (when you use the *admin* account), you
are allowed to login to any Control Center.

.. [1]
   To start the Germ on a Linux machine, prefix the command line input
   with "mono".

.. [2]
   For more information about the different permissions supported by
   Dei, please refer to :ref:`section:dei-user-accounts`.
   
.. include:: ../apilink-directives.txt