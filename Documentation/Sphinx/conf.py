# -*- coding: utf-8 -*-

import sys, os, re

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
sys.path.append(os.path.abspath('.'))

# -- General configuration -----------------------------------------------------

from sphinx.directives.code import LiteralInclude
# monkey-patch `literalinclude` directive to support spaces.
# see https://bitbucket.org/birkenfeld/sphinx/issue/1123/literalinclude-directive-does-not-support
LiteralInclude.final_argument_whitespace = True

extensions = ['sphinx.ext.todo', 'sphinx.ext.ifconfig', 'sphinx.ext.pngmath', 'sphinxcontrib.spelling', 'extensions', 'sphinx.ext.extlinks', 'table_styling', 'layoutblock']

spelling_lang='en_AU'
spelling_show_suggestions=True
spelling_word_list_filename='spelling_wordlist.txt'

# The version info for the project you're documenting, acts as replacement for
# |version| and |release|, also used in various other places throughout the
# built documents.
#
with open(os.path.join('..', '..', 'VERSION'), 'r') as version_file:
    release = version_file.readline()

version = release.rpartition('.')[0]

if tags.has('sdk'):
   net_api_uri_pattern = '../API/html/{0}.htm'
else:
   net_api_uri_pattern = 'http://www.scalify.com/badumna/api/net/' + release + '/Index.aspx?topic=html/{0}.htm'

   
extlinks = {
    'cppapi': ('http://www.scalify.com/badumna/api/cpp/' + release + '/%s', None)
}

linkcheck_ignore = [
    r'https?://\+:8080/',
    r'https?://localhost:\d+/?',

    # This shouldn't really be ignored, but causes spurious errors because it doesn't
    # allow HEAD requests:
    re.escape(r'http://developer.android.com/sdk/index.html'),
]

if tags.has('cloud') and tags.has('web'):
    templates_path = ['_cloud_badumna_com_templates']
elif tags.has('scalify') and tags.has('web'):
    templates_path = ['_scalify_com_templates']
    html_file_suffix = '.php'
    html_additional_pages = {
        'index': 'index.html'
    }
else:
    templates_path = ['_sdk_templates']

html_static_path = ['_static']

# The suffix of source filenames.
source_suffix = '.rst'

# The master toctree document.
master_doc = 'index'

# General information about the project.
project = u'Badumna'
copyright = u'2012,2013 Scalify Pty Ltd'

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
exclude_patterns = ['_build']

# The name of the Pygments (syntax highlighting) style to use.
pygments_style = 'sphinx'

highlight_language = 'c#'


# -- Options for HTML output ---------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
html_theme_path = ['themes']
html_theme = 'badumna'


# The name for this set of Sphinx documents.  If None, it defaults to
# "<project> v<release> documentation".
html_title = 'Scalify'

# A shorter title for the navigation bar.  Default is the same as html_title.
#html_short_title = 'Documentation'

if tags.has('scalify') and tags.has('web'):
    html_short_title = 'Documentation'
else:
    html_short_title = 'How-to guide'


# The name of an image file (relative to this directory) to place at the top
# of the sidebar.
html_logo = './images/logo_resize.png'

# If false, no module index is generated.
html_domain_indices = False

# If false, no index is generated.
html_use_index = False

# If true, links to the reST sources are added to the pages.
html_show_sourcelink = False

# If true, "Created using Sphinx" is shown in the HTML footer. Default is True.
html_show_sphinx = False

html_add_permalinks = None
