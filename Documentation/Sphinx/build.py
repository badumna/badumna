#!/usr/bin/env python
import sys, os, subprocess
from bs4 import BeautifulSoup
from glob import iglob
import shutil
import urlparse

os.chdir(os.path.dirname(__file__))
scripts_dir = os.path.abspath('../../Third Party/Python27-env/Scripts')
os.environ['PATH'] = scripts_dir + os.pathsep + os.environ.get('PATH', '')

common_args = ['sphinx-build', '-w', 'errors.log', '-n']

args = sys.argv[1:]
if len(args) == 0:
    cmd = 'scalify-web'
    opts = []
else:
    cmd = args[0]
    opts = args[1:]

def run(*args):
    command = common_args + opts + list(args)
    output_dir = args[-1]
    output_file = os.path.join(output_dir, "output.txt")
    if os.path.exists(output_file):
        os.remove(output_file)
    print repr(command)
    p = subprocess.Popen(command)
    retcode = p.wait()
    if os.path.exists(output_file):
        print '-' * 20
        print "Contents of {}:".format(output_file)
        with open(output_file) as f:
            for line in f:
                print >> sys.stderr, line.strip()

    if retcode != 0:
        sys.exit(retcode)

def make_docs(name, source, tags):
    print "Building {} documentation...".format(name)
    doctree = '_build/doctrees/' + name
    output = '_build/' + name
    run(*([x for t in tags for x in ['-t', t]] + ['-c', '.', '-b', 'html', '-d', doctree, source, output]))


def fixup_cloud_web_links(build_dir, site_prefix='docs/'):
    """
    Fix up internal links for SilverStripe.
    
    We need a prefix so we hit our controller. But, we can't just use relative links
    because SilverStripe inserts (and seems to require) a <base> tag,
    which makes the relative links resolve against the base, rather than
    the page's location.
    """

    def adjust(tag, attribute_name):
        url = urlparse.urlparse(tag[attribute_name])

        # Only adjust if it's not an external link or a fragment, and it's not already adjusted
        if len(url.netloc) == 0 and len(url.path) > 0 and not url.path.startswith(site_prefix):
            tag[attribute_name] = urlparse.urljoin(site_prefix, tag[attribute_name])

    for html_file in iglob(os.path.join(build_dir, '*.html')):
        with open(html_file) as html_input:
            html_soup = BeautifulSoup(html_input.read())

        for a_tag in html_soup.select('a'):
            adjust(a_tag, 'href')

        for form_tag in html_soup.select('form'):
            adjust(form_tag, 'action')

        with open(html_file, 'w') as html_output:
            html_output.write(str(html_soup))


docsets = {
    'sdk': 'Manual',
    'scalify-web': '.',
    'cloud-web': 'Manual'
}

other_commands = {
    'linkcheck': 'Checking links',
    'spelling': 'Checking spelling'
}

if cmd in docsets:
    name = cmd
    source = docsets[cmd]
    tags = cmd.split('-')
    make_docs(name, source, tags)

    if cmd == 'cloud-web':
        fixup_cloud_web_links('_build/' + name)

elif cmd in other_commands:
    description = other_commands[cmd]
    print description + "..."
    run('-b', cmd, '-q', '-aE', '-d', '_build/doctrees/' + cmd, '.', '_build/' + cmd)

else:
    assert False, "unknown command: " + cmd
