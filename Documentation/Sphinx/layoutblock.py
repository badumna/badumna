from docutils.parsers.rst import Directive, directives
from docutils import nodes


class frameblock(nodes.General, nodes.Element):
    pass

class FrameBlock(Directive):
    has_content = True
    required_arguments = 1
    final_argument_whitespace = True
    option_spec = {}

    node_class = frameblock

    def run(self):
        self.assert_has_content()
        text = '\n'.join(self.content)
        env = self.state.document.settings.env

        node = self.node_class(text, **self.options)
        node.class_name = self.arguments[0]
        self.add_name(node) 
        self.state.nested_parse(self.content, self.content_offset, node)
        return [node]

def visit_frameblock_html(self, node):
    self.body.append(self.starttag(node, 'div', CLASS='frame-' + node.class_name))

def depart_frameblock_html(self, node):
    self.body.append('</div>\n')

def visit_frameblock_latex(self, node):
    pass

def depart_frameblock_latex(self, node):
    pass

def setup(app):
    ''' Install extension '''

    app.add_node(frameblock,
        html=(visit_frameblock_html, depart_frameblock_html),
        latex=(visit_frameblock_latex, depart_frameblock_latex))

    app.add_directive('frame-block', FrameBlock)