#!/usr/bin/env python
# encoding: utf-8

from docutils import nodes, utils
from sphinx.locale import _
from sphinx.environment import NoUri
import sphinx.roles
from docutils.parsers.rst import states, directives, Directive
from docutils.parsers.rst.roles import set_classes
from sphinx.util.nodes import set_source_info
from pygments.formatters import HtmlFormatter
from pygments import highlight
from pygments.lexers import get_lexer_by_name, TextLexer


def get_config(state, name):
    try:
        value = getattr(state.document.settings.env.app.config, name)
        if not value:
            raise AttributeError
        return value
    except AttributeError, err:
        raise ValueError("'{}' is not set".format(name))


def make_api_nodes(text, link):
    node = nodes.reference('', '', refuri=link, classes=["apilink"])
    node.append(nodes.literal(text, text))
    return node


class ApiLink(Directive):
    required_arguments = 2
    uri_config_name = None

    def run(self):
        if not isinstance(self.state, states.SubstitutionDef):
            error = self.state_machine.reporter.error(
                'Invalid context: the "%s" directive can only be used '
                'within a substitution definition.' % (self.name),
                nodes.literal_block(self.block_text, self.block_text), line=self.lineno)
            return [error]
        
        return [make_api_nodes(self.arguments[0], get_config(self.state, self.uri_config_name).format(self.arguments[1]))]


class NetApiLink(ApiLink):
    uri_config_name = 'net_api_uri_pattern'


def api_role(name, rawtext, text, lineno, inliner, options, content, uri_config_name):
    """Link to Badumna API documentation.

    Returns 2 part tuple containing list of nodes to insert into the
    document and a list of system messages.  Both are allowed to be
    empty.

    :param name: The role name used in the document.
    :param rawtext: The entire markup snippet, with role.
    :param text: The text marked with the role.
    :param lineno: The line number where rawtext appears in the input.
    :param inliner: The inliner instance that called us.
    :param options: Directive options for customization.
    :param content: The directive content for customization.
    """
    title, link = str(text).rsplit(' ', 1)
    ref = get_config(inliner, uri_config_name).format(link)
    return [make_api_nodes(title, ref)], []


def netapi_role(name, rawtext, text, lineno, inliner, options={}, content=[]):
    return api_role(name, rawtext, text, lineno, inliner, options, content, 'net_api_uri_pattern')


def menuselection_bigger_role(name, rawtext, text, lineno, inliner, options={}, content=[]):
    """Modifies the built-in menuselection role to use a bigger arrow character."""
    text = text.replace('-->', u'\N{BLACK RIGHT-POINTING TRIANGLE}')
    return sphinx.roles.menusel_role(name, rawtext, text, lineno, inliner, options, content)


def setup(app):
    """Install the plugin.
    
    :param app: Sphinx application context.
    """
    app.add_config_value('net_api_uri_pattern', None, 'env')
    app.add_role('netapi', netapi_role)
    app.add_directive('apilink', NetApiLink)

    app.add_role('menuselection', menuselection_bigger_role)
