@echo off

rd /s /q html
md html
for %%i in (*.rst) do @"%~dp0\..\..\Third Party\Python27-env\Scripts\rst2html.py" --stylesheet=style.css --link-stylesheet --template=template.html "%%i" "html\%%~ni.html"
copy style.css html
