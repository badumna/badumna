--------------------------------------------
How To Use The AutoTest Testing Architecture
--------------------------------------------


Introduction
============
The AutoTest testing architecture uses the Python scripting language and the `Amazon Elastic Compute Cloud <http://aws.amazon.com/ec2/>`_ to coordinate large numbers of virtual machine instances in order to conduct large-scale tests on the core functions of the Badumna Network Suite.


The AutoTest Solution
=====================
The source code for the testing architecture can be found in the code repository at `Badumna repository <https://dev.badumna.com/svn/Badumna/trunk/Source/Testing/AutoTesting>`_.
Search for AutoTesting Visual Studio solution file. 
The AutoTesting solution contains the following projects:

+--------------------+----------------------------------------------------------------+
| Project names      | Details information                                            |
+====================+================================================================+
|AutoTest            |Common code used by the other projects                          |
+--------------------+----------------------------------------------------------------+
|AutoTestArbitration | Arbitration server to be used in tests                         |
+--------------------+----------------------------------------------------------------+
|AutoTestManager     | Manager application used to coordinate AutoTestPeers           |
+--------------------+----------------------------------------------------------------+
|AutoTestPeer        | Simple application used to test various Badumna functionality. |
+--------------------+----------------------------------------------------------------+
|AutoTestPingPong    | Simple application used to test network latency.               |
+--------------------+----------------------------------------------------------------+
|AutoTestPrivateChat | Simple application used to test private chat.                  |
+--------------------+----------------------------------------------------------------+
|AutoTestScript      | Collection of python scripts used in testing                   |
+--------------------+----------------------------------------------------------------+

The copy_libraries python script in the AutoTest folder copies the Badumna DLLs to a Libraries folder, which is referenced by the projects. 
When the solution is compiled the executable and reference files for AutoTestPeer, AutoTestPingPong, and AutoTestPrivateChat are copied within an AutoTestManager folder in the AutoTestScript folder. 
The executable and reference files for AutoTestArbitration are copied to an AutoTestArbitration folder. 
These folders will be uploaded to an Amazon instance (Creating an Amazon Machine Image (AMI) will be explained later).

Software Installation
=====================
Before the python script can be used the following software need to be installed, and the system variables added:

1. Install Python. (Be careful with the directory path, as I found that Python tended to complain if the directory path has spaces in it.)
2. Set the following two environment variables:
	* `export AWS_ACCESS_KEY_ID=AKIAIUG6EUVJMILAENHA AWS_SECRET_ACCESS_KEY=hC5iPoGflv9+BshHGpbqEJHvMOuiT2DCMP61cLi7`
3. If you use a proxy to access the web, then create a file called ``.boto`` in your home directory (e.g. ``C:\Users\jrandom\.boto``) with contents like::

		[Boto]
		proxy = wwwproxy.student.unimelb.edu.au
		proxy_port = 8000
		proxy_user = joerandom
		proxy_pass = p4ssw0rd

After installing all the required software and adding the system variables, you will be able to run all the python script by either double-clicking on the file, or typing “python filename.py” on the command line.

How to Access Amazon EC2
========================
In order to access the Amazon EC2, you must first get Santosh to create an account for you. After that you can then use the following link to get to the login screen from your browser:

Click on this link to access `Scalify EC2_account <https://www.amazon.com/ap/signin?openid.assoc_handle=aws&openid.return_to=https%3A%2F%2Fsignin.aws.amazon.com%2Foauth%3Fresponse_type%3Dcode%26client_id%3Darn%253Aaws%253Aiam%253A%253A015428540659%253Auser%252Fhomepage%26redirect_uri%3Dhttps%253A%252F%252Fconsole.aws.amazon.com%252Fconsole%252Fhome%253F%2526isauthcode%253Dtrue%26noAuthCookie%3Dtrue&openid.mode=checkid_setup&openid.ns=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0&openid.identity=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0%2Fidentifier_select&openid.claimed_id=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0%2Fidentifier_select&action=&disableCorpSignUp=&clientContext=&marketPlaceId=&poolName=460604284712&authCookies=&pageId=aws.iam&siteState=&accountStatusPolicy=P1&sso=&openid.pape.preferred_auth_policies=MultifactorPhysical&openid.pape.max_auth_age=43200&openid.ns.pape=http%3A%2F%2Fspecs.openid.net%2Fextensions%2Fpape%2F1.0&server=%2Fap%2Fsignin%3Fie%3DUTF8&accountPoolAlias=nicta>`_.

This link lets Amazon know that you are trying to log in using the NICTA account. Hopefully you will only need to use this link once, and next time you go to aws.amazon.com and log into the EC2 Management Console the browser will remember to reference the NICTA account, otherwise bookmars the link above.
When  you have successfully logged in, choose EC2 from the list of web services. From the EC2 Dashboard there is a button to launch instances, as well as various links to such pages as status lists of your launched instances, status lists of spot requests, and your list of personally-created Amazon Machine Images.

Amazon Machine Images
=====================
Amazon instances are just virtual machines which have been booted using an Amazon Machine Image (AMI), a snapshot of the hard drive at the moment the image was created. If you start an Amazon instance, install some software to it, and create an AMI from it, then you can launch many instances using that image, so that all the instances will have the installed software on them. 
This is the most important part of the AutoTesting testing architecture.
Amazon provides you with some basic AMIs, such ones with only Linux or Windows installed. I started an instance using the basic Amazon Linux AMI, installed Mono to it, and thus created the Base Linux Instance. 
This is the starting point for all subsequent AMIs created to test Badumna on Linux.

How To Launch Amazon Instances
==============================
Before we create our own AMIs, we must first know how to launch instances.

1. Press the “Launch Instance” button on the EC2 Dashboard.
2. Select “Classic Wizard” and press “Continue”.
3. Choose an AMI to use and select it by pressing “Select” next to the relevant AMI option. Basic Amazon AMIs are available on the “Quick Start” page, AMIs that we created are available in the “My AMIs” page.
4. Input the number of instances that you would like launch. Select the instance type, which dictates the amount of CPU and memory resources available to each instance. For most tests we use the High-CPU Medium type. Next, choose either “Launch Instances” or “Request Spot Instances”. The first option launches on-demand instances which have a set price and launch immediately. The second option is basically bidding for instances at a price that you set. The current price of instances is shown, and you use that as a guide to how much you want to pay for each instance, which you input into the “Max Price”. Press “Continue”.
5. Continue through the advanced instance options, leaving the options at default.
6. Continue through the tag screen.
7. Choose from the existing key pairs. Public/private key pairs are used to securely connect to our instances. For our test we use the “amazonkey” key pair. Press “Continue”.
8. Choose the security group. Security groups specify which ports are open or blocked on the instances. For our Linux instances we use the “autotest-linux” security group. Press “Continue”.
9. Review your options, and press “Launch”. 

If you chose “Launch Instances” in step 4 your instances should start up immediately. If you chose “Request Spot Instances” your spot requests will be launched, but depending on availability and demand it might take a while before you get your Amazon instances. You can check on the status of your spot requests at the “Spot Requests” page. A status of “Open” means the request is still waiting to be activated, while a status of “Active” means an Amazon instance has been started for that request.
You can cancel a spot request before it is activated by selecting it and pressing the “Cancel” button. Even when spot requests have been activated you should cancel them to clean up the status list.

How to Terminate Amazon Instances
=================================

1. Go to the Instances page of the Amazon EC2.
2. Select the relevant instance, right click on it, and select Terminate

How to Create a New Badumna AMI
===============================

When you are to test a new version of Badumna you will need to do the following to create a new Badumna AMI:

1. Install the new Badumna Network Suite.
2. Build the AutoTest solution using the new Badumna DLLs.
3. Create a user account for the Dei Server, and update the AutoTestManager\AutoTestPeer\DeiConfig.txt file in the AutoTestScript folder with the user details. The python scripting expects an account of username “user”, with password “user_password”.
4. Start an Amazon instance using the Basic Badumna Linux Instance AMI. Note that as this AMI was built from the Basic Amazon Linux instance, if we start it as a micro instance we get to use it for free. As we are just using this instance to upload software to, a micro instance will do just fine.
5. FTP to the Amazon instance using the username “ec2-user” and the AmazonKey.ppk file in the AutoTestScript folder.
6. In the home directory, copy over the following:
	* The AutoTestManager folder from the AutoTestScript folder.
	* The AutoTestArbitration folder from the AutoTestScript folder.
	* The SeedPeer folder from the new Badumna installation.
	* The OverloadPeer folder from the new Badumna installation.
	* The DeiServer folder from the new Badumna installation, making sure the database file containing the newly created user account is present.
	* The HttpTunnel folder from the new Badumna installation.
7. Close the FTP connection. Select the instance from the instance list in Amazon EC2, right click on it and select “Create Image (EBS AMI)”. The name for the new AMI should include the version number of the new Badumna installation for easy reference.
8. From the AMIs page, check on the status of the new AMI. When completed, terminate the instance.

How to Delete an Old Badumna AMI
================================

1. Go to the AMIs page in the Amazon EC2.
2. Select the relevant AMI and press the de-register button.
3. Go to the EBS Snapshots page in the Amazon EC2.
4. Select the EBS snapshot which corresponds to the AMI you have de-registered, and delete it. If you don’t know which is the correct snapshot, you can select all the EBS snapshots and press delete. Don’t worry about deleting wanted snapshots, as only the snapshot which is no longer attached to an AMI will actually be deleted.The command will complain that it was only partly successful, which is fine by us.

AutoTest Test Scripts
=====================

The following is a simple test script:

::

	from apps import *
	if __name__ == "__main__":
		schedule(Seed(0.0, ‘amazon-1’, 80.0, ''))
		schedule(Peer(10.0, ‘amazon-2’, 60.0, 
		    '--num-peers=3 --add-avatar=RandomWalker'))
		go()

The first line imports code from apps.py. This allows us to use the functions schedule() and go(). Schedule() adds an application to an app list, but does not run it. Go() is called after all applications have been scheduled, and actually runs all the applications.

The applications that can be scheduled are:
* Peer : Executes an AutoTestManager, which is used to execute AutoTestPeers.
* Seed : Executes a seed peer.
* Overload : Executes an overload peer.
* Arbitration : Executes an AutoTestArbitration server.
* Dei : Executes a Dei server.
* Tunnel : Executes a tunnel server.

The constructor for each application has four parameters, as such:

::

	App(start time, machine name, time duration, arguments)

Going back to the above test script, the first call to schedule() adds a seed peer to the app list, scheduling for it to execute at 0.0 seconds from the start of the test, executing the seed peer on the Amazon instance with machine name “amazon-1”, for the seed peer to run for 80.0 seconds, with no further arguments. 
The second call to schedule() adds an AutoTestManager to the app list, scheduling for it to execute at 10.0 seconds from start, executing it on the Amazon instance with machine name “amazon-2”, for the AutoTestPeers managed by the AutoTestManager to run for 60.0 seconds, and arguments which specify for the AutoTestManager to start 3 AutoTestPeers and for each peer to create a RandomWalker local avatar. 
Check the source code, or run the relevant application executables with the command line argument “-h” to find out what arguments are available to use.

Note that if there are service peers scheduled, their IP addresses will be automatically passed to the other peers that require them. So with the above test script, the AutoTestManager will be passed the IP address of the seed peer.

Before, we mentioned machine names. In the AutoTestScript folder there is a machinelist.txt file. This is a list of Amazon instances available to use in a test. Each of the instances is given an arbitrary name used to reference the instance. Before a test can be started, the relevant number of Amazon instances should be launched, and then setupmachines.py should be executed. This file contains the following code:

::

	from machines import *
	if __name__ == "__main__":
		populatemachinelist('ec2-user')

The first line imports code from machines.py. 
This allows us to use populatemachinelist(), which contacts Amazon EC2 to obtain a list of all running Amazon instances, adding them to machinelist.txt. 
The parameter “ec2-user” specifies to use that as the username in order to connect to the instances. 
As each instance is added to the list it is assigned a machine name of type “amazon-” followed by a number.

When go() is called a test will commence. 
A folder called “Temp” will be created. 
When each application is finished its relevant test result files will be automatically downloaded to a self-named folder in the Temp folder. When all applications have finished, graph plots will be made from the test results, and the Temp folder will be renamed to “Test” followed by a number.

Note that sometimes a connection with an Amazon instance might fail to establish or be lost. 
This would result in the test running much longer than it should. 
When this happens, the only thing to do is to cancel the python script with Control-C. 
This might require test results files to be manually downloaded from the relevant failed instances. 
Also, because no graphs were plotted since not all applications finished, graphs would need to be manually plotted by writing a script that calls the function plotcharts() from charts.py.







