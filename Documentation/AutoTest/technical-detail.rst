Local Avatar default parameter
==============================

By default the local avatar in auto testing has these parameters:

- Bound top = 0.0
- Bound left = 0.0
- Bound bottom = 250
- Bound right = 300
- Move amount = 40.0
- Rotate amount = 100.0

The format used to set this properties from AutoTestManager is as follow:

::

	--add-avatar="AvatarType;MoveAmount;RotateAmount;BoundTop;BoundLeft;
		BoundBottom;BoundRight"


Creating a new test script
==========================

In order to create a new test script file, you must first add the following line at the top of the file:

::

	from apps import *

This will give you access to the two following functions:

::

	schedule(app)
	go()

The function schedule(app) is called to add an application onto the application list. The function go()
is called to activate all the applications on the application list.

The test script file must also contain the line:

::

	if __name__ == "__main__":

All the code contained in the code block under this line will be run when the test script file is
double-clicked on, or if the file is executed by calling "python testscriptfile.py" on the command-line.

The types of applications that can be sceduled are:

+------------------------------------------------+
+ Peer(time, name, duration, arguments)          +
+------------------------------------------------+
+ Seed(time, name, duration, arguments)          +
+------------------------------------------------+
+ Overload(time, name, duration, arguments)      +
+------------------------------------------------+
+ Arbitration(time, name, duration, arguments)   +
+------------------------------------------------+
+ Dei(time, name, duration, arguments)           +
+------------------------------------------------+


where,	

- time is the amount of time after go() is called for when the application will execute
- name is the name of the machine to deploy the application to
- duration is the amount of time that the application should run for
- arguments are command-line arguments which are to be passed to the application upon execution.


Steps required for starting the test
====================================

This section will give some guidance on how to start the test on EC2-Amazon instance from the beginning.

1. Start EC2-instance, choose the image that created for the Linux auto-test purposes.
2. Start python interactive console (i.e iPython).
3. Run the setupmachines.py by using the following command:
	::

		run setupmachines

   It will populate all the running instances and store it as machineslist.txt.
4. Start the test, remember you can prepare the test script before actually running the test.
   See the normal_test_template.py on how to build one of the normal test script.
   Run the test script with the following command:
   ::

		run test_script

   *Note: replace the 'test_script' with your own test script name.*

Steps for running the script to plot test results manually
==========================================================

This section will explain on how to use Charts2.py to generate plot manually.

Run the following command on any python interactive console.
::
	
	from charts2 import generate_all
	generate_all('Test_directory')
	
