﻿<?xml version="1.0" encoding="utf-8"?>
<topic id="32559833-f5d6-41a9-88c4-743137bdb774" revisionNumber="1">
  <developerConceptualDocument xmlns="http://ddue.schemas.microsoft.com/authoring/2003/5" xmlns:xlink="http://www.w3.org/1999/xlink">
    <!--
    <summary>
      <para>Optional summary abstract</para>
    </summary>
    -->
    <introduction>
      <!-- Uncomment this to generate an outline of the section and sub-section
           titles.  Specify a numeric value as the inner text to limit it to
           a specific number of sub-topics when creating the outline.  Specify
           zero (0) to limit it to top-level sections only.  -->
      <!-- <autoOutline /> -->
      <para>This is the API Documentation for the Badumna Network Suite - vVERSION.
      It includes the API documentation for the Badumna network library (Badumna.dll),
      and associated libraries for use with the Dei authentication server (Dei.dll)and
      the remote game administration Control Center (GermHarness.dll).</para>
    </introduction>
    <!-- Add one or more top-level section elements.  These are collapsible.
         If using <autoOutline />, add an address attribute to identify it
         and specify a title so that it can be jumped to with a hyperlink. -->
    <section address="Section1">
      <title>Features</title>
      <content>
        <!-- Uncomment this to create a sub-section outline
        <autoOutline /> -->
        <sections>
          <section address="Badumna">
          	<title>Badumna</title>
          	<content>
          	  <para>The Badumna network library allows the creation of a peer-to-peer network
          	  for multiplayer applications supporting:</para>
              <list class="bullet">
                <listItem>Automated state synchronisation</listItem>
                <listItem>Chat service with support for proximity and private chat</listItem>
                <listItem>Built-in dead reckoning at network level</listItem>
                <listItem>Support for dynamically created multiple scenes/rooms</listItem>
                <listItem>Built-in file transfer</listItem>
                <listItem>Built-in firewall and NAT traversal</listItem>
                <listItem>Built-in data encryption</listItem>
                <listItem>Congestion avoidance</listItem>
              </list>
            </content>
          </section>
          <section address="Dei">
          	<title>Dei</title>
          	<content>
          	  <para>The Dei library provides support for using the Dei authentication service
          	  in a Badumna-enabled application, including:</para>
              <list class="bullet">
                <listItem>SSL based authentication</listItem>
                <listItem>Support for secure token exchange for identity protection</listItem>
                <listItem>Support for blacklisting users</listItem>
              </list>
            </content>
          </section>
          <section address="GermHarness">
          	<title>GermHarness</title>
          	<content>
          	  <para>The GermHarness library is used to allow custom services for a Badumna network
          	  to be remotely controled by the Badumna Network Suite's Control Center, allowing:</para>
              <list class="bullet">
                <listItem>Starting and stopping services remotely</listItem>
                <listItem>Monitoring status and load on supporting servers</listItem>
              </list>
            </content>
          </section>
        </sections>
      </content>
    </section>
    <section address="Section2">
      <title>More information</title>
      <content>
      <para>
      The
      <externalLink>
        <linkText>Badumna Documentation</linkText>
        <linkAlternateText>Open the Badumna Manual</linkAlternateText>
        <linkUri>http://www.scalify.com/documentation/index.html</linkUri>
      </externalLink>
      contains a series of tutorials showing Badumna API usage in the building of example games.
      </para>
      </content>
    </section>
    <relatedTopics>
      <externalLink>
          <linkText>Scalify web site</linkText>
          <linkAlternateText>Go to Scalify web site</linkAlternateText>
          <linkUri>http://www.scalify.com</linkUri>
      </externalLink>
      <externalLink>
          <linkText>Badumna Documentation</linkText>
          <linkAlternateText>Open the Badumna Manual</linkAlternateText>
          <linkUri>http://www.scalify.com/documentation/index.html</linkUri>
      </externalLink>
    </relatedTopics>
  </developerConceptualDocument>
</topic>