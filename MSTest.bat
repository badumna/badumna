@echo off
:: remove the quotation mark from the arguments
:: http://ss64.com/nt/syntax-esc.html
SET _string=###%1%###
SET _string=%_string:"###=%
SET _string=%_string:###"=%
SET _string=%_string:###=%

set PathToWebRoot=%_string%
mstest %2 %3 %4 %5 %6
