<?php
require_once('recaptcha/recaptchalib.php');
if($_SERVER['SERVER_NAME'] == 'clients.warbweb.com') {
	$request['downloadPage'] = $_SERVER['HTTP_HOST'].'/Scalify/download.php';
} else {
	$request['downloadPage'] = $_SERVER['HTTP_HOST'].'/download.php';
}
switch($_GET['package']) {
	case 'BadumnaTrial' :
		$request['package'] = 'Badumna Trial';
		$request['icon'] = '<img src="images/ico_Trial.png" class="media__img">';
		$request['subject'] = 'Scalify :: Download '.$request['package'];
	break;
	case 'BadumnaIndie' :
		$request['package'] = 'Badumna Indie';
		$request['icon'] = '<img src="images/ico_Indie.png" class="media__img">';
		$request['subject'] = 'Scalify :: Purchase '.$request['package'];
	break;
	case 'BadumnaPro' :
		$request['package'] = 'Badumna Pro';
		$request['icon'] = '<img src="images/ico_Pro.png" class="media__img">';
		$request['subject'] = 'Scalify :: Purchase '.$request['package'];
	break;
}
function form() {
?>
<p>Please fill out the simple form below and press submit. You will be emailed with a link to download Badumna Network Suite.</p>
<form name="form" id="form" method="post" action="">
<div class="left">
	<label>Full name</label>
	<input type="text" class="tf validate[required]" name="name" id="name" value="<?php if(isset($_POST['name'])) echo $_POST['name'];?>" />
</div>
<div class="right">
	<label>E-mail address</label>
	<input type="text" class="tf validate[required,custom[email]]" name="email" id="email" value="<?php if(isset($_POST['email'])) echo $_POST['email'];?>" />
</div>
<div class="clear"></div>
<label>Enter your company name (optional).</label>
<input type="text" class="tf1" name="company" value="<?php if(isset($_POST['company'])) echo $_POST['company'];?>" />
<div class="captcha">
<?php $publickey = "6LfnqLwSAAAAAFPryVhU7oWsg1xChR8_JHbz_mIB";?>
<?php echo recaptcha_get_html($publickey);?>
</div>
<input type="submit" name="Submit" value="Download" class="sendBtn" />
</form>
<?php
}

function sendMail($request) {
 	$toMail = 'santosh.kulkarni@nicta.com.au';

	$fromMail = 'From: '.$_POST['email'].'' . "\r\n" .
		    'X-Mailer: PHP/' .phpversion();

	$content .= 
	'Name: '.$_POST['name'].'
Email: '.$_POST['email'].'
Company: '.$_POST['company'].'
        ';

	$subject = $request['subject'];
	
	if(!mail($toMail, $subject, $content, $fromMail))
		return false;

	if($request['package'] == 'Badumna Trial') {
	
		$new_content .= 
	  'Thank you for signing up for the trial version of Badumna.
You can download the software here: http://www.scalify.com/download.php
Best wishes,
The Scalify team
          ';		
		$new_toMail = $_POST['email'];

		$new_subject = $request['subject'];

		if(!mail($new_toMail, $new_subject, $new_content,"From: \"Scalify Support\" <info@scalify.com>")) {
			return false;
		} else {
				return true;
		}
	}
}

function captcha() {
	$privatekey = "6LfnqLwSAAAAAE_RPumtWDNWW2n1tdnAOt6U0pQu";
	$resp = recaptcha_check_answer ($privatekey,
								$_SERVER["REMOTE_ADDR"],
								$_POST["recaptcha_challenge_field"],
								$_POST["recaptcha_response_field"]);
	
	return (!$resp->is_valid) ? false : true;
}

function proceedForm($request) {
	if(isset($_POST['Submit'])) {
		if(captcha()) {
			if(sendMail($request)) {
				if($request['package'] != 'Badumna Trial') {
					?>
						<script>
						window.onload = function() {
							parent.window.submitPaypal();
						}
						</script>
						<p class="dispalyOk">Thank you for filling out your information.<br>You will be redirected to PayPal</p>
					<?php
				} else {
				echo '<p class="dispalyOk">Thank you for filling out your information.<br>An email has been sent with download information</p>';
				}
				
			} else {
				echo '<p class="dispalyError">Error in sending form</p>';
				form();
			}
		} else {
			echo '<p class="dispalyError">The CAPTCHA wasn\'t entered correctly. Please try again.</p>';
			form();
		}
	} else {
		form();
	}
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
		<title>Download Badumna</title>
		<script type="text/javascript" src="//use.typekit.net/vrv3fgc.js"></script>
    <script type="text/javascript">try{Typekit.load();}catch(e){}</script>
    <link type="text/css" rel="stylesheet" href="css/main.min.css">
    <script>parent.inherit(window)(function(){ $('#form').validationEngine(); });</script>
</head>
<body class="modal-window">
	
	<div class="media">
		<?php echo $request['icon'];?>
		<div class="media__body">
			<h1><?php echo $request['package'];?></h1>
		</div>
	</div>
	<?php proceedForm($request);?>

</body>
</html>
