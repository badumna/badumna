<?php
include_once('_inc/config.php');

$page_title = 'Download';
$section_id = 4;
$page_id = 4;


###############################################################
# Page Password Protect 2.13
###############################################################
# Visit http://www.zubrag.com/scripts/ for updates
############################################################### 
#
# Usage:
# Set usernames / passwords below between SETTINGS START and SETTINGS END.
# Open it in browser with "help" parameter to get the code
# to add to all files being protected. 
#    Example: password_protect.php?help
# Include protection string which it gave you into every file that needs to be protected
#
# Add following HTML code to your page where you want to have logout link
# <a href="http://www.example.com/path/to/protected/page.php?logout=1">Logout</a>
#
###############################################################

/*
-------------------------------------------------------------------
SAMPLE if you only want to request login and password on login form.
Each row represents different user.

$LOGIN_INFORMATION = array(
  'indie0001' => 'badumna0001',
  'indie0002' => 'badumna0002',
  'indie0003' => 'badumna0003'
);

--------------------------------------------------------------------
SAMPLE if you only want to request only password on login form.
Note: only passwords are listed

$LOGIN_INFORMATION = array(
  'root',
  'testpass',
  'passwd'
);

--------------------------------------------------------------------
*/

##################################################################
#  SETTINGS START
##################################################################

// Add login/password pairs below, like described above
// NOTE: all rows except last must have comma "," at the end of line
$LOGIN_INFORMATION = array(
  'indie10001' => 'pH8sT8em',
  'indie10002' => 'QaW9q9bA',
  'indie10003' => 'r2?drAaW',
  'indie10004' => ';Se5atrd',
  'indie10005' => 'ChuGur6T',
  'indie10006' => '+3J9UbsP',
  'indie10007' => '5e7@kEaH',
  'indie10008' => '+uHTJ9=g',
  'indie10009' => '?3buXU6e',
  'custom1019' => '?3buYU6e',
  'indie10010' => 'cus9BUE?'
);

// request login? true - show login and password boxes, false - password box only
define('USE_USERNAME', true);

// User will be redirected to this page after logout
define('LOGOUT_URL', 'http://www.scalify.com');

// time out after NN minutes of inactivity. Set to 0 to not timeout
define('TIMEOUT_MINUTES', 0);

// This parameter is only useful when TIMEOUT_MINUTES is not zero
// true - timeout time from last activity, false - timeout time from login
define('TIMEOUT_CHECK_ACTIVITY', true);

##################################################################
#  SETTINGS END
##################################################################


///////////////////////////////////////////////////////
// do not change code below
///////////////////////////////////////////////////////

// timeout in seconds
$timeout = (TIMEOUT_MINUTES == 0 ? 0 : time() + TIMEOUT_MINUTES * 60);

// logout?
if(isset($_GET['logout'])) {
  setcookie("verify", '', $timeout, '/'); // clear password;
  header('Location: ' . LOGOUT_URL);
  exit();
}

if(!function_exists('showLoginPasswordProtect')) {

  // show login form
  function showLoginPasswordProtect($error_msg) {
    global $header, $footer;

    include_once($header);
?>
        <div id="content">
            <div class="left">
                <img src="images/download.png" width="400" alt="Download">
        		</div>
            <div class="right">
                <form method="post">
                    <p>To download your copy of Badumna please enter the user credentials provided to you.</p>
                    <font color="red"><?php echo $error_msg; ?></font>
                    <?php if (USE_USERNAME) echo '<div><label>Login</label><input type="text" class="tf1" name="access_login" /></div>'; ?>
                    <div>
                      <label>Password</label>
                      <input type="password" name="access_password" />
                    </div>
                    <input type="submit" name="Submit" value="Login" class="sendBtn" />
                </form>
            </div>
            <div class="clear"></div>
        </div>
<?php
    include_once($footer);

    // stop at this point
    die();

    }
}

// user provided password
if (isset($_POST['access_password'])) {

  $login = isset($_POST['access_login']) ? $_POST['access_login'] : '';
  $pass = $_POST['access_password'];
  if (!USE_USERNAME && !in_array($pass, $LOGIN_INFORMATION)
  || (USE_USERNAME && ( !array_key_exists($login, $LOGIN_INFORMATION) || $LOGIN_INFORMATION[$login] != $pass ) ) 
  ) {
    showLoginPasswordProtect("Incorrect login name or password.");
  }
  else {
    // set cookie if password was validated
    setcookie("verify", md5($login.'%'.$pass), $timeout, '/');
    
    // Some programs (like Form1 Bilder) check $_POST array to see if parameters passed
    // So need to clear password protector variables
    unset($_POST['access_login']);
    unset($_POST['access_password']);
    unset($_POST['Submit']);
  }

}

else {

  // check if password cookie is set
  if (!isset($_COOKIE['verify'])) {
    showLoginPasswordProtect("");
  }

  // check if cookie is good
  $found = false;
  foreach($LOGIN_INFORMATION as $key=>$val) {
    $lp = (USE_USERNAME ? $key : '') .'%'.$val;
    if ($_COOKIE['verify'] == md5($lp)) {
      $found = true;
      // prolong timeout
      if (TIMEOUT_CHECK_ACTIVITY) {
        setcookie("verify", md5($lp), $timeout, '/');
      }
      break;
    }
  }
  if (!$found) {
    showLoginPasswordProtect("");
  }

}

?>
