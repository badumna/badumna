<?php
include_once('_inc/config.php');

$page_title = 'Features';
$section_id = 2;
$page_id = 2;

include_once($header);
?>

    <div id="content">
        <div class="left">
        	<ul class="nav" style="margin-top:5px;">
            	<li><img src="images/ico_game_nav.png" width="26" height="27" alt="Game client specific features" /><a href="#i_1" class="scroll">Game client specific features</a></li>
           	  <li><img src="images/ico_server_nav.png" width="20" height="22" alt="Server specific features" /><a href="#i_2" class="scroll">Server specific features</a></li>
           	  <li class="game"><img src="images/ico_monitoring_nav.png" width="23" height="19" alt="Game administration and monitoring features" /><a href="#i_3" class="scroll">Game administration and monitoring features</a></li>
           	  <li><img src="images/ico_features_nav.png" width="21" height="21" alt="Other features" /><a href="#i_4" class="scroll">Other features</a></li>
          </ul>
                     </div>
        <div class="right">
            <p><b>Badumna Pro</b> is a complete technology solution designed for fast and efficient creation of multi-user applications, especially online games and virtual worlds. </p>
    		<p>The Badumna network library provides a scalable service for game state synchronisation and object replication by using a decentralised network. The Pro version also includes
    		applications for additional centralised services including authentication, arbitration, load distribution, and game administration. </p> 
    		<p>Badumna comes with a rich set of documentation, tutorials and many example games complete with source code.</p>
    		<p>Following is a list of all the features supported by the current version of Badumna. </p>
    		
    							<div class="divider"></div>
            <div id="i_1"></div>
            <div class="item">
                <h2><img src="images/ico_game.png" width="54" height="50" alt="Badumna" />Game client specific features</h2>
                <ul>
                    <li>Badumna Scenes</li>
                        <ul>
                            <li>Automated state synchronisation</li>
                            <li>Built-in dead reckoning</li>
                            <li>Remote procedure calls</li>
                            <li>Chat service with support for proximity and private chat</li>
                            <li>Buddy list with fully automated presence service</li>
                            <li>Built-in streaming protocol</li>
                            <li>Distributed validation</li>
                            <li>Distributed NPCs</li>
                        </ul>
                	<li>Badumna Matches</li>
                        <ul>
                            <li>Matchmaking</li>
                            <li>Automated state synchronisation</li>
                            <li>Built-in dead reckoning</li>
                            <li>Remote procedure calls</li>
                            <li>In-match chat</li>
                            <li>Hosted NPCs</li>
                        </ul>

                    <li>Dynamically created multiple scenes/matches</li>
                	<li>User friendly API that matches game functionality</li>
                	<li>Publishing platforms supported: <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>.NET, C++, Unity, XNA game studio,
    				<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Android, iOS<br>
    					</b></li>
                	<li>Game spectators</li>
                	<li>Item support</li>
                	<li>Built-in data encryption</li>
                	<li>Congestion aviodance</li>
                	<li>Visibility across all shards/games</li>
                	<li>Guaranteed connectivity</li>
                	<li>Secure connections</li>
                	<li>Http tunneling</li>
                </ul>
                <div class="divider"></div>
            </div> 
            <div id="i_2"></div>   
            <div class="item">
                <h2><img src="images/ico_server.png" width="40" height="43" alt="Server specific features" />Server specific features</h2>
                	<ul>
                    	<li>Multi-platform: Run on Windows, Mac, Linux</li>
                    	<li>Easy deployment (dedicated, cloud, vps)</li>
                    	<li>Authentication and identity protection</li>
                    	<li>Byte stream interface to minimise network traffic</li>
                    	<li>Built-in serialisation of data</li>
                    	<li>Support for complex game logic</li>
                    	<li>Database abstraction layer</li>
                    	<li>Dynamic rate control for better performance</li>
                    	<li>Off-loading service to keep the network congestion free</li>
                    	<li>Source code available to extend or customise server functionality</li>
                    	<li>Built-in matchmaking service</li>
                    </ul>
                <div class="divider"></div>
            </div>  
            <div id="i_3"></div>  
            <div class="item">
                <h2><img src="images/ico_server.png" width="40" height="43" alt="Game administration and monitoring features" />Game administration and monitoring features</h2>
                	<ul>
                    	<li>Remote game administration</li>
                    	<li>Simplified web interface</li>
                    	<li>Support to remotely install servers application (start, stop and restart services)</li>
                    	<li>Track online users in real-time and obtain vital game statistics.</li>
                    	<li>Support for monitoring the load on hosted services.</li>
                    	<li>Automated failed service notification.</li>
                    </ul>
                <div class="divider"></div>
            </div> 
            <div id="i_4"></div>     
            <div class="item">
                <h2><img src="images/ico_features.png" width="34" height="34" alt="Other features" />Other features</h2>
                	<ul>
                    	<li>Trace build available for better debugging during development</li>
                    	<li>Many sample games complete with source code</li>
                    	<li>Extensive developer documentation</li>
                    	<li>Unity package</li>
                    	<li>XNA game studio package</li>
                    </ul>
            </div>    
    	</div>
        <div class="clear"></div>
    </div>

<?php include_once($footer) ?>