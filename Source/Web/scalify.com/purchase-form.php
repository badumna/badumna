<?php
session_start();
function form() {
?>
<p>Please fill out the simple form below and press submit. You will then be directed to PayPal to complete your purchase of Badumna Network Suite.</p>
<form name="form" id="form" method="post" action="">
<div class="left">
	<label>Full name</label>
	<input type="text" class="tf validate[required]" name="name" id="name" value="<?php if(isset($_POST['name'])) echo $_POST['name'];?>" />
</div>
<div class="right">
	<label>E-mail address</label>
	<input type="text" class="tf validate[required,custom[email]]" name="email" id="email" value="<?php if(isset($_POST['email'])) echo $_POST['email'];?>" />
</div>
<div class="clear"></div>
<label>Enter your company name (optional).</label>
<input type="text" class="tf1" name="company" value="<?php if(isset($_POST['company'])) echo $_POST['company'];?>" />
<label><input class="validate[required]" type="checkbox" name="license" id="license" /> I qualify for the Indie license</label>
<br>
<input type="submit" name="Submit" value="Download" class="sendBtn" />
</form>
<?php
}

function proceedForm(/*$request*/) {
	if(isset($_POST['Submit'])) {
		saveSession();
	} else {
		form();
	}
}


function saveSession() {
	$_SESSION['purchase']['name'] = $_POST['name'];
	$_SESSION['purchase']['email'] = $_POST['email'];
	$_SESSION['purchase']['company'] = $_POST['company'];
	?>
	<script>
	window.onload = function() {
		parent.window.submitPaypal();
	}
	</script>
	
	<p class="dispalyOk">Thank you for filling out your information.<br>You will be redirected to PayPal.</p>
	<?php
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
		<title>Download Badumna</title>
		<script type="text/javascript" src="//use.typekit.net/vrv3fgc.js"></script>
    <script type="text/javascript">try{Typekit.load();}catch(e){}</script>
    <link type="text/css" rel="stylesheet" href="css/main.min.css">
		<script>parent.inherit(window)(function(){ $('#form').validationEngine(); });</script>
</head>
<body class="modal-window">
	
	<div class="media">
		<img src="images/ico_Indie.png" class="media__img">
		<div class="media__body">
			<h1>Badumna Indie</h1>
		</div>
	</div>
	<?php proceedForm(/*$request*/);?>

</body>
</html>