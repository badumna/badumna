<?php

// Globals
$site_root = '/';  // Change this to match the path to the root of the site in the URL

$page_title = '';
$section_id = 0;
$page_id = 0;

/*
  ## Website pages ##

  Page            ID    Section
  - - - - - - - - - - - - - - - - -
  Home            1     1
  Features        2     2
  Showcase        3     3
  Download        4     4
  Documentation   5     5
  Forum           6     6
  Contact         7     7
*/

// Shorthand header and footer includes
$header = dirname(__FILE__) . '/header.php';
$footer = dirname(__FILE__) . '/footer.php';

?>