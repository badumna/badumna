<?php global $site_root, $page_title, $section_id; ?>
<!DOCTYPE html>
<!--[if lte IE 9]><html lang="en" class="no-js ie"><![endif]-->
<!--[if gt IE 9]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <link rel="shortcut icon" href="<?php echo $site_root ?>images/favicon.png">
    <title><?php if($page_title) echo $page_title . ' | '; ?>Scalify</title>
    <script type="text/javascript" src="//use.typekit.net/zub8eno.js"></script>
    <script type="text/javascript">try{Typekit.load();}catch(e){}</script>
    <link type="text/css" rel="stylesheet" href="<?php echo $site_root ?>css/main.min.css">
</head>
<body>

<div class="header">
    <div class="holder">
      <a href="<?php echo $site_root ?>index.php" class="logo notext">Scalify</a>
      <div class="nav">
          <a href="<?php echo $site_root ?>features.php"<?php if($section_id==2) echo ' class="current"' ?>>Features</a>
          <a href="<?php echo $site_root ?>showcase.php"<?php if($section_id==3) echo ' class="current"' ?>>Showcase</a>
          <a href="<?php echo $site_root ?>store.php"<?php if($section_id==4) echo ' class="current"' ?>>Download</a>
          <a href="<?php echo $site_root ?>documentation/"<?php if($section_id==5) echo ' class="current"' ?>>Documentation</a>
          <a href="<?php echo $site_root ?>forum/"<?php if($section_id==6) echo ' class="current"' ?>>Forum</a>
          <a href="<?php echo $site_root ?>contact.php" class="last<?php if($section_id==7) echo ' current' ?>">Contact</a>
      </div>
    </div>
</div>
<div class="holder">