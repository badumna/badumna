
<?php
function truncate($text, $len = 120) {
     if(empty($text)) {
             return "";
     }
     if(strlen($text)<$len) {
                return $text;
     }       
     return preg_match("/(.{1,$len})\s./ms", $text, $match) ? $match[1] ."..." : substr($text, 0, $len)."...";
}

function getNews() {
    $ch = curl_init("http://www.scalify.com/blog/?feed=rss2");
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $output = curl_exec($ch);
    curl_close($ch);

    $objDOM = new DOMDocument();
    @$objDOM->loadXML($output);
    $record = $objDOM->getElementsByTagName("item");
    $i = 0;
    foreach( $record as $value ) {
        $i++;
        $item[$i]['title'] = $value->getElementsByTagName("title")->item(0)->nodeValue;
        $item[$i]['description'] = $value->getElementsByTagName("description")->item(0)->nodeValue;
        $item[$i]['link'] = $value->getElementsByTagName("link")->item(0)->nodeValue;
        $item[$i]['date'] = $value->getElementsByTagName("pubDate")->item(0)->nodeValue;
    }

    global $site_root;
    if(is_array($item)) {
    ?>  
        <p><a href="<?php echo $site_root ?>blog"><?php echo date('d/m/y',strtotime($item[1]['date']))?> - <?php echo $item[1]['title'];?></a>
        <br><?php echo truncate($item[1]['description'],100);?>
    <?php
    } else {
    ?>
        <p>Can not load XML feed...</p>
    <?php
    }
}

global $site_root;

?>

</div> <!-- /.holder -->
<div class="footer">
    <div class="holder">
        <div class="col">
            <div class="media">
                <img src="<?php echo $site_root ?>images/orb.png" alt="Badumna" class="media__img">
                <div class="media__body">
                    <h4 class="cloud-heading">Badumna Cloud</h4>
                    <p>Check out our other networking solutions over on <a href="https://cloud.badumna.com/">badumna cloud</a></p>
                </div>
            </div>
        </div>
        <div class="col">
            <h4>News</h4>
            <?php getNews(); ?>
        </div>
        <div class="col last">
            <h4>Newsletter</h4>
            <p>Join us to start receiving news about new products and services.</p>
        </div>
    </div>
    <div class="holder">
        <div class="col">
            <a href="https://cloud.badumna.com/" class="block-link">
                Visit Badumna Cloud
                <i class="block-link__icon">&#8250;</i>
            </a>
        </div>
        <div class="col">
            <a href="<?php echo $site_root ?>blog" class="block-link">
                View all posts
                <i class="block-link__icon">&#8250;</i>
            </a>
        </div>
        <div class="col last">
            <form action="http://scalify.createsend.com/t/t/s/mluul/" method="post" class="newsletter-signup block-link">
                <input type="email" name="cm-mluul-mluul" id="mluul-mluul" placeholder="Email address">
                <button type="submit" class="block-link__icon">&#8250;</button>
            </form>
        </div>
    </div>
</div>
<div class="sub-footer">
    <div class="holder">
        <div class="copy">&copy; 2010 Scalify. All rights reserved. <a href="<?php echo $site_root ?>terms.php">Terms &amp; Conditions</a> | <a href="<?php echo $site_root ?>privacy.php">Privacy Policy</a></div>
        <div class="nav">
            <a href="<?php echo $site_root ?>features.php">Features</a>|<a href="<?php echo $site_root ?>showcase.php">Showcase</a>|<a href="<?php echo $site_root ?>store.php">Download</a>| <a href="<?php echo $site_root ?>documentation/">Documentation</a>|<a href="<?php echo $site_root ?>forum/">Forum</a>| <a href="<?php echo $site_root ?>contact.php">Contact</a>
        </div>
    </div>
</div>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="<?php echo $site_root ?>js/jquery-1.9.1.min.js"><\/script>')</script>
<script src="<?php echo $site_root ?>js/main.min.js"></script>

<script>
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36519288-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>
    
</body>
</html>