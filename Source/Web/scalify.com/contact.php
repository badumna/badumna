<?php
include_once('_inc/config.php');

$page_title = 'Contact';
$section_id = 7;
$page_id = 7;


require_once('recaptcha/recaptchalib.php');

function form() {

$states = array('AF' => 'Afghanistan','AL' => 'Albania','DZ' => 'Algeria','AS' => 'American Samoa','AD' => 'Andorra','AO' => 'Angola','AI' => 'Anguilla','AQ' => 'Antarctica','AG' => 'Antigua And Barbuda','AR' => 'Argentina','AM' => 'Armenia','AW' => 'Aruba','AU' => 'Australia','AT' => 'Austria','AZ' => 'Azerbaijan','BS' => 'Bahamas','BH' => 'Bahrain','BD' => 'Bangladesh','BB' => 'Barbados','BY' => 'Belarus','BE' => 'Belgium','BZ' => 'Belize','BJ' => 'Benin','BM' => 'Bermuda','BT' => 'Bhutan','BO' => 'Bolivia','BA' => 'Bosnia And Herzegowina','BW' => 'Botswana','BV' => 'Bouvet Island','BR' => 'Brazil','IO' => 'British Indian Ocean Territory','BN' => 'Brunei Darussalam','BG' => 'Bulgaria','BF' => 'Burkina Faso','BI' => 'Burundi','KH' => 'Cambodia','CM' => 'Cameroon','CA' => 'Canada','CV' => 'Cape Verde','KY' => 'Cayman Islands','CF' => 'Central African Republic','TD' => 'Chad','CL' => 'Chile','CN' => 'China','CX' => 'Christmas Island','CC' => 'Cocos (Keeling) Islands','CO' => 'Colombia','KM' => 'Comoros','CG' => 'Congo','CD' => 'Congo, The Democratic Republic Of The','CK' => 'Cook Islands','CR' => 'Costa Rica','CI' => 'Cote D\'Ivoire','HR' => 'Croatia (Local Name: Hrvatska)','CU' => 'Cuba','CY' => 'Cyprus','CZ' => 'Czech Republic','DK' => 'Denmark','DJ' => 'Djibouti','DM' => 'Dominica','DO' => 'Dominican Republic','TP' => 'East Timor','EC' => 'Ecuador','EG' => 'Egypt','SV' => 'El Salvador','GQ' => 'Equatorial Guinea','ER' => 'Eritrea','EE' => 'Estonia','ET' => 'Ethiopia','FK' => 'Falkland Islands (Malvinas)','FO' => 'Faroe Islands','FJ' => 'Fiji','FI' => 'Finland','FR' => 'France','FX' => 'France, Metropolitan','GF' => 'French Guiana','PF' => 'French Polynesia','TF' => 'French Southern Territories','GA' => 'Gabon','GM' => 'Gambia','GE' => 'Georgia','DE' => 'Germany','GH' => 'Ghana','GI' => 'Gibraltar','GR' => 'Greece','GL' => 'Greenland','GD' => 'Grenada','GP' => 'Guadeloupe','GU' => 'Guam','GT' => 'Guatemala','GN' => 'Guinea','GW' => 'Guinea-Bissau','GY' => 'Guyana','HT' => 'Haiti','HM' => 'Heard And Mc Donald Islands','VA' => 'Holy See (Vatican City State)','HN' => 'Honduras','HK' => 'Hong Kong','HU' => 'Hungary','IS' => 'Iceland','IN' => 'India','ID' => 'Indonesia','IR' => 'Iran (Islamic Republic Of)','IQ' => 'Iraq','IE' => 'Ireland','IL' => 'Israel','IT' => 'Italy','JM' => 'Jamaica','JP' => 'Japan','JO' => 'Jordan','KZ' => 'Kazakhstan','KE' => 'Kenya','KI' => 'Kiribati','KP' => 'Korea, Democratic People\'S Republic Of','KR' => 'Korea, Republic Of','KW' => 'Kuwait','KG' => 'Kyrgyzstan','LA' => 'Lao People\'S Democratic Republic','LV' => 'Latvia','LB' => 'Lebanon','LS' => 'Lesotho','LR' => 'Liberia','LY' => 'Libyan Arab Jamahiriya','LI' => 'Liechtenstein','LT' => 'Lithuania','LU' => 'Luxembourg','MO' => 'Macau','MK' => 'Macedonia, Former Yugoslav Republic Of','MG' => 'Madagascar','MW' => 'Malawi','MY' => 'Malaysia','MV' => 'Maldives','ML' => 'Mali','MT' => 'Malta','MH' => 'Marshall Islands','MQ' => 'Martinique','MR' => 'Mauritania','MU' => 'Mauritius','YT' => 'Mayotte','MX' => 'Mexico','FM' => 'Micronesia, Federated States Of','MD' => 'Moldova, Republic Of','MC' => 'Monaco','MN' => 'Mongolia','MS' => 'Montserrat','MA' => 'Morocco','MZ' => 'Mozambique','MM' => 'Myanmar','NA' => 'Namibia','NR' => 'Nauru','NP' => 'Nepal','NL' => 'Netherlands','AN' => 'Netherlands Antilles','NC' => 'New Caledonia','NZ' => 'New Zealand','NI' => 'Nicaragua','NE' => 'Niger','NG' => 'Nigeria','NU' => 'Niue','NF' => 'Norfolk Island','MP' => 'Northern Mariana Islands','NO' => 'Norway','OM' => 'Oman','PK' => 'Pakistan','PW' => 'Palau','PA' => 'Panama','PG' => 'Papua New Guinea','PY' => 'Paraguay','PE' => 'Peru','PH' => 'Philippines','PN' => 'Pitcairn','PL' => 'Poland','PT' => 'Portugal','PR' => 'Puerto Rico','QA' => 'Qatar','RE' => 'Reunion','RO' => 'Romania','RU' => 'Russian Federation','RW' => 'Rwanda','KN' => 'Saint Kitts And Nevis','LC' => 'Saint Lucia','VC' => 'Saint Vincent And The Grenadines','WS' => 'Samoa','SM' => 'San Marino','ST' => 'Sao Tome And Principe','SA' => 'Saudi Arabia','SN' => 'Senegal','SC' => 'Seychelles','SL' => 'Sierra Leone','SG' => 'Singapore','SK' => 'Slovakia (Slovak Republic)','SI' => 'Slovenia','SB' => 'Solomon Islands','SO' => 'Somalia','ZA' => 'South Africa', 'ES' => 'Spain','LK' => 'Sri Lanka','SH' => 'St. Helena','PM' => 'St. Pierre And Miquelon','SD' => 'Sudan','SR' => 'Suriname','SJ' => 'Svalbard And Jan Mayen Islands','SZ' => 'Swaziland','SE' => 'Sweden','CH' => 'Switzerland','SY' => 'Syrian Arab Republic','TW' => 'Taiwan','TJ' => 'Tajikistan','TZ' => 'Tanzania, United Republic Of','TH' => 'Thailand','TG' => 'Togo','TK' => 'Tokelau','TO' => 'Tonga','TT' => 'Trinidad And Tobago','TN' => 'Tunisia','TR' => 'Turkey','TM' => 'Turkmenistan','TC' => 'Turks And Caicos Islands','TV' => 'Tuvalu','UG' => 'Uganda','UA' => 'Ukraine','AE' => 'United Arab Emirates','GB' => 'United Kingdom','US' => 'United States','UY' => 'Uruguay','UZ' => 'Uzbekistan','VU' => 'Vanuatu','VE' => 'Venezuela','VN' => 'Viet Nam','VG' => 'Virgin Islands (British)','VI' => 'Virgin Islands (U.S.)','WF' => 'Wallis And Futuna Islands','EH' => 'Western Sahara','YE' => 'Yemen','YU' => 'Yugoslavia','ZM' => 'Zambia','ZW' => 'Zimbabwe' );

?>
	<form name="contact" id="form" method="post" action="">
		<label>E-mail address</label>
		<input type="text" name="email" id="email" class="tf validate[required,custom[email]]" value="<?php if(isset($_POST['email'])) echo $_POST['email'];?>">
		<label>In which country do you reside?</label>
		<select name="state" id="state" class="select validate[required]">
		<option value="">Choose One:</option>
		<?php
		foreach($states as $key => $val) {
			$selected = (isset($_POST['state']) && $_POST['state'] == $key) ? ' selected' : '';
			echo '<option value="'.$key.'"'.$selected.'>'.$val.'</option>'."\r\n";
		}
		?>
		</select>
		<label>What is the subject of your comment or question?</label>
		<input type="text" name="subject" id="subject" class="tf2 validate[required]" value="<?php if(isset($_POST['subject'])) echo $_POST['subject'];?>" />
		<label>Enter your comments in the box below:</label>
		<textarea name="comments" id="comments" class="ta validate[required]"><?php if(isset($_POST['comments'])) echo $_POST['comments'];?></textarea>
		<div class="captcha">
		<?php
		
		$publickey = "6LfnqLwSAAAAAFPryVhU7oWsg1xChR8_JHbz_mIB"; 
		echo recaptcha_get_html($publickey);
		
		?>
		</div>
		<input type="submit" name="Submit" value="Send" class="sendBtn">
	</form>
<?php
}

function sendMail() {
	$tomail = 'santosh.kulkarni@nicta.com.au';
	
	$fromMail = 'From: '.$_POST['email'].'' . "\r\n" .
                    'X-Mailer: PHP/' . phpversion();

	$subject = '[Badumna Comment] '.$_POST['subject'];
	
	$content .=  
	'From: '.$_POST['email'].'
Country: '.$_POST['state'].'
Comments: '.nl2br($_POST['comments']).'
	';
	
		if(!mail($tomail, $subject, $content, $fromMail)) {
			return false;
		} else {
			return true;
		}
}

function captcha() {
	$privatekey = "6LfnqLwSAAAAAE_RPumtWDNWW2n1tdnAOt6U0pQu";
	$resp = recaptcha_check_answer ($privatekey,
								$_SERVER["REMOTE_ADDR"],
								$_POST["recaptcha_challenge_field"],
								$_POST["recaptcha_response_field"]);
	
	return (!$resp->is_valid) ? false : true;
}

function proceedForm() {
	if(array_key_exists('Submit', $_POST)) {
		if(captcha()) {
			if(sendMail()) {
				echo '<p >The form has been successfully sent</p>';
			} else {
				echo '<p >Error in sending form</p>';
				form();
			}
		} else {
			echo '<p class="dispalyError">The CAPTCHA wasn\'t entered correctly.<br>Please try again.</p>';
			form();
		}
	} else {
		form();
	}
}

include_once($header);
?>

    <div id="content">
        <div class="left">
            <img src="images/about.png" width="410" height="395" alt="Contact">
        </div>
        <div class="right">
            <h1 class="hlv">About</h1>
            <p>Badumna Network Suite is a product of Scalify Pty Ltd, an Australian company that specialises in networking technologies for multiplayer applications.</p>
            <p> Our core technology is a result of several years of research and development at Australia's ICT centre of excellence, <a href="http://www.nicta.com.au/about" target=" blank">NICTA</a>.  We invest in extensive R&amp;D to continuously improve the core technology and introduce new innovations for multi-player networking. </p>

            <div class="divider"></div>
     	
            <h1 class="hlv">Contact</h1>
            <ul>
            	<li>Licensing inquiries: <a href="mailto:licensing@scalify.com">licensing@scalify.com</a><li>
             	<li>General inquiries: <a href="mailto:info@scalify.com">info@scalify.com</a></li>
            </ul>

            <div class="divider"></div>
          
            <h1 class="hlv">Send Us Your Thoughts</h1>
            <?php proceedForm();?>
        </div>
        <div class="clear"></div>
    </div>

<?php include_once($footer) ?>