<?php
include_once('_inc/config.php');

$page_title = 'Download';
$section_id = 4;
$page_id = 4;

include("./password_protect.php");

if (isset($_POST['dotnet']) || isset($_POST['dotnet_x']))
{ 
    // the file made available for download via this PHP file
  downloadInstaller('../protected/Installer/BadumnaPro-dotNET-v3.2.0.zip');
} 

if (isset($_POST['cpp']) || isset($_POST['cpp_x'])) 
{ 
  downloadInstaller('../protected/Installer/BadumnaPro-C++-v3.2.0.zip');
} 

if (isset($_POST['flash']) || isset($_POST['flash_x']))
{
  downloadInstaller('../protected/Installer/Badumna-Flash-v2.3.3.zip');
}

if (isset($_POST['unity']) || isset($_POST['unity_x']))
{
  downloadInstaller('../protected/Installer/BadumnaPro-Unity-v3.2.0.zip');
}

function downloadInstaller($path)
{
  $mm_type="application/octet-stream"; // modify accordingly to the file type of $path, but in most cases no need to do so

  header("Pragma: public");
  header("Expires: 0");
  header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
  header("Cache-Control: public");
  header("Content-Description: File Transfer");
  header("Content-Type: " . $mm_type);
  header("Content-Length: " .(string)(filesize($path)) );
  header('Content-Disposition: attachment; filename="'.basename($path).'"');
  header("Content-Transfer-Encoding: binary\n");

  readfile($path); // outputs the content of the file
  exit();
}

include_once($header);
?>

  <div id="content">
    <div class="left">
        <img src="images/download.png" width="400" alt="Download">
    </div>
    <div class="right">
        <h1>Badumna Pro v3.2</h1>
        <form action='' method='post'>
          
          <input style='width:274px;height:162px' type='image' name='cpp' src='images/download-cplusplus.png' value='cpp' alt='Download C++'>

          <input style='width:274px;height:162px' type='image' name='unity' src='images/download-unity.png' value='unity' alt='Download Unity'>
        </form>
    </div>
    <div class="clear"></div>
  </div>

<?php include_once($footer) ?>
