<?php
include_once('_inc/config.php');

$page_title = 'Showcase';
$section_id = 3;
$page_id = 3;

include_once($header);
?>
    
    <div id="content">
        <div class="left">
        	<ul class="nav" style="margin-top:5px;">
            	<li><img src="images/ico-wander-flower.png" width="22" height="22" alt="Wander" /><a href="#i_2" class="scroll">Wander</a></li>
                <li><img src="images/ico_Lapland_nav.png" width="22" height="22" alt="Polar Heroes" /><a href="#i_2b" class="scroll">Polar Heroes</a></li>
               
           	  <li><img src="images/ico_Vastchat_nav.png" width="22" height="22" alt="Vastchat" /><a href="#i_3" class="scroll">VastPark</a></li>
           	  <li><img src="images/ico_BadumnaSim_nav.png" width="22" height="22" alt="Badumna Sim" /><a href="#i_4" class="scroll">Badumna Sim</a></li>
            	
          </ul>
            &nbsp;
        </div>
        <div class="right">            
            <div id="i_2"></div>
            <div class="item">
                <h2><img src="images/ico-wander-flower.png" width="50" height="50" alt="Wander Icon" />Wander</h2>
              <p>Wander is a non-combat, non-competitive, collaborative multiplayer game. |  <a href="http://www.wanderthegame.com/" target="_blank">Launch website</a></p>
              <p><img src="images/WanderBanner.png" width="558" height="239" alt="Wander" /></p>
              <div class="divider"></div>
            </div> 
            
            
             <div id="i_2b"></div>
            <div class="item">
                <h2><img src="images/ico_Lapland.png" width="50" height="50" alt="Lapland Icon" />Polar Heroes</h2>
              <p>A 3D experience and edutainment environment for children and parents.| <a href="http://www.polarheroes.com" target="_blank">Launch website</a></p>
              <p><img src="images/Lapland.png" width="558" height="239" alt="Lapland" /></p>
              <div class="divider"></div>
            </div> 
            
                
          
             <div id="i_3"></div>   
            <div class="item">
                <h2><img src="images/ico_Vastchat.png" width="50" height="50" alt="Vastchat Icon" />VastPark</h2>
              <p>A virtual worlds platform for enterprise visualisation &amp; collaboration. |  <a href="http://www.vastpark.com/" target="_blank">Launch website</a></p>
              <p><img src="images/Vastchat.jpg" width="557" height="239" alt="Vastchat" /></p>
                <div class="divider"></div>
            </div>  
            
        
            <div id="i_4"></div>  
            <div class="item">
                <h2><img src="images/ico_BadumnaSim.png" width="50" height="50" alt="Badumna Sim Icon" />Badumna Sim</h2>
                <p>A plugin to support OpenSim, an open-source virtual worlds platform. |  <a href="http://opensimulator.org/wiki/Main_Page" target="_blank">Launch website</a></p>
              <p><img src="images/BadumnaSim.jpg" width="557" height="239" alt="Badumna Sim" /></p>
                <div class="divider"></div>
            </div> 

        </div>
        <div class="clear"></div>
    </div>

<?php include_once($footer) ?>
