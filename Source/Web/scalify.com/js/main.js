
$(function(){

  // Placeholder polyfill
  $('input, textarea').placeholder();

  // Modal window for download page forms
  if($('a[rel*=leanModal]').length){
    $('a[rel*=leanModal]').leanModal({ top : 120, closeButton: '.modal__close' });
  }
        
  // Paypal form
  if($('.BadumnaIndie').length){
    $('.BadumnaIndie').live('click', function() {
        $('#paypalForm').submit();
    });  
  }

});