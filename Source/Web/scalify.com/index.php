<?php
include_once('_inc/config.php');

$page_title = 'Badumna Network Suite';
$section_id = 1;
$page_id = 1;

include_once($header);
?>
    <div id="home">

        <div class="homepage-feature cycle-slideshow" 
          data-cycle-slides="> div.homepage-feature__slide"
          data-cycle-timeout="4000"
          data-cycle-pager=".homepage-feature__pager"
          data-cycle-pause-on-hover="true"
          data-cycle-log="false"
        >
          <div class="homepage-feature__slide">
            <div class="homepage-feature__slide-details">
              <h2>Launch real-time multiplayer games in no time with <span>Badumna Cloud</span></h2>
              <p>No network coding. No message limits. Unleash your creativity!</p>
              <p><a href="https://cloud.badumna.com/" class="btn">Learn more</a></p>
            </div>
            <img src="images/slider/tablet.png" class="homepage-feature__slide-shot">
          </div>
          <div class="homepage-feature__slide">
            <div class="homepage-feature__slide-details">
              <h2>A complete framework for <span>multiplayer applications</span></h2>
              <p>Scalify's Badumna Network Suite utilises a decentralized architecture that eliminates major problems with conventional client-server solutions for multiplayer applications - poor scalability and performance; and high operating costs.</p>
            </div>
            <img src="images/slider/ship.png" alt="" class="homepage-feature__slide-shot">
          </div>
          <div class="homepage-feature__slide">
            <div class="homepage-feature__slide-details">
              <h2>A superior business case for <span>game publishers</span></h2>
              <p>"We chose Badumna for Polar Heroes due to the superior scalability and the significant reduction in server and bandwidth requirements that it offers."
              <br>&mdash;<a href="http://www.fantastec.fi/trailer.html" target="_blank"><strong>Juha V&auml;is&auml;nen, CEO Fantastec</strong></a></p>
              <p><a href="/showcase.php" class="btn">View showcase</a></p>
            </div>
            <img src="images/slider/laptop1.png" alt="" class="homepage-feature__slide-shot">
          </div>
          <div class="homepage-feature__slide">
            <div class="homepage-feature__slide-details">
              <h2>Developed and supported by <span>networking experts</span></h2>
              <p>"We are very happy with the product, but more importantly the team. The team has such a great combination of excellent communication, patience, and domain experience."
              <br>&mdash;<a href="" target="_blank"><strong>Dan Castro, Director WishB</strong></a></p>
              <p><a href="/showcase.php" class="btn">View showcase</a></p>
            </div>
            <img src="images/slider/laptop2.png" alt="" class="homepage-feature__slide-shot">
          </div>
          <div class="homepage-feature__slide">
            <div class="homepage-feature__slide-details">
              <h2>The future of <span>multiplayer networking</span></h2>
              <p>"Badumna's mix between traditional client-server and managed peer-to-peer is super interesting and has enormous potential."
              <br>&mdash;<a href="" target="_blank"><strong>Lars "Kroll" Kristensen, Technical Director Unity Studios</strong></a></p>
              <p><a href="/showcase.php" class="btn">View showcase</a></p>
            </div>
            <img src="images/slider/troll.png" alt="" class="homepage-feature__slide-shot troll">
          </div>
          <div class="homepage-feature__pager"></div>
        </div>

        <div class="intro">
          <p>Badumna is a powerful technology suite that helps you create large multi-user applications such as online games and virtual worlds. Badumna uses a unique approach based on a decentralised architecture, rather than client-server.</p>
          <p>This provides almost unlimited scalability, better networking performance, lower operating costs and the freedom to design multiplayer applications that were simply not possible before. </p>
        </div>

        <div class="left">
            <a href="documentation/Articles/why-choose.php" class=""><img src="images/Why-Choose-Badumna-2.jpg" width="515" height="252" alt="Why Choose Badumna" /></a>
        </div>

        <div class="right">
            <a href="documentation/Articles/badumna-in-5.php"><img src="images/5-minutes.jpg" width="515" height="252" alt="Badumna in 5 minutes" /></a>
        </div>

    </div>

<?php include_once($footer) ?>
