<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'blog');

/** MySQL database username */
define('DB_USER', 'blog');

/** MySQL database password */
define('DB_PASSWORD', 'scale_news');

/** MySQL hostname */
define('DB_HOST', 'scalify.db');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'J!xiw^wD9aly+Pq0K_/%haiJ,:?9nfWs1=a_:-+-KyN*}S<2C:DUnefqx4dK1?C_');
define('SECURE_AUTH_KEY',  'qKsxgqau)g+2>qt_T/yQ:hR1*?=&~jw3+ijtWNVoA$Gjkg-R1}UDS+JfGxXnj<Vp');
define('LOGGED_IN_KEY',    'I 5wql3|;4]vQCv&V;Z#F-_<P|5:WIR3^IwQ-7+cE&+*(tO3kbdtU-nB=!sr;BC=');
define('NONCE_KEY',        'fQRDf.}8]BKK)}JzG7(W=ji|FmI.0*$MLY/7Rr-Q{M#OD`)lu_:-ej)%H7}Oj[H#');
define('AUTH_SALT',        'LNVS_z_ydM~a+Eze[ I]w[^~WYC3:]<Ve?U:reG3l-na4|#IR55hU_OF@j^Wb1m7');
define('SECURE_AUTH_SALT', 'W=Auu9+P#v7 :*aK7uMy%LsFV!3n-<q&pl`qt&/n6M+ V}M8yp6ra{p;I2TH~Ig]');
define('LOGGED_IN_SALT',   'B+E$uELX+ugO@ApGb HKj|CYJ G@l(Av4g|vvZlo1?<7e]ghZ$S.Sk[GphSFMz|g');
define('NONCE_SALT',       '<@UdZ fvJ.T_Y[w7Bt3 j;kOO3A|wkfJ|[.jSEwQA=PPwfky*$oC.xwT@B_+CDN4');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress.  A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de.mo to wp-content/languages and set WPLANG to 'de' to enable German
 * language support.
 */
define ('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);


/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
