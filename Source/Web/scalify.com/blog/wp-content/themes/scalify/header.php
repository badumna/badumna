<!DOCTYPE html>
<!--[if lte IE 9]><html class="no-js ie"  <?php language_attributes(); ?>><![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js"  <?php language_attributes(); ?>> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <title><?php wp_title('|', true, 'right'); ?> <?php bloginfo('name'); ?></title>

    <script type="text/javascript" src="//use.typekit.net/zub8eno.js"></script>
    <script type="text/javascript">try{Typekit.load();}catch(e){}</script>

    <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>">

    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

    <link rel="shortcut icon" href="<?php bloginfo('stylesheet_directory') ?>/images/favicon.png">

    <?php //if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); ?>
    <?php wp_head(); ?>
</head>
<body>

<div class="header">
    <div class="holder">
      <a href="/index.php" class="logo notext">Scalify</a>
      <div class="nav">
          <a href="/features.php">Features</a>
          <a href="/showcase.php">Showcase</a>
          <a href="/store.php">Download</a>
          <a href="/documentation/">Documentation</a>
          <a href="/forum/">Forum</a>
          <a href="/contact.php" class="last">Contact</a>
      </div>
    </div>
</div>
<div class="holder">
    <div id="content">
        <div class="left">
            <img src="<?php bloginfo('stylesheet_directory') ?>/images/blog.png" width="410" height="443" alt="About">
        </div>
        <div class="right">
