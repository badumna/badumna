<?php get_header(); ?>

	<?php query_posts('showposts=100');?>

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

	<h1 class="hlv title"><span><?php the_time('j/m/y') ?> - </span><?php echo get_the_title(); ?></h1>
	<p>

	<?php the_content(); ?> 
	</p>
	<div class="divider"></div>
	<?php endwhile; else: ?>

	<p>Sorry, no posts matched your criteria.</p>

	<?php endif; ?>

<?php get_footer(); ?>
