<?php
include_once('_inc/config.php');

$page_title = 'Terms and Conditions';

include_once($header);
?>

    <div id="content">
        <div class="left">
            <img src="images/Warrior.png" width="420" alt="about">
    	</div>
        <div class="right">
        	<h1 class="hlv">Terms and Conditions</h1>
    		<h3>General</h3>
    		<p>These terms, together with any additional terms, notices and  disclaimers (together these Terms of Use) contained elsewhere on this website, comprise the conditions on which you may  have access to the SCALIFY Website and use of content and other services  on the SCALIFY Website. Your continued access and use of the SCALIFY  Website constitutes your ongoing acceptance and acknowledgment of these  Terms of Use, as amended from time to time. If you object to any of  these conditions, you must immediately discontinue your access to and  use of the SCALIFY Website.</p>
            <p>These Terms of Use are governed by the law in force in Victoria, Australia. Both you and SCALIFY irrevocably submit to the non-exclusive  jurisdiction of the courts of New South Wales, Australia and courts of  appeal from them.</p>
            <h3>Disclaimers</h3>
            <p>SCALIFY is not liable to you or anyone else for any loss in connection with use of the SCALIFY Website or a linked website.</p>
            <p>This disclaimer is not restricted or modified by any of the following specific warnings and disclaimers.</p>
            <p>SCALIFY is not liable to you or anyone else if interference with or  damage to your computer systems occurs in connection with use of the  SCALIFY Website or a linked website. You must take your own precautions  to ensure that whatever you select for your use from the SCALIFY Website  is free of viruses or anything else (such as worms or trojan horses)  that may interfere with or damage the operations of your computer  systems.</p>
            <p>SCALIFY may, from time to time, change or add to the SCALIFY Website  (including these Terms of Use) or the information, products or services  without notice. However, SCALIFY does not undertake to keep the SCALIFY  Website updated. SCALIFY is not liable to you or anyone else if errors  occur in the information on the SCALIFY Website or if that information is  not up-to-date.</p>
            <p>To the extent permitted by applicable law, all representations,  warranties and other terms are excluded. You must ensure that your  access to the SCALIFY Website is not illegal or prohibited by laws which  apply to you or in your location.</p>
            <h3>Linked Websites</h3>
            <p>The SCALIFY Website may contain links to other websites. Those links  are provided for convenience only and may not remain current or be  maintained.</p>
            <p>Links to linked websites should not be construed as any endorsement,  approval, recommendation, or preference by us of the owners or  operators of those sites, or for any information, products or services  referred to on those sites.<br />            <br />            In providing such links, SCALIFY does not accept responsibility for, or endorse the content or condition of, any linked site.</p>
        </div>
        <div class="clear"></div>
    </div>
            
<?php include_once($footer) ?>