<?php
include_once('_inc/config.php');

$page_title = 'Store';
$section_id = 4;
$page_id = 4;

include_once($header);
?>

    <div id="content">
        <div class="left">
        	<ul class="nav" style="margin-top:5px;">
            	<li><img src="images/ico_Trial_nav.png" width="22" height="22" alt="Badumna Trial" /><a href="#i_1" class="scroll">Badumna Pro (Trial)</a></li>
           	  <li><img src="images/ico_Indie_nav.png" width="22" height="22" alt="Badumna Indie" /><a href="#i_2" class="scroll">Badumna Pro (Indie)</a></li>
           	  <li><img src="images/ico_Pro_nav.png" width="22" height="22" alt="Badumna Pro" /><a href="#i_3" class="scroll">Badumna Pro</a></li>
          </ul>
            &nbsp;
        </div>
        <div class="right">
            <div id="i_1"></div>
            <div class="itemStore">
                <h2><img src="images/ico_Trial.png" width="50" height="50" alt="Badumna Trial Icon" />Badumna Pro (Trial)</h2>
                <a href="#download-trial" rel="leanModal"><img src="images/trial.png" width="202" height="147" alt="Badumna Trial" align="left"></a>
            <p class="store"><b>Badumna Pro (Trial) –</b> is intended for evaluation purposes only. It includes all the features of Badumna Network Suite. However, entities will timeout after 60 minutes of game play.</p>
               	<div class="clear"></div>
                <div class="divider"></div>
            </div> 
            <div id="i_2"></div>   
            <div class="itemStore">
    			<h2><img src="images/ico_Indie.png" width="50" height="50" alt="Badumna Indie Icon" />Badumna Pro (Indie)</h2>
    			<a href="#download-indie" rel="leanModal"><img src="images/Indie.png" width="202" height="151" alt="Badumna Indie" align="left" style="margin-bottom: 20px;"></a>
            <p class="store"><b>Badumna Pro (Indie) –</b> is available to all independent developers. It is offered in three different tiers. 
              Tier-1: $200 (max 500CCU), Tier-2: $1000 (max 1000CCU), and Tier-3: $4000 (max 5000CCU). You can switch to a higher tier by paying the difference.
              Indie license is offered as a once only license fee per application.</p>
    		  <p class="store" style="color:#ff1010;">You need to satisfy the criteria for the Indie license (less than five employees and $200K annual revenue) in order to purchase this version of Badumna.</p>
                <div class="clear"></div>
                <div class="divider"></div>
            </div>  
            <div id="i_3"></div>  
            <div class="itemStore">
                <h2><img src="images/ico_Pro.png" width="50" height="50" alt="Badumna Pro Icon" />Badumna Pro</h2>
              <p><b>Badumna Pro -</b>  is the full suite of Badumna components, enabling developers to build and deploy large scale multiplayer applications. 
    			Badumna Pro is licensed via a Pay-As-You-Grow monthly subscription, based on the peak concurrent users and the type of application. </p>
    		 <p>We offer different pricing schedules for MMOs, social games, mobile games and non-gaming apps.
    		 This helps us ensure that your operating costs with a Badumna-powered solution will always be much lower than a conventional client-server solution.</p>
    		 <p>If you are considering migrating an existing game to Badumna, tell us what you are paying today for the networking software, server and bandwidth and we’ll give you an offer that smashes it! </p>
    		 <p>Unlike other solutions, Badumna pricing is per application, not per server. </p>
    			<p><a href="contact.php">Contact us to inquire about a Badumna Pro subscription</a></p> 
                <div class="clear"></div>
                <div class="divider"></div>
            </div> 
             
        </div>
        <div class="clear"></div>
    </div>

    <div id="download-trial" class="modal">
        <div class="modal__header">
            <h3>Download Badumna Trial</h3>
            Free of charge
            <a class="modal__close" href="#"></a>
        </div>
        <iframe id="download-trial" src="download-form.php?package=BadumnaTrial" width="590" height="520" frameborder="0"></iframe>
    </div>

    <div id="download-indie" class="modal">
        <div class="modal__header">
            <h3>Download Badumna Indie</h3>
            US$200
            <a class="modal__close" href="#"></a>
        </div>
        <iframe name="download-indie" src="purchase-form.php?package=BadumnaIndie" width="590" height="370" frameborder="0"></iframe>
    </div>

    <script>
    function submitPaypal() {
        document.getElementById('paypalForm').submit();
    }
    </script>

	<form id="paypalForm" name="paypalForm" action="https://www.paypal.com/cgi-bin/webscr" method="post">
		<input type="hidden" name="cmd" value="_s-xclick">
		<input type="hidden" name="hosted_button_id" value="263TTQDDNAGB2">
		<!--<input type="image" src="https://www.paypal.com/en_AU/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online.">-->
		<img alt="" border="0" src="https://www.paypal.com/en_AU/i/scr/pixel.gif" width="1" height="1">
	</form>

<?php include_once($footer) ?>