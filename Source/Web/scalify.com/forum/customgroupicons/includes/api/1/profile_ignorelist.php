<?php
/*======================================================================*\
|| #################################################################### ||
|| # vBulletin 4.1.7 - Licence Number VBFF0F72A8
|| # ---------------------------------------------------------------- # ||
|| # Copyright �2000-2011 vBulletin Solutions Inc. All Rights Reserved. ||
|| # This file may not be redistributed in whole or significant part. # ||
|| # ---------------- VBULLETIN IS NOT FREE SOFTWARE ---------------- # ||
|| # http://www.vbulletin.com | http://www.vbulletin.com/license.html # ||
|| #################################################################### ||
\*======================================================================*/
if (!VB_API) die;

$VB_API_WHITELIST = array(
	'response' => array(
		'HTML' => array(
			'ignorelist' => array(
				'*' => array(
					'container', 'friendcheck_checked',
					'user' => array(
						'userid', 'avatarurl', 'avatarwidth', 'avatarheight',
						'username', 'type', 'checked'
					)
				)
			),
			'ignore_username'
		)
	),
	'show' => array(
		'ignorelist'
	)
);

/*======================================================================*\
|| ####################################################################
|| # Downloaded: 18:40, Tue Oct 25th 2011
|| # CVS: $RCSfile$ - $Revision: 35584 $
|| ####################################################################
\*======================================================================*/