<?php
/*======================================================================*\
|| #################################################################### ||
|| # vBulletin 4.0.7 - Licence Number VBFF0F72A8
|| # ---------------------------------------------------------------- # ||
|| # Copyright �2000-2010 vBulletin Solutions Inc. All Rights Reserved. ||
|| # This file may not be redistributed in whole or significant part. # ||
|| # ---------------- VBULLETIN IS NOT FREE SOFTWARE ---------------- # ||
|| # http://www.vbulletin.com | http://www.vbulletin.com/license.html # ||
|| #################################################################### ||
\*======================================================================*/

error_reporting(E_ALL & ~E_NOTICE);

// This is a template file for creating new upgrade scripts. To use it,
// simply alter the following definitions.

// You also need to create the appropriate array in
// $upgrade_phrases['upgradeX.php']['steps'] in the upgrade_language_en.php
// for the step titles and add the new version to the array of
// versions in upgrademain.php

// After that write the code for the steps.

define('THIS_SCRIPT', 'upgrade_406.php');
define('VERSION', '4.0.6');
define('PREV_VERSION', '4.0.5');

$phrasegroups = array();
$specialtemplates = array();


// require the code that makes it all work...
require_once('./upgradecore.php');

// #############################################################################
// welcome step
if ($vbulletin->GPC['step'] == 'welcome')
{
	if ($vbulletin->options['templateversion'] == PREV_VERSION)
	{
		echo "<blockquote><p>&nbsp;</p>";
		echo "$vbphrase[upgrade_start_message]";
		echo "<p>&nbsp;</p></blockquote>";
	}
	else
	{
		echo "<blockquote><p>&nbsp;</p>";
		echo "$vbphrase[upgrade_wrong_version]";
		echo "<p>&nbsp;</p></blockquote>";
		print_upgrade_footer();
	}
}

// #############################################################################
if ($vbulletin->GPC['step'] == 1)
{
	// fix imgdir_gradients stylevar in non-MASTER styles VBIV-8052
	$stylevar_result = $db->query_read("
		SELECT *
		FROM " . TABLE_PREFIX . "stylevar
		WHERE stylevarid = 'imgdir_gradients'
	");

	$stylevars = array();

	while ($stylevar = $db->fetch_array($stylevar_result))
	{
		if ($stylevar['styleid'] == -1)
		{
			continue;
		}

		$value = unserialize($stylevar['value']);
		if (key($value) == 'string')
		{
			$stylevars[] = $stylevar;
		}
	}

	$total = count($stylevars);

	if ($total > 0)
	{
		$i = 1;
		foreach ($stylevars AS $stylevar)
		{
			$value = unserialize($stylevar['value']);
			$new_value = array('imagedir' => $value['string']);
			$db->query_write("
				UPDATE " . TABLE_PREFIX . "stylevar
				SET value = '" . $db->escape_string(serialize($new_value)) . "'
				WHERE
					stylevarid = 'imgdir_gradients'
						AND
					styleid = " . intval($stylevar['styleid']) . "
			");
			$upgrade->show_message(sprintf($upgrade_phrases['upgrade_406.php']['updating_stylevars_in_styleid_x_y_of_z'], $stylevar['styleid'], $i, $total));
			++$i;
		}
	}
	else
	{
		$upgrade->show_message($upgrade_phrases['upgrade_406.php']['no_stylevars_need_updating']);
	}

	$upgrade->execute();
}

// #############################################################################
// FINAL step (notice the SCRIPTCOMPLETE define)
if ($vbulletin->GPC['step'] == 2)
{
	// tell log_upgrade_step() that the script is done
	define('SCRIPTCOMPLETE', true);
}

// #############################################################################

print_next_step();
print_upgrade_footer();

/*======================================================================*\
|| ####################################################################
|| # Downloaded: 19:24, Mon Oct 4th 2010
|| # CVS: $RCSfile$ - $Revision: 37905 $
|| ####################################################################
\*======================================================================*/
?>
