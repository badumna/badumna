<?php
include_once('_inc/config.php');

$page_title = 'Download';
$section_id = 4;
$page_id = 4;

include_once($header);
?>

    <div id="content">
        <div class="left_modified">
            <img src="images/download.png" width="400" alt="Download">
    	</div>
        <div class="right_modified">
            <h1>Badumna Pro v3.2 (Trial)</h1>
    		
    		<a href="installers/BadumnaPro-C++-v3.2.0-Trial.zip"><img src="images/download-cplusplus.png" width="274" height="162" alt="Download C++" /></a>

    		<a href="installers/BadumnaPro-Unity-v3.2.0-Trial.zip"><img src="images/download-unity.png" width="274" height="162" alt="Download Unity" /></a>
        </div>
        <div class="clear"></div>
    </div>

<?php include_once($footer) ?>