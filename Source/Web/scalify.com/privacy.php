<?php
include_once('_inc/config.php');

$page_title = 'Privacy Policy';

include_once($header);
?>

    <div id="content">
        <div class="left">
            <img src="images/Wizard.png" width="420" alt="about">
    		</div>
        <div class="right">
            <h1 class="hlv">Privacy Policy</h1>
      			<p>SCALIFY is committed to protecting your privacy.
      			<h3>1. What is personal information</h3>
      			<p>
      			Personal information is information that personally identifies an individual. Personal information may include a person's  name, mailing address, telephone number, title, employer, interests and e-mail address. 
      			</p>
      			<h3>2. How we collect personal information?</h3>
      			<p>
      			SCALIFY collects personal information in a number of ways including
      			<ul>
      			<li>directly from you, when you provide information by phone or in a form,  when you submit your personal details through SCALIFY's website or use our products;</li>
      			<li>from third parties such as SCALIFY's related companies, partners and members, credit reporting agencies or your representatives; or</li>
      			<li>from publicly available sources of information;</li>
      			</ul>
      			<p>Where you interact with us via our web site, we may automatically collect the IP address, internet service provider used to contact our site and various other parameters of the connection(s) made to our site. This information is used to administer and improve the performance of our site. Some of web pages also use "cookies". Most browsers allow you to control these "cookies". You may choose to access the site without use of  "cookies" but it may reduce the user experience and require you re-enter certain information.</p>

      			<p>Where we collect personal information about you from a third party source, we will take reasonable steps to contact you and ensure that you are aware of the purposes for which we are collecting your personal information and the organisations to which we might disclose your personal information.</p>
      			</p>
                		<h3>3. Notice of collection and how we use your personal information</h3>
      			<p>
      			Where SCALIFY collects personal information directly from you we will give you notice of our intended use for that information through use of a "purpose statement". These purposes include, but are not limited to, the following:
      			<ul>
      			<li>to provide information, goods or services to you;  </li>
      			<li>to promote and market SCALIFY, including its people and activities, to you;</li>
      			<li>for research purposes;</li>
      			<li>to provide statistical information (not including individual identities) to government and other organisations;</li>
      			<li>to assess, receive, manage or terminate receipt of goods or services from you or the organisation which employs you;</li>
      			<li>to assess, manage and terminate joint ventures and collaborative projects;</li>
      			<li>as required by law;</li>
      			<li>purposes related or ancillary to the main reason we collect it;</li>
      			<li>other purposes which are related to (or, in the case of sensitive information, directly related to) the primary purpose of collection which you would reasonably expect.</li>
      			</ul>
      			<p>
      			<h3>4. When we disclose your personal information</h3>
      			<p>
      			<p>We may transfer personal information to organisations outside SCALIFY including related companies of SCALIFY, SCALIFY members and partners, third parties who assist us in achieving the purpose or to whom we have outsourced various business functions.</p>

      <p>In some cases, these third parties may reside outside Australia. In each case we will take reasonable steps to ensure that these third parties protect your personal information in a similar way to SCALIFY.</p>

      <p>In addition, SCALIFY may disclose your personal information to:</p>
      			<ul>
      			<li>your authorised representatives or legal advisors (e.g. when requested by you to do so);</li>
      			<li>credit-reporting and fraud-checking agencies;</li>
      			<li>SCALIFY's professional advisors including its accountants, auditors and lawyers; and</li>
      			<li>Government and regulatory authorities and other organisations, as required or authorised by law.</li>
      			</ul>
      <p>We do not disclose personal information we collect to third parties for the purposes of allowing them to direct market their products and services.</p>

      <p>If you do not provide information which we request, then we may not be able to or may not be willing to fulfil the applicable purpose of collection. For example, we may not provide you with appropriate information or services or may not consider an application or proposal by you.</p>
      		</p>
      		
      		<h3>5. Gaining access to information we hold about you</h3>
      		<p>
      		<p>Subject to any exceptions allowed by law, SCALIFY will, on request, provide you with access to the personal information we hold about you.</p>

      <p>Your request to obtain access will be dealt with in a reasonable time.</p>

      <p>If SCALIFY refuses to provide you with access to the information, SCALIFY will provide you with reasons for the refusal.</p>
      		</p>

      		<h3>6. Keeping your personal information up-to-date</h3>
      		<p>
      <p>		We take reasonable steps to ensure that your personal information is accurate, complete, and up-to-date whenever we collect or use it.</p>

      <p>If you find that the personal information we hold about you is inaccurate, incomplete or out-of-date, please contact us immediately and we will take reasonable steps to correct this information, or if necessary, we will discuss alternative action with you.</p>
      		</p>

      		<h3>7. Security of your personal information</h3>
      		<p>
      <p>		We will take reasonable steps to protect any personal information that we hold from misuse, loss, unauthorised access, modification or disclosure.</p>

      <p>Your personal information may be stored either in hardcopy documents, as electronic data, or in SCALIFY's software or systems.</p>

      <p>SCALIFY will take reasonable steps to destroy or permanently de-identify personal information that is no longer needed for the purpose for which the information was collected.</p>
      		</p>

      		<h3>8. Our privacy practices in relation to emails</h3>
      		<p>
      		SCALIFY's IT Department logs and stores email messages by capturing each email log file on the mail server. The details captured are date, sender, receiver, subject, size of email message, routing information and group that sender belongs to. This information is then stored on a server for a maximum of 10 years, after which time it is deleted. This information may be "de-identified" (so that it can no longer be used to identify any individual) and then used in its de-identified form for internal research purposes and aesthetic purposes approved through applicable SCALIFY processes. Further, if an investigation is necessary, SCALIFY, a law enforcement agency or other government agency may inspect the above information for the purposes of the investigation. We will not otherwise use or disclose the above information except in accordance with applicable laws, including the Privacy Act 1988 (Cth).
      		</p>

      		<h3>9. Unsolicited Commercial Email - SPAM </h3>
      		<p>
      		<p>SCALIFY does not send commercial electronic messages unless the relevant electronic account holder consents or SCALIFY is otherwise permitted to send them under the Spam Act 2003 (Cth). SCALIFY includes information about SCALIFY and an unsubscribe facility in commercial electronic messages it sends.</p>

      <p>Where you provide your email address to SCALIFY using the SCALIFY Website (for instance, by sending an electronic message or by completing a web form), SCALIFY will only use your email address for the purposes for which you provided it or agreed it may be used.  SCALIFY does not use any address harvesting software. Nor does it use any lists directly or indirectly derived from use of address harvesting software.</p>
      		</p>

      		<h3>10. Employee Privacy </h3>
      		<p>
      <p>		Employees of SCALIFY may request access to their personal information, such as contact details, for the purpose of keeping that information up to date.</p>

      <p>Employees records are generally exempt from the Privacy Act 1988 (Cth) and employees do NOT have the right to access, evaluate or amend their employee records such as performance reviews and confidential records.</p>

      <p>SCALIFY must deal with employee records in a way that is related to their employment relationship and may disclose personal information on all its current and former employees to third parties who assist us in achieving our legitimate business objectives in relation to employees or to whom SCALIFY has outsourced business functions, including a superannuation fund to secure superannuation benefits for employees.</p>

      <p>SCALIFY will comply with the Privacy Act 1988 (Cth) and the National Privacy Principles in relation to contractors, sub-contractors or prospective employees.</p>
      		</p>

      		<h3>11. Sensitive information </h3>
      		<p>
      		All sensitive information will be collected, used and disclosed in accordance with the Privacy Act 1988 (Cth) and the National Privacy Principles (NPP 10).
      		</p>

      		<h3>12. How to opt out of receiving communications from SCALIFY? </h3>
      		<p>
      		You can opt-out of a mailing list or other information stream by sending an email to <a href="mailto:webmaster@SCALIFY.com">webmaster@SCALIFY.com</a>.
      		</p>

      		<h3>13. How to contact us?</h3>
      		<p>
      <p>		If you wish to gain access to your personal information or make a complaint about a breach of your privacy or if you have a query on how your personal information is collected or used, or any other query relating to SCALIFY's Privacy Policy and Disclosure Statement, please contact our Privacy Officer:</p>

      <p>Email: <a href="mailto:privacy@SCALIFY.com">privacy@SCALIFY.com</a></p>

      <p>We will respond to your query or complaint as soon as possible and will try to resolve any complaint as soon as possible.</p>
      		</p>
        </div>
        <div class="clear"></div>
    </div>
            
<?php include_once($footer) ?>