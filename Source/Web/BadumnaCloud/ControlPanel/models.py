from __future__ import absolute_import
from django.db import models
from django.conf import settings
from django.core.exceptions import PermissionDenied
from django.core.mail import send_mail
from django.core.urlresolvers import reverse
from django.contrib.auth.models import BaseUserManager, AbstractBaseUser

import logging
import time
import random
import string
from .util.datetime import to_unix_time, from_unix_time
LOGGER = logging.getLogger(__name__)

class BadumnaUserManager(BaseUserManager):
    '''
    Manager class to create normal & admin users.

    NOTE that we accept `username` as required by the django API,
    but never use it.
    '''
    def create_user(self, username, email, password=None):
        if not email:
            raise ValueError('Users must have an email address')

        user = self.model(
            email=BadumnaUserManager.normalize_email(email),
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password):
        user = self.create_user(None, email, password=password)
        user.is_admin = True
        user.save(using=self._db)
        return user

class BadumnaUser(AbstractBaseUser):
    '''
    A badumna User model
    '''
    class Meta:
        ordering = ['email']

    email = models.EmailField(
        verbose_name='email address',
        max_length=255,
        unique=True,
        db_index=True,
    )
    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)
    date_joined = models.DateTimeField(auto_now_add=True, editable=False)
    objects = BadumnaUserManager()
    USERNAME_FIELD='email'

    def get_full_name(self): return self.email
    def get_short_name(self): return self.email
    def __unicode__(self): return self.email

    @property
    def username(self): return self.email

    # we don't use permissions yet, this is just required by the API
    def has_perm(self, perm, obj=None): return True
    def has_module_perms(self, app_label): return True

    @property
    def is_staff(self): return self.is_admin

    def email_user(self, subject, message, from_email=None):
        return send_mail(subject, message, from_email, [self.email])


class SessionHistory(models.Model):
    raw_data = models.CharField(null=True, max_length=2000)
    active_sessions = models.IntegerField(default=0, null=False)

    def _get_items(self):
        raw = self.raw_data
        if not raw:
            return []
        entries = raw.split("|")
        if not entries: return []
        items = []
        for entry in entries:
            try:
                timestamp, value = entry.split(':',1)
                timestamp = int(timestamp)
                timestamp = from_unix_time(timestamp)
                value = int(value)
                items.append((timestamp, value))
            except StandardError as e:
                LOGGER.error("Error: skipping unparseble SessionHistory(id=%s) item: %s", self.id, entry)
        # LOGGER.debug("items = %r" % (items,))
        return items

    def _set_items(self, items):
        serialized = []
        for timestamp, num_users in items:
            timestamp = to_unix_time(timestamp)
            serialized.append("%s:%s" % (timestamp, num_users))
        self.raw_data = "|".join(serialized)

    items = property(_get_items, _set_items)

class ApplicationManager(models.Manager):

    # The possible characters that random keys will be generated from.
    # NOTE that we don't include both upper and lowercase letters,
    # as some DB backends compare case-insensitively.
    RANDOM_KEY_CHARS = string.uppercase + string.digits
    MAX_KEY_LENGTH = 10
    MIN_KEY_LENGTH = 5
    KEY_RETRIES_PER_LENGTH = 3

    def get_for(self, user, **kw):
        obj = self.get(**kw)
        if obj.visible_to(user):
            return obj
        LOGGER.debug("User %r denied access to application %r", user, kw)
        raise PermissionDenied()
    
    def random_key(self):
        for key_len in xrange(self.MIN_KEY_LENGTH, self.MAX_KEY_LENGTH + 1):
            for tries in xrange(0, self.KEY_RETRIES_PER_LENGTH):
                key = self._random_key(key_len)
                if self.filter(key=key).count() > 0:
                    LOGGER.info("Key collision on: %r", key)
                else:
                    return key
        raise ValueError("All generated keys collided!")

    def _random_key(self, key_len):
        return ''.join([random.choice(self.RANDOM_KEY_CHARS) for _ in xrange(0, key_len)])

class Application(models.Model):
    class Meta:
        ordering = ['id']
    objects = ApplicationManager()
    user = models.ForeignKey(BadumnaUser, editable=False)
    name = models.CharField(max_length=50)
    key = models.CharField(max_length=ApplicationManager.MAX_KEY_LENGTH, unique=True, db_index=True, editable=False, null=True, default=None)
    _session_history = models.ForeignKey(SessionHistory, unique=True, editable=False, null=True)

    def __unicode__(self):
        return "%s/%s (%s)" % (self.user.email, self.name, self.key)

    def get_absolute_url(self):
        return reverse('ControlPanel.status', kwargs={'app_key':self.key})
    
    def visible_to(self, user):
        return user.is_admin or self.user == user
    
    def save(self, *a, **k):
        if self.key is None:
            self.key = type(self).objects.random_key()
        return super(Application, self).save(*a, **k)

    def get_session_history(self):
        result = self._session_history
        if result is None:
            LOGGER.debug("creating SessionHistory object for %s", self)
            result = self._session_history = SessionHistory.objects.create()
            self.save()
        return result

