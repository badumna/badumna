import os, sys
from django.core.management.base import NoArgsCommand, CommandError
from django.conf import settings
from django.template import Template, Context
from StringIO import StringIO

class Command(NoArgsCommand):
    help = 'Render all ahead-of-time templates into static files'

    @staticmethod
    def get_vars(obj):
        if isinstance(obj, dict): return obj.copy()
        return {key: getattr(obj, key) for key in dir(obj)}

    def handle_noargs(self, **options):
        settings.STATIC = True
        vars = self.get_vars(settings)
        root_dir = os.path.abspath(os.path.join(os.path.dirname(__file__), '../../../'))
        os.chdir(root_dir)

        template_paths = {}

        errors_src = 'Site/templates/errors'
        errors_dest = 'Site/static/errors'
        print repr(os.listdir(errors_src))
        for filename in os.listdir(errors_src):
            template_paths[os.path.join(errors_src, filename)] = os.path.join(errors_dest, filename)

        for source, dest in template_paths.items():
            print >> sys.stderr, "Rendering %s -> %s" % (source, dest)
            self.render_file(vars, source, dest)

    @classmethod
    def render_file(cls, vars, source, dest):
        if isinstance(source, basestring):
            with open(source) as f:
                template = Template(f.read())
        else:
            template = Template(source.read())

        vars = cls.get_vars(vars)
        ctx = Context(vars)
        output = template.render(ctx)

        if isinstance(dest, basestring):
            parent_dir = os.path.dirname(dest)
            if not os.path.exists(parent_dir):
                os.makedirs(parent_dir)
            with open(dest, 'w') as f:
                f.write(output)
        else:
            dest.write(output)
