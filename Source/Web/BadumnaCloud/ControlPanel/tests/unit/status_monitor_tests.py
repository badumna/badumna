from __future__ import absolute_import
from .base import *
from .. import helpers
from ControlPanel.background import stats_monitor
from ControlPanel.models import Application
from ControlPanel.util import datetime
from ControlPanel.util.datetime import timedelta
import urllib2
import time
from StringIO import StringIO
import logging
LOGGER = logging.getLogger(__name__)

class StatsMonitorTest(TestCase):
    def setUp(self):
        super(StatsMonitorTest, self).setUp()
        self.stats_url = 'http://example.com/stats'
        self.wait_time = 2.0
        self.history_length = 5

        modify(stats_monitor).MONITOR = self.monitor = self._create_monitor()
        when(urllib2).urlopen.then_return(StringIO('mock http response'))
    
    def tearDown(self):
        self.monitor.stop()
        super(StatsMonitorTest, self).tearDown()
    
    def _create_monitor(self):
        return stats_monitor.StatsMonitor(
                url=self.stats_url,
                wait_time = timedelta(seconds=self.wait_time),
                history_length = self.history_length)
    
    def freeze_time(self):
        now = datetime.utcnow()
        when(datetime).utcnow().then_return(now)
        return now

    def test_should_run_in_a_background_thread_as_scheduled(self):
        expect(self.monitor)._run_one().twice()
        self.monitor.start()
        time.sleep(self.wait_time + (self.wait_time / 2))

    def test_should_abort_if_it_is_not_the_global_instance(self):
        stats_monitor.MONITOR = self._create_monitor()
        self.monitor.start()
        time.sleep(self.wait_time / 2)
        self.assertFalse(self.monitor._thread.is_alive())

    def test_should_abort_when_stopped(self):
        self.monitor.start()
        time.sleep(self.wait_time / 2)
        self.assertTrue(self.monitor._thread.is_alive())
        self.monitor.stop(wait=False)
        time.sleep(self.wait_time)
        self.assertFalse(self.monitor._thread.is_alive())

    def test_should_set_ok_to_false_and_continue_when_an_error_occurs(self):
        expect(self.monitor)._run_one().twice().and_raise(StandardError("things did not proceed as planned"))
        self.monitor.start()
        time.sleep(self.wait_time + (self.wait_time / 2))
        self.assertFalse(helpers.monitor_is_ok())

    def test_should_set_ok_to_true_when_stats_are_updated_successfully(self):
        when(self.monitor)._run_one().then_raise(StandardError("errors"))
        self.monitor.start()
        time.sleep(self.wait_time / 2)

        # after first fetch, there were errors
        self.assertFalse(helpers.monitor_is_ok())
        when(self.monitor)._run_one.then_return(None)
        time.sleep(self.wait_time)

        # but the second fetch should succeed
        self.assertTrue(helpers.monitor_is_ok())

    def test_should_update_stats_for_every_application(self):
        user = self.create_user()
        now = self.freeze_time()

        trailing_edge = now - timedelta(seconds=(self.wait_time * self.history_length))
        too_old_time = trailing_edge - timedelta(seconds=1)
        not_quite_too_old_time = trailing_edge + timedelta(seconds=1)
        LOGGER.debug("trailing edge will be %s", trailing_edge.ctime())

        def create_app(i):
            app = Application.objects.create(user=user, name="app-%s" % i)
            app.get_session_history().items = [
                (too_old_time, 10),
                (not_quite_too_old_time, 20)
            ]
            app.get_session_history().save()
            return app

        applications = list(enumerate([create_app(i) for i in range(0,5)]))

        # generate XML for all scenes except the last app.
        # Note that Users is always 0 or 1 for a non-Dei app, because UserIDs are not used.
        scenes_xml = "\n".join([
            '<Scene><Name>{}</Name><Users>1</Users><Sessions>{}</Sessions></Scene>'.format(app.key, sessions)
            for sessions,app in applications[:-1]
            ])

        xml = '''
        <Statistics>
            <Statistics>
                <NumberOfSessions>30</NumberOfSessions>
            </Statistics>
            <Scenes>
                ''' + scenes_xml + '''
            </Scenes>
        </Statistics>
        '''

        when(urllib2).urlopen(self.stats_url).then_return(StringIO(xml))
        self.monitor._run_one()

        # fetch apps from the DB
        for i, app in applications:
            app = Application.objects.get(id=app.id) # refresh
            history = app.get_session_history().items
            dates = [item[0].ctime() for item in history]
            numbers = [item[1] for item in history]
            LOGGER.debug("checking: %s" % (app.name,))
            self.assertEqual(dates, [
                not_quite_too_old_time.ctime(),
                now.ctime()
            ])

            expected_users = 0 if i == len(applications) - 1 else i
            self.assertEqual(numbers, [20, expected_users])

    def test_response_with_no_scenes_should_be_treated_as_no_users_in_each_scene(self):
        xml = '''
        <Statistics>
            <Statistics>
                <NumberOfSessions>0</NumberOfSessions>
            </Statistics>
        </Statistics>
        '''

        user = self.create_user()
        app = Application.objects.create(user=user, name="app")
        now = self.freeze_time()

        when(urllib2).urlopen(self.stats_url).then_return(StringIO(xml))
        self.monitor._run_one()

        app = Application.objects.get(id=app.id)
        history = [(time.ctime(), val) for time, val in app.get_session_history().items]
        self.assertEqual(history, [(now.ctime(), 0)])
