from __future__ import absolute_import
from .base import *

from ControlPanel.forms import BadumnaRegistrationForm, CanResendActivation

class BadumnaRegistrationFormTest(TestCase):
    def setUp(self):
        self.active_email = 'active@example.com'
        self.create_user(self.active_email)
        self.inactive_email = 'inactive@example.com'
        inactive_user = self.create_inactive_user(self.inactive_email)
        self.new_email = 'newuser@example.com'

    def with_defaults(self, **kwargs):
        defaults = {
                'password1': 'pass',
                'password2':'pass',
                'agree_terms_and_conds': True,
            }
        defaults.update(kwargs)
        return defaults

    def test_should_reject_active_users_email(self):
        form = BadumnaRegistrationForm(self.with_defaults(email=self.active_email))
        self.assertEqual(form.errors, {'email': [u'This email address is already in use.']})

    def test_should_show_resend_link_for_inactive_user_mail(self):
        form = BadumnaRegistrationForm(self.with_defaults(email=self.inactive_email))
        self.assertEqual(form.errors, {'email':[u'This email address is already in use.', unicode(CanResendActivation(self.inactive_email))]})

    def test_should_allow_new_users(self):
        form = BadumnaRegistrationForm(self.with_defaults(email=self.new_email))
        self.assertEqual(form.errors, {})
