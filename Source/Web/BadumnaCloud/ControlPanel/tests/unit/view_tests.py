from __future__ import absolute_import
from mocktest import expect, when

from django.conf import settings
from django.core.urlresolvers import reverse
from StringIO import StringIO
import json
import unittest
import cgi
from ControlPanel.models import Application
from ControlPanel.background import stats_monitor
from ControlPanel.util.datetime import from_unix_time

from .base import *

class CreateAppTest(TestCase, RequiresAuthentication):
    url = reverse('ControlPanel.create_app')
    def test_creates_an_app(self):
        user = self.login()
        app_name = 'App Name'
        response = self.client.post(self.url, {'name':app_name})
        self.assertEqual(Application.objects.filter(name=app_name, user=user).count(), 1)
        self.assertRedirects(response, reverse('ControlPanel.list_apps'))

    def test_reject_missing_name(self):
        user = self.login()
        response = ok(self.client.post(self.url, {}))
        self.assertEqual(Application.objects.filter(user=user).count(), 0)
        self.assertContains(response, "required")

    def test_reject_blank_name(self):
        user = self.login()
        response = ok(self.client.post(self.url, {'name':''}))
        self.assertEqual(Application.objects.filter(user=user).count(), 0)
        self.assertContains(response, "required")

class DeleteAppTest(TestCase, RequiresAuthentication):
    url = reverse('ControlPanel.delete_app')
    def setUp(self):
        super(DeleteAppTest, self).setUp()
        self.user = self.create_user()
        self.other_user = self.create_user("someone_else@example.com")
        self.app = Application.objects.create(user=self.user)

    def _post(self, app_key=None):
        if app_key is None:
            app_key = self.app.key
        return self.client.post(self.url, {'key': app_key})

    def test_rejects_GET_request(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 405)

    def test_requires_logged_in_user(self):
        self._assert_is_login_redirect(self._post())

    def test_not_found_app_key_fails(self):
        self.login(self.user)
        self.assertEqual(self._post(app_key="12").status_code, 404)

    def test_requires_ownership(self):
        user = self.login(self.other_user)
        response = self._post()
        self.assertEqual(response.status_code, 403)

    def test_deletes_an_app(self):
        user = self.login(self.user)
        response = self._post()
        self.assertRedirects(response, reverse('ControlPanel.list_apps'))
        self.assertRaises(Application.DoesNotExist, lambda: Application.objects.get(id=self.app.id))

class AppListTest(TestCase, RequiresAuthentication):
    url = reverse('ControlPanel.list_apps')
    def create_app(self, user):
        return Application.objects.create(user=user)

    def test_lists_all_users_applications(self):
        current_user = self.create_user("myself@example.com")
        other_user = self.create_user("other@example.com")
        self.login(current_user)

        my_apps = [self.create_app(current_user) for i in range(0,3)]
        other_apps = [self.create_app(other_user) for i in range(0,5)]

        response = ok(self.client.get(self.url))
        self.assertEqual(list(response.context['apps']), my_apps)

class AppStatusTest(TestCase, RequiresAuthentication):
    def setUp(self):
        super(AppStatusTest, self).setUp()

        self.user = self.create_user()
        self.app = Application.objects.create(name='Application Name', user=self.user)
        self.url = self.app.get_absolute_url()

    def test_only_visible_to_owner(self):
        self.login(self.user)
        other_user = self.login(self.create_user('other@example.com'))
        other_app = Application.objects.create(name='Other app', user=self.user)
        response = self.client.get(other_app.get_absolute_url())
        self.assertEqual(response.status_code, 403) # permission denied

class AppStatusJsonTest(AppStatusTest):
    def setUp(self):
        super(AppStatusJsonTest, self).setUp()
        self.url = reverse('ControlPanel.status.json', kwargs={'app_key':self.app.key})
        modify(stats_monitor).MONITOR = self.monitor = mock('status monitor')

    def test_returns_stats_from_application_as_utc_timestamps(self):
        session_history_items = [
            (from_unix_time(100.5), 1),
            (from_unix_time(200), 2)]

        history = self.app.get_session_history()
        history.items = session_history_items
        history.save()

        self.login(self.user)
        response = ok(self.client.get(self.url))
        self.assertEqual(response['Content-type'], 'application/json')
        response = json.loads(response.content)
        self.assertEqual(response, {
            'series': [
                [100, 1],
                [200, 2]
            ]
        })

    @unittest.skip("Not implemented")
    def test_adds_warning_alongside_data_when_stats_monitor_is_experiencing_trouble(self):
        self.monitor.ok = False
        self.login(self.user)
        response = ok(self.client.get(self.url))
        response = json.loads(response.content)
        self.assertEqual({'warning', 'series'}, set(response.keys()))

class AppConfigTest(TestCase):
    def setUp(self):
        self.user = self.create_user()
        self.app = Application.objects.create(name='Application Name', user=self.user)
        self.url = "/config/%s" % (cgi.escape(self.app.key))

    def test_config_returned_for_valid_app(self):
        response = ok(self.client.get(self.url))
        self.assertEqual(response['Content-Type'], 'text/xml')

    def test_config_denied_for_missing_app(self):
        self.app.delete()

        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 404)

    def test_config_includes_seed_peer(self):
        response = ok(self.client.get(self.url))
        expected_initializer = "<Initializer>" + settings.BADUMNA_NETWORK['SEED_PEERS'][0] + ":"
        self.assertTrue(expected_initializer in re.sub('\s+', '', response.content))

class ResendActivationTest(TestCase):
    url = reverse('registration_resend_activation')

    def setUp(self):
        super(ResendActivationTest, self).setUp()
        self.active_user = self.create_user('active@example.com')
        self.inactive_user = self.create_inactive_user('inactive@example.com')

    def test_should_resend_activation(self):
        def get_page():
            response = ok(self.client.get(self.url, {'email': self.inactive_user.email}))
            self.assertRedirects(response, reverse('registration_complete'))
        expect_emails(get_page, num=1)

    def test_should_404_for_an_active_or_unknown_user(self):
        self.assertEqual(self.client.get(self.url, {'email':self.active_user.email}).status_code, 404)
        self.assertEqual(self.client.get(self.url, {'email':'unknown@example.com'}).status_code, 404)

