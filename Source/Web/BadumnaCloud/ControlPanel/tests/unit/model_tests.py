from __future__ import absolute_import
from django.core.exceptions import PermissionDenied

from .base import *
from ControlPanel.models import SessionHistory, Application
from ControlPanel.util.datetime import datetime, timedelta, utcnow

class ApplicationModelTests(TestCase):
    def setUp(self):
        super(ApplicationModelTests, self).setUp()
        self.user = self.create_user()

    def test_should_create_session_history_on_demand(self):
        app = Application.objects.create(user=self.user)
        self.assertEqual(app._session_history, None)
        session_history = app.get_session_history()
        self.assertNotEqual(app._session_history, None)
        self.assertEqual(app.get_session_history(), session_history)
    
    def test_allows_access_from_admin_and_owner(self):
        user = self.create_user('owner@example.com')
        admin = self.create_user('admin@example.com')
        admin.is_admin = True
        user.is_admin = False
        app = Application.objects.create(user=user)
        self.assertEqual(Application.objects.get_for(admin, key=app.key), app)
        self.assertEqual(Application.objects.get_for(user, key=app.key), app)

    def test_denies_access_from_other_users(self):
        user = self.create_user('other@example.com')
        owner = self.create_user('owner@example.com')
        app = Application.objects.create(user=owner)
        self.assertRaises(PermissionDenied, lambda: Application.objects.get_for(user, key=app.key))
    
    def test_generates_random_key_on_save(self):
        when(Application.objects)._random_key.then_return("key")
        app = Application(user=self.user)

        self.assertEqual(app.key, None)
        app.save()
        self.assertEqual(app.key, "key")

    def test_generates_random_key_repeatedly_on_collision(self):
        keys = ['key1','key1','key1','key2']
        expect(Application.objects)._random_key.exactly(4).times.and_call(lambda *a: keys.pop(0))
        app1 = Application(user=self.user)
        app2 = Application(user=self.user)

        app1.save()
        self.assertEqual(app1.key, "key1")
        app2.save()
        self.assertEqual(app2.key, "key2")

    def test_gives_up_after_3_tries_of_each_valid_key_length(self):
        key = "key"
        expect(Application.objects)._random_key(5).exactly(3).times.and_return(key)
        expect(Application.objects)._random_key(6).exactly(3).times.and_return(key)
        expect(Application.objects)._random_key(7).exactly(3).times.and_return(key)
        expect(Application.objects)._random_key(8).exactly(3).times.and_return(key)
        expect(Application.objects)._random_key(9).exactly(3).times.and_return(key)
        expect(Application.objects)._random_key(10).exactly(3).times.and_return(key)

        # pretend key is *always* taken
        when(Application.objects).filter.then_return(mock('results').with_methods(count=1))

        app = Application(user=self.user)
        self.assertRaises(ValueError, lambda: app.save())

class SessionHistoryTests(TestCase):
    def test_should_serialize_and_deserialize_history(self):
        hist = SessionHistory.objects.create()
        self.assertEqual(hist.items, [])
        before = utcnow() - timedelta(minutes=2)
        now = utcnow()
        hist.items = [(before, 9), (now, 10)]
        hist.save()
        self.assertNotEqual(hist.raw_data, None)

        items = hist.items
        dates = [item[0].ctime() for item in items]
        values = [item[1] for item in items]
        self.assertEqual(dates, [before.ctime(), now.ctime()])
        self.assertEqual(values, [9, 10])

    def test_should_drop_unparseable_items(self):
        # (errors will be logged, but this data is
        # transient and we don't want a bad record to block
        # all stats collection)
        hist = SessionHistory.objects.create()
        self.assertEqual(hist.items, [])
        before = utcnow() - timedelta(minutes=2)
        now = utcnow()
        hist.items = [(before, 9), (now, 10)]
        hist.save()
        assert len(hist.items) == 2

        # corrupt the second item
        hist.raw_data = hist.raw_data + "__fdfs"
        hist.save()

        assert len(hist.items) == 1

