import mocktest
from mocktest import mock, expect, when, modify
from bs4 import BeautifulSoup
from urlparse import urlparse
import re

import django.test
from django.conf import settings
from django.core.urlresolvers import reverse
from django.contrib.auth import get_user_model
User = get_user_model()
from registration.models import RegistrationProfile
from ControlPanel.tests.helpers import expect_emails

def soup(response):
    return BeautifulSoup(ok(response).content)

OK_RESPONSES = (200, 302)
def not_ok(response):
    assert response.status_code not in OK_RESPONSES, "Response code IS ok: %s\n%s" % (response.status_code,response.content)
    return response

def ok(response):
    assert response.status_code in OK_RESPONSES, "Response code not ok: %s\n%s" % (response.status_code,response.content)
    return response

class TestCase(django.test.TestCase, mocktest.TestCase):
    def create_user(self, username='test@example.com'):
        password = 'password'
        user = User.objects.create_user(username, username, password)
        user.plaintext_password = password
        return user

    def create_inactive_user(self, username='test@example.com'):
        user = RegistrationProfile.objects.create_inactive_user(None, username, None, None)
        return user

    def login(self, user=None):
        if user is None:
            user = self.user = self.create_user()
        login = self.client.login(username=user.username, password=user.plaintext_password)
        self.failUnless(login, 'Could not log in')
        return user

class RequiresAuthentication(object):
    def _assert_is_login_redirect(self, response):
        self.assertEqual(response.status_code, 302)
        location = response['Location']
        url = urlparse(location)
        path = re.sub('^http://[^/]+', '', location)
        self.assertEqual(url.path, reverse(settings.LOGIN_URL))
        self.assertEqual(url.query, 'next=' + self.url)

    def test_requires_logged_in_user(self):
        self._assert_is_login_redirect(self.client.get(self.url))

