
from django_nose import NoseTestSuiteRunner

class CustomNoseRunner(NoseTestSuiteRunner):
    def teardown_databases(self, *args, **kwargs):
        """Overridden to not delete the database, becase it'll get deleted on startup,
        and deletion fails on the build server because the webserver thread isn't done yet (I think?)."""
        pass
