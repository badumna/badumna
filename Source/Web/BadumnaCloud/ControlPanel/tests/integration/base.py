from __future__ import absolute_import
from django.test import LiveServerTestCase, TransactionTestCase
from django.core.urlresolvers import reverse
import mocktest
import os
import sys
import re
import logging
from django.conf import settings
from django.core import mail
from selenium.webdriver.support.wait import WebDriverWait
from selenium.common.exceptions import NoSuchElementException, MoveTargetOutOfBoundsException, TimeoutException
from nose.plugins.attrib import attr
from ControlPanel.tests.helpers import expect_emails
LOGGER = logging.getLogger(__name__)

__all__ = ['IntegrationTestBase', 'ActiveUserBase', 'AdminUserBase', 'mocktest', 'NoSuchElementException', 'expect_emails']


_sentinel = object()

# You can switch between browser implementations by setting $SELENIUM_BROWSER
DESIRED_BROWSER = os.environ.get('SELENIUM_BROWSER', 'firefox')
if DESIRED_BROWSER == 'chrome':
    from selenium.webdriver.chrome.webdriver import WebDriver as ChromeWebDriver
    path_to_chrome = os.path.abspath(os.path.join(os.path.dirname(__file__), '../../../../../../Third Party/chromedriver.exe'))
    assert os.path.exists(path_to_chrome)
    def WebDriver():
        return ChromeWebDriver(path_to_chrome)
elif DESIRED_BROWSER == 'firefox':
    from selenium.webdriver.firefox.webdriver import WebDriver
    def init_selenium():
        return WebDriver()
else:
    raise AssertionError("Unknown browser: %s" % (DESIRED_BROWSER,))

class Object(object):pass

# manage selenium & live django server lifecycle
SELENIUM = None
LIVE_SERVER = None
def START():
    '''
    Called once per IntegrationTestBase subclass,
    but only performs work the first time
    '''
    global SELENIUM, LIVE_SERVER
    if not SELENIUM:
        SELENIUM = WebDriver()
        SELENIUM.implicitly_wait(1) # wait 1 second before failing on "element not found"
    if not LIVE_SERVER:
        LIVE_SERVER = LiveServer()
        LIVE_SERVER.start()

def STOP():
    '''
    Called once after running all tests in the `integration_tests` module
    '''
    global SELENIUM, LIVE_SERVER
    if SELENIUM:
        SELENIUM.quit()
        SELENIUM = None
    if LIVE_SERVER:
        LIVE_SERVER.stop()
        LIVE_SERVER = None

# We copy the important bits of LiveServerTestCase
# but without inheriting from unittest.TestCase
# since we run the server at a module-level, not a class-level.
class LiveServer(object):
    start = LiveServerTestCase.setUpClass
    stop = LiveServerTestCase.tearDownClass
    @property
    def live_server_url(self):
        return 'http://%s:%s' % (
            LiveServerTestCase.server_thread.host, LiveServerTestCase.server_thread.port)


@attr('integration')
class IntegrationTestBase(TransactionTestCase, mocktest.TestCase):
    @classmethod
    def setUpClass(cls):
        START()
        cls.selenium = SELENIUM
        super(IntegrationTestBase, cls).setUpClass()

    @property
    def live_server_url(self):
        return LIVE_SERVER.live_server_url

    def get(self, u='/'):
        return self.selenium.get(self.url(u))

    def url(self, u):
        if u.startswith('/'):
            raise AssertionError("Do not use literal URLs - pass in the name of a URL instead")
        u = reverse(u)
        return self.live_server_url + u

    def css(self, selector, elem=_sentinel, wait=True):
        if elem is _sentinel:
            elem = self.selenium

        # workaround lack of `nonlocal` in python 2
        state = Object()
        def find():
            state.found = elem.find_element_by_css_selector(selector)
            return state.found
        try:
            self.wait.until_success(lambda *a: find())
        except TimeoutException:
            raise NoSuchElementException("Could not find by css selector: %s" % (selector,))
        return state.found

    def get_wait(self, timeout=5, ignored_exceptions=None):
        if ignored_exceptions is None:
            ignored_exceptions = (NoSuchElementException, MoveTargetOutOfBoundsException, AssertionError)
        return WaitExtensions(SELENIUM, timeout=5, ignored_exceptions=ignored_exceptions)
    wait = property(get_wait)

    def all_css(self, selector, elem=_sentinel):
        if elem is _sentinel:
            elem = self.selenium
        return elem.find_elements_by_css_selector(selector)

    def form_errors(self):
        return [elem.text.strip() for elem in self.all_css("form .alert, form .help-inline, form .help-block")]

    def ensure_navbar_visible(self):
        is_visible = lambda *a: self.css(".nav").is_displayed()
        if not is_visible():
            LOGGER.debug("Opening navbar")
            self.css(".btn-navbar").click()
            self.wait.until(is_visible)

    def ensure_user_menu_visible(self):
        self.ensure_navbar_visible()
        is_visible = lambda *a: self.css(".user-menu").is_displayed()
        if not is_visible():
            LOGGER.debug("Opening user menu")
            try:
                self.css("a.toggle-user-menu").click() # show user menu dropdown
            except NoSuchElementException:
                raise AssertionError("Can't show user menu - you are not logged in!")
            self.wait.until(is_visible)
        return self.css(".user-menu")

    def assertLoggedIn(self, username=None):
        try:
            current_user = self.css(".currentUserEmail").text.lower()
        except NoSuchElementException:
            raise AssertionError("Not logged in")

        if username is not None:
            username = username.lower()
            self.assertEqual(current_user, username)
        self.assertEqual(self.all_css("a.login"), [])

    def assertNotLoggedIn(self):
        self.ensure_navbar_visible()
        try:
            self.assertEqual(self.css("a.login").text.lower(), "log in")
        except NoSuchElementException:
            raise AssertionError("Logged in!")
        self.assertEqual(self.all_css(".currentUserEmail"), [])

    def set_selected(self, element, selected):
        is_checked = lambda: element.get_attribute("checked") == 'true'
        if is_checked() != selected:
            element.click()
        assert is_checked() == selected

    def signup(self, username, password, password2=None, agree_to_terms = True, join_newsletter = False, expect_success=True):
        if password2 is None: password2 = password
        self.get("index")
        self.ensure_navbar_visible()
        link = self.css("a.signup")
        link.click()
        self.assertEqual(self.selenium.current_url, self.url("registration_register"))
        signup_form = self.css("form.signup")
        self.enter_text(signup_form.find_element_by_css_selector("input[name='email']"), username)
        self.enter_text(signup_form.find_element_by_css_selector("input[name='password1']"), password)
        self.enter_text(signup_form.find_element_by_css_selector("input[name='password2']"), password2)
        self.set_selected(signup_form.find_element_by_css_selector("input[name='agree_terms_and_conds']"), agree_to_terms)
        self.set_selected(signup_form.find_element_by_css_selector("input[name='join_newsletter']"), join_newsletter)

        if expect_success:
            email, = expect_emails(signup_form.submit, num=1)
            self.assertEqual(self.selenium.current_url, self.url("registration_complete"))
            return email
        else:
            signup_form.submit()

    def activate(self, email, expect_success=True):
        self.click_first_email_link(email)

        if expect_success:
            self.assertLoggedIn()

    def click_first_email_link(self, email):
        LOGGER.debug("Email contents:\n%s", email.body)
        link = re.search("<(http://[^/]+/[^>]+)>", email.body).group(1)
        self.selenium.get(link)

    def enter_text(self, elem, text):
        for attempt in range(0,5):
            elem.clear()
            elem.send_keys(text)
            if elem.get_attribute("value") == text:
                return
        self.fail("Could not reliably enter text: %s" % (text,))

    def show_user_menu(self):
        self.ensure_user_menu_visible()
    
    def current_dropdown_menu(self):
        return self.css(".dropdown.open .dropdown-menu")

    def logout(self):
        self.ensure_user_menu_visible()
        self.css("a.logout").click()
        self.assertNotLoggedIn()

    def login(self, username=None, password=None, expect_success=True):
        if username is None: username = self.username
        if password is None: password = self.password
        self.selenium.get(self.url('login'))

        form = self.selenium.find_element_by_css_selector("form.login")
        self.enter_text(form.find_element_by_css_selector("input[name='username']"), username)
        self.enter_text(form.find_element_by_css_selector("input[name='password']"), password)
        form.submit()

        if expect_success:
            self.assertLoggedIn(username)

    def save_screenshot(self, filename):
        path = os.path.abspath(filename)
        SELENIUM.get_screenshot_as_file(path)
        print >> sys.stderr, "Screenshot saved to %s" % (filename,)

class ActiveUserBase(object):
    fixtures=['active-user.json']
    username = 'active@example.com'
    password = 'password'

class AdminUserBase(object):
    fixtures=['active-user.json', 'admin-user.json']
    username = 'admin@example.com'
    password = 'password'
    non_admin_username = 'active@example.com'

class WaitExtensions(WebDriverWait):
    def until_success(self, fn, *a):
        def action(*inner_a):
            try:
                fn(*inner_a)
                return True
            except self._ignored_exceptions as e:
                LOGGER.debug("Caught exception in `until_success` (will retry) - %s: %s" % (type(e).__name__, e))
                return False
        return self.until(action, *a)
