from __future__ import absolute_import
import re
from django.conf import settings
from ControlPanel.models import Application, BadumnaUser
from ControlPanel import views
from ControlPanel.background import stats_monitor
import datetime
import pytz.reference
from .base import *
import logging
import unittest
from selenium.webdriver.support import expected_conditions as EC
LOGGER = logging.getLogger(__name__)

class ApplicationManagementTest(IntegrationTestBase, ActiveUserBase):
    def create_app(self, app_name):
        self.css("a.createApp").click()
        form = self.css("form.createApp")
        self.enter_text(self.css('input[name=name]', form), app_name)
        form.submit()
        self.assertEqual(self.selenium.current_url, self.url("ControlPanel.list_apps"))

    @property
    def _listed_app_names(self):
        return [elem.text for elem in self.all_css(".appName")]

    def test_creating_an_app(self):
        self.login()
        self.create_app("My First App")
    
    def test_list_apps(self):
        self.login()
        self.create_app("My First App")
        self.create_app("My Second App")
        self.assertEqual(self._listed_app_names, ["My First App", "My Second App"])

    def test_app_status(self):
        self.login()
        self.create_app("My First App")
        self.css(".appStatus").click()
        self.assertMatches(mocktest.string_matching(".*/apps/[^/]+/$"), self.selenium.current_url)

    def test_delete_app(self):
        self.login()
        self.create_app("My First App")
        self.create_app("My Second App")

        self.all_css(".appStatus")[0].click()
        self.css("a.deleteApp").click()

        self.wait.until(lambda _: self.css("#deleteConfirm").value_of_css_property('opacity') == '1')
        self.css(".modal .btn.btn-danger").click()

        self.wait.until_success(lambda a: self.assertEqual(self.selenium.current_url, self.url("ControlPanel.list_apps")))
        self.assertEqual(self._listed_app_names, ["My Second App"])

    def test_delete_app_can_be_cancelled(self):
        self.login()
        self.create_app("My First App")
        self.create_app("My Second App")

        self.all_css(".appStatus")[0].click()
        self.css("a.deleteApp").click()
        cancel_btn = self.css(".modal .btn.cancel")
        self.wait.until_success(lambda a: cancel_btn.click())

        self.get("ControlPanel.list_apps")
        self.assertEqual(self._listed_app_names, ["My First App", "My Second App"])

class ApplicationStatusTest(IntegrationTestBase, ActiveUserBase):
    def setUp(self):
        super(ApplicationStatusTest, self).setUp()
        self.app = self.create_app("My First App")
        session_history = self.app.get_session_history()
        utcnow = datetime.datetime.utcnow()
        graph_window_size = settings.STATISTICS_REFRESH_INTERVAL * (settings.STATISTICS_HISTORY_LENGTH - 2)
        LOGGER.debug(
                "refresh_interval = %.1fm, history_length = %d. expecting graph window to span %.1fm",
                settings.STATISTICS_REFRESH_INTERVAL.total_seconds() / 60.0,
                settings.STATISTICS_HISTORY_LENGTH,
                graph_window_size.total_seconds() / 60)

        session_history.items = [
            (utcnow - graph_window_size + datetime.timedelta(seconds=10), 0),
            (utcnow - datetime.timedelta(seconds=5), 2)
        ]
        LOGGER.debug("saved history items: %r", session_history.items)
        LOGGER.debug(
            "in the local timezone, the history item times are: %r",
            [time.replace(tzinfo=pytz.utc).astimezone(pytz.reference.LocalTimezone()).ctime() for time, val in session_history.items]
        )
        session_history.save()

        self.login()

        # note that using `now` means the JS and Python clocks must agree on their timezone!
        self.min_graph_time = (datetime.datetime.now() - graph_window_size) - datetime.timedelta(minutes=1)
        self.css(".appStatus").click()
        self.wait.until(lambda driver: self._graph_displayed())
        self.max_graph_time = (datetime.datetime.now())

    def create_app(self, name):
        app = Application.objects.create(user=BadumnaUser.objects.get(email=self.username), name=name)
        app.save()
        return app

    def warning_message(self, *ignored):
        try:
            elem = self.css(".warning")
        except NoSuchElementException:
            return None
        else:
            if not elem.is_displayed():
                return None
            return elem.text

    def no_warning_message(self, *ignored):
        '''Explicitly check for a *missing* warning, as opposed to an empty message (which is an intermediate state)'''
        return self.warning_message() is None

    def _graph_displayed(self):
        return len(self.all_css("#graph-live canvas")) > 0

    def _graph_labels(self):
        return [elem.text for elem in self.all_css("#graph-live .tickLabels .xAxis .tickLabel")]

    def test_session_history_is_in_user_local_time_and_matches_saved_history_window(self):
        labels = self._graph_labels()
        def parse_time(label):
            return datetime.datetime.strptime(label, "%I:%M%p").time()

        self.assertTrue(len(labels) > 3)
        labels = list(map(parse_time, labels))
        first_time = labels[0]
        last_time = labels[-1]

        first_time = datetime.datetime.combine(self.min_graph_time.date(), first_time)
        last_time  = datetime.datetime.combine(self.max_graph_time.date(), last_time)

        def time_is_within(time, earliest, latest):
            result = earliest <= time <= latest
            LOGGER.debug("Time %s within %s and %s?: %s",
                    time.ctime(),
                    earliest.ctime(),
                    latest.ctime(),
                    result)
            return result

        LOGGER.debug("min_graph_time is %s, max_graph_time is %s", self.min_graph_time.ctime(), self.max_graph_time.ctime())
        LOGGER.debug("graph labels are:\n%s", "\n".join([t.isoformat() for t in labels]))

        leniance = datetime.timedelta(minutes=6)
        one_day = datetime.timedelta(days=1)

        # Leniance is applied to each end of the graph window because `flot` automatically picks
        # appropriate labels, they don't necessarily lie at the start and end of the graph period.

        # Note that we compare using datetimes (so that midnight overflow is handled correctly),
        # but we can't be sure which day a given label refers to. So we also try +-1 day, and make
        # sure it falls in *one* of thoes ranges.
        def ensure_label_within_range(label_time, min_time, max_time):
            self.assertTrue(any([
                time_is_within(label_time,           min_time, max_time),
                time_is_within(label_time - one_day, min_time, max_time),
                time_is_within(label_time + one_day, min_time, max_time),
            ]), "Expected %s to be between %s and %s" % (
                label_time.time().isoformat(),
                min_time.time().isoformat(),
                max_time.time().isoformat()
            ))

        min_last_label_time = self.min_graph_time + leniance
        ensure_label_within_range(first_time, self.min_graph_time, self.min_graph_time + leniance)
        ensure_label_within_range(last_time, self.max_graph_time - leniance, self.max_graph_time)

    def stub_stats_with_quick_refresh(self):
        '''use a very small stats update timeout, so the UI updates frequently'''
        mocktest.modify(settings).STATISTICS_REFRESH_INTERVAL = datetime.timedelta(seconds=2)
        self.selenium.refresh()

    @unittest.skip("Not implemented")
    def test_warning_reflects_state_of_update_thread(self):
        self.stub_stats_with_quick_refresh()

        fake_error = False
        def run_update():
            if fake_error:
                raise StandardError("update failed")
            else:
                pass # update succeeded

        self.monitor._run_one = run_update
        self.assertEqual(self.warning_message(), None)

        # turn the warning light on
        fake_error = True
        self.wait.until(self.warning_message)
        self.assertEqual(self.warning_message(), "Statistics collection is down - these results may be incomplete")

        # turn it off again
        fake_error = False
        self.wait.until(self.no_warning_message)

    def test_warning_shown_when_server_is_down(self):
        self.stub_stats_with_quick_refresh()
        self.assertEqual(self.warning_message(), None)

        # the status.json request will fail without this object available
        app_objects = Application.objects
        mocktest.modify(Application).objects = None

        self.wait.until(self.warning_message)
        self.assertEqual(self.warning_message(), "Error: Server not responding")

        # revert to original setup
        Application.objects = app_objects

        self.wait.until(self.no_warning_message)
