from __future__ import absolute_import
from .base import *

# We don't thoroughly test the admin pages, but we do test that
# everything returns a response without raising exceptions.

class AdminTest(IntegrationTestBase, AdminUserBase):
    def login(self, *a, **k):
        super(AdminTest, self).login(*a, **k)
        self.get("admin:index")

    def test_admin_denied_for_normal_user(self):
        self.login(self.non_admin_username)
        self.assert_title("Log in")

    def test_list_profiles(self):
        self.login()
        self.click_link(text="Registration profiles")
        self.assert_title("Select registration profile to change")

    def test_list_users(self):
        self.login()
        self.click_link(text="Badumna users")
        self.assert_title("Select badumna user to change")

    def test_list_apps(self):
        self.login()
        self.click_link(text="Applications")
        self.assert_title("Select application to change")

    def test_list_session_histories(self):
        self.login()
        self.click_link(text="Session historys")
        self.assert_title("Select session history to change")

    def click_link(self, text):
        links = filter(lambda link: link.text == text, self.all_css("a[href]"))
        assert len(links) > 0, "No links found with text %s" % (text,)
        assert len(links) < 2, "Too many links found with text %s" % (text,)
        return links[0].click()

    def assert_ok(self, msg=None):
        self.assert_title(None)

    def assert_title(self, msg=None):
        # error pages don't have the "admin" header, so
        # this means a page rendered successfully
        expected = (msg + " | Django site admin" if msg else "Django administration")
        self.wait.until(lambda driver: driver.title == expected)
