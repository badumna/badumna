from __future__ import absolute_import
from django.conf import settings
import createsend as CS
import re

from .base import *

class SignupTest(IntegrationTestBase):
    def setUp(self):
        super(SignupTest, self).setUp()
        self.cs_subscriber = mocktest.mock('CampaignMonitor subscriber')
        mocktest.when(CS).Subscriber().then_return(self.cs_subscriber)

    def test_signup_success(self):
        mocktest.expect(self.cs_subscriber).add.never()
        email = self.signup(username="test@example.com", password="test")
        self.assertEqual(self.css("h1").text, "Hold tight!")

        self.assertEqual(email.subject, "BadumnaCloud Activation Key")
        self.assertEqual(email.to, ["test@example.com"])
        self.assertEqual(email.from_email, 'cloud@badumna.com')

        self.activate(email)
        self.assertLoggedIn("test@example.com")

    def test_signup_opting_in_to_newsletter(self):
        mocktest.expect(self.cs_subscriber).add.once()
        email = self.signup(username="test@example.com", password="test", join_newsletter=True)

    def test_must_agree_to_terms_and_conditions(self):
        email = self.signup(username="test@example.com", password="test", agree_to_terms=False, expect_success=False)
        self.assertEqual(self.form_errors(), ["You must agree to the Terms & Conditions"])

    def test_resend_activation_email_from_signup(self):
        email1 = self.signup(username="test@example.com", password="pass1")
        self.signup(username="test@example.com", password="pass2", expect_success=False)
        email2, = expect_emails(lambda: self.css("a.resendActivation").click())

        self.assertEqual(self.selenium.current_url, self.url("registration_complete"))

        self.assertEqual(email1.body, email2.body)
        self.activate(email2)
        self.assertLoggedIn("test@example.com")

    def test_resend_activation_email_from_login(self):
        self.signup(username="test@example.com", password="pass1")
        self.login(username="test@example.com", password="pass1", expect_success=False)
        self.assertNotLoggedIn()
        email, = expect_emails(lambda: self.css("a.resendActivation").click())
        self.activate(email)

class ActiveUserTest(IntegrationTestBase, ActiveUserBase):

    def test_login_success(self):
        self.login()
        self.assertEqual(self.form_errors(), [])
        self.assertEqual(self.selenium.current_url, self.url("ControlPanel.list_apps"))
        self.assertLoggedIn(self.username)

    def test_login_wrong_password(self):
        self.login(password="wrong", expect_success=False)
        self.assertNotEqual(self.form_errors(), [])
        self.assertNotLoggedIn()

    def test_login_invalid_user(self):
        self.login(username="someone_else@example.com", expect_success=False)
        self.assertNotEqual(self.form_errors(), [])
        self.assertNotLoggedIn()

    def test_cant_reactivate_an_active_user_via_signup(self):
        self.signup(self.username, 'passwd', expect_success=False)
        self.assertEqual(self.form_errors(), ["This email address is already in use."])

    def test_cant_reactivate_an_active_user_via_login(self):
        self.login(self.username, 'not_my_passwd', expect_success=False)
        self.assertEqual(self.form_errors(), ["Please enter a correct email address and password. Note that both fields may be case-sensitive."])

    def test_logout(self):
        self.login()
        self.assertLoggedIn(self.username)
        self.logout()
        self.assertEqual(self.css("h1").text, "You have been logged out.")

    def test_signup_ensures_unique_email(self):
        self.signup(username=self.username, password="password", expect_success=False)
        self.assertEqual(self.form_errors(), ['This email address is already in use.'])
        self.assertEqual(self.selenium.current_url, self.url("registration_register"))

    def test_change_password(self):
        self.login()
        menu = self.ensure_user_menu_visible()
        self.css("a.changePassword", menu).click()

        form = self.css("form.changePassword")
        new_password="sekret!"
        self.enter_text(self.css("input[name='old_password']", form), self.password)
        self.enter_text(self.css("input[name='new_password1']", form), new_password)
        self.enter_text(self.css("input[name='new_password2']", form), new_password)
        form.submit()

        self.assertEqual(self.selenium.current_url, self.url("django.contrib.auth.views.password_change_done"))

        # old password does not work:
        self.login(self.username, self.password, expect_success=False)

        # new password works:
        self.login(self.username, new_password)

    def test_change_password_requires_original_password(self):
        self.login()
        self.selenium.get(self.url('password_change'))

        form = self.css("form.changePassword")
        new_password="sekret!"
        self.enter_text(self.css("input[name='old_password']", form), self.password + "_not")
        self.enter_text(self.css("input[name='new_password1']", form), new_password)
        self.enter_text(self.css("input[name='new_password2']", form), new_password)
        form.submit()

        self.assertEqual(self.form_errors(), ["Your old password was entered incorrectly. Please enter it again."])

    def test_forgot_password(self):
        self.selenium.get(self.url('login'))
        self.css("a.forgotPassword").click()

        form = self.css("form.forgotPassword")
        self.enter_text(self.css("input[name='email']", form), self.username)
        email, = expect_emails(form.submit)

        self.assertEqual(email.subject, "BadumnaCloud Password Reset")
        self.assertEqual(email.to, [self.username])
        self.assertEqual(email.from_email, 'cloud@badumna.com')

        self.click_first_email_link(email)

        form = self.css("form.resetPassword")
        new_password="sekret!"
        self.enter_text(self.css("input[name='new_password1']", form), new_password)
        self.enter_text(self.css("input[name='new_password2']", form), new_password)
        form.submit()

        self.assertEqual(self.selenium.current_url, self.url("django.contrib.auth.views.password_reset_complete"))

        # old password does not work:
        self.login(self.username, self.password, expect_success=False)

        # new password works:
        self.login(self.username, new_password)



