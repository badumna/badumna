from django.core import mail
from django.conf import settings
import time

def expect_emails(action, num=1, args=(), **kwargs):
    initial_emails = len(mail.outbox)
    action(*args, **kwargs)
    err = None
    for tries in range(0, 5):
        try:
            new_emails = mail.outbox[initial_emails:]
            assert len(new_emails) == num, "Expected %s emails, but got %s" % (num, len(new_emails))
            return new_emails
        except AssertionError as err:
            time.sleep(0.2)
            continue
    raise err

def monitor_is_ok():
    with open(settings.STATISTICS_STATUS_FILE, 'r') as f:
        return f.read().strip() == 'OK'
