from __future__ import print_function
from __future__ import absolute_import
from django.conf import settings
import importlib
import sys
import os
import cgi
import itertools
import re
import traceback
import shutil
from .integration import base

expand_path = lambda p: os.path.abspath(os.path.realpath(p)).lower()
from nose.plugins import Plugin


# Hacky plugin to get `django-nose` to run tests from other apps (like vanilla django does)
class ThirdPartyTestsPlugin(Plugin):
    name = 'vendor-tests'
    SKIP_APPS = (
        'Site', # ours
        'ControlPanel', # ours
        'django_nose', # has a lot of things that look like tests (but aren't)
        'django.contrib.contenttypes', # FIXME: why do all these tests fail?
    )

    def configure(self, options, conf):
        super(ThirdPartyTestsPlugin, self).configure(options, conf)
        if not self.enabled:
            return

        self.app_paths = []
        for app in settings.INSTALLED_APPS:
            if app in self.SKIP_APPS: continue
            module = importlib.import_module(app)
            self.app_paths.append(expand_path(os.path.dirname(module.__file__)))
        # print "\n - ".join((self.app_paths))

    def wantDirectory(self, d):
        fullpath = expand_path(d)
        want = any(app_path.startswith(fullpath) for app_path in self.app_paths)
        # print>> sys.stderr, "want dir? " + repr(d) + " // " + repr(want)
        # print>> sys.stderr, fullpath
        return want

class ScreenshotLoggerPlugin(Plugin):
    name = 'screenshots'
    def configure(self, options, conf):
        super(ScreenshotLoggerPlugin, self).configure(options, conf)
        if not self.enabled:
            return
        self.html = None

    def addFailure(self, test, err):
        self._report(test, err)

    def addError(self, test, err):
        self._report(test, err)

    def _report(self, test, err):
        h = cgi.escape
        def p(s): print(s, file=self.html)
        def debug(s): print(s, file=sys.stderr)
        if self.html is None:
            self.html = open('test-results.html', 'w')
            p('''
            <html>
            <head>
            <title>Test errors</title>
            <style>
            ''')
            with open(os.path.join(os.path.dirname(__file__), '../../Site/static/bootstrap/css/bootstrap.min.css')) as boot:
                shutil.copyfileobj(boot, self.html)

            p('''
                body {
                    background-color: #ddd;
                    padding:2em;
                }
                img {
                    margin: 1em;
                    border: 10px solid #666;
                    max-width: 300px;
                }
                img:hover {
                    max-width: 100%;
                }

                pre {
                    display:inline-block;
                    font-size: 0.9em;
                    overflow:auto;
                    max-height:20em;
                }
                hr {
                    height: 1em;
                    background: #888;
                    margin-bottom:2em;
                }
                code {
                    white-space: pre;
                }
            </style>
            </head>
            <body>
            ''')
        else:
            p('<hr />')

        err_type, err_val, err_stack = err
        err_stack = "".join(traceback.format_tb(err_stack))

        def capture_marker(line):
            match = re.match('-+ >> (begin|end) captured ([\w ]+)', line)
            if not match: return None
            return match.group(2).strip()

        # debug("\n ## ".join(err_val.splitlines()))
        err_text = None
        captured_output = []
        current_capture_group = None
        for capture_group, content in itertools.groupby(str(err_val).splitlines(), capture_marker):
            if capture_group is not None:
                current_capture_group = capture_group
                continue
            content = '\n'.join(content)
            if current_capture_group is None:
                # debug("Error text: %r" % (content,))
                err_text = content
                continue
            # debug("Captured output: %r" % (content,))
            captured_output.append((current_capture_group, content))

        p('<p class="lead">Test failed: <strong>%s</strong></p>' % (test.id(),))
        p('<p class="alert alert-error"><strong>%s:</strong><br />' % h(err_type.__name__))
        if err_text:
            p('<code>%s</code>' % h(err_text))
        p('<h5>Backtrace:</h5><pre>%s</pre>' % (h(err_stack),))
        p('</p>')
        for label, content in captured_output:
            p("<h4>Captured %s:</h4>" % (h(label)))
            p("<pre>%s</pre>" % (h(content),))
        if base.SELENIUM:
            try:
                screenshot = base.SELENIUM.get_screenshot_as_base64()
            except Exception as e:
                p('<p>(Couldn\'t capture screenshot: %s)</p>' % (h(str(e)),))
            else:
                p('<p><img src="data:image/png;base64,%s"/></p>' % (screenshot,))

    def finalize(self, *a):
        if self.html:
            print("</body></html>", file=self.html)
            print("Test failures (with screenshots) saved to:\n  %s" % "file://%s" % (os.path.abspath(self.html.name).replace("\\","/")), file=sys.stderr)
            self.html.close()
            self.html = None

