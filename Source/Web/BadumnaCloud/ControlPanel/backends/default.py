import urllib2
import json

from registration.backends.default import DefaultBackend
from django.contrib.auth import login
from django.conf import settings

from ControlPanel.models import Application            

import logging
LOGGER = logging.getLogger(__name__)

def subscribe_user_to_newsletter(email):
    import createsend as CS
    LOGGER.info("Subscribing user to newsletter: %s", email)
    CS.CreateSend.api_key = settings.CAMPAIGN_MONITOR['API_KEY']
    subscriber = CS.Subscriber()
    try:
        subscriber.add(
            list_id = settings.CAMPAIGN_MONITOR['LIST_ID'],
            email_address = email,
            name = None,
            custom_fields = settings.CAMPAIGN_MONITOR['CUSTOM_FIELDS'],
            resubscribe = True,
        )
    except:
        LOGGER.error("Error subscribing %s to newsletter:", email, exc_info=True)
        raise

# Override the default registration and but without making use of a username
# (a bit hacky, but slightly better than copy-and-pasting their implementation)
class BadumnaBackend(DefaultBackend):
    def register(self, request, **kwargs):
        kwargs['username'] = None
        if kwargs['join_newsletter']:
            subscribe_user_to_newsletter(kwargs['email'])
        newUser = super(BadumnaBackend, self).register(request, **kwargs)
        if request.META.has_key('HTTP_USER_AGENT') and request.META['HTTP_USER_AGENT'] == "Unity_Editor":
            instance = Application()
            instance.user = newUser
            instance.name = "unity-demo"
            instance.save()
            request.session['key'] = instance.key
        return newUser

    def activate(self, request, **kwargs):
        activated = super(BadumnaBackend, self).activate(request, **kwargs)
        if activated:
            activated.backend = 'django.contrib.auth.backends.ModelBackend'
            login(request, activated)
        return activated

    def post_activation_redirect(self, request, user):
        """
        Return the name of the URL to redirect to after successful
        account activation.
        
        """
        return ('ControlPanel.list_apps', (), {})


