from django.conf.urls import *
from django.views.generic import TemplateView

from registration.views import activate
from registration.views import register
from ControlPanel.forms import BadumnaRegistrationForm
from ControlPanel.views import resend_activation_email
from ControlPanel.views import post_registration

urlpatterns = patterns('',
    url(r'^activate/(?P<activation_key>\w+)/$',
        activate,
        {'backend': 'ControlPanel.backends.default.BadumnaBackend'},
        name='registration_activate'),
    url(r'^register/$',
        register,
        {
            'backend': 'ControlPanel.backends.default.BadumnaBackend',
            'form_class': BadumnaRegistrationForm,
        },
        name='registration_register'),
    url(r'^register/complete/$',
        post_registration,
        name='registration_complete'),
    url(r'^register/send-activation/$',
        resend_activation_email,
        name='registration_resend_activation'),
    # url(r'^register/closed/$',
    #     TemplateView.as_view(template_name='registration/registration_closed.html'),
    #     name='registration_disallowed'),
    (r'^', include('registration.auth_urls')),
)
