(function($) {
    window.initStatusGraphs = function initStatusGraphs(query_url, update_interval, history_length) {
        var graph_container = null;
        var request = null;
        var timeFormat = "%h:%M%p";
        var warning = null;
        var warning_elem = null;
        var currentData = null;

        var mainColor = "rgb(85, 160, 230)";
        var baseOptions = {
            series: {
                lines: { show: true, fill: true },
            },
            colors: [mainColor],
            legend: { show: false },
            yaxis: { min: 0, tickDecimals: 0, show:true},
            grid: {
                show: true,
                hoverable: true,
                backgroundColor: { colors: ["#fff", "#eee"] }
            }
        };

        var requestFailed = function requestFailed(err) {
            debug.error(err);
            warning = "Error: Server not responding"
            updateWarning();
        };

        var dateFromTimestamp = function(timestamp) {
            var date = new Date(0);
            date.setUTCSeconds(timestamp);
            return date;
        };

        var toFakeGMT = function(date) {
            // We take a *local* date, and shift it into GMT (without changing the actual wall-time).
            // Flot renders all dates at UTC, but we want localtime displayed.
            return date.getTime() - (date.getTimezoneOffset() * 60 * 1000); // getTimezoneOffset returns minutes
        };

        var updateWarning = function updateWarning() {
            if (warning) {
                warning_elem.text(warning).slideDown();
            } else {
                warning_elem.slideUp();
            }
        };

        var plotData = function(data) {
            var series = $.map(data.series, function(pair) {
                var timestamp = pair[0], value=pair[1];
                // convert timestamp to javascript time in the user's timezone
                var date = dateFromTimestamp(timestamp);
                // debug.debug("got value " + value + " at time " + date);
                timestamp = toFakeGMT(date);
                return [[timestamp, value]];
            });

            var maxDate = toFakeGMT(new Date());


            var minDate = maxDate - (1000 * (update_interval * (history_length - 2)));
            var options = $.extend({}, baseOptions, {
                xaxis: {
                    mode: "time",
                    show: true,
                    min: minDate,
                    max: maxDate,
                    timeformat: timeFormat,
                    twelveHourClock: true
                }
            });

            series = {data: series};

            debug.debug("Graphing", [series], " with opts ", options, " into container ", graph_container.get());
            $.plot(graph_container, [series], options);
        }

        var onDataReceived = function onDataReceived(data) {
            debug.log("onDataReceived: ", data);
            currentData = data;
            warning = data.warning;
            plotData(data);
            updateWarning();
        };

        var redraw = function redraw() {
            if (currentData == null) return;
            plotData(currentData);
        };
        
        var fetchData = function fetchData() {
            if (request != null) {
                try {
                    request.abort();
                } catch (e) {
                    // we don't really care, we just abort for politeness
                }
                request = null;
            }

            request = $.ajax(query_url, {
                type: 'GET',
                dataType: 'json',
                success: onDataReceived,
                error: requestFailed
            });

        };

        $(function() {
            // Run when page is ready
            graph_container = $("#graph-live");
            warning_elem = $("#graph-warning");
            var tooltip = $("<span class=\"label label-inverse\">");
            tooltip.css({
                position: 'absolute',
                display: 'none',
                opacity: 0.80
            }).appendTo("body");

            graph_container.bind("plothover", function (event, pos, item) {
                if (item && item.seriesIndex > 0) {
                    var date = dateFromTimestamp(item.datapoint[0] / 1000);
                    console.log(item);
                    tooltip.text(item.datapoint[1].toFixed(0) + " sessions at " + $.plot.formatDate(date, timeFormat));
                    tooltip.css({
                        left: pos.pageX + 20,
                        top: pos.pageY + 15
                    });
                    tooltip.show();
                } else {
                    tooltip.hide();
                }
            });

            fetchData(1);
            
            // redraw the graph at least every 5 seconds, but only update the backing data at least every 30s
            var redraw_interval = Math.min(update_interval / 2, 5);
            setInterval(redraw, redraw_interval * 1000);

            var data_reload_interval = Math.min(update_interval / 2, 30);
            setInterval(fetchData, data_reload_interval * 1000);
        });
    }

})(jQuery);
