from __future__ import absolute_import
import logging
import threading
import itertools
import time
import contextlib
import urllib, urllib2
from xml.etree import ElementTree as Tree
from ..models import Application
from ..util import datetime

LOGGER = logging.getLogger(__name__)

class StatsMonitor(object):
    def __init__(self, url, wait_time, history_length):
        self.url = url
        self.wait_time = wait_time
        self.history_length = history_length
        self._end = threading.Event()
        self._thread = None
    
    def start(self):
        assert not self.running, "Already running!"
        LOGGER.info("start()")
        self._end.clear()
        self._thread = threading.Thread(target=self._run, name=type(self).__name__)
        self._thread.daemon = True
        self._thread.start()

    def _write_status(self, ok):
        with open(settings.STATISTICS_STATUS_FILE, 'w') as f:
            f.write(('OK' if ok else 'FAIL') + "\n")

    def _error(self):
        self._write_status(ok=False)

    def _ok(self):
        self._write_status(ok=True)

    @property
    def running(self):
        return self._thread and self._thread.is_alive()

    def stop(self, wait=True):
        LOGGER.info("stop() called")
        self._end.set()
        if wait and self._thread:
            LOGGER.info("waiting...")
            self._thread.join()
        LOGGER.info("stop() complete")

    def _run(self):
        self._ok()
        while True:
            if MONITOR is not self:
                LOGGER.warn("orphan monitor detected - aborting")
                return
            if self._end.is_set():
                LOGGER.info("self._end is set; stopping")
                return
            try:
                self._run_one()
            except urllib2.URLError as e:
                LOGGER.error("Error updating statistics from %s: %s", self.url, e)
                self._error()
            except StandardError:
                LOGGER.error("Error updating statistics from %s:", self.url, exc_info=True)
                self._error()
            else:
                self._ok()
            sleep_seconds = self.wait_time.total_seconds()
            LOGGER.info("sleeping for %s seconds...", sleep_seconds)
            time.sleep(sleep_seconds)

    def _run_one(self):
        xml = self._fetch()
        self._update(xml)

    def _fetch(self):
        LOGGER.info("requesting: %s", self.url)
        xml = None
        with contextlib.closing(urllib2.urlopen(self.url)) as response:
            xml = Tree.fromstring(response.read())

        if LOGGER.isEnabledFor(logging.DEBUG):
            LOGGER.debug("got status XML: %r", Tree.tostring(xml, encoding='utf-8'))

        return xml
        
    def _update(self, xml):
        now = datetime.utcnow()
        trailing_edge = now - (self.wait_time * self.history_length)
        def too_old(pair):
            return pair[0] < trailing_edge

        scene_users = {}
        scenes = xml.find("Scenes")
        if scenes is None:
            LOGGER.warn("Stats server returned a payload with NO active scenes. This is suspicious")
        else:
            for elem in scenes.findall("Scene"):
                scene_users[elem.find("Name").text] = int(elem.find("Sessions").text)
        LOGGER.debug("there are %s active scenes", len(scene_users))

        for app in Application.objects.all():
            session_history = app.get_session_history()
            items = session_history.items
            # LOGGER.debug("existing user history for %s: %s", app, items)
            current_value = scene_users.get(app.key, 0)
            items = list(itertools.dropwhile(too_old, items))
            items.append((now, current_value))
            session_history.active_sessions = current_value
            # LOGGER.debug("new user history for %s: %s", app, items)
            session_history.items = items
            session_history.save()


def create_monitor():
    '''create a monitor with the default settings'''
    url = 'http://%s:%s/' % (settings.STATISTICS_SERVER_HOST_INTERNAL, settings.STATISTICS_SERVER_PORT)
    return StatsMonitor(
            url=url,
            wait_time=settings.STATISTICS_REFRESH_INTERVAL,
            history_length=settings.STATISTICS_HISTORY_LENGTH + 4)

from django.conf import settings
MONITOR = None

if not (settings.TEST or getattr(settings, 'STATIC', False)):
    import sys, os
    if not sys.platform.startswith('win'):
        import fcntl
        # we can't just open __file__ here, because __file__ can be either the .py or the .pyc
        _self = open(os.path.join(os.path.dirname(__file__), 'stats_monitor.py'), 'r')
        try:
            fcntl.flock(_self, fcntl.LOCK_EX | fcntl.LOCK_NB)
        except IOError:
            # could not acquire lock
            logging.warn("PID %s could not acquire lock - not starting monitor", os.getpid())
            _self.close()
        else:
            logging.warn("PID %s acquired lock - starting monitor", os.getpid())
            MONITOR = create_monitor()
            MONITOR.start()
    else: # win32
        MONITOR = create_monitor()
        MONITOR.start()

