from __future__ import absolute_import
from django.shortcuts import render_to_response, redirect
from django.template import RequestContext
from django.conf import settings
from django.http import HttpResponse, Http404
from django.contrib.auth import decorators
from django.views.decorators.http import require_POST
from registration.models import RegistrationProfile

import urllib, urllib2
import contextlib
import logging
import json
from xml.etree import ElementTree as Tree
from datetime import datetime, timedelta
import time
from .models import Application
from .forms import ApplicationForm
from .background import stats_monitor
from .util.datetime import to_unix_time
LOGGER = logging.getLogger(__name__)

def login_required(fn):
    wrap_active_users = decorators.user_passes_test(lambda u: u.is_active)(fn)
    wrap_login_required = decorators.login_required(wrap_active_users)
    return wrap_login_required

def index(request):
    if request.user.is_authenticated():
        return redirect('ControlPanel.list_apps')
    return redirect('login')

@login_required
def status(request, app_key):
    app = Application.objects.get_for(request.user, key=app_key)
    return render_to_response('ControlPanel/status.html',
            {
                'app':app,
                'STATISTICS_REFRESH_INTERVAL': int(settings.STATISTICS_REFRESH_INTERVAL.total_seconds()),
                'STATISTICS_HISTORY_LENGTH': settings.STATISTICS_HISTORY_LENGTH,
            }, RequestContext(request))

@login_required
def status_json(request, app_key):
    app = Application.objects.get_for(request.user, key=app_key)
    response = {}
    response['series'] = [(to_unix_time(timestamp), val) for timestamp, val in app.get_session_history().items]
    LOGGER.debug("responding with %r", response)
    return HttpResponse(json.dumps(response), mimetype="application/json")

@login_required
def list_apps(request):
    user_apps = Application.objects.filter(user=request.user).order_by('id')
    return render_to_response('ControlPanel/apps.html', {'apps':user_apps}, RequestContext(request))

@login_required
def create_app(request):
    if request.method == 'POST':
        form = ApplicationForm(request.POST)
    else:
        form = ApplicationForm()
    if form.is_valid():
        instance = form.save(commit=False)
        instance.user = request.user
        instance.save()
        return redirect('ControlPanel.list_apps')
    else:
        return render_to_response(
                'ControlPanel/create_app.html',
                {'form': form},
                RequestContext(request))

@require_POST
@login_required
def delete_app(request):
    try:
        app = Application.objects.get_for(request.user, key=request.POST['key'])
    except Application.DoesNotExist as e:
        raise Http404
    LOGGER.debug("Deleting app: %r", app)
    app.delete()
    return redirect('ControlPanel.list_apps')


def app_config(request, app_key):
    try:
        app = Application.objects.get(key=app_key)
    except Application.DoesNotExist as e:
        raise Http404
    ctx = dict(
        app=app,
        BADUMNA_NETWORK=settings.BADUMNA_NETWORK
    )
    return render_to_response('ControlPanel/config.xml', ctx, mimetype="text/xml")

def resend_activation_email(request):
    from django.contrib.auth import get_user_model
    User = get_user_model()
    from django.contrib.sites.models import get_current_site
    try:
        user = User.objects.get(email=request.GET['email'])
        if user.is_active:
            raise Http404
        RegistrationProfile.objects.get(user=user).send_activation_email(get_current_site(request))
    except (User.DoesNotExist, RegistrationProfile.DoesNotExist): raise Http404
    return redirect('registration_complete')

def post_registration(request):
    key = request.session.get('key', '')
    if request.META.has_key('HTTP_USER_AGENT') and request.META['HTTP_USER_AGENT'] == "Unity_Editor":
        return HttpResponse('key:' + key, content_type="text/plain")
    else:
        return render_to_response('registration/registration_complete.html', RequestContext(request))        

