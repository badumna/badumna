import logging
from django.contrib import admin
from ControlPanel.models import BadumnaUser, Application, SessionHistory
from django.contrib.admin import SimpleListFilter
from registration.models import RegistrationProfile
from registration.admin import RegistrationAdmin

from django.contrib.auth.models import Group
admin.site.unregister(Group)
admin.site.unregister(RegistrationProfile)

# make the admin site use our own login page:
from ControlPanel.views import login_required
admin.site.login = login_required(admin.site.login)

class IntRangeFilter(SimpleListFilter):
    def queryset(self, request, queryset):
        limits = self.value()
        kw = {}
        if limits is None:
            return queryset

        if limits == '0':
            kw[self.parameter_name] = 0
        else:
            if limits.endswith('+'):
                start = int(limits[:-1])
            else:
                start, end = limits.split('-')
                kw[self.parameter_name + '__lt'] = end
            kw[self.parameter_name + '__gte'] = start
        return queryset.filter(**kw)

class ActiveSessionsFilter(IntRangeFilter):
    title='active sessions'
    parameter_name = '_session_history__active_sessions'
    def lookups(self, request, model_admin):
        return (
            ('0', 'No sessions'),
            ('1+', 'At least one'),
            ('1-10', '1-10'),
            ('10-100', '10-100'),
            ('100+', '100+'),
        )

class BadumnaUserAdmin(admin.ModelAdmin):
    list_display=('email', 'application__count', 'is_active', 'is_admin')
    list_filter=('is_active','is_admin')
    search_fields=['email']

    def application__count(self, user):
        return Application.objects.filter(user=user).count()

admin.site.register(BadumnaUser, BadumnaUserAdmin)

class ApplicationAdmin(admin.ModelAdmin):
    list_display=('key', 'name', 'active_sessions', 'user', 'session_history_id')
    list_filter=(ActiveSessionsFilter,)
    list_select_related=True
    search_fields=['user__email','name','key','session_history_id']
    def lookup_allowed(self, key, value):
        return (
            key.startswith('_session_history__') or
            super(ApplicationAdmin, self).lookup_allowed(key, value)
        )

    def active_sessions(self, app):
        return app.get_session_history().active_sessions

    def session_history_id(self, app):
        return app.get_session_history().id

admin.site.register(Application, ApplicationAdmin)

class SessionHistoryAdmin(admin.ModelAdmin):
    list_display=('id', 'active_sessions')
    search_fields=['id']
admin.site.register(SessionHistory, SessionHistoryAdmin)


class BadumnaRegistrationAdmin(RegistrationAdmin):
    list_display=('user','is_active', 'activation_key_expired')
    search_fields=[]
    # list_filter=('user','activation_key_expired')
    def is_active(self, profile):
        return profile.user.is_active
    is_active.boolean=True

admin.site.register(RegistrationProfile, BadumnaRegistrationAdmin)
