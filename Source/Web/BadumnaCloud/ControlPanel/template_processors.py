from django.core.urlresolvers import reverse
LOGGED_IN_MENU = [
        {
            'view': 'ControlPanel.list_apps',
            'text': 'Applications',
        },
]

ANONYMOUS_MENU = [
        {
            'view': 'registration_register',
            'class': 'signup',
            'text': 'Sign up',
        },
        {
            'view': 'login',
            'class': 'login',
            'text': 'Log in',
        },
]

def navigation(request):
    menu = []
    menu_src = LOGGED_IN_MENU if request.user.is_authenticated() else ANONYMOUS_MENU
    for item in menu_src:
        item = item.copy()
        item['url'] = reverse(item['view'])
        item['active'] = item['url'] == request.path
        menu.append(item)
    return {
            'NAVIGATION_MENU': menu,
    }

