from __future__ import absolute_import
import logging
import urllib
from django.forms import Form
from django import forms
from django.forms.util import ErrorList
from django.utils.html import format_html
from django.utils.translation import ugettext_lazy as _
from registration.forms import RegistrationForm, RegistrationFormUniqueEmail
from django.core.urlresolvers import reverse
from django.conf import settings
from django.contrib.auth import get_user_model
User = get_user_model()
LOGGER = logging.getLogger(__name__)

from .models import Application

class CanResendActivation():
    '''A custom error class that renders as a link to resend your activation email'''
    def __init__(self, email):
        self.email = email

    def __unicode__(self):
        return format_html(
            'Click to <a href="{url}" class="resendActivation">re-send your activation email</a>.',
            url=reverse('registration_resend_activation') + "?email=" + urllib.quote(self.email)
        )

    def __repr__(self): return "CanResendActivation(%r)" % (self.email,)
    def __eq__(self, other): return type(other) == type(self) and other.email == self.email
    def __ne__(self, other): return not self.__eq__(other)
    def __hash__(self): return hash(self.email)


class BadumnaRegistrationForm(RegistrationForm):
    agree_terms_and_conds = forms.BooleanField(
            required=True,
            label='I agree to the <a href="/terms-and-conditions">Terms & Conditions</a>',
            error_messages={'required': 'You must agree to the Terms & Conditions'})
    join_newsletter = forms.BooleanField(required=False, label='Subscribe to the newsletter')

    def __init__(self, *args, **kwargs):
        '''Override the default form, removing the username field'''
        super(BadumnaRegistrationForm, self).__init__(*args, **kwargs)
        del self.fields['username']

    def clean_email(self):
        '''
        Returns an error if the email address is used. If
        the email is used but not yet activated, includes a second error
        with a "resend activation email" link.
        '''
        email = self.cleaned_data['email']
        email_condition = dict(email__iexact=email)
        try:
            user = User.objects.get(**email_condition)
        except User.DoesNotExist:
            return self.cleaned_data['email']
        else:
            errors = [_("This email address is already in use.")]
            if not user.is_active:
                errors.append(CanResendActivation(email))
            raise forms.ValidationError(errors)

from django.contrib.auth.forms import AuthenticationForm
class BadumnaLoginForm(AuthenticationForm):
    def clean(self):
        try:
            return super(BadumnaLoginForm, self).clean()
        except forms.ValidationError as error:
            # slightly hacky: only check for a resend email option if we get the "inactive user" error message
            if self.error_messages['inactive'] in error.messages:
                email = self.cleaned_data['username']
                try:
                    user = User.objects.get(email__iexact=email, is_active=False)
                except User.DoesNotExist:
                    pass
                else:
                    error.messages.append(CanResendActivation(email))
            raise error

class ApplicationForm(forms.ModelForm):
    class Meta:
        model = Application

