from __future__ import absolute_import
from django.conf.urls import patterns, include, url
from django.views.generic import TemplateView

from .forms import BadumnaLoginForm

app_key_match = '(?P<app_key>[^/]+)'
urlpatterns = patterns('',
    url(r'^$', 'ControlPanel.views.index', name='index'),
    url(r'^apps/$', 'ControlPanel.views.list_apps', name='ControlPanel.list_apps'),
    url(r'^apps/create/$', 'ControlPanel.views.create_app', name='ControlPanel.create_app'),
    url(r'^apps/delete/$', 'ControlPanel.views.delete_app', name='ControlPanel.delete_app'),
    url(r'^apps/' + app_key_match + '/$', 'ControlPanel.views.status', name='ControlPanel.status'),
    url(r'^apps/' + app_key_match + '/status.json$', 'ControlPanel.views.status_json', name='ControlPanel.status.json'),

    # override the default django login view  to use our custom form
    url(r'^account/login/$', 'django.contrib.auth.views.login',
        {'authentication_form': BadumnaLoginForm}, name='login'),

    (r'^account/', include('django.contrib.auth.urls')),
    (r'^account/', include('ControlPanel.account_urls')),

)
