# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'BadumnaUser'
        db.create_table(u'ControlPanel_badumnauser', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('password', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('last_login', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now)),
            ('email', self.gf('django.db.models.fields.EmailField')(unique=True, max_length=255, db_index=True)),
            ('is_active', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('is_admin', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('date_joined', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'ControlPanel', ['BadumnaUser'])

        # Adding model 'SessionHistory'
        db.create_table(u'ControlPanel_sessionhistory', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('raw_data', self.gf('django.db.models.fields.CharField')(max_length=1000, null=True)),
        ))
        db.send_create_signal(u'ControlPanel', ['SessionHistory'])

        # Adding model 'Application'
        db.create_table(u'ControlPanel_application', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['ControlPanel.BadumnaUser'])),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('_session_history', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['ControlPanel.SessionHistory'], unique=True, null=True)),
        ))
        db.send_create_signal(u'ControlPanel', ['Application'])


    def backwards(self, orm):
        # Deleting model 'BadumnaUser'
        db.delete_table(u'ControlPanel_badumnauser')

        # Deleting model 'SessionHistory'
        db.delete_table(u'ControlPanel_sessionhistory')

        # Deleting model 'Application'
        db.delete_table(u'ControlPanel_application')


    models = {
        u'ControlPanel.application': {
            'Meta': {'object_name': 'Application'},
            '_session_history': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['ControlPanel.SessionHistory']", 'unique': 'True', 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['ControlPanel.BadumnaUser']"})
        },
        u'ControlPanel.badumnauser': {
            'Meta': {'object_name': 'BadumnaUser'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'unique': 'True', 'max_length': '255', 'db_index': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_admin': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        },
        u'ControlPanel.sessionhistory': {
            'Meta': {'object_name': 'SessionHistory'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'raw_data': ('django.db.models.fields.CharField', [], {'max_length': '1000', 'null': 'True'})
        }
    }

    complete_apps = ['ControlPanel']