# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Application.key'
        db.add_column(u'ControlPanel_application', 'key',
                      self.gf('django.db.models.fields.CharField')(default=None, max_length=10, unique=True, null=True, db_index=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Application.key'
        db.delete_column(u'ControlPanel_application', 'key')


    models = {
        u'ControlPanel.application': {
            'Meta': {'object_name': 'Application'},
            '_session_history': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['ControlPanel.SessionHistory']", 'unique': 'True', 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'key': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '10', 'unique': 'True', 'null': 'True', 'db_index': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['ControlPanel.BadumnaUser']"})
        },
        u'ControlPanel.badumnauser': {
            'Meta': {'object_name': 'BadumnaUser'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'unique': 'True', 'max_length': '255', 'db_index': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_admin': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        },
        u'ControlPanel.sessionhistory': {
            'Meta': {'object_name': 'SessionHistory'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'raw_data': ('django.db.models.fields.CharField', [], {'max_length': '1000', 'null': 'True'})
        }
    }

    complete_apps = ['ControlPanel']