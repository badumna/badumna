# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import DataMigration
from django.db import models

from ControlPanel.models import Application
class Migration(DataMigration):

    def forwards(self, orm):
        for app in orm.Application.objects.all():
            if app.key is None:
                print "Generating key for app: %s" %(app.id,)
                app.key = Application.objects.random_key()
                app.save()

    def backwards(self, orm):
        pass

    models = {
        u'ControlPanel.application': {
            'Meta': {'object_name': 'Application'},
            '_session_history': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['ControlPanel.SessionHistory']", 'unique': 'True', 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'key': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '10', 'unique': 'True', 'null': 'True', 'db_index': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['ControlPanel.BadumnaUser']"})
        },
        u'ControlPanel.badumnauser': {
            'Meta': {'object_name': 'BadumnaUser'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'unique': 'True', 'max_length': '255', 'db_index': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_admin': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        },
        u'ControlPanel.sessionhistory': {
            'Meta': {'object_name': 'SessionHistory'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'raw_data': ('django.db.models.fields.CharField', [], {'max_length': '1000', 'null': 'True'})
        }
    }

    complete_apps = ['ControlPanel']
    symmetrical = True
