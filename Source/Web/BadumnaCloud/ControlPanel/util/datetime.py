from __future__ import absolute_import
import time
import calendar
from datetime import timedelta, datetime
utcnow = datetime.utcnow

# note that all times are stored / treated as UTC, so that we can deserialize them in javascript
# and they will be converted properly to the user's time zone

def to_unix_time(time_obj):
    return int(calendar.timegm(time_obj.utctimetuple()))

from_unix_time = datetime.utcfromtimestamp

