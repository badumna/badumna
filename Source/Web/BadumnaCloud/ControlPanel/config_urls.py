# separated into its own file so that it can be rooted at /config, not /cp/config

from __future__ import absolute_import
from django.conf.urls import *
from .urls import app_key_match

urlpatterns = patterns('',
    url(r'^' + app_key_match + '$', 'ControlPanel.views.app_config', name='ControlPanel.config'),
)
