RUNNING:

  ./django.sh runserver
  OR
  python ./manage.py runserver

Instead of runserver, you might also like:

 - test (alias for `test unit`)
 - test all
 - test integration
 - test --with-vendor-tests (run tests from installed django apps)

CHANGING DEPENDENCIES:

 - modify requirements.txt
 (deps will be updated the next time you run django)

REINSTALLING DEPENDENCIES:

 > python install.py
