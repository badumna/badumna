#!/bin/env python

import sys
import argparse
import logging
import signal
import subprocess
from collections import OrderedDict

DRY_RUN = True

signal.signal(signal.SIGINT, signal.SIG_IGN)  # The default sigint handler seems to screw with raw_input


def fab(command, server_args=None):
    server = 'production'
    if server_args:
        server += ':' + server_args

    logging.debug('Executing "{}" on "{}"'.format(' '.join(command), server))
    if not DRY_RUN:
        subprocess.check_call(['fab', '-f', 'fabfile.py', server] + command)

def reboot(name, server_args=None):
    fab(['{}.stop'.format(name), 'reboot:roles={}'.format(name)], server_args)
    fab(['{}.start'.format(name), '-n', '9'], server_args)

def reboot_cloud_inactive():
    fab(['reboot:roles=web'])
    fab(['web.start_apache', '-n', '9'])  # Apache actually starts automatically, but this lets us ensure that it's back up.

def swap_active():
    prompt_to_continue(
        'About to swap the active and inactive cloud web servers.\nMake sure the inactive server is working and up-to-date!',
        expected='cloud-inactive is peachy!')
    fab(['quick', 'web.swap_active'])

def reboot_cloud_active():
    swap_active()
    reboot_cloud_inactive()
    swap_active()

def prompt_to_continue(message, expected='yes'):
    logging.info(message)

    if DRY_RUN:
        logging.debug('Dry run, continuing automatically.')
        return

    while True:
        logging.info('Enter "{}" to continue, or ^C to abort'.format(expected))
        if raw_input('--> ') == expected:
            break

servers = OrderedDict([
    ('seed0', lambda: reboot('seed', 'seed=0')),
    ('seed1', lambda: reboot('seed', 'seed=1')),
    ('stats', lambda: reboot('stats')),
    ('matchmaking', lambda: reboot('matchmaking')),
    ('cloud-inactive', reboot_cloud_inactive),
    ('cloud-active', reboot_cloud_active),
    ('monitoring', lambda: reboot('monitoring')),
])

def main(to_reboot=[], dry_run=True, log_level=logging.INFO):
    if len(to_reboot) == 0:
        to_reboot = servers.keys()

    global DRY_RUN
    DRY_RUN = dry_run

    logging.getLogger().setLevel(log_level)

    logging.info('Rebooting {} ({}dry run)'.format(', '.join(to_reboot), '' if DRY_RUN else 'NOT '))

    done = []

    try:
        prompt_to_continue('Stop the aa_crashmail task at http://badumna-monitor.cloudapp.net/', expected='done')
    
        for name in to_reboot:
            prompt_to_continue('Ready to reboot "{}"?'.format(name), expected='yes')
            servers[name]()
            done.append(name)
    except (EOFError):
        logging.warn('Aborting...')

    if len(done) > 0:
        logging.info('Successfully rebooted: ' + ' '.join(done))

    remaining = to_reboot[len(done):]
    if len(remaining) > 0:
        logging.warn('Yet to reboot: ' + ' '.join(remaining))

    logging.info('Don''t forget to restart the "reboot required" and "aa_crashmail" tasks on the monitoring machine if required!')

if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO, format='%(message)s')

    parser = argparse.ArgumentParser(description='Reboot cloud servers.')
    parser.add_argument('--dry-run', '-n', action='store_true', help="don't actually reboot, just show what would happen")
    parser.add_argument('--verbose', '-v', action='store_true', help="print debugging information")
    parser.add_argument('server', nargs='*', help='the servers to reboot, leave empty for all servers, specify a range with ".." (e.g. "stats.." would reboot the stats server onwards)')
    args = parser.parse_args()

    def validate_server(name):
        if len(name) > 0 and name not in servers:
            logging.error("Unknown server: " + name)
            exit(1)

    to_reboot = []
    for name in args.server:
        if '..' in name:
            start, end = name.split('..')
            validate_server(start)
            validate_server(end)

            collecting = len(start) == 0
            for server in servers.keys():
                if server == start:
                    collecting = True
                if collecting:
                    to_reboot.append(server)
                if server == end:
                    break
        else:
            validate_server(name)
            to_reboot.append(name)

    main(to_reboot, args.dry_run, logging.DEBUG if args.verbose else logging.INFO)
