#!/usr/bin/env python

# This file contains all the status checks we run on the servers.
# Each toplevel function should check some desired condition.
# When the condition is false, the function should raise an exception.

import sys, os
import local_env
local_env.init()

import time
import subprocess
import logging
import itertools
from datetime import datetime, timedelta
import re

DELAY_SECS = 120
SSH_SOCKET_ROOT = os.environ['SSH_SOCKET_ROOT']
USER='monitor'

if not os.path.isdir(SSH_SOCKET_ROOT):
    os.makedirs(SSH_SOCKET_ROOT)

def _run_ssh(host, cmd, as_run_user=False):
    socket_path = _ssh_socket_path(host)

    if as_run_user:
        cmd = ['sudo','-u', RUN_USER, '--'] + cmd

    logging.info("running: %r", cmd)

    # SSH is incapable of passing through arguments cleanly, so we manually escape them for bash before giving them to SSH:
    cmdline = " ".join(map(bash_escape, cmd))

    cmd = ['ssh',
            '-o', 'ControlMaster=auto',
            '-o', 'ControlPersist=%s' % (DELAY_SECS + 30),
            '-o', 'ControlPath=' + socket_path,
            host,'--', cmdline]
    return log_call(subprocess.check_output, cmd).rstrip()

def log_call(action, arg1, *a, **k):
    logging.info("calling %s(%r)", action.__name__, arg1)
    return action(arg1, *a, **k)

def _ssh_socket_path(host):
    return os.path.join(SSH_SOCKET_ROOT, host)

def _kill_old_connections(host):
    logging.error("Killing active connections to %s", host)
    socket_path = _ssh_socket_path(host)

    # move the socket to a new path unique to our PID
    new_path = socket_path + "_dead_%s" % (os.getpid())
    try:
        os.rename(socket_path, new_path)
    except OSError:
        # we were pre-empted, the file doesn't exist
        return

    try:
        log_call(subprocess.check_call, ['ssh', '-o', 'ControlPath=' + new_path, '-O', 'exit', host])
    finally:
        os.remove(new_path)

def _get_pid(host, pidfile):
    out = _run_ssh(host, ['cat', pidfile])
    logging.info("pidfile contents: %s", out)
    return int(out)

def check_instance_id(host):
    '''
    Kills any mismatched SSH connections, where `mismatched` means that
    the host has been swapped since the SSH connection was made.
    '''
    # get the public IP of the host:
    current_ip = log_call(subprocess.check_output, ['dig', '+short', host]).strip()

    ssh_connection_ip = _run_ssh(host, ['curl', '--silent', '--show-error', 'http://instance-data/latest/meta-data/public-ipv4']).strip()

    logging.info("host %s is assigned IP %s, ssh is connected to IP %s", host, current_ip, ssh_connection_ip)
    if current_ip != ssh_connection_ip:
        # rename the file to prevent race conditions with other `check` processes using it
        logging.error("IP address has changed - killing all active sessions")
        _kill_old_connections(host)
        

def check_pid(host, pidfile, expected_exe = None):
    '''Check that the process named in `pidfile` is alive and is running `expected_exe` (if given)'''
    pid = _get_pid(host, pidfile)
    if expected_exe is not None:
        exe = _run_ssh(host, ['readlink', '-f', '/proc/%s/exe' % (pid,)], as_run_user=True)
        assert exe == expected_exe, "Expected %r, got %r" % (expected_exe, exe)

def check_udp_port(host, pidfile, port):
    '''Check that a given UDP port is being listened on by the process named in `pidfile`'''
    pid = _get_pid(host, pidfile)
    _run_ssh(host, ['lsof', '-p', str(pid), '-i', 'UDP:%s' % (port,)], as_run_user=True)

def _check_http_cmd(url):
    return ['curl', '--insecure', '--silent', '--show-error', '--fail', '--location', '--write-out', r'%{http_code} %{url_effective}\n', '--output', '/dev/null', url]

def check_internal_http(host, url):
    '''Runs `curl` on the remote server, for HTTP endpoints that aren't publically acessible'''
    _run_ssh(host, _check_http_cmd(url))

def check_http(url):
    '''runs `curl` locally to check that a URL gives us back a successful response code'''
    log_call(subprocess.check_call, _check_http_cmd(url))

def tail_log(host, path, filter=None):
    '''
    NOTE: this function does not return until it fails (i.e `pattern` is found or a connection erorr occurs).
    It *DOES NOT* check for historical data, it only captures the first log line
    that is written after it starts.
    '''
    filter = "" if filter is None else ("| " + filter)

    # first, check we have permission to tail this file (tail -F doesn't fail on permission error, sadly)
    _run_ssh(host, ['bash', '-c', 'tail -n0 "$1"', '-', path])

    # wait for an error to occur, then
    # get the last error line from the file
    try:
        contents = _run_ssh(host, ['bash', '-c','pid="$$"; tail -n0 -F "$1" {filter} | (head -n 1; kill "$pid")'.format(filter=filter), '-', path]).strip()
    except subprocess.CalledProcessError as err:
        contents = err.output.strip()
        if not contents:
            raise err

    raise AssertionError("error in log file %s (%s):\n%s\n(Note: there may be more errors, this check stops after the first)" %(path, host, contents))

def file_exists(host, path):
    contents = _run_ssh(host, ['bash', '-c', 'if test -e "$1"; then echo "YES"; else echo "NO"; fi', '-', path]).strip()
    assert contents in ('YES','NO'), "Unexpected response: %s" % (contents,)
    return contents == 'YES'

def check_status_file(host, path, max_age):
    '''
    Checks that a file contains the text 'OK', and that it
    was modifed less than `max_age` seconds ago
    '''
    contents = _run_ssh(host, ['cat', path]).strip()
    assert contents == 'OK', "Contents are not OK: %s" % (contents,)

    mtime = _run_ssh(host, ['stat','-c','%Y', path])
    mtime = int(mtime.strip())
    age = time.time() - mtime
    logging.info("status file is %s seconds old", age)
    if (age > max_age):
        raise AssertionError("status file is %s seconds old, maximum is %s" % (max_age,))

def check_reboot_required(host):
    assert not file_exists(host, '/var/run/reboot-required'), "Reboot required!"

def run_once(host, *command):
    '''
    Runs an SSH command a single time.
    The command is then automatically removed by the runner.
    '''
    _run_ssh(host, list(command), as_run_user=True)

def run_action(action, repeat=True):
    ssh_errors = 0
    while True:
        try:
            last_connect = datetime.now()
            action()
            ssh_errors = 0
            if not repeat: break
            logging.info("Check complete. Sleeping for %s seconds...", DELAY_SECS)
            time.sleep(DELAY_SECS)
        except subprocess.CalledProcessError as e:
            # SSH connections are killed and reinstated
            # when a host changes IP address. We should
            # retry if we get a connection error.
            if e.returncode == 255 and e.cmd[0] == 'ssh':
                ssh_errors += 1
                duration = datetime.now() - last_connect
                # if this was the first error (or the last connection lasted more than 120 seconds), retry
                if ssh_errors == 1 or duration > timedelta(seconds=120):
                    logging.warn("SSH connection error, retrying in 2 seconds...")
                    time.sleep(2)
                    continue
                else:
                    logging.warn("SSH connection failed too many times - aborting")
            raise

def make_runner(args):
    '''Splits a sequence of args into a sequence of commands.
    Returns a function that runs each of these commands
    in order.
    '''
    commands = []
    current_command = []
    groups = itertools.groupby(args, lambda arg: arg == ';')
    for is_semicolon, args in groups:
        args = list(args)
        if is_semicolon or not args: continue

        # the first arg must always be the name of a global function
        args[0] = globals()[args[0]]
        commands.append(args)
    assert len(commands) > 0, "No commands given!"

    def run():
        for command in commands:
            logging.info("Running check: %s", command[0].__name__)
            command[0](*command[1:])

        commands[:] = [c for c in commands if c[0] != run_once]


    return run

def bash_escape(s, inside=None):
    # from https://raw.github.com/gfxmonk/bash-escape/master/bash_escape.py
    s = re.sub('([\'\"\\?])', r'\\\1', s)
    s = s.replace('\t', r'\t')
    s = s.replace('\n', r'\n')
    s = s.replace('\r', r'\r')
    s = s.replace('\x0b', r'\x0b')
    s = s.replace('\x0c', r'\x0c')
    s = "$'%s'" % (s,)
    if inside:
        s = "%s%s%s" % (inside, s, inside)
    return s

if __name__ == '__main__':
    from optparse import OptionParser
    p = OptionParser()
    p.add_option('--forever', action='store_true', dest='repeat', default=True, help='repeat forever (default)')
    p.add_option('--once', action='store_false', dest='repeat', help='check once only')
    p.add_option('--delay', type='int', help='set delay (in seconds default %default)', default=DELAY_SECS)
    p.add_option('--run-user', default='badumna', help='the user who owns logs, processes, etc')
    p.add_option('-v', '--verbose', action='store_true', default=False, help='enable debug logging')
    opts, args = p.parse_args()
    logging.basicConfig(
            level=logging.DEBUG if opts.verbose else logging.INFO,
            format="%(asctime)s %(levelname)s: %(message)s")
    DELAY_SECS = opts.delay
    RUN_USER = opts.run_user
    logging.info("Called with arguments: %r", args)
    try:
        runner = make_runner(args)
    except (KeyError, IndexError, AssertionError) as e:
        print >> sys.stderr, "%s: %s" % (type(e).__name__, e)
        p.print_usage()
        sys.exit(1)
    try:
        run_action(runner, repeat=opts.repeat)
    except (StandardError, subprocess.CalledProcessError) as e:
        logging.fatal("%s: %s", type(e).__name__, e)
        sys.exit(1)
