from fabric.api import *

from badumna_fabric import stats, vcs, web, seed, matchmaking, common, monitoring
from badumna_fabric.common import *
from badumna_fabric.environments import *

COMPONENTS = [stats, web, seed, matchmaking, monitoring]

@task
def shell():
    run("bash")

@task
@roles(*ROLES)
def update_system_packages():
    apt_get("update")
    apt_get("upgrade")

@task
def status():
    '''Show the status of ALL available services'''
    ok = True
    web_statuses = [
        web.status_apache,
    ]

    # HACK: fabric execute() runs web.status_db on the WEB host if no DB host is
    # defined. That's wrong, so we just don't run it if there is no DB box.
    if env.roledefs[DB]:
        web_statuses.append(web.status_db)

    for task in [
            monitoring.status
            ] + web_statuses + [
            web.version,
            stats.status,
            stats.version,
            seed.status,
            seed.version,
            matchmaking.status,
            matchmaking.version,
        ]:
        try:
            execute(task)
        except AssertionError, e:
            ok = False
            print "FAILED: %s" % (e)
    if not ok:
        raise AssertionError("one or more services are not running (see above)")

def global_task(name, exclude=[], keep_going=True):
    '''Generates a global task that runs the task of the same name across all components'''
    def _task(*a, **k):
        ok = True
        for component in COMPONENTS:
            if component in exclude: continue
            print "Running: %s.%s" % (component.__name__, name)
            try:
                execute(getattr(component,name), *a, **k)
            except KeyboardInterrupt: raise
            except Exception as e:
                if not keep_going: raise
                ok = False
                print("%s: %s" % (type(e).__name__, e))
                print("Task failed (but keep_going is true)")
        assert ok, "One or more tasks failed - see above for errors"

    _task.__name__ = name
    _task.__doc__ = 'Run the `%s` task across all components' % name
    globals()[name] = task(_task)

global_task('deploy', keep_going=False)
global_task('version', exclude=[monitoring])
global_task('system_packages')
global_task('start', exclude=[web])
global_task('stop', exclude=[web])
global_task('erase', exclude=[monitoring])

@task
@roles(*ROLES)
def reboot_all():
    sudo('reboot')

@task
def reboot():
    '''Reboot hosts (with confirmation)
    (pass in `roles=<...>` to set which roles will be rebooted)'''
    assert prompt("About to REBOOT hosts: %r\nOK? [y\N]" % (env.all_hosts,)).lower().strip() == 'y', "Cancelled"
    sudo('reboot')

@task
@roles(*ROLES)
def update_ssh_keys():
    '''Update SSH authorized_keys (use this
    after changing `deployment/authorized_keys`)'''
    common.update_ssh_keys()

@task
@roles(*ROLES)
def sh_all(*a):
    assert len(a) > 0, "need at least one argument!"
    result = run_as(ADMIN_USER, " ".join(a), warn_only=True)

@task
def update_hostnames(role=None):
    '''
    Update /etc/external-hostname
    (You should not need to do this unless you've altered instance allocations manually)
    '''
    if role is not None:
        set_hostnames(env.roledefs[role])
    else:
        set_hostnames()

for role in ROLES:
    def sh(*a):
        assert len(a) > 0, "need at least one argument!"
        result = run_as(ADMIN_USER, " ".join(a), warn_only=True)
    sh.__name__ = 'sh_%s' % role
    globals()[sh.__name__] = task(roles(role)(sh))

@task
def tasks():
    from fabric import main
    import itertools
    import inspect
    def command_list(docstrings=True):
        result = []
        task_names = main._task_names(main.state.commands)
        max_len = reduce(lambda a, b: max(a, len(b)), task_names, 0) + 1
        last_namespace = None
        module_names = [m.__name__.split('.')[-1] for m in COMPONENTS]
        for name in task_names:
            namespace = name.rsplit('.', 1)[0]
            if namespace not in module_names: namespace = 'global'
            if namespace != last_namespace :
                result.append("\n" + (" %s tasks " % (namespace.upper(),)).center( max_len, "*"))
                last_namespace = namespace
            output = None

            fn = main.crawl(name, main.state.commands).wrapped
            docstring = main._print_docstring(docstrings, name)
            if docstring:
                lines = filter(lambda line: line.strip(), docstring.splitlines())

                argspec = inspect.getargspec(fn)
                if argspec.args:
                    lines.append("(optional arguments: %s)" % (", ".join(argspec.args),))

                #padding
                lines = [' ' + line.strip() for line in lines]
                # leading space on subsequent lines
                lines[1:] = [" " * max_len + line for line in lines[1:]]
                output = (name + " ").ljust(max_len, '-') + "\n".join(lines)
                result.append(output)
        return result
    print "\n".join(command_list())
