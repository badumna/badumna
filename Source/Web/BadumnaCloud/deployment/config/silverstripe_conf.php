<?php

global $project;
$project = 'mysite';

global $databaseConfig;
$databaseConfig = array(
        "type" => 'MySQLDatabase',
        "server" => '{{settings.DATABASES.default.HOST}}',
        "port" => '{{settings.DATABASES.default.PORT}}',
        "username" => '{{settings.DATABASES.default.USER}}',
        "password" => '{{settings.DATABASES.default.PASSWORD}}',
        "database" => '{{mysql_cms_db}}',
        "path" => '',
);

MySQLDatabase::set_connection_charset('utf8');

// Set the current theme. More themes can be downloaded from
// http://www.silverstripe.org/themes/
SSViewer::set_theme('badumna');

// Set the site locale
i18n::set_locale('en_US');

// Enable nested URLs for this site (e.g. page/sub-page/)
if (class_exists('SiteTree')) SiteTree::enable_nested_urls();

{% if admin_pass %}
Security::setDefaultAdmin("admin", "{{admin_pass}}");
{% endif %}

{% if dev_mode %}
Director::set_environment_type("dev");
{% endif %}
