import os
from fabric.api import env

def check_ssh_keys():
    from paramiko.agent import Agent
    keys = Agent().get_keys()
    if len(keys) == 0:
        print "WARNING: you have no SSH keys set up (in ssh-agent).\nThis almost certainly won't work.\nContinue anyway? [y/n]",
        assert raw_input().strip().lower() in ('y','')

def override_server_keys():
    # fabric doesn't give us any control over
    # server key management, so we have to modify
    # the underlying paramiko SSH library instead.
    from paramiko import client, hostkeys
    from paramiko.ssh_exception import SSHException
    from logging import DEBUG
    from binascii import hexlify

    # resolve hosts ONLY via our policy:
    env.reject_unknown_hosts = True
    env.disable_known_hosts = True

    original_class = client.MissingHostKeyPolicy
    if getattr(original_class, '_badumna_patched', False):
        print "Note: load_server_keys() called twice!"
        return

    keys_file = os.path.join(env.project_path, "deployment", "known_hosts")
    badumna_hosts = hostkeys.HostKeys(keys_file)
    class CustomRejectionPolicy(client.MissingHostKeyPolicy):
        _badumna_patched = True
        def missing_host_key(self, client, hostname, key):
            # HostKeys doesn't believe a host can have multiple keys,
            # so we can't just use the provided `lookup` function.
            if key is not None:
                all_keys = [entry.key for entry in badumna_hosts._entries]
                if key in all_keys:
                    # We found a matching host in local host_keys file.
                    # We didn't check that it is the *right* host, but being MITM'd by
                    # our own server is a pretty low risk...
                    return
                client._log(DEBUG, 'Rejecting %s host key for %s: %s' % (
                    key.get_name(),
                    hostname,
                    hexlify(key.get_fingerprint())
                ))
                print ('Key check failed. To accept this key, add the following line to %s:\n%s' % (
                    keys_file,
                    hostkeys.HostKeyEntry(hostnames=[hostname], key=key).to_line()
                ))
            raise SSHException('Server %r not found in known_hosts' % hostname)
    client.RejectPolicy = CustomRejectionPolicy

