from __future__ import absolute_import
import os
import tempfile
import shutil
import contextlib
import subprocess
from StringIO import StringIO
from fabric.api import *
from . import common
from . import vcs
from .common import *

def start_stop_cmd(service):
    return 'start-stop-daemon --verbose --pidfile="%s" --exec="/usr/bin/mono" ' % (service.pidfile,)

def start_cmd(service, oknodo, args):
    cmd = start_stop_cmd(service)
    if oknodo:
        cmd += "--oknodo "
    # make sure `pid` and `log` can be read by monitoring user
    cmd += " --umask=022 --background --make-pid --start --startas '%s' -- %s" % (service.wrapper_script, args)
    return cmd

def upload(service):
    '''Upload $ROLE service'''
    temp_dir, istemp = package_service(service)
    dest = service.bindir
    run_as(BADUMNA_USER, "rm -rf " + dest)
    run_as(BADUMNA_USER, "mkdir -p " + dest)
    try:
        putdir_as(BADUMNA_USER, temp_dir, dest)
    finally:
        if istemp:
            shutil.rmtree(temp_dir)
    create_wrapper_script(service)

    start_script =  '%s/start-%s.sh' % (env.home, service.package_name)
    put_as(BADUMNA_USER, StringIO("#!/bin/bash\n%s" % (start_cmd(service, oknodo=True, args=''),)), start_script)
    run_as(BADUMNA_USER, "chmod +x " + start_script)

    if service.use_network_config:
        create_config_xml(service)

def tail_log(service):
    '''Tail $ROLE logs'''
    run_as(BADUMNA_USER, "tail -n 100 -F %s" % service.logfile)

def stop(service, oknodo=False):
    '''Stop $ROLE peer'''
    cmd = start_stop_cmd(service) + "--stop --retry 10"
    if oknodo:
        cmd += " --oknodo"
    run_as(BADUMNA_USER, cmd)

def start(service, oknodo=False, args=''):
    '''Start $ROLE peer'''
    run_as(BADUMNA_USER, start_cmd(service, oknodo, args), pty=False)
    status(service)

def restart(service, args=''):
    '''Restart $ROLE peer'''
    stop(service, oknodo=True)
    start(service, args)

def status(service):
    '''Print status of $ROLE peer'''
    result = run_as(BADUMNA_USER, start_stop_cmd(service) + "--status", ok_quiet=True, warn_only=True)
    assert result.succeeded, "%s not running!" % (service.name,)
    print "%s running" % (service.name,)

def kill_mono_processes():
    run_as(BADUMNA_USER, "pkill mono")

def package_service(service):
    '''Returns (location, is_temp)'''
    root = get_server_package()
    if root is not None:
        return (os.path.join(root, service.package_name), False)

    temp_dir = tempfile.mkdtemp(prefix="BadumnaCloud-apps-")
    try:
        run_build_cmd(['package:BadumnaCloud:export[release]', '--', service.package_name, temp_dir])
        with open(os.path.join(temp_dir, "GIT_COMMIT"), 'w') as f:
            f.write("(WORKING COPY)\n")
        return (temp_dir, True)
    except:
        shutil.rmtree(temp_dir)
        raise

def killall_mono(*args_ignored):
    # Last-resort to kill any runaway mono processes
    kill_mono_processes()

def deploy(service):
    '''Perform a full $ROLE deploy'''
    system_packages(service)
    stop(service, oknodo=True)
    upload(service)
    start(service)

def system_packages(service):
    '''Install system packages for $ROLE instances'''
    common.install_system_packages_for(service.role)

def create_wrapper_script(service):
    vars = service.extend_vars(env)
    common.render_and_put(vars, "deployment/config/mono_wrapper.sh.tmpl", service.wrapper_script)
    run_as(BADUMNA_USER, "chmod +x " + service.wrapper_script)

def create_config_xml(service):
    path = os.path.join(env.project_path, "ControlPanel/templates/ControlPanel/config.xml")
    env.settings.BADUMNA_NETWORK['HOSTED_SERVICE'] = True
    common.render_and_put(env.settings, path, service.badumna_config)
    env.settings.BADUMNA_NETWORK['HOSTED_SERVICE'] = False

def create_wrapper(service_factory, role, globals):
    def wrap(fn, default=False):
        '''return a fabric task that wraps a mono_service function for the given service & role'''
        @contextlib.wraps(fn)
        def t(*a, **k):
            return fn(service_factory(), *a, **k)
        if t.__doc__:
            t.__doc__ = t.__doc__.replace('$ROLE', role)
        globals[fn.__name__] = task(default=default)(roles(role)(t))
    return wrap

def version(service):
    run_as(BADUMNA_USER, "cat %s/GIT_COMMIT" % (service.bindir,))
    run_as(BADUMNA_USER, "md5sum %s" % (service.exe,))

def erase(service):
    run_as(BADUMNA_USER, "rm -rf %s" % (service.basedir,))

class MonoService(object):
    def __init__(self,
            basedir,
            exe,
            name,
            package_name,
            role,
            use_network_config=False,
            run_args=''):
        self.basedir = basedir
        # NOTE: these paths are all UNIX paths, regardless of the
        # source OS (so we don't use os.path.join(...))
        self.pidfile = self.basedir + "/pid"
        self.logfile = self.basedir + "/log"
        self.bindir = self.basedir + "/bin"
        self.exe = self.bindir + "/" + exe
        self.role = role
        self.wrapper_script = self.bindir + "/" + os.path.splitext(exe)[0] + ".sh"
        self.name = name
        self.package_name = package_name
        self.run_args = run_args
        self.use_network_config = use_network_config
        if use_network_config:
            self.badumna_config = self.bindir + "/badumna-config.xml"
            self.run_args += " --badumna-config-file=%s" % (self.badumna_config,)

    def extend_vars(self, env):
        env = dict(env).copy()
        for k in dir(self):
            env[k] = getattr(self,k)
        return env
