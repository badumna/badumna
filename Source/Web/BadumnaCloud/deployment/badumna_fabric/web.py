from __future__ import absolute_import

from fabric.api import *
from fabric.context_managers import shell_env, hide, cd
from fabric.decorators import runs_once
import subprocess
import urllib2
import time

from .common import *

@task
@roles(WEB, DB, DB_ADMIN)
@runs_once
def system_packages():
    '''Install base system'''
    execute(db_system_packages)
    execute(web_system_packages)
    execute(db_admin_system_packages)

def get_instance_id(host):
    return urllib2.urlopen('https://%s/ec2-id' % (host,)).read().strip()

def get_instance_ids(hosts):
    return list(map(get_instance_id, hosts))

@task
def swap_active():
    '''Alter EC2 IP address assignments to swap active & inactive servers'''
    # first figure out hostname -> instance-id mapping
    hosts = env.all_web_hosts
    assert len(hosts) == 2, "Expected 2 hosts, got %s" % (len(instances))
    new_hostnames = list(reversed(hosts))
    original_instance_ids = get_instance_ids(hosts)
    print "Current instance assignments:"
    for hostname, instance in zip(hosts, original_instance_ids):
        print " - %s: %s" % (hostname, instance)

    # then we use the EC2 API to get both instances
    assert env.aws_credentials_module is not None, "No aws_credentials_module in env"
    execfile(env.aws_credentials_module)
    from boto.ec2.connection import EC2Connection
    conn = EC2Connection()
    groups = list(conn.get_all_security_groups("cloud webserver"))
    assert len(groups) == 1, "Expected 1 group, got %s" % (len(groups))
    group = groups[0]
    instances = group.instances()
    assert len(instances) == 2, "Expected 2 instances, got %s" % (len(instances))
    current_ips = map(lambda instance: instance.ip_address, instances)
    new_ips = list(reversed(current_ips))
    
    # set the hostnames to what they will be after swapping:
    set_hostnames(hosts = hosts, names = new_hostnames)

    # swap the IPs:
    for new_ip, instance in zip(new_ips, instances):
        print "assigning IP address %s to instance %s" % (new_ip, instance.id)
        assert instance.use_ip(new_ip), "Failed to assign IP"

    # verify that it worked - it can take some time to take effect,
    # so waiting helps ensure you don't go checking to the wrong machine until
    # the swap is complete...
    print "Checking that hosts are reachable by their new addresses ..."
    while True:
        try:
            current_instance_ids = get_instance_ids(new_hostnames)
        except urllib2.URLError as e:
            print "Got %s: %s" % (type(e).__name__, e)
        else:
            if current_instance_ids == original_instance_ids:
                print "OK\n"
                break
        print "..."
        time.sleep(5)

    print "New instance assignments:"
    for hostname, instance in zip(new_hostnames, original_instance_ids):
        print " - %s: %s" % (hostname, instance)

@task
@roles(DB)
def db_system_packages(): install_system_packages_for(DB)

@task
def deploy_marketing():
    '''Deploy the marketing site only'''
    execute(system_packages)
    execute(create_db)
    execute(update_marketing_site)

@task
@roles(WEB)
def upload_static_content():
    '''Upload docs & SDKs'''
    upload_docs()
    upload_sdks()

@task
@roles(WEB)
def upload_docs():
    '''Upload documentation'''
    base = env.settings.MARKETING_SITE_STATIC_ROOT + "/docs"
    ensure_empty_dir(base)
    server_package = get_server_package()
    if server_package is None:
        prompt("NOTE: not uploading docs...\n(press return to continue...)")
        return
    put_as(BADUMNA_USER, os.path.join(get_server_package(), "Documentation/Sphinx/*"), base)

@task
@roles(WEB)
def upload_sdks():
    '''Upload SDKs'''
    base = env.settings.MARKETING_SITE_STATIC_ROOT + "/sdk"
    ensure_empty_dir(base)
    for sdk in ("unity", "dotnet", "cpp"):
        local_file = get_sdk_archive(sdk)
        put_as(BADUMNA_USER, local_file, base + "/" + os.path.basename(local_file))

@task
@roles(DB_ADMIN)
def import_evolution7_db_dump():
    assert prompt("This will ERASE any existing data in the marketing DB. Are you SURE? [y/N]") == "y"
    create_db()
    assert False, "todo: upload DB dump file"
    run_as(BADUMNA_USER,
            'mysql --user="%(user)s" --password="%(password)s" --host="%(host)s" --database="%(dbname)s" < "%(root)s/dbdump.sql"' %
            {
                "user": env.mysql_user,
                "password": env.mysql_pass,
                "host": env.mysql_host,
                "dbname": env.mysql_cms_db,
                "root": env.home,
            })

@task
@roles(WEB)
def ensure_marketing_site():
    if not file_exists(env.marketing_rpath):
        update_marketing_site()
    else:
        marketing_config()

@task
@roles(WEB)
def update_marketing_site(repo=None):
    '''Update marketing site.
    `repo`, if given, should be a path to a local checkout (the HEAD will be used)
    By default, it will deploy from the cloud-www repo on dev.badumna.com'''
    #NOTE: we should change to using something more instantaneous (a single symlink to switch between deployed versions?)
    # if we plan to do this on the live server
    repo = repo or os.environ.get('MARKETING_REPO', "ssh://dev.badumna.com/git/cloud-www.git")
    if os.path.isdir(repo):
        git_export = subprocess.Popen("git archive HEAD --format=tar | gzip", shell=True, stdout=subprocess.PIPE, cwd=repo)
    else:
        git_export = subprocess.Popen("git archive HEAD --remote=%s --format=tar | gzip" % repo, shell=True, stdout=subprocess.PIPE)
    assert git_export.poll() in (None, 0)
    tempfile = "/tmp/marketing-git-export.tgz"
    put_as(BADUMNA_USER, git_export.stdout, tempfile)
    assert git_export.wait() == 0
    run_as(BADUMNA_USER, "rm -rf " + env.marketing_rpath)
    run_as(BADUMNA_USER, "mkdir " + env.marketing_rpath)
    with cd(env.marketing_rpath):
        run_as(BADUMNA_USER, "tar xzf " + tempfile)
    run_as(BADUMNA_USER, "rm " + tempfile)

    marketing_config()

@task
@roles(WEB)
def marketing_config(admin_pass=None, dev_mode=False):
    vars = template_command_module().get_vars(env)
    vars['admin_pass'] = admin_pass
    vars['dev_mode'] = bool(dev_mode)
    print "** Marketing site development options:\nADMIN_PASS: %r\nDEV_MODE:%r" % (admin_pass, dev_mode)
    render_and_put(vars, os.path.join(env.project_path, "deployment/config/silverstripe_conf.php"),
        env.marketing_rpath + "/mysite/_config.php")

@task
@roles(WEB)
def web_system_packages():
    install_system_packages_for(WEB)

    # also enable apache modules
    mods = ('rewrite', 'wsgi', 'headers', 'ssl', 'proxy', 'proxy_http', 'php5')
    sudo("a2enmod " + (" ".join(mods)))

    # NOTE: When we stop proxying http <-> https config, we can disable previously-used modules:
    # mods = ('proxy', 'proxy_http')
    # sudo("a2dismod " + (" ".join(mods)))

    sudo('chown -R root.monitor /var/log/apache2')
    # configure log rotation
    if put_if_changed(ROOT_USER,
            os.path.join(env.project_path, 'deployment/config/logrotate.d/apache2'),
            '/etc/logrotate.d/apache2'):
        # immediately rotate the logs if the config has changed
        sudo('logrotate -f /etc/logrotate.conf')

@task
@roles(DB_ADMIN)
def db_admin_system_packages(): install_system_packages_for(DB_ADMIN)

@task
@roles(WEB)
def local_packages():
    with cd(env.remote_project_path):
        run_as(BADUMNA_USER, "sed -i -E -e '/#.*TESTING|DEVELOPMENT/,$d' deployment/requirements.txt")
        run_django("validate", env.http_proxy)


@task
@roles(WEB)
def dbshell():
    run_django("dbshell")

@task
@roles(WEB)
def update():
    env.vcs_update()
    post_update()

@task
@roles(WEB)
def post_update():
    local_packages()
    run_django("apply_static_templates")
    copy_static_files()
    web_filesystem_overlay()
    migrate_db()
    reload_apache()

def copy_static_files():
    '''Copy static files to a directory *outside* of the
    source code. This ensures that our (static) maintenance
    page / error pages will keep working while the code
    is being updated'''

    run_as(BADUMNA_USER, "mkdir -p '%(static_root)s'" % env)

    # rsync --delete-after ensures the least downtime for static files
    # (hopefully, the time between consecutive calls to unlink() and rename())
    run_as(BADUMNA_USER, "rsync -a --delete-after '%(remote_project_path)s/Site/static/' '%(static_root)s'" % env)

    # now run django's collectstatic to make sure we haven't missed any static files from other apps:
    run_django("collectstatic --noinput")

@task
@roles(WEB)
def erase():
    for path in [env.static_root, env.remote_project_path]:
        run_as(BADUMNA_USER, "rm -rf " + path)

def web_filesystem_overlay():
    "Overlay (via symlink and upload) parts of our deployed app elsewhere onto the filesystem"

    #apache
    vars = template_command_module().get_vars(env.settings)
    vars['DJANGO_SETTINGS_MODULE'] = env.django_settings_module
    vars['ROOT_DIR'] = env.remote_project_path

    ensure_empty_dir("/etc/apache2/sites-available", user=ROOT_USER)
    render_and_put(vars, os.path.join(env.project_path, "deployment/config/apache/sites-available/badumna-cloud"), '/etc/apache2/sites-available/badumna-cloud')
    sudo("rm -rf /etc/apache2/sites-enabled")
    sudo("ln -sfn sites-available /etc/apache2/sites-enabled")

    render_and_put(
            {'RUN_USER':BADUMNA_USER},
            os.path.join(env.project_path, 'deployment/config/apache/envvars'),
            '/etc/apache2/envvars',
            user=ROOT_USER)

    # ssl certificate files must be owned & readable only by root
    cert_location = os.path.normpath(os.path.join(env.project_path, "../../Credentials/BadumnaCloud/https_cert/"))
    sudo("mkdir -p %s" % env.settings.SSL_CERT_ROOT)
    for filename in os.listdir(cert_location):
        put_as(ROOT_USER, os.path.join(cert_location, filename), env.settings.SSL_CERT_ROOT)

@task(default=True)
def deploy():
    '''Full deploy of website (including maintenance mode & DB migrations)'''
    deploy_cp()
    deploy_marketing()
    execute(upload_static_content)

@task(default=True)
def deploy_cp():
    '''Deploy just the control panel (django) code'''
    execute(system_packages)
    execute(create_db)
    execute(maintenance_start)
    execute(update)
    execute(maintenance_stop)

for service_roles, service, name in (
        ((WEB,), 'apache2', 'apache'),
        ((DB,), 'mysql', 'db')):
    service_cmd(globals(), service_roles, service, name, "stop")
    service_cmd(globals(), service_roles, service, name, "start")
    service_cmd(globals(), service_roles, service, name, "restart")
    service_cmd(globals(), service_roles, service, name, "status")

@task
def restart_services():
    '''restart all services'''
    execute(stop_apache)
    execute(restart_db)
    execute(start_apache)

@task
@roles(WEB)
def maintenance_start():
    '''Disable the running site (enable maintenance mode)'''
    sudo("touch %(maintenance_file)s" % env)
    try:
        reload_apache()
    except:
        if prompt("Could not enable maintenance mode.\nContinue anyway? [y/N]") == 'y':
            return
        else:
            raise

@task
@roles(WEB)
def maintenance_stop():
    '''disable maintenance mode'''
    sudo("rm -f %(maintenance_file)s" % env)
    reload_apache()

@task
@roles(WEB)
def reload_apache():
    sudo("service apache2 reload")

def run_sql(query):
    display_query = query.replace(env.mysql_pass, '**********')
    print "Running SQL: " + display_query
    run_as(BADUMNA_USER, ("mysql --user=%(mysql_run_user)s -p'%(mysql_pass)s' -h '%(mysql_host)s'" % env) + ' -e "%s"' % (query,), ok_quiet=True)

@task
@roles(DB_ADMIN)
def drop_db():
    '''Delete MySQL user and DB'''
    assert prompt('really DROP the database? [y/N]') == 'y'
    db_admin_system_packages()
    run_sql("DROP DATABASE IF EXISTS %(mysql_db)s;" % env)

    # workaround for lack of `DROP USER IF EXISTS`
    run_sql("GRANT USAGE ON *.* TO %(mysql_user)s%(mysql_user_scope)s; DROP USER %(mysql_user)s%(mysql_user_scope)s" % env)

@task
@roles(DB_ADMIN)
@runs_once
def create_db():
    '''Setup MySQL DB and accounts'''
    if env.SKIP_BASE_SYSTEM: return
    db_system_packages()
    db_admin_system_packages()
    with hide('running'):
        #NOTE: if this fails on a fresh box, you may need to make sure the root password is set. e.g:
        # mysqladmin -u root password '<mysql_pass>'
        run_sql("GRANT USAGE on *.* to %(mysql_user)s%(mysql_user_scope)s IDENTIFIED BY '%(mysql_pass)s'" % env)

        # drop default anonymous@localhost user, as it stupidly shadows badumna@localhost and denies access
        run_sql("GRANT USAGE ON *.* TO ''@'localhost'; drop user ''@'localhost'; ")

        # create DBs:
        run_sql("CREATE DATABASE IF NOT EXISTS %(mysql_db)s;" % env)
        run_sql("CREATE DATABASE IF NOT EXISTS %(mysql_stats_db)s;" % env)
        run_sql("CREATE DATABASE IF NOT EXISTS %(mysql_cms_db)s;" % env)
        run_sql("GRANT all privileges on %(mysql_db)s.*       to %(mysql_user)s%(mysql_user_scope)s;" % env)
        run_sql("GRANT all privileges on %(mysql_stats_db)s.* to %(mysql_user)s%(mysql_user_scope)s;" % env)
        run_sql("GRANT all privileges on %(mysql_cms_db)s.*   to %(mysql_user)s%(mysql_user_scope)s;" % env)
        run_sql("FLUSH PRIVILEGES;")


@roles(WEB)
def migrate_db():
    run_django("syncdb")
    run_django("migrate")

def run_django(cmd, extra_env={}):
    with shell_env(DJANGO_SETTINGS_MODULE = env.django_settings_module, **extra_env):
        run_as(BADUMNA_USER, ("python %(remote_project_path)s/manage.py " % env) + cmd)

@task
@roles(WEB)
def tail(log):
    '''e.g `fab tail:apache2/error.log`'''
    sudo("tail -n100 -F /var/log/" + log)

def remove_testing_requirements():
    run_as(BADUMNA_USER, "sed -i -e '/# .* TESTING,$/d' deployment/requirements.txt")

@task
@roles(WEB)
def version():
    run_as(BADUMNA_USER, "cat %(remote_project_path)s/GIT_COMMIT" % env)
    run_as(BADUMNA_USER, "md5sum %s/sdk/*" % env.settings.MARKETING_SITE_STATIC_ROOT)
