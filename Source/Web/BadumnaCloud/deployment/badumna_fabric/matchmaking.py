from __future__ import absolute_import
from fabric.api import *
from . import mono_service
from .common import MATCHMAKING

wrap = mono_service.create_wrapper(lambda: env.matchmaking_server, role=MATCHMAKING, globals=globals())

wrap(mono_service.deploy, default=True)
wrap(mono_service.upload)
wrap(mono_service.tail_log)
wrap(mono_service.start)
wrap(mono_service.stop)
wrap(mono_service.status)
wrap(mono_service.restart)
wrap(mono_service.system_packages)
wrap(mono_service.killall_mono)
wrap(mono_service.version)
wrap(mono_service.erase)
