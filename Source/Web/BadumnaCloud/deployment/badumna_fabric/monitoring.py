from __future__ import absolute_import

from StringIO import StringIO
from fabric.api import *
from fabric.context_managers import shell_env, cd
import sys
import time
import itertools

from .common import *
from . import mono_service

@task(default=True)
@roles(MONITORING)
def deploy(email_on=True):
    '''Deploy monitoring server.
    If email_on is False, the crashmail event listener will
    not automatically start (avoids excessive emails when testing)'''
    system_packages()
    stop(oknodo=True)
    update(email_on=email_on)
    install_local_packages()
    start()

@task
@roles(MONITORING)
def system_packages():
    install_system_packages_for(MONITORING)

@task
@roles(MONITORING)
def update(email_on=True):
    if email_on in ('False', 'false', '0'): email_on = False

    check_path = env.settings.SUPERVISORD_BIN + "/status_checks.py"
    vars = template_command_module().get_vars(env)
    vars['CHECK_CMD'] = 'python ' + check_path + ' --run-user=' + BADUMNA_USER
    vars['CHECK_OPTS'] = "\n".join([
        "startretries=0",
        "autorestart=false",
        "redirect_stderr=true",
        "startsecs=0",
        "exitcodes=0",
    ])

    vars['AUTO_RESTART_CHECK_OPTS'] = "\n".join([
        "startretries=5",
        "autorestart=true",
        "redirect_stderr=true",
        "startsecs=120",
        "exitcodes=0",
    ])

    vars['seed_server_command'] = mono_service.start_cmd(env.seed_server, oknodo=True, args='')
    vars['matchmaking_server_command'] = mono_service.start_cmd(env.matchmaking_server, oknodo=True, args='')

    # replace the single monitoring box with "localhost"
    monitoring_hosts = env.roledefs[MONITORING]
    monitoring_host = None
    if len(monitoring_hosts) == 1:
        monitoring_host = monitoring_hosts[0]

    host_groups = {
        'WEB_HOSTS': env.all_web_hosts,
        'DYNAMIC_HOSTS': env.dynamic_hosts,
        'ALL_HOSTS': env.all_known_hosts,
    }

    def replace_localhost(hosts):
        hosts = set(hosts)
        if monitoring_host in hosts:
            hosts.remove(monitoring_host)
            hosts.add('localhost')
        return sorted(hosts)

    # replace roledefs by substituting localhost for the monitoring host
    vars['roledefs'] = dict([(role, replace_localhost(hosts)) for role, hosts in env.roledefs.items()])

    for key, hosts in host_groups.items():
        hosts = replace_localhost(set(hosts) - set(env.inactive_servers))
        vars[key] = [{'hostname': host, 'nickname': host.split('.')[0] } for host in hosts]

    vars['STATISTICS_MAX_AGE_SECONDS'] = env.settings.STATISTICS_REFRESH_INTERVAL.total_seconds() * 2
    vars['AUTOSTART_CRASHMAIL'] = email_on

    # run_as(MONITORING_USER, 'mkdir -p %s' % env.settings.SUPERVISORD_BIN)
    run_as(MONITORING_USER, 'mkdir -p %s/deployment' % env.settings.SUPERVISORD_BIN)
    # run_as(MONITORING_USER, 'mkdir -p %s' % env.settings.SUPERVISORD_STATE)
    run_as(MONITORING_USER, 'mkdir -p %s/logs' % env.settings.SUPERVISORD_STATE)
    put_as(MONITORING_USER,
            os.path.join(env.project_path, 'deployment/status_checks.py'),
            check_path)
    run_as(MONITORING_USER, "chmod +x %s/status_checks.py" % env.settings.SUPERVISORD_BIN)

    put_as(MONITORING_USER,
            os.path.join(env.project_path, 'local_env.py'),
            env.settings.SUPERVISORD_BIN)

    put_as(MONITORING_USER,
            StringIO("supervisor\nsuperlance"),
            env.settings.SUPERVISORD_BIN + "/deployment/requirements.txt")

    render_and_put(
            vars,
            os.path.join(env.project_path, 'deployment/config/supervisord.conf'),
            env.settings.SUPERVISORD_BIN + '/supervisord.conf',
            user=MONITORING_USER)

    # The monitoring user needs passwordless access to all our boxes.
    # So we check their private key into VCS.
    # We limit the risk of unauthorized access by using a `monitoring`
    # user who can't do much but look at logs.
    put_as(MONITORING_USER,
            os.path.join(env.project_path, '../../Credentials/BadumnaCloud/monitoring_ssh/id_rsa'),
            '/home/%s/.ssh/id_rsa' % (MONITORING_USER),
            mode=0600)
    put_as(ROOT_USER, os.path.join(env.project_path, 'deployment/known_hosts'), '/etc/ssh/ssh_known_hosts', mode=0644)

    # prevent adding new known_hosts for this user (it adds IP address entries and breaks every time we swap servers)
    run_as(MONITORING_USER, "ln -sfn /dev/null /home/%s/.ssh/known_hosts" % (MONITORING_USER))

def _run_supervisor(cmd, args="", **k):
    with cd(env.settings.SUPERVISORD_BIN):
        return run_as(
            MONITORING_USER,
            "PATH=\"$PWD/python-env/bin:$PATH\" ./python-env/bin/%s -c ./supervisord.conf %s" % (cmd, args), **k)

@task
@roles(MONITORING)
def install_local_packages():
    with shell_env(**env.http_proxy):
        return run_as(MONITORING_USER, "python %s/local_env.py init" % (env.settings.SUPERVISORD_BIN))

@task
@roles(MONITORING)
def start():
    '''Start supervisord'''
    _run_supervisor("supervisord")

@task
@roles(MONITORING)
def status():
    '''Print status to console'''
    _run_supervisor("supervisorctl", "status")

def wait_until_supervisor_port_free():
    for attempt in range(1, 10):
        if is_supervisor_port_free():
            return
        time.sleep(2)
    else:
        raise AssertionError("Supervisor port (%s) is in use" % env.settings.SUPERVISORD_PORT)

def is_supervisor_port_free():
    output = StringIO()
    run_as(MONITORING_USER, "lsof -i TCP:%s || echo NONE" % (env.settings.SUPERVISORD_PORT), stdout=output)
    return not ('LISTEN' in output.getvalue())

@task
@roles(MONITORING)
def stop(oknodo=False):
    '''Stop monitoring server'''
    try:
        output = StringIO()
        cmd = _run_supervisor("supervisorctl", "shutdown", stdout=output, warn_only=True)
        output = output.getvalue()
        print output
        assert cmd.succeeded, "supervisorctl shutdown failed"
        success_lines = [line.strip() for line in output.splitlines() if
                line.endswith("Shut down")]
        assert len(success_lines) == 1, "Output doesn't look successful"
    except (StandardError, AssertionError) as e:
        print >> sys.stderr, "Error - %s: %s" % (type(e).__name__, e)
        if oknodo:
            pass
        else:
            raise
    wait_until_supervisor_port_free()

@task
@roles(MONITORING)
def tail_log():
    '''Tail supervisord.log'''
    logfile = env.settings.SUPERVISORD_STATE + "/logs/supervisord.log"
    run_as(MONITORING_USER, "tail -n 100 -F " + logfile)
