from __future__ import absolute_import
from fabric.api import *

from .common import *
from . import web
from . import mono_service

wrap = mono_service.create_wrapper(lambda: env.stats_server, role=STATS, globals=globals())

@task(default=True)
@roles(STATS)
def deploy():
    '''Update & deploy statistics server'''
    system_packages()
    execute(web.create_db)
    stop(oknodo=True)
    upload()
    post_upload()

@roles(STATS)
def upload():
    mono_service.upload(env.stats_server)
    dest = env.stats_server.bindir
    # remove default config
    run_as(BADUMNA_USER, "rm " + dest + "/*.config")

@task
@roles(STATS)
def post_upload():
    write_config()
    start()

@task
@roles(STATS)
def write_config():
    vars = env.stats_server.extend_vars(env)
    render_and_put(vars, "deployment/config/StatisticsServer.exe.config.tmpl", env.stats_server.exe + ".config")

wrap(mono_service.tail_log)
wrap(mono_service.start)
wrap(mono_service.stop)
wrap(mono_service.status)
wrap(mono_service.restart)
wrap(mono_service.system_packages)
wrap(mono_service.killall_mono)
wrap(mono_service.version)
wrap(mono_service.erase)

