from __future__ import absolute_import

import os
import re
import contextlib
import subprocess
from StringIO import StringIO

from fabric.api import *
from fabric import api, context_managers

# role tags
WEB = 'web'
DB = 'db'
DB_ADMIN = 'db_admin'
STATS = 'stats'
MATCHMAKING = 'matchmaking'
SEED = 'seed'
MONITORING = 'monitoring'

#users
ROOT_USER = 'root'
ADMIN_USER = 'ubuntu'
BADUMNA_USER = 'badumna'
MONITORING_USER = 'monitor'

ROLES = [WEB, DB, DB_ADMIN, STATS, MATCHMAKING, SEED, MONITORING]

mono_packages = [
    'mono-runtime',
    'libmono2.0-cil',
    'libmono-system-core4.0-cil',
]

# packages that may be installed by default (or manually in the past) which we don't want:
UNWANTED_PACKAGES = (
        'apticron',
        'sendmail',
        'apt-listchanges',
)

SYSTEM_PACKAGES = {
        'ALL': (
            'mailutils',
            'postfix',
            'libsasl2-modules', # required for postfix SMTP auth
            'anacron',
        ),
        WEB: (
            'apache2',
            'postfix',
            'libapache2-mod-wsgi',
            'mysql-client',

            # for django site
            'python-mysqldb',
            'python2.7',
            'python-pip',
            'python-virtualenv',

            # for marketing site:
            'libapache2-mod-php5',
            'php5-mysqlnd',
        ),
        DB: (
            'mysql-server',
        ),
        DB_ADMIN: (
            'mysql-client',
        ),
        STATS: ['mysql-client'] + mono_packages,
        SEED: mono_packages,
        MATCHMAKING: mono_packages,
        MONITORING: (
            'python2.7',
            'python-pip',
            'python-virtualenv',
        ),
    }


def apt_get(cmd):
    sudo("DEBIAN_FRONTEND=noninteractive apt-get -y " + cmd)

def install_system_packages_for(role):
    if env.SKIP_BASE_SYSTEM:
        print "Skipping system packages..."
        return
    with context_managers.hide('running'):
        init_base_system(role)
    pkglist = list(SYSTEM_PACKAGES[role]) + list(SYSTEM_PACKAGES['ALL'])
    ensure_package_status(wanted=pkglist, unwanted=list(UNWANTED_PACKAGES))
    apply_system_settings(role)

def init_base_system(role):
    create_user_accounts(role)

    # ensure UMASK is set to 026 - files are only visible to their owner / group
    # (but directories can be traversed by anyone)
    sudo(r"sed -i -e 's/^UMASK\s.*/UMASK\t\t026/' /etc/login.defs")

    proxy = getattr(env, 'http_proxy', None)
    apt_proxy_conf = '/etc/apt/apt.conf.d/http-proxy'
    if proxy:
        put(StringIO('''
        Acquire::http::Proxy "%s";
        '''.strip() % (proxy['http_proxy'],)), apt_proxy_conf, use_sudo=True)
    else:
        run("rm -f " + apt_proxy_conf, ok_quiet=True)

def set_hostnames(hosts, names=None):
    if hosts is None:
        hosts = env.all_hosts
    if names is None:
        names = hosts

    def set_hostname(host, name):
        print "Setting hostname: %s" % (name,)
        put_as(ROOT_USER, StringIO(name), "/etc/hostname-external", mode=0644)

    for host, name in zip(hosts, names):
        execute(set_hostname, host, name, hosts=[host])

def ensure_package_status(wanted, unwanted):
    wanted = set(wanted)
    unwanted = set(unwanted)
    all_packages = wanted.union(unwanted)

    package_regexes = "|".join(['^%s$' % (re.escape(package),) for package in all_packages])
    installed_packages = set(run(r"dpkg --get-selections | grep '\sinstall' | cut -f 1 | grep -E '" + package_regexes + "'").splitlines())
    # print repr(installed_packages)
    to_remove = unwanted.intersection(installed_packages)
    to_install = wanted.difference(installed_packages)
    if to_remove:
        to_remove = sorted(to_remove)
        print("REMOVING: %r" % (to_remove))
        apt_get('remove --purge ' + ' '.join(to_remove))
    apt_get('autoremove --purge')
    if to_install:
        to_install = sorted(to_install)
        print("INSTALLING: %r" % (to_install))
        apt_get('install ' + ' '.join(to_install))

def apply_system_settings(role):
    # settings should be consistent across all machines in
    # a role, so we just pick the first:
    try:
        hosts = env.roledefs[role]
        host = env.roledefs[role][0]
    except IndexError:
        # there are no hosts, which means we're not doing anything anyway...
        return

    set_hostnames(hosts)

    ## interfaces ##
    # The Azure machine seems to have periodic issues with DNS lookups.  To
    # try to mitigate this we add Google's public nameserver as a backup.
    if role == MONITORING:
        put_as(ROOT_USER, os.path.join(env.project_path, 'deployment/config/interfaces.monitoring'), '/etc/network/interfaces', mode=0644)
        run_as(ROOT_USER, "ifdown eth0 && ifup eth0")

    ## override PS1. Because each user's default bashrc fights this, we symlink them to it
    put_as(ROOT_USER, os.path.join(env.project_path, 'deployment/config/badumna-profile.sh'), '/etc/badumna-profile.sh', mode=0644)
    run_as(ROOT_USER, "for base in /root /home/badumna /home/ubuntu; do ln -sfn /etc/badumna-profile.sh $base/.bashrc; done")

    ## UNATTENDED-UPGRADES ##
    base = os.path.join(env.project_path, 'deployment/config/package-updates')
    for file in os.listdir(base):
        dest = '/etc/apt/apt.conf.d/' + file
        render_and_put(env.settings, os.path.join(base, file), dest, user=ROOT_USER, mode=0644)

    ## ANACRON ##
    # anacron runs our daily tasks (including automatic security upgrades),
    # which we want to happen while we're in the office.
    # This crontab file makes anacron start at 10:30AM instead of the default 7:30
    put_as(ROOT_USER, os.path.join(env.project_path, 'deployment/config/anacron'), '/etc/cron.d/anacron', mode=0644)


    ## POSTFIX (email) ##
    postfix_base = os.path.join(env.project_path, 'deployment/config/postfix')
    api_key_module = {}
    execfile(os.path.join(env.project_path, "../../Credentials/BadumnaCloud/postmark/api_keys.py"), api_key_module)

    vars = api_key_module['postmark_settings_for'](host)
    if vars is None:
        assert not env.PRODUCTION, "postmark settings undefined for production host %s" % ( host,)
        vars = {'use_postmark': False}
    else:
        vars['use_postmark'] = True

    sudo("rm -f /etc/postfix/*.map*")
    for filename in os.listdir(postfix_base):
        src = os.path.join(postfix_base, filename)
        dest = '/etc/postfix/' + filename
        render_and_put(vars, src, dest, user=ROOT_USER, mode=0644)
        if filename.endswith(".map"):
            sudo('postmap ' + dest)
    sudo("rm -f /etc/postfix/generic{,.db}")
    sudo("service postfix reload")


def update_ssh_keys():
    def put_ssh_keys(user):
        home = '/root' if user == ROOT_USER else '/home/' + user
        sudo("mkdir -p %s/.ssh/" % home)
        sudo("chown %s.%s %s/.ssh/" % (user, user, home))
        sudo("chmod 700 %s/.ssh/" % home)
        put_as(user,
            os.path.join(env.project_path, "deployment/authorized_keys"),
            "%s/.ssh/authorized_keys" % home,
            mode=0600, atomic=True)
        # disable password login - this allows us to provision a VM with a single-use root
        # password: after we deploy to it once, you can only get in with SSH keys.
        sudo("passwd --lock %s" % (user,), ok_quiet=True)

    for user in set(env.bootstrap_users.values()):
        # `bootstrap_users` lists bootstrap users for each role,
        # so we just update SSH keys for users that exist on this machine
        if user_exists(user):
            put_ssh_keys(user)
    put_ssh_keys(ADMIN_USER)

    sudo('d=/home/%(user)s/.ssh && mkdir -p "$d" && chown -R %(user)s.%(user)s "$d" && chmod 700 "$d"' % dict(user=MONITORING_USER))
    put_as(MONITORING_USER,
            os.path.join(env.project_path, "../../Credentials/BadumnaCloud/monitoring_ssh/id_rsa.pub"),
            "/home/%s/.ssh/authorized_keys" % MONITORING_USER,
            mode=0600, atomic=True)
    sudo("passwd --lock %s" % (user,), ok_quiet=True)

def get_bootstrap_user(role):
    return env.bootstrap_users.get(role, env.bootstrap_users[None])

def user_exists(username):
    return run("id " + username, quiet=True).succeeded

def file_exists(path):
    return run("test -e  \"" + path + "\"", quiet=True).succeeded

def create_user_accounts(role):
    def ensure_user(name):
        if not user_exists(name):
            sudo("adduser %s --disabled-password --gecos ''" % (name,))

    bootstrap_user = get_bootstrap_user(role)
    with context_managers.settings(user=bootstrap_user):
        # ubuntu user: can sudo without password
        ensure_user(ADMIN_USER)
        ensure_user(MONITORING_USER)

        #NOTE: the call below *MUST* use atomic=True, as incomplete
        # or multi-step transfers will lock us out of the server!
        put_as(ROOT_USER,
                os.path.join(env.project_path, "deployment/config/sudoers.d/90-cloudimg-ubuntu"),
                "/etc/sudoers.d/90-cloudimg-ubuntu",
                mode=0400, atomic=True)
        sudo("f=/etc/sudoers.d/waagent; if [ -f $f ]; then rm $f; fi") # remove default azure config, as it contradicts the above config

        update_ssh_keys()

    ensure_user(BADUMNA_USER)
    sudo("passwd --lock %s" % (BADUMNA_USER,))
    for user in (BADUMNA_USER, MONITORING_USER):
        # these users only get to be in their own group, not admin / wheel / etc
        sudo("usermod -G %s %s" % (user, user))

def service_cmd(glob, service_roles, service, name, cmd):
    t = lambda: sudo("service %s %s" % (service, cmd))
    key = "%s_%s" % (cmd, name)
    t.__name__ = key
    glob[key] = roles(*service_roles)(task(t))


def template_command_module():
    '''This module is only importable after envonments.base_env() has setup sys.path correctly,
    so we lazy-import it'''
    from ControlPanel.management.commands.apply_static_templates import Command
    return Command

def render_and_put(vars, path, dest_path, log=False, user=BADUMNA_USER, **kwargs):
    rendered = StringIO()
    template_command_module().render_file(vars, path, rendered)
    put_as(user, rendered, dest_path, **kwargs)
    if log:
        print " ---- Contents of %s: ---- " % (dest_path,)
        print rendered.getvalue()
        print " ---- END ---- "

@contextlib.contextmanager
def chdir(d):
    old = os.getcwd()
    os.chdir(d)
    try:
        yield
    finally:
        os.chdir(old)

def wrap_run_fn(fn):
    def _run(command, **k):
        '''
        wrapper for fabric's `run` with additional ok_quiet keyword argument
        (fabric conflates verbosity with ignoring errors which is bad)
        '''
        k = k.copy()

        def pop_arg(name, default=None):
            try:
                return k.pop(name)
            except KeyError:
                return default

        if pop_arg('ok_quiet'):
            warn_only = pop_arg('warn_only')
            k['warn_only'] = True
            capture = 'stdout' not in k
            if capture:
                stream = StringIO()
                k['stdout'] = stream
            try:
                result = fn(command, **k)
            except:
                if capture: print stream.getvalue()
                raise
            else:
                msg = "Command failed: %s" % (command,)
                if result.return_code != 0:
                    if warn_only:
                        if capture: print stream.getvalue()
                        print msg
                    else:
                        api.abort(msg)
                return result
        else:
            return fn(command, **k)
    return _run

run = wrap_run_fn(api.run)
sudo = wrap_run_fn(api.sudo)

def putdir_as(user, src, dest, **k):
    '''
    put_as doesn't work for folder destinations, due to how the underlying `put` works with
    use_sudo=True
    '''
    for filename in os.listdir(src):
        put_as(user, os.path.join(src, filename), "%s/%s" % (dest, filename))

def put_as(user, path, dest, atomic=False, mode=None, **k):
    # kind of a hack, when uploading files we want the side effect of warning about
    # a workspace that differs from the current COMMIT
    if not isinstance(path, StringIO):
        env.get_git_commit()
    # (we could extract the file contents from the relevant commit instead of using the
    # on-disk version, but that gets very confusing)

    orig_dest = dest
    if atomic:
        assert not os.path.isdir(path), "Can't atomically transfer directories (only files)"
        assert os.path.isfile(path), "Not a file: %s" % (path,)
        import uuid
        dest = '/tmp/' + uuid.uuid1().hex
    put(path, dest, use_sudo=True, **k)

    vars = dict(user=user, dest=dest)
    cmd = "chown -R %(user)s.%(user)s %(dest)s"
    if mode is not None:
        vars['mode'] = mode
        cmd += " && chmod -R %(mode)o %(dest)s"
    sudo(cmd % vars, ok_quiet=True)
    if atomic:
        sudo("mv %s %s" % (dest, orig_dest))

def get_server_package():
    envvar = 'server_package'
    root = getattr(env, envvar, os.environ.get('SERVER_PKG', None))
    while True:
        if root == 'build':
            return None
        if root is None:
            print "\n(to skip this question in the future, export SERVER_PKG=<path>)"
            root = prompt("Where is the BadumnaCloud-Server package folder? ")

        if os.path.isdir(root):
            setattr(env, envvar, root)
            return root
        else:
            print "Package location %s is not a directory." % (root,)
            root = None

def get_sdk_archive(name):
    envvar = name.upper() + '_SDK'
    root = os.environ.get(envvar, None)
    while True:
        if root is None:
            print "\n(to skip this question in the future, export %s=<path>)" % (envvar,)
            root = prompt("Where is the BadumnaCloud %s SDK zipfile?" % (name,))

        if os.path.isfile(root):
            os.environ[envvar] = root
            return root
        else:
            print "Location %s is not a file." % (root,)
            root = None


def ensure_empty_dir(path, user=BADUMNA_USER):
    assert path.startswith("/"), "path should be an absolute unix path"
    run_as(user, "rm -rf " + path)
    run_as(user, "mkdir -p " + path)

def run_build_cmd(args):
    project_root = os.path.abspath(os.path.join(env.project_path, "../../../"))
    with chdir(project_root):
        subprocess.check_call(["python", "Build/build.py"] + args)

def run_as(user, *a, **k):
    if user == env.user:
        return run(*a, **k)
    else:
        return sudo(*a, user=user, **k)

def put_if_changed(user, local_file, remote_file, **k):
    stdout = StringIO()
    local_md5 = md5sum(local_file)
    run_as(user, "md5sum " + remote_file, stdout=stdout)
    remote_md5 = stdout.getvalue()
    if local_md5 not in remote_md5:
        put_as(user, local_file, remote_file, **k)
        return True
    return False

import hashlib
from functools import partial

def md5sum(filename):
    with open(filename, mode='rb') as f:
        d = hashlib.md5()
        for buf in iter(partial(f.read, 128), b''):
            d.update(buf)
    return d.hexdigest()
