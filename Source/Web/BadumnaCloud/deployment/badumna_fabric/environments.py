from __future__ import absolute_import
import sys, os
from importlib import import_module
import subprocess

from fabric.api import *
from fabric.decorators import runs_once

from .common import *
from . import vcs
from .mono_service import MonoService
from . import ssh_hacks

def base_config():
    env.user = 'ubuntu'
    env.home = '/home/%s' % (BADUMNA_USER,)
    env.shell = '/bin/bash -c' # don't use a login shell
    env.project_path = os.path.abspath(os.path.dirname(os.path.dirname(os.path.dirname(__file__))))
    env.SKIP_BASE_SYSTEM = getattr(env, 'SKIP_BASE_SYSTEM', False)

    ssh_hacks.check_ssh_keys()
    ssh_hacks.override_server_keys()

    env.project_rpath = 'Source/Web/BadumnaCloud'
    env.remote_project_path = env.home + '/BadumnaCloud'
    if not hasattr(env, 'django_settings_module'):
        env.django_settings_module = 'Site.settings.production'
    os.environ['DJANGO_SETTINGS_MODULE'] = env.django_settings_module
    sys.path.insert(0,env.project_path)
    env.settings = import_module(env.django_settings_module)
    env.static_root = env.settings.STATIC_ROOT
    env.marketing_rpath = env.settings.MARKETING_SITE_ROOT
    env.maintenance_file = env.settings.MAINTENANCE_FILE
    env.db_conf = env.settings.DATABASES['default']
    env.mysql_host = env.db_conf['HOST']
    env.mysql_run_user = 'root'
    env.mysql_user = env.db_conf['USER']
    env.mysql_user_scope = "@'%'"
    env.mysql_pass = env.db_conf['PASSWORD']
    env.mysql_db = env.db_conf['NAME']
    env.mysql_stats_db = 'BadumnaStatistics'
    env.mysql_cms_db = 'BadumnaCms'
    env.get_git_commit = get_git_commit
    env.vcs_update = vcs.git_export
    env.aws_credentials_module = None

    env.stats_server = MonoService(
        basedir = "%(home)s/StatisticsServer" % env,
        exe = "StatisticsServer.exe",
        name="Statistics server",
        role=STATS,
        package_name = "StatisticsServer",
        # message interval is 5 minutes:
        run_args="--http-serve='http://+:%s/' --message-interval=%s" % (env.settings.STATISTICS_SERVER_PORT, 5 * 60)
    )
    env.stats_server.port = env.settings.STATISTICS_SERVER_PORT

    env.matchmaking_server = MonoService(
        basedir = "%(home)s/MatchmakingServer" % env,
        exe ="MatchmakingServer.exe",
        name="Matchmaking server",
        package_name = "MatchmakingServer",
        role=MATCHMAKING,
        use_network_config=True,
        run_args="--badumna-config-port=%s --cloud" % env.settings.BADUMNA_NETWORK['MATCHMAKING_PORT'],
    )

    env.seed_server = MonoService(
        basedir = "%(home)s/SeedPeer" % env,
        exe ="SeedPeer.exe",
        name="Seed peer",
        package_name = "SeedPeer",
        use_network_config=True,
        role=SEED,
        run_args="--badumna-config-port=%s --cloud" % env.settings.BADUMNA_NETWORK['SEED_PORT']
    )

def get_git_commit():
    commit = getattr(env, '_git_commit', None)
    def check_commit(commit):
        if vcs.workspace_differs_from(commit):
            msg = "\nWARN: Workspace differs from commit %s.\nAre you sure you want to continue? [Y/n]" % (commit,)
            assert prompt(msg).lower().strip() in ('','y'), "Cancelled"
            return commit

    if commit is None:
        commit = os.environ.get('COMMIT', None)
        if commit == 'working':
            commit = vcs.git_snapshot_hash()
            print "Using COMMIT=%s (workspace changes)" % (commit,)
        else:
            if commit is not None:
                # resolve commit (a commit-ish which may be symbolic like "HEAD~3") into a commit ID:
                commit = check_commit(vcs.parse_rev(commit))
            else:
                commit = get_package_commit()
                if commit is None:
                    commit = vcs.git_snapshot_hash()
                else:
                    print "NOTE: Using COMMIT=%s to match Cloud-Server package. Override by exporting $COMMIT=working|<ref>" % (commit,)
                    check_commit(commit)
        env._git_commit = commit
    return env._git_commit

def get_package_commit():
    '''gets the commit used to build this package, or None
    if no package is in use'''
    pkg = get_server_package()
    if pkg is None: return None
    with open(os.path.join(pkg, 'GIT_COMMIT')) as f:
        return f.read().strip()

@task
def production_LIVE():
    '''Apply actions to real site, on LIVE web server'''
    production(live=True)

@task
def production_ALL():
    '''Apply actions to real site, including both active & inactive web servers'''
    production(all=True)

@task(alias='prod')
def production(all=False, live=False, seed=None):
    '''Apply actions to real site (inactive webserver)'''
    env.bootstrap_user = 'ubuntu'
    env.bootstrap_users = {
        None: 'ubuntu', # default
        MONITORING: 'azureuser',
    }
    env.PRODUCTION = True
    base_config()
    env.all_known_hosts = set()

    # wrap hostnames in `host` to ensure they're recorded in `env.all_known_hosts`
    def host(hostname):
        env.all_known_hosts.add(hostname)
        return hostname

    live_cloud_server = host('cloud.badumna.com')
    inactive_cloud_server = host('cloud-inactive.badumna.com')

    env.all_web_hosts = [live_cloud_server, inactive_cloud_server]
    env.inactive_servers = [inactive_cloud_server]
    env.dynamic_hosts = env.all_web_hosts[:]

    env.aws_credentials_module = os.path.normpath(os.path.join(env.project_path, "../../Credentials/AWS/env.py"))

    cloud_servers = [inactive_cloud_server]
    if live and all:
        raise AssertionError("You should only specify one of `live`, `all`")
    if live or all:
        print "NOTE: applying actions to LIVE cloud webserver!"
        if all:
            cloud_servers.append(live_cloud_server)
        else:
            cloud_servers = [live_cloud_server]
    seed_peers = [host(h) for h in env.settings.BADUMNA_NETWORK['SEED_PEERS']]

    # pass 0 or 1 to select an individual seed peer
    if seed is not None:
        seed_idx = int(seed)
        seed_peers = [seed_peers[seed_idx]]

    env.roledefs = {
        WEB: cloud_servers,
        DB: [],
        DB_ADMIN: cloud_servers[:1],
        STATS: [host(env.settings.STATISTICS_SERVER_HOST)],
        MATCHMAKING: [host(env.settings.BADUMNA_NETWORK['MATCHMAKING_PEER'])],
        SEED: seed_peers,
        MONITORING: [host('badumna-monitor.cloudapp.net')],
    }

    # This key is only used for bootstrapping servers - once users are set up,
    # authorized_keys is replaced with our own version (which does NOT include this key)
    env.key_filename = os.path.abspath(os.path.join(env.project_path, '../../Testing/AutoTesting/AutoTestScript/amazonkey.pem'))
    assert os.path.exists(env.key_filename), "No such file: %s" % (env.key_filename,)

    env.http_proxy = {}

    report_hosts()

@task(alias='test')
def testing():
    '''Apply actions to test VM(s)'''
    # used only for creating the real users
    env.bootstrap_users = {
        None: 'root',
    }
    env.PRODUCTION = False

    env.django_settings_module = 'Site.settings.testing'
    base_config()
    ubuntu_vm = "192.168.1.114"
    env.all_known_hosts = env.all_web_hosts = set([ubuntu_vm])
    env.roledefs = {
        WEB: [ubuntu_vm],
        DB: [ubuntu_vm],
        DB_ADMIN: [ubuntu_vm],
        STATS: [ubuntu_vm],
        MATCHMAKING: [ubuntu_vm],
        SEED: [ubuntu_vm],
        MONITORING: [ubuntu_vm],
    }
    env.inactive_servers = []
    env.http_proxy = {
        'http_proxy':  'http://wwwproxy.unimelb.edu.au:8000',
        'https_proxy': 'http://wwwproxy.unimelb.edu.au:8000',
    }
    #env.server_package = os.environ.get('SERVER_PKG', 'build')
    env.dynamic_hosts = []
    report_hosts()

@task
def aws_testing(ec2instance):
    '''Apply actions to test EC2 instance(s)'''
    env.bootstrap_user = 'ubuntu'
    env.bootstrap_users = {
        None: 'ubuntu', # default
    }
    env.PRODUCTION = False

    env.django_settings_module = 'Site.settings.aws_testing'
    base_config()
    env.all_known_hosts = env.all_web_hosts = set([ec2instance])
    env.roledefs = {
        WEB: [ec2instance],
        DB: [ec2instance],
        DB_ADMIN: [ec2instance],
        STATS: [ec2instance],
        MATCHMAKING: [ec2instance],
        SEED: [ec2instance],
        MONITORING: [ec2instance],
    }

    env.aws_credentials_module = os.path.normpath(os.path.join(env.project_path, "../../Credentials/AWS/env.py"))
    # This key is only used for bootstrapping servers - once users are set up,
    # authorized_keys is replaced with our own version (which does NOT include this key)
    env.key_filename = os.path.abspath(os.path.join(env.project_path, '../../Testing/AutoTesting/AutoTestScript/amazonkey.pem'))
    assert os.path.exists(env.key_filename), "No such file: %s" % (env.key_filename,)

    env.http_proxy = {}
    env.inactive_servers = []
    env.dynamic_hosts = []

    report_hosts()

@task
def quick():
    '''Skip setup tasks (packages, users, etc)'''
    env.SKIP_BASE_SYSTEM = True

def report_hosts():
    print "** hosts **"
    for role, hosts in env.roledefs.items():
        if hosts:
            print "   - %-20s: %s" % (role + " server", ", ".join(hosts))


