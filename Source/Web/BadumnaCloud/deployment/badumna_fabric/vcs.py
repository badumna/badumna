from __future__ import absolute_import

import subprocess
import tempfile
import os
from StringIO import StringIO

from fabric.api import *

from . import common
from .common import BADUMNA_USER, put_as, run_as

def git_snapshot_hash():
    if git_snapshot_hash._ is None:
        commit = subprocess.check_output(['git','stash', 'create']).strip() or 'HEAD'
        print "Temporary commit: {}".format(commit)
        git_snapshot_hash._ = commit
    return git_snapshot_hash._
git_snapshot_hash._ = None

def git_export():
    commit = env.get_git_commit()
    run_as(BADUMNA_USER, "mkdir -p %(remote_project_path)s" % env)
    with cd(env.remote_project_path):
        filter = "| grep -v python-env" if env.get('force',False) is False else ''
        run_as(BADUMNA_USER, "ls -1 " + filter + " |  while read f; do rm -rf $f; done")

        with common.chdir(env.project_path):
            archive_cmd = ['git','archive', '--format=tar', commit, '.']
            archive_name = "repo.tgz"

            with tempfile.TemporaryFile() as local_file:
                subprocess.check_call(archive_cmd, stdout = local_file)
                put_as(BADUMNA_USER, local_file, archive_name)
            try:
                run_as(BADUMNA_USER, 'tar -xf ' + archive_name)
            finally:
                run_as(BADUMNA_USER, "rm -f " + archive_name)
            # write a GIT_COMMIT file for correlating server components later
            put_as(BADUMNA_USER, StringIO(commit + "\n"), "GIT_COMMIT")

def parse_rev(commit_ish):
    return subprocess.check_output(['git', 'rev-parse', '--verify', commit_ish]).strip()

def workspace_differs_from(commit_ish):
    return subprocess.call(['git','diff-index', '--quiet', commit_ish]) != 0
