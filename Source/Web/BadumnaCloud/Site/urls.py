from django.conf.urls import patterns, include, url
from django.views.generic import TemplateView
from django.http import HttpResponse

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    ('^', include('ControlPanel.urls')),
    url('^config/', include('ControlPanel.config_urls')),
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^dumpEmailContents', TemplateView.as_view(template_name='all_emails.txt', content_type='text/plain')),
)

# Error handlers are required to return a fully-rendered string, not just a TemplateResponse
class FullyRenderedTemplateView(TemplateView):
    status_code = 200
    def get(self, *a, **k):
        content = super(FullyRenderedTemplateView, self).get(*a, **k).render()
        response = HttpResponse(content)
        response.status_code = self.status_code
        return response
    # handle every http method
    head = post = options = get

handler404 = FullyRenderedTemplateView.as_view(template_name='errors/404.html', status_code=404)
handler500 = FullyRenderedTemplateView.as_view(template_name='errors/500.html', status_code=500)
