from __future__ import absolute_import
from .base import *

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'BadumnaCloud',
        'USER': 'badumna',
        'PASSWORD': 'OeX7KoZ57Z+zxgLR',
        'HOST': 'badumna-cloud.cxeghj8avzdn.us-east-1.rds.amazonaws.com',
        'PORT': '3306',
    }
}

ADMIN_EMAIL = 'cloud-admin@scalify.com'
ALLOWED_HOSTS = ['.badumna.com', 'localhost']

STATISTICS_SERVER_HOST = 'tracker.badumna.com'
STATISTICS_SERVER_HOST_INTERNAL = '10.147.156.178' # we need to access the stats via its internal IP due to firewall restrictions
BADUMNA_NETWORK = dict(
        SEED_PEERS = [
            'seed0.badumna.com',
            'seed1.badumna.com',
        ],
        SEED_PORT = BADUMNA_NETWORK['SEED_PORT'],
        MATCHMAKING_PORT = BADUMNA_NETWORK['MATCHMAKING_PORT'],
        MATCHMAKING_PEER = 'matchmaking0.badumna.com',
        APPNAME = BADUMNA_NETWORK['APPNAME'],
)

CAMPAIGN_MONITOR['LIST_ID'] = "3e3a07ddb04699536bc6682c40ed3358"
CAMPAIGN_MONITOR['API_KEY'] = "98df827c15608a3811f11465ad850b9c",


SSL_CERT_ROOT = '/etc/apache2/cert'

# only serve cookies over https:
SESSION_COOKIE_SECURE = True
CSRF_COOKIE_SECURE = True


SITE_ROOT = 'https://cloud.badumna.com'
EMAIL_PORT = 25
LOGGING['loggers'][''] = console_logger
console_logger['level'] = 'WARN'
