from __future__ import absolute_import
from . import base as _base_settings
cm_list_id = _base_settings.CAMPAIGN_MONITOR['LIST_ID']
from .production import *

ec2instance = '54.224.189.78' #replace with ec2-instance public address
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'BadumnaCloud',
        'USER': 'badumna',
        'PASSWORD': 'OeX7KoZ57Z+zxgLR',
        'HOST': 'localhost',
        'PORT': '3306',
    }
}

BADUMNA_NETWORK = dict(
        SEED_PEERS = [ec2instance],
        SEED_PORT = 21251,
        MATCHMAKING_PEER = ec2instance,
        MATCHMAKING_PORT = 21260,
        APPNAME = 'com.badumna.cloud-test',
        HOSTED_SERVICE = False
)

# use development (localhost) stats server locations
STATISTICS_SERVER_HOST = _base_settings.STATISTICS_SERVER_HOST
STATISTICS_SERVER_HOST_INTERNAL = _base_settings.STATISTICS_SERVER_HOST_INTERNAL

SITE_ROOT = 'https://' + ec2instance
ALLOWED_HOSTS = ['*']

EMAIL_USE_TLS = True
EMAIL_HOST = 'smtp.live.com'
EMAIL_PORT = 587
   
# Optional SMTP authentication information for EMAIL_HOST.
EMAIL_HOST_USER = 'indra.kurniawan@outlook.com'
EMAIL_HOST_PASSWORD = 'scalify-bns11'

DEFAULT_FROM_EMAIL = 'cloud@lbadumna.com'
ADMIN_EMAIL = _base_settings.ADMIN_EMAIL
CAMPAIGN_MONITOR['LIST_ID'] = cm_list_id