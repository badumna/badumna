from __future__ import absolute_import
from .base import *

DEBUG = True
TEMPLATE_DEBUG = DEBUG
INTERNAL_IPS=('127.0.0.1',)
STATISTICS_STATUS_FILE = os.path.normpath(os.path.join(__file__, '../../../.statistics.status'))

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3', # 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': 'site.s3db',                      # Or path to database file if using sqlite3.
        'TEST_NAME': 'test.s3db',        # Specifying a name for the test database stops the sqlite backend from creating an in memory database, which has some race issues (see https://code.djangoproject.com/ticket/19856)
        'USER': '',                      # Not used with sqlite3.
        'PASSWORD': '',                  # Not used with sqlite3.
        'HOST': '',                      # Set to empty string for localhost. Not used with sqlite3.
        'PORT': '',                      # Set to empty string for default. Not used with sqlite3.
    }
}

# comment these back in if you want to play with graph rendering (and are impatient):
STATISTICS_REFRESH_INTERVAL = timedelta(seconds=5)
# STATISTICS_HISTORY_LENGTH = 100

SITE_ROOT = 'http://localhost:8000'

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

TEST_RUNNER = 'ControlPanel.tests.runner.CustomNoseRunner'
NOSE_PLUGINS = [
    'ControlPanel.tests.ThirdPartyTestsPlugin',
    'ControlPanel.tests.ScreenshotLoggerPlugin',
]

NOSE_ARGS = [
    '--verbose',
    '--with-screenshots',
    '--logging-filter=!selenium.webdriver.remote.remote_connection,ControlPanel,Site',
]
import shlex
NOSE_ARGS.extend(shlex.split(os.environ.get('NOSE_ARGS', '')))

# FIXME: this is a very hacky way to add 'test' subcommands...
if sys.argv[1:2] == ['test']:
    TEST = True
    if sys.argv[2:3] == ['unit']:
        NOSE_ARGS.append('--attr=!integration')
        del sys.argv[2]
    elif sys.argv[2:3] == ['integration']:
        NOSE_ARGS.append('--attr=integration')
        del sys.argv[2]
    elif sys.argv[2:3] == ['all']:
        del sys.argv[2]
    elif sys.argv[2:3] == []:
        # skip integration tests if no other args given
        NOSE_ARGS.append('--attr=!integration')

    if '--pdb' in sys.argv[2:] + NOSE_ARGS:
        # pdb ALL the things
        NOSE_ARGS.append('--pdb-failures')


if TEST:
    INSTALLED_APPS = list(INSTALLED_APPS) + ['django_nose']
    INSTALLED_APPS = tuple(INSTALLED_APPS)

LOGGING['handlers']['console']['formatter'] = 'simple'

if DEBUG and not TEST:
    console_logger['level'] = 'DEBUG'
    LOGGING['loggers']['ControlPanel'] = console_logger
    LOGGING['loggers']['Site'] = console_logger
    LOGGING['loggers']['django.request'] = console_logger

