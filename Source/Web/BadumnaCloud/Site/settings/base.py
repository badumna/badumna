# Django settings for Site project.
import os, sys
from datetime import timedelta

ADMINS = (
    # ('Your Name', 'your_email@example.com'),
)
ADMIN_EMAIL = 'root@localhost'

MANAGERS = ADMINS

STATISTICS_SERVER_HOST = 'localhost'
STATISTICS_SERVER_HOST_INTERNAL = STATISTICS_SERVER_HOST
STATISTICS_SERVER_PORT = 21257
STATISTICS_REFRESH_INTERVAL = timedelta(minutes=1)
STATISTICS_HISTORY_LENGTH = 50
STATISTICS_STATUS_FILE = '/tmp/badumna-cloud-statistics.status'
MARKETING_SITE_ROOT = '/home/badumna/cloud-www'
MARKETING_SITE_STATIC_ROOT = '/home/badumna/cloud-www.static'
APP_ROOT = '/cp'

CAMPAIGN_MONITOR = dict(
        API_KEY = "DEVELOPMENT_MODE",
        LIST_ID = "fe55595f0806e9234a80342299e6cbdc", # Sample list (for testing / devlopment)
        CUSTOM_FIELDS = [
            dict(Key='source', Value='cloud_registration')
        ],
)

BADUMNA_NETWORK = dict(
        SEED_PEERS = ['localhost'],
        SEED_PORT = 21251,
        MATCHMAKING_PEER = None, # use distributed lookup - otherwise you have to place your public IP here
        MATCHMAKING_PORT = 21260,
        APPNAME = 'com.badumna.cloud0',
        HOSTED_SERVICE = False
)

SUPERVISORD_STATE='/home/monitor/Supervisord.State'
SUPERVISORD_BIN='/home/monitor/Supervisord'
SUPERVISORD_PORT=9000

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# In a Windows environment this must be set to your system time zone.
TIME_ZONE = 'Australia/Melbourne'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en-au'


# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = False

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale.
USE_L10N = True

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = True

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/home/media/media.lawrence.com/media/"
MEDIA_ROOT = ''

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://media.lawrence.com/media/", "http://example.com/media/"
MEDIA_URL = ''

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/home/media/media.lawrence.com/static/"
STATIC_ROOT = '/home/badumna/BadumnaCloud.static/'

# URL prefix for static files.
# Example: "http://media.lawrence.com/static/"
STATIC_URL = '/static/'

# Additional locations of static files
STATICFILES_DIRS = (
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    os.path.abspath(os.path.join(os.path.dirname(__file__), "../static")),
)

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
#    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

# Make this unique, and don't share it with anybody.
SECRET_KEY = 'ocbg0ux6quqqg+)0ek4pdisp&amp;=f_6#=pnuiu+g9ni&amp;j=wpxs$c'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
#     'django.template.loaders.eggs.Loader',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    # Uncomment the next line for simple clickjacking protection:
    # 'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    "django.contrib.auth.context_processors.auth",
    "django.core.context_processors.debug",
    "django.core.context_processors.i18n",
    "django.core.context_processors.media",
    "django.core.context_processors.static",
    "django.core.context_processors.tz",
    "ControlPanel.template_processors.navigation",
    "django.contrib.messages.context_processors.messages",
)


ROOT_URLCONF = 'Site.urls'

# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = 'Site.wsgi.application'

TEMPLATE_DIRS = (
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    os.path.abspath(os.path.join(os.path.dirname(__file__), "../templates")),
)

AUTH_USER_MODEL = 'ControlPanel.BadumnaUser'
SITE_DOMAIN = 'cloud.badumna.com'
DEFAULT_FROM_EMAIL = 'cloud@badumna.com'
MONITOR_EMAIL = 'cloud-monitoring@badumna.com'
EMAIL_HOST = 'localhost'

INSTALLED_APPS = (
    'django.contrib.auth',
    # 'django.contrib.sites',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.admin',
    'django.contrib.admindocs',
    'registration',
    'django_forms_bootstrap',
    'south',
    'ControlPanel',
)

ACCOUNT_ACTIVATION_DAYS = 7 # One-week activation window

# note that these are route names, not literal URLs
LOGIN_URL = 'login'
LOGOUT_URL = 'logout'
LOGIN_REDIRECT_URL = 'ControlPanel.list_apps'

# This is only used by apache, so it doesn't matter if it doesn't
# exist on a dev machine
MAINTENANCE_FILE = '/var/maintenance-mode.txt'

# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'formatters': {
        'verbose': {
            'format': '%(asctime)s [%(process)d/%(thread)d %(module)s %(levelname)s]: %(message)s'
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        },
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'verbose'
        },
    },
    'loggers': {
    },
}


console_logger = {
    'level':'INFO',
    'handlers': ['console'],
}

TEST = False
