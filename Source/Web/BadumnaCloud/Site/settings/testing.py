from __future__ import absolute_import
from . import base as _base_settings
cm_list_id = _base_settings.CAMPAIGN_MONITOR['LIST_ID']
from .production import *

DATABASES['default']['HOST'] = 'localhost'

# use development (localhost) stats server locations
STATISTICS_SERVER_HOST = _base_settings.STATISTICS_SERVER_HOST
STATISTICS_SERVER_HOST_INTERNAL = _base_settings.STATISTICS_SERVER_HOST_INTERNAL

SITE_ROOT = 'https://192.168.0.128'
ALLOWED_HOSTS = ['*']

EMAIL_HOST = 'smtp.unimelb.edu.au'
DEFAULT_FROM_EMAIL = 'noreply@localhost'
BADUMNA_NETWORK = _base_settings.BADUMNA_NETWORK
ADMIN_EMAIL = _base_settings.ADMIN_EMAIL
CAMPAIGN_MONITOR['LIST_ID'] = cm_list_id
