"""
WSGI config for ControlPanel project.

This module contains the WSGI application used by Django's development server
and any production WSGI deployments. It should expose a module-level variable
named ``application``. Django's ``runserver`` and ``runfcgi`` commands discover
this application via the ``WSGI_APPLICATION`` setting.

Usually you will have the standard Django WSGI application here, but it also
might make sense to replace the whole Django WSGI application with a custom one
that later delegates to the Django one. For example, you could introduce WSGI
middleware here, or combine a Django application with an application of another
framework.

"""
import os, sys
sys.path.insert(0, os.path.join(os.path.dirname(os.path.dirname(__file__))))
import local_env
local_env.init()

# relax the OS's umask - files we create *should* be readable by other users
os.umask(0o022)

# This application object is used by any WSGI server configured to use this
# file. This includes Django's development server, if the WSGI_APPLICATION
# setting points here.
from django.core.wsgi import get_wsgi_application
_application = get_wsgi_application()

def application(environ, *a):
    global application

    # allow DJANGO_SETTINGS_MODULE to be specified by apache config SetEnv
    if not 'DJANGO_SETTINGS_MODULE' in os.environ:
        os.environ['DJANGO_SETTINGS_MODULE'] = environ['DJANGO_SETTINGS_MODULE']

    # we don't need this wrapper from now on:
    application = _application
    return _application(environ, *a)
