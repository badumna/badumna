#!/usr/bin/env python
import os
import sys

if __name__ == "__main__":
    import local_env
    local_env.init()

    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "Site.settings.development")
    print "Using settings: %s" % (os.environ['DJANGO_SETTINGS_MODULE'])

    from django.core.management import execute_from_command_line

    if sys.argv[1:2] == ['test']:
        # colorama patches sys.stdout / stderr for coloured output
        import colorama
        colorama.init()
        os.environ['NOSE_WITH_YANC'] = os.environ['NOSE_YANC_COLOR'] = 'on'

    execute_from_command_line(sys.argv)
