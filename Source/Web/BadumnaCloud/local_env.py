import os, sys, subprocess, shutil
ACTIVATED = False

root = os.path.abspath(os.path.dirname(__file__))
P = lambda *r: os.path.join(root, *r)

virtualenv = P("python-env")
requirements = P("deployment/requirements.txt")
cloned_requirements = os.path.join(virtualenv, "requirements.txt")
download_cache = os.path.join(virtualenv, "Cache")

def is_windows():
    return sys.platform.startswith('win')

def install():
    bundled_virtualenv = P("../../../Third Party/Python27/virtualenv.py")
    if os.path.exists(bundled_virtualenv):
        cmd = [sys.executable, bundled_virtualenv]
    else:
        # We deploy this directory (wihout the rest of the badumna repo) to linux
        # servers, in which case we expect `virtualenv`
        # to be installed system-wide.
        cmd = ['virtualenv']

    if is_windows():
        # HACK: pip for python-win64 can't build C extensions without VS PRO (which we don't have on the build box),
        # so we must install them globally and manually:
        cmd.append("--system-site-packages")

    print "Creating virtualenv: %s" % virtualenv
    subprocess.check_call(cmd + [virtualenv])
    activate()
    _install_packages()

def _install_packages():
    exe = 'pip.exe' if is_windows() else 'pip'
    cmd = [exe, "install", "-r", requirements]
    print "Installing packages ..."
    print repr(cmd)
    subprocess.check_call(cmd, shell=is_windows())
    shutil.copy(requirements, cloned_requirements)

def update():
    # we would prefer to run `pip upgrade`, but upgrades in `pip` are not reliable:
    update.has_run = True
    reset()
update.has_run = False

def reset():
    if os.path.exists(virtualenv):
        print "removing %s" % virtualenv
        shutil.rmtree(virtualenv)
    install()

def activate():
    global ACTIVATED
    if not ACTIVATED:
        # virtualenv has different locations on Windows / posix
        possible_files = [
            os.path.join(virtualenv, "Scripts", "activate_this.py"),
            os.path.join(virtualenv, "bin", "activate_this.py"),
        ]
        activate_this = filter(os.path.exists, possible_files)
        assert len(activate_this) == 1, "Can't find activation script in:\n%s" % ("\n".join(possible_files))
        activate_this = activate_this[0]

        execfile(activate_this, dict(__file__=activate_this))
        ACTIVATED = True

def init():
    # ensure virtualenv exists
    os.environ['PIP_DOWNLOAD_CACHE'] = download_cache
    if not os.path.exists(virtualenv):
        install()

    # and make sure python-env/requirements.txt is at least as new as requirements.txt
    # (i.e uprgade packages when requirements change)
    with open(requirements) as f:
        latest = f.read()
    current = ''
    try:
        with open(cloned_requirements) as f:
            current = f.read()
    except IOError:
        pass
    if current != latest:
        print "Current requirements.txt differs from latest - updating"
        update()
    activate()

if __name__ == '__main__':
    init()
    if 'init' not in sys.argv[1:]:
        if not update.has_run:
            update()
