﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Controls.Primitives;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;

using Microsoft.Research.DynamicDataDisplay.DataSources;


namespace Visualizer
{
    public partial class StatConfig : UserControl
    {
        public int UpdateFrequency
        {
            get
            {
                int frequency;
                string frequencyString = (string)this.Dispatcher.Invoke(new Func<string>(delegate { return this.UpdateFrequencyText.Text; }));
                int.TryParse(frequencyString, out frequency);
                return Math.Max(1, frequency);
            }
        }

        private ObservableCollection<GraphConfig> mGraphs = new ObservableCollection<GraphConfig>(GraphConfig.DefaultGraphs.Values);
        private ObservableCollection<SourceConfig> mSources = new ObservableCollection<SourceConfig>(SourceConfig.DefaultSources.Values);


        public StatConfig()
        {
            InitializeComponent();

            this.Graphs.ItemsSource = this.mGraphs;
            this.Sources.ItemsSource = this.mSources;
        }

        private void NewGraph_Click(object sender, RoutedEventArgs e)
        {
            GraphConfig graphConfig = new GraphConfig();
            this.mGraphs.Add(graphConfig);
            this.Graphs.SelectedItem = graphConfig;
            this.Title.Focus();
        }

        private void DeleteGraph_Click(object sender, RoutedEventArgs e)
        {
            List<GraphConfig> selected = new List<GraphConfig>(this.Graphs.SelectedItems.Cast<GraphConfig>());
            foreach (GraphConfig graphConfig in selected)
            {
                this.mGraphs.Remove(graphConfig);
            }
        }

        private void NewSource_Click(object sender, RoutedEventArgs e)
        {
            SourceConfig sourceConfig = new SourceConfig();
            this.mSources.Add(sourceConfig);
            this.Sources.SelectedItem = sourceConfig;
            this.SourceName.Focus();
        }

        private void DeleteSource_Click(object sender, RoutedEventArgs e)
        {
            List<SourceConfig> selected = new List<SourceConfig>(this.Sources.SelectedItems.Cast<SourceConfig>());
            foreach (SourceConfig sourceConfig in selected)
            {
                foreach (GraphConfig graphConfig in this.mGraphs)
                {
                    graphConfig.Sources.Remove(sourceConfig);
                }
                this.mSources.Remove(sourceConfig);
            }
        }

        private void TextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            TextBox textBox = sender as TextBox;
            if (textBox != null)
            {
                textBox.SelectAll();
            }
        }

        private void AddSource_Click(object sender, RoutedEventArgs e)
        {
            GraphConfig graphConfig = this.Graphs.SelectedItem as GraphConfig;
            if (graphConfig != null)
            {
                foreach (SourceConfig sourceConfig in this.Sources.SelectedItems)
                {
                    if (!graphConfig.Sources.Contains(sourceConfig))
                    {
                        graphConfig.Sources.Add(sourceConfig);
                    }
                }
            }
        }

        private void RemoveSource_Click(object sender, RoutedEventArgs e)
        {
            GraphConfig graphConfig = this.Graphs.SelectedItem as GraphConfig;
            if (graphConfig == null)
            {
                return;
            }

            List<SourceConfig> selected = new List<SourceConfig>(this.SourcesInGraph.SelectedItems.Cast<SourceConfig>());
            foreach (SourceConfig sourceConfig in selected)
            {
                graphConfig.Sources.Remove(sourceConfig);
            }
        }

        public void CreateGraphs(double height, out FrameworkElement element, out List<SourceConfig> sources)
        {
            UniformGrid grid = new UniformGrid();
            element = grid;
            grid.Columns = 2;

            sources = new List<SourceConfig>();

            foreach (GraphConfig graphConfig in this.mGraphs)
            {
                if (!graphConfig.IsEnabled)
                {
                    continue;
                }

                Graph graph = new Graph(height);
                grid.Children.Add(graph);

                graph.Title = graphConfig.Title;
                graph.XAxisLabel = graphConfig.XLabel;
                graph.YAxisLabel = graphConfig.YLabel;

                foreach (SourceConfig sourceConfig in graphConfig.Sources)
                {
                    sourceConfig.MakeNewSource();
                    graph.AddSeries(sourceConfig.Name, sourceConfig.Source);
                    sources.Add(sourceConfig);
                }
            }
        }
    }

    class IsNotNull : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return value != null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }
}
