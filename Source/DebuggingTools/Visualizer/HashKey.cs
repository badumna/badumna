﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;

namespace Visualizer
{
    class HashKey : IParser<HashKey>
    {
        public static HashKey Parse(ref string line)
        {
            if (line.Length < 40)
            {
                return null;
            }

            HashKey hashKey = new HashKey(line.Substring(0, 40));
            line = line.Substring(40);
            return hashKey;
        }

        HashKey IParser<HashKey>.Parse(ref string line)
        {
            return HashKey.Parse(ref line);
        }


        private string mHash;
        public string HashString { get { return this.mHash; } }

        private double mHashValue;
        public double HashValue { get { return this.mHashValue; } }

        private double mAngle;
        public double Angle { get { return this.mAngle; } }


        public HashKey()
        {
        }

        public HashKey(string hashKey)
        {
            if (hashKey.Length != 40)
            {
                throw new ArgumentException("HashKey must be 40 hex characters");
            }

            this.mHash = hashKey;

            for (int i = 0; i < 5; i++)
            {
                string uintPart = hashKey.Substring(i * 8, 8);
                uint uintValue = uint.Parse(uintPart, NumberStyles.HexNumber);
                this.mHashValue *= (long)1 << 32;
                this.mHashValue += uintValue;
            }

            this.mAngle = 2.0 * Math.PI * (this.mHashValue / Math.Pow(2.0, 160.0) - 0.5);
        }
    }
}
