﻿using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System;
using System.Diagnostics;

namespace Visualizer
{
    public class MessageDiagram
    {
        public class Node
        {
            public string Name { get; private set; }
            public int Index { get; private set; }
            public ObservableCollection<double> LifeTimeEvents { get; private set; }

            public bool IsUp { get { return this.LifeTimeEvents.Count % 2 == 1; } }


            public Node(string name, int index)
            {
                this.Name = name;
                this.Index = index;
                this.LifeTimeEvents = new ObservableCollection<double>();
            }
        }

        public struct MessageHop
        {
            public double TimeMs { get { return this.mTimeMs; } }
            public int NodeIndex { get { return this.mNodeIndex; } }

            private double mTimeMs;
            private int mNodeIndex;

            public MessageHop(int nodeIndex, double timeMs)
            {
                this.mNodeIndex = nodeIndex;
                this.mTimeMs = timeMs;
            }
        }

        public class Message : INotifyPropertyChanged
        {
            public string Label { get; private set;}
            public int Size { get; private set; }
            
            public List<MessageHop> Hops { get; private set; }
            public MessageHop Start { get { return this.Hops[0]; } }
            public MessageHop End { get { return this.Hops[this.Hops.Count - 1]; } }

            public bool IsCompleted
            {
                get { return this.mIsCompleted; }
                set
                {
                    this.mIsCompleted = value;
                    this.OnPropertyChanged("IsCompleted");
                }
            }

            public bool IsDropped
            {
                get { return this.mIsDropped; }
                set
                {
                    this.mIsDropped = value;
                    this.OnPropertyChanged("IsDropped");
                }
            }

            public string DropReason
            {
                get { return this.mDropReason; }
                set
                {
                    this.mDropReason = value;
                    this.OnPropertyChanged("DropReason");
                }
            }

            public event PropertyChangedEventHandler PropertyChanged;


            private bool mIsCompleted;
            private bool mIsDropped;
            private string mDropReason;


            public Message(string label, MessageHop initialHop, int size)
            {
                this.Label = label;
                this.Size = size;
                this.Hops = new List<MessageHop> { initialHop };
            }

            public void AddHop(int nodeIndex, double timeMs)
            {
                this.Hops.Add(new MessageHop(nodeIndex, timeMs));
                this.OnPropertyChanged("Hops");
            }

            private void OnPropertyChanged(string propertyName)
            {
                PropertyChangedEventHandler handler = this.PropertyChanged;
                if (handler != null)
                {
                    handler(this, new PropertyChangedEventArgs(propertyName));
                }
            }
        }


        public ReadOnlyObservableCollection<Message> Messages { get { return this.mReadOnlyMessages; } }
        public ReadOnlyObservableCollection<Node> Nodes { get { return this.mReadOnlyNodes; } }

        private ObservableCollection<Node> mNodes;
        private ReadOnlyObservableCollection<Node> mReadOnlyNodes;

        private Dictionary<ulong, Message> mMessages;
        private ObservableCollection<Message> mObserveableMessages;
        private ReadOnlyObservableCollection<Message> mReadOnlyMessages;

        
        public MessageDiagram()
        {
            this.mNodes = new ObservableCollection<Node>();
            this.mReadOnlyNodes = new ReadOnlyObservableCollection<Node>(this.mNodes);

            this.mMessages = new Dictionary<ulong, Message>();
            this.mObserveableMessages = new ObservableCollection<Message>();
            this.mReadOnlyMessages = new ReadOnlyObservableCollection<Message>(this.mObserveableMessages);
        }


        public void AddNode(TimeSpan time, int nodeId, string nodeName)
        {
            lock (this.mNodes)
            {
                if (nodeId >= this.mNodes.Count)
                {
                    Debug.Assert(nodeId == this.mNodes.Count);
                    this.mNodes.Add(new Node(nodeName, nodeId));
                }

                var node = this.mNodes[nodeId];
                if (!node.IsUp)
                {
                    node.LifeTimeEvents.Add(time.TotalMilliseconds);
                }
            }
        }

        public void RemoveNode(TimeSpan time, int nodeId)
        {
            lock (this.mNodes)
            {
                Node node = this.mNodes[nodeId];

                if (node.IsUp)
                {
                    node.LifeTimeEvents.Add(time.TotalMilliseconds);
                }
            }
        }

        public void AddMessage(TimeSpan time, ulong messageId, int sourceNodeId, string label, int size)
        {
            lock (this.mMessages)
            {
                Message message = new Message(label, new MessageHop(sourceNodeId, time.TotalMilliseconds), size);
                this.mMessages.Add(messageId, message);
                this.mObserveableMessages.Add(message);
            }
        }

        public void AddMessageHop(TimeSpan time, ulong messageId, int nodeId, bool isFinalHop)
        {
            lock (this.mMessages)
            {
                Message message;
                if (!this.mMessages.TryGetValue(messageId, out message))
                {
                    return;
                }

                message.AddHop(nodeId, time.TotalMilliseconds);
                if (isFinalHop)
                {
                    message.IsCompleted = true;
                }
            }
        }

        public void DropMessage(TimeSpan time, ulong messageId, int nodeId, string reason)
        {
            lock (this.mMessages)
            {
                Message message;
                if (!this.mMessages.TryGetValue(messageId, out message))
                {
                    return;
                }

                message.AddHop(nodeId, time.TotalMilliseconds);
                message.IsCompleted = true;
                message.IsDropped = true;
                message.DropReason = reason;
            }
        }



#if false
        public void Render(Canvas canvas)
        {
            canvas.Children.Clear();
            double lastEventTime = MessageDiagram.mHeight - 10;// : this.mMessages[this.mMessages.Count - 1].End.Y;

            // Background 
            Brush backgroundBrush = new SolidColorBrush(Colors.BlanchedAlmond);

            Rectangle background = new Rectangle()
            {
                Width = MessageDiagram.CanvasDimension.Width,
                Height = MessageDiagram.CanvasDimension.Height,
                Fill = backgroundBrush,
                Stroke = backgroundBrush
            };
            Canvas.SetLeft(background, MessageDiagram.CanvasDimension.X);
            Canvas.SetTop(background, MessageDiagram.CanvasDimension.Y);
            canvas.Children.Add(background);

            // Draw node lines 
            lock (this.mNodes)
            {
                foreach (Node node in this.mNodes)
                {
                    TextBlock nodeName = new TextBlock(new Run(node.Name));
                    nodeName.FontSize = 100;
                    Canvas.SetLeft(nodeName, node.XPosition - MessageDiagram.mMinimumNodeWidth / 2);
                    Canvas.SetTop(nodeName, node.LifeTimeEvents[0] * MessageDiagram.mTimeScale - 200);
                    canvas.Children.Add(nodeName);

                    for (int i = 0; i < node.LifeTimeEvents.Count; i += 2)
                    {
                        Line line = new Line();

                        line.X1 = node.XPosition;
                        line.X2 = node.XPosition;
                        line.Y1 = node.LifeTimeEvents[i] * MessageDiagram.mTimeScale;
                        line.Y2 = (node.LifeTimeEvents.Count > i + 1 ? node.LifeTimeEvents[i + 1] : lastEventTime) * MessageDiagram.mTimeScale;
                        line.StrokeThickness = 5;

                        if (node.Name.Contains("router"))
                        {
                            line.Stroke = Brushes.Gray;
                        }
                        else
                        {
                            line.Stroke = Brushes.Black;
                        }

                        canvas.Children.Add(line);
                    }
                }
            }

            // Draw message 
            lock (this.mMessages)
            {
                double lastMessage = 0;
                double lastMessageLeft = 0;
                double lastMessageTop = 0;
                double textHeight = 50;

                foreach (Message message in this.mMessages.Values)
                {
                    double left = -500;
                    double top = message.Start.Y * MessageDiagram.mTimeScale;

                    if (lastMessageTop + textHeight > top)
                    {
                        top = lastMessageTop + textHeight;
                    }

                    string label = string.Format("{0}-{1} {2}", message.Start.Y, message.End.Y, message.Label);
                    if (message.IsDropped)
                    {
                        label += String.Format("({0})", message.DropReason);
                    }

                    TextBlock messageLabel = new TextBlock(new Run(label));
                    messageLabel.FontSize = textHeight;
                    if (message.IsDropped)
                    {
                        messageLabel.Foreground = Brushes.Red;
                    }
                    Canvas.SetLeft(messageLabel, left);
                    Canvas.SetTop(messageLabel, top - 25);
                    canvas.Children.Add(messageLabel);

                    lastMessage = message.Start.Y;
                    lastMessageLeft = left;
                    lastMessageTop = top;

                    Line line = new Line();

                    line.X1 = message.Start.X;
                    line.Y1 = message.Start.Y * MessageDiagram.mTimeScale;

                    int segmentNumber = 0;
                    foreach (Point point in message.Points)
                    {
                        line.X2 = point.X;
                        line.Y2 = point.Y * MessageDiagram.mTimeScale;
                      
                        if (segmentNumber == 2)
                        {
                            line.StrokeThickness = 10;
                        }
                        else
                        {
                            line.StrokeThickness = 5;
                        }

                        if (message.IsDropped)
                        {
                            line.Stroke = Brushes.Red;
                        }
                        else
                        {
                            line.Stroke = Brushes.Black;
                        }

                        canvas.Children.Add(line);

                        segmentNumber++;
                        line = new Line();

                        line.X1 = point.X;
                        line.Y1 = point.Y * MessageDiagram.mTimeScale;
                    }
                }
            }
        }
#endif


    }
}
