﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Diagnostics;
using System.Net;
using System.Windows.Threading;

using IO = System.IO;

using Microsoft.Win32;
using System.IO;
using System.Runtime.InteropServices;
using System.ComponentModel;
using System.Threading;


namespace Visualizer
{
    public partial class MainWindow : Window
    {
        private Queue<string> mLines;
        private string mFilename;

        private int mLastNodeId;
        private Dictionary<PeerAddress, int> mNodeIds;

        private DispatcherTimer mPlaybackTimer;

        private long mInitialTimeMs;
        private int mFrameCount;

        private volatile bool mWorkerShouldStop;


        public MainWindow()
        {
            InitializeComponent();
            this.DhtCanvas.DefaultViewport = DhtInfo.CanvasDimension;
            this.PositionCanvas.DefaultViewport = EntityInfo.CanvasDimension;
            this.Reset();
        }

        private void Reset()
        {
            this.mLastNodeId = 0;
            this.mNodeIds = new Dictionary<PeerAddress, int>();
            this.mFrameCount = 0;
            this.mInitialTimeMs = -1;
        }

        // TODO: Make as enumerator
        private string[] ReadAllLinesShare(string filename)
        {
            List<string> result = new List<string>();

            using (FileStream stream = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            using (StreamReader reader = new StreamReader(stream))
            {
                while (true)
                {
                    string line = reader.ReadLine();
                    if (line == null)
                    {
                        return result.ToArray();
                    }
                    result.Add(line);
                }
            }
        }

        private void LoadFile(string filename)
        {
            this.Reset();
            this.mFilename = filename;
            this.mLines = new Queue<string>(this.ReadAllLinesShare(filename));
        }

        private void SeekToTime(long timeMs)
        {
            if (this.mFilename == null)
            {
                return;
            }

            this.LoadFile(this.mFilename);

            while (true)
            {
                string line = this.mLines.Peek();
                string[] parts = line.Split(',');

                long lineTimeMs = long.Parse(parts[0]);

                if (this.mInitialTimeMs < 0)
                {
                    this.mInitialTimeMs = lineTimeMs;
                }

                if (lineTimeMs - this.mInitialTimeMs >= timeMs)
                {
                    return;
                }

                int itemCount = int.Parse(parts[2]);
                for (int i = 0; i < itemCount + 1; i++)
                {
                    this.mLines.Dequeue();
                }
            }
        }

        private void MakeVideoFrames()
        {
            string frameName = this.FrameNames.Text.Trim();
            if (frameName.Length == 0)
            {
                return;
            }

            long startTime;
            long.TryParse(this.StartTime.Text, out startTime);
            this.SeekToTime(startTime);

            long endTime;
            long.TryParse(this.EndTime.Text, out endTime);

            this.Progress.Value = 0;
            this.ProgressText.Text = "0%";

            Action<int> progressNotification = null;
            progressNotification =
                delegate(int percentage)
                {
                    if (!this.Dispatcher.CheckAccess())
                    {
                        this.Dispatcher.BeginInvoke(progressNotification, percentage);
                        return;
                    }
                    this.Progress.Value = percentage;
                    this.ProgressText.Text = String.Format("{0}%", percentage);
                };

            this.mWorkerShouldStop = false;
            Thread worker = new Thread(delegate() { this.MakeVideoFrames(frameName, progressNotification, endTime); });
            worker.SetApartmentState(ApartmentState.STA);
            worker.IsBackground = true;
            worker.Start();
        }

        private void MakeVideoFrames(string frameName, Action<int> progressNotification, long endTime)
        {
            int totalLines = this.mLines.Count;

            int fps = 30;
            double msPerFrame = 1000.0 / fps;
            double msToNextChange = 0.0;
            double time = 0.0;
            int interFrameTime;
            int width = 700;
            int height = 700;

            ZoomCanvas dhtFrameCanvas = new ZoomCanvas() { Width = width, Height = height, DefaultViewport = DhtInfo.CanvasDimension };
            ZoomCanvas positionFrameCanvas = new ZoomCanvas() { Width = width, Height = height, DefaultViewport = EntityInfo.CanvasDimension };

            do
            {
                interFrameTime = this.ParseBlock(this.mLines, dhtFrameCanvas, positionFrameCanvas, false);
                msToNextChange += interFrameTime;

                string entitiesName = string.Format(@"video\{0}_ent_{1:000000}.png", frameName, this.mFrameCount);
                VideoUtil.SaveToPng(entitiesName, positionFrameCanvas, width, width);

                //string dhtName = string.Format(@"video\{0}_dht_{1:000000}.png", frameName, this.mFrameCount);
                //VideoUtil.SaveToPng(dhtName, dhtFrameCanvas, width, width);
                
                this.mFrameCount++;
                msToNextChange -= msPerFrame;

                while (msToNextChange > 0)
                {
                    File.Copy(entitiesName, string.Format(@"video\{0}_ent_{1:000000}.png", frameName, this.mFrameCount));
                    //File.Copy(dhtName, string.Format(@"video\{0}_dht_{1:000000}.png", frameName, this.mFrameCount));
                    this.mFrameCount++;
                    msToNextChange -= msPerFrame;
                }

                progressNotification((int)Math.Ceiling((1.0 - (double)this.mLines.Count / totalLines) * 100.0));
                time += interFrameTime;
            }
            while (!this.mWorkerShouldStop &&
                   interFrameTime > 0 &&
                   !(endTime > 0 && time - this.mInitialTimeMs >= endTime));
        }

        private int ParseBlock(Queue<string> lines)
        {
            return this.ParseBlock(lines, this.DhtCanvas, this.PositionCanvas, true);
        }

        private int ParseBlock(Queue<string> lines, ZoomCanvas dhtCanvas, ZoomCanvas positionCanvas, bool updateTime)
        {
            long initialTimeMs = -1;
            long lastTimeMs = -1;
            long timeMs = -1;

            List<DhtInfo> dhtInfos = new List<DhtInfo>();
            Dictionary<int, EntityInfo> entityInfos = new Dictionary<int, EntityInfo>();
            Dictionary<int, ImInfo> imInfos = new Dictionary<int, ImInfo>();

            bool failed = false;

            while (lines.Count > 0)
            {
                try
                {
                    string line = lines.Peek();
                    string[] parts = line.Split(',');

                    timeMs = long.Parse(parts[0]);
                    if (initialTimeMs < 0)
                    {
                        initialTimeMs = timeMs;
                    }
                    PeerAddress peerAddress = PeerAddress.Parse(ref parts[1]);
                    int itemCount = int.Parse(parts[2]);

                    // Detect a big jump in time => new checkpoint round
                    if (lastTimeMs > 0 && timeMs - lastTimeMs > 500)  // TODO: Not the best
                    {
                        break;
                    }
                    lastTimeMs = timeMs;
                    lines.Dequeue();  // Dequeue peeked header

                    int nodeId = this.GetNodeId(peerAddress);

                    dhtInfos[nodeId] = new DhtInfo(lines);
                    entityInfos[nodeId] = new EntityInfo(lines);
                    imInfos[nodeId] = new ImInfo(lines);
                }
                catch (Exception) // TODO: More specific
                {
                    failed = true;
                }
            }

            // -------8<----------- Cut here to remove dodgy code -----------------8<----------------------------
            //    
            // Fake the details for the missing peer in the "three avatars run in" demo
            //
            //EntityInfo fakeInfo = null;
            //foreach (EntityInfo info in entityInfos.Values)
            //{
            //    if (info.LocalEntities[0].Guid.ToString() == "Full cone|128.250.76.96:2045-58156")
            //    {
            //        fakeInfo = new EntityInfo();
            //        fakeInfo.RemoteEntities.AddRange(info.RemoteEntities);


            //        fakeInfo.LocalEntities.Add(new EntityInfo.Entity
            //        {
            //            Guid = new BadumnaId(new PeerAddress("Full cone", new IPEndPoint(IPAddress.Parse("128.250.77.151"), 2047)), 39682),
            //            InterestRadius = 100,
            //            Radius = 1,
            //            Position = new Vector3(1344.617, 5.332084, 1559.714)
            //        });
            //        break;
            //    }
            //}
            //if (fakeInfo != null)
            //{
            //    entityInfos[99] = fakeInfo;
            //}
            //
            // -------8<----------- Cut here to remove dodgy code -----------------8<----------------------------



            DhtInfo.Render(dhtInfos, dhtCanvas);
            EntityInfo.Render(entityInfos, positionCanvas);
            if (this.mInitialTimeMs < 0)
            {
                this.mInitialTimeMs = initialTimeMs;
            }

            if (updateTime)
            {
                this.Time.Text = String.Format("{0:N3}", (lastTimeMs - this.mInitialTimeMs) / 1000.0);
            }

            int result = (int)(timeMs - initialTimeMs);  // Returns 0 if there are no more frames
            if (failed)
            {
                result = 0;
            }

            //if (this.RecordFrames.IsChecked == true)
            //{
            //    string frameName = this.FrameNames.Text.Trim();
            //    if (frameName.Length > 0)
            //    {
            //        VideoUtil.SaveToPng(string.Format(@"video\{0}_dht_{1:0000}.png", frameName, this.mFrameCount), this.DhtCanvas, 2048, 2048);
            //        VideoUtil.SaveToPng(string.Format(@"video\{0}_ent_{1:0000}.png", frameName, this.mFrameCount), this.PositionCanvas, 2048, 2048);
            //        this.mFrameCount++;
            //    }
            //}

            return result;
        }

        private int GetNodeId(PeerAddress address)
        {
            int id;
            if (!this.mNodeIds.TryGetValue(address, out id))
            {
                this.mLastNodeId++;
                id = this.mLastNodeId;
                this.mNodeIds[address] = id;
            }
            return id;
        }

        private void LoadFile_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog fileDialog = new OpenFileDialog();
            if (fileDialog.ShowDialog() == true)
            {
                this.LoadFile(fileDialog.FileName);
            }
        }

        private void Parse_Click(object sender, RoutedEventArgs e)
        {
            if (this.mLines == null)
            {
                return;
            }

            this.ParseBlock(this.mLines);
        }

        private void Play_Click(object sender, RoutedEventArgs e)
        {
            if (this.mLines == null)
            {
                return;
            }

            double speedFactor = 1.0;

            if (this.mPlaybackTimer == null)
            {
                this.mPlaybackTimer = new DispatcherTimer();
                this.mPlaybackTimer.Tick += delegate
                {
                    int delayMs = this.ParseBlock(this.mLines);
                    if (delayMs > 0)
                    {
                        this.mPlaybackTimer.Interval = TimeSpan.FromMilliseconds(delayMs / speedFactor);
                        //this.mPlaybackTimer.Interval = TimeSpan.Zero;
                    }
                    else
                    {
                        this.mPlaybackTimer.Stop();
                    }
                };
            }

            this.mPlaybackTimer.Interval = TimeSpan.Zero;
            this.mPlaybackTimer.Start();
        }

        private void Stop_Click(object sender, RoutedEventArgs e)
        {
            if (this.mPlaybackTimer != null)
            {
                this.mPlaybackTimer.Stop();
            }
        }

        private void ReloadFile_Click(object sender, RoutedEventArgs e)
        {
            if (this.mFilename != null)
            {
                this.LoadFile(this.mFilename);
            }
        }

        private void ResetFrameCount_Click(object sender, RoutedEventArgs e)
        {
            this.mFrameCount = 0;
        }

        private void MakeFrames_Click(object sender, RoutedEventArgs e)
        {
            this.MakeVideoFrames();
        }

        private void CancelWorker_Click(object sender, RoutedEventArgs e)
        {
            this.mWorkerShouldStop = true;
        }

        private void Seek_Click(object sender, RoutedEventArgs e)
        {
            long seekTime;
            if (long.TryParse(this.StartTime.Text, out seekTime))
            {
                this.SeekToTime(seekTime);
            }
        }
    }
}
