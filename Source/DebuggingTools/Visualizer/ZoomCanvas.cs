﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows;
using System.Windows.Media;
using System.Windows.Input;
using System.Windows.Shapes;

namespace Visualizer
{
    public class ZoomCanvas : Canvas
    {
        private Rect mDefaultViewport;
        public Rect DefaultViewport
        {
            get { return this.mDefaultViewport; }
            set
            {
                this.mDefaultViewport = value;
                if (!this.mViewportHasBeenInitialized)
                {
                    this.Viewport = value;
                }
            }
        }

        private Rect mViewport;
        public Rect Viewport
        {
            get { return this.mViewport; }
            set
            {
                this.mViewportHasBeenInitialized = true;
                this.mViewport = value;
                this.UpdateTransform();
            }
        }

        private bool mViewportHasBeenInitialized;
        private Point mDragStart;

        public ZoomCanvas()
        {
            this.SizeChanged += delegate { this.UpdateTransform(); };
        }

        protected override void OnMouseDown(MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
            {
                this.mDragStart = e.GetPosition(this);
                e.Handled = true;
                return;
            }

            base.OnMouseDown(e);
        }

        protected override void OnMouseUp(MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
            {
                Point dragEnd = e.GetPosition(this);
                e.Handled = true;

                Rect newViewport = new Rect(this.mDragStart, dragEnd);
                if (newViewport.Width < 200 || newViewport.Height < 200)
                {
                    return;  // Too small a drag, probably accidental
                }

                this.Viewport = newViewport;

                return;
            }

            if (e.ChangedButton == MouseButton.Right)
            {
                this.Viewport = this.DefaultViewport;
                e.Handled = true;
                return;
            }

            base.OnMouseDown(e);
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);
        }

        private void UpdateTransform()
        {
            TransformGroup transform = new TransformGroup();

            double xScale = this.ActualWidth / this.Viewport.Width;
            double yScale = this.ActualHeight / this.Viewport.Height;
            double scale = Math.Min(xScale, yScale);

            transform.Children.Add(new TranslateTransform(-this.Viewport.X, -this.Viewport.Y));
            transform.Children.Add(new ScaleTransform(scale, scale));
            this.RenderTransform = transform;
        }
    }
}
