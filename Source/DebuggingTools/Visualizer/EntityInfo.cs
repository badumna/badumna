﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows;
using System.Windows.Shapes;
using System.Windows.Media;
using System.Diagnostics;

namespace Visualizer
{
    // Facade.mStack.mObjectManager.mLocalWrappers{}.value.mGuid
    // Facade.mStack.mObjectManager.mLocalWrappers{}.value.mNetworkObjectData.Position
    // Facade.mStack.mObjectManager.mLocalWrappers{}.value.mNetworkObjectData.AreaOfInterestRadius
    // Facade.mStack.mObjectManager.mLocalWrappers{}.value.mNetworkObjectData.Radius
    //
    // Facade.mStack.mObjectManager.mRemoteWrappers{}.value.mGuid
    // Facade.mStack.mObjectManager.mRemoteWrappers{}.value.mSpatialEntityData.Position

    public class EntityInfo
    {
        public static readonly Rect CanvasDimension = new Rect(-1000.0, -1000.0, 2000.0, 2000.0);
        //public static readonly Rect CanvasDimension = new Rect(680.0, 740.0, 650.0, 650.0);


        internal class Entity
        {
            public BadumnaId Guid;
            public Vector3 Position;
            public float InterestRadius;
            public float Radius;
        }


        private List<Entity> mLocalEntities = new List<Entity>();
        internal List<Entity> LocalEntities { get { return this.mLocalEntities; } }

        private List<Entity> mRemoteEntities = new List<Entity>();
        internal List<Entity> RemoteEntities { get { return this.mRemoteEntities; } }


        public EntityInfo()
        {
        }

        public EntityInfo(List<Badumna.DataTypes.BadumnaId> originalGuids, List<Badumna.DataTypes.Vector3> originalPositions, List<float> originalInterestRadii,
            List<float> originalRadii, List<Badumna.DataTypes.BadumnaId> replicaGuids, List<Badumna.DataTypes.Vector3> replicaPositions)
        {
            List<BadumnaId> internalOriginalGuids = originalGuids.ConvertAll<BadumnaId>(
                delegate(Badumna.DataTypes.BadumnaId badumnaId)
                {
                    string str = badumnaId.ToString();
                    return BadumnaId.Parse(ref str);
                });

            List<Vector3> internalOriginalPositions = originalPositions.ConvertAll<Vector3>(
                delegate(Badumna.DataTypes.Vector3 badumnaVector)
                {
                    return new Vector3(badumnaVector);
                });

            List<BadumnaId> internalReplicaGuids = replicaGuids.ConvertAll<BadumnaId>(
                delegate(Badumna.DataTypes.BadumnaId badumnaId)
                {
                    string str = badumnaId.ToString();
                    return BadumnaId.Parse(ref str);
                });

            List<Vector3> internalReplicaPositions = replicaPositions.ConvertAll<Vector3>(
                delegate(Badumna.DataTypes.Vector3 badumnaVector)
                {
                    return new Vector3(badumnaVector);
                });

            this.Initialize(internalOriginalGuids, internalOriginalPositions, originalInterestRadii, originalRadii, internalReplicaGuids, internalReplicaPositions);
        }

        public EntityInfo(Queue<string> lines)
        {
            List<BadumnaId> originalGuids = Parser.ParseList<BadumnaId>(lines.Dequeue());
            List<Vector3> originalPositions = Parser.ParseList<Vector3>(lines.Dequeue());
            List<float> originalInterestRadii = Parser.ParseListFloat(lines.Dequeue());
            List<float> originalRadii = Parser.ParseListFloat(lines.Dequeue());

            List<BadumnaId> replicaGuids = Parser.ParseList<BadumnaId>(lines.Dequeue());
            List<Vector3> replicaPositions = Parser.ParseList<Vector3>(lines.Dequeue());

            this.Initialize(originalGuids, originalPositions, originalInterestRadii, originalRadii, replicaGuids, replicaPositions);
        }

        private void Initialize(List<BadumnaId> originalGuids, List<Vector3> originalPositions, List<float> originalInterestRadii,
            List<float> originalRadii, List<BadumnaId> replicaGuids, List<Vector3> replicaPositions)
        {
            if (originalGuids.Count != originalPositions.Count ||
                originalGuids.Count != originalInterestRadii.Count ||
                originalGuids.Count != originalRadii.Count)
            {
                throw new ArgumentException("Missing details");
            }

            for (int i = 0; i < originalGuids.Count; i++)
            {
                this.mLocalEntities.Add(new Entity
                {
                    Guid = originalGuids[i],
                    Position = originalPositions[i],
                    InterestRadius = originalInterestRadii[i],
                    Radius = originalRadii[i]
                });
            }

            if (replicaGuids.Count != replicaPositions.Count)
            {
                throw new ArgumentException("Missing details");
            }

            for (int i = 0; i < replicaGuids.Count; i++)
            {
                this.mRemoteEntities.Add(new Entity
                {
                    Guid = replicaGuids[i],
                    Position = replicaPositions[i]
                });
            }
        }

        public static void Render(Dictionary<int, EntityInfo> entityInfos, ZoomCanvas canvas)
        {
            canvas.Children.Clear();

            Brush backgroundBrush = new SolidColorBrush(Color.FromRgb(0x33, 0x67, 0xA0));
            Brush entityBrush = new SolidColorBrush(Color.FromRgb(0xFD, 0x6A, 0x22));
            Brush interestRegionBrush = new SolidColorBrush(Color.FromRgb(0xFD, 0xDB, 0x22));

            Rectangle background = new Rectangle()
            {
                Width = EntityInfo.CanvasDimension.Width,
                Height = EntityInfo.CanvasDimension.Height,
                Fill = backgroundBrush,
                Stroke = backgroundBrush
            };
            Canvas.SetLeft(background, EntityInfo.CanvasDimension.X);
            Canvas.SetTop(background, EntityInfo.CanvasDimension.Y);
            canvas.Children.Add(background);


            foreach (EntityInfo entityInfo in entityInfos.Values)
            {
                foreach (Entity entity in entityInfo.mLocalEntities)
                {
                    Ellipse body = new Ellipse
                    {
                        Width = 8 * entity.Radius,  //2 * entity.InterestRadius,
                        Height = 8 * entity.Radius,
                        Fill = entityBrush,
                        Stroke = entityBrush
                    };
                    Canvas.SetLeft(body, entity.Position.X - 4 * entity.Radius);
                    Canvas.SetTop(body, entity.Position.Z - 4 * entity.Radius);

                    Ellipse interest = new Ellipse
                    {
                        Width = 2 * entity.InterestRadius,
                        Height = 2 * entity.InterestRadius,
                        Fill = Brushes.Transparent,
                        Stroke = interestRegionBrush,
                        StrokeThickness = 3.0,
                        StrokeDashArray = new DoubleCollection(new double[] { 5, 2 })
                    };
                    Canvas.SetLeft(interest, entity.Position.X - entity.InterestRadius);
                    Canvas.SetTop(interest, entity.Position.Z - entity.InterestRadius);

                    canvas.Children.Add(body);
                    canvas.Children.Add(interest);
                }
            }

            foreach (EntityInfo entityInfo in entityInfos.Values)
            {
                // TODO: Was the assert below ever actually valid?  Why wouldn't it work for multiple entities?
                //Debug.Assert(entityInfo.mLocalEntities.Count <= 1, "Interest connections don't work for multiple local entities yet");
                foreach (Entity entity in entityInfo.mLocalEntities)
                {
                    foreach (Entity remoteEntity in entityInfo.mRemoteEntities)
                    {
                        double x1 = entity.Position.X;
                        double y1 = entity.Position.Z;
                        double x2 = remoteEntity.Position.X;
                        double y2 = remoteEntity.Position.Z;

                        // Don't draw the interest relationships during a teleport because it clutters things too much
                        if (Math.Pow(x1 - x2, 2.0) + Math.Pow(y1 - y2, 2.0) > 1500.0 * 1500.0)
                        {
                            continue;
                        }

                        Line connection = new Line
                        {
                            X1 = x1,
                            Y1 = y1,
                            X2 = x2,
                            Y2 = y2,
                            Stroke = Brushes.White,
                            StrokeThickness = 0.75
                        };
                        canvas.Children.Add(connection);
                    }
                }
            }
        }
    }
}
