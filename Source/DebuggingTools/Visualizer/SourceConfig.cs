﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Windows;
using Microsoft.Research.DynamicDataDisplay.DataSources;
using System.Data;
using System.Windows.Threading;

namespace Visualizer
{
    public class SourceConfig : INotifyPropertyChanged
    {
        public static Dictionary<string, SourceConfig> DefaultSources = new Dictionary<string, SourceConfig>
        {
            { "AvgInbound", new SourceConfig("Avg Inbound Bandwidth", "Avg(Value)", "Time = '$now' AND Name = 'AverageInboundBandwidth'") },
            { "MaxInbound", new SourceConfig("Max Inbound Bandwidth", "Max(Value)", "Time = '$now' AND Name = 'AverageInboundBandwidth'") },
            { "AvgOutbound", new SourceConfig("Avg Outbound Bandwidth", "Avg(Value)", "Time = '$now' AND Name = 'AverageOutboundBandwidth'") },
            { "MaxOutbound", new SourceConfig("Max Outbound Bandwidth", "Max(Value)", "Time = '$now' AND Name = 'AverageOutboundBandwidth'") },

            { "TotalMissing", new SourceConfig("Total Missing Objects", "Sum(Value)", "Time = '$now' AND Name = 'MissingObjects'") },
            { "AvgMissing", new SourceConfig("Avg Missing Objects", "Avg(Value)", "Time = '$now' AND Name = 'MissingObjects'") },
            { "MaxMissing", new SourceConfig("Max Missing Objects", "Max(Value)", "Time = '$now' AND Name = 'MissingObjects'") },
            { "TotalHallucinated", new SourceConfig("Total Hallucinated Objects", "Sum(Value)", "Time = '$now' AND Name = 'HallucinatedObjects'") },
            { "AvgHallucinated", new SourceConfig("Avg Hallucinated Objects", "Avg(Value)", "Time = '$now' AND Name = 'HallucinatedObjects'") },
            { "MaxHallucinated", new SourceConfig("Max Hallucinated Objects", "Max(Value)", "Time = '$now' AND Name = 'HallucinatedObjects'") },
            { "AvgDistErr", new SourceConfig("Avg Distance Error", "Avg(Value)", "Time = '$now' AND Name = 'AverageDistanceError'") },
            { "MaxDistErr", new SourceConfig("Max Distance Error", "Max(Value)", "Time = '$now' AND Name = 'MaximumDistanceError'") },

            { "AvgImBytes", new SourceConfig("Avg Interest Management", "Avg(Value)", "Time = '$now' AND Group = 'InterestManagement' AND Name = 'AverageIMBandwidth'") },
            { "MaxImBytes", new SourceConfig("Max Interest Management", "Max(Value)", "Time = '$now' AND Group = 'InterestManagement' AND Name = 'AverageIMBandwidth'") },
              
            { "GossipIMCalls", new SourceConfig("IM calls", "Sum(Value)", "Time = '$now' AND Name = 'IMCalls'") },
          //  { "GossipInteresections", new SourceConfig("Known Intersections", "Avg(Value)", "Time = '$now' AND Name = 'KnownIntersections'") },

           // { "GossipRatio", new SourceConfig("Gossip Ratio", "Avg(Value)", "Time = '$now' AND Group = 'Gossip' AND Name = 'IntroRatio'") },   
          //  { "GossipRate", new SourceConfig("Gossip Rate", "Avg(Value)", "Time = '$now' AND Group = 'Gossip' AND Name = 'Rate'") },            
          //  { "GossipDiscovery", new SourceConfig("Gossip Rate", "Avg(Value)", "Time = '$now' AND Group = 'Gossip' AND Name = 'Discovery'") }, 

            { "PeerCount", new SourceConfig("Peer Count", "Count(Value)", "Time = '$now' AND Name = 'OnlinePeers'") }, // Always exactly one OnlinePeers entry per peer per sample

            { "AvgEstimatedBandwidth", new SourceConfig("Estimated Bandwidth", "Avg(Value)", "Time = '$now' AND Name = 'EstimatedBandwidth'", 8.0 / 1024.0) },
        };


        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private string mName;
        public string Name
        {
            get { return this.mName; }
            set
            {
                this.mName = value;
                this.OnPropertyChanged("Name");
            }
        }

        private string mExpression;
        public string Expression
        {
            get { return this.mExpression; }
            set
            {
                this.mExpression = value;
                this.OnPropertyChanged("Expression");
            }
        }

        private string mFilter;
        public string Filter
        {
            get { return this.mFilter; }
            set
            {
                this.mFilter = value;
                this.OnPropertyChanged("Filter");
            }
        }

        private double mScaleFactor;
        public double ScaleFactor
        {
            get { return this.mScaleFactor; }
            set
            {
                this.mScaleFactor = value;
                this.OnPropertyChanged("ScaleFactor");
            }
        }

        public ObservableDataSource<Point> Source { get; private set; }

        public SourceConfig()
            : this("<new source>", "", "")
        {
        }

        public SourceConfig(string name, string expression, string filter)
            : this(name, expression, filter, 1.0)
        {
        }

        public SourceConfig(string name, string expression, string filter, double scaleFactor)
        {
            this.Name = name;
            this.Expression = expression;
            this.Filter = filter;
            this.ScaleFactor = scaleFactor;
            this.MakeNewSource();
        }

        public SourceConfig(SourceConfig other)
            : this(other.Name, other.Expression, other.Filter, other.ScaleFactor)
        {
        }

        public void MakeNewSource()
        {
            this.Source = new ObservableDataSource<Point>();
            this.Source.SetXYMapping(
                delegate(Point p)
                {
                    return new Point(p.X, p.Y * this.ScaleFactor);
                });
        }

        public void Append(Dispatcher dispatcher, int time, object result)
        {
            double value = 0;

            // Unbox appropriately
            if (result is double)
            {
                value = (double)result;
            }
            else if (result is int)
            {
                value = (int)result;
            }

            this.Source.AppendAsync(dispatcher, new Point(time, value));
        }

        public void Append(Dispatcher dispatcher, int time, DataTable stats)
        {
            string filter = this.Filter.Replace("$now", time.ToString());
            object result = stats.Compute(this.Expression, filter);

            if (result != null)
            {
                this.Append(dispatcher, time, result);
            }
        }
    }
}
