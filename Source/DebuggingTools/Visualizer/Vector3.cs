﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Visualizer
{
    class Vector3 : IParser<Vector3>
    {
        public static Vector3 Parse(ref string line)
        {
            Regex regex = new Regex(@"^<(?<x>[^,]+),(?<y>[^,]+),(?<z>[^,]+)>");
            Match match = regex.Match(line);
            if (!match.Success)
            {
                return null;
            }

            line = line.Substring(match.Length);
            return new Vector3(double.Parse(match.Groups["x"].Value),
                double.Parse(match.Groups["y"].Value),
                double.Parse(match.Groups["z"].Value));
        }

        Vector3 IParser<Vector3>.Parse(ref string line)
        {
            return Vector3.Parse(ref line);
        }

        private double mX;
        public double X { get { return this.mX; } }

        private double mY;
        public double Y { get { return this.mY; } }

        private double mZ;
        public double Z { get { return this.mZ; } }


        public Vector3()
        {
        }

        public Vector3(Badumna.DataTypes.Vector3 badumnaVector)
        {
            this.mX = badumnaVector.X;
            this.mY = badumnaVector.Y;
            this.mZ = badumnaVector.Z;
        }

        public Vector3(double x, double y, double z)
        {
            this.mX = x;
            this.mY = y;
            this.mZ = z;
        }
    }
}
