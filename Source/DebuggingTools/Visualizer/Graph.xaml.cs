﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Research.DynamicDataDisplay.DataSources;
using System.ComponentModel;
using Microsoft.Research.DynamicDataDisplay;
using System.Windows.Threading;

namespace Visualizer
{
    /// <summary>
    /// Interaction logic for Graph.xaml
    /// </summary>
    public partial class Graph : UserControl
    {
        public static Graph Create(Dispatcher dispatcher, GraphConfig config)
        {
            if (!dispatcher.CheckAccess())
            {
                return (Graph)dispatcher.Invoke(new Func<Dispatcher, GraphConfig, Graph>(Graph.Create), dispatcher, config);
            }

            Graph graph = new Graph
            {
                Title = config.Title,
                XAxisLabel = config.XLabel,
                YAxisLabel = config.YLabel
            };
            graph.ApplyGraphConfig(config);
            return graph;
        }

        private static List<Color> LegendColors = new List<Color>
        {
            Colors.Red,
            Colors.Green,
            Colors.Blue,
            Colors.Orange,
            Colors.Purple,
            Colors.SkyBlue,
            Colors.PaleVioletRed
        };

        private Dictionary<string, ObservableDataSource<Point>> mData = new Dictionary<string, ObservableDataSource<Point>>();

        private TextBlock mHeaderTextBlock;
        public string Title
        {
            get { return this.mHeaderTextBlock.Text; }
            set { this.mHeaderTextBlock.Text = value; }
        }

        private VerticalAxisTitle mVerticalAxisTitle;
        public string YAxisLabel
        {
            get { return this.mVerticalAxisTitle.Text; }
            set { this.mVerticalAxisTitle.Text = value; }
        }

        private HorizontalAxisTitle mHorizontalAxisTitle;
        public string XAxisLabel
        {
            get { return this.mHorizontalAxisTitle.Text; }
            set { this.mHorizontalAxisTitle.Text = value; }
        }

        public Graph()
            : this(400)
        {
        }

        public Graph(double height)
        {
            InitializeComponent();

            this.Height = height;

            Header header = new Header();
            this.mHeaderTextBlock = new TextBlock { HorizontalAlignment = HorizontalAlignment.Center };
            header.Contents = this.mHeaderTextBlock;
            this.Plot.Children.Add(header);

            this.mVerticalAxisTitle = new VerticalAxisTitle();
            this.Plot.Children.Add(this.mVerticalAxisTitle);

            this.mHorizontalAxisTitle = new HorizontalAxisTitle();
            this.Plot.Children.Add(this.mHorizontalAxisTitle);

            this.Plot.Viewport.FitToView();
        }

        public ObservableDataSource<Point> AddSeries(string name)
        {
            if (!this.CheckAccess())
            {
                return (ObservableDataSource<Point>)this.Dispatcher.Invoke(
                    new Func<string, ObservableDataSource<Point>>(this.AddSeries), name);
            }

            ObservableDataSource<Point> source = new ObservableDataSource<Point>();
            source.SetXYMapping(p => p);
            this.AddSeries(name, source);
            return source;
        }

        public void AddSeries(string name, ObservableDataSource<Point> source)
        {
            if (!this.CheckAccess())
            {
                this.Dispatcher.Invoke(new Action<string, ObservableDataSource<Point>>(this.AddSeries), name, source);
                return;
            }

            this.Plot.AddLineGraph(source, Graph.LegendColors[this.mData.Count % Graph.LegendColors.Count], 1, name);
            this.mData[name] = source;
        }

        private void ApplyGraphConfig(GraphConfig config)
        {
            foreach (SourceConfig sourceConfig in config.Sources)
            {
                this.AddSeries(sourceConfig.Name, sourceConfig.Source);
            }
        }
    }
}
