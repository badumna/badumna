﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Shapes;
using System.Windows;
using System.Windows.Media;
using System.Windows.Controls;
using System.Net;
using System.Windows.Documents;

namespace Visualizer
{
    // Facade.mStack.mDhtFacades.mTiers[1].mRouter.mLeafSet.mLocalNodeInfo.mKey
    // Facade.mStack.mDhtFacades.mTiers[1].mRouter.mLeafSet.mSuccessors{}.mKey
    // Facade.mStack.mDhtFacades.mTiers[1].mRouter.mLeafSet.mPredecessors{}.mKey

    public class DhtInfo
    {
        public static readonly Rect CanvasDimension = new Rect(-1250.0, -1250.0, 2500.0, 2500.0);
        private const double DhtRadius = 1000.0;

        private HashKey mLocalKey;
        private List<HashKey> mSuccessors;
        private List<HashKey> mPredecessors;
        private bool mIsQuarantined = false;

        public DhtInfo(Queue<string> lines)
        {
            string line = lines.Dequeue();
            this.mLocalKey = HashKey.Parse(ref line);
            this.mSuccessors = Parser.ParseList<HashKey>(lines.Dequeue());
            this.mPredecessors = Parser.ParseList<HashKey>(lines.Dequeue());
        }

        public DhtInfo(string localKey, bool isQuarantined, IEnumerable<string> predecessors, IEnumerable<string> successors)
        {
            this.mLocalKey = HashKey.Parse(ref localKey);
            this.mPredecessors = new List<HashKey>();
            this.mSuccessors = new List<HashKey>();
            this.mIsQuarantined = isQuarantined;

            foreach (string key in predecessors)
            {
                if (!string.IsNullOrEmpty(key))
                {
                    string keyCopy = key;
                    this.mPredecessors.Add(HashKey.Parse(ref keyCopy));
                }
            }
            foreach (string key in successors)
            {
                if (!string.IsNullOrEmpty(key))
                {
                    string keyCopy = key;
                    this.mSuccessors.Add(HashKey.Parse(ref keyCopy));
                }
            }
        }

        public Path MakeLeafSetPath()
        {
            PathGeometry geometry = new PathGeometry();

            foreach (HashKey key in this.mSuccessors)
            {
                geometry.Figures.Add(DhtInfo.RenderLink(this.mLocalKey, key));
            }
            foreach (HashKey key in this.mPredecessors)
            {
                geometry.Figures.Add(DhtInfo.RenderLink(this.mLocalKey, key));
            }

            return new Path { Data = geometry };
        }

        private static void RenderDhtCircle(Canvas canvas)
        {
            Ellipse dot = new Ellipse();
            dot.Width = dot.Height = DhtInfo.DhtRadius * 2;

            Canvas.SetLeft(dot, -DhtInfo.DhtRadius);
            Canvas.SetTop(dot, -DhtInfo.DhtRadius);

            dot.Stroke = Brushes.Gray;

            canvas.Children.Add(dot);
        }

        private static PathFigure RenderLink(HashKey start, HashKey end)
        {
            Point startPoint = new Point(DhtInfo.DhtRadius * Math.Cos(start.Angle), DhtInfo.DhtRadius * Math.Sin(start.Angle));
            Point endPoint = new Point(DhtInfo.DhtRadius * Math.Cos(end.Angle), DhtInfo.DhtRadius * Math.Sin(end.Angle));

            double midAngle;
            double sweep = Math.Abs(end.Angle - start.Angle);

            if (sweep < Math.PI)
            {
                if (start.Angle < end.Angle)
                {
                    midAngle = start.Angle + sweep / 2.0;
                }
                else
                {
                    midAngle = end.Angle + sweep / 2.0;
                }
            }
            else
            {
                sweep = 2 * Math.PI - sweep;
                if (start.Angle < end.Angle)
                {
                    midAngle = start.Angle - sweep / 2.0;
                }
                else
                {
                    midAngle = end.Angle - sweep / 2.0;
                }
            }

            double midPointRadius = DhtInfo.DhtRadius * (1.0 - sweep / Math.PI);
            Point midPoint = new Point(midPointRadius * Math.Cos(midAngle), midPointRadius * Math.Sin(midAngle));

            BezierSegment segment = new BezierSegment(midPoint, midPoint, endPoint, true);
            return new PathFigure(startPoint, new PathSegment[] { segment }, false);
        }

        public static void Render(List<DhtInfo> dhtInfos, Canvas canvas)
        {
            canvas.Children.Clear();

            Brush backgroundBrush = Brushes.White; //new SolidColorBrush(Color.FromRgb(0x33, 0xA0, 0x6A));
            Brush linkBrush = Brushes.DarkKhaki; // new SolidColorBrush(Color.FromRgb(0xFD, 0xDB, 0x22));
            Brush quarantinedNodeBrush = new SolidColorBrush(Colors.Gray);

            Rectangle background = new Rectangle()
            {
                Width = DhtInfo.CanvasDimension.Width,
                Height = DhtInfo.CanvasDimension.Height,
                Fill = backgroundBrush,
                Stroke = backgroundBrush
            };
            Canvas.SetLeft(background, DhtInfo.CanvasDimension.X);
            Canvas.SetTop(background, DhtInfo.CanvasDimension.Y);
            canvas.Children.Add(background);

            DhtInfo.RenderDhtCircle(canvas);

            foreach (DhtInfo dhtInfo in dhtInfos)
            {
                Path leafSet = dhtInfo.MakeLeafSetPath();
                leafSet.Stroke = linkBrush;
                leafSet.Fill = Brushes.Transparent;
                leafSet.StrokeThickness = 5;
                canvas.Children.Add(leafSet);
            }

            foreach (DhtInfo dhtInfo in dhtInfos)
            {
                if (dhtInfo.mIsQuarantined)
                {
                    canvas.Children.Add(DhtInfo.DrawDot(dhtInfo.mLocalKey, quarantinedNodeBrush, 50));
                }
                else
                {
                    canvas.Children.Add(DhtInfo.DrawDot(dhtInfo.mLocalKey, linkBrush, 50));
                    canvas.Children.Add(DhtInfo.DrawText(dhtInfo.mLocalKey.HashString.Remove(3) + "...", dhtInfo.mLocalKey));
                }
            }
        }

        public static void RenderPath(IEnumerable<string> pathNodes, Canvas canvas)
        {
            Brush linkBrush = new SolidColorBrush(Colors.Brown);
            List<HashKey> keys = new List<HashKey>();

            foreach (string node in pathNodes)
            {
                string copy = node;
                keys.Add(HashKey.Parse(ref copy));
            }

            if (keys.Count < 2)
            {
                return;
            }

            for (int i = 0; i < keys.Count - 1; i++)
            {
                if (keys[i] != keys[i + 1])
                {
                    PathGeometry geometry = new PathGeometry(new PathFigure[] { DhtInfo.RenderLink(keys[i], keys[i + 1]) });
                    Path path = new Path { Data = geometry };

                    path.Stroke = linkBrush;
                    path.Fill = Brushes.Transparent;
                    path.StrokeThickness = 2;

                    canvas.Children.Add(path);
                }

                canvas.Children.Add(DhtInfo.DrawDot(keys[i], linkBrush, 20));
            }

            canvas.Children.Add(DhtInfo.DrawDot(keys[0], Brushes.HotPink, 20));
            canvas.Children.Add(DhtInfo.DrawDot(keys[keys.Count - 1], Brushes.Blue, 20));
        }

        private static Ellipse DrawDot(HashKey hashKey, Brush brush, int radius)
        {
            double dotX = DhtInfo.DhtRadius * Math.Cos(hashKey.Angle);
            double dotY = DhtInfo.DhtRadius * Math.Sin(hashKey.Angle);

            Ellipse dot = new Ellipse();
            dot.Width = dot.Height = radius;
            Canvas.SetLeft(dot, dotX - radius / 2);
            Canvas.SetTop(dot, dotY - radius / 2);

            dot.Stroke = brush;
            dot.Fill = brush;

            return dot;
        }

        private static TextBlock DrawText(string text, HashKey hashKey)
        {
            double dotX = DhtInfo.DhtRadius * Math.Cos(hashKey.Angle);
            double dotY = DhtInfo.DhtRadius * Math.Sin(hashKey.Angle);

            TextBlock peerId = new TextBlock(new Run(text));
            peerId.FontSize = 40;
            Canvas.SetLeft(peerId, dotX * 1.1 - 25);
            Canvas.SetTop(peerId, dotY * 1.1 - 25);

            return peerId;
        }

        public static void RenderReplicas(Canvas canvas, string originalHashKey, string hostKey, IEnumerable<string> replicasPeers)
        {
            HashKey hashKey = new HashKey(originalHashKey);
            canvas.Children.Add(DhtInfo.DrawRectangle(hashKey, Brushes.Blue, 50));
            DhtInfo.DrawText(originalHashKey.Remove(3) + "...", hashKey);

            double dotX = DhtInfo.DhtRadius * Math.Cos(hashKey.Angle) * 0.9;
            double dotY = DhtInfo.DhtRadius * Math.Sin(hashKey.Angle) * 0.9;

            HashKey hostHashKey = new HashKey(hostKey);

            double endX = DhtInfo.DhtRadius * Math.Cos(hostHashKey.Angle);
            double endY = DhtInfo.DhtRadius * Math.Sin(hostHashKey.Angle);

            Line line = new Line() { X1 = dotX, Y1 = dotY, X2 = endX, Y2 = endY };
            line.Stroke = Brushes.Blue;
            line.StrokeThickness = 1;

            canvas.Children.Add(line);

            foreach (string hashKeyString in replicasPeers)
            {
                HashKey replicaHostHashKey = new HashKey(hashKeyString);
                canvas.Children.Add(DhtInfo.DrawRectangle(replicaHostHashKey, Brushes.Blue, 20));
            }
        }

        private static Rectangle DrawRectangle(HashKey hashKey, Brush brush, int radius)
        {
            double dotX = DhtInfo.DhtRadius * Math.Cos(hashKey.Angle) * 0.9;
            double dotY = DhtInfo.DhtRadius * Math.Sin(hashKey.Angle) * 0.9;

            Rectangle rectangle = new Rectangle();

            rectangle.Width = rectangle.Height = radius;

            Canvas.SetLeft(rectangle, dotX - radius / 2);
            Canvas.SetTop(rectangle, dotY - radius / 2);

            rectangle.Stroke = brush;
            rectangle.Fill = brush;

            return rectangle;
        }
    }
}
