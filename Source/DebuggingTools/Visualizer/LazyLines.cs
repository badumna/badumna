﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Visualizer
{
    class LazyLines : IDisposable
    {
        private StreamReader mReader;
        private string mPeekedLine;


        public LazyLines(string filename)
        {
            this.mReader = new StreamReader(new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.ReadWrite));
        }

        public string Peek()
        {
            if (this.mPeekedLine == null)
            {
                this.mPeekedLine = this.mReader.ReadLine();
            }

            return this.mPeekedLine;
        }

        public string Get()
        {
            if (this.mPeekedLine != null)
            {
                this.mPeekedLine = null;
                return this.mPeekedLine;
            }

            return this.mReader.ReadLine();
        }

        public void Dispose()
        {
            if (this.mReader != null)
            {
                this.mReader.Close();
                this.mReader = null;
            }
        }
    }
}
