﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Net;

namespace Visualizer
{
    class BadumnaId : IParser<BadumnaId>
    {
        public static BadumnaId Parse(ref string line)
        {
            string originalLine = line;

            PeerAddress peerAddress = PeerAddress.Parse(ref line);
            if (peerAddress == null)
            {
                return null;
            }

            Regex regex = new Regex(@"^-(?<id>\d+)");
            Match match = regex.Match(line);

            if (!match.Success)
            {
                line = originalLine;
                return null;
            }

            int id = int.Parse(match.Groups["id"].Value);

            line = line.Substring(match.Length);
            return new BadumnaId(peerAddress, id);
        }

        BadumnaId IParser<BadumnaId>.Parse(ref string line)
        {
            return BadumnaId.Parse(ref line);
        }

        private PeerAddress mPeerAddress;
        private int mLocalId;


        public BadumnaId()
        {
        }

        public BadumnaId(PeerAddress peerAddress, int localId)
        {
            this.mPeerAddress = peerAddress;
            this.mLocalId = localId;
        }

        public override string ToString()
        {
            return this.mPeerAddress.ToString() + "-" + this.mLocalId.ToString();
        }
    }
}
