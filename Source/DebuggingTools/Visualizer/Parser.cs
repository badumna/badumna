﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Visualizer
{
    interface IParser<T>
    {
        T Parse(ref string line);
    }

    class Parser
    {
        public static List<T> ParseList<T>(string line) where T : IParser<T>, new()
        {
            T t = new T();
            
            List<T> result = new List<T>();

            while (line.Length > 0)
            {
                T nextT = t.Parse(ref line);
                if (nextT == null)
                {
                    return null;
                }
                result.Add(nextT);

                if (line.Length > 0)
                {
                    if (line[0] != ',')
                    {
                        return null;
                    }

                    line = line.Substring(1);
                }
            }

            return result;
        }

        public static List<float> ParseListFloat(string line)
        {
            List<float> result = new List<float>();

            string[] parts = line.Split(',');

            foreach (string part in parts)
            {
                float nextFloat;
                if (!float.TryParse(line, out nextFloat))
                {
                    return null;
                }
                result.Add(nextFloat);
            }

            return result;
        }
    }
}
