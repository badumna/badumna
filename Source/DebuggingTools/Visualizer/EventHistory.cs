﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.Windows.Threading;

namespace Visualizer
{
    public class EventHistory
    {
        public class Event
        {
            public double TimeMs { get; private set; }
            public string Name { get; private set; }

            public Event(double timeMs, string name)
            {
                this.TimeMs = timeMs;
                this.Name = name;
            }
        }

        public class Node
        {
            public string Name { get; private set; }
            public ObservableCollection<Event> Events { get; private set; }

            public Node(string name)
            {
                this.Name = name;
                this.Events = new ObservableCollection<Event>();
            }
        }


        public ReadOnlyObservableCollection<Node> Nodes { get { return this.mReadOnlyNodes; } }

        private ObservableCollection<Node> mNodes;
        private ReadOnlyObservableCollection<Node> mReadOnlyNodes;
        private Dictionary<string, Node> mKeyedNodes;

        public EventHistory()
        {
            this.mNodes = new ObservableCollection<Node>();
            this.mReadOnlyNodes = new ReadOnlyObservableCollection<Node>(this.mNodes);
            this.mKeyedNodes = new Dictionary<string, Node>();
        }


        public void AddEvent(Dispatcher dispatcher, string peerAddress, double timeMs, string label)
        {
            if (!dispatcher.CheckAccess())
            {
                dispatcher.BeginInvoke(new Action<Dispatcher, string, double, string>(this.AddEvent),
                    DispatcherPriority.Background,
                    dispatcher, peerAddress, timeMs, label);
                return;
            }

            Node node;
            if (!this.mKeyedNodes.TryGetValue(peerAddress, out node))
            {
                node = new Node(peerAddress);
                this.mKeyedNodes[peerAddress] = node;
                this.mNodes.Add(node);
            }

            node.Events.Add(new Event(timeMs, label));
        }
    }
}
