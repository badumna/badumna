﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Text.RegularExpressions;

namespace Visualizer
{
    class PeerAddress : IParser<PeerAddress>, IEquatable<PeerAddress>
    {
        public static PeerAddress Parse(ref string line)
        {
            Regex regex = new Regex(@"^((?<nat>[^|]+)\|)?(?<ip>\d+\.\d+\.\d+\.\d+):(?<port>\d+)");
            Match match = regex.Match(line);

            if (!match.Success)
            {
                return null;
            }

            string natType = "Unknown";
            if (match.Groups["nat"].Success)
            {
                natType = match.Groups["nat"].Value;
            }
            
            IPAddress address = IPAddress.Parse(match.Groups["ip"].Value);
            int port = int.Parse(match.Groups["port"].Value);

            line = line.Substring(match.Length);
            return new PeerAddress(natType, new IPEndPoint(address, port));
        }
        
        PeerAddress IParser<PeerAddress>.Parse(ref string line)
        {
            return PeerAddress.Parse(ref line);
        }


        private string mNatType;
        private IPEndPoint mEndPoint;

        public PeerAddress(string natType, IPEndPoint endPoint)
        {
            this.mNatType = natType;
            this.mEndPoint = endPoint;
        }

        public bool Equals(PeerAddress other)
        {
            return other != null && this.mNatType == other.mNatType && this.mEndPoint.Equals(other.mEndPoint);
        }

        public override bool Equals(object obj)
        {
            return this.Equals(obj as PeerAddress);
        }

        public override int GetHashCode()
        {
            return this.mNatType.GetHashCode() ^ this.mEndPoint.GetHashCode();
        }

        public override string ToString()
        {
            return this.mNatType + "|" + this.mEndPoint.ToString();
        }
    }
}
