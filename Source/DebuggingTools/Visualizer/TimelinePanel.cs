﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows;

namespace Visualizer
{
    public class TimelinePanel : Panel
    {
        public static readonly DependencyProperty StartProperty = DependencyProperty.RegisterAttached("Start", typeof(double), typeof(TimelinePanel),
            new FrameworkPropertyMetadata(0.0, FrameworkPropertyMetadataOptions.AffectsParentMeasure));

        public static readonly DependencyProperty UnitsPerTickProperty = DependencyProperty.Register("UnitsPerTick", typeof(double), typeof(TimelinePanel),
            new FrameworkPropertyMetadata(1.0, FrameworkPropertyMetadataOptions.AffectsMeasure));

        public static double GetStart(UIElement element)
        {
            return (double)element.GetValue(StartProperty);
        }

        public static void SetStart(UIElement element, double value)
        {
            element.SetValue(StartProperty, value);
        }


        public double UnitsPerTick
        {
            get { return (double)this.GetValue(UnitsPerTickProperty); }
            set { this.SetValue(UnitsPerTickProperty, value); }
        }

        public bool AllowOverlap { get; set; }
        public Orientation Orientation { get; set; }

        private SortedList<double, List<UIElement>> mSortedChildren = new SortedList<double, List<UIElement>>();
        private double maxRowSize;


        public TimelinePanel()
        {
            this.Orientation = System.Windows.Controls.Orientation.Horizontal;
        }

        private void Layout(Action<UIElement, Point, Point> layoutAction)
        {
            // Measure is guaranteed to have been called at least once before arrange, and
            // adding / removing a child from a Panel must surely invalidate measure, so
            // mSortedChildren should always be valid in this method.

            // This records the next useable position in each row (column for vertical layout).
            // Rows are added as necessary.
            List<double> nextAvailablePosition = new List<double> { 0 };

            for (int childListIndex = 0; childListIndex < this.mSortedChildren.Count; childListIndex++)
            {
                double startTime = this.mSortedChildren.Keys[childListIndex];
                double startPosition = startTime * this.UnitsPerTick;

                List<UIElement> elements = this.mSortedChildren.Values[childListIndex];
                foreach (UIElement element in elements)
                {
                    int nextRowIndex = 0;

                    if (!this.AllowOverlap)
                    {
                        nextRowIndex = -1;

                        for (int i = 0; i < nextAvailablePosition.Count; i++)
                        {
                            if (nextAvailablePosition[i] <= startPosition)
                            {
                                nextRowIndex = i;
                                break;
                            }
                        }

                        if (nextRowIndex < 0)
                        {
                            nextRowIndex = nextAvailablePosition.Count;
                            nextAvailablePosition.Add(0);
                        }
                    }

                    Point start;
                    if (this.Orientation == System.Windows.Controls.Orientation.Horizontal)
                    {
                        start = new Point(startPosition, nextRowIndex * this.maxRowSize);
                    }
                    else
                    {
                        start = new Point(nextRowIndex * this.maxRowSize, startPosition);
                    }

                    Point end = start + (Vector)element.DesiredSize;
                    nextAvailablePosition[nextRowIndex] = this.Orientation == System.Windows.Controls.Orientation.Horizontal ? end.X : end.Y;
                    layoutAction(element, start, end);
                }
            }
        }

        protected override Size MeasureOverride(Size availableSize)
        {
            this.mSortedChildren.Clear();
            this.maxRowSize = 0;

            foreach (UIElement element in this.Children)
            {
                element.Measure(availableSize);
                if (this.Orientation == System.Windows.Controls.Orientation.Horizontal)
                {
                    this.maxRowSize = Math.Max(this.maxRowSize, element.DesiredSize.Height);
                }
                else
                {
                    this.maxRowSize = Math.Max(this.maxRowSize, element.DesiredSize.Width);
                }

                double startTime = TimelinePanel.GetStart(element);
                List<UIElement> elements;
                if (!this.mSortedChildren.TryGetValue(startTime, out elements))
                {
                    elements = new List<UIElement>();
                    this.mSortedChildren[startTime] = elements;
                }
                elements.Add(element);
            }

            Size size = new Size();
            this.Layout(
                delegate(UIElement element, Point start, Point end)
                {
                    size.Width = Math.Max(size.Width, end.X);
                    size.Height = Math.Max(size.Height, end.Y);
                });

            return size;
        }

        protected override Size ArrangeOverride(Size finalSize)
        {
            this.Layout(
                delegate(UIElement element, Point start, Point end)
                {
                    element.Arrange(new Rect(start, end));
                });

            return finalSize;
        }
    }
}
