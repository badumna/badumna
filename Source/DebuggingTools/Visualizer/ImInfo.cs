﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;

namespace Visualizer
{
    class ImInfo
    {
        public ImInfo(Queue<string> lines)
        {
            List<BadumnaId> interested = Parser.ParseList<BadumnaId>(lines.Dequeue());

            List<BadumnaId> eventRegions = Parser.ParseList<BadumnaId>(lines.Dequeue());
            List<float> eventRegionRadii = Parser.ParseListFloat(lines.Dequeue());
            List<Vector3> eventRegionCentroids = Parser.ParseList<Vector3>(lines.Dequeue());

            List<BadumnaId> interestRegions = Parser.ParseList<BadumnaId>(lines.Dequeue());
            List<float> interestRegionRadii = Parser.ParseListFloat(lines.Dequeue());
            List<Vector3> interestRegionCentroids = Parser.ParseList<Vector3>(lines.Dequeue());
        }

        public static void Render(Dictionary<int, ImInfo> imInfos, Canvas canvas)
        {
        }
    }
}
