﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using System.IO;

namespace Visualizer
{
    class VideoUtil
    {
        public static void SaveToPng(string filename, ZoomCanvas canvas, int width, int height)
        {
            //Rect originalViewport = canvas.Viewport;
            //canvas.Viewport = canvas.DefaultViewport;

            Size size = new Size(width, height);
            canvas.Measure(size);
            canvas.Arrange(new Rect(size));
            canvas.UpdateLayout();

            RenderTargetBitmap renderBitmap = new RenderTargetBitmap(width, height, 96.0, 96.0, PixelFormats.Pbgra32);
            renderBitmap.Render(canvas);

            using (FileStream outStream = new FileStream(filename, FileMode.Create))
            {
                PngBitmapEncoder encoder = new PngBitmapEncoder();
                encoder.Frames.Add(BitmapFrame.Create(renderBitmap));
                encoder.Save(outStream);
            }

            //canvas.Viewport = originalViewport;
        }
    }
}
