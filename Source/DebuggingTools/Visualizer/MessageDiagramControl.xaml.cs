﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using System.ComponentModel;
using System.Linq;
using System.Windows.Input;

namespace Visualizer
{
    public class HopsToPointsConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            List<MessageDiagram.MessageHop> hops = (List<MessageDiagram.MessageHop>)values[0];
            PointCollection points = new PointCollection();  // TODO: It'd be nicer if we could do this on demand rather than converting the entire collection each time

            double nodeWidth = (double)values[1] + 16;  // add margin

            double timeScale = (double)values[2];

            foreach (MessageDiagram.MessageHop hop in hops)
            {
                points.Add(new Point((hop.NodeIndex + 0.5) * nodeWidth + 8, hop.TimeMs * timeScale));
            }

            return points;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }

    public class IndexToColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            int index = (int)value;
            int maxIndex = 20;

            index = (index * 7) % maxIndex;

            return FromHSL(360.0 * index / maxIndex, 1.0, 0.6);
        }

        public static Color FromHSL(double hue, double saturation, double lightness)
        {
            hue = (hue % 360.0) / 360.0;
            saturation = Math.Max(0.0, Math.Min(1.0, saturation));
            lightness = Math.Max(0.0, Math.Min(1.0, lightness));

            double q;
            if (lightness < 0.5)
            {
                q = lightness * (1 + saturation);
            }
            else
            {
                q = lightness * (1 - saturation) + saturation;
            }

            double p = 2 * lightness - saturation;

            double[] t = new double[3];

            for (int i = 0; i < t.Length; i++)
            {
                t[i] = hue + (1.0 - i) / 3.0;
                if (t[i] > 1.0)
                {
                    t[i] -= 1.0;
                }
                else if (t[i] < 0.0)
                {
                    t[i] += 1.0;
                }
            }

            double[] c = new double[3];
            for (int i = 0; i < c.Length; i++)
            {
                if (t[i] < 1.0 / 6.0)
                {
                    c[i] = p + (q - p) * 6.0 * t[i];
                }
                else if (t[i] < 0.5)
                {
                    c[i] = q;
                }
                else if (t[i] < 2.0 / 3.0)
                {
                    c[i] = p + (q - p) * 6.0 * (2.0 / 3.0 - t[i]);
                }
                else
                {
                    c[i] = p;
                }
            }

            return Color.FromScRgb(1.0f, (float)c[0], (float)c[1], (float)c[2]);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }


    public partial class MessageDiagramControl : UserControl, INotifyPropertyChanged
    {
        public MessageDiagram MessageDiagram
        {
            get { return this.mMessageDiagram; }
            set
            {
                this.mMessageDiagram = value;
                this.OnPropertyChanged("MessageDiagram");
            }
        }

        public double NodeWidth
        {
            get { return this.nodeWidth; }
            set
            {
                this.nodeWidth = value;
                this.OnPropertyChanged("NodeWidth");
            }
        }

        public double TimeScale
        {
            get { return this.timeScale; }
            set
            {
                this.timeScale = value;
                this.OnPropertyChanged("TimeScale");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;


        private MessageDiagram mMessageDiagram;
        private double nodeWidth;
        private double timeScale;


        public MessageDiagramControl()
        {
            InitializeComponent();
            this.DataContext = this;
            this.NodeWidth = 150;
            this.TimeScale = 1;

            // Forward the mousewheel events from the description listbox's implict scrollviewer to the top level scrollviewer.
            this.DescriptionTimeline.PreviewMouseWheel += delegate(object sender, MouseWheelEventArgs e)
            {
                e.Handled = true;
                var routed = new MouseWheelEventArgs(e.MouseDevice, e.Timestamp, e.Delta);
                routed.RoutedEvent = UIElement.MouseWheelEvent;
                this.DescriptionTimeline.RaiseEvent(routed);
            };

            /*
            // Test data
            this.MessageDiagram = new MessageDiagram();
            var nodes = (from x in Enumerable.Range(0, 5) select this.MessageDiagram.AddNode(20, string.Format("Node {0}", x))).ToArray();
            this.MessageDiagram.AddMessage("Test message", 40, nodes[0], 0, 1500);
            //this.MessageDiagram.AddMessageHop(0, 100, nodes[1], false);
            this.MessageDiagram.AddMessageHop(0, 110, nodes[1], true);

            this.MessageDiagram.AddMessage("Test message2", 41, nodes[1], 1, 1500);
            //this.MessageDiagram.AddMessageHop(1, 120, nodes[2], false);
            //this.MessageDiagram.AddMessageHop(1, 140, nodes[3], false);
            this.MessageDiagram.AddMessageHop(1, 150, nodes[4], true);
            */
        }

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
