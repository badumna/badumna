﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Collections.ObjectModel;

namespace Visualizer
{
    public class GraphConfig : INotifyPropertyChanged
    {
        public static Dictionary<string, GraphConfig> DefaultGraphs;

        static GraphConfig()
        {
            GraphConfig.DefaultGraphs = new Dictionary<string, GraphConfig>();

            GraphConfig bandwidthGraph = new GraphConfig { Title = "Bandwidth", YLabel = "kb/s", IsEnabled = false };
            bandwidthGraph.Sources.Add(SourceConfig.DefaultSources["AvgInbound"]);
            bandwidthGraph.Sources.Add(SourceConfig.DefaultSources["MaxInbound"]);
            bandwidthGraph.Sources.Add(SourceConfig.DefaultSources["AvgOutbound"]);
            bandwidthGraph.Sources.Add(SourceConfig.DefaultSources["MaxOutbound"]);
            GraphConfig.DefaultGraphs["Bandwidth"] = bandwidthGraph;

            GraphConfig entityGraph = new GraphConfig { Title = "Entity Status", YLabel = "Entity count", IsEnabled = false };
            entityGraph.Sources.Add(SourceConfig.DefaultSources["TotalMissing"]);
            entityGraph.Sources.Add(SourceConfig.DefaultSources["TotalHallucinated"]);
            GraphConfig.DefaultGraphs["EntityStatus"] = entityGraph;
        }


        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private bool mIsEnabled = true;
        public bool IsEnabled
        {
            get { return this.mIsEnabled; }
            set
            {
                this.mIsEnabled = value;
                this.OnPropertyChanged("IsEnabled");
            }
        }

        private string mTitle;
        public string Title
        {
            get { return this.mTitle; }
            set
            {
                this.mTitle = value;
                this.OnPropertyChanged("Title");
            }
        }

        private string mXLabel;
        public string XLabel
        {
            get { return this.mXLabel; }
            set
            {
                this.mXLabel = value;
                this.OnPropertyChanged("XLabel");
            }
        }

        private string mYLabel;
        public string YLabel
        {
            get { return this.mYLabel; }
            set
            {
                this.mYLabel = value;
                this.OnPropertyChanged("YLabel");
            }
        }

        public ObservableCollection<SourceConfig> Sources { get; private set; }

        public GraphConfig()
            : this("<new graph>", "Time", "")
        {
        }

        public GraphConfig(string title, string xLabel, string yLabel)
        {
            this.Sources = new ObservableCollection<SourceConfig>();

            this.Title = title;
            this.XLabel = xLabel;
            this.YLabel = yLabel;
        }
    }
}
