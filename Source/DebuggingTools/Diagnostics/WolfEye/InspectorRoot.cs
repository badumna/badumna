﻿using System;
using System.Collections.Generic;
using System.Text;
using Badumna;
using Badumna.Core;

namespace Diagnostics.WolfEye
{
    class InspectorRoot
    {
        public NetworkFacade Facade;
        public NetworkEventQueue Queue;
    }
}
