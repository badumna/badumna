﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.Collections;
using System.Text.RegularExpressions;
using System.Diagnostics;

#if DEBUG
using NUnit.Framework;
#endif

using Badumna.Core;
using Badumna.Utilities;
using Badumna;


namespace Diagnostics.WolfEye
{
    enum PathElementType : byte
    {
        None,
        Field,
        Array,
        Indexer,
        Object
    }

    abstract class PathElement : IParseable
    {
        private static Dictionary<Type, PathElementType> PathTypeToEnum = new Dictionary<Type,PathElementType>
        {
            { typeof(FieldPath), PathElementType.Field },
            { typeof(ArrayPath), PathElementType.Array },
            { typeof(IndexerPath), PathElementType.Indexer },
            { typeof(ObjectPath), PathElementType.Object }
        };

        public static PathElementType ToPathElementType(Type type)
        {
            PathElementType enumType;
            PathElement.PathTypeToEnum.TryGetValue(type, out enumType);
            return enumType;
        }

        public static PathElement MakePathElement(PathElementType type)
        {
            switch (type)
            {
                case PathElementType.Field:
                    return new FieldPath();

                case PathElementType.Array:
                    return new ArrayPath();

                case PathElementType.Indexer:
                    return new IndexerPath();

                case PathElementType.Object:
                    return new ObjectPath();

                default:
                    return null;
            }
        }

        public static bool TryParse(Type type, string data, out object result)
        {
            MethodInfo parseMethod =
                type.GetMethod("TryParse", BindingFlags.Static | BindingFlags.Public, null,
                new Type[] { typeof(string), type.MakeByRefType() },
                null);

            result = null;

            if (parseMethod != null)
            {
                object[] parameters = new object[] { data, null };
                object success = parseMethod.Invoke(null, parameters);
                if (success is bool)
                {
                    result = parameters[1];
                    return (bool)success;
                }
            }
            
            return false;
        }

        /// <summary>
        /// Follows the path.  Paths can potentially refer to multiple items (e.g. wildcard IndexerPath), so
        /// this returns a list of results.
        /// </summary>
        abstract public List<object> Follow(object fromHere);

        abstract public void FromMessage(MessageBuffer message);
        abstract public void ToMessage(MessageBuffer message, Type parameterType);
    }

    class FieldPath : PathElement, IEquatable<FieldPath>
    {
        private string mTypeName;
        private string mFieldName;


        public FieldPath()
            : this("")
        {
        }

        /// <summary>
        /// Specifies a path to the first field with this name in the type's hierarchy.
        /// </summary>
        /// <param name="fieldName"></param>
        public FieldPath(string fieldName)
            : this("", fieldName)
        {
        }

        public FieldPath(string typeName, string fieldName)
        {
            this.mTypeName = typeName;
            this.mFieldName = fieldName;
        }

        public static FieldPath TryParse(ref string path)
        {
            Regex identifierRegex = new Regex(@"^[\p{Lu}\p{Ll}\p{Lt}\p{Lm}\p{Lo}\p{Nl}\p{Mn}\p{Mc}\p{Nd}\p{Pc}\p{Cf}]+");
            Match match = identifierRegex.Match(path);
            if (!match.Success)
            {
                return null;
            }

            path = path.Substring(match.Length);
            return new FieldPath(match.Value);
        }

        public override List<object> Follow(object fromHere)
        {
            if (fromHere == null)
            {
                return null;
            }

            BindingFlags searchFlags = BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.DeclaredOnly;

            if (this.mTypeName.Length == 0)
            {
                for (Type type = fromHere.GetType(); type != null; type = type.BaseType)
                {
                    FieldInfo fieldInfo = type.GetField(this.mFieldName, searchFlags);
                    if (fieldInfo != null)
                    {
                        return new List<object> { fieldInfo.GetValue(fromHere) };
                    }

                    PropertyInfo propertyInfo = type.GetProperty(this.mFieldName, searchFlags);
                    if (propertyInfo != null)
                    {
                        return new List<object> { propertyInfo.GetValue(fromHere, null) };
                    }
                }
            }
            else
            {
                Type type = Type.GetType(this.mTypeName);
                if (type != null)
                {
                    FieldInfo fieldInfo = type.GetField(this.mFieldName, searchFlags);
                    if (fieldInfo != null)
                    {
                        return new List<object> { fieldInfo.GetValue(fromHere) };
                    }

                    PropertyInfo propertyInfo = type.GetProperty(this.mFieldName, searchFlags);
                    if (propertyInfo != null)
                    {
                        return new List<object> { propertyInfo.GetValue(fromHere, null) };
                    }
                }
            }

            return null;
        }

        public bool Equals(FieldPath other)
        {
            return other != null &&
                   other.mTypeName == this.mTypeName &&
                   other.mFieldName == this.mFieldName;
        }
        
        public override bool Equals(object obj)
        {
            return this.Equals(obj as FieldPath);
        }

        public override int GetHashCode()
        {
            return this.mFieldName.GetHashCode();
        }

        public override string ToString()
        {
            return this.mFieldName;
        }

        public override void FromMessage(MessageBuffer message)
        {
            this.mTypeName = message.ReadString();
            this.mFieldName = message.ReadString();
        }

        public override void ToMessage(MessageBuffer message, Type parameterType)
        {
            message.Write(this.mTypeName);
            message.Write(this.mFieldName);
        }
    }

    class ArrayPath : PathElement, IEquatable<ArrayPath>
    {
        private int mIndex;


        public ArrayPath()
            : this(0)
        {
        }

        public ArrayPath(int index)
        {
            this.mIndex = index;
        }

        public static ArrayPath TryParse(ref string path)
        {
            Regex arrayRegex = new Regex(@"^\[(?<index>(\d)+)\]");
            Match match = arrayRegex.Match(path);
            if (!match.Success)
            {
                return null;
            }

            int index;
            if (!int.TryParse(match.Groups["index"].Value, out index))
            {
                return null;
            }

            path = path.Substring(match.Length);
            return new ArrayPath(index);
        }

        public override List<object> Follow(object fromHere)
        {
            if (fromHere is IList)
            {
                return new List<object> { ((IList)fromHere)[this.mIndex] };
            }
            else if (fromHere is Array)
            {
                return new List<object> { ((Array)fromHere).GetValue(this.mIndex) };
            }
            else
            {
                int i = 0;
                foreach (object elem in (IEnumerable)fromHere)
                {
                    if (i == this.mIndex)
                    {
                        return new List<object> { elem };
                    }

                    i++;
                }

                return new List<object>();
            }
        }

        public bool Equals(ArrayPath other)
        {
            return other != null && other.mIndex == this.mIndex;
        }

        public override bool Equals(object obj)
        {
            return this.Equals(obj as ArrayPath);
        }

        public override int GetHashCode()
        {
            return this.mIndex.GetHashCode();
        }

        public override string ToString()
        {
            return "[" + this.mIndex.ToString() + "]";
        }

        public override void FromMessage(MessageBuffer message)
        {
            this.mIndex = message.ReadInt();
        }

        public override void ToMessage(MessageBuffer message, Type parameterType)
        {
            message.Write(this.mIndex);
        }
    }

    class IndexerPath : PathElement, IEquatable<IndexerPath>
    {
        private object mIndex;
        private bool mTriedParsing;

        /// <summary>
        /// [DO NOT USE] Only for IParseable.FromMessage
        /// </summary>
        public IndexerPath()
            : this(null)
        {
        }

        /// <summary>
        /// Creates a path that looks up an indexed element.  If 'index' is null
        /// this path represents all elements in the indexed collection.
        /// </summary>
        /// <param name="index"></param>
        public IndexerPath(object index)
        {
            this.mIndex = index;
        }

        public static IndexerPath TryParse(ref string path)
        {
            Regex arrayRegex = new Regex(@"^\{(?<index>[^}]*)\}");
            Match match = arrayRegex.Match(path);
            if (!match.Success)
            {
                return null;
            }

            path = path.Substring(match.Length);
            if (match.Groups["index"].Value.Length > 0)
            {
                return new IndexerPath(match.Groups["index"].Value);
            }
            else
            {
                return new IndexerPath(null);  // "indexedField{}" represents all items
            }
        }

        public override List<object> Follow(object fromHere)
        {
            // TODO: Support classes which have overloaded indexers

            if (this.mIndex != null)
            {
                PropertyInfo propertyInfo = fromHere.GetType().GetProperty("Item");

                ParameterInfo[] paramInfo = propertyInfo.GetIndexParameters();

                if (paramInfo.Length != 1)  // Only support single argument indexers at the moment
                {
                    return new List<object>();
                }

                if (!this.mTriedParsing && this.mIndex is string &&
                    !paramInfo[0].ParameterType.IsAssignableFrom(this.mIndex.GetType()))
                {
                    this.mTriedParsing = true;
                    object parsed;
                    if (PathElement.TryParse(paramInfo[0].ParameterType, (string)this.mIndex, out parsed))
                    {
                        this.mIndex = parsed;
                    }
                }

                return new List<object> { propertyInfo.GetValue(fromHere, new object[] { this.mIndex }) };
            }
            else
            {
                IEnumerable enumerable = fromHere as IEnumerable;
                if (enumerable == null)
                {
                    return null;
                }

                List<object> result = new List<object>();
                foreach (object element in enumerable)
                {
                    result.Add(element);
                }
                return result;
            }
        }

        public bool Equals(IndexerPath other)
        {
            return other != null && other.mIndex.Equals(this.mIndex);
        }

        public override bool Equals(object obj)
        {
            return this.Equals(obj as IndexerPath);
        }

        public override int GetHashCode()
        {
            return this.mIndex.GetHashCode();
        }

        public override string ToString()
        {
            return "{" + (this.mIndex == null ? "" : this.mIndex.ToString()) + "}";
        }

        public override void FromMessage(MessageBuffer message)
        {
            PackagedObject package = new PackagedObject(null);
            package.FromMessage(message);
            this.mIndex = package.Object;
        }

        public override void ToMessage(MessageBuffer message, Type parameterType)
        {
            new PackagedObject(this.mIndex).ToMessage(message, parameterType);
        }
    }

    class ObjectPath : PathElement, IList<PathElement>, IEquatable<ObjectPath>
    {
        private List<PathElement> mPath;


        public ObjectPath()
        {
            this.mPath = new List<PathElement>();
        }

        public ObjectPath(ObjectPath toCopy)
        {
            this.mPath = new List<PathElement>(toCopy);
        }

        // Non-ref version
        public static ObjectPath FromString(string path)
        {
            return ObjectPath.TryParse(ref path);
        }

        public static ObjectPath TryParse(ref string path)
        {
            // ObjectPath   -- separated by '.'
            // IndexerPath  -- delimited by {}
            // ArrayPath    -- delimited by []
            // FieldPath    -- identifier name

            ObjectPath objectPath = new ObjectPath();

            while (true)
            {
                FieldPath fieldPath = FieldPath.TryParse(ref path);
                if (fieldPath == null)
                {
                    return null;
                }
                objectPath.Add(fieldPath);

                IndexerPath indexerPath = IndexerPath.TryParse(ref path);
                if (indexerPath != null)
                {
                    objectPath.Add(indexerPath);
                }
                else
                {
                    ArrayPath arrayPath = ArrayPath.TryParse(ref path);
                    if (arrayPath != null)
                    {
                        objectPath.Add(arrayPath);
                    }
                }

                if (path.Length == 0)
                {
                    return objectPath;
                }

                if (path[0] != '.')
                {
                    return null;
                }

                path = path.Substring(1);
            }
        }

        // Mmm, monadish...
        public override List<object> Follow(object fromHere)
        {
            List<object> current = new List<object> { fromHere };

            foreach (PathElement pathElement in this.mPath)
            {
                List<object> next = new List<object>();

                foreach (object obj in current)
                {
                    List<object> paths = pathElement.Follow(obj);
                    if (paths != null)
                    {
                        next.AddRange(pathElement.Follow(obj));
                    }
                    else
                    {
                        Logger.TraceWarning(LogTag.OldStyle, "Failed to follow path : {0}", this.ToString());
                    }
                }

                current = next;
            }

            return current;
        }

        public int IndexOf(PathElement item)
        {
            return this.mPath.IndexOf(item);
        }

        public void Insert(int index, PathElement item)
        {
            this.mPath.Insert(index, item);
        }

        public void RemoveAt(int index)
        {
            this.mPath.RemoveAt(index);
        }

        public PathElement this[int index]
        {
            get
            {
                return this.mPath[index];
            }
            set
            {
                this.mPath[index] = value;
            }
        }

        public void Add(PathElement item)
        {
            this.mPath.Add(item);
        }

        public void Clear()
        {
            this.mPath.Clear();
        }

        public bool Contains(PathElement item)
        {
            return this.mPath.Contains(item);
        }

        public void CopyTo(PathElement[] array, int arrayIndex)
        {
            this.mPath.CopyTo(array, arrayIndex);
        }

        public int Count
        {
            get { return this.mPath.Count; }
        }

        public bool IsReadOnly
        {
            get { return false; }
        }

        public bool Remove(PathElement item)
        {
            return this.mPath.Remove(item);
        }

        public IEnumerator<PathElement> GetEnumerator()
        {
            return this.mPath.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable)this.mPath).GetEnumerator();
        }


        public override void FromMessage(MessageBuffer message)
        {
            this.mPath.Clear();
            int count = message.ReadInt();
            for (int i = 0; i < count; i++)
            {
                PathElement element = PathElement.MakePathElement((PathElementType)message.ReadByte());
                if (element == null)
                {
                    Debug.Fail("Got unknown PathElement type");
                    return;
                }
                element.FromMessage(message);
                this.mPath.Add(element);
            }
        }

        public override void ToMessage(MessageBuffer message, Type parameterType)
        {
            message.Write(this.mPath.Count);
            foreach (PathElement element in this.mPath)
            {
                message.Write((byte)PathElement.ToPathElementType(element.GetType()));
                element.ToMessage(message, null);
            }
        }

        // TODO: Canonicalize paths when testing equality
        public bool Equals(ObjectPath other)
        {
            if (other == null || other.Count != this.Count)
            {
                return false;
            }

            for (int i = 0; i < this.Count; i++)
            {
                if (!this[i].Equals(other[i]))
                {
                    return false;
                }
            }

            return true;
        }

        public override bool Equals(object obj)
        {
            return this.Equals(obj as ObjectPath);  // TODO: A single PathElement wrapped in an ObjectPath should probably be considered equal to that PathElement unwrapped
        }

        public override int GetHashCode()
        {
            return this.Count == 0 ? 0 : this.Count ^ this[this.Count].GetHashCode();  // TODO: Fix this when canoncializing...
        }

        public override string ToString()
        {
            StringBuilder pathString = new StringBuilder();

            foreach (PathElement element in this.mPath)
            {
                if (element is FieldPath && pathString.Length > 0)
                {
                    pathString.Append('.');
                }

                pathString.Append(element.ToString());
            }

            return pathString.ToString();
        }
    }

    /*
#if DEBUG
    [TestFixture]
    public class FieldPathTestHelper : ParseableTestHelper
    {
        private class FieldPathTester : ParseableTester<FieldPath>
        {
            public override ICollection<FieldPath> CreateExpectedValues()
            {
                return new List<FieldPath>
                {
                    new FieldPath(""),
                    new FieldPath("mFieldName")
                };
            }
        }

        public FieldPathTestHelper()
            : base(new FieldPathTester())
        {
        }
    }

    [TestFixture]
    public class ArrayPathTestHelper : ParseableTestHelper
    {
        private class ArrayPathTester : ParseableTester<ArrayPath>
        {
            public override ICollection<ArrayPath> CreateExpectedValues()
            {
                return new List<ArrayPath>
                {
                    new ArrayPath(),
                    new ArrayPath(42)
                };
            }
        }

        public ArrayPathTestHelper()
            : base(new ArrayPathTester())
        {
        }
    }

    [TestFixture]
    public class IndexerPathTestHelper : ParseableTestHelper
    {
        private class IndexerPathTester : ParseableTester<IndexerPath>
        {
            public override ICollection<IndexerPath> CreateExpectedValues()
            {
                return new List<IndexerPath>
                {
                    new IndexerPath(new PeerAddress(System.Net.IPAddress.Loopback, 42, NatType.RestrictedPort))
                };
            }
        }

        public IndexerPathTestHelper()
            : base(new IndexerPathTester())
        {
        }
    }

    [TestFixture]
    public class ObjectPathTestHelper : ParseableTestHelper
    {
        private class ObjectPathTester : ParseableTester<ObjectPath>
        {
            public override ICollection<ObjectPath> CreateExpectedValues()
            {
                List<ObjectPath> expected = new List<ObjectPath>();

                ObjectPath path1 = new ObjectPath();
                path1.Add(new FieldPath("mField"));
                path1.Add(new ArrayPath(2));
                expected.Add(path1);

                ObjectPath path2 = new ObjectPath();
                path2.Add(new FieldPath("mField2"));
                path2.Add(path1);
                path2.Add(new FieldPath("mField3"));
                expected.Add(path2);

                return expected;
            }
        }

        public ObjectPathTestHelper()
            : base(new ObjectPathTester())
        {
        }
    }
#endif*/
}
