﻿using System;
using System.Collections.Generic;
using System.Text;
using Badumna.Core;
using System.Diagnostics;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace Diagnostics.WolfEye
{
    class PackagedObject : IParseable
    {
        private static readonly BinaryFormatter Formatter;

        static PackagedObject()
        {
            PackagedObject.Formatter = new BinaryFormatter();
            PackagedObject.Formatter.SurrogateSelector = new ParseableSurrogateSelector();
        }


        public enum SerializationStrategy : byte
        {
            Default,
            Null,
            DotNet,
            IParseable,
            String
        }


        private object mObject;
        public object Object { get { return this.mObject; } }
        private SerializationStrategy mStrategy;


        // For IParseable
        private PackagedObject()
        {
        }

        public PackagedObject(object obj)
            : this(obj, SerializationStrategy.Default)
        {
        }

        public PackagedObject(object obj, SerializationStrategy strategy)
        {
            this.mObject = obj;
            if (strategy == SerializationStrategy.Default)
            {
                this.mStrategy = this.DetermineStrategy();
            }
            else
            {
                this.mStrategy = strategy;
            }
        }

        private SerializationStrategy DetermineStrategy()
        {
            if (this.mObject == null)
            {
                return SerializationStrategy.Null;
            }

            if (this.mObject is IParseable)
            {
                return SerializationStrategy.IParseable;
            }

            if (this.mObject.GetType().IsSerializable)
            {
                return SerializationStrategy.DotNet;
            }

            return SerializationStrategy.String;
        }

        public void FromMessage(MessageBuffer message)
        {
            this.mStrategy = (SerializationStrategy)message.ReadByte();

            switch (this.mStrategy)
            {
                case SerializationStrategy.Null:
                    this.mObject = null;
                    break;

                case SerializationStrategy.DotNet:
                    int length = message.ReadInt();
                    byte[] bytes = message.ReadBytes(length);
                    using (MemoryStream stream = new MemoryStream(bytes))
                    {
                        this.mObject = PackagedObject.Formatter.Deserialize(stream);
                    }
                    break;

                case SerializationStrategy.IParseable:
                    string typeName = message.ReadString();
                    Type type = Type.GetType(typeName);
                    this.mObject = Activator.CreateInstance(type, true);
                    ((IParseable)this.mObject).FromMessage(message);
                    break;

                case SerializationStrategy.String:
                    this.mObject = message.ReadString();
                    break;

                default:
                    Debug.Fail("Invalid serialization strategy");
                    break;
            }
        }

        public void ToMessage(MessageBuffer message, Type parameterType)
        {
            message.Write((byte)this.mStrategy);

            switch (this.mStrategy)
            {
                case SerializationStrategy.Null:
                    // Nothing needs to be done here; this strategy can only represent a null value
                    break;

                case SerializationStrategy.DotNet:
                    using (MemoryStream stream = new MemoryStream())
                    {
                        PackagedObject.Formatter.Serialize(stream, this.mObject);
                        message.Write((int)stream.Length);
                        message.Write(stream.ToArray(), (int)stream.Length);
                    }
                    break;

                case SerializationStrategy.IParseable:
                    message.Write(this.mObject.GetType().AssemblyQualifiedName);
                    ((IParseable)this.mObject).ToMessage(message, parameterType);
                    break;

                case SerializationStrategy.String:
                    message.Write(this.mObject.ToString());
                    break;

                default:
                    Debug.Fail("Invalid serialization strategy");
                    break;
            }
        }
    }
}
