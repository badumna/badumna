﻿using System;
using System.Reflection;
using System.Collections.Generic;
using System.Collections;
using Badumna.Core;
using Badumna.Transport;
using System.Net;
using System.Threading;

namespace Diagnostics.WolfEye
{
    class Eye
    {
        public delegate void EyeResult(object returnValue, Exception maybeException);

        private delegate T Func<T>();


        private object mRoot;
        private TransportProtocol mProtocol;
        private int mNextRequestId;
        private Dictionary<int, EyeResult> mResultCallbacks = new Dictionary<int, EyeResult>();  // TODO: Expire old delegates

        private Dictionary<int, List<ObjectPath>> mFieldLists = new Dictionary<int, List<ObjectPath>>();


        public Eye(object root, TransportProtocol protocol)
        {
            if (root == null)
            {
                throw new ArgumentNullException("root");
            }

            this.mRoot = root;
            this.mProtocol = protocol;
        }

        public object GetObject(PathElement objectPath)
        {
            List<object> objects = this.GetObjects(objectPath);
            switch (objects.Count)
            {
                case 0:
                    return null;

                case 1:
                    return objects[0];
                    
                default:
                    throw new ArgumentException("Expected path without wildcards");
            }
        }

        public List<object> GetObjects(PathElement objectPath)
        {
            return objectPath == null ? new List<object> { this.mRoot } : objectPath.Follow(this.mRoot);
        }

        private TransportEnvelope MakeReturnEnvelope()
        {
            return this.mProtocol.GetMessageFor(this.mProtocol.CurrentEnvelope.Source, QualityOfService.Reliable);
        }

        private int SaveReturnCallback(EyeResult callback)
        {
            int requestId = Interlocked.Increment(ref this.mNextRequestId);
            if (callback != null)
            {
                lock (this.mResultCallbacks)
                {
                    this.mResultCallbacks[requestId] = callback;
                }
            }
            return requestId;
        }

        private void InvokeAndReturnResponse<T>(int requestId, Eye.Func<T> func)
        {
            T t = default(T);
            Exception error = null;

            try
            {
                t = func();
            }
            catch (Exception e)
            {
                error = e;
            }

            TransportEnvelope envelope = this.MakeReturnEnvelope();
            this.mProtocol.RemoteCall(envelope, this.ReturnResult, requestId, new PackagedObject(t), new PackagedObject(error));
            this.mProtocol.SendMessage(envelope);
        }

        public void RequestObject(IPEndPoint peer, ObjectPath objectPath, EyeResult returnCallback)
        {
            int requestId = this.SaveReturnCallback(returnCallback);
            TransportEnvelope envelope = this.mProtocol.GetMessageFor(new PeerAddress(peer, NatType.Unknown), QualityOfService.Reliable);
            this.mProtocol.RemoteCall(envelope, this.RequestObject, requestId, objectPath);
            this.mProtocol.SendMessage(envelope);
        }

        [DiagnosticProtocolMethod(DiagnosticMethod.EyeRequestObject)]
        private void RequestObject(int requestId, ObjectPath objectPath)
        {
            this.InvokeAndReturnResponse(requestId, delegate { return this.GetObject(objectPath); });
        }

        public void RequestRunTimeType(IPEndPoint peer, ObjectPath objectPath, EyeResult returnCallback)
        {
            int requestId = this.SaveReturnCallback(returnCallback);
            TransportEnvelope envelope = this.mProtocol.GetMessageFor(new PeerAddress(peer, NatType.Unknown), QualityOfService.Reliable);
            this.mProtocol.RemoteCall(envelope, this.RequestRunTimeType, requestId, objectPath);
            this.mProtocol.SendMessage(envelope);
        }

        [DiagnosticProtocolMethod(DiagnosticMethod.EyeRequestRunTimeType)]
        private void RequestRunTimeType(int requestId, ObjectPath objectPath)
        {
            this.InvokeAndReturnResponse<string>(requestId, delegate
            {
                object obj = this.GetObject(objectPath);
                return obj != null ? obj.GetType().AssemblyQualifiedName : null;
            });
        }



        public void RequestKeys(IPEndPoint peer, ObjectPath dictionaryPath, EyeResult returnCallback)
        {
            int requestId = this.SaveReturnCallback(returnCallback);
            TransportEnvelope envelope = this.mProtocol.GetMessageFor(new PeerAddress(peer, NatType.Unknown), QualityOfService.Reliable);
            this.mProtocol.RemoteCall(envelope, this.RequestKeys, requestId, dictionaryPath);
            this.mProtocol.SendMessage(envelope);
        }

        [DiagnosticProtocolMethod(DiagnosticMethod.EyeRequestKeys)]
        private void RequestKeys(int requestId, ObjectPath dictionaryPath)
        {
            this.InvokeAndReturnResponse(requestId, delegate
            {
                List<object> packagedKeys = new List<object>();
                foreach (object key in ((IDictionary)this.GetObject(dictionaryPath)).Keys)
                {
                    packagedKeys.Add(key);
                }
                return packagedKeys;
            });
        }

        public void RequestLength(IPEndPoint peer, ObjectPath listPath, EyeResult returnCallback)
        {
            int requestId = this.SaveReturnCallback(returnCallback);
            TransportEnvelope envelope = this.mProtocol.GetMessageFor(new PeerAddress(peer, NatType.Unknown), QualityOfService.Reliable);
            this.mProtocol.RemoteCall(envelope, this.RequestLength, requestId, listPath);
            this.mProtocol.SendMessage(envelope);
        }

        [DiagnosticProtocolMethod(DiagnosticMethod.EyeRequestLength)]
        private void RequestLength(int requestId, ObjectPath listPath)
        {
            this.InvokeAndReturnResponse(requestId, delegate
            {
                object obj = this.GetObject(listPath);

                int length = -1;
                if (obj is IList)
                {
                    length = ((IList)obj).Count;
                }
                else if (obj is Array)
                {
                    length = ((Array)obj).Length;
                }

                return length;
            });
        }

        public void CreateFieldList(IPEndPoint peer, int id, List<ObjectPath> fieldList, EyeResult returnCallback)
        {
            int requestId = this.SaveReturnCallback(returnCallback);
            TransportEnvelope envelope = this.mProtocol.GetMessageFor(new PeerAddress(peer, NatType.Unknown), QualityOfService.Reliable);
            this.mProtocol.RemoteCall(envelope, this.CreateFieldList, requestId, id, fieldList);
            this.mProtocol.SendMessage(envelope);
        }

        [DiagnosticProtocolMethod(DiagnosticMethod.EyeCreateFieldList)]
        private void CreateFieldList(int requestId, int id, List<ObjectPath> fieldList)
        {
            this.InvokeAndReturnResponse(requestId, delegate
            {
                this.mFieldLists[id] = fieldList;
                return id;
            });
        }

        public void RetrieveFields(IPEndPoint peer, int fieldListId, EyeResult returnCallback)
        {
            int requestId = this.SaveReturnCallback(returnCallback);
            TransportEnvelope envelope = this.mProtocol.GetMessageFor(new PeerAddress(peer, NatType.Unknown), QualityOfService.Reliable);
            this.mProtocol.RemoteCall(envelope, this.RetrieveFields, requestId, fieldListId);
            this.mProtocol.SendMessage(envelope);
        }

        [DiagnosticProtocolMethod(DiagnosticMethod.EyeRetrieveFields)]
        private void RetrieveFields(int requestId, int fieldListId)
        {
            this.InvokeAndReturnResponse(requestId, delegate
            {
                List<ObjectPath> fieldList;
                if (!this.mFieldLists.TryGetValue(fieldListId, out fieldList))
                {
                    return null;
                }

                List<object> result = new List<object>();

                foreach (ObjectPath path in fieldList)
                {
                    result.Add(this.GetObjects(path));
                }

                return result;
            });
        }
        
        [DiagnosticProtocolMethod(DiagnosticMethod.EyeReturnResult)]
        private void ReturnResult(int requestId, PackagedObject result, PackagedObject exception)
        {
            EyeResult handler;
            lock (this.mResultCallbacks)
            {
                if (this.mResultCallbacks.TryGetValue(requestId, out handler))
                {
                    this.mResultCallbacks.Remove(requestId);
                }
            }

            if (handler != null)
            {
                Exception ex = null;

                if (exception.Object != null)
                {
                    if (exception.Object is Exception)
                    {
                        ex = (Exception)exception.Object;
                    }
                    else
                    {
                        ex = new Exception(exception.Object.ToString());
                    }
                }

                handler(result.Object, ex);
            }
        }
    }
}
