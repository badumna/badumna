
using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;

using Badumna;
using Badumna.Core;
using Badumna.Utilities;
using Badumna.Transport;

namespace Diagnostics
{
    public class Statistics : IParseable, IStatistics
    {
        private const double KilobitsPerByte = 8.0 / 1024.0;

        private TimeSpan mStartTime;
        private TimeSpan mElapsedTime;
        public TimeSpan ElapsedTime { get { return this.mElapsedTime; } }

        private Dictionary<String, Dictionary<String, double>> mGeneralValues = new Dictionary<string, Dictionary<string, double>>();
        public Dictionary<String, Dictionary<String, double>> GeneralValues { get { return this.mGeneralValues; } }

        // Must be public for use as IParseable in diagnostics protocol
        public Statistics()
        {
            this.Reset(TimeSpan.Zero);
        }

        private void Reset(TimeSpan startTime)
        {
            this.mStartTime = startTime;
            this.mGeneralValues.Clear();
        }

        internal void StartCollecting(TimeSpan startTime)
        {
            this.Reset(startTime);
        }

        internal void FinishCollecting(ProtocolStack stack, NetworkEventQueue eventQueue, TimeSpan finishTime)
        {
            this.mElapsedTime = finishTime - this.mStartTime;

            double totalSeconds = this.mElapsedTime.TotalSeconds;
            if (totalSeconds > 0.0)
            {
                double totalBytesSent;
                if (this.TryGetValue("Transport", "TotalBytesSent", out totalBytesSent))
                {
                    this.Record("Transport", "AverageOutboundBandwidth", totalBytesSent * Statistics.KilobitsPerByte / totalSeconds);
                }

                double totalBytesReceived;
                if (this.TryGetValue("Transport", "TotalBytesReceived", out totalBytesReceived))
                {
                    this.Record("Transport", "AverageInboundBandwidth", totalBytesReceived * Statistics.KilobitsPerByte / totalSeconds);
                }

                double imBytes;
                if (this.TryGetValue("InterestManagement", "ParseBytes", out imBytes))
                {
                    this.Record("InterestManagement", "AverageIMBandwidth", imBytes * Statistics.KilobitsPerByte / totalSeconds);
                }
            }

            this.Record("General", "NetworkQueueLength", eventQueue.Count);
            this.Record("General", "ElapsedSeconds", this.mElapsedTime.TotalSeconds);
        }

        public bool TryGetValue(String group, String name, out double value)
        {
            value = default(double);

            Dictionary<string, double> groupDict;
            if (!this.mGeneralValues.TryGetValue(group, out groupDict))
            {
                return false;
            }

            return groupDict.TryGetValue(name, out value);
        }
        
        public void Record(String group, String name, double value)
        {
            Dictionary<string, double> groupDict;
            this.mGeneralValues.TryGetValue(group, out groupDict);
            if (groupDict == null)
            {
                groupDict = new Dictionary<string,double>();
                this.mGeneralValues[group] = groupDict;
            }

            double total;
            groupDict.TryGetValue(name, out total);
            groupDict[name] = total + value;
        }

        #region IParseable Members

        void IParseable.ToMessage(MessageBuffer message, Type parameterType)
        {
            if (null != message)
            {
                message.PutObject(this.mGeneralValues, typeof(Dictionary<String, Dictionary<String, double>>));
            }
        }

        void IParseable.FromMessage(MessageBuffer message)
        {
            if (null != message)
            {
                this.mGeneralValues = message.GetObject(
                    typeof(Dictionary<String, Dictionary<String, double>>),
                    f => { return Activator.CreateInstance(typeof(Dictionary<String, Dictionary<String, double>>), true); }) as Dictionary<String, Dictionary<String, double>>;
            }
        }

        #endregion
    }
}

