using System;
using System.Collections.Generic;
using System.Text;

using Badumna;
using Badumna.Core;
using Badumna.DistributedHashTable;
using Diagnostics;

namespace Badumna.Multicast
{
    partial class MulticastRouterCheckpointData : ICheckpointData
    {

        #region ICheckpointData Members

        public new List<string> Validate(PeerAddress localAddress,
            Dictionary<PeerAddress, CheckpointItemSet> globalData, Dictionary<string, object> summaries)
        {
            return base.Validate(localAddress, globalData, summaries);
        }

        protected override bool IsMember(PeerAddress address, Dictionary<PeerAddress, CheckpointItemSet> globalData)
        {
            CheckpointItemSet itemSet = null;
            globalData.TryGetValue(address, out itemSet);
            if (null != itemSet)
            {
                return null != itemSet.GetItem<MulticastFacadeCheckpointData>("MulticastFacade");
            }

            return false;
        }

        public new void Render(System.Drawing.Graphics context, CheckpointItemSet otherData, Dictionary<string, object> options)
        {
            if (options.ContainsKey("dht radius"))
            {
                float radius = (float)options["dht radius"];

                if (options.ContainsKey("render leafset"))
                {
                    DhtVisualiser.Instance.RenderLeafset(this.mLeafSet, context, radius - 10f);
                }
            }
        }


        #endregion


    }
}
