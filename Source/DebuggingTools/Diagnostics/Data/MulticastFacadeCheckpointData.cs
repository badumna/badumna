using System;
using System.Collections.Generic;
using System.Text;

using Badumna;
using Badumna.Core;
using Badumna.Utilities;
using Diagnostics;

namespace Badumna.Multicast
{
    partial class MulticastFacadeCheckpointData : ICheckpointData
    {
        #region ICheckpointData Members

        public List<string> Validate(PeerAddress localAddress, Dictionary<PeerAddress, CheckpointItemSet> globalData, Dictionary<string, object> summaries)
        {
            List<String> issues = new List<string>();

            foreach (KeyValuePair<BadumnaId, MulticastGroupCheckpointData> item in this.mGroups)
            {
                issues.AddRange(item.Value.Validate(localAddress, globalData, summaries));
            }

            return issues;
        }

        public void Render(System.Drawing.Graphics context, CheckpointItemSet otherData, Dictionary<string, object> options)
        {
            foreach (KeyValuePair<BadumnaId, MulticastGroupCheckpointData> item in this.mGroups)
            {
                item.Value.Render(context, otherData, options);
            }          
        }

        public void GetStatistics(PeerAddress localAddress, Dictionary<string, object> summaries, Statistics stats)
        {
        }

        #endregion
    }
}
