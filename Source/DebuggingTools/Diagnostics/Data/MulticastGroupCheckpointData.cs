using System;
using System.Collections.Generic;
using System.Text;

using Badumna;
using Badumna.Core;
using Diagnostics;

namespace Badumna.Multicast
{
    partial class MulticastGroupCheckpointData : ICheckpointData
    {
        #region ICheckpointData Members

        public List<string> Validate(PeerAddress localAddress, Dictionary<PeerAddress, CheckpointItemSet> globalData, Dictionary<string, object> summaries)
        {
            return this.mRouter.Validate(localAddress, globalData, summaries);
        }

        public void Render(System.Drawing.Graphics context, CheckpointItemSet otherData, Dictionary<string, object> options)
        {
            this.mRouter.Render(context, otherData, options);
        }

        public void GetStatistics(PeerAddress localAddress, Dictionary<string, object> summaries, Statistics stats)
        {
        }

        #endregion
    }
}
