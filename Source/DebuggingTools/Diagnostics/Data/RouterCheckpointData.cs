using System;
using System.Collections.Generic;
using System.Text;

using Badumna;
using Badumna.Core;
using Diagnostics;

namespace Badumna.DistributedHashTable
{
    partial class RouterCheckpointData : ICheckpointData
    {
        #region ICheckpointData Members

        public List<string> Validate(PeerAddress localAddress, Dictionary<PeerAddress, CheckpointItemSet> globalData, Dictionary<string, object> summaries)
        {
            
            List<String> issues = new List<string>();
            List<PeerAddress> peersInThisDht;

            // Create the list of all peers in this dht and save as summary data.
            if (!summaries.ContainsKey("Router"))
            {
                peersInThisDht = new List<PeerAddress>();

                foreach (KeyValuePair<PeerAddress, CheckpointItemSet> peerItemSet in globalData)
                {
                    if (this.IsMember(peerItemSet.Key, globalData))
                    {
                        peersInThisDht.Add(peerItemSet.Key);
                    }
                }
                summaries.Add("Router", peersInThisDht);
            }

            peersInThisDht = (List<PeerAddress>)summaries["Router"];

            // Validate the leafset
            LeafSet expectedLeafset = new LeafSet();

            expectedLeafset.Initialize(localAddress);
            foreach (PeerAddress peer in peersInThisDht)
            {
                expectedLeafset.AddNeighbour(peer);
            }

            if (expectedLeafset.UniqueNodeCount != this.mLeafSet.UniqueNodeCount)
            {
                issues.Add(String.Format("{0} : Expected leafset to have {1} unique neighbours but it actually has {2}",
                    localAddress, expectedLeafset.UniqueNodeCount, this.mLeafSet.UniqueNodeCount));
            }
            else
            {
                for (int i = -this.mLeafSet.PredecessorCount; i <= this.mLeafSet.SuccessorCount; i++)
                {
                    try
                    {
                        if (!expectedLeafset[i].Address.Equals(this.mLeafSet[i].Address))
                        {
                            issues.Add(String.Format("{0} : Expected leafset member {3} to be {1} but it is actually {2}",
                                localAddress, expectedLeafset[i].Address, this.mLeafSet[i].Address, i));
                        }
                    }
                    catch (ArgumentOutOfRangeException) { }
                }
            }

            return issues;
        }

        protected virtual bool IsMember(PeerAddress address, Dictionary<PeerAddress, CheckpointItemSet> globalData)
        {
            CheckpointItemSet itemSet = null;
            globalData.TryGetValue(address, out itemSet);
            if (null != itemSet)
            {
                return null != itemSet.GetItem<DhtFacadeCheckpointData>("DhtFacade");
            }

            return false;
        }


        public void Render(System.Drawing.Graphics context, CheckpointItemSet otherData, Dictionary<string, object> options)
        {
            DhtVisualiser.Instance.Render(this, context, otherData, options);
        }

        public void GetStatistics(PeerAddress localAddress, Dictionary<string, object> summaries, Statistics stats)
        {
        }

        #endregion
    }
}

