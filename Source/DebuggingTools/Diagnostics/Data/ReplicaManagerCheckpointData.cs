using System;
using System.Collections.Generic;
using System.Text;

using Badumna;
using Badumna.Core;
using Badumna.Utilities;
using Diagnostics;

namespace Badumna.DistributedHashTable
{
    internal interface IReplicaManagerCheckpointData
    {
        void ValidateReplicas(PeerAddress localAddress, Dictionary<PeerAddress, DhtFacadeCheckpointData> dhtData,
            LeafSet leafset, ref List<String> issueList);
    }

#if false  // Currently disabled because replica store checkpoint data is quite large
    partial class ReplicaManagerCheckpointData<T> : IReplicaManagerCheckpointData where T : IReplicatedObject
    {
        public void ValidateReplicas(PeerAddress localAddress, Dictionary<PeerAddress, DhtFacadeCheckpointData> dhtData, 
            LeafSet leafset, ref List<String> issueList)
        {
            foreach (KeyValuePair<HashKey, SortedList<UniqueNetworkId, T>> replicaHashKeyListPair in this.mReplicatedObjectStore)
            {
                HashKey replicaKey = replicaHashKeyListPair.Key;

                // Check that the replica is stored on each neighbour of the leafset.

                for (int i = -leafset.PredecessorCount; i < leafset.SuccessorCount; i++)
                {
                    if (leafset[i].Address.Equals(localAddress))
                    {
                        continue;
                    }


                    if (!dhtData.ContainsKey(leafset[i].Address))
                    {
                        issueList.Add(String.Format("No checkpoint data for peer {0}", leafset[i].Address));
                        continue;
                    }

                    if (!dhtData[leafset[i].Address].mReplicaManagerStore.ContainsKey(typeof(T)))
                    {
                        issueList.Add(String.Format("Peer {0} {1}th neighbour {2} doesn't contain any replicas for type {3}",
                            localAddress, i, leafset[i].Address, typeof(T).Name));
                        continue;
                    }

                    ReplicaManagerCheckpointData<T> neighboursReplicas = 
                        dhtData[leafset[i].Address].mReplicaManagerStore[typeof(T)] as ReplicaManagerCheckpointData<T>;

                    if (neighboursReplicas == null)
                    {
                        issueList.Add(String.Format("Failed to convert neighbour's replica manager to required type!"));
                        continue;
                    }

                    if (!neighboursReplicas.mReplicatedObjectStore.ContainsKey(replicaKey))
                    {
                        issueList.Add(String.Format("Peer {0} {1}th neighbour {2} doesn't contain any replicas for key {3}",
                            localAddress, i, leafset[i].Address, replicaKey));
                        continue;
                    }

                    foreach (KeyValuePair<UniqueNetworkId, T> replicaPair in replicaHashKeyListPair.Value)
                    {
                        if (neighboursReplicas.mReplicatedObjectStore[replicaKey].ContainsKey(replicaPair.Key))
                        {
                            IReplicatedObject neighboursCopy = neighboursReplicas.mReplicatedObjectStore[replicaKey][replicaPair.Key];

                            if (!neighboursCopy.ModificationNumber.Equals(replicaPair.Value.ModificationNumber))
                            {
                                issueList.Add(String.Format("Peer {0} {1}th neighbour {2} replica modification nuber for {3} is different. Expected {4} but was {5}",
                                    localAddress, i, leafset[i].Address, replicaPair.Value.Guid, replicaPair.Value.ModificationNumber, neighboursCopy.ModificationNumber));
                            }
                        }
                        else
                        {
                            issueList.Add(String.Format("Peer {0} {1}th neighbour {2} does not have a copy of replica {3}",
                                localAddress, i, leafset[i].Address, replicaPair.Value.Guid));
                        }
                    }
                }
            }
        }
    }
#endif

}
