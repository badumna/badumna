using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

using Badumna.Core;
using Diagnostics;

namespace Badumna.InterestManagement
{
    partial class BoundedImManagerCheckpointData : ICheckpointData
    {
        public List<string> Validate(PeerAddress localAddress, Dictionary<PeerAddress, CheckpointItemSet> globalData, Dictionary<String, Object> summaries)
        {
            return this.mImServer.Validate(localAddress, globalData, summaries);
        }

        public void Render(Graphics context, CheckpointItemSet otherData, Dictionary<String, Object> options)
        {
            this.mImServer.Render(context, otherData, options);
        }

        public void GetStatistics(PeerAddress localAddress, Dictionary<String, Object> summaries, Statistics stats)
        {
        }
    }
}
