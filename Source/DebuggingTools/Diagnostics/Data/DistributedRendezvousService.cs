using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;

using Badumna.Core;
using Badumna.Utilities;
using Badumna.DistributedHashTable;

namespace Diagnostics.Data
{   
    internal class DistributedRendezvousService : RendezvousService<DhtEnvelope> 
    {
        private DhtFacade mDht;

        private static DhtFacade mFacade;
        public static DhtFacade Facade
        {
            set { DistributedRendezvousService.mFacade = value; }
        }

        /*
        public static IRendezvousService InstanstiateService(ConnectionTable connectionTable)
        {
            if (null == DistributedRendezvousService.mFacade)
            {
                return new DistributedRendezvousService(connectionTable, NetworkContext.CurrentContext.DhtTiers[1]);
            }

            return new DistributedRendezvousService(connectionTable, DistributedRendezvousService.mFacade);
        }
        */

        public DistributedRendezvousService(ConnectionTable connectionTable, DhtFacade dhtFacade)
            : base(connectionTable, dhtFacade)
        {
            this.mDht = dhtFacade;
        }

        public override DhtEnvelope GetMessageFor(params object[] args)
        {
            DhtEnvelope envelope = this.mDht.GetMessageFor(args);

            this.SealMessage(envelope, this.mSeal);

            return envelope;
        }

        protected override void DelayedRegisterRequest(PeerAddress publicAddress)
        {
            // Do nothing. The distributed version does not require registering.
        }

        protected override DhtEnvelope PrepareRequestMessage(PeerAddress destinationsPublicAddress, QualityOfService qos)
        {
            HashKey destinationKey = HashKey.Hash(destinationsPublicAddress.HumanReadable());

            // The qos needs to be reliable because it may be lost if the first rout fails and it doesn't attempt excluded retries.
            // It needs to be immediate because relay messages cannot be allowed to piggyback messages to the intermediate nodes,
            // since they may be assumed to be a part of the relay message and sent to the destination. It also makes sense to 
            // expedite rendezvous service requests to ensure connections are formed as fast as possible.
            QualityOfService immediateAndReliable = new QualityOfService(true, 0);
            DhtEnvelope envelope = this.mDht.GetMessageFor(destinationKey, immediateAndReliable);

            this.SealMessage(envelope, this.mSeal);

            return envelope;
        }

        protected override void RouteRequest(DhtEnvelope messageEnvelope, PeerAddress destinationsPublicAddress)
        {
            // Delay the route request to ensure that all dht tiers have time to initialize.
            NetworkEventQueue.Instance.Push("Rendezvous route request", 1, new GenericCallBack<DhtEnvelope>(this.mDht.RouteMessage), messageEnvelope);
        }

        protected override void RegisterAddress(PeerAddress address)
        {
            Logger.TraceInformation("Request to register address {0} for rendezvous on the DHT.", address.HumanReadable());
        }

        protected override void RelayMessage(PeerAddress source, PeerAddress destination, TransportEnvelope payloadMessage)
        {
            if (!this.mConnectionTable.IsKnown(destination))
            {
                this.MakeConnectionRequestWith(destination);
                this.mConnectionTable.ConfirmConnection(destination);
            }

            // Ensure that a connection is made with the source of this message optimize the routing to a direct send.
            this.mConnectionTable.ConfirmConnection(source);

            base.RelayMessage(source, destination, payloadMessage);
        }
        
        protected override void InitiateUnidirectionalHolePunch(PeerAddress source, PeerAddress destination)
        {
            if (!this.mConnectionTable.IsConnected(destination))
            {
                this.MakeConnectionRequestWith(destination);
                this.mConnectionTable.ConfirmConnection(destination);
            }

            if (source.Equals(NetworkContext.CurrentContext.PublicAddress) && this.mConnectionTable.IsKnown(destination))
            {
                Logger.TraceVerbose(                    "Ignoring request for hole punch with {0} because a connection is already in the process of being established.", destination.HumanReadable());
                return;
            }

            base.InitiateUnidirectionalHolePunch(source, destination);
        }

        private void MakeConnectionRequestWith(PeerAddress address)
        {
            HashKey key = HashKey.Hash(address);
            IList<NodeInfo> nodeList = this.mDht.GetReplicatingNodesList(key, 2);

            if (nodeList.Count < 1)
            {
                Logger.TraceError(                    "A rendezvous request has been routed to a node that has no eligible neighbours.");
                return;
            }

            Logger.TraceVerbose(                "Handling request for unknown peer {0}. Asking neighbours to connect.", address.HumanReadable());

            foreach (NodeInfo node in nodeList)
            {
                if (!node.Address.Equals(NetworkContext.CurrentContext.PublicAddress))
                {
                    DhtEnvelope envelope = this.GetMessageFor(node.Address, QualityOfService.Reliable);

                    this.RemoteCall(envelope, this, "ConnectionRequest", address);
                    this.SendMessage(envelope);
                }
            }
        }

        [ProtocolMethod]
        private void ConnectionRequest(PeerAddress destination)
        {
            DhtEnvelope envelope = this.GetMessageFor(destination, QualityOfService.Reliable);

            this.RemoteCall(envelope, this, "ConnectWith", this.CurrentEnvelope.Source);
            this.SendMessage(envelope);
        }

        [ProtocolMethod]
        private void ConnectWith(PeerAddress address)
        {
            this.mConnectionTable.ConfirmConnection(address);
        }
        
    }
}
