/***************************************************************************
 *  File Name: ObjectManagerCheckpointData.cs
 *
 *  Copyright (C) 2007-2008 All Rights Reserved.
 * 
 *  Written by
 *      David Churchill <dgc@csse.unimelb.edu.au>
 ****************************************************************************/
using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;

using Badumna.Core;
using Badumna;
using Badumna.Utilities;
using Diagnostics;

namespace Badumna.ObjectManagement
{
    partial class ObjectManagerCheckpointData : ICheckpointData
    {
        public List<string> Validate(PeerAddress localAddress, Dictionary<PeerAddress, CheckpointItemSet> globalData, Dictionary<string, object> summaries)
        {
            List<string> issueList = new List<string>();

            this.ValidateInternally(localAddress, ref issueList);
            this.ValidateLocally(localAddress, globalData[localAddress], ref issueList);

            if (!summaries.ContainsKey("MasterObjects"))
            {
                summaries["MasterObjects"] = this.ExtractMasterObjects(globalData);
            }
            Dictionary<BadumnaId, LocalObjectWrapperCheckpointData> masterObjects =
                (Dictionary<BadumnaId, LocalObjectWrapperCheckpointData>)summaries["MasterObjects"];

            this.ValidateGlobally(localAddress, masterObjects, ref issueList);

            foreach (KeyValuePair<BadumnaId, RemoteObjectWrapperCheckpointData> wrapperKVP in this.mRemoteWrappers)
            {
                issueList.AddRange(wrapperKVP.Value.Validate(localAddress, globalData, summaries));
            }

            return issueList;
        }

        private Dictionary<BadumnaId, LocalObjectWrapperCheckpointData> ExtractMasterObjects(Dictionary<PeerAddress, CheckpointItemSet> globalData)
        {
            Dictionary<BadumnaId, LocalObjectWrapperCheckpointData> masterObjects =
                new Dictionary<BadumnaId, LocalObjectWrapperCheckpointData>();

            foreach (KeyValuePair<PeerAddress, CheckpointItemSet> datum in globalData)
            {
                ObjectManagerCheckpointData manager = datum.Value.GetItem<ObjectManagerCheckpointData>("ObjectManager");
                if (manager == null)
                {
                    Debug.Fail(String.Format("Peer {0} is missing object manager checkpoint data", datum.Key));
                    continue;
                }
                PeerAddress managerAddress = datum.Key;

                foreach (KeyValuePair<BadumnaId, LocalObjectWrapperCheckpointData> objectPair in manager.mLocalWrappers)
                {
                    if (objectPair.Key.Address.Equals(managerAddress))
                    {
                        masterObjects.Add(objectPair.Key, objectPair.Value);
                    }
                }
            }

            return masterObjects;
        }
        
        private void ValidateInternally(PeerAddress localAddress, ref List<string> issueList)
        {
            List<BadumnaId> tempLocalObjects = new List<BadumnaId>();

            foreach (KeyValuePair<BadumnaId, LocalObjectWrapperCheckpointData> localWrapper in this.mLocalWrappers)
            {
                if (!localWrapper.Key.Equals(localWrapper.Value.mGuid))
                {
                    issueList.Add(String.Format("Local wrapper list on peer {0} has wrapper with guid {1} stored under key {2}",
                        localAddress, localWrapper.Value.mGuid, localWrapper.Key));
                }

                if (!localWrapper.Key.Address.Equals(localAddress))
                {
                    issueList.Add(String.Format("Local wrapper list on peer {0} has wrapper with non-local guid {1}",
                        localAddress, localWrapper.Key));
                }

                tempLocalObjects.Add(localWrapper.Key);
            }

            foreach (KeyValuePair<BadumnaId, RemoteObjectWrapperCheckpointData> remoteWrapper in this.mRemoteWrappers)
            {
                if (remoteWrapper.Key != remoteWrapper.Value.mGuid)
                {
                    issueList.Add(String.Format("Remote wrapper list on peer {0} has object with guid {1} stored under key {2}",
                        localAddress, remoteWrapper.Value.mGuid, remoteWrapper.Key));
                }

                if (remoteWrapper.Key.Address.Equals(localAddress) && !tempLocalObjects.Contains(remoteWrapper.Key))
                {
                    issueList.Add(String.Format("Remote wrapper list on peer {0} contains local object {1} which is not on the local wrapper list",
                        localAddress, remoteWrapper.Key));
                }
            }
        }

        private void ValidateLocally(PeerAddress localAddress, CheckpointItemSet itemSet, ref List<string> issueList)
        {
        }

        private void ValidateGlobally(PeerAddress localAddress, Dictionary<BadumnaId, LocalObjectWrapperCheckpointData> masterObjects, ref List<string> issueList)
        {
        }

        // Returns the subset of objects that are in the peer's interest region(s)
        private Dictionary<BadumnaId, CommonNetworkObjectDataCheckpointData>
            VisibleObjects(Dictionary<BadumnaId, CommonNetworkObjectDataCheckpointData> objectList)
        {
            Dictionary<BadumnaId, CommonNetworkObjectDataCheckpointData> result =
                new Dictionary<BadumnaId, CommonNetworkObjectDataCheckpointData>();


            foreach (KeyValuePair<BadumnaId, CommonNetworkObjectDataCheckpointData> remoteEntity in objectList)
            {
                Vector3 remotePosition = remoteEntity.Value.Position;
                float remoteRadius = remoteEntity.Value.Radius;

                foreach (KeyValuePair<BadumnaId, LocalObjectWrapperCheckpointData> localWrapper in this.mLocalWrappers)
                {
                    CommonNetworkObjectDataCheckpointData localData = localWrapper.Value.mNetworkObjectData;
                    Vector3 localPosition = localData.Position;
                    float localAoiRadius = localData.AreaOfInterestRadius;

                    if (Geometry.SpheresIntersect(localPosition, localAoiRadius, remotePosition, remoteRadius))
                    {
                        result.Add(remoteEntity.Key, remoteEntity.Value);
                        break;
                    }
                }
            }

            return result;
        }

        public void GetStatistics(PeerAddress localAddress, Dictionary<string, object> summaries, Statistics stats)
        {
            // Calculate distance errors in the local view

            Dictionary<BadumnaId, LocalObjectWrapperCheckpointData> masterObjects =
                (Dictionary<BadumnaId, LocalObjectWrapperCheckpointData>)summaries["MasterObjects"];

            Dictionary<BadumnaId, CommonNetworkObjectDataCheckpointData> masterSpatialEntities =
                new Dictionary<BadumnaId, CommonNetworkObjectDataCheckpointData>();
            foreach (KeyValuePair<BadumnaId, LocalObjectWrapperCheckpointData> kvp in masterObjects)
            {
                masterSpatialEntities[kvp.Key] = kvp.Value.mNetworkObjectData;
            }


            Dictionary<BadumnaId, CommonNetworkObjectDataCheckpointData> remoteSpatialEntities =
                new Dictionary<BadumnaId, CommonNetworkObjectDataCheckpointData>();
            foreach (KeyValuePair<BadumnaId, RemoteObjectWrapperCheckpointData> kvp in this.mRemoteWrappers)
            {
                remoteSpatialEntities[kvp.Key] = kvp.Value.mSpatialEntityData;
            }

            Dictionary<BadumnaId, CommonNetworkObjectDataCheckpointData> realObjects = this.VisibleObjects(masterSpatialEntities);
            Dictionary<BadumnaId, CommonNetworkObjectDataCheckpointData> peerViewObjects = this.VisibleObjects(remoteSpatialEntities);

            stats.Record("ObjectManager", "VisibleObjects", peerViewObjects.Count);
            stats.Record("ObjectManager", "RealObjects", realObjects.Count);

            double averageDistanceError = 0.0;
            double maxDistanceError = 0.0;
            int goodObjectCount = 0;

            foreach (KeyValuePair<BadumnaId, CommonNetworkObjectDataCheckpointData> realObject in
                new Dictionary<BadumnaId, CommonNetworkObjectDataCheckpointData>(realObjects))
            {
                if (peerViewObjects.ContainsKey(realObject.Key))
                {
                    Vector3 realObjectPosition = realObject.Value.Position;

                    CommonNetworkObjectDataCheckpointData peerViewObject = peerViewObjects[realObject.Key];
                    Vector3 peerViewObjectPosition = peerViewObject.Position;

                    float distError = (peerViewObjectPosition - realObjectPosition).Magnitude;
                    averageDistanceError += distError;
                    goodObjectCount++;
                    if (distError > maxDistanceError)
                    {
                        maxDistanceError = distError;
                    }

                    // Remove from both lists so discrepancies can be checked later
                    realObjects.Remove(realObject.Key);
                    peerViewObjects.Remove(realObject.Key);
                }
                else if (this.mLocalWrappers.ContainsKey(realObject.Key))
                {
                    // Local objects may not necessarily be instantiated locally.  Count it as a good object for consistency with the
                    // case when they are instantiated.
                    goodObjectCount++;
                    realObjects.Remove(realObject.Key);
                }
            }

            //if (realObjects.Count > 0)
            //{
            //    Debug.WriteLine("Missing entities on " + localAddress.ToString() + ": " +
            //        String.Join(", ", new List<UniqueNetworkId>(realObjects.Keys).ConvertAll<string>(delegate(UniqueNetworkId id) { return id.ToString(); }).ToArray()));
            //}

            stats.Record("ObjectManager", "AverageDistanceError", goodObjectCount > 0 ? averageDistanceError / goodObjectCount : 0.0);
            stats.Record("ObjectManager", "MaximumDistanceError", maxDistanceError);
            stats.Record("ObjectManager", "GoodObjects", goodObjectCount);  // Convenience - the distance errors are useful if this is positive
            stats.Record("ObjectManager", "MissingObjects", realObjects.Count);
            stats.Record("ObjectManager", "HallucinatedObjects", peerViewObjects.Count);
        }


        public void Render(System.Drawing.Graphics context, CheckpointItemSet otherData, Dictionary<string, object> options)
        {
            bool peerSelected = options.ContainsKey("peer");

            Dictionary<string, object> remoteObjectOptions = new Dictionary<string, object>(options);
            remoteObjectOptions.Add("colour", "Orange");

            foreach (RemoteObjectWrapperCheckpointData nodData in this.mRemoteWrappers.Values)
            {
                if (peerSelected)
                {
                    nodData.Render(context, otherData, remoteObjectOptions);
                }
            }

            foreach (LocalObjectWrapperCheckpointData nodData in this.mLocalWrappers.Values)
            {
                nodData.Render(context, otherData, options);
            }
        }
    }
}
