using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.Drawing.Drawing2D;
using System.Drawing;

using Badumna.Core;
using Badumna;
using Diagnostics;

namespace Badumna.DistributedHashTable
{
    partial class DhtFacadeCheckpointData : ICheckpointData
    {
        public List<string> Validate(PeerAddress localAddress, Dictionary<PeerAddress, CheckpointItemSet> globalData, Dictionary<string, object> summaries)
        {
            List<String> issueList = new List<String>();
            
            /*
            if (!summaries.ContainsKey("DhtData"))
            {
                summaries["DhtData"] = this.ExtractDhtData(globalData);
            }
            Dictionary<PeerAddress, DhtFacadeCheckpointData> dhtData =
                (Dictionary<PeerAddress, DhtFacadeCheckpointData>)summaries["DhtData"];
            */
           // this.ValidateLeafSet(localAddress, new List<PeerAddress>(globalData.Keys), ref issueList);

            LeafSet leafset = this.GetLeafSet();
            if (leafset != null)
            {
                //foreach (KeyValuePair<Type, object> replicaManager in this.mReplicaManagerStore)
                //{
                //    if (replicaManager.Value is IReplicaManagerCheckpointData)
                //    {
                //        ((IReplicaManagerCheckpointData)replicaManager.Value).ValidateReplicas(localAddress, dhtData, leafset, ref issueList);
                //    }
                //}
            }
            else
            {
                issueList.Add(String.Format("Could not get leafset for peer {0}", localAddress));
            }

            issueList.AddRange((this.mRouter as RouterCheckpointData).Validate(localAddress, globalData, summaries));

            return issueList;
        }

        /*
        private Dictionary<PeerAddress, DhtFacadeCheckpointData> ExtractDhtData(Dictionary<PeerAddress, CheckpointItemSet> globalData)
        {
            Dictionary<PeerAddress, DhtFacadeCheckpointData> dhtData = new Dictionary<PeerAddress, DhtFacadeCheckpointData>();

            foreach (KeyValuePair<PeerAddress, CheckpointItemSet> datum in globalData)
            {
                DhtFacadeCheckpointData dhtItem = datum.Value.GetItem<DhtFacadeCheckpointData>("DhtFacade");
                if (dhtItem == null)
                {
                    Debug.Fail(String.Format("DHT checkpoint data for peer {0} is missing", datum.Key));
                    continue;
                }

                dhtData.Add(datum.Key, dhtItem);
            }

            return dhtData;
        }
        */

        private LeafSet GetLeafSet()
        {
            RouterCheckpointData router = this.mRouter as RouterCheckpointData;
            return router != null ? router.mLeafSet : null;
        }


        private void ValidateLeafSet(PeerAddress localAddress, List<PeerAddress> peerList, ref List<String> issueList)
        {
            RouterCheckpointData router = this.mRouter as RouterCheckpointData;

            if (router == null)
            {
                Debug.Fail("Unknown IRouter implementation");
                return;
            }
            
            LeafSet expectedLeafset = new LeafSet();

            // Construct the expected leafset by attempting to add all the known addresses
            expectedLeafset.Initialize(localAddress);
            foreach (PeerAddress address in peerList)
            {
                if (!address.Equals(localAddress))
                {
                    expectedLeafset.AddNeighbour(address);
                }
            }
            
            bool displayLeafset = false;

            // Check that the expected and actual leafsets have the same size.
            if (router.mLeafSet.PredecessorCount != expectedLeafset.PredecessorCount)
            {
                issueList.Add(String.Format("Leafset predecessor count mismatch for peer {0} : should be {1} but is {2}",
                    localAddress, expectedLeafset.PredecessorCount, router.mLeafSet.PredecessorCount));
                displayLeafset = true;
            }

            if (router.mLeafSet.SuccessorCount != expectedLeafset.SuccessorCount)
            {
                issueList.Add(String.Format("Leafset successor count mismatch for peer {0} : should be {1} but is {2}",
                    localAddress, expectedLeafset.SuccessorCount, router.mLeafSet.SuccessorCount));
                displayLeafset = true;
            }

            if (router.mLeafSet.UniqueNodeCount != expectedLeafset.UniqueNodeCount)
            {
                issueList.Add(String.Format("Leafset unique node count mismatch for peer {0} : should be {1} but is {2}",
                    localAddress, expectedLeafset.UniqueNodeCount, router.mLeafSet.UniqueNodeCount));
                displayLeafset = true;
            }

            // Check that each entry in the expected and actual leafsets are the same.
            for (int i = -expectedLeafset.PredecessorCount; i <= expectedLeafset.SuccessorCount; i++)
            {
                try
                {
                    if (!expectedLeafset[i].Address.Equals(router.mLeafSet[i].Address))
                    {
                        issueList.Add(String.Format("Peer {0} has unexpected leafset entry at {1}. Expected {2} but was {3}",
                            localAddress, i, expectedLeafset[i].Address, router.mLeafSet[i].Address));
                        displayLeafset = true;
                    }
                }
                catch (ArgumentOutOfRangeException)
                {
                }
            }

            if (displayLeafset)
            {
                for (int i = -router.mLeafSet.PredecessorCount; i <= router.mLeafSet.SuccessorCount; i++)
                {
                    try
                    {
                        issueList.Add(String.Format("{0}  {1}", i, router.mLeafSet[i].Address));
                    }
                    catch (ArgumentOutOfRangeException)
                    {
                    }
                }
            }
        }

        public void Render(Graphics context, CheckpointItemSet otherData, Dictionary<string, object> options)
        {
            if (this.mRouter != null)
            {
                if (this.mControllerManager != null)
                {
                    this.mControllerManager.Render(context, otherData, options);
                }

                if (options.ContainsKey("dht radius"))
                {
                    float radius = (float)options["dht radius"];

                    if (this.mTier == (int)NamedTier.OpenAddresses)
                    {
                        LeafSet leafset = this.GetLeafSet();
                        if (null == leafset || leafset[0].Address.Tier > 1)
                        {
                            return;
                        }
                        options["dht radius"] = radius / 2f;
                    }

                    (this.mRouter as ICheckpointData).Render(context, otherData, options);
                    options["dht radius"] = radius;
                }
            }
        }

        public void GetStatistics(PeerAddress localAddress, Dictionary<string, object> summaries, Statistics stats)
        {
        }
    }
}
