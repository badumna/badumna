using System;
using System.Collections.Generic;
using System.Text;

using Badumna.Core;
using Badumna;
using Badumna.Utilities;
using Diagnostics;

namespace Badumna.InterestManagement
{
    partial class RegionManagerCheckpointData : ICheckpointData
    {
        private class RegionData
        {
            public ImsRegion Region;
            public int Level = 1;   // 0 represents the top level IMS (e.g. spatial index on DHT)
            public List<BadumnaId> ContainedBy = new List<BadumnaId>();

            public RegionData(ImsRegion region)
            {
                this.Region = region;
            }
        }

        [NonSerialized]
        private List<string> mResults;


        public List<string> Validate(PeerAddress localAddress, Dictionary<PeerAddress, CheckpointItemSet> globalData, Dictionary<string, object> summaries)
        {
            this.mResults = new List<string>();

            if (this.mSubscriptionRegion == null)
            {
                // Region manager hasn't been initialized yet, can't validate
                return this.mResults;
            }

            if (!summaries.ContainsKey("ImsRegions"))
            {
                summaries["ImsRegions"] = this.GetImsRegions(globalData);
            }
            Dictionary<BadumnaId, RegionData> imsRegions = (Dictionary<BadumnaId, RegionData>)summaries["ImsRegions"];


            RegionData thisRegionData;  // Only set if this manages an IMS region
            imsRegions.TryGetValue(this.mSubscriptionRegion.Guid, out thisRegionData);


            // Check that all IMSes which the managed region intersects are on the IMS list
            // TODO: Check that IMSes that shouldn't be on a list aren't on the list

            bool intersectsIms = false;
            foreach (KeyValuePair<BadumnaId, RegionData> imsRegion in imsRegions)
            {
                if (imsRegion.Key.Equals(this.mSubscriptionRegion.Guid) ||  // Bounded IMSes shouldn't be checked against themselves
                    (thisRegionData != null && thisRegionData.Level < imsRegion.Value.Level))  // IMSes further down in the tree won't be on IMS lists higher in the tree
                {
                    continue;
                }

                if (Geometry.SpheresIntersect(this.mSubscriptionRegion.Centroid, this.mSubscriptionRegion.Radius,
                    imsRegion.Value.Region.Centroid, imsRegion.Value.Region.Radius))
                {
                    intersectsIms = true;
                    if (!this.mImServiceIds.Contains(imsRegion.Key))
                    {
                        this.mResults.Add(String.Format("Region {0} intersects the space owned by bounded IMS {1} but manager does not have it on its IMS list",
                            this.mSubscriptionRegion.Guid, imsRegion.Key));
                    }
                }
                else
                {
                    if (this.mImServiceIds.Contains(imsRegion.Key))
                    {
                        this.mResults.Add(String.Format("Region {0} does not intersect the space owned by bounded IMS {1} but manager has it on its IMS list",
                            this.mSubscriptionRegion.Guid, imsRegion.Key));
                    }
                }

                // The exact condition for when a manager should have the parent ims on the list is difficult to determine, but
                // if the region is completely contained by a bounded IMS (less Parameters.ParentRegionShrinkAmount) then it 
                // definately *shouldn't* be.  Equally, if the region does not intersect any bounded IMS then it *must* be.
                if (Geometry.SphereContained(imsRegion.Value.Region.Centroid, imsRegion.Value.Region.Radius - Parameters.ParentRegionShrinkAmount,
                        this.mSubscriptionRegion.Centroid, this.mSubscriptionRegion.Radius) &&
                    this.mImServiceIds.Contains(this.mInitialImsId))
                {
                    this.mResults.Add(String.Format("Region {0} is completely contained by bounded IMS {1} but manager has initial IMS on its IMS list",
                        this.mSubscriptionRegion.Guid, imsRegion.Key));
                }
            }

            if (!intersectsIms && !this.mImServiceIds.Contains(this.mInitialImsId))
            {
                this.mResults.Add(String.Format("Region {0} doesn't intersect any bounded IMS but manager does not have initial IMS on its IMS list",
                    this.mSubscriptionRegion.Guid));
            }

            return this.mResults;
        }

        private Dictionary<BadumnaId, RegionData> GetImsRegions(Dictionary<PeerAddress, CheckpointItemSet> globalData)
        {
            Dictionary<BadumnaId, RegionData> result = new Dictionary<BadumnaId, RegionData>();

            // Extract the bounded IMS regions from the global data
            foreach (KeyValuePair<PeerAddress, CheckpointItemSet> peerData in globalData)
            {
                BoundedImManagerCheckpointData bimData =
                    peerData.Value.GetItem<BoundedImManagerCheckpointData>("BoundedImManager");

                if (bimData == null)
                {
                    //NetworkTrace.TraceWarning("Peer {0} has no BoundedImManagerCheckpointData", peerData.Key);
                    // Probably no BIMS being used
                    continue;
                }

                foreach (KeyValuePair<string, BoundedObjectStoreCheckpointData> store in bimData.mImServer.mStores)
                {
                    result.Add(store.Value.mGuid, new RegionData(store.Value.mBoundingRegion));
                }
            }


            // Determine the level structure of the bounded IMS regions.  Levels are set to the minimum level that meets the
            // contraint:
            //     If IMS region r is fully contained by IMS region c, then r's level is at least c's level + 1.

            // Determine the contained lists
            foreach (KeyValuePair<BadumnaId, RegionData> regionPair in result)
            {
                ImsRegion thisRegion = regionPair.Value.Region;

                foreach (KeyValuePair<BadumnaId, RegionData> otherRegionPair in result)
                {
                    if (regionPair.Key == otherRegionPair.Key)
                    {
                        continue;
                    }

                    ImsRegion otherRegion = otherRegionPair.Value.Region;
                    if (Geometry.SphereContained(otherRegion.Centroid, otherRegion.Radius - Parameters.ParentRegionShrinkAmount, 
                        thisRegion.Centroid, thisRegion.Radius))
                    {
                        regionPair.Value.ContainedBy.Add(otherRegionPair.Key);
                    }
                }
            }

            // Determine the levels
            List<BadumnaId> remaining = new List<BadumnaId>(result.Keys);
            bool stillChanging = true;
            while (remaining.Count > 0 && stillChanging)
            {
                stillChanging = 
                    remaining.RemoveAll(
                        delegate(BadumnaId id)
                        {
                            return result[id].ContainedBy.Count == 0;
                        }) 
                    > 0;

                foreach (BadumnaId id in remaining)
                {
                    stillChanging |=
                        result[id].ContainedBy.RemoveAll(
                            delegate(BadumnaId i)
                            {
                                return !remaining.Contains(i);
                            })
                        > 0;

                    result[id].Level++;
                }
            }

            if (remaining.Count > 0)
            {
                this.mResults.Add("Could not extract level structure from IMS regions (has cycles?)");
            }

            return result;
        }


        public void Render(System.Drawing.Graphics context, CheckpointItemSet otherData, Dictionary<string, object> options)
        {
        }

        public void GetStatistics(PeerAddress localAddress, Dictionary<string, object> summaries, Statistics stats)
        {
        }
    }
}
