using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

using Badumna.Core;
using Badumna;
using Badumna.Utilities;
using Diagnostics;

namespace Badumna.InterestManagement
{
    partial class BoundedObjectStoreCheckpointData
    {
        public override List<string> Validate(PeerAddress localAddress, Dictionary<PeerAddress, CheckpointItemSet> globalData, Dictionary<string, object> summaries)
        {
            List<string> results = new List<string>();

            results.AddRange(this.mBoundingRegionManager.Validate(localAddress, globalData, summaries));
            results.AddRange(base.Validate(localAddress, globalData, summaries));

            return results;
        }

        public override void Render(Graphics context, CheckpointItemSet otherData, Dictionary<string, object> options)
        {
            bool peerSelected = options.ContainsKey("peer");

            foreach (KeyValuePair<BadumnaId, ImsRegion> region in this.mSpatialObjects)
            {
                if (!peerSelected && region.Value.Type == ImsRegion.RegionType.ImService && !region.Value.Guid.Equals(this.mGuid))
                {
                    // Global view, so don't render the neighbouring stores
                    continue;
                }

                region.Value.Render(context);
            }
        }
    }
}