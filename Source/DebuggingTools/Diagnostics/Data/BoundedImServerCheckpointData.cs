using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

using Badumna.Core;
using Badumna;
using Diagnostics;

namespace Badumna.InterestManagement
{
    partial class BoundedImServerCheckpointData : ICheckpointData
    {
        public List<string> Validate(PeerAddress localAddress, Dictionary<PeerAddress, CheckpointItemSet> globalData, Dictionary<string, object> summaries)
        {
            List<string> results = new List<string>();

            foreach (KeyValuePair<string, BoundedObjectStoreCheckpointData> store in this.mStores)
            {
                results.AddRange(store.Value.Validate(localAddress, globalData, summaries));
            }

            return results;
        }

        public void Render(Graphics context, CheckpointItemSet otherData, Dictionary<string, object> options)
        {
            foreach (KeyValuePair<string, BoundedObjectStoreCheckpointData> store in this.mStores)
            {
                store.Value.Render(context, otherData, options);
            }
        }

        public void GetStatistics(PeerAddress localAddress, Dictionary<string, object> summaries, Statistics stats)
        {
        }
    }
}