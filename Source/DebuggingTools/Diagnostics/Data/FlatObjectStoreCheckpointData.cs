using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

using Badumna;
using Badumna.Core;
using Badumna.Utilities;
using Diagnostics;

namespace Badumna.InterestManagement
{
    partial class FlatObjectStoreCheckpointData : ICheckpointData
    {
        public virtual List<string> Validate(PeerAddress localAddress, Dictionary<PeerAddress, CheckpointItemSet> globalData, Dictionary<string, object> summaries)
        {
            List<string> results = new List<string>();

            // Check the collision list
            foreach (KeyValuePair<BadumnaId, ImsRegion> region in this.mSpatialObjects)
            {
                List<BadumnaId> collisionsFound = new List<BadumnaId>();

                foreach (KeyValuePair<BadumnaId, ImsRegion> otherRegion in this.mSpatialObjects)
                {
                    if (!region.Key.Equals(otherRegion.Key) && region.Value.IsIntersecting(otherRegion.Value))
                    {
                        collisionsFound.Add(otherRegion.Key);
                    }
                }

                List<BadumnaId> missingList = new List<BadumnaId>();
                List<BadumnaId> extrasList = new List<BadumnaId>();

                if (!this.mCollisions.ContainsKey(region.Key))
                {
                    missingList = collisionsFound;
                }
                else
                {
                    List<BadumnaId> collisionList = new List<BadumnaId>(this.mCollisions[region.Key]);

                    foreach (BadumnaId id in collisionsFound)
                    {
                        if (!collisionList.Contains(id))
                        {
                            missingList.Add(id);
                            continue;
                        }

                        collisionList.Remove(id);
                    }

                    extrasList = collisionList;
                }


                if (missingList.Count > 0)
                {
                    List<string> missingListIds = missingList.ConvertAll<string>(
                        delegate(BadumnaId id)
                        {
                            System.Diagnostics.Debug.Assert(this.mSpatialObjects.ContainsKey(id));
                            return this.mSpatialObjects[id].ToString();
                        });

                    results.Add(String.Format("object store is missing collision(s) between {0} and [{1}]",
                        region.Value, Utils.Join(missingListIds, ", ")));
                }

                if (extrasList.Count > 0)
                {
                    List<string> extrasListIds = extrasList.ConvertAll<string>(
                        delegate(BadumnaId id)
                        {
                            return this.mSpatialObjects[id].ToString();
                        });
                    results.Add(String.Format("object store has non-existant collision(s) between {0} and [{1}]",
                        region.Value, Utils.Join(extrasListIds, ", ")));
                }
            }

            return results;
        }

        public virtual void Render(Graphics context, CheckpointItemSet otherData, Dictionary<string, object> options)
        {
            foreach (KeyValuePair<BadumnaId, ImsRegion> region in this.mSpatialObjects)
            {
                region.Value.Render(context);
            }
        }

        public void GetStatistics(PeerAddress localAddress, Dictionary<string, object> summaries, Statistics stats)
        {
        }
    }
}
