﻿using System;
using System.Collections.Generic;
using System.Text;

using Badumna;
using Badumna.Core;
using Diagnostics;

namespace Badumna.DistributedHashTable
{
    partial class ControllerManagerCheckpointData : ICheckpointData
    {
        #region ICheckpointData Members

        public List<string> Validate(Badumna.Core.PeerAddress localAddress, Dictionary<Badumna.Core.PeerAddress, CheckpointItemSet> globalData, Dictionary<string, object> summaries)
        {
            return new List<string>();
        }

        public void Render(System.Drawing.Graphics context, CheckpointItemSet otherData, Dictionary<string, object> options)
        {
            foreach (String controllerName in this.mControllerNames)
            {
                DhtVisualiser.Instance.RenderController(controllerName, context, otherData, options);
            }
        }

        public void GetStatistics(Badumna.Core.PeerAddress localAddress, Dictionary<string, object> summaries, Statistics stats)
        {
        }

        #endregion
    }
}
