﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

using Badumna.Core;
using Diagnostics;

namespace Badumna.ObjectManagement
{
    partial class RemoteObjectWrapperCheckpointData : ICheckpointData
    {
        public List<string> Validate(PeerAddress localAddress, Dictionary<PeerAddress, CheckpointItemSet> globalData, Dictionary<string, object> summaries)
        {
            List<string> results = new List<string>();

            return results;
        }

        public void Render(Graphics context, CheckpointItemSet otherData, Dictionary<string, object> options)
        {
            float x = this.mSpatialEntityData.Position.X;
            float y = -this.mSpatialEntityData.Position.Z;
            float aoi = this.mSpatialEntityData.AreaOfInterestRadius;
            float r = this.mSpatialEntityData.Radius;

            if (options.ContainsKey("swap y/z") && (bool)options["swap y/z"])
            {
                y = this.mSpatialEntityData.Position.Y;
            }

            Brush fillBrush;
            if (options.ContainsKey("colour"))
            {
                fillBrush = new SolidBrush(Color.FromName((string)options["colour"]));
            }
            else
            {
                fillBrush = new SolidBrush(Color.Red);
            }

            using (fillBrush)
            {
                context.DrawEllipse(Pens.Black, x - aoi, y - aoi, aoi * 2.0f, aoi * 2.0f);
                context.FillEllipse(fillBrush, x - r, y - r, r * 2.0f, r * 2.0f);
            }
        }

        public void GetStatistics(PeerAddress localAddress, Dictionary<string, object> summaries, Statistics stats)
        {
        }
    }
}
