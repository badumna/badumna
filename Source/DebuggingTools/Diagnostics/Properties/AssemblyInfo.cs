﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Diagnostics")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: InternalsVisibleTo("CustomControls, PublicKey=0024000004800000940000000602000000240000525341310004000001000100437008DC126F026A038171F7156E507FFCF8156A97F50D88E67633B107122E300995E28B0C02A83747A75275307DE875571372CF780CBBAE9C25DF42D3E6045A6297C1492FCAF7B126792BC3C70B89F3A3E7BADDE0E14662F03C5C7C52833DE85D6D16A3CB9BE068E02C4665DA76CB521EE3AF59968E497523CED7DFA9E8ECC3")]
[assembly: InternalsVisibleTo("NetworkSimulator, PublicKey=00240000048000009400000006020000002400005253413100040000010001007de84bbfe3dfb92d8a50a9b0b804eb07a02d28bb5b5665246b5a338a47df060263efe25fb2be507841aa9247a054092558cce470818f8c6ea295f8714f6e3f7494869e0b14c52f26a41e0e83f280e094e2c54150653db0b1fff68ea4ffba589787839be3f40bb6c3bc62a251ab54e60b0c1ff6c8e9afd8bf0be402929f40f4b6")]

[assembly: InternalsVisibleTo("ControlCenter, PublicKey=00240000048000009400000006020000002400005253413100040000010001009F9636C881E201164E5F0C3823E7F775E37A7128370A7F0E5748ACAF081CEB680C7E9D928C507FE26BB2C6F0510B68CE11FF4B9E6FC3C57FBFF9D8D302D543929F716440B84DE88C94FF13D34BB3381CDABC5928BC6E9152C0FB0265879767026F8CC98B5DE2FF330946237EF95B9DAD7512700CED1877DA5384877402384FCC")]
[assembly: InternalsVisibleTo("TheWolf, PublicKey=00240000048000009400000006020000002400005253413100040000010001007de84bbfe3dfb92d8a50a9b0b804eb07a02d28bb5b5665246b5a338a47df060263efe25fb2be507841aa9247a054092558cce470818f8c6ea295f8714f6e3f7494869e0b14c52f26a41e0e83f280e094e2c54150653db0b1fff68ea4ffba589787839be3f40bb6c3bc62a251ab54e60b0c1ff6c8e9afd8bf0be402929f40f4b6")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("4d39eedb-97a0-48d6-8de9-a8d5c144ea9a")]

