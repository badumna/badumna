using System;
using System.Collections.Generic;
using System.Text;
using System.Net;

using Badumna;
using Badumna.Core;
using Badumna.Utilities;

using Diagnostics.WolfEye;
using System.Xml;


namespace Diagnostics
{
    class DiagnosticsExtension : IDiagnosticsExtension
    {
        private DiagnosticController mDiagnosticsController;
        private INetworkFacadeInternal mNetworkFacadeInternal;

        public void Initialize(INetworkFacadeInternal facadeInternal)
        {
            this.mNetworkFacadeInternal = facadeInternal;

            try
            {
                XmlDocument document = new XmlDocument();
                document.Load("DiagnosticsConfig.xml");
                DiagnosticModule config = new DiagnosticModule(document);
                if (config.IsEnabled)
                {
                    this.Connect(config.ServerAddress.EndPoint);
                }
            }
            catch (Exception)
            {
            }
        }

        public void Connect(IPEndPoint serverEndPoint)
        {
            this.Disconnect();

            Logger.Statistics = new Statistics();
            this.mDiagnosticsController = new DiagnosticController((INetworkFacade)this.mNetworkFacadeInternal);
            this.mDiagnosticsController.Protocol.RequestShutdownEvent += this.mNetworkFacadeInternal.ShutdownHandler;
            this.mDiagnosticsController.InitializeAsClient(serverEndPoint);
            this.mNetworkFacadeInternal.ProcessingNetworkState += this.ProcessMessages;
        }

        public void Disconnect()
        {
            if (this.mDiagnosticsController != null)
            {
                this.mDiagnosticsController.Protocol.RequestShutdownEvent -= this.mNetworkFacadeInternal.ShutdownHandler;
                this.mNetworkFacadeInternal.ProcessingNetworkState -= this.ProcessMessages;
                this.mDiagnosticsController.ShutDown();
                this.mDiagnosticsController = null;
            }
        }

        private void ProcessMessages(object sender, EventArgs e)
        {
            this.mDiagnosticsController.ProcessMessages();
        }

        public void Shutdown()
        {
            this.Disconnect();
            this.mNetworkFacadeInternal = null;
        }
    }
}
