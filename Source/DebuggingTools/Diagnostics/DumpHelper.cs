﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using System.IO;
using System.Diagnostics;
using Microsoft.Win32.SafeHandles;

namespace Diagnostics
{
    static class DumpHelper
    {
        [Flags]
        public enum DumpType : int
        {
            MiniDumpNormal                           = 0x00000000,
            MiniDumpWithDataSegs                     = 0x00000001,
            MiniDumpWithFullMemory                   = 0x00000002,
            MiniDumpWithHandleData                   = 0x00000004,
            MiniDumpFilterMemory                     = 0x00000008,
            MiniDumpScanMemory                       = 0x00000010,
            MiniDumpWithUnloadedModules              = 0x00000020,
            MiniDumpWithIndirectlyReferencedMemory   = 0x00000040,
            MiniDumpFilterModulePaths                = 0x00000080,
            MiniDumpWithProcessThreadData            = 0x00000100,
            MiniDumpWithPrivateReadWriteMemory       = 0x00000200,
            MiniDumpWithoutOptionalData              = 0x00000400,
            MiniDumpWithFullMemoryInfo               = 0x00000800,
            MiniDumpWithThreadInfo                   = 0x00001000,
            MiniDumpWithCodeSegs                     = 0x00002000 
        }

        [DllImport("dbghelp.dll")]
        private static extern bool MiniDumpWriteDump(IntPtr processHandle, int processId, SafeFileHandle outputFile, 
            DumpType dumpType, IntPtr exceptionParam, IntPtr userStreamParam, IntPtr callbackParam);

        private delegate void Invoker();

        public static void WriteMiniDump(FileStream outputFile, DumpType dumpType)
        {
            
            using (Process proc = Process.GetCurrentProcess())
            {
                Invoker invoker = delegate()
                {
                    DumpHelper.MiniDumpWriteDump(proc.Handle, proc.Id, outputFile.SafeFileHandle, dumpType, IntPtr.Zero, IntPtr.Zero, IntPtr.Zero);
                };

                // Run dump from a different thread so that we get a proper stack trace for this thread
                IAsyncResult result = invoker.BeginInvoke(null, null);
                invoker.EndInvoke(result);
            }
        }
    }
}
