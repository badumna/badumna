﻿using System;
using System.Collections.Generic;
using System.Text;
using Badumna.Core;
using System.IO.Compression;
using System.IO;

namespace Diagnostics
{
    class CompressedString : IParseable
    {
        private string mContent;
        public string Content { get { return this.mContent; } }

        private byte[] mCompressed;


        public CompressedString()
        {
            this.mContent = "";
        }

        public CompressedString(string content)
        {
            if (content == null)
            {
                throw new ArgumentNullException("content");
            }

            this.mContent = content;
        }

        public void ToMessage(MessageBuffer message, Type parameterType)
        {
            if (this.mCompressed == null)
            {
                MemoryStream output = new MemoryStream();
                using (DeflateStream input = new DeflateStream(output, CompressionMode.Compress))
                using (StreamWriter sw = new StreamWriter(input))
                {
                    sw.Write(this.mContent);
                }
                this.mCompressed = output.ToArray();
            }

            message.Write(this.mCompressed.Length);
            message.Write(this.mCompressed, 0, this.mCompressed.Length);
        }

        public void FromMessage(MessageBuffer message)
        {
            int length = message.ReadInt();
            this.mCompressed = message.ReadBytes(length);
            MemoryStream input = new MemoryStream(this.mCompressed);
            using (DeflateStream output = new DeflateStream(input, CompressionMode.Decompress))
            using (StreamReader sr = new StreamReader(output))
            {
                this.mContent = sr.ReadToEnd();
            }
        }
    }
}
