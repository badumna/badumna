using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Diagnostics;
using System.Net;
using System.IO;

using Badumna;
using Badumna.Core;
using Badumna.Utilities;
using Badumna.Transport;

namespace Diagnostics
{
    class ConnectionEventArgs : EventArgs
    {
        private Badumna.Transport.ConnectionEventArgs mArgs;
        public bool IsConnected { get { return this.mArgs.IsConnected; } }
        public IPEndPoint Address { get { return this.mArgs.Address.EndPoint; } }

        public ConnectionEventArgs(Badumna.Transport.ConnectionEventArgs args)
        {
            this.mArgs = args;
        }
    }

    class DiagnosticController
    {
        public static void RequestDiagnosticsConnect(
            INetworkFacade networkFacade,
            IPEndPoint clientDispatchEndPoint,
            IPEndPoint clientPublicEndPoint,
            IPEndPoint serverEndPoint)
        {
            MainNetworkFacade facade = networkFacade as MainNetworkFacade;
            if (facade == null)
            {
                throw new InvalidOperationException("Facade isn't a CoreNetworkFacade");
            }
            ConnectionTable connectionTable = facade.Stack.ConnectionTable;

            TransportEnvelope envelope = connectionTable.GetConnectionlessMessageFor(new PeerAddress(clientDispatchEndPoint, NatType.Unknown));
            connectionTable.RemoteCall(envelope, facade.Diagnostics.InitializeAndConnect, serverEndPoint);
            connectionTable.SendConnectionlessMessage(envelope, new PeerAddress(clientPublicEndPoint, NatType.Unknown), false);
        }

        public static void RequestDiagnosticsDisconnect(
            INetworkFacade networkFacade,
            IPEndPoint clientDispatchEndPoint,
            IPEndPoint clientPublicEndPoint)
        {
            MainNetworkFacade facade = networkFacade as MainNetworkFacade;
            if (facade == null)
            {
                throw new InvalidOperationException("Facade isn't a CoreNetworkFacade");
            }
            ConnectionTable connectionTable = facade.Stack.ConnectionTable;

            TransportEnvelope envelope = connectionTable.GetConnectionlessMessageFor(new PeerAddress(clientDispatchEndPoint, NatType.Unknown));
            connectionTable.RemoteCall(envelope, facade.Diagnostics.Disconnect);
            connectionTable.SendConnectionlessMessage(envelope, new PeerAddress(clientPublicEndPoint, NatType.Unknown), false);
        }

        private TcpTransportLayer mTransportLayer;
        private DiagnosticProtocol mDiagnosticProtocol;
        internal DiagnosticProtocol Protocol { get { return this.mDiagnosticProtocol; } }

        public event EventHandler<ConnectionEventArgs> ConnectionEvent;

        private List<ConnectionEventArgs> mConnectionEvents = new List<ConnectionEventArgs>();

        private readonly object mConnectionEventLock = new object();

        private IPEndPoint mServerAddress;

        private NetworkEventQueue eventQueue;

        private INetworkConnectivityReporter connectivityReporter;

        private INetworkAddressProvider addressProvider;

        public DiagnosticController(INetworkFacade networkFacade)
        {
            INetworkFacadeInternal internalFacade = (INetworkFacadeInternal)networkFacade;
            this.connectivityReporter = ((MainNetworkFacade)networkFacade).Stack.ConnectivityReporter;
            this.addressProvider = ((MainNetworkFacade)networkFacade).Stack.TransportLayer;

            this.eventQueue = internalFacade.EventQueue;
            this.mTransportLayer = new TcpTransportLayer(this.connectivityReporter);
            this.mDiagnosticProtocol = new DiagnosticProtocol(
                networkFacade,
                this.mTransportLayer,
                this.mTransportLayer,
                this.eventQueue,
                internalFacade.TimeKeeper,
                this.addressProvider);
            this.mDiagnosticProtocol.ForgeMethodList();

            this.mTransportLayer.ConnectionChanged += this.ConnectionEventHandler;
        }

        public void InitializeAsServer(int port)
        {
            this.mTransportLayer.Initialize(port);

            // Server only - ignores the initialization usually done by the UDPTransportLayer
            this.connectivityReporter.NetworkInitializingEvent += CurrentContext_NetworkInitializingEvent;
            this.connectivityReporter.SetStatus(ConnectivityStatus.Online);
        }

        private void CurrentContext_NetworkInitializingEvent()
        {
            this.connectivityReporter.SetStatus(ConnectivityStatus.Online);
        }

        public void InitializeAsClient(IPEndPoint serverAddress)
        {
            this.mTransportLayer.ConnectionChanged += this.ClientDisconnectionEventHandler;
            this.mServerAddress = serverAddress;

            this.AttemptReconnectWithServer();
        }

        private void ConnectionEventHandler(object sender, Badumna.Transport.ConnectionEventArgs e)
        {
            if (null != this.ConnectionEvent)
            {
                lock (this.mConnectionEventLock)
                {
                    this.mConnectionEvents.Add(new ConnectionEventArgs(e));
                }
            }
        }

        private void ClientDisconnectionEventHandler(object sender, Badumna.Transport.ConnectionEventArgs e)
        {
            if (!e.IsConnected)
            {
                TcpTransportLayer client = sender as TcpTransportLayer;

                if (null != client)
                {
                    this.eventQueue.Schedule(10000, this.AttemptReconnectWithServer);
                }
            }
        }

        private void AttemptReconnectWithServer()
        {
            // To avoid hairpin issues (can only be run on FullCone or open NATs)
            if (this.mServerAddress.Address.Equals(this.addressProvider.PublicAddress.Address))
            {
                this.mServerAddress = new IPEndPoint(IPAddress.Loopback, this.mServerAddress.Port);
            }

            if (null != this.mDiagnosticProtocol)
            {
                if (this.connectivityReporter.Status == ConnectivityStatus.Online)
                {
                    this.mDiagnosticProtocol.SendStatus(this.mServerAddress);
                }
                this.eventQueue.Schedule(10000, this.AttemptReconnectWithServer);
            }
        }
        public void ProcessMessages()
        {
            this.mTransportLayer.ProcessMessages();

            if (null != this.ConnectionEvent)
            {
                lock (this.mConnectionEventLock)
                {
                    foreach (ConnectionEventArgs e in this.mConnectionEvents)
                    {
                        this.ConnectionEvent.Invoke(this, e);
                    }

                    this.mConnectionEvents.Clear();
                }
            }
        }

        public void ShutDown()
        {
            try
            {
                this.mTransportLayer.ShutDown();
            }
            catch (Exception) { }
        }
    }
}
