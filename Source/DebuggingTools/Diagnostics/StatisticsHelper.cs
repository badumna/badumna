using System;
using System.Collections.Generic;
using System.Text;

using Badumna;
using Badumna.Core;
using Badumna.Utilities;

namespace Diagnostics
{
    public class StatisticsHelper
    {
        public static IStatistics CollectStatistics(object stack, object eventQueue, TimeSpan now)
        {
            Statistics stats = Logger.Statistics as Statistics;

            if (null != stats)
            {
                stats.FinishCollecting(stack as ProtocolStack, eventQueue as NetworkEventQueue, now);
            }

            Statistics newStats = new Statistics();
            newStats.StartCollecting(now);
            Logger.Statistics = newStats;

            return stats;
        }
    }
}
