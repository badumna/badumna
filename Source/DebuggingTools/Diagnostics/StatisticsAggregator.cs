using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.IO;

using Badumna.Core;
using Badumna.Utilities;

namespace Diagnostics
{
    public class StatisticsAggregator : IDisposable
    {
        internal event GenericCallBack<int, double, string, string, string, double> NewStatistic;

        private DataTable mTable;
        public DataTable Table { get { return this.mTable; } }
        private TimeSpan mStartTime;
        private StreamWriter mOutputFile;


        public StatisticsAggregator(TimeSpan startTime)
            : this(null, startTime)
        {
        }

        public StatisticsAggregator(string fileName, TimeSpan startTime)
        {
            this.mStartTime = startTime;

            this.CreateTable();

            if (fileName == null)
            {
                fileName = "";
            }
            fileName = fileName.Trim();


            if (fileName.Length > 0)
            {
                this.mOutputFile = new StreamWriter(fileName, false);

                // Write column headers
                for (int i = 0; i < this.mTable.Columns.Count; i++)
                {
                    if (i < this.mTable.Columns.Count - 1)
                    {
                        this.mOutputFile.Write("\"{0}\",", this.mTable.Columns[i].ColumnName);
                    }
                    else
                    {
                        this.mOutputFile.WriteLine("\"{0}\"", this.mTable.Columns[i].ColumnName);
                    }
                }
            }
        }

        private void CreateTable()
        {
            this.mTable = new DataTable("Peer Statistics");
            this.mTable.Columns.Add("Serial", typeof(int));
            this.mTable.Columns.Add("Time", typeof(double));
            this.mTable.Columns.Add("Peer", typeof(string));
            this.mTable.Columns.Add("Group", typeof(string));
            this.mTable.Columns.Add("Name", typeof(string));
            this.mTable.Columns.Add("Value", typeof(double));
        }

        public void ProcessStatistics(TimeSpan time, int serial, Dictionary<String, IStatistics> peerStatistics)
        {
            double timeOffset = (time - this.mStartTime).TotalSeconds;

            foreach (KeyValuePair<String, IStatistics> data in peerStatistics)
            {
                Statistics stats = data.Value as Statistics;
                if (null != stats)
                {
                    foreach (KeyValuePair<String, Dictionary<String, double>> groupDict in stats.GeneralValues)
                    {
                        foreach (KeyValuePair<String, double> entry in groupDict.Value)
                        {
                            DataRow row = this.mTable.NewRow();

                            row["Serial"] = serial;
                            row["Time"] = timeOffset;
                            row["Peer"] = data.Key;
                            row["Group"] = groupDict.Key;
                            row["Name"] = entry.Key;
                            row["Value"] = entry.Value;

                            this.mTable.Rows.Add(row);

                            if (this.NewStatistic != null)
                            {
                                this.NewStatistic(serial, timeOffset, data.Key, groupDict.Key, entry.Key, entry.Value);
                            }
                        }
                    }
                }
            }
        }

        public void Flush()
        {
            if (this.mOutputFile != null)
            {
                foreach (DataRow row in this.mTable.Rows)
                {
                    for (int i = 0; i < row.ItemArray.Length; i++)
                    {
                        if (i < row.ItemArray.Length - 1)
                        {
                            this.mOutputFile.Write("\"{0}\",", row.ItemArray[i]);
                        }
                        else
                        {
                            this.mOutputFile.WriteLine("{0}", row.ItemArray[i]);
                        }
                    }
                }
            }

            this.mTable.Rows.Clear();
        }

        public void Close()
        {
            this.Dispose();
        }

        public void Dispose()
        {
            if (this.mOutputFile != null)
            {
                this.Flush();
                this.mOutputFile.Close();
                this.mOutputFile = null;
            }
        }
    }
}
