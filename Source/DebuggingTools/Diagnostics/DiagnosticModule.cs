using System.Xml.XPath;

using Badumna.Core;

namespace Diagnostics
{
    class DiagnosticModule
    {
        public PeerAddress ServerAddress { get; private set; }

        public bool IsEnabled
        {
            get
            {
                return this.ServerAddress != null && this.ServerAddress != PeerAddress.Nowhere;
            }
        }

        /// <summary>
        /// Initializes a new instance of the DiagnosticModule class.
        /// </summary>
        /// <param name="xml">The xml containing the configuration options, or null to use the default options.</param>
        public DiagnosticModule(IXPathNavigable xml)
        {
            if (xml != null)
            {
                this.Configure(xml);
            }
        }

        private void Configure(IXPathNavigable navigable)
        {
            XPathNavigator navigator = navigable.CreateNavigator();

            XPathNavigator serverAddressNode = navigator.SelectSingleNode("child::ServerAddress");

            if (null != serverAddressNode)
            {
                this.ServerAddress = PeerAddress.GetAddress(serverAddressNode.InnerXml.Trim()) ?? null;
            }
        }
    }
}
