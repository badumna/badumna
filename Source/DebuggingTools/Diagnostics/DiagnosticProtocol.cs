using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Net;
using System.IO.Compression;

using Badumna;
using Badumna.Core;
using Badumna.Utilities;
using Badumna.Transport;

using Diagnostics.WolfEye;

#if DEBUG
using NUnit.Framework;
#endif

namespace Diagnostics
{
    class VersionRequestArgs : EventArgs
    {
        private IPEndPoint mAddress;
        public IPEndPoint Address
        {
            get { return this.mAddress; }
        }

        // TODO: Do something about the sending of the public address as a string and an IPEndPoint.
        //       Stems from PeerAddress not being exported from Badumna.  Should probably at least
        //       eliminate the string version and just add a NAT type field.
        private String mPublicAddress;
        public String PublicAddress
        {
            get { return this.mPublicAddress; }
        }

        private IPEndPoint mPublicEndPoint;
        public IPEndPoint PublicEndPoint
        {
            get { return this.mPublicEndPoint; }
        }

        private int mProcessID;
        public int ProcessID
        {
            get { return this.mProcessID; }
        }

        private String mApplicationVersion;
        public String ApplicationVersion
        {
            get { return this.mApplicationVersion; }
        }

        public VersionRequestArgs(IPEndPoint diagnosticAddress, String publicAddress, IPEndPoint publicEndPoint, int pid, String applicationVersion)
        {
            this.mAddress = diagnosticAddress;
            this.mPublicAddress = publicAddress;
            this.mPublicEndPoint = new IPEndPoint(publicEndPoint.Address, publicEndPoint.Port); // newed so we pass an IPEndPoint not some derivative class such as PeerAddress which might not be .Equal to an equivalent IPEndPoint.
            this.mProcessID = pid;
            this.mApplicationVersion = applicationVersion;
        }
    }

    public enum ClientStatus { Running, Paused };

    class ClientStatusArgs : EventArgs
    {
        private IPEndPoint mAddress;
        public IPEndPoint Address { get { return this.mAddress; } }

        private ClientStatus mStatus;
        public ClientStatus Status { get { return this.mStatus; } }

        public ClientStatusArgs(IPEndPoint address, ClientStatus status)
        {
            this.mAddress = address;
            this.mStatus = status;
        }
    }

    class StatisticsArgs : EventArgs
    {
        private PeerAddress mAddress;  // Note: This address has the port of the application protocol, not the port of the diagnostic protocol
        public String Address { get { return this.mAddress.ToString(); } }

        public IPEndPoint PublicEndPoint { get { return this.mAddress.EndPoint; } }

        private int mSerial;
        public int Serial { get { return this.mSerial; } }

        private Statistics mStatistics;
        public Statistics Statistics { get { return this.mStatistics; } }

        public StatisticsArgs(PeerAddress address, int serial, Statistics stats)
        {
            this.mAddress = address;
            this.mSerial = serial;
            this.mStatistics = stats;
        }
    }

    class BadumnaNetworkStatusArgs : EventArgs
    {
        public string Status;

        public BadumnaNetworkStatusArgs(string status)
        {
            this.Status = status;
        }
    }

    class DiagnosticProtocol : TransportProtocol, IMessageConsumer<TransportEnvelope>
    {
        private readonly INetworkFacade networkFacade;

        public event EventHandler<VersionRequestArgs> VersionRequestEvent;
        public event EventHandler<ClientStatusArgs> ClientStatusEvent;
        public event EventHandler<BadumnaNetworkStatusArgs> BadumnaNetworkStatusEvent;
        public event EventHandler<EventArgs> RequestShutdownEvent;
        public event EventHandler<StatisticsArgs> StatisticsEvent;

        private Eye mEye;
        public Eye Eye { get { return this.mEye; } }

        private NetworkEventQueue eventQueue;
        private ITime timeKeeper;
        private INetworkAddressProvider addressProvider;

        public DiagnosticProtocol(
            INetworkFacade networkFacade,
            IMessageDispatcher<TransportEnvelope> transport,
            IMessageProducer<TransportEnvelope> messageProducer,
            NetworkEventQueue eventQueue,
            ITime timeKeeper,
            INetworkAddressProvider addressProvider)
            : base("Diagnostic", typeof(DiagnosticProtocolMethodAttribute), t => Activator.CreateInstance(t, true))
        {
            if (networkFacade == null)
            {
                throw new ArgumentNullException("networkFacade");
            }
            this.networkFacade = networkFacade;
            this.addressProvider = addressProvider;

            messageProducer.MessageConsumer = this;
            this.DispatcherMethod = transport.SendMessage;
            this.eventQueue = eventQueue;
            this.timeKeeper = timeKeeper;

            InspectorRoot root = new InspectorRoot();
            root.Facade = (NetworkFacade)this.networkFacade;
            root.Queue = this.eventQueue;
            this.mEye = new Eye(root, this);
            this.Parser.RegisterMethodsIn(this.mEye);
        }

        private void SendFile(String fileName)
        {
            try
            {
                using (MemoryStream compressedInput = new MemoryStream())
                {
                    byte[] buffer = new byte[32768];
                    int bytesRead;

                    using (FileStream input = new FileStream(fileName, FileMode.Open, FileAccess.Read))
                    using (GZipStream compresser = new GZipStream(compressedInput, CompressionMode.Compress, true))
                    {
                        while ((bytesRead = input.Read(buffer, 0, buffer.Length)) > 0)
                        {
                            compresser.Write(buffer, 0, bytesRead);
                        }
                    }

                    compressedInput.Position = 0;
                    buffer = new byte[500];
                    while ((bytesRead = compressedInput.Read(buffer, 0, buffer.Length)) > 0)
                    {
                        TransportEnvelope envelope = this.GetMessageFor(this.CurrentEnvelope.Source, QualityOfService.Reliable);

                        byte[] sendBuffer = buffer;
                        if (bytesRead < buffer.Length)
                        {
                            sendBuffer = new byte[bytesRead];
                            Array.Copy(buffer, sendBuffer, bytesRead);
                        }
                        this.RemoteCall(envelope, this.ReceiveLogFile, Path.GetFileName(fileName) + ".gz", sendBuffer);
                        this.SendMessage(envelope);

                    }
                }
            }
            catch (IOException)
            {
            }
        }


        [DiagnosticProtocolMethod(DiagnosticMethod.ReceiveLogFile)]
        private void ReceiveLogFile(String logFileName, byte[] logData)
        {
            if (!Directory.Exists("logs"))
            {
                Directory.CreateDirectory("logs");
            }

            using (FileStream output = new FileStream(Path.Combine("logs", Path.GetFileName(logFileName)), FileMode.Append))
            {
                output.Write(logData, 0, logData.Length);
            }
        }


        public void MakePauseRequest(IPEndPoint address, bool pause)
        {
            TransportEnvelope envelope = this.GetMessageFor(new PeerAddress(address, NatType.Unknown), QualityOfService.Reliable);

            byte pauseByte = 0x00;
            if (pause)
            {
                pauseByte = 0x01;
            }

            this.RemoteCall(envelope, this.Pause, pauseByte);
            this.SendMessage(envelope);
        }

        [DiagnosticProtocolMethod(DiagnosticMethod.Pause)]
        public void Pause(byte pause)
        {
            Logger.TraceInformation(LogTag.OldStyle, "Received request to{0}pause event queue.", pause == 0x00 ? " un" : " ");
            this.eventQueue.IsPaused = (pause == 0x01);
            this.SendStatus();
        }

        public void RequestStatus(IPEndPoint address)
        {
            TransportEnvelope envelope = this.GetMessageFor(new PeerAddress(address, NatType.Unknown), QualityOfService.Reliable);
            this.RemoteCall(envelope, this.SendStatus);
            this.SendMessage(envelope);
        }

        [DiagnosticProtocolMethod(DiagnosticMethod.SendStatus)]
        private void SendStatus()
        {
            this.SendStatus(this.CurrentEnvelope.Source.EndPoint);
        }

        public void SendStatus(IPEndPoint destination)
        {
            ClientStatus status = ClientStatus.Running;

            if (this.eventQueue.IsPaused)
            {
                status = ClientStatus.Paused;
            }

            TransportEnvelope envelope = this.GetMessageFor(new PeerAddress(destination, NatType.Unknown), QualityOfService.Reliable);
            this.RemoteCall(envelope, this.ReceiveStatus, (int)status);
            this.SendMessage(envelope);
        }

        [DiagnosticProtocolMethod(DiagnosticMethod.ReceiveStatus)]
        private void ReceiveStatus(int intStatus)
        {
            if (this.ClientStatusEvent != null)
            {
                this.ClientStatusEvent.Invoke(this, new ClientStatusArgs(this.CurrentEnvelope.Source.EndPoint, (ClientStatus)intStatus));
            }
        }

        public void RequestBadumnaNetworkStatus(IPEndPoint address)
        {
            TransportEnvelope envelope = this.GetMessageFor(new PeerAddress(address, NatType.Unknown), QualityOfService.Reliable);
            this.RemoteCall(envelope, this.SendBadumnaNetworkStatus);
            this.SendMessage(envelope);
        }

        [DiagnosticProtocolMethod(DiagnosticMethod.SendBadumnaNetworkStatus)]
        private void SendBadumnaNetworkStatus()
        {
            TransportEnvelope envelope = this.GetMessageFor(this.CurrentEnvelope.Source, QualityOfService.Reliable);
            string status = this.networkFacade.GetNetworkStatus().ToString();
            this.RemoteCall(envelope, this.ReceiveBadumnaNetworkStatus, new CompressedString(status));
            this.SendMessage(envelope);
        }

        [DiagnosticProtocolMethod(DiagnosticMethod.ReceiveBadumnaNetworkStatus)]
        private void ReceiveBadumnaNetworkStatus(CompressedString status)
        {
            EventHandler<BadumnaNetworkStatusArgs> handler = this.BadumnaNetworkStatusEvent;
            if (handler != null)
            {
                handler(this, new BadumnaNetworkStatusArgs(status.Content));
            }
        }

        public void RequestStatistics(IPEndPoint address, String experimentName, int serial)
        {
            TransportEnvelope envelope = this.GetMessageFor(new PeerAddress(address, NatType.Unknown), QualityOfService.Reliable);
            this.RemoteCall(envelope, this.RequestStatistics, experimentName, serial);
            this.SendMessage(envelope);
        }

        [DiagnosticProtocolMethod(DiagnosticMethod.RequestStatistics)]
        private void RequestStatistics(String experimentName, int serial)
        {
            Statistics stats = null;
            MainNetworkFacade facade = this.networkFacade as MainNetworkFacade;
            if (facade != null)
            {
                stats = (Statistics)StatisticsHelper.CollectStatistics(facade.Stack, this.eventQueue, this.timeKeeper.Now);
            }

            TransportEnvelope envelope = this.GetMessageFor(this.CurrentEnvelope.Source, QualityOfService.Reliable);
            this.RemoteCall(envelope, this.ReceiveStatistics,
                this.addressProvider.PublicAddress, serial, stats ?? new Statistics());
            this.SendMessage(envelope);
        }

        [DiagnosticProtocolMethod(DiagnosticMethod.ReceiveStatistics)]
        private void ReceiveStatistics(PeerAddress peer, int serial, Statistics stats)
        {
            EventHandler<StatisticsArgs> handler = this.StatisticsEvent;
            if (handler != null)
            {
                handler(this, new StatisticsArgs(peer, serial, stats));
            }
        }

        [DiagnosticProtocolMethod(DiagnosticMethod.HandleVersionRequest)]
        protected void HandleVersionRequest()
        {
            System.Diagnostics.Process proc = System.Diagnostics.Process.GetCurrentProcess();
            int pid = proc.Id;
            proc.Close();

            TransportEnvelope envelope = this.GetMessageFor(this.CurrentEnvelope.Source, QualityOfService.Reliable);
            this.RemoteCall(envelope, this.HandleVersionReply, this.addressProvider.PublicAddress, pid,
                "{version string no longer used}");
            this.SendMessage(envelope);
        }

        [DiagnosticProtocolMethod(DiagnosticMethod.HandleVersionReply)]
        protected void HandleVersionReply(PeerAddress publicAddress, int pid, String applicationVersion)
        {
            if (null != VersionRequestEvent)
            {
                VersionRequestArgs args = new VersionRequestArgs(this.CurrentEnvelope.Source.EndPoint, publicAddress.ToString(), publicAddress.EndPoint, pid, applicationVersion);

                this.VersionRequestEvent.Invoke(this, args);
            }
        }

        public void RequestVersion(IPEndPoint address)
        {
            TransportEnvelope envelope = this.GetMessageFor(new PeerAddress(address, NatType.Unknown), QualityOfService.Reliable);

            this.RemoteCall(envelope, this.HandleVersionRequest);
            this.SendMessage(envelope);
        }

        [DiagnosticProtocolMethod(DiagnosticMethod.Shutdown)]
        private void Shutdown()
        {
            if (this.RequestShutdownEvent != null)
            {
                this.RequestShutdownEvent(this, null);
            }
        }

        public void RequestShutdown(IPEndPoint address)
        {
            TransportEnvelope envelope = this.GetMessageFor(new PeerAddress(address, NatType.Unknown), QualityOfService.Reliable);

            this.RemoteCall(envelope, this.Shutdown);
            this.SendMessage(envelope);
        }

        #region IMessageConsumer<TransportEnvelope> Members

        public void ProcessMessage(TransportEnvelope envelope)
        {
            this.ParseMessage(envelope, 0);
        }

        #endregion
    }



#if DEBUG
    #region Unit tests.

    /*
    [TestFixture]
    public class DiagnosticProtocolTester
    {
        private NetworkContext mSourceContext;
        private NetworkContext mDestinationContext;

        private MockTransportLayer mTransportSource;
        private MockTransportLayer mTransportDestination;
        
        private DiagnosticProtocol mDiagnosticSource;
        private DiagnosticProtocol mDiagnosticDestination;

        private IPEndPoint mVersionRequestSource;
        private String mVersionRequestString;

        [SetUp]
        public void Initialize()
        {
            NetworkEventQueue.Instance.Reset();

            this.mSourceContext = new NetworkContext();
            this.mDestinationContext = new NetworkContext();

            this.mSourceContext.PublicAddress = PeerAddress.GetAddress("127.0.0.1", 0);
            this.mDestinationContext.PublicAddress = PeerAddress.GetAddress("127.0.0.2", 0);

            this.mTransportSource = new MockTransportLayer(this.mSourceContext);
            this.mTransportDestination = new MockTransportLayer(this.mDestinationContext);

            this.mTransportSource.AddLink(this.mTransportDestination, 10, 1.0, 64);
            this.mTransportDestination.AddLink(this.mTransportSource, 10, 1.0, 64);

            this.mDiagnosticSource = new DiagnosticProtocol(this.mTransportSource);
            this.mDiagnosticDestination = new DiagnosticProtocol(this.mTransportDestination);

            this.mVersionRequestSource = PeerAddress.Nowhere;
            this.mVersionRequestString = String.Empty;

            this.mDiagnosticSource.VersionRequestEvent += this.VersionRequestHanlder;
        }

        [TearDown]
        public void Terminate() 
        {
            NetworkEventQueue.Instance.Reset();
        }

        private void VersionRequestHanlder(Object sender, EventArgs e)
        {
            VersionRequestArgs args = e as VersionRequestArgs;

            if (null != args)
            {
                this.mVersionRequestSource = args.Address;
                this.mVersionRequestString = args.ApplicationVersion;
            }
        }

        [Test]
        public void GetVersionTest()
        {            
            this.mDiagnosticSource.RequestVersion(this.mDestinationContext.PublicAddress);

            NetworkEventQueue.Instance.RunAll();

            // Test that the correct version string is passed in reply.
            Assert.AreEqual(this.mDestinationContext.PublicAddress, this.mVersionRequestSource);
            Assert.AreEqual("Unknown", this.mVersionRequestString); // Unknown is default version string.
        }
    }
    */

    #endregion
#endif // DEBUG
}
