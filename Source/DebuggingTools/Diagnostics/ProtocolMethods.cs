﻿using System;
using System.Collections.Generic;
using System.Text;
using Badumna.Core;

namespace Diagnostics
{
    class DiagnosticProtocolMethodAttribute : ProtocolMethodAttribute
    {
        public DiagnosticProtocolMethodAttribute(DiagnosticMethod method)
            : base((int)method)
        {
        }
    }

    internal enum DiagnosticMethod
    {
        SendLogFile,
        ReceiveLogFile,
        Pause,
        SendStatus,
        ReceiveStatus,
        SendBadumnaNetworkStatus,
        ReceiveBadumnaNetworkStatus,
        RequestStatistics,
        ReceiveStatistics,
        CheckForUpdate,
        HandleVersionRequest,
        HandleVersionReply,
        ProcessRemoteException,
        Shutdown,

        EyeRequestObject,
        EyeRequestRunTimeType,
        EyeRequestKeys,
        EyeRequestLength,
        EyeReturnResult,
        EyeCreateFieldList,
        EyeRetrieveFields,
    }
}
