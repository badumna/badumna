﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Text.RegularExpressions;

namespace TheWolf
{
    class TypeName
    {
        public string Name { get; set; }

        public TypeName[] TypeParameters { get; set; }

        public string AssemblyName { get; set; }

        public string AssemblyProperties { get; set; }


        public TypeName(string fullTypeName)
        {
            List<string> parts = TypeName.SplitByCommas(fullTypeName);

            if (parts.Count > 0)
            {
                this.Name = parts[0].Trim();
                parts.RemoveAt(0);
            }
            if (parts.Count > 0)
            {
                this.AssemblyName = parts[0].Trim();
                parts.RemoveAt(0);
            }
            if (parts.Count > 0)
            {
                this.AssemblyProperties = string.Join(",", parts.ToArray()).Trim();
            }

            this.ExtractGenericTypeParameters();
        }

        /// <summary>
        /// Splits up the input string at commas, paying attention to
        /// escape characters and blocks surrounded by square brackets.
        /// </summary>
        private static List<string> SplitByCommas(string input)
        {
            List<string> result = new List<string>();

            using (StringReader reader = new StringReader(input))
            {
                StringBuilder part = new StringBuilder();

                bool escapeNext = false;
                int depth = 0;
                int next;
                while ((next = reader.Read()) != -1)
                {
                    if (escapeNext)
                    {
                        escapeNext = false;
                        part.Append((char)next);
                        continue;
                    }

                    switch ((char)next)
                    {
                        case '\\':
                            escapeNext = true;
                            break;

                        case '[':
                            depth++;
                            break;

                        case ']':
                            depth--;
                            break;

                        case ',':
                            if (depth == 0)
                            {
                                result.Add(part.ToString());
                                part = new StringBuilder();
                                continue;
                            }
                            break;
                    }

                    part.Append((char)next);
                }

                result.Add(part.ToString());
            }

            return result;
        }

        private void ExtractGenericTypeParameters()
        {
            if (this.Name == null)
            {
                return;
            }

            using (StringReader reader = new StringReader(this.Name))
            {
                StringBuilder name = new StringBuilder();

                bool done = false;
                bool escapeNext = false;
                int next = 0;
                while (!done && (next = reader.Read()) != -1)
                {
                    if (escapeNext)
                    {
                        escapeNext = false;
                    }
                    else
                    {
                        switch ((char)next)
                        {
                            case '\\':
                                escapeNext = true;
                                break;

                            case '[':
                                done = true;
                                continue;
                        }
                    }

                    name.Append((char)next);
                }

                if ((char)next == '[' && (char)reader.Peek() == '[')
                {
                    string typeParams = reader.ReadToEnd();
                    typeParams = typeParams.Remove(typeParams.Length - 1, 1);  // Remove trailing ']'

                    List<string> types = TypeName.SplitByCommas(typeParams);
                    List<TypeName> parsedNames = new List<TypeName>();

                    foreach (string type in types)
                    {
                        string t = type.Remove(0, 1); // Initial '['
                        t = t.Remove(t.Length - 1, 1); // Final ']'
                        parsedNames.Add(new TypeName(t));
                    }

                    this.Name = name.ToString();
                    this.TypeParameters = parsedNames.ToArray();
                }
            }
        }

        public override string ToString()
        {
            string typeName = this.Name;
            if (typeName != null && this.TypeParameters != null && this.TypeParameters.Length > 0)
            {
                typeName += "[";

                for (int i = 0; i < this.TypeParameters.Length; i++)
                {
                    if (i > 0)
                    {
                        typeName += ",";
                    }
                    typeName += "[" + this.TypeParameters[i].ToString() + "]";
                }

                typeName += "]";
            }

            return string.Join(", ", new string[] { typeName, this.AssemblyName, this.AssemblyProperties });
        }
    }
}
