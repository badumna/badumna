﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace TheWolf
{
    class TwoWayMap<A, B>
    {
        protected Dictionary<A, B> mForward = new Dictionary<A, B>();
        protected Dictionary<B, A> mBackward = new Dictionary<B, A>();

        public B this[A index]
        {
            get { return this.mForward[index]; }
            set
            {
                this.mForward.Add(index, value);  // Add - because we don't support updating of mappings, and to ensure a 1:1 mapping
                this.mBackward.Add(value, index);
            }
        }

        public bool TryGetValue(A key, out B value)
        {
            return this.mForward.TryGetValue(key, out value);
        }

        public bool TryGetValue(B key, out A value)
        {
            return this.mBackward.TryGetValue(key, out value);
        }
    }

    class TwoWayMap<A> : TwoWayMap<A, A>
    {
        public enum Direction
        {
            Forward,
            Backward
        }

        public bool TryGetValue(A key, out A value, Direction direction)
        {
            switch (direction)
            {
                case Direction.Forward:
                    return this.mForward.TryGetValue(key, out value);

                case Direction.Backward:
                    return this.mBackward.TryGetValue(key, out value);

                default:
                    throw new ArgumentOutOfRangeException("direction");
            }
        }
    }
}
