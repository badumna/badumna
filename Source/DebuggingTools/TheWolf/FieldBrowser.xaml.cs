﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net;
using System.Collections.ObjectModel;

namespace TheWolf
{
    partial class FieldBrowser : UserControl
    {
        private ObservableCollection<ListTreeNode> mItems = new ObservableCollection<ListTreeNode>();


        public FieldBrowser()
        {
            InitializeComponent();

            this.ListView.ItemsSource = this.mItems;
        }

        public void Add(PeerNode peerNode)
        {
            if (!this.CheckAccess())
            {
                this.Dispatcher.BeginInvoke(new Action<PeerNode>(this.Add), peerNode);
                return;
            }

            this.mItems.Add(new ListTreeNode(this.mItems, 0, peerNode));
        }

        public void Remove(PeerNode peerNode)
        {
            if (!this.CheckAccess())
            {
                this.Dispatcher.BeginInvoke(new Action<PeerNode>(this.Remove), peerNode);
                return;
            }

            for (int i = 0; i < this.mItems.Count; i++)
            {
                if (this.mItems[i].Level == 0 && this.mItems[i].Node == peerNode)
                {
                    this.mItems.RemoveAt(i);
                    break;
                }
            }
        }

        private void Update_Click(object sender, RoutedEventArgs e)
        {
            ListTreeNode listTreeNode = this.ListView.SelectedItem as ListTreeNode;
            if (listTreeNode != null)
            {
                listTreeNode.Node.Get();
            }
        }

        private void AddWatch_Click(object sender, RoutedEventArgs e)
        {
            ListTreeNode listTreeNode = this.ListView.SelectedItem as ListTreeNode;
            if (listTreeNode != null)
            {
                Commands.AddWatchCommand.Execute(
                    new AddWatchParameter
                    {
                        Path = listTreeNode.Node.ObjectPath,
                        Name = listTreeNode.Node.Name
                    },
                    this);
            }
        }

        private void CopyPath_Click(object sender, RoutedEventArgs e)
        {
            ListTreeNode listTreeNode = this.ListView.SelectedItem as ListTreeNode;
            if (listTreeNode != null)
            {
                Clipboard.SetText(listTreeNode.Node.ObjectPath.Path.ToString());
            }
        }
    }
}
