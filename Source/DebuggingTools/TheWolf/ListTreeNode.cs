﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.ComponentModel;
using System.Collections.Specialized;
using System.Collections.ObjectModel;


namespace TheWolf
{
    class ListTreeNode : INotifyPropertyChanged, IDisposable
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private int mLevel;
        public int Level { get { return this.mLevel; } }

        public int Indentation { get { return this.mLevel * 20; } }


        private bool mIsExpanded;
        public bool IsExpanded
        {
            get { return this.mIsExpanded; }
            set
            {
                if (this.mIsExpanded != value)
                {
                    this.mIsExpanded = value;

                    if (value)
                    {
                        this.Expand();
                    }
                    else
                    {
                        this.Collapse();
                    }

                    this.OnPropertyChanged("IsExpanded");
                }
            }
        }

        private Node mNode;
        public Node Node { get { return this.mNode; } }

        
        private ObservableCollection<ListTreeNode> mItems;


        public ListTreeNode(ObservableCollection<ListTreeNode> items, int level, Node node)
        {
            this.mItems = items;
            this.mLevel = level;
            this.mNode = node;

            this.mNode.Children.CollectionChanged += NodeChildrenChanged;
        }

        public void Dispose()
        {
            this.mNode.Children.CollectionChanged -= NodeChildrenChanged;
        }

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private void NodeChildrenChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (this.IsExpanded)
            {
                if (e.Action == NotifyCollectionChangedAction.Reset)
                {
                    this.RemoveAllChildren();
                }
                else
                {
                    if (e.OldItems != null)
                    {
                        foreach (Node node in e.OldItems)
                        {
                            this.RemoveChild(node);
                        }
                    }

                    if (e.NewItems != null)
                    {
                        foreach (Node node in e.NewItems)
                        {
                            this.InsertChild(node);
                        }
                    }
                }
            }
        }

        private int GetMyIndex()
        {
            return this.mItems.IndexOf(this);
        }

        private void InsertChild(Node node)
        {
            ListTreeNode newListTreeNode = new ListTreeNode(this.mItems, this.Level + 1, node);
            this.mItems.Insert(this.GetMyIndex() + 1, newListTreeNode);
        }

        private void RemoveChild(Node node)
        {
            for (int i = this.GetMyIndex() + 1; i < this.mItems.Count; i++)
            {
                ListTreeNode listTreeNode = this.mItems[i];
                if (listTreeNode.Level <= this.Level)
                {
                    break;
                }

                if (listTreeNode.Level == this.Level + 1 && listTreeNode.mNode == node)
                {
                    listTreeNode.RemoveAllChildren();  // Can only affect higher indexes, so our index is still ok
                    this.mItems.RemoveAt(i);
                }
            }
        }

        private void RemoveAllChildren()
        {
            for (int i = this.GetMyIndex() + 1; i < this.mItems.Count;)
            {
                ListTreeNode listTreeNode = this.mItems[i];
                if (listTreeNode.Level <= this.Level)
                {
                    break;
                }

                listTreeNode.RemoveAllChildren();  // Can only affect higher indexes, so our index is still ok
                this.mItems.RemoveAt(i);  // After removing this, there'll be a new node at i, so no need to increment i
            }
        }

        private void Expand()
        {
            foreach (Node node in this.mNode.Children)
            {
                this.InsertChild(node);
            }
            this.mNode.PopulateChildren();
        }

        private void Collapse()
        {
            this.RemoveAllChildren();
        }
    }
}
