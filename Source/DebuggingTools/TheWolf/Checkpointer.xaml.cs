﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

using Microsoft.Win32;

using Diagnostics.WolfEye;
using System.IO;
using System.Net;


namespace TheWolf
{
    /// <summary>
    /// Interaction logic for Checkpointer.xaml
    /// </summary>
    public partial class Checkpointer : UserControl
    {
        private int mLastFieldListId;
        private List<ObjectPath> mLastFieldList;

        private StreamWriter mCheckpointFile;
        private DispatcherTimer mCheckpointTimer;


        public Checkpointer()
        {
            InitializeComponent();

            MainWindow.Current.NewPeer += this.CreateList;
        }

        private void CreateList_Click(object sender, RoutedEventArgs e)
        {
            List<ObjectPath> fields = new List<ObjectPath>();
            foreach (string line in this.FieldList.Text.Split(new char[] { '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries))
            {
                string pathString = line.Trim();

                if (pathString.StartsWith("--"))  // Comment line
                {
                    continue;
                }

                ObjectPath path = ObjectPath.FromString(pathString);
                if (path == null)
                {
                    MessageBox.Show("Couldn't parse path string: " + pathString);
                    return;
                }
                fields.Add(path);
            }

            this.mLastFieldListId++;
            this.mLastFieldList = fields;

            MainWindow.Current.OnAllPeers(this.CreateList);
        }

        private void CreateList(PeerNode node)
        {
            if (this.mLastFieldList == null)
            {
                return;
            }

            MainWindow.Current.Diagnostics.Protocol.Eye.CreateFieldList(
                node.DiagnosticsEndPoint,
                this.mLastFieldListId,
                this.mLastFieldList,
                delegate(object returnValue, Exception maybeException)
                {
                    if (maybeException != null)
                    {
                        throw maybeException;  // TODO: Probably not the best idea
                    }
                });
        }

        private void RetrieveList_Click(object sender, RoutedEventArgs e)
        {
            MainWindow.Current.OnAllPeers(
                delegate(PeerNode node)
                {
                    this.CheckpointPeer(node);
                });
        }

        private void SelectFile_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog dialog = new SaveFileDialog();
            if (dialog.ShowDialog() == true)
            {
                this.mCheckpointFile = new StreamWriter(new FileStream(dialog.FileName, FileMode.Create, FileAccess.Write, FileShare.Read));
            }
        }

        private void CloseFile_Click(object sender, RoutedEventArgs e)
        {
            if (this.mCheckpointFile != null)
            {
                this.mCheckpointFile.Close();
                this.mCheckpointFile = null;
            }
        }

        private void StartCheckpointing_Click(object sender, RoutedEventArgs e)
        {
            this.StopCheckpointing_Click(this, null);
            this.mCheckpointTimer = new DispatcherTimer { Interval = TimeSpan.FromSeconds(int.Parse(this.Frequency.Text)) };
            this.mCheckpointTimer.Tick += this.CheckpointTimer_Tick;
            this.mCheckpointTimer.Start();
        }

        private void CheckpointPeer(PeerNode node)
        {
            int id = this.mLastFieldListId;
            if (id < 0)
            {
                return;
            }

            MainWindow.Current.Diagnostics.Protocol.Eye.RetrieveFields(
                node.DiagnosticsEndPoint,
                id,
                delegate(object returnValue, Exception maybeException)
                {
                    if (maybeException != null)
                    {
                        throw maybeException;  // TODO: Probably not the best idea;  Actually, processing of the exception should happen somehow as part of the callback mechanism if possible?
                    }

                    this.DumpCheckpoint(returnValue as List<object>, node.PublicEndPoint);
                });
        }

        private void DumpCheckpoint(List<object> result, IPEndPoint peerAddress)
        {
            if (result == null)
            {
                return;
            }

            this.LogLine(String.Format("{0},{1},{2}", (int)DateTime.Now.TimeOfDay.TotalMilliseconds, peerAddress, result.Count));
            foreach (List<object> objectList in result)
            {
                var objs = from obj in objectList select obj.ToString();
                this.LogLine(String.Join(",", objs.ToArray()));
            }
        }

        private void LogLine(string line)
        {
            //this.Results.AppendText(line);
            //this.Results.AppendText("\n");
            if (this.mCheckpointFile != null)
            {
                this.mCheckpointFile.WriteLine(line);
            }
        }

        private void CheckpointTimer_Tick(object sender, EventArgs e)
        {
            MainWindow.Current.OnAllPeers(
                delegate(PeerNode node)
                {
                    this.CheckpointPeer(node);
                });
        }

        private void StopCheckpointing_Click(object sender, RoutedEventArgs e)
        {
            if (this.mCheckpointTimer != null)
            {
                this.mCheckpointTimer.Stop();
                this.mCheckpointTimer = null;
            }
        }

        private void Clear_Click(object sender, RoutedEventArgs e)
        {
            this.Results.Clear();
        }
    }
}
