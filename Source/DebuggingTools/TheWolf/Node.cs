﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using System.Collections;
using System.Net;
using System.ComponentModel;

using Diagnostics.WolfEye;
using Diagnostics;
using System.Reflection;


namespace TheWolf
{
    class Node : INotifyPropertyChanged
    {
        static Node EmptyNode = new Node(null, "...", null);


        public event PropertyChangedEventHandler PropertyChanged;

        protected PeerQualifiedPath mObjectPath;
        public PeerQualifiedPath ObjectPath { get { return this.mObjectPath; } }

        public ObservableCollection<Node> Children { get; private set; }

        private string mName;
        public string Name
        {
            get { return this.mName; }

            protected set
            {
                if (this.mName != value)
                {
                    this.mName = value;
                    this.OnPropertyChanged("Name");
                }
            }
        }

        private string mCompileTimeTypeName;
        public string CompileTimeTypeName
        {
            get { return this.mCompileTimeTypeName; }

            protected set
            {
                this.mCompileTimeTypeName = value;
                this.OnPropertyChanged("CompileTimeTypeName");
            }
        }


        private Type mCompileTimeType;
        public Type CompileTimeType
        {
            get { return this.mCompileTimeType; }

            protected set
            {
                this.mCompileTimeType = value;
                if (value != null)
                {
                    this.CompileTimeTypeName = DotfuscatorMap.Current.OriginalTypeName(value.AssemblyQualifiedName);
                }
                else
                {
                    this.CompileTimeTypeName = "";
                }
                this.OnPropertyChanged("CompileTimeType");
            }
        }

        private string mValue;
        public string Value
        {
            get { return this.mValue; }

            protected set
            {
                if (this.mValue != value)
                {
                    this.mValue = value;
                    this.OnPropertyChanged("Value");
                }
            }
        }

        private Type mRunTimeType;
        public Type RunTimeType
        {
            get { return this.mRunTimeType; }

            protected set
            {
                this.mRunTimeType = value;
                this.OnPropertyChanged("RunTimeType");
            }
        }


        public Node(PeerQualifiedPath objectPath, string name, Type compileTimeType)
        {
            this.Children = new ObservableCollection<Node>();
            if (Node.EmptyNode != null)  // Only null during construction of EmptyNode
            {
                this.Children.Add(Node.EmptyNode);
            }

            this.mObjectPath = objectPath;

            this.Name = name;
            this.CompileTimeType = compileTimeType;
        }

        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public void Get()
        {
            if (this.mObjectPath == null)
            {
                return;
            }

            // TODO: This can no longer detect if RequestObject failed due to inability to serialize
            this.mObjectPath.Diagnostics.Eye.RequestObject(this.mObjectPath.DiagnosticEndPoint, this.mObjectPath.Path,
                delegate(object obj, Exception maybeException)
                {
                    if (maybeException != null)
                    {
                        throw maybeException;  // TODO: Probably not a good idea...
                    }

                    if (obj != null)
                    {
                        this.Value = obj.ToString();
                        this.RunTimeType = obj.GetType();
                    }
                    else
                    {
                        this.Value = "(null)";
                        this.RunTimeType = null;
                    }
                });
        }

        public virtual void PopulateChildren()
        {
        }

        public override string ToString()
        {
            return this.Name;
        }
    }

    class PeerNode : Node
    {
        public IPEndPoint PublicEndPoint { get; private set; }

        private IPEndPoint mDiagnosticEndPoint;
        public IPEndPoint DiagnosticsEndPoint { get { return this.mDiagnosticEndPoint; } }

        private DiagnosticProtocol mDiagnostics;


        public PeerNode(IPEndPoint endPoint, DiagnosticProtocol diagnostics, IPEndPoint diagnosticEndpoint)
            : base(new PeerQualifiedPath(endPoint, diagnostics, diagnosticEndpoint, null), endPoint.ToString(), null)
        {
            this.PublicEndPoint = endPoint;

            this.mDiagnostics = diagnostics;
            this.mDiagnosticEndPoint = diagnosticEndpoint;
        }

        public override void PopulateChildren()
        {
            FieldNode rootField = new FieldNode(
                new PeerQualifiedPath(this.PublicEndPoint, this.mDiagnostics, this.mDiagnosticEndPoint, new ObjectPath()),
                "", typeof(object));
            rootField.PopulateChildren(delegate
            {
                this.Children.Clear();
                foreach (Node node in rootField.Children)
                {
                    this.Children.Add(node);
                }
            });
        }
    }

    class LeafNode : Node
    {
        public LeafNode(PeerQualifiedPath objectPath, string name, Type compileTimeType)
            : base(objectPath, name, compileTimeType)
        {
            this.Children.Clear();
            this.Get();
        }

        public override void PopulateChildren()
        {
        }
    }

    class DictionaryNode : Node
    {
        public DictionaryNode(PeerQualifiedPath objectPath, string name, Type compileTimeType)
            : base(objectPath, name, compileTimeType)
        {
        }

        public override void PopulateChildren()
        {
            // TODO: This can no longer detect if RequestKeys failed due to inability to serialize

            this.mObjectPath.Diagnostics.Eye.RequestKeys(this.mObjectPath.DiagnosticEndPoint, this.mObjectPath.Path,
                delegate(object keys, Exception maybeException)
                {
                    if (maybeException != null)
                    {
                        throw maybeException;  // TODO: Probably not a good idea...
                    }

                    this.Children.Clear();

                    foreach (object key in (List<object>)keys)
                    {
                        // TODO: Make ObjectPath classes immutable?
                        PeerQualifiedPath valuePath = new PeerQualifiedPath(this.mObjectPath);
                        valuePath.Path.Add(new IndexerPath(key));
                        this.Children.Add(new FieldNode(valuePath, "[" + key.ToString() + "]", typeof(object)));  // TODO: Correct type
                    }
                });
        }
    }

    class ListNode : Node
    {
        public ListNode(PeerQualifiedPath objectPath, string name, Type compileTimeType)
            : base(objectPath, name, compileTimeType)
        {
        }

        public override void PopulateChildren()
        {
            // TODO: This can no longer detect if RequestKeys failed due to inability to serialize

            this.mObjectPath.Diagnostics.Eye.RequestLength(this.mObjectPath.DiagnosticEndPoint, this.mObjectPath.Path,
                delegate(object count, Exception maybeException)
                {
                    if (maybeException != null)
                    {
                        throw maybeException;  // TODO: Probably not a good idea...
                    }

                    this.Children.Clear();

                    int intCount = Math.Min((int)count, 20); // TODO: Do something better :)
                    for (int i = 0; i < intCount; i++)
                    {
                        PeerQualifiedPath valuePath = new PeerQualifiedPath(this.mObjectPath);
                        valuePath.Path.Add(new ArrayPath(i));
                        this.Children.Add(new FieldNode(valuePath, "[" + i.ToString() + "]", typeof(object))); // TODO: Correct type
                    }
                });
        }
    }

    class FieldNode : Node
    {
        private static List<Type> BaseTypes = new List<Type>
        {
            typeof(sbyte),
            typeof(short),
            typeof(int),
            typeof(long),
            typeof(ushort),
            typeof(byte),
            typeof(uint),
            typeof(ulong),
            typeof(decimal),
            typeof(float),
            typeof(double),
            typeof(bool),
            typeof(char),
            typeof(string)
        };


        public FieldNode(PeerQualifiedPath objectPath, string name, Type compileTimeType)
            : base(objectPath, name, compileTimeType)
        {
        }


        public override void PopulateChildren()
        {
            this.PopulateChildren(null);
        }

        public void PopulateChildren(Action onComplete)
        {
            this.mObjectPath.Diagnostics.Eye.RequestRunTimeType(this.mObjectPath.DiagnosticEndPoint, this.mObjectPath.Path,
                delegate(object remoteTypeName, Exception maybeException)
                {
                    if (maybeException != null)
                    {
                        throw maybeException;  // TODO: Probably not a good idea...
                    }

                    this.Children.Clear();

                    if (remoteTypeName == null)
                    {
                        // Implies object at this path is null
                        if (onComplete != null)
                        {
                            onComplete();
                        }
                        return;
                    }

                    // TODO: This needs some attention, it's a bit ugly.  Also, it will only work
                    //       if the first obfuscated name encountered is a type.  If a type is
                    //       encountered which has obfuscated field names but not an obfuscated type
                    //       name, then those names won't be unobfuscated.

                    TypeName remoteType = new TypeName((string)remoteTypeName);

                    string originalTypeName = (string)remoteTypeName;

                    if (DotfuscatorMap.IsObfuscatedName(remoteType.Name))
                    {
                        originalTypeName = DotfuscatorMap.Current.OriginalTypeName((string)remoteTypeName);
                        if (originalTypeName == (string)remoteTypeName)
                        {
                            // Name seems to be obfuscated, but translating it gave us the same thing back,
                            // try load the missing mapping.
                            SelectMapDialog selectMapDialog = new SelectMapDialog();
                            selectMapDialog.AssemblyName = remoteType.AssemblyName + ", " + remoteType.AssemblyProperties;
                            if (selectMapDialog.ShowDialog() == true)
                            {
                                DotfuscatorMap.Current.AddMappings(selectMapDialog.MapPath);
                                originalTypeName = DotfuscatorMap.Current.OriginalTypeName((string)remoteTypeName);
                            }
                        }
                    }


                    // Initially, assume we can load the type directly
                    Type type = Type.GetType((string)remoteTypeName);

                    if (type == null && originalTypeName != (string)remoteTypeName)
                    {
                        // Could be that the remote type is obfuscated but the matching local one isn't
                        type = Type.GetType(originalTypeName);
                    }


                    if (type == null)
                    {
                        // TODO: Do something...
                    }

                    BindingFlags searchFlags = BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic |
                                               BindingFlags.DeclaredOnly;

                    for (; type != null; type = type.BaseType)
                    {
                        foreach (KeyValuePair<string, Type> field in DotfuscatorMap.GetFieldList(type, searchFlags))
                        {
                            PeerQualifiedPath newPath = new PeerQualifiedPath(this.mObjectPath);

                            remoteTypeName = DotfuscatorMap.Current.ObfuscatedTypeName(type.AssemblyQualifiedName);
                            newPath.Path.Add(new FieldPath((string)remoteTypeName, DotfuscatorMap.Current.ObfuscatedFieldName(type, field.Key)));

                            string unobfuscatedFieldName = DotfuscatorMap.Current.OriginalFieldName(type, field.Key);

                            if (typeof(IDictionary).IsAssignableFrom(field.Value))
                            {
                                this.Children.Add(new DictionaryNode(newPath, unobfuscatedFieldName, field.Value));
                            }
                            else if (typeof(IList).IsAssignableFrom(field.Value) || typeof(Array).IsAssignableFrom(field.Value))
                            {
                                this.Children.Add(new ListNode(newPath, unobfuscatedFieldName, field.Value));
                            }
                            else if (FieldNode.BaseTypes.Contains(field.Value) || typeof(Enum).IsAssignableFrom(field.Value))
                            {
                                this.Children.Add(new LeafNode(newPath, unobfuscatedFieldName, field.Value));
                            }
                            else
                            {
                                this.Children.Add(new FieldNode(newPath, unobfuscatedFieldName, field.Value));
                            }
                        }
                    }

                    if (onComplete != null)
                    {
                        onComplete();
                    }
                });
        }
    }
}
