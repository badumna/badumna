﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Reflection;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using System.Xml;
using System.Collections.ObjectModel;
using System.Linq;

using Badumna;
using Diagnostics;
using Visualizer;
using Badumna.Utilities;


namespace TheWolf
{
    partial class MainWindow : Window
    {
        private readonly INetworkFacade networkFacade;

        private const int Port = 21252;  // TODO: Allow configuration

        public static MainWindow Current { get { return (MainWindow)App.Current.MainWindow; } }


        private DiagnosticController mDiagnostics;
        public DiagnosticController Diagnostics { get { return this.mDiagnostics; } }

        private ObservableCollection<PeerNode> mPeers = new ObservableCollection<PeerNode>();

        // Maps public addresses to PeerNodes
        private Dictionary<IPEndPoint, PeerNode> mPeerNodes = new Dictionary<IPEndPoint, PeerNode>();

        // Maps diagnostic addresses to public addresses
        private Dictionary<IPEndPoint, IPEndPoint> mDiagnosticToPublicEndPoint = new Dictionary<IPEndPoint, IPEndPoint>();
        public Dictionary<IPEndPoint, IPEndPoint> DiagnosticToPublicEndPoint { get { return this.mDiagnosticToPublicEndPoint; } }

        public event Action<PeerNode> NewPeer;

        private List<IPEndPoint> mWaitingForStats = new List<IPEndPoint>();
        private int mRequestedStatsSerial;
        private StatisticsAggregator mStatsAggregator = new StatisticsAggregator("dave.csv", DateTime.Now.TimeOfDay);
        private List<SourceConfig> mGraphSources = new List<SourceConfig>();

        private int mStatisticsSerial;
        private int StatisticsSerial
        {
            get { return this.mStatisticsSerial; }
            set
            {
                this.mStatisticsSerial = value;
                this.StatisticsSerialText.Text = value.ToString();
            }
        }

        private DispatcherTimer mStatisticsTimer;


        public MainWindow()
        {
            InitializeComponent();

            this.StatisticsSerial = 0;
            this.PeerList.ItemsSource = this.mPeers;

            using (StreamReader reader = new StreamReader(App.GetResourceStream(new Uri("NetworkConfig.xml", UriKind.Relative)).Stream))
            {
                XmlDocument config = new XmlDocument();
                config.LoadXml(reader.ReadToEnd());
                Options options = new Options(config);
                this.networkFacade = NetworkFacade.Create(options);
            }
            this.networkFacade.Login(); // TODO: Allow specification of auth token somehow

            AppDomain.CurrentDomain.AssemblyResolve += this.ResolveAssembly;

            this.mDiagnostics = new DiagnosticController(this.networkFacade);
            this.mDiagnostics.ConnectionEvent += this.ConnectionChanged;
            this.mDiagnostics.Protocol.VersionRequestEvent += this.PeerVersion;
            this.mDiagnostics.Protocol.StatisticsEvent += this.NewStatistics;
            this.mDiagnostics.InitializeAsServer(MainWindow.Port);

            DispatcherTimer timer = new DispatcherTimer();
            timer.Tick += delegate
            {
                this.mDiagnostics.ProcessMessages();
            };
            timer.Interval = TimeSpan.FromSeconds(0.1);
            timer.Start();
        }

        public void OnAllPeers(Action<PeerNode> action)
        {
            foreach (PeerNode node in this.mPeers)
            {
                action(node);
            }
        }

        public void OnSelectedPeers(Action<PeerNode> action)
        {
            foreach (PeerNode node in this.PeerList.SelectedItems)
            {
                action(node);
            }
        }

        private void PeerVersion(object sender, VersionRequestArgs e)
        {
            lock (this.mDiagnosticToPublicEndPoint)
            {
                this.mDiagnosticToPublicEndPoint[e.Address] = e.PublicEndPoint;
            }
            PeerNode peerNode = new PeerNode(e.PublicEndPoint, this.mDiagnostics.Protocol, e.Address);

            lock (this.mPeerNodes)
            {
                PeerNode oldNode;
                if (this.mPeerNodes.TryGetValue(peerNode.PublicEndPoint, out oldNode))
                {
                    this.FieldBrowser.Remove(oldNode);
                }

                this.mPeerNodes[peerNode.PublicEndPoint] = peerNode;
                this.mPeers.Add(peerNode);
                this.FieldBrowser.Add(peerNode);
            }

            Action<PeerNode> handler = this.NewPeer;
            if (handler != null)
            {
                handler(peerNode);
            }
        }

        private void ConnectionChanged(object sender, ConnectionEventArgs e)
        {
            if (e.IsConnected)
            {
                this.Status.Text = e.Address.ToString() + " connected";
                this.mDiagnostics.Protocol.RequestVersion(e.Address);
            }
            else
            {
                lock (this.mDiagnosticToPublicEndPoint)
                {
                    IPEndPoint publicAddress;
                    if (this.mDiagnosticToPublicEndPoint.TryGetValue(e.Address, out publicAddress))
                    {
                        lock (this.mPeerNodes)
                        {
                            PeerNode oldNode;
                            if (this.mPeerNodes.TryGetValue(publicAddress, out oldNode))
                            {
                                this.mPeerNodes.Remove(publicAddress);
                                this.mPeers.Remove(oldNode);
                                this.FieldBrowser.Remove(oldNode);
                            }
                        }

                        this.mDiagnosticToPublicEndPoint.Remove(e.Address);
                    }
                }
            }
        }

        protected override void OnClosed(EventArgs e)
        {
            this.mDiagnostics.ShutDown();
            base.OnClosed(e);
        }

        Assembly ResolveAssembly(object sender, ResolveEventArgs args)
        {
            SelectAssemblyDialog selectAssemblyDialog = new SelectAssemblyDialog();
            selectAssemblyDialog.Owner = this;
            selectAssemblyDialog.AssemblyName = args.Name;
            if (selectAssemblyDialog.ShowDialog() == true)
            {
                return Assembly.LoadFile(selectAssemblyDialog.AssemblyPath);
            }
            return null;
        }


        private void ExitMenuClick(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void AboutMenuClick(object sender, RoutedEventArgs e)
        {
            About aboutBox = new About();
            aboutBox.ShowDialog();
        }

        private void OnAddWatch(object sender, ExecutedRoutedEventArgs e)
        {
            AddWatchParameter parameter = (AddWatchParameter)e.Parameter;
            this.WatchList.AddPeer(parameter.Path.PeerEndPoint, parameter.Path.DiagnosticEndPoint);
            if (parameter.Path.Path != null)
            {
                this.WatchList.AddWatch(parameter.Path.Path, parameter.Name);
            }
        }

        private void AddPeerToWatchList_Click(object sender, RoutedEventArgs e)
        {
            foreach (PeerNode peerNode in this.PeerList.SelectedItems)
            {
                this.WatchList.AddPeer(peerNode.PublicEndPoint, peerNode.DiagnosticsEndPoint);
            }
        }

        private IPEndPoint ResolveEndPoint(string endPoint)
        {
            if (endPoint.Length == 0)
            {
                return null;
            }

            string[] addressParts = endPoint.Split(new char[] { ':' }, 2);

            if (addressParts.Length != 2)
            {
                this.Status.Text = "Must include port";
                return null;
            }

            int port;
            if (!int.TryParse(addressParts[1], out port) || port < IPEndPoint.MinPort || port > IPEndPoint.MaxPort)
            {
                this.Status.Text = "Invalid port";
                return null;
            }

            IPAddress[] addresses = null;
            try
            {
                addresses = Dns.GetHostAddresses(addressParts[0]);
            }
            catch (SocketException)
            {
            }

            IPAddress resolvedAddress = null;
            if (addresses != null)
            {
                foreach (IPAddress address in addresses)
                {
                    // TODO: Remove this check; it's here as a workaround because PeerAddress currently
                    //       doesn't cope with IPv6 addresses.
                    if (address.AddressFamily == AddressFamily.InterNetwork)
                    {
                        resolvedAddress = address;
                        break;
                    }
                }
            }

            if (resolvedAddress == null)
            {
                this.Status.Text = "Could not resolve hostname '" + addressParts[0] + "'";
                return null;
            }

            return new IPEndPoint(resolvedAddress, port);
        }

        private void ConnectPeerClick(object sender, RoutedEventArgs e)
        {
            IPEndPoint peerDispatchEndPoint = this.ResolveEndPoint(this.PeerDispatchAddress.Text.Trim());
            if (peerDispatchEndPoint == null)
            {
                return;
            }
            IPEndPoint peerPublicEndPoint = this.ResolveEndPoint(this.PeerPublicAddress.Text.Trim());
            if (peerPublicEndPoint == null)
            {
                return;
            }

            DiagnosticController.RequestDiagnosticsConnect(
                this.networkFacade,
                peerDispatchEndPoint,
                peerPublicEndPoint,
                new IPEndPoint(IPAddress.Loopback, MainWindow.Port));
            this.Status.Text = "Connection request sent";
        }

        private void GetStatisticsClick(object sender, RoutedEventArgs e)
        {
            this.GetStatistics();
        }

        private void GetStatistics()
        {
            this.StatisticsSerial++;

            this.mRequestedStatsSerial = this.StatisticsSerial;
            this.mWaitingForStats = new List<IPEndPoint>();
            this.mStatsAggregator.Flush();

            foreach (KeyValuePair<IPEndPoint, IPEndPoint> endPointPair in this.mDiagnosticToPublicEndPoint)
            {
                this.Diagnostics.Protocol.RequestStatistics(endPointPair.Key, this.TestName.Text, this.StatisticsSerial);
                this.mWaitingForStats.Add(endPointPair.Value);
            }
        }

        void NewStatistics(object sender, StatisticsArgs args)
        {
            if (args.Serial != this.mRequestedStatsSerial || !this.mWaitingForStats.Contains(args.PublicEndPoint))
            {
                return;
            }

            this.mWaitingForStats.Remove(args.PublicEndPoint);
            this.mStatsAggregator.ProcessStatistics(
                DateTime.Now.TimeOfDay,
                args.Serial,
                new Dictionary<string, IStatistics> { { args.Address, args.Statistics } });

            if (this.mWaitingForStats.Count == 0 && this.mGraphSources != null)
            {
                foreach (SourceConfig source in this.mGraphSources)
                {
                    source.Append(this.Dispatcher, (int)DateTime.Now.TimeOfDay.TotalMilliseconds, this.mStatsAggregator.Table);
                }
            }
        }

        private void ConfigureStats_Click(object sender, RoutedEventArgs e)
        {
            StatConfigWindow config = new StatConfigWindow();
            if (config.ShowDialog() == true)
            {
                FrameworkElement graphElement;
                config.Config.CreateGraphs(double.NaN, out graphElement, out this.mGraphSources);
                this.Graphs.Children.Clear();
                this.Graphs.Children.Add(graphElement);

                if (this.mStatisticsTimer != null)
                {
                    this.mStatisticsTimer.Stop();
                }

                this.mStatisticsTimer = new DispatcherTimer();
                this.mStatisticsTimer.Tick += delegate
                {
                    this.GetStatistics();
                };
                this.mStatisticsTimer.Interval = TimeSpan.FromMilliseconds(config.Config.UpdateFrequency);
                this.mStatisticsTimer.Start();
            }
        }
    }
}
