﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Microsoft.Win32;

namespace TheWolf
{
    /// <summary>
    /// Interaction logic for SelectMapDialog.xaml
    /// </summary>
    public partial class SelectMapDialog : Window
    {
        public string MapPath { get { return this.MapPathTextBox.Text; } }

        public string AssemblyName
        {
            get { return this.AssemblyNameRun.Text; }
            set { this.AssemblyNameRun.Text = value; }
        }

        
        public SelectMapDialog()
        {
            InitializeComponent();

            this.OkButton.IsEnabled = false;
        }

        private void SelectMapClick(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openDialog = new OpenFileDialog();
            openDialog.Filter = "Maps (*.xml)|*.xml|All Files (*.*)|*.*";
            if (openDialog.ShowDialog() == true)
            {
                this.MapPathTextBox.Text = openDialog.FileName;
                this.OkButton.IsEnabled = true;
            }
        }

        private void OkButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }
    }
}
