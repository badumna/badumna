﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Markup;
using Diagnostics.WolfEye;
using Microsoft.Windows.Controls;
using System.Windows.Media;
using System.Diagnostics;
using Microsoft.Windows.Controls.Primitives;

namespace TheWolf
{
    partial class WatchList : UserControl
    {
        private class WatchHeader
        {
            private int mIndex;
            public int Index { get { return this.mIndex; } }

            private string mName;

            public WatchHeader(int index, string name)
            {
                this.mIndex = index;
                this.mName = name;
            }

            public override string ToString()
            {
                return this.mName;
            }
        }

        private class PeerWatches : INotifyPropertyChanged
        {
            public event PropertyChangedEventHandler PropertyChanged;

            public string Peer { get; private set; }

            public string this[int index]
            {
                get
                {
                    return this.mValues[index];
                }
            }

            private IPEndPoint mPeerEndPoint;
            private IPEndPoint mDiagnosticEndPoint;
            public IPEndPoint DiagnosticEndPoint { get { return this.mDiagnosticEndPoint; } }

            private Dictionary<int, ObjectPath> mWatches;

            private Dictionary<int, string> mValues = new Dictionary<int, string>();


            public PeerWatches(IPEndPoint peerEndPoint, IPEndPoint diagnosticEndPoint, Dictionary<int, ObjectPath> watches)
            {
                this.Peer = peerEndPoint.ToString();
                this.mPeerEndPoint = peerEndPoint;
                this.mDiagnosticEndPoint = diagnosticEndPoint;
                this.mWatches = watches;
            }

            private void OnPropertyChanged(string propertyName)
            {
                PropertyChangedEventHandler handler = this.PropertyChanged;
                if (handler != null)
                {
                    handler(this, new PropertyChangedEventArgs(propertyName));
                }
            }

            public void Update(int index)
            {
                PeerQualifiedPath path = new PeerQualifiedPath(this.mPeerEndPoint, null, this.mDiagnosticEndPoint, this.mWatches[index]);
                string oldValue;
                this.mValues.TryGetValue(index, out oldValue);
                this.mValues[index] = oldValue + " (updating)";
                this.OnPropertyChanged("Item[]");

                ((MainWindow)App.Current.MainWindow).Diagnostics.Protocol.Eye.RequestObject(
                    this.mDiagnosticEndPoint, this.mWatches[index],
                    delegate(object obj, Exception maybeException)
                    {
                        if (maybeException != null)
                        {
                            throw maybeException;  // TODO: Probably not a good idea...
                        }

                        this.mValues[index] = obj != null ? obj.ToString() : "(null)";
                        this.OnPropertyChanged("Item[]");
                    });
            }

            public void UpdateAll()
            {
                foreach (int key in this.mWatches.Keys)
                {
                    this.Update(key);
                }
            }

            public void Remove(int index)
            {
                this.mValues.Remove(index);
            }
        }


        private ObservableCollection<PeerWatches> mPeerWatches = new ObservableCollection<PeerWatches>();

        private Dictionary<int, ObjectPath> mWatches = new Dictionary<int, ObjectPath>();
        private int mNextWatchKey = 0;
        private Dictionary<int, DataTemplate> mCellTemplates = new Dictionary<int, DataTemplate>();

        private DataTemplate mHeaderTemplate;


        public WatchList()
        {
            InitializeComponent();

            this.mHeaderTemplate = (DataTemplate)this.FindResource("WatchHeaderTemplate");

            this.Grid.ItemsSource = this.mPeerWatches;
        }

        private void OnRefreshCell(object sender, ExecutedRoutedEventArgs args)
        {
            DataGridRow row;
            DataGridCell cell = FindAncestor<DataGridCell>((DependencyObject)args.OriginalSource);

            if (cell != null)
            {
                row = FindAncestor<DataGridRow>(cell);

                if (row == null)
                {
                    Debug.Fail("Couldn't find DataGridRow for a cell");
                    return;
                }

                PeerWatches peerWatches = (PeerWatches)row.Item;
                int index = ((WatchHeader)cell.Column.Header).Index;
                peerWatches.Update(index);
                return;
            }

            DataGridColumnHeader columnHeader = FindAncestor<DataGridColumnHeader>((DependencyObject)args.OriginalSource);
            if (columnHeader != null)
            {
                int index = ((WatchHeader)columnHeader.Column.Header).Index;
                foreach (PeerWatches peerWatch in this.mPeerWatches)
                {
                    peerWatch.Update(index);
                }
                return;
            }

            row = FindAncestor<DataGridRow>((DependencyObject)args.OriginalSource);
            if (row != null)
            {
                PeerWatches peerWatch = (PeerWatches)row.Item;
                peerWatch.UpdateAll();
                return;
            }

            Debug.Fail("Got refresh command but couldn't determine the source");
        }

        private static T FindAncestor<T>(DependencyObject start) where T : class
        {
            while (start != null && !(start is T))
            {
                start = VisualTreeHelper.GetParent(start);
            }
            return start as T;
        }

        public void AddPeer(IPEndPoint peerEndPoint, IPEndPoint diagnosticEndPoint)
        {
            foreach (PeerWatches peerWatch in this.mPeerWatches)
            {
                if (diagnosticEndPoint.Equals(peerWatch.DiagnosticEndPoint))
                {
                    return;
                }
            }

            PeerWatches newPeer = new PeerWatches(peerEndPoint, diagnosticEndPoint, this.mWatches);
            this.mPeerWatches.Add(newPeer);
        }

        private DataTemplate MakeCellTemplate(string bindingPath)
        {
            string templateXaml =
                @"<DataTemplate xmlns=""http://schemas.microsoft.com/winfx/2006/xaml/presentation""
                                xmlns:local=""clr-namespace:TheWolf;assembly=TheWolf"">
                    <Grid>
                      <Grid.ColumnDefinitions>
                        <ColumnDefinition Width=""Auto"" />
                        <ColumnDefinition Width=""*"" />
                      </Grid.ColumnDefinitions>

                      <TextBlock Grid.Column=""0"" Text=""{0}""/>

                      <Button Grid.Column=""1"" VerticalAlignment=""Center"" HorizontalAlignment=""Right"" Margin=""5,0,0,0""
                              Command=""local:Commands.RefreshCommand"" Foreground=""Transparent"">
                        <Button.Template>
                          <ControlTemplate TargetType=""Button"">
                            <ContentPresenter />
                          </ControlTemplate>
                        </Button.Template>
                        <Image Stretch=""None"" Source=""/TheWolf;component/Icons/control_repeat.png"" />
                      </Button>
                    </Grid>
                  </DataTemplate>";

            string boundXaml = String.Format(templateXaml, "{Binding " + bindingPath + "}");
            return (DataTemplate)XamlReader.Parse(boundXaml);
        }
        
        private DataTemplate GetWatchCellTemplate(int key)
        {
            DataTemplate dataTemplate;
            if (!this.mCellTemplates.TryGetValue(key, out dataTemplate))
            {
                dataTemplate = this.MakeCellTemplate("[" + key.ToString() + "]");
                this.mCellTemplates[key] = dataTemplate;
            }
            return dataTemplate;
        }

        public void AddWatch(ObjectPath path, string name)
        {
            int key = this.mNextWatchKey;
            this.mNextWatchKey++;

            this.mWatches[key] = path;


            this.Grid.Columns.Add(
                new DataGridTemplateColumn
                {
                    Header = new WatchHeader(key, name),
                    HeaderTemplate = this.mHeaderTemplate,
                    CellTemplate = this.GetWatchCellTemplate(key)
                });
        }

        private void OnRemoveWatch(object sender, ExecutedRoutedEventArgs args)
        {
            DataGridColumnHeader columnHeader = FindAncestor<DataGridColumnHeader>((DependencyObject)args.OriginalSource);
            if (columnHeader != null)
            {
                int index = ((WatchHeader)columnHeader.Column.Header).Index;
                this.Grid.Columns.Remove(columnHeader.Column);
                this.mWatches.Remove(index);
                foreach (PeerWatches peerWatch in this.mPeerWatches)
                {
                    peerWatch.Remove(index);
                }
                return;
            }

            DataGridRow row = FindAncestor<DataGridRow>((DependencyObject)args.OriginalSource);
            if (row != null)
            {
                PeerWatches peerWatch = (PeerWatches)row.Item;
                this.mPeerWatches.Remove(peerWatch);
                return;
            }

            Debug.Fail("Got remove watch command but can't determine watch row/column");
        }
    }
}
