﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Microsoft.Win32;

namespace TheWolf
{
    public partial class SelectAssemblyDialog : Window
    {
        public string AssemblyPath { get { return this.AssemblyPathTextBox.Text; } }

        public string AssemblyName
        {
            get { return this.AssemblyNameRun.Text; }
            set { this.AssemblyNameRun.Text = value; }
        }


        public SelectAssemblyDialog()
        {
            InitializeComponent();

            this.OkButton.IsEnabled = false;
        }

        private void SelectAssemblyClick(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openDialog = new OpenFileDialog();
            openDialog.Filter = "Assemblies (*.exe; *.dll)|*.exe;*.dll|All Files (*.*)|*.*";
            if (openDialog.ShowDialog() == true)
            {
                this.AssemblyPathTextBox.Text = openDialog.FileName;
                this.OkButton.IsEnabled = true;
            }
        }

        private void OkButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }
    }
}
