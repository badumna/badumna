﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.XPath;
using System.Xml.Linq;
using System.IO;
using System.Reflection;

namespace TheWolf
{
    class DotfuscatorMap
    {
        private class TypeMap
        {
            private TwoWayMap<string> mFieldMap = new TwoWayMap<string>();
            public TwoWayMap<string> FieldMap { get { return this.mFieldMap; } }


            public void AddFieldMapping(string originalName, string obfuscatedName)
            {
                this.mFieldMap[originalName] = obfuscatedName;
            }
        }

        private class AssemblyMap
        {
            private TwoWayMap<string> mTypeNameMap = new TwoWayMap<string>();
            public TwoWayMap<string> TypeNameMap { get { return this.mTypeNameMap; } }

            private Dictionary<string, TypeMap> mTypeMaps = new Dictionary<string, TypeMap>();


            public void AddTypeMapping(string originalName, string obfuscatedName, TypeMap typeMap)
            {
                this.mTypeNameMap[originalName] = obfuscatedName;
                this.mTypeMaps[originalName] = typeMap;
            }

            public TypeMap GetTypeMap(string originalName)
            {
                TypeMap map;
                this.mTypeMaps.TryGetValue(originalName, out map);
                return map;
            }
        }


        private static DotfuscatorMap mCurrent = new DotfuscatorMap();
        public static DotfuscatorMap Current { get { return DotfuscatorMap.mCurrent; } }


        private Dictionary<string, AssemblyMap> mAssemblies = new Dictionary<string, AssemblyMap>();


        private DotfuscatorMap()
        {
        }


        public void AddMappings(string fileName)
        {
            XElement root = XElement.Load(fileName);

            foreach (XElement module in root.Element("mapping").Elements())
            {
                string name = (string)module.Element("name");
                name = Path.GetFileNameWithoutExtension(name);
                AssemblyMap assemblyMap = new AssemblyMap();
                this.mAssemblies[name] = assemblyMap;

                foreach (XElement type in module.Elements("type"))
                {
                    string originalName = (string)type.Element("name");
                    TypeMap typeMap = new TypeMap();
                    assemblyMap.AddTypeMapping(originalName, (string)type.Element("newname") ?? originalName, typeMap);

                    foreach (XElement field in type.Element("fieldlist").Elements())
                    {
                        originalName = (string)field.Element("name");
                        typeMap.AddFieldMapping(originalName, (string)field.Element("newname") ?? originalName);
                    }
                }
            }
        }

        
        // These functions rely on the assumption that no one's going to name a field using the range of
        // names that the obfuscator uses.  If a mapping isn't found in any instance it's assumed the field
        // wasn't obfuscated and the given parameter is simply passed back.


        public string ObfuscatedTypeName(string originalQualifiedTypeName)
        {
            return this.ConvertTypeName(originalQualifiedTypeName, TwoWayMap<string>.Direction.Forward);
        }

        public string OriginalTypeName(string obfuscatedQualifiedTypeName)
        {
            return this.ConvertTypeName(obfuscatedQualifiedTypeName, TwoWayMap<string>.Direction.Backward);
        }

        private string ConvertTypeName(string qualifiedTypeName, TwoWayMap<string>.Direction direction)
        {
            TypeName typeName = new TypeName(qualifiedTypeName);

            if (string.IsNullOrEmpty(typeName.AssemblyName))
            {
                throw new ArgumentException("Type name doesn't seem to be qualified with assembly name");
            }

            AssemblyMap map;
            if (this.mAssemblies.TryGetValue(typeName.AssemblyName, out map))
            {
                string convertedTypeName;
                if (map.TypeNameMap.TryGetValue(typeName.Name, out convertedTypeName, direction))
                {
                    typeName.Name = convertedTypeName;
                }
            }

            if (typeName.TypeParameters != null)
            {
                for (int i = 0; i < typeName.TypeParameters.Length; i++)
                {
                    typeName.TypeParameters[i] = new TypeName(this.ConvertTypeName(typeName.TypeParameters[i].ToString(), direction));
                }
            }

            return typeName.ToString();
        }


        public string ObfuscatedFieldName(Type type, string originalFieldName)
        {
            string originalTypeName = this.OriginalTypeName(type.AssemblyQualifiedName);
            string obfuscatedFieldName = this.ObfuscatedFieldName(originalTypeName, originalFieldName);
            if (obfuscatedFieldName != originalFieldName)
            {
                return obfuscatedFieldName;
            }

            return originalFieldName;
        }

        private string ObfuscatedFieldName(string originalQualifiedTypeName, string originalFieldName)
        {
            return this.ConvertFieldName(originalQualifiedTypeName, originalFieldName, TwoWayMap<string>.Direction.Forward);
        }

        public string OriginalFieldName(Type type, string obfuscatedFieldName)
        {
            string originalTypeName = this.OriginalTypeName(type.AssemblyQualifiedName);
            string originalFieldName = this.OriginalFieldName(originalTypeName, obfuscatedFieldName);
            if (originalFieldName != obfuscatedFieldName)
            {
                return originalFieldName;
            }

            return obfuscatedFieldName;
        }

        private string OriginalFieldName(string originalQualifiedTypeName, string obfuscatedFieldName)
        {
            return this.ConvertFieldName(originalQualifiedTypeName, obfuscatedFieldName, TwoWayMap<string>.Direction.Backward);
        }

        private string ConvertFieldName(string originalQualifiedTypeName, string fieldName, TwoWayMap<string>.Direction direction)
        {
            TypeName typeName = new TypeName(originalQualifiedTypeName);

            if (string.IsNullOrEmpty(typeName.AssemblyName))
            {
                throw new ArgumentException("Type name doesn't seem to be qualified with assembly name");
            }

            AssemblyMap map;
            if (this.mAssemblies.TryGetValue(typeName.AssemblyName, out map))
            {
                TypeMap typeMap = map.GetTypeMap(typeName.Name);
                if (typeMap != null)
                {
                    string convertedFieldName;
                    if (typeMap.FieldMap.TryGetValue(fieldName, out convertedFieldName, direction))
                    {
                        fieldName = convertedFieldName;
                    }
                }
            }

            return fieldName;
        }


        public static bool IsObfuscatedName(string name)
        {
            return (int)name[name.Length - 1] >= 5888;  // UTF-16 value of lowest character that Dotfuscator seems to use for renaming
        }

        public static Dictionary<string, Type> GetFieldList(Type type, BindingFlags searchFlags)
        {
            Dictionary<string, Type> result = new Dictionary<string, Type>();

            FieldInfo[] fieldInfos = type.GetFields(searchFlags);
            foreach (FieldInfo fieldInfo in fieldInfos)
            {
                result.Add(fieldInfo.Name, fieldInfo.FieldType);
            }

            return result;
        }
    }
}
