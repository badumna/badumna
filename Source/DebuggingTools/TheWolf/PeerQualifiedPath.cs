﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;

using Diagnostics.WolfEye;
using Diagnostics;


namespace TheWolf
{
    class PeerQualifiedPath
    {
        private IPEndPoint mPeerEndPoint;
        public IPEndPoint PeerEndPoint { get { return this.mPeerEndPoint; } }

        private DiagnosticProtocol mDiagnostics;
        public DiagnosticProtocol Diagnostics { get { return this.mDiagnostics; } }

        private IPEndPoint mDiagnosticEndPoint;
        public IPEndPoint DiagnosticEndPoint { get { return this.mDiagnosticEndPoint; } }

        private ObjectPath mPath;
        public ObjectPath Path { get { return this.mPath; } }


        public PeerQualifiedPath(IPEndPoint peerEndPoint, DiagnosticProtocol diagnostics, IPEndPoint diagnosticEndPoint, ObjectPath path)
        {
            this.mPeerEndPoint = peerEndPoint;
            this.mDiagnostics = diagnostics;
            this.mDiagnosticEndPoint = diagnosticEndPoint;
            this.mPath = path;
        }

        public PeerQualifiedPath(PeerQualifiedPath toCopy)
        {
            this.mPeerEndPoint = toCopy.mPeerEndPoint;
            this.mDiagnostics = toCopy.mDiagnostics;
            this.mDiagnosticEndPoint = toCopy.mDiagnosticEndPoint;
            this.mPath = new ObjectPath(toCopy.mPath);
        }
    }
}
