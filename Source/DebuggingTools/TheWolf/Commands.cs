﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;

namespace TheWolf
{
    class AddWatchParameter
    {
        public PeerQualifiedPath Path;
        public string Name;
    }

    public static class Commands
    {
        public static readonly RoutedCommand RefreshCommand = new RoutedCommand();
        public static readonly RoutedCommand AddWatchCommand = new RoutedCommand();
        public static readonly RoutedCommand RemoveWatchCommand = new RoutedCommand();
    }
}
