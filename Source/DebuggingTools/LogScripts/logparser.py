#!/usr/bin/env python
import os
import re
from fileinput import FileInput
import heapq
from fnmatch import fnmatch
from collections import namedtuple, OrderedDict
from itertools import ifilter, imap
from datetime import datetime


LogLine = namedtuple('LogLine', 'time, thread, level, tags, message')
PeerAddress = namedtuple('PeerAddress', 'nat, ip, port')
BadumnaId = namedtuple('BadumnaId', 'peer, local')
Event = namedtuple('Event', 'time, type, args')


main_re = re.compile(r'^.*? (?P<level>.*?):.*?: (?P<time>.*?) : {(?P<tags>.*?)}(?P<message>.*)$')
thread_re = re.compile(r'^    ThreadId=(.*)$')
date_re = re.compile(r'^    DateTime=(.*\.\d{6})\dZ$')
stack_re = re.compile(r'^  at .*$')
peer_re = re.compile(r'^(?P<nat>.*)\|(?P<ip>.*):(?P<port>.*)$')
badumna_id_re = re.compile(r'^(?P<peer>.*)-(?P<local>.*)$')


def parse_peer_address(s):
    match = peer_re.match(s)
    if match is None:
        raise ValueError('Failed to parse peer address: ' + s)

    groups = match.groupdict()
    return PeerAddress(groups['nat'], groups['ip'], int(groups['port']))


def parse_badumna_id(s):
    match = badumna_id_re.match(s)
    if match is None:
        raise ValueError('Failed to parse badumna id: ' + s)
    
    groups = match.groupdict()
    return BadumnaId(parse_peer_address(groups['peer']), int(groups['local']))


def parse_message(f, mono_style=True):
    while True:
        line = f.readline()
        if line == '':
            return None
        
        main_match = main_re.match(line)
        if main_match is not None or stack_re.match(line) is None:
            break
        # TODO: Report that we found an exception if stack_re matches

    if main_match is None:
        raise ValueError('Failed to parse log line')

    thread = 0

    if not mono_style:
        thread_match = thread_re.match(f.readline())
        date_match = date_re.match(f.readline())
        if thread_match is None or date_match is None:
            raise ValueError('Failed to parse log line')
        
        #time = datetime.strptime(date_match.group(1), '%Y-%m-%dT%H:%M:%S.%f')
        thread = int(thread_match.group(1))

    main_dict = main_match.groupdict()
    level = main_dict['level']
    tags = main_dict['tags'].split(', ')
    message = main_dict['message'].strip()
    time = int(main_dict['time'])

    return LogLine(time, thread, level, tags, message)
    

def log_lines(filename):
    f = FileInput(filename)
    f.readline()  # Skip crufty first line

    try:
        while True:
            log = parse_message(f)
            if log is None:
                break
            yield log
    except Exception:
        print 'Parsing failed on line {}'.format(f.filelineno())
        raise


next_peer_id = 0
peer_ids = OrderedDict()

def get_peer_id(peer):
    global next_peer_id
    
    if peer not in peer_ids:
        peer_ids[peer] = next_peer_id
        next_peer_id += 1

    return peer_ids[peer]


rules = []

def rule(r, tag=None):
    def add_rule(f):
        rules.append((re.compile(r), tag, f))
        return f
    return add_rule


def process_line(line):
    for rule in rules:
        match = rule[0].match(line.message)
        if match is not None and (rule[1] is None or rule[1] in line.tags):
            result = rule[2](*match.groups())
            return Event(line.time, result[0], result[1:])

    return Event(line.time, 'Other', line.message)


def events(filename):
    return ifilter(
        lambda x: x is not None,
        imap(process_line, log_lines(filename)))


@rule(r'PublicAddress set (?P<address>.*)')
def on_address_set(address):
    return ('PublicAddress', parse_peer_address(address))

@rule(r'(?P<peer>.*) State=(?P<state>.*)')
def on_connection_state(peer, state):
    return ('Connection', parse_peer_address(peer), state)

@rule(r'(?P<peer>.*?\|.*?) (?P<message>.*)', 'Connection')
def on_connection_other(peer, message):
    return ('ConnectionOther', parse_peer_address(peer), message)

@rule(r'Interested (?P<action>add|remove) (?P<entity>.*)')
def on_interest(action, entity):
    return ('Interest', parse_badumna_id(entity), action)

@rule(r'Overload (?P<action>add|remove) (?P<entity>.*)')
def on_overload(action, entity):
    return ('Overload', parse_badumna_id(entity), action)

@rule(r'Replica (?P<action>add|remove) (?P<entity>.*?-\d*).*')
def on_replica_change(action, entity):
    return ('Replica', parse_badumna_id(entity), action)
    

def find_files(directory, pattern):
    """Return a list of paths to files matching 'pattern' in 'directory' or one of its subdirectories."""
    
    result = []
    
    for root, dirs, files in os.walk(directory):
        for file in files:
            if fnmatch(file, pattern):
                result.append(os.path.join(root, file))
                
    return result



# NOTE: Only using ip+port as key for dicts because
#       we may not initially know the proper NAT type
#       of a given peer.
# THIS WON'T WORK if the internal ip and public ip are different.
def key(p):
    return p.ip + str(p.port)


directory = 'Data/Badumna 2.0.0.7611/Stats 09.14.11 Test1'

peers = {}

log_files = find_files(directory, 'badumna-*.log')

def get_peer_address(log_file):
    for event in events(log_file):
        if event.type != 'PublicAddress':
            raise Exception('Expected PublicAddress as first event')
        return event.args[0]


def annotate(tag, iterable):
    return ((x, tag) for x in iterable)


# *** Also want to plot status of overload server against both peers
#     in case overload server lost connection.

def compare_pair(*peer_indexes):
    assert len(peer_indexes) == 2   # TODO: Support more peers?

    files = [log_files[i] for i in peer_indexes]
    addresses = [key(get_peer_address(f)) for f in files]
    evts = [annotate(i, events(files[i])) for i in range(len(files))]
    states = []
    for i in range(len(files)):
        states.append({
            'connection': '.',
            'interest': ' ',
            'replica': ' '
        })

    limit = 0
    for event, peer in heapq.merge(*evts):
        other = 1 - peer
        
        if event.type == 'Connection':
            # Ignore event unless it involves the other peer we care about
            if key(event.args[0]) != addresses[other]:
                continue

            status = event.args[1]
            states[peer]['connection'] = '|' if status == 'Connected' else ':' if status == 'Initializing' else '.'

        elif event.type == 'Interest':
            if key(event.args[0].peer) != addresses[other]:
                continue

            states[peer]['interest'] = '|' if event.args[1] == 'add' else ' '

        elif event.type == 'Overload':
            if key(event.args[0].peer) != addresses[other]:
                continue

            assert states[peer]['interest'] != ' '
            states[peer]['interest'] = '/' if event.args[1] == 'add' else '|'

        elif event.type == 'Replica':
            if key(event.args[0].peer) != addresses[other]:
                continue

            states[peer]['replica'] = '|' if event.args[1] == 'add' else ' '

        else:
            continue

        # TODO: Print out events too, so can see if we're getting duplicates of events
        #       (which we can't see because they don't alter state).

        print '{:<8}{}{}{}   {}{}{}'.format(
            event.time,
            states[0]['connection'],
            states[0]['interest'],
            states[0]['replica'],
            states[1]['connection'],
            states[1]['interest'],
            states[1]['replica'])

        limit -= 1
        if limit == 0:
            break






            
