#!/bin/bash
if [ ! -e "$1" ]
then
	echo "$1 does not exist."
	exit 1
fi

cat $1 | grep "replica with key" | gawk '{print $6}' | sort > replica.bts

rm -f replica_count.bts

for replica in `cat replica.bts | uniq`
do
	count=`cat replica.bts | grep $replica | wc -l`
	echo $count $replica >> replica_count.bts
done

cat replica_count.bts | sort -n > replica_count_sorted.bts

rm -f replica_count.bts
rm -f replica.bts

