#!/bin/bash

if [ ! -e "$1" ]
then
	echo "$1 does not exist."
	exit 1
fi

cat $1 | grep "Writing call to" | gawk '{print $8}' | sort > outgoing_traffic.bts
cat $1 | grep "Reading method" | gawk '{print $8}' | sort > incoming_traffic.bts
rm -f outgoing_method.bts
rm -f incoming_method.bts
for method in `cat outgoing_traffic.bts | uniq`
do
	count=`cat outgoing_traffic.bts | grep $method | wc -l`
	echo $count $method >> outgoing_method.bts
done

for method in `cat incoming_traffic.bts | uniq`
do
	count=`cat incoming_traffic.bts | grep $method | wc -l`
	echo $count $method >> incoming_method.bts
done

cat outgoing_method.bts | sort -n > outgoing_sorted.bts
cat incoming_method.bts | sort -n > incoming_sorted.bts

rm -f outgoing_traffic.bts
rm -f incoming_traffic.bts
rm -f outgoing_method.bts
rm -f incoming_method.bts
