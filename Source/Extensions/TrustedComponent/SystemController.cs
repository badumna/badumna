﻿using System;
using System.Collections.Generic;
using System.Text;

using Badumna.Core;
using Badumna.Security;
using Badumna.Utilities;
using Badumna.Transport;
using Badumna.DistributedHashTable;
using Badumna;

namespace TrustedComponent
{
    class SystemController : SystemControlService
    {
        public SystemController(DhtProtocol parent, ConnectionTable connectionTable, TokenCache tokens)
            : base(parent, connectionTable, tokens)
        {
        }

        public void DistributeBlacklistEntry(long blacklistedUserId, TimeSpan blacklistEntryLifeSpan)
        {
            Permission permission = this.tokens.GetPermission(PermissionType.Blacklist);
            PrivateKey keyPair = this.tokens.GetPermissionKeyPair();

            if (permission != null && keyPair != null)
            {
                long timeStamp = DateTime.Now.Ticks;
                Signature signature = new Signature(keyPair, blacklistedUserId, blacklistEntryLifeSpan, timeStamp);

                DhtEnvelope envelope = this.GetMessageFor((HashKey)null, QualityOfService.Reliable);
                envelope.Source = PeerAddress.Nowhere; // So that the source (a trusted peer) remains anonymous

                this.RemoteCall(envelope, this.BlacklistUser, blacklistedUserId, blacklistEntryLifeSpan, timeStamp, permission, signature);
                this.SendMessage(envelope);
            }
            else
            {
                Logger.TraceError(LogTag.Security, "Do not have permission to black list users.");
            }
        }
    }
}
