﻿using System;
using System.Collections.Generic;
using System.Text;

using Badumna;
using Badumna.Core;
using Badumna.Security;
using Badumna.Transport;
using Badumna.DistributedHashTable;

namespace TrustedComponent
{
    class Plugin : ITrustedExtension
    {
        #region ITrustedExtension Members

        public void Initialize()
        {
        }

        public void Shutdown()
        {
        }

        public IComplaintForwarder CreateComplaintForwarder(TransportProtocol parent, INetworkAddressProvider addressProvider, TokenCache tokens, INetworkConnectivityReporter connectivityReporter)
        {
            return new TrustedComplaintsForwarder(new ComplaintTracker(), parent, addressProvider, tokens, connectivityReporter);
        }

        public DhtFacade CreateTrustedDhtFacade(RouterMultiplexer multiplexer, IConnectionTable connectionTable)
        {
            return null;
        }

        public SystemControlService CreateSystemControlService(DhtProtocol parent, ConnectionTable connectionTable, TokenCache tokens)
        {
            return new SystemControlService(parent, connectionTable, tokens);
        }

        #endregion
    }
}
