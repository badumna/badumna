﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Threading;

using Badumna.Core;
using Badumna.Security;

using TrustedComponent.BlackListing;

namespace TrustedComponent
{
    class BlackLister
    {
        private Complaints mComplaints;
        private ArrayList mBlockedUsers;
        private readonly string mBlackListName = "blacklist.dat";
        private Threshold mThreshold = new Threshold();

        public BlackLister(bool from)
        {  
            this.mComplaints = new Complaints(from); 
            if ((this.mBlockedUsers = Utility.ReadArray(this.mBlackListName)) == null)
            {
                this.mBlockedUsers = new ArrayList();
                Utility.WriteArray(this.mBlackListName, this.mBlockedUsers);
            }
        }

        private bool Analyse(Complaints complaints)
        {
            long complaner = 1;
            long complanee = 2;
            complaints.GetAllComplaintsByComplainer(complaner);
            complaints.GetAllComplaintsOfComplainee(complanee);
            complaints.GetComplaineeCurrentRank(complanee);
            complaints.GetComplaineePeerAddresses(complanee);
            complaints.GetComplaineePublicKeys(complanee);
            complaints.GetComplaineeRankHistory(complanee);
            complaints.GetComplaineesUserNamesByComplainer(complaner);
            complaints.GetComplainerCurrentRank(complaner);
            complaints.GetComplainerPeerAddresses(complaner);
            complaints.GetComplainerPublicKeys(complaner);
            complaints.GetComplainerRankHistory(complaner);
            complaints.GetComplainersUserNamesOfComplainee(complanee);
            complaints.GetLastComplaintByComplainer(complaner);
            complaints.GetLastComplaintOfComplainee(complanee);
            complaints.GetNumberOfComplaintsByComplainer(complaner);
            complaints.GetNumberOfComplaintsOfComplainee(complanee);
            complaints.GetTotalNumberOfComplaints();
            
  
            return true;
        }

        
        public void BlockUser(string userName)
        {
            if(!this.mBlockedUsers.Contains(userName))
            {
                this.mBlockedUsers.Add(userName);
                Utility.WriteArray(this.mBlackListName, this.mBlockedUsers);
            }
        }

        public void UnblockUser(string userName)
        {
            this.mBlockedUsers.Remove(userName);
            Utility.WriteArray(this.mBlackListName, this.mBlockedUsers);
        }

        public bool IsBlocked(string userName)
        {
            return this.mBlockedUsers.Contains(userName);
        }       

        public void ClearAll()
        {
            Utility.DeleteArray(this.mBlackListName);
        }
        
        public Complaints Complaints { get { return this.mComplaints; } }


        //#region class RankedUser
        //[Serializable]
        //class RankedUser
        //{
        //    private class Properties
        //    {
        //        private AccessControlTypes mAccessType;
        //        private PeerAddress mPeerAddress;
        //        private PublicKey mPublicKey;
        //        private Ranker mRanker;

        //        internal AccessControlTypes AccessType
        //        {
        //            get { return this.mAccessType; }
        //            set { this.mAccessType = value; }
        //        }

        //        internal PeerAddress PeerAddress
        //        {
        //            get { return this.mPeerAddress; }
        //            set { this.mPeerAddress = value; }
        //        }

        //        internal PublicKey PublicKey
        //        {
        //            get { return this.mPublicKey; }
        //            set { this.mPublicKey = value; }
        //        }

        //        internal Ranker Ranker
        //        {
        //            get { return this.mRanker; }
        //            set { this.mRanker = value; }
        //        }
        //    }

        //    private ArrayList mProperties;
        //    private readonly string mUserName;

        //    public string UserName { get { return this.mUserName; } }

        //    public RankedUser(string userName)
        //    {
        //        if (userName == null) throw new ArgumentNullException("userName");

        //        this.mUserName = userName;
        //    }

        //    internal bool AddAllProperties(AccessControlTypes accessType, PeerAddress peerAddress, byte[] publicKey, Ranker ranker)
        //    {
        //        if (peerAddress == null) throw new ArgumentNullException("peerAddress");
        //        if (publicKey == null) throw new ArgumentNullException("publicKey");
        //        if (ranker == null) throw new ArgumentNullException("ranker");

        //        Properties properties = new Properties();
        //        properties.AccessType = accessType;
        //        properties.PeerAddress = peerAddress;
        //        properties.PublicKey = publicKey;
        //        properties.Ranker = ranker;

        //        this.mProperties.Add(properties); //TODO: Add checks
        //        return true;
        //    }

        //    void ClearAllProperties()
        //    {
        //        if (this.mProperties != null)
        //        {
        //            this.mProperties.Clear();
        //        }
        //    }

        //    public string UserName { get { return this.mUserName; } }

        //    //public AccessControlTypes Access 
        //    //{ 
        //    //    get { return this.mAccess; }
        //    //    set { this.mAccess = value; }
        //    //}

        //    //public ClientCertificateProperties Certificate { get { return this.mCertificate; } }

        //    //public Ranker Ranker 
        //    //{ 
        //    //    get { return this.mRanker; }
        //    //    set { this.mRanker = value; }
        //    //}
        //}
        //#endregion

        //#region class Sleuth
        //class Sleuth
        //{
        //    string[] FindAllAssociatedUsers(string userName)
        //    {

        //        return null;
        //    }
        //    string FindUserByUserName(string userName, Dictionary<string, RankedUser> source)
        //    {

        //        //source.
        //        return null;
        //    }

        //    string FindUserByIpAddress(string ip)
        //    {
        //        return null;
        //    }

        //}
        //#endregion
    }
}
