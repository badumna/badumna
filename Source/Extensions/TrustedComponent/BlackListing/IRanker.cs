﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TrustedComponent.BlackListing
{
    interface IRanker
    {
        void IncreaseRank(Rank rank);
        void DecreaseRank(Rank rank);
    }
}
