﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TrustedComponent.BlackListing
{
    class Range
    {
        int mUpperBorder;
        int mLowerBorder;

        public Range(int lowerBorder, int upperBorder)
        {
            if (upperBorder <= lowerBorder) throw new ArgumentOutOfRangeException("upperBorder <= lowerBorder");
            this.mUpperBorder = upperBorder;
            this.mLowerBorder = lowerBorder;
        }

        public int UpperBorder { get { return this.mUpperBorder; } }
        public int LowerBorder { get { return this.mLowerBorder; } }
    }
}
