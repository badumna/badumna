﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TrustedComponent.BlackListing
{
    internal enum AccessControlTypes
    {
        None,
        Allowed,
        Blocked
    } 
}
