﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

using Badumna.Core;
using Badumna.Security;
using Badumna.DistributedHashTable;

namespace TrustedComponent.BlackListing
{
    
    [Serializable]
    class Plaint
    {
       
        private long mComplainerUserId = 0;
        private PeerAddress mComplainerPeerAddress = PeerAddress.Nowhere;
        private PublicKey mComplainerPublicKey = null;
        private Ranker mComplainerRanker = null;
//        private DateTime mComplainerTime; //add later
        private long mComplaineeUserId = 0;
        private PeerAddress mComplaineePeerAddress = PeerAddress.Nowhere;
        private PublicKey mComplaineePublicKey = null;
        private Ranker mComplaineeRanker = null;
//        private DateTime mComplaineeTime; //add later
        private ComplaintProperties mComplaint = null;
        private DateTime mTimestamp = DateTime.Now;                   
        

        public Plaint()/*long complainerUserId, Ranker complainerRanker, PeerAddress complainerPeerAddress, PublicKey complainerPublicKey, 
                      long complaineeUserId, Ranker complaineeRanker, PeerAddress complaineePeerAddress, PublicKey complaineePublicKey, 
                      ComplaintProperties complaint, DateTime timestamp)*/
        {
            /*
            if (complainerRanker == null) throw new ArgumentNullException("complainerRanker");
            if (complainerPeerAddress == null) throw new ArgumentNullException("complainerPeerAddress");
            if (complainerPublicKey == null) throw new ArgumentNullException("complainerPublicKey");
            if (complaineeRanker == null) throw new ArgumentNullException("complaineeRanker");
            if (complaineePeerAddress == null) throw new ArgumentNullException("complaineePeerAddress");
            if (complaineePublicKey == null) throw new ArgumentNullException("complaineePublicKey");            
            if (complaint == null) throw new ArgumentNullException("complaint");
            if (timestamp == null) throw new ArgumentNullException("timestamp");

            this.mComplainerUserId = complainerUserId;
            this.mComplainerRanker = complainerRanker;
            this.mComplainerPeerAddress = complainerPeerAddress;
            this.mComplainerPublicKey = complainerPublicKey;
            this.mComplaineeUserId = complaineeUserId;
            this.mComplaineeRanker = complaineeRanker;
            this.mComplaineePeerAddress = complaineePeerAddress;
            this.mComplaineePublicKey = complaineePublicKey;
            this.mComplaint = complaint;
            this.mTimestamp = timestamp;*/
        }

        public ArrayList SerializeForDatabase()
        {
            ArrayList array = new ArrayList();
         /*   array.Add(this.mComplainerUserId);
            array.Add(this.mComplainerRanker.Rank);
            array.Add(this.mComplainerPeerAddress.HashAddress);
            array.Add(this.mComplainerPublicKey.RawData);
            array.Add(this.mComplaineeUserId);
            array.Add(this.mComplaineeRanker.Rank);
            array.Add(this.mComplaineePeerAddress.HumanReadable());
            array.Add(this.mComplaineePublicKey.RawData);
            array.Add(this.mComplaint.Type);
            array.Add(this.mTimestamp.ToBinary());
            */
            return array;
        }

        public static Plaint DeserializeFromDatabase(ArrayList array)
        {
         /*   if (array == null) throw new ArgumentNullException("array");
            try
            {
                long complainerUserId = (long)array[0];
                Ranker complainerRanker = new Ranker((int)array[1]);
                PeerAddress complainerPeerAddress = new PeerAddress((HashKey)array[2]);
                PublicKey complainerPublicKey = new PublicKey((byte[])array[3]);

                long complaineeUserId = (long)array[4];
                Ranker complaineeRanker = new Ranker((int)array[5]);
                PeerAddress complaineePeerAddress = new PeerAddress((HashKey)array[6]);
                PublicKey complaineePublicKey = new PublicKey((byte[])array[7]);

                ComplaintProperties complaint = new ComplaintProperties((ComplaintTypes)array[8]);
                DateTime timestamp = new DateTime((long)array[9]);

                return new Plaint(complainerUserId, complainerRanker, complainerPeerAddress, complainerPublicKey,
                                  complaineeUserId, complaineeRanker, complaineePeerAddress, complaineePublicKey, complaint, timestamp);
            }
            catch (Exception)
            {
                return null;
            }*/
            return null;
        }

        internal long ComplainerUserId { get { return this.mComplainerUserId; } }
        internal PeerAddress ComplainerPeerAddress { get { return this.mComplainerPeerAddress; } }
        internal PublicKey ComplainerPublicKey { get { return this.mComplainerPublicKey; } }
        internal Ranker ComplainerRanker { get { return this.mComplainerRanker; } }
        internal long ComplaineeUserId { get { return this.mComplaineeUserId; } }
        internal PeerAddress ComplaineePeerAddress { get { return this.mComplaineePeerAddress; } }
        internal PublicKey ComplaineePublicKey { get { return this.mComplaineePublicKey; } }
        internal Ranker ComplaineeRanker { get { return this.mComplaineeRanker; } }
        internal ComplaintProperties Complaint { get { return this.mComplaint; } }
        internal DateTime Timestamp { get { return this.mTimestamp; } }

    }
     
}
