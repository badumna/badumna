﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TrustedComponent.BlackListing
{
    class Ranker : IRanker
    {
        private Rank mRank;
        private Threshold mThreshold;
        private Range mTrustedPeerRange;
        private Range mNormalPeerRange;
        private Range mMaliciousPeerRange;

        public Ranker()
        {
            this.mThreshold = new Threshold();
            this.mRank = new Rank();
            if (this.mRank.RankValue > this.mThreshold.Maximum || this.mRank.RankValue < this.mThreshold.Minimum) throw new ArgumentOutOfRangeException("rank");
            this.mTrustedPeerRange = new Range(this.mThreshold.Maximum - this.mThreshold.UpperDelta + 1, this.mThreshold.Maximum);
            this.mNormalPeerRange = new Range(this.mThreshold.Minimum + this.mThreshold.LowerDelta, this.mThreshold.Maximum - this.mThreshold.UpperDelta);
            this.mMaliciousPeerRange = new Range(this.mThreshold.Minimum, this.mThreshold.Minimum + this.mThreshold.LowerDelta - 1);
        }

        public Ranker(int rank)
        {
            this.mThreshold = new Threshold();
            this.mRank = new Rank(rank);
            if (this.mRank.RankValue > this.mThreshold.Maximum || this.mRank.RankValue < this.mThreshold.Minimum) throw new ArgumentOutOfRangeException("rank");
            this.mTrustedPeerRange = new Range(this.mThreshold.Maximum - this.mThreshold.UpperDelta + 1, this.mThreshold.Maximum);
            this.mNormalPeerRange = new Range(this.mThreshold.Minimum + this.mThreshold.LowerDelta, this.mThreshold.Maximum - this.mThreshold.UpperDelta);
            this.mMaliciousPeerRange = new Range(this.mThreshold.Minimum, this.mThreshold.Minimum + this.mThreshold.LowerDelta - 1);
        }

        public Ranker(Threshold threshold, Rank rank)
        {
            if (threshold == null) throw new ArgumentNullException("threshold");
            if (rank == null) throw new ArgumentNullException("rank");
            if (rank.RankValue > threshold.Maximum || rank.RankValue < threshold.Minimum) throw new ArgumentOutOfRangeException("rank");

            this.mThreshold = threshold;
            this.mRank = rank;
            this.mTrustedPeerRange = new Range(this.mThreshold.Maximum - this.mThreshold.UpperDelta + 1, this.mThreshold.Maximum);
            this.mNormalPeerRange = new Range(this.mThreshold.Minimum + this.mThreshold.LowerDelta, this.mThreshold.Maximum - this.mThreshold.UpperDelta);
            this.mMaliciousPeerRange = new Range(this.mThreshold.Minimum, this.mThreshold.Minimum + this.mThreshold.LowerDelta - 1);
        }

        public bool IsTrustedPeer
        {
            get
            {
                if (this.mRank.RankValue <= this.mTrustedPeerRange.UpperBorder && this.mRank.RankValue >= this.mTrustedPeerRange.LowerBorder)
                    return true;
                else
                    return false;
            }
        }

        public bool IsNormalPeer
        {
            get
            {
                if (this.mRank.RankValue <= this.mNormalPeerRange.UpperBorder && this.mRank.RankValue >= this.mNormalPeerRange.LowerBorder)
                    return true;
                else
                    return false;
            }
        }

        public bool IsMaliciousPeer
        {
            get
            {
                if (this.mRank.RankValue <= this.mMaliciousPeerRange.UpperBorder && this.mRank.RankValue >= this.mMaliciousPeerRange.LowerBorder)
                    return true;
                else
                    return false;
            }
        }

        public Range TrustedPeerRange { get { return this.mTrustedPeerRange; } }
        public Range NormalPeerRange { get { return this.mNormalPeerRange; } }
        public Range MaliciousPeerrange { get { return this.mMaliciousPeerRange; } }

        public void IncreaseRank(Rank rank)
        {
            this.mRank.Increase(rank.RankValue);
        }

        public void DecreaseRank(Rank rank)
        {
            this.mRank.Decrease(rank.RankValue);
        }

        public void VoidRank()
        {
            this.mRank.Void();
        }

        public int Rank { get { return this.mRank.RankValue; } }

        public Threshold ThresholdLevel
        {
            get { return this.mThreshold; }
            set
            {
                if (value == null) throw new ArgumentNullException("value");
                this.mThreshold = value;
            }
        }
    }
}
