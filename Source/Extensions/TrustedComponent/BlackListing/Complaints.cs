﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;

using Badumna.Core;
using Badumna.Security;

namespace TrustedComponent.BlackListing
{
    class Complaints
    {
        private ArrayList mComplaints;
        private readonly string mComplaintsName;

        public Complaints()
        {
            this.mComplaintsName = "complaintsall.dat";
            this.mComplaints = new ArrayList();
        }

        public Complaints(bool from)
        {
            if (from)
            {
                this.mComplaintsName = "complaintsfrom.dat";
            }
            else
            {
                this.mComplaintsName = "complaintsby.dat";
            }
            if ((this.mComplaints = Utility.ReadArray(this.mComplaintsName)) == null)
            {
                this.mComplaints = new ArrayList();
            }
        }

        internal void AddComplaint(Plaint complaint)
        {
            if (complaint == null) throw new ArgumentNullException("complaint");
            this.mComplaints.Add(complaint);
            Utility.WriteArray(this.mComplaintsName, this.mComplaints);
        }

        internal void DeleteComplaints()
        {
            if (this.mComplaints != null)
            {
                this.mComplaints.Clear();
                Utility.DeleteArray(this.mComplaintsName);
            }           
        }        

        public ArrayList SerializeForDatabase()
        {
            ArrayList array = new ArrayList();
            foreach (Plaint plaint in this.mComplaints)
            {
                array.Add(plaint.SerializeForDatabase());
            }
            return array;
        }

        public static Complaints DeserializeFromDatabase(ArrayList array)
        {
            if (array == null) throw new ArgumentNullException("array");

            Complaints complaints = new Complaints(); 
            try
            {
                foreach (ArrayList arr in array)
                {
                    complaints.AddComplaint(Plaint.DeserializeFromDatabase(arr));
                }
                return complaints;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public int GetTotalNumberOfComplaints()
        {
            return this.mComplaints.Count;
        }

        public int GetNumberOfComplaintsByComplainer(long userId)
        {
            int counter = 0;
            foreach (Plaint complaint in this.mComplaints)
            {
                if(complaint.ComplainerUserId.Equals(userId)) counter++;
            }
            return counter;
        }

        public int GetNumberOfComplaintsOfComplainee(long userId)
        {
            int counter = 0;
            foreach (Plaint complaint in this.mComplaints)
            {
                if (complaint.ComplaineeUserId.Equals(userId)) counter++;
            }
            return counter;
        }

        public List<long> GetComplaineesUserNamesByComplainer(long userId)
        {
            List<long> complainees = new List<long>();
            foreach (Plaint complaint in this.mComplaints)
            {
                if (complaint.ComplainerUserId.Equals(userId))
                {
                    if (!complainees.Contains(complaint.ComplaineeUserId))
                    {
                        complainees.Add(complaint.ComplaineeUserId);
                    }
                }
            }
            return complainees;
        }

        public List<long> GetComplainersUserNamesOfComplainee(long userId)
        {
            List<long> complainers = new List<long>();
            foreach (Plaint complaint in this.mComplaints)
            {
                if (complaint.ComplaineeUserId.Equals(userId))
                {
                    if (!complainers.Contains(complaint.ComplainerUserId))
                    {
                        complainers.Add(complaint.ComplaineeUserId);
                    }
                }
            }
            return complainers;
        }       

        public int GetComplainerCurrentRank(long userId)
        {  
            DateTime current = DateTime.MinValue;
            int rank = -10000;
            foreach (Plaint complaint in this.mComplaints)
            {
                if (complaint.ComplainerUserId.Equals(userId))
                {
                    int compareValue = -1;
                    try
                    {
                        compareValue = complaint.Timestamp.CompareTo(current);
                    }
                    catch (ArgumentException)
                    {
                        throw new ArgumentException("compareValue");
                    }
                    if (compareValue >= 0)
                    {
                        current = complaint.Timestamp;
                        rank = complaint.ComplainerRanker.Rank;
                    }
                }
            }
            return rank;
        }

        public Dictionary<int, DateTime> GetComplainerRankHistory(long userId)
        {
            Dictionary<int, DateTime> ranks = new Dictionary<int, DateTime>();
            foreach (Plaint complaint in this.mComplaints)
            {
                if (complaint.ComplainerUserId.Equals(userId))
                {
                    ranks.Add(complaint.ComplainerRanker.Rank, complaint.Timestamp);
                }
            }
            return ranks;
        }

        public int GetComplaineeCurrentRank(long userId)
        {
            DateTime current = DateTime.MinValue;
            int rank = -10000;
            foreach (Plaint complaint in this.mComplaints)
            {
                if (complaint.ComplaineeUserId.Equals(userId))
                {
                    int compareValue = -1;
                    try
                    {
                        compareValue = complaint.Timestamp.CompareTo(current);
                    }
                    catch (ArgumentException)
                    {
                        throw new ArgumentException("compareValue");
                    }
                    if (compareValue >= 0)
                    {
                        current = complaint.Timestamp;
                        rank = complaint.ComplaineeRanker.Rank;
                    }
                }
            }
            return rank;
        }

        public Dictionary<int, DateTime> GetComplaineeRankHistory(long userId)
        {
            Dictionary<int, DateTime> ranks = new Dictionary<int, DateTime>();
            foreach (Plaint complaint in this.mComplaints)
            {
                if (complaint.ComplaineeUserId.Equals(userId))
                {
                    ranks.Add(complaint.ComplaineeRanker.Rank, complaint.Timestamp);
                }
            }
            return ranks;
        }


        public List<PeerAddress> GetComplainerPeerAddresses(long userId)
        {
            List<PeerAddress> peerAddresses = new List<PeerAddress>();
            foreach (Plaint complaint in this.mComplaints)
            {
                if (complaint.ComplainerUserId.Equals(userId))
                {
                    peerAddresses.Add(complaint.ComplainerPeerAddress);
                }
            }
            return peerAddresses;
        }

        public List<PeerAddress> GetComplaineePeerAddresses(long userId)
        {
            List<PeerAddress> peerAddresses = new List<PeerAddress>();
            foreach (Plaint complaint in this.mComplaints)
            {
                if (complaint.ComplaineeUserId.Equals(userId))
                {
                    peerAddresses.Add(complaint.ComplaineePeerAddress);
                }
            }
            return peerAddresses;
        }

        public List<PublicKey> GetComplainerPublicKeys(long userId)
        {
            List<PublicKey> publicKeys = new List<PublicKey>();
            foreach (Plaint complaint in this.mComplaints)
            {
                if (complaint.ComplainerUserId.Equals(userId))
                {
                    publicKeys.Add(complaint.ComplainerPublicKey);
                }
            }
            return publicKeys;
        }

        public List<PublicKey> GetComplaineePublicKeys(long userId)
        {
            List<PublicKey> publicKeys = new List<PublicKey>();
            foreach (Plaint complaint in this.mComplaints)
            {
                if (complaint.ComplaineeUserId.Equals(userId))
                {
                    publicKeys.Add(complaint.ComplaineePublicKey);
                }
            }
            return publicKeys;
        }

        public List<ComplaintProperties> GetAllComplaintsByComplainer(long userId)
        {
            List<ComplaintProperties> complaintProperties = new List<ComplaintProperties>();
            foreach (Plaint complaint in this.mComplaints)
            {
                if (complaint.ComplainerUserId.Equals(userId))
                {
                    complaintProperties.Add(complaint.Complaint);
                }
            }
            return complaintProperties;
        }

        public ComplaintProperties GetLastComplaintByComplainer(long userId)
        {
            DateTime current = DateTime.MinValue;
            ComplaintProperties complaintProperties = null;
            foreach (Plaint complaint in this.mComplaints)
            {
                if (complaint.ComplainerUserId.Equals(userId))
                {
                    int compareValue = -1;
                    try
                    {
                        compareValue = complaint.Timestamp.CompareTo(current);
                    }
                    catch (ArgumentException)
                    {
                        throw new ArgumentException("compareValue");
                    }
                    if (compareValue >= 0)
                    {
                        current = complaint.Timestamp;
                        complaintProperties = complaint.Complaint;
                    }
                }
            }
            return complaintProperties;
        }

        public List<ComplaintProperties> GetAllComplaintsOfComplainee(long userId)
        {
            List<ComplaintProperties> complaintProperties = new List<ComplaintProperties>();
            foreach (Plaint complaint in this.mComplaints)
            {
                if (complaint.ComplaineeUserId.Equals(userId))
                {
                    complaintProperties.Add(complaint.Complaint);
                }
            }
            return complaintProperties;
        }

        public ComplaintProperties GetLastComplaintOfComplainee(long userId)
        {
            DateTime current = DateTime.MinValue;
            ComplaintProperties complaintProperties = null;
            foreach (Plaint complaint in this.mComplaints)
            {
                if (complaint.ComplaineeUserId.Equals(userId))
                {
                    int compareValue = -1;
                    try
                    {
                        compareValue = complaint.Timestamp.CompareTo(current);
                    }
                    catch (ArgumentException)
                    {
                        throw new ArgumentException("compareValue");
                    }
                    if (compareValue >= 0)
                    {
                        current = complaint.Timestamp;
                        complaintProperties = complaint.Complaint;
                    }
                }
            }
            return complaintProperties;
        }
    }
}
