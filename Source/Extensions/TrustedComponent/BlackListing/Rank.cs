﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TrustedComponent.BlackListing
{
    class Rank
    {
        private int mRank;

        public Rank()
        {
            this.mRank = 0;
        }

        public Rank(int rank)
        {
            this.mRank = rank;
        }

        public void Increase(int rank)
        {
            if (rank <= 0) throw new ArgumentOutOfRangeException("rank <= 0");
            this.mRank += rank;
        }

        public void Decrease(int rank)
        {
            if (rank <= 0) throw new ArgumentOutOfRangeException("rank <= 0");
            this.mRank -= rank;
        }

        public void Void()
        {
            this.mRank = 0;
        }

        public int RankValue { get { return this.mRank; } }
    }
}
