﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TrustedComponent.BlackListing
{
    class Threshold
    {
        private int mMininum;
        private int mMaximun;
        private int mUpperDelta;
        private int mLowerDelta;

        public Threshold()
        {
            this.mMaximun = 100;
            this.mUpperDelta = 10;
            this.mLowerDelta = 10;
            this.mMininum = -100;
        }

        public Threshold(int maximum, int upperDelta, int lowerDelta, int minimum)
        {
            if (upperDelta <= 0) throw new ArgumentOutOfRangeException("upperDelta <= 0");
            if (lowerDelta <= 0) throw new ArgumentOutOfRangeException("lowerDelta <= 0");
            if (maximum <= minimum) throw new ArgumentOutOfRangeException("maximum <= minimum");
            if (maximum - upperDelta <= minimum + lowerDelta) throw new ArgumentOutOfRangeException("maximum - upperDelta <= minimum + lowerDelta");

            this.mMaximun = maximum;
            this.mUpperDelta = upperDelta;
            this.mLowerDelta = lowerDelta;
            this.mMininum = minimum;
        }

        public int Maximum { get { return this.mMaximun; } }
        public int Minimum { get { return this.mMininum; } }
        public int UpperDelta { get { return this.mUpperDelta; } }
        public int LowerDelta { get { return this.mLowerDelta; } }
    }
}
