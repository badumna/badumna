﻿using System;

using Badumna.Security;
using Badumna.Core;

namespace TrustedComponent
{
    interface IComplaintTracker
    {
        void ProcessComplaintAbout(ICertificateToken complainerCertificate, ICertificateToken complaineeCertificate, ComplaintProperties complaint);
        void ProcessComplaintBy(ICertificateToken complainerCertificate, ICertificateToken complaineeCertificate, ComplaintProperties complaint);
    }
}
