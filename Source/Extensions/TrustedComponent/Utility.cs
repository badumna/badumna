﻿using System;
using System.Collections;
using System.Text;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;

namespace TrustedComponent
{
    class Utility
    {
        internal static void WriteArray(string fileName, ArrayList array)
        {
            if (fileName == null) throw new ArgumentNullException("fileName");
            FileStream fs = new FileStream(fileName, FileMode.Create);
            BinaryFormatter formatter = new BinaryFormatter();
            try
            {
                formatter.Serialize(fs, array);
            }
            catch (SerializationException e)
            {
                Console.WriteLine("Failed to serialize. Reason: " + e.Message);
                throw;
            }
            finally
            {
                fs.Close();
            }
        }

        internal static ArrayList ReadArray(string fileName)
        {
            if (fileName == null) throw new ArgumentNullException("fileName");
            ArrayList array = new ArrayList();
            if (File.Exists(fileName))
            {
                FileStream fs = new FileStream(fileName, FileMode.Open);
                try
                {
                    BinaryFormatter formatter = new BinaryFormatter();
                    array = (ArrayList)formatter.Deserialize(fs);
                }
                catch (SerializationException e)
                {
                    Console.WriteLine("Failed to deserialize. Reason: " + e.Message);
                    throw;
                }
                finally
                {
                    fs.Close();
                }
            }
            return array;
        }

        internal static void DeleteArray(string fileName)
        {
            if (fileName == null) throw new ArgumentNullException("fileName");

            File.Delete(fileName);
        }
    }
}
