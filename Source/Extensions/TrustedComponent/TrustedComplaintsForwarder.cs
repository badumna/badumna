﻿using System;
using System.Collections.Generic;
using System.Text;

using Badumna.Security;
using Badumna.Core;
using Badumna.Utilities;
using Badumna.Transport;
using Badumna.DistributedHashTable;

namespace TrustedComponent
{
    class TrustedComplaintsForwarder : ComplaintForwarder
    {
        private IComplaintTracker mTracker;

        public TrustedComplaintsForwarder(IComplaintTracker tracker, TransportProtocol parent, INetworkAddressProvider addressProvider, TokenCache tokens, INetworkConnectivityReporter connectivityReporter)
            : base(parent, addressProvider, tokens)
        {
            this.mTracker = tracker;

            connectivityReporter.NetworkOnlineEvent += this.InformProxyOfAddress;
            if (connectivityReporter.Status == ConnectivityStatus.Online)
            {
                this.InformProxyOfAddress();
            }
        }

        private void InformProxyOfAddress()
        {
            foreach (PeerAddress proxyAddress in this.mProxyAddresses)
            {
                this.AddProxyAddress(this.addressProvider.PublicAddress, proxyAddress);
            }
        }

        protected override void ForwardComplaint(ICertificateToken complaineeCertificate, ICertificateToken complainerCertificate, ComplaintProperties complaint)
        {
            HashKey complaineeKey = HashKey.Hash(BitConverter.GetBytes(complaineeCertificate.UserId));
            HashKey complainerKey = HashKey.Hash(BitConverter.GetBytes(complainerCertificate.UserId));

            this.SendComplaintTo(complaineeKey, complaineeCertificate, complainerCertificate, complaint);
            this.SendComplaintTo(complainerKey, complaineeCertificate, complainerCertificate, complaint);
        }

        private void SendComplaintTo(HashKey key, ICertificateToken complaineeCertificate, ICertificateToken complainerCertificate, ComplaintProperties complaint)
        {
            // TODO : Actually route the request on the dht
            this.mTracker.ProcessComplaintBy(complaineeCertificate, complainerCertificate, complaint);
            this.mTracker.ProcessComplaintAbout(complaineeCertificate, complainerCertificate, complaint);
        }
    }
}
