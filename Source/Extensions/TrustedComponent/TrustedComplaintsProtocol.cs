﻿using System;
using System.Collections.Generic;
using System.Text;

using Badumna.Security;
using Badumna.Core;
using Badumna.Utilities;
using Badumna.Transport;
using Badumna.DistributedHashTable;

namespace TrustedComponent
{
    class TrustedComplaintsProtocol : ComplaintForwarder
    {
        private IComplaintTracker mTracker;

        public TrustedComplaintsProtocol(ComplaintTracker tracker, TransportProtocol parent)
            : base(parent)
        {
            this.mTracker = tracker;
        }


        protected override void ForwardComplaint(byte[] serializedComplaineeCertificate, byte[] serializedComplainerCertificate, ComplaintProperties complaint)
        {
            CertificateToken complaineeCertificate = CertificateToken.DeserializeCerttificate(serializedComplaineeCertificate);
            CertificateToken complainerCertificate = CertificateToken.DeserializeCerttificate(serializedComplainerCertificate);

            HashKey complaineeKey = HashKey.Hash(complaineeCertificate.UserName);
            HashKey complainerKey = HashKey.Hash(complainerCertificate.UserName);

            this.SendComplaintTo(complaineeKey, serializedComplaineeCertificate, serializedComplainerCertificate, complaint);
            this.SendComplaintTo(complainerKey, serializedComplaineeCertificate, serializedComplainerCertificate, complaint);
        }

        private void SendComplaintTo(HashKey key, byte[] serializedComplaineeCertificate, byte[] serializedComplainerCertificate, ComplaintProperties complaint)
        {
        /*        DhtEnvelope envelope = this.GetMessageFor(key, QualityOfService.Reliable);

            this.RemoteCall(envelope, this.ProcessComplaint, serializedComplaineeCertificate, serializedComplainerCertificate, complaint);
            this.SendMessage(envelope);
         */
        }
        /*
        protected void ProcessComplaint(byte[] serializedComplaineeCertificate, byte[] serializedComplainerCertificate, ComplaintProperties complaint)
        {
            CertificateToken complaineeCertificate = CertificateToken.DeserializeCerttificate(serializedComplaineeCertificate);
            CertificateToken complainerCertificate = CertificateToken.DeserializeCerttificate(serializedComplainerCertificate);

            if (this.CurrentEnvelope.DestinationKey == HashKey.Hash(complaineeCertificate.UserName))
            {
                this.mTracker.ProcessComplaintBy(complaineeCertificate, complainerCertificate, complaint);
            }
            else if (this.CurrentEnvelope.DestinationKey == HashKey.Hash(complainerCertificate.UserName))
            {
                this.mTracker.ProcessComplaintAbout(complaineeCertificate, complainerCertificate, complaint);
            }
            else
            {
                Logger.TraceError("Received complaint at key {0} which is not valid for either complainee or complainer.", this.CurrentEnvelope.DestinationKey);
            }
        }
         */
    }
}
