﻿//-----------------------------------------------------------------------
// <copyright file="SerializationTests.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace Utilities
{
    using System.Collections.Generic;
    using NUnit.Framework;

    [TestFixture]
    public class SerializationTests
    {
        [TestCase(0)]
        [TestCase(1)]
        [TestCase(123)]
        [TestCase(-0)]
        [TestCase(-1)]
        [TestCase(-123)]
        [TestCase(int.MaxValue)]
        [TestCase(int.MinValue)]
        public void IntegersSerializeCorrectly(int a)
        {
            byte[] b = Serialization.Serialize(a);
            int c = (int)Serialization.Deserialize(b);
            Assert.AreEqual(a, c);
        }

        [TestCase("")]
        [TestCase("a")]
        [TestCase("a very long string with whitespace")]
        [TestCase("a very long string with whitespace \n and other \t gubbins")]
        public void StringsSerializeCorrectly(string a)
        {
            byte[] b = Serialization.Serialize(a);
            string c = (string)Serialization.Deserialize(b);
            Assert.AreEqual(a, c);
        }

        [Test]
        public void IntegerListsSerializeCorrectly()
        {
            List<int> a = new List<int> { 1, 2, 3 };
            byte[] b = Serialization.Serialize(a);
            List<int> c = (List<int>)Serialization.Deserialize(b);
            Assert.AreEqual(a, c);
        }

        [Test]
        public void IntegerArraysSerializeCorrectly()
        {
            int[] a = new int[] { 0, 1, 2 };
            byte[] b = Serialization.Serialize(a);
            int[] c = (int[])Serialization.Deserialize(b);
            Assert.AreEqual(a, c);
        }
    }
}
