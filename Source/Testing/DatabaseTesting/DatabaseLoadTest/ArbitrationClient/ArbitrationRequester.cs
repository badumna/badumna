﻿//-----------------------------------------------------------------------
// <copyright file="ArbitrationRequester.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace ArbitrationClient
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Timers;
    using Badumna.Arbitration;
    using Utilities;

    /// <summary>
    /// Sends arbitration requsts, listens for replies, and gathers performance statistics.
    /// </summary>
    public class ArbitrationRequester
    {
        /// <summary>
        /// The arbitrator to use.
        /// </summary>
        private IArbitrator arbitrator = null;

        /// <summary>
        /// Timer used to schedule regular arbitration events.
        /// </summary>
        private Timer arbitrationScheduler;

        /// <summary>
        /// Track incrementallly increasing message IDs.
        /// </summary>
        private int nextMessageId = 0;        

        /// <summary>
        /// Used to track mean RTT.
        /// </summary>
        private float meanRTT = 0f;

        /// <summary>
        /// Used to track last hundred RTTs.
        /// </summary>
        private int[] lastHundredRTTs = new int[100];

        /// <summary>
        /// Dictionary to hold timestamps when messages were sent so RTTs can be calculated.
        /// </summary>
        private Dictionary<int, DateTime> sendTimes = new Dictionary<int, DateTime>();

        /// <summary>
        /// Initializes a new instance of the ArbitrationRequester class.
        /// </summary>
        /// <param name="arbitratorToUse">The arbitrator to request from.</param>
        public ArbitrationRequester(IArbitrator arbitratorToUse)
        {
            Debug.Assert(arbitratorToUse != null, "ArbitrationRequester must be constructed with IArbitrator");
            this.arbitrator = arbitratorToUse;
            this.arbitrator.Connect();
            this.arbitrator.ServerEvent = delegate(byte[] message)
            {
                this.ReceiveReply(message);
            };
        }

        /// <summary>
        /// Gets the id to use for the next message.
        /// </summary>
        private int NextMessageId
        {
            get
            {
                return this.nextMessageId++;
            }
        }

        /// <summary>
        /// Gets the mean RTT of the last hundered requests.
        /// </summary>
        private int MeanRTTLastHundred
        {
            get
            {
                return (int)this.lastHundredRTTs.Average();
            }
        }

        /// <summary>
        /// Schedule arbitration requests.
        /// </summary>
        /// <param name="interval">Number of milliseconds between requests.</param>
        public void ScheduleRequests(int interval)
        {
            Console.WriteLine("Scheduling arbitration requests.");
            this.arbitrationScheduler = new Timer(interval);
            this.arbitrationScheduler.Elapsed += new System.Timers.ElapsedEventHandler(this.SendRequest);
            this.arbitrationScheduler.Enabled = true;
        }

        /// <summary>
        /// Pause arbitration requests.
        /// </summary>
        public void PauseRequests()
        {
            Console.WriteLine("Pausing");
            this.arbitrationScheduler.Enabled = false;
        }

        /// <summary>
        /// Resume arbitration requests.
        /// </summary>
        public void ResumeRequests()
        {
            Console.WriteLine("Resuming");
            this.arbitrationScheduler.Enabled = true;
        }

        /// <summary>
        /// Toggles arbitration requests on or off.
        /// </summary>
        public void ToggleRequests()
        {
            if (this.arbitrationScheduler.Enabled)
            {
                this.PauseRequests();
            }
            else
            {
                this.ResumeRequests();
            }
        }

        /// <summary>
        /// Stop arbitration requests, and stop listening for replies.
        /// </summary>
        public void Shutdown()
        {
            this.PauseRequests();
            this.arbitrator.ServerEvent = null;
            this.arbitrator = null;
        }

        /// <summary>
        /// Print performance statistics to the console.
        /// </summary>
        public void PrintPerformanceStats()
        {
            Console.WriteLine(
                "Mean RTT (overall / last 100 requests): {0} / {1}",
                this.meanRTT,
                this.MeanRTTLastHundred);
        }

        /// <summary>
        /// Send an arbitration request to the arbitrator.
        /// </summary>
        /// <param name="sender">The timer that scheduled this call.</param>
        /// <param name="e">Elapsed event args.</param>
        private void SendRequest(object sender, System.Timers.ElapsedEventArgs e)
        {
            Random random = new Random();
            int playerId1 = random.Next(101);
            int playerId2 = playerId1;
            while (playerId2 == playerId1)
            {
                playerId2 = random.Next(101);
            }

            if (playerId2 < playerId1)
            {
                int temp = playerId1;
                playerId1 = playerId2;
                playerId2 = temp;
            }

            int messageId = this.NextMessageId;
            int[] messageData = new int[] { messageId, playerId1, playerId2 };
            Console.WriteLine(
                "Sending arbitration request {0} ({1} v {2})",
                messageData[0],
                messageData[1],
                messageData[2]);
            byte[] message = Serialization.Serialize(messageData);
            this.sendTimes[messageId] = DateTime.Now;
            this.arbitrator.SendEvent(message);
        }

        /// <summary>
        /// Handle replies sent from the arbitrator.
        /// </summary>
        /// <param name="message">The message received.</param>
        private void ReceiveReply(byte[] message)
        {
            int id = (int)Serialization.Deserialize(message);
            TimeSpan rtt = DateTime.Now - this.sendTimes[id];
            this.sendTimes.Remove(id);
            this.meanRTT = ((id * this.meanRTT) + rtt.Milliseconds) / (id + 1f);
            this.lastHundredRTTs[id % 100] = rtt.Milliseconds;
            Console.WriteLine(
                "Received arbitration message: {0}, RTT: {1}, Mean RTT: {2} ({3})",
                id,
                rtt.Milliseconds,
                this.meanRTT,
                this.MeanRTTLastHundred);
        }
    }
}
