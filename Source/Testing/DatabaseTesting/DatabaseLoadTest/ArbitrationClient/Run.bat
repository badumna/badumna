REM Script to start server and client

REM Start the server
cd ..\ArbitrationServer\
start "" ArbitrationServer.exe

REM Wait five seconds
ping 127.0.0.1 -n 5 > nul

REM Start the client
cd ..\ArbitrationClient\
start "" ArbitrationClient.exe