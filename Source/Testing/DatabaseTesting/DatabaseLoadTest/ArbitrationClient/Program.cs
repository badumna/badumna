﻿//-----------------------------------------------------------------------
// <copyright file="Program.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace ArbitrationClient
{
    using System;
    using System.Timers;
    using Badumna;
    using Badumna.Arbitration;

    /// <summary>
    /// Class to hold main routine.
    /// </summary>
    internal class Program
    {
        /// <summary>
        /// Timer used to schedule regular processing (the 'tick').
        /// </summary>
        private static Timer ticker = new Timer(1000 / 60);

        /////// <summary>
        /////// The arbitrator to use for arbitration.
        /////// </summary>
        ////private static Badumna.Arbitration.IArbitrator arbitrator = null;

        /// <summary>
        /// The entry point for the application.
        /// </summary>
        /// <param name="args">Command line arguments.</param>
        private static void Main(string[] args)
        {
            Console.WriteLine("Initializing network.");
            INetworkFacade networkFacade = NetworkFacade.Create(new Options());
            Console.Write("Logging in...");
            if (networkFacade.Login())
            {
                Console.WriteLine("done.");

                // Get the arbitrator
                IArbitrator arbitrator = null;
                try
                {
                    arbitrator = networkFacade.GetArbitrator("spammee");
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Could not find arbitrator ({0}).", ex.Message);
                }

                // Create arbitration requester
                ArbitrationRequester requester = new ArbitrationRequester(arbitrator);
                
                Console.WriteLine(string.Empty);
                Console.WriteLine("------------------------------------------");
                Console.WriteLine("Instructions:");
                Console.WriteLine(" Q : Quit");
                Console.WriteLine(" N : Network status");
                Console.WriteLine(" Space : Pause/Resume arbitration requests");
                Console.WriteLine("------------------------------------------");
                Console.WriteLine(string.Empty);

                // Set up regular processing
                ticker.Elapsed += delegate { networkFacade.ProcessNetworkState(); };
                ticker.Enabled = true;

                // Schedule regular arbitration requests
                requester.ScheduleRequests(10);

                // Respond to user input
                bool exitRequested = false;
                while (!exitRequested)
                {
                    ConsoleKeyInfo keyInfo = Console.ReadKey(true);
                    if (keyInfo.KeyChar.ToString().ToLower().Equals("q"))
                    {
                        exitRequested = true;
                    }
                    else if (keyInfo.KeyChar.ToString().ToLower().Equals("n"))
                    {
                        NetworkStatus status = networkFacade.GetNetworkStatus();
                        Console.WriteLine("-------------------");
                        Console.WriteLine(status.ToString());
                        Console.WriteLine("-------------------");
                    }
                    else if (keyInfo.Key == ConsoleKey.Spacebar)
                    {
                        requester.ToggleRequests();
                    }
                }

                // Halt scheduled processing.
                ticker.Enabled = false;
                requester.Shutdown();

                Console.WriteLine("Logging out.");
                networkFacade.Logout();
            }
            else
            {
                Console.WriteLine("failed :(");
            }

            networkFacade.Shutdown();
            Console.WriteLine("Program terminated. Hit a key to close window.");
            Console.ReadKey();
        }

        /// <summary>
        /// Handle arbitration messages received from arbitration server
        /// </summary>
        /// <param name="message">The message received.</param>
        private static void HandleArbitrationEvent(byte[] message)
        {
            Console.WriteLine("Received arbitration message: {0}", message.ToString());
        }
    }
}
