﻿//-----------------------------------------------------------------------
// <copyright file="Serialization.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace Utilities
{
    using System;
    using System.IO;
    using System.Runtime.Serialization.Formatters.Binary;

    /// <summary>
    /// Utility class providing static serialization methods.
    /// </summary>
    public class Serialization
    {
        /// <summary>
        /// Serializes an object to a byte array.
        /// </summary>
        /// <param name="objectToSerialize">The object to serialize.</param>
        /// <returns>A byte array.</returns>
        public static byte[] Serialize(object objectToSerialize)
        {
            MemoryStream memoryStream = new MemoryStream();
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            binaryFormatter.Serialize(memoryStream, objectToSerialize);
            return memoryStream.ToArray();
        }

        /// <summary>
        /// Deserializers an object from a byte array.
        /// </summary>
        /// <param name="byteArrayToDeserialize">The byte array to deserialize.</param>
        /// <returns>The deserialized object.</returns>
        public static object Deserialize(byte[] byteArrayToDeserialize)
        {
            MemoryStream memoryStream = new MemoryStream(byteArrayToDeserialize);
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            memoryStream.Position = 0;
            return binaryFormatter.Deserialize(memoryStream);
        }
    }
}
