﻿//-----------------------------------------------------------------------
// <copyright file="Program.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace TestTests
{
    using System;

    /// <summary>
    /// Class to hold main routine.
    /// </summary>
    internal class Program
    {
        /// <summary>
        /// Entry point for application.
        /// </summary>
        /// <param name="args"></param>
        [STAThread]
        private static void Main(string[] args)
        {
            ////AppDomain.CurrentDomain.ExecuteAssembly(
            ////       @"C:\Program Files\NUnit 2.5.5\bin\net-2.0\nunit-console.exe",
            ////       null,
            ////       new string[] { Assembly.GetExecutingAssembly().Location });

            Console.WriteLine("Foo");
            Console.ReadLine();
        }
    }
}
