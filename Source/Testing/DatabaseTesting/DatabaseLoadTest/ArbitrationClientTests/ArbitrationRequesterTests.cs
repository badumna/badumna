﻿//-----------------------------------------------------------------------
// <copyright file="ArbitrationRequesterTests.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace ArbitrationClient
{
    using Badumna.Arbitration;
    using NUnit.Framework;
    using Rhino.Mocks;

    [TestFixture]
    public class ArbitrationRequesterTests
    {
        private ArbitrationRequester arbitrationRequester;
        private IArbitrator mockArbitrator;

        [TestFixtureSetUp]
        public void SetUp()
        {
            this.mockArbitrator = MockRepository.GenerateMock<IArbitrator>();
            this.arbitrationRequester = new ArbitrationRequester(this.mockArbitrator);
        }

        [TestFixtureTearDown]
        public void TearDown()
        {
            this.mockArbitrator = null;
            this.arbitrationRequester = null;
        }

        [Test]
        public void RequesterSendsRequests()
        {
            this.mockArbitrator.Expect(a => a.SendEvent(null)).Repeat.Times(1);

            this.arbitrationRequester.ScheduleRequests(100);
            System.Threading.Thread.Sleep(5000);

            this.mockArbitrator.VerifyAllExpectations();
            this.mockArbitrator.BackToRecord();
        }
    }
}
