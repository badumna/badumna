﻿//-----------------------------------------------------------------------
// <copyright file="ArbitrationProcess.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace ArbitrationServer
{
    using System; 
    using System.IO;

    using Badumna;
    using GermHarness;

    /// <summary>
    /// Hosted process to provide test arbitration service to a Badumna network.
    /// </summary>
    internal class ArbitrationProcess : IHostedProcess
    {
        /// <summary>
        /// The arbitrator.
        /// </summary>
        /// <param name="arguments"></param>
        private Arbitrator arbitrator;

        #region IHostedProcess Members

        /// <summary>
        /// Called by process host to initialize the process.
        /// </summary>
        /// <param name="arguments">Command line arguments passed by process host.</param>
        public void OnInitialize(string[] arguments)
        {
            this.arbitrator = new Arbitrator(/*this.connectionString*/);
            this.arbitrator.OnInitialize();
        }

        /// <summary>
        /// Called periodically by the process host, to trigger regularly scheduled processing.
        /// </summary>
        /// <param name="delayMilliseconds">Milliseconds since last called.</param>
        /// <returns>Flag indicating success or errors.</returns>
        public bool OnPerformRegularTasks(int delayMilliseconds)
        {
            return true;
        }

        /// <summary>
        /// Called by the process host to pass a request.
        /// Does nothing, but is required by interface.
        /// </summary>
        /// <param name="requestType">Type of the request (usually cast from enum)</param>
        /// <param name="request">The request that the process should know how to handle.</param>
        /// <returns>Response for host.</returns>
        public byte[] OnProcessRequest(int requestType, byte[] request)
        {
            return null;
        }

        /// <summary>
        /// Called by the process host on shutdown.
        /// </summary>
        public void OnShutdown()
        {
            this.arbitrator.OnShutdown();
        }

        /// <summary>
        /// Called by process host on start.
        /// </summary>
        /// <returns>Success flag (always true at present).</returns>
        public bool OnStart()
        {
            // registering the ArbitrationEventHandler
            NetworkFacade.Instance.RegisterArbitrationHandler(
                this.arbitrator.HandleClientEvent,
                TimeSpan.FromMinutes(10),
                this.arbitrator.HandleClientDisconnect);
            Console.WriteLine("The arbitrator is in session.");
            return true;
        }

        #endregion
    }
}
