﻿//-----------------------------------------------------------------------
// <copyright file="Arbitrator.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace ArbitrationServer
{
    using System;
    using Badumna;
    using Utilities;

    /// <summary>
    /// Arbitrator to make decisions on client requests.
    /// </summary>
    internal class Arbitrator
    {
        /// <summary>
        /// Database manager to encapsulate database access.
        /// </summary>
        private DatabaseManager databaseManager;

        /// <summary>
        /// Handle Arbitration requests from clients
        /// </summary>
        /// <param name="sessionId">TODO: Work out how/why session IDs are used.</param>
        /// <param name="message">The event details</param>
        public void HandleClientEvent(int sessionId, byte[] message)
        {
            int[] messageData = (int[])Serialization.Deserialize(message);
            Console.WriteLine("Received client event {0} from session id: {1}", messageData[0], sessionId);
            this.databaseManager.Arbitrate(messageData[1], messageData[2]);
            byte[] replyMessage = Serialization.Serialize(messageData[0]);
            this.SendServerEvent(sessionId, replyMessage);
        }

        /// <summary>
        /// Handle client disconnections.
        /// This method is registered with the network facade, and will be called when clients disconnect.
        /// </summary>
        /// <param name="sessionId">TODO: Work out how session IDs are used.</param>
        public void HandleClientDisconnect(int sessionId)
        {
            Console.WriteLine("Received client disconnect with session id: {0}", sessionId);
        }

        /// <summary>
        /// Send an event from the server to clients. 
        /// </summary>
        /// <param name="destinationSessionId">The session ID (TODO better comment).</param>
        /// <param name="message">The serialized message.</param>
        public void SendServerEvent(int destinationSessionId, byte[] message)
        {
            Console.WriteLine("Sending message to session {0}", destinationSessionId);
            NetworkFacade.Instance.SendServerArbitrationEvent(destinationSessionId, message);
        }

        /// <summary>
        /// Called to initialize arbitrator.
        /// </summary>
        internal void OnInitialize()
        {
            this.databaseManager = new DatabaseManager();
            this.databaseManager.Connect();
            this.databaseManager.PrepareDb();
        }

        /// <summary>
        /// Called to tidy up when finished with arbitrator.
        /// </summary>
        internal void OnShutdown()
        {
            this.databaseManager.Disconnect();
        }
    }
}
