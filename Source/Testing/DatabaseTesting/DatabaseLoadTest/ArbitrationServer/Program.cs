﻿//-----------------------------------------------------------------------
// <copyright file="Program.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace ArbitrationServer
{
    using GermHarness;

    /// <summary>
    /// Program class to hold main routine.
    /// </summary>
    internal class Program
    {
        /// <summary>
        /// Entry point to program.
        /// Creates a PeerHarness and uses it to run the arbitration process.
        /// Command line arguments accepted: [--db_config=database] [--dei_config=deiConfig]
        /// </summary>
        /// <param name="args">Command line arguments.</param>
        internal static void Main(string[] args)
        {
            // Start the arbitration process.
            PeerHarness harness = new PeerHarness();
            harness.Start(args, new ArbitrationProcess());
        }
    }
}
