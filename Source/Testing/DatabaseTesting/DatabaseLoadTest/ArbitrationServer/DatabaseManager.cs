﻿//-----------------------------------------------------------------------
// <copyright file="DatabaseManager.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace ArbitrationServer
{
    using System;
    using System.Data;
    using System.Data.SqlClient;

    /// <summary>
    /// Encapsulates access to the database.
    /// </summary>
    internal class DatabaseManager
    {
        /// <summary>
        /// A connection to the database for re-use.
        /// </summary>
        private IDbConnection dbConnection = null;

        /// <summary>
        /// Connect to the database.
        /// Must be called before arbitration decisions are made.
        /// </summary>
        public void Connect()
        {
            string connectionString = "Data Source=(local)\\SQLEXPRESS;Initial Catalog=ArbitrationTest;Integrated Security=True";
            this.dbConnection = new SqlConnection(connectionString);
            this.dbConnection.Open();
        }

        /// <summary>
        /// Disconect from the database.
        /// Should be called when finished arbitrating.
        /// After being called, no further arbitration decisions can be made before Connect is called again.
        /// </summary>
        public void Disconnect()
        {
            this.dbConnection.Close();
            this.dbConnection = null;
        }

        /// <summary>
        /// Prepare the databsae with test data.
        /// </summary>
        public void PrepareDb()
        {
            Random random = new Random();

            IDbCommand dbCommand = new SqlCommand("Delete From GameData", (SqlConnection)this.dbConnection);
            dbCommand.ExecuteNonQuery();

            for (int i = 1; i <= 100; i++)
            {
                float datum = random.Next(10000) / 10000f;
                string sql = string.Format(
                    "Insert Into GameData (Player, Value) Values ('{0}', '{1}')", i, datum);
                Console.Write("Setting player {0} to {1} ... ", i, datum);
                try
                {
                    dbCommand = new SqlCommand(sql, (SqlConnection)this.dbConnection);
                    dbCommand.ExecuteNonQuery();
                    Console.WriteLine("Done");
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Failed: (" + ex.Message + ")");
                }
                finally
                {
                    dbCommand.Dispose();
                }
            }
        }

        /// <summary>
        /// Make an arbitration decision about two players.
        /// </summary>
        /// <param name="player1">The ID of the first player.</param>
        /// <param name="player2">The ID of the second  player.</param>
        /// <returns>Some rubbish.</returns>
        public bool Arbitrate(int player1, int player2)
        {
            IDbCommand dbCommand = new SqlCommand(
                string.Format("Select * From GameData Where Player = {0}", player1),
                (SqlConnection)this.dbConnection);
            IDataReader dataReader = dbCommand.ExecuteReader();
            float value1 = 0f;
            if (dataReader.Read())
            {
                value1 = (float)dataReader["Value"];
            }

            dataReader.Close();
            dbCommand = new SqlCommand(
                string.Format("Select * From GameData Where Player = {0}", player2),
                (SqlConnection)this.dbConnection);
            dataReader = dbCommand.ExecuteReader();
            float value2 = 0f;
            if (dataReader.Read())
            {
                value2 = (float)dataReader["Value"];
            }

            dataReader.Close();

            Console.Write("{0} > {1} ? ", value1, value2);

            if (value1 > value2)
            {
                Console.WriteLine("Yes - updating.");
                dbCommand = new SqlCommand(
                    string.Format("Update GameData Set Value = '{0}' Where Player = '{1}'", value2, player1),
                    (SqlConnection)this.dbConnection);
                dbCommand.ExecuteNonQuery();
                dbCommand = new SqlCommand(
                    string.Format("Update GameData Set Value = '{0}' Where Player = '{1}'", value1, player2),
                    (SqlConnection)this.dbConnection);
                dbCommand.ExecuteNonQuery();
            }
            else
            {
                Console.WriteLine("No.");
            }

            return true;
        }
    }
}
