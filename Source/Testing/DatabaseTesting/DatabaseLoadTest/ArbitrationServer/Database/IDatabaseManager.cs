﻿//-----------------------------------------------------------------------
// <copyright file="IDatabaseManager.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace ArbitrationServer.Database
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Interface for database managers to support.
    /// </summary>
    internal interface IDatabaseManager : IDisposable
    {
        /// <summary>
        /// Update a record in the database.
        /// </summary>
        /// <param name="userId">The user ID, used to identify the record to be updated.</param>
        /// <param name="table">The table the record is in.</param>
        /// <param name="itemNames">The columns in the record to update.</param>
        /// <param name="values">The new values to update to.</param>
        /// <returns>Whether the update was successful.</returns>
        bool UpdateRecord(int userId, string table, string[] itemNames, object[] values);

        /// <summary>
        /// Insert a record into the database
        /// </summary>
        /// <param name="table">The table to insert the record into.</param>
        /// <param name="itemNames">The columns for which a value is provided.</param>
        /// <param name="values">The values to insert.</param>
        /// <returns>Whether the insertion was successful.</returns>
        bool InsertRecord(string table, string[] itemNames, object[] values);

        /// <summary>
        /// Delete a record from the database.
        /// </summary>
        /// <param name="userId">The user ID used to identify the record.</param>
        /// <param name="table">The table to delete the record from.</param>
        /// <returns>Whether the deletion wsa successful.</returns>
        bool DeleteRecord(int userId, string table);

        /// <summary>
        /// Get a record from the database.
        /// </summary>
        /// <param name="itemName">The column to return.</param>
        /// <param name="table">The table to get from.</param>
        /// <param name="condition">The condition to select record by.</param>
        /// <returns>The data record.</returns>
        object GetRecord(string itemName, string table, string condition);
    }
}
