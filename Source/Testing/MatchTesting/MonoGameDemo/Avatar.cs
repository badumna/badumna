﻿//-----------------------------------------------------------------------
// <copyright file="Avatar.cs" company="Scalify">
//     Copyright (c) 2012 Scalify. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BadumnaCloudDemo
{
    using System;
    using System.Diagnostics;
    using Badumna;
    using Badumna.DataTypes;
    using Color = Microsoft.Xna.Framework.Color;

    // Base class for simulation avatars.
    #region Avatar class
    public class Avatar : IEquatable<Avatar>
    {
        private bool destroyed;
        private Vector3 position;

        private Action<string> onMessage;

        public Avatar(Action<string> onMessage)
        {
            this.CheckCalls();
            this.onMessage = onMessage;
        }

        // All entities must have a position which is automatically replicated.
        [Replicable]
        [Smoothing(Interpolation = 200, Extrapolation = 0)]
        public Vector3 Position
        {
            get
            {
                this.CheckCalls();
                return this.position;
            }
            
            set
            {
                this.CheckCalls();
                this.position = value;
            }
        }

        // The colour of the avatar is marked as a replicable property.
        [Replicable]
        public Color Colour { get; set; }

        [Replicable]
        public void ReceiveMessage(string message)
        {
            this.CheckCalls();
            this.onMessage(message);
        }

        public void Destroy()
        {
            this.destroyed = true;
        }

        public void CheckCalls()
        {
            Debug.Assert(System.Threading.Thread.CurrentThread == Game1.UpdateThread, "Avatar member called outside update thread.");
            Debug.Assert(!this.destroyed, "Avatar member called after destruction.");
        }

        public bool Equals(Avatar other)
        {
            CheckCalls();
            return object.ReferenceEquals(this, other);
        }

        public override bool Equals(object obj)
        {
            CheckCalls();
            return object.ReferenceEquals(this, obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public static bool operator ==(Avatar a, Avatar b)
        {
            return Object.Equals(a, b);
        }

        public static bool operator !=(Avatar a, Avatar b)
        {
            return !(a == b);
        }
    }
    #endregion
}