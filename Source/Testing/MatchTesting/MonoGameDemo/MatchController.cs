﻿//-----------------------------------------------------------------------
// <copyright file="MatchController.cs" company="Scalify">
//     Copyright (c) 2012 Scalify. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BadumnaCloudDemo
{
    using System;
    using System.Diagnostics;
    using Badumna;

    public class MatchController
    {
        private int counter;

        public event Action<string> MessageReceived;

        [Replicable]
        public void ReceiveMessage(string message)
        {
            Debug.Assert(System.Threading.Thread.CurrentThread == Game1.UpdateThread, "Controller RPC called outside update thread.");
            var handler = this.MessageReceived;
            if (handler != null)
            {
                handler(message);
            }
        }

        [Replicable]
        public int Counter
        {
            get
            {
                return this.counter;
            }

            set
            {
                this.counter = value;
                var handler = this.MessageReceived;
                if (handler != null)
                {
                    handler("Set controller counter to " + this.counter);
                }
            }
        }
    }
 }