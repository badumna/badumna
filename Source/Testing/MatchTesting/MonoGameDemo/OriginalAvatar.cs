﻿//-----------------------------------------------------------------------
// <copyright file="OriginalAvatar.cs" company="Scalify">
//     Copyright (c) 2012 Scalify. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BadumnaCloudDemo
{
    using System;
    using Badumna.DataTypes;
    using Color = Microsoft.Xna.Framework.Color;

    // Class for representing the local player's avatar.
    #region OriginalAvatar class
    public class OriginalAvatar : Avatar
    {
        // The speed the avatar moves at in units per second.
        public const float MoveSpeed = 50f;

        // The location the avatar is moving towards.
        private Vector3 target;

        public OriginalAvatar(Vector3 position, Action<string> onMessage)
            : base(onMessage)
        {
            this.Position = position;
            this.target = position;
            var random = new Random();
            this.Colour = new Color(random.Next(255), random.Next(255), random.Next(255));
        }

        // Update the avatar by moving it towards its target if required.
        public void Update(double deltaSeconds)
        {
            if (this.Position != this.target)
            {
                float maxMovement = MoveSpeed * (float)deltaSeconds;
                Vector3 displacement = this.target - this.Position;
                if (displacement.Magnitude <= maxMovement)
                {
                    this.Position = this.target;
                }
                else
                {
                    this.Position += displacement.Normalize() * maxMovement;
                }
            }
        }

        // Set a new target for the avatar to move towards.
        public void SetTarget(float x, float y)
        {
            this.target.X = x;
            this.target.Y = y;
        }
    }
    #endregion
}
