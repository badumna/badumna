﻿//-----------------------------------------------------------------------
// <copyright file="NPCAvatar.cs" company="Scalify">
//     Copyright (c) 2012 Scalify. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BadumnaCloudDemo
{
    using System;
    using Badumna.DataTypes;
    using Color = Microsoft.Xna.Framework.Color;

    // Class for representing the local player's avatar.
    #region OriginalAvatar class
    public class NPCAvatar : Avatar
    {
        private Random random = new Random();

        // The speed the avatar moves at in units per second.
        public const float MoveSpeed = 50f;

        public NPCAvatar(Action<string> onMessage)
            : this(Vector3.Zero, onMessage)
        {
        }

        public NPCAvatar(Vector3 position, Action<string> onMessage)
            : base(onMessage)
        {
            this.Position = position;
            this.Colour = new Color(this.random.Next(255), this.random.Next(255), this.random.Next(255));
        }

        // Update the avatar by moving it towards its target if required.
        public void Update(double deltaSeconds)
        {
            Vector3 movement = Vector3.Zero;
            if (this.Position.X < 100)
            {
                movement += Vector3.UnitX;
            }

            if (this.Position.X > 300)
            {
                movement -= Vector3.UnitX;
            }

            if (this.Position.Y < 100)
            {
                movement += Vector3.UnitY;
            }

            if (this.Position.Y > 300)
            {
                movement -= Vector3.UnitX;
            }

            if (movement == Vector3.Zero)
            {

                movement.X = (float)((2 * this.random.NextDouble()) - 1);
                movement.Y = (float)((2 * this.random.NextDouble()) - 1);
            }

            float maxMovement = MoveSpeed * (float)deltaSeconds;
            this.Position += movement.Normalize() * maxMovement;
        }
    }
    #endregion
}
