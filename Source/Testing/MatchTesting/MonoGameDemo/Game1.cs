﻿using System;
using System.Collections.Generic;
using System.Linq;
using Badumna;
using Badumna.Core;
using Badumna.DataTypes;
using Badumna.SpatialEntities;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Badumna.Match;
using System.Text;
using Badumna.Utilities;
using Badumna.Utilities.Logging;

namespace BadumnaCloudDemo
{
    // This is the main type for your game
    public class Game1 : Game
    {
        public static System.Threading.Thread UpdateThread = null;
        private int screenWidth = 320;
        private int screenHeight = 480;

        private GraphicsDeviceManager graphics;
        private SpriteBatch spriteBatch;

        private INetworkFacade network;
        private List<Avatar> replicas = new List<Avatar>();
        private List<OriginalAvatar> hostedEntities = new List<OriginalAvatar>();
        private List<OriginalAvatar> localEntities = new List<OriginalAvatar>();

        private Texture2D sprite;
        private Texture2D blankTexture;
        private SpriteFont font;
        private Vector2 spriteOrigin;
        private bool loggedIn;
        private MouseState lastMouseState;
        private KeyboardState currentKeyboardState;
        private KeyboardState lastKeyboardState;
        private Queue<string> log = new Queue<string>();

        bool initBegun = false;
        private MatchController controller;
        private Match<MatchController> match;

        private MatchmakingResult matchmakingResult = null;

        public Game1()
        {
            this.graphics = new GraphicsDeviceManager(this);
            this.Content.RootDirectory = "Content";
            this.IsMouseVisible = true;
            this.graphics.PreferredBackBufferWidth = screenWidth;
            this.graphics.PreferredBackBufferHeight = screenHeight;
        }

        public void Shutdown()
        {
            this.loggedIn = false;

            if (this.network != null)
            {
                this.network.Shutdown();
                this.network = null;
            }
        }

        // Allows the game to perform any initialization it needs to before starting to run.
        // This is where it can query for any required services and load any non-graphic
        // related content.  Calling base.Initialize will enumerate through any components
        // and initialize them as well.
        protected override void Initialize()
        {
            base.Initialize();
            this.WriteToLog("Press S for server, or P for Peer");
        }

        // LoadContent will be called once per game and is the place to load
        // all of your content.
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            this.spriteBatch = new SpriteBatch(GraphicsDevice);

            this.sprite = Content.Load<Texture2D>("Avatar");
            this.spriteOrigin = new Vector2(this.sprite.Width / 2f, this.sprite.Height / 2f);
            this.font = Content.Load<SpriteFont>("Font");
            this.blankTexture = Content.Load<Texture2D>("Pixel");
        }

        // Allows the game to run logic such as updating the world,
        // checking for collisions, gathering input, and playing audio.
        //  - gameTime: Provides a snapshot of timing values.
        protected override void Update(GameTime gameTime)
        {
            if (UpdateThread == null)
            {
                UpdateThread = System.Threading.Thread.CurrentThread;
            }

            this.lastKeyboardState = this.currentKeyboardState;
            this.currentKeyboardState = Keyboard.GetState();
            if (!this.initBegun)
            {
                if (this.keyPressed(Keys.S))
                {
                    this.InitializeBadumna(true);
                    this.initBegun = true;
                    return;
                }

                if (this.keyPressed(Keys.P))
                {
                    this.InitializeBadumna(false);
                    this.initBegun = true;
                    return;
                }
            }

            if (this.loggedIn)
            {
                this.network.ProcessNetworkState();
                MouseState mouseState = Mouse.GetState();
                if (this.keyPressed(Keys.C) && this.match == null)
                {
                    this.WriteToLog("Creating new match.");
                    var criteria = new MatchmakingCriteria();
                    this.controller = new MatchController();
                    this.match = this.network.Match.CreateMatch(
                        this.controller,
                        criteria,
                        4,
                        "John" + new Random().Next(0, 100),
                        this.CreateReplica,
                        this.RemoveReplica);
                    this.ConfigureMatch();
                }

                if (this.keyPressed(Keys.J) && this.match == null)
                {
                    this.WriteToLog("Searching for a match.");
                    var criteria = new MatchmakingCriteria();
                    this.network.Match.FindMatches(criteria, this.HandleMatchQuery);
                }

                if (this.keyPressed(Keys.L) && this.match != null)
                {
                    this.match.Leave();
                    this.localEntities.Clear();
                    this.hostedEntities.Clear();
                    this.match = null;
                }

                if (this.keyPressed(Keys.K) && this.matchmakingResult != null)
                {
                    this.WriteToLog("Joining match:");
                    this.WriteToLog(" - " + this.matchmakingResult.Certificate.MatchIdentifier);
                    this.WriteToLog(" - " + this.matchmakingResult.Certificate.Host.ToString());
                    this.controller = new MatchController();
                    this.match = this.network.Match.JoinMatch<MatchController>(
                        this.controller,
                        this.matchmakingResult,
                        "Jane" + new Random().Next(0, 100),
                        this.CreateReplica,
                        this.RemoveReplica);
                    this.ConfigureMatch();
                    this.matchmakingResult = null;
                }

                if (this.keyPressed(Keys.R) && this.localEntities.Count > 0)
                {
                    this.match.CallMethodOnReplicas(this.localEntities[0].ReceiveMessage, "Avatar original says foo");
                }

                if (this.keyPressed(Keys.T) && this.replicas.Count > 0)
                {
                    this.match.CallMethodOnOriginal(this.replicas[0].ReceiveMessage, "Avatar replica says foo");
                }

                if (this.keyPressed(Keys.F) && this.hostedEntities.Count > 0)
                {
                    if (this.match.State == MatchStatus.Hosting)
                    {
                        this.match.CallMethodOnReplicas(this.hostedEntities[0].ReceiveMessage, "Hosted original says foo");
                    }
                }

                if (this.keyPressed(Keys.G) && this.hostedEntities.Count > 0)
                {
                    if (this.match.State != MatchStatus.Hosting)
                    {
                        this.match.CallMethodOnOriginal(this.hostedEntities[0].ReceiveMessage, "Hosted replica says foo");
                    }
                }

                if (this.keyPressed(Keys.D1) && this.match != null)
                {
                    this.match.CallMethodOnMembers(this.match.Controller.ReceiveMessage, "Controller says foo to all members.");
                }

                if (this.keyPressed(Keys.D2) && this.match != null)
                {
                    this.match.CallMethodOnHost(this.match.Controller.ReceiveMessage, "Controller says foo to host.");
                }

                if (this.keyPressed(Keys.D3) && this.match != null)
                {
                    foreach (var member in this.match.Members)
                    {
                        if (member != this.match.MemberIdentity)
                        {
                            this.match.CallMethodOnMember(member, this.match.Controller.ReceiveMessage, "Controller says foo to 1st member.");
                            return;
                        }
                    }
                }

                if (this.keyPressed(Keys.Add) && this.match != null)
                {
                    this.match.Controller.Counter++;
                }

                if (this.keyPressed(Keys.C) && this.match != null)
                {
                    this.match.Chat("Hello!");
                }

                if (this.keyPressed(Keys.V) && this.match != null)
                {
                    foreach (var member in this.match.Members)
                    {
                        if (member != this.match.MemberIdentity)
                        {
                            this.match.Chat(member, "Hello to " + member.Name);
                        }
                    }
                }

                if (this.keyPressed(Keys.N) && this.match != null)
                {
                    this.CreateAndRegisterLocalEntity();
                }

                if (this.keyPressed(Keys.M) && this.match != null)
                {
                    this.RemoveLocalEntity();
                }

                if (this.keyPressed(Keys.H) && this.match != null && this.match.State == MatchStatus.Hosting)
                {
                    this.CreateAndRegisterHostedEntity();
                }

                if (this.keyPressed(Keys.J) && this.match != null && this.match.State == MatchStatus.Hosting)
                {
                    this.RemoveHostedEntity();
                }

                if (mouseState.LeftButton == ButtonState.Pressed &&
                    this.lastMouseState.LeftButton == ButtonState.Released)
                {
                    float offset = 0;
                    foreach (var le in this.localEntities)
                    {
                        le.SetTarget(mouseState.X + offset, mouseState.Y + offset);
                        offset += 5;
                    }

                    if (this.match != null && this.match.State == MatchStatus.Hosting)
                    {
                        foreach (var he in this.hostedEntities)
                        {
                            he.SetTarget(mouseState.X + offset, mouseState.Y);
                            offset += 5;
                        }
                    }
                }

                foreach (var le in this.localEntities)
                {
                    le.Update(gameTime.ElapsedGameTime.TotalSeconds);
                }

                foreach (var he in this.hostedEntities)
                {
                        he.Update(gameTime.ElapsedGameTime.TotalSeconds);
                }


                this.lastMouseState = mouseState;
            }

            base.Update(gameTime);
        }

        private void HandleMatchQuery(MatchmakingQueryResult queryResult)
        {
            this.WriteToLog("Received matchmaking results: " + queryResult.Results.Count());
            if (queryResult.Error != MatchError.None)
            {
                this.WriteToLog("Matchmaking failed: " + queryResult.Error);
                return;
            }

            foreach (var result in queryResult.Results)
            {
                this.matchmakingResult = result;
                return;
            }
        }

        private void ConfigureMatch()
        {
            this.match.StateChanged += this.OnMatchStatusChange;
            this.match.MemberAdded += (s, e) => this.WriteToLog("Member added: " + e.Member);
            this.match.MemberRemoved += (s, e) => this.WriteToLog("Member removed: " + e.Member);
            this.match.Controller.MessageReceived += this.WriteToLog;
            this.match.ChatMessageReceived += this.HandleChatMessage;
            ////this.CreateAndRegisterLocalAvatar();
        }

        protected override void OnExiting(object sender, EventArgs args)
        {
            base.OnExiting(sender, args);
            this.Shutdown();
        }

        // This is called when the game should draw itself.
        //  - gameTime: Provides a snapshot of timing values.
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.DarkOrange);

            this.spriteBatch.Begin();

            // Draw layout
            this.spriteBatch.Draw(this.blankTexture, new Rectangle(0, 0, screenWidth, 80), Color.White);

            // Write status
            Vector2 textLocation = Vector2.Zero;
            string statusMessage = this.match != null ? "In match." : "Not in match.";
            this.spriteBatch.DrawString(this.font, statusMessage, textLocation, Color.Black);
            textLocation.Y += 20;
            statusMessage = "Members: " + (this.match != null ? this.match.MemberCount.ToString() : "-");
            this.spriteBatch.DrawString(this.font, statusMessage, textLocation, Color.Black);
            textLocation.Y += 20;
            statusMessage = "State: " + (this.match != null ? this.match.State.ToString() : "-");
            this.spriteBatch.DrawString(this.font, statusMessage, textLocation, Color.Black);
            textLocation.Y += 20;
            statusMessage = "Error: " + (this.match != null ? this.match.Error.ToString() : "-");
            this.spriteBatch.DrawString(this.font, statusMessage, textLocation, Color.Black);
            textLocation.Y += 20;

            // Draw log messages
            lock (this.log)
            {
                foreach (var message in this.log)
                {
                    try
                    {
                        this.spriteBatch.DrawString(this.font, message, textLocation, Color.Black);
                    }
                    catch (ArgumentException)
                    {
                        this.spriteBatch.DrawString(this.font, "[Unprintable log message]", textLocation, Color.Black);
                    }

                    textLocation.Y += 20;
                }
            }

            // Draw avatars
            foreach (var le in this.localEntities)
            {
                this.DrawAvatar(le);
            }

            foreach (var replica in this.replicas)
            {
                this.DrawAvatar(replica);
            }

            foreach (var he in this.hostedEntities)
            {
                this.DrawAvatar(he);
            }

            this.spriteBatch.End();
            base.Draw(gameTime);
        }

        // Draw an avatar (can be original or replica).
        //  - avatar: The avatar to draw.
        private void DrawAvatar(Avatar avatar)
        {
            var pos = new Vector2(avatar.Position.X, avatar.Position.Y);
            this.spriteBatch.Draw(
                this.sprite, pos, null, avatar.Colour, 0f, this.spriteOrigin, 1f, 0, 0f);
        }

        // Called by Badumna to create an object representing a replica entity.
        //  - matchID: The match the replica belongs to.
        //  - entitytype: An integer indicating the type of the entity.
        // Returns: A new replica.
        private object CreateReplica(Match match, EntitySource source, uint entitytype)
        {
            if (source == EntitySource.Peer)
            {
                var replica = new Avatar(this.WriteToLog);
                this.replicas.Add(replica);
                return replica;
            }
            else
            {
                var entity = new OriginalAvatar(Badumna.DataTypes.Vector3.Zero, this.WriteToLog);
                ////new NPCAvatar(Badumna.DataTypes.Vector3.Zero, this.WriteToLog);
                this.hostedEntities.Add(entity);
                return entity;
            }
        }

        // Called by Badumna to remove a replica from a scene.
        //  - matchID: The ID of the match the replica belongs to.
        //  - replica: The replica to remove.
        private void RemoveReplica(Match match, EntitySource source, object replica)
        {
            if (source == EntitySource.Peer)
            {
                this.replicas.Remove(replica as Avatar);
            }
            else
            {
                this.hostedEntities.Remove(replica as OriginalAvatar);
            }
        }

        private void InitializeBadumna(bool isServer)
        {
            this.WriteToLog("Initializing Badumna...");
            var options = new Options();
            options.Connectivity.ApplicationName = "GeoffMatchTest";
            options.Connectivity.ConfigureForLan();
            int port = 21280;
            if (isServer)
            {
                this.WriteToLog("Using port " + port);
                options.Connectivity.ConfigureForSpecificPort(port);
            }

            options.Matchmaking.ActiveMatchLimit = 999;
            var address = "172.16.28.148";
            ////var address = "192.168.56.1";
            ////var address = "192.168.1.3";
            options.Matchmaking.ServerAddress = address + ":" + port.ToString();

            ////options.Logger.LoggerType = LoggerType.None;
            ////options.Logger.LogLevel = LogLevel.Information;
            ////var logger = new DelegateLog();
            ////logger.OnMessage += (l, t, m) => this.WriteToLog(t + ": " + m);
            ////logger.OnException += (l, t, e, m) => this.WriteToLog(t + " " + e + ": " + m);
            ////Badumna.Utilities.Logger.Log = logger;
            options.Logger.LoggerType = LoggerType.File;
            options.Logger.LogLevel = LogLevel.Information;
            options.Logger.IncludeTags = LogTag.Match; // | LogTag.Autoreplication;
            options.Logger.LogTimestamp = true;

            NetworkFacade.BeginCreate(
                options,
                result =>
                {
                    try
                    {
                        this.network = NetworkFacade.EndCreate(result);
                        this.WriteToLog("Badumna initialization succeeded.");
                    }
                    catch (BadumnaException ex)
                    {
                        this.WriteToLog("Badumna initialization failed.");
                        this.WriteToLog(ex.Message);
                        this.WriteToLog("Please check:");
                        this.WriteToLog(" - your application ID");
                        this.WriteToLog(" - your internet connection");
                        return;
                    }

                    this.network.TypeRegistry.RegisterValueType<Color>(
                        (c, w) => { w.Write(c.R); w.Write(c.G); w.Write(c.B); w.Write(c.A); },
                        r => new Color(r.ReadByte(), r.ReadByte(), r.ReadByte(), r.ReadByte()));

                    this.network.Login("Foo");

                    this.WriteToLog("c/j: create/join match.");

                    this.loggedIn = true;
                });

        }

        private void OnMatchStatusChange(Match match, MatchStatusEventArgs e)
        {
            this.WriteToLog(e.Status.ToString());
            if (e.Status == MatchStatus.Closed)
            {
                this.match = null;
            }

            if (e.Error != MatchError.None)
            {
                this.WriteToLog(e.Error.ToString());
            }
        }

        private void HandleChatMessage(Match match, MatchChatEventArgs e)
        {
            var builder = new StringBuilder(e.Sender.Name);
            if (e.Type == ChatType.Public)
            {
                builder.Append(" shouted \"");
            }
            else
            {
                builder.Append(" whispered \"");
            }

            builder.Append(e.Message);
            builder.Append("\"");
            this.WriteToLog(builder.ToString());
        }

        private void WriteToLog(string message)
        {
            var text = DateTime.Now.ToString("HH:mm:ss") + " " + message;
            Console.WriteLine(text);
            lock (this.log)
            {
                this.log.Enqueue(text);

                // Cull old log messages.
                while (this.log.Count > 5)
                {
                    this.log.Dequeue();
                }
            }
        }

        private bool keyPressed(Keys key)
        {
            return (this.IsActive && this.currentKeyboardState.IsKeyDown(key) &&
                this.lastKeyboardState.IsKeyUp(key));
        }

        private void CreateAndRegisterLocalEntity()
        {
            var le = new OriginalAvatar(
                new Badumna.DataTypes.Vector3(100, 100, 0),
                this.WriteToLog);
            this.localEntities.Add(le);
            this.match.RegisterEntity(le, 0);
        }

        private void CreateAndRegisterHostedEntity()
        {
            var he = new OriginalAvatar(Badumna.DataTypes.Vector3.Zero, this.WriteToLog);
            this.hostedEntities.Add(he);
            this.match.RegisterHostedEntity(he, 0);
        }

        private void RemoveHostedEntity()
        {
            if (this.hostedEntities.Count > 0)
            {
                this.match.UnregisterHostedEntity(this.hostedEntities[0]);
                this.hostedEntities[0].Destroy();
                this.hostedEntities.RemoveAt(0);
            }
        }

        private void RemoveLocalEntity()
        {
            if (this.localEntities.Count > 0)
            {
                this.match.UnregisterEntity(this.localEntities[0]);
                this.localEntities[0].Destroy();
                this.localEntities.RemoveAt(0);
            }
        }
    }
}
