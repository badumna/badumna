﻿//-----------------------------------------------------------------------
// <copyright file="Constants.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2010 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace AutoTest
{
    using System.Diagnostics.CodeAnalysis;

    /// <summary>
    /// Commonly used constants.
    /// </summary>
    public class Constants
    {
        /// <summary>
        /// String for "--test-type=" command line argument.
        /// </summary>
        public const string TestTypeString = "test-type=";

        /// <summary>
        /// String for "--num-peers=" command line argument.
        /// </summary>
        [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1650:ElementDocumentationMustBeSpelledCorrectly", Justification = "Reviewed. Suppression is OK here.")]
        public const string NumPeersString = "num-peers=";

        /// <summary>
        /// String for "--file-name=" command line argument.
        /// </summary>
        public const string FileNameString = "file-name=";

        /// <summary>
        /// String for "--application-name=" command line argument.
        /// </summary>
        public const string AppNameString = "application-name=";

        /// <summary>
        /// String for "--scene-name=" command line argument.
        /// </summary>
        public const string SceneNameString = "scene-name=";

        /// <summary>
        /// String for "--life-time=" command line argument.
        /// </summary>
        public const string LifeTimeString = "life-time=";

        /// <summary>
        /// String for "--script-file=" command line argument.
        /// </summary>
        public const string ScriptFileString = "script-file=";

        /// <summary>
        /// String for "--avatar-type=" command line argument.
        /// </summary>
        public const string AddAvatarString = "add-avatar=";

        /// <summary>
        /// String for "--record-interval=" command line argument.
        /// </summary>
        public const string RecordIntervalString = "record-interval=";

        /// <summary>
        /// String for "--seed-peer=" command line argument.
        /// </summary>
        public const string SeedPeerString = "seed-peer=";

        /// <summary>
        /// String for "--disable-broadcast" command line argument.
        /// </summary>
        public const string DisableBroadcastString = "disable-broadcast";

        /// <summary>
        /// String for "--disable-port-forwarding" command line argument.
        /// </summary>
        public const string DisablePortForwardingStrng = "disable-port-forwarding";

        /// <summary>
        /// String for "--overload-peer=" command line argument.
        /// </summary>
        public const string OverloadPeerString = "overload-peer=";

        /// <summary>
        /// String for "--discover-overload" command line argument.
        /// </summary>
        public const string DiscoverOverloadString = "discover-overload";

        /// <summary>
        /// String for "--force-overload" command line argument.
        /// </summary>
        public const string ForceOverloadString = "force-overload";

        /// <summary>
        /// String for "--arbitration-peer=" command line argument.
        /// </summary>
        public const string ArbitrationPeerString = "arbitration-peer=";

        /// <summary>
        /// String for "--discover-arbitration" command line argument.
        /// </summary>
        public const string DiscoverArbitrationString = "discover-arbitration";

        /// <summary>
        /// String for "--arbitration-interval=" command line argument.
        /// </summary>
        public const string ArbitrationIntervalString = "arbitration-interval=";

        /// <summary>
        /// String for "--dei-config-string=" command line argument.
        /// </summary>
        public const string DeiConfigStringString = "dei-config-string=";

        /// <summary>
        /// String for "--bandwidth-limit=" command line argument.
        /// </summary>
        public const string BandwidthLimitString = "bandwidth-limit=";

        /// <summary>
        /// String for "--tunnel-server=" command line argument.
        /// </summary>
        public const string TunnelServerString = "tunnel-server=";

        /// <summary>
        /// String for "--log-events" command line argument.
        /// </summary>
        public const string LogEventsString = "log-events";

        /// <summary>
        /// String for "--area-of-interest=" command line argument.
        /// </summary>
        public const string AreaOfInterestString = "area-of-interest=";

        /// <summary>
        /// String for "--entity-velocity=" command line argument.
        /// </summary>
        public const string EntityVelocityString = "entity-velocity=";

        /// <summary>
        /// String for "--output-directory=" command line argument.
        /// </summary>
        public const string OutputDirectoryString = "output-directory=";

        /// <summary>
        /// String for "--user-name=" command line argument.
        /// </summary>
        public const string UserNameString = "user-name=";

        /// <summary>
        /// String for "--num-users=" command line argument.
        /// </summary>
        [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1650:ElementDocumentationMustBeSpelledCorrectly", Justification = "Reviewed. Suppression is OK here.")]
        public const string NumUsersString = "num-users=";

        /// <summary>
        /// String for "--num-friends=" command line argument.
        /// </summary>
        [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1650:ElementDocumentationMustBeSpelledCorrectly", Justification = "Reviewed. Suppression is OK here.")]
        public const string NumFriendsString = "num-friends=";

        /// <summary>
        /// String for "--chat-delay=" command line argument.
        /// </summary>
        public const string ChatDelayString = "chat-delay=";

        /// <summary>
        /// String for "--set-ping" command line argument.
        /// </summary>
        public const string SetPingString = "set-ping";

        /// <summary>
        /// String for "DateTime" heading.
        /// </summary>
        public const string DateTimeString = "DateTime";

        /// <summary>
        /// String for "ProcessID" heading.
        /// </summary>
        public const string ProcessIdString = "ProcessID";

        /// <summary>
        /// String for "ActiveConnections" heading.
        /// </summary>
        public const string ActiveConnectionsString = "ActiveConnections";

        /// <summary>
        /// String for "TotalBytesSentPerSecond" heading.
        /// </summary>
        public const string TotalBytesSentString = "TotalBytesSentPerSecond";

        /// <summary>
        /// String for "TotalBytesReceivedPerSecond" heading.
        /// </summary>
        public const string TotalBytesReceivedString = "TotalBytesReceivedPerSecond";

        /// <summary>
        /// String for "RemoteEntityCount" heading.
        /// </summary>
        public const string RemoteEntityCountString = "RemoteEntityCount";

        /// <summary>
        /// String for "InitializingConnections" heading.
        /// </summary>
        public const string InitializingConnectionsString = "InitializingConnections";

        /// <summary>
        /// String for "ReceivedUpdateCount" heading.
        /// </summary>
        public const string ReceivedUpdateCountString = "ReceivedUpdateCount";

        /// <summary>
        /// String for "CurrentCPUUsage" heading.
        /// </summary>
        public const string CurrentCpuUsageString = "CurrentCPUUsage";

        /// <summary>
        /// String for "AvailableRAM" heading.
        /// </summary>
        public const string AvailableRamString = "AvailableRAM";

        /// <summary>
        /// String for "EventType" heading.
        /// </summary>
        public const string EventTypeString = "EventType";

        /// <summary>
        /// String for "Data" heading.
        /// </summary>
        public const string DataString = "Data";

        /// <summary>
        /// Preferred format for DateTime string representation.
        /// </summary>
        public const string DateTimeFormat = "yyyy-MM-dd HH:mm:ss.ffffff";

        //// Matchmaking

        /// <summary>
        /// String for "--matchmaking-request" command line argument.
        /// </summary>
        public const string MatchmakingRequest = "matchmaking-request=";

        /// <summary>
        /// String for "--matchmaking-delays" command line argument.
        /// </summary>
        public const string MatchmakingDelays = "matchmaking-delays=";

        /// <summary>
        /// String for "--cloud-identifier" command line argument.
        /// </summary>
        public const string CloudIdentifier = "cloud-identifier=";

        /// <summary>
        /// String for "--config-filename" command line argument.
        /// </summary>
        public const string ConfigFileName = "config-filename=";

        /// <summary>
        /// String for "--match-size" command line argument.
        /// </summary>
        public const string MatchSize = "match-size=";

        /// <summary>
        /// String for "--host-leave-after=" command line argument.
        /// </summary>
        public const string HostLeaveAfter = "host-leave-after=";

        /// <summary>
        /// String for "--num-hosted-entities=" command line argument.
        /// </summary>
        public const string NumHostedEntities = "num-hosted-entities=";
    }
}