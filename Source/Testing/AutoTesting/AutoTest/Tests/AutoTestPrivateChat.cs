﻿//-----------------------------------------------------------------------
// <copyright file="AutoTestPrivateChat.cs" company="Scalify">
//     Copyright (c) 2012 Scalify. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace AutoTest.Tests
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Threading;
    using System.Xml;

    using Badumna;
    using Badumna.Chat;
    using Badumna.DataTypes;
    using Badumna.SpatialEntities;
    using Badumna.Utilities.Logging;

    using Mono.Options;
    using AutoTest.Tests.Behaviour;

    /// <summary>
    /// Delegate for friends list updating function.
    /// </summary>
    /// <param name="friends">Friends list.</param>
    public delegate void FriendsListUpdater(IEnumerable<Friend> friends);

    /// <summary>
    /// Delegate for send to list updating function.
    /// </summary>
    /// <param name="friends">Friends list.</param>
    public delegate void SendToListUpdater(IEnumerable<Friend> friends);

    /// <summary>
    /// Delegate for incoming message updating function.
    /// </summary>
    /// <param name="name">Friend name.</param>
    /// <param name="message">Message text.</param>
    public delegate void IncomingMessageUpdater(string name, string message);

    /// <summary>
    /// Peer used to test presence.
    /// </summary>
    public class AutoTestPrivateChat : AutoTestBase
    {
        /// <summary>
        /// Total number of users.
        /// </summary>
        private int numUsers = 20;

        /// <summary>
        /// Number of friends per buddy list.
        /// </summary>
        private int numFriends = 10;

        /// <summary>
        /// Manager of chat service.
        /// </summary>
        private ChatManager chatManager;

        /// <summary>
        /// Accumulated delay time before chat session is created.
        /// </summary>
        private float chatSessionDelayTime;

        /// <summary>
        /// String describing presence status of friends list.
        /// </summary>
        private string statusText = string.Empty;

        /// <summary>
        /// Presence status of local user.
        /// </summary>
        private ChatStatus presenceStatus = ChatStatus.Online;

        /// <summary>
        /// Name of local user.
        /// </summary>
        private string userName = string.Empty;

        /// <summary>
        /// Chat session delay timer.
        /// </summary>
        private Timer chatDelayTimer;

        /// <summary>
        /// Initializes a new instance of the AutoTestPrivateChat class.
        /// </summary>
        public AutoTestPrivateChat()
        {            
            var testOptions = new OptionSet()
            {
                { Constants.UserNameString, "Name of user.", var => { this.userName = var; }},
                { Constants.NumUsersString, "Total number of user.", (int var) => { this.numUsers = var; }},
                { Constants.NumFriendsString, "Number of friends.", (int var) => { this.numFriends = var; }},
                { Constants.ChatDelayString, "Private chat delay time in seconds.", (float var) => { this.chatSessionDelayTime = var; }}
            };

            foreach (var option in testOptions)
            {
                this.optionSet.Add(option);
            }
        }

        /// <inheritdoc />
        public override string ToString()
        {
            return "AutoTestPeer";
        }

        /// <inheritdoc />
        protected override void Update(TimeSpan interval)
        {
            if (this.forceExit)
            {
                this.Stop();
            }

            if (this.network.IsLoggedIn)
            {
                this.network.ProcessNetworkState();
            }

            this.UpdateProcess(interval);         
        }

        /// <inheritdoc />
        protected override void OnStartUp()
        {
            this.StartUpBadumna();

            this.chatDelayTimer = new Timer(
                    state =>
                    {
                        List<string> friendList = this.GetFriendList(this.userName);
                        this.chatManager = new ChatManager(this.network, this.SetFriendList, this.SendToFriendList, this.AddIncomingMessage);
                        this.chatManager.Initialize(this.userName, friendList);
                    },
                    null,
                    (int)TimeSpan.FromSeconds(this.chatSessionDelayTime).TotalMilliseconds,
                    Timeout.Infinite);
            
            base.OnStartUp();
        }

        /// <summary>
        /// Called when the peer is shutting down.
        /// </summary>
        protected override void OnShutDown()
        {
            this.ShutDownBadumna();
            base.OnShutDown();
        }

        /// <inheritdoc/>
        protected override void UpdateRecord()
        {
            XmlDocument networkStatus = this.network.GetNetworkStatus().ToXml();
            int activeConnectionCount = int.Parse(networkStatus.SelectSingleNode("/Status/Connectivity/" + Constants.ActiveConnectionsString).InnerText);
            int initializingConnectionCount = int.Parse(networkStatus.SelectSingleNode("/Status/Connectivity/" + Constants.InitializingConnectionsString).InnerText);
            int totalBytesSentPerSecond = int.Parse(networkStatus.SelectSingleNode("/Status/TransferRate/" + Constants.TotalBytesSentString).InnerText);
            int totalBytesReceivedPerSecond = int.Parse(networkStatus.SelectSingleNode("/Status/TransferRate/" + Constants.TotalBytesReceivedString).InnerText);
            int remoteEntityCount = 0;
            int receivedUpdateCount = 0;

            var machineStatus = this.performanceMonitor.GetMachineStatus();
            this.RecordStats(activeConnectionCount, totalBytesSentPerSecond, totalBytesReceivedPerSecond, remoteEntityCount, initializingConnectionCount, receivedUpdateCount, machineStatus[0], machineStatus[1]);

            this.RecordEvent("Presence Status", this.statusText);
        }

        /// <summary>
        /// Initialize Badumna functionality.
        /// </summary>
        private void StartUpBadumna()
        {
            this.network = NetworkFacade.Create(this.GenerateNetworkConfig());

            for (int i = 0; i < BadumnaLoginAttempts; ++i)
            {
                this.network.Login(this.userName);

                if (this.network.IsLoggedIn)
                {
                    this.RecordEvent("Badumna Log In", string.Empty);

                    this.network.RegisterEntityDetails(150.0f, new Vector3(20.0f, 20.0f, 20.0f).Magnitude);
                    this.scene = this.network.JoinScene(this.sceneName, this.CreateSpatialReplica, this.RemoveSpatialReplica);

                    break;
                }

                this.RecordLog("Failed to login to Network Facade.");
            }

            if (!this.network.IsLoggedIn)
            {
                this.forceExit = true;
            }
        }

        /// <summary>
        /// Generate the network configuration for Badumna.
        /// </summary>
        /// <returns>Network configuration.</returns>
        private Options GenerateNetworkConfig()
        {
            var options = new Options();
            options.Logger.LoggerType = LoggerType.DotNetTrace;
            options.Logger.LoggerConfig = "<Logger Type=\"DotNetTraceLogger\" Enabled=\"true\"><Verbosity>Verbose</Verbosity><TraceFile Enabled=\"true\" ></TraceFile><TraceConsole Enabled=\"false\" /></Logger>";
            options.Logger.IncludeTags = LogTag.Event | LogTag.Overload | LogTag.Connection | LogTag.Presence | LogTag.Replication;

            if (!string.IsNullOrEmpty(this.seedPeer))
            {
                options.Connectivity.SeedPeers.Add(this.seedPeer);
            }
            else
            {
                options.Connectivity.ConfigureForLan();
            }

            if (!string.IsNullOrEmpty(this.appName))
            {
                options.Connectivity.ApplicationName = this.appName;
            }

            return options;
        }

        /// <summary>
        /// Creates a remote avatar and adds it to the avatar list.
        /// </summary>
        /// <param name="scene">Current scene.</param>
        /// <param name="entityId">ID of the entity</param>
        /// <param name="entityType">Type of the entity.</param>
        /// <returns>Spatial replica of the remote avatar.</returns>
        private IReplicableEntity CreateSpatialReplica(NetworkScene scene, BadumnaId entityId, uint entityType)
        {
            return null;
        }

        /// <summary>
        /// Removes remote avatar from the avatar list.
        /// </summary>
        /// <param name="scene">Current scene.</param>
        /// <param name="replica">Spatial replica to remove.</param>
        private void RemoveSpatialReplica(NetworkScene scene, IReplicableEntity replica)
        {
        }
        
        /// <summary>
        /// Update the main process functionality.
        /// </summary>
        /// <param name="timeStep">Time since last update.</param>
        private void UpdateProcess(TimeSpan interval)
        {
            if (!this.network.IsLoggedIn)
            {
                this.forceExit = true;
            }
        }

        /// <summary>
        /// Shut down Badumna functionality.
        /// </summary>
        private void ShutDownBadumna()
        {
            if (this.scene != null)
            {
                this.scene.Leave();
                this.scene = null;
            }

            this.network.Shutdown();

            this.RecordEvent("Badumna Shutdown", string.Empty);
        }

        /// <summary>
        /// Gets friends list.
        /// </summary>
        /// <param name="userName">Name of user.</param>
        /// <returns>Friends list.</returns>
        private List<string> GetFriendList(string userName)
        {
            if (userName.StartsWith("amazon-"))
            {
                int userNumber = int.Parse(userName.Substring(7));
                List<string> friendArray = new List<string>();

                for (int i = -this.numFriends / 2; i <= this.numFriends / 2; ++i)
                {
                    if (i == 0)
                    {
                        continue;
                    }

                    int friendNumber = userNumber + i;

                    if (friendNumber <= 0)
                    {
                        friendNumber += this.numUsers;
                    }
                    else if (friendNumber > this.numUsers)
                    {
                        friendNumber -= this.numUsers;
                    }

                    friendArray.Add("amazon-" + friendNumber.ToString());
                }

                return friendArray;
            }
            else
            {
                return new List<string>();
            }
        }

        /// <summary>
        /// Sets friends list.
        /// </summary>
        /// <param name="friends">List of friends.</param>
        private void SetFriendList(IEnumerable<Friend> friends)
        {
            string status = "Status: ";

            for (int i = 0; i < this.numUsers; ++i)
            {
                string name = "amazon-" + (i + 1).ToString();

                if (this.userName == name)
                {
                    status += ((int)this.presenceStatus).ToString();
                }
                else
                {
                    foreach (Friend friend in friends)
                    {
                        if (friend.Name == name)
                        {
                            status += ((int)friend.Status).ToString();
                            break;
                        }
                    }
                }
            }

            this.statusText = status;
        }

        /// <summary>
        /// Sends to friends list.
        /// </summary>
        /// <param name="friends">List of friends.</param>
        private void SendToFriendList(IEnumerable<Friend> friends)
        {
        }

        /// <summary>
        /// Adds incoming message.
        /// </summary>
        /// <param name="name">Name of sender.</param>
        /// <param name="message">Incoming message.</param>
        private void AddIncomingMessage(string name, string message)
        {
        }
    }
}
