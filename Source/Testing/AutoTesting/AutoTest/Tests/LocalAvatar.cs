﻿//-----------------------------------------------------------------------
// <copyright file="LocalAvatar.cs" company="Scalify">
//     Copyright (c) 2012 Scalify. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace AutoTest.Tests
{
    using System;
    using System.Drawing;
    using System.IO;

    using Badumna;
    using Badumna.DataTypes;
    using Badumna.SpatialEntities;
    using Badumna.Utilities;

    /// <summary>
    /// Represents a local avatar in the Badumna peer.
    /// </summary>
    public class LocalAvatar : Avatar
    {
        /// <summary>
        /// The random generator.
        /// </summary>
        private readonly Random randomGenerator = new Random();

        /// <summary>
        /// Reference to network facade.
        /// </summary>
        private INetworkFacade network;

        /// <summary>
        /// Region bounds with the following format, bound top; bound bottom, bound left; bound right; height
        /// </summary>
        private float[] regionBounds;        

        /// <summary>
        /// Amount to move each update.
        /// </summary>
        private float moveAmount = 40.0f;

        /// <summary>
        /// Amount to rotate each update.
        /// </summary>
        private float rotateAmount = 100.0f;

        /// <summary>
        /// Current state of the entity.
        /// </summary>
        private EntityState currentState = EntityState.Target;

        /// <summary>
        /// Current destination of the entity.
        /// </summary>
        private Vector3 destination = new Vector3();

        /// <summary>
        /// Initializes a new instance of the LocalAvatar class.
        /// </summary>
        /// <param name="network">Network facade.</param>
        public LocalAvatar(INetworkFacade network)
            :this(network, new float[] { 0.0f, 250.0f, 0.0f, 300.0f, 0.0f })
        {
        }

        /// <summary>
        /// Initializes a new instance of the LocalAvatar class.
        /// </summary>
        /// <param name="network">Network facade.</param>
        /// <param name="regionBound"> Region bounds with the following format, bound top; bound bottom, bound left; bound right; height</param>
        public LocalAvatar(INetworkFacade network, float[] regionBound)
        {
            this.network = network;
            this.regionBounds = regionBound;
            this.Guid = System.Guid.NewGuid().ToString();
            this.IsOriginal = true;
        }

        /// <summary>
        /// Enumeration defining bound index.
        /// </summary>
        protected enum BoundIndex
        {
            boundTop,
            boundBottom,
            boundLeft,
            boundRight,
            height
        }

        /// <summary>
        /// Enumeration defining entity states.
        /// </summary>
        public enum EntityState : uint
        {
            /// <summary>
            /// Entity selects next target.
            /// </summary>
            Target,

            /// <summary>
            /// Entity rotates towards target.
            /// </summary>
            Rotate,

            /// <summary>
            /// Entity moves towards target.
            /// </summary>
            Move,
        }

        /// <summary>
        /// Gets or sets the move amount.
        /// </summary>
        protected float MoveAmount
        {
            get { return this.moveAmount; }

            set { this.moveAmount = value; }
        }

        /// <summary>
        /// Gets or sets the rotate amount.
        /// </summary>
        protected float RotateAmount
        {
            get { return this.rotateAmount; }

            set { this.rotateAmount = value; }
        }

        /// <summary>
        /// Gets or sets the entity state.
        /// </summary>
        protected EntityState CurrentState
        {
            get { return this.currentState; }

            set { this.currentState = value; }
        }

        /// <summary>
        /// Gets or sets the target position.
        /// </summary>
        protected Vector3 Destination
        {
            get { return this.destination; }

            set { this.destination = value; }
        }

        /// <summary>
        /// Gets the region bounds.
        /// </summary>
        protected float[] RegionBounds
        {
            get { return this.regionBounds; }
        }

        /// <summary>
        /// Calculates a random position.
        /// </summary>
        /// <returns>Random position vector.</returns>
        public Vector3 CalculateRandomDestination()
        {
            Vector3 position = new Vector3();
            position.X = this.randomGenerator.Next(
                0, 
                (int)(this.regionBounds[(int)BoundIndex.boundRight] - this.regionBounds[(int)BoundIndex.boundLeft])) + this.regionBounds[(int)BoundIndex.boundLeft];
            position.Y = this.regionBounds[(int)BoundIndex.height];
            position.Z = this.randomGenerator.Next(
                0, 
                (int)(this.regionBounds[(int)BoundIndex.boundBottom] - this.regionBounds[(int)BoundIndex.boundTop])) + this.regionBounds[(int)BoundIndex.boundTop];
            return position;
        }        

        /// <summary>
        /// Calculates a random color.
        /// </summary>
        /// <returns>Random color.</returns>
        public UnityEngine.Color CalculateRandomColor()
        {
            byte[] colorBytes = new byte[3];
            this.randomGenerator.NextBytes(colorBytes);
            return new UnityEngine.Color((float)colorBytes[0], (float)colorBytes[1], (float)colorBytes[2]);
        }

        /// <summary>
        /// Calculates the angle between two vectors.
        /// </summary>
        /// <param name="forwardVector">First vector used in comparision.</param>
        /// <param name="topVector">Up vector.</param>
        /// <param name="compareVector">Second vector used in comparison.</param>
        /// <returns>Angle between the two vectors.</returns>
        public double GetAngleXY(Vector3 forwardVector, Vector3 topVector, Vector3 compareVector)
        {
            Vector3 unitForwardVector = new Vector3(forwardVector.X, forwardVector.Y, forwardVector.Z);
            unitForwardVector /= unitForwardVector.Magnitude;
            Vector3 unittopVector = new Vector3(topVector.X, topVector.Y, topVector.Z);
            unittopVector /= unittopVector.Magnitude;
            Vector3 unitLeftVector = new Vector3();
            unitLeftVector.X = (unittopVector.Y * unitForwardVector.Z) - (unittopVector.Z * unitForwardVector.Y);
            unitLeftVector.Y = (unittopVector.Z * unitForwardVector.X) - (unittopVector.X * unitForwardVector.Z);
            unitLeftVector.Z = (unittopVector.X * unitForwardVector.Y) - (unittopVector.Y * unitForwardVector.X);
            Vector3 unitCompareVector = new Vector3(compareVector.X, compareVector.Y, compareVector.Z);
            unitCompareVector /= unitCompareVector.Magnitude;

            float dotForward = unitForwardVector.Dot(unitCompareVector);
            float dotLeft = unitLeftVector.Dot(unitCompareVector);
            double angle = 0f;

            if (dotForward > 0f)
            {
                if (dotLeft > 0f)
                {
                    angle = (Math.PI * 1.5f) + Math.Acos(dotLeft);
                }
                else
                {
                    angle = Math.Acos(dotForward);
                }
            }
            else
            {
                if (dotLeft > 0f)
                {
                    angle = Math.PI + Math.Acos(-dotForward);
                }
                else
                {
                    angle = (Math.PI * 0.5f) + Math.Acos(-dotLeft);
                }
            }

            return angle * 180f / Math.PI;
        }        
    }
}
