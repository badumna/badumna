﻿//------------------------------------------------------------------------------
// <copyright file="Count.cs" company="National ICT Australia Limited">
//     Copyright (c) 2013 National ICT Australia Limited. All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

namespace AutoTest.Tests.Counting
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Globalization;

    /// <summary>
    /// A sequence number, timestamp pair.
    /// </summary>
    public struct Count
    {
        public int SequenceNumber { get; private set; }

        public TimeSpan Timestamp { get; private set; }

        public Count(int sequenceNumber, TimeSpan timestamp) : this()
        {
            this.SequenceNumber = sequenceNumber;
            this.Timestamp = timestamp;
        }

        public Count(BinaryReader reader) : this()
        {
            this.SequenceNumber = reader.ReadInt32();
            this.Timestamp = TimeSpan.FromTicks(reader.ReadInt64());
        }

        public Count(string s) : this()
        {
            var parts = s.Split(',');
            if (parts.Length != 2)
            {
                throw new ArgumentException("s");
            }

            this.SequenceNumber = int.Parse(parts[0], CultureInfo.InvariantCulture);
            this.Timestamp = TimeSpan.FromTicks(long.Parse(parts[1], CultureInfo.InvariantCulture));
        }

        public void Serialize(BinaryWriter writer)
        {
            writer.Write(this.SequenceNumber);
            writer.Write(this.Timestamp.Ticks);
        }

        public override string ToString()
        {
            return string.Format(CultureInfo.InvariantCulture, "{0},{1}", this.SequenceNumber, this.Timestamp.Ticks);
        }
    }
}
