﻿//------------------------------------------------------------------------------
// <copyright file="CounterSink.cs" company="National ICT Australia Limited">
//     Copyright (c) 2013 National ICT Australia Limited. All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

namespace AutoTest.Tests.Counting
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Consumes periodic messages and measures their intervals.
    /// </summary>
    public class CounterSink
    {
        /// <summary>
        /// The last count received.
        /// </summary>
        private Count lastCount;

        /// <summary>
        /// The time the last count arrived.
        /// </summary>
        private TimeSpan lastArrivalTime;

        /// <summary>
        /// Delegate to get the current time.
        /// </summary>
        private Func<TimeSpan> getCurrentTime;

        /// <summary>
        /// Called when we receive a message.
        /// </summary>
        private Action<bool, TimeSpan, TimeSpan> onStatistics;

        /// <summary>
        /// Initializes a new instance of the CounterSink class.
        /// </summary>
        /// <param name="getCurrentTime">Delegate that returns the current time</param>
        /// <param name="onStatistics">Called when new statistics are available, with (missing, interval, estimatedDelay).  If missing is true, the interval should be ignored.</param>
        public CounterSink(Func<TimeSpan> getCurrentTime, Action<bool, TimeSpan, TimeSpan> onStatistics)
        {
            this.getCurrentTime = getCurrentTime;
            this.onStatistics = onStatistics;
        }

        /// <summary>
        /// Called periodically with the current count.
        /// </summary>
        /// <param name="count">The count</param>
        public void OnCount(Count count)
        {
            var now = this.getCurrentTime();

            if (count.Equals(this.lastCount))
            {
                return;
            }

            bool missing = count.SequenceNumber != this.lastCount.SequenceNumber + 1;
            var interval = now - this.lastArrivalTime;
            var estimatedDelay = now - count.Timestamp;  // average of the two estimated values from each side of a connection at a given time should be the delay (half the rtt)

            if (this.lastArrivalTime != TimeSpan.Zero)
            {
                this.onStatistics(missing, interval, estimatedDelay);
            }

            this.lastArrivalTime = now;
            this.lastCount = count;
        }
    }
}
