﻿//------------------------------------------------------------------------------
// <copyright file="Counter.cs" company="National ICT Australia Limited">
//     Copyright (c) 2013 National ICT Australia Limited. All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

namespace AutoTest.Tests.Counting
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Generates periodic messages with timestamp and sequence number.
    /// </summary>
    public class Counter
    {
        /// <summary>
        /// The last count we generated.
        /// </summary>
        private Count lastCount;

        /// <summary>
        /// Delegate to get the current time.
        /// </summary>
        private Func<TimeSpan> getCurrentTime;

        /// <summary>
        /// Time between generated messages.
        /// </summary>
        private TimeSpan interval;

        /// <summary>
        /// Delegate called when a count is generated.
        /// </summary>
        private Action<Count> onCount;

        /// <summary>
        /// Initializes a new instance of the Counter class.
        /// </summary>
        /// <param name="interval">Time between generated messages</param>
        /// <param name="getCurrentTime">Delegate that returns the current time</param>
        /// <param name="onCount">Delegate called when a count is generated</param>
        public Counter(TimeSpan interval, Func<TimeSpan> getCurrentTime, Action<Count> onCount)
        {
            this.interval = interval;
            this.getCurrentTime = getCurrentTime;
            this.onCount = onCount;
        }

        /// <summary>
        /// Generates a message if enough time has elapsed.
        /// </summary>
        /// <remarks>
        /// Must be called much more frequently than the generate message interval.
        /// </remarks>
        public void Update()
        {
            var now = this.getCurrentTime();
            var elapsed = now - this.lastCount.Timestamp;
            if (elapsed > this.interval)
            {
                this.lastCount = new Count(this.lastCount.SequenceNumber + 1, now);
                this.onCount(this.lastCount);
            }
        }

        /// <summary>
        /// Reset the last count to the given value.
        /// </summary>
        /// <remarks>
        /// The next count sent will be one more than this value, and will be
        /// sent one interval after calling this method.
        /// </remarks>
        /// <param name="count">The new "last count".</param>
        public void SetLastCount(Count count)
        {
            this.lastCount = new Count(count.SequenceNumber, this.getCurrentTime());
        }
    }
}
