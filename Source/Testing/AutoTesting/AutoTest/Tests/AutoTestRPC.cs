﻿// -----------------------------------------------------------------------
// <copyright file="AutoTestRPC.cs" company="Scalify">
//     Copyright (c) 2013 Scalify. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace AutoTest.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Text;
    using AutoTest.Tests.Behaviour;
    using Mono.Options;
    using System.Timers;
    using Badumna.Utilities;
    using Badumna;

    /// <summary>
    /// Auto test RPC with new autoreplication.
    /// </summary>
    public class AutoTestRPC : AutoTestPeer
    {
        /// <summary>
        /// Time interval in between regular sequence updates.
        /// </summary>
        private const double SequenceUpdateInterval = 10.0f;

        /// <summary>
        /// The local avatar.
        /// </summary>
        private LocalAvatar localAvatar;

        /// <summary>
        /// Stopwatch.
        /// </summary>
        private Stopwatch stopwatch;

        /// <summary>
        /// The timer.
        /// </summary>
        private Timer timer;

        /// <summary>
        /// Sequence number.
        /// </summary>
        private int sequence;

        /// <summary>
        /// The public address of the peer.
        /// </summary>
        private string publicAddress;

        /// <inheritdoc/>
        public override string ToString()
        {
            return "AutoTestRPC";
        }

        /// <inheritdoc/>
        public override void AddAvatar(LocalAvatar avatar)
        {
            this.scene.RegisterEntity(avatar, (uint)EntityType.Avatar, 2.0f, 20.0f);            

            avatar.Position = avatar.CalculateRandomDestination();
            avatar.Color = avatar.CalculateRandomColor();

            this.localAvatar = avatar;
            this.localAvatar.ackReceievedHandler += this.AckReceievedHandler;
        }

        /// <inheritdoc/>
        protected override void Update(TimeSpan interval)
        {
            if (this.localAvatar != null && this.scene != null)
            {
                this.localAvatar.Update(interval);
            }

            base.Update(interval);
        }
        
        /// <inheritdoc/>
        protected override void OnStartUp()
        {
            base.OnStartUp();

            this.stopwatch = new Stopwatch();

            this.timer = new Timer();
            this.timer.Elapsed += new ElapsedEventHandler(this.SendNewSequence);
            this.timer.Interval = TimeSpan.FromSeconds(SequenceUpdateInterval).TotalMilliseconds;
            this.timer.Start();

            this.RecordEvent("Username", this.UserName);
            this.publicAddress = this.network.GetNetworkStatus().PublicAddress;
        }       

        /// <inheritdoc/>
        protected override void OnShutDown()
        {
            this.timer.Dispose();
            this.stopwatch.Stop();
            
            if (this.scene != null)
            {
                this.scene.UnregisterEntity(this.localAvatar);
                this.localAvatar.ackReceievedHandler -= this.AckReceievedHandler;
                this.scene.Leave();
                this.scene = null;
            }

            this.network.Shutdown();

            this.RecordEvent("Badumna Shutdown", string.Empty);
        }

        /// <inheritdoc/>
        protected override void SequenceChangedHandler(int sequence, Avatar avatar)
        {
            avatar.SendAckToOriginal(sequence, this.UserName, this.scene);
            base.SequenceChangedHandler(sequence, avatar);
        }

        /// <summary>
        /// Send a new sequence to replicas every interval.
        /// </summary>
        /// <param name="sender">Object sender.</param>
        /// <param name="e">Elapsed event arguments.</param>
        private void SendNewSequence(object sender, ElapsedEventArgs e)
        {
            if (this.localAvatar != null)
            {
                this.stopwatch.Reset();
                this.stopwatch.Start();
                this.localAvatar.SendNewSequenceToReplicas(this.sequence++, this.scene);

                if (this.sequence % 3 == 0)
                {
                    var diag = (this.network as INetworkFacadeInternal).GetDiagnosticInformation();
                    var connections = diag.Get<DiagnosticInfo>("connection_table").Get<DiagnosticInfo>("connections");

                    foreach (var key in connections.Keys)
                    {
                        var connection = connections.Get<DiagnosticInfo>(key);
                        var rtt = connection.Get<double>("rtt_ms");
                        this.RecordEvent("rtt", string.Format("{0},{1},{2}", this.publicAddress, key, rtt));
                    }
                }
            }
        }

        /// <summary>
        /// Handle the incoming acknowledgement from 
        /// </summary>
        /// <param name="sequence">Sequence number.</param>
        /// <param name="peerId">Sender id.</param>
        private void AckReceievedHandler(int sequence, string peerId)
        {
            var data = string.Format("{0},{1},{2}", peerId, sequence, this.stopwatch.ElapsedMilliseconds);
            this.RecordEvent("Ping", data);
        }
    }
}
