﻿//-----------------------------------------------------------------------
// <copyright file="ChatManager.cs" company="Scalify">
//     Copyright (c) 2012 Scalify. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace AutoTest.Tests
{
    using System.Collections.Generic;
    using System.IO;

    using Badumna;
    using Badumna.Chat;
    using Badumna.DataTypes;

    /// <summary>
    /// The ChatManager class implements fully distributed private chat by using Badumna's chat service. 
    /// </summary>
    internal class ChatManager
    {
        /// <summary>
        /// The friends list.
        /// </summary>
        private FriendsList friends;

        /// <summary>
        /// The primary interface to Badumna.
        /// </summary>
        private INetworkFacade networkFacade;

        /// <summary>
        /// The user name of the local peer. 
        /// </summary>
        private string myName;

        /// <summary>
        /// The delegate for updating the friends list.
        /// </summary>
        private FriendsListUpdater friendsListUpdater;

        /// <summary>
        /// The delegate for updating the send to list.
        /// </summary>
        private SendToListUpdater sendToListUpdater;

        /// <summary>
        /// The delegate for updating the incoming message textbox. 
        /// </summary>
        private IncomingMessageUpdater incomingMessageUpdater;

        /// <summary>
        /// Initializes a new instance of the <see cref="ChatManager"/> class.
        /// </summary>
        /// <param name="networkFacade">The network facade.</param>
        /// <param name="friendsListUpdater">The friends list updater delegate.</param>
        /// <param name="sendToListUpdater">The send to list updater delegate.</param>
        /// <param name="incomingMessageUpdater">The incoming message updater delegate.</param>
        public ChatManager(
            INetworkFacade networkFacade,
            FriendsListUpdater friendsListUpdater,
            SendToListUpdater sendToListUpdater,
            IncomingMessageUpdater incomingMessageUpdater)
        {
            this.networkFacade = networkFacade;

            this.friends = new FriendsList();
            this.friendsListUpdater = friendsListUpdater;
            this.sendToListUpdater = sendToListUpdater;
            this.incomingMessageUpdater = incomingMessageUpdater;
        }

        /// <summary>
        /// Gets or sets my username.
        /// </summary>
        /// <value>The username.</value>
        public string MyUsername
        {
            get { return this.myName; }
            set { this.myName = value; }
        }

        /// <summary>
        /// Shutdowns this instance.
        /// </summary>
        public void Shutdown()
        {
        }

        /// <summary>
        /// Initializes this instance. Must be called after initializing the badumna suite.
        /// </summary>
        /// <param name="userName">Name of user.</param>
        /// <param name="friendsNames">List of friends names.</param>
        /// <returns>True if successful or false on failure.</returns>
        public bool Initialize(string userName, IEnumerable<string> friendsNames)
        {
            if (this.networkFacade.ChatSession == null)
            {
                return false;
            }

            this.myName = userName;

            bool success = this.friends.LoadFriendsList(friendsNames);
            if (!success)
            {
                return false;
            }

            this.networkFacade.ChatSession.OpenPrivateChannels(this.HandleChannelInvitation);
            this.networkFacade.ChatSession.ChangePresence(ChatStatus.Online);

            // invite all users to private channel
            foreach (Friend friend in this.friends)
            {
                this.networkFacade.ChatSession.InviteUserToPrivateChannel(friend.Name);
            }

            this.UpdateFriendsList();
            this.UpdateSendToList();

            return true;
        }

        /// <summary>
        /// Sets the presence status.
        /// </summary>
        /// <param name="status">The presence status.</param>
        public void SetPresenceStatus(ChatStatus status)
        {
            if (this.networkFacade.ChatSession != null)
            {
                this.networkFacade.ChatSession.ChangePresence(status);
            }

            this.UpdateFriendsList();
        }

        /// <summary>
        /// Sends the chat message.
        /// </summary>
        /// <param name="username">The username of the receiver.</param>
        /// <param name="message">The message to be sent.</param>
        public void SendChatMessage(string username, string message)
        {
            Friend friend = this.friends.GetFriend(username);
            if (friend != null)
            {
                friend.Channel.SendMessage(message);
            }
        }

        /// <summary>
        /// Reads the username from the friends list file.
        /// </summary>
        /// <param name="filepath">The path of the friends list file.</param>
        /// <returns>The user name or null on failure.</returns>
        private string ReadUsername(string filepath)
        {
            using (TextReader tr = new StreamReader(filepath))
            {
                try
                {
                    string line = tr.ReadLine();
                    tr.Close();

                    return line;
                }
                catch
                {
                    return null;
                }
            }
        }

        /// <summary>
        /// Updates the friends list.
        /// </summary>
        private void UpdateFriendsList()
        {
            if (this.friendsListUpdater != null)
            {
                this.friendsListUpdater(this.friends);
            }
        }

        /// <summary>
        /// Updates the send to list.
        /// </summary>
        private void UpdateSendToList()
        {
            if (this.sendToListUpdater != null)
            {
                this.sendToListUpdater(this.friends);
            }
        }

        /// <summary>
        /// Updates the friend presence status.
        /// </summary>
        /// <param name="name">The friend's name.</param>
        /// <param name="status">The presence status.</param>
        private void UpdateFriendPresenceStatus(string name, ChatStatus status)
        {
            this.friends.SetFriendChatStatus(name, status);
            this.UpdateFriendsList();
        }

        /// <summary>
        /// Badumna requires INetworkFacade.ProcessNetworkState to be called regularly. 
        /// </summary>
        private void RegularProcessing()
        {
            if (this.networkFacade.IsLoggedIn)
            {
                this.networkFacade.ProcessNetworkState();
            }
        }

        /// <summary>
        /// Handles the channel invitation.
        /// </summary>
        /// <param name="channel">The channel id.</param>
        /// <param name="username">The username.</param>
        private void HandleChannelInvitation(ChatChannelId channel, string username)
        {
            Friend friend = this.friends.GetFriend(username);
            if (friend != null)
            {
                if (friend.Channel != null)
                {
                    if (friend.Channel.Id == channel)
                    {
                        // We already have this channel
                        return;
                    }
                    else
                    {
                        // This friend's channel has changed - unsubscribe from the old one.
                        friend.Channel.Unsubscribe();
                    }
                }

                // Accept the invitation.
                friend.Channel = this.networkFacade.ChatSession.AcceptInvitation(channel, this.HandlePrivateMessage, this.HandlePresence);
            }
        }

        /// <summary>
        /// Handles the incoming private message.
        /// </summary>
        /// <param name="channel">The chat channel.</param>
        /// <param name="userId">The sender user id.</param>
        /// <param name="message">The chat message.</param>
        private void HandlePrivateMessage(IChatChannel channel, BadumnaId userId, string message)
        {
            string name = this.friends.GetFriendNameByChannelId(channel.Id);
            if (name != null && this.incomingMessageUpdater != null)
            {
                this.incomingMessageUpdater(name, message);
            }
        }

        /// <summary>
        /// Handles the presence.
        /// </summary>
        /// <param name="channel">The chat channel.</param>
        /// <param name="userId">The sender user id.</param>
        /// <param name="username">The username.</param>
        /// <param name="status">The current present status.</param>
        private void HandlePresence(IChatChannel channel, BadumnaId userId, string username, ChatStatus status)
        {
            this.friends.SetFriendChatStatus(username, status);
            if (this.friendsListUpdater != null)
            {
                this.friendsListUpdater(this.friends);
            }

            // after the status becomes Offline, the chat channel id becomes invalid. 
            if (status == ChatStatus.Offline)
            {
                Friend friend = this.friends.GetFriend(username);
                if (friend != null)
                {
                    friend.Channel = null;
                }
            }
        }
    }
}
