﻿//-----------------------------------------------------------------------
// <copyright file="RemoteAvatar.cs" company="Scalify">
//     Copyright (c) 2012 Scalify. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace AutoTest.Tests
{
    using System;
    using System.Drawing;
    using System.IO;

    using Badumna.DataTypes;
    using Badumna.SpatialEntities;
    using Badumna.Utilities;

    /// <summary>
    /// Represents a remote avatar in the Badumna peer.
    /// </summary>
    public class RemoteAvatar : Avatar, ISpatialReplica, IDeadReckonable
    {
        /// <summary>
        /// Flag to record statistics.
        /// </summary>
        private bool recordStats = false;

        /// <summary>
        /// Number of received updates.
        /// </summary>
        private int receivedUpdateCount = 0;

        /// <summary>
        /// Event record updater.
        /// </summary>
        private EventRecordUpdater eventRecordUpdater;

        /// <summary>
        /// Initializes a new instance of the RemoteAvatar class.
        /// </summary>
        /// <param name="recordStats">Flag to record statistics.</param>
        /// <param name="eventRecordUpdater">Event record updater.</param>
        public RemoteAvatar(bool recordStats, EventRecordUpdater eventRecordUpdater)
        {
            this.recordStats = recordStats;
            this.eventRecordUpdater = eventRecordUpdater;
        }

        /// <summary>
        /// Gets the number of received updates.
        /// </summary>
        public int ReceivedUpdateCount
        {
            get
            {
                int returnValue = this.receivedUpdateCount;
                this.receivedUpdateCount = 0;

                return returnValue;
            }

            private set
            {
                this.receivedUpdateCount = value;
            }
        }

        /// <summary>
        /// Method that deserializes data from received network packets.
        /// </summary>
        /// <param name="includedParts">Parts to deserialize.</param>
        /// <param name="stream">Current stream.</param>
        /// <param name="estimatedMillisecondsSinceDeparture">Estimated milliseconds since departure.</param>
        public void Deserialize(BooleanArray includedParts, Stream stream, int estimatedMillisecondsSinceDeparture)
        {
            if (this.recordStats)
            {
                ++this.ReceivedUpdateCount;
            }

            BinaryReader reader = new BinaryReader(stream);

            try
            {
                if (includedParts[(int)Avatar.StateSegment.Orientation])
                {
                    this.Orientation = reader.ReadSingle();
                }

                if (includedParts[(int)Avatar.StateSegment.Color])
                {
                    byte a = reader.ReadByte();
                    byte r = reader.ReadByte();
                    byte g = reader.ReadByte();
                    byte b = reader.ReadByte();
                    this.Color = Color.FromArgb(a, r, g, b);
                }
            }
            catch
            {
            }
            

            ////if (includedParts[(int)Avatar.StateSegment.Sequence])
            ////{
            ////    this.Sequence = reader.ReadInt64();
            ////}

            if (this.eventRecordUpdater != null)
            {
                string data = string.Format(
                    "GUID={0} Position=<{1} {2} {3}> Sequence={4}",
                    this.Guid.ToString(),
                    this.Position.X.ToString(),
                    this.Position.Y.ToString(),
                    this.Position.Z.ToString(),
                    this.Sequence.ToString());
                this.eventRecordUpdater("Deserialise Replica", data);
            }
        }

        public void AttemptMovement(Vector3 reckonedPosition)
        {
            this.Position = reckonedPosition;
        }

        public Vector3 Velocity { get; set; }
    }
}
