﻿//-----------------------------------------------------------------------
// <copyright file="AutoTestPeer.cs" company="Scalify">
//     Copyright (c) 2012 Scalify. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace AutoTest.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Xml;
    using System.Linq;

    using AutoTest.ArbitrationEvents;
    using AutoTest.Tests.Behaviour;

    using Badumna;
    using Badumna.Arbitration;
    using Badumna.DataTypes;
    using Badumna.Security;
    using Badumna.SpatialEntities;

    using Dei;

    using Mono.Options;

    /// <summary>
    /// Peer used to test Badumna.
    /// </summary>
    public class AutoTestPeer : AutoTestBase
    {
        /// <summary>
        /// Object to lock access to avatarList to make it thread safe.
        /// </summary>
        private static readonly object avatarListLock = new object();

        /// <summary>
        /// List of avatars.
        /// </summary>
        private static readonly List<IBehaviour> avatarList = new List<IBehaviour>();

        /// <summary>
        /// List of local avatars.
        /// </summary>
        private readonly List<Avatar> localAvatarList = new List<Avatar>();

        /// <summary>
        /// Object to lock access to entityIdList to make it thread safe.
        /// </summary>
        private readonly object entityIdListLock = new object();

        /// <summary>
        /// List of IDs of entities that have been created or removed since the last time step.
        /// </summary>
        ////private readonly List<string> entityIdList = new List<string>();

        /// <summary>
        /// Dei Identity Provider.
        /// </summary>
        protected IIdentityProvider identityProvider;

        /// <summary>
        /// Badumna arbitration service.
        /// </summary>
        private IArbitrator moveArbitrator;

        /// <summary>
        /// Accumulated time since last arbitration update.
        /// </summary>
        private TimeSpan lastArbitrationUpdateTime;

        /// <summary>
        /// Accumulated time since last sequence update.
        /// </summary>
        ////private double lastSequenceUpdateTime = 0.0f;        

        /// <summary>
        /// Name of avatar type to add to the peer.
        /// </summary>
        private string addAvatar;

        /// <summary>
        /// Flag indicating whether disabling of broadcast was set using command line option.
        /// </summary>
        private bool disableBroadcastIsSet;

        /// <summary>
        /// Flag indicating whether disabling of port forwarding was set using command line option.
        /// </summary>
        private bool disablePortForwardingIsSet;

        /// <summary>
        /// IP address and port number of the overload peer.
        /// </summary>
        private string overloadPeer;

        /// <summary>
        /// Flag indicating whether overload service discovery was set using command line option.
        /// </summary>
        private bool discoverOverloadIsSet;

        /// <summary>
        /// Flag indicating whether forcing of overload was set using command line option.
        /// </summary>
        private bool forceOverloadIsSet;

        /// <summary>
        /// IP address and port number of the arbitration peer.
        /// </summary>
        private string arbitrationPeer;

        /// <summary>
        /// Flag indicating whether arbitration service discovery was set using command line option.
        /// </summary>
        private bool discoverArbitrationIsSet;

        /// <summary>
        /// The time between arbitration events.
        /// </summary>
        private TimeSpan? arbitrationInterval;

        /// <summary>
        /// String containing the Dei configuration.
        /// </summary>
        private string deiConfigString = string.Empty;

        /// <summary>
        /// Number of remaining attempts to login to arbitration server.
        /// </summary>
        private int arbitrationConnectionAttempts = 3;

        /// <summary>
        /// Flag to indicate that connection has been established with move arbitrator.
        /// </summary>
        private bool connectedToMoveArbitrator = true;

        /// <summary>
        /// Flag to attempt to log back into arbitration server.
        /// </summary>
        private bool attemptArbitrationLogin;

        /// <summary>
        /// The desired bandwidth limit, in bytes.
        /// </summary>
        private int? bandwidthLimit;

        /// <summary>
        /// IP address and port number of the tunnel server.
        /// </summary>
        private string tunnelServer;

        /// <summary>
        /// Area of interest radius of entities.
        /// </summary>
        private float areaOfInterest = 20.0f;

        /// <summary>
        /// Maximum entity velocity.
        /// </summary>
        private float entityVelocity = 6.0f;

        /// <summary>
        /// The name of the config file to initialize the Badumna
        /// options from.
        /// </summary>
        private string configFileName;

        /// <summary>
        /// Initializes a new instance of the AutoTestPeer class.
        /// </summary>
        public AutoTestPeer()
        {
            var testOptions = new OptionSet
            {
                {
                    Constants.AddAvatarString,
                    "Name of avatar type to add to peer.",
                    var => { this.addAvatar = var; }
                },
                {
                    Constants.DisableBroadcastString,
                    "Disable broadcast.",
                    var => { this.disableBroadcastIsSet = true; }
                },
                {
                    Constants.DisablePortForwardingStrng,
                    "Disable port forwarding.",
                    var => { this.disablePortForwardingIsSet = true; }
                },
                {
                    Constants.OverloadPeerString,
                    "IP address and port number of overload peer.",
                    var => { this.overloadPeer = var; }
                },
                {
                    Constants.DiscoverOverloadString,
                    "Enable overload service discovery.",
                    var => { this.discoverOverloadIsSet = true; }
                },
                {
                    Constants.ForceOverloadString,
                    "Force peer to use overload peer.",
                    var => { this.forceOverloadIsSet = true; }
                },
                {
                    Constants.ArbitrationPeerString,
                    "IP address and port number of arbitration peer.",
                    var => { this.arbitrationPeer = var; }
                },
                {
                    Constants.DiscoverArbitrationString,
                    "Enable arbitration service discovery.",
                    var => { this.discoverArbitrationIsSet = true; }
                },
                {
                    Constants.ArbitrationIntervalString,
                    "Time between arbitration events, in seconds.",
                    (float var) => { this.arbitrationInterval = TimeSpan.FromSeconds(var); }
                },
                {
                    Constants.DeiConfigStringString,
                    "String containing the Dei configuration.",
                    var => { this.deiConfigString = var; }
                },
                {
                    Constants.BandwidthLimitString,
                    "Desired bandwith limit, in bytes.",
                    (int var) => { this.bandwidthLimit = var; }
                },
                {
                    Constants.TunnelServerString,
                    "IP address and port number of tunnel server.",
                    var => { this.tunnelServer = var; }
                },
                {
                    Constants.AreaOfInterestString,
                    "Area of interest radius of entities.",
                    (float var) => { this.areaOfInterest = var; }
                },
                {
                    Constants.EntityVelocityString,
                    "Maximum entity velocity.",
                    (float var) => { this.entityVelocity = var; }
                },
                {
                    Constants.UserNameString,
                    "Username use to login to Badumna.",
                    var => { this.UserName = var; }
                },
                {
                    Constants.ConfigFileName,
                    "Name of config file to initialize Badumna configuration from.",
                    var => { this.configFileName = var; }
                }
            };

            foreach (var option in testOptions)
            {
                this.optionSet.Add(option);
            }
        }

        /// <summary>
        /// Gets the avatar list lock.
        /// </summary>
        public static object AvatarListLock
        {
            get { return avatarListLock; }
        }

        /// <summary>
        /// Gets the avatar list.
        /// </summary>
        public static List<IBehaviour> AvatarList
        {
            get { return avatarList; }
        }

        /// <summary>
        /// Gets the user name used for login to Badumna.
        /// </summary>
        protected string UserName { get; private set; }

        /// <summary>
        /// Add local avatar to the peer.
        /// </summary>
        /// <param name="avatar">Local avatar to add.</param>
        public virtual void AddAvatar(LocalAvatar avatar)
        {
            this.scene.RegisterEntity(avatar, (uint)EntityType.Avatar, 2.0f, this.areaOfInterest);
            this.localAvatarList.Add(avatar);

            lock (AvatarListLock)
            {
                AvatarList.Add(avatar);
            }

            avatar.Position = avatar.CalculateRandomDestination();
            avatar.Color = avatar.CalculateRandomColor();

            var data = string.Format("GUID={0}", avatar.Guid);
            this.RecordEvent("Add Avatar", data);
        }

        /// <inheritdoc />
        public override string ToString()
        {
            return "AutoTestPeer";
        }

        /// <inheritdoc />
        protected override void Update(TimeSpan interval)
        {
            this.forceExit |= !this.network.IsLoggedIn;
            if (this.forceExit)
            {
                this.Stop();
                return;
            }

            this.network.ProcessNetworkState();

            lock (AvatarListLock)
            {
                for (var i = 0; i < AvatarList.Count; ++i)
                {
                    AvatarList[i].Update(interval);
                }
            }

            if (this.arbitrationInterval.HasValue && this.attemptArbitrationLogin)
            {
                this.ConnectToArbitrator();
                this.attemptArbitrationLogin = false;
            }

            ////if (this.logEventsIsSet)
            ////{
            ////    this.lastSequenceUpdateTime += timeStep;

            ////    while (this.lastSequenceUpdateTime >= 5.0f)
            ////    {
            ////        foreach (LocalAvatar localAvatar in this.localAvatarList)
            ////        {
            ////            localAvatar.Sequence++;

            ////            string data = string.Format(
            ////                "GUID={0} Sequence={1}",
            ////                localAvatar.Guid.ToString(),
            ////                localAvatar.Sequence.ToString());
            ////            this.RecordEvent("Update Sequence", data);
            ////        }

            ////        this.lastSequenceUpdateTime -= 5.0f;
            ////    }
            ////}

            if (this.arbitrationInterval.HasValue)
            {
                this.lastArbitrationUpdateTime += interval;

                while (this.lastArbitrationUpdateTime >= this.arbitrationInterval.Value)
                {
                    this.UpdateArbitration(this.arbitrationInterval.Value);
                    this.lastArbitrationUpdateTime -= this.arbitrationInterval.Value;
                }
            }
        }

        /// <summary>
        /// Called when the peer is starting up.
        /// </summary>
        protected override void OnStartUp()
        {
            base.OnStartUp();
            this.StartBadumna();
            this.StartArbitration();
        }

        /// <summary>
        /// Called when the peer is shutting down.
        /// </summary>
        protected override void OnShutDown()
        {
            this.DisconnectFromArbitration();
            this.ShutDownBadumna();
            base.OnShutDown();
        }

        /// <inheritdoc/>
        protected override void UpdateRecord()
        {
            var currentTime = DateTime.Now;
            var processId = Process.GetCurrentProcess().Id;
            var activeConnectionCount = 0;
            var initializingConnectionCount = 0;
            var totalBytesSentPerSecond = 0;
            var totalBytesReceivedPerSecond = 0;
            var remoteEntityCount = 0;
            var receivedUpdateCount = 0;

            if (string.IsNullOrEmpty(this.tunnelServer) && this.network != null)
            {
                var networkStatus = this.network.GetNetworkStatus().ToXml();
                activeConnectionCount = int.Parse(networkStatus.SelectSingleNode("/Status/Connectivity/" + Constants.ActiveConnectionsString).InnerText);
                initializingConnectionCount = int.Parse(networkStatus.SelectSingleNode("/Status/Connectivity/" + Constants.InitializingConnectionsString).InnerText);
                totalBytesSentPerSecond = int.Parse(networkStatus.SelectSingleNode("/Status/TransferRate/" + Constants.TotalBytesSentString).InnerText);
                totalBytesReceivedPerSecond = int.Parse(networkStatus.SelectSingleNode("/Status/TransferRate/" + Constants.TotalBytesReceivedString).InnerText);
            }

            lock (AvatarListLock)
            {
                for (var i = 0; i < AvatarList.Count; ++i)
                {
                    var avatar = AvatarList[i] as Avatar;

                    if (avatar != null && !avatar.IsOriginal)
                    {
                        //receivedUpdateCount += remoteAvatar.ReceivedUpdateCount;
                        ++remoteEntityCount;
                    }
                }
            }

            var machineStatus = this.performanceMonitor.GetMachineStatus();
            this.RecordStats(activeConnectionCount, totalBytesSentPerSecond, totalBytesReceivedPerSecond, remoteEntityCount, initializingConnectionCount, receivedUpdateCount, machineStatus[0], machineStatus[1]);

            var replicas = string.Empty;

            ////lock (this.entityIdListLock)
            ////{
            ////    if (this.entityIdList.Count > 0)
            ////    {
            ////        replicas = this.entityIdList[0].ToString();
            ////    }

            ////    for (var i = 1; i < this.entityIdList.Count; ++i)
            ////    {
            ////        replicas = string.Format("{0},{1}", replicas, this.entityIdList[i]);
            ////    }

            ////    this.entityIdList.Clear();
            ////}

            this.RecordReplica(remoteEntityCount, replicas);
        }

        /// <summary>
        /// Generate the network configuration for Badumna.
        /// </summary>
        /// <returns>Network configuration.</returns>
        protected virtual Options GenerateBadumnaOptions()
        {
            Options options;

            if (!string.IsNullOrEmpty(this.configFileName))
            {
                options = new Options(this.configFileName);
            }
            else
            {
                options = new Options();

                options.Logger.LoggerType = LoggerType.File;
                options.Logger.LogLevel = LogLevel.Warning;
            }
            
            if (!string.IsNullOrEmpty(this.appName))
            {
                options.Connectivity.ApplicationName = this.appName;
            }

            if (!string.IsNullOrEmpty(this.seedPeer))
            {
                options.Connectivity.SeedPeers.Add(this.seedPeer);
            }
            else if (string.IsNullOrEmpty(this.configFileName))
            {
                options.Connectivity.ConfigureForLan();
            }

            if (this.disableBroadcastIsSet)
            {
                options.Connectivity.IsBroadcastEnabled = false;
            }

            if (this.disablePortForwardingIsSet)
            {
                options.Connectivity.IsPortForwardingEnabled = false;
            }

            if (this.discoverOverloadIsSet)
            {
                options.Overload.IsClientEnabled = true;
            }

            if (!string.IsNullOrEmpty(this.overloadPeer))
            {
                options.Overload.IsClientEnabled = true;
                options.Overload.ServerAddress = this.overloadPeer;
            }

            if (this.forceOverloadIsSet)
            {
                options.Overload.IsClientEnabled = true;
                options.Overload.EnableForcedOverload("i_understand_this_is_for_testing_only");
            }

            if (!string.IsNullOrEmpty(this.arbitrationPeer))
            {
                options.Arbitration.Servers.Add(new ArbitrationServerDetails("Movement", this.arbitrationPeer));
            }
            else if (this.discoverArbitrationIsSet || this.arbitrationInterval.HasValue)
            {
                options.Arbitration.Servers.Add(new ArbitrationServerDetails("Movement"));
            }

            if (this.bandwidthLimit.HasValue)
            {
                options.Connectivity.EnableTransportLimiter("i_understand_this_is_for_testing_only");
                options.Connectivity.BandwidthLimit = this.bandwidthLimit.Value;
            }

            if (!string.IsNullOrEmpty(this.tunnelServer))
            {
                options.Connectivity.TunnelMode = TunnelMode.On;
                options.Connectivity.TunnelUris.Add("http://" + this.tunnelServer + "/");
            }

            return options;
        }

        /// <summary>
        /// Creates a remote avatar and adds it to the avatar list.
        /// </summary>
        /// <param name="scene">Current scene.</param>
        /// <param name="entityId">ID of the entity</param>
        /// <param name="entityType">Type of the entity.</param>
        /// <returns>Spatial replica of the remote avatar.</returns>
        protected IReplicableEntity CreateSpatialReplica(NetworkScene scene, BadumnaId entityId, uint entityType)
        {
            if (entityType != (uint)EntityType.Avatar)
            {
                return null;
            }

            var data = string.Format("GUID={0}", entityId);
            this.RecordEvent("Create Replica", data);

            var replica = new Avatar(this.RecordEvent);

            lock (AvatarListLock)
            {
                AvatarList.Add(replica);
            }

            replica.sequenceChangedHandler += this.SequenceChangedHandler;

            return replica;
        }

        /// <summary>
        /// Removes remote avatar from the avatar list.
        /// </summary>
        /// <param name="scene">Current scene.</param>
        /// <param name="replica">Spatial replica to remove.</param>
        protected void RemoveSpatialReplica(NetworkScene scene, IReplicableEntity replica)
        {
            var avatar = replica as Avatar;
            if (avatar == null)
            {
                return;
            }

            lock (AvatarListLock)
            {
                for (int i = 0; i < AvatarList.Count; ++i)
                {
                    if (AvatarList[i].Guid == avatar.Guid)
                    {
                        AvatarList.RemoveAt(i);
                        avatar.sequenceChangedHandler -= this.SequenceChangedHandler;
                        break;
                    }
                }
            }

            var data = string.Format("GUID={0}", avatar.Guid);
            this.RecordEvent("Remove Replica", data);
        }

        /// <summary>
        /// Sequence change handler.
        /// </summary>
        /// <param name="sequence">Sequence number</param>
        protected virtual void SequenceChangedHandler(int sequence, Avatar avatar)
        {
        }

        /// <summary>
        /// Initialize Badumna functionality.
        /// </summary>
        private void StartBadumna()
        {
            if (!string.IsNullOrEmpty(this.cloudIdentifier))
            {
                System.Environment.SetEnvironmentVariable(
                    "BADUMNA_CLOUD_URI",
                    "http://184.72.143.166");
                System.Environment.SetEnvironmentVariable(
                    "BADUMNA_TRACKER",
                    "184.72.143.166");
                //System.Environment.SetEnvironmentVariable(
                //    "BADUMNA_LOG_OVERRIDE",
                //    @"<Module Name='Logger' Enabled='true' EnableDotNetTraceForwarding='false' SuppressAssertUI='false' IncludeTags='All' ExcludeTags='None' LogLevel='Information' LogTimestamp='false' LoggerType='File' />");
                this.network = NetworkFacade.Create(this.cloudIdentifier);
            }
            else
            {
                this.network = NetworkFacade.Create(this.GenerateBadumnaOptions());
            }

            for (var i = 0; i < BadumnaLoginAttempts; ++i)
            {
                if (this.DoDeiLogin())
                {
                    this.network.Login(this.identityProvider);
                }
                else if (!string.IsNullOrEmpty(this.UserName))
                {
                    this.network.Login(this.UserName);
                }
                else
                {
                    this.network.Login();
                }

                if (this.network.IsLoggedIn)
                {
                    this.RecordEvent("Badumna Log In", this.network.GetNetworkStatus().ToString());

                    this.network.RegisterEntityDetails(this.areaOfInterest, 6.0f);

                    this.network.TypeRegistry.RegisterValueType(
                        (c, w) => { w.Write(c.r); w.Write(c.g); w.Write(c.b); },
                        r => new UnityEngine.Color(r.ReadSingle(), r.ReadSingle(), r.ReadSingle()));

                    if (!string.IsNullOrEmpty(this.sceneName))
                    {
                        // HACK: 
                        this.scene = this.network.JoinMiniScene(
                            this.sceneName, this.CreateSpatialReplica, this.RemoveSpatialReplica);
                        this.RecordEvent("Join scene", this.scene.Name);
                    }

                    if (!string.IsNullOrEmpty(this.addAvatar))
                    {
                        this.AddAvatar(this.CreateAvatar(this.addAvatar));
                    }

                    break;
                }

                this.RecordLog("Failed to login to Network Facade.");
            }

            if (!this.network.IsLoggedIn)
            {
                this.forceExit = true;
            }
        }

        /// <summary>
        /// Shut down Badumna functionality.
        /// </summary>
        private void ShutDownBadumna()
        {
            if (this.scene != null)
            {
                for (var i = 0; i < this.localAvatarList.Count; ++i)
                {
                    this.scene.UnregisterEntity(this.localAvatarList[i]);
                }

                this.scene.Leave();
                this.scene = null;
            }

            lock (AvatarListLock)
            {
                AvatarList.Clear();
            }

            this.localAvatarList.Clear();

            this.RecordEvent("Badumna Shutdown", this.network.GetNetworkStatus().ToString());

            this.network.Shutdown();
        }

        /// <summary>
        /// Initialize arbitration functionality.
        /// </summary>
        private void StartArbitration()
        {
            if (this.arbitrationInterval.HasValue)
            {
                this.ConnectToArbitrator();
            }
        }

        /// <summary>
        /// Connect to arbitration server.
        /// </summary>
        private void ConnectToArbitrator()
        {
            if (this.arbitrationConnectionAttempts > 0)
            {
                this.moveArbitrator = this.network.GetArbitrator("Movement");
                this.moveArbitrator.Connect(
                    this.HandleMoveArbitratorConnectionResult,
                    this.HandleMoveArbitratorConnectionFailure,
                    this.HandleArbitrationEvent);

                --this.arbitrationConnectionAttempts;
            }
            else
            {
                this.forceExit = true;
            }
        }

        /// <summary>
        /// Handles move arbitration connection result.
        /// </summary>
        /// <param name="result">Result of move arbitration connection.</param>
        private void HandleMoveArbitratorConnectionResult(ServiceConnectionResultType result)
        {
            switch (result)
            {
                case ServiceConnectionResultType.Success:
                    this.RecordLog("Connected to arbitration service.");
                    this.connectedToMoveArbitrator = true;
                    break;

                case ServiceConnectionResultType.ConnectionTimeout:
                    this.RecordLog("Failed to connect to an arbitration service host.");
                    this.attemptArbitrationLogin = true;
                    break;

                case ServiceConnectionResultType.ServiceNotAvailable:
                    this.RecordLog("Failed to locate any arbitration service host.");
                    this.forceExit = true;
                    break;
            }
        }

        /// <summary>
        /// Handles move arbitration connection failure.
        /// </summary>
        private void HandleMoveArbitratorConnectionFailure()
        {
            this.RecordLog("Connection to an arbitration service host was lost.");
            this.connectedToMoveArbitrator = false;
            this.attemptArbitrationLogin = true;
        }

        /// <summary>
        /// Handle arbitration event.
        /// </summary>
        /// <param name="message">Message as byte array.</param>
        private void HandleArbitrationEvent(byte[] message)
        {
            var serverEvent = EventSet.Deserialize(message);
        }

        /// <summary>
        /// Creates an avatar according to the specification.
        /// </summary>
        /// <param name="avatarSpecification">Type and configuration of avatar</param>
        /// <returns>The newly created avatar</returns>
        private LocalAvatar CreateAvatar(string avatarSpecification)
        {
            var factories = new Dictionary<string, Func<INetworkFacade, string, LocalAvatar>>
            {
                { "Beacon", Beacon.Create },
                { "Bouncer", Bouncer.Create },
                { "Follower", Follower.Create },
                { "RandomWalker", RandomWalker.Create },
                { "UnityWalker", UnityWalker.Create }
            };

            var args = avatarSpecification.Split(new char[] { ';' }, 2);
            var avatarType = args[0];
            var avatarConfig = args.Length > 1 ? args[1] : "";

            Func<INetworkFacade, string, LocalAvatar> factory;
            if (!factories.TryGetValue(avatarType, out factory))
            {
                throw new ArgumentException("Unknown avatar type: " + avatarType, "avatarSpecification");
            }

            return factory(this.network, avatarConfig);
        }

        /// <summary>
        /// Attempt to log into Dei server.
        /// </summary>
        /// <returns>True, if dei login was successful.</returns>
        protected bool DoDeiLogin()
        {
            if (string.IsNullOrEmpty(this.deiConfigString))
            {
                return false;
            }

            var parts = this.deiConfigString.Split(';');
            if (parts.Length != 5 || parts.Any(x => x.Length == 0))
            {
                return false;
            }

            var deiHost = parts[0];
            var deiPort = parts[1];
            var useSslConnection = parts[2];
            var deiUsername = parts[3];
            var deiPassword = parts[4];

            using (var session = new Session(deiHost, ushort.Parse(deiPort), null))
            {
                session.UseSslConnection = bool.Parse(useSslConnection);
                session.IgnoreSslErrors = true;

                var result = session.Authenticate(deiUsername, deiPassword);
                if (!result.WasSuccessful)
                {
                    var data = string.Format("Dei: {0}", result.ErrorDescription);
                    this.RecordLog(data);
                    this.identityProvider = null;
                    return false;
                }

                result = session.SelectIdentity(Character.None, out this.identityProvider);
                return result.WasSuccessful;
            }
        }

        /// <summary>
        /// Update the arbitration functionality.
        /// </summary>
        /// <param name="timeStep">Time since last update.</param>
        private void UpdateArbitration(TimeSpan interval)
        {
            this.SendMoveArbitrationEvent();
        }

        /// <summary>
        /// Send arbitration event.
        /// </summary>
        /// <param name="clientEvent">Arbitration event to send.</param>
        private void SendEvent(ArbitrationEvent clientEvent)
        {
            this.moveArbitrator.SendEvent(EventSet.Serialize(clientEvent));
        }

        /// <summary>
        /// Send move event to the arbitration server.
        /// </summary>
        private void SendMoveArbitrationEvent()
        {
            if (!this.connectedToMoveArbitrator)
            {
                return;
            }

            lock (AvatarListLock)
            {
                for (int i = 0; i < AvatarList.Count; ++i)
                {
                    var localAvatar = AvatarList[i] as LocalAvatar;

                    if (localAvatar != null)
                    {
                        this.SendEvent(new MoveEvent(localAvatar.Position));
                    }
                }
            }
        }

        /// <summary>
        /// Disconnect from arbitration service.
        /// </summary>
        private void DisconnectFromArbitration()
        {
            if (!this.arbitrationInterval.HasValue)
            {
                return;
            }

            if (this.connectedToMoveArbitrator)
            {
                this.SendEvent(new OfflineEvent());
            }
        }
    }
}
