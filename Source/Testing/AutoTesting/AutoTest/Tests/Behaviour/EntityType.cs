﻿//------------------------------------------------------------------------------
// <copyright file="EntityType.cs" company="National ICT Australia Limited">
//     Copyright (c) 2013 National ICT Australia Limited. All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

namespace AutoTest.Tests.Behaviour
{
    /// <summary>
    /// Enumeration defining types of entities in the game.
    /// </summary>
    public enum EntityType : uint
    {
        /// <summary>
        /// Entity is an avatar.
        /// </summary>
        Avatar,

        /// <summary>
        /// Ping entity.
        /// </summary>
        Ping,

        /// <summary>
        /// Pong entity.
        /// </summary>
        Pong
    }
}
