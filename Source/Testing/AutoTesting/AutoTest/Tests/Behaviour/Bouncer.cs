﻿//------------------------------------------------------------------------
// <copyright file="Bouncer.cs" company="Scalify">
//      Copyright (c) 2012 Scalify. All rights reserved.
// </copyright>
//------------------------------------------------------------------------

namespace AutoTest.Tests.Behaviour
{
    using System;
    using Badumna;
    using Badumna.DataTypes;

    /// <summary>
    /// An avatar that moves between two points.
    /// </summary>
    public class Bouncer : LocalAvatar
    {
        /// <summary>
        /// Amount of time to wait in the rotate state before moving on.
        /// </summary>
        private static readonly TimeSpan RotateStateInterval = TimeSpan.FromSeconds(15.0);

        /// <summary>
        /// Flag indicating the first update.
        /// </summary>
        private bool firstUpdate = true;

        /// <summary>
        /// Amount of time spent in rotate state.
        /// </summary>
        private TimeSpan totalRotateTime;

        /// <summary>
        /// First target point.
        /// </summary>
        private Vector3 firstPoint = new Vector3();

        /// <summary>
        /// Second target point.
        /// </summary>
        private Vector3 secondPoint = new Vector3();

        /// <summary>
        /// Initializes a new instance of the Bouncer class.
        /// </summary>
        /// <param name="networkFacade">Network facade.</param>
        public Bouncer(INetworkFacade networkFacade)
            : base(networkFacade)
        {
            this.firstPoint = this.CalculateRandomDestination();
            this.secondPoint = this.CalculateRandomDestination();
        }

        /// <summary>
        /// Initializes a new instance of the Bouncer class.
        /// </summary>
        /// <param name="networkFacade">Network facade.</param>
        /// <param name="moveAmount">Move amount.</param>
        /// <param name="rotateAmount">Rotate amount.</param>
        /// <param name="x0">X coordinate of first point.</param>
        /// <param name="y0">Y coordinate of first point.</param>
        /// <param name="x1">X coordinate of second point.</param>
        /// <param name="y1">Y coordinate of second point.</param>
        public Bouncer(INetworkFacade networkFacade, float moveAmount, float rotateAmount, float x0, float y0, float x1, float y1)
            : base(networkFacade)
        {
            if (moveAmount > 0.0f)
            {
                this.MoveAmount = moveAmount;
            }

            if (rotateAmount > 0.0f)
            {
                this.RotateAmount = rotateAmount;
            }

            this.firstPoint = new Vector3(x0, y0, 0.0f);
            this.secondPoint = new Vector3(x1, y1, 0.0f);
        }

        public static Bouncer Create(INetworkFacade networkFacade, string config)
        {
            var args = config.Split(';');
            if (args.Length == 6)
            {
                return new Bouncer(
                    networkFacade,
                    float.Parse(args[0]),
                    float.Parse(args[1]),
                    float.Parse(args[2]),
                    float.Parse(args[3]),
                    float.Parse(args[4]),
                    float.Parse(args[5]));
            }

            return new Bouncer(networkFacade);
        }

        /// <summary>
        /// Method called every update.
        /// </summary>
        /// <param name="timeStep">Time since last update.</param>
        public override void Update(TimeSpan interval)
        {
            if (this.firstUpdate)
            {
                this.Position = this.firstPoint;
                this.Destination = this.firstPoint;
                this.firstUpdate = false;
            }

            switch (this.CurrentState)
            {
                case EntityState.Target:
                    {
                        if (this.Destination != this.firstPoint)
                        {
                            this.Destination = this.firstPoint;
                        }
                        else
                        {
                            this.Destination = this.secondPoint;
                        }

                        this.CurrentState = EntityState.Rotate;
                        this.totalRotateTime = TimeSpan.Zero;
                    }

                    break;

                case EntityState.Rotate:
                    {
                        Vector3 zeroVector = new Vector3(0.0f, 0.0f, 0.0f);
                        Vector3 forwardVector = new Vector3(1.0f, 0.0f, 0.0f);
                        Vector3 upwardVector = new Vector3(0.0f, 0.0f, -1.0f);
                        Vector3 deltaVector = this.Destination - this.Position;

                        this.totalRotateTime += interval;

                        if (deltaVector == zeroVector)
                        {
                            this.CurrentState = EntityState.Target;
                        }
                        else
                        {
                            float angle = (float)this.GetAngleXY(forwardVector, upwardVector, deltaVector);
                            float deltaAngle = angle - this.Orientation;

                            if (deltaAngle > 0.0f)
                            {
                                if (deltaAngle < this.RotateAmount * interval.TotalSeconds)
                                {
                                    this.Orientation = angle;

                                    if (this.totalRotateTime >= Bouncer.RotateStateInterval)
                                    {
                                        this.CurrentState = EntityState.Move;
                                    }
                                }
                                else if (deltaAngle > 180.0f)
                                {
                                    this.Orientation -= this.RotateAmount * (float)interval.TotalSeconds;
                                }
                                else
                                {
                                    this.Orientation += this.RotateAmount * (float)interval.TotalSeconds;
                                }
                            }
                            else
                            {
                                if (deltaAngle > -this.RotateAmount * interval.TotalSeconds)
                                {
                                    this.Orientation = angle;

                                    if (this.totalRotateTime >= Bouncer.RotateStateInterval)
                                    {
                                        this.CurrentState = EntityState.Move;
                                    }
                                }
                                else if (deltaAngle < -180.0f)
                                {
                                    this.Orientation += this.RotateAmount * (float)interval.TotalSeconds;
                                }
                                else
                                {
                                    this.Orientation -= this.RotateAmount * (float)interval.TotalSeconds;
                                }
                            }
                        }
                    }

                    break;

                case EntityState.Move:
                    {
                        Vector3 deltaVector = this.Destination - this.Position;
                        float distanceSquared = (deltaVector.X * deltaVector.X) + (deltaVector.Y * deltaVector.Y) + (deltaVector.Z * deltaVector.Z);

                        if (distanceSquared < (this.MoveAmount * this.MoveAmount * interval.TotalSeconds * interval.TotalSeconds))
                        {
                            this.Position = this.Destination;
                            this.CurrentState = EntityState.Target;
                        }
                        else
                        {
                            Vector3 unitDeltaVector = deltaVector / deltaVector.Magnitude;
                            this.Position += unitDeltaVector * this.MoveAmount * (float)interval.TotalSeconds;
                            this.CurrentState = EntityState.Rotate;
                        }
                    }

                    break;
            }

            base.Update(interval);
        }
    }
}