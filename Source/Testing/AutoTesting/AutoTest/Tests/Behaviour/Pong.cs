﻿//-----------------------------------------------------------------------
// <copyright file="Pong.cs" company="Scalify">
//     Copyright (c) 2013 Scalify. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace AutoTest.Tests.Behaviour
{
    using System;

    using Badumna;
    using Badumna.Autoreplication;

    /// <summary>
    /// Pong entity
    /// </summary>
    public class Pong : Avatar
    {
        /// <summary>
        /// Ping sequence number.
        /// </summary>
        private int sequence;

        /// <summary>
        /// Initializes a new instance of the <see cref="Pong"/> class.
        /// </summary>
        /// <param name="eventRecordUpdater">Event record updater.</param>
        public Pong(EventRecordUpdater eventRecordUpdater)
            : base(eventRecordUpdater)
        {
        }

        /// <summary>
        /// Gets or sets sequence number.
        /// </summary>
        [Replicable]
        public int Sequence
        {
            get
            {
                return this.sequence;
            }

            set
            {
                this.sequence = value;

                if (this.eventRecordUpdater != null)
                {
                    string data = string.Format(
                        "GUID={0}, Pong sequence={1}, TimeStamp={2}",
                        this.Guid,
                        this.sequence,
                        DateTime.Now.Ticks);
                    this.eventRecordUpdater("Pong Sequence Increment", data);
                }
            }
        }

        /// <summary>
        /// Update the sequence number with the given value
        /// </summary>
        /// <param name="seq">Given sequence number.</param>
        public void UpdateSequence(int seq)
        {
            this.sequence = seq;
        }
    }
}
