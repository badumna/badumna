﻿// -----------------------------------------------------------------------
// <copyright file="IBehaviour.cs" company="Scalify">
//     Copyright (c) 2012 Scalify. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System;

namespace AutoTest.Tests.Behaviour
{
    /// <summary>
    /// IBehaviour interface.
    /// </summary>
    public interface IBehaviour
    {
        /// <summary>
        /// Gets the player Unique id.
        /// </summary>
        string Guid { get; }

        /// <summary>
        /// Update method.
        /// </summary>
        /// <param name="timeStep">Time since last update.</param>
        void Update(TimeSpan interval);
    }
}
