﻿//------------------------------------------------------------------------
// <copyright file="Follower.cs" company="Scalify">
//      Copyright (c) 2012 Scalify. All rights reserved.
// </copyright>
//------------------------------------------------------------------------

namespace AutoTest.Tests.Behaviour
{
    using System;

    using AutoTest.Tests;
    
    using Badumna;
    using Badumna.DataTypes;

    /// <summary>
    /// An avatar that follows another avatar.
    /// </summary>
    public class Follower : LocalAvatar
    {
        /// <summary>
        /// Initializes a new instance of the Follower class.
        /// </summary>
        /// <param name="networkFacade">Network facade.</param>
        public Follower(INetworkFacade networkFacade)
            : base(networkFacade)
        {
            this.TargetGuid = null;
        }

        /// <summary>
        /// Initializes a new instance of the Follower class.
        /// </summary>
        /// <param name="networkFacade">Network facade.</param>
        /// <param name="moveAmount">Move amount.</param>
        /// <param name="rotateAmount">Rotate amount.</param>
        /// <param name="regionBounds">Region bounds.</param>
        public Follower(INetworkFacade networkFacade, float moveAmount, float rotateAmount, float[] regionBounds)
            : base(networkFacade, regionBounds)
        {
            this.TargetGuid = null;

            if (moveAmount > 0.0f)
            {
                this.MoveAmount = moveAmount;
            }

            if (rotateAmount > 0.0f)
            {
                this.RotateAmount = rotateAmount;
            }            
        }

        public static Follower Create(INetworkFacade networkFacade, string config)
        {
            var args = config.Split(';');
            if (args.Length == 6)
            {
                return new Follower(
                    networkFacade,
                    float.Parse(args[0]),
                    float.Parse(args[1]),
                    new float[] { float.Parse(args[2]), float.Parse(args[3]), float.Parse(args[4]), float.Parse(args[5]), 0 });
            }

            return new Follower(networkFacade);
        }

        /// <summary>
        /// Gets or sets the target GUID.
        /// </summary>
        protected string TargetGuid { get; set; }

        /// <summary>
        /// Method called every update.
        /// </summary>
        /// <param name="timeStep">Time since last update.</param>
        public override void Update(TimeSpan interval)
        {
            switch (this.CurrentState)
            {
                case EntityState.Target:
                    {
                        bool targetFound = false;

                        lock (AutoTestPeer.AvatarListLock)
                        {
                            for (int i = 0; i < AutoTestPeer.AvatarList.Count; ++i)
                            {
                                Avatar avatar = AutoTestPeer.AvatarList[i] as Avatar;

                                if ((this.TargetGuid != null) && (avatar.Guid == this.TargetGuid))
                                {
                                    this.Destination = avatar.Position;
                                    targetFound = true;
                                    break;
                                }
                            }
                        }

                        if (targetFound == false)
                        {
                            Vector3 zeroVector = new Vector3(0.0f, 0.0f, 0.0f);
                            float distance = 3.40282e+038f;

                            lock (AutoTestPeer.AvatarListLock)
                            {
                                for (int i = 0; i < AutoTestPeer.AvatarList.Count; ++i)
                                {
                                    Avatar avatar = AutoTestPeer.AvatarList[i] as Avatar;

                                    if (avatar.Guid != this.Guid)
                                    {
                                        Vector3 deltaVector = avatar.Position - this.Position;

                                        if ((deltaVector == zeroVector) || (deltaVector.Magnitude < distance))
                                        {
                                            this.TargetGuid = avatar.Guid;
                                            this.Destination = avatar.Position;
                                            distance = deltaVector.Magnitude;
                                            targetFound = true;
                                        }
                                    }
                                }
                            }
                        }

                        if (targetFound == true)
                        {
                            this.CurrentState = EntityState.Rotate;
                        }
                    }

                    break;

                case EntityState.Rotate:
                    {
                        Vector3 zeroVector = new Vector3(0.0f, 0.0f, 0.0f);
                        Vector3 forwardVector = new Vector3(1.0f, 0.0f, 0.0f);
                        Vector3 upwardVector = new Vector3(0.0f, 0.0f, -1.0f);
                        Vector3 deltaVector = this.Destination - this.Position;

                        if (deltaVector == zeroVector)
                        {
                            this.CurrentState = EntityState.Target;
                        }
                        else
                        {
                            float angle = (float)this.GetAngleXY(forwardVector, upwardVector, deltaVector);
                            float deltaAngle = angle - this.Orientation;

                            if (deltaAngle > 0.0f)
                            {
                                if (deltaAngle < this.RotateAmount * interval.TotalSeconds)
                                {
                                    this.Orientation = angle;
                                    this.CurrentState = EntityState.Move;
                                }
                                else if (deltaAngle < 90.0f)
                                {
                                    this.Orientation += this.RotateAmount * (float)interval.TotalSeconds;
                                    this.CurrentState = EntityState.Move;
                                }
                                else if (deltaAngle < 180.0f)
                                {
                                    this.Orientation += this.RotateAmount * (float)interval.TotalSeconds;
                                    this.CurrentState = EntityState.Target;
                                }
                                else
                                {
                                    this.Orientation -= this.RotateAmount * (float)interval.TotalSeconds;
                                    this.CurrentState = EntityState.Target;
                                }
                            }
                            else
                            {
                                if (deltaAngle > -this.RotateAmount * interval.TotalSeconds)
                                {
                                    this.Orientation = angle;
                                    this.CurrentState = EntityState.Move;
                                }
                                else if (deltaAngle > -90.0f)
                                {
                                    this.Orientation -= this.RotateAmount * (float)interval.TotalSeconds;
                                    this.CurrentState = EntityState.Move;
                                }
                                else if (deltaAngle > -180.0f)
                                {
                                    this.Orientation -= this.RotateAmount * (float)interval.TotalSeconds;
                                    this.CurrentState = EntityState.Target;
                                }
                                else
                                {
                                    this.Orientation += this.RotateAmount * (float)interval.TotalSeconds;
                                    this.CurrentState = EntityState.Target;
                                }
                            }
                        }
                    }

                    break;

                case EntityState.Move:
                    {
                        Vector3 position = this.Position;
                        Vector3 forwardVector = new Vector3();
                        double angle = this.Orientation * Math.PI / 180.0f;
                        forwardVector.X = (float)(this.MoveAmount * interval.TotalSeconds * Math.Cos(angle));
                        forwardVector.Y = (float)(this.MoveAmount * interval.TotalSeconds * Math.Sin(angle));
                        position += forwardVector;

                        if (position.X < this.RegionBounds[(int)BoundIndex.boundLeft])
                        {
                            position.X = this.RegionBounds[(int)BoundIndex.boundLeft];
                        }
                        else if (position.X > this.RegionBounds[(int)BoundIndex.boundRight])
                        {
                            position.X = this.RegionBounds[(int)BoundIndex.boundRight];
                        }

                        if (position.Y < this.RegionBounds[(int)BoundIndex.boundTop])
                        {
                            position.Y = this.RegionBounds[(int)BoundIndex.boundTop];
                        }
                        else if (position.Y > this.RegionBounds[(int)BoundIndex.boundBottom])
                        {
                            position.Y = this.RegionBounds[(int)BoundIndex.boundBottom];
                        }

                        this.Position = position;
                        this.CurrentState = EntityState.Target;
                    }

                    break;
            }

            base.Update(interval);
        }
    }
}