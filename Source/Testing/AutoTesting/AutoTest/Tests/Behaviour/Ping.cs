﻿//-----------------------------------------------------------------------
// <copyright file="Ping.cs" company="Scalify">
//     Copyright (c) 2013 Scalify. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace AutoTest.Tests.Behaviour
{
    using System;

    using Badumna;
    using Badumna.Autoreplication;

    /// <summary>
    /// Delegate for notify that a new ping sequence recevied.
    /// </summary>
    /// <param name="sequence">Sequence number.</param>
    public delegate void PingSequenceHandler(int sequence);

    /// <summary>
    /// Ping entity.
    /// </summary>
    public class Ping : Avatar
    {
        /// <summary>
        /// Time interval in between regular sequence updates.
        /// </summary>
        private static readonly TimeSpan SequenceUpdateInterval = TimeSpan.FromSeconds(10.0);

        /// <summary>
        /// Ping sequence number.
        /// </summary>
        private int sequence;

        /// <summary>
        /// Cumulative time.
        /// </summary>
        private TimeSpan cumulativeTime;

        /// <summary>
        /// On ping sequence changed event.
        /// </summary>
        public event PingSequenceHandler OnPingSequenceChanged;

        /// <summary>
        /// Initializes a new instance of the <see cref="Ping"/> class.
        /// </summary>
        /// <param name="eventRecordUpdater">Event record updater.</param>
        public Ping(EventRecordUpdater eventRecordUpdater)
            : base(eventRecordUpdater)
        {
        }

        /// <summary>
        /// Gets or sets sequence number.
        /// </summary>
        [Replicable]
        public int Sequence
        {
            get
            {
                return this.sequence;
            }

            set
            {
                this.sequence = value;
                if (this.OnPingSequenceChanged != null)
                {
                    this.OnPingSequenceChanged(value);
                }
            }
        }

        ///<inheritdoc/>
        public override void Update(TimeSpan interval)
        {
            this.cumulativeTime += interval;

            if (this.cumulativeTime >= SequenceUpdateInterval)
            {
                this.sequence++;
                this.cumulativeTime = TimeSpan.Zero;

                if (this.eventRecordUpdater != null)
                {
                    string data = string.Format(
                        "GUID={0}, Sequence={1}, TimeStamp={2}",
                        this.Guid,
                        this.sequence,
                        DateTime.Now.Ticks);
                    this.eventRecordUpdater("Ping Sequence Increment", data);
                }
            }
        }
    }
}
