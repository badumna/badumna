﻿//------------------------------------------------------------------------
// <copyright file="RandomWalker.cs" company="Scalify">
//      Copyright (c) 2012 Scalify. All rights reserved.
// </copyright>
//------------------------------------------------------------------------

namespace AutoTest.Tests.Behaviour
{
    using System;
    using Badumna;
    using Badumna.DataTypes;

    /// <summary>
    /// An avatar that wanders randomly.
    /// </summary>
    public class RandomWalker : LocalAvatar
    {
        /// <summary>
        /// Initializes a new instance of the RandomWalker class.
        /// </summary>
        /// <param name="networkFacade">Network facade.</param>
        public RandomWalker(INetworkFacade networkFacade)
            : base(networkFacade)
        {
        }

        /// <summary>
        /// Initializes a new instance of the RandomWalker class.
        /// </summary>
        /// <param name="networkFacade">Network facade.</param>
        /// <param name="moveAmount">Move amount.</param>
        /// <param name="rotateAmount">Rotate amount.</param>
        /// <param name="regionBounds">Region bounds.</param>
        public RandomWalker(INetworkFacade networkFacade, float moveAmount, float rotateAmount, float[] regionBounds)
            : base(networkFacade, regionBounds)
        {
            if (moveAmount > 0.0f)
            {
                this.MoveAmount = moveAmount;
            }

            if (rotateAmount > 0.0f)
            {
                this.RotateAmount = rotateAmount;
            }
        }

        public static RandomWalker Create(INetworkFacade networkFacade, string config)
        {
            var args = config.Split(';');
            if (args.Length == 6)
            {
                return new RandomWalker(
                    networkFacade,
                    float.Parse(args[0]),
                    float.Parse(args[1]),
                    new float[] { float.Parse(args[2]), float.Parse(args[3]), float.Parse(args[4]), float.Parse(args[5]), 0 });
            }

            return new RandomWalker(networkFacade);
        }

        /// <summary>
        /// Method called every update.
        /// </summary>
        /// <param name="timeStep">Time since last update.</param>
        public override void Update(TimeSpan interval)
        {
            switch (this.CurrentState)
            {
                case EntityState.Target:
                    {
                        this.Destination = this.CalculateRandomDestination();
                        this.CurrentState = EntityState.Rotate;
                    }

                    break;

                case EntityState.Rotate:
                    {
                        Vector3 zeroVector = new Vector3(0.0f, 0.0f, 0.0f);
                        Vector3 forwardVector = new Vector3(1.0f, 0.0f, 0.0f);
                        Vector3 upwardVector = new Vector3(0.0f, 0.0f, -1.0f);
                        Vector3 deltaVector = this.Destination - this.Position;

                        if (deltaVector == zeroVector)
                        {
                            this.CurrentState = EntityState.Target;
                        }
                        else
                        {
                            float angle = (float)this.GetAngleXY(forwardVector, upwardVector, deltaVector);
                            float deltaAngle = angle - this.Orientation;

                            if (deltaAngle > 0.0f)
                            {
                                if (deltaAngle < this.RotateAmount * interval.TotalSeconds)
                                {
                                    this.Orientation = angle;
                                    this.CurrentState = EntityState.Move;
                                }
                                else if (deltaAngle > 180.0f)
                                {
                                    this.Orientation -= this.RotateAmount * (float)interval.TotalSeconds;
                                }
                                else
                                {
                                    this.Orientation += this.RotateAmount * (float)interval.TotalSeconds;
                                }
                            }
                            else
                            {
                                if (deltaAngle > -this.RotateAmount * interval.TotalSeconds)
                                {
                                    this.Orientation = angle;
                                    this.CurrentState = EntityState.Move;
                                }
                                else if (deltaAngle < -180.0f)
                                {
                                    this.Orientation += this.RotateAmount * (float)interval.TotalSeconds;
                                }
                                else
                                {
                                    this.Orientation -= this.RotateAmount * (float)interval.TotalSeconds;
                                }
                            }
                        }
                    }

                    break;

                case EntityState.Move:
                    {
                        Vector3 deltaVector = this.Destination - this.Position;
                        float distanceSquared = (deltaVector.X * deltaVector.X) + (deltaVector.Y * deltaVector.Y) + (deltaVector.Z * deltaVector.Z);

                        if (distanceSquared < (this.MoveAmount * this.MoveAmount * (float)interval.TotalSeconds * (float)interval.TotalSeconds))
                        {
                            this.Position = this.Destination;
                            this.CurrentState = EntityState.Target;
                        }
                        else
                        {
                            Vector3 unitDeltaVector = deltaVector / deltaVector.Magnitude;
                            this.Position += unitDeltaVector * this.MoveAmount * (float)interval.TotalSeconds;
                            this.CurrentState = EntityState.Rotate;
                        }
                    }

                    break;
            }

            base.Update(interval);
        }
    }
}