﻿//-----------------------------------------------------------------------
// <copyright file="UnityWalker.cs" company="Scalify">
//     Copyright (c) 2012 Scalify. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace AutoTest.Tests.Behaviour
{
    using System;
    using Badumna;

    using Badumna.Autoreplication;
    using Badumna.DataTypes;

    /// <summary>
    /// The unity walker.
    /// </summary>
    public class UnityWalker : RandomWalker
    {
        /// <summary>
        /// Reference to network facade.
        /// </summary>
        private readonly INetworkFacade network;        

        /// <summary>
        /// Initializes a new instance of the <see cref="UnityWalker"/> class.
        /// </summary>
        /// <param name="networkFacade">The network facade.</param>
        public UnityWalker(INetworkFacade networkFacade)
            : base(networkFacade, 2.25f, 0.0f, new float[] { -10, 10, -10, 10, -17.33f })
        {
            this.network = networkFacade;
            this.AnimationName = "idle";
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UnityWalker"/> class.
        /// </summary>
        /// <param name="networkFacade">The network facade.</param>
        /// <param name="moveAmount">The move amount.</param>
        /// <param name="rotateAmount">The rotate amount.</param>
        /// <param name="regionBounds">Region bounds.</param>
        public UnityWalker(INetworkFacade networkFacade, float moveAmount, float rotateAmount, float[] regionBounds)
            : base(networkFacade, moveAmount, rotateAmount, regionBounds)
        {
            this.network = networkFacade;
            this.AnimationName = "idle";
        }

        public static new UnityWalker Create(INetworkFacade networkFacade, string config)
        {
            var args = config.Split(';');
            if (args.Length == 7)
            {
                return new UnityWalker(
                    networkFacade,
                    float.Parse(args[0]),
                    float.Parse(args[1]),
                    new float[] { float.Parse(args[2]), float.Parse(args[3]), float.Parse(args[4]), float.Parse(args[5]), float.Parse(args[6]) });
            }

            return new UnityWalker(networkFacade);
        }

        /// <summary>
        /// Gets or sets the velocity.
        /// </summary>
        public Vector3 Velocity { get; set; }

        /// <inheritdoc/>
        public override void Update(TimeSpan interval)
        {
            switch (this.CurrentState)
            {
                case EntityState.Target:
                    this.Destination = this.CalculateRandomDestination();
                    this.CurrentState = EntityState.Move;
                    break;
                case EntityState.Move:
                    var direction = (this.Destination - this.Position).Normalize();
                    direction *= this.MoveAmount;
                    var path = direction * (float)interval.TotalSeconds;

                    // remaining distance in 2D
                    var remainingDistance =
                        (float)
                        Math.Sqrt(
                            Math.Pow(this.Destination.X - this.Position.X, 2)
                            + Math.Pow(this.Destination.Z - this.Position.Z, 2));

                    var path2D = new Vector3(path.X, 0, path.Z);

                    if (remainingDistance <= path2D.Magnitude || remainingDistance < 0.5f)
                    {
                        this.CurrentState = EntityState.Target;
                        this.AnimationName = "idle";
                        this.Velocity = new Vector3(0, 0, 0);
                    }
                    else
                    {
                        var temp = this.Position + path;
                        temp.Y = this.RegionBounds[(int)BoundIndex.height];

                        var orientation = (float)(Math.Atan2(direction.X, direction.Z) * 180.0f / Math.PI);

                        this.Orientation = orientation;
                        this.Position = temp;
                        this.AnimationName = "walk";
                        this.Velocity = direction;
                    }

                    break;
            }
        }                
    }
}
