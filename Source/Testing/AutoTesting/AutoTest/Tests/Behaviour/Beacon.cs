﻿//------------------------------------------------------------------------
// <copyright file="Beacon.cs" company="Scalify">
//      Copyright (c) 2012 Scalify. All rights reserved.
// </copyright>
//------------------------------------------------------------------------

namespace AutoTest.Tests.Behaviour
{
    using System;
    using System.Threading;
    using Badumna;
    using Badumna.DataTypes;

    /// <summary>
    /// An avatar that makes a move at regular intervals.
    /// </summary>
    public class Beacon : LocalAvatar
    {
        /// <summary>
        /// Time interval for the avatar to make a move.
        /// </summary>
        private float timeInterval = 10.0f;

        /// <summary>
        /// Float used to time movements.
        /// </summary>
        private Timer timer;

        /// <summary>
        /// Indication of move direction.
        /// </summary>
        private int moveDirection = 1;

        /// <summary>
        /// Initializes a new instance of the Beacon class.
        /// </summary>
        /// <param name="networkFacade">Network facade.</param>
        public Beacon(INetworkFacade networkFacade)
            : base(networkFacade)
        {
            this.MoveAmount = 10.0f;
        }

        /// <summary>
        /// Initializes a new instance of the Beacon class.
        /// </summary>
        /// <param name="networkFacade">Network facade.</param>
        /// <param name="timeInterval">Time interval.</param>
        /// <param name="regionBounds">Region bounds.</param>
        public Beacon(INetworkFacade networkFacade, float timeInterval, float[] regionBounds)
            : base(networkFacade, regionBounds)
        {
            this.MoveAmount = 10.0f;

            if (timeInterval > 0.0f)
            {
                this.timeInterval = timeInterval;
                this.timer = new Timer(
                    (object state) => { this.CurrentState = EntityState.Move; }, 
                    null, 
                    0, 
                    (int)TimeSpan.FromSeconds(this.timeInterval).TotalMilliseconds);
            }            
        }

        public static Beacon Create(INetworkFacade networkFacade, string config)
        {
            var args = config.Split(';');
            if (args.Length == 5)
            {
                return new Beacon(
                    networkFacade,
                    float.Parse(args[0]),
                    new float[] { float.Parse(args[1]), float.Parse(args[2]), float.Parse(args[3]), float.Parse(args[4]), 0 });
            }

            return new Beacon(networkFacade);
        }

        /// <summary>
        /// Method called every update.
        /// </summary>
        /// <param name="timeStep">Time since last update.</param>
        public override void Update(TimeSpan interval)
        {
            switch (this.CurrentState)
            {
                case EntityState.Move:
                    {
                        Vector3 forwardVector = new Vector3(1f, 0f, 0f);
                        this.Position += forwardVector * this.MoveAmount * this.moveDirection;
                        this.moveDirection *= -1;
                        this.CurrentState = EntityState.Target;
                    }

                    break;
            }

            base.Update(interval);
        }
    }
}