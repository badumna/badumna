﻿//-----------------------------------------------------------------------
// <copyright file="AutoTestPingPong.cs" company="Scalify">
//     Copyright (c) 2013 Scalify. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace AutoTest.Tests
{
    using System;
    using System.Collections.Generic;

    using AutoTest.Tests.Behaviour;

    using Mono.Options;
    using Badumna;
    using Badumna.SpatialEntities;
    using Badumna.DataTypes;
    using System.Xml;
    using System.Diagnostics;

    /// <summary>
    /// Automated ping pong test.
    /// </summary>
    public class AutoTestPingPong : AutoTestBase
    {
        /// <summary>
        /// List of remote avatars.
        /// </summary>
        private readonly List<Avatar> remoteAvatarList = new List<Avatar>();

        /// <summary>
        /// List of local avatars.
        /// </summary>
        private readonly List<Avatar> localAvatarList = new List<Avatar>();

        /// <summary>
        /// Flag indicating whether pinging was set using command line option.
        /// </summary>
        private bool setPingIsSet = false;

        /// <summary>
        /// Area of interest radius of entities.
        /// </summary>
        private float areaOfInterest = 20.0f;

        /// <summary>
        /// Initializes a new instance of the <see cref="AutoTestPingPong"/> class.
        /// </summary>
        public AutoTestPingPong()
        {
            this.optionSet.Add(
                Constants.SetPingString,
                "Set as ping peer.",
                var => { this.setPingIsSet = true; });
        }

        ///<inheritdoc/>
        public override string ToString()
        {
            return "AutoTestPingPong";
        }

        /// <summary>
        /// Add avatar to the scene.
        /// </summary>
        /// <param name="avatar">Avatar object.</param>
        /// <param name="type">Avatar type.</param>
        public void AddAvatar(Avatar avatar, EntityType type)
        {
            if (avatar == null)
            {
                return;
            }

            this.scene.RegisterEntity(avatar, (uint)type, 2.0f, this.areaOfInterest);
            this.localAvatarList.Add(avatar);

            var data = string.Format(
                "GUID={0}",
                avatar.Guid);
            this.RecordEvent("Add Avatar", data);
        }

        ///<inheritdoc/>
        protected override void Update(TimeSpan interval)
        {
            if (this.forceExit)
            {
                this.Stop();
            }

            this.UpdateProcess(interval);
        }

        ///<inheritdoc/>
        protected override void OnStartUp()
        {
            base.OnStartUp();
            this.StartBadumna();
        }

        ///<inheritdoc/>
        protected override void OnShutDown()
        {
            this.ShutdownBadumna();
            base.OnShutDown();
        }

        /// <inheritdoc/>
        protected override void UpdateRecord()
        {
            var currentTime = DateTime.Now;
            var processId = Process.GetCurrentProcess().Id;
            var activeConnectionCount = 0;
            var initializingConnectionCount = 0;
            var totalBytesSentPerSecond = 0;
            var totalBytesReceivedPerSecond = 0;
            var remoteEntityCount = this.remoteAvatarList.Count;
            var receivedUpdateCount = 0;

            if (this.network != null)
            {
                XmlDocument networkStatus = this.network.GetNetworkStatus().ToXml();
                activeConnectionCount = int.Parse(networkStatus.SelectSingleNode("/Status/Connectivity/" + Constants.ActiveConnectionsString).InnerText);
                initializingConnectionCount = int.Parse(networkStatus.SelectSingleNode("/Status/Connectivity/" + Constants.InitializingConnectionsString).InnerText);
                totalBytesSentPerSecond = int.Parse(networkStatus.SelectSingleNode("/Status/TransferRate/" + Constants.TotalBytesSentString).InnerText);
                totalBytesReceivedPerSecond = int.Parse(networkStatus.SelectSingleNode("/Status/TransferRate/" + Constants.TotalBytesReceivedString).InnerText);
            }
           
            var machineStatus = this.performanceMonitor.GetMachineStatus();
            this.RecordStats(activeConnectionCount, totalBytesSentPerSecond, totalBytesReceivedPerSecond, remoteEntityCount, initializingConnectionCount, receivedUpdateCount, machineStatus[0], machineStatus[1]);

            var replicas = string.Empty;
            this.RecordReplica(remoteEntityCount, replicas);
        }

        /// <summary>
        /// Initialize Badumna functionality.
        /// </summary>
        private void StartBadumna()
        {
            if (!string.IsNullOrEmpty(this.cloudIdentifier))
            {
                this.network = NetworkFacade.Create(this.cloudIdentifier);
            }
            else
            {
                this.network = NetworkFacade.Create(this.GenerateOptions());
            }

            for (var i = 0; i < BadumnaLoginAttempts; ++i)
            {
                    this.network.Login();

                if (this.network.IsLoggedIn)
                {
                    this.RecordEvent("Badumna Log In", this.network.GetNetworkStatus().ToString());

                    this.network.RegisterEntityDetails(
                        this.areaOfInterest,
                        0.0f);

                    this.network.TypeRegistry.RegisterValueType(
                    (c, w) =>
                    {
                        w.Write(c.r);
                        w.Write(c.g);
                        w.Write(c.b);
                    },
                    r => new UnityEngine.Color(r.ReadSingle(), r.ReadSingle(), r.ReadSingle()));

                    if (!string.IsNullOrEmpty(this.sceneName))
                    {
                        this.scene = this.network.JoinScene(
                            this.sceneName, this.CreateSpatialReplica, this.RemoveSpatialReplica);
                    }

                    if (this.setPingIsSet)
                    {
                        this.AddAvatar(new Ping(this.RecordEvent) { Guid = System.Guid.NewGuid().ToString() }, EntityType.Ping);
                    }
                    else
                    {
                        this.AddAvatar(new Pong(this.RecordEvent) { Guid = System.Guid.NewGuid().ToString() }, EntityType.Pong);
                    }

                    break;
                }

                this.RecordLog("Failed to login to Network Facade.");
            }

            if (!this.network.IsLoggedIn)
            {
                this.forceExit = true;
            }
        }

        /// <summary>
        /// Generate the network configuration for Badumna.
        /// </summary>
        /// <returns>Network configuration.</returns>
        private Options GenerateOptions()
        {
            Options options = new Options();
            options.Logger.LoggerType = LoggerType.DotNetTrace;
            options.Logger.LoggerConfig = "<Logger Type=\"DotNetTraceLogger\" Enabled=\"true\"><Verbosity>Information</Verbosity><TraceFile Enabled=\"true\" ></TraceFile><TraceConsole Enabled=\"false\" /></Logger>";
            options.Logger.IncludeTags = Badumna.LogTag.Event | Badumna.LogTag.Overload | Badumna.LogTag.Connection;

            if (!string.IsNullOrEmpty(this.appName))
            {
                options.Connectivity.ApplicationName = this.appName;
            }

            if (!string.IsNullOrEmpty(this.seedPeer))
            {
                options.Connectivity.SeedPeers.Add(this.seedPeer);
            }
            else
            {
                options.Connectivity.ConfigureForLan();
            }

            return options;
        }

        /// <summary>
        /// Creates a remote avatar and adds it to the avatar list.
        /// </summary>
        /// <param name="scene">Current scene.</param>
        /// <param name="entityId">ID of the entity</param>
        /// <param name="entityType">Type of the entity.</param>
        /// <returns>Spatial replica of the remote avatar.</returns>
        private IReplicableEntity CreateSpatialReplica(NetworkScene scene, BadumnaId entityId, uint entityType)
        {
            if (entityType == (uint)EntityType.Ping || entityType == (uint)EntityType.Pong)
            {
                var data = string.Format(
                    "Badumna ID={0}",
                    entityId);
                this.RecordEvent("Create Replica", data);

                Avatar replica = null;

                if (entityType == (uint)EntityType.Ping)
                {
                    replica = new Ping(this.RecordEvent);
                    (replica as Ping).OnPingSequenceChanged += this.OnPingSequenceChangedHandler;
                }
                else
                {
                    replica = new Pong(this.RecordEvent);
                }

                this.remoteAvatarList.Add(replica);

                return replica;
            }

            return null;
        }

        /// <summary>
        /// Removes remote avatar from the avatar list.
        /// </summary>
        /// <param name="scene">Current scene.</param>
        /// <param name="replica">Spatial replica to remove.</param>
        private void RemoveSpatialReplica(NetworkScene scene, IReplicableEntity replica)
        {
            if (replica != null)
            {
                this.remoteAvatarList.Remove((replica as Avatar));

                if (replica is Ping)
                {
                    (replica as Ping).OnPingSequenceChanged -= this.OnPingSequenceChangedHandler;
                }
            }

            var data = string.Format(
                "GUID={0}",
                (replica as Avatar).Guid);
            this.RecordEvent("Remove Replica", data);
        }

        /// <summary>
        /// Shut down Badumna functionality.
        /// </summary>
        private void ShutdownBadumna()
        {
            if (this.scene != null)
            {
                foreach (var avatar in this.localAvatarList)
                {
                    this.scene.UnregisterEntity(avatar);
                }

                this.scene.Leave();
                this.scene = null;
            }

            this.localAvatarList.Clear();
            
            this.RecordEvent("Badumna Shutdown", this.network.GetNetworkStatus().ToString());

            this.network.Shutdown();
        }

        /// <summary>
        /// Update the main process functionality.
        /// </summary>
        /// <param name="timeStep">Delta time since last update.</param>
        private void UpdateProcess(TimeSpan interval)
        {
            if (this.network.IsLoggedIn)
            {
                this.network.ProcessNetworkState();

                foreach(var avatar in this.localAvatarList)
                {
                    avatar.Update(interval);
                }
            }
            else
            {
                this.forceExit = true;
            }
        }

        /// <summary>
        /// Handle ping sequence changed event.
        /// </summary>
        /// <param name="sequence">Sequence number.</param>
        private void OnPingSequenceChangedHandler(int sequence)
        {
            foreach (var avatar in this.localAvatarList)
            {
                if (avatar is Pong)
                {
                    (avatar as Pong).UpdateSequence(sequence);
                }
            }
        }
    }
}
