﻿//-----------------------------------------------------------------------
// <copyright file="FriendList.cs" company="Scalify">
//     Copyright (c) 2012 Scalify. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace AutoTest.Tests
{
    using System.Collections.Generic;
    using System.Diagnostics;

    using Badumna.Chat;

    /// <summary>
    /// The FriendsList class represent all friends of the local user.  
    /// </summary>
    internal class FriendsList : IEnumerable<Friend>
    {
        /// <summary>
        /// All friends, indexed by their unique name. 
        /// </summary>
        private Dictionary<string, Friend> friends;

        /// <summary>
        /// Initializes a new instance of the <see cref="FriendsList"/> class.
        /// </summary>
        public FriendsList()
        {
            this.friends = new Dictionary<string, Friend>();
        }

        /// <summary>
        /// Loads the friends list.
        /// </summary>
        /// <param name="friendsNames">List of friends names.</param>
        /// <returns>True if friends list is successfully loaded.</returns>
        public bool LoadFriendsList(IEnumerable<string> friendsNames)
        {
            foreach (string name in friendsNames)
            {
                Friend friend = new Friend(name);
                this.AddFriend(friend);
            }

            return true;
        }

        /// <summary>
        /// Sets the friend chat presence status.
        /// </summary>
        /// <param name="name">The name of the friend.</param>
        /// <param name="status">The presence status.</param>
        public void SetFriendChatStatus(string name, ChatStatus status)
        {
            Friend friend;
            if (this.friends.TryGetValue(name, out friend))
            {
                friend.Status = status;
            }
            else
            {
                Debug.Fail("Unknown friend.");
            }
        }

        /// <summary>
        /// Sets the friend chat channel id.
        /// </summary>
        /// <param name="name">The name of the friend.</param>
        /// <param name="channel">The chat channel.</param>
        public void SetFriendChatChannelId(string name, IChatChannel channel)
        {
            Friend friend;
            if (this.friends.TryGetValue(name, out friend))
            {
                friend.Channel = channel;
            }
        }

        /// <summary>
        /// Gets the friend object according to the specified unique name.
        /// </summary>
        /// <param name="name">The friend name.</param>
        /// <returns>The friend object that matches the specified name.</returns>
        public Friend GetFriend(string name)
        {
            Friend friend;
            if (this.friends.TryGetValue(name, out friend))
            {
                return friend;
            }

            return null;
        }

        /// <summary>
        /// Gets the friend name according to the specified chat channel id.
        /// </summary>
        /// <param name="id">The chat channel id.</param>
        /// <returns>The friend name.</returns>
        public string GetFriendNameByChannelId(ChatChannelId id)
        {
            foreach (Friend friend in this.friends.Values)
            {
                if (friend.Channel != null && friend.Channel.Equals(id))
                {
                    return friend.Name;
                }
            }

            return null;
        }

        #region IEnumerable<Friend> Members

        /// <summary>
        /// Returns an enumerator that iterates through the collection.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.Collections.Generic.IEnumerator`1"/> that can be used to iterate through the collection.
        /// </returns>
        public IEnumerator<Friend> GetEnumerator()
        {
            if (this.friends.Count == 0)
            {
                yield break;
            }

            foreach (KeyValuePair<string, Friend> kvp in this.friends)
            {
                yield return kvp.Value;
            }
        }

        #endregion

        #region IEnumerable Members

        /// <summary>
        /// Returns an enumerator that iterates through a collection.
        /// </summary>
        /// <returns>
        /// An <see cref="T:System.Collections.IEnumerator"/> object that can be used to iterate through the collection.
        /// </returns>
        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        #endregion

        /// <summary>
        /// Adds the friend.
        /// </summary>
        /// <param name="friend">The friend.</param>
        /// <returns>true if successful, or false if the friend is already in the friend list.</returns>
        private bool AddFriend(Friend friend)
        {
            if (!this.friends.ContainsKey(friend.Name))
            {
                this.friends[friend.Name] = friend;
                return true;
            }

            return false;
        }
    }
}
