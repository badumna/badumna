﻿//-----------------------------------------------------------------------
// <copyright file="Avatar.cs" company="Scalify">
//     Copyright (c) 2012 Scalify. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace AutoTest.Tests
{
    using System;
    using System.Drawing;
    using System.IO;

    using AutoTest.Tests.Behaviour;

    using Badumna;
    using Badumna.Autoreplication;
    using Badumna.DataTypes;
    using Badumna.SpatialEntities;

    /// <summary>
    /// Delegate for notify that a new sequence is recevied.
    /// </summary>
    /// <param name="sequence">Sequence number.</param>
    public delegate void SequenceChangedHandler(int sequence, Avatar avatar);

    /// <summary>
    /// Delegate used for handling an incoming ack from replica.
    /// </summary>
    /// <param name="sequence">Sequence number.</param>
    /// <param name="peerId">Sender id.</param>
    public delegate void AckReceivedHandler(int sequence, string peerId);

    /// <summary>
    /// Represents an avatar in the Badumna peer.
    /// </summary>
    public class Avatar : IBehaviour, IReplicableEntity
    {
        /// <summary>
        /// Avatar's position.
        /// </summary>
        private Vector3 position;

        /// <summary>
        /// Orientation of the avatar.
        /// </summary>
        private float orientation;

        /// <summary>
        /// Event record updater.
        /// </summary>
        protected EventRecordUpdater eventRecordUpdater;

        /// <summary>
        /// Sequence changed handler.
        /// </summary>
        public event SequenceChangedHandler sequenceChangedHandler;

        /// <summary>
        /// Handle any incoming ack from replicas.
        /// </summary>
        public event AckReceivedHandler ackReceievedHandler;

        /// <summary>
        /// Initializes a new instance of the Avatar class.
        /// </summary>
        public Avatar()
        {            
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Avatar"/> class.
        /// </summary>
        /// <param name="eventRecordUpdater">Event record updater.</param>
        public Avatar(EventRecordUpdater eventRecordUpdater)        
        {
            this.eventRecordUpdater = eventRecordUpdater;
        }

        /// <summary>
        /// Gets or sets the position of the avatar.
        /// </summary>
        [Smoothing(Interpolation = 200, Extrapolation = 0)]
        public Vector3 Position 
        {
            get
            {
                return this.position;
            }
            set
            {
                this.position = value;

#if VERBOSE_TEST
                if (this.eventRecordUpdater != null)
                {
                    string data = string.Format(
                        "GUID={0} Position=<{1} {2} {3}>",
                        this.Guid,
                        this.position.X.ToString(),
                        this.position.Y.ToString(),
                        this.position.Z.ToString());
                    this.eventRecordUpdater("Deserialise position", data);
                }
#endif
            }
        }       

        /// <summary>
        /// Gets or sets the orientation of the avatar.
        /// </summary>
        [Replicable]
        public float Orientation
        {
            get
            {
                return this.orientation;
            }

            set
            {
                // Limit the value to modulo 360 degrees
                while (value >= 360.0f)
                {
                    value -= 360.0f;
                }

                while (value < 0.0f)
                {
                    value += 360.0f;
                }

                this.orientation = value;
            }
        }

        /// <summary>
        /// Gets or sets the character animation name.
        /// </summary>
        [Replicable]
        public string AnimationName { get; set; }

        /// <summary>
        /// Gets or sets color of the avatar.
        /// </summary>
        ////[Replicable]
        public UnityEngine.Color Color { get; set; }

        /// <summary>
        /// Gets or sets avatar's guid.
        /// </summary>
        [Replicable] // Note: disable this when do testing with Unity.
        public string Guid { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the avatar is an original.
        /// </summary>
        public bool IsOriginal { get; set; }

        /// <summary>
        /// Update method.
        /// </summary>
        /// <param name="timeStep">Time since last update.</param>
        public virtual void Update(TimeSpan interval)
        {
        }

        /// <summary>
        /// Updating the sequence on replicas.
        /// </summary>
        /// <param name="sequence">Sequence number.</param>
        /// <param name="scene">The scene we're on.</param>
        public void SendNewSequenceToReplicas(int sequence, NetworkScene scene)
        {
            scene.CallMethodOnReplicas(this.CalledOnReplicas, sequence);
        }

        public void SendAckToOriginal(int sequence, string id, NetworkScene scene)
        {
            scene.CallMethodOnOriginal(this.CalledOnOriginal, sequence, id);
        }

        /// <summary>
        /// RPC that called on replica side.
        /// </summary>
        /// <param name="sequence">Sequence number.</param>
        [Replicable]
        public void CalledOnReplicas(int sequence)
        {
            var handler = this.sequenceChangedHandler;
            if (handler != null)
            {
                handler(sequence, this);
            }
        }

        /// <summary>
        /// RPC that called on original side.
        /// </summary>
        /// <param name="sequence">Sequence number.</param>
        /// <param name="id">Sender id.</param>
        [Replicable]
        public void CalledOnOriginal(int sequence, string id)
        {
            var handler = this.ackReceievedHandler;
            if (handler != null)
            {
                handler(sequence, id);
            }
        }
    }
}
