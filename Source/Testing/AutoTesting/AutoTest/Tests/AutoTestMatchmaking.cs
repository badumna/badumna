﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AutoTestMatchmaking.cs" company="Scalify">
//     Copyright (c) 2012 Scalify. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace AutoTest.Tests
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Timers;

    using AutoTest.Tests.Behaviour;

    using Badumna.Matchmaking;
    using Badumna.SpatialEntities;

    using Mono.Options;

    /// <summary>
    /// The auto test matchmaking.
    /// </summary>
    public class AutoTestMatchmaking : AutoTestPeer
    {
        /// <summary>
        /// The matchmaking delays.
        /// </summary>
        private readonly List<int> matchmakingDelays = new List<int>();

        /// <summary>
        /// The matchmaking request.
        /// </summary>
        private string matchmakingRequest;

        /// <summary>
        /// The local avatar.
        /// </summary>
        private LocalAvatar localAvatar;

        /// <summary>
        /// The matchmaking request.
        /// </summary>
        private MatchmakingOptions matchmakingOptions;

        /// <summary>
        /// The timer.
        /// </summary>
        private Timer timer;

        /// <summary>
        /// Initializes a new instance of the <see cref="AutoTestMatchmaking"/> class.
        /// </summary>
        public AutoTestMatchmaking()
        {
            this.optionSet.Add(
                Constants.MatchmakingRequest,
                "Matchmaking request with the following format, MinPlayers;MaxPlayers;PlayerGroup;PlayerAttributes.",
                var => { this.matchmakingRequest = var; });
            
            this.optionSet.Add(
                Constants.MatchmakingDelays,
                "Matchmaking delays with the following format, delay in seconds;delay in seconds, etc.",
                var =>
                {
                    foreach (var i in var.Split(';'))
                    {
                        this.matchmakingDelays.Add(int.Parse(i));
                    }
                });

            this.sceneName = null;
        }

        /// <inheritdoc/>
        public override string ToString()
        {
            return "AutoTestMatchmaking";
        }

        /// <inheritdoc/>
        public override void AddAvatar(LocalAvatar avatar)
        {
            this.localAvatar = avatar;
        }

        /// <inheritdoc/>
        protected override void Update(TimeSpan interval)
        {
            if (this.localAvatar != null && this.scene != null)
            {
                (this.localAvatar as IBehaviour).Update(interval);
            }

            base.Update(interval);
        }

        /// <inheritdoc/>
        protected override void OnStartUp()
        {
            base.OnStartUp();

            // scheduling the matchmaking request
            if (this.matchmakingDelays.Count <= 0)
            {
                return;
            }

            this.timer = new Timer();
            this.timer.Elapsed += this.RequestingMatch;
            this.timer.Interval = TimeSpan.FromSeconds(this.matchmakingDelays[0]).TotalMilliseconds;
            this.timer.Start();

            this.matchmakingDelays.RemoveAt(0);
        }

        /// <inheritdoc/>
        protected override void OnShutDown()
        {
            if (this.scene != null)
            {
                this.scene.UnregisterEntity(this.localAvatar);
                this.scene.Leave();
                this.scene = null;
            }

            this.network.Shutdown();

            this.RecordEvent("Badumna Shutdown", string.Empty);
        }

        /// <summary>
        /// Requesting a match
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Elapsed event arguments.</param>
        private void RequestingMatch(object sender, ElapsedEventArgs e)
        {
            this.timer.Stop();

            if (this.matchmakingOptions == null)
            {
                // Parse the matchmaking request string;
                var split = this.matchmakingRequest.Split(';');

                if (split.Length != 4)
                {
                    throw new ArgumentException("Requesting match string is not valid.");
                }

                this.matchmakingOptions = new MatchmakingOptions
                {
                    MinimumPlayers = short.Parse(split[0]),
                    MaximumPlayers = short.Parse(split[1]),
                    PlayerGroup = ushort.Parse(split[2]),
                    PlayerAttributes = Convert.ToUInt32(split[3], 16),
                    Timeout = TimeSpan.FromSeconds(40)
                };
            }

            this.network.BeginMatchmaking(
                this.OnCompletion,
                this.OnProgress,
                this.matchmakingOptions);

            // update the timer.
            if (this.matchmakingDelays.Count > 0)
            {
                this.timer.Interval = TimeSpan.FromSeconds(this.matchmakingDelays[0]).TotalMilliseconds;
                this.timer.Start();
                this.matchmakingDelays.RemoveAt(0);
            }            
        }

        /// <summary>
        /// The request on progress.
        /// </summary>
        /// <param name="update">The progress message. </param>
        private void OnProgress(MatchmakingProgressEvent update)
        {
            this.RecordEvent("Matchmaking", update.ToString());
        }

        
        /// <summary>
        /// On completion.
        /// </summary>
        /// <param name="roomId"> The room id.</param>
        /// <param name="expectedPlayer">The expected player.</param>
        /// <param name="playerId">The player id.</param>
        private void OnCompletion(MatchmakingAsyncResult result)
        {
            if (result.Succeeded)
            {
                var match = result.Match;

                this.RecordEvent("Matchmaking", string.Format("Join Room number {0}, expected players : {1}, player id : {2}", match.RoomId, match.ExpectedPlayers, match.PlayerId));

                this.sceneName = match.RoomId;
                if (this.scene != null)
                {
                    this.scene.UnregisterEntity(this.localAvatar);
                    this.scene.Leave();
                    this.scene = null;
                }

                this.scene = this.network.JoinMiniScene(this.sceneName, this.CreateSpatialReplica, this.RemoveSpatialReplica);
                this.scene.RegisterEntity(this.localAvatar, (uint)EntityType.Avatar, 2.0f, 20.0f);
            }
            else
            {
                string errorMessage = "Unkown error.";
                if (result.Error == MatchmakingError.ConnectionFailed)
                {
                    errorMessage = "Connection failed.";
                }
                else if (result.Error == MatchmakingError.Timeout)
                {
                    errorMessage = "Timed out finding a match.";
                }

                this.RecordEvent("Matchmaking", errorMessage);
            }
        }
    }
}
