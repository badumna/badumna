﻿//------------------------------------------------------------------------------
// <copyright file="HostedCounter.cs" company="National ICT Australia Limited">
//     Copyright (c) 2013 National ICT Australia Limited. All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

namespace AutoTest.Tests.Match
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Badumna;
    using AutoTest.Tests.Counting;

    /// <summary>
    /// A hosted entity with a counter.
    /// </summary>
    public class HostedCounter
    {
        [Replicable]
        private Count count { get; set; }

        [Replicable]
        public string Name { get; set; }

        private Counter counter;

        private CounterSink replicationCounterSink;

        private CounterSink rpcCounterSink;

        public Badumna.Match.Match Match { get; set; }

        /// <summary>
        /// Constructor for original avatar.
        /// </summary>
        public HostedCounter(
            TimeSpan countInterval,
            Statistics onStatistics,
            Func<TimeSpan> getCurrentTime)
        {
            this.counter = new Counter(
                countInterval,
                getCurrentTime,
                (count) =>
                {
                    this.count = count;
                    this.Match.CallMethodOnReplicas(this.NewCount, count);
                });

            this.replicationCounterSink = new CounterSink(
                getCurrentTime,
                (m, i, e) => onStatistics("HostedReplication", this.Name, m, i, e));

            this.rpcCounterSink = new CounterSink(
                getCurrentTime,
                (m, i, e) => onStatistics("HostedRPC", this.Name, m, i, e));
        }

        public void UpdateOnHost()
        {
            this.counter.Update();
        }

        public void UpdateOnClient()
        {
            this.replicationCounterSink.OnCount(this.count);
        }

        /// <summary>
        /// Called when we become responsible for this entity.
        /// </summary>
        public void OnTakeover()
        {
            this.counter.SetLastCount(this.count);
        }

        [Replicable]
        private void NewCount(Count count)
        {
            this.rpcCounterSink.OnCount(count);
        }
    }
}
