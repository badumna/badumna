﻿//------------------------------------------------------------------------------
// <copyright file="AutoTestMatch.cs" company="National ICT Australia Limited">
//     Copyright (c) 2013 National ICT Australia Limited. All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

namespace AutoTest.Tests.Match
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Text;
    using AutoTest.Tests.Behaviour;
    using Badumna;
    using Badumna.Match;
    using Badumna.DataTypes;
    using System.IO;
    using AutoTest.Tests.Counting;

    public delegate void Statistics(string kind, string source, bool missing, TimeSpan interval, TimeSpan estimatedDelay);

    /// <summary>
    /// Tests for the new match system, Badumna.Match.
    /// </summary>
    public class AutoTestMatch : AutoTestPeer
    {
        /// <summary>
        /// Time between match join attempts.
        /// </summary>
        private static readonly TimeSpan JoinAttemptInterval = TimeSpan.FromSeconds(2);

        /// <summary>
        /// Time before we abandon a match that isn't full.
        /// </summary>
        private static readonly TimeSpan NonFullMatchAbandonmentTimeout = TimeSpan.FromSeconds(7);

        /// <summary>
        /// Interval between counts.
        /// </summary>
        /// <remarks>
        /// TODO: Make configurable.
        /// </remarks>
        private static readonly TimeSpan CountInterval = TimeSpan.FromSeconds(1);

        /// <summary>
        /// A source of random numbers.
        /// </summary>
        private Random random = new Random(Process.GetCurrentProcess().Id);

        /// <summary>
        /// The match.
        /// </summary>
        private Match<Controller> match;

        /// <summary>
        /// Time until we retry joining a match.
        /// </summary>
        private TimeSpan timeUntilJoinAttempt;

        /// <summary>
        /// Time until we abandon our match if it doesn't fill.
        /// </summary>
        private TimeSpan timeUntilAbandonment;

        /// <summary>
        /// Indicates if a match query is in progress.
        /// </summary>
        private bool isQueryInProgress;

        /// <summary>
        /// Number of peers in each match.
        /// </summary>
        private int matchSize = 5;

        /// <summary>
        /// The host will leave after hosting the match for this amount of time (for testing host migration).
        /// </summary>
        private TimeSpan hostLeaveAfter;

        /// <summary>
        /// Our avatar.
        /// </summary>
        private CountingAvatar avatar;

        /// <summary>
        /// Replica avatars.
        /// </summary>
        private List<CountingAvatar> replicas = new List<CountingAvatar>();

        /// <summary>
        /// Time source.
        /// </summary>
        private Stopwatch stopwatch;

        /// <summary>
        /// The time we started hosting.
        /// </summary>
        private TimeSpan hostingStartTime;

        /// <summary>
        /// The match controller.
        /// </summary>
        private Controller controller;

        private Counter chatCounter;

        private Dictionary<MemberIdentity, CounterSink> chatCounterSinks = new Dictionary<MemberIdentity, CounterSink>();

        private int numberOfHostedEntities;

        /// <summary>
        /// Initializes a new instance of the Match class.
        /// </summary>
        public AutoTestMatch()
        {
            this.optionSet.Add(
                Constants.MatchSize,
                "The number of peers in each Badumna.Match",
                (int var) => { this.matchSize = var; });

            this.optionSet.Add(
                Constants.HostLeaveAfter,
                "The time in seconds after which the host will leave the match",
                (int var) => { this.hostLeaveAfter = TimeSpan.FromSeconds(var); });

            this.optionSet.Add(
                Constants.NumHostedEntities,
                "The number of hosted entities in each match",
                (int var) => { this.numberOfHostedEntities = var; });
        }

        /// <inheritdoc />
        protected override void OnStartUp()
        {
            base.OnStartUp();

            this.network.TypeRegistry.RegisterValueType(
                (c, w) => c.Serialize(w),
                (r) => new Count(r));

            this.network.RPCManager.RegisterRPCSignature<Count>();
            this.network.RPCManager.RegisterRPCSignature<MemberIdentity, Count>();

            this.stopwatch = new Stopwatch();
            this.stopwatch.Start();
        }

        /// <inheritdoc />
        protected override void Update(TimeSpan interval)
        {
            base.Update(interval);

            if (this.timeUntilJoinAttempt > TimeSpan.Zero)
            {
                this.timeUntilJoinAttempt -= interval;
            }
                            
            if (this.timeUntilAbandonment > TimeSpan.Zero)
            {
                this.timeUntilAbandonment -= interval;

                if (this.timeUntilAbandonment <= TimeSpan.Zero)
                {
                    this.RecordEvent("Match", "Abandoning non-full match");
                    this.LeaveMatch();
                }
            }

            var makeQuery =
                !this.isQueryInProgress &&
                this.match == null &&
                this.timeUntilJoinAttempt <= TimeSpan.Zero;

            if (makeQuery)
            {
                this.RecordEvent("Match", "Finding");
                this.isQueryInProgress = true;
                this.network.Match.FindMatches(
                    new MatchmakingCriteria(),
                    (result) =>
                    {
                        this.isQueryInProgress = false;
                        this.timeUntilJoinAttempt = AutoTestMatch.JoinAttemptInterval;

                        if (result.Error != MatchError.None)
                        {
                            this.RecordEvent("Match", "Query failed: " + result.Error);
                            return;
                        }

                        MatchmakingResult matchId = null;
                        if (result.Results.Count > 0)
                        {
                            var matchIndex = this.random.Next(result.Results.Count);
                            matchId = result.Results[matchIndex];
                        }

                        this.EnterMatch(matchId);
                    });
            }

            if (this.match == null)
            {
                return;
            }

            if (this.hostLeaveAfter > TimeSpan.Zero &&
                this.match.State == MatchStatus.Hosting &&
                this.stopwatch.Elapsed > this.hostingStartTime + this.hostLeaveAfter)
            {
                this.LeaveMatch();
                return;
            }

            this.controller.Update();

            this.avatar.Update();

            foreach (var replica in this.replicas)
            {
                replica.Update();
            }

            this.chatCounter.Update();
        }

        /// <summary>
        /// Enter the specified match (or create a new one if none specified).
        /// </summary>
        /// <param name="matchId">The match idenfier, or null to create a new match</param>
        private void EnterMatch(MatchmakingResult matchId)
        {
            var username = "default";
            Func<TimeSpan> getCurrentTime = () => this.stopwatch.Elapsed;

            this.controller = new Controller(
                this.numberOfHostedEntities,
                AutoTestMatch.CountInterval,
                this.OnStatistics,
                getCurrentTime);

            CreateReplica createReplica =
                (Match match, EntitySource source, uint entityType) =>
                {
                    if (source == EntitySource.Host)
                    {
                        return this.controller.CreateHostedCounter();
                    }
                    else
                    {
                        this.RecordEvent("Match", "Create replica");
                        var replica = new CountingAvatar(this.OnStatistics, () => this.stopwatch.Elapsed);
                        replica.Match = this.match;
                        this.replicas.Add(replica);
                        return replica;
                    }
                };

            RemoveReplica removeReplica =
                (Match match, EntitySource source, object replica) =>
                {
                    if (source == EntitySource.Host)
                    {
                        this.controller.RemoveHostedCounter((HostedCounter)replica);
                    }
                    else
                    {
                        this.RecordEvent("Match", "Remove replica");
                        this.replicas.Remove((CountingAvatar)replica);
                    }
                };

            if (matchId == null)
            {
                this.RecordEvent("Match", "Creating");
                this.match = this.network.Match.CreateMatch(this.controller, new MatchmakingCriteria(), this.matchSize, username, createReplica, removeReplica);
            }
            else
            {
                this.RecordEvent("Match", "Joining");
                this.match = this.network.Match.JoinMatch(this.controller, matchId, username, createReplica, removeReplica);
            }

            this.timeUntilAbandonment = AutoTestMatch.NonFullMatchAbandonmentTimeout;  // TODO: Add some random variation, so they don't all sync up.

            this.match.StateChanged +=
                (m, args) =>
                {
                    this.RecordEvent("Match", string.Format(
                        "StateChanged: match = {0}, status = {1}, error = {2}",
                        m.MatchIdentifier,
                        args.Status,
                        args.Error));

                    if (args.Status == MatchStatus.Hosting)
                    {
                        this.hostingStartTime = this.stopwatch.Elapsed;
                    }
                    else
                    {
                        this.hostingStartTime = TimeSpan.Zero;
                    }

                    if (args.Error != MatchError.None)
                    {
                        this.LeaveMatch();
                    }
                };

            this.match.MemberAdded +=
                (m, args) =>
                {
                    if (m.MemberCount == this.matchSize)
                    {
                        this.timeUntilAbandonment = TimeSpan.Zero;
                    }
                };

            this.match.MemberRemoved +=
                (m, args) =>
                {
                    this.timeUntilAbandonment = AutoTestMatch.NonFullMatchAbandonmentTimeout;
                };

            this.controller.Match = this.match;

            this.avatar = new CountingAvatar(AutoTestMatch.CountInterval, getCurrentTime);
            this.avatar.Name = this.match.MemberIdentity.ID.ToString();
            this.avatar.Match = this.match;
            this.match.RegisterEntity(this.avatar, 0);

            this.chatCounter = new Counter(
                AutoTestMatch.CountInterval,
                getCurrentTime,
                (count) => this.match.Chat(count.ToString()));

            this.match.ChatMessageReceived +=
                (match, args) =>
                {
                    CounterSink sink;
                    if (!this.chatCounterSinks.TryGetValue(args.Sender, out sink))
                    {
                        sink = new CounterSink(
                            () => this.stopwatch.Elapsed,
                            (m, i, e) => this.OnStatistics("Chat", args.Sender.ID.ToString(), m, i, e));
                        this.chatCounterSinks[args.Sender] = sink;
                    }

                    sink.OnCount(new Count(args.Message));
                };
        }

        /// <summary>
        /// Leave the match
        /// </summary>
        private void LeaveMatch()
        {
            if (this.match == null)
            {
                return;
            }

            this.RecordEvent("Match", "Leaving");
            this.match.UnregisterEntity(this.avatar);
            this.match.Leave();
            this.match = null;
            this.controller = null;
            this.avatar = null;
            this.replicas.Clear();
            this.chatCounter = null;
            this.chatCounterSinks.Clear();
        }

        /// <inheritdoc />
        protected override void OnShutDown()
        {
            this.LeaveMatch();
            base.OnShutDown();
        }

        private void OnStatistics(string kind, string source, bool missing, TimeSpan interval, TimeSpan estimatedDelay)
        {
            if (!missing)
            {
                this.RecordEvent(kind + ",Interval", source + "," + interval.TotalMilliseconds.ToString());
            }
            else
            {
                this.RecordEvent(kind + ",Missing", source);
            }

            this.RecordEvent(kind + ",EstDelay", source + "," + estimatedDelay.TotalMilliseconds.ToString());
        }
    }
}
