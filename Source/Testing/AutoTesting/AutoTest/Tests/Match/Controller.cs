﻿//------------------------------------------------------------------------------
// <copyright file="Controller.cs" company="National ICT Australia Limited">
//     Copyright (c) 2013 National ICT Australia Limited. All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

namespace AutoTest.Tests.Match
{
    using System;
    using System.Collections.Generic;

    using AutoTest.Tests.Counting;

    using Badumna;
    using Badumna.Match;

    /// <summary>
    /// The match controller.
    /// </summary>
    public class Controller
    {
        private Counter counter;

        private Dictionary<MemberIdentity, CounterSink> rpcCounterSinks = new Dictionary<MemberIdentity, CounterSink>();

        private CounterSink replicationCounterSink;

        private Func<TimeSpan> getCurrentTime;

        private Statistics onStatistics;
        
        private TimeSpan countInterval;
        
        private List<HostedCounter> hostedCounters = new List<HostedCounter>();

        private bool wasHosting;

        public Match<Controller> Match { get; set; }

        [Replicable]
        private Count count { get; set; }

        private readonly int numberOfHostedEntities;

        public Controller(
            int numberOfHostedEntities,
            TimeSpan countInterval,
            Statistics onStatistics,
            Func<TimeSpan> getCurrentTime)
        {
            this.numberOfHostedEntities = numberOfHostedEntities;
            this.onStatistics = onStatistics;
            this.getCurrentTime = getCurrentTime;
            this.countInterval = countInterval;

            this.counter = new Counter(
                countInterval,
                getCurrentTime,
                (count) =>
                {
                    if (this.Match.State == MatchStatus.Hosting)
                    {
                        this.count = count;
                    }

                    this.Match.CallMethodOnMembers(this.NewCount, this.Match.MemberIdentity, count);
                });

            this.replicationCounterSink = new CounterSink(
                getCurrentTime,
                (m, i, e) => onStatistics("ControllerReplication", "MatchHost", m, i, e));
        }

        public HostedCounter CreateHostedCounter()
        {
            var hostedCounter = new HostedCounter(
                this.countInterval,
                this.onStatistics,
                this.getCurrentTime);
            hostedCounter.Match = this.Match;
            hostedCounter.Name = this.Match.MemberIdentity.ID.ToString();
            this.hostedCounters.Add(hostedCounter);
            return hostedCounter;
        }

        public void RemoveHostedCounter(HostedCounter hostedCounter)
        {
            this.hostedCounters.Remove(hostedCounter);
        }

        public void Update()
        {
            if (this.Match.State == MatchStatus.Hosting)
            {
                if (!this.wasHosting)
                {
                    this.counter.SetLastCount(this.count);

                    foreach (var hostedCounter in this.hostedCounters)
                    {
                        hostedCounter.OnTakeover();
                    }

                    while (this.hostedCounters.Count < this.numberOfHostedEntities)
                    {
                        this.Match.RegisterHostedEntity(this.CreateHostedCounter(), 0);
                    }

                    this.wasHosting = true;
                }

                foreach (var hostedCounter in this.hostedCounters)
                {
                    hostedCounter.UpdateOnHost();
                }
            }
            else
            {
                this.wasHosting = false;
                this.replicationCounterSink.OnCount(this.count);

                foreach (var hostedCounter in this.hostedCounters)
                {
                    hostedCounter.UpdateOnClient();
                }
            }

            this.counter.Update();
        }
        
        [Replicable]
        private void NewCount(MemberIdentity source, Count count)
        {
            CounterSink sink;
            if (!this.rpcCounterSinks.TryGetValue(source, out sink))
            {
                sink = new CounterSink(
                    this.getCurrentTime,
                    (m, i, e) => this.onStatistics("ControllerRPC", source.ID.ToString(), m, i, e));
                this.rpcCounterSinks[source] = sink;
            }

            sink.OnCount(count);
        }
    }
}
