﻿//-----------------------------------------------------------------------
// <copyright file="Program.cs" company="Scalify">
//     Copyright (c) 2012 Scalify. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace AutoTest
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;

    using AutoTest.Tests;
    using AutoTest.Tests.Match;

    using Mono.Options;

    /// <summary>
    /// The AutoTest is used to test the core functionality of Badumna.
    /// </summary>
    internal class Program
    {
        /// <summary>
        /// Option set for parsing command line options.
        /// </summary>
        private readonly OptionSet optionSet;

        /// <summary>
        /// Factory methods for the different test types.
        /// </summary>
        private readonly Dictionary<string, Func<AutoTestBase>> testFactory = new Dictionary<string, Func<AutoTestBase>>
        {
            { "AutoTestPeer", () => new AutoTestPeer() },
            { "AutoTestPrivateChat", () => new AutoTestPrivateChat() },
            { "AutoTestMatchmaking", () => new AutoTestMatchmaking() },
            { "AutoTestPingPong", () => new AutoTestPingPong() },
            { "AutoTestRPC", () => new AutoTestRPC() },
            { "AutoTestMatch", () => new AutoTestMatch() },
        };

        /// <summary>
        /// Auto test instance depend on the test type.
        /// </summary>
        private AutoTestBase autoTest;

        /// <summary>
        /// Auto test type, by default is AutoTestPeer.
        /// </summary>
        private string testType = "AutoTestPeer";

        /// <summary>
        /// Indicates whether we should display the usage message.
        /// </summary>
        private bool showHelp;

        /// <summary>
        /// AutoTestPeer main function, which takes arguments from the command line.
        /// </summary>
        /// <param name="args">Command line arguments.</param>
        public static void Main(string[] args)
        {
            new Program().Run(args);
        }

        /// <summary>
        /// Initializes the Program class.
        /// </summary>
        public Program()
        {
            this.optionSet = new OptionSet
            {
                {
                    Constants.TestTypeString, 
                    "Auto test type that should be run (see below for list of test types).",
                    var => { this.testType = var; }
                }, 
                {
                    "h|help",
                    "Display this message and exit.", 
                    var => { this.showHelp = true; }
                }
            };
        }

        /// <summary>
        /// AutoTest main function.
        /// </summary>
        /// <param name="args">Command line arguments.</param>
        private void Run(string[] args)
        {
            try
            {
                var peerArgs = this.optionSet.Parse(args);

                Func<AutoTestBase> constructor;
                if (!this.testFactory.TryGetValue(this.testType, out constructor))
                {
                    Console.WriteLine("Unknown test type: {0}", this.testType);
                    Environment.Exit(1);
                }

                this.autoTest = constructor();

                if (this.showHelp)
                {
                    this.WriteUsageAndExit();
                }

                var extraArgs = autoTest.Initialize(peerArgs);

                if (extraArgs.Count > 0)
                {
                    HandleUnknownArguments(peerArgs);
                }
                else
                {
                    Console.CancelKeyPress += delegate(object sender, ConsoleCancelEventArgs e)
                    {
                        e.Cancel = true;
                        autoTest.Stop();
                    };

                    Console.WriteLine("AutoTest has been started.");
                    autoTest.Start();
                    Console.WriteLine("AutoTest has been stopped.");
                }
            }
            catch (OptionException ex)
            {
                Console.WriteLine("AutoTestPeer: {0}", ex.Message);
                Console.WriteLine("Try 'AutoTest --help --test-type=' for more information.");
            }
        }

        /// <summary>
        /// Write error message listing unknown arguments.
        /// </summary>
        /// <param name="args">Unknown arguments.</param>
        private void HandleUnknownArguments(IEnumerable<string> args)
        {
            Console.Write("AutoTestPeer: Unknown argument(s):");

            foreach (var arg in args)
            {
                Console.Write(" " + arg);
            }

            Console.WriteLine(".");
            Console.WriteLine("Try 'AutoTest --help --test-type=' for more information.");
        }

        /// <summary>
        /// Write usage instructions and exit.
        /// </summary>
        private void WriteUsageAndExit()
        {
            Console.WriteLine("Usage: AutoTest [Options]");
            Console.WriteLine("Start a AutoTest on the local machine.");
            Console.WriteLine("\nOptions:");

            this.optionSet.WriteOptionDescriptions(Console.Out);

            if (autoTest != null)
            {
                Console.WriteLine("\n" + autoTest);
                autoTest.WriteOptionDescriptions(Console.Out);
            }

            Console.WriteLine("\nUse the following command line to get more help for each different test:");
            Console.WriteLine("AutoTest.exe --test-type=type -h");

            Console.WriteLine("\nAvailable test types: {0}", string.Join(", ", new List<string>(this.testFactory.Keys).ToArray()));

            Environment.Exit(0);
        }
    }
}
