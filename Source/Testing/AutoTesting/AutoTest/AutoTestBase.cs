﻿//-----------------------------------------------------------------------
// <copyright file="AutoTestBase.cs" company="Scalify">
//     Copyright (c) 2012 Scalify. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace AutoTest
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Text;
    using System.Threading;

    using Badumna;
    using Badumna.SpatialEntities;

    using Mono.Options;

    using Utilities;

    /// <summary>
    /// Base class for autotests, mainly respsonible for logging.
    /// </summary>
    public abstract class AutoTestBase : ProcessBase
    {
        /// <summary>
        /// The file streams collection.
        /// </summary>
        private readonly Dictionary<string, FileStream> fileStreams = new Dictionary<string, FileStream>();

        /// <summary>
        /// Number of times to attempt Badumna login.
        /// </summary>
        protected const int BadumnaLoginAttempts = 3;

        /// <summary>
        /// Badumna specific: The primary interface to Badumna.
        /// </summary>
        protected INetworkFacade network;

        /// <summary>
        /// The name of the application.
        /// </summary>
        protected string appName = string.Empty;

        /// <summary>
        /// IP address and port number of the seed peer.
        /// </summary>
        protected string seedPeer;

        /// <summary>
        /// Badumna specific: Network scene.
        /// </summary>
        protected NetworkScene scene;

        /// <summary>
        /// Name of network scene.
        /// </summary>
        protected string sceneName = string.Empty;

        /// <summary>
        /// Performance monitor.
        /// </summary>
        protected PerformanceMonitor performanceMonitor;

        /// <summary>
        /// The life time that the peer will run for.
        /// </summary>
        protected TimeSpan lifeTime = TimeSpan.FromSeconds(-1);

        /// <summary>
        /// The time between recording intervals.
        /// </summary>
        protected TimeSpan recordInterval = TimeSpan.FromSeconds(-1);

        /// <summary>
        /// Name of output directory to store records.
        /// </summary>
        protected string outputDirectory = "Records";

        /// <summary>
        /// String containing file name of statistics record.
        /// </summary>
        protected string statsFilename;

        /// <summary>
        /// String containing file name of error record.
        /// </summary>
        protected string errorFilename;

        /// <summary>
        /// String containing file name of log record.
        /// </summary>
        protected string logFilename;

        /// <summary>
        /// String containing file name of replica record.
        /// </summary>
        protected string replicaFilename;

        /// <summary>
        /// String containing file name of event record.
        /// </summary>
        protected string eventFilename;

        /// <summary>
        /// Flag indicating whether event logging was set using command line option.
        /// </summary>
        private bool logEventsIsSet;

        /// <summary>
        /// Flag whether to force the application to exit.
        /// </summary>
        protected bool forceExit;

        /// <summary>
        /// Flag indicating whether the peer is running in mono.
        /// </summary>
        protected bool isMono;

        /// <summary>
        /// Life time timer.
        /// </summary>
        protected Timer lifeTimeTimer;

        /// <summary>
        /// Record interval timer.
        /// </summary>
        protected Timer recordTimer;

        /// <summary>
        /// The cloud identifier string.
        /// </summary>
        protected string cloudIdentifier;

        /// <summary>
        /// Initializes a new instance of the <see cref="AutoTestBase"/> class.
        /// </summary>
        public AutoTestBase()
        {
            var testOptions = new OptionSet()
            {
                {
                    Constants.AppNameString,
                    "Application name to give the Badumna network.",
                    var => { this.appName = var; }
                },
                {
                    Constants.SceneNameString,
                    "Name of network scene to add avatars to.",
                    var => { this.sceneName = var; }
                },
                {
                    Constants.LifeTimeString,
                    "How long the peer will run for, in seconds.",
                    (float var) => { this.lifeTime = TimeSpan.FromSeconds(var); }
                },                
                {
                    Constants.RecordIntervalString,
                    "Time between recording intervals, in seconds.",
                    (float var) => { this.recordInterval = TimeSpan.FromSeconds(var); }
                },                
                {
                    Constants.SeedPeerString,
                    "IP address and port number of seed peer.",
                    var => { this.seedPeer = var; }
                },                
                {
                    Constants.LogEventsString,
                    "Enable event logging.",
                    var => { this.logEventsIsSet = true; }
                },
                {
                    Constants.OutputDirectoryString,
                    "Name of output directory.",
                    var => { this.outputDirectory = var; }
                },
                {
                    Constants.CloudIdentifier,
                    "Cloud identifier string used in Badumna Cloud.",
                    var => { this.cloudIdentifier = var; }
                }
            };

            foreach (var option in testOptions)
            {
                this.optionSet.Add(option);
            }
        }

        /// <inheritdoc/>
        protected override void OnStartUp()
        {
            this.isMono = Type.GetType("Mono.Runtime") != null;

            if (this.lifeTime > TimeSpan.Zero)
            {
                this.lifeTimeTimer = new Timer((state) => { this.Stop(); }, null, this.lifeTime, TimeSpan.FromMilliseconds(-1));
            }
            
            this.StartUpRecord();

            if (this.recordInterval > TimeSpan.Zero)
            {
                this.recordTimer = new Timer((state) => { this.UpdateRecord(); }, null, TimeSpan.Zero, this.recordInterval);
            }
        }

        /// <inheritdoc/>
        protected override void OnShutDown()
        {
            foreach (var keyPair in this.fileStreams)
            {
                keyPair.Value.Close();
            }

            this.fileStreams.Clear();
        }

        /// <summary>
        /// Update the recording functionality.
        /// </summary>
        protected abstract void UpdateRecord();

        /// <summary>
        /// Record statistics to file.
        /// </summary>
        /// <param name="activeConnections">Number of active connections.</param>
        /// <param name="totalBytesSentPerSecond">Total bytes sent per second.</param>
        /// <param name="totalBytesReceivedPerSecond">Total bytes received per second.</param>
        /// <param name="remoteEntities">Number of remote entities.</param>
        /// <param name="initializingConnections">Number of initializing connections.</param>
        /// <param name="receivedUpdates">Number of received updates.</param>
        /// <param name="currentCPUUsage">Current CPU usage.</param>
        /// <param name="availableRAM">Available RAM.</param>
        protected void RecordStats(int activeConnections, int totalBytesSentPerSecond, int totalBytesReceivedPerSecond, int remoteEntities, int initializingConnections, int receivedUpdates, string currentCPUUsage, string availableRAM)
        {
            string statsRecord = string.Format(
                "{0},{1},{2},{3},{4},{5},{6},{7},{8},{9}\r\n",
                DateTime.Now.ToString(Constants.DateTimeFormat),
                Process.GetCurrentProcess().Id,
                activeConnections,
                totalBytesSentPerSecond,
                totalBytesReceivedPerSecond,
                remoteEntities,
                initializingConnections,
                receivedUpdates,
                currentCPUUsage,
                availableRAM);
            this.RecordText(this.statsFilename, statsRecord);
        }

        /// <summary>
        /// Record events to file.
        /// </summary>
        /// <param name="eventType">Type of event.</param>
        /// <param name="data">Data to record.</param>
        protected void RecordEvent(string eventType, string data)
        {
            if (!this.logEventsIsSet)
            {
                return;
            }

            data = data.Replace("\r", "").Replace('\n', ';');  // Make sure it all fits on one line

            string eventRecord = string.Format(
                "{0},{1},{2},{3}\r\n",
                DateTime.Now.ToString(Constants.DateTimeFormat),
                Process.GetCurrentProcess().Id,
                eventType,
                data);
            this.RecordText(this.eventFilename, eventRecord);
        }

        /// <summary>
        /// Record replicas to file.
        /// </summary>
        /// <param name="remoteEntities">Number of remote entities.</param>
        /// <param name="data">Data to record.</param>
        protected void RecordReplica(int remoteEntities, string data)
        {
            string replicaRecord = string.Format(
                "{0},{1},{2},{3}\r\n",
                DateTime.Now.ToString(Constants.DateTimeFormat),
                Process.GetCurrentProcess().Id,
                remoteEntities,
                data);
            this.RecordText(this.replicaFilename, replicaRecord);
        }

        /// <summary>
        /// Record log to file.
        /// </summary>
        /// <param name="data">Data to record.</param>
        protected void RecordLog(string data)
        {
            string logRecord = string.Format(
                "{0} {1}\r\n",
                DateTime.Now.ToString(Constants.DateTimeFormat),
                data);
            this.RecordText(this.logFilename, logRecord);
        }

        /// <summary>
        /// Record text to a file.
        /// </summary>
        /// <param name="filename">Name of file to record to.</param>
        /// <param name="value">Text string.</param>
        protected void RecordText(string filename, string value, bool flush = false)
        {
            if (string.IsNullOrEmpty(filename))
            {
                return;
            }

            byte[] info = new UTF8Encoding(true).GetBytes(value);

            FileStream fileStream;
            if (!this.fileStreams.TryGetValue(filename, out fileStream))
            {
                fileStream = new FileStream(filename, FileMode.Append);
                this.fileStreams.Add(filename, fileStream);
            }

            lock (fileStream)
            {
                fileStream.Write(info, 0, info.Length);

                if (flush)
                {
                    fileStream.Flush();
                }
            }
        }

        /// <summary>
        /// Prints out unhandled exceptions.
        /// </summary>
        /// <param name="sender">Reference to sender.</param>
        /// <param name="args">Unhandled exception event arguments.</param>
        protected void LastChanceHandler(object sender, UnhandledExceptionEventArgs args)
        {
            this.RecordText(this.errorFilename, "****** LastChanceHandler ******\n", flush: true);

            var ex = (Exception)args.ExceptionObject;
            while (ex != null)
            {
                this.RecordText(this.errorFilename, string.Format("ExceptionType: {0}\n", ex.GetType().Name), flush: true);
                this.RecordText(this.errorFilename, string.Format("HelpLine: {0}\n", ex.HelpLink), flush: true);
                this.RecordText(this.errorFilename, string.Format("Message: {0}\n", ex.Message), flush: true);
                this.RecordText(this.errorFilename, string.Format("Source: {0}\n", ex.Source), flush: true);

                this.RecordText(this.errorFilename, "StackTrace:\n", flush: true);
                foreach (var frame in new StackTrace(ex, true).GetFrames())
                {
                    this.RecordText(
                        this.errorFilename,
                        string.Format(
                            "   {0} ({1}:{2} 0x{3:X})\n",
                            frame.GetMethod(),
                            frame.GetFileName(),
                            frame.GetFileLineNumber(),
                            frame.GetILOffset()));
                }

                this.RecordText(this.errorFilename, string.Format("TargetSite: {0}\n", ex.TargetSite), flush: true);
                
                ex = ex.InnerException;
                if (ex != null)
                {
                    this.RecordText(this.errorFilename, "****** Inner Exception ******\n", flush: true);
                }
            }
        }

        /// <summary>
        /// Initialize recording functionality.
        /// </summary>
        private void StartUpRecord()
        {
            var processId = Process.GetCurrentProcess().Id;

            this.statsFilename = Path.Combine(this.outputDirectory, string.Format("stats{0}.csv", processId));
            this.errorFilename = Path.Combine(this.outputDirectory, string.Format("error{0}.txt", processId));
            this.logFilename = Path.Combine(this.outputDirectory, string.Format("log{0}.txt", processId));
            this.replicaFilename = Path.Combine(this.outputDirectory, string.Format("replica{0}.csv", processId));
            this.eventFilename = Path.Combine(this.outputDirectory, string.Format("event{0}.csv", processId));

            if (!Directory.Exists(this.outputDirectory))
            {
                Directory.CreateDirectory(this.outputDirectory);
            }

            AppDomain.CurrentDomain.UnhandledException += this.LastChanceHandler;

            if (this.recordInterval > TimeSpan.Zero)
            {
                this.performanceMonitor = new PerformanceMonitor(Environment.OSVersion);

                var headings = string.Format(
                    "{0},{1},{2},{3},{4},{5},{6},{7},{8},{9}\r\n",
                    Constants.DateTimeString,
                    Constants.ProcessIdString,
                    Constants.ActiveConnectionsString,
                    Constants.TotalBytesSentString,
                    Constants.TotalBytesReceivedString,
                    Constants.RemoteEntityCountString,
                    Constants.InitializingConnectionsString,
                    Constants.ReceivedUpdateCountString,
                    Constants.CurrentCpuUsageString,
                    Constants.AvailableRamString);
                this.RecordText(this.statsFilename, headings);

                headings = string.Format(
                    "{0},{1},{2}\r\n",
                    Constants.DateTimeString,
                    Constants.ProcessIdString,
                    Constants.RemoteEntityCountString);
                this.RecordText(this.replicaFilename, headings);
            }

            if (this.logEventsIsSet)
            {
                var headings = string.Format(
                    "{0},{1},{2},{3}\r\n",
                    Constants.DateTimeString,
                    Constants.ProcessIdString,
                    Constants.EventTypeString,
                    Constants.DataString);
                this.RecordText(this.eventFilename, headings);
            }
        }
    }
}
