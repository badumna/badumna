﻿//------------------------------------------------------------------------
// <copyright file="OfflineEvent.cs" company="NICTA Pty Ltd">
//      Copyright (c) 2010 NICTA Pty Ltd. All rights reserved.
// </copyright>
//------------------------------------------------------------------------

namespace AutoTest
{
    using System.IO;

    using Badumna.Arbitration;

    /// <summary>
    /// Offline event.
    /// </summary>
    public class OfflineEvent : ArbitrationEvent
    {
        /// <summary>
        /// Initializes a new instance of the OfflineEvent class.
        /// </summary>
        public OfflineEvent()
        {
        }

        /// <summary>
        /// Initializes a new instance of the OfflineEvent class.
        /// </summary>
        /// <param name="reader">Binary reader to read in parameters from.</param>
        public OfflineEvent(BinaryReader reader)
        {
        }

        /// <inheritdoc/>
        public override void Serialize(BinaryWriter writer)
        {            
        }
    }
}