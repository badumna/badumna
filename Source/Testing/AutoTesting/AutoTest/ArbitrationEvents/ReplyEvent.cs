﻿//------------------------------------------------------------------------
// <copyright file="ReplyEvent.cs" company="NICTA Pty Ltd">
//      Copyright (c) 2010 NICTA Pty Ltd. All rights reserved.
// </copyright>
//------------------------------------------------------------------------

namespace AutoTest
{
    using System.IO;

    using Badumna.Arbitration;

    /// <summary>
    /// ReplyEvent class is used as ack
    /// </summary>
    public class ReplyEvent : ArbitrationEvent
    {
        /// <summary>
        /// Initializes a new instance of the ReplyEvent class.
        /// </summary>
        public ReplyEvent()
        {
        }

        /// <summary>
        /// Initializes a new instance of the ReplyEvent class.
        /// </summary>
        /// <param name="reader">Binary reader to read in parameters from.</param>
        public ReplyEvent(BinaryReader reader)
        {
        }

        /// <inheritdoc/>
        public override void Serialize(BinaryWriter writer)
        {            
        }
    }
}