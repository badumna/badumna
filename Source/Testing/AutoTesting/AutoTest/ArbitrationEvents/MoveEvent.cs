﻿//------------------------------------------------------------------------
// <copyright file="MoveEvent.cs" company="NICTA Pty Ltd">
//      Copyright (c) 2010 NICTA Pty Ltd. All rights reserved.
// </copyright>
//------------------------------------------------------------------------

namespace AutoTest
{
    using System.IO;

    using Badumna.Arbitration;
    using Badumna.DataTypes;

    /// <summary>
    /// Move event is sent from the client to arbitration server and store the position
    /// of that particular player relative to the game scene
    /// </summary>
    public class MoveEvent : ArbitrationEvent
    {
        /// <summary>
        /// Initializes a new instance of the MoveEvent class.
        /// </summary>
        /// <param name="destination">Psotion of the move event.</param>
        public MoveEvent(Vector3 destination)
        {
            this.Destination = destination;
        }

        /// <summary>
        /// Initializes a new instance of the MoveEvent class.
        /// </summary>
        /// <param name="reader">Binary reader to read in parameters.</param>
        public MoveEvent(BinaryReader reader)
        {
            Vector3 destination = new Vector3();
            destination.X = reader.ReadSingle();
            destination.Y = reader.ReadSingle();
            destination.Z = reader.ReadSingle();
            this.Destination = destination;
        }

        /// <summary>
        /// Gets the size of Vector3 data types to represent the position.
        /// </summary>
        public override int SerializedLength
        {
            get { return 3 * sizeof(float); }
        }

        /// <summary>
        /// Gets or sets the destination.
        /// </summary>
        public Vector3 Destination { get; set; }

        /// <summary>
        /// Serialize position to binary stream.
        /// </summary>
        /// <param name="writer">Used to write to binary stream.</param>
        public override void Serialize(BinaryWriter writer)
        {
            writer.Write(this.Destination.X);
            writer.Write(this.Destination.Y);
            writer.Write(this.Destination.Z);
        }
    }
}