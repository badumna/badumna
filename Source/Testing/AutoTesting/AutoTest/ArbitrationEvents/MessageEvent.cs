﻿//---------------------------------------------------------------------------------
// <copyright file="MessageEvent.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2010 NICTA Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

namespace AutoTest
{
    using System.IO;

    using Badumna.Arbitration;

    /// <summary>
    /// The message event.
    /// </summary>
    public class MessageEvent : ArbitrationEvent
    {
        /// <summary>
        /// The content of the message.
        /// </summary>
        private int randomNumber;

        /// <summary>
        /// Initializes a new instance of the <see cref="MessageEvent"/> class.
        /// </summary>
        /// <param name="randomNumber">The random number.</param>
        public MessageEvent(int randomNumber)
        {
            this.randomNumber = randomNumber;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MessageEvent"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public MessageEvent(BinaryReader reader)
        {
            this.randomNumber = reader.ReadInt32();
        }

        /// <summary>
        /// Gets the length of the serialized data.
        /// </summary>
        /// <value>The length of the serialized data.</value>
        public override int SerializedLength
        {
            get { return 1 * sizeof(int); }
        }

        /// <summary>
        /// Gets the random number.
        /// </summary>
        /// <value>The random number.</value>
        public int RandomNumber
        {
            get { return this.randomNumber; }
        }

        /// <summary>
        /// Serializes the status of the object into specified writer.
        /// </summary>
        /// <param name="writer">The binary writer.</param>
        public override void Serialize(BinaryWriter writer)
        {
            writer.Write(this.randomNumber);
        }
    }
}