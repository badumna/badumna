﻿// -----------------------------------------------------------------------
// <copyright file="EventSet.cs" company="Scalify">
//      Copyright (c) 2013 Scalify. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace AutoTest.ArbitrationEvents
{
    using Badumna.Arbitration;

    /// <summary>
    /// Arbitration event sets.
    /// </summary>
    public sealed class EventSet
    {
        /// <summary>
        /// The arbitrationEventSet to use to serialize and deserialize arbitration events.
        /// </summary>
        private static ArbitrationEventSet eventSet = CreateEventSet();

        /// <summary>
        /// Initializes a new instance of the <see cref="EventSet"/> class.
        /// </summary>
        /// <remarks>Prevents a default instance of the EventSet class from being created.</remarks>
        private EventSet()
        {
        }

        /// <summary>
        /// Deserialize an arbitration event.
        /// </summary>
        /// <param name="message">A serialized arbitration event.</param>
        /// <returns>The deserialized arbitration event.</returns>
        public static ArbitrationEvent Deserialize(byte[] message)
        {
            return eventSet.Deserialize(message);
        }

        /// <summary>
        /// Serialize an arbitrationEvent.
        /// </summary>
        /// <param name="arbitrationEvent">The arbitration event.</param>
        /// <returns>The serialized arbitration event.</returns>
        public static byte[] Serialize(ArbitrationEvent arbitrationEvent)
        {
            return eventSet.Serialize(arbitrationEvent);
        }

        /// <summary>
        /// Create an arbitration event set containing all arbitration events.
        /// </summary>
        /// <returns>A new arbitration event set.</returns>
        private static ArbitrationEventSet CreateEventSet()
        {
            eventSet = new ArbitrationEventSet();
            eventSet.Register(
                typeof(MoveEvent),
                typeof(MessageEvent),
                typeof(ReplyEvent),
                typeof(OfflineEvent));
            
            return eventSet;
        }
    }
}
