﻿//-----------------------------------------------------------------------
// <copyright file="ProcessBase.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2010 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace AutoTest
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Threading;

    using Mono.Options;

    /// <summary>
    /// Delegate for event recording function.
    /// </summary>
    /// <param name="eventType">Event type.</param>
    /// <param name="data">Event data.</param>
    public delegate void EventRecordUpdater(string eventType, string data);

    /// <summary>
    /// Base class for auto testing processes.
    /// </summary>
    public abstract class ProcessBase
    {
        /// <summary>
        /// The interval between updates.
        /// </summary>
        private readonly TimeSpan updateInterval = TimeSpan.FromMilliseconds(1000/60.0f);

        /// <summary>
        /// Option set for parsing command line options.
        /// </summary>
        protected readonly OptionSet optionSet = new OptionSet();

        /// <summary>
        /// A value indicating whether the peer is running.
        /// </summary>
        private bool isRunning;

        /// <summary>
        /// Initializes a new instance of the ProcessBase class.
        /// </summary>
        public ProcessBase()
        {
        }

        /// <summary>
        /// Initializes a new instance of the ProcessBase class with custom update rate.
        /// </summary>
        /// <param name="updateInterval">The interval between updates.</param>
        public ProcessBase(TimeSpan updateInterval)
            : this()
        {
            this.updateInterval = updateInterval;
        }

        /// <summary>
        /// Initialize the process with command line arguments.
        /// </summary>
        /// <param name="args">Initialization arguments.</param>
        /// <returns>Any arguments that were not parsed.</returns>
        public List<string> Initialize(IEnumerable<string> args)
        {
            return this.optionSet.Parse(args);
        }

        /// <summary>
        /// Write a list of supported command line options.
        /// </summary>
        /// <param name="tw">The text writer to use.</param>
        public void WriteOptionDescriptions(TextWriter tw)
        {
            this.optionSet.WriteOptionDescriptions(tw);
        }

        /// <summary>
        /// Start the process.
        /// </summary>
        public void Start()
        {
            this.OnStartUp();

            this.isRunning = true;
            do
            {
                this.Update(this.updateInterval);
                Thread.Sleep(this.updateInterval);
            }
            while (this.isRunning);

            this.OnShutDown();
        }

        /// <summary>
        /// Stop the process.
        /// </summary>
        public void Stop()
        {
            this.isRunning = false;
        }

        /// <summary>
        /// Called regularly to perform updates.
        /// </summary>
        /// <param name="timeStep">Time since last update.</param>
        protected abstract void Update(TimeSpan interval);

        /// <summary>
        /// Called when the process is starting up.
        /// </summary>
        protected abstract void OnStartUp();

        /// <summary>
        /// Called when the process is shutting down.
        /// </summary>
        protected abstract void OnShutDown();
    }
}