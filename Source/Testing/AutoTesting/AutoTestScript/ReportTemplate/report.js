var urls = [
    ["Active Connections","active-connection.json"],
    ["Bytes Sent", "bytes-sent.json"],
    ["Bytes Received", "bytes-received.json"],
    ["CPU Usage", "cpu-usage.json"],
    ["Remote Entities", "remote-entity.json"],
    ["Received Updates", "received-updates.json"]
];

var pageUrls = [];
// var pageIndex = 0;
var plotStyle = {
    width:"600px",
    height:"330px",
};
var pageData = [];

$(function(){
    // populate pageUrls
    function onListReceived(series) {
        pageUrls = series['dirs'];

        var currentPage;
        function onHashchange(e)
        {
            debug.log("hashchange triggered");
            currentPage = parseInt(toDefault($.bbq.getState("page"), "0"));
            plotGraphs(currentPage);
        };

        $(window).bind('hashchange', onHashchange);
        $(window).trigger('hashchange');

        $("#pagination").paginate({
            count                   : pageUrls.length,
            start                   : currentPage + 1,
            display                 : 19,
            border                  : false,
            text_color              : '#888',
            background_color        : '#EEE',
            text_hover_color        : 'black',
            background_hover_color  : '#CFCFCF',
            images                  : false,
            mouse                   : 'press',
            onChange                : onPageChange
        });
    }

    function toDefault(val, defaultVal)
    {
        if (val === undefined) {
            return defaultVal;
        }

        return val;
    }

    $.ajax({
        url: 'list.json',
        type: 'GET',
        dataType: 'json',
        success: onListReceived
    });

    var options = {
        lines: { show: true },
        points: { show: false },
        xaxis: { tickDecimals: 1, tickSize: 100, axisLabel: "time (second)" },
        grid: { hoverable: true, clickable: true }
    };

    function plotGraphs(pageIndex) {
        var content = $("#plots");
        var choiceContainer = $("#choices");

        content.empty();
        $(urls).each(function(i, urlinfo) {
            var container = $("<div>");
            container.addClass("plot-container");
            var plot = $("<div>");

            var title = urlinfo[0];
            var url = urlinfo[1];

            var stateKey = "plot." + url + ".enabled";
            var enabled = toDefault($.bbq.getState(stateKey), "true") == "true";
            // debug.log("enabled = " + enabled);

            var check = $("<input type=\"checkbox\">");
            check.attr("checked", enabled);
            var header = $("<h2>");
            header.append(check).append(" " + title);
            container.append(header);
            check.change(function() {
                debug.log("Checkbox changed for " + stateKey + " to " + (this.checked ? "true" : "false"));
                var state = {}
                state[stateKey] = this.checked ? "true" : "false"
                $.bbq.pushState(state);
            });
            content.append(container);

            if (enabled) {
                plot.css(plotStyle);
                var maxSetting = $("<input type=\"text\" size=\"4\">");
                var maxLabel = $("<label>").text("max");
                var maxKey = "plot." + url + ".max";
                var max = parseInt($.bbq.getState(maxKey), 10);
                if (max == 0 || isNaN(max)) {
                    max = null;
                }
                    
                maxSetting.val(max || "");
                maxSetting.change(function() {
                    var state = {};
                    state[maxKey] = $(this).val();
                    $.bbq.pushState(state);
                });

                header.append(", ").append(maxSetting).append(maxLabel);
                container.append(plot);
                plotData(plot, pageUrls[pageIndex] + '/' + url, url);
            }
        });

        retrieveTestInfo(pageIndex);
    }

    function showTooltip(x, y, contents) {
        $('<div id="tooltip">' + contents + '</div>').css( {
            position: 'absolute',
            display: 'none',
            top: y + 5,
            left: x + 5,
            border: '1px solid #fdd',
            padding: '2px',
            'background-color': '#fee',
            opacity: 0.80
        }).appendTo("body").fadeIn(200);
    }
    
    function plotData(plotSpace, urlData, plotName)
    {
        function onDataReceived(series) {
            var data = [series];
            var plotOptions = {};
            var max = $.bbq.getState("plot." + plotName + ".max");
            if(max) {
                plotOptions.yaxis = {max: max}
            }
            $.plot(plotSpace, data, $.extend({}, options, plotOptions));
        }

        $.ajax({
            url: urlData,
            type: 'GET',
            dataType: 'json',
            success: onDataReceived,
            error: function(a) { alert("AJAX failed"); }
        });

                            
        var previousPoint = null;
        plotSpace.bind("plothover", function (event, pos, item) {
            if (item) {
                if (previousPoint != item.dataIndex) {
                    previousPoint = item.dataIndex;
                    
                    $("#tooltip").remove();
                    var x = item.datapoint[0].toFixed(2),
                        y = item.datapoint[1].toFixed(2),
                        t = item.series.timestamps[item.dataIndex];

                    showTooltip(item.pageX, item.pageY,
                        y + " at " + x + "s [" + t + "]");
                }
            }
            else {
                $("#tooltip").remove();
                previousPoint = null;
            }
        });
    }

    function retrieveTestInfo(pageIndex){
        
        function onDataReceived(data){
            $("#title").text(data.title);
        }

        $.ajax({
            url: pageUrls[pageIndex] + '/test-info.json',
            type: 'GET',
            dataType: 'json',
            success: onDataReceived
        });
    }

    function onPageChange(page){
        debug.log("onPageChange, " + page);
        $.bbq.pushState({"page": parseInt(page) - 1});
    }
});
