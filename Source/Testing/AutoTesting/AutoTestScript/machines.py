﻿import os
import time
import init_env

from boto.ec2.connection import EC2Connection
import boto.ec2

colName = 0
colAddress = 1
colPort = 2
colCategory = 3
colOS = 4
colUsername = 5
colPassword = 6
colKey = 7
colFree = 8
machineListFile = 'machinelist.txt'
machineList = []

def initmachinelist(filename):
	file = open(filename, 'r')
	line = file.readline()
	line = file.readline()
	while len(line) != 0:
		parsemachine(line)
		line = file.readline()
	file.close()

def parsemachine(line):
	machine = line.split()
	machine.append(True)
	machineList.append(machine)

def getmachinelist():
	if len(machineList) == 0:
		initmachinelist(machineListFile)
	return machineList

def getfreemachinelist():
	list = getmachinelist()
	machines = []
	for machine in list:
		if True == machine[colFree]:
			machines.append(machine)
	return machines

def getmachinebyname(name):
	list = getmachinelist()
	for machine in list:
		if name == machine[colName]:
			return machine

def getfreemachinebyname(name):
	machine = getmachinebyname(name)
	if machine != None:
		if True == machine[colFree]:
			return machine

def getmachinebyaddress(address):
	list = getmachinelist()
	for machine in list:
		if address == machine[colAddress]:
			return machine

def getfreemachinebyaddress(address):
	machine = getmachinebyaddress(address)
	if machine != None:
		if True == machine[colAddress]:
			return machine

def getmachinelistbycategory(category):
	list = getmachinelist()
	machines = []
	for machine in list:
		if category == machine[colCategory]:
			machines.append(machine)
	return machines

def getfreemachinelistbycategory(category):
	list = getmachinelistbycategory(category)
	machines = []
	for machine in list:
		if True == machine[colFree]:
			machines.append(machine)
	return machines

def getmachinelistbyos(os):
	list = getmachinelist()
	machines = []
	for machine in list:
		if os == machine[colOS]:
			machines.append(machine)
	return machines

def getfreemachinelistbyos(os):
	list = getmachinelistbyos(os)
	machines = []
	for machine in list:
		if True == machine[colFree]:
			machines.append(machine)
	return machines

def getnextfreemachinebycategory(category):
	list = getmachinelistbycategory(category)
	for machine in list:
		if True == machine[colFree]:
			return machine

def getnextfreemachinebyos(os):
	list = getmachinelistbyos(os)
	for machine in list:
		if True == machine[colFree]:
			return machine

def getnextfreemachinenamebycategory(category):
	machine = getnextfreemachinebycategory(category)
	if machine != None:
		return machine[colName]
	return ''

def getnextfreemachinenamebyos(os):
	machine = getnextfreemachinebyos(os)
	if machine != None:
		return machine[colName]
	return ''

def startspotinstances(price, image, count):
	conn = EC2Connection(os.getenv('AWS_ACCESS_KEY_ID'), os.getenv('AWS_SECRET_ACCESS_KEY'))
	print 'Connected to Amazon EC2.'
	runningOld = 0
	timeDuration = 0
	reservations = conn.get_all_instances()
	instances = [i for r in reservations for i in r.instances]
	for instance in instances:
		if instance.state == 'running':
			runningOld = runningOld + 1
	runningNew = runningOld
	while runningNew < runningOld + count:
		countNew = runningOld + count - runningNew
		conn.request_spot_instances(price, image, count=countNew, key_name='amazonkey', security_groups=['autotest-linux'], instance_type='c1.medium')
		print 'Time: ' + str(timeDuration) + '  Spot Instances Requested: ' + str(countNew)
		print 'Time: ' + str(timeDuration) + '  Spot Requests Active: 0'
		activeList = []
		while len(activeList) < countNew:
			time.sleep(1.0)
			timeDuration = timeDuration + 1
			activeList = []
			requests = conn.get_all_spot_instance_requests()
			for request in requests:
				if request.state == 'active':
					activeList.append(request.id)
			if timeDuration % 10 == 0:
				print 'Time: ' + str(timeDuration) + '  Spot Requests Active: ' + str(len(activeList))
		print 'Time: ' + str(timeDuration) + '  Spot Requests Active: ' + str(len(activeList))
		conn.cancel_spot_instance_requests(activeList)
		print 'Time: ' + str(timeDuration) + '  All spot requests cancelled.'
		running = 0
		pending = 1
		while pending > 0:
			time.sleep(1.0)
			timeDuration = timeDuration + 1
			running = 0
			pending = 0
			reservations = conn.get_all_instances()
			instances = [i for r in reservations for i in r.instances]
			for instance in instances:
				if instance.state == 'running':
					running = running + 1
				elif instance.state == 'pending':
					pending = pending + 1
			if timeDuration % 10 == 0:
				print 'Time: ' + str(timeDuration) + '  Spot Instances Running: ' + str(running) + '  Spot Instances Pending: ' + str(pending)
		print 'Time: ' + str(timeDuration) + '  Spot Instances Running: ' + str(running)
		runningNew = running

def populatemachinelist(username):
	conn = EC2Connection(os.getenv('AWS_ACCESS_KEY_ID'), os.getenv('AWS_SECRET_ACCESS_KEY'))
	print 'Connected to Amazon EC2.'
	fout = open('machinelist.txt', 'w')
	fout.write('Machine-name\t\tIP-address\t\tPort\t\tCategory\t\tOS\t\tUsername\t\tPassword\t\tPrivate-key\n')
	reservations = conn.get_all_instances()
	instances = [i for r in reservations for i in r.instances]
	iter = 1
	for instance in instances:
		if instance.state == 'running':
			dnsParts = instance.dns_name.split('.')
			ipParts = dnsParts[0].split('-')
			fout.write('amazon-' + str(iter) + '\t\t' + ipParts[1] + '.' + ipParts[2] + '.' + ipParts[3] + '.' + ipParts[4] + '\t\t22\t\tamazon\t\tLinux\t\t' + username + '\t\tNone\t\tamazonkey.pem\n')
			print 'Added amazon-' + str(iter) + ' to machine list.'
			iter = iter + 1
	fout.close()

def stopinstances():
	conn = EC2Connection(os.getenv('AWS_ACCESS_KEY_ID'), os.getenv('AWS_SECRET_ACCESS_KEY'))
	print 'Connected to Amazon EC2.'
	reservations = conn.get_all_instances()
	instances = [i for r in reservations for i in r.instances]
	count = 0
	for instance in instances:
		if instance.state == 'running':
			instance.stop()
			count = count + 1
	print 'Time: 0  Instances Stopped: ' + str(count)
	timeDuration = 0
	shutting = 1
	while shutting > 0:
		time.sleep(1.0)
		timeDuration = timeDuration + 1
		shutting = 0
		reservations = conn.get_all_instances()
		instances = [i for r in reservations for i in r.instances]
		for instance in instances:
			if instance.state == 'shutting-down':
				shutting = shutting + 1
		if timeDuration % 10 == 0:
			print 'Time: ' + str(timeDuration) + '  Instances Shutting Down: ' + str(shutting)
	print 'Time: ' + str(timeDuration) + '  All instances terminated.'

def terminateinstances():
	# Not so sure whether the stop instaces function works as excpected
	conn = EC2Connection(os.getenv('AWS_ACCESS_KEY_ID'), os.getenv('AWS_SECRET_ACCESS_KEY'))
	instances = conn.get_all_instances()
	j = [i for r in instances for i in r.instances]
	j = j[6:]

	id = [i.id for i in j]
	conn.terminate_instances(id)

def rebootinstances(idx):
	conn = EC2Connection(os.getenv('AWS_ACCESS_KEY_ID'), os.getenv('AWS_SECRET_ACCESS_KEY'))
	instances = conn.get_all_instances()
	j = [i for r in instances for i in r.instances]
	j = j[idx:]

	for instance in j:
		instance.reboot()	
	
def cancelopenspotrequests():
	conn = EC2Connection(os.getenv('AWS_ACCESS_KEY_ID'), os.getenv('AWS_SECRET_ACCESS_KEY'))
	print 'Connected to Amazon EC2.'
	requests = conn.get_all_spot_instance_requests()
	requestList = []
	for request in requests:
		if request.state == 'open':
			requestList.append(request.id)
	conn.cancel_spot_instance_requests(requestList)
	
def cancelspotrequests():
	conn = EC2Connection(os.getenv('AWS_ACCESS_KEY_ID'), os.getenv('AWS_SECRET_ACCESS_KEY'))
	print 'Connected to Amazon EC2.'
	requests = conn.get_all_spot_instance_requests()
	requestList = []
	for request in requests:
		requestList.append(request.id)
	conn.cancel_spot_instance_requests(requestList)

exclusion = ['184.72.143.166', '54.235.129.247', '54.235.177.247', '54.235.177.243', '54.235.177.236', '54.243.208.89', '54.243.136.137']

def populatemachinelist_all_regions(username, filename = 'machinelist.txt'):
	conn = EC2Connection(os.getenv('AWS_ACCESS_KEY_ID'), os.getenv('AWS_SECRET_ACCESS_KEY'))
	print 'Connected to Amazon EC2.'

	regions = boto.ec2.regions()

	fout = open(filename, 'w')
	fout.write('Machine-name\t\tIP-address\t\tPort\t\tCategory\t\tOS\t\tUsername\t\tPassword\t\tPrivate-key\t\Region\n')
	iter = 1
	
	for j in range(0, len(regions)):
		connection = regions[j].connect()
		reservations = connection.get_all_instances()
		instances = [i for r in reservations for i in r.instances]
		for instance in instances:
			if instance.state == 'running' and not instance.ip_address in exclusion:
				dnsParts = instance.dns_name.split('.')
				ipParts = dnsParts[0].split('-')
				fout.write('amazon-' + str(iter) + '\t\t' + ipParts[1] + '.' + ipParts[2] + '.' + ipParts[3] + '.' + ipParts[4] + '\t\t22\t\tamazon\t\tLinux\t\t' + username + '\t\tNone\t\tamazonkey.pem\t\t' + instance.region.name + '\n')
				print 'Added amazon-' + str(iter) + ' to machine list.'
				iter = iter + 1
	fout.close()

def terminateinstances_all_regions():
	conn = EC2Connection(os.getenv('AWS_ACCESS_KEY_ID'), os.getenv('AWS_SECRET_ACCESS_KEY'))
	print 'Connected to Amazon EC2.'

	regions = boto.ec2.regions()

	counter = 0
	for j in range(0, len(regions)):
		connection = regions[j].connect()
		reservations = connection.get_all_instances()
		instances = [i for r in reservations for i in r.instances]
		for instance in instances:
			if instance.state == 'running' and not instance.ip_address in exclusion:
				print 'Terminate instance ' + instance.id + ', ' + instance.ip_address
				connection.terminate_instances(str(instance.id))
				counter += 1
	print str(counter) + " instances are termianted"

def restartinstances_all_regions():
	conn = EC2Connection(os.getenv('AWS_ACCESS_KEY_ID'), os.getenv('AWS_SECRET_ACCESS_KEY'))
	print 'Connected to Amazon EC2.'

	regions = boto.ec2.regions()

	counter = 0
	for j in range(0, len(regions)):
		connection = regions[j].connect()
		reservations = connection.get_all_instances()
		instances = [i for r in reservations for i in r.instances]
		for instance in instances:
			if instance.state == 'running' and not instance.ip_address in exclusion:
				print 'Restart instance ' + instance.id + ', ' + instance.ip_address
				connection.reboot_instances(str(instance.id))
				counter += 1
	print str(counter) + " instances are rebooted"