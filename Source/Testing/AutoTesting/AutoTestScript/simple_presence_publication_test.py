#!/usr/bin/env python
from apps import *

DEBUG = False
# DEBUG = True

if __name__ == "__main__":
	if not DEBUG:
		schedule(Seed(0.0, 'amazon-11', 300.0, '--application-name=auto_test'))

	# test variable
	seed_peer_address = '23.22.205.32:21251'
	num_users = 9
	num_friends = 0
	peer_id = 2


	i = 1
	if not DEBUG:
		schedule(Peer(0.0, 'amazon-' + str(i), 300.0, '--test-type=AutoTestPrivateChat --application-name=auto_test --seed-peer=' + seed_peer_address + ' --user-name=amazon-' + str(i) + ' --num-users=' + str(num_users) + ' --num-friends=' + str(num_friends) + ' --log-events'
			#+ ' --chat-delay=60'
			))
	for i in range(peer_id, peer_id + num_users):
		args = (0.0, 'amazon-' + str(i), 300.0, '--test-type=AutoTestPeer --application-name=auto_test --seed-peer=' + seed_peer_address + ' --log-events')
		if not DEBUG:
			schedule(Peer(*args))
		else:
			print repr(args)
	
	if not DEBUG:
		go()
	
