import math
import os

stats_file_name = r'v2 45-50 20690-20715.csv'
directory_name = r'C:\Documents and Settings\ahung\Desktop\Velocity Test Final\Velocity Test x2'

def get_time(line):
	parts = line.split(',')
	return parts[0]

def get_process_id(line):
	parts = line.split(',')
	return parts[1]
	
def get_position_x(line):
	parts = line.split('<')
	bits = parts[1].split()
	return float(bits[0])

def get_guid(line):
	parts = line.split('=')
	bits = parts[1].split()
	return bits[0] + ' ' + bits[1]

def filter_velocity_stats(directory, filename):
	local_guid = ''
	local_position = 0.0
	remote_guid = ''
	filter_state = 0
	names = filename.split('.')
	stats_file = open(os.path.join(directory, names[0] + '_filtered.csv'), 'wb')
	for line in open(os.path.join(directory, filename), 'rb'):
		parts = line.split(',')
		if filter_state == 0:
			if parts[2].startswith('Add Avatar') == True:
				local_guid = get_guid(line)
				filter_state = 1				
		elif filter_state == 1:
			if parts[2].startswith('Update Position') == True:
				if local_guid.startswith(get_guid(line)) == True:
					local_position = get_position_x(line)
					filter_state = 2
				else:
					remote_guid = get_guid(line)
		elif filter_state == 2:
			if parts[2].startswith('Update Position') == True:
				if local_guid.startswith(get_guid(line)) == True:
					local_position = get_position_x(line)
				else:
					remote_guid = get_guid(line)
					distance = math.fabs(local_position - get_position_x(line))
					if distance <= 165.0:
						stats_file.write(get_time(line) + ',' + get_process_id(line) + ',Update Position,GUID=' + get_guid(line) + ' Distance=' + str(distance) +'\n')
						filter_state = 3
			elif parts[2].startswith('Create Replica') == True:
				if remote_guid.startswith(get_guid(line)) == True:
					stats_file.write(line)
			elif parts[2].startswith('Remove Replica') == True:
				if remote_guid.startswith(get_guid(line)) == True:
					stats_file.write(line)
		elif filter_state == 3:
			if parts[2].startswith('Update Position') == True:
				if local_guid.startswith(get_guid(line)) == True:
					local_position = get_position_x(line)
				else:
					distance = math.fabs(local_position - get_position_x(line))
					if distance >= 165.0:
						stats_file.write(get_time(line) + ',' + get_process_id(line) + ',Update Position,GUID=' + get_guid(line) + ' Distance=' + str(distance) +'\n')
						filter_state = 2
			elif parts[2].startswith('Create Replica') == True:
				if remote_guid.startswith(get_guid(line)) == True:
					stats_file.write(line)
			elif parts[2].startswith('Remove Replica') == True:
				if remote_guid.startswith(get_guid(line)) == True:
					stats_file.write(line)
	stats_file.close()

filter_velocity_stats(directory_name, stats_file_name)
