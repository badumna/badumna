﻿import math
import os
import paramiko
from machines import colAddress, colPort, colCategory, colUsername, colPassword, colKey, getmachinebyname

def connectclient(name):
	machine = getmachinebyname(name)
	client = paramiko.SSHClient()
	client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
	if machine[colCategory] == 'amazon':
		client.connect(machine[colAddress], port=int(machine[colPort]), username=machine[colUsername], password=None, pkey=None, key_filename=machine[colKey])
	else:
		client.connect(machine[colAddress], port=int(machine[colPort]), username=machine[colUsername], password=machine[colPassword])
	return client

def runcommand(name, command):
	machine = getmachinebyname(name)
	print '[' + machine[colAddress] + '] run: ' + command
	client = connectclient(name)
	stdin, stdout, stderr = client.exec_command(command)
	stdin.close()
	for line in stdout.read().splitlines():
		print '[' + machine[colAddress] + '] output: ' + line
	for line in stderr.read().splitlines():
		print '[' + machine[colAddress] + '] error: ' + line
	client.close()

def putcommand(name, localPath, remotePath):
	machine = getmachinebyname(name)
	print '[' + machine[colAddress] + '] put: ' + localPath + ', ' + remotePath
	client = connectclient(name)
	ftp = client.open_sftp()
	ftp.put(localPath, remotePath)
	ftp.close()
	client.close()

def getcommand(name, remotePath, localPath):
	machine = getmachinebyname(name)
	print '[' + machine[colAddress] + '] get: ' + remotePath + ', ' + localPath
	client = connectclient(name)
	ftp = client.open_sftp()
	ftp.get(remotePath, localPath)
	ftp.close()
	client.close()

def mgetcommand(name, remotePath, localPath):
	machine = getmachinebyname(name)
	client = connectclient(name)
	stdin, stdout, stderr = client.exec_command('ls ' + remotePath)
	stdin.close()
	for line in stdout.read().splitlines():
		if line.startswith('ls: cannot access') == False:
			words = line.split()
			for word in words:
				parts = word.split('/')
				print '[' + machine[colAddress] + '] get: ' + word + ', ' + localPath + parts[len(parts) - 1]
				ftp = client.open_sftp()
				ftp.get(word, localPath + parts[len(parts) - 1])
				ftp.close()
	stderr.close()

def runrootcommand(name, command):
	machine = getmachinebyname(name)
	print '[' + machine[colAddress] + '] run: ' + command
	client = paramiko.SSHClient()
	client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
	client.connect(machine[colAddress], port=int(machine[colPort]), username='root', password=None, pkey=None, key_filename='amazonkey.pem')
	stdin, stdout, stderr = client.exec_command(command)
	stdin.close()
	for line in stdout.read().splitlines():
		print '[' + machine[colAddress] + '] output: ' + line
	for line in stderr.read().splitlines():
		print '[' + machine[colAddress] + '] error: ' + line
	client.close()

def filterpingtestnet(directory, filename):
	eventlog = open(directory + 'allevents.csv', 'rb')
	stats = open(directory + filename + '.csv', 'wb')
	line = eventlog.readline()
	while len(line) != 0:
		parts = line.split(',')
		if parts[2].endswith('Ping'):
			stats.write(parts[3])
		line = eventlog.readline()
	eventlog.close()
	stats.close()
