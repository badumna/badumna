﻿#!/usr/bin/env python
import sys
from machines import *
if __name__ == "__main__":

	if sys.argv[1] == 'setup':
		print 'populate machine list'
		populatemachinelist_all_regions('ec2-user')
	elif sys.argv[1] == 'terminate':
		print 'terminate instances'
		terminateinstances_all_regions()
