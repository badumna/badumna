﻿import init_env
import os
import paramiko
import threading
import time
from charts2 import plotcharts2
from charts2 import generate_report
from machines import colName, colAddress, colPort, colCategory, colUsername, colPassword, colKey, colFree, getmachinebyname
from xml.dom import minidom

# Set this to False when running a large-scale test
# where you've pre-loaded the amazon image with the test
# dependencies, otherwise you'll use heaps of data:
UPLOAD_TO_ALL_MACHINES = True

typePeer = 0
typeSeed = 1
typeOverload = 2
typeArbitration = 3
typeDei = 4
typeTunnel = 5
customType = 6

tempDir = 'Temp'
testDir = 'Test'
appList = []

def schedule(app):
	appList.append(app)

def clear_scheduler():
	del appList[:]

def go():
	init()
	for app in appList:
		app.start()
	wait()
	for app in appList:
		time.sleep(0.1)
		app.close()
	close()	

def init():
	if os.path.isdir(tempDir) == False:
		print '[localhost] local: mkdir ' + tempDir
		os.system('mkdir ' + tempDir)

def wait():
	timeDuration = 0
	isRunning = True
	print '[localhost] time: 0'
	while isRunning == True:
		time.sleep(1.0)
		timeDuration = timeDuration + 1
		if timeDuration % 10 == 0:
			print '[localhost] time: ' + str(timeDuration)
		isRunning = False
		for app in appList:
			if app.isrunning() == True:
				isRunning = True
				break

def close():
	generate_report(os.path.join(tempDir, 'Report'), 'ReportTemplate')
	if os.path.isdir(tempDir):
		iter = 1
		while os.path.isdir(testDir + str(iter)):
			iter = iter + 1
		print '[localhost] local: move ' + tempDir + ' ' + testDir + str(iter)
		os.system('move ' + tempDir + ' ' + testDir + str(iter))
	appList = []

class AppBase(threading.Thread):
	def __init__(self, time, name, duration, arguments, emulate_packet_loss = 0, network_emulate_duration = 0):
		threading.Thread.__init__(self)
		self.time = time
		self.duration = duration
		self.peers = ''
		self.arguments = arguments
		self.machine = getmachinebyname(name)
		self.running = False
		self.connections = []
		self.directory = ''
		self.packet_loss = emulate_packet_loss
		self.network_emulate_duration = network_emulate_duration

		if self.machine != None:
			self.machine[colFree] = False
	def run(self):
		if self.machine != None:
			self.running = True
			time.sleep(self.time)
			self.connect()
			self.load()
			self.execute()
			time.sleep(self.duration)
			self.kill()
			self.finish()
			self.disconnect()
			self.running = False
	def close(self):
		if self.machine != None:
			self.plot()
	def connect(self):
		client = paramiko.SSHClient()
		client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
		if self.machine[colCategory] == 'amazon':
			client.connect(self.machine[colAddress], port=int(self.machine[colPort]), username=self.machine[colUsername], password=None, pkey=None, key_filename=self.machine[colKey])
		else:
			client.connect(self.machine[colAddress], port=int(self.machine[colPort]), username=self.machine[colUsername], password=self.machine[colPassword])
		self.connections.append(client)
		print '[' + self.machine[colAddress] + '] Connection established for ' + self.machine[colName] + '.'
	def disconnect(self):
		for conn in self.connections:
			conn.close()
	def isrunning(self):
		return self.running
	def gettype(self):
		return None
	def getaddress(self):
		if self.machine != None:
			return self.machine[colAddress]
	def load(self):
		print 'Overload this function.'
	def execute(self):
		print 'Overload this function.'
	def kill(self):
		print 'Overload this function.'
	def finish(self):
		print 'Overload this function.'
	def plot(self):
		print '[' + self.machine[colAddress] + '] No charts to plot.'
	def runcommand(self, command):
		print '[' + self.machine[colAddress] + '] run: ' + command
		for conn in self.connections:
			stdin, stdout, stderr = conn.exec_command(command)
			stdin.close()
			for line in stdout.read().splitlines():
				print '[' + self.machine[colAddress] + '] output: ' + line
			for line in stderr.read().splitlines():
				print '[' + self.machine[colAddress] + '] error: ' + line
	def runsudocommand(self, command):
		print '[' + self.machine[colAddress] + '] run sudo: ' + command
		for conn in self.connections:
			channel = conn.get_transport().open_session()
			channel.get_pty()
			channel.exec_command(command)
	def putcommand(self, localPath, remotePath):
		print '[' + self.machine[colAddress] + '] put: ' + localPath + ', ' + remotePath
		for conn in self.connections:
			ftp = conn.open_sftp()
			ftp.put(localPath, remotePath)
			ftp.close()
	def getcommand(self, remotePath, localPath):
		print '[' + self.machine[colAddress] + '] get: ' + remotePath + ', ' + localPath
		for conn in self.connections:
			ftp = conn.open_sftp()
			ftp.get(remotePath, localPath)
			ftp.close()
	def localcommand(self, command):
		print '[' + self.machine[colAddress] + '] local: ' + command
		os.system(command)
	def mgetcommand(self, remotePath, localPath):
		for conn in self.connections:
			stdin, stdout, stderr = conn.exec_command('ls ' + remotePath)
			stdin.close()
			for line in stdout.read().splitlines():
				if line.startswith('ls: cannot access') == False:
					words = line.split()
					for word in words:
						parts = word.split('/')
						self.getcommand(word, localPath + parts[len(parts) - 1])
			stderr.close()

class Peer(AppBase):
	def run(self):
		if self.machine != None:
			self.running = True
			time.sleep(self.time)
			self.connect()
			self.load()
			self.execute()

			if self.network_emulate_duration != 0:
				time.sleep(self.network_emulate_duration)
				self.disable_network_emulator()
				time.sleep(self.duration - self.network_emulate_duration + 60.0)
			else:
				time.sleep(self.duration + 60.0)
			self.kill()
			self.finish()
			self.disconnect()
			self.running = False
	def gettype(self):
		if self.machine != None:
			return typePeer
	def load(self):
		if UPLOAD_TO_ALL_MACHINES or self.machine[colCategory] != 'amazon':
			if os.path.isdir('AutoTestManager'):
				self.runcommand('mkdir -p AutoTestManager')
				self.runcommand('mkdir -p AutoTestManager/AutoTestPeer')
				self.putcommand('AutoTestManager/AutoTestManager.exe', 'AutoTestManager/AutoTestManager.exe')
				self.putcommand('AutoTestManager/AutoTest.exe', 'AutoTestManager/AutoTest.exe')
				self.putcommand('AutoTestManager/Mono.Options.dll', 'AutoTestManager/Mono.Options.dll')
				self.putcommand('AutoTestManager/Badumna.dll', 'AutoTestManager/Badumna.dll')
				self.putcommand('AutoTestManager/Dei.dll', 'AutoTestManager/Dei.dll')
				self.putcommand('AutoTestManager/UnityEngine.dll', 'AutoTestManager/UnityEngine.dll')
				self.putcommand('AutoTestManager/PerformanceMonitor.dll', 'AutoTestManager/PerformanceMonitor.dll')
			else:
				print 'AutoTestManager folder could not be found.'
				self.machine = None
		if self.packet_loss != 0:
			self.runsudocommand('sudo tc qdisc replace dev eth0 root netem loss %f%%' % self.packet_loss)
		else:
			self.runsudocommand('sudo tc qdisc del dev eth0 root')

	def disable_network_emulator(self):
		self.runsudocommand('sudo tc qdisc del dev eth0 root')

	def execute(self):
		self.setupseed()
		self.setupoverload()
		self.setuparbitration()
		self.setupdei()
		self.setuptunnel()
		self.runcommand('cd AutoTestManager && chmod u=rwx *')
		if self.machine[colCategory] == 'amazon':
			self.runcommand('TERM=cygwin && export TERM && cd AutoTestManager && screen -dm -S autotestpeer -L mono AutoTestManager.exe --application-name=autotest --record-interval=1.0 --life-time=' + str(self.duration) + self.peers + ' ' + self.arguments)
		else:
			self.runcommand('TERM=cygwin && export TERM && cd AutoTestManager && screen -dm -S autotestpeer -L ./AutoTestManager.exe --application-name=autotest --record-interval=1.0 --life-time=' + str(self.duration) + self.peers + ' ' + self.arguments)
	def kill(self):
		screenFound = False
		screenId = ''
		for conn in self.connections:
			stdin, stdout, stderr = conn.exec_command('screen -ls')
			stdin.close()
			for line in stdout.read().splitlines():
				words = line.split()
				for word in words:
					if word.endswith('.autotestpeer'):
						screenId = word.split('.')[0]
						screenFound = True
						break
			for line in stderr.read().splitlines():
				print line
		if screenFound:
			self.runcommand('kill -9 ' + screenId)
	def finish(self):
		iter = 1
		while os.path.isdir(tempDir + '\Records-Peer-' + self.machine[colName] + '-' + str(iter)):
			iter = iter + 1
		self.directory = 'Records-Peer-' + self.machine[colName] + '-' + str(iter)
		self.localcommand('cd ' + tempDir + ' && mkdir ' + self.directory)
		self.getcommand('AutoTestManager/Records/allstats.csv', tempDir + '/' + self.directory + '/allstats.csv')
		self.mgetcommand('AutoTestManager/Records/error*.txt', tempDir + '/' + self.directory + '/')
		self.mgetcommand('AutoTestManager/Records/log*.txt', tempDir + '/' + self.directory + '/')
		self.mgetcommand('AutoTestManager/Records/replica*.csv', tempDir + '/' + self.directory + '/')
		self.mgetcommand('AutoTestManager/Records/event*.csv', tempDir + '/' + self.directory + '/')
		self.getcommand('AutoTestManager/screenlog.0', tempDir + '/' + self.directory + '/screenlog.txt')
		self.mgetcommand('../../tmp/badumna/badumna*.log', tempDir + '/' + self.directory + '/')
		if self.machine[colCategory] == 'amazon':
			self.runcommand('rm -r AutoTestManager/Records/')
			self.runcommand('rm AutoTestManager/screenlog.0')
			self.runcommand('rm -r ../../tmp/badumna/')
		else:
			self.runcommand('rm -r AutoTestManager/')
	def plot(self):
		print '[' + self.machine[colAddress] + '] Plotting charts for ' + self.directory + '.'
		plotcharts2(os.getcwd() + '\\' + tempDir + '\\' + self.directory, 'allstats')
	def setupseed(self):
		for app in appList:
			if app.gettype() == typeSeed:
				self.peers = self.peers + ' --seed-peer=' + app.getaddress() + ':21251'
	def setupoverload(self):
		for app in appList:
			if app.gettype() == typeOverload:
				self.peers = self.peers + ' --overload-peer=' + app.getaddress() + ':21252'
	def setuparbitration(self):
		for app in appList:
			if app.gettype() == typeArbitration:
				self.peers = self.peers + ' --arbitration-peer=' + app.getaddress() + ':21260'
	def setupdei(self):
		for app in appList:
			if app.gettype() == typeDei:
				self.peers = self.peers + ' --dei-config-string="' + app.getaddress() + ';21248;false;user;user_password"'
	def setuptunnel(self):
		for app in appList:
			if app.gettype() == typeTunnel:
				self.peers = self.peers + ' --tunner-server=' + app.getaddress() + ':21247'

class RemoteProcess(AppBase):
	def __init__(self, application, time, name, duration, arguments):
		AppBase.__init__(self, time, name, duration, arguments)
		self.application = application

	def gettype(self):
		if self.machine != None:
			return customType
	
	def load(self):
		if UPLOAD_TO_ALL_MACHINES or self.machine[colCategory] != 'amazon':
			if self.application == 'MatchmakingServer':
				parentDir = '../../../Applications/MatchmakingServer/MatchmakingServer/bin/Debug/'
				destDir = self.application + '/'
				if os.path.isdir(parentDir):
					self.runcommand('mkdir -p MatchmakingServer')
					self.putcommand(parentDir + 'MatchmakingServer.exe', destDir + 'MatchmakingServer.exe')
					self.putcommand(parentDir + 'Mono.Options.dll', destDir + 'Mono.Options.dll')
					self.putcommand(parentDir + 'Badumna.dll', destDir + 'Badumna.dll')
					self.putcommand(parentDir + 'Dei.dll', destDir + 'Dei.dll')
					self.putcommand(parentDir + 'GermHarness.dll', destDir + 'GermHarness.dll')
					self.putcommand(parentDir + 'PerformanceMonitor.dll', destDir + 'PerformanceMonitor.dll')
				else:
					print application + ' folder could not be found.'
					self.machine = None
	
	def execute(self):
		self.setupseed()
		self.runcommand('cd ' + self.application + ' && chmod u=rwx *')
		if self.machine[colCategory] == 'amazon':
			self.runcommand('TERM=cygwin && export TERM && cd %(application)s && screen -dm -S custom -L mono %(application)s.exe --application-name=autotest' %{ 'application' : self.application} + self.peers + ' ' + self.arguments)
		else:
			self.runcommand('TERM=cygwin && export TERM && cd %(application)s && screen -dm -S custom -L ./%(application)s.exe --application-name=autotest' %{ 'application' : self.application} + self.peers + ' ' + self.arguments)

	def kill(self):
		screenFound = False
		screenId = ''
		for conn in self.connections:
			stdin, stdout, stderr = conn.exec_command('screen -ls')
			stdin.close()
			for line in stdout.read().splitlines():
				words = line.split()
				for word in words:
					if word.endswith('.autotestpeer'):
						screenId = word.split('.')[0]
						screenFound = True
						break
			for line in stderr.read().splitlines():
				print line
		if screenFound:
			self.runcommand('kill -9 ' + screenId)

	def finish(self):		
		self.directory = 'Records-' + self.application + '-' + self.machine[colName]
		self.localcommand('cd ' + tempDir + ' && mkdir ' + self.directory)
		self.getcommand(self.application + '/allstats.csv', tempDir + '/' + self.directory + '/allstats.csv')
		self.mgetcommand('../../tmp/badumna/badumna*.log', tempDir + '/' + self.directory + '/')
		
	def plot(self):
		print '[' + self.machine[colAddress] + '] Plotting charts for ' + self.directory + '.'
		plotcharts2(os.getcwd() + '\\' + tempDir + '\\' + self.directory, 'allstats')
		
	def setupseed(self):
		for app in appList:
			if app.gettype() == typeSeed:
				if self.application == 'MatchmakingServer':
					self.peers = self.peers + ' --badumna-config-seedpeer=' + app.getaddress() + ':21251'
				else:
					self.peers = self.peers + ' --seed-peer=' + app.getaddress() + ':21251'

class Seed(AppBase):
	def gettype(self):
		if self.machine != None:
			return typeSeed
	def load(self):
		if UPLOAD_TO_ALL_MACHINES or self.machine[colCategory] != 'amazon':
			parentDir = '../../../Applications/SeedPeer/SeedPeer/bin/Debug/'
			if os.path.isdir(parentDir):
				self.runcommand('mkdir -p SeedPeer')
				self.putcommand(parentDir + 'SeedPeer.exe', 'SeedPeer/SeedPeer.exe')
				self.putcommand(parentDir + 'Badumna.dll', 'SeedPeer/Badumna.dll')
				self.putcommand(parentDir + 'Dei.dll', 'SeedPeer/Dei.dll')
				self.putcommand(parentDir + 'GermHarness.dll', 'SeedPeer/GermHarness.dll')
				self.putcommand(parentDir + 'Mono.Options.dll', 'SeedPeer/Mono.Options.dll')
				self.putcommand(parentDir + 'deiConfig', 'SeedPeer/deiConfig')
			else:
				print 'SeedPeer folder could not be found.'
				self.machine = None
	def execute(self):
		self.setupdei()
		self.runcommand('cd SeedPeer && chmod u=rwx *')
		if self.machine[colCategory] == 'amazon':
			self.runcommand('cd SeedPeer && screen -dm -S autotestseed -L mono SeedPeer.exe -v --application-name=autotest' + self.peers + ' ' + self.arguments)
		else:
			self.runcommand('cd SeedPeer && screen -dm -S autotestseed -L ./SeedPeer.exe -v --application-name=autotest' + self.peers + ' ' + self.arguments)
	def kill(self):
		screenFound = False
		screenId = ''
		for conn in self.connections:
			stdin, stdout, stderr = conn.exec_command('screen -ls')
			stdin.close()
			for line in stdout.read().splitlines():
				words = line.split()
				for word in words:
					if word.endswith('.autotestseed'):
						screenId = word.split('.')[0]
						screenFound = True
						break
			for line in stderr.read().splitlines():
				print line
		if screenFound:
			self.runcommand('kill -9 ' + screenId)
	def finish(self):
		localRecordDir = 'Records-Seed-' + self.machine[colName]
		self.localcommand('cd ' + tempDir + ' && mkdir ' + localRecordDir)
		self.getcommand('SeedPeer/screenlog.0', tempDir + '/' + localRecordDir + '/screenlog.txt')
		if self.machine[colCategory] == 'amazon':
			self.runcommand('rm SeedPeer/screenlog.0')
		else:
			self.runcommand('rm -r SeedPeer/')
	def setupdei(self):
		for app in appList:
			if app.gettype() == typeDei:
				self.peers = self.peers + ' --dei-config-string="' + app.getaddress() + ';21248;false;user;user_password"'

class Overload(AppBase):
	def gettype(self):
		if self.machine != None:
			return typeOverload
	def load(self):
		if self.machine[colCategory] != 'amazon':
			if os.path.isdir('OverloadPeer'):
				self.runcommand('mkdir -p OverloadPeer')
				self.putcommand('OverloadPeer/OverloadPeer.exe', 'OverloadPeer/OverloadPeer.exe')
				self.putcommand('OverloadPeer/Badumna.dll', 'OverloadPeer/Badumna.dll')
				self.putcommand('OverloadPeer/Dei.dll', 'OverloadPeer/Dei.dll')
				self.putcommand('OverloadPeer/GermHarness.dll', 'OverloadPeer/GermHarness.dll')
				self.putcommand('OverloadPeer/Mono.Options.dll', 'OverloadPeer/Mono.Options.dll')
			else:
				print 'OverloadPeer folder could not be found.'
				self.machine = None
	def execute(self):
		self.setupseed()
		self.setupdei()
		self.runcommand('cd OverloadPeer && chmod u=rwx *')
		if self.machine[colCategory] == 'amazon':
			self.runcommand('cd OverloadPeer && screen -dm -S autotestoverload -L mono OverloadPeer.exe -v --application-name=autotest' + self.peers + ' ' + self.arguments)
		else:
			self.runcommand('cd OverloadPeer && screen -dm -S autotestoverload -L ./OverloadPeer.exe -v --application-name=autotest' + self.peers + ' ' + self.arguments)
	def kill(self):
		screenFound = False
		screenId = ''
		for conn in self.connections:
			stdin, stdout, stderr = conn.exec_command('screen -ls')
			stdin.close()
			for line in stdout.read().splitlines():
				words = line.split()
				for word in words:
					if word.endswith('.autotestoverload'):
						screenId = word.split('.')[0]
						screenFound = True
						break
			for line in stderr.read().splitlines():
				print line
		if screenFound:
			self.runcommand('kill -9 ' + screenId)
	def finish(self):
		localRecordDir = 'Records-Overload-' + self.machine[colName]
		self.localcommand('cd ' + tempDir + ' && mkdir ' + localRecordDir)
		self.getcommand('OverloadPeer/screenlog.0', tempDir + '/' + localRecordDir + '/screenlog.txt')
		if self.machine[colCategory] == 'amazon':
			self.runcommand('rm OverloadPeer/screenlog.0')
		else:
			self.runcommand('rm -r OverloadPeer/')
	def setupseed(self):
		for app in appList:
			if app.gettype() == typeSeed:
				self.peers = self.peers + ' --badumna-config-seedpeer=' + app.getaddress() + ':21251'
	def setupdei(self):
		for app in appList:
			if app.gettype() == typeDei:
				self.peers = self.peers + ' --dei-config-string="' + app.getaddress() + ';21248;false;user;user_password"'

class Arbitration(AppBase):
	def run(self):
		if self.machine != None:
			self.running = True
			time.sleep(self.time)
			self.execute()
			time.sleep(self.duration + 30.0)
			self.kill()
			self.finish()
			self.disconnect()
			self.running = False
	def gettype(self):
		if self.machine != None:
			return typeArbitration
	def load(self):
		if self.machine[colCategory] != 'amazon':
			if os.path.isdir('AutoTestArbitration'):
				self.runcommand('mkdir -p AutoTestArbitration')
				self.putcommand('AutoTestArbitration/AutoTestArbitration.exe', 'AutoTestArbitration/AutoTestArbitration.exe')
				self.putcommand('AutoTestArbitration/AutoTest.dll', 'AutoTestArbitration/AutoTest.dll')
				self.putcommand('AutoTestArbitration/Badumna.dll', 'AutoTestArbitration/Badumna.dll')
				self.putcommand('AutoTestArbitration/Mono.Options.dll', 'AutoTestArbitration/Mono.Options.dll')
				self.putcommand('AutoTestArbitration/NetworkConfig.xml', 'AutoTestArbitration/NetworkConfig.xml')
			else:
				print 'AutoTestArbitration folder could not be found.'
				self.machine = None
	def execute(self):
		self.setupseed()
		self.setupdei()
		self.runcommand('cd AutoTestArbitration && chmod u=rwx *')
		if self.machine[colCategory] == 'amazon':
			self.runcommand('cd AutoTestArbitration && screen -dm -S autotestarbitration -L mono AutoTestArbitration.exe --application-name=autotest --record-interval=1.0 --life-time=' + str(self.duration) + self.peers + ' ' + self.arguments)
		else:
			self.runcommand('cd AutoTestArbitration && screen -dm -S autotestarbitration -L ./AutoTestArbitration.exe --application-name=autotest --record-interval=1.0 --life-time=' + str(self.duration) + self.peers + ' ' + self.arguments)
	def kill(self):
		screenFound = False
		screenId = ''
		for conn in self.connections:
			stdin, stdout, stderr = conn.exec_command('screen -ls')
			stdin.close()
			for line in stdout.read().splitlines():
				words = line.split()
				for word in words:
					if word.endswith('.autotestarbitration'):
						screenId = word.split('.')[0]
						screenFound = True
						break
			for line in stderr.read().splitlines():
				print line
		if screenFound:
			self.runcommand('kill -9 ' + screenId)
	def finish(self):
		localRecordDir = 'Records-Arbitration-' + self.machine[colName]
		self.localcommand('cd ' + tempDir + ' && mkdir ' + localRecordDir)
		self.getcommand('AutoTestArbitration/statistics.csv', tempDir + '/' + localRecordDir + '/statistics.csv')
		self.getcommand('AutoTestArbitration/screenlog.0', tempDir + '/' + localRecordDir + '/screenlog.txt')
		if self.machine[colCategory] == 'amazon':
			self.runcommand('rm AutoTestArbitration/statistics.csv')
			self.runcommand('rm AutoTestArbitration/screenlog.0')
		else:
			self.runcommand('rm -r AutoTestArbitration/')
	def plot(self):
		localRecordDir = 'Records-Arbitration-' + self.machine[colName]
		print '[' + self.machine[colAddress] + '] Plotting charts for ' + localRecordDir + '.'
		#plotcharts(os.getcwd() + '\\' + tempDir + '\\' + localRecordDir + '\\', 'statistics')
	def setupseed(self):
		for app in appList:
			if app.gettype() == typeSeed:
				self.peers = self.peers + ' --seed-peer=' + app.getaddress() + ':21251'
	def setupdei(self):
		for app in appList:
			if app.gettype() == typeDei:
				self.peers = self.peers + ' --dei-config-string="' + app.getaddress() + ';21248;false;user;user_password"'

class Dei(AppBase):
	def gettype(self):
		if self.machine != None:
			return typeDei
	def load(self):
		if self.machine[colCategory] != 'amazon':
			if os.path.isdir('DeiServer'):
				self.runcommand('mkdir -p DeiServer')
				self.putcommand('DeiServer/DeiServer.exe', 'DeiServer/DeiServer.exe')
				self.putcommand('DeiServer/Badumna.dll', 'DeiServer/Badumna.dll')
				self.putcommand('DeiServer/Dei.dll', 'DeiServer/Dei.dll')
				self.putcommand('DeiServer/GermHarness.dll', 'DeiServer/GermHarness.dll')
				self.putcommand('DeiServer/Mono.Options.dll', 'DeiServer/Mono.Options.dll')
				self.putcommand('DeiServer/Mono.Security.dll', 'DeiServer/Mono.Security.dll')
				self.putcommand('DeiServer/sqlite3.dll', 'DeiServer/sqlite3.dll')
				self.putcommand('DeiServer/System.Data.SQLite.dll', 'DeiServer/System.Data.SQLite.dll')
				self.putcommand('DeiServer/X509CertificateCreator.dll', 'DeiServer/X509CertificateCreator.dll')
				self.putcommand('DeiServer/DeiAccounts.s3db', 'DeiServer/DeiAccounts.s3db')
				self.putcommand('DeiServer/Badumna.dll.config', 'DeiServer/Badumna.dll.config')
				self.putcommand('DeiServer/DeiServer.exe.config', 'DeiServer/DeiServer.exe.config')
			else:
				print 'DeiServer folder could not be found.'
				self.machine = None
	def execute(self):
		self.runcommand('cd DeiServer && chmod u=rwx *')
		if self.machine[colCategory] == 'amazon':
			self.runcommand('cd DeiServer && screen -dm -S autotestdei -L mono DeiServer.exe -v' + self.peers + ' ' + self.arguments)
		else:
			self.runcommand('cd DeiServer && screen -dm -S autotestdei -L ./DeiServer.exe -v' + self.peers + ' ' + self.arguments)
	def kill(self):
		screenFound = False
		screenId = ''
		for conn in self.connections:
			stdin, stdout, stderr = conn.exec_command('screen -ls')
			stdin.close()
			for line in stdout.read().splitlines():
				words = line.split()
				for word in words:
					if word.endswith('.autotestdei'):
						screenId = word.split('.')[0]
						screenFound = True
						break
			for line in stderr.read().splitlines():
				print line
		if screenFound:
			self.runcommand('kill -9 ' + screenId)
	def finish(self):
		localRecordDir = 'Records-Dei-' + self.machine[colName]
		self.localcommand('cd ' + tempDir + ' && mkdir ' + localRecordDir)
		self.getcommand('DeiServer/screenlog.0', tempDir + '/' + localRecordDir + '/screenlog.txt')
		if self.machine[colCategory] == 'amazon':
			self.runcommand('rm DeiServer/screenlog.0')
		else:
			self.runcommand('rm -r DeiServer/')

class Tunnel(AppBase):
	def gettype(self):
		if self.machine != None:
			return typeTunnel
	def load(self):
		if self.machine[colCategory] != 'amazon':
			if os.path.isdir('HttpTunnel'):
				self.runcommand('mkdir -p HttpTunnel')
				self.putcommand('HttpTunnel/Tunnel.exe', 'HttpTunnel/Tunnel.exe')
				self.putcommand('HttpTunnel/Badumna.dll', 'HttpTunnel/Badumna.dll')
				self.putcommand('HttpTunnel/GermHarness.dll', 'HttpTunnel/GermHarness.dll')
				self.putcommand('HttpTunnel/Mono.Options.dll', 'HttpTunnel/Mono.Options.dll')
				self.putcommand('HttpTunnel/Badumna.dll.config', 'HttpTunnel/Badumna.dll.config')
				self.putcommand('HttpTunnel/NetworkConfig.xml', 'HttpTunnel/NetworkConfig.xml')
			else:
				print 'HttpTunnel folder could not be found.'
				self.machine = None
	def execute(self):
		self.runcommand('cd HttpTunnel && chmod u=rwx *')
		if self.machine[colCategory] == 'amazon':
			self.runcommand('cd HttpTunnel && screen -dm -S autotesttunnel -L mono Tunnel.exe -v' + self.peers + ' ' + self.arguments)
		else:
			self.runcommand('cd HttpTunnel && screen -dm -S autotesttunnel -L ./Tunnel.exe -v' + self.peers + ' ' + self.arguments)
	def kill(self):
		screenFound = False
		screenId = ''
		for conn in self.connections:
			stdin, stdout, stderr = conn.exec_command('screen -ls')
			stdin.close()
			for line in stdout.read().splitlines():
				words = line.split()
				for word in words:
					if word.endswith('.autotesttunnel'):
						screenId = word.split('.')[0]
						screenFound = True
						break
			for line in stderr.read().splitlines():
				print line
		if screenFound:
			self.runcommand('kill -9 ' + screenId)
	def finish(self):
		localRecordDir = 'Records-Tunnel-' + self.machine[colName]
		self.localcommand('cd ' + tempDir + ' && mkdir ' + localRecordDir)
		self.getcommand('HttpTunnel/screenlog.0', tempDir + '/' + localRecordDir + '/screenlog.txt')
		if self.machine[colCategory] == 'amazon':
			self.runcommand('rm HttpTunnel/screenlog.0')
		else:
			self.runcommand('rm -r HttpTunnel/')
