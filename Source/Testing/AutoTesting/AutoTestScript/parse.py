
import re

def parse_log(lines):
	while True:
		line = lines.next()
		parts = line.split(';', 4)
		if len(parts) != 4:
			continue

		time = int(parts[0])
		tag = parts[2]
		message = parts[3].strip()

		if tag.find('RemoteCall') == -1:
			continue

		yield (time, message)


def transport_messages(log_entries):
	address_re = re.compile('.*src=([^,]*), dst=([^,]*), (<header not set>|seq=\d+, flags=\w*) (.*)')
	for entry in log_entries:
		direction = entry[1][:3]
		if direction not in ['-->', '<--']:
			continue

		match = address_re.match(entry[1])
		if match is None:
			continue

		groups = match.groups()
		src = groups[0]
		dst = groups[1]
		call = groups[3]

		yield (entry[0], direction, dst if direction == '-->' else src, call)

def parse_log_tag(log_tags, lines):
	while True:
		line = lines.next()

		parts = line.split(';', 4)

		if len(parts) != 4:
			continue

		time = int(parts[0])
		log_level = parts[1]
		tag = parts[2]
		messages = parts[3]

		for t in log_tags:
			if tag.find(t) != -1:
				yield (time, log_level, tag, messages)
				break


def convert(name, tag):
	with open(name) as input:
		messages = [m for m in parse_log_tag(tag, input)]

	with open(name + '-' + '-'.join(tag) + '.tsv', 'w') as out:
		out.write('time\tlog leve\tlog tag\tmessage\n')

		for m in messages:
			for i in range(0, len(m)):
				out.write(str(m[i]).strip() + '\t')
			out.write('\n')

def go(name):
	with open(name) as inp:
		messages = [m for m in transport_messages(parse_log(inp)) if m[2] != '<not set>']

	endpoint_only = lambda x: x.split('|')[1]

	addresses = sorted(set(endpoint_only(x[2]) for x in messages))
	address_to_id = {x[1]: x[0] for x in enumerate(addresses)}
	#last_message_time = [-1] * len(addresses)
	last_message_time = [""] * len(addresses)

	with open(name + '-processed.tsv', 'w') as outf:
		outf.write('time\t' + '\t'.join(addresses) + '\n')

		for message in messages:
			outf.write(str(message[0]))
			for i in range(len(last_message_time)):
				outf.write('\t')
				if i == address_to_id[endpoint_only(message[2])]:
					outf.write('"' + message[1] + ' ' + message[3] + '"')
					#last_message_time[i] = message[0]
					last_message_time[i] = '"' + message[1] + ' ' + message[3][:10] + '"'
				#else:
				#	outf.write(last_message_time[i])
					#outf.write(str(message[0] - last_message_time[i]) if last_message_time[i] > 0 else "")
			outf.write('\n')
