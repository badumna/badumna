import math
import os

stats_file_name = 'collated_stats.csv'
directory_name = r'W:\Source\Testing\AutoTesting\AutoTestScript\Test Results 3\Churn Test 50pc Scene 25'
hour_cut_off = 6
minute_cut_off = 3

def iterate_through_subdir(directory):
	nodes = os.listdir(directory)
	for node in nodes:
		if node == 'allstats.csv':
		#	collate_stats(directory, 'allstats.csv')
			collate_remote_entity_counts(directory, 'allstats.csv')
		#if node.startswith('event'):
		#	filter_velocity_test(directory, node)
		#	filter_discover_test(directory, node)
		elif os.path.isdir(os.path.join(directory, node)):
			iterate_through_subdir(os.path.join(directory, node))

def collate_stats(directory, filename):
	print 'Collating ' + os.path.join(directory, filename)
	stats_file = open(os.path.join(directory_name, stats_file_name), 'ab')
	for line in open(os.path.join(directory, filename), 'rb'):
		if line.startswith('DateTime') == False:
			if line.startswith('2012') == True:
				parts = line.split()
				bits = parts[1].split(':')
				if int(bits[0]) > hour_cut_off:
					stats_file.write(line)
				elif int(bits[0]) == hour_cut_off:
					if int(bits[1]) >= minute_cut_off:
						stats_file.write(line)
	stats_file.close()

def get_time_from_stats(line):
	parts = line.split(',')
	return parts[0]

def get_remote_entity_count_from_stats(line):
	parts = line.split(',')
	return parts[5]
	
def collate_remote_entity_counts(directory, filename):
	print 'Collating ' + os.path.join(directory, filename)
	stats_file = open(os.path.join(directory_name, stats_file_name), 'ab')
	for line in open(os.path.join(directory, filename), 'rb'):
		if line.startswith('DateTime') == False:
			if line.startswith('2012') == True:
				parts = line.split()
				bits = parts[1].split(':')
				if int(bits[0]) > hour_cut_off:
					stats_file.write(get_time_from_stats(line) + ',' + get_remote_entity_count_from_stats(line) + '\n')
				elif int(bits[0]) == hour_cut_off:
					if int(bits[1]) >= minute_cut_off:
						stats_file.write(get_time_from_stats(line) + ',' + get_remote_entity_count_from_stats(line) + '\n')
	stats_file.close()
	
def gettime(line):
	parts = line.split(',')
	return parts[0]

def getpositionx(line):
	parts = line.split('<')
	bits = parts[1].split()
	return float(bits[0])

def getpositiony(line):
	parts = line.split('<')
	bits = parts[1].split()
	return float(bits[1])

def getguid(line):
	parts = line.split('=')
	bits = parts[1].split()
	return bits[0] + ' ' + bits[1]

def stringexistsinstringarray(array, text):
	for member in array:
		if member.startswith(text) == True:
			return True
	return False

def filter_velocity_test(directory, filename):
	print 'Filtering ' + os.path.join(directory, filename)
	stats_file = open(os.path.join(directory_name, stats_file_name), 'ab')
	peerlist = []
	newpeerfound = True
	while newpeerfound == True:
		newpeerfound = False
		peerguid = ' '
		peerstate = 0
		positionx = 0.0
		positiony = 0.0
		eventlog = open(os.path.join(directory, filename), 'rb')
		line = eventlog.readline()
		while len(line) != 0:
			parts = line.split(',')
			if peerstate == 0:
				if parts[2].startswith('Update Position') == True:
					positionx = getpositionx(line)
					positiony = getpositiony(line)
					peerstate = 1
			elif peerstate == 1:
				if parts[2].startswith('Deserialise Replica') == True:
					if peerguid.startswith(' ') == True:
						newguid = getguid(line)
						if stringexistsinstringarray(peerlist, newguid) == False:
							if positiony == getpositiony(line):
								distance = math.fabs(positionx - getpositionx(line))
								if distance > 10.0:
									peerlist.append(newguid)
									peerguid = newguid
									newpeerfound = True
									peerstate = 2
				elif parts[2].startswith('Update Position') == True:
					positionx = getpositionx(line)
					positiony = getpositiony(line)
			elif peerstate == 2:
				if parts[2].startswith('Create Replica') == True:
					if peerguid.startswith(getguid(line)) == True:
						stats_file.write(gettime(line) + ',Create Replica,' + peerguid + '\n')
						peerstate = 3
				elif parts[2].startswith('Update Position') == True:
					positionx = getpositionx(line)
					positiony = getpositiony(line)
			elif peerstate == 3:
				if parts[2].startswith('Deserialise Replica') == True:
					if peerguid.startswith(getguid(line)) == True:
						distance = math.fabs(positionx - getpositionx(line))
						if distance <= 150.0:
							stats_file.write(gettime(line) + ',Deserialise Replica,' + peerguid + ',' + str(distance) + '\n')
							peerstate = 4
				elif parts[2].startswith('Update Position') == True:
					positionx = getpositionx(line)
					positiony = getpositiony(line)
			elif peerstate == 4:
				if parts[2].startswith('Deserialise Replica') == True:
					if peerguid.startswith(getguid(line)) == True:
						distance = math.fabs(positionx - getpositionx(line))
						if distance >= 150.0:
							stats_file.write(gettime(line) + ',Deserialise Replica,' + peerguid + ',' + str(distance) + '\n')
							peerstate = 5
				elif parts[2].startswith('Update Position') == True:
					positionx = getpositionx(line)
					positiony = getpositiony(line)
			elif peerstate == 5:
				if parts[2].startswith('Remove Replica') == True:
					if peerguid.startswith(getguid(line)) == True:
						stats_file.write(gettime(line) + ',Remove Replica,' + peerguid + '\n')
						peerstate = 2
				elif parts[2].startswith('Update Position') == True:
					positionx = getpositionx(line)
					positiony = getpositiony(line)
			line = eventlog.readline()
		eventlog.close()
	stats_file.close()

def filter_discover_test(directory, filename):
	print 'Filtering ' + os.path.join(directory, filename)
	stats_file = open(os.path.join(directory_name, stats_file_name), 'ab')
	peer_number = int(directory.split('-')[3])
	create_count = 0
	for line in open(os.path.join(directory, filename), 'rb'):
		parts = line.split(',')
		if parts[2].startswith('Badumna Log In') == True:
			stats_file.write(line)
		elif parts[2].startswith('Create Replica') == True:
			if create_count < (peer_number - 2):
				stats_file.write(line)
				create_count = create_count + 1
	stats_file.close()

iterate_through_subdir(directory_name)
