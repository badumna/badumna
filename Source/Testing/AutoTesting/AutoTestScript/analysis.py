#!/bin/env python

from __future__ import print_function
import os
import sys
import re
import collections
from datetime import datetime
import random
import math
from PIL import Image
import glob
import pickle
import operator
from pprint import pprint
from scipy.stats import gaussian_kde
import numpy


#  --------------------------  Parsing  -----------------------------------

parsers = {
    'int': int,
    'str': str,
    'float': float,
    'datetime': lambda x: datetime.strptime(x, '%Y-%m-%d %H:%M:%S.%f')
}

grammar = {
    'Id': ('{}_Scene{}_{}', [('region', 'str'), ('scene', 'int'), ('id', 'int')]),
    'BadumnaLogIn': ('{},{},Badumna Log In,Public address: {};Private address{}', [('timestamp', 'datetime',), ('pid', 'int'), ('address', 'str'), ('rest', 'str')]),
    'UsernameLogEntry': ('{},{},Username,{}', [('timestamp', 'datetime'), ('pid', 'int'), ('id', 'Id')]),
    'PingLogEntry': ('{},{},Ping,{},{},{}', [('timestamp', 'datetime'), ('pid', 'int'), ('id', 'Id'), ('sequence_number', 'int'), ('ping', 'float')]),
    'RttLogEntry': ('{},{},rtt,{},{},{}', [('timestamp', 'datetime'), ('pid', 'int'), ('source', 'str'), ('dest', 'str'), ('rtt', 'float')]),
    'CountLogEntry': ('{0},{1},{2},(EstDelay|Interval),{4},{5}', [('timestamp', 'datetime'), ('pid', 'int'), ('system', 'str'), ('kind', 'str'), ('source', 'str'), ('value', 'float')]),
    'MissingCountLogEntry': ('{0},{1},{2},Missing,{3}', [('timestamp', 'datetime'), ('pid', 'int'), ('system', 'str'), ('source', 'str')])
}


def parser(regex, result_type):
    """Create a parser that parses text matching the regex into a object of result_type."""
    regex = re.compile(regex)
    def parse(text):
        match = regex.match(text)
        if match is not None:
            return result_type(*[parsers[t](x) for t, x in zip(result_type.types, match.groups())])
    return parse

def process_grammar(grammar):
    """Convert a grammar into parsers and classes."""
    for name, description in grammar.iteritems():
        text = description[0]
        fields = description[1]
        field_names = [x[0] for x in fields]
        new_type = collections.namedtuple(name, field_names)
        globals()[name] = new_type
        new_type.types = [x[1] for x in fields]
        def make_str(text):
            new_type.__str__ = lambda self: text.format(*self)
        make_str(text)
        regex = text.format(*['(?P<{}>.*)'.format(n) for n in field_names])
        parsers[name] = parser(regex, new_type)

process_grammar(grammar)
parse_id = parsers['Id']
parse_log_entry = parsers['PingLogEntry']
parse_login = parsers['BadumnaLogIn']
parse_username_entry = parsers['UsernameLogEntry']
parse_rtt_entry = parsers['RttLogEntry']
parse_count_entry = parsers['CountLogEntry']
parse_missing_count_entry = parsers['MissingCountLogEntry']

#  --------------------------  Generate test data  -----------------------------------


def get_normal_sample(mean, stdev):
    """Returns a normally distributed random sample."""
    u1 = random.random()
    u2 = random.random()
    z = math.sqrt(-2 * math.log(u1)) * math.cos(2 * math.pi * u2)
    return stdev * z + mean


def gen_data_old():
    """Generates a set of sample data."""
    regions = ['usa_east', 'usa_west', 'asia', 'uk']
    scene_size = 20
    total_peers = 1000
    total_scenes = total_peers // scene_size
    total_pings = 50
    max_id = scene_size / len(regions)

    mean = 50
    stdev = 5

    output_dir = 'ping-data'

    for peer in (Id(r, s, i) for r in regions for s in range(1, total_scenes + 1) for i in range(1, max_id + 1)):
        with open('{}\\{}.csv'.format(output_dir, peer), 'w') as output:
            for seq in range(total_pings):
                for partner in (Id(r, peer.scene, i) for r in regions for i in range(1, max_id + 1)):
                    if peer == partner:
                        continue
                    output.write(str(PingLogEntry(timestamp=datetime(2013,1,1), pid=1, id=partner, sequence_number=seq, ping=get_normal_sample(mean, stdev))) + '\n')

def load_data_old(input_dir='ping-data'):
    """Loads the dumped data into a dictionary:
       {peer_id: {partner_id: [ping1, ping2, ...], ...}, ...}
    """
    data = {}
    max_ping = 0
    num_entries = 0
    for filename in glob.iglob(input_dir + '\\*.csv'):
        with open(filename) as input_file:
            peer_id = parse_id(filename[:filename.rfind('.')])
            data[peer_id] = {}
            assert peer_id is not None
            for line in input_file:
                entry = parse_log_entry(line)
                if entry is not None:
                    if entry.id not in data[peer_id]:
                        num_entries += 1
                    data[peer_id].setdefault(entry.id, []).append(entry.ping)
                    max_ping = max(max_ping, entry.ping)

    return (data, num_entries, max_ping)


#  -------------------------- Main  -----------------------------------

def load_ping_data(input_dir):
    """Loads the dumped data into a dictionary:
       {peer_id: {partner_id: [ping1, ping2, ...], ...}, ...}
    """
    data = {}
    max_ping = 0
    num_entries = 0
    max_sequence = 0
    for filename in glob.iglob(input_dir + '\\*\\event*.csv'):
        print("Loading " + filename)
        with open(filename) as input_file:
            peer_id = None
            for line in input_file:
                entry = parse_log_entry(line)
                if entry is not None:
                    assert peer_id is not None
                    if entry.id not in data[peer_id]:
                        num_entries += 1
                    data[peer_id].setdefault(entry.id, []).append(entry.ping)
                    max_ping = max(max_ping, entry.ping)
                    max_sequence = max(max_sequence, len(data[peer_id][entry.id]))

                entry = parse_username_entry(line)
                if entry is not None:
                    peer_id = entry.id
                    data[peer_id] = {}

                entry = parse_rtt_entry(line)
                if entry is not None:
                    # TODO: Process RTTs
                    pass

    return (data, num_entries, max_ping, max_sequence)


def get_colour(region_a, region_b):
    """
    Get a colour to represent a region pair.

    Intra-region pairs aren't distinguished well by colour,
    but can be determined by size.

    Red: us-east, ap OR us-east, us-east
    Green: us-west, ap OR us-west, us-west
    Blue: eu, ap OR eu, eu
    Black: ap, ap
    Yellow: us-east, us-west
    Magenta: eu, us-east
    Cyan: eu, us-west
    """
    colours = {
        'us-east': (255, 0, 0),
        'us-west': (0, 255, 0),
        'eu': (0, 0, 255),
        'ap': (0, 0, 0),
    }
    return tuple(int((v[0] + v[1]) / 2) for v in zip(colours[region_a], colours[region_b]))

regions = ['us-east', 'us-west', 'eu', 'ap']

region_pairs = []
for region_a in regions:
    for region_b in regions:
        # Don't want to add mirrored duplicates
        if (region_b, region_a) not in region_pairs:
            region_pairs.append((region_a, region_b))


def select_region_pair(data, region_pair):
    """Enumerate ping sets for a given region pair."""
    for peer_id, peer_data in data.iteritems():
        for partner_id, partner_data in peer_data.iteritems():
            if (peer_id.region == region_pair[0] and partner_id.region == region_pair[1]) or (peer_id.region == region_pair[1] and partner_id.region == region_pair[0]):
                yield partner_data


def dump_all(data, num_entries, max_ping, max_sequence):
    """
    Makes an image where each row corresponds to the set of pings
    from peer x to peer y.  Each pixel is one ping.
    """
    image = Image.new('RGB', (max_sequence, num_entries))
    pixels = image.load()

    y = 0
    for peer_id, peer_data in data.iteritems():
        for partner_id, partner_data in peer_data.iteritems():
            x = 0
            for ping in partner_data:
                colour = get_colour(peer_id.region, partner_id.region)
                ratio = 1.0 - float(ping) / max_ping
                pixels[x, y] = tuple(int(x * ratio) for x in colour)
                x += 1
            y += 1

    return image


def ping_times(data, num_entries, max_ping):
    """
    Makes an image where each row corresponds to the set of
    pings from peer x to peer y.  Each ping is represented
    by a red pixel, the horizontal position indicates the
    ping time.
    """
    image = Image.new('RGB', (max_ping + 1, num_entries), (255, 255, 255))
    pixels = image.load()

    alpha = 0.2

    y = 0

    for region_pair in region_pairs:
        for partner_data in select_region_pair(data, region_pair):
            for ping in partner_data:
                x = int(ping)
                colour = get_colour(region_pair[0], region_pair[1])
                pixels[x, y] = tuple(int(v[0] * (1 - alpha) + v[1]) for v in zip(pixels[x, y], colour))
            y += 1

    return image


def make_histogram(samples, bucket_size):
    """Return a list of (midpoint, count, normalized_count) where
          midpoint - value identifying the bucket, it's middle value
                    (e.g. for bucket_size = 10, the midpoints will be 5, 15, 25, etc)
          count - the count of samples that fall in this bucket
          normalized_count - count divided by the total number of samples
    """
    histogram = {}
    max_bucket = 0
    for sample in samples:
        bucket = int(math.floor(float(sample) / bucket_size))
        max_bucket = max(max_bucket, bucket)
        histogram[bucket] = histogram.setdefault(bucket, 0) + 1

    midpoint = bucket_size / 2.0
    return [(x * bucket_size + midpoint, histogram.get(x, 0), float(histogram.get(x, 0)) / len(samples)) for x in range(max_bucket + 1)]


def get_stats(samples):
    """Return (mean, stdev) of the samples."""
    mean = float(sum(samples)) / len(samples)
    stdev = math.sqrt(sum(math.pow(x - mean, 2) for x in samples) / len(samples))
    return (mean, stdev)


def make_update_delay_histograms(data):
    """
    Makes histograms and summary CSV files for the update delay (estimated by halving the ping time).
    """
    with open('summary.csv', 'w') as summary_file:
        summary_file.write('Regions,Mean,Stdev\n')

        for region_pair in region_pairs:
            # Just combine all ping samples for a region pair together.
            pings = reduce(operator.add, select_region_pair(data, region_pair), [])
            # And halve, to get the update delay
            pings = [x / 2.0 for x in pings]

            region_desc = '{}_{}'.format(region_pair[0], region_pair[1])

            summary_file.write('{},{},{}\n'.format(region_desc, *get_stats(pings)))

            with open(region_desc + '.csv', 'w') as region_file:
                region_file.write('Midpoint,Count,Normalized\n')
                for entry in make_histogram(pings, 10):
                    region_file.write('{},{},{}\n'.format(*entry))


def load_dataset(filename, loader):
    try:
        with open(filename, 'rb') as pickle_file:
            dataset = pickle.load(pickle_file)
            print("Loaded cached data.")
    except:
        print("No cached data found, loading from original files.")
        dataset = loader()
        with open(filename, 'wb') as pickle_file:
            pickle.dump(dataset, pickle_file)

    return dataset


def ping_main():
    dataset = load_dataset('ping-data.bin', lambda: load_ping_data(DATA_DIR))

    data, num_entries, max_ping, max_sequence = dataset
    max_ping = int(math.ceil(max_ping))

    #dump_all(data, num_entries, max_ping, max_sequence).save("raw.bmp")

    ping_times(data, num_entries, max_ping).save("test.bmp")
    make_update_delay_histograms(data)


#  -------------------------- Count test  -----------------------------------

CountKey = collections.namedtuple('CountKey', ['peer', 'channel', 'source', 'kind'])

def load_count_data(input_dir):
    results = {}

    min_timestamp = None
    def update_timestamp(ts):
        return min(min_timestamp, ts) if min_timestamp is not None else ts

    for filename in glob.iglob(input_dir + '\\*\\event*.csv'):
        print("Loading " + filename)
        peer_id = None
        with open(filename) as input_file:
            for line in input_file:
                entry = parse_login(line)
                if entry is not None:
                    peer_id = entry.address
                    min_timestamp = update_timestamp(entry.timestamp)
                    print('Address = ' + peer_id)

                entry = parse_count_entry(line)
                if entry is not None:
                    results.setdefault(CountKey(peer_id, entry.system, entry.source, entry.kind), []).append((entry.timestamp, entry.value))
                    min_timestamp = update_timestamp(entry.timestamp)
                    continue

                entry = parse_missing_count_entry(line)
                if entry is not None:
                    results.setdefault(CountKey(peer_id, entry.system, entry.source, 'Missing'), []).append((entry.timestamp, 1))
                    min_timestamp = update_timestamp(entry.timestamp)
                    continue

    return dict((key, [((value[0] - min_timestamp).total_seconds(), value[1]) for value in values]) for key, values in results.iteritems())

def normalize_to_zero_mean(values):
    mean = numpy.mean(values)
    return (value - mean for value in values)

def count_main(data_dir):
    return load_dataset('count-data.bin', lambda: load_count_data(data_dir))


def summarize(values):
    return [len(values), numpy.mean(values), numpy.std(values)] + numpy.percentile(values, [0, 25, 50, 75, 100])

def filter_dict(d, crit):
    return {k: v for k, v in d.iteritems() if crit(k)}

#DATA_DIR = r'C:\Users\davidgc\Workspaces\badumna\Source\Testing\AutoTesting\AutoTestManager\bin\Debug'
DATA_DIR = r'C:\Users\davidgc\Workspaces\badumna\Source\Testing\AutoTesting\AutoTestScript\Test9'


counts = count_main(DATA_DIR)

colour_map = {
    'Replication': '#d36060',
    'RPC': '#d3b660',
    'ControllerRPC': '#9ad360',
    'Chat': '#60d37d',
    'ControllerReplication': '#60d3d3',
    'HostedReplication': '#607dd3',
    'HostedRPC': '#9960d3'
}

# reduce(lambda d, i: d.update({i[0].channel: d.get(i[0].channel, 0) + len(i[1])}) or d, (i for i in counts.iteritems() if i[0].kind == 'Missing'), {})

# with open('intervals.csv', 'w') as fout:
#     fout.write('peer,channel,source,kind,count,mean,stdev,min,25th,50th,75th,max\n')
#     for key, values in counts.iteritems():
#         if key.kind == 'Interval':
#             fout.write(','.join(str(x) for x in list(key) + summarize(list(v[1] for v in values))) + '\n')

# i = 0
# fig = figure()
# fig.add_subplot(1, 1, 1, axisbg='#2b1344')
# for k in sorted((k for k in counts.iterkeys() if k.kind == 'Interval' and k.peer == peer), key=lambda k: (k.source, k.channel)):
#     arr = np.array(counts[k])
#     scatter(arr[:,0], i * np.ones(arr.shape[0]), color=colour_map[k.channel])
#     i += 1

# per_chan = {}
# for k, v in counts.iteritems():
#     if k.kind == 'Interval':
#         per_chan[k.channel] = per_chan.get(k.channel, 0) + len(v)
