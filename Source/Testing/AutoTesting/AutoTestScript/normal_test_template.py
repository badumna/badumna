﻿#--------------------------------------------------------------------------------------------
# Normal test template
#--------------------------------------------------------------------------------------------

from apps import *

DEBUG = True

if __name__ == "__main__":
	if not DEBUG:
		schedule(Seed(0.0, 'amazon-1', 600.0, '--application-name=auto_test'))

	# test variable
	seed_peer_address = 'seedpeer.example.com:21251'
	total_scenes = 10
	peer_each_scene = 50
	peer_id = 2

	# iterate for each different scene
	for j in range(1, total_scenes + 1):
		for i in range(peer_id, peer_id + peer_each_scene):
			if not DEBUG:
				schedule(Peer(0.0, 'amazon-' + str(i), 600.0, '--application-name=auto_test --add-avatar="RandomWalker;40;100;0;0,750;900" --seed-peer=' + seed_peer_address +' --scene-name=Scene' + str(j)))
			else:
				print 'schedule(Peer(0.0, amazon-' + str(i) + ', 600.0, --application-name=auto_test --add-avatar="RandomWalker;40;100;0;0,750;900" --seed-peer=' + seed_peer_address +' --scene-name=Scene' + str(j) + '))'
		peer_id += peer_each_scene
	
	if not DEBUG:
		go()