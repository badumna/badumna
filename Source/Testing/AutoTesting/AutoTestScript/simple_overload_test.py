﻿from apps import *
from machines import *

if __name__ == "__main__":
	schedule(Overload(0.0, getnextfreemachinenamebycategory('nicta'), 80.0, ''))
	schedule(Peer(10.0, getnextfreemachinenamebycategory('nicta'), 60.0, '--num-peers=3 --add-avatar=RandomWalker --force-overload'))
	go()