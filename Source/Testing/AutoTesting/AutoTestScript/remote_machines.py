import paramiko
from  machines import *
import threading


class ConnectionTest(threading.Thread):
	def __init__(self, name):
		threading.Thread.__init__(self)
		self.machine = getmachinebyname(name)
	def run(self):
		self.running = True
		client = paramiko.SSHClient()
		client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
		try:
			client.connect(self.machine[colAddress], port=int(self.machine[colPort]), username=self.machine[colUsername], password=None, pkey=None, key_filename=self.machine[colKey])
		except:
			print '[' + self.machine[colAddress] + '] Connection exception - ' + self.machine[colName] + '.'
			self.running = False
			return

		stdin, stdout, stderr = client.exec_command('ls')
		files = stdout.readlines()
		if len(files) == 5:
			print '[' + self.machine[colAddress] + '] Connection established for ' + self.machine[colName] + '.'
		else:
			print '[' + self.machine[colAddress] + '] Connection error - ' + self.machine[colName] + '.'			
		self.running = False

connections = []

def schedule(test):
	connections.append(test)

def start():
	for test in connections:
		test.start()
	finish = False
	while not finish:
		finish = True
		for test in connections:
			if test.running == True:
				finish = False

peer_id = 601
totalpeer = 700

for i in range(peer_id, totalpeer + 1):
	schedule(ConnectionTest('amazon-%s' %i))

start()