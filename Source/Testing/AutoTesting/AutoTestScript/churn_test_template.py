#--------------------------------------------------------------------------------------------
# Churn test template
#--------------------------------------------------------------------------------------------

from apps import *

DEBUG = True

if __name__ == "__main__":
	
	# test variable
	seed_peer_address = 'seedpeer.example.com:21251'
	churn_test_rate = 0.2
	total_scenes = 10
	peer_each_scene = 50
	peer_id = 2

	if not DEBUG:
		schedule(Seed(0.0, 'amazon-1', 900.0, '--application-name=auto_test'))

	# calculate number of peer killed mid way.
	peer_killed = int(peer_each_scene * churn_test_rate)

	# iterate for each different scene
	for j in range(1, total_scenes + 1):
		for i in range(peer_id, peer_id + peer_each_scene - peer_killed):
			if not DEBUG:
				schedule(Peer(0.0, 'amazon-' + str(i), 900.0, '--application-name=auto_test --add-avatar="RandomWalker;40;100;0;0,750;900" --seed-peer=' + seed_peer_address +' --scene-name=Scene' + str(j)))
			else:
				print 'schedule(Peer(0.0, amazon-' + str(i) + ', 900.0, --application-name=auto_test --add-avatar="RandomWalker;40;100;0;0,750;900" --seed-peer=' + seed_peer_address +' --scene-name=Scene' + str(j) + '))'
		
		# peers that killed after 5 minutes
		for i in range(peer_id + peer_each_scene - peer_killed, peer_id + peer_each_scene):
			if not DEBUG:
				schedule(Peer(0.0, 'amazon-' + str(i), 300.0, '--application-name=auto_test --add-avatar="RandomWalker;40;100;0;0,750;900" --seed-peer=' + seed_peer_address +' --scene-name=Scene' + str(j)))
			else:
				print 'schedule(Peer(0.0, amazon-' + str(i) + ', 300.0, --application-name=auto_test --add-avatar="RandomWalker;40;100;0;0,750;900" --seed-peer=' + seed_peer_address +' --scene-name=Scene' + str(j) + '))'

		# restart peer at 10th minutes.
		for i in range(peer_id + peer_each_scene - peer_killed, peer_id + peer_each_scene):
			if not DEBUG:
				schedule(Peer(600.0, 'amazon-' + str(i), 300.0, '--application-name=auto_test --add-avatar="RandomWalker;40;100;0;0,750;900" --seed-peer=' + seed_peer_address +' --scene-name=Scene' + str(j)))
			else:
				print 'schedule(Peer(600.0, amazon-' + str(i) + ', 300.0, --application-name=auto_test --add-avatar="RandomWalker;40;100;0;0,750;900" --seed-peer=' + seed_peer_address +' --scene-name=Scene' + str(j) + '))'

		peer_id += peer_each_scene
	
	if not DEBUG:
		go()