#-------------------------------------------------------------------------------------------
# Script used for converting the .csv file to json format.
#-------------------------------------------------------------------------------------------
import init_env

import os
import shutil
import re
import sys
from collections import namedtuple, defaultdict
import simplejson as json


regexps = [
    re.compile(r'.*? (?P<time>.*?),(?P<pid>\d+),(?P<active_connection>.*?),(?P<bytes_sent>.*?),(?P<bytes_received>.*?),(?P<remote_entity>.*?),(?P<initialize_connection>.*?),(?P<update_count>.*?),(?P<cpu_usage>\d+(\.\d{1,2})?) %,(?P<free_mem>\d+(\.\d{1,2})?) MB'), # allstats
    re.compile(r'.*? (?P<time>.*?),(?P<pid>\d+),(?P<remote_entity>\d+),')
]

def get_millisecond(time):
    el = time.split(':')
    return int(el[0]) * 3600000 + int(el[1]) * 60000 + float(el[2]) * 1000

StatItem = namedtuple('StatItem',
        ['timestamp','milliseconds','active_connection','bytes_sent','bytes_received','remote_entity','initialize_connection','update_count','cpu_usage','free_mem']
        )

#-------------------------------------------------------------------------------------------
# Plotcharts2
#-------------------------------------------------------------------------------------------
def plotcharts2(directory, filename):
    parent_directory = os.path.dirname(directory)
    if not os.path.exists(os.path.join(directory, filename + '.csv')):
        print "SKIPPING: " + directory
        return

    allstats = defaultdict(lambda: [])
    for line in open(os.path.join(directory, filename + '.csv'), 'rb'):
        allstats_match = regexps[0].match(line.strip())
        if allstats_match:
            groups = allstats_match.groupdict()
            val = allstats[groups['pid']]
            val.append(StatItem(
                timestamp=groups['time'],
                milliseconds=get_millisecond(groups['time']),
                active_connection=int(groups['active_connection']),
                bytes_sent=int(groups['bytes_sent']),
                bytes_received=int(groups['bytes_received']),
                remote_entity=int(groups['remote_entity']),
                initialize_connection=int(groups['initialize_connection']),
                update_count=int(groups['update_count']),
                cpu_usage=float(groups['cpu_usage']),
                free_mem=float(groups['free_mem'])
           ))
    
    for pid, data in allstats.items():
        process_stats(data, directory, parent_directory, pid)

def zip_stat_items(items):
    '''Convert a list of StatItem objects into a single StatItem object where each
    field is a list of the individual values (preserving order)
    '''
    zipped = defaultdict(lambda: [])
    items = [item._asdict() for item in items]
    for field in StatItem._fields:
        zipped[field] = [item[field] for item in items]
    return StatItem(**zipped)

#---------------------------------------------------------------------------------------
# Outputing json file that will be used by flot to plot the graph.
#---------------------------------------------------------------------------------------
def process_stats(data, directory, parent_directory, pid):
    basename = os.path.basename(directory)
    print directory

    data = zip_stat_items(data)
    relative_time = [(t - data.milliseconds[0])/ 1000. for t in data.milliseconds]

    # create new directory for each process
    output_dir = os.path.join(parent_directory, "Report\%(peer)s_output_%(pid)s" %{ 'peer': basename, 'pid': pid })

    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    write_json_output(relative_time ,  data.active_connection ,  data.timestamp ,  'Active connection' ,  'active-connection.json' ,  output_dir)
    write_json_output(relative_time ,  data.bytes_sent        ,  data.timestamp ,  'Bytes sent'        ,  'bytes-sent.json'        ,  output_dir)
    write_json_output(relative_time ,  data.bytes_received    ,  data.timestamp ,  'Bytes received'    ,  'bytes-received.json'    ,  output_dir)
    write_json_output(relative_time ,  data.cpu_usage         ,  data.timestamp ,  'Cpu usage'         ,  'cpu-usage.json'         ,  output_dir)
    write_json_output(relative_time ,  data.remote_entity     ,  data.timestamp ,  'Remote entity'     ,  'remote-entity.json'     ,  output_dir)
    write_json_output(relative_time ,  data.update_count      ,  data.timestamp ,  'Received updates'  ,  'received-updates.json'  ,  output_dir)

    write_test_info_output(directory, pid, output_dir)

def write_json_output(x, y, timestamps, label, output, directory):
    with open(os.path.join(directory, output), 'w') as f:
        json.dump({ 'label': label, 'data': zip(x,y), 'timestamps': timestamps }, f)

def write_test_info_output(directory, pid, output_dir):
    with open(os.path.join(output_dir, "test-info.json"), 'w') as f:
        json.dump({ 'title': os.path.basename(directory) + "_" + pid, 'pid': pid }, f)

#---------------------------------------------------------------------------------------
# Reporting - generate html report, copy the report template to the test result folder.
#---------------------------------------------------------------------------------------

def generate_report(directory, template_dir):
    dirs = []
    
    for r in (n for n in os.listdir(directory) if os.path.isdir(os.path.join(directory, n))):
        dirs.append(r)

    # generate list.json
    file_output = open(os.path.join(directory, 'list.json'), 'w')
    print >> file_output, "{ \"dirs\": %(data)s }" %{ 'data': str(dirs).replace("'", "\"") }
    file_output.close()

    # copy template
    for f in os.listdir(template_dir):
        shutil.copy(os.path.join(template_dir, f), directory)

def generate_all(tempdir="Temp"):
    '''Generate a report from all directories under `tempdir`'''
    reports_dir = os.path.join(tempdir, "Report")
    if os.path.exists(reports_dir):
        shutil.rmtree(reports_dir)

    for d in os.listdir(tempdir):
        plotcharts2(os.path.join(tempdir, d), "allstats")
    generate_report(reports_dir, "ReportTemplate")

if __name__ == '__main__':
    generate_all(*sys.argv[1:])
