﻿//-----------------------------------------------------------------------
// <copyright file="Program.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2010 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace AutoTestManager
{
    using System;
    using System.Collections.Generic;

    using Mono.Options;

    /// <summary>
    /// The AutoTestManager is used to manage multiple instances of AutoTestPeers.
    /// </summary>
    internal class Program
    {
        /// <summary>
        /// The AutoTestManager used to control AutoTestPeers.
        /// </summary>
        private static AutoTestManager autoTestManager = new AutoTestManager();

        /// <summary>
        /// Option set for parsing command line options.
        /// </summary>
        private static OptionSet optionSet = new OptionSet()
        {
            { "h|help", "Display this message and exit.", var =>
                {
                    Program.WriteUsageAndExit();
                }
            }
        };

        /// <summary>
        /// AutoTestManager main function, which takes arguments from the command line.
        /// </summary>
        /// <param name="args">Command line arguments.</param>
        private static void Main(string[] args)
        {
            try
            {
                var managerArgs = Program.optionSet.Parse(args);
                var extraArgs = Program.autoTestManager.Initialize(managerArgs);

                if (extraArgs.Count > 0)
                {
                    Program.HandleUnknownArguments(extraArgs);
                }
                else
                {
                    Console.CancelKeyPress += delegate(object sender, ConsoleCancelEventArgs e)
                    {
                        e.Cancel = true;
                        Program.autoTestManager.Stop();
                    };

                    Console.WriteLine("AutoTestManager has been started.");
                    Program.autoTestManager.Start();
                    Console.WriteLine("AutoTestManager has been stopped.");
                }
            }
            catch (OptionException ex)
            {
                Console.WriteLine("AutoTestManager: {0}", ex.Message);
                Console.WriteLine("Try 'AutoTestManager --help' for more information.");
            }
        }

        /// <summary>
        /// Write error message listing unknown arguments.
        /// </summary>
        /// <param name="args">Unknown arguments.</param>
        private static void HandleUnknownArguments(IEnumerable<string> args)
        {
            Console.Write("AutoTestManager: Unknown argument(s):");

            foreach (string arg in args)
            {
                Console.Write(" " + arg);
            }

            Console.WriteLine(".");
            Console.WriteLine("Try 'AutoTestManager --help' for more information.");
        }

        /// <summary>
        /// Write usage instructions and exit.
        /// </summary>
        private static void WriteUsageAndExit()
        {
            Console.WriteLine("Usage: AutoTestManager [Options]");
            Console.WriteLine("Start a AutoTestManager on the local machine.");
            Console.WriteLine(string.Empty);
            Console.WriteLine("Options:");
            Program.autoTestManager.WriteOptionDescriptions(Console.Out);
            Program.optionSet.WriteOptionDescriptions(Console.Out);
            Environment.Exit(0);
        }
    }
}