﻿//-----------------------------------------------------------------------
// <copyright file="AutoTestManager.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2010 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace AutoTestManager
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Text;
    using System.Threading;

    using AutoTest;

    using Mono.Options;

    /// <summary>
    /// Manager used to control AutoTestPeers.
    /// </summary>
    public class AutoTestManager : ProcessBase
    {
        /// <summary>
        /// Time interval in between regular main process updates.
        /// </summary>
        private static TimeSpan ProcessUpdateInterval = TimeSpan.FromMilliseconds(200);

        /// <summary>
        /// List to store references to peers.
        /// </summary>
        private readonly List<Process> peerProcesses = new List<Process>();

        /// <summary>
        /// Flag indicating whether the manager is running in mono.
        /// </summary>
        private bool isMono;

        /// <summary>
        /// Dictionary that stored all the arguments provided by the user.
        /// </summary>
        private Dictionary<string, string> arguments = new Dictionary<string, string>();

        /// <summary>
        /// The name of the peer program file.
        /// </summary>
        private string fileName = "AutoTest.exe";

        /// <summary>
        /// Name of output directory to store records.
        /// </summary>
        private string outputDirectory = "Records";

        /// <summary>
        /// The life time that the peers will run for, in seconds.
        /// </summary>
        private double lifeTime = -1.0f;

        /// <summary>
        /// The number of peers to deploy.
        /// </summary>
        private int numPeers = 1;               

        /// <summary>
        /// Timer used for stopping the manager.
        /// </summary>
        private Timer timer;
        
        /// <summary>
        /// Initializes a new instance of the AutoTestManager class.
        /// </summary>
        public AutoTestManager()
            : base(ProcessUpdateInterval)
        {
            var managerOptions = new OptionSet
            {
                 {
                     Constants.FileNameString, "Name of the peer program file.",
                     var => { this.fileName = var; }
                 },
                 {
                     Constants.TestTypeString,
                     "Auto test type that should be run (e.g. AutoTestPeer, AutoTestPrivateChat).", 
                     var => { this.AddArgument(Constants.TestTypeString, var); }
                 },
                 {
                     Constants.NumPeersString, "Number of peers to deploy.",
                     (int var) => { this.numPeers = var; }
                 },
                 {
                     Constants.AppNameString, "Name of the application.",
                     var => { this.AddArgument(Constants.AppNameString, var); }
                 },
                 {
                     Constants.SceneNameString, "Name of network scene to add avatars to.",
                     var => { this.AddArgument(Constants.SceneNameString, var); }
                 },
                 {
                     Constants.LifeTimeString,
                     "How long the peers will run for, in seconds.",
                     (float var) => 
                     { 
                         this.AddArgument(Constants.LifeTimeString, var.ToString());
                         this.lifeTime = var;
                     }
                 },
                 {
                     Constants.AddAvatarString, "Name of avatar type to add to peer.",
                     var => { this.AddArgument(Constants.AddAvatarString, var); }
                 },
                 {
                     Constants.RecordIntervalString,
                     "Time between recording intervals, in seconds.",
                     var => { this.AddArgument(Constants.RecordIntervalString, var); }
                 },
                 {
                     Constants.SeedPeerString, "IP address and port number of seed peer.",
                     var => { this.AddArgument(Constants.SeedPeerString, var); }
                 },
                 {
                     Constants.DisableBroadcastString, "Disable broadcast.",
                     var => { this.AddArgument(Constants.DisableBroadcastString); }
                 },
                 {
                     Constants.DisablePortForwardingStrng, "Disable port forwarding.",
                     var => { this.AddArgument(Constants.DisablePortForwardingStrng); }
                 },
                 {
                     Constants.OverloadPeerString,
                     "IP address and port number of overload peer.",
                     var => { this.AddArgument(Constants.OverloadPeerString, var); }
                 },
                 {
                     Constants.DiscoverOverloadString, "Enable overload service discovery.",
                     var => { this.AddArgument(Constants.DiscoverOverloadString); }
                 },
                 {
                     Constants.ForceOverloadString, "Force peer to use overload peer.",
                     var => { this.AddArgument(Constants.ForceOverloadString); }
                 },
                 {
                     Constants.ArbitrationPeerString,
                     "IP address and port number of arbitration peer.",
                     var => { this.AddArgument(Constants.ArbitrationPeerString, var); }
                 },
                 {
                     Constants.DiscoverArbitrationString,
                     "Enable arbitration service discovery.",
                     var => { this.AddArgument(Constants.DiscoverArbitrationString); }
                 },
                 {
                     Constants.ArbitrationIntervalString,
                     "Time between arbitration events, in seconds.",
                     var => { this.AddArgument(Constants.ArbitrationIntervalString, var); }
                 },
                 {
                     Constants.DeiConfigStringString,
                     "String containing the Dei configuration.",
                     var => { this.AddArgument(Constants.DeiConfigStringString, var); }
                 },
                 {
                     Constants.BandwidthLimitString, "Desired bandwith limit, in bytes.",
                     var => { this.AddArgument(Constants.BandwidthLimitString, var); }
                 },
                 {
                     Constants.TunnelServerString,
                     "IP address and port number of tunnel server.",
                     var => { this.AddArgument(Constants.TunnelServerString, var); }
                 },
                 {
                     Constants.LogEventsString, "Enable event logging.",
                     var => { this.AddArgument(Constants.LogEventsString); }
                 },
                 {
                     Constants.AreaOfInterestString, "Area of interest radius of entities.",
                     var => { this.AddArgument(Constants.AreaOfInterestString, var); }
                 },
                 {
                     Constants.EntityVelocityString, "Maximum entity velocity.",
                     var => { this.AddArgument(Constants.EntityVelocityString, var); }
                 },
                 {
                     Constants.OutputDirectoryString, "Name of output directory.",
                     var => 
                     { 
                         this.AddArgument(Constants.OutputDirectoryString, var);
                         this.outputDirectory = var;
                     }
                 },
                 {
                     Constants.UserNameString, "Name of user.",
                     var => { this.AddArgument(Constants.UserNameString, var); }
                 },
                 {
                     Constants.SetPingString, "Set as ping peer.",
                     var => { this.AddArgument(Constants.SetPingString); }
                 },
                 {
                     Constants.NumUsersString, "Total number of user.",
                     var => { this.AddArgument(Constants.NumUsersString, var); }
                 },
                 {
                     Constants.NumFriendsString, "Number of friends.",
                     var => { this.AddArgument(Constants.NumFriendsString, var); }
                 },
                 {
                     Constants.ChatDelayString, "Private chat delay time in seconds.",
                     var => { this.AddArgument(Constants.ChatDelayString, var); }
                 },
                 {
                     Constants.MatchmakingRequest, 
                     "Matchmaking request with the following format, MaxPlayers;MinPlayers;PlayerGroup;PlayerAttributes.",
                     var => { this.AddArgument(Constants.MatchmakingRequest, var); }
                 },
                 {
                     Constants.MatchmakingDelays,
                     "Matchmaking delays with the following format, delay in seconds;delay in seconds, etc.",
                     var => { this.AddArgument(Constants.MatchmakingDelays, var); }
                 },
                 {
                     Constants.CloudIdentifier,
                     "Cloud identifier string used in Badumna Cloud.",
                     var => { this.AddArgument(Constants.CloudIdentifier, var); }
                 },
                 {
                     Constants.ConfigFileName,
                    "Name of config file to initialize Badumna configuration from.",
                     var => { this.AddArgument(Constants.ConfigFileName, var); }
                 },
                 {
                     Constants.MatchSize,
                    "The number of peers in each Badumna.Match",
                     var => { this.AddArgument(Constants.MatchSize, var); }
                 },
                 {
                     Constants.HostLeaveAfter,
                     "The time in seconds after which the host will leave the match",
                     var => { this.AddArgument(Constants.HostLeaveAfter, var); }
                 },
                 {
                     Constants.NumHostedEntities,
                     "The number of hosted entities in each match",
                     var => { this.AddArgument(Constants.NumHostedEntities, var); }
                 }
             };

            foreach (var option in managerOptions)
            {
                this.optionSet.Add(option);
            }

            this.GenerateDefaultArguments();
        }

        /// <inheritdoc />
        protected override void Update(TimeSpan interval)
        { 
            this.UpdateProcess(interval);
        }

        /// <inheritdoc />
        protected override void OnStartUp()
        {
            // Set the timer for stopping the manager if the lifetime is set.
            // adding 30 seconds to allow the peer process to stop gracefully.
            if (this.lifeTime > 0)
            {
                this.timer = new Timer(
                    (object state) => { this.Stop(); },
                    null,
                    (long)(this.lifeTime + 30) * 1000,
                    Timeout.Infinite);
            }

            this.isMono = Type.GetType("Mono.Runtime") != null;            

            this.DeployPeers();
        }

        /// <summary>
        /// Called when the manager is shutting down.
        /// </summary>
        protected override void OnShutDown()
        {
            if (this.timer != null)
            {
                this.timer.Dispose();
            }

            this.KillPeers();
            this.CollateOutputs();            
        }

        /// <summary>
        /// Update the main process functionality.
        /// </summary>
        /// <param name="timeStep">Time since last update.</param>
        private void UpdateProcess(TimeSpan interval)
        {
            int peerCount = this.peerProcesses.Count;

            for (int i = peerCount - 1; i >= 0; --i)
            {
                var process = this.peerProcesses[i];

                if (process.HasExited)
                {
                    this.peerProcesses.RemoveAt(i);
                    Console.WriteLine("AutoTestManager: Process {0} exited with code {1}", process.Id, process.ExitCode);
                    Console.WriteLine("AutoTestManager: {0} peers left.", this.peerProcesses.Count);
                }
            }

            if (this.peerProcesses.Count == 0)
            {
                this.Stop();
            }
        }

        /// <summary>
        /// Generate the default arguments, the arguments that must be provided 
        /// and have default values.
        /// </summary>
        private void GenerateDefaultArguments()
        {
            this.arguments.Add(
                Constants.TestTypeString, 
                string.Format(" --{0}AutoTestPeer", Constants.TestTypeString));

            this.arguments.Add(
                Constants.OutputDirectoryString,
                string.Format(" --{0}{1}", Constants.OutputDirectoryString, this.outputDirectory));
        }

        /// <summary>
        /// Add command line without custom argument.
        /// </summary>
        /// <param name="type">Command type.</param>
        private void AddArgument(string type)
        {
            // will overwrite the default arguments.
            this.arguments[type] = string.Format(" --{0}", type);
        }

        /// <summary>
        /// Add command line with an argument.
        /// </summary>
        /// <param name="type">Command line type.</param>
        /// <param name="value">Argument value.</param>
        private void AddArgument(string type, string value)
        {
            // will overwrite the default arguments.
            if (string.IsNullOrEmpty(value))
            {
                throw new ArgumentException(string.Format("Invalid arguments of type {0}", type));
            }

            this.arguments[type] = string.Format(" --{0}{1}", type, value); 
        }

        /// <summary>
        /// Deploy peers using the command line settings.
        /// </summary>
        private void DeployPeers()
        {
            if (this.numPeers <= 0)
            {
                return;
            }

            var startInfo = new ProcessStartInfo();
            startInfo.WindowStyle = ProcessWindowStyle.Hidden;
            startInfo.FileName = this.fileName;

            if (this.isMono)
            {
                startInfo.FileName = "mono";
                startInfo.Arguments = this.fileName;
            }

            foreach (var argument in this.arguments.Values)
            {
                startInfo.Arguments += argument;
            }
            
            for (var i = 0; i < this.numPeers; ++i)
            {
                var process = new Process { StartInfo = startInfo };
                process.Start();

                this.peerProcesses.Add(process);
            }

            Console.WriteLine("AutoTestManager: {0} peers deployed.", this.numPeers);
        }

        /// <summary>
        /// Kill the peers.
        /// </summary>
        private void KillPeers()
        {
            var peers = this.peerProcesses.Count;

            if (peers == 0)
            {
                return;
            }

            foreach (var process in this.peerProcesses)
            {
                if (!process.HasExited)
                {
                    process.Kill();
                }
            }
            
            this.peerProcesses.Clear();
        }

        /// <summary>
        /// Collates data from individual peer stats files into one file.
        /// </summary>
        private void CollateOutputs()
        {
            if (!Directory.Exists(this.outputDirectory))
            {
                return;
            }

            string[] outputs = new string[] { "stat*.csv", "error*.txt", "log*.txt", "replica*.csv", "event*.csv" };

            foreach (var output in outputs)
            {
                string[] files = Directory.GetFiles(this.outputDirectory, output, SearchOption.TopDirectoryOnly);

                string separator = "\\";

                if (this.isMono)
                {
                    separator = "/";
                }

                FileStream fileStream = new FileStream(string.Format("{0}{1}all{2}", this.outputDirectory, separator, output.Replace('*', 's')), FileMode.Create);
                
                List<string> data = new List<string>();
                string headings = string.Empty;

                foreach (var filename in files)
                {
                    this.WriteLine(fileStream, filename);

                    TextReader reader = File.OpenText(filename);
                    string line = reader.ReadLine();

                    while (line != null)
                    {
                        this.WriteLine(fileStream, line);
                        line = reader.ReadLine();
                    }
                }
                
                fileStream.Close();
            }
        }

        /// <summary>
        /// Add text to file stream.
        /// </summary>
        /// <param name="fileStream">Reference to file stream.</param>
        /// <param name="value">Text to add.</param>
        private void WriteLine(FileStream fileStream, string value)
        {
            if (fileStream == null)
            {
                return;
            }

            byte[] info = new UTF8Encoding(true).GetBytes(value + "\r\n");
            fileStream.Write(info, 0, info.Length);
        }              
    }
}