﻿//-----------------------------------------------------------------------
// <copyright file="Program.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2010 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace AutoTestArbitration
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Reflection;
    using System.Xml;

    using AutoTest;

    using Badumna;
    using Badumna.ServiceDiscovery;

    using Mono.Options;
    using GermHarness;

    /// <summary>
    /// TODO: Arbitration Peer description.
    /// </summary>
    internal class Program
    {
        /// <summary>
        /// The peer harness for hosting the process and communicating with the Control Center.
        /// </summary>
        private static PeerHarness peerHarness;

        /// <summary>
        /// Name of network configuration file.
        /// </summary>
        private static string configFile = "NetworkConfig.xml";        

        /// <summary>
        /// IP address and port number of the seed peer.
        /// </summary>
        private static string seedPeer = string.Empty;

        /// <summary>
        /// Option set for parsing command line arguments.
        /// </summary>
        private static OptionSet optionSet = new OptionSet()
            {
                { "n|network-configuration=", "Read the network configuration from this file.", var =>
                    {
                        Program.configFile = var;
                    }
                },
                { "h|help", "Display this message and exit.", var =>
                    {
                        Program.WriteUsageAndExit();
                    }
                },
                { Constants.SeedPeerString, "IP address and port number of seed peer.", var =>
                    {
                        Program.seedPeer = var;
                    }
                },
            };

        /// <summary>
        /// Entry point for program.
        /// </summary>
        /// <param name="args">Command line arguments.</param>
        private static void Main(string[] args)
        {
            // Parse program-specific command line arguments.
            try
            {
                Program.peerHarness = new PeerHarness(new Options(Program.LoadOptions()));
                Program.peerHarness.ProcessNetworkStateIntervalMs = 10;

                List<string> extras = Program.optionSet.Parse(args);
                string[] harnessArgs = extras.ToArray();

                if (Program.peerHarness.Initialize(ref harnessArgs, new ArbitrationProcess(Program.peerHarness)))
                {
                    if (harnessArgs.Length > 0)
                    {
                        Program.HandleUnknownArguments(harnessArgs);
                    }
                    else
                    {
                        // install the cancel key handler
                        Console.CancelKeyPress += delegate(object sender, ConsoleCancelEventArgs e)
                        {
                            e.Cancel = true;
                            Program.peerHarness.Stop();
                        };

                        // start the harness to launch the arbitration peer, this will not return until the arbitration peer process is terminated.
                        Program.peerHarness.RegisterService(ServerType.Arbitration);
                        Program.peerHarness.Start();
                        Console.WriteLine("Arbitration peer has been stopped.");
                    }
                }
                else
                {
                    Console.WriteLine("ArbitrationPeer: Could not initialize harness.");
                }
            }
            catch (OptionException ex)
            {
                Console.WriteLine("ArbitrationPeer: {0}", ex.Message);
                Console.WriteLine("Try `ArbitrationPeer --help' for more information.");
            }
        }

        /// <summary>
        /// Write error message listing unknown arguments.
        /// </summary>
        /// <param name="arguments">Unknown arguments.</param>
        private static void HandleUnknownArguments(string[] arguments)
        {
            Console.Write("ArbitrationPeer: Unknown argument(s):");
            foreach (string arg in arguments)
            {
                Console.Write(" " + arg);
            }

            Console.WriteLine(".");
            Console.WriteLine("Try `ArbitrationPeer --help' for more information.");
        }

        /// <summary>
        /// Write usage instructions and exit.
        /// </summary>
        private static void WriteUsageAndExit()
        {
            Console.WriteLine("Usage: ArbitrationPeer [Options]");
            Console.WriteLine("Start an Arbitration Peer on the local machine.");
            Console.WriteLine(string.Empty);
            Console.WriteLine("Options:");
            Program.peerHarness.WriteOptionDescriptions(Console.Out);
            Program.optionSet.WriteOptionDescriptions(Console.Out);
            Environment.Exit(0);
        }

        /// <summary>
        /// Load the network configuration for Badumna.
        /// </summary>
        /// <returns>Network configuration.</returns>
        private static XmlDocument LoadOptions()
        {
            XmlDocument networkConfig = new XmlDocument();
            networkConfig.Load(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), Program.configFile));

            if (!string.IsNullOrEmpty(Program.seedPeer))
            {
                XmlNode seedPeerNode = networkConfig.SelectSingleNode("/Modules/Module/Initializer");
                seedPeerNode.InnerText = Program.seedPeer;
            }

            return networkConfig;
        }
    }
}