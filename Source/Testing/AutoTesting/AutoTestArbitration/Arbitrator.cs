﻿//------------------------------------------------------------------------
// <copyright file="Arbitrator.cs" company="NICTA">
//      Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//------------------------------------------------------------------------

namespace AutoTestArbitration
{
    using System;

    using AutoTest;
    using AutoTest.ArbitrationEvents;

    using Badumna;
    using Badumna.Arbitration;

    /// <summary>
    /// Arbitrator class.
    /// </summary>
    public class Arbitrator
    {
        /// <summary>
        /// The network facade through which to access Badumna features.
        /// </summary>
        private INetworkFacade networkFacade;

        /// <summary>
        /// Reference to world which handles arbitration events.
        /// </summary>
        private World world;

        /// <summary>
        /// Stores number of arbitration events handled.
        /// </summary>
        private int numArbitrationEvents = 0;

        /// <summary>
        /// Initializes a new instance of the Arbitrator class.
        /// </summary>
        /// <param name="networkFacade">The network facade to use.</param>
        public Arbitrator(INetworkFacade networkFacade)
        {
            this.networkFacade = networkFacade;
            this.world = new World(this);
        }

        /// <summary>
        /// Gets the value of numArbitrationEvents.
        /// </summary>
        public int NumArbitrationEvents
        {
            get { return this.numArbitrationEvents; }
        }

        /// <summary>
        /// Set numArbitrationEvents back to zero.
        /// </summary>
        public void ResetNumArbitrationEvents()
        {
            this.numArbitrationEvents = 0;
        }

        /// <summary>
        /// Receive message from the client, which then this message will be 
        /// deserialize and pass it to Handle function
        /// </summary>
        /// <param name="sessionId">Session ID.</param>
        /// <param name="message">Message in byte array.</param>
        public void HandleClientEvent(int sessionId, byte[] message)
        {
            ArbitrationEvent clientEvent;

            try
            {
                clientEvent = EventSet.Deserialize(message);
            }
            catch (Exception)
            {
                return;
            }

            Console.WriteLine("{0} from session id:{1}", clientEvent.GetType().Name, sessionId);
            this.world.Handle(clientEvent, sessionId);
            this.numArbitrationEvents++;
        }

        /// <summary>
        /// This callback function will be called when there is a disconnected Player.
        /// </summary>
        /// <param name="sessionId">Session ID.</param>
        public void HandleClientDisconnect(int sessionId)
        {
            this.world.RemovePlayer(sessionId);
        }

        /// <summary>
        /// Send the event from the server to clients
        /// </summary>
        /// <param name="destinationSessionId">Destination session ID.</param>
        /// <param name="serverEvent">Server event.</param>
        public void SendServerEvent(int destinationSessionId, ArbitrationEvent serverEvent)
        {
            this.networkFacade.SendServerArbitrationEvent(
                destinationSessionId,
                EventSet.Serialize(serverEvent));
        }
    }
}