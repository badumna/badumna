﻿//------------------------------------------------------------------------
// <copyright file="World.cs" company="NICTA">
//      Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//------------------------------------------------------------------------

namespace AutoTestArbitration
{
    using System;
    using System.Collections.Generic;

    using AutoTest;

    using Badumna.Arbitration;
    using Badumna.DataTypes;

    /// <summary>
    /// World class represent the world or scene in api example, and have a responsibility to 
    /// handle the arbitration events (i.e. receiving, sending and handling events)
    /// </summary>
    public class World
    {
        /// <summary>
        /// Dictionary of client event handlers.
        /// </summary>
        private Dictionary<Type, ClientEventHandler> clientHandlers;

        /// <summary>
        /// Dictionary of players.
        /// </summary>
        private Dictionary<int, Player> players = new Dictionary<int, Player>();

        /// <summary>
        /// Reference to arbitrator object.
        /// </summary>
        private Arbitrator arbitrator;

        /// <summary>
        /// Initializes a new instance of the World class.
        /// </summary>
        /// <param name="arbitrator">Arbitration handler.</param>
        public World(Arbitrator arbitrator)
        {
            this.arbitrator = arbitrator;

            this.clientHandlers = new Dictionary<Type, ClientEventHandler>
            {
                { typeof(MoveEvent), this.Move },
                { typeof(OfflineEvent), this.Offline }
            };
        }

        /// <summary>
        /// Delegate of client event handler.
        /// </summary>
        /// <param name="clientEvent">Client event.</param>
        /// <param name="sessionId">Session ID.</param>
        private delegate void ClientEventHandler(ArbitrationEvent clientEvent, int sessionId);

        /// <summary>
        /// Receive the incoming event and pass it to the right handler
        /// if there is not appropriate handler then do nothing about it
        /// </summary>
        /// <param name="clientEvent">Client event.</param>
        /// <param name="sessionId">Session ID.</param>
        public void Handle(ArbitrationEvent clientEvent, int sessionId)
        {
            ClientEventHandler handler;

            if (!this.clientHandlers.TryGetValue(clientEvent.GetType(), out handler))
            {
                return;
            }

            handler(clientEvent, sessionId);
        }

        /// <summary>
        /// Remove player.
        /// </summary>
        /// <param name="sessionId">Session ID.</param>
        public void RemovePlayer(int sessionId)
        {
            if (this.players.ContainsKey(sessionId))
            {
                this.players.Remove(sessionId);
            }
        }

        /// <summary>
        /// Move player.
        /// </summary>
        /// <param name="clientEvent">Client event.</param>
        /// <param name="sessionId">Session ID.</param>
        private void Move(ArbitrationEvent clientEvent, int sessionId)
        {
            Player player = this.GetOrCreatePlayer(sessionId);
            player.Position = ((MoveEvent)clientEvent).Destination;
            this.arbitrator.SendServerEvent(sessionId, new ReplyEvent());
            Console.WriteLine("Player with sessionId: {0} move to {1}", sessionId, player.Position.ToString());
        }

        /// <summary>
        /// Remove player.
        /// </summary>
        /// <param name="clientEvent">Client event.</param>
        /// <param name="sessionId">Session ID.</param>
        private void Offline(ArbitrationEvent clientEvent, int sessionId)
        {
            this.RemovePlayer(sessionId);
        }

        /// <summary>
        /// Gets or create a player.
        /// </summary>
        /// <param name="sessionId">Session ID.</param>
        /// <returns>Player object.</returns>
        private Player GetOrCreatePlayer(int sessionId)
        {
            Player player;
            if (!this.players.TryGetValue(sessionId, out player))
            {
                player = new Player();
                this.players.Add(sessionId, player);
            }

            return player;
        }

        /// <summary>
        /// Player class is currently only has position as its attribute
        /// TODO: More attributes might be required to handle various events
        /// </summary>
        private class Player
        {
            /// <summary>
            /// Gets or sets the player position.
            /// </summary>
            public Vector3 Position { get; set; }
        }
    }
}