﻿//------------------------------------------------------------------------
// <copyright file="ArbitrationProcess.cs" company="NICTA">
//      Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//------------------------------------------------------------------------

namespace AutoTestArbitration
{
    using System;
    using System.IO;

    using AutoTest;
    using GermHarness;

    /// <summary>
    /// Arbitration process.
    /// </summary>
    public class ArbitrationProcess : IHostedProcess
    {
        /// <summary>
        /// The peer harness hosting this process.
        /// </summary>
        private readonly IPeerHarness peerHarness;

        /// <summary>
        /// Reference to arbitrator class.
        /// </summary>
        private Arbitrator arbitrator;

        /// <summary>
        /// Initializes a new instance of the ArbitrationProcess class.
        /// </summary>
        /// <param name="peerHarness">The peer harness hosting this process.</param>
        public ArbitrationProcess(IPeerHarness peerHarness)
        {
            if (peerHarness == null)
            {
                throw new ArgumentNullException("peerHarness");
            }

            this.peerHarness = peerHarness;
        }

        /// <summary>
        /// Called by process host to initialize the process.
        /// </summary>
        /// <param name="arguments">Command line arguments passed by process host.</param>
        public void OnInitialize(ref string[] arguments)
        {
        }

        /// <summary>
        /// Called periodically by the process host, to trigger regularly scheduled processing.
        /// </summary>
        /// <param name="delayMilliseconds">Milliseconds since last called.</param>
        /// <returns>Flag indicating success or errors.</returns>
        public bool OnPerformRegularTasks(int delayMilliseconds)
        {
            return true;
        }

        /// <summary>
        /// Called by the process host to pass a request.
        /// Does nothing, but is required by interface.
        /// </summary>
        /// <param name="requestType">Type of the request (usually cast from enum)</param>
        /// <param name="request">The request that the process should know how to handle.</param>
        /// <returns>Response for host.</returns>
        public byte[] OnProcessRequest(int requestType, byte[] request)
        {
            return null;
        }

        /// <summary>
        /// Called on shutdown.
        /// </summary>
        public void OnShutdown()
        {
        }

        /// <summary>
        /// Called on start.
        /// </summary>
        /// <returns>True always.</returns>
        public bool OnStart()
        {
            this.arbitrator = new Arbitrator(this.peerHarness.NetworkFacadeInstance);

            // registering the ArbitrationEventHandler
            this.peerHarness.NetworkFacadeInstance.RegisterArbitrationHandler(
                this.arbitrator.HandleClientEvent,
                TimeSpan.FromMinutes(10),
                this.arbitrator.HandleClientDisconnect);
            Console.WriteLine("The arbitrator is in session.");
            return true;
        }

        /// <summary>
        /// Write a list of supported command line arguments.
        /// </summary>
        /// <param name="tw">The text writer to use.</param>
        public void WriteOptionsDescription(TextWriter tw)
        {
        }
    }
}