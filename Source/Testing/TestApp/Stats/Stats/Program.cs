﻿//-----------------------------------------------------------------------
// <copyright file="Program.cs" company="Scalify">
//     Copyright (c) 2013 Scalify. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Stats
{
    using System;
    using System.Collections.Generic;
    using System.Threading;
    using Mono.Options;

    /// <summary>
    /// An main class for this test application.
    /// </summary>
    internal class Program
    {
        /// <summary>
        /// Option set.
        /// </summary>
        private static OptionSet optionSet = new OptionSet()
            {
                { "h|help", "show this message and exit.",
                    v => WriteUsageAndExit() },
                { "life-time", "how long this application should running for (seconds)",
                    (int v) => lifeTime = v }
            };

        /// <summary>
        /// Application life time in seconds.
        /// </summary>
        private static int lifeTime;

        /// <summary>
        /// Application entry point.
        /// </summary>
        /// <param name="args">Command line arguments.</param>
        internal static void Main(string[] args)
        {
            List<string> extras;
            try
            {
                extras = optionSet.Parse(args);

                if (extras.Count != 0)
                {
                    Console.Out.WriteLine("Invalid command line argument.");
                    WriteUsageAndExit();
                }

                var network = new Network();
                var thread = new Thread(network.Run);
                thread.Start();

                if (lifeTime > 0)
                {
                    Thread.Sleep((int)TimeSpan.FromSeconds(lifeTime).TotalMilliseconds);
                }
                else
                {
                    while (true)
                    {
                        var key = Console.ReadKey();
                        if (key.KeyChar == 'x')
                        {
                            break;
                        }

                        Thread.Sleep(1000);
                    }
                }

                network.Stop();

                // give sometimes before the background thread is finished.
                thread.Join((int)TimeSpan.FromSeconds(5).TotalMilliseconds);
            }
            catch (Exception e)
            {
                Console.Out.WriteLine(string.Format("Exception: {0}", e.Message));
            }
        }

        /// <summary>
        /// Write option description and exit.
        /// </summary>
        private static void WriteUsageAndExit()
        {
            optionSet.WriteOptionDescriptions(Console.Out);
            Environment.Exit(0);
        }
    }
}
