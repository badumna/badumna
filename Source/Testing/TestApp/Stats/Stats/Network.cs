﻿//-----------------------------------------------------------------------
// <copyright file="Network.cs" company="Scalify">
//     Copyright (c) 2013 Scalify. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Stats
{
    using System;
    using Badumna;
    using Badumna.Core;
    using Badumna.Utilities;

    /// <summary>
    /// Network class.
    /// </summary>
    public class Network
    {
        /// <summary>
        /// Network facade instance.
        /// </summary>
        private INetworkFacade facade;

        /// <summary>
        /// Statistics tracker.
        /// </summary>
        private StatisticsTracker tracker;

        /// <summary>
        /// A value indicating whether the peer test is running.
        /// </summary>
        private bool isRunning;

        /// <summary>
        /// A value indicating whether the peer sending a payload to tracker server.
        /// </summary>
        private bool isTracking;

        /// <summary>
        /// Initializes a new instance of the <see cref="Network"/> class.
        /// </summary>
        public Network()
        {
            Options options = new Options();

            options.Connectivity.ApplicationName = "stats_server_test";

            NetworkFacade.BeginCreate(
                options,
                result =>
                {
                    try
                    {
                        this.facade = NetworkFacade.EndCreate(result);
                        this.Trace("Badumna initialization succeeded.");
                    }
                    catch (BadumnaException e)
                    {
                        this.Trace("Exception: {0}", e.Message);
                    }

                    this.facade.Login();
                    tracker = this.facade.CreateTracker("172.16.22.14", 21256, TimeSpan.FromSeconds(5), "initial");
                });
        }

        /// <summary>
        /// Run the main loop for network class.
        /// </summary>
        public void Run()
        {
            this.isRunning = true;

            while (this.isRunning)
            {
                if (this.facade != null && this.facade.IsLoggedIn)
                {
                    if (!this.isTracking && this.tracker != null)
                    {
                        this.Trace("Starting tracker.");
                        this.tracker.SetUserID(System.Diagnostics.Process.GetCurrentProcess().Id);
                        this.tracker.Start();
                        this.isTracking = true;
                    }

                    this.facade.ProcessNetworkState();
                }

                // Sleep for 33 ms
                System.Threading.Thread.Sleep(1000 / 30);
            }

            this.tracker.Stop();
            this.Shutdown();
        }

        /// <summary>
        /// Stop the peer.
        /// </summary>
        public void Stop()
        {
            this.isRunning = false;
        }

        /// <summary>
        /// Shutdown network.
        /// </summary>
        private void Shutdown()
        {
            this.Trace("Shutting down.");
            this.facade.Shutdown();
        }

        /// <summary>
        /// Trace message to console output.
        /// </summary>
        /// <param name="message">Trace messages.</param>
        /// <param name="args">Message arguments.</param>
        private void Trace(string message, params object[] args)
        {
            Console.Out.WriteLine(string.Format(message, args));
        }
    }
}
