﻿using System;
using System.Collections.Generic;
using System.Text;
using Badumna.Streaming;
using Badumna.Core;
using Badumna.Utilities;
using System.IO;
using System.Threading;

namespace Streaming
{
    class Program
    {
        static void GenerateBinaryFile(string filename)
        {
            File.Delete(filename);

            using (FileStream stream = new FileStream(filename, FileMode.Create))
            {
                using (BinaryWriter writer = new BinaryWriter(stream))
                {
                    for (int i = 0; i < 1024 * 128; i++)
                    {
                        writer.Write(i);
                    }
                    
                    writer.Close();
                }
            }
        }

        static void Main(string[] args)
        {
            StreamingTester streamTester;
            bool sender = false;
            if (args.Length > 0)
            {
                sender = true;
            }

            Thread.Sleep(25000);
            Console.WriteLine("sleep returned.");

            streamTester = new StreamingTester(sender);
            streamTester.Start();
            int numOfCompleted = 0;

            while (numOfCompleted < 2000)
            {
                string filename = StreamingTester.GetFileName(numOfCompleted);
                filename = Directory.GetCurrentDirectory() + Path.DirectorySeparatorChar + filename;
                Console.WriteLine("full path = {0}", filename);
                if (sender)
                {
                    Console.WriteLine("going to generate the file.");
                    GenerateBinaryFile(filename);
                    Console.WriteLine("file ready.");
                    streamTester.RequestToSendFile(filename);
                }

                Console.WriteLine("Going to call WaitUntilSendStarted - {0}", filename);
                streamTester.WaitUntilSendStarted(filename);
                Console.WriteLine("Going to call WaitUntilSendCompleted - {0}", filename);
                streamTester.WaitUntilSendCompleted(filename);
                Console.WriteLine("done - {0}.", filename);

                numOfCompleted++;
            }

            Console.WriteLine("All done! Please press ctrl+c to stop and exit.");
            while (true)
            {
                Thread.Sleep(10000);
            }

            streamTester.Shutdown();
        }
    }
}
