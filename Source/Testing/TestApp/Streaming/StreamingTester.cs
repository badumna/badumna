﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Threading;
using System.Threading;
using System.Timers;

using Badumna;
using Badumna.DataTypes;
using Badumna.Streaming;
using Badumna.Core;
using Badumna.Utilities;
using Badumna.SpatialEntities;
using System.IO;

namespace Streaming
{
    class StreamingTester
    {
        private DispatcherTimer mProcessTimer;
        private bool mIsLoggedIn;

        // sending peer or the reciving peer
        private bool mSender;

        private BadumnaId mId;
        // counter
        private int mCounter;

        private const string StreamTag = "Streaming-Tester-Stream-Tag-7890";
        private const string ReceiverIdVarName = "streaming_test_receiver_id_436";

        private System.Timers.Timer mTimer;
        private IStreamController mReceiveController;
        private IStreamController mSendController;
        private List<string> mStartedFilenames;
        private List<string> mCompletedFilenames;
        private INetworkFacade networkFacade;

        public StreamingTester(bool sender)
        {
            this.mSender = sender;
            this.mCounter = 0;
            this.mReceiveController = null;
            this.mSendController = null;
            this.mId = BadumnaId.GetNextUniqueId();

            this.mStartedFilenames = new List<string>();
            this.mCompletedFilenames = new List<string>();
        }

        public void Start()
        {
            this.networkFacade = NetworkFacade.Create(new Options("NetworkConfig.xml"));

            /*this.mProcessTimer = new DispatcherTimer();
            this.mProcessTimer.Interval = TimeSpan.FromSeconds(1.0 / 60.0);
            this.mProcessTimer.Tick += delegate { this.RegularProcessing(); };
            this.mProcessTimer.IsEnabled = true;
            */
            mTimer = new System.Timers.Timer();
            mTimer.Elapsed += new ElapsedEventHandler( this.RegularProcessing );
            mTimer.Interval = 30;
            mTimer.Start();

            this.Login();

            // let the reciving peer accept all incoming files.  
            if (this.mIsLoggedIn && !this.mSender)
            {
                string idString = this.mId.ToString();
                System.Environment.SetEnvironmentVariable(ReceiverIdVarName, idString, EnvironmentVariableTarget.Machine);
                Console.WriteLine("receiver has set the env variable named {0} to {1}", ReceiverIdVarName, idString);
                this.networkFacade.Streaming.SubscribeToReceiveStreamRequests(StreamTag, this.StreamingRequestHandler);
            }
        }

        public void RequestToSendFile(string filename)
        {
            string receiverId = System.Environment.GetEnvironmentVariable(ReceiverIdVarName, EnvironmentVariableTarget.Machine);
            if (receiverId == null || receiverId.Length == 0)
            {
                throw new InvalidOperationException("Couldn't determine the receiver id.");
            }

            BadumnaId id = BadumnaId.TryParse(receiverId);
            this.mSendController = this.networkFacade.Streaming.BeginSendReliableFile(StreamTag, filename, id, "username", this.FileSendComplete, filename);
            this.mStartedFilenames.Add(filename);
        }

        public void Shutdown()
        {
            this.Logout();

            if (!this.mSender)
            {
                System.Environment.SetEnvironmentVariable(ReceiverIdVarName, "");
            }
        }

        public static string GetFileName(int count)
        {
            return string.Format("saved_streaming_tester_file_{0}", count);
        }

        public void WaitUntilSendStarted(string filename)
        {
            while (!this.mStartedFilenames.Contains(filename))
            {
                Thread.Sleep(1000);
            }
        }

        public void WaitUntilSendCompleted(string filename)
        {
            int counter = 0;
            while (!this.mCompletedFilenames.Contains(filename))
            {
                Thread.Sleep(1000);
                counter++;

                if (counter % 10 == 0 || counter != 0)
                {
                    if (this.mSendController == null)
                    {
                        continue;
                    }

                    long total = this.mSendController.BytesTotal;
                    long sent = this.mSendController.BytesTransfered;

                    Console.WriteLine("{0}: {1}%", filename, (double)sent / (double)total * 100.0);
                }
            }
        }

        private void StreamingRequestHandler(object sender, ReceiveStreamEventArgs args)
        {
            Console.WriteLine("StreamingRequestHandler is called. Going to accept the income file request.");
            string filename = GetFileName(this.mCounter);
            filename = Directory.GetCurrentDirectory() + Path.DirectorySeparatorChar + filename;
            File.Delete(filename);
            this.mReceiveController = args.AcceptFile(filename, this.FileAcceptComplete, filename);
            this.mStartedFilenames.Add(filename);
            this.mCounter++;
            Console.WriteLine("StreamingRequestHandler is going to return. {0} has been added to the start list.", filename);
        }

        private void FileAcceptComplete(IAsyncResult ar)
        {
            Console.WriteLine("FileAcceptComplete is called.");
            string filename = ar.AsyncState as string;
            if (filename != null)
            {
                Console.WriteLine("file accepted. filename = {0}", filename);
                this.mCompletedFilenames.Add(filename);
            }
            else
            {
                throw new InvalidCastException("failed to convert ar to filename string.");
            }
        }

        private void FileSendComplete(IAsyncResult ar)
        {
            Console.WriteLine("FileSendComplete is called.");
            string filename = ar.AsyncState as string;
            if (filename != null)
            {
                Console.WriteLine("file sent. filename = {0}", filename);
                this.mCompletedFilenames.Add(filename);
            }
            else
            {
                throw new InvalidCastException("failed to convert ar to filename string.");
            }
        }

        private void Login()
        {
            bool success = this.networkFacade.Login();
            if (success)
            {
                this.mIsLoggedIn = true;
            }
        }

        private void Logout()
        {
            this.networkFacade.Logout();
            this.mIsLoggedIn = false;
        }

        private void RegularProcessing(object source, ElapsedEventArgs args)
        {
            if (this.mIsLoggedIn)
            {
                this.networkFacade.ProcessNetworkState();
            }
        }
    }
}
