﻿namespace CppWrapperTestStub
{
    /// <summary>
    /// This is a stub class used in CppWrapper's unit tests. 
    /// </summary>
    public class SubClass : BaseClass
    {
        public SubClass()
            : base()
        {
        }

        public SubClass(int value1)
            : base(value1)
        {
        }

        public override string VirtualMethodReturnsStringName()
        {
            return "sub";
        }
    }
}
