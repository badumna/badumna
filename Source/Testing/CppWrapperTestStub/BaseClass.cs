﻿using System;

namespace CppWrapperTestStub
{
    /// <summary>
    /// This is a stub class used in CppWrapper's unit tests. 
    /// </summary>
    public class BaseClass
    {
        private int intProperty;
        private string stringProperty;

        public static readonly int constValueOneHundred = 100;

        public BaseClass()
        {
            this.intProperty = 0;
        }

        public BaseClass(int value1)
        {
            this.intProperty = value1;
        }

        public int PropertyInt
        {
            set { this.intProperty = value; }
            get { return this.intProperty; }
        }

        public string PropertyString
        {
            set { this.stringProperty = value; }
            get { return this.stringProperty; }
        }

        public static int StaticIntegerPropertyReturn100
        {
            get { return 100; }
        }

        public int PropertyIntThrowException
        {
            set
            {
                throw new InvalidOperationException("expected");
            }

            get
            {
                throw new InvalidOperationException("expected");
            }
        }

        public void VoidMethod()
        {
        }

        public bool BoolMethodReturnTrue()
        {
            return true;
        }

        public bool BoolMethodReturnInputValue(bool input)
        {
            return input;
        }

        static public bool StaticBoolMethodReturnInputValue(bool input)
        {
            return input;
        }

        public void VoidMethodThrowInvalidOperationException()
        {
            throw new InvalidOperationException("expected");
        }

        public bool BoolMethodThrowInvalidOperationException()
        {
            throw new InvalidOperationException("expected");
        }

        public virtual string VirtualMethodReturnsStringName()
        {
            return "base";
        }

        public byte[] GetByteArray(int size)
        {
            byte[] data = new byte[size];
            return data;
        }

        public bool CanGetHostName()
        {
            try
            {
                string hostname = System.Net.Dns.GetHostName();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }
    }
}
