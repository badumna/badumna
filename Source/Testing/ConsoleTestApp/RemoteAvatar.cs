﻿using System;
using System.Collections.Generic;
using System.Text;
using Badumna.SpatialEntities;
using System.IO;
using Badumna.DataTypes;

namespace ConsoleTestApp
{
    public class RemoteAvatar : Avatar, ISpatialReplica, IDeadReckonable
    {
        private Vector3 v;

        /// <summary>
        /// Initializes a new instance of the RemoteAvatar class.
        /// </summary>
        public RemoteAvatar()
        {
            this.v = new Vector3(0, 0, 0);
        }

        #region ISpatialReplica Members

        public void Deserialize(Badumna.Utilities.BooleanArray includedParts, System.IO.Stream stream, int estimatedMillisecondsSinceDeparture)
        {
            BinaryReader reader = new BinaryReader(stream);

            if (includedParts[(int)SpatialEntityStateSegment.Position])
            {
                Console.WriteLine("Position has been updated to {0}", this.Position.X);
            }

            if (includedParts[(int)StateSegment.Counter])
            {
                this.Counter = reader.ReadInt32();
                Console.WriteLine("Counter has been updated to {0}", this.Counter);
            }
        }

        #endregion

        #region IDeadReckonable Members

        public Badumna.DataTypes.Vector3 Velocity
        {
            get
            {
                return this.v;
            }
            set
            {
                this.v = value;
            }
        }

        public void AttemptMovement(Badumna.DataTypes.Vector3 reckonedPosition)
        {
            this.Position = reckonedPosition;
        }

        #endregion
    }
}
