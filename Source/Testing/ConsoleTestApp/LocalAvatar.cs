﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Badumna;
using Badumna.SpatialEntities;
using System.IO;
using Badumna.DataTypes;

namespace ConsoleTestApp
{
    public class LocalAvatar : Avatar, ISpatialOriginal, IDeadReckonable
    {
        private Vector3 v;
        private INetworkFacade networkFacade;

        public LocalAvatar(INetworkFacade networkFacade)
        {
            this.v = new Vector3(0.1f, 0, 0);
            this.networkFacade = networkFacade;
        }

        #region ISpatialOriginal Members

        public void Serialize(Badumna.Utilities.BooleanArray requiredParts, System.IO.Stream stream)
        {
            BinaryWriter writer = new BinaryWriter(stream);

            if (requiredParts[(int)StateSegment.Counter])
            {
                writer.Write(this.Counter);
            }
        }

        public void IncreaseCounter()
        {
            this.Counter = this.Counter + 1;
        }

        protected override void OnCounterUpdate()
        {
            this.networkFacade.FlagForUpdate(this, (int)StateSegment.Counter);
        }

        protected override void OnPositionUpdate()
        {
            this.networkFacade.FlagForUpdate(this, (int)SpatialEntityStateSegment.Position);
        }

        #endregion

        #region IDeadReckonable Members

        public Badumna.DataTypes.Vector3 Velocity
        {
            get
            {
                return this.v;    
            }
            set
            {
                this.v = value;
            }
        }

        public void AttemptMovement(Badumna.DataTypes.Vector3 reckonedPosition)
        {
        }

        #endregion
    }
}