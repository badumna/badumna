﻿using System;
using System.Collections.Generic;
using System.Text;

using Badumna.DataTypes;
using Badumna.SpatialEntities;
using System.IO;

namespace ConsoleTestApp
{
    /// <summary>
    /// Enumeration defining serialization elements.
    /// </summary>
    public enum StateSegment : int
    {
        /// <summary>
        /// The update counter.
        /// </summary>
        Counter = SpatialEntityStateSegment.FirstAvailableSegment,
    }

    public class Avatar : ISpatialEntity
    {
        private int counter;

        /// <summary>
        /// Position of the avatar.
        /// </summary>
        private Vector3 position = new Vector3(0f, 0f, 0f);

        /// <summary>
        /// Gets or sets the avatar's unique Badumna id.
        /// </summary>
        public BadumnaId Guid { get; set; }

        /// <summary>
        /// Gets or sets the avatar's radius.
        /// </summary>
        public float Radius { get; set; }

        /// <summary>
        /// Gets or sets the avatar's radius of its area of interest.
        /// </summary>
        public float AreaOfInterestRadius { get; set; }

        /// <summary>
        /// Handle events.
        /// </summary>
        /// <param name="stream">Current stream.</param>
        public void HandleEvent(Stream stream)
        {
        }

        /// <summary>
        /// Update method.
        /// </summary>
        public virtual void Update()
        {
            // Do nothing
        }


        #region ISpatialEntity Members

        public Vector3 Position
        {
            get
            {
                return position;
            }
            set
            {
                this.position = value;
                this.OnPositionUpdate();
            }
        }

        #endregion

        public int Counter
        {
            get
            {
                return this.counter;
            }
            set
            {
                this.counter = value;
                this.OnCounterUpdate();
            }
        }

        protected virtual void OnPositionUpdate()
        {
        }

        protected virtual void OnCounterUpdate()
        {
        }
    }
}
