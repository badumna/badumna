﻿using System;
using System.Collections.Generic;
using System.Text;
using Badumna;
using System.Threading;
using Badumna.SpatialEntities;
using Badumna.DataTypes;

namespace ConsoleTestApp
{
    /// <summary>
    /// Enumeration defining types of entities in the game.
    /// </summary>
    public enum EntityType : uint
    {
        /// <summary>
        /// Entity is an avatar.
        /// </summary>
        Avatar,
    }

    public class ConsoleApp
    {
        private Timer timer;

        private LocalAvatar localAvatar;

        private List<RemoteAvatar> remoteAvatar = new List<RemoteAvatar>();

        private NetworkScene scene;

        private INetworkFacade networkFacade;

        public void Run()
        {
            bool success = this.InitializeBadumna();
            if (!success)
            {
                Console.WriteLine("Failed to initialize the badumna engine.");

                Environment.Exit(1);
            }

            TimerCallback callback = new TimerCallback(this.ProcessNetworkStatus);
            timer = new Timer(callback, null, 0, 50);

            // hook up some code to handle network address changed event.
            this.networkFacade.AddressChangedEvent += this.NetworkAddressChangedHandler;

            // Badumna specific: Join the network scene
            this.scene = this.networkFacade.JoinScene("console_test_app_scene", this.CreateSpatialReplica, this.RemoveSpatialReplica);

            this.localAvatar = new LocalAvatar(this.networkFacade);
            this.localAvatar.Radius = 100.0f;
            this.localAvatar.AreaOfInterestRadius = 100.0f;
            this.scene.RegisterEntity(this.localAvatar, (uint)EntityType.Avatar);
            this.localAvatar.Position = new Vector3(0, 0, 0);
            this.localAvatar.Velocity = new Vector3(0.1f, 0.0f, 0.0f);
            

            // enter into the main loop.
            this.MainLoop();
        }

        private bool InitializeBadumna()
        {
            Options options = new Options();

            options.Connectivity.ApplicationName = "Badumna_Console_App";
            options.Connectivity.StartPortRange = 22850;
            options.Connectivity.EndPortRange = 22859;
            options.Connectivity.BroadcastPort = 22890;
            options.Connectivity.IsBroadcastEnabled = true;

            options.Logger.LoggerType = LoggerType.DotNetTrace;
            options.Logger.LoggerConfig =
                @"<Logger Type=""DotNetTraceLogger"">
                     <Verbosity>Verbose</Verbosity>
                     <TraceFile Enabled=""true"" />
                  </Logger>";

            this.networkFacade = NetworkFacade.Create(options);
            bool success = this.networkFacade.Login();

            Console.WriteLine(this.networkFacade.GetNetworkStatus().ToString());

            return success;
        }

        private void ProcessNetworkStatus(object o)
        {
            if (NetworkFacade.IsInstantiated && this.networkFacade.IsLoggedIn)
            {
                this.networkFacade.ProcessNetworkState();
            }
        }

        public ISpatialReplica CreateSpatialReplica(NetworkScene scene, BadumnaId entityId, uint entityType)
        {
            foreach (RemoteAvatar avatar in this.remoteAvatar)
            {
                if (avatar.Guid.Equals(entityId))
                {
                    return null;
                }
            }

            RemoteAvatar remoteAvatar = new RemoteAvatar();
            remoteAvatar.Guid = entityId;
            this.remoteAvatar.Add(remoteAvatar);

            Console.WriteLine("a remote replica has been created, id = {0}", entityId);

            return remoteAvatar;
        }

        /// <summary>
        /// Removes remote avatar from the avatar list.
        /// </summary>
        /// <param name="scene">Current scene.</param>
        /// <param name="replica">Spatial replica to remove.</param>
        public void RemoveSpatialReplica(NetworkScene scene, ISpatialReplica replica)
        {
            RemoteAvatar remoteAvatar = replica as RemoteAvatar;

            if (remoteAvatar != null)
            {
                for (int i = 0; i < this.remoteAvatar.Count; i++)
                {
                    if (remoteAvatar.Guid.Equals(this.remoteAvatar[i].Guid))
                    {
                        this.remoteAvatar.RemoveAt(i);
                        Console.WriteLine("a remote replica has been removed.");
                        break;
                    }
                }
            }
        }

        private void MainLoop()
        {
            Console.WriteLine("Press i to increase the counter or x to exit.");

            while (true)
            {
                ConsoleKeyInfo keyinfo = Console.ReadKey();
                if (keyinfo.KeyChar == 'x')
                {
                    break;
                }
                else if (keyinfo.KeyChar == 'j')
                {
                    Console.WriteLine("Going to unregister and register entity.");
                    this.localAvatar.Velocity = new Vector3(0, 0, 0);
                    this.scene.UnregisterEntity(this.localAvatar);
                    this.localAvatar.Position = new Vector3(0, 0, 0);
                    this.scene.RegisterEntity(this.localAvatar, (uint)EntityType.Avatar);
                    this.localAvatar.Velocity = new Vector3(0.1f, 0, 0);
                }
                else if (keyinfo.KeyChar == 'm')
                {
                    Vector3 positionChange = new Vector3();
                    positionChange.X = 1.0f;
                    this.localAvatar.Position += positionChange;
                }
                else if (keyinfo.KeyChar == 'i')
                {
                    if (this.localAvatar != null)
                    {
                        this.localAvatar.IncreaseCounter();
                    }
                }
            }

            Console.WriteLine("Going to exit.");

            this.Shutdown();
        }

        private void Shutdown()
        {
            if (this.scene != null)
            {
                if (this.localAvatar != null)
                {
                    this.scene.UnregisterEntity(this.localAvatar);
                }

                // Badumna specific: leave the scene
                this.scene.Leave();
                this.scene = null;
            }

            // Badumna specific: log out from and shut down the network
            this.networkFacade.Logout();
            this.networkFacade.Shutdown(true);
        }

        private void NetworkAddressChangedHandler()
        {
            Console.Error.WriteLine("Network address has changed. Time to press x and exit.");
        }
    }
}
