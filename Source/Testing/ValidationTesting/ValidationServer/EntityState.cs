﻿//-----------------------------------------------------------------------
// <copyright file="EntityState.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace ValidationServer
{
    using System;
    using Badumna.Validation;

    /// <summary>
    /// Demo EntityState for testing.
    /// </summary>
    [Serializable]
    internal class EntityState : ISerializedEntityState
    {
        /// <summary>
        /// A value indicating whether the x coordinate is present.
        /// </summary>
        private bool hasX;

        /// <summary>
        /// A value indicating whether the y coordinate is present.
        /// </summary>
        private bool hasY;

        /// <summary>
        /// The x position coordinate.
        /// </summary>
        private float x;

        /// <summary>
        /// The y position coordinate.
        /// </summary>
        private float y;

        /// <summary>
        /// Initializes a new instance of the EntityState class.
        /// </summary>
        /// <param name="x">The x position coordinate, or null.</param>
        /// <param name="y">The y position coordinate, or null.</param>
        public EntityState(float? x, float? y)
        {
            if (x != null)
            {
                this.hasX = true;
                this.x = x.Value;
            }

            if (y != null)
            {
                this.hasY = true;
                this.y = y.Value;
            }
        }

        /// <summary>
        /// Gets the x position coordinate.
        /// </summary>
        public float X
        {
            get { return this.x; }
        }

        /// <summary>
        /// Gets the y position coordinate.
        /// </summary>
        public float Y
        {
            get { return this.y; }
        }

        /// <summary>
        /// Gets a value indicating whether the x coordinate is present.
        /// </summary>
        public bool HasX
        {
            get { return this.hasX; }
        }

        /// <summary>
        /// Gets a value indicating whether the y coordinate is present.
        /// </summary>
        public bool HasY
        {
            get { return this.hasY; }
        }

        /// <inheritdoc/>
        public void Update(ISerializedEntityState stateUpdate)
        {
            if (stateUpdate == null)
            {
                throw new ArgumentNullException("stateUpdate");
            }

            EntityState state = stateUpdate as EntityState;
            if (state == null)
            {
                throw new ArgumentException("stateUpdate must be instance of EntityState");
            }

            if (state.HasX)
            {
                this.hasX = true;
                this.x = state.x;
            }

            if (state.HasY)
            {
                this.hasY = true;
                this.y = state.y;
            }

            Console.WriteLine(DateTime.Now.ToString() + " - EntityState.Update(...)");
        }
    }
}
