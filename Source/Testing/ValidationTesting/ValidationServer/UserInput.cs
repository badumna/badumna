﻿//-----------------------------------------------------------------------
// <copyright file="UserInput.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace ValidationServer
{
    using System;
    using Badumna.Validation;

    /// <summary>
    /// Demo UserInput for testing.
    /// </summary>
    internal class UserInput : IUserInput
    {
        /// <summary>
        /// A value indicating whether the up key is currently pressed.
        /// </summary>
        private bool upPressed;

        /// <summary>
        /// A value indicating whether the down key is currently pressed.
        /// </summary>
        private bool downPressed;

        /// <summary>
        /// Initializes a new instance of the UserInput class.
        /// </summary>
        /// <param name="upPressed">A value indicating whether the up key is currently pressed.</param>
        /// <param name="downPressed">A value indicating whether the down key is currently pressed.</param>
        public UserInput(bool upPressed, bool downPressed)
        {
            this.upPressed = upPressed;
            this.downPressed = downPressed;
        }

        /// <summary>
        /// Gets a value indicating whether the up key is currently pressed.
        /// </summary>
        public bool UpPressed
        {
            get
            {
                return this.upPressed;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the down key is currently pressed.
        /// </summary>
        public bool DownPressed
        {
            get
            {
                return this.downPressed;
            }
        }

        /// <inheritdoc/>
        public byte[] ToBytes()
        {
            byte data = 0;
            if (this.upPressed)
            {
                data &= 1;
            }

            if (this.downPressed)
            {
                data &= 2;
            }

            return new byte[1] { data };
        }

        /// <inheritdoc/>
        public void FromBytes(byte[] bytes)
        {
            this.upPressed = (bytes[0] & 1) > 0;
            this.downPressed = (bytes[0] & 2) > 0;
        }
    }
}
