﻿//-----------------------------------------------------------------------
// <copyright file="ValidationServerProcess.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace ValidationServer
{
    using System;
    using Badumna;
    using Badumna.Validation;
    using Badumna.Validation.Allocation;
    using GermHarness;

    /// <summary>
    /// Hosted process to provide validation service to a Badumna network.
    /// </summary>
    internal class ValidationServerProcess : IHostedProcess
    {
        /// <summary>
        /// The time of the lsat tick.
        /// </summary>
        private DateTime? lastTickTime = null;

        /// <summary>
        /// A flag indicating whether validator allocation requests are accepted.
        /// </summary>
        private bool acceptRequests = false;

        /// <summary>
        /// A flag indicating whether an entity has been registered yet.
        /// </summary>
        private bool registeredEntity = false;

        /// <summary>
        ///  A local validated entity.
        /// </summary>
        private ValidatedEntity entity;

        /// <summary>
        /// An ID for the entity.
        /// </summary>
        private Guid entityId;

        /// <summary>
        /// Initializes a new instance of the ValidationServerProcess class.
        /// </summary>
        public ValidationServerProcess()
        {
        }

        /// <inheritdoc/>
        public void OnInitialize(ref string[] arguments)
        {
            // No op.
        }

        /// <inheritdoc/>
        public void OnShutdown()
        {
            // No op.
        }

        /// <inheritdoc/>
        public bool OnStart()
        {
            ////// registering the ArbitrationEventHandler
            ////NetworkFacade.Instance.RegisterArbitrationHandler(
            ////    this.server.HandleClientMessage,
            ////    TimeSpan.FromSeconds(60),
            ////    this.server.HandleClientDisconnect);
            ////Console.WriteLine("The validation server is ready.");
            return true;
        }

        /// <inheritdoc/>
        public bool OnPerformRegularTasks(int delayMilliseconds)
        {
            if (!this.registeredEntity)
            {
                if (Console.KeyAvailable)
                {
                    ConsoleKeyInfo keyInfo = Console.ReadKey(true);
                    if (keyInfo.Key == ConsoleKey.T)
                    {
                        this.acceptRequests = !this.acceptRequests;
                        NetworkFacade.Instance.ValidationFacade.AllocationDecisionMethod = () => { return this.acceptRequests; };
                        if (this.acceptRequests)
                        {
                            Console.WriteLine("Accepting requests");
                        }
                        else
                        {
                            Console.WriteLine("Rejecting requests");
                        }
                    }
                    else if (keyInfo.Key == ConsoleKey.R)
                    {
                        Console.WriteLine("Registering entity.");
                        this.entity = new ValidatedEntity();
                        this.entityId = Guid.NewGuid();
                        try
                        {
                            NetworkFacade.Instance.ValidationFacade.RegisterValidatedEntity(
                                this.entityId, this.entity);
                            this.registeredEntity = true;
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine("Exception: " + ex.Message);
                        }
                    }
                }
            }
            else
            {
                try
                {
                    if (this.entity.Status == EntityStatus.Validating)
                    {
                        DateTime currentTime = DateTime.Now;
                        TimeSpan interval;
                        if (this.lastTickTime.HasValue)
                        {
                            interval = currentTime - this.lastTickTime.Value;
                        }
                        else
                        {
                            interval = TimeSpan.FromMilliseconds(20.0);
                        }

                        this.lastTickTime = currentTime;

                        var userInput = new byte[0];
                        ////Console.WriteLine(
                        ////    "Sending input: up={0}, down={1}",
                        ////    userInput.UpPressed,
                        ////    userInput.DownPressed);
                        NetworkFacade.Instance.ValidationFacade.UpdateEntity(
                            this.entityId,
                            interval,
                            userInput);
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Exception: " + ex.Message);
                }
            }

            // Trigger any regular processing here.
            return true;
        }

        /// <inheritdoc/>
        public byte[] OnProcessRequest(int requestType, byte[] request)
        {
            // Add any Control Center request handling here.
            return null;
        }

        /// <inheritdoc/>
        public void WriteOptionsDescription(System.IO.TextWriter tw)
        {
            // Add process-specific command line option descriptions here.
        }

        /// <summary>
        /// Handle validator allocation results.
        /// </summary>
        /// <param name="result">The result (success or timeout).</param>
        /// <param name="validatorAddress">The address of the assigned validator.</param>
        private void HandleAllocationReply(AllocationRequestResult result, string validatorAddress)
        {
            Console.WriteLine("Reply: result = {0}, address == {1}", result, validatorAddress);
        }

        /// <summary>
        /// Gets user input from Keyboard.
        /// </summary>
        /// <returns>The current user input.</returns>
        private UserInput GetUserInput()
        {
            bool upPressed = NativeKeyboard.IsKeyDown(KeyCode.Up);
            bool downPressed = NativeKeyboard.IsKeyDown(KeyCode.Down);
            return new UserInput(upPressed, downPressed);
        }
    }
}
