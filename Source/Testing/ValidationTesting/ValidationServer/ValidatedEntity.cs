﻿//-----------------------------------------------------------------------
// <copyright file="ValidatedEntity.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace ValidationServer
{
    using System;
    using Badumna.DataTypes;
    using Badumna.Validation;

    /// <summary>
    /// Demo ValidatedEntity for testing.
    /// </summary>
    internal class ValidatedEntity : IValidatedEntity
    {
        /// <summary>
        /// Speed at which the eneity moves in units per second.
        /// </summary>
        private const float MoveSpeedUnitsPerSecond = 2f;

        /// <summary>
        /// The x position coordinate.
        /// </summary>
        private float x;

        /// <summary>
        /// The y position coordinate.
        /// </summary>
        private float y;

        /// <summary>
        /// The old x position coordinate.
        /// </summary>
        private float oldX;

        /// <summary>
        /// The old y position coordinate.
        /// </summary>
        private float oldY;

        /// <summary>
        /// Initializes a new instance of the ValidatedEntity class.
        /// </summary>
        public ValidatedEntity()
        {
            Console.WriteLine(DateTime.Now.ToString() + " - Created entity");
            this.oldX = 0f;
            this.oldY = 0f;
            this.x = 0f;
            this.y = 0f;
        }

        /// <inheritdoc/>
        public Vector3 Position
        {
            get
            {
                return new Vector3(this.x, this.y, 0);
            }
            
            set
            {
                throw new NotImplementedException();
            }
        }

        /// <inheritdoc/>
        public float Radius { get; set; }

        /// <inheritdoc/>
        public float AreaOfInterestRadius { get; set; }

        /// <inheritdoc/>
        public BadumnaId Guid
        {
            get
            {
                throw new NotImplementedException();
            }
            
            set
            {
                throw new NotImplementedException();
            }
        }

        /// <inheritdoc/>
        public EntityStatus Status { get; set; }

        /// <inheritdoc/>
        public void RestoreState(ISerializedEntityState state)
        {
            EntityState entityState = state as EntityState;
            this.x = entityState.X;
            this.y = entityState.Y;

            Console.WriteLine(DateTime.Now.ToString() + " - ValidatedEntity.RestoreState(...)");
        }

        /// <inheritdoc/>
        public ISerializedEntityState SaveState()
        {
            Console.WriteLine(DateTime.Now.ToString() + " - ValidatedEntity.SaveState(...)");
            return new EntityState(this.x, this.y);
        }

        /// <inheritdoc/>
        public void Update(TimeSpan timeInterval, byte[] input, UpdateMode mode)
        {
            ////float deltaSeconds = (float)timeInterval.TotalSeconds;
            ////UserInput userInput = input as UserInput;
            ////if (userInput.DownPressed)
            ////{
            ////    this.y -= MoveSpeedUnitsPerSecond * deltaSeconds;
            ////}
            ////else if (userInput.UpPressed)
            ////{
            ////    this.y += MoveSpeedUnitsPerSecond * deltaSeconds;
            ////}

            ////Console.WriteLine(DateTime.Now.ToString() + " - ValidatedEntity.Update(...)");
        }

        /// <inheritdoc/>
        public ISerializedEntityState CreateUpdate()
        {
            Console.WriteLine(DateTime.Now.ToString() + " - ValidatedEntity.GetUpdate(...)");
            float? x = (this.x != this.oldX) ? this.x : (float?)null;
            float? y = (this.y != this.oldY) ? this.y : (float?)null;
            this.oldX = this.x;
            this.oldY = this.y;
            return new EntityState(x, y);
        }

        /// <inheritdoc/>
        public void Serialize(Badumna.Utilities.BooleanArray requiredParts, System.IO.Stream stream)
        {
            throw new NotImplementedException();
        }

        /// <inheritdoc/>
        public void Deserialize(Badumna.Utilities.BooleanArray includedParts, System.IO.Stream stream, int estimatedMillisecondsSinceDeparture)
        {
            throw new NotImplementedException();
        }

        /// <inheritdoc/>
        public void HandleEvent(System.IO.Stream stream)
        {
            throw new NotImplementedException();
        }

        /// <inheritdoc/>
        public ISerializedEntityState MergeStateUpdates(ISerializedEntityState current, ISerializedEntityState update)
        {
            throw new NotImplementedException();
        }
    }
}
