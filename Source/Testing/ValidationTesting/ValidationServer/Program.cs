﻿//-----------------------------------------------------------------------
// <copyright file="Program.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2011 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace ValidationServer
{
    using System;
    using System.Collections.Generic;
    using Badumna;
    using GermHarness;
    using Mono.Options;

    /// <summary>
    /// Program class to hold Main method entry point.
    /// </summary>
    internal class Program
    {
        /// <summary>
        /// The peer harness for hosting the process and communicating with Control Center.
        /// </summary>
        private static PeerHarness peerHarness = new PeerHarness(new Options("NetworkConfig.xml"));

        /// <summary>
        /// The arbitration process hosted by the peer harness.
        /// </summary>
        private static ValidationServerProcess process = new ValidationServerProcess();

        /// <summary>
        /// The option set for parsing command line arguments.
        /// </summary>
        private static OptionSet optionSet = new OptionSet()
            {
                { "h|help", "show this message and exit.", v =>
                    {
                        if (v != null)
                        {
                            Program.WriteUsageAndExit();
                        }
                    }
                }
            };

        /// <summary>
        /// Entry point to program.
        /// Creates a PeerHarness and uses it to run the arbitration process.
        /// Command line arguments accepted: [--db_config=database] [--dei_config=deiConfig]
        /// </summary>
        /// <param name="arguments">Command line arguments.</param>
        internal static void Main(string[] arguments)
        {
            // Parse program-specific command line arguments.
            List<string> extras = null;
            try
            {
                extras = optionSet.Parse(arguments);

                string[] harnessArgs = extras.ToArray();
                if (peerHarness.Initialize(ref harnessArgs, process))
                {
                    if (harnessArgs.Length > 0)
                    {
                        string assemblyName = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name;
                        Console.Write("{0}: Unknown argument(s):", assemblyName);
                        foreach (string arg in harnessArgs)
                        {
                            Console.Write(" " + arg);
                        }

                        Console.WriteLine(".");
                        Console.WriteLine("Try `{0} --help' for more information.", assemblyName);
                    }
                    else
                    {
                        // Start the arbitration process.
                        peerHarness.Start();
                    }
                }
                else
                {
                    Console.WriteLine("Could not initialize peer harness.");
                }
            }
            catch (OptionException ex)
            {
                string assemblyName = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name;
                Console.Write("{0}: ", assemblyName);
                Console.WriteLine(ex.Message);
                Console.WriteLine("Try `{0} --help' for more information.", assemblyName);
            }
        }

        /// <summary>
        /// Writes the usage.
        /// </summary>
        private static void WriteUsageAndExit()
        {
            string assemblyName = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name;
            Console.WriteLine("Usage: {0} [Options]", assemblyName);
            Console.WriteLine("Start a Buddy List Arbitration Server on the local machine.");
            Console.WriteLine(string.Empty);
            Console.WriteLine("Options:");
            peerHarness.WriteOptionDescriptions(Console.Out);
            Program.optionSet.WriteOptionDescriptions(Console.Out);
            Environment.Exit(0);
        }
    }
}
