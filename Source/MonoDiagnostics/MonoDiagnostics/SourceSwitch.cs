﻿using System;

namespace System.Diagnostics
{
    public class SourceSwitch
    {
        // Summary:
        //     Initializes a new instance of the System.Diagnostics.SourceSwitch class,
        //     specifying the name of the source.
        //
        // Parameters:
        //   name:
        //     The name of the source.
        public SourceSwitch(string name) { }

        // Summary:
        //     Gets or sets the level of the switch.
        //
        // Returns:
        //     One of the System.Diagnostics.SourceLevels values that represents the event
        //     level of the switch.
        public SourceLevels Level { get; set; }
    }
}
