﻿//----------------------------------------------------------
// <copyright file="MonoSourceLevels.cs" company="NICTA">
// Copyright (c)2012 All Right Reserved
// </copyright>
// <summary>A Mono copy of System.Diagnositcs.SourceLevels</summary>
//----------------------------------------------------------
using System;
using System.ComponentModel;

namespace System.Diagnostics
{
    /// <summary>
    ///     Specifies the levels of trace messages filtered by the source switch and
    ///     event type filter.
    /// </summary>
    [Flags]
    public enum SourceLevels
    {
        /// <summary>
        ///     Allows all events through.
        /// </summary>
        All = -1,

        /// <summary>
        ///     Does not allow any events through.
        /// </summary>
        Off = 0,

        /// <summary>
        ///     Allows only System.Diagnostics.TraceEventType.Critical events through.
        /// </summary>
        Critical = 1,

        /// <summary>
        ///     Allows System.Diagnostics.TraceEventType.Critical and System.Diagnostics.TraceEventType.Error
        /// </summary>
        Error = 3,

        /// <summary>
        ///     Allows System.Diagnostics.TraceEventType.Critical, System.Diagnostics.TraceEventType.Error,
        ///     and System.Diagnostics.TraceEventType.Warning events through.
        /// </summary>
        Warning = 7,

        /// <summary>
        ///     Allows System.Diagnostics.TraceEventType.Critical, System.Diagnostics.TraceEventType.Error,
        ///     System.Diagnostics.TraceEventType.Warning, and System.Diagnostics.TraceEventType.Information
        ///     events through.
        /// </summary>
        Information = 15,

        /// <summary>
        ///     Allows System.Diagnostics.TraceEventType.Critical, System.Diagnostics.TraceEventType.Error,
        ///     System.Diagnostics.TraceEventType.Warning, System.Diagnostics.TraceEventType.Information,
        ///     and System.Diagnostics.TraceEventType.Verbose events through.
        /// </summary>
        Verbose = 31,

        /// <summary>
        ///     Allows the System.Diagnostics.TraceEventType.Stop, System.Diagnostics.TraceEventType.Start,
        ///     System.Diagnostics.TraceEventType.Suspend, System.Diagnostics.TraceEventType.Transfer,
        ///     and System.Diagnostics.TraceEventType.Resume events through.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Advanced)]
        ActivityTracing = 65280,
    }
}