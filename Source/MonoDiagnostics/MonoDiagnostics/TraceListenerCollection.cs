﻿using System;

namespace System.Diagnostics
{
    public class TraceListenerCollection
    {
        //
        // Summary:
        //     Clears all the listeners from the list.
        public void Clear() { }
        // Summary:
        //     Adds a System.Diagnostics.TraceListener to the list.
        //
        // Parameters:
        //   listener:
        //     A System.Diagnostics.TraceListener to add to the list.
        //
        // Returns:
        //     The position at which the new listener was inserted.
        public int Add(TraceListener listener) { return 0; }
        //
        // Summary:
        //     Removes from the collection the specified System.Diagnostics.TraceListener.
        //
        // Parameters:
        //   listener:
        //     A System.Diagnostics.TraceListener to remove from the list.
        public void Remove(TraceListener listener) { }

    }
}
