﻿using System;
using System.Collections;
using System.Reflection;

namespace System.Collections.Specialized
{
    // Summary:
    //     Represents an indexed collection of key/value pairs.
    public interface IOrderedDictionary : IDictionary, ICollection, IEnumerable
    {
        object this[int index] { get; set; }
        new IDictionaryEnumerator GetEnumerator();
        void Insert(int index, object key, object value);
        void RemoveAt(int index);
    }
}
