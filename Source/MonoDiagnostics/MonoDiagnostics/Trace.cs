﻿using System;

namespace System.Diagnostics
{
    public sealed class Trace
    {
        //
        // Summary:
        //     Writes an error message to the trace listeners in the System.Diagnostics.Trace.Listeners
        //     collection using the specified array of objects and formatting information.
        //
        // Parameters:
        //   format:
        //     A format string that contains zero or more format items, which correspond
        //     to objects in the args array.
        //
        //   args:
        //     An object array containing zero or more objects to format.
        [Conditional("TRACE")]
        public static void TraceError(string format, params object[] args)
        {
        }
        //
        // Summary:
        //     Writes a warning message to the trace listeners in the System.Diagnostics.Trace.Listeners
        //     collection using the specified array of objects and formatting information.
        //
        // Parameters:
        //   format:
        //     A format string that contains zero or more format items, which correspond
        //     to objects in the args array.
        //
        //   args:
        //     An object array containing zero or more objects to format.
        [Conditional("TRACE")]
        public static void TraceWarning(string format, params object[] args)
        {
        }

        //
        // Summary:
        //     Writes an informational message to the trace listeners in the System.Diagnostics.Trace.Listeners
        //     collection using the specified array of objects and formatting information.
        //
        // Parameters:
        //   format:
        //     A format string that contains zero or more format items, which correspond
        //     to objects in the args array.
        //
        //   args:
        //     An object array containing zero or more objects to format.
        [Conditional("TRACE")]
        public static void TraceInformation(string format, params object[] args)
        {
        }
    }
}
