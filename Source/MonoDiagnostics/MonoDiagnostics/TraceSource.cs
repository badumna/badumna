﻿using System;

namespace System.Diagnostics
{
    public class TraceSource
    {
        //
        // Summary:
        //     Initializes a new instance of the System.Diagnostics.TraceSource class, using
        //     the specified name for the source and the default source level at which tracing
        //     is to occur.
        //
        // Parameters:
        //   name:
        //     The name of the source, typically the name of the application.
        //
        //   defaultLevel:
        //     A bitwise combination of the System.Diagnostics.SourceLevels values that
        //     specifies the default source level at which to trace.
        //
        // Exceptions:
        //   System.ArgumentNullException:
        //     name is null.
        //
        //   System.ArgumentException:
        //     name is an empty string ("").
        public TraceSource(string name, SourceLevels defaultLevel)
        {
        }

        //
        // Summary:
        //     Gets the collection of trace listeners for the trace source.
        //
        // Returns:
        //     A System.Diagnostics.TraceListenerCollection that contains the active trace
        //     listeners associated with the source.
        public TraceListenerCollection Listeners { get { return new TraceListenerCollection(); } }

        //
        // Summary:
        //     Gets or sets the source switch value.
        //
        // Returns:
        //     A System.Diagnostics.SourceSwitch object representing the source switch value.
        //
        // Exceptions:
        //   System.ArgumentNullException:
        //     System.Diagnostics.TraceSource.Switch is set to null.
        public SourceSwitch Switch { get; set; }

        //
        // Summary:
        //     Writes a trace event message to the trace listeners in the System.Diagnostics.TraceSource.Listeners
        //     collection using the specified event type, event identifier, and message.
        //
        // Parameters:
        //   eventType:
        //     One of the System.Diagnostics.TraceEventType values that specifies the event
        //     type of the trace data.
        //
        //   id:
        //     A numeric identifier for the event.
        //
        //   message:
        //     The trace message to write.
        //
        // Exceptions:
        //   System.ObjectDisposedException:
        //     An attempt was made to trace an event during finalization.
        [Conditional("TRACE")]
        public void TraceEvent(TraceEventType eventType, int id, string message) { }

    }
}
