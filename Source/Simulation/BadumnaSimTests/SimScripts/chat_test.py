﻿
from pysim import push_repeat, line_graph, validate_stats
from ScriptSim import Sim
from NetworkSimulator import ChatTestStatsCollector
from agent_tests import ChatPeerInitializer, initialize_run

chat_messages_received=[]

def start_chat_peer(peer, peer_name):
    if peer_name == "Node7":
        peer.InitiateChatWith("Node6")

def stop_chat_peer(peer, peer_name):
    if peer_name == "Node7" or peer_name == "Node6":
        #peer.AssertReceived(1)
        ChatTestStatsCollector.Instance.SetMessageReceived(peer_name, peer.GetNumberOfMessageReceived())
        ChatTestStatsCollector.Instance.SetMessageSent(peer_name, peer.GetNumberOfMessageSent())

def chat_test(num_peers, stack_type, peer_start_delay=5000, helper=None):
    peer_init = ChatPeerInitializer(stack_type, helper, start_chat_peer, stop_chat_peer)
    initialize_run(peer_init, 7, peer_start_delay)

peer_range = [7.0]
in_Bps = []
out_Bps = []


sim = Sim.Current
    
for num_peers in peer_range:
    sim.Reset()
    helper = DiagnosticsHelper() # TODO: Sim has to be reset before instantiating DiagnosticsHelper, otherwise time offsets are wrong.  Not sure what to do, but it should be nicer/shouldn't necessarly have to remember to call sim.Reset
    chat_test(num_peers, SimulatedStackType.Chat, 15000.0, helper=helper) 
    push_repeat(helper.GetStatistics, 1000)
    sim.RunFor(550000)   # TODO: Should really run the test until it settles, rather than for some arbitrary time