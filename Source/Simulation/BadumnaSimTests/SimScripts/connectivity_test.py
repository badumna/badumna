﻿from pysim import push, push_repeat, line_graph, validate_stats
from ScriptSim import Sim, ConnectivityPeer
from System import Console


#    enum NatType
#    {
#        Unknown = 0,
#        Blocked = 1,
#        Open = 2,
#        SymmetricFirewall = 3,
#        FullCone = 4,
#        SymmetricNat = 5,
#        RestrictedPort = 6,
#        RestrictedCone = 7,
#        MangledFullCone = 8,
#        Internal = 9,
#        HashAddressable = 10,
#    }

nat_names = [ "Unknown", "Blocked", "Open", "SymmetricFirewall", "FullCone", "SymmetricNat", "RestrictedPort", "RestrictedCone", "MangledFullCone", "Internal" ]

# TODO: should change back to all nat types.
#nats=[2,4,5,6,7,8]
nats=[2,4]
nat_pairs_to_test=[]
message_length=600

def make_nat_pairs():
    for i in nats:
        for j in nats:
            nat_pairs_to_test.append([i,j])
 
class ConnectivityPeerInitializer: 
    def __init__(self, helper, end_time, start_peer, stop_peer, test_name):
        self._stack_type = SimulatedStackType.TransportOnly
        self._helper = helper
        self._new_peer_function = start_peer 
        self._end_peer_function = stop_peer
        self._end_time = end_time
        self._test_name = test_name
        self._peer_list = []
    
    def initialize(self, sender, init_args):
        facade = init_args.SimulatorFacade
        facade.CreateStack(self._stack_type)
        peer = ConnectivityPeer(facade, init_args.NodeName)
        self._peer_list.append(peer)
        if self._helper is not None:
            self._helper.Add(peer)
        peer.Start() # this should be scheduled!  and, what does uptime mean? means the *node* goes down after certain time?
        if self._new_peer_function is not None:
            push(10000, self._new_peer_function, self._peer_list, init_args.NodeName)
        if self._end_peer_function is not None:
            push(self._end_time, self._end_peer_function, self._peer_list, init_args.NodeName, self._test_name)
        

def initialize_connectivity_run(peer_start_delay, nat_pair, stop_time, start_function, stop_function, domains, reliability=1.0, latency_variation=0.0, helper=None):
    test_name = "Connectivity test between " + nat_names[nat_pair[0]] + " and " + nat_names[nat_pair[1]] + " with reliability " + str(reliability) + " and latency variation " + str(latency_variation)
    peer_initializer = ConnectivityPeerInitializer(helper, stop_time, start_function, stop_function, test_name).initialize
    
    ni = NetworkInitializer()
    ni.SetDefaults()
    ni.Topology.HasFullConectivity = False
    #ni.Topology.MessageTracer = Sim.Current.MessageTracer;
    ni.Reliability = reliability
    ni.LatencyVariation = latency_variation 
    ni.HairpinProbability = 0.0
    ni.Topology.Load("ConnectivityTopology.txt")
    ni.Reset()  # here because InitializeNode*s* calls it, and want to make sure we have the same behaviour in both cases
    
    Sim.Current.RepeatGetStatistics(1000)
    
    push(0, ni.InitializeNode, peer_initializer, 0, 0)
    ni.Topology.SetDomainsNat(0, 2) # Set to Open
    
    push(peer_start_delay, ni.InitializeNode, peer_initializer, domains[0], 1)
    ni.Topology.SetDomainsNat(domains[0], nat_pair[0])
    
    push(peer_start_delay * 2, ni.InitializeNode, peer_initializer, domains[1], 2)
    ni.Topology.SetDomainsNat(domains[1], nat_pair[1])
    
def send_test(start_function, stop_function, domains=[1,2], reliability=1.0, latency_variation=0.0):
    sim = Sim.Current
    
    init_delay = 15000 # 15 secs
    
    for nat_pair in nat_pairs_to_test:
        sim.Reset()
        stop_time = 240000
       
        initialize_connectivity_run(init_delay, nat_pair, stop_time, start_function, stop_function, domains, reliability, latency_variation, DiagnosticsHelper())
        sim.RunFor(stop_time + init_delay*3)   # TODO: Should really run the test until it settles, rather than for some arbitrary time
    

def start_send_one_way(peers, peer_name):
    if peer_name == "Node2":
        peers[2].SendMessageTo(peers[1].PeerId, message_length)

def stop_send_one_way(peers, peer_name, test_name):
    if peer_name == "Node1":
        peers[1].AssertReceived(1)
        Console.WriteLine(test_name + " Complete ")

def start_send_two_way(peers, peer_name):
    if peer_name == "Node2":
        peers[2].SendMessageTo(peers[1].PeerId, message_length)
        peers[1].SendMessageTo(peers[2].PeerId, message_length)

def stop_send_two_way(peers, peer_name, test_name):
    if peer_name == "Node1":
        peers[1].AssertReceived(1)
    if peer_name == "Node2":
        peers[2].AssertReceived(1)
        Console.WriteLine(test_name + " Complete ")

make_nat_pairs()
reliabilities_to_test=[0.95]
latency_variations=[0.25]

def run_test():
    for i in range(0,1):
        Console.WriteLine("--- Round " + str(i) + " ---")
        for reliability in reliabilities_to_test:
            Console.WriteLine("--- Reliability " + str(reliability) + " ---")
            for latency_variation in latency_variations:
                Console.WriteLine("--- Latency variation " + str(latency_variation * 100) + "% ---")
                Console.WriteLine("--- One way  ---")
                send_test(start_send_one_way, stop_send_one_way, [1,2], reliability, latency_variation)
                Console.WriteLine("--- Two way  ---")
                send_test(start_send_two_way, stop_send_two_way, [1,2], reliability, latency_variation)
                Console.WriteLine("--- No connectivity  ---")
                Console.WriteLine("--- One way  ---")
                send_test(start_send_one_way, stop_send_one_way, [1,3], reliability, latency_variation) # no connectivity between domains 1 and 3 
                Console.WriteLine("--- Two way  ---")
                send_test(start_send_two_way, stop_send_two_way, [1,3], reliability, latency_variation)
                Console.WriteLine("--- No hairpin  ---")
                Console.WriteLine("--- One way  ---")
                send_test(start_send_one_way, stop_send_one_way, [2,2], reliability, latency_variation) # Hairpin test
                Console.WriteLine("--- Two way  ---")
                send_test(start_send_two_way, stop_send_two_way, [2,2], reliability, latency_variation)
                Console.WriteLine("--- One way connectivity  ---")
                Console.WriteLine("--- One way  ---")
                send_test(start_send_one_way, stop_send_one_way, [2,3], 0.9, 0.4) # one way connectivity 2 -> 3
                Console.WriteLine("--- Two way  ---")
                send_test(start_send_two_way, stop_send_two_way, [2,3], 0.9, 0.4)

run_test()
#send_test(start_send_one_way, stop_send_one_way, [2,3], 0.9, 0.4) # one way connectivity 2 -> 3
#send_test(start_send_two_way, stop_send_two_way, [2,3], 0.9, 0.4)