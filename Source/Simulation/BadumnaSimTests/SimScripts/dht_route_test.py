﻿import System
from ScriptSim import Sim, MainWindow, RunSettings
from Visualizer import SourceConfig
from NetworkSimulator import DhtPeer, DhtRouteStatsCollector
from dht_init import Initializer, ReportRunHelper, make_dht_report
from pysim import add_graph, push, push_repeat
from System import Console

class TestRunner:
    def __init__(self, dht_initializer, report_helper):
        self._test_message_number = 0
        self._dht_initializer = dht_initializer
        
    def begin_test(self, start_delay, interval):
        push(start_delay, self._start_loop, interval)

    def _message_arrival(self, route_properties):
        #Console.WriteLine("route time = {0}, # hops = {1}, avg hop time = {2}", route_properties.TotalRouteTime.TotalMilliseconds, route_properties.NumberOfHops, route_properties.AverageHopTime.TotalMilliseconds);
        #print(route_properties.TotalRouteTime)
        DhtRouteStatsCollector.Instance.Add(route_properties.NumberOfHops, route_properties.TotalRouteTime.TotalMilliseconds, route_properties.AverageHopTime.TotalMilliseconds)       
 
    def _start_loop(self, interval):
        push_repeat(self._route_test_message, interval)
    
    def _route_test_message(self):
        peer = self._dht_initializer.get_random_peer()
        if peer is not None:
            peer.RouteMessage("somewhere"+ str(self._test_message_number), "blahblah" + str(self._test_message_number), self._message_arrival)
            self._test_message_number = self._test_message_number + 1 # for randomization


if __name__ == '__main__':
    run_length_ms = 7200000
    sample_period_ms = 10000
    peer_start_delay = 10000

    for num_peers in [25]:
        run_settings = RunSettings("DhtTests", str(num_peers) + " peers", True) # output directory, testname, save statistics
        intitializer = Initializer(DhtPeer, SimulatedStackType.Dht)  
        
        #intitializer.set_node_lifetime(run_length_ms / 1000, 0) # Set the lifetime to the length of the simulation to ensure all peers remain online 
                                                                # for the entire time.
        intitializer.set_node_lifetime(3600, 3600, 1800, 1800)
         
        test_runner = TestRunner(intitializer, None)
        sim = intitializer.initialize_run(run_settings, num_peers, peer_start_delay, True, None, str(num_peers) + " peers")
        test_runner.begin_test(30000, 5000)
        sim.RunFor(run_length_ms)
