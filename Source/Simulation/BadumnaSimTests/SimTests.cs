﻿//-----------------------------------------------------------------------
// <copyright file="SimTests.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using Badumna.Core;
using Badumna.ServiceDiscovery;
using NetworkSimulator;
using NUnit.Framework;
using NetworkSimulator.Instrumentation;

namespace BadumnaTests.ConsoleSim
{
    [TestFixture]
    [DontPerformCoverage]
    public class SimTests
    {
        private ConsoleSimRunner simRunner;

        [SetUp]
        public void Initialize()
        {
            this.simRunner = new ConsoleSimRunner();
            this.simRunner.Initialize();
        }

        [Test]
        public void ServiceDiscoveryProducesHighSuccessRateUnderChurn()
        {
            this.simRunner.Run("SimScripts\\dht_service_discovery_test.py");
            Assert.AreEqual(this.simRunner.CompletedSimulationTests, 1);

            long numberOfQuery = ServiceDiscoveryStatsCollector.GetInstance(null).GetNumberOfQuery();
            long numberOfSuccessQuery = ServiceDiscoveryStatsCollector.GetInstance(null).GetNumberOfSuccessfulQuery();
            float successRate = (float)numberOfSuccessQuery / (float)numberOfQuery;
            Assert.IsTrue(successRate > 0.99f);

            double dhtAccessRate = ServiceDiscoveryStatsCollector.GetInstance(null).GetDhtAccessRate();
            System.Console.WriteLine("dht access rate is {0}", dhtAccessRate);
            Assert.IsTrue(dhtAccessRate < 0.001);
        }

        [Test, Ignore]
        public void StreamingWillSuccessfullyCompleteWithoutChurn()
        {
            this.simRunner.Run("SimScripts\\streaming_test.py");
            Assert.AreEqual(1, this.simRunner.CompletedSimulationTests);

            Assert.IsTrue(StreamingTestStatsCollector.Instance.StreamingTestPassed());
        }

        [Test]
        public void ChatMessagesCanBeSuccessfullyDelivered()
        {
            this.simRunner.Run("SimScripts\\chat_test.py");
            Assert.AreEqual(1, this.simRunner.CompletedSimulationTests);

            // message received
            Assert.AreEqual(1, ChatTestStatsCollector.Instance.GetMessageReceived("Node6"));
            Assert.AreEqual(1, ChatTestStatsCollector.Instance.GetMessageReceived("Node7"));

            // message sent
            Assert.AreEqual(1, ChatTestStatsCollector.Instance.GetMessageSent("Node6"));
            Assert.AreEqual(1, ChatTestStatsCollector.Instance.GetMessageSent("Node7"));

            ChatTestStatsCollector.Instance.Reset();
        }

        [Test, Ignore]
        public void CompleteConnectivityTestCanPass()
        {
            this.simRunner.Run("SimScripts\\connectivity_test.py");
            Assert.AreEqual(1, this.simRunner.CompletedSimulationTests);
        }

        [Test, Ignore]
        public void RandomWalkerEntityReplicationTest()
        {
            this.simRunner.Run("SimScripts\\random_walk.py");
            Assert.AreEqual(1, this.simRunner.CompletedSimulationTests);
        }

        [Test, Ignore]
        public void DhtRouteTest()
        {
            this.simRunner.Run("SimScripts\\dht_route_test.py");
            Assert.AreEqual(this.simRunner.CompletedSimulationTests, 1);

            float avgNumOfHops = DhtRouteStatsCollector.Instance.AverageHops;
            float avgRouteTime = DhtRouteStatsCollector.Instance.AverageRouteTime;
            float avgPerHopRouteTime = DhtRouteStatsCollector.Instance.AveragePerHopRouteTime;

            // just to ensure things did not go crazy. 
            Assert.IsTrue(avgNumOfHops <= 3.0f);
            Assert.IsTrue(avgNumOfHops > 0f);
            Assert.IsTrue(avgRouteTime < 1000.0f);
            Assert.IsTrue(avgPerHopRouteTime < 500.0f);

            DhtRouteStatsCollector.Instance.Reset();
        }

        [Test, Ignore]
        public void SimpleEntityReplicationTest()
        {
            this.simRunner.Run("SimScripts\\simple_replication_test.py");
            Assert.AreEqual(1, this.simRunner.CompletedSimulationTests);
        }
    }
}
