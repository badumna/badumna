﻿//-----------------------------------------------------------------------
// <copyright file="ConsoleSimRunner.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.IO;
using System.Reflection;
using System.Threading;
using System.Xml;
using Badumna.Utilities;
using ScriptSim;

namespace BadumnaTests.ConsoleSim
{
    internal class ConsoleSimRunner
    {
        public static readonly string ScriptDirectory;
        public static readonly string LibDirectory;

        private int completedSimulationTests;

        static ConsoleSimRunner()
        {
            ConsoleSimRunner.ScriptDirectory = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "SimScripts");
            ConsoleSimRunner.LibDirectory = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "Lib");
        }

        public int CompletedSimulationTests
        {
            get { return this.completedSimulationTests; }
        }

        public void Initialize()
        {
            JobRunner.ScriptDirectory = Path.Combine(Directory.GetCurrentDirectory(), "Scripts");
            JobRunner.LibDirectory = Path.Combine(Directory.GetCurrentDirectory(), "Lib");

            // initialize the random source
            NetworkSimulator.RandomSource.Initialize((int)DateTime.UtcNow.Ticks);

            // use the console sim please
            System.Environment.SetEnvironmentVariable(Sim.mSimTypeEnvironmentVariableName, Sim.mSimTypeConsoleSim);
            JobRunner.Create(null);

            XmlDocument config = new XmlDocument();
            using (TextReader reader = File.OpenText("NetworkConfig.xml"))
            {
                config.Load(reader);
            }
        }

        public bool Run(string scriptName)
        {
            if (scriptName == null)
            {
                throw new ArgumentException("scriptName");
            }

            this.completedSimulationTests = 0;

            Console.WriteLine("Going to run sim script = " + scriptName);

            DateTime startTime = DateTime.UtcNow;

            Action<JobRunner.ExecutionItem, Exception> failed = null;
            failed = delegate(JobRunner.ExecutionItem item, Exception e)
            {
                throw new Exception("sim failed.");
            };

            Action<JobRunner.ExecutionItem> completed = null;
            completed = delegate(JobRunner.ExecutionItem item)
            {
                Console.WriteLine("script {0} completed.", scriptName);
                this.completedSimulationTests++;
            };

            JobRunner.Create(null);
            JobRunner.Current.Execute(scriptName, completed, failed);

            while (JobRunner.Current.IsExecuting || !JobRunner.Current.QueueIsEmpty())
            {
                Thread.Sleep(TimeSpan.FromSeconds(1));
            }

            TimeSpan diff = DateTime.UtcNow - startTime;
            Console.WriteLine("Total simulation runtime : {0} seconds", diff.TotalSeconds);

            return true;
        }
    }
}
