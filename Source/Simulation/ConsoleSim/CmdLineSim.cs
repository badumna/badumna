﻿using System;
using System.IO;
using System.Windows;
using System.Xml;
using System.Threading;
using System.Reflection;

using Badumna;
using Badumna.Core;
using Badumna.ServiceDiscovery;
using NetworkSimulator;
using ScriptSim;

namespace ConsoleSim
{
    class CmdLineSim
    {
        static void Main(string[] args)
        {
            if (args.Length <= 0)
            {
                return;
            }
            DateTime startTime = DateTime.UtcNow;

            // initialize the random source
            RandomSource.Initialize((int)DateTime.UtcNow.Ticks);

            // use the console sim please
            System.Environment.SetEnvironmentVariable(Sim.mSimTypeEnvironmentVariableName, Sim.mSimTypeConsoleSim);

            JobRunner.LibDirectory = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "Lib");
            JobRunner.ScriptDirectory = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "Scripts");

            Console.WriteLine("script = " + args[0]);
            JobRunner.Create(null);

            Console.WriteLine("current dir = {0}", Directory.GetCurrentDirectory());

            XmlDocument config = new XmlDocument();
            try
            {
                using (TextReader reader = File.OpenText("NetworkConfig.xml"))
                {
                    config.Load(reader);
                }
            }
            catch (Exception)
            {
                Console.WriteLine("Could not read NetworkConfig.xml, using defaults.");
            }
            //config.Load(Application.GetContentStream(new Uri("/NetworkConfig.xml", UriKind.Relative)).Stream);
            Options options = new Options(config);
            NetworkFacade.Create(options);

            Action<JobRunner.ExecutionItem, Exception> failed = null;
            failed = delegate(JobRunner.ExecutionItem item, Exception e)
            {
                Console.WriteLine("script failed.");
            };

            Action<JobRunner.ExecutionItem> completed = null;
            completed = delegate(JobRunner.ExecutionItem item)
            {
                Console.WriteLine("script completed.");
            };

            string scriptFileName = args[0];
            JobRunner.Current.Execute(scriptFileName, completed, failed);

            while (JobRunner.Current.IsExecuting || !JobRunner.Current.QueueIsEmpty())
            {
                Thread.Sleep(TimeSpan.FromSeconds(1));
            }

            TimeSpan diff = DateTime.UtcNow - startTime;
            Console.WriteLine("Total simulation runtime : {0} seconds", diff.TotalSeconds);
        }
    }
}
