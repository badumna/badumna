﻿
import System
from ScriptSim import AgentPeer, ChatPeer, Sim, MainWindow
from NetworkSimulator import DiagnosticsHelper, NetworkInitializer
from Badumna.DataTypes import Vector3
from pysim import push, push_repeat, push_n_shot, graph_parse_bytes, graph_call_counts

class AgentPeerInitializer:
    def __init__(self, behaviour_factory, stack_type, position_seed, radius, interest_radius, start_zone_size, helper, payload_size=0):
        self._behaviour_factory = behaviour_factory
        self._stack_type = stack_type
        self._radius = radius
        self._interest_radius = interest_radius
        self._start_zone_size = start_zone_size
        self._payload_size = payload_size
        self._helper = helper
        self._random = System.Random(position_seed)
    
    def initialize(self, sender, init_args):
        facade = init_args.SimulatorFacade
        facade.CreateStack(self._stack_type)
        x = self._start_zone_size * (self._random.NextDouble() - 0.5) * 2.0
        y = self._start_zone_size * (self._random.NextDouble() - 0.5) * 2.0
        z = self._start_zone_size * (self._random.NextDouble() - 0.5) * 2.0
        start_position = Vector3(x, y, z)
        peer = AgentPeer(facade, self._radius, self._interest_radius, start_position, self._behaviour_factory(start_position),
            self._payload_size)
        if self._helper is not None:
            self._helper.Add(peer)
        peer.Start() # this should be scheduled!  and, what does uptime mean? means the *node* goes down after certain time?

class ChatPeerInitializer: # No agents just peers
    def __init__(self, stack_type, helper, new_peer_function=None, end_peer_function=None):
        self._stack_type = stack_type
        self._helper = helper
        self._new_peer_function = new_peer_function
        self._end_peer_function = end_peer_function
    
    def initialize(self, sender, init_args):
        facade = init_args.SimulatorFacade
        facade.CreateStack(self._stack_type)
        peer = ChatPeer(facade, init_args.NodeName)
        if self._helper is not None:
            self._helper.Add(peer)
        peer.Start() # this should be scheduled!  and, what does uptime mean? means the *node* goes down after certain time?
        if self._new_peer_function is not None:
            self._new_peer_function(peer, init_args.NodeName)
        if self._end_peer_function is not None:
            push(100000, self._end_peer_function, peer, init_args.NodeName)
 


def run_with_report(initializer, duration, sample_period_ms, report, run_title, graphs, settings, show_parse_bytes=True, show_call_counts=True):
    sim = Sim.Current
    sim.Reset(settings)

    initializer(sim.Helper)
    
    #im_components = ['DhtFacade', 'DhtProtocol', 'TieredRouter', 'ReplicaManager`1', 'IdAddressedRouter']
    #stack_graph = make_stack_graph(im_components, SAMPLE_PERIOD_MS)
    #graph = Graph.Create(MainWindow.Current.Dispatcher, stack_graph)
    #report.AddGraph(graph)

    report.Start(run_title)
    
    #def tmp3(*args):
        #Sim.Current.UpdateSources(stack_graph.Sources)
        
    if show_parse_bytes:
        sources = {}
        def tmp(*args):
            graph_parse_bytes(run_title, report, graphs, sources)
        sim.Helper.NewStatistics += tmp
    if show_call_counts:
        sources =  {}
        def tmp(*args):
            graph_call_counts(run_title, report, graphs, sources)
        sim.Helper.NewStatistics += tmp
        
    sim.RepeatGetStatistics(sample_period_ms)
    if duration > 0:
        sim.RunFor(duration)
    else:
        while True:
            sim.RunFor(sample_period_ms)


def static_num():
    k = 0
    while True:
        k += 1
        yield k
        
next_node_number = static_num().next

def initialize_run(peer_initializer, num_peers, peer_start_delay, has_full_connectivity=True, plot_sequence_diagram=False, inbound=64, outbound=64):
    ni = NetworkInitializer()
    ni.SetDefaults()
    ni.NodeInboundBandwidthCapacity = inbound
    ni.NodeOutboundBandwidthCapacity = outbound
    ni.NodeQueueBufferSize = min(inbound, outbound) * 0.25 * 1024 / 8
    ni.Topology.HasFullConectivity = has_full_connectivity # Specifies whether all nodes can communicate with all others or not
    if plot_sequence_diagram:
        ni.Topology.MessageTracer = Sim.Current.MessageTracer
    ni.NumberOfNodes = num_peers
    ni.Topology.Load("mking-t.txt")  # TODO: Don't reload every time.  Change to using a singlton NetworkInitializer and it will get cached automatically
    ni.Reset()  # here because InitializeNode*s* calls it, and want to make sure we have the same behaviour in both cases
    if peer_start_delay == 0:
        ni.InitializeNodes(peer_initializer.initialize)  # also resets sim
    else:
        def tmp():
            ni.InitializeNode(peer_initializer.initialize, next_node_number())
        push_n_shot(tmp, peer_start_delay, num_peers)



    


def interest_radius_to_zone_size(interest_radius, ratio):
    # ratio defines the interest radius compared to the random walk size.  ratio > 1
    # means all entities will be able to see all others at all times.  ratio = 1
    # specifies that the minimum interest radius should be used so that they can
    # all still see each other (ignores size of entities, but is approx correct).  ratio < 1
    # means that entities may not be able to see each other.
    return interest_radius / (2.0 * 1.4142 * ratio)

