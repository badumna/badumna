/***************************************************************************
 *  File Name: RandomWalker.cs
 *
 *  Copyright (C) 2007 All Rights Reserved.
 * 
 *  Written by
 *      David Churchill <dgc@csse.unimelb.edu.au>
 ****************************************************************************/


using System;
using System.Collections.Generic;
using System.Text;

using Badumna;
using Badumna.DataTypes;

namespace Agency
{
    public class RandomWalker : IBehaviour
    {
        private Agent mControlledAgent;

        private readonly float mMaxSegmentLength;
        private readonly float mXMin;
        private readonly float mXMax;
        private readonly float mYMin;
        private readonly float mYMax;
        private readonly float mZMin;
        private readonly float mZMax;

        private Random mRandom;
        private readonly int mRandomSeed;
        private readonly float mSpeed;  // m/s

        private Vector3 mDestination;

        private enum State { Initializing, Turning, Travelling }
        private State mState;


        public RandomWalker(float speed, int seed, float xMin, float xMax, float yMin, float yMax, float zMin, float zMax)
        {
            if (speed <= 0.0f)
            {
                throw new ArgumentException("Speed must be positive");
            }
            this.mSpeed = speed;

            this.mRandomSeed = seed;
            this.mRandom = new Random(seed);

            this.mXMin = xMin;
            this.mXMax = xMax;
            this.mYMin = yMin;
            this.mYMax = yMax;
            this.mZMin = zMin;
            this.mZMax = zMax;

            this.mMaxSegmentLength = Math.Max(xMax - xMin, Math.Max(yMax - yMin, zMax - zMin)) / 4.0f;

            this.Reset();
        }

        public float MaximumSpeed { get { return this.mSpeed; } }

        public IBehaviour Clone()
        {
            return new RandomWalker(this.mSpeed, this.mRandomSeed, this.mXMin, this.mXMax, this.mYMin, this.mYMax, this.mXMin, this.mXMax);
        }

        public void SetAgent(Agent controlledAgent)
        {
            this.mControlledAgent = controlledAgent;
        }

        private float RandomWithin(float min, float max)
        {
            return min + (float)(this.mRandom.NextDouble() * (max - min));
        }

        private void ChooseDestination(Vector3 currentPosition)
        {
            Vector3 tempDestination = new Vector3(
                this.RandomWithin(this.mXMin, this.mXMax),
                this.RandomWithin(this.mYMin, this.mYMax),
                this.RandomWithin(this.mZMin, this.mZMax));

            Vector3 direction = (tempDestination - this.mControlledAgent.Position).Normalize();

            float distance = (float)(this.mMaxSegmentLength * this.mRandom.NextDouble());

            this.mDestination = currentPosition + distance * direction;
        }

        public void Execute(TimeSpan duration)
        {
            if (this.mControlledAgent == null)
            {
                throw new InvalidOperationException("SetAgent must be called first");
            }

            bool stateChanged = false;

            if (this.mState == State.Initializing)
            {
                float x = this.RandomWithin(this.mXMin, this.mXMax);
                float y = this.RandomWithin(this.mYMin, this.mYMax);
                float z = this.RandomWithin(this.mZMin, this.mZMax);
                this.mControlledAgent.Position = new Vector3(x, y, z);
                stateChanged = true;
                this.mState = State.Turning;
            }

            float remainingSeconds = (float)duration.TotalSeconds;

            Vector3 position = this.mControlledAgent.Position;
            Vector3 velocity = this.mControlledAgent.Velocity;

            while (remainingSeconds > 0.0)
            {
                if (this.mState == State.Turning)
                {
                    this.ChooseDestination(position);

                    velocity = this.mDestination - position;
                    float mag = velocity.Magnitude;
                    if (mag < 0.000001)
                    {
                        position = this.mDestination;
                        velocity = new Vector3(0.0F, 0.0F, 0.0F);
                    }
                    else
                    {
                        velocity *= this.mSpeed / mag;
                        this.mState = State.Travelling;
                    }
                    stateChanged = true;
                }

                if (this.mState == State.Travelling)
                {
                    float remainingDistance = (this.mDestination - position).Magnitude;
                    Vector3 path = velocity * remainingSeconds;
                    if (remainingDistance < path.Magnitude)
                    {
                        position = this.mDestination;
                        velocity = new Vector3(0.0F, 0.0F, 0.0F);
                        this.mState = State.Turning;
                        stateChanged = true;
                        remainingSeconds -= path.Magnitude / this.mSpeed;
                    }
                    else
                    {
                        position += path;
                        remainingSeconds = 0.0f;
                    }

                    if (position.X < this.mXMin || position.X > this.mXMax ||
                        position.Y < this.mYMin || position.Y > this.mYMax ||
                        position.Z < this.mZMin || position.Z > this.mZMax)
                    {
                        position = this.mControlledAgent.Position;
                        this.mState = State.Turning;
                        stateChanged = true;
                    }
                }
            }

            this.mControlledAgent.Position = position;
            this.mControlledAgent.Velocity = velocity;

            if (stateChanged)
            {
                this.mControlledAgent.MarkForUpdate();
            }
        }

        public void Reset()
        {
            this.mState = State.Initializing;
        }
    }
}
