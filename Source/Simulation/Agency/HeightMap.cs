using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

using Badumna;

namespace Agency
{
    class HeightMap
    {
        private Bitmap mBitmap;

        private float mHeightScale;
        private float mXOffset;
        private float mYOffset;
        private float mXScale;
        private float mYScale;

        public HeightMap(String fileName, float heightScale, RectangleF fitToRectangle)
        {
            this.mBitmap = new Bitmap(fileName);
            this.mHeightScale = heightScale;

            this.mXScale = this.mBitmap.Width / fitToRectangle.Width;
            this.mYScale = this.mBitmap.Height /fitToRectangle.Height;

            this.mXOffset = fitToRectangle.Left;
            this.mYOffset = fitToRectangle.Top;
        }

        public float GetHeight(float x, float z)
        {
            int xPixel = (int)((x * this.mXScale) + this.mXOffset);
            int yPixel = (int)((z * this.mYScale) + this.mYOffset);

            Color pixel = this.mBitmap.GetPixel(xPixel, yPixel);

            float saturation = pixel.GetSaturation();
            float hue = pixel.GetHue();
            float brightness = pixel.GetBrightness();

            return brightness * this.mHeightScale + 1f; // Add 1 to avoid falling through the floor at 0.
        }
    }
}
