/***************************************************************************
 *  File Name: Idler.cs
 *
 *  Copyright (C) 2007 All Rights Reserved.
 * 
 *  Written by
 *      David Churchill <dgc@csse.unimelb.edu.au>
 ****************************************************************************/


using System;
using System.Collections.Generic;
using System.Text;

using Badumna;
using Badumna.DataTypes;

namespace Agency
{
    public class Idler : IBehaviour
    {
        private Agent mControlledAgent;

        private bool mCalled;


        public Idler()
        {
            this.Reset();
        }

        public float MaximumSpeed { get { return 1; } }

        public IBehaviour Clone()
        {
            return new Idler();
        }

        public void SetAgent(Agent controlledAgent)
        {
            this.mControlledAgent = controlledAgent;
        }

        public void Execute(TimeSpan duration)
        {
            if (this.mControlledAgent == null)
            {
                throw new InvalidOperationException("SetAgent must be called first");
            }

            if (!this.mCalled)
            {
                this.mCalled = true;

                this.mControlledAgent.Velocity = new Vector3();
                this.mControlledAgent.MarkForUpdate();
            }
        }

        public void Reset()
        {
            this.mCalled = false;
        }
    }
}
