/***************************************************************************
 *  File Name: Script.cs
 *
 *  Copyright (C) 2007 All Rights Reserved.
 * 
 *  Written by
 *      David Churchill <dgc@csse.unimelb.edu.au>
 ****************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;

using System.Drawing;
using System.Drawing.Drawing2D;

using Badumna;

namespace Agency
{
    public class Script : IBehaviour
    {
        private Agent mControlledAgent;

        private SortedList<long, IBehaviour> mScript = new SortedList<long, IBehaviour>();
        private long mCurrentTime;
        private int mCurrentCue;

        private IBehaviour mCurrentBehaviour;


        public Script()
        {
            this.Reset();
        }

        public float MaximumSpeed
        {
            get
            {
                var maxSpeed = 0.1f;
                foreach (var behaviour in this.mScript.Values)
                {
                    maxSpeed = Math.Max(maxSpeed, behaviour.MaximumSpeed);
                }

                return maxSpeed;
            }
        }

        public IBehaviour Clone()
        {
            Script cloned = new Script();
            foreach (KeyValuePair<long, IBehaviour> action in this.mScript)
            {
                IBehaviour clonedBehaviour = action.Value == null ? null : action.Value.Clone();
                cloned.AddCue(TimeSpan.FromTicks(action.Key), clonedBehaviour);
            }
            return cloned;
        }

        public void SetAgent(Agent controlledAgent)
        {
            this.mControlledAgent = controlledAgent;
            foreach (KeyValuePair<long, IBehaviour> action in this.mScript)
            {
                if (action.Value != null)
                {
                    action.Value.SetAgent(controlledAgent);
                }
            }
        }

        public void Reset()
        {
            this.mCurrentTime = 0;
            this.mCurrentCue = -1;
            this.mCurrentBehaviour = null;
        }

        public void AddCue(TimeSpan timeOffset, IBehaviour behaviour)
        {
            if (this.mScript.Count == 0 && behaviour == null)
            {
                throw new ArgumentException("First cue cannot be null");
            }

            this.mScript.Add(timeOffset.Ticks, behaviour);
        }

        private long TimeToNextCue()
        {
            if (this.mCurrentCue + 1 < this.mScript.Count)
            {
                return this.mScript.Keys[this.mCurrentCue + 1] - this.mCurrentTime;
            }
            else
            {
                return long.MaxValue;
            }
        }

        public void Execute(TimeSpan duration)
        {
            if (this.mControlledAgent == null)
            {
                throw new InvalidOperationException("SetAgent must be called first");
            }

            long ticksLeft = duration.Ticks;

            while (ticksLeft > 0)
            {
                long timeToCue = this.TimeToNextCue();

                if (timeToCue == 0)
                {
                    this.mCurrentCue++;
                    this.mCurrentBehaviour = this.mScript.Values[this.mCurrentCue];
                    if (this.mCurrentBehaviour == null)
                    {
                        this.Reset();
                    }
                    else
                    {
                        this.mCurrentBehaviour.Reset();
                    }
                    continue;
                }

                long currentDuration = Math.Min(ticksLeft, timeToCue);
                if (this.mCurrentBehaviour != null)
                {
                    this.mCurrentBehaviour.Execute(TimeSpan.FromTicks(currentDuration));
                }

                ticksLeft -= currentDuration;
                this.mCurrentTime += currentDuration;
            }
        }
    }
}
