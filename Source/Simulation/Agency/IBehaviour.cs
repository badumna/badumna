/***************************************************************************
 *  File Name: IBehaviour.cs
 *
 *  Copyright (C) 2007 All Rights Reserved.
 * 
 *  Written by
 *      David Churchill <dgc@csse.unimelb.edu.au>
 ****************************************************************************/


using System;
using System.Collections.Generic;
using System.Text;

using Badumna;

namespace Agency
{
    public interface IBehaviour
    {
        float MaximumSpeed { get; }

        IBehaviour Clone();

        void SetAgent(Agent controlledAgent);

        void Execute(TimeSpan duration);

        void Reset();
    }
}
