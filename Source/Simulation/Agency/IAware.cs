using System;
using System.Collections.Generic;
using System.Text;

namespace Agency
{
    public interface IAware
    {
        List<Agent> NearbyAgents { get; }
    }
}
