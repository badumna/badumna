using System;
using System.Collections.Generic;
using System.Text;

namespace Agency
{
    public interface IOrientable
    {
        float Orientation { get; set; }
    }
}
