/***************************************************************************
 *  File Name: Agent.cs
 *
 *  Copyright (C) 2007 All Rights Reserved.
 * 
 *  Written by
 *      David Churchill <dgc@csse.unimelb.edu.au>
 ****************************************************************************/

using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization.Formatters.Binary;
using System.Drawing;

using Badumna;
using Badumna.Replication;
using Badumna.DataTypes;
using Badumna.Utilities;
using Badumna.SpatialEntities;

namespace Agency
{
    public class Agent : ISpatialOriginal, ISpatialReplica
    {
        private readonly INetworkFacade networkFacade;

        protected Vector3 mPosition;  // metres
        public Vector3 Position
        {
            get { return this.mPosition; }
            set { this.mPosition = value; }
        }
        protected Vector3 mVelocity; // metres / sec
        public Vector3 Velocity
        {
            get { return this.mVelocity; }
            set { this.mVelocity = value; }
        }

        private IBehaviour mBehaviour;
        public IBehaviour Behaviour
        {
            get { return this.mBehaviour; }
            set
            {
                this.mBehaviour = value.Clone();
                this.mBehaviour.SetAgent(this);
            }
        }

        protected float mRadius;
        protected float mInterestRadius;

        public Agent(INetworkFacade networkFacade)
            : this(networkFacade, new Vector3(0.0F, 0.0F, 0.0F), 0.0F, 200.0F)
        {
        }

        public Agent(INetworkFacade networkFacade, Vector3 position, float radius, float interestRadius)
        {
            if (networkFacade == null)
            {
                throw new ArgumentNullException("networkFacade");
            }
            
            this.networkFacade = networkFacade;

            this.mPosition = position;
            this.mRadius = radius;
            this.mInterestRadius = interestRadius;

            this.mVelocity = new Vector3(0.0f, 0.0f, 0.0f);
        }

        public virtual void Execute(TimeSpan duration)
        {
            if (this.mBehaviour != null)
            {
                this.mBehaviour.Execute(duration);
            }
            else
            {
                this.mPosition += this.mVelocity * (float)duration.TotalSeconds;
            }
        }

        public virtual void Render(Graphics context)
        {
        }

        public virtual void MarkForUpdate()
        {
            this.networkFacade.FlagForUpdate(this, new BooleanArray(true));  // TODO: Flag segments only as required
        }

        public bool IsVisible(ISpatialReplica target)
        {
            float dist = (this.mPosition - target.Position).Magnitude;
            return dist < this.mInterestRadius + target.Radius;
        }

        #region ILocalEntity, IRemoteEntity Members

        public float AreaOfInterestRadius
        {
            get { return this.mInterestRadius; }
            set { this.mInterestRadius = value; }
        }

        public BadumnaId Guid { get; set; }

        public float Radius
        {
            get { return this.mRadius; }
            set { this.mRadius = value; }
        }

        public float X
        {
            get { return this.mPosition.X; }
            set { this.mPosition.X = value; }
        }

        public float Y
        {
            get { return this.mPosition.Y; }
            set { this.mPosition.Y = value; }
        }

        public float Z
        {
            get { return this.mPosition.Z; }
            set { this.mPosition.Z = value; }
        }

        public virtual void Serialize(BooleanArray requiredParts, Stream outboundStream)
        {
        }

        public virtual void Deserialize(BooleanArray includedParts, Stream inboundStream, int estimatedMillisecondsSinceDeparture)
        {
        }

        #endregion


        public void HandleEvent(Stream stream)
        {
        }
    }
}
