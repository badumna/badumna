/***************************************************************************
 *  File Name: Seeker.cs
 *
 *  Copyright (C) 2007 All Rights Reserved.
 * 
 *  Written by
 *      David Churchill <dgc@csse.unimelb.edu.au>
 ****************************************************************************/


using System;
using System.Collections.Generic;
using System.Text;

using Badumna;
using Badumna.DataTypes;

namespace Agency
{
    public class Seeker : IBehaviour
    {
        private Agent mControlledAgent;

        private readonly Vector3 mDestination;
        private readonly float mSpeed;  // m/s

        private enum State { Initializing, Travelling, Arrived }
        private State mState;


        public Seeker(Vector3 destination, float speed)
        {
            this.mDestination = destination;

            if (speed < 0.0F)
            {
                throw new ArgumentException("Speed must be non-negative");
            }
            this.mSpeed = speed;

            this.Reset();
        }

        public float MaximumSpeed { get { return this.mSpeed; } }
        
        public IBehaviour Clone()
        {
            return new Seeker(this.mDestination, this.mSpeed);
        }

        public void SetAgent(Agent controlledAgent)
        {
            this.mControlledAgent = controlledAgent;
        }

        public void Execute(TimeSpan duration)
        {
            if (this.mControlledAgent == null)
            {
                throw new InvalidOperationException("SetAgent must be called first");
            }

            bool stateChanged = false;

            Vector3 position = this.mControlledAgent.Position;
            Vector3 velocity = this.mControlledAgent.Velocity;

            if (this.mState == State.Initializing)
            {
                velocity = this.mDestination - position;
                float mag = velocity.Magnitude;
                if (mag < 0.000001)
                {
                    position = this.mDestination;
                    velocity = new Vector3(0.0F, 0.0F, 0.0F);
                    this.mState = State.Arrived;
                }
                else
                {
                    velocity *= this.mSpeed / mag;
                    this.mState = State.Travelling;
                }
                stateChanged = true;
            }


            if (this.mState == State.Travelling)
            {
                float remainingDistance = (this.mDestination - position).Magnitude;
                Vector3 path = velocity * (float)duration.TotalSeconds;
                if (remainingDistance < path.Magnitude)
                {
                    position = this.mDestination;
                    velocity = new Vector3(0.0F, 0.0F, 0.0F);
                    this.mState = State.Arrived;
                    stateChanged = true;
                }
                else
                {
                    position += path;
                }
            }

            this.mControlledAgent.Position = position;
            this.mControlledAgent.Velocity = velocity;

            if (stateChanged)
            {
                this.mControlledAgent.MarkForUpdate();
            }
        }

        public void Reset()
        {
            this.mState = State.Initializing;
        }
    }
}
