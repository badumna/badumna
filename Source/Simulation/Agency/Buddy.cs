using System;
using System.Collections.Generic;
using System.Text;

using Badumna;
using Badumna.DataTypes;

namespace Agency
{
    public enum BuddyState
    {
        Nearby,
        Chasing,
        Idle,
        Default,
    }

    public class BuddyEventArgs : EventArgs
    {
        private BuddyState mState;
        public BuddyState State { get { return this.mState; } }

        public BuddyEventArgs(BuddyState state)
        {
            this.mState = state;
        }
    }

    public class Buddy<T> : DirectionalBehaviour<T> where T : Agent, IOrientable, IAware
    {
        private double mSecondsSinceLastChange;
        private IBehaviour mDefaultBehaviour;
        private bool mUseDefault = true;

        private static HeightMap mHeightMap = new HeightMap("terrain.png", 640, new System.Drawing.RectangleF(0,0, 2000f, 2000f));

        private BadumnaId mBuddyId;
        public BadumnaId BuddyId { get { return this.mBuddyId; } }

        public event EventHandler<BuddyEventArgs> BuddyStateChangedEvent;
        private BuddyState mState = BuddyState.Default;

        public Buddy(float minX, float minY, float maxX, float maxY)
            : base (minX, minY, maxX, maxY)
        {
            this.mDefaultBehaviour = new RandomWalker(0.5f, (int)(DateTime.Now.TimeOfDay.Ticks % Int32.MaxValue), minX, maxX, minY, maxY, 0, 0);

            this.Reset();
        }

        public override void Reset()
        {
            base.Reset();
            this.mTurnRateAtTopSpeed = 45;
            this.mDefaultBehaviour.Reset();
        }

        public override IBehaviour Clone()
        {
            Buddy<T> clone = new Buddy<T>(this.mRegionMin.X, this.mRegionMin.Y, this.mRegionMax.X, this.mRegionMax.Y);

            clone.BuddyStateChangedEvent = this.BuddyStateChangedEvent;

            return clone;
        }

        public override void SetAgent(Agent controlledAgent)
        {
            base.SetAgent(controlledAgent);
            this.mDefaultBehaviour.SetAgent(controlledAgent);
        }

        public override float GetHeight(float x, float z)
        {
            if (null == Buddy<T>.mHeightMap)
            {
                return this.mControlledAgent.Position.Y;
            }

            return Buddy<T>.mHeightMap.GetHeight(x, z);
        }

        public override void Execute(TimeSpan duration)
        {
            if (this.mSecondsSinceLastChange < 0.1)
            {
                if (this.mUseDefault)
                {
                    this.mDefaultBehaviour.Execute(duration);  // TODO: This doesn't set orientation, and doesn't hit borders set by this.Move
                }
                else
                {
                    this.Move(duration);
                }
                this.mSecondsSinceLastChange += duration.TotalSeconds;
                return;
            }

            this.mSecondsSinceLastChange = 0.0;

            List<Agent> visibleAgents = this.mControlledAgent.NearbyAgents.FindAll(this.mControlledAgent.IsVisible);

            if (visibleAgents.Count > 0)
            {
                if (null == this.mBuddyId)
                {
                    this.mBuddyId = visibleAgents[0].Guid;
                }

                this.mUseDefault = false;

                Vector3 buddyPosition = new Vector3(0.0f, 0.0f, 0.0f);
                foreach (Agent agent in visibleAgents)
                {
                    if (agent.Guid.Equals(this.mBuddyId))
                    {
                        buddyPosition = agent.Position;
                        break;
                    }
                }
                if (buddyPosition.Magnitude <= 0.0001f) // Buddy is no longer in visible objects list.
                {
                    this.mBuddyId = null;
                    this.mUseDefault = true;
                    return;
                }

                buddyPosition.Y = this.GetHeight(buddyPosition.X, buddyPosition.Z);

                // Check boundaries.
                if (buddyPosition.X > this.mRegionMax.X - 50.0f || buddyPosition.X < this.mRegionMin.X + 50.0f ||
                    buddyPosition.Z > this.mRegionMax.Y - 50.0f || buddyPosition.Z < this.mRegionMin.Y + 50.0f)
                {
                    this.mSpeed = this.MaximumSpeed;
                    this.TurnTowards(new Vector3(0.0f, 0.0f, 0.0f));
                }
                else
                {
                    float distance = (float)(buddyPosition - this.mControlledAgent.Position).Magnitude;

                    if (distance < 5.0f)
                    {                        
                        if (this.mSpeed != 0.0f)
                        {
                            this.mControlledAgent.MarkForUpdate();
                        }

                        this.mSpeed = 0.0f;
                        this.Move(duration);

                        if (this.mState == BuddyState.Chasing)
                        {
                            this.mState = BuddyState.Nearby;
                            if (null != this.BuddyStateChangedEvent)
                            {
                                this.BuddyStateChangedEvent.Invoke(this, new BuddyEventArgs(BuddyState.Nearby));
                            }
                        }
                        else if (this.mState != BuddyState.Idle)
                        {
                            this.mState = BuddyState.Idle;
                            if (null != this.BuddyStateChangedEvent)
                            {
                                this.BuddyStateChangedEvent.Invoke(this, new BuddyEventArgs(BuddyState.Idle));
                            }
                        }
                    }
                    else
                    {
                        if (this.mSpeed != this.MaximumSpeed)
                        {
                            this.mControlledAgent.MarkForUpdate();
                        }
                        this.mSpeed = this.MaximumSpeed;

                        if (this.mState != BuddyState.Chasing)
                        {
                            this.mState = BuddyState.Chasing;
                            if (null != this.BuddyStateChangedEvent)
                            {
                                this.BuddyStateChangedEvent.Invoke(this, new BuddyEventArgs(BuddyState.Chasing));
                            }
                        }
                    }

                    this.TurnTowards(buddyPosition);
                }

                this.Move(duration);
                return;
            }
            else
            {
                if (this.mState != BuddyState.Default)
                {
                    this.mState = BuddyState.Default;
                    if (null != this.BuddyStateChangedEvent)
                    {
                        this.BuddyStateChangedEvent.Invoke(this, new BuddyEventArgs(BuddyState.Default));
                    }
                }
                this.mUseDefault = true;
            }
        }
    }
}
