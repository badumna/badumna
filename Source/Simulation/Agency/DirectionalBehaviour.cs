using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

using Badumna;
using Badumna.DataTypes;

namespace Agency
{
    public abstract class DirectionalBehaviour<T> : IBehaviour where T : Agent, IOrientable, IAware
    {
        protected T mControlledAgent;

        protected readonly PointF mRegionMin;
        protected readonly PointF mRegionMax;

        protected float mSpeed;
        protected float mTurnRateAtTopSpeed;

        public float MaximumSpeed { get { return 6.0f; } }

        public DirectionalBehaviour(float minX, float minY, float maxX, float maxY)
        {
            this.mRegionMin = new PointF(minX, minY);
            this.mRegionMax = new PointF(maxX, maxY);
        }

        public virtual void Reset()
        {
            this.mSpeed = 0.0f;
            this.mTurnRateAtTopSpeed = 20.0f;
        }

        public abstract IBehaviour Clone();

        public virtual void SetAgent(Agent controlledAgent)
        {
            if (!(controlledAgent is T))
            {
                throw new ArgumentException("controlledAgent");
            }

            this.mControlledAgent = (T)controlledAgent;
        }

        public abstract void Execute(TimeSpan duration);

        public abstract float GetHeight(float x, float z);

        protected void TurnAwayFrom(Vector3 goal)
        {
            goal.Y = this.GetHeight(goal.X, goal.Z);

            // Translate goal (x,y) vector to origin, flip, translate back
            // pos + -(x - pos)
            Vector3 antiGoal = 2.0f * this.mControlledAgent.Position - goal;
            this.TurnTowards(antiGoal);
        }

        protected void TurnTowards(Vector3 goal)
        {
            Vector3 delta = goal - this.mControlledAgent.Position;

            delta.Y = 0; // Ignore Y component to simplify calculation

            if (delta.Magnitude == float.NaN || delta.Magnitude <= 1.0f)
            {
                return;
            }

            Vector3 velocity = this.mControlledAgent.Velocity;
            float velocityMagnitude = velocity.Magnitude;
            float crossProductY = (delta.X * -velocity.Z + delta.Z * velocity.X);
            double dotProduct = Vector3.DotProduct(delta, velocity);
            double deltaAngle = (Math.Acos(dotProduct / (velocityMagnitude * delta.Magnitude)) * 180.0 / Math.PI) % 180;
            float turnRate = 360;
            
            if (double.IsNaN(deltaAngle))
            {
                // The current orientation is facing towards the goal already.
                return;
            }

            if (velocityMagnitude > 0.01)
            {
                turnRate = this.mTurnRateAtTopSpeed / (velocityMagnitude / this.MaximumSpeed);
            }            

            float turnAngle = (float)Math.Min(turnRate, deltaAngle) % 360 * -Math.Sign(crossProductY);
            
            this.mControlledAgent.Orientation += turnAngle;

            while (this.mControlledAgent.Orientation < 0.0f)
            {
                this.mControlledAgent.Orientation += 360.0f;
            }

            while (this.mControlledAgent.Orientation > 360.0f)
            {
                this.mControlledAgent.Orientation -= 360.0f;
            }

            this.mControlledAgent.MarkForUpdate();
        }

        protected void Move(TimeSpan duration)
        {
            Vector3 position = this.mControlledAgent.Position;

            double angle = (this.mControlledAgent.Orientation - 90) * Math.PI / 180.0;
            Vector3 velocity = new Vector3(0f, 0f, 0f);
            velocity.X = (float)(this.mSpeed * Math.Cos(angle));
            velocity.Z = -(float)(this.mSpeed * Math.Sin(angle));

            position += this.mControlledAgent.Velocity * (float)duration.TotalSeconds;
            position.Y = this.GetHeight(position.X, position.Z);

            bool hasChanged = false;
            if (position.X > this.mRegionMax.X - 25)
            {
                this.mControlledAgent.Orientation = 180.0f;
                position.X = this.mRegionMax.X - 25;
                velocity.X = -1.0f;
                velocity.Z = 0.0f;
                hasChanged = true;
            }

            if (position.Z > this.mRegionMax.Y - 25)
            {
                this.mControlledAgent.Orientation = 270.0f;
                position.Z = this.mRegionMax.Y - 25;
                velocity.X = 0.0f;
                velocity.Z = -1.0f;
                hasChanged = true;
            }

            if (position.X < this.mRegionMin.X + 25)
            {
                this.mControlledAgent.Orientation = 0.0f;
                position.X = this.mRegionMin.X + 25;
                velocity.X = 1.0f;
                velocity.Z = 0.0f;
                hasChanged = true;
            }

            if (position.Z < this.mRegionMin.Y + 25)
            {
                this.mControlledAgent.Orientation = 90.0f;
                position.Z = this.mRegionMin.Y + 25;
                velocity.X = 0.0f;
                velocity.Z = 1.0f;
                hasChanged = true;
            }

            this.mControlledAgent.Position = position;
            this.mControlledAgent.Velocity = velocity;
            if (hasChanged)
            {
                this.mControlledAgent.MarkForUpdate();
            }
        }
    }
}
