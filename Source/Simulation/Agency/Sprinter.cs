﻿using System;
using System.Collections.Generic;
using System.Text;

using Badumna;
using Badumna.DataTypes;

namespace Agency
{
    public class Sprinter<T> : DirectionalBehaviour<T> where T : Agent, IOrientable, IAware
    {
        private Vector3 mStartingLineA;
        private Vector3 mStartingLineB;
        private Vector3 mStartingPosition;
        private float mDistance;

        private static HeightMap mHeightMap = new HeightMap("terrain.png", 640, new System.Drawing.RectangleF(0, 0, 2000f, 2000f));
        private static Random mSource = new Random((int)DateTime.Now.Ticks);

        public Sprinter(Vector3 startingLineA, Vector3 startingLineB, float distance, float speed)
            : base(float.MinValue, float.MinValue, float.MaxValue, float.MaxValue)
        {
            this.mStartingLineA = startingLineA;
            this.mStartingLineB = startingLineB;
            this.mSpeed = speed;
            this.mDistance = distance;
        }

        public override IBehaviour Clone()
        {
            Sprinter<T> clone = new Sprinter<T>(this.mStartingLineA, this.mStartingLineB, this.mDistance, this.mSpeed);
            return clone;
        }

        public override void SetAgent(Agent controlledAgent)
        {
            base.SetAgent(controlledAgent);
            this.SetStartingPosition();
        }

        private void SetStartingPosition()
        {
            Vector3 ab = this.mStartingLineB - this.mStartingLineA;
            float distance = ab.Magnitude * (float)Sprinter<T>.mSource.NextDouble();

            Vector3 position = this.mStartingPosition = this.mStartingLineA + (ab.Normalize() * distance);
            position.Y = this.GetHeight(position.X, position.Z);
            this.mControlledAgent.Position = position;

            float turnAngle = 0; 
            
            if (ab.Z > 0.01 || ab.Z < -0.01)
            {
                turnAngle = (float)((Math.Atan(ab.X / ab.Z) * 180.0 / Math.PI) + 90);
            }

            this.mControlledAgent.Orientation = turnAngle;


            double angle = (this.mControlledAgent.Orientation - 90) * Math.PI / 180.0;
            Vector3 velocity = new Vector3(0f, 0f, 0f);
            velocity.X = (float)(this.mSpeed * Math.Cos(angle));
            velocity.Z = -(float)(this.mSpeed * Math.Sin(angle));

            this.mControlledAgent.Velocity = velocity;
            this.mControlledAgent.MarkForUpdate();
        }


        public override void Execute(TimeSpan duration)
        {
            if ((this.mStartingPosition - this.mControlledAgent.Position).Magnitude > this.mDistance)
            {
                this.SetStartingPosition();
            }

            Vector3 position = this.mControlledAgent.Position;

            position += this.mControlledAgent.Velocity * (float)duration.TotalSeconds;
            position.Y = this.GetHeight(position.X, position.Z);

            this.mControlledAgent.Position = position;
        }

        public override float GetHeight(float x, float z)
        {
            if (null == Sprinter<T>.mHeightMap)
            {
                return this.mControlledAgent.Position.Y;
            }

            return Sprinter<T>.mHeightMap.GetHeight(x, z);
        }
    }
}
