using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

using Badumna;
using Badumna.DataTypes;


namespace Agency
{
    public class Swarmer<T> : DirectionalBehaviour<T> where T : Agent, IOrientable, IAware
    {
        private double mSecondsSinceLastChange;
        private IBehaviour mDefaultBehaviour;
        private bool mUseDefault = true;

        private int mGroupSize;


        public Swarmer(int groupSize, float minX, float minY, float maxX, float maxY)
            : base(minX, minY, maxX, maxY)
        {
            this.mGroupSize = groupSize;
            this.mDefaultBehaviour = new RandomWalker(6f, (int)(DateTime.Now.TimeOfDay.Ticks % Int32.MaxValue), minX, maxX, minY, maxY, 0, 0);

            this.Reset();
        }

        public override void Reset()
        {
            base.Reset();
            this.mDefaultBehaviour.Reset();
        }

        public override IBehaviour Clone()
        {
            return new Swarmer<T>(this.mGroupSize, this.mRegionMin.X, this.mRegionMin.Y, this.mRegionMax.X, this.mRegionMax.Y);
        }

        public override void SetAgent(Agent controlledAgent)
        {
            base.SetAgent(controlledAgent);
            this.mDefaultBehaviour.SetAgent(controlledAgent);
        }

        public override float GetHeight(float x, float z)
        {
            return this.mControlledAgent.Position.Y;
        }

        public override void Execute(TimeSpan duration)
        {
            if (this.mSecondsSinceLastChange < 0.5)
            {
                if (this.mUseDefault)
                {
                    this.mDefaultBehaviour.Execute(duration);  // TODO: This doesn't set orientation, and doesn't hit borders set by this.Move
                }
                else
                {
                    this.Move(duration);
                }
                this.mSecondsSinceLastChange += duration.TotalSeconds;
                return;
            }

            this.mSecondsSinceLastChange = 0.0;

            List<Agent> visibleAgents = this.mControlledAgent.NearbyAgents.FindAll(this.mControlledAgent.IsVisible);

            if (visibleAgents.Count > 0)
            {
                this.mUseDefault = false;

                Vector3 averagePosition = new Vector3(0.0f, 0.0f, 0.0f);

                foreach (Agent agent in visibleAgents)
                {
                    if (agent.Guid != this.mControlledAgent.Guid)
                    {
                        averagePosition += agent.Position;
                    }

                    float distance = (float)(agent.Position - this.mControlledAgent.Position).Magnitude;

                    if (distance < 5.0f)
                    {
                        this.TurnAwayFrom(agent.Position);
                        this.mSpeed = this.MaximumSpeed;
                        this.Move(duration);
                        this.mControlledAgent.MarkForUpdate();
                        return;
                    }
                }

                averagePosition *= 1.0f / visibleAgents.Count;
                averagePosition.Y = this.GetHeight(averagePosition.X, averagePosition.Z);                

                // Check boundaries.
                if (averagePosition.X > this.mRegionMax.X - 50.0f || averagePosition.X < this.mRegionMin.X + 50.0f ||
                    averagePosition.Z > this.mRegionMax.Y - 50.0f || averagePosition.Z < this.mRegionMin.Y + 50.0f)
                {
                    this.mSpeed = this.MaximumSpeed;
                    this.TurnTowards(new Vector3(0.0f, 0.0f, 0.0f));
                }
                else
                {
                    if (visibleAgents.Count >= this.mGroupSize)
                    {
                        this.mSpeed = this.MaximumSpeed;
                        this.TurnAwayFrom(averagePosition);
                    }
                    else
                    {
                        this.mSpeed = this.MaximumSpeed;
                        this.TurnTowards(averagePosition);
                    }
                }

                this.Move(duration);
                this.mControlledAgent.MarkForUpdate();

                return;
            }
            else
            {
                this.mUseDefault = true;
            }
        }
    }
}
