﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;

using NetworkSimulator;
using Visualizer;
using System.Windows.Threading;
using NetworkSimulator.Instrumentation;

namespace ScriptSim
{
    public class VisualMessageTracer
    {
        private readonly MessageDiagramControl control;

        public VisualMessageTracer(MessageDiagramControl control, EventfulMessageTracer messageTracer)
        {
            this.control = control;
            this.control.MessageDiagram = new MessageDiagram();

            messageTracer.OnNodeAdded += this.Dispatched<TimeSpan, int, string>(this.control.MessageDiagram.AddNode);
            messageTracer.OnNodeRemoved += this.Dispatched<TimeSpan, int>(this.control.MessageDiagram.RemoveNode);
            messageTracer.OnMessage += this.Dispatched<TimeSpan, ulong, int, string, int>(this.control.MessageDiagram.AddMessage);
            messageTracer.OnMessageHop += this.Dispatched<TimeSpan, ulong, int, bool>(this.control.MessageDiagram.AddMessageHop);
            messageTracer.OnMessageDropped += this.Dispatched<TimeSpan, ulong, int, string>(this.control.MessageDiagram.DropMessage);
        }

        private Action<A, B> Dispatched<A, B>(Action<A, B> action)
        {
            return (a, b) => this.Invoke(() => action(a, b));
        }

        private Action<A, B, C> Dispatched<A, B, C>(Action<A, B, C> action)
        {
            return (a, b, c) => this.Invoke(() => action(a, b, c));
        }

        private Action<A, B, C, D> Dispatched<A, B, C, D>(Action<A, B, C, D> action)
        {
            return (a, b, c, d) => this.Invoke(() => action(a, b, c, d));
        }

        private Action<A, B, C, D, E> Dispatched<A, B, C, D, E>(Action<A, B, C, D, E> action)
        {
            return (a, b, c, d, e) => this.Invoke(() => action(a, b, c, d, e));
        }

        private void Invoke(Action action)
        {
            if (!this.control.CheckAccess())
            {
                this.control.Dispatcher.BeginInvoke(action);
                return;
            }

            action();
        }
    }
}
