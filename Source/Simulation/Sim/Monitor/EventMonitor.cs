﻿//------------------------------------------------------------------------------
// <copyright file="EventMonitor.cs" company="National ICT Australia Limited">
//     Copyright (c) 2012 National ICT Australia Limited. All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System;
using NetworkSimulator;
using Visualizer;

namespace ScriptSim.Monitor
{
    /// <summary>
    /// Monitors network events.
    /// </summary>
    internal class EventMonitor : Monitor
    {
        /// <summary>
        /// Initializes a new instance of the EventMonitor class.
        /// </summary>
        /// <param name="eventDiagram">The event diagram</param>
        /// <param name="simulation">The simulation to monitor</param>
        public EventMonitor(EventDiagram eventDiagram, Simulation simulation)
            : base(simulation, TimeSpan.Zero)
        {
            eventDiagram.EventHistory = new EventHistory();

            simulation.EventPerformed +=
                delegate(string peerAddress, TimeSpan currentTime, string label)
                {
                    eventDiagram.EventHistory.AddEvent(eventDiagram.Dispatcher, peerAddress, currentTime.TotalMilliseconds, label);
                };
        }
    }
}
