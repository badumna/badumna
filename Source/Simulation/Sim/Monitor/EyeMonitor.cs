﻿//------------------------------------------------------------------------------
// <copyright file="EyeMonitor.cs" company="National ICT Australia Limited">
//     Copyright (c) 2012 National ICT Australia Limited. All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using NetworkSimulator;

namespace ScriptSim.Monitor
{
    /// <summary>
    /// Monitors a specific variable over time.
    /// </summary>
    internal class EyeMonitor : Monitor
    {
        /// <summary>
        /// The path to monitor.
        /// </summary>
        private string path;

        /// <summary>
        /// The peer to monitor.
        /// </summary>
        private ISimulatorFacade peer;

        /// <summary>
        /// Initializes a new instance of the EyeMonitor class.
        /// </summary>
        /// <param name="simulation">The simulation</param>
        /// <param name="period">The period between updates</param>
        /// <param name="peer">The peer to monitor.</param>
        /// <param name="path">The path to the variable to monitor.</param>
        public EyeMonitor(Simulation simulation, TimeSpan period, ISimulatorFacade peer, string path)
            : base(simulation, period)
        {
            this.peer = peer;
            this.path = path;
        }

        /// <summary>
        /// Triggered when a new sample is available.  Signature is (double timeInSeconds, double sample).
        /// </summary>
        public event Action<double, double> OnSample;

        /// <inheritdoc />
        protected override void OnUpdate()
        {
            var handler = this.OnSample;
            if (handler != null)
            {
                handler(this.Simulation.Now.TotalSeconds, Convert.ToDouble(this.peer.GetObject<object>(this.path)));
            }
        }
    }
}
