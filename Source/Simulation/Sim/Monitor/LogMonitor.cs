﻿//------------------------------------------------------------------------------
// <copyright file="LogMonitor.cs" company="National ICT Australia Limited">
//     Copyright (c) 2012 National ICT Australia Limited. All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using Badumna;
using Badumna.Utilities;
using NetworkSimulator;

namespace ScriptSim.Monitor
{
    /// <summary>
    /// Monitors the log output from the simulation.
    /// </summary>
    internal class LogMonitor : Monitor
    {
        /// <summary>
        /// Initializes a new instance of the LogMonitor class.
        /// </summary>
        /// <param name="simulation">The simulation to monitor</param>
        /// <param name="onLogMessage">The action to invoke for each log message</param>
        /// <param name="includeTags">Included tags</param>
        /// <param name="excludeTags">Excluded tags</param>
        public LogMonitor(Simulation simulation, Action<LogLevel, LogTag, string> onLogMessage, LogTag includeTags, LogTag excludeTags)
            : base(simulation, TimeSpan.Zero)
        {
            var log = new DelegateLog();
            log.OnMessage += (level, tags, message) => onLogMessage(level, tags, message);
            log.OnException += (level, tags, exception, message) => onLogMessage(level, tags, message + " : " + ExceptionToString(exception));
            Logger.Log = log;
            Logger.IncludeTags = includeTags;
            Logger.ExcludeTags = excludeTags;
            Logger.Level = LogLevel.Information;
        }

        /// <summary>
        /// Converts an exception to a string.
        /// </summary>
        /// <param name="exception">The exception</param>
        /// <returns>The string representation</returns>
        private static string ExceptionToString(Exception exception)
        {
            string result = "";

            while (exception != null)
            {
                if (result.Length > 0)
                {
                    result += " -> ";
                }

                result += exception.GetType().ToString() + ": " + exception.Message;
                exception = exception.InnerException;
            }

            return result;
        }
    }
}
