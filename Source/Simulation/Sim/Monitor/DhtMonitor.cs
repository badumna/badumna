﻿//------------------------------------------------------------------------------
// <copyright file="DhtMonitor.cs" company="National ICT Australia Limited">
//     Copyright (c) 2012 National ICT Australia Limited. All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NetworkSimulator;
using NetworkSimulator.Instrumentation;
using NetworkSimulator.Peer;

using Visualizer;

namespace ScriptSim.Monitor
{
    /// <summary>
    /// Monitors the DHT status.
    /// </summary>
    internal class DhtMonitor : Monitor
    {
        /// <summary>
        /// The canvas
        /// </summary>
        private ZoomCanvas canvas;

        /// <summary>
        /// Initializes a new instance of the DhtMonitor class.
        /// </summary>
        /// <param name="canvas">The canvas</param>
        /// <param name="simulation">The simulation to monitor</param>
        /// <param name="period">The period between updates</param>
        public DhtMonitor(ZoomCanvas canvas, Simulation simulation, TimeSpan period)
            : base(simulation, period)
        {
            this.canvas = canvas;
            this.canvas.DefaultViewport = DhtInfo.CanvasDimension;
        }

        /// <inheritdoc />
        protected override void OnUpdate()
        {
            List<VisualizedDhtNode> nodes = VisualizedDhtNode.GetLeafSetAsStrings(this.Simulation);
            List<DhtInfo> dhtInfos = new List<DhtInfo>();

            foreach (VisualizedDhtNode node in nodes)
            {
                dhtInfos.Add(new DhtInfo(node.Key, node.IsQuarantined, node.Predecessors, node.Successors));
            }

            List<List<string>> routePaths = DhtPeer.GetRoutePaths();
            List<ReplicaProperties> replicas = DhtPeer.GetReplicaProperties();

            this.canvas.Dispatcher.BeginInvoke(new Action(delegate()
            {
                DhtInfo.Render(dhtInfos, this.canvas);
                foreach (List<string> path in routePaths)
                {
                    DhtInfo.RenderPath(path, this.canvas);
                }

                foreach (ReplicaProperties replicaProperty in replicas)
                {
                    DhtInfo.RenderReplicas(this.canvas, replicaProperty.Key, replicaProperty.HostKey, replicaProperty.Replicas);
                }
            }));
        }
    }
}
