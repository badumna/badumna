﻿//------------------------------------------------------------------------------
// <copyright file="SpatialMonitor.cs" company="National ICT Australia Limited">
//     Copyright (c) 2012 National ICT Australia Limited. All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using Badumna.DataTypes;
using NetworkSimulator;
using Visualizer;

namespace ScriptSim.Monitor
{
    /// <summary>
    /// Monitors the spatial entities.
    /// </summary>
    internal class SpatialMonitor : Monitor
    {
        /// <summary>
        /// The canvas
        /// </summary>
        private ZoomCanvas canvas;

        /// <summary>
        /// Initializes a new instance of the SpatialMonitor class.
        /// </summary>
        /// <param name="canvas">The canvas</param>
        /// <param name="simulation">The simulation to monitor</param>
        /// <param name="period">The period between updates</param>
        public SpatialMonitor(ZoomCanvas canvas, Simulation simulation, TimeSpan period)
            : base(simulation, period)
        {
            this.canvas = canvas;
            this.canvas.DefaultViewport = EntityInfo.CanvasDimension;
        }

        /// <inheritdoc />
        protected override void OnUpdate()
        {
            var originalGuids = this.Simulation.GetObjectsFromEachPeer<BadumnaId>("Stack.mSpatialEntityManager.originals{}.value.Guid").ToList();
            var originalPositions = this.Simulation.GetObjectsFromEachPeer<Vector3>("Stack.mSpatialEntityManager.originals{}.value.Position").ToList();
            var originalInterestRadii = this.Simulation.GetObjectsFromEachPeer<float>("Stack.mSpatialEntityManager.originals{}.value.spatialOriginal.AreaOfInterestRadius").ToList();
            var originalRadii = this.Simulation.GetObjectsFromEachPeer<float>("Stack.mSpatialEntityManager.originals{}.value.spatialOriginal.Radius").ToList();
            var replicaGuids = this.Simulation.GetObjectsFromEachPeer<BadumnaId>("Stack.mSpatialEntityManager.replicas{}.value.Guid").ToList();
            var replicaPositions = this.Simulation.GetObjectsFromEachPeer<Vector3>("Stack.mSpatialEntityManager.replicas{}.value.Position").ToList();

            Dictionary<int, EntityInfo> entityInfos = new Dictionary<int, EntityInfo>();

            for (int i = 0; i < originalGuids.Count; i++)
            {
                EntityInfo entityInfo = new EntityInfo(
                    originalGuids[i].ToList(),
                    originalPositions[i].ToList(),
                    originalInterestRadii[i].ToList(),
                    originalRadii[i].ToList(),
                    replicaGuids[i].ToList(),
                    replicaPositions[i].ToList());
                entityInfos[i] = entityInfo;
            }

            this.canvas.Dispatcher.BeginInvoke(new Action(() => EntityInfo.Render(entityInfos, this.canvas)));
        }
    }
}
