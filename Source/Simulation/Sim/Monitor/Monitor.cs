﻿//------------------------------------------------------------------------------
// <copyright file="Monitor.cs" company="National ICT Australia Limited">
//     Copyright (c) 2012 National ICT Australia Limited. All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NetworkSimulator;

namespace ScriptSim.Monitor
{
    /// <summary>
    /// A monitor that reports on the state of the simulation.
    /// </summary>
    internal abstract class Monitor
    {
        /// <summary>
        /// The period between monitor updates.  Setting to TimeSpan.Zero to disables updates.
        /// </summary>
        private TimeSpan period;

        /// <summary>
        /// Initializes a new instance of the Monitor class.
        /// </summary>
        /// <param name="simulation">The simulation to monitor</param>
        /// <param name="period">The period between updates (use TimeSpan.Zero to disable updates)</param>
        protected Monitor(Simulation simulation, TimeSpan period)
        {
            this.Simulation = simulation;
            this.period = period;
            this.Schedule();
        }

        /// <summary>
        /// Gets the simulation being monitored.
        /// </summary>
        protected Simulation Simulation { get; private set; }

        /// <summary>
        /// Called during Update, extension point for derived classes.  Overriders need not call the base method in Monitor.
        /// </summary>
        protected virtual void OnUpdate()
        {
        }

        /// <summary>
        /// Updates the monitor.
        /// </summary>
        private void Update()
        {
            this.Schedule();
            this.OnUpdate();
        }

        /// <summary>
        /// Schedules the update action.
        /// </summary>
        private void Schedule()
        {
            if (this.period == TimeSpan.Zero)
            {
                return;
            }

            this.Simulation.Schedule(this.period, this.Update);
        }
    }
}
