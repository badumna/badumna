﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Data;
using Visualizer;

namespace ScriptSim
{
    public class Report : FlowDocument
    {
        private class GraphInfo
        {
            public Graph Graph;
            public GraphConfig GraphConfig;
        }

        private List<GraphInfo> mGraphs = new List<GraphInfo>();
        private List<GraphInfo> mNonStatisticsGraphs = new List<GraphInfo>();

        public Report()
        {
            //this.SetBinding(FlowDocument.PageWidthProperty, new Binding("ActualWidth") { RelativeSource = new RelativeSource(RelativeSourceMode.FindAncestor, typeof(UIElement), 2) });
            this.SetBinding(FlowDocument.ColumnWidthProperty, new Binding("PageWidth") { Source = this });
        }

        public void AddTitle(string title)
        {
            if (!this.CheckAccess())
            {
                this.Dispatcher.Invoke(new Action<string>(this.AddTitle), title);
                return;
            }

            this.Blocks.Add(new Paragraph(new Run(title)) { TextAlignment = TextAlignment.Center });
        }

        /// <summary>
        /// Adds a new graph specified by graphConfig.
        /// </summary>
        /// <remarks>
        /// Any sources defined on the graph will automatically during a run if Start() is called
        /// before the run begins.
        /// </remarks>
        public void AddGraph(GraphConfig graphConfig, bool updateOnNewStatistics)
        {
            if (!this.CheckAccess())
            {
                this.Dispatcher.Invoke(new Action<GraphConfig, bool>(this.AddGraph), graphConfig, updateOnNewStatistics);
                return;
            }

            Graph graph = new Graph
            {
                Height = 400,
                HorizontalAlignment = HorizontalAlignment.Stretch,
                Title = graphConfig.Title,
                XAxisLabel = graphConfig.XLabel,
                YAxisLabel = graphConfig.YLabel
            };
            this.AddGraph(graph);

            if (updateOnNewStatistics)
            {
                this.mGraphs.Add(new GraphInfo { Graph = graph, GraphConfig = graphConfig });
            }
            else
            {
                this.mNonStatisticsGraphs.Add(new GraphInfo { Graph = graph, GraphConfig = graphConfig });
            }
        }

        public void AddGraph(GraphConfig graphConfig)
        {
            this.AddGraph(graphConfig, true);
        }


        /// <summary>
        /// Adds a new graph.
        /// </summary>
        /// <remarks>
        /// Sources on the graph must be updated manually by the caller.
        /// </remarks>
        /// <param name="graph"></param>
        public void AddGraph(Graph graph)
        {
            if (!this.CheckAccess())
            {
                this.Dispatcher.Invoke(new Action<Graph>(this.AddGraph), graph);
                return;
            }

            this.Blocks.Add(new BlockUIContainer(graph));
        }

        public void Start(string runTitle)
        {
            if (!this.CheckAccess())
            {
                this.Dispatcher.Invoke(new Action<string>(this.Start), runTitle);
                return;
            }

            List<SourceConfig> sources = new List<SourceConfig>();

            foreach (GraphInfo graphInfo in this.mGraphs)
            {
                foreach (SourceConfig sourceConfig in graphInfo.GraphConfig.Sources)
                {
                    SourceConfig newSource = new SourceConfig(sourceConfig) { Name = runTitle };
                    graphInfo.Graph.AddSeries(newSource.Name, newSource.Source);
                    sources.Add(newSource);
                }
            }
            
           // Sim.Current.Helper.NewStatistics += delegate { Sim.Current.UpdateSources(sources); };

            foreach (GraphInfo graphInfo in this.mNonStatisticsGraphs)
            {
                foreach (SourceConfig sourceConfig in graphInfo.GraphConfig.Sources)
                {
                    graphInfo.Graph.AddSeries(sourceConfig.Name, sourceConfig.Source);
                }
            }
        }

    }
}
