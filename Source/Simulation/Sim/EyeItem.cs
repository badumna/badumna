﻿using System.Windows;
using Microsoft.Research.DynamicDataDisplay.DataSources;
using NetworkSimulator;
using System.Collections.Generic;
using System.Windows.Threading;
using System;
using NetworkSimulator.Peer;
using NetworkSimulator.Instrumentation;

namespace ScriptSim
{
    public enum SummaryFunction
    {
        Sum,
        Average,
        Max,
        Min
    }

    public class EyeItem
    {
        public string Label { get; set; }
        public string Path { get; set; }
        public string Peers
        {
            get { return this.mPeers; }
            set
            {
                this.mPeers = value;
                this.ExtractIndexes();
            }
        }
        public SummaryFunction Function { get; set; }

        public ObservableDataSource<Point> Source { get; private set; }

        private string mPeers;
        private List<int> mPeerIndexes = new List<int>();


        public EyeItem()
        {
            this.Source = new ObservableDataSource<Point>();
        }

        private void ExtractIndexes()
        {
            this.mPeerIndexes.Clear();

            if (this.mPeers.Trim() == "*")
            {
                return;  // Empty peer index list means all peers
            }

            string[] parts = this.mPeers.Split(',');
            this.mPeerIndexes.Clear();

            foreach (string part in parts)
            {
                int index;
                if (int.TryParse(part.Trim(), out index))
                {
                    this.mPeerIndexes.Add(index);
                }
            }
        }

        public void Update(Dispatcher dispatcher, Simulation simulation)
        {
            var peers = new List<ISimulatorFacade>();

            foreach (int index in this.mPeerIndexes)
            {
                if (index < simulation.Peers.Count)
                {
                    peers.Add(simulation.Peers[index]);
                }
            }

            if (this.mPeerIndexes.Count == 0)
            {
                peers = simulation.Peers;
            }


            List<double> values = peers.ConvertAll<double>(
                delegate(ISimulatorFacade peer)
                {
                    try
                    {
                        object obj = peer.GetObject<object>(this.Path);

                        double value;

                        if (obj is bool)
                        {
                            value = (bool)obj ? 1.0 : 0.0;
                        }
                        else if (obj is int)
                        {
                            value = (int)obj;
                        }
                        else if (obj is long)
                        {
                            value = (long)obj;
                        }
                        else
                        {
                            value = (double)obj;
                        }

                        return value;
                    }
                    catch (Exception)
                    {
                        return 0.0;
                    }
                });


            double result = 0.0;

            switch (this.Function)
            {
                case SummaryFunction.Sum:
                    result = this.Sum(values);
                    break;

                case SummaryFunction.Average:
                    result = this.Average(values);
                    break;
                
                case SummaryFunction.Min:
                    result = this.Min(values);
                    break;

                case SummaryFunction.Max:
                    result = this.Max(values);
                    break;
            }

            this.Source.AppendAsync(dispatcher, new Point(simulation.Now.TotalSeconds, result));
        }

        private double Sum(IEnumerable<double> values)
        {
            double result = 0.0;
            foreach (double value in values)
            {
                result += value;
            }
            return result;
        }

        private double Average(IEnumerable<double> values)
        {
            double result = 0.0;
            int count = 0;
            foreach (double value in values)
            {
                result += value;
                count++;
            }

            return count == 0 ? 0 : (result / count);
        }

        private double Min(IEnumerable<double> values)
        {
            double result = double.MaxValue;
            foreach (double value in values)
            {
                result = Math.Min(result, value);
            }
            return result == double.MaxValue ? 0 : result;
        }

        private double Max(IEnumerable<double> values)
        {
            double result = double.MinValue;
            foreach (double value in values)
            {
                result = Math.Max(result, value);
            }
            return result == double.MinValue ? 0 : result;
        }
    }
}
