﻿//------------------------------------------------------------------------------
// <copyright file="EventfulMessageTracer.cs" company="National ICT Australia Limited">
//     Copyright (c) 2013 National ICT Australia Limited. All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

namespace ScriptSim
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using NetworkSimulator.Instrumentation;

    /// <summary>
    /// An IMessageTracer that exposes events.
    /// </summary>
    public class EventfulMessageTracer : IMessageTracer
    {
        /// <summary>
        /// The id for the next node added.
        /// </summary>
        private int nextNodeId;

        /// <summary>
        /// Mapping for node names to ids.
        /// </summary>
        private Dictionary<string, int> nodeIds = new Dictionary<string, int>();

        /// <summary>
        /// Triggered with (time, nodeId, nodeName) when a node is added.
        /// </summary>
        public event Action<TimeSpan, int, string> OnNodeAdded;

        /// <summary>
        /// Triggered with (time, nodeId) when a node is removed.
        /// </summary>
        public event Action<TimeSpan, int> OnNodeRemoved;

        /// <summary>
        /// Triggered with (time, messageId, sourceNodeId, label, size) when a message is sent.
        /// </summary>
        public event Action<TimeSpan, ulong, int, string, int> OnMessage;

        /// <summary>
        /// Triggered with (time, messageId, nodeId, isFinalHop) when a message is received.
        /// </summary>
        public event Action<TimeSpan, ulong, int, bool> OnMessageHop;

        /// <summary>
        /// Triggered with (time, messageId, nodeId, reason) when a message is dropped.
        /// </summary>
        public event Action<TimeSpan, ulong, int, string> OnMessageDropped;

        /// <inheritdoc />
        public bool TraceRouters { get; set; }

        /// <inheritdoc />
        public int AddNode(double timeMs, string nodeName)
        {
            int nodeId;
            if (!this.nodeIds.TryGetValue(nodeName, out nodeId))
            {
                nodeId = this.nextNodeId;
                this.nextNodeId++;
                this.nodeIds[nodeName] = nodeId;
            }

            var handler = this.OnNodeAdded;
            if (handler != null)
            {
                handler(TimeSpan.FromMilliseconds(timeMs), nodeId, nodeName);
            }

            return nodeId;
        }

        /// <inheritdoc />
        public void RemoveNode(double timeMs, int index)
        {
            var handler = this.OnNodeRemoved;
            if (handler != null)
            {
                handler(TimeSpan.FromMilliseconds(timeMs), index);
            }
        }

        /// <inheritdoc />
        public void AddMessage(string label, double timeMs, int sourceNodeIndex, ulong messageId, int size)
        {
            var handler = this.OnMessage;
            if (handler != null)
            {
                handler(TimeSpan.FromMilliseconds(timeMs), messageId, sourceNodeIndex, label, size);
            }
        }

        /// <inheritdoc />
        public void AddMessageHop(ulong messageId, double timeMs, int nodeIndex, bool isFinalHop)
        {
            var handler = this.OnMessageHop;
            if (handler != null)
            {
                handler(TimeSpan.FromMilliseconds(timeMs), messageId, nodeIndex, isFinalHop);
            }
        }

        /// <inheritdoc />
        public void DropMessage(ulong messageId, double timeMs, int nodeIndex, string reason)
        {
            var handler = this.OnMessageDropped;
            if (handler != null)
            {
                handler(TimeSpan.FromMilliseconds(timeMs), messageId, nodeIndex, reason);
            }
        }
    }
}
