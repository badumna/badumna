﻿//------------------------------------------------------------------------------
// <copyright file="CategoryChart.cs" company="National ICT Australia Limited">
//     Copyright (c) 2012 National ICT Australia Limited. All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls.DataVisualization.Charting;
using System.Windows.Data;
using System.Windows.Media;

namespace ScriptSim
{
    /// <summary>
    /// A chart displays values for different categories.
    /// </summary>
    /// <typeparam name="SeriesType">Type of the series to use</typeparam>
    /// <typeparam name="IndependentType">Type of the independent value</typeparam>
    /// <typeparam name="DependentType">Type of the dependent value</typeparam>
    public class CategoryChart : INotifyPropertyChanged
    {
        /// <summary>
        /// Locates the key inside the data collection.
        /// </summary>
        private Dictionary<string, int> indexes;

        /// <summary>
        /// Backing field for Data.
        /// </summary>
        private ObservableCollection<KeyValuePair<string, double>> data;

        /// <summary>
        /// Initializes a new instance of the CategoryChart class.
        /// </summary>
        /// <param name="title">The title of the chart</param>
        public CategoryChart(string title)
        {
            this.Clear();

            this.Chart = new System.Windows.Controls.DataVisualization.Charting.Chart
            {
                Title = title,
                DataContext = this
            };

            ////var series = new ColumnSeries
            var series = new BarSeries
            {
                IndependentValuePath = "Key",
                DependentValuePath = "Value"
            };

            series.SetBinding(ColumnSeries.ItemsSourceProperty, "Data");

            this.Chart.Series.Add(series);

            var axis = new CategoryAxis
            {
                Orientation = AxisOrientation.X,
            };

            ////axis.AxisLabelStyle = new Style();
            ////axis.AxisLabelStyle.Setters.Add(new Setter(Axis.LayoutTransformProperty, new RotateTransform(-90)));

            this.Chart.Axes.Add(axis);
        }

        /// <inheritdoc />
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Gets the chart.
        /// </summary>
        public System.Windows.Controls.DataVisualization.Charting.Chart Chart { get; private set; }

        /// <summary>
        /// Gets the chart data.
        /// </summary>
        public ObservableCollection<KeyValuePair<string, double>> Data
        {
            get
            {
                return this.data;
            }

            private set
            {
                this.data = value;
                this.OnPropertyChanged("Data");
            }
        }

        /// <summary>
        /// Gets or sets the value corresponding to the given key.
        /// Returns 0 if the key doesn't not exist.
        /// </summary>
        /// <param name="key">The desired key.</param>
        /// <returns>The value</returns>
        public double this[string key]
        {
            get
            {
                int index;
                if (!this.indexes.TryGetValue(key, out index))
                {
                    return 0;
                }

                return this.Data[index].Value;
            }

            set
            {
                var newValue = new KeyValuePair<string, double>(key, value);

                int index;
                if (this.indexes.TryGetValue(key, out index))
                {
                    this.Data[index] = newValue;
                }
                else
                {
                    index = this.Data.Count;
                    this.indexes[key] = index;
                    this.Data.Add(newValue);
                }
            }
        }

        /// <summary>
        /// Clears all data from the chart.
        /// </summary>
        public void Clear()
        {
            this.indexes = new Dictionary<string, int>();

            this.Data = new ObservableCollection<KeyValuePair<string, double>>();
            
            var view = CollectionViewSource.GetDefaultView(this.Data);
            view.SortDescriptions.Add(new SortDescription("Value", ListSortDirection.Descending));
        }

        /// <summary>
        /// Raise the PropertyChanged event.
        /// </summary>
        /// <param name="name">Name of the changed property</param>
        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }
    }
}
