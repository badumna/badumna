﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Linq;

using Microsoft.Win32;

using Badumna.Utilities;
using System.IO;
using Badumna.Core;
using Badumna;


namespace ScriptSim
{
    /// <summary>
    /// Interaction logic for LogViewer.xaml
    /// </summary>
    partial class LogViewer : UserControl
    {
        private class LogItem
        {
            private const string Separator = " : ";

            public long Time { get; private set; }
            public LogLevel Level { get; private set; }
            public LogTag Tag { get; private set; }
            public string Description { get; private set; }

            public LogItem(LogLevel level, LogTag tag, string message)
            {
                this.Level = level;
                this.Tag = tag;

                string[] parts = message.Split(new string[] { LogItem.Separator }, 2, StringSplitOptions.None);

                if (parts.Length > 0)
                {
                    long time;
                    long.TryParse(parts[0], out time);
                    this.Time = time;
                }
                if (parts.Length > 1)
                {
                    this.Description = parts[1];
                }
            }

            public override string ToString()
            {
                return string.Join(LogItem.Separator, new string[] { this.Level.ToString(), this.Tag.ToString(), this.Time.ToString(), this.Description });
            }
        }

        private class LogFilterItem : INotifyPropertyChanged
        {
            public event PropertyChangedEventHandler PropertyChanged;

            private bool mIsEnabled;
            public bool IsEnabled
            {
                get { return this.mIsEnabled; }
                set
                {
                    if (this.mIsEnabled != value)
                    {
                        this.mIsEnabled = value;
                        this.OnPropertyChanged("IsEnabled");
                    }
                }
            }

            public LogLevel Level { get; private set; }

            public LogFilterItem(LogLevel level)
            {
                this.Level = level;
            }

            private void OnPropertyChanged(string properyName)
            {
                PropertyChangedEventHandler handler = this.PropertyChanged;
                if (handler != null)
                {
                    handler(this, new PropertyChangedEventArgs(properyName));
                }
            }
        }

        private List<LogFilterItem> mTraceLevels = new List<LogFilterItem>
        {
            new LogFilterItem(LogLevel.Error) { IsEnabled = true },
            new LogFilterItem(LogLevel.Warning) { IsEnabled = true },
            new LogFilterItem(LogLevel.Information) { IsEnabled = true }
        };

        private ObservableCollection<LogItem> mLogItems = new ObservableCollection<LogItem>();

        public LogViewer()
        {
            InitializeComponent();
            this.Log.ItemsSource = this.mLogItems;

            this.LogLevelFilter.ItemsSource = this.mTraceLevels;

            var view = CollectionViewSource.GetDefaultView(this.Log.ItemsSource);
            view.Filter = this.IsLogItemEnabled;
            foreach (LogFilterItem filter in this.mTraceLevels)
            {
                filter.PropertyChanged += delegate { view.Refresh(); };
            }
            this.FilterString.TextChanged += delegate { view.Refresh(); };
        }

        private bool IsLogItemEnabled(object item)
        {
            LogItem logItem = (LogItem)item;

            if (this.FilterString.Text.Length > 0 && !logItem.Description.Contains(this.FilterString.Text))
            {
                return false;
            }

            LogFilterItem filter = this.mTraceLevels.Find(lfi => lfi.Level == logItem.Level); // TODO: Eliminate Find
            return filter == null || filter.IsEnabled;
        }

        public void WriteLine(LogLevel level, LogTag tag, string message)
        {
            if (!this.CheckAccess())
            {
                this.Dispatcher.BeginInvoke(new Action<LogLevel, LogTag, string>(this.WriteLine), level, tag, message);
                return;
            }
#if TRACE
            this.mLogItems.Add(new LogItem(level, tag, message));
#endif
        }

        public void Clear()
        {
            if (!this.CheckAccess())
            {
                this.Dispatcher.Invoke(new Action(this.Clear));
                return;
            }

            this.mLogItems.Clear();
        }

        private void ClearLog_Click(object sender, RoutedEventArgs e)
        {
            this.Clear();
        }

        private void SaveLog_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveDialog = new SaveFileDialog();
            saveDialog.Filter = "Log files (*.log)|*.log|All files (*.*)|*.*";
            if (saveDialog.ShowDialog() == true)
            {
                var lines = from item in this.mLogItems where this.IsLogItemEnabled(item) select item.ToString();
                File.WriteAllLines(saveDialog.FileName, lines.ToArray());
            }
        }
    }
}
