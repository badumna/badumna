﻿//------------------------------------------------------------------------------
// <copyright file="Job.cs" company="National ICT Australia Limited">
//     Copyright (c) 2012 National ICT Australia Limited. All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.DataVisualization;
using System.Windows.Documents;
using NetworkSimulator;
using NetworkSimulator.Scenario;
using ScriptSim.Monitor;
using Visualizer;

namespace ScriptSim
{
    using System.Linq;
    using Badumna;

    /// <summary>
    /// A simulation job
    /// </summary>
    internal class Job : FlowDocument
    {
        /// <summary>
        /// The start button.
        /// </summary>
        private Button startButton;

        /// <summary>
        /// Supported monitor types.
        /// </summary>
        private Dictionary<string, Action<Simulation, TimeSpan, EventfulMessageTracer>> monitors;

        /// <summary>
        /// Indicates whether a monitor is enabled (not present in the dictionary means not enabled).
        /// </summary>
        private Dictionary<string, bool> monitorIsEnabled = new Dictionary<string, bool>();

        /// <summary>
        /// Initializes a new instance of the Job class.
        /// </summary>
        /// <param name="title">The job title</param>
        public Job(string title)
        {
            this.monitors = new Dictionary<string, Action<Simulation, TimeSpan, EventfulMessageTracer>>
            {
                { "DHT", this.AddDhtMonitor },
                { "Spatial", this.AddSpatialMonitor },
                { "Events", this.AddEventMonitor },
                { "Log", this.AddLogMonitor },
                { "Packets", this.AddMessageTracer },
                { "Traffic", this.AddTrafficChart },
                { "Connections", this.AddConnectionChart },
                { "RTT", this.AddRttChart },
            };

            foreach (var monitor in this.monitors)
            {
                var monitorName = monitor.Key;
                var checkBox = new CheckBox { Content = monitorName };
                checkBox.Checked += delegate { this.monitorIsEnabled[monitorName] = true; };
                checkBox.Unchecked += delegate { this.monitorIsEnabled[monitorName] = false; };
                this.Blocks.Add(new BlockUIContainer(checkBox));
            }

            this.Blocks.Add(new Paragraph(new Run(title)));

            this.startButton = new Button
            {
                Content = "Start"
            };

            this.startButton.Click += this.Start_Click;

            this.Blocks.Add(new BlockUIContainer(this.startButton));
        }

        /// <summary>
        /// Creates the scenario.
        /// </summary>
        /// <returns>The scenario</returns>
        private SimpleScenario MakeScenario()
        {
            var topology = new Topology();
            ////topology.Load("mking-t.txt");
            topology.SetSimpleToplogy(1, 10);
            topology.DeviationFactor = 0.05;
            topology.HasFullConnectivity = true;

            return new RandomWalk
            {
                Topology = topology,
                Reliability = 1,

                BufferSize = 640 * 1024,
                InboundBandwidth = 640,
                OutboundBandwidth = 640,

                NumberOfScenes = 1,
                NumberOfPeersPerScene = 3,

                Radius = 1,
                InterestRadius = 10,
                Speed = 1,
                ZoneSize = 10,
                PositionSeed = 0,
            };

            ////return new LongRunningSeed
            ////{
            ////    Topology = topology,
            ////    Reliability = 1,

            ////    BufferSize = 640 * 1024,
            ////    InboundBandwidth = 640,
            ////    OutboundBandwidth = 640,

            ////    PeerCount = 15
            ////};
        }

        /// <summary>
        /// Starts the job
        /// </summary>
        /// <param name="sender">The sender</param>
        /// <param name="e">The event arguments</param>
        private void Start_Click(object sender, RoutedEventArgs e)
        {
            this.startButton.IsEnabled = false;

            ////var scenario = this.MakeScenario();

            var messageTracer = new EventfulMessageTracer();
            ////scenario.MessageTracer = messageTracer;

            ////var logText = new TextBox
            ////{
            ////    Height = 400,
            ////    IsReadOnly = true
            ////};
            ////this.Blocks.Add(new BlockUIContainer(logText));
            ////scenario.OnResult += discoveryTime => this.Dispatcher.Invoke(new Action(() => logText.Text += discoveryTime + "\n"));

            var latency = 470;
            Simulation simulation;
            var runner = SimulationTests.Connection.RelayTests.DetermineEstimatedRtt(TimeSpan.FromMilliseconds(latency), SimNatType.Open, SimNatType.Open, out simulation, messageTracer);

            ////var simulation = scenario.Create();
            ////var duration = TimeSpan.FromHours(5);
            ////Action runner = () => simulation.RunFor(duration);

            var timeText = new Label();
            this.Blocks.Add(new BlockUIContainer(timeText));
            Action updateTime = null;
            updateTime =
                delegate()
                {
                    this.Dispatcher.Invoke(new Action(() => timeText.Content = simulation.Now));
                    simulation.Schedule(TimeSpan.FromMilliseconds(50), updateTime);
                };
            updateTime();
            
            var samplePeriod = TimeSpan.FromSeconds(1);

            foreach (var monitor in this.monitorIsEnabled)
            {
                if (!monitor.Value)
                {
                    continue;
                }

                this.monitors[monitor.Key](simulation, samplePeriod, messageTracer);
            }

            Task.Factory.StartNew(runner, TaskCreationOptions.LongRunning)
                .ContinueWith(
                delegate(Task task)
                {
                    if (task.IsFaulted)
                    {
                        var exception = task.Exception.InnerExceptions.Count == 1 ? task.Exception.InnerExceptions[0] : task.Exception;

                        var caption = "Exception: " + exception.GetType().Name;
                        var detail = new StringBuilder();
                        detail.AppendLine("Simulation threw unhandled exception.")
                            .AppendLine()
                            .AppendLine("Message: " + exception.Message)
                            .AppendLine()
                            .AppendLine("Stack trace: " + exception.StackTrace)
                            .AppendLine()
                            .AppendLine("Launch debugger?");

                        if (MessageBox.Show(detail.ToString(), caption, MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                        {
                            Debugger.Break();
                        }
                    }
                },
                TaskScheduler.FromCurrentSynchronizationContext());
        }

        /// <summary>
        /// Adds a monitor for the DHT
        /// </summary>
        /// <param name="simulation">The simulation</param>
        /// <param name="samplePeriod">The sample period</param>
        /// <param name="messageTracer">The message tracer</param>
        private void AddDhtMonitor(Simulation simulation, TimeSpan samplePeriod, EventfulMessageTracer messageTracer)
        {
            var dhtCanvas = new ZoomCanvas
            {
                Width = 400,
                Height = 400
            };

            this.Blocks.Add(new BlockUIContainer(dhtCanvas));
            new DhtMonitor(dhtCanvas, simulation, samplePeriod);
        }

        /// <summary>
        /// Adds a monitor for spatial entities
        /// </summary>
        /// <param name="simulation">The simulation</param>
        /// <param name="samplePeriod">The sample period</param>
        /// <param name="messageTracer">The message tracer</param>
        private void AddSpatialMonitor(Simulation simulation, TimeSpan samplePeriod, EventfulMessageTracer messageTracer)
        {
            var spatialCanvas = new ZoomCanvas
            {
                Width = 400,
                Height = 400
            };

            this.Blocks.Add(new BlockUIContainer(spatialCanvas));
            new SpatialMonitor(spatialCanvas, simulation, samplePeriod);
        }

        /// <summary>
        /// Adds a monitor for events
        /// </summary>
        /// <param name="simulation">The simulation</param>
        /// <param name="samplePeriod">The sample period</param>
        /// <param name="messageTracer">The message tracer</param>
        private void AddEventMonitor(Simulation simulation, TimeSpan samplePeriod, EventfulMessageTracer messageTracer)
        {
            var eventDiagram = new EventDiagram
            {
                Width = 400,
                Height = 400
            };
            
            this.Blocks.Add(new BlockUIContainer(eventDiagram));
            new EventMonitor(eventDiagram, simulation);
        }

        /// <summary>
        /// Adds a monitor for log messages
        /// </summary>
        /// <param name="simulation">The simulation</param>
        /// <param name="samplePeriod">The sample period</param>
        /// <param name="messageTracer">The message tracer</param>
        private void AddLogMonitor(Simulation simulation, TimeSpan samplePeriod, EventfulMessageTracer messageTracer)
        {
            var logViewer = new LogViewer();
            this.Blocks.Add(new BlockUIContainer(logViewer));
            new LogMonitor(simulation, logViewer.WriteLine, LogTag.RemoteCall, LogTag.None);
        }

        /// <summary>
        /// Adds a monitor for messages
        /// </summary>
        /// <param name="simulation">The simulation</param>
        /// <param name="samplePeriod">The sample period</param>
        /// <param name="messageTracer">The message tracer</param>
        private void AddMessageTracer(Simulation simulation, TimeSpan samplePeriod, EventfulMessageTracer messageTracer)
        {
            var sequenceDiagram = new MessageDiagramControl { Height = 800 };
            this.Blocks.Add(new BlockUIContainer(sequenceDiagram));
            new VisualMessageTracer(sequenceDiagram, messageTracer);
        }

        /// <summary>
        /// Adds a monitor for traffic volumes
        /// </summary>
        /// <param name="simulation">The simulation</param>
        /// <param name="samplePeriod">The sample period</param>
        /// <param name="messageTracer">The message tracer</param>
        private void AddTrafficChart(Simulation simulation, TimeSpan samplePeriod, EventfulMessageTracer messageTracer)
        {
            var trafficChart = new CategoryChart("Traffic");
            trafficChart.Chart.Height = 800;
            trafficChart.Chart.LegendStyle = new Style(typeof(Legend));
            trafficChart.Chart.LegendStyle.Setters.Add(new Setter(Legend.VisibilityProperty, Visibility.Collapsed));
            trafficChart.Chart.LegendStyle.Setters.Add(new Setter(Legend.WidthProperty, 0.0));
            trafficChart.Chart.LegendStyle.Setters.Add(new Setter(Legend.HeightProperty, 0.0));
            this.Blocks.Add(new BlockUIContainer(trafficChart.Chart));

            messageTracer.OnMessage +=
                delegate(TimeSpan time, ulong messageId, int sourceNodeId, string label, int size)
                {
                    // Strip off acks and sequence number so we just have the message type.
                    var messageType = new string(label.TakeWhile(x => x != '(' && x != '[').ToArray()).Trim();
                    if (messageType.Length == 0)
                    {
                        messageType = "ack only";
                    }

                    this.Dispatcher.Invoke(new Action(() => trafficChart[messageType] += size));
                };
        }

        /// <summary>
        /// Adds a monitor for connections
        /// </summary>
        /// <param name="simulation">The simulation</param>
        /// <param name="samplePeriod">The sample period</param>
        /// <param name="messageTracer">The message tracer</param>
        private void AddConnectionChart(Simulation simulation, TimeSpan samplePeriod, EventfulMessageTracer messageTracer)
        {
            var connectionChart = new System.Windows.Controls.DataVisualization.Charting.Chart
            {
                Title =
                    "Active connections",
                Height = 500
            };

            foreach (var i in new List<int> { 5, 10, 15, 20 })
            {
                var peer = simulation.Peers[i];
                var connectionMonitor = new EyeMonitor(
                    simulation, samplePeriod, peer, "Stack.ConnectionTable.mConnectionSet.Count");
                var connectionData = new ObservableCollection<KeyValuePair<double, double>>();
                connectionMonitor.OnSample +=
                    (time, sample) =>
                    this.Dispatcher.BeginInvoke(
                        new Action(() => connectionData.Add(new KeyValuePair<double, double>(time, sample))));

                connectionChart.Series.Add(
                    new System.Windows.Controls.DataVisualization.Charting.ScatterSeries
                    {
                        IndependentValuePath =
                            "Key",
                        DependentValuePath =
                            "Value",
                        ItemsSource =
                            connectionData
                    });
            }

            this.Blocks.Add(new BlockUIContainer(connectionChart));
        }

        /// <summary>
        /// Adds a monitor for RTT
        /// </summary>
        /// <param name="simulation">The simulation</param>
        /// <param name="samplePeriod">The sample period</param>
        /// <param name="messageTracer">The message tracer</param>
        private void AddRttChart(Simulation simulation, TimeSpan samplePeriod, EventfulMessageTracer messageTracer)
        {
            var rttMonitor = new EyeMonitor(
                simulation,
                samplePeriod,
                simulation.Peers[0],
                "Stack.ConnectionTable.mConnections[0].Value.RoundTripTimeEstimate");
            var rttData = new ObservableCollection<KeyValuePair<double, double>>();
            rttMonitor.OnSample +=
                (time, sample) =>
                this.Dispatcher.BeginInvoke(
                    new Action(() => rttData.Add(new KeyValuePair<double, double>(time, sample))));

            var rttChart = new System.Windows.Controls.DataVisualization.Charting.Chart
            {
                Title = "RTT",
                Height = 500
            };

            rttChart.Series.Add(
                new System.Windows.Controls.DataVisualization.Charting.ScatterSeries
                {
                    IndependentValuePath = "Key",
                    DependentValuePath = "Value",
                    ItemsSource = rttData
                });

            this.Blocks.Add(new BlockUIContainer(rttChart));
        }
    }
}
