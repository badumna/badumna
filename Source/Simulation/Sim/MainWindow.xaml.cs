﻿//------------------------------------------------------------------------------
// <copyright file="MainWindow.xaml.cs" company="National ICT Australia Limited">
//     Copyright (c) 2012 National ICT Australia Limited. All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;

using NetworkSimulator;
using NetworkSimulator.Scenario;

using ScriptSim.Monitor;

using Visualizer;

namespace ScriptSim
{
    partial class MainWindow : Window
    {
        private List<TabItem> reportTabs = new List<TabItem>();

        public MainWindow()
        {
            InitializeComponent();

            this.AddTab("Job 1", new FlowDocumentScrollViewer() { Document = new Job("test") });
        }

        public void AddTab(string header, object content)
        {
            CloseableTabItem tab = this.MakeClosableTab(header, content);
            this.Tabs.Items.Add(tab);
        }

        private CloseableTabItem MakeClosableTab(string header, object content)
        {
            CloseableTabItem tab = new CloseableTabItem { Header = header, Content = content };
            tab.CloseTab += delegate
            {
                this.Tabs.Items.Remove(tab);
            };

            return tab;
        }

        private void Exit_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Exit?", "Confirm", MessageBoxButton.OKCancel) == MessageBoxResult.OK)
            {
                this.Close();
            }
        }
    }
}
