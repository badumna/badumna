﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Research.DynamicDataDisplay;
using Microsoft.Win32;
using System.IO;

namespace ScriptSim
{
    public partial class EyeGraph : UserControl
    {
        public string Label { get; set; }
        public string YAxisLabel { get; set; }

        private List<EyeItem> mLineGraphs = new List<EyeItem>();


        public EyeGraph()
        {
            InitializeComponent();
        }

        public void Add(EyeItem item)
        {
            if (!this.mLineGraphs.Contains(item))
            {
                this.Plot.AddLineGraph(item.Source, 1.0, item.Label ?? "<unnamed>");
                this.mLineGraphs.Add(item);
            }
        }

        private void SaveAsCsv_Click(object sender, RoutedEventArgs e)
        {
            if (this.mLineGraphs.Count == 0)
            {
                return;
            }

            SaveFileDialog saveDialog = new SaveFileDialog();
            saveDialog.Filter = "CSV Files|*.csv|All Files|*.*";
            if (saveDialog.ShowDialog() == true)
            {
                using (StreamWriter writer = File.CreateText(saveDialog.FileName))
                {
                    // Assumes all the line graphs have same x-axis points
                    for (int i = 0; i < this.mLineGraphs[0].Source.Collection.Count; i++)
                    {
                        StringBuilder line = new StringBuilder();
                        line.Append(this.mLineGraphs[0].Source.Collection[i].X).Append(',');

                        for (int j = 0; j < this.mLineGraphs.Count; j++)
                        {
                            if (j != 0)
                            {
                                line.Append(',');
                            }

                            line.Append(this.mLineGraphs[j].Source.Collection[i].Y);
                        }

                        writer.WriteLine(line.ToString());
                    }
                }
            }
        }
    }
}
