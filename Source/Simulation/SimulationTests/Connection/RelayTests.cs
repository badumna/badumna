﻿//------------------------------------------------------------------------------
// <copyright file="RelayTests.cs" company="National ICT Australia Limited">
//     Copyright (c) 2013 National ICT Australia Limited. All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

namespace SimulationTests.Connection
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using NetworkSimulator;
    using NUnit.Framework;
    using Badumna;
    using Badumna.Utilities;
using NetworkSimulator.Instrumentation;

    /// <summary>
    /// Tests for the relay system.
    /// </summary>
    public class RelayTests
    {
        /// <summary>
        /// Check that a connection between two symmetric NATs calculates the correct RTT.
        /// </summary>
        [Test]
        public void RttIsValidForRelayedConnection()
        {
            var latency = 100;
            Simulation sim;
            var rtts = RelayTests.DetermineEstimatedRtt(TimeSpan.FromMilliseconds(latency), SimNatType.SymmetricNat, SimNatType.SymmetricNat, out sim)();

            var maxExpectedDirectRtt = latency * 2 + 50;
            var maxExpectedRelayedRtt = 2 * maxExpectedDirectRtt;

            Assert.LessOrEqual(rtts[0], maxExpectedDirectRtt);
            Assert.Greater(rtts[1], maxExpectedDirectRtt);
            Assert.LessOrEqual(rtts[1], maxExpectedRelayedRtt);
        }

        [Test]
        public void Rtts()
        {
            var step = 10;
            var limit = 2000;
            for (var latency = step; latency <= limit; latency += step)
            {
                Simulation sim;
                Func<double> getRtt = () => RelayTests.DetermineEstimatedRtt(TimeSpan.FromMilliseconds(latency), SimNatType.Open, SimNatType.Open, out sim)()[1];
                Console.WriteLine("{0}, {1}", latency, string.Join(", ", Enumerable.Range(0, 3).Select(x => getRtt()).ToList()));
            }
        }

        /// <summary>
        /// Prepares a simulation to determine the estimate of RTT produced by the network stack.
        /// </summary>
        /// <remarks>
        /// Returns a list containing two RTT estimates.
        /// The first element is the RTT estimate made by peer1 for the connection to the seed peer.
        /// The second element is the RTT estimate made by peer1 for the connection to peer2.
        /// </remarks>
        /// <param name="latency">The latency of a single hop</param>
        /// <param name="peer1">The NAT type of peer 1</param>
        /// <param name="peer2">The NAT type of peer 2</param>
        /// <param name="simulation">The prepared simulation</param>
        /// <param name="messageTracer">Optional message tracer</param>
        /// <returns>A function that will execute the simulation and return the two RTT estimates</returns>
        public static Func<List<double>> DetermineEstimatedRtt(TimeSpan latency, SimNatType peer1, SimNatType peer2, out Simulation simulation, IMessageTracer messageTracer = null)
        {
            var sim = simulation = Simulation.CreateWithPeers(latency, messageTracer, peer1, peer2);

            var rttHistory = new List<Queue<double>>
            {
                new Queue<double>(),
                new Queue<double>()
            };

            var samplePeriod = TimeSpan.FromMilliseconds(100);
            var historyDuration = TimeSpan.FromSeconds(30);
            var historyLength = (int)(historyDuration.TotalSeconds / samplePeriod.TotalSeconds);

            var finished = false;

            sim.Repeat(
                samplePeriod,
                () =>
                {
                    var rtts = new List<double>(sim.Peers[1].GetObjects<double>("Stack.ConnectionTable.Connections{}.RoundTripTimeEstimate"));
                    if (rtts.Count != rttHistory.Count)
                    {
                        return;
                    }

                    var done = true;
                    for (int i = 0; i < rtts.Count; ++i)
                    {
                        rttHistory[i].Enqueue(rtts[i]);
                        while (rttHistory[i].Count > historyLength)
                        {
                            rttHistory[i].Dequeue();
                        }

                        if (rttHistory[i].Count == historyLength)
                        {
                            var average = 0.0;
                            var max = double.MinValue;
                            var min = double.MaxValue;

                            foreach (var rtt in rttHistory[i])
                            {
                                average += rtt;
                                max = Math.Max(max, rtt);
                                min = Math.Min(min, rtt);
                            }

                            average /= rttHistory[i].Count;

                            var variation = (max - min) / average;
                            if (variation > 0.02)
                            {
                                done = false;
                            }
                        }
                    }

                    finished = done;
                });

            return () =>
            {
                sim.RunFor(TimeSpan.FromMinutes(1));  // Allow time for packet sending to start (i.e. DHT quarantine period to elapse).

                sim.RunUntil(
                    () => finished,
                    TimeSpan.FromMinutes(5),
                    () => Assert.Fail(string.Format("Failed to settle")));

                Assert.AreEqual(2, sim.Peers[1].GetObject<int>("Stack.ConnectionTable.Connections.Count"));
                Assert.AreEqual(
                    sim.Peers[0].Address,
                    sim.Peers[1].GetObject<object>("Stack.ConnectionTable.Connections[0].PeersPublicAddress").ToString(),
                    "Expected first connection to be with seed peer");

                return new List<double>(sim.Peers[1].GetObjects<double>("Stack.ConnectionTable.Connections{}.RoundTripTimeEstimate"));
            };
        }
    }
}
