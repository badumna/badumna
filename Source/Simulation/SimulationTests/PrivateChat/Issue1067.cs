﻿//------------------------------------------------------------------------------
// <copyright file="PrivateChat.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2013 National ICT Australia Limited. All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

namespace SimulationTests.PrivateChat
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using NUnit.Framework;
    using NetworkSimulator;
    using NetworkSimulator.Peer;
    using Badumna.Utilities;
    using Badumna;

    [TestFixture]
    class Issue1067
    {
        [Test]
        public void Test()
        {
            var topology = new Topology();
            topology.SetSimpleToplogy(1, 10);
            topology.DeviationFactor = 0.05;
            topology.HasFullConnectivity = true;
            var scenario = new NetworkSimulator.Scenario.SimpleScenario
            {
                Topology = topology,
                Reliability = 1,

                BufferSize = 640 * 1024,
                InboundBandwidth = 640,
                OutboundBandwidth = 640,
            };

            var simulation = scenario.Create();

            scenario.Options.Options.Logger.LoggerType = LoggerType.File;
            scenario.Options.Options.Logger.LogLevel = LogLevel.Information;
            scenario.Options.Options.Logger.IncludeTags = LogTag.Presence | LogTag.Chat;
            scenario.Options.Options.Logger.LogTimestamp = true;

            scenario.Options.Options.Connectivity.StartPortRange = 21300;
            scenario.Options.Options.Connectivity.EndPortRange = 21300;
            var seedPeer = simulation.StartPeer(scenario.Options);

            scenario.Options.Options.Connectivity.StartPortRange = 21301;
            scenario.Options.Options.Connectivity.EndPortRange = 21301;
            var fakeRNG1 = new FakeRNG();
            scenario.Options.RandomNumberGenerator = fakeRNG1;
            fakeRNG1.IntValue = 7;
            var facade1 = simulation.StartPeer(scenario.Options);
            var chatter1 = new ChatPeer(facade1, "Alice");
            
            scenario.Options.Options.Connectivity.StartPortRange = 21302;
            scenario.Options.Options.Connectivity.EndPortRange = 21302;
            var fakeRNG2 = new FakeRNG();
            scenario.Options.RandomNumberGenerator = fakeRNG2;
            fakeRNG2.IntValue = 107;
            var facade2 = simulation.StartPeer(scenario.Options);
            var chatter2 = new ChatPeer(facade2, "Bob");

            var fakeRNG3 = new FakeRNG();
            scenario.Options.RandomNumberGenerator = fakeRNG3;
            fakeRNG3.IntValue = 107;
            ChatPeer chatter3 = null;

            simulation.Schedule(TimeSpan.FromSeconds(30), () => chatter1.Start());
            simulation.Schedule(TimeSpan.FromSeconds(40), () => chatter1.InitiateChatWith("Bob"));

            simulation.Schedule(TimeSpan.FromSeconds(60), () => chatter2.Start());
            simulation.Schedule(TimeSpan.FromSeconds(70), () => chatter2.InitiateChatWith("Alice"));

            simulation.Schedule(TimeSpan.FromSeconds(120), () => chatter1.SendMessage("Bob", "Foo"));
            simulation.Schedule(TimeSpan.FromSeconds(120), () => chatter2.SendMessage("Alice", "Bar"));
            simulation.Schedule(TimeSpan.FromSeconds(135), chatter2.Crash);
            simulation.Schedule(TimeSpan.FromSeconds(135.5), () => simulation.StopPeer(facade2));
            simulation.Schedule(
                TimeSpan.FromSeconds(137),
                () =>
                {
                    var facade3 = simulation.RestartPeer(facade2, scenario.Options);
                    chatter3 = new ChatPeer(facade3, "Bob");
                    chatter3.Start();
                });

            simulation.Schedule(TimeSpan.FromSeconds(138), () => chatter3.InitiateChatWith("Alice"));
            simulation.Schedule(TimeSpan.FromSeconds(140), () => chatter3.SendMessage("Alice", "Buzz"));

            simulation.RunFor(TimeSpan.FromSeconds(160));
        }

        private class FakeRNG : IRandomNumberGenerator
        {
            public double DoubleValue { get; set; }

            public int IntValue { get; set; }

            public uint UIntValue { get; set; }
            
            public double NextDouble()
            {
                return this.DoubleValue;
            }

            public int Next(int minValue, int maxValue)
            {
                if (this.IntValue < minValue || this.IntValue >= maxValue)
                {
                    throw new InvalidOperationException("Set the fake value to lie within the required range.");
                }

                return this.IntValue;
            }

            public int Next(int maxValue)
            {
                if (this.IntValue < 0 || this.IntValue >= maxValue)
                {
                    throw new InvalidOperationException("Set the fake value to lie within the required range.");
                }

                return this.IntValue;
            }

            public uint NextUInt()
            {
                return this.UIntValue;
            }
        }
    }
}
