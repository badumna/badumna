﻿//-----------------------------------------------------------------------
// <copyright file="Program.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2013 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace SimulationTests
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;

    /// <summary>
    /// A program to launch the NUnit GUI and run the unit tests in this assembly.
    /// </summary>
    class Program
    {
        /// <summary>
        /// The entry point for the program.
        /// </summary>
        /// <param name="args">Command line arguments</param>
        [STAThread]
        static void Main(string[] args)
        {
            bool useGui = (args.Length > 0 && args[0].ToUpper() == "GUI");
            List<string> arguments = new List<string>();
            arguments.Add(Assembly.GetExecutingAssembly().Location);
            arguments.AddRange(args);
            if (useGui)
            {
                arguments.Remove("GUI");
                ////arguments.Add("/run");
                NUnit.Gui.AppEntry.Main(arguments.ToArray());
            }
            else
            {
                arguments.Add("/wait");
                NUnit.ConsoleRunner.Runner.Main(arguments.ToArray());
            }
        }
    }
}
