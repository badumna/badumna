﻿//-----------------------------------------------------------------------
// <copyright file="Program.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2013 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace SimulationTests.Match
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using NUnit.Framework;
    using NetworkSimulator;
    using NetworkSimulator.Scenario;

    [TestFixture]
    class MatchTests
    {
        [SetUp]
        public void SetUp()
        {
        }

        [TearDown]
        public void TearDown()
        {
        }

        [Test]
        public void Test()
        {
            var topology = new Topology();
            topology.SetSimpleToplogy(1, 10);
            topology.DeviationFactor = 0.05;
            topology.HasFullConnectivity = true;
            var scenario = new MatchScenario
            {
                Topology = topology,
                Reliability = 1,

                BufferSize = 640 * 1024,
                InboundBandwidth = 640,
                OutboundBandwidth = 640,

                NumberOfPeers = 2,
                MatchSize = 5
            };

            var simulation = scenario.Create();

            // TODO: Setup matchmaking scenario.

            simulation.RunFor(TimeSpan.FromMinutes(3));
        }
    }
}
