//------------------------------------------------------------------------------
// <copyright file="SymmetricNat.cs" company="National ICT Australia Limited">
//     Copyright (c) 2013 National ICT Australia Limited. All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;

using Badumna.Core;

namespace NetworkSimulator.Nat
{
    /// <summary>
    /// A symmetric address translator.
    /// </summary>
    internal class SymmetricNat : IAddressTranslator
    {
        /// <summary>
        /// The device's public address
        /// </summary>
        private PeerAddress publicAddress;

        /// <summary>
        /// The next available public port
        /// </summary>
        private int nextPublicPort;

        /// <summary>
        /// Mappings between public and private addresses
        /// </summary>
        private Dictionary<PeerAddress, PortMap> translations = new Dictionary<PeerAddress, PortMap>();

        /// <summary>
        /// Initializes a new instance of the SymmetricNat class.
        /// </summary>
        /// <param name="publicAddress">The device's public address</param>
        public SymmetricNat(PeerAddress publicAddress)
        {
            this.publicAddress = publicAddress;
        }

        /// <inheritdoc />
        public PeerAddress AddAddress(out PeerAddress publicAddress)
        {
            PeerAddress newPrivateAddress  = this.GetNodesPrivateAddress(this.nextPublicPort++);
            publicAddress = this.PrivateAddressToPublicAddress(newPrivateAddress);

            this.translations.Add(publicAddress, new PortMap(publicAddress));
            return newPrivateAddress;
        }

        /// <inheritdoc />
        public void RemoveAddress(PeerAddress publicAddress)
        {
            List<PeerAddress> translationsForRemoval = new List<PeerAddress>();

            foreach (KeyValuePair<PeerAddress, PortMap> translationKVP in this.translations)
            {
                if (translationKVP.Value.PrimaryPublicAddress.Equals(publicAddress))
                {
                    translationsForRemoval.Add(translationKVP.Key);
                }
            }

            foreach (PeerAddress translationAddress in translationsForRemoval)
            {
                this.translations.Remove(translationAddress);
            }
        }

        /// <inheritdoc />
        public PeerAddress Inbound(PeerAddress source, PeerAddress destination)
        {
            PortMap translation;

            if (this.translations.TryGetValue(destination, out translation))
            {
                if (translation.AcceptMessage(source, destination.Port))
                {
                    return new PeerAddress(this.GetNodesPrivateAddress(translation.PrimaryPublicAddress.Port));
                }
            }

            return null;
        }

        /// <inheritdoc />
        public PeerAddress Outbound(PeerAddress sourcesPrivateAddress, PeerAddress destination)
        {
            PortMap translation;
            PeerAddress sourcesPublicAddress = this.PrivateAddressToPublicAddress(sourcesPrivateAddress);

            if (this.translations.TryGetValue(sourcesPublicAddress, out translation))
            {
                PeerAddress publicMapping = translation.TranslateOutbound(destination, this.nextPublicPort);

                if (publicMapping.Port == this.nextPublicPort)
                {
                    this.nextPublicPort++;
                }

                if (!this.translations.ContainsKey(publicMapping))
                {
                    this.translations.Add(publicMapping, translation);
                }

                return publicMapping;
            }

            return null;
        }

        /// <inheritdoc />
        public bool IsInternalAddress(PeerAddress address)
        {
            byte[] addressBytes = address.GetAddressBytes();

            return (addressBytes[0] == 168 && addressBytes[1] == 192) || address.Address.Equals(this.publicAddress.Address);
        }

        /// <summary>
        /// Maps a private address to a public address
        /// </summary>
        /// <param name="address">The private address</param>
        /// <returns>The public address</returns>
        private PeerAddress PrivateAddressToPublicAddress(PeerAddress address)
        {
            int port = address.Address.GetAddressBytes()[3];

            return new PeerAddress(
                this.publicAddress.Address,
                port,
                this.publicAddress.NatType);
        }

        /// <summary>
        /// Maps a public port to a private address.
        /// </summary>
        /// <param name="publicPort">The public port</param>
        /// <returns>The private address</returns>
        private PeerAddress GetNodesPrivateAddress(int publicPort)
        {
            return new PeerAddress(
                new System.Net.IPAddress(new byte[4] { 168, 192, 0, (byte)publicPort }),
                0,
                NatType.Unknown);
        }

        /// <summary>
        /// Port mappings for a single local peer.
        /// </summary>
        private class PortMap
        {
            /// <summary>
            /// Mappings from remote addresses to local addresses.
            /// </summary>
            private Dictionary<PeerAddress, PeerAddress> portMap = new Dictionary<PeerAddress, PeerAddress>();

            /// <summary>
            /// Initializes a new instance of the SymmetricNat.PortMap class.
            /// </summary>
            /// <param name="primaryPublicAddress">The primary public address of the peer</param>
            public PortMap(PeerAddress primaryPublicAddress)
            {
                this.PrimaryPublicAddress = primaryPublicAddress;
            }

            /// <summary>
            /// Gets the primary public address of the peer (i.e. the address reported by STUN).
            /// </summary>
            public PeerAddress PrimaryPublicAddress { get; private set; }

            /// <summary>
            /// Get the public source address for this peer and the given destination.
            /// </summary>
            /// <param name="destination">The destination</param>
            /// <param name="nextAvailablePort">The next available port on the NAT device</param>
            /// <returns>The public source address</returns>
            public PeerAddress TranslateOutbound(PeerAddress destination, int nextAvailablePort)
            {
                PeerAddress sourceAddress;
                if (!this.portMap.TryGetValue(destination, out sourceAddress))
                {
                    sourceAddress = new PeerAddress(this.PrimaryPublicAddress.Address, nextAvailablePort, this.PrimaryPublicAddress.NatType);
                    this.portMap[destination] = sourceAddress;
                }

                return sourceAddress;
            }

            /// <summary>
            /// Determine whether a message should be accepted for this peer from the given source.
            /// </summary>
            /// <param name="source">The source address</param>
            /// <param name="destinationPort">The destination port</param>
            /// <returns>True if the message should be accepted</returns>
            public bool AcceptMessage(PeerAddress source, int destinationPort)
            {
                PeerAddress normalizedSourceAddress = new PeerAddress(source.EndPoint, NatType.Unknown);

                if (!this.portMap.ContainsKey(normalizedSourceAddress))
                {
                    return false;
                }

                return destinationPort == this.portMap[normalizedSourceAddress].Port;
            }
        }
    }
}
