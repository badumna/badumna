// <autogenerated />

using System;
using System.Collections.Generic;
using System.Text;
using System.Net;

using Badumna.Core;
using Badumna.Utilities;
using Badumna;

namespace NetworkSimulator.Nat
{
    /// <summary>
    /// Restricted cone and restricted port NATs
    /// </summary>
    class RestrictedNat : IAddressTranslator
    {
        class PortRestriction
        {
            private PeerAddress mPublicAddress = PeerAddress.Nowhere;
            public PeerAddress PublicAddress { get { return this.mPublicAddress; } }

            private List<PeerAddress> mPunchedAddresses = new List<PeerAddress>();

            public PortRestriction(PeerAddress publicAddress)
            {
                this.mPublicAddress = publicAddress;
            }

            public void Reset()
            {
            }

            public bool AcceptMessage(PeerAddress sourceAddress, int destinationPort)
            {
                switch (this.PublicAddress.NatType)
                {
                    case NatType.RestrictedCone:
                        if (!this.IsPunched(sourceAddress, false))
                        {
                            return false;
                        }
                        break;

                    case NatType.RestrictedPort:
                        if (!this.IsPunched(sourceAddress, true))
                        {
                            Logger.TraceInformation(LogTag.Simulator, "NAT at {0} drops message from {1}",
                                this.PublicAddress, sourceAddress);

                          //  Results.Instance.Increment("Messages dropped by router");
                            return false;
                        }
                        break;
                }

                return true;
            }

            public PeerAddress TranslateOutbound(PeerAddress destination)
            {
                PeerAddress sourceAddress = this.mPublicAddress;

                if (this.mPublicAddress.NatType == NatType.RestrictedPort &&
                    !this.mPunchedAddresses.Contains(destination))
                {
                    this.mPunchedAddresses.Add(destination);
                }

                if (this.mPublicAddress.NatType == NatType.RestrictedCone &&
                    !this.mPunchedAddresses.Contains(destination))
                {
                    PeerAddress matchedAddress = new PeerAddress(destination.Address, 0, destination.NatType);

                    this.mPunchedAddresses.Add(matchedAddress);
                }

                return sourceAddress;
            }

            public bool IsPunched(PeerAddress address, bool matchPort)
            {
                PeerAddress matchedAddress = address;

                if (!matchPort)
                {
                    matchedAddress = new PeerAddress(address.Address, 0, address.NatType);
                }

                return this.mPunchedAddresses.Contains(matchedAddress);
            }
        }

        private PeerAddress mPublicAddress;
        private int mNextPort;
        private Dictionary<PeerAddress, PortRestriction> mTranslations = new Dictionary<PeerAddress, PortRestriction>();

        public RestrictedNat(PeerAddress publicAddress)
        {
            this.mPublicAddress = publicAddress;
        }

        #region IAddressTranslator Members

        public PeerAddress AddAddress(out PeerAddress publicAddress)
        {
            PeerAddress newPrivateAddress  = this.GetNodesPrivateAddress(this.mNextPort++);
            publicAddress = this.PrivateAddressToPublicAddress(newPrivateAddress);

            this.mTranslations.Add(publicAddress, new PortRestriction(publicAddress));
            return newPrivateAddress;
        }

        public void RemoveAddress(PeerAddress publicAddress)
        {
            if (this.mTranslations.ContainsKey(publicAddress))
            {
                this.mTranslations.Remove(publicAddress);
            }
        }

        public PeerAddress Inbound(PeerAddress source, PeerAddress destination)
        {
            PortRestriction translation = null;
            PeerAddress publicDestination = this.PublicAddress(destination);

            if (source.Equals(this.mPublicAddress) && publicDestination.Equals(this.mPublicAddress))
            {
                // Internal route.
                return this.GetNodesPrivateAddress(publicDestination.Port);
            }

            if (null != publicDestination && this.mTranslations.TryGetValue(publicDestination, out translation))
            {
                PeerAddress translatedDestination = new PeerAddress(this.GetNodesPrivateAddress(destination.Port));

                if (translation.AcceptMessage(source, destination.Port))
                {
                    return translatedDestination;
                }
            }

            return null;
        }

        public PeerAddress Outbound(PeerAddress sourcesPrivateAddress, PeerAddress destination)
        {
            PortRestriction translation = null;
            PeerAddress sourcesPublicAddress = this.PrivateAddressToPublicAddress(sourcesPrivateAddress);

            if (this.mTranslations.TryGetValue(sourcesPublicAddress, out translation))
            {
                return translation.TranslateOutbound(destination);
            }

            return null;
        }

        public bool IsInternalAddress(PeerAddress address)
        {
            byte[] addressBytes = address.GetAddressBytes();

            return (addressBytes[0] == 168 && addressBytes[1] == 192) || address.Equals(this.mPublicAddress);
        }

        #endregion


        private PeerAddress PrivateAddressToPublicAddress(PeerAddress address)
        {
            byte[] bytes = address.Address.GetAddressBytes();

            Array.Reverse(bytes);

            int port = (int)BitConverter.ToInt16(bytes, 0);
        
            return this.GetNodesPublicAddress(port);
        }


        private PeerAddress GetNodesPrivateAddress(int publicPort)
        {
            byte[] addressBytes = new byte[4] { 168, 192, 0, (byte)publicPort };

            return new PeerAddress(new System.Net.IPAddress(addressBytes), 0, NatType.Unknown);
        }

        private PeerAddress GetNodesPublicAddress(int number)
        {
            return new PeerAddress(this.mPublicAddress.Address, number, this.mPublicAddress.NatType);
        }

        private PeerAddress PublicAddress(PeerAddress address)
        {
            if (!this.IsInternalAddress(address))
            {
                return null;
            }

            byte[] addressBytes = address.GetAddressBytes();

            if (addressBytes[0] == 168 && addressBytes[1] == 192)
            {
                return this.GetNodesPublicAddress(addressBytes[3]);
            }

            return address;
        }
    }
}
