//------------------------------------------------------------------------------
// <copyright file="IAddressTranslator.cs" company="National ICT Australia Limited">
//     Copyright (c) 2013 National ICT Australia Limited. All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;

using Badumna.Core;

namespace NetworkSimulator.Nat
{
    /// <summary>
    /// A NAT device.
    /// </summary>
    internal interface IAddressTranslator
    {
        /// <summary>
        /// Allocates a new address behind the NAT.
        /// </summary>
        /// <param name="publicAddress">The public address corresponding to the newly allocated private address (i.e. the address that would be determined by STUN)</param>
        /// <returns>The newly allocated private address</returns>
        PeerAddress AddAddress(out PeerAddress publicAddress);

        /// <summary>
        /// Deallocates an address from behind the NAT.
        /// </summary>
        /// <param name="publicAddress">The public address to deallocate</param>
        void RemoveAddress(PeerAddress publicAddress);

        /// <summary>
        /// Get the private address for an inbound packet.
        /// </summary>
        /// <param name="source">The source of the packet</param>
        /// <param name="destination">The destination address (IP should match the NAT device's public IP)</param>
        /// <returns>The corresponding address behind the NAT</returns>
        PeerAddress Inbound(PeerAddress source, PeerAddress destination);

        /// <summary>
        /// Get the public address for an outbound packet.
        /// </summary>
        /// <param name="source">The private source address</param>
        /// <param name="destination">The destination address</param>
        /// <returns>The corresponding public address</returns>
        PeerAddress Outbound(PeerAddress source, PeerAddress destination);

        /// <summary>
        /// Determine if the given address corresponds to a device on this NAT.
        /// </summary>
        /// <remarks>
        /// The address corresponds to a device on the NAT if it's a private
        /// address owned by the NAT, or a corresponding public address.
        /// </remarks>
        /// <param name="address">The address to check</param>
        /// <returns>True if the address belongs to the NAT</returns>
        bool IsInternalAddress(PeerAddress address);
    }
}
