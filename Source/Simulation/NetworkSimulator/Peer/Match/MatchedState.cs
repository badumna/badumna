﻿//------------------------------------------------------------------------------
// <copyright file="MatchedState.cs" company="Scalify">
//     Copyright (c) 2013 Scalify. All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

namespace NetworkSimulator.Peer
{
    using System;
    using System.Collections.Generic;
    using Badumna.DataTypes;
    using Badumna.Match;

    /// <summary>
    /// State when peer is in (or joining) a known match.
    /// </summary>
    internal class MatchedState : State
    {
        /// <summary>
        /// A random number generator for spawning entities at random intervals.
        /// </summary>
        private readonly Random random = new Random();

        /// <summary>
        /// Controls the mean rate at which entities are created.
        /// </summary>
        private readonly TimeSpan meanEntityCreationInterval = TimeSpan.FromSeconds(1);

        /// <summary>
        /// For accessing Badumna API.
        /// </summary>
        private readonly Facade matchFacade;

        /// <summary>
        /// The match the peer is in.
        /// </summary>
        private Match match;

        /// <summary>
        /// A list of replicas currently in the match.
        /// </summary>
        private List<Entity> replicas = new List<Entity>();

        /// <summary>
        /// A list of originals currently in the match.
        /// </summary>
        private List<Entity> originals = new List<Entity>();

        /// <summary>
        /// Initializes a new instance of the MatchedState class with a matchmaking result to join.
        /// </summary>
        /// <param name="matchFacade">The match facade.</param>
        /// <param name="result">The query result to join.</param>
        public MatchedState(Facade matchFacade, MatchmakingResult result)
        {
            Console.WriteLine("Joining match.");
            this.matchFacade = matchFacade;
            this.match = this.matchFacade.JoinMatch(result, "Default name", this.CreateReplica, this.RemoveReplica);
        }

        /// <summary>
        /// Initializes a new instance of the MatchedState class for creating a new match.
        /// </summary>
        /// <param name="matchFacade">The match facade.</param>
        public MatchedState(Facade matchFacade)
        {
            Console.WriteLine("Creating match.");
            this.matchFacade = matchFacade;
            this.match = this.matchFacade.CreateMatch(
                new MatchmakingCriteria(),
                4,
                "default",
                this.CreateReplica,
                this.RemoveReplica);
        }

        /// <inheritdoc />
        public override State Update(TimeSpan deltaTime)
        {
            if (this.match.State == MatchStatus.Connected ||
                this.match.State == MatchStatus.Hosting)
            {
                if (this.random.NextDouble() < (deltaTime.TotalMilliseconds / this.meanEntityCreationInterval.TotalMilliseconds))
                {
                    Console.WriteLine("Creating original.");
                    var original = new Entity(true);
                    this.originals.Add(original);
                    this.match.RegisterEntity(original, 0);

                    if (this.originals.Count > 10)
                    {
                        Console.WriteLine("Removing original.");
                        this.match.UnregisterEntity(this.originals[0]);
                        this.originals[0].Destroy();
                        this.originals.RemoveAt(0);
                    }
               }
            }

            return this;
        }

        /// <summary>
        /// Create a replica entity.
        /// </summary>
        /// <param name="match">The match the entity belongs to.</param>
        /// <param name="source">The replica source (host or regular peer).</param>
        /// <param name="entityType">An application defined integer representing entity type.</param>
        /// <returns>A new entity.</returns>
        private object CreateReplica(Match match, EntitySource source, uint entityType)
        {
            Console.WriteLine("Creating replica.");
            var replica = new Entity();
            this.replicas.Add(replica);
            return replica;
        }

        /// <summary>
        /// Remove a replica entity.
        /// </summary>
        /// <param name="match">The match the entity belongs to.</param>
        /// <param name="source">The replica source (host or regular peer).</param>
        /// <param name="replica">The replica to removed.</param>
        private void RemoveReplica(Match match, EntitySource source, object replica)
        {
            Console.WriteLine("Removing replica.");
            var entity = replica as Entity;
            this.replicas.Remove(entity);
        }
    }
}
