﻿//------------------------------------------------------------------------------
// <copyright file="QueryingState.cs" company="Scalify">
//     Copyright (c) 2013 Scalify. All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

namespace NetworkSimulator.Peer
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Badumna.Match;

    /// <summary>
    /// State when trying to find a match to join. 
    /// </summary>
    internal class QueryingState : State
    {
        /// <summary>
        /// Facade exposing Badumna match functionality.
        /// </summary>
        private readonly Facade matchFacade;

        /// <summary>
        /// The latest matchmaking result received.
        /// </summary>
        private MatchmakingQueryResult queryResult;

        /// <summary>
        /// A value indicating whether a matchmaking query has been sent yet.
        /// </summary>
        private bool querySent = false;

        /// <summary>
        /// Initializes a new instance of the QueryingState class.
        /// </summary>
        /// <param name="matchFacade">Facade exposing Badumna match functionality.</param>
        public QueryingState(Facade matchFacade)
        {
            this.matchFacade = matchFacade;
        }

        /// <inheritdoc />
        public override State Update(TimeSpan deltaTime)
        {
            if (!this.querySent)
            {
                this.SendQuery();
                this.querySent = true;
            }

            if (this.queryResult == null)
            {
                return this;
            }

            if (this.queryResult.Error == MatchError.None)
            {
                // Join first available match.
                foreach (var result in this.queryResult.Results)
                {
                    return new MatchedState(this.matchFacade, result);
                }

                // Create own match, if none found.
                return new MatchedState(this.matchFacade);
            }

            // Just re-query if there was an error, as we don't want to create
            // empty matches if matchmaker is not reachable.
            this.queryResult = null;
            this.SendQuery();
            return this;
        }

        /// <summary>
        /// Handle a matchmaking query result.
        /// </summary>
        /// <param name="result">The result.</param>
        private void HandleQueryResult(MatchmakingQueryResult result)
        {
            this.queryResult = result;
        }

        /// <summary>
        /// Query the matchmaker to find a match.
        /// </summary>
        private void SendQuery()
        {
            Console.WriteLine("Sending matchmaking query.");
            var criteria = new MatchmakingCriteria();
            this.matchFacade.FindMatches(criteria, this.HandleQueryResult);
        }
    }
}
