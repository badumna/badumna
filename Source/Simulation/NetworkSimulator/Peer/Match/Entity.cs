﻿//------------------------------------------------------------------------------
// <copyright file="Entity.cs" company="Scalify">
//     Copyright (c) 2013 Scalify. All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

namespace NetworkSimulator.Peer
{
    using System;
    using System.Threading;
    using Badumna;
    using Badumna.DataTypes;

    /// <summary>
    /// A class to represent a replicable entity.
    /// </summary>
    internal class Entity
    {
        /// <summary>
        /// For synchronizing ID generation.
        /// </summary>
        private static readonly object locker = new object();

        /// <summary>
        /// The thread that the entity was created in.
        /// </summary>
        private readonly Thread creationThread;

        /// <summary>
        /// The ID of the current entity.
        /// </summary>
        private readonly int id;

        /// <summary>
        /// Next ID to assign to an entity.
        /// </summary>
        private static int nextID = 0;

        /// <summary>
        /// A value indicating whether the entity has been marked as destroyed.
        /// </summary>
        private bool destroyed;

        /// <summary>
        /// The position of the entity.
        /// </summary>
        private Vector3 position;

        /// <summary>
        /// Initializes a new instance of the Entity class.
        /// </summary>
        public Entity()
            : this(false)
        {
        }
        
        /// <summary>
        /// Initializes a new instance of the Entity class.
        /// </summary>
        /// <param name="original">A value indicating whether the entity is an original.</param>
        public Entity(bool original)
        {
            this.creationThread = Thread.CurrentThread;

            if (original)
            {
                lock (locker)
                {
                    this.id = nextID++;
                }
            }
        }

        /// <summary>
        /// Gets or sets the entity's position.
        /// </summary>
        [Replicable]
        public Vector3 Position
        {
            get
            {
                this.CheckAccess();
                return this.position;
            }

            set
            {
                this.CheckAccess();
                this.position = value;
            }
        }

        /// <summary>
        /// Gets the ID of this instance.
        /// </summary>
        public int ID
        {
            get { return this.id; }
        }

        /// <summary>
        /// Mark the entiy as destroyed, so all subsequent access attempts will throw.
        /// </summary>
        public void Destroy()
        {
            this.CheckAccess();
            this.destroyed = true;
        }

        /// <summary>
        /// Gets a string identifying the entity.
        /// </summary>
        /// <returns>The string.</returns>
        public override string ToString()
        {
            return "Entity " + this.ID.ToString();
        }

        /// <summary>
        /// Check that the current thread is the one the entity was created in.
        /// </summary>
        private void CheckThread()
        {
            if (Thread.CurrentThread != this.creationThread)
            {
                throw new InvalidOperationException("Entity accessed outside of creation thread.");
            }
        }

        /// <summary>
        /// Throw if the entity has been marked as destroyed.
        /// </summary>
        private void CheckNotDestroyed()
        {
            if (this.destroyed)
            {
                throw new InvalidOperationException("Entity accessed after destruction.");
            }
        }

        /// <summary>
        /// Throw if the entity should not be accessed at this point.
        /// </summary>
        private void CheckAccess()
        {
            this.CheckThread();
            this.CheckNotDestroyed();
        }
    }
}
