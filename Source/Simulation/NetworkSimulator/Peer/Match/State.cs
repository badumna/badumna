﻿//------------------------------------------------------------------------------
// <copyright file="State.cs" company="Scalify">
//     Copyright (c) 2013 Scalify. All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

namespace NetworkSimulator.Peer
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Base class for states (using state pattern).
    /// </summary>
    internal abstract class State
    {
        /// <summary>
        /// Called periodically to update the state.
        /// </summary>
        /// <param name="deltaTime">Time elapsed since last update.</param>
        /// <returns>The new state after the update has executed.</returns>
        public abstract State Update(TimeSpan deltaTime);
    }
}
