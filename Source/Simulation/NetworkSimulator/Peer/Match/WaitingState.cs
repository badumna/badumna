﻿//------------------------------------------------------------------------------
// <copyright file="WaitingState.cs" company="Scalify">
//     Copyright (c) 2013 Scalify. All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

namespace NetworkSimulator.Peer
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// State for waiting before entering next state.
    /// </summary>
    internal class WaitingState : State
    {
        /// <summary>
        /// The time to wait in this state before transitioning.
        /// </summary>
        private readonly TimeSpan delay;

        /// <summary>
        /// The state to transition to after waiting.
        /// </summary>
        private readonly State nextState;

        /// <summary>
        /// The amount of time waited so far.
        /// </summary>
        private TimeSpan elapsed;

        /// <summary>
        /// Initializes a new instance of the WaitingState class.
        /// </summary>
        /// <param name="delay">The time to wait in this state before transitioning.</param>
        /// <param name="nextState">The state to transition to after waiting.</param>
        public WaitingState(TimeSpan delay, State nextState)
        {
            Console.WriteLine("Created waiting state");
            this.delay = delay;
            this.nextState = nextState;
        }

        /// <inheritdoc />
        public override State Update(TimeSpan deltaTime)
        {
            this.elapsed += deltaTime;
            if (this.elapsed > this.delay)
            {
                Console.WriteLine("Leaving waiting state");
                return this.nextState;
            }

            return this;
        }
    }
}
