﻿//------------------------------------------------------------------------------
// <copyright file="IEventHandler.cs" company="Scalify">
//     Copyright (c) 2013 Scalify. All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

namespace NetworkSimulator.Peer
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Interface specifying events that states must handle.
    /// </summary>
    internal interface IEventHandler
    {
    }
}
