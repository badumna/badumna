﻿//------------------------------------------------------------------------------
// <copyright file="MatchPeer.cs" company="National ICT Australia Limited">
//     Copyright (c) 2013 National ICT Australia Limited. All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

namespace NetworkSimulator.Peer
{
    using System;
    using Badumna.Match;
    using NetworkSimulator;

    /// <summary>
    /// A peer for testing match functionality.
    /// </summary>
    public class MatchPeer : BadumnaPeer
    {
        /// <summary>
        /// The simulator network facade.
        /// </summary>
        private ISimulatorFacade facade;
        
        /// <summary>
        /// The user name used by this peer when joining a match.
        /// </summary>
        private string userName;

        /////// <summary>
        /////// A state machine for governing peer behaviour.
        /////// </summary>
        ////private StateMachine stateMachine;

        /// <summary>
        /// The current state the peer is in (using state pattern).
        /// </summary>
        private State currentState;

        /// <summary>
        /// Number of seconds to wait before trying matchmaking.
        /// </summary>
        private double matchmakingDelaySeconds = 0;

        /// <summary>
        /// Initializes a new instance of the MatchPeer class.
        /// </summary>
        /// <param name="facade">The simulator network facade to use.</param>
        /// <param name="userName">The user name used by this peer when joining a match.</param>
        /// <param name="matchmakingDelaySeconds">The number of seconds to wait before trying matchmaking.</param>
        public MatchPeer(ISimulatorFacade facade, string userName, double matchmakingDelaySeconds)
            : base(facade)
        {
            this.userName = userName;
            this.facade = facade;
            this.matchmakingDelaySeconds = matchmakingDelaySeconds;
        }

        /// <inheritdoc/>
        protected override void OnStart()
        {
            base.OnStart();

            this.currentState = new WaitingState(
                TimeSpan.FromSeconds(this.matchmakingDelaySeconds),
                new QueryingState(this.facade.Match));
        }

        /// <inheritdoc/>
        protected override void OnExecute(TimeSpan duration)
        {
            this.currentState = this.currentState.Update(duration);
            base.OnExecute(duration);
        }

        /// <inheritdoc/>
        protected override void OnShutdown()
        {
            base.OnShutdown();
        }

        /// <inheritdoc/>
        protected override void OnReset()
        {
            base.OnReset();
        }
    }
}
