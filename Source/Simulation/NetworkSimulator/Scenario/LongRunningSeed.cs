﻿//------------------------------------------------------------------------------
// <copyright file="LongRunningSeed.cs" company="National ICT Australia Limited">
//     Copyright (c) 2012 National ICT Australia Limited. All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

namespace NetworkSimulator.Scenario
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;
    using Agency;
    using Badumna.DataTypes;
    using Badumna.DistributedHashTable;
    using NetworkSimulator.Peer;

    /// <summary>
    /// Test the scenario where a seed peer runs for a long time, has a large number of previous connections
    /// which are now disconnected.  Ensure time-to-discovery of spatial replicas is still within acceptable
    /// bounds.
    /// </summary>
    public class LongRunningSeed : SimpleScenario
    {
        /// <summary>
        /// The simulation.
        /// </summary>
        private Simulation simulation;

        /// <summary>
        /// The simulated peers.
        /// </summary>
        private List<AgentPeer> peers = new List<AgentPeer>();

        /// <summary>
        /// The seed peer.
        /// </summary>
        private ISimulatorFacade seedPeer;

        /// <summary>
        /// The time the peers were started.
        /// </summary>
        private TimeSpan startTime;

        /// <summary>
        /// Initializes a new instance of the LongRunningSeed class.
        /// </summary>
        public LongRunningSeed()
        {
            this.PeerCount = 15;
        }

        /// <summary>
        /// Reports the time taken until full discovery is made.  Triggered once per complete discovery.
        /// </summary>
        public event Action<TimeSpan> OnResult;

        /// <summary>
        /// Gets or sets the number of peers that should be started together.
        /// </summary>
        /// <remarks>
        /// This many peers will be started, then time-to-discovery will be 
        /// measured (the time until all peers see all entities).
        /// The process will then repeat.
        /// Defaults to 15.
        /// </remarks>
        public int PeerCount { get; set; }

        /// <inheritdoc />
        protected override void Configure(Simulation simulation)
        {
            simulation.NumberOfSeedPeers = 1;
            this.seedPeer = simulation.StartPeer(this.Options);  // seed peer
            this.seedPeer.Login();

            this.simulation = simulation;
            this.Supervisor();
        }

        /// <summary>
        /// Creates a new peer with an idling entity.
        /// </summary>
        /// <returns>The new peer</returns>
        private AgentPeer CreatePeer()
        {
            var facade = this.simulation.StartPeer(this.Options);
            var radius = 1.0f;
            var interestRadius = 10.0f;
            var startPosition = new Vector3();
            var behaviour = new Idler();
            var payloadSize = 0;
            var peer = new AgentPeer(
                facade,
                "scene",
                radius,
                interestRadius,
                startPosition,
                behaviour,
                payloadSize);
            peer.Start();
            return peer;
        }

        /// <summary>
        /// Orchestrates the simulation.
        /// </summary>
        /// <remarks>
        /// Repeatedly start pairs of peers, always with new addresses.  Time how long it takes
        /// for discovery of replicas each time.
        /// </remarks>
        private void Supervisor()
        {
#if false
            if (this.peers.Count == 0)
            {
                // Reset the routing tables
                Func<Type, string, FieldInfo> field = (type, name) => type.GetField(name, System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);
                Func<Type, object, string, object> getField = (type, obj, name) => field(type, name).GetValue(obj);
                Action<Type, object, string, object> setField = (type, obj, name, value) => field(type, name).SetValue(obj, value);

                var seed = (SimulatorFacade)this.seedPeer;
                foreach (var dht in seed.Stack.DhtFacades)
                {
                    if (dht == null)
                    {
                        continue;
                    }

                    var router = (TieredRouter)getField(typeof(DhtFacade), dht, "mRouter");
                    var routingTable = (RoutingTable)getField(typeof(Router), router, "mRoutingTable");
                    var rows = (int)getField(typeof(RoutingTable), routingTable, "mNumberOfRows");
                    var columns = (int)getField(typeof(RoutingTable), routingTable, "mNumberOfColumns");
                    setField(typeof(RoutingTable), routingTable, "mTable", new List<Badumna.DistributedHashTable.NodeInfo>[rows, columns]);
                }
            }
#endif

            while (this.peers.Count < this.PeerCount)
            {
                this.startTime = this.simulation.Now;
                this.peers.Add(this.CreatePeer());
            }

            if (this.peers.TrueForAll(p => p.RemoteAgents.Count == this.PeerCount - 1))
            {
                var discoveryTime = this.simulation.Now - this.startTime;
                var handler = this.OnResult;
                if (handler != null)
                {
                    handler(discoveryTime);
                }

                if (this.PeerCount < 60)
                {
                    this.PeerCount += 15;
                }
                else
                {
                    this.PeerCount = 15;

                    foreach (var peer in this.peers)
                    {
                        peer.Shutdown();
                        this.simulation.StopPeer(peer.NetworkFacade);
                    }

                    this.peers.Clear();
                }

                // 3 minutes between trials
                this.simulation.Schedule(TimeSpan.FromMinutes(3), this.Supervisor);
                return;
            }

            // 100ms between checks for discovery.
            this.simulation.Schedule(TimeSpan.FromMilliseconds(100), this.Supervisor);
        }
    }
}
