﻿//------------------------------------------------------------------------------
// <copyright file="SimpleScenario.cs" company="National ICT Australia Limited">
//     Copyright (c) 2012 National ICT Australia Limited. All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using Badumna;
using NetworkSimulator.Instrumentation;

namespace NetworkSimulator.Scenario
{
    /// <summary>
    /// A simple scenario, where all peers run for the same time, and are all configured with the same options.
    /// </summary>
    public class SimpleScenario : IScenario
    {
        /// <summary>
        /// Gets or sets the message tracer.
        /// </summary>
        public IMessageTracer MessageTracer { get; set; }

        /// <summary>
        /// Gets or sets the topology.
        /// </summary>
        public Topology Topology { get; set; }

        /// <summary>
        /// Gets or sets the size of the inbound and the outbound buffers in bytes.
        /// </summary>
        public double BufferSize { get; set; }

        /// <summary>
        /// Gets or sets the maximum inbound bandwidth in kilobits / second.
        /// </summary>
        public double InboundBandwidth { get; set; }

        /// <summary>
        /// Gets or sets the maximum outbound bandwidth in kilobits / second.
        /// </summary>
        public double OutboundBandwidth { get; set; }

        /// <summary>
        /// Gets or sets the probability a message will be successfully routed on each local network.
        /// </summary>
        public double Reliability { get; set; }

        /// <summary>
        /// Gets or sets the simulator facade options.
        /// </summary>
        public SimulatorFacadeOptions Options { get; set; }

        /// <inheritdoc />
        public Simulation Create()
        {
            this.Options = new SimulatorFacadeOptions
            {
                BufferSize = this.BufferSize,
                InboundBandwidth = this.InboundBandwidth,
                OutboundBandwidth = this.OutboundBandwidth,
                RouterId = -1, // random
                Options = new Options()
            };

            this.Options.Options.Connectivity.IsBroadcastEnabled = false;

            var simulation = new Simulation(
                this.Topology,
                routerId => new RouterOptions { NatType = 2, CanHairpin = true, Reliability = this.Reliability },
                this.MessageTracer);

            simulation.NumberOfSeedPeers = 1;

            this.Configure(simulation);

            return simulation;
        }

        /// <summary>
        /// Configures the simulation by scheduling any required events.
        /// </summary>
        /// <param name="simulation">The simulation to configure.</param>
        protected virtual void Configure(Simulation simulation)
        {
        }
    }
}
