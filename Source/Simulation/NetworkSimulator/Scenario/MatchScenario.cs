﻿//------------------------------------------------------------------------------
// <copyright file="MatchScenario.cs" company="National ICT Australia Limited">
//     Copyright (c) 2012 National ICT Australia Limited. All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System;

using Agency;
using Badumna;
using Badumna.DataTypes;

using NetworkSimulator.Instrumentation;
using NetworkSimulator.Peer;

namespace NetworkSimulator.Scenario
{
    /// <summary>
    /// Random walk scenario.
    /// </summary>
    public class MatchScenario : SimpleScenario
    {
        /// <summary>
        /// Gets or sets the number of peers to create.
        /// </summary>
        public int NumberOfPeers { get; set; }

        /// <summary>
        /// Gets or sets the number of peers allowed in a match.
        /// </summary>
        public int MatchSize { get; set; }

        /// <inheritdoc />
        protected override void Configure(Simulation simulation)
        {
            simulation.NumberOfSeedPeers = 1;
            var seedpeerFacade = simulation.StartPeer(this.Options);
            var seedPeer = new BadumnaPeer(seedpeerFacade);
            seedPeer.Start();

            this.Options.Options.Logger.LoggerType = LoggerType.File;
            this.Options.Options.Logger.LogLevel = LogLevel.Information;
            this.Options.Options.Logger.IncludeTags =
                LogTag.Match |
                LogTag.Autoreplication |
                LogTag.Simulator;

            this.Options.Options.Matchmaking.ActiveMatchLimit = 999;
            var matchmakerFacade = simulation.StartPeer(this.Options);
            var matchmaker = new BadumnaPeer(matchmakerFacade);
            matchmaker.Start();

            this.Options.Options.Matchmaking.ActiveMatchLimit = 0;
            this.Options.Options.Matchmaking.ServerAddress = matchmakerFacade.SocketAddress;

            Console.WriteLine("Starting {0} match peers.", this.NumberOfPeers);
            for (int i = 0; i < this.NumberOfPeers; ++i)
            {
                var facade = simulation.StartPeer(this.Options);
                var peer = new MatchPeer(facade, string.Format("user{0:000}", i), (i + 1) * 30);
                peer.Start();
            }
            
            ////for (int i = 0; i < this.NumberOfScenes; ++i)
            ////{
            ////    for (int j = 0; j < this.NumberOfPeersPerScene; ++j)
            ////    {
            ////        var facade = simulation.StartPeer(this.Options);

            ////        var startPosition = new Vector3(startCoordinate(), startCoordinate(), startCoordinate());

            ////        var walker = new RandomWalker(
            ////            this.Speed,
            ////            (i * this.NumberOfPeersPerScene) + j,
            ////            -this.ZoneSize,
            ////            this.ZoneSize,
            ////            -this.ZoneSize,
            ////            this.ZoneSize,
            ////            -this.ZoneSize,
            ////            this.ZoneSize);

            ////        var peer = new AgentPeer(
            ////            facade,
            ////            "scene-" + i,
            ////            this.Radius,
            ////            this.InterestRadius,
            ////            startPosition,
            ////            walker,
            ////            this.PayloadSize);

            ////        peer.Start();
            ////    }
            ////}
        }
    }
}
