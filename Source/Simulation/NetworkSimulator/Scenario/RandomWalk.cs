﻿//------------------------------------------------------------------------------
// <copyright file="RandomWalk.cs" company="National ICT Australia Limited">
//     Copyright (c) 2012 National ICT Australia Limited. All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System;

using Agency;
using Badumna;
using Badumna.DataTypes;

using NetworkSimulator.Instrumentation;
using NetworkSimulator.Peer;

namespace NetworkSimulator.Scenario
{
    /// <summary>
    /// Random walk scenario.
    /// </summary>
    public class RandomWalk : SimpleScenario
    {
        /// <summary>
        /// Gets or sets the number of scenes.
        /// </summary>
        public int NumberOfScenes { get; set; }

        /// <summary>
        /// Gets or sets the number of peers per scene.
        /// </summary>
        public int NumberOfPeersPerScene { get; set; }

        /// <summary>
        /// Gets or sets the size of the random walk.  The walk will extend to +/- ZoneSize in each dimension.
        /// </summary>
        public float ZoneSize { get; set; }

        /// <summary>
        /// Gets or sets the seed of the random number generator used to initialize the entity positions.
        /// </summary>
        public int PositionSeed { get; set; }

        /// <summary>
        /// Gets or sets the entity radius.
        /// </summary>
        public float Radius { get; set; }

        /// <summary>
        /// Gets or sets the interest radius.
        /// </summary>
        public float InterestRadius { get; set; }

        /// <summary>
        /// Gets or sets the entity speed.
        /// </summary>
        public float Speed { get; set; }

        /// <summary>
        /// Gets or sets the extra payload size.  This number of bytes is added to each entity update sent.
        /// </summary>
        public int PayloadSize { get; set; }

        /// <inheritdoc />
        protected override void Configure(Simulation simulation)
        {
            var random = new Random(this.PositionSeed);
            Func<float> startCoordinate = () => (float)(this.ZoneSize * (random.NextDouble() - 0.5) * 2.0);

            for (int i = 0; i < this.NumberOfScenes; ++i)
            {
                for (int j = 0; j < this.NumberOfPeersPerScene; ++j)
                {
                    var facade = simulation.StartPeer(this.Options);

                    var startPosition = new Vector3(startCoordinate(), startCoordinate(), startCoordinate());

                    var walker = new RandomWalker(
                        this.Speed,
                        (i * this.NumberOfPeersPerScene) + j,
                        -this.ZoneSize,
                        this.ZoneSize,
                        -this.ZoneSize,
                        this.ZoneSize,
                        -this.ZoneSize,
                        this.ZoneSize);

                    var peer = new AgentPeer(
                        facade,
                        "scene-" + i,
                        this.Radius,
                        this.InterestRadius,
                        startPosition,
                        walker,
                        this.PayloadSize);

                    peer.Start();
                }
            }
        }
    }
}
