﻿//------------------------------------------------------------------------------
// <copyright file="IScenario.cs" company="National ICT Australia Limited">
//     Copyright (c) 2012 National ICT Australia Limited. All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetworkSimulator.Scenario
{
    /// <summary>
    /// Provides a method to set up a Simulation for a particular scenario.
    /// </summary>
    public interface IScenario
    {
        /// <summary>
        /// Creates the scenario.
        /// </summary>
        /// <returns>The prepared simulation</returns>
        Simulation Create();
    }
}
