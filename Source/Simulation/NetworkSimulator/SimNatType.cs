﻿//------------------------------------------------------------------------------
// <copyright file="NatType.cs" company="National ICT Australia Limited">
//     Copyright (c) 2013 National ICT Australia Limited. All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

namespace NetworkSimulator
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// NAT types
    /// </summary>
    /// <remarks>
    /// This is here because Badumna.Core.NatType isn't exposed publicly.
    /// Kludgy yet effective.
    /// </remarks>
    public enum SimNatType
    {
        Unknown = 0,
        Blocked = 1,
        Open = 2,
        SymmetricFirewall = 3,
        FullCone = 4,
        SymmetricNat = 5,
        RestrictedPort = 6,
        RestrictedCone = 7,
        MangledFullCone = 8,
        Internal = 9,
        HashAddressable = 10
    }
}
