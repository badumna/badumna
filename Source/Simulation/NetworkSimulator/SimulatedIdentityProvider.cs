﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Security.Cryptography;

using Badumna;
using Badumna.Core;
using Badumna.Utilities;
using Badumna.Security;

namespace NetworkSimulator
{

    // Uses a simgle token suplier so we don't need to create an empty token (which is expensive) for every peer.
    class SimulatedIdentityProvider : IIdentityProvider
    {
        private static UnverifiedIdentityProvider mIdentityProvider;

        static SimulatedIdentityProvider()
        {
            SimulatedIdentityProvider.mIdentityProvider = new UnverifiedIdentityProvider();
        }

        public void Activate()
        {
        }

        public void Dispose()
        {
        }

        public SymmetricKeyToken GetNetworkParticipationToken()
        {
            return SimulatedIdentityProvider.mIdentityProvider.GetNetworkParticipationToken();
        }

        public PermissionListToken GetPermissionListToken()
        {
            return SimulatedIdentityProvider.mIdentityProvider.GetPermissionListToken();
        }

        public TimeToken GetTimeToken()
        {
            return SimulatedIdentityProvider.mIdentityProvider.GetTimeToken();
        }

        public TrustedAuthorityToken GetTrustedAuthorityToken()
        {
            return SimulatedIdentityProvider.mIdentityProvider.GetTrustedAuthorityToken();
        }

        public CertificateToken GetUserCertificateToken()
        {
            return SimulatedIdentityProvider.mIdentityProvider.GetUserCertificateToken();
        }


        #region IIdentityProvider Members

        public void Shutdown()
        {
        }

        #endregion
    }
}
