﻿//------------------------------------------------------------------------------
// <copyright file="SimulatorFacadeOptions.cs" company="National ICT Australia Limited">
//     Copyright (c) 2012 National ICT Australia Limited. All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Badumna;
using Badumna.Utilities;

namespace NetworkSimulator
{
    /// <summary>
    /// Container for all configurable options related to creating a simulator facade.
    /// </summary>
    public class SimulatorFacadeOptions
    {
        /// <summary>
        /// Initializes a new instance of the SimulatorFacadeOptions class.
        /// </summary>
        public SimulatorFacadeOptions()
        {
        }
        
        /// <summary>
        /// Initializes a new instance of the SimulatorFacadeOptions class as a deep copy
        /// of an existing instance.
        /// </summary>
        /// <param name="other">The instance to make a deep copy of.</param>
        public SimulatorFacadeOptions(SimulatorFacadeOptions other)
        {
            this.RouterId = other.RouterId;
            this.Options = new Options(other.Options);
            this.BufferSize = other.BufferSize;
            this.InboundBandwidth = other.InboundBandwidth;
            this.OutboundBandwidth = other.OutboundBandwidth;
            this.RandomNumberGenerator = other.RandomNumberGenerator;
        }

        /// <summary>
        /// Gets or sets the ID of the router to connect to, or -1 to use a random router.
        /// </summary>
        public int RouterId { get; set; }

        /// <summary>
        /// Gets or sets the Badumna options.
        /// </summary>
        public Options Options { get; set; }

        /// <summary>
        /// Gets or sets the size of the inbound and the outbound buffers in bytes.
        /// </summary>
        public double BufferSize { get; set; }

        /// <summary>
        /// Gets or sets the maximum inbound bandwidth in kilobits / second.
        /// </summary>
        public double InboundBandwidth { get; set; }

        /// <summary>
        /// Gets or sets the maximum outbound bandwidth in kilobits / second.
        /// </summary>
        public double OutboundBandwidth { get; set; }

        /// <summary>
        /// Gets or sets a random number generator to use in the peer.
        /// </summary>
        public IRandomNumberGenerator RandomNumberGenerator { get; set; }
    }
}
