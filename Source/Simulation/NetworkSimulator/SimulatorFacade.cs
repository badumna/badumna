//------------------------------------------------------------------------------
// <copyright file="SimulatorFacade.cs" company="National ICT Australia Limited">
//     Copyright (c) 2012 National ICT Australia Limited. All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;

using Badumna;
using Badumna.Chat;
using Badumna.Core;
using Badumna.Utilities;

using Diagnostics;
using Diagnostics.WolfEye;

using NetworkSimulator.Instrumentation;
using NetworkSimulator.ProtocolStacks;

namespace NetworkSimulator
{
    /// <summary>
    /// Simulated network facade (one per simulated peer).
    /// </summary>
    internal class SimulatorFacade : MainNetworkFacade, ISimulatorFacade
    {
        /// <summary>
        /// Used to inspect arbitrary fields inside the facade at runtime.
        /// </summary>
        private Eye eye;

        /// <summary>
        /// Initializes a new instance of the SimulatorFacade class.
        /// </summary>
        /// <param name="options">The options</param>
        /// <param name="transport">The transport</param>
        /// <param name="connectivityReporter">Connectivity reporter</param>
        /// <param name="eventQueue">The event queue</param>
        /// <param name="timeKeeper">The timekeeper</param>
        /// <param name="randomNumberGenerator">A random number generator.</param>
        private SimulatorFacade(
            Options options,
            SimulatedTransport transport,
            INetworkConnectivityReporter connectivityReporter,
            Simulator eventQueue,
            ITime timeKeeper,
            IRandomNumberGenerator randomNumberGenerator)
            : base(options, transport, eventQueue, timeKeeper, connectivityReporter, randomNumberGenerator)
        {
            if (options.Connectivity.IsBroadcastEnabled)
            {
                throw new ArgumentException("Broadcast is not yet supported in the simulator (it doesn't use the simulated transport)");
            }

            transport.SetSimulatorFacade(this);
            this.Transport = transport;

            this.eye = new Eye(this, null);

            this.Stack = new FullStack(
                this, options, transport, null, eventQueue, timeKeeper, connectivityReporter, randomNumberGenerator);
            this.Stack.Initialize();
        }

        /// <inheritdoc />
        public override InitializationState InitializationProgress
        {
            get { return InitializationState.Complete; }
        }

        /// <summary>
        /// Gets the simple send protocol.  Only valid in TransportOnly stacks.
        /// </summary>
        public NetworkSimulator.ProtocolStacks.Services.ISimpleSend SimpleSend { get; private set; }

        /// <inheritdoc />
        public string Address
        {
            get { return this.Transport.Address.ToString(); }
        }

        /// <inheritdoc />
        public string SocketAddress
        {
            get
            {
                return this.Transport.Address.Address.ToString()
                    + ":"
                    + this.Transport.Address.Port.ToString();
            }
        }

        /// <inheritdoc />
        public override bool IsOnline
        {
            get { return this.Transport.NetworkConnectivityReporter.Status == ConnectivityStatus.Online; }
        }

        /// <inheritdoc />
        public override bool IsOffline
        {
            get { return this.Transport.NetworkConnectivityReporter.Status == ConnectivityStatus.Offline; }
        }

        /// <summary>
        /// Gets the simulated transport.
        /// </summary>
        internal SimulatedTransport Transport { get; private set; }

        /// <inheritdoc />
        public T GetObject<T>(string path)
        {
            var objectPath = ObjectPath.FromString(path);
            if (objectPath == null)
            {
                throw new ArgumentException("path");
            }

            return (T)this.eye.GetObject(objectPath);
        }

        /// <inheritdoc />
        public IEnumerable<T> GetObjects<T>(string path)
        {
            var objectPath = ObjectPath.FromString(path);
            if (objectPath == null)
            {
                throw new ArgumentException("path");
            }

            return this.eye.GetObjects(objectPath).ConvertAll(x => (T)x);
        }

        /// <inheritdoc />
        public IStatistics GetStatistics()
        {
            return StatisticsHelper.CollectStatistics(this.Stack, this.eventQueue, this.timeKeeper.Now);
        }

        /// <inheritdoc />
        public override void Shutdown(bool blockUntilComplete)
        {
            // TODO: Figure out exactly what shutdown is required here.
        }

        /// <summary>
        /// Creates a new SimulatorFacade.
        /// </summary>
        /// <param name="simulatorOptions">Configurable parameters for the facade</param>
        /// <param name="publicAddress">The public address of the transport</param>
        /// <param name="privateAddress">The private address of the transport</param>
        /// <param name="router">The router the transport is connected to</param>
        /// <param name="messageTracer">The message tracer</param>
        /// <param name="eventQueue">The event queue</param>
        /// <param name="timeKeeper">The timekeeper</param>
        /// <returns>The new facade</returns>
        internal static SimulatorFacade Create(
            SimulatorFacadeOptions simulatorOptions,
            PeerAddress publicAddress,
            PeerAddress privateAddress,
            Router router,
            IMessageTracer messageTracer,
            Simulator eventQueue,
            ITime timeKeeper)
        {
            // Setup logging.
            if (simulatorOptions.Options.Logger != null)
            {
                Logger.Initialize(timeKeeper);
                Logger.Configure(simulatorOptions.Options.Logger);
            }

            ////simulatorOptions.Options.Validate();

            INetworkConnectivityReporter connectivityReporter = new NetworkConnectivityReporter(eventQueue);

            SimulatedTransport transport = new SimulatedTransport(
                simulatorOptions.BufferSize,
                simulatorOptions.InboundBandwidth,
                simulatorOptions.OutboundBandwidth,
                publicAddress,
                privateAddress,
                connectivityReporter,
                router,
                messageTracer,
                eventQueue,
                timeKeeper);

            var rng = simulatorOptions.RandomNumberGenerator ?? new RandomNumberGenerator();
            return new SimulatorFacade(simulatorOptions.Options, transport, connectivityReporter, eventQueue, timeKeeper, rng);
        }

        /// <inheritdoc />
        protected override void Initialize()
        {
            // Do nothing, in particular, don't do what the base class does.
        }

        /// <inheritdoc />
        protected override IChatSession CreateChatSession()
        {
            return this.Stack.CreateChatSession();
        }
    }
}
