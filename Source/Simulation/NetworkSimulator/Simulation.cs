//------------------------------------------------------------------------------
// <copyright file="Simulation.cs" company="National ICT Australia Limited">
//     Copyright (c) 2012 National ICT Australia Limited. All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;

using Badumna;
using Badumna.Core;
using Badumna.Transport;
using Badumna.Utilities;
using NetworkSimulator.Instrumentation;

namespace NetworkSimulator
{
    /// <summary>
    /// A simulation job.  This is the primary interface to the network simulator code.
    /// </summary>
    public class Simulation : ITime
    {
        /// <summary>
        /// The event queue.
        /// </summary>
        private readonly Simulator simulator;

        /// <summary>
        /// The backbone of the simulated network.
        /// </summary>
        private readonly Backbone backbone;

        /// <summary>
        /// The seed peers.
        /// </summary>
        private readonly List<string> seedPeers = new List<string>();
        
        /// <summary>
        /// Initializes a new instance of the Simulation class.
        /// </summary>
        /// <param name="topology">The topology</param>
        /// <param name="routerOptionsMap">A function mapping from router id to the options for that router</param>
        /// <param name="messageTracer">The message tracer</param>
        public Simulation(
            Topology topology,
            Func<int, RouterOptions> routerOptionsMap,
            IMessageTracer messageTracer)
        {
            NetworkSimulator.RandomSource.Initialize(0);  // TODO: Make seed configurable, and make this not a singleton.
            this.simulator = new Simulator();
            this.simulator.NewNetworkEvent += this.OnEventPerformed;
            this.backbone = new Backbone(topology, routerOptionsMap, this.simulator, this.simulator, messageTracer);
            this.Peers = new List<ISimulatorFacade>();
        }

        /// <summary>
        /// Invoked when a simulated event is performed.  The signature of the handler is 
        /// (string peerAddress, TimeSpan currentTime, string description).
        /// </summary>
        public event Action<string, TimeSpan, string> EventPerformed;

        /// <summary>
        /// Gets the list of peers.  The collection should not be mutated by users of this class (i.e. don't add, remove, or reorder elements).
        /// </summary>
        public List<ISimulatorFacade> Peers { get; private set; }

        /// <summary>
        /// Gets or sets the number of seed peers.
        /// The first NumberOfSeedPeers peers started will be added to the seed peer list for the other peers.
        /// </summary>
        public int NumberOfSeedPeers { get; set; }

        /// <summary>
        /// Gets the current simulated time.
        /// </summary>
        public TimeSpan Now
        {
            get { return this.simulator.Now; }
        }

        /// <summary>
        /// Creates a simulation with an Open seed peer and other peers as specified.
        /// </summary>
        /// <remarks>
        /// The seed peer is the first peer in Simulation.Peers.
        /// The remaining peers appear in the order specified.
        /// </remarks>
        /// <param name="latency">The latency of a single hop</param>
        /// <param name="messageTracer">The message tracer</param>
        /// <param name="clientNatTypes">The NAT types of the client (i.e. non-seed) peers</param>
        /// <returns>The simulation</returns>
        public static Simulation CreateWithPeers(TimeSpan latency, IMessageTracer messageTracer, params SimNatType[] clientNatTypes)
        {
            var natTypes = new List<SimNatType> { SimNatType.Open };  // First peer is the seed peer
            natTypes.AddRange(clientNatTypes);

            var topology = new Topology();
            topology.SetSimpleToplogy(natTypes.Count, (int)latency.TotalMilliseconds);
            topology.HasFullConnectivity = true;

            Func<int, RouterOptions> routerOptions = id => new RouterOptions
            {
                NatType = natTypes[id],
                CanHairpin = true,
                Reliability = 1.0
            };

            var simulation = new Simulation(topology, routerOptions, messageTracer);

            Func<int, SimulatorFacadeOptions> peerOptions = id =>
            {
                var options = new SimulatorFacadeOptions
                {
                    BufferSize = 640 * 1024,
                    InboundBandwidth = 640,
                    OutboundBandwidth = 640,
                    RouterId = id,
                    Options = new Badumna.Options()
                };

                options.Options.Connectivity.IsBroadcastEnabled = false;

                options.Options.Logger.LoggerType = Badumna.LoggerType.File;
                options.Options.Logger.LogLevel = Badumna.LogLevel.Information;
                options.Options.Logger.IncludeTags = LogTag.All;
                return options;
            };

            simulation.NumberOfSeedPeers = 1;

            var peers = Enumerable.Range(0, natTypes.Count).Select(i => simulation.StartPeer(peerOptions(i))).ToList();

            foreach (var peer in peers)
            {
                peer.Login();
            }

            return simulation;
        }

        /// <summary>
        /// Starts a new peer with the given options and appends it to <see cref="Peers"/>.
        /// </summary>
        /// <remarks>
        /// The new peer will be at the end of <see cref="Peers"/> when this method returns.
        /// </remarks>
        /// <param name="facadeOptions">The options for the peer.</param>
        /// <returns>The facade of the new peer</returns>
        public ISimulatorFacade StartPeer(SimulatorFacadeOptions facadeOptions)
        {
            var copiedOptions = new SimulatorFacadeOptions(facadeOptions);
            foreach (string seedPeer in this.seedPeers)
            {
                if (!copiedOptions.Options.Connectivity.SeedPeers.Contains(seedPeer))
                {
                    copiedOptions.Options.Connectivity.SeedPeers.Add(seedPeer);
                }
            }

            ISimulatorFacade facade = this.backbone.AllocateNode(copiedOptions);
            this.Peers.Add(facade);

            if (this.seedPeers.Count < this.NumberOfSeedPeers)
            {
                this.seedPeers.Add(((SimulatorFacade)facade).Transport.Address.EndPoint.ToString());
            }

            return facade;
        }

        /// <summary>
        /// Removes a peer from the simulated network.
        /// </summary>
        /// <param name="facade">The peer to remove</param>
        public void StopPeer(ISimulatorFacade facade)
        {
            this.backbone.RemoveNode(facade);
            this.Peers.Remove(facade);
        }

        /// <summary>
        /// Restart a peer at same address as a previously stopped one.
        /// </summary>
        /// <param name="facade">The facade of the stopped peer.</param>
        /// <param name="facadeOptions">Options for the new facade.</param>
        /// <returns>The facade of the new peer.</returns>
        public ISimulatorFacade RestartPeer(ISimulatorFacade facade, SimulatorFacadeOptions facadeOptions)
        {
            if (this.Peers.Contains(facade))
            {
                throw new ArgumentException("You cannot restart a peer that is currently running. It must be stopped first.");
            }

            var newFacade = this.backbone.RestoreNode(facade, facadeOptions);
            this.Peers.Add(newFacade);
            return newFacade;
        }

        /// <summary>
        /// Schedules an event on the simulator event queue.
        /// </summary>
        /// <param name="delay">Simulated time to wait before executing the action</param>
        /// <param name="handler">The action to execute</param>
        public void Schedule(TimeSpan delay, Action handler)
        {
            this.simulator.Schedule(delay, delegate(object o) { handler(); }, null);
        }

        /// <summary>
        /// Schedules an event to repeat every <paramref name="period"/> units of time.
        /// The first event occurs <paramref name="period"/> units of time from now.
        /// </summary>
        /// <param name="period">The time between each event</param>
        /// <param name="handler">The action to execute</param>
        public void Repeat(TimeSpan period, Action handler)
        {
            Action repeater = null;
            repeater = delegate
            {
                this.Schedule(period, repeater);
                handler();
            };

            this.Schedule(period, repeater);
        }

        /// <summary>
        /// Runs a single event from the simulator queue and returns a value indicating whether any events remain on the queue.
        /// </summary>
        /// <returns><c>true</c> if the queue contains more events, <c>false</c> otherwise</returns>
        public bool RunOne()
        {
            this.simulator.RunOne();
            return this.simulator.Count > 0;
        }

        /// <summary>
        /// Runs the simulator for the specified amount of time.
        /// </summary>
        /// <param name="duration">The time to run for</param>
        public void RunFor(TimeSpan duration)
        {
            if (this.simulator.Suspend)
            {
                throw new InvalidOperationException("Simulator was suspended for an unknown reason");
            }

            this.simulator.RunFor(duration);
        }

        /// <summary>
        /// Runs the simulator until either condition is true or allowedTime has elapsed.
        /// The return value indicates the amount of time elapsed.
        /// </summary>
        /// <remarks>
        /// If condition doesn't become true then the simulated time on exit may be much greater
        /// than the time at start + allowedTime, depending on how soon the first event occurs
        /// after the allowedTime.
        /// </remarks>
        /// <param name="condition">Termination condition</param>
        /// <param name="allowedTime">Allowed time</param>
        /// <param name="failureAction">Action to execute if condition doesn't become true within allowedTime</param>
        /// <returns>Simulated time elapsed during this method call</returns>
        public TimeSpan RunUntil(Func<bool> condition, TimeSpan allowedTime, Action failureAction)
        {
            return this.simulator.RunUntil(() => condition(), allowedTime, () => failureAction());
        }

        /// <summary>
        /// Invokes <see cref="RunUntil(GenericCallBackReturn{bool}, TimeSpan, GenericCallBack)"/> with no
        /// <c>failureAction</c>.
        /// </summary>
        /// <param name="condition">Termination condition</param>
        /// <param name="allowedTime">Allowed time</param>
        /// <returns>Simulated time elapsed during this method call</returns>
        public TimeSpan RunUntil(Func<bool> condition, TimeSpan allowedTime)
        {
            return this.simulator.RunUntil(() => condition(), allowedTime);
        }

        /// <summary>
        /// Returns an enumeration of the specified object from each peer.
        /// </summary>
        /// <typeparam name="T">The type of object to return</typeparam>
        /// <param name="path">The path to the object on the peer</param>
        /// <returns>The enumeration of objects</returns>
        public IEnumerable<T> GetObjectFromEachPeer<T>(string path)
        {
            return from peer in this.Peers select peer.GetObject<T>(path);
        }

        /// <summary>
        /// Returns an enumeration of the specified objects from each peer.
        /// </summary>
        /// <typeparam name="T">The type of object to return</typeparam>
        /// <param name="path">The path to the objects on the peer</param>
        /// <returns>The enumeration of objects</returns>
        public IEnumerable<IEnumerable<T>> GetObjectsFromEachPeer<T>(string path)
        {
            return from peer in this.Peers select peer.GetObjects<T>(path);
        }

        /// <summary>
        /// Invoked when the simulator performs an event.
        /// </summary>
        /// <param name="peerAddress">The peer address</param>
        /// <param name="currentTime">The current time</param>
        /// <param name="description">The event description</param>
        private void OnEventPerformed(string peerAddress, TimeSpan currentTime, string description)
        {
            Action<string, TimeSpan, string> handler = this.EventPerformed;
            if (handler != null)
            {
                handler(peerAddress, currentTime, description);
            }
        }
    }
}
