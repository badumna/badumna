//------------------------------------------------------------------------------
// <copyright file="Backbone.cs" company="National ICT Australia Limited">
//     Copyright (c) 2012 National ICT Australia Limited. All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Diagnostics;

using Badumna;
using Badumna.Core;
using Badumna.Utilities;

using NetworkSimulator.Instrumentation;

namespace NetworkSimulator
{
    /// <summary>
    /// Represents the backbone of the simulated network.
    /// The backbone connects routers together, and can route messages to the appropriate router.
    /// </summary>
    public class Backbone
    {
        /// <summary>
        /// The simulation event queue
        /// </summary>
        private Simulator eventQueue;

        /// <summary>
        /// The time keeper
        /// </summary>
        private ITime timeKeeper;

        /// <summary>
        /// The network topology
        /// </summary>
        private Topology topology;

        /// <summary>
        /// The message tracer
        /// </summary>
        private IMessageTracer messageTracer;

        /// <summary>
        /// The subnet routers
        /// </summary>
        private List<Router> routers = new List<Router>();

        /// <summary>
        /// Initializes a new instance of the Backbone class.
        /// </summary>
        /// <param name="topology">The network topology</param>
        /// <param name="options">A function that takes a router id and returns the options for the corresponding router</param>
        /// <param name="eventQueue">The simulation event queue</param>
        /// <param name="timeKeeper">The timekeeper</param>
        /// <param name="messageTracer">The message tracer</param>
        public Backbone(Topology topology, Func<int, RouterOptions> options, object eventQueue, object timeKeeper, IMessageTracer messageTracer)
        {
            this.topology = topology;
            this.eventQueue = (Simulator)eventQueue;
            this.timeKeeper = (ITime)timeKeeper;
            this.messageTracer = messageTracer;

            for (int i = 0; i < this.topology.Count; ++i)
            {
                this.routers.Add(new Router(i, this, options(i), messageTracer, this.eventQueue, this.timeKeeper));
            }
        }

        /// <summary>
        /// Gets the number of routers connected to the backbone.
        /// </summary>
        public int RouterCount
        {
            get { return this.routers.Count; }
        }

        /// <summary>
        /// A NAT type allocator that allocates a random NAT type to each router.
        /// </summary>
        /// <param name="routerId">The id of the router (ignored)</param>
        /// <returns>A random NAT type</returns>
        public static object RandomNatAllocator(int routerId)
        {
            NatType[] types =
            {
                NatType.FullCone,
                NatType.Open,
                NatType.RestrictedCone,
                NatType.RestrictedPort,
              ////  NatType.SymmetricFirewall,   DGC: Not sure why this one is disabled.
                NatType.SymmetricNat,
                NatType.MangledFullCone
            };

            return types[RandomSource.Generator.Next(types.Length)];
        }

        /// <summary>
        /// Creates a node connected to the specified router.
        /// </summary>
        /// <param name="simulatorOptions">Configurable parameters for the facade</param>
        /// <returns>The facade for the new node</returns>
        public ISimulatorFacade AllocateNode(SimulatorFacadeOptions simulatorOptions)
        {
            int routerId = simulatorOptions.RouterId;
            if (routerId == -1)
            {
                routerId = RandomSource.Generator.Next(this.RouterCount);
            }

            return this.routers[routerId].AddLocalNode(simulatorOptions);
        }

        /// <summary>
        /// Disconnects the node from its router.
        /// </summary>
        /// <param name="facade">The facade to disconnect</param>
        public void RemoveNode(ISimulatorFacade facade)
        {
            var router = this.routers[this.GetRouterId(((SimulatorFacade)facade).Transport.PrivateAddress)];
            router.RemoveLocalNode(facade);
        }

        /// <summary>
        /// Create a node with the same address as a previously disconnected node.
        /// </summary>
        /// <param name="facade">THe previsously disconnected facade.</param>
        /// <param name="simulatorOptions">Configuration options for the facade.</param>
        /// <returns>The facade for the new node.</returns>
        public ISimulatorFacade RestoreNode(ISimulatorFacade facade, SimulatorFacadeOptions simulatorOptions)
        {
            var router = this.routers[this.GetRouterId(((SimulatorFacade)facade).Transport.PrivateAddress)];
            return router.RestoreLocalNode(facade, simulatorOptions);
        }

        /// <summary>
        /// Routes the message to the router responsible for the destination address.
        /// </summary>
        /// <param name="messageWrapper">The message to route</param>
        internal void RouteMessage(MessageWrapper messageWrapper)
        {
            PeerAddress sourceAddress = messageWrapper.Source;
            PeerAddress destinationAddress = messageWrapper.Destination;

            int sourceIndex = this.GetRouterId(sourceAddress);
            int destinationIndex = this.GetRouterId(destinationAddress);

            Debug.Assert(
                sourceIndex < this.routers.Count,
                String.Format("Index {0} from address {1} is invalid", sourceIndex, sourceAddress));

            Debug.Assert(
                destinationIndex < this.routers.Count,
                String.Format("Index {0} from address {1} is invalid", destinationIndex, destinationAddress));

            if (this.routers.Count <= destinationIndex || destinationIndex < 0)
            {
                Logger.TraceWarning(LogTag.Simulator, "Incorrect domain index {0} for address {1}", destinationIndex, destinationAddress);
                return;
            }

            Router domain = this.routers[destinationIndex];
            int latency = this.topology.NextLatencyBetween(sourceIndex, destinationIndex);

            if (latency < 0)
            {
                if (this.messageTracer != null)
                {
                    string reason = String.Format("No connectivity between {0} and {1}", sourceAddress.Address, destinationAddress.Address);
                    if (this.messageTracer.TraceRouters)
                    {
                        messageWrapper.Drop(messageWrapper.LastHop, reason);
                    }
                    else
                    {
                        messageWrapper.Drop(reason);
                    }
                }

                return;
            }

            this.eventQueue.Schedule(latency, domain.RouteMessage, messageWrapper);
        }

        /// <summary>
        /// Gets the router id from the peer address.
        /// </summary>
        /// <param name="address">The peer address</param>
        /// <returns>The router id</returns>
        private int GetRouterId(PeerAddress address)
        {
            byte[] bytes = address.Address.GetAddressBytes();
            Array.Reverse(bytes);

            // Reversed from network order, first 2 bytes become last two.
            return (int)BitConverter.ToInt16(bytes, 2);
        }
    }
}
