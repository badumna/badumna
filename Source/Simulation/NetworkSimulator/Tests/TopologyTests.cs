﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using NetworkSimulator;

namespace NetworkSimulatorTests
{
    class MockDomain : IDomain
    {
        public IMessageTracer MessageTracer { get; set; }

        private int mNumber;
        public int Number { get { return this.mNumber; } }

        private int mArrivalCount;
        public int ArrivalCount { get { return this.mArrivalCount; } }

        private NatType mNatType;

        private static int mTotalArrivalCount;
        public static int TotalArrivalCount
        {
            get { return MockDomain.mTotalArrivalCount; }
            set { MockDomain.mTotalArrivalCount = value; }
        }

        private NetworkEventQueue eventQueue;
        private ITime timeKeeper;

        public MockDomain(int number, Backbone topology, NatType natType, NetworkEventQueue eventQueue, ITime timeKeeper)
        {
            this.mNumber = number;
            this.mNatType = natType;
            this.eventQueue = eventQueue;
            this.timeKeeper = timeKeeper;
        }

        public void RouteMessage(MessageWrapper message)//TransportEnvelope envelope, PeerAddress source, PeerAddress destination)
        {
            MockDomain.mTotalArrivalCount++;
            this.mArrivalCount++;
        }

        public Node AddNode()
        {
            PeerAddress address = new PeerAddress(System.Net.IPAddress.Any, 0, this.mNatType);
            return new Node(address, address, this, null, this.eventQueue, this.timeKeeper);
        }

        public void RemoveNode(Node node)
        {
        }

        public void ClearAll()
        {
            this.mArrivalCount = 0;
        }

        public void DropMessage(MessageWrapper messageWrapper, string reason)
        {
        }
    }

    [TestFixture]
    public class TopologyTests
    {
        private Backbone mTopology;
        private int mNumAllocatedNodes = 0;
        private NatType mNextNatType = NatType.Blocked;

        private List<NatType> mNatTypeList;

        private Simulator eventQueue;

        [SetUp]
        public void Initialize()
        {
            //  Results.Instance.Clear();
            this.eventQueue = new Simulator();

            MockDomain.TotalArrivalCount = 0;
            this.mNumAllocatedNodes = 0;

            ITime timeKeeper = this.eventQueue;
            this.mTopology = new Backbone(new Options(), new SimulatorWrapper(this.eventQueue), new TimeKeeperWrapper(timeKeeper));
        }

        [TearDown]
        public void Terminate()
        {
            this.mTopology = null;
        }

        [Test]
        public void HarnessTest()
        {
        }

        [Test]
        public void LoadTest()
        {
            // Test that we can load the topology.
            this.mTopology.Load<MockDomain>("TestTopology.txt");
            // Assert.AreEqual("Test", Results.Instance.GetValue("topology"));

            // Test that the correct number of domains are loaded
            Assert.AreEqual(3, this.mTopology.NumDomains);

            // Test that the correct latencies were loaded.
            Assert.AreEqual(52, this.mTopology.LatencyBetween(1, 2));
            Assert.AreEqual(56, this.mTopology.LatencyBetween(1, 3));
            Assert.AreEqual(16, this.mTopology.LatencyBetween(2, 3));

            Assert.AreEqual(52, this.mTopology.LatencyBetween(2, 1));
            Assert.AreEqual(56, this.mTopology.LatencyBetween(3, 1));
            Assert.AreEqual(16, this.mTopology.LatencyBetween(3, 2));
        }

        [Test]
        public void LatencyTest()
        {
            this.mTopology.Load<Domain>("TestTopology.txt");

            Node firstNode = this.mTopology.GetDomain(1).AddNode();
            Node secondNode = this.mTopology.GetDomain(2).AddNode();
            Node thirdNode = this.mTopology.GetDomain(3).AddNode();

            Assert.AreEqual(52, this.mTopology.LatencyBetween(firstNode.Address, secondNode.Address));
            Assert.AreEqual(56, this.mTopology.LatencyBetween(firstNode.Address, thirdNode.Address));
            Assert.AreEqual(16, this.mTopology.LatencyBetween(secondNode.Address, thirdNode.Address));

            Assert.AreEqual(52, this.mTopology.LatencyBetween(secondNode.Address, firstNode.Address));
            Assert.AreEqual(56, this.mTopology.LatencyBetween(thirdNode.Address, firstNode.Address));
            Assert.AreEqual(16, this.mTopology.LatencyBetween(thirdNode.Address, secondNode.Address));
        }

        [Test, ExpectedException(typeof(FileNotFoundException))]
        public void LoadFailTest()
        {
            // Test that failing to load throws an exception.
            this.mTopology.Load<MockDomain>("not a file");
        }

        [Test]
        public void LoadTestTwo()
        {
            this.mTopology.Load<MockDomain>("mking-t.txt");
            //  Assert.AreEqual("E2EGraph", Results.Instance.GetValue("topology"));
            Assert.AreEqual(1740, this.mTopology.NumDomains);
        }

        [Test]
        public void GetDomainTest()
        {
            this.LoadTest();
            // Test that we can get a domain.
            Assert.AreEqual(1, (this.mTopology.GetDomain(1) as MockDomain).Number);
        }

        [Test]
        public void GetDomainTest2()
        {
            this.LoadTest();

            // Test that we can get a domain.
            PeerAddress address = PeerAddress.GetAddress("0.1.0.0", 0);
            Assert.AreEqual(1, (this.mTopology.GetDomain(address) as MockDomain).Number);
        }

        [Test]
        public void RouteTest()
        {
            this.LoadTest();
            this.PerformRouteTest();
        }

        public void PerformRouteTest()
        {
            PeerAddress a = PeerAddress.GetAddress("0.1.0.0", 0);
            PeerAddress b = PeerAddress.GetAddress("0.2.0.0", 0);
            PeerAddress c = PeerAddress.GetAddress("0.3.0.0", 0);

            TransportEnvelope envelope = new TransportEnvelope(a, QualityOfService.Reliable);

            int minLatency = Math.Min(this.mTopology.LatencyBetween(1, 3), this.mTopology.LatencyBetween(3, 2));
            int maxLatency = Math.Max(this.mTopology.LatencyBetween(1, 3), this.mTopology.LatencyBetween(3, 2));

            ITime timeKeeper = this.eventQueue;
            MessageWrapper messageA = new MessageWrapper(envelope, a, c, null, 0, timeKeeper);
            MessageWrapper messageB = new MessageWrapper(envelope, c, b, null, 0, timeKeeper);

            // Test that we can route a message over the topology.
            this.mTopology.RouteMessage(messageA);
            this.mTopology.RouteMessage(messageB);

            this.eventQueue.RunFor(TimeSpan.FromMilliseconds(minLatency - 1));
            //   Assert.AreEqual(0, Results.Instance.GetCount("arrivals"));

            // Test that the messages arrive correctly.
            // Test that the messages took the correct amount of time to arrive.
            this.eventQueue.RunFor(TimeSpan.FromMilliseconds(minLatency));

            Assert.AreEqual(1, MockDomain.TotalArrivalCount);
            Assert.AreEqual(0, (this.mTopology.GetDomain(1) as MockDomain).ArrivalCount);
            Assert.AreEqual(1, (this.mTopology.GetDomain(2) as MockDomain).ArrivalCount);
            Assert.AreEqual(0, (this.mTopology.GetDomain(3) as MockDomain).ArrivalCount);

            this.eventQueue.RunFor(TimeSpan.FromMilliseconds(maxLatency));
            Assert.AreEqual(2, MockDomain.TotalArrivalCount);
            Assert.AreEqual(0, (this.mTopology.GetDomain(1) as MockDomain).ArrivalCount);
            Assert.AreEqual(1, (this.mTopology.GetDomain(2) as MockDomain).ArrivalCount);
            Assert.AreEqual(1, (this.mTopology.GetDomain(3) as MockDomain).ArrivalCount);
        }

        public void InitializeNode(ISimulatorFacade facade, int i)
        {
            Assert.AreEqual(this.mNextNatType, ((SimulatorFacade)facade).Node.Address.NatType);
            this.mNumAllocatedNodes++;
        }

        public void InitializeNodeFail(ISimulatorFacade facade, int i)
        {
            throw new Exception("test exception");
        }

        [Test]
        public void AllocateTest()
        {
            this.mNextNatType = NatType.RestrictedPort;
            this.mTopology.SetNextAllocatedDomainsNat(this.mNextNatType);
            this.mTopology.Load<MockDomain>("TestTopology.txt");

            // Test that we can allocate a number of nodes 
            // Test that the InitializeNode event is called.
            // Test that the correct Nat type is assigned.

            this.mTopology.RandomlyAllocateNodes(5, this.InitializeNode);

            Assert.AreEqual(5, this.mNumAllocatedNodes);
        }

        [Test]
        public void AllocFailTest()
        {
            this.mTopology.Load<MockDomain>("TestTopology.txt");

            // Test that we can allocate a number of nodes 
            // Test that the InitializeNode event is called.

            this.mTopology.RandomlyAllocateNodes(5, this.InitializeNodeFail);
        }

        [Test]
        public void ResetTest()
        {
            this.RouteTest();

            // Test that reset 
            this.mTopology.Reset();
            MockDomain.TotalArrivalCount = 0;

            this.PerformRouteTest();
        }

        [Test]
        public void RandomlySelectNatTypeTest()
        {
            this.mNatTypeList = new List<NatType>();

            this.mTopology.RandomNatSelection = true;
            this.mTopology.Load<MockDomain>("mking-t.txt");

            this.mTopology.RandomlyAllocateNodes(100, new InitializeNodeDelegate(mTopology_NodeInitializer));

            // Test that given enough allocated nodes randomly selecting the Nat type will choose all types.
            Assert.AreEqual(5, this.mNatTypeList.Count);
        }

        void mTopology_NodeInitializer(ISimulatorFacade facade, int nthNode)
        {
            if (!this.mNatTypeList.Contains(((SimulatorFacade)facade).Node.Address.NatType))
            {
                this.mNatTypeList.Add(((SimulatorFacade)facade).Node.Address.NatType);
            }
            Assert.Fail();   // RandomlySetNextAllocatedNodesNat no longer exists
            // this.mTopology.RandomlySetNextAllocatedNodesNat();
        }

    }
}
