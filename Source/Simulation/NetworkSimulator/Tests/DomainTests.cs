﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;

namespace NetworkSimulatorTests
{
    [TestFixture]
    public class DomainTests
    {
        private Domain mDomain;

        private TestProtocolLayer2<TransportEnvelope> mSourceLayer2;
        private TestProtocolLayer2<TransportEnvelope> mDestinationLayer2;

        [SetUp]
        public void Initialize()
        {
            Results.Instance.Clear();
            Simulator.Instance.Reset();
            Domain.Reliability = 1.0;
            this.mDomain = new Domain(1, null, NatType.Unknown);
            this.mSourceLayer2 = null;
            this.mDestinationLayer2 = null;
        }

        [TearDown]
        public void Terminate()
        {
            this.mDomain = null;
        }     

        [Test]
        public void AddNodeTest()
        {
            // Test that we can add a node.
            Node nodeA = this.mDomain.AddNode();
            Node nodeB = this.mDomain.AddNode();

            // Test that we can determine if a node is in the domain.
            Assert.IsTrue(this.mDomain.IsInDomain(nodeA.Address));
            Assert.IsTrue(this.mDomain.IsInDomain(nodeB));            
        }

        [Test]
        public void IsInDomainTest()
        {
            this.mDomain.AddNode();

            // Test that an address not in the domain returns false from IsInDomain
            Assert.IsFalse(this.mDomain.IsInDomain(PeerAddress.GetAddress("0.99.0.0", 99)));
            Assert.IsFalse(this.mDomain.IsInDomain(PeerAddress.GetAddress("128.250.34.80", 0)));            
        }

        [Test]
        public void RemoveNodeTest()
        {
            Node nodeA = this.mDomain.AddNode();
            Node nodeB = this.mDomain.AddNode();

            Assert.IsTrue(this.mDomain.IsInDomain(nodeA.Address));
            Assert.IsTrue(this.mDomain.IsInDomain(nodeB));         

            // Test that we remove a node.
            this.mDomain.RemoveNode(nodeA);
            Assert.IsFalse(this.mDomain.IsInDomain(nodeA));
        }

        [Test]
        public void RemoveFailTest()
        {
            // Test that attempting to remove an unknown node does nothing.
            Assert.AreEqual(0, this.mDomain.NodeCount);
            this.mDomain.RemoveNode(new Node(PeerAddress.Nowhere, PeerAddress.Nowhere, null));
            Assert.AreEqual(0, this.mDomain.NodeCount);

            Node a = this.mDomain.AddNode();
            Assert.AreEqual(1, this.mDomain.NodeCount);

            this.mDomain.RemoveNode(a);
            Assert.AreEqual(0, this.mDomain.NodeCount);
            this.mDomain.RemoveNode(a);
            Assert.AreEqual(0, this.mDomain.NodeCount);
        }

        [Test]
        public void AddRemoveNodeTest()
        {

            // Test that we can add and remove multiple nodes.
            Node a = this.mDomain.AddNode();
            Node b = this.mDomain.AddNode();
            Node c = this.mDomain.AddNode();
            Node d = this.mDomain.AddNode();

            Assert.AreEqual(4, this.mDomain.NodeCount);
            Assert.IsTrue(this.mDomain.IsInDomain(a));
            Assert.IsTrue(this.mDomain.IsInDomain(b));
            Assert.IsTrue(this.mDomain.IsInDomain(c));
            Assert.IsTrue(this.mDomain.IsInDomain(d));

            this.mDomain.RemoveNode(b);
            Assert.AreEqual(3, this.mDomain.NodeCount);
            Assert.IsTrue(this.mDomain.IsInDomain(a));
            Assert.IsFalse(this.mDomain.IsInDomain(b));
            Assert.IsTrue(this.mDomain.IsInDomain(c));
            Assert.IsTrue(this.mDomain.IsInDomain(d));

            Node e = this.mDomain.AddNode();
            Assert.AreEqual(4, this.mDomain.NodeCount);
            Assert.IsTrue(this.mDomain.IsInDomain(a));
            Assert.IsFalse(this.mDomain.IsInDomain(b));
            Assert.IsTrue(this.mDomain.IsInDomain(c));
            Assert.IsTrue(this.mDomain.IsInDomain(d));
            Assert.IsTrue(this.mDomain.IsInDomain(e));

            this.mDomain.RemoveNode(a);
            this.mDomain.RemoveNode(e);
            Assert.AreEqual(2, this.mDomain.NodeCount);
            Assert.IsFalse(this.mDomain.IsInDomain(a));
            Assert.IsFalse(this.mDomain.IsInDomain(b));
            Assert.IsTrue(this.mDomain.IsInDomain(c));
            Assert.IsTrue(this.mDomain.IsInDomain(d));
            Assert.IsFalse(this.mDomain.IsInDomain(e));
        }


        [Test]
        public void GetNodeTest()
        {
            // Test that we can add a node.
            Node nodeA = this.mDomain.AddNode();

            // Test that we can determine if a node is in the domain.
            Assert.AreEqual(nodeA.Address, this.mDomain.GetNode(nodeA.Address).Address);
        }

        [Test]
        public void GetNodeFailTest()
        {
            // Test that we can add a node.

            // Test that we can determine if a node is in the domain.
            Assert.AreEqual(null, this.mDomain.GetNode(PeerAddress.GetAddress("128.250.34.80", 0)));
        }

        [Test]
        public void LocalRouteTest()
        {
            Node source = this.mDomain.AddNode();
            Node destination = this.mDomain.AddNode();

            TestProtocolLayer2 sourceLayer2 = new TestProtocolLayer2(source);
            TestProtocolLayer2 destinationLayer2 = new TestProtocolLayer2(destination);

            TransportEnvelope envelope = sourceLayer2.GetMessageFor(destination.Address, QualityOfService.Unreliable);

            source.BringOnline();
            destination.BringOnline();

            Simulator.Instance.RunFor(TimeSpan.FromSeconds(1));

            // Test that a message can be routed within the domain.
            this.mDomain.RouteMessage(envelope, source.Address, destination.Address);

            // Test that the message is delived with the correct delay.
            Simulator.Instance.RunFor(TimeSpan.FromMilliseconds(Domain.InDomainLatencyMs-1));
            Assert.AreEqual(0, destinationLayer2.MessagesArrivedCount);

            Simulator.Instance.RunFor(TimeSpan.FromMilliseconds(Domain.InDomainLatencyMs));
            Assert.AreEqual(1, destinationLayer2.MessagesArrivedCount);
        }


        [Test]
        public void InterDomainRouteTest()
        {
            Topology topology = new Topology();

            topology.Load<Domain>("TestTopology.txt");

            Node source = topology.GetDomain(1).AddNode();
            Node destination = topology.GetDomain(2).AddNode();
            int latency = Domain.InDomainLatencyMs + topology.LatencyBetween(1, 2);

            Domain domain1 = topology.GetDomain(1) as Domain;
            Domain domain2 = topology.GetDomain(2) as Domain;

            Assert.IsTrue(domain1.IsInDomain(source));
            Assert.IsTrue(domain2.IsInDomain(destination));
            Assert.IsFalse(domain1.IsInDomain(destination));
            Assert.IsFalse(domain2.IsInDomain(source));

            TestProtocolLayer2 sourceLayer2 = new TestProtocolLayer2(source);
            TestProtocolLayer2 destinationLayer2 = new TestProtocolLayer2(destination);

            source.BringOnline();
            destination.BringOnline();

            Simulator.Instance.RunFor(TimeSpan.FromSeconds(1));

            // Test that a message for another domain is routed correctly.
            TransportEnvelope envelope = sourceLayer2.GetMessageFor(destination.Address, QualityOfService.Unreliable);

            topology.GetDomain(1).RouteMessage(envelope, source.Address, destination.Address);
            
            // Test that the message is delived with the correct delay.
            Simulator.Instance.RunFor(TimeSpan.FromMilliseconds(latency - 1));
            Assert.AreEqual(0, destinationLayer2.MessagesArrivedCount);

            Simulator.Instance.RunFor(TimeSpan.FromMilliseconds(latency));
            Assert.AreEqual(1, destinationLayer2.MessagesArrivedCount);
        }


        [Test]
        public void ReliabilityTest()
        {
            Node nodeA = this.mDomain.AddNode();
            Node nodeB = this.mDomain.AddNode();

            // Set bandwidth very large so it doesn't effect the result.
            nodeA.OutboundBandwidth = 100000;
            nodeA.InboundBandwidth = 100000;
            nodeB.OutboundBandwidth = 100000;
            nodeB.InboundBandwidth = 100000;

            int numSent = 500;
            TestProtocolLayer2 sourceLayer2 = new TestProtocolLayer2(nodeA);
            TestProtocolLayer2 destinationLayer2 = new TestProtocolLayer2(nodeB);

            nodeA.BringOnline();
            nodeB.BringOnline();

            Simulator.Instance.RunFor(TimeSpan.FromSeconds(1));

            TransportEnvelope envelope = sourceLayer2.GetMessageFor(nodeB.Address, QualityOfService.Unreliable);

            // Test that the reliability field for domains is accurate.
            Domain.Reliability = 0.3;

            for (int i = 0; i < numSent; i++)
            {
                sourceLayer2.SendMessage(envelope);
            }

            Simulator.Instance.RunFor(TimeSpan.FromMilliseconds(100));

            Assert.AreEqual(numSent, nodeA.NumSentMessages);

            double percentageArrived = ((double)destinationLayer2.MessagesArrivedCount / (double)numSent) * 100.0;

            System.Console.WriteLine("" + percentageArrived + "% messages arrived.");

            Assert.IsTrue(20 < percentageArrived, "Too few messages arrived.");
            Assert.IsTrue(40 > percentageArrived, "Too many messages arrived.");            
        }

        private int SendAToB(Node source, Node destination)
        {
            if (null == this.mSourceLayer2)
            {
                this.mSourceLayer2 = new TestProtocolLayer2(source);
            }

            if (null == this.mDestinationLayer2)
            {
                this.mDestinationLayer2 = new TestProtocolLayer2(destination);
            }

            Simulator.Instance.Reset();

            TransportEnvelope envelope = this.mSourceLayer2.GetMessageFor(destination.Address, QualityOfService.Unreliable);

            // Test that a message can be routed within the domain.
            this.mSourceLayer2.SendMessage(envelope);

            Assert.AreEqual(0, this.mDestinationLayer2.MessagesArrivedCount);
            Simulator.Instance.RunFor(TimeSpan.FromMilliseconds(Domain.InDomainLatencyMs*2));
            System.Console.WriteLine("arrived = " + this.mDestinationLayer2.MessagesArrivedCount);
            return this.mDestinationLayer2.MessagesArrivedCount;
        }

        private int SendBToA(Node source, Node destination)
        {
            if (null == this.mSourceLayer2)
                this.mSourceLayer2 = new TestProtocolLayer2(source);

            if (null == this.mDestinationLayer2)
                this.mDestinationLayer2 = new TestProtocolLayer2(destination);

            Simulator.Instance.Reset();

            TransportEnvelope envelope = this.mDestinationLayer2.GetMessageFor(source.Address, QualityOfService.Unreliable);

            // Test that a message can be routed within the domain.
            this.mDestinationLayer2.SendMessage(envelope);

            Assert.AreEqual(0, this.mSourceLayer2.MessagesArrivedCount);
            Simulator.Instance.RunFor(TimeSpan.FromMilliseconds(Domain.InDomainLatencyMs*2));
            System.Console.WriteLine("arrived = " + this.mSourceLayer2.MessagesArrivedCount);
            return this.mSourceLayer2.MessagesArrivedCount;
        }

        [Test]
        public void OpenNatTest()
        {
            NatType natType = NatType.Open;
            Node nodeA = this.mDomain.AddNode();
            Node nodeB = this.mDomain.AddNode(natType);

            nodeA.BringOnline();
            nodeB.BringOnline();

            Simulator.Instance.RunFor(TimeSpan.FromSeconds(1));

            Assert.AreEqual(natType, nodeB.Address.NatType);
            Assert.AreEqual(1, this.SendAToB(nodeA, nodeB), "Failed to deliver for Open Nat");
        }

        [Test]
        public void BlockedNatTest()
        {
            NatType natType = NatType.Blocked;
            Node nodeA = this.mDomain.AddNode();
            Node nodeB = this.mDomain.AddNode(natType);

            nodeA.BringOnline();
            nodeB.BringOnline();

            Simulator.Instance.RunFor(TimeSpan.FromSeconds(1));

            Assert.AreEqual(natType, nodeB.Address.NatType);
            Assert.AreEqual(0, this.SendAToB(nodeA, nodeB), "Message arrival for Blocked Nat");
        }

        [Test]
        public void FullConeNatTest()
        {
            NatType natType = NatType.FullCone;
            Node nodeA = this.mDomain.AddNode();
            Node nodeB = this.mDomain.AddNode(natType);

            nodeA.BringOnline();
            nodeB.BringOnline();

            Simulator.Instance.RunFor(TimeSpan.FromSeconds(1));

            Assert.AreEqual(natType, nodeB.Address.NatType);
            Assert.AreEqual(1, this.SendAToB(nodeA, nodeB), "Failed to Deliver message for FullCone Nat");
        }

        [Test]
        public void RestrictedConeNatTest()
        {
            NatType natType = NatType.RestrictedCone;
            Node nodeA = this.mDomain.AddNode(NatType.Open);
            Node nodeB = this.mDomain.AddNode(natType);

            nodeA.BringOnline();
            nodeB.BringOnline();

            Simulator.Instance.RunFor(TimeSpan.FromSeconds(1));

            Assert.AreEqual(natType, nodeB.Address.NatType);
            Assert.AreEqual(0, this.SendAToB(nodeA, nodeB), "Message arrival for Restricted Cone Nat");
            Assert.AreEqual(1, this.SendBToA(nodeA, nodeB));
            Assert.AreEqual(1, this.SendAToB(nodeA, nodeB), "Failed to deliver message after hole punch.");
        }


        [Test]
        public void RestrictedPortNatTest()
        {
            NatType natType = NatType.RestrictedPort;
            Node nodeA = this.mDomain.AddNode(NatType.Open);
            Node nodeB = this.mDomain.AddNode(natType);

            nodeA.BringOnline();
            nodeB.BringOnline();

            Simulator.Instance.RunFor(TimeSpan.FromSeconds(1));

            Assert.AreEqual(natType, nodeB.Address.NatType);
            Assert.AreEqual(0, this.SendAToB(nodeA, nodeB), "Message arrival for Restricted Port Nat");
            Assert.AreEqual(1, this.SendBToA(nodeA, nodeB));
            Assert.AreEqual(1, this.SendAToB(nodeA, nodeB), "Failed to deliver message after hole punch.");
        }


        [Test]
        public void SymmetricNatTest()
        {
            NatType natType = NatType.SymmetricNat;
            Node nodeA = this.mDomain.AddNode(NatType.Open);
            Node nodeB = this.mDomain.AddNode(natType);

            nodeA.BringOnline();
            nodeB.BringOnline();

            Simulator.Instance.RunFor(TimeSpan.FromSeconds(1));

            Assert.AreEqual(natType, nodeB.Address.NatType);
            Assert.AreEqual(0, this.SendAToB(nodeA, nodeB), "Message arrival for Symmetric Nat");
            Assert.AreEqual(1, this.SendBToA(nodeA, nodeB));
            Assert.AreEqual(0, this.SendAToB(nodeA, nodeB), "Message arrival for Symmetric Nat");
        }

        [Test]
        public void SymmetricNatTest2()
        {
            NatType natType = NatType.SymmetricNat;
            Node nodeA = this.mDomain.AddNode(natType);
            Node nodeB = this.mDomain.AddNode(natType);

            nodeA.BringOnline();
            nodeB.BringOnline();

            Simulator.Instance.RunFor(TimeSpan.FromSeconds(1));

            Assert.AreEqual(natType, nodeB.Address.NatType);
            Assert.AreEqual(0, this.SendBToA(nodeA, nodeB)); 
            Assert.AreEqual(0, this.SendAToB(nodeA, nodeB), "Message delivered between two symmetric nats.");
        }
        
    }
}
