﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;

namespace NetworkSimulatorTests
{
    class BandwidthTestLayer2 : TransportProtocol
    {
        private int mMessageNum;
        private TimeSpan mSendDelay = new TimeSpan(0);
        private int mBalastLength = 3;

        public BandwidthTestLayer2(TransportProtocol layer1)
            : base(layer1)
        {
            this.ForgeMethodList();
        }

        public void StartSending(double kbsSendRate, PeerAddress destination)
        {
            double messagesPerSecond = 0;
            int messageLength = 0;

            // Increase the balast until the number of sends per millisecond is greater than 1.
            // This is done to ensure that time is monotonic and larger send delays get a closer 
            // approximation of send rate.
            while (this.mSendDelay.TotalMilliseconds < 5)
            {
                // Determine the delay between message sends
                this.mBalastLength++;
                TransportEnvelope envelope = this.GetMessageFor(PeerAddress.GetLoopback(99), QualityOfService.Unreliable);
                messageLength = envelope.Length + 28; // Including IP and UDP headers
                messagesPerSecond = (kbsSendRate * 1000.0) / (double)(messageLength * 8);
                this.mSendDelay = TimeSpan.FromSeconds(1.0 / messagesPerSecond);                
            }

            System.Console.WriteLine("Message length = {0}, with {1} bytes balast.", messageLength, this.mBalastLength);
            System.Console.WriteLine("Send message rate = {0} per second", messagesPerSecond);
            System.Console.WriteLine("-> one message every {0}ms", this.mSendDelay.TotalMilliseconds);
            System.Console.WriteLine("-> {0}BpMs" , (messagesPerSecond * messageLength) / 1000);
            System.Console.WriteLine("Capacity needs {0} bytes", (messagesPerSecond * messageLength * 15) / 1000); 

            this.ContinuousSend(destination);
        }

        private void ContinuousSend(PeerAddress destination)
        {
            Results.Instance.TimerStart(this.mMessageNum);
            TransportEnvelope envelope = this.GetMessageFor(destination, QualityOfService.Unreliable);
            int delay = (int)this.mSendDelay.TotalMilliseconds;

            String balast = new String('.', this.mBalastLength - 3);
            this.RemoteCall(envelope, this.TestCB, this.mMessageNum++);
            this.RemoteCall(envelope, this.BalastMethod, balast);
            this.SendMessage(envelope);
            Simulator.Instance.Schedule(delay, this.ContinuousSend, destination);
        }

        [ProtocolMethod]
        public void TestCB(int messageNum)
        {
            Results.Instance.TimerStop("message arrival time", messageNum);
            Results.Instance.Increment("message arrivals");
        }

        [ProtocolMethod]
        public void BalastMethod(String balast)
        {
            if (null != balast)
            {
                System.Console.WriteLine("balast length is {0}", 1 + balast.Length);
            }
        }
    }



    [TestFixture]
    public class NodeTests
    {
        private Domain mDomain;
        private Node mNodeA;
        private Node mNodeB;

        [SetUp]
        public void Initialize()
        {
            Simulator.Instance.Reset();
            Results.Instance.Clear();
            this.mDomain = new Domain(1, null, NatType.Unknown);
            this.mNodeA = this.mDomain.AddNode();
            this.mNodeB = this.mDomain.AddNode();    
        
        

        }

        [TearDown]
        public void Terminate()
        {
        }

        [Test]
        public void HarnessTest()
        {
        }

        [Test]
        public void SendTest()
        {
            TestProtocolLayer2 sourceLayer2 = new TestProtocolLayer2(this.mNodeA);
            TestProtocolLayer2 destinationLayer2 = new TestProtocolLayer2(this.mNodeB);

            this.mNodeA.BringOnline();
            this.mNodeB.BringOnline();
            Simulator.Instance.RunFor(TimeSpan.FromSeconds(1));

            TransportEnvelope envelope = sourceLayer2.GetMessageFor(this.mNodeB.Address, QualityOfService.Unreliable);
            sourceLayer2.SendMessage(envelope);

            Simulator.Instance.RunFor(TimeSpan.FromMilliseconds(1000));
            
            // Test that we can send messages.
            Assert.AreEqual(1, destinationLayer2.MessagesArrivedCount);
            Assert.AreEqual(1, this.mNodeA.NumSentMessages);
            Assert.AreEqual(1, this.mNodeB.NumRecvMessages);
        }

        [Test]
        public void SendTest2()
        {
            TestProtocolLayer2 sourceLayer2 = new TestProtocolLayer2(this.mNodeA);

            TransportEnvelope envelope = sourceLayer2.GetMessageFor(this.mNodeA.Address, QualityOfService.Unreliable);
            sourceLayer2.SendMessage(envelope);

            Simulator.Instance.RunFor(TimeSpan.FromMilliseconds(1000));

            // Test that we can send messages.
            Assert.AreEqual(1, sourceLayer2.MessagesArrivedCount);

            // Test that sends to the same nodes are not counted as sends/recvs
            Assert.AreEqual(0, this.mNodeA.NumSentMessages);
            Assert.AreEqual(0, this.mNodeA.NumRecvMessages);
        }


        [Test]
        public void UploadTest()
        {
            BandwidthTestLayer2 sourceLayer2 = new BandwidthTestLayer2(this.mNodeA);
            BandwidthTestLayer2 destinationLayer2 = new BandwidthTestLayer2(this.mNodeB);

            this.mNodeA.BringOnline();
            this.mNodeB.BringOnline();
            Simulator.Instance.RunFor(TimeSpan.FromSeconds(1));

            sourceLayer2.StartSending(57, this.mNodeB.Address);

            Simulator.Instance.RunFor(TimeSpan.FromMilliseconds(1000));

            // Test that outbound bandwidth is accurate.

            // In this test the bandwidth usage is a lot less than the capacity, so the only delay sould be latency.
            Assert.Less(Results.Instance.GetCollectionAverage("message arrival time")*1000.0, (Domain.InDomainLatencyMs*2)+1);
        }

        [Test]
        public void UploadAtCapacityTest()
        {
            BandwidthTestLayer2 sourceLayer2 = new BandwidthTestLayer2(this.mNodeA);
            BandwidthTestLayer2 destinationLayer2 = new BandwidthTestLayer2(this.mNodeB);

            this.mNodeB.InboundBandwidth = 100000;

            this.mNodeA.BringOnline();
            this.mNodeB.BringOnline();
            Simulator.Instance.RunFor(TimeSpan.FromSeconds(1));

            sourceLayer2.StartSending(64, this.mNodeB.Address);
            Simulator.Instance.RunFor(TimeSpan.FromMilliseconds(1000));

            // Test that outbound bandwidth is accurate.

            // In this test the same number of messages are being created as being sent, this will incur a delay ontop of the latency,
            // but the queue length will not continue to grow indefinately.
            double averageMessageDelayMs = Results.Instance.GetCollectionAverage("message arrival time") * 1000;
            Assert.Less(averageMessageDelayMs, (Domain.InDomainLatencyMs*2)+1 );
            Assert.Less(averageMessageDelayMs, Domain.InDomainLatencyMs * 4);
        }

        [Test]
        public void UploadOverCapacityTest()
        {
            BandwidthTestLayer2 sourceLayer2 = new BandwidthTestLayer2(this.mNodeA);
            BandwidthTestLayer2 destinationLayer2 = new BandwidthTestLayer2(this.mNodeB);

            this.mNodeA.BringOnline();
            this.mNodeB.BringOnline();
            Simulator.Instance.RunFor(TimeSpan.FromSeconds(1));

            this.mNodeB.InboundBandwidth = 100000;
            sourceLayer2.StartSending(70, this.mNodeB.Address);

            Simulator.Instance.RunFor(TimeSpan.FromMilliseconds(1000));

            // Test that the outbound bandwidth is accurate.
            // Since messages are created faster than they can be sent the delay will continue to grow indefinately
            Assert.Less(Results.Instance.GetCollectionAverage("message arrival time") * 1000.0, (Domain.InDomainLatencyMs*2)+1 );
        }


        [Test]
        public void UploadBufferOverflowTest()
        {
            BandwidthTestLayer2 sourceLayer2 = new BandwidthTestLayer2(this.mNodeA);
            BandwidthTestLayer2 destinationLayer2 = new BandwidthTestLayer2(this.mNodeB);

            this.mNodeA.BringOnline();
            this.mNodeB.BringOnline();
            Simulator.Instance.RunFor(TimeSpan.FromSeconds(1));

            this.mNodeB.InboundBandwidth = 100000;
            sourceLayer2.StartSending(150, this.mNodeB.Address);

            Simulator.Instance.RunFor(TimeSpan.FromMilliseconds(10000));

            // Test that the outbound bandwidth is accurate.

            // Since messages are created fatser than they can be sent the delay will continue to grow indefinately
            Assert.Less(Domain.InDomainLatencyMs * 2, Results.Instance.GetCollectionAverage("message arrival time") * 1000);

            // Test that packets are lost when capacity exceeds buffer
            Assert.Greater(Results.Instance.GetCount("buffer overflow message loss"), 0);
        }

        [Test]
        public void UnreliableUploadTests()
        {
            // Test bandwidth again for in an unreliable domain.
            Domain.Reliability = 0.9;

            this.UploadTest();

            this.mDomain = new Domain(1, null, NatType.Unknown);
            this.mNodeA = this.mDomain.AddNode();
            this.mNodeB = this.mDomain.AddNode();
            Simulator.Instance.Reset();
            Results.Instance.Clear();
            this.UploadAtCapacityTest();

            this.mDomain = new Domain(1, null, NatType.Unknown);
            this.mNodeA = this.mDomain.AddNode();
            this.mNodeB = this.mDomain.AddNode(); Simulator.Instance.Reset();
            Results.Instance.Clear();
            this.UploadOverCapacityTest();

            this.mDomain = new Domain(1, null, NatType.Unknown);
            this.mNodeA = this.mDomain.AddNode();
            this.mNodeB = this.mDomain.AddNode();
            Simulator.Instance.Reset();
            Results.Instance.Clear();
            this.UploadBufferOverflowTest();
        }

        [Test]
        public void DownloadTest()
        {
            BandwidthTestLayer2 sourceLayer2 = new BandwidthTestLayer2(this.mNodeA);
            BandwidthTestLayer2 destinationLayer2 = new BandwidthTestLayer2(this.mNodeB);

            this.mNodeA.BringOnline();
            this.mNodeB.BringOnline();
            Simulator.Instance.RunFor(TimeSpan.FromSeconds(1));

            this.mNodeA.OutboundBandwidth = 10000;
            this.mNodeB.InboundBandwidth = 64;

            sourceLayer2.StartSending(57, this.mNodeB.Address);

            Simulator.Instance.RunFor(TimeSpan.FromMilliseconds(1000));

            // Test inbound bandwidth below capacity.

            // In this test the bandwidth usage is a lot less than the capacity, so the only delay sould be latency.
            Assert.Less(Results.Instance.GetCollectionAverage("message arrival time") * 1000, (Domain.InDomainLatencyMs*2)+1);
        }

        [Test]
        public void DownloadAtCapacityTest()
        {
            BandwidthTestLayer2 sourceLayer2 = new BandwidthTestLayer2(this.mNodeA);
            BandwidthTestLayer2 destinationLayer2 = new BandwidthTestLayer2(this.mNodeB);

            this.mNodeA.OutboundBandwidth = 10000;
            this.mNodeB.InboundBandwidth = 64;

            this.mNodeA.BringOnline();
            this.mNodeB.BringOnline();
            Simulator.Instance.RunFor(TimeSpan.FromSeconds(1));

            sourceLayer2.StartSending(64, this.mNodeB.Address);

            Simulator.Instance.RunFor(TimeSpan.FromMilliseconds(1000));

            // Test inbound bandwidth at capacity.

            // In this test the same number of messages are being created as being sent, this will incur a delay ontop of the latency,
            // but the queue length will not continue to grow indefinately.
            double averageMessageDelayMs = Results.Instance.GetCollectionAverage("message arrival time") * 1000;
            Assert.Less(averageMessageDelayMs, (Domain.InDomainLatencyMs*2)+1 );
            Assert.Less(averageMessageDelayMs, Domain.InDomainLatencyMs * 4);
        }

        [Test]
        public void DownloadOverCapacityTest()
        {
            BandwidthTestLayer2 sourceLayer2 = new BandwidthTestLayer2(this.mNodeA);
            BandwidthTestLayer2 destinationLayer2 = new BandwidthTestLayer2(this.mNodeB);

            this.mNodeA.OutboundBandwidth = 10000;
            this.mNodeB.InboundBandwidth = 64;

            this.mNodeA.BringOnline();
            this.mNodeB.BringOnline();
            Simulator.Instance.RunFor(TimeSpan.FromSeconds(1));

            sourceLayer2.StartSending(70, this.mNodeB.Address);

            Simulator.Instance.RunFor(TimeSpan.FromMilliseconds(1000));

            // Test inbound bandwidth above capacity.

            // Since messages are created fatser than they can be sent the delay will continue to grow indefinately
            Assert.Less(Results.Instance.GetCollectionAverage("message arrival time") * 1000, (Domain.InDomainLatencyMs * 2)+1);
        }


        [Test]
        public void DownloadBufferOverflowTest()
        {
            BandwidthTestLayer2 sourceLayer2 = new BandwidthTestLayer2(this.mNodeA);
            BandwidthTestLayer2 destinationLayer2 = new BandwidthTestLayer2(this.mNodeB);

            this.mNodeA.OutboundBandwidth = 1000000;
            this.mNodeB.InboundBandwidth = 64;

            this.mNodeA.BringOnline();
            this.mNodeB.BringOnline();
            Simulator.Instance.RunFor(TimeSpan.FromSeconds(1));

            sourceLayer2.StartSending(150, this.mNodeB.Address);

            Simulator.Instance.RunFor(TimeSpan.FromMilliseconds(10000));

            // Test inbound bandwidth at buffer overflow.

            // Since messages are created fatser than they can be sent the delay will continue to grow indefinately
            Assert.Less(Domain.InDomainLatencyMs * 2, Results.Instance.GetCollectionAverage("message arrival time") * 1000);

            // Test that packets are lost when capacity exceeds buffer
            Assert.Greater(Results.Instance.GetCount("buffer overflow message loss"), 0);
        }

        [Test]
        public void UnreliableDownloadTests()
        {
            // Test inbound bandwidth again with an unreliable domain.
            Domain.Reliability = 0.9;

            this.DownloadTest();

            this.mDomain = new Domain(1, null, NatType.Unknown);
            this.mNodeA = this.mDomain.AddNode();
            this.mNodeB = this.mDomain.AddNode();
            Simulator.Instance.Reset();
            Results.Instance.Clear();
            this.DownloadAtCapacityTest();

            this.mDomain = new Domain(1, null, NatType.Unknown);
            this.mNodeA = this.mDomain.AddNode();
            this.mNodeB = this.mDomain.AddNode(); Simulator.Instance.Reset();
            Results.Instance.Clear();
            this.DownloadOverCapacityTest();

            this.mDomain = new Domain(1, null, NatType.Unknown);
            this.mNodeA = this.mDomain.AddNode();
            this.mNodeB = this.mDomain.AddNode();
            Simulator.Instance.Reset();
            Results.Instance.Clear();
            this.DownloadBufferOverflowTest();
        }

        public void MultiDomainInit()
        {
            Topology topology = new Topology();

            topology.Load<Domain>("TestZeroLatencyTopology.txt");

            Domain domain1 = topology.GetDomain(1) as Domain;
            Domain domain3 = topology.GetDomain(3) as Domain;

            this.mDomain =domain1;
            this.mNodeA = this.mDomain.AddNode();
            this.mNodeB = domain3.AddNode();

            Domain.Reliability = 0.9;

            Simulator.Instance.Reset();
            Results.Instance.Clear();
        }

        [Test]
        public void MultiDomainTest()
        {
            // Test that reliability and and bandwidth work over multipleDomains.
            this.MultiDomainInit();
            this.DownloadTest();

            this.MultiDomainInit();
            this.DownloadAtCapacityTest();

            this.MultiDomainInit();
            this.DownloadOverCapacityTest();

            this.MultiDomainInit();
            this.DownloadBufferOverflowTest();

            this.MultiDomainInit();
            this.UnreliableDownloadTests();



            this.MultiDomainInit();
            this.UploadTest();

            this.MultiDomainInit();
            this.UploadAtCapacityTest();

            this.MultiDomainInit();
            this.UploadOverCapacityTest();

            this.MultiDomainInit();
            this.UploadBufferOverflowTest();

            this.MultiDomainInit();
            this.UnreliableUploadTests();

        }


        [Test]
        public void StatusTest()
        {
            // Test that we can put a node online.
            Assert.AreEqual(ConnectivityStatus.Initializing, this.mNodeA.NetworkContext.Status);
            this.mNodeA.BringOnline();
            Assert.AreEqual(ConnectivityStatus.Initializing, this.mNodeA.NetworkContext.Status);
            Simulator.Instance.RunFor(TimeSpan.FromMilliseconds(Parameters.NodeInitializationDelayMs));
            Assert.AreEqual(ConnectivityStatus.Online, this.mNodeA.NetworkContext.Status);

            // Test that we can put a node offline.
            this.mNodeA.TakeOffline();
            Assert.AreEqual(ConnectivityStatus.Offline, this.mNodeA.NetworkContext.Status);

            // Test that we can bring it back online
            Assert.AreEqual(ConnectivityStatus.Offline, this.mNodeA.NetworkContext.Status);
            this.mNodeA.BringOnline();
            Assert.AreEqual(ConnectivityStatus.Initializing, this.mNodeA.NetworkContext.Status);
            Simulator.Instance.RunFor(TimeSpan.FromMilliseconds(Parameters.NodeInitializationDelayMs));
            Assert.AreEqual(ConnectivityStatus.Online, this.mNodeA.NetworkContext.Status);

            // Test that a node that is initializing can be put offline and it will never come online.
            this.mNodeA.TakeOffline();
            Assert.AreEqual(ConnectivityStatus.Offline, this.mNodeA.NetworkContext.Status);
            this.mNodeA.BringOnline();
            Assert.AreEqual(ConnectivityStatus.Initializing, this.mNodeA.NetworkContext.Status);
            this.mNodeA.TakeOffline();
            Assert.AreEqual(ConnectivityStatus.Offline, this.mNodeA.NetworkContext.Status);
            Simulator.Instance.RunFor(TimeSpan.FromMilliseconds(Parameters.NodeInitializationDelayMs));
            Assert.AreEqual(ConnectivityStatus.Offline, this.mNodeA.NetworkContext.Status);
        }

        public void NodeUp()
        {
            Results.Instance.TimerStart("UpTime");
            Results.Instance.TimerStop("Down time", "DownTime");
        }

        public void NodeDown()
        {
            Results.Instance.TimerStop("Up time", "UpTime");

            // Start the down time timer after the time taken to initialize a node.
            // This ensures that the down time is timed from the point the node goes down,
            // to the point it starts initializing.
            Simulator.Instance.Schedule(Parameters.NodeInitializationDelayMs, this.NodeInitializationStarted);
        }

        public void NodeInitializationStarted()
        {
            Results.Instance.TimerStart("DownTime");
        }


        [Test]
        public void SessionTimeTest()
        {
            Results.Instance.Clear();

            this.mNodeA = new Node(PeerAddress.Nowhere, PeerAddress.Nowhere, this.mDomain);
            this.mNodeA.NetworkContext.Status = ConnectivityStatus.Initializing;
            this.mNodeA.NetworkContext.NetworkOfflineEvent += this.NodeDown;
            this.mNodeA.NetworkContext.NetworkOnlineEvent += this.NodeUp;            

            this.mNodeA.SetAverageSessionTime(1000, 20, 500, 50);

            System.Console.WriteLine("{0} start", Simulator.Instance.Now.TotalSeconds);
            Simulator.Instance.RunFor(TimeSpan.FromMinutes(15));

            double averageUpTime = Results.Instance.GetCollectionAverage("Up time");
            double averageDownTime = Results.Instance.GetCollectionAverage("Down time");

            double stdDevUpTime = Results.Instance.GetCollectionStdDev("Up time");
            double stdDevDownTime = Results.Instance.GetCollectionStdDev("Down time");

            System.Console.WriteLine("{0}, {1}", averageUpTime, averageDownTime);
            System.Console.WriteLine("{0}, {1}", stdDevUpTime, stdDevDownTime);

            Assert.IsTrue(averageUpTime < 1.1, "Average too large");
            Assert.IsTrue(averageUpTime > 0.9, "Average too small");
            Assert.IsTrue(stdDevUpTime > 0.015, "StdDev too small");
            Assert.IsTrue(stdDevUpTime < 0.025, "StdDev too small");

            Assert.IsTrue(averageDownTime < 0.55, "Average too large");
            Assert.IsTrue(averageDownTime > 0.45, "Average too small");
            Assert.IsTrue(stdDevDownTime > 0.045, "StdDev too small");
            Assert.IsTrue(stdDevDownTime < 0.055, "StdDev too small");

            Results.Instance.DumpAll(System.Console.Out);
        }

    }
}
