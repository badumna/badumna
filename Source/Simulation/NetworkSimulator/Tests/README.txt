﻿This project contains the old nUnit test code from NetworkSimulator.
It was moved here as part of a cleanup of the NetworkSimulator code.
At that time the unit tests hadn't been maintained for some time,
so chances are they're borked.