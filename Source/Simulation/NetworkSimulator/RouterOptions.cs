﻿//------------------------------------------------------------------------------
// <copyright file="RouterOptions.cs" company="National ICT Australia Limited">
//     Copyright (c) 2012 National ICT Australia Limited. All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetworkSimulator
{
    /// <summary>
    /// Options for routers.
    /// </summary>
    public class RouterOptions
    {
        /// <summary>
        /// Gets or sets the NAT type of the router (typed as object because NatType is not public).
        /// </summary>
        public object NatType { get; set; }

        /// <summary>
        /// Gets or sets the probability that a message won't get dropped when being sent between the router and a local peer.
        /// </summary>
        public double Reliability { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the router is able to hairpin messages (route from a local peer
        /// to a local peer using the public IP address).
        /// </summary>
        public bool CanHairpin { get; set; }
    }
}
