﻿//------------------------------------------------------------------------------
// <copyright file="Topology.cs" company="National ICT Australia Limited">
//     Copyright (c) 2012 National ICT Australia Limited. All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.IO;

namespace NetworkSimulator
{
    /// <summary>
    /// Represents the topology of a network as a matrix of latencies between subnets.
    /// </summary>
    public class Topology
    {
        /// <summary>
        /// The name of the file the topology was loaded from.
        /// </summary>
        private string filename;

        /// <summary>
        /// The modifcation time of the file when it was loaded.
        /// </summary>
        private DateTime fileModificationTime;

        /// <summary>
        /// The latency data.  Each entry [x, y] gives the latency of a message from subnet x to subnet y.
        /// A negative value indicates there is no path from x to y.
        /// </summary>
        private int[,] latencyMatrix;

        /// <summary>
        /// Gets the number of subnets in the topology.
        /// </summary>
        public int Count { get; private set; }

        /// <summary>
        /// Gets or sets the standard deviation of the latencies, represented as the fraction of the latency.
        /// e.g. a value of 0.1 indicates a standard deviation of 20ms for a latency of 200ms.
        /// </summary>
        public double DeviationFactor { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to ignore any entries in the latency matrix that indicate a lack of connectivity
        /// between two subnets (negative values in the matrix).
        /// Negative values are replaced with 0ms latencies (probably should be a more reasonable default latency).
        /// </summary>
        public bool HasFullConnectivity { get; set; }

        /// <summary>
        /// Changes this topology to represent a simple topology with the given number of subnets and the
        /// given latency between all subnets.
        /// </summary>
        /// <param name="subnetCount">The number of subnets</param>
        /// <param name="latency">The latency between each subnet</param>
        public void SetSimpleToplogy(int subnetCount, int latency)
        {
            this.fileModificationTime = default(DateTime);
            this.filename = null;

            this.Count = subnetCount;
            this.latencyMatrix = new int[this.Count, this.Count];
            for (int i = 0; i < this.Count; i++)
            {
                for (int j = 0; j < this.Count; j++)
                {
                    this.latencyMatrix[i, j] = latency;
                }
            }
        }

        /// <summary>
        /// Loads latency data from the specified file.
        /// </summary>
        /// <param name="filename">The data file</param>
        public void Load(string filename)
        {
            DateTime modificationTime = File.GetLastWriteTimeUtc(filename);

            if (filename == this.filename && modificationTime == this.fileModificationTime)
            {
                // Already loaded, no need to do anything
                return;
            }

            using (FileStream stream = new FileStream(filename, FileMode.Open, FileAccess.Read))
            using (StreamReader reader = new StreamReader(stream))
            {
                string[] header = reader.ReadLine().Split(new char[] { ' ' });
                
                this.Count = int.Parse(header[2]);

                // Discard until "node" is found.
                while (reader.Peek() != 'n')
                {
                    reader.ReadLine();
                }

                string line;

                // Read n networks (we assume the domains are all numbered sequentially from 1)
                for (int i = 0; i < this.Count; i++)
                {
                    line = reader.ReadLine();
                    while (!line.Contains("node"))
                    {
                        line = reader.ReadLine();
                    }
                }

                this.latencyMatrix = new int[this.Count, this.Count];

                // Read latencies
                while (!reader.EndOfStream)
                {
                    line = reader.ReadLine();

                    if (line.Length == 0)
                    {
                        continue;
                    }

                    string[] comments = line.Split('#');
                    string[] columns = comments[0].Split(new char[] { ',', ' ' });
                    int i = int.Parse(columns[0]) - 1;
                    int j = int.Parse(columns[1]) - 1;
                    int latencyIJ = int.Parse(columns[2]);
                    int latencyJI = latencyIJ;

                    if (columns.Length > 3)
                    {
                        latencyJI = int.Parse(columns[3]);
                    }

                    this.latencyMatrix[i, j] = latencyIJ;
                    this.latencyMatrix[j, i] = latencyJI;
                }
            }

            this.filename = filename;
            this.fileModificationTime = modificationTime;
        }

        /// <summary>
        /// Generate a latency value for a packet between the specified subnets.
        /// </summary>
        /// <param name="sourceId">ID of the source network</param>
        /// <param name="destinationId">ID of the destination network</param>
        /// <returns>A latency sample, or a negative value to indicate the networks are isolated</returns>
        public int NextLatencyBetween(int sourceId, int destinationId)
        {
            double a = RandomSource.Generator.NextDouble();
            double b = RandomSource.Generator.NextDouble();

            // Box - Muller transform.
            double stdNormalDistributedVar = Math.Sqrt(-2.0 * Math.Log(a)) * Math.Cos(2.0 * Math.PI * b);

            double latency = (double)this.latencyMatrix[sourceId, destinationId];

            // Negative value indicates no connection
            if (latency < 0)
            {
                if (this.HasFullConnectivity)
                {
                    latency = 0;
                }
                else
                {
                    return (int)latency;
                }
            }

            double latencyAfterDeviation = latency * (1 + (this.DeviationFactor * stdNormalDistributedVar));

            return (int)Math.Max(0, latencyAfterDeviation);
        }
    }
}
