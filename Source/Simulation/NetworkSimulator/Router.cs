//------------------------------------------------------------------------------
// <copyright file="Router.cs" company="National ICT Australia Limited">
//     Copyright (c) 2012 National ICT Australia Limited. All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Diagnostics;

using Badumna;
using Badumna.Core;
using Badumna.Utilities;

using NetworkSimulator.Instrumentation;
using NetworkSimulator.Nat;

namespace NetworkSimulator
{
    /// <summary>
    /// A simulated network router / gateway which supports different NAT algorithms.
    /// The router can route messages directly to machines connected
    /// locally, or forward messages to the backbone if they are destined for other subnets.
    /// </summary>
    /// <remarks>
    /// The terms "peer", "node", and "transport" are all used interchangably in this class, and all refer to
    /// instances of <see cref="SimulatedTransport"/>, which represents a peer's connection to the simulated network.
    /// </remarks>
    internal class Router
    {
        /// <summary>
        /// The time taken in milliseconds to route a message from the router to a local node.
        /// </summary>
        public static readonly int LocalNetworkLatencyMs = 1;

        /// <summary>
        /// Unique identifier for this router.
        /// </summary>
        private int id;

        /// <summary>
        /// The backbone, used to forward messages destined for other subnets.
        /// </summary>
        private Backbone backbone;

        /// <summary>
        /// The options for the router.
        /// </summary>
        private RouterOptions options;

        /// <summary>
        /// The address translation algorithm.
        /// </summary>
        private IAddressTranslator addressTranslator;

        /// <summary>
        /// Transports for peers connected to this router's local network.
        /// </summary>
        private Dictionary<PeerAddress, SimulatedTransport> nodes = new Dictionary<PeerAddress, SimulatedTransport>();

        /// <summary>
        /// Traces messages passing through this router.
        /// </summary>
        private IMessageTracer messageTracer;

        /// <summary>
        /// The message tracer id for this router.
        /// </summary>
        private int tracerId;

        /// <summary>
        /// The simulation event queue.
        /// </summary>
        private Simulator eventQueue;

        /// <summary>
        /// The time keeper.
        /// </summary>
        private ITime timeKeeper;

        /// <summary>
        /// Initializes a new instance of the Router class.
        /// </summary>
        /// <param name="number">The id for this router</param>
        /// <param name="backbone">The backbone the router is connected to</param>
        /// <param name="options">The options</param>
        /// <param name="messageTracer">The message tracer</param>
        /// <param name="eventQueue">The simulation event queue</param>
        /// <param name="timeKeeper">The time keeper</param>
        public Router(
            int number,
            Backbone backbone,
            RouterOptions options,
            IMessageTracer messageTracer,
            Simulator eventQueue,
            ITime timeKeeper)
        {
            this.id = number;
            this.backbone = backbone;
            this.options = options;
            this.messageTracer = messageTracer;
            this.eventQueue = eventQueue;
            this.timeKeeper = timeKeeper;

            NatType natType = (NatType)options.NatType;
            PeerAddress publicAddress = this.GetPublicAddress(natType);

            switch (natType)
            {
                case NatType.Unknown:
                    this.addressTranslator = new OpenNat(publicAddress);
                    break;

                case NatType.Open:
                    this.addressTranslator = new OpenNat(publicAddress);
                    break;

                case NatType.FullCone:
                    this.addressTranslator = new FullConeNat(publicAddress);
                    break;

                case NatType.MangledFullCone:
                    this.addressTranslator = new MangledFullConeNat(publicAddress);
                    break;

                case NatType.RestrictedCone:
                    this.addressTranslator = new RestrictedNat(publicAddress);
                    break;

                case NatType.RestrictedPort:
                    this.addressTranslator = new RestrictedNat(publicAddress);
                    break;

                case NatType.SymmetricNat:
                    this.addressTranslator = new SymmetricNat(publicAddress);
                    break;

                default:
                    break;
            }

            if (this.addressTranslator == null)
            {
                throw new ArgumentException("Unknown or unsupported NAT type", "natType");
            }
        }

        /// <summary>
        /// Creates a simulated facade (peer) and connects it to the local side of the router.
        /// </summary>
        /// <param name="simulatorOptions">Configurable parameters for the facade</param>
        /// <returns>The new facade</returns>
        public ISimulatorFacade AddLocalNode(SimulatorFacadeOptions simulatorOptions)
        {
            PeerAddress publicAddress;
            PeerAddress privateAddress = this.addressTranslator.AddAddress(out publicAddress);

            if (this.id != 1 && this.nodes.Count == 0 && this.messageTracer != null && this.messageTracer.TraceRouters)
            {
                this.tracerId = this.messageTracer.AddNode(
                    this.timeKeeper.Now.TotalMilliseconds,
                    string.Format("{0} router", publicAddress.NatType.ToString()));
            }

            SimulatorFacade facade = SimulatorFacade.Create(
                simulatorOptions,
                publicAddress,
                privateAddress,
                this,
                this.messageTracer,
                this.eventQueue,
                this.timeKeeper);

            this.nodes.Add(privateAddress, facade.Transport);

            return facade;
        }

        /// <summary>
        /// Disconnects a simulated facade (peer) from the router.
        /// </summary>
        /// <param name="facade">The facade to disconnect</param>
        public void RemoveLocalNode(ISimulatorFacade facade)
        {
            this.nodes.Remove(((SimulatorFacade)facade).Transport.PrivateAddress);
        }

        /// <summary>
        /// Restore a node by creating a new facade with same address as a disconnected one.
        /// </summary>
        /// <param name="facade">The facade of the peer that has been disconnected.</param>
        /// <param name="simulatorOptions">Configuration options for the facade.</param>
        /// <returns>A new facade with the same address as the stopped one.</returns>
        public ISimulatorFacade RestoreLocalNode(ISimulatorFacade facade, SimulatorFacadeOptions simulatorOptions)
        {
            PeerAddress publicAddress = ((SimulatorFacade)facade).Transport.PublicAddress;
            PeerAddress privateAddress = ((SimulatorFacade)facade).Transport.PrivateAddress;

            if (this.nodes.ContainsKey(privateAddress))
            {
                throw new InvalidOperationException("Cannot restore a node that still exists.");
            }

            SimulatorFacade newFacade = SimulatorFacade.Create(
                simulatorOptions,
                publicAddress,
                privateAddress,
                this,
                this.messageTracer,
                this.eventQueue,
                this.timeKeeper);

            this.nodes.Add(privateAddress, newFacade.Transport);

            return newFacade;
        }

        /// <summary>
        /// Disconnects the simulated transport from the router.
        /// </summary>
        /// <param name="node">The transport to disconnect</param>
        public void RemoveLocalNode(SimulatedTransport node)
        {
            this.addressTranslator.RemoveAddress(node.PrivateAddress);
        }

        /// <summary>
        /// Routes the message towards its destination.
        /// </summary>
        /// <param name="messageWrapper">The message to route</param>
        public void RouteMessage(MessageWrapper messageWrapper)
        {
            SimulatedTransport sourceNode = null;
            SimulatedTransport destinationNode = null;
            PeerAddress publicSource = new PeerAddress(messageWrapper.Source);

            if (this.addressTranslator.IsInternalAddress(messageWrapper.Source))
            {
                publicSource = this.addressTranslator.Outbound(messageWrapper.Source, messageWrapper.Destination);

                if (null != publicSource && this.nodes.TryGetValue(messageWrapper.Source, out sourceNode))
                {
                    // Remove the message from the sources outbound queue.
                    sourceNode.DequeFromOutbound(messageWrapper.Length);

                    // Drop messages according to the reliability probability
                    if (this.options.Reliability < RandomSource.Generator.NextDouble())
                    {
                        this.DropMessage(messageWrapper, "packet loss");
                        return;
                    }
                }
                else if (null == publicSource)
                {
                    Logger.TraceInformation(LogTag.Simulator, "NAT at {0} drops outbound message from {1} on port {2}", messageWrapper.Destination.Address, messageWrapper.Source, messageWrapper.Destination.Port);
                    this.DropMessage(messageWrapper, "No outbound mapping");
                    return;
                }
                else
                {
                    Logger.TraceInformation(LogTag.Simulator, "Router at {0} drops outbound message from {0} ({1})", messageWrapper.Destination, messageWrapper.Source, publicSource);
                    this.DropMessage(messageWrapper, "Source has unknown internal address");
                    return;
                }
            }

            if (this.messageTracer != null && this.messageTracer.TraceRouters)
            {
                messageWrapper.AddHop(this.tracerId);
            }

            if (this.addressTranslator.IsInternalAddress(messageWrapper.Destination))
            {
                PeerAddress translatedDestination = this.addressTranslator.Inbound(publicSource, messageWrapper.Destination);

                if (null != translatedDestination && this.nodes.TryGetValue(translatedDestination, out destinationNode))
                {
                    double delayMs = destinationNode.GetMsDelayForInboundMessage(messageWrapper.Length);
                    if (delayMs < 0)
                    {
                        this.DropMessage(messageWrapper, "Destination buffer overflow");
                        return;
                    }

                    delayMs += Router.LocalNetworkLatencyMs;

                    messageWrapper.Envelope.SetDestination(translatedDestination);

                    if (null != sourceNode && translatedDestination.Equals(messageWrapper.Destination))
                    {
                        // The destination is an internal address so the source should be too.
                        publicSource = messageWrapper.Source;
                    }

                    if (null != sourceNode && !translatedDestination.Equals(messageWrapper.Destination))
                    {
                        if (messageWrapper.Destination.NatType != NatType.Open && !this.options.CanHairpin)
                        {
                            Logger.TraceInformation(LogTag.Simulator, "Router at {0} drops hairpin message from {1}", messageWrapper.Destination, messageWrapper.Source);
                            this.DropMessage(messageWrapper, "No hairpin");
                            return;
                        }
                    }

                    this.eventQueue.Schedule((int)delayMs, destinationNode.ReceiveMessage, messageWrapper, messageWrapper.Destination.Port);
                    return;
                }
                else if (null == translatedDestination)
                {
                    Logger.TraceInformation(LogTag.Simulator, "NAT at {0} drops message from {1} on port {2}", messageWrapper.Destination.Address, messageWrapper.Source, messageWrapper.Destination.Port);
                    this.DropMessage(messageWrapper, "No inbound NAT mapping");
                }
                else
                {
                    Logger.TraceInformation(LogTag.Simulator, "Router at {0} drops message from {0} ({1})", messageWrapper.Destination, messageWrapper.Source, publicSource);
                    this.DropMessage(messageWrapper, "Mapped address is unknown");
                }
            }
            else if (null != this.backbone)
            {
                if (null == sourceNode)
                {
                    Debug.Assert(
                        false,
                        String.Format("This domain contains neither the source ({0}) nor the destination ({1}).", messageWrapper.Source, messageWrapper.Destination));
                }

                if (this.messageTracer != null && this.messageTracer.TraceRouters)
                {
                    messageWrapper.AddHop(this.tracerId);
                }

                messageWrapper.Source = new PeerAddress(publicSource);

                this.backbone.RouteMessage(messageWrapper);
            }
        }

        /// <summary>
        /// Returns the peer address for the router.
        /// </summary>
        /// <param name="natType">The nat type</param>
        /// <returns>The peer address</returns>
        private PeerAddress GetPublicAddress(NatType natType)
        {
            int addressInt = this.id << 16;
            byte[] addressBytes = BitConverter.GetBytes(addressInt);

            Array.Reverse(addressBytes);

            return new PeerAddress(new System.Net.IPAddress(addressBytes), 0, natType);
        }

        /// <summary>
        /// Record the message as dropped.
        /// </summary>
        /// <param name="messageWrapper">The message to be dropped</param>
        /// <param name="reason">The reason for dropping the message</param>
        private void DropMessage(MessageWrapper messageWrapper, string reason)
        {
            if (this.messageTracer != null && this.messageTracer.TraceRouters)
            {
                messageWrapper.Drop(this.tracerId, reason);
            }
            else
            {
                messageWrapper.Drop(reason);
            }
        }
    }
}
