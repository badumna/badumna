﻿//-----------------------------------------------------------------------
// <copyright file="ISimulatorFacade.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2011 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System.Collections.Generic;

using Badumna;
using Badumna.Chat;
using Badumna.Core;
using Badumna.Utilities;

using NetworkSimulator.ProtocolStacks.Services;

namespace NetworkSimulator
{
    /// <summary>
    /// The set of public members provided by the simulator facade.  The primary reason this interface
    /// exists is so that MainNetworkFacade in Badumna.dll can be internal (if SimulatorFacade is public
    /// then its base class MainNetworkFacade must be public too; instead we have this public interface
    /// ISimulatorFacade).
    /// </summary>
    public interface 
        ISimulatorFacade : INetworkFacade
    {
        /// <summary>
        /// Gets the public address of the peer (including NAT type).
        /// </summary>
        string Address { get; }

        /// <summary>
        /// Gets the public IP address and port of the peer.
        /// </summary>
        string SocketAddress { get; }

        /// <summary>
        /// Gets the simple-send protocol for the peer (this is only valid in the TransportOnly stack).
        /// </summary>
        ISimpleSend SimpleSend { get; }

        /// <summary>
        /// Returns the specified object using WolfEye.
        /// </summary>
        /// <param name="path">A dotted path to the desired object, rooted at SimulatorFacade (similar to what
        /// would be entered in the Visual Studio watch window).</param>
        /// <typeparam name="T">The type of the value to get.</typeparam>
        /// <returns>The specified object.</returns>
        T GetObject<T>(string path);

        /// <summary>
        /// Returns the specified objects using WolfEye.
        /// </summary>
        /// <param name="path">A dotted path identifying the desired objects (may contain wildcards).</param>
        /// <typeparam name="T">The type of the values to get.</typeparam>
        /// <returns>A list containing the specified objects.</returns>
        IEnumerable<T> GetObjects<T>(string path);

        /// <summary>
        /// Gets the statistics from the protocol stack.
        /// </summary>
        /// <returns>The statistics instance.</returns>
        IStatistics GetStatistics();
    }
}
