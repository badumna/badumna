﻿// <autogenerated />

using System;
using System.Collections.Generic;
using System.Text;
using Badumna.Utilities;
using Badumna.Core;

namespace NetworkSimulator.Instrumentation
{
    public class StreamingTestStatsCollector
    {
        private bool sendCompleted;

        private TimeSpan sendCompletionTime;

        private bool receiveCompleted;

        private TimeSpan receiveCompletionTime;

        private static StreamingTestStatsCollector instance = new StreamingTestStatsCollector();

        public static StreamingTestStatsCollector Instance
        {
            get { return instance; }
        }

        public StreamingTestStatsCollector()
        {
            this.Reset();
        }

        public void StreamSendCompleted(TimeSpan time)
        {
            Console.WriteLine("Send Completed!");
            this.sendCompleted = true;
            this.sendCompletionTime = time;
        }

        public void StreamReceiveCompleted(TimeSpan time)
        {
            Console.WriteLine("Receive Completed!");
            this.receiveCompleted = true;
            this.receiveCompletionTime = time;
        }

        public bool StreamingTestPassed()
        {
            Console.WriteLine("StreamingTestPassed is called.");
            if (!this.receiveCompleted || !this.sendCompleted)
            {
                return false;
            }

            double secondDiff = (this.sendCompletionTime - this.receiveCompletionTime).TotalSeconds;
            if (Math.Abs(secondDiff) > 5.0)
            {
                Console.WriteLine("diff = {0}", secondDiff);
                return false;
            }

            Console.WriteLine("time diff is {0}, send complete time is {1}, receive complete time is {2}", secondDiff, this.sendCompletionTime, this.receiveCompletionTime);

            return true;
        }

        private void Reset()
        {
            this.sendCompleted = false;
            this.receiveCompleted = false;
            this.sendCompletionTime = TimeSpan.Zero;
            this.receiveCompletionTime = TimeSpan.Zero;
        }
    }
}
