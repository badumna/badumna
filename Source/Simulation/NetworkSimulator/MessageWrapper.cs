﻿using System;
using System.Collections.Generic;
using System.Text;

using Badumna;
using Badumna.Core;
using Badumna.Utilities;
using Badumna.Transport;
using NetworkSimulator.Instrumentation;

namespace NetworkSimulator
{
    class MessageWrapper
    {
        public TransportEnvelope Envelope { get; private set; }
        public ulong mId { get; private set; }
        public PeerAddress Source { get;  set; }
        public PeerAddress Destination { get;  set; }
        public int LastHop { get; private set; }
        public int Length { get { return this.Envelope.Length; } }

        private static ulong mNextId;

        private IMessageTracer mTracer;
        private ITime timeKeeper;

        public MessageWrapper(
            TransportEnvelope envelope,
            PeerAddress source,
            PeerAddress destination,
            IMessageTracer tracer,
            int tracerId,
            ITime timeKeeper)
        {
            this.Envelope = new TransportEnvelope(envelope);
#if TRACE
            this.Envelope.AppendLabel(String.Format("{0} bytes", this.Envelope.Length));
#endif
            this.Source = source;
            this.Destination = destination;
            this.LastHop = tracerId;
            this.mTracer = tracer;
            this.mId = MessageWrapper.mNextId++;

            this.timeKeeper = timeKeeper;

            if (tracer != null)
            {
                tracer.AddMessage(envelope.Label, this.timeKeeper.Now.TotalMilliseconds, tracerId, this.mId, this.Envelope.Length);
            }
        }

        public void AddHop(int nodeId)
        {
            if (this.mTracer != null)
            {
                this.mTracer.AddMessageHop(this.mId, this.timeKeeper.Now.TotalMilliseconds, nodeId, false);
            }
            this.LastHop = nodeId;
        }

        public void HasArrived(int nodeId)
        {
            if (this.mTracer != null)
            {
                this.mTracer.AddMessageHop(this.mId, this.timeKeeper.Now.TotalMilliseconds, nodeId, true);
            }
        }

        public void Drop(int nodeId, string reason)
        {
            Logger.TraceInformation(LogTag.Simulator, "Message from {0} to {1} has been dropped. ({2})", this.Source, this.Destination, reason);

            if (this.mTracer != null)
            {
                this.mTracer.DropMessage(this.mId, this.timeKeeper.Now.TotalMilliseconds, nodeId, reason);
            }
        }

        public void Drop(string reason)
        {
            this.Drop(this.LastHop, reason);
        }
    }
}
