using System;
using System.Collections.Generic;
using System.Text;

using Badumna.Core;
using Badumna.Transport;
using Badumna;
using Badumna.Utilities;
using Badumna.DataTypes;

namespace NetworkSimulator.ProtocolStacks.Services
{
    class SimpleSend : TransportProtocol, ISimpleSend
    {
        private int mNumMessagesArrived = 0;
        public int NumMessagesArrived
        {
            get { return this.mNumMessagesArrived; }
            set { this.mNumMessagesArrived = value; }
        }

        public SimpleSend(TransportProtocol layer1)
            : base(layer1)
        {
        }

        public void SendTo(BadumnaId destination, string payload)
        {
            TransportEnvelope envelope = this.GetMessageFor(destination.Address, QualityOfService.Reliable);

            Logger.TraceInformation(LogTag.Simulator, "Sending message to {0}", envelope.Destination);

            this.RemoteCall(envelope, this.TestMethod, payload);
            this.SendMessage(envelope);
        }

        [ConnectionfulProtocolMethod(0)]
        private void TestMethod(string payload)
        {
            this.mNumMessagesArrived++;

            Logger.TraceInformation(LogTag.Simulator, "Message arrived");
        }
    }
}
