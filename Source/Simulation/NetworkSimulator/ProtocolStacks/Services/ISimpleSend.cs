﻿using System;
namespace NetworkSimulator.ProtocolStacks.Services
{
    public interface ISimpleSend
    {
        int NumMessagesArrived { get; set; }
        void SendTo(Badumna.DataTypes.BadumnaId destination, string payload);
    }
}
