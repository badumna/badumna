//------------------------------------------------------------------------------
// <copyright file="SimulatedTransport.cs" company="National ICT Australia Limited">
//     Copyright (c) 2012 National ICT Australia Limited. All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using Badumna;
using Badumna.Core;
using Badumna.Transport;
using Badumna.Utilities;
using NetworkSimulator.Instrumentation;

namespace NetworkSimulator
{
    /// <summary>
    /// A <see cref="Transport"/> that sends packets via a simulated network.
    /// </summary>
    /// <remarks>
    /// Each simulated peer in the network has its own SimulatedTransport.
    /// </remarks>
    internal class SimulatedTransport : Transport
    {
        /// <summary>
        /// Total size of the typical IP and UDP headers attached to each packet.
        /// </summary>
        private const int IPAndUDPHeaderSize = 28;

        /// <summary>
        /// Number of inbound bytes currently in flight and buffered locally.
        /// </summary>
        private double inboundBytesInTransit;

        /// <summary>
        /// Number of outbound bytes currently in flight and buffered locally.
        /// </summary>
        private double outboundBytesInTransit;

        /// <summary>
        /// The maximum inbound bandwidth available to the transport, in kilobits / second.
        /// </summary>
        private double inboundBandwidth;

        /// <summary>
        /// The maximum outbound bandwidth available to the transport, in kilobits / second.
        /// </summary>
        private double outboundBandwidth;

        /// <summary>
        /// The size of both the inbound and outbound buffers (there are two buffers, each of this size).
        /// </summary>
        private double bufferSize;

        /// <summary>
        /// The router this transport is attached to.
        /// </summary>
        private Router router;

        /// <summary>
        /// The message tracer
        /// </summary>
        private IMessageTracer messageTracer;

        /// <summary>
        /// The message tracer id for this transport
        /// </summary>
        private int tracerId;

        /// <summary>
        /// The timekeeper
        /// </summary>
        private ITime timeKeeper;

        /// <summary>
        /// The simulator facade
        /// </summary>
        private SimulatorFacade simulatorFacade;

        /// <summary>
        /// Initializes a new instance of the SimulatedTransport class.
        /// </summary>
        /// <param name="bufferSize">The size of the inbound and the outbound buffers</param>
        /// <param name="inboundBandwidth">The maximum inbound bandwidth in kilobits / second.</param>
        /// <param name="outboundBandwidth">The maximum outbound bandwidth in kilobits / second.</param>
        /// <param name="publicAddress">The public address of the transport</param>
        /// <param name="privateAddress">The private address of the transport</param>
        /// <param name="connectivityReporter">Connectivity reporter</param>
        /// <param name="router">The router the transport is connected to</param>
        /// <param name="messageTracer">The message tracer</param>
        /// <param name="eventQueue">The event queue</param>
        /// <param name="timeKeeper">The timekeeper</param>
        public SimulatedTransport(
            double bufferSize,
            double inboundBandwidth,
            double outboundBandwidth,
            PeerAddress publicAddress,
            PeerAddress privateAddress,
            INetworkConnectivityReporter connectivityReporter,
            Router router,
            IMessageTracer messageTracer,
            NetworkEventQueue eventQueue,
            ITime timeKeeper)
            : base(eventQueue, timeKeeper, connectivityReporter)
        {
            this.bufferSize = bufferSize;
            this.inboundBandwidth = inboundBandwidth;
            this.outboundBandwidth = outboundBandwidth;
            
            this.Address = publicAddress;
            this.PrivateAddress = privateAddress;

            this.router = router;
            this.timeKeeper = timeKeeper;

            this.SetAddresses(new List<PeerAddress>(new PeerAddress[] { new PeerAddress(privateAddress) }), new PeerAddress(publicAddress));

            this.messageTracer = messageTracer;
            if (this.messageTracer != null)
            {
                this.tracerId = this.messageTracer.AddNode(timeKeeper.Now.TotalMilliseconds, this.Address.ToString());
            }
        }

        /// <summary>
        /// Gets the connectivity reporter.
        /// </summary>
        public INetworkConnectivityReporter NetworkConnectivityReporter
        {
            get { return this.ConnectivityReporter; }
        }

        /// <summary>
        /// Gets the total number of packets sent by this transport.
        /// </summary>
        public int TotalPacketsSent { get; private set; }

        /// <summary>
        /// Gets the total number of packets received by this transport.
        /// </summary>
        public int TotalPacketsReceived { get; private set; }

        /// <summary>
        /// Gets the public address of this transport.
        /// </summary>
        public PeerAddress Address { get; private set; }

        /// <summary>
        /// Gets the private address of this transport.
        /// </summary>
        public PeerAddress PrivateAddress { get; private set; }

        /// <summary>
        /// Sets the simulator facade that this transport belongs to.
        /// </summary>
        /// <param name="simulatorFacade">The simulator facade</param>
        public void SetSimulatorFacade(SimulatorFacade simulatorFacade)
        {
            this.simulatorFacade = simulatorFacade;
        }

        /// <summary>
        /// Calculates the delay in milliseconds for a packet of the length specified to pass from the
        /// router, through the transport's buffer, and be ready for processing.
        /// </summary>
        /// <param name="length">The length of the packet</param>
        /// <returns>The delay in milliseconds, or -1 if the transport's buffer will not be big enough to hold the message</returns>
        public double GetMsDelayForInboundMessage(int length)
        {
            int packetSize = length + SimulatedTransport.IPAndUDPHeaderSize;

            double inboundCapacityBytesPerMs = this.inboundBandwidth / 8;
            double maxInboundBytesInTransit = inboundCapacityBytesPerMs * Router.LocalNetworkLatencyMs;

            double queueLength = Math.Max(0, this.inboundBytesInTransit + packetSize - maxInboundBytesInTransit);
            if (this.bufferSize < queueLength)
            {
                return -1;
            }

            this.inboundBytesInTransit += packetSize;

            return queueLength / inboundCapacityBytesPerMs;
        }

        /// <summary>
        /// Handles an incoming message destined for this transport.
        /// </summary>
        /// <param name="messageWrapper">The message</param>
        /// <param name="destinationPort">The destination port</param>
        public void ReceiveMessage(MessageWrapper messageWrapper, int destinationPort)
        {
            if (this.ConnectivityReporter.Status != ConnectivityStatus.Online)
            {
                Logger.TraceInformation(LogTag.Simulator, "{0} ignoring message from {1} as not online.", this.PublicAddress, messageWrapper.Source.EndPoint);
                return;
            }

            int packetLength = messageWrapper.Length + SimulatedTransport.IPAndUDPHeaderSize;
            this.TotalPacketsReceived++;
            this.inboundBytesInTransit -= packetLength;

            messageWrapper.HasArrived(this.tracerId);

            messageWrapper.Envelope.ResetParseOffset();
            PeerAddress source = new PeerAddress(messageWrapper.Source.EndPoint, NatType.Unknown);
            messageWrapper.Envelope.Source = source;
            messageWrapper.Envelope.SetDestination(this.Address);
            messageWrapper.Envelope.ArrivalTime = this.timeKeeper.Now;

            Logger.TraceInformation(LogTag.Simulator, "{0} receives {1} bytes from {2}", this.PublicAddress, packetLength, messageWrapper.Source);
            this.ProcessMessageArrival(messageWrapper.Envelope);
        }

        /// <summary>
        /// Remove the specified number of bytes from the outbound bytes in transit,
        /// because a message of the given length has been received and forwarded by the router.
        /// </summary>
        /// <param name="length">The number of bytes sent</param>
        public void DequeFromOutbound(double length)
        {
            this.outboundBytesInTransit -= length + SimulatedTransport.IPAndUDPHeaderSize;
        }

        /// <inheritdoc />
        protected override void SendPacket(TransportEnvelope envelope)
        {
            if (!Parameters.EncryptTraffic && this.simulatorFacade != null)
            {
                envelope.SetLabel(this.simulatorFacade.Stack.Describe(envelope));
            }

            MessageWrapper message = new MessageWrapper(
                envelope,
                this.PrivateAddressesList[0],
                new PeerAddress(envelope.Destination.EndPoint, NatType.Unknown),
                this.messageTracer,
                this.tracerId,
                this.timeKeeper);

            int packetSize = envelope.Length + SimulatedTransport.IPAndUDPHeaderSize;

            double outboundCapacityBytesPerMs = this.outboundBandwidth / 8;
            double maxOutboundBytesInTransit = outboundCapacityBytesPerMs * Router.LocalNetworkLatencyMs;

            double queueLength = Math.Max(0, (this.outboundBytesInTransit + packetSize) - maxOutboundBytesInTransit);

            if (this.bufferSize < queueLength)
            {
                message.Drop("Source buffer overflow");
                return;
            }

            this.outboundBytesInTransit += packetSize;

            this.TotalPacketsSent++;

            int delayMs = (int)((queueLength / outboundCapacityBytesPerMs) + Router.LocalNetworkLatencyMs);

            this.EventQueue.Schedule(delayMs, this.router.RouteMessage, message);
            this.UpdateSendStatistics(envelope.Length);

            Logger.TraceInformation(LogTag.Simulator, "{0} sends {1} bytes to {2}", this.PublicAddress, packetSize, envelope.Destination);
        }
    }
}
