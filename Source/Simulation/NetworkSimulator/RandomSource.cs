using System;
using System.Collections.Generic;
using System.Text;

namespace NetworkSimulator
{
    public class RandomSource
    {
        public static Random Generator { get { return Badumna.Utilities.RandomSource.Generator; } }

        public static void Initialize()
        {
            Badumna.Utilities.RandomSource.Initialize();
        }

        public static void Initialize(int seed)
        {
            Badumna.Utilities.RandomSource.Initialize(seed);
        }
    }
}
