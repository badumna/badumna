using System;
using System.Collections.Generic;
using System.Text;


namespace NetworkSimulator
{
    public interface ISimulation
    {
        bool PauseOnFailure { set; get; }
        void InitializeNodeHandler(object sender, NodeInitializationEventArgs e);
        void Run();
        void Render(System.Drawing.Graphics context, Dictionary<string, object> options);
    }
}
