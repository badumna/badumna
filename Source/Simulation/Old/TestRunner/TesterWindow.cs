using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Reflection;

using NetworkSimulator;

using ExperimentLibrary;

namespace TestRunner
{
    public partial class TesterWindow : Form
    {
        private List<ISimulation> mSimulations = new List<ISimulation>();
        private ParameterSampler mSampler = new ParameterSampler();
        private Dictionary<double, Results> mRunResults = new Dictionary<double, Results>();
        private List<String> mParametersToPlot = new List<string>();

        public TesterWindow()
        {
            InitializeComponent();
            this.button1.Click += new EventHandler(button1_Click);
            this.networkCharacteristicsControl1.ProgressChanged += networkCharacteristicsControl1_ProgressChanged;
            this.networkCharacteristicsControl1.ParameterSampleCompleteEvent += networkCharacteristicsControl1_RunCompleteEvent;
            this.PopulateTestList();

            this.comboBox1.SelectedIndexChanged += new EventHandler(comboBox1_SelectedIndexChanged);
        }

        void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.SelectParameterToPlot(this.comboBox1.SelectedItem as String);
        }

        private void SelectParameterToPlot(String parameter)
        {
            if (this.checkBox1.CheckState != CheckState.Checked)
            {
                this.resultPlotterControl1.Reset();
                this.resultPlotterControl1.ClearParameters();
            }

            this.resultPlotterControl1.AddParameter(parameter);
            this.resultPlotterControl1.PlotResults(this.mRunResults);
        }

        void networkCharacteristicsControl1_RunCompleteEvent(object sender, SimulationRunEventArgs e)
        {
        }

        void networkCharacteristicsControl1_ProgressChanged(object sender, SimulationRunEventArgs e)
        {
            if (!this.mRunResults.ContainsKey(e.SamplePoint))
            {
                this.mRunResults.Add(e.SamplePoint, new Results());
            }

            Results.Instance.CaptureSummary(this.mRunResults[e.SamplePoint]);
            this.progressBar1.Increment(1);

            List<String> keyNames = new List<string>();
            this.mRunResults[e.SamplePoint].FillKeyList(keyNames);

            this.resultPlotterControl1.PlotResults(this.mRunResults);

            foreach (String key in keyNames)
            {
                if (!this.comboBox1.Items.Contains(key))
                {
                    this.comboBox1.Items.Add(key);
                }
            }
        }

        void button1_Click(object sender, EventArgs e)
        {
            int numberOfRuns = (int)this.numericUpDown1.Value;
            this.progressBar1.Value = 0;
            this.progressBar1.Maximum = this.networkCharacteristicsControl1.NumberOfParameterisations * numberOfRuns;

            ISimulation simulation = this.mSimulations[this.comboBox2.SelectedIndex];

            simulation.PauseOnFailure = this.pauseOnFailureCheckBox.CheckState == CheckState.Checked;
            this.networkCharacteristicsControl1.RunSimulation(simulation, numberOfRuns);

            this.mRunResults = new Dictionary<double, Results>();

            ParamterControl parameter = ParamterControl.Selected;

            if (null != parameter)
            {
                this.resultPlotterControl1.Clear();
                this.resultPlotterControl1.Initialize(this.mSampler, "Test results", parameter.ParameterName);
            }
        }
                

        private void TesterWindow_Load(object sender, EventArgs e)
        {
            this.networkCharacteristicsControl1.ParametersChanged += new EventHandler(networkCharacteristicsControl1_ParametersChanged);
        }

        void networkCharacteristicsControl1_ParametersChanged(object sender, EventArgs e)
        {
            this.textBox1.Text = this.networkCharacteristicsControl1.NumberOfParameterisations.ToString();
        }

        private void PopulateTestList()
        {
            this.comboBox2.Items.Add("Connectivity Test");
            this.mSimulations.Add(new ConnectivitySimulation());

            this.comboBox2.Items.Add("Distributed Connectivity Test");
            this.mSimulations.Add(new DistributedConnectivitySimulation());

            this.comboBox2.Items.Add("Dht Route Test");
            this.mSimulations.Add(new DhtRouteSimulation());

            this.comboBox2.Items.Add("DHT Poke Test");
            this.mSimulations.Add(new DhtPokeSimulation());

            this.comboBox2.Items.Add("DHT Replica Test");
            this.mSimulations.Add(new DhtDataSimulation());

            this.comboBox2.Items.Add("Interest Management Test");
            this.mSimulations.Add(new DistributedIMSimulation());

            this.comboBox2.Items.Add("Multicast Test");
            this.mSimulations.Add(new MulticastSimulation());


            this.comboBox2.SelectedIndex = 0;
        }
    }
}