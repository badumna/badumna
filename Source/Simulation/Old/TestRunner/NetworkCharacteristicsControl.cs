using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.IO;
using System.Windows.Forms;
using System.Deployment.Application;

using Badumna.Utilities;
using NetworkSimulator;

namespace TestRunner
{
    public partial class NetworkCharacteristicsControl : UserControl
    {
        private int mNumberOfParameterisations = 1;
        public int NumberOfParameterisations { get { return this.mNumberOfParameterisations; } }

        private Results mResults = new Results();
        public Results SimulationResults { get { return this.mResults; } }

        private bool mTopologyHasLoaded;
        private Topology mTopology = new Topology();
        private List<NetworkInitializer> mInitializers = new List<NetworkInitializer>();

        private int mParameterIndex;
        private decimal[] mParameterRange;
        private double mSamplePoint;
        private String mParameterName;

        public event EventHandler ParametersChanged;
        public event EventHandler<SimulationRunEventArgs> ProgressChanged;
        public event EventHandler<SimulationRunEventArgs> ParameterSampleCompleteEvent;

        public NetworkCharacteristicsControl()
        {
            InitializeComponent();

            this.button1.Click += new EventHandler(button1_Click);

            this.latencyVariationControl.ValueChanged += this.HandleParameterChange;
            this.reliabilityParameter.ValueChanged += this.HandleParameterChange;
            this.uploadParameter.ValueChanged += this.HandleParameterChange;
            this.downloadParamter.ValueChanged += this.HandleParameterChange;
            this.queueSizeParameter.ValueChanged += this.HandleParameterChange;
            this.avgUptimeParamter.ValueChanged += this.HandleParameterChange;
            this.stdDevUptimeParamter.ValueChanged += this.HandleParameterChange;
            this.avgDowntimeParamter.ValueChanged += this.HandleParameterChange;
            this.stdDevDowntimeParamter.ValueChanged += this.HandleParameterChange;
            this.numberOfPeersParamter.ValueChanged += this.HandleParameterChange;
            this.gracefuleExitParameter.ValueChanged += this.HandleParameterChange;

            this.LoadTopologyFile("TenPeersTopology.txt");
        }
        

        void button1_Click(object sender, EventArgs e)
        {
            this.openFileDialog1.ShowDialog(this);
            this.LoadTopologyFile(this.openFileDialog1.FileName);
        }

        private void LoadTopologyFile(String fileName)
        {
            if (File.Exists(fileName))
            {
                FileInfo fileInfo = new FileInfo(fileName);

                this.textBox1.ForeColor = Color.Black;
                this.textBox1.Text = fileInfo.Name;
                this.mTopology = new Topology();
                this.mTopology.Load<Domain>(fileName);
                this.mTopologyHasLoaded = true;
            }
            else
            {
                this.textBox1.ForeColor = Color.Red;
                this.textBox1.Text = "No topology files specified.";
            }
        }


        private void HandleParameterChange(object sender, EventArgs e)
        {
            this.mNumberOfParameterisations = 1;
            this.mNumberOfParameterisations *= this.latencyVariationControl.NumberOfParameterisations;
            this.mNumberOfParameterisations *= this.reliabilityParameter.NumberOfParameterisations;
            this.mNumberOfParameterisations *= this.uploadParameter.NumberOfParameterisations;
            this.mNumberOfParameterisations *= this.downloadParamter.NumberOfParameterisations;
            this.mNumberOfParameterisations *= this.queueSizeParameter.NumberOfParameterisations;
            this.mNumberOfParameterisations *= this.avgUptimeParamter.NumberOfParameterisations;
            this.mNumberOfParameterisations *= this.stdDevUptimeParamter.NumberOfParameterisations;
            this.mNumberOfParameterisations *= this.avgDowntimeParamter.NumberOfParameterisations;
            this.mNumberOfParameterisations *= this.stdDevDowntimeParamter.NumberOfParameterisations;
            this.mNumberOfParameterisations *= this.numberOfPeersParamter.NumberOfParameterisations;
            this.mNumberOfParameterisations *= this.gracefuleExitParameter.NumberOfParameterisations;

            if (null != this.ParametersChanged)
            {
                this.ParametersChanged(this, new EventArgs());
            }
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private ISimulation mSimulation;
        private int mNumberOfRuns;


        public void RunSimulation(ISimulation simulation, int numberOfRuns)
        {
            if (!this.mTopologyHasLoaded)
            {
                return;
            }

            this.mTopology.Reset();
            Simulator.Instance.Reset();

            this.mSimulation = simulation;
            this.mNumberOfRuns = numberOfRuns;

            if (null != ParamterControl.Selected)
            {
                this.mParameterName = ParamterControl.Selected.ParameterName;
                this.mParameterRange = ParamterControl.Selected.ValueRange;
                this.mParameterIndex = 0;
            }

            this.NextSamplePoint();
            this.LoadInitializers(this.mInitializers);
            this.RunSimulationInThread();
        }

        private void NextSamplePoint()
        {
            this.mSamplePoint = 0.0;
            if (null != this.mParameterRange && this.mParameterIndex < this.mParameterRange.Length)
            {
                this.mSamplePoint = (double)this.mParameterRange[this.mParameterIndex++];
            }
        }            

        public void ParameterSampleComplete(object sender, EventArgs e)
        {

            if (null != this.ParameterSampleCompleteEvent)
            {
                this.ParameterSampleCompleteEvent(this, new SimulationRunEventArgs(this.mSamplePoint));
            }

            this.NextSamplePoint();
            this.RunSimulationInThread();
        }
        
        public void IncrementProgress(object sender, EventArgs e)
        {
            if (null != this.ProgressChanged)
            {
                this.ProgressChanged(this, new SimulationRunEventArgs(this.mSamplePoint));
            }
        }

        private void RunSimulationInThread()
        {
            if (this.mInitializers.Count > 0)
            {
                SimulationThread thread = new SimulationThread();
                NetworkInitializer initializer = this.mInitializers[0];

                this.mInitializers.RemoveAt(0);
                initializer.Topology = this.mTopology;

                thread.Start(this, initializer, String.Format("{0} {1}", this.mParameterName, this.mSamplePoint), 
                    this.mSimulation, this.mNumberOfRuns);
            }
        }

        private void LoadInitializers(List<NetworkInitializer> inializerList)
        {
            this.mTopology.HasFullConectivity = this.checkBox1.CheckState == CheckState.Checked;
            this.mTopology.RandomNatSelection = this.checkBox2.CheckState == CheckState.Checked;

            foreach (decimal latency in this.latencyVariationControl.ValueRange)
            {
                foreach (decimal reliability in this.reliabilityParameter.ValueRange)
                {
                    foreach (decimal upload in this.uploadParameter.ValueRange)
                    {
                        foreach (decimal download in this.downloadParamter.ValueRange)
                        {
                            foreach (decimal queueSize in this.queueSizeParameter.ValueRange)
                            {
                                foreach (decimal avgUptime in this.avgUptimeParamter.ValueRange)
                                {
                                    foreach (decimal stdDevUptime in this.stdDevUptimeParamter.ValueRange)
                                    {
                                        foreach (decimal avgDowntime in this.avgDowntimeParamter.ValueRange)
                                        {
                                            foreach (decimal stdDevDowntime in this.stdDevDowntimeParamter.ValueRange)
                                            {
                                                foreach (decimal numberOfNodes in this.numberOfPeersParamter.ValueRange)
                                                {
                                                    foreach (decimal chanceOfGracefulExit in this.gracefuleExitParameter.ValueRange)
                                                    {
                                                        NetworkInitializer initializer = new NetworkInitializer();

                                                        initializer.Topology = this.mTopology;
                                                        initializer.LatencyVariation = (double)latency / 100.0;
                                                        initializer.Reliability = (double)reliability;
                                                        initializer.NodeOutboundBandwidthCapacity = (double)upload;
                                                        initializer.NodeInboundBandwidthCapacity = (double)download;
                                                        initializer.NodeQueueBufferSize = (double)queueSize;
                                                        initializer.AverageUpTimeMilliseconds = (double)avgUptime * 1000.0;
                                                        initializer.StdDevUpTimeMilliseconds = (double)stdDevUptime * 1000.0;
                                                        initializer.AverageDownTimeMilliseconds = (double)avgDowntime * 1000.0;
                                                        initializer.StdDevDownTimeMilliseconds = (double)stdDevDowntime * 1000.0;
                                                        initializer.NumberOfNodes = (int)numberOfNodes;
                                                        initializer.ChanceOfGracefulExit = (double)chanceOfGracefulExit / 100.0;

                                                        inializerList.Add(initializer);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

    }



    public class SimulationRunEventArgs : EventArgs
    {
        private double mSamplePoint;
        public double SamplePoint { get { return this.mSamplePoint; } }

        public SimulationRunEventArgs(double samplePoint)
            : base()
        {
            this.mSamplePoint = samplePoint;
        }
    }
}
