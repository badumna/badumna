using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace TestRunner
{
    public partial class ParamterControl : UserControl
    {
        public event EventHandler ValueChanged;
        private static event EventHandler UnfixAllEvent;

        private static ParamterControl mSelected;
        public static ParamterControl Selected { get { return ParamterControl.mSelected; } }

        private String mParameterName;
        public String ParameterName
        {
            get { return this.mParameterName; }
            set { this.mParameterName = value; this.groupBox1.Text = value; }
        }

        private bool mValueChanged;

        public decimal MinimumValue
        {
            get { return this.fixedUpDown.Minimum; } 
            set
            {
                this.fixedUpDown.Minimum = value;
                this.minimumUpDown.Minimum = value;
                this.maximumUpDown.Minimum = value;
                this.stepUpDown.Value = (this.fixedUpDown.Maximum - this.fixedUpDown.Minimum) / (decimal)100;
                this.mValueChanged = true;
            }               
        }

        public decimal MaximumValue
        {
            get { return this.fixedUpDown.Maximum; }
            set
            {
                this.fixedUpDown.Maximum = value;
                this.minimumUpDown.Maximum = value;
                this.maximumUpDown.Maximum = value;
                this.maximumUpDown.Value = value;
                this.stepUpDown.Maximum = value / (decimal)2;
                this.stepUpDown.Value = (this.fixedUpDown.Maximum - this.fixedUpDown.Minimum) / (decimal)100;
                this.mValueChanged = true;
            }
        }

        public bool IsFixedValue
        {
            get { return this.checkBox1.CheckState == CheckState.Checked; }
        }

        public decimal FixedValue
        {
            set { this.checkBox1.CheckState = CheckState.Checked; this.fixedUpDown.Value = value; }
            get { this.mValueChanged = true; return this.fixedUpDown.Value; }
        }

        public int NumberOfParameterisations
        {
            get
            {
                if (this.IsFixedValue)
                {
                    return 1;
                }

                return (int)((this.maximumUpDown.Value - this.minimumUpDown.Value) / this.stepUpDown.Value + (decimal)0.5);
            }
        }

        private decimal[] mValueRange;
        public decimal[] ValueRange 
        { 
            get 
            {
                this.CalculateRange();
                return this.mValueRange; 
            }
        }

        public ParamterControl()
        {
            InitializeComponent();

            ParamterControl.UnfixAllEvent += new EventHandler(ParamterControl_UnfixAllEvent);

            this.checkBox1.CheckedChanged += new EventHandler(checkBox1_CheckedChanged);
            this.checkBox1.CheckState = CheckState.Checked;

            this.stepUpDown.Minimum = 0;

            this.fixedUpDown.Maximum = decimal.MaxValue;
            this.maximumUpDown.Maximum = decimal.MaxValue;
            this.minimumUpDown.Maximum = decimal.MaxValue;

            this.fixedUpDown.ValueChanged += new EventHandler(HandleValueChanged);
            this.minimumUpDown.ValueChanged += new EventHandler(HandleValueChanged);
            this.maximumUpDown.ValueChanged += new EventHandler(HandleValueChanged);
            this.stepUpDown.ValueChanged += new EventHandler(HandleValueChanged);
            this.mValueChanged = true;
        }

        void ParamterControl_UnfixAllEvent(object sender, EventArgs e)
        {
            if (!sender.Equals(this))
            {
                this.checkBox1.CheckState = CheckState.Checked;
            }
        }

        void HandleValueChanged(object sender, EventArgs e)
        {
            if (null != this.ValueChanged)
            {
                this.ValueChanged(this, e);
            }
            this.mValueChanged = true;
        }

        void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            bool enableFixed = this.checkBox1.CheckState == CheckState.Checked;

            if (!enableFixed)
            {
                ParamterControl.UnfixAllEvent.Invoke(this, e);
                ParamterControl.mSelected = this;
            }
            else
            {
                if (this.Equals(ParamterControl.mSelected))
                {
                    ParamterControl.mSelected = null;
                }
            }

            this.minimumUpDown.Enabled = !enableFixed;
            this.maximumUpDown.Enabled = !enableFixed;
            this.stepUpDown.Enabled = !enableFixed;
            this.fixedUpDown.Enabled = enableFixed;
            this.mValueChanged = true;
        }

        private void CalculateRange()
        {
            if (!this.mValueChanged)
            {
                return;
            }
            this.mValueChanged = false;

            if (this.IsFixedValue)
            {
                this.mValueRange = new decimal[1] { this.FixedValue };
                return;
            }

            this.mValueRange = new decimal[1 + (int)((this.maximumUpDown.Value - this.minimumUpDown.Value) / this.stepUpDown.Value)];

            int i = 0;
            for (decimal v = this.minimumUpDown.Value; v <= this.maximumUpDown.Value; v += this.stepUpDown.Value)
            {
                this.mValueRange[i++] = v;
            }
        }

    }
}
