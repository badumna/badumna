using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Threading;

using Badumna.Utilities;
using NetworkSimulator;

namespace TestRunner
{
    public class SimulationThread
    {
        private static TraceWindow mTraceWindow;
        private ISimulation mSimulation;
        private NetworkInitializer mInitializer;
        private NetworkCharacteristicsControl mController;
        private int mNumberOfRuns;
        private String mDescription;

        private Thread mThread;

        public SimulationThread()
        {
            if (null == SimulationThread.mTraceWindow)
            {
                SimulationThread.mTraceWindow = new TraceWindow();
                SimulationThread.mTraceWindow.Show();
            }
         
            SimulationThread.mTraceWindow.Exit += new EventHandler(mTraceWindow_Exit);
            Application.ApplicationExit += this.mTraceWindow_Exit;

        }

        void mTraceWindow_Exit(object sender, EventArgs e)
        {
            if (null != this.mThread)
            {
                this.mThread.Abort();
            }

            SimulationThread.mTraceWindow = null;
        }

        public void Start(NetworkCharacteristicsControl controller, NetworkInitializer initializer, String description,
            ISimulation simulation, int numberOfRuns)
        {
            this.mController = controller;
            this.mSimulation = simulation;
            this.mInitializer = initializer;
            this.mNumberOfRuns = numberOfRuns;
            this.mDescription = description;

            SimulationThread.mTraceWindow.Simulation = this.mSimulation;

            this.mThread = new Thread(this.RunSimulation);

            Domain.Reliability = initializer.Reliability;
            this.mThread.Start();
        }

        private void RunSimulation()
        {

            for (int i = 0; i < this.mNumberOfRuns; i++)
            {
                SimulationThread.mTraceWindow.Description = String.Format("{0} : Run {1}", this.mDescription, i);
                this.mInitializer.Topology.ClearNodes();

                this.mInitializer.InitializeNodeEvent += SimulationThread.mTraceWindow.RecordNewNode;
                this.mInitializer.InitializeNodes(this.mSimulation.InitializeNodeHandler);
                this.mInitializer.InitializeNodeEvent -= SimulationThread.mTraceWindow.RecordNewNode;
                
                this.mSimulation.Run();
                this.mController.Invoke(new EventHandler(this.mController.IncrementProgress), this, new EventArgs());

                SimulationThread.mTraceWindow.ResetTrace();
                SimulationThread.mTraceWindow.Clear();

                Simulator.Instance.Reset();
                Results.Instance.Clear();
            }


            SimulationThread.mTraceWindow.Simulation = null;
            this.mController.Invoke(new EventHandler(this.mController.ParameterSampleComplete), this, new EventArgs());
        }

    }
}
