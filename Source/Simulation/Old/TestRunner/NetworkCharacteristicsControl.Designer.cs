namespace TestRunner
{
    partial class NetworkCharacteristicsControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.numberOfPeersParamter = new TestRunner.ParamterControl();
            this.stdDevDowntimeParamter = new TestRunner.ParamterControl();
            this.avgDowntimeParamter = new TestRunner.ParamterControl();
            this.stdDevUptimeParamter = new TestRunner.ParamterControl();
            this.avgUptimeParamter = new TestRunner.ParamterControl();
            this.queueSizeParameter = new TestRunner.ParamterControl();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.downloadParamter = new TestRunner.ParamterControl();
            this.uploadParameter = new TestRunner.ParamterControl();
            this.latencyVariationControl = new TestRunner.ParamterControl();
            this.reliabilityParameter = new TestRunner.ParamterControl();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.gracefuleExitParameter = new TestRunner.ParamterControl();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.gracefuleExitParameter);
            this.groupBox1.Controls.Add(this.numberOfPeersParamter);
            this.groupBox1.Controls.Add(this.stdDevDowntimeParamter);
            this.groupBox1.Controls.Add(this.avgDowntimeParamter);
            this.groupBox1.Controls.Add(this.stdDevUptimeParamter);
            this.groupBox1.Controls.Add(this.avgUptimeParamter);
            this.groupBox1.Controls.Add(this.queueSizeParameter);
            this.groupBox1.Controls.Add(this.groupBox3);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.downloadParamter);
            this.groupBox1.Controls.Add(this.uploadParameter);
            this.groupBox1.Controls.Add(this.latencyVariationControl);
            this.groupBox1.Controls.Add(this.reliabilityParameter);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(637, 722);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Network Characteristics";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // numberOfPeersParamter
            // 
            this.numberOfPeersParamter.FixedValue = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numberOfPeersParamter.Location = new System.Drawing.Point(6, 611);
            this.numberOfPeersParamter.MaximumValue = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numberOfPeersParamter.MinimumValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numberOfPeersParamter.Name = "numberOfPeersParamter";
            this.numberOfPeersParamter.ParameterName = "Number of Peers";
            this.numberOfPeersParamter.Size = new System.Drawing.Size(625, 48);
            this.numberOfPeersParamter.TabIndex = 11;
            // 
            // stdDevDowntimeParamter
            // 
            this.stdDevDowntimeParamter.FixedValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.stdDevDowntimeParamter.Location = new System.Drawing.Point(6, 557);
            this.stdDevDowntimeParamter.MaximumValue = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.stdDevDowntimeParamter.MinimumValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.stdDevDowntimeParamter.Name = "stdDevDowntimeParamter";
            this.stdDevDowntimeParamter.ParameterName = "Standard Deviation Node Downtime (Seconds)";
            this.stdDevDowntimeParamter.Size = new System.Drawing.Size(625, 48);
            this.stdDevDowntimeParamter.TabIndex = 10;
            // 
            // avgDowntimeParamter
            // 
            this.avgDowntimeParamter.FixedValue = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.avgDowntimeParamter.Location = new System.Drawing.Point(6, 503);
            this.avgDowntimeParamter.MaximumValue = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.avgDowntimeParamter.MinimumValue = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.avgDowntimeParamter.Name = "avgDowntimeParamter";
            this.avgDowntimeParamter.ParameterName = "Average Node Downtime (Seconds) ";
            this.avgDowntimeParamter.Size = new System.Drawing.Size(625, 48);
            this.avgDowntimeParamter.TabIndex = 9;
            // 
            // stdDevUptimeParamter
            // 
            this.stdDevUptimeParamter.FixedValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.stdDevUptimeParamter.Location = new System.Drawing.Point(6, 449);
            this.stdDevUptimeParamter.MaximumValue = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.stdDevUptimeParamter.MinimumValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.stdDevUptimeParamter.Name = "stdDevUptimeParamter";
            this.stdDevUptimeParamter.ParameterName = "Standard Deviation Node Uptime (Seconds)";
            this.stdDevUptimeParamter.Size = new System.Drawing.Size(625, 48);
            this.stdDevUptimeParamter.TabIndex = 8;
            // 
            // avgUptimeParamter
            // 
            this.avgUptimeParamter.FixedValue = new decimal(new int[] {
            3600,
            0,
            0,
            0});
            this.avgUptimeParamter.Location = new System.Drawing.Point(6, 395);
            this.avgUptimeParamter.MaximumValue = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.avgUptimeParamter.MinimumValue = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.avgUptimeParamter.Name = "avgUptimeParamter";
            this.avgUptimeParamter.ParameterName = "Average Node Uptime (Seconds)";
            this.avgUptimeParamter.Size = new System.Drawing.Size(625, 48);
            this.avgUptimeParamter.TabIndex = 7;
            // 
            // queueSizeParameter
            // 
            this.queueSizeParameter.FixedValue = new decimal(new int[] {
            128000,
            0,
            0,
            0});
            this.queueSizeParameter.Location = new System.Drawing.Point(6, 341);
            this.queueSizeParameter.MaximumValue = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.queueSizeParameter.MinimumValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.queueSizeParameter.Name = "queueSizeParameter";
            this.queueSizeParameter.ParameterName = "Node Queue Size (Bytes)";
            this.queueSizeParameter.Size = new System.Drawing.Size(625, 48);
            this.queueSizeParameter.TabIndex = 6;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.checkBox2);
            this.groupBox3.Controls.Add(this.checkBox1);
            this.groupBox3.Location = new System.Drawing.Point(6, 73);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(625, 46);
            this.groupBox3.TabIndex = 5;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Topology Parameters";
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(115, 19);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(205, 17);
            this.checkBox2.TabIndex = 1;
            this.checkBox2.Text = "Simulate Network Address Translation";
            this.checkBox2.UseVisualStyleBackColor = true;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Checked = true;
            this.checkBox1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox1.Location = new System.Drawing.Point(7, 20);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(102, 17);
            this.checkBox1.TabIndex = 0;
            this.checkBox1.Text = "Full connectivity";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.button1);
            this.groupBox2.Controls.Add(this.textBox1);
            this.groupBox2.Location = new System.Drawing.Point(6, 21);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(625, 46);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Topology File";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(544, 20);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "Select file";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(7, 20);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(531, 20);
            this.textBox1.TabIndex = 0;
            // 
            // downloadParamter
            // 
            this.downloadParamter.FixedValue = new decimal(new int[] {
            640,
            0,
            0,
            65536});
            this.downloadParamter.Location = new System.Drawing.Point(6, 281);
            this.downloadParamter.MaximumValue = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.downloadParamter.MinimumValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.downloadParamter.Name = "downloadParamter";
            this.downloadParamter.ParameterName = "Node Donload Limit (Kb/s)";
            this.downloadParamter.Size = new System.Drawing.Size(625, 54);
            this.downloadParamter.TabIndex = 3;
            // 
            // uploadParameter
            // 
            this.uploadParameter.FixedValue = new decimal(new int[] {
            640,
            0,
            0,
            65536});
            this.uploadParameter.Location = new System.Drawing.Point(6, 229);
            this.uploadParameter.MaximumValue = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.uploadParameter.MinimumValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.uploadParameter.Name = "uploadParameter";
            this.uploadParameter.ParameterName = "Node Upload Limit (Kb/s) ";
            this.uploadParameter.Size = new System.Drawing.Size(625, 46);
            this.uploadParameter.TabIndex = 2;
            // 
            // latencyVariationControl
            // 
            this.latencyVariationControl.FixedValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.latencyVariationControl.Location = new System.Drawing.Point(6, 177);
            this.latencyVariationControl.MaximumValue = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.latencyVariationControl.MinimumValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.latencyVariationControl.Name = "latencyVariationControl";
            this.latencyVariationControl.ParameterName = "Latency Variation (%)";
            this.latencyVariationControl.Size = new System.Drawing.Size(625, 46);
            this.latencyVariationControl.TabIndex = 1;
            // 
            // reliabilityParameter
            // 
            this.reliabilityParameter.FixedValue = new decimal(new int[] {
            95,
            0,
            0,
            0});
            this.reliabilityParameter.Location = new System.Drawing.Point(6, 125);
            this.reliabilityParameter.MaximumValue = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.reliabilityParameter.MinimumValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.reliabilityParameter.Name = "reliabilityParameter";
            this.reliabilityParameter.ParameterName = "Reliability (%)";
            this.reliabilityParameter.Size = new System.Drawing.Size(625, 46);
            this.reliabilityParameter.TabIndex = 0;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.Filter = "(*.txt)|*.txt|All files (*.*)|*.*\"";
            // 
            // gracefuleExitParameter
            // 
            this.gracefuleExitParameter.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.gracefuleExitParameter.FixedValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.gracefuleExitParameter.Location = new System.Drawing.Point(6, 665);
            this.gracefuleExitParameter.MaximumValue = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.gracefuleExitParameter.MinimumValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.gracefuleExitParameter.Name = "gracefuleExitParameter";
            this.gracefuleExitParameter.ParameterName = "Chance of Graceful exit (%)";
            this.gracefuleExitParameter.Size = new System.Drawing.Size(625, 48);
            this.gracefuleExitParameter.TabIndex = 12;
            // 
            // NetworkCharacteristicsControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox1);
            this.Name = "NetworkCharacteristicsControl";
            this.Size = new System.Drawing.Size(637, 722);
            this.groupBox1.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private ParamterControl downloadParamter;
        private ParamterControl uploadParameter;
        private ParamterControl latencyVariationControl;
        private ParamterControl reliabilityParameter;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.CheckBox checkBox2;
        private ParamterControl queueSizeParameter;
        private ParamterControl avgUptimeParamter;
        private ParamterControl stdDevDowntimeParamter;
        private ParamterControl avgDowntimeParamter;
        private ParamterControl stdDevUptimeParamter;
        private ParamterControl numberOfPeersParamter;
        private ParamterControl gracefuleExitParameter;
    }
}
