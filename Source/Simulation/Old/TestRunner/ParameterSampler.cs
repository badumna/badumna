using System;
using System.Collections.Generic;
using System.Text;

using NetworkSimulator;

namespace TestRunner
{
    public class ParameterSampler : ResultSampler
    {
        private double mXAxisValue;

        public void SampleAtXValue(double xAxis)
        {
            this.mXAxisValue = xAxis;
            this.Sample();
        }

        protected override double Xaxis()
        {
            return this.mXAxisValue;
        }
    }
}
