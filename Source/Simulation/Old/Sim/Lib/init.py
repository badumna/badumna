﻿
# Contains statements that will be executes once per ScriptScope (including for the console's ScriptScope).  Any names
# defined here (including imports) will be accessible by scripts executed by the ScriptRunner or by using #exec in the console.
# They will not be available to code in scripts loaded as modules via the import keyword (because they get their own namespace).

import sys
import clr

clr.AddReference("mscorlib")
clr.AddReference("System")
clr.AddReference("WindowsBase")
clr.AddReference("PresentationCore")
clr.AddReference("Badumna")
clr.AddReference("Sim")
clr.AddReference("NetworkSimulator")
clr.AddReference("Agency")
clr.AddReference("DynamicDataDisplay")
clr.AddReference("Visualizer")

import System
from NetworkSimulator import SimulatedStackType, NetworkInitializer
from NetworkSimulator.Peer import Peer, BadumnaPeer, AgentPeer
from NetworkSimulator.Instrumentation import DiagnosticsHelper
from ScriptSim import Sim, MainWindow
import Agency
from System.Windows import Point
