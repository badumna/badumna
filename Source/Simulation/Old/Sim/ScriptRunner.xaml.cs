﻿using System;
using System.IO;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Controls;
using System.Collections.ObjectModel;
using System.Windows;
using NetworkSimulator;

namespace ScriptSim
{
    /// <summary>
    /// Interaction logic for ScriptRunner.xaml
    /// </summary>
    partial class ScriptRunner : UserControl
    {
        private class ScriptInfo : IEquatable<ScriptInfo>
        {
            public string Path { get; private set; }
            public string Name { get; private set; }

            public ScriptInfo(string path)
            {
                this.Path = path;
                this.Name = System.IO.Path.GetFileNameWithoutExtension(path);
            }

            public override string ToString()
            {
                return this.Name;
            }

            public override int GetHashCode()
            {
                return this.Path.GetHashCode();
            }

            public override bool Equals(object obj)
            {
                return this.Equals(obj as ScriptInfo);
            }

            public bool Equals(ScriptInfo other)
            {
                if (other == null)
                {
                    return false;
                }

                return this.Path.Equals(other.Path);
            }
        }


        private const string PythonGlob = "*.py";

        private readonly ObservableCollection<ScriptInfo> mScripts = new ObservableCollection<ScriptInfo>();
        private string mScriptDirectory;


        public ScriptRunner()
        {
            InitializeComponent();

            this.ScriptList.ItemsSource = this.mScripts;

            //Binding binding = new Binding("CurrentItem");
            //binding.Source = Python.Current;
            //this.Job.SetBinding(TextBlock.TextProperty, binding);
        }

        public void SetScriptDirectory(string directory)
        {
            this.mScriptDirectory = directory;
            FileSystemWatcher watcher = new FileSystemWatcher(directory, ScriptRunner.PythonGlob);
            watcher.NotifyFilter = NotifyFilters.LastAccess | NotifyFilters.LastWrite | NotifyFilters.FileName | NotifyFilters.DirectoryName;
            watcher.Created += this.ScriptsChanged;
            watcher.Deleted += this.ScriptsChanged;
            watcher.Changed += this.ScriptsChanged;
            watcher.Renamed += this.ScriptRenamed;
            watcher.Error += delegate { this.RefreshScripts(); };
            watcher.EnableRaisingEvents = true;
            this.RefreshScripts();
        }

        private void RefreshScripts()
        {
            if (!this.CheckAccess())
            {
                this.Dispatcher.BeginInvoke(new Action(this.RefreshScripts));
                return;
            }

            this.mScripts.Clear();
            string[] scripts = Directory.GetFiles(this.mScriptDirectory, ScriptRunner.PythonGlob, SearchOption.TopDirectoryOnly);
            foreach (string script in scripts)
            {
                this.mScripts.Add(new ScriptInfo(script));
            }
        }

        private void AddScript(string scriptPath)
        {
            if (!this.CheckAccess())
            {
                this.Dispatcher.BeginInvoke(new Action<string>(this.AddScript), scriptPath);
                return;
            }

            ScriptInfo scriptInfo = new ScriptInfo(scriptPath);
            if (!this.mScripts.Contains(scriptInfo))
            {
                this.mScripts.Add(scriptInfo);
            }
        }

        private void RemoveScript(string scriptPath)
        {
            if (!this.CheckAccess())
            {
                this.Dispatcher.BeginInvoke(new Action<string>(this.RemoveScript), scriptPath);
                return;
            }

            this.mScripts.Remove(new ScriptInfo(scriptPath));
        }

        private void ScriptRenamed(object sender, RenamedEventArgs e)
        {
            this.RemoveScript(e.OldFullPath);
            this.AddScript(e.FullPath);
        }

        private void ScriptsChanged(object sender, FileSystemEventArgs e)
        {
            switch (e.ChangeType)
            {
                case WatcherChangeTypes.Created:
                    this.AddScript(e.FullPath);
                    break;

                case WatcherChangeTypes.Deleted:
                    this.RemoveScript(e.FullPath);
                    break;
            }
        }

        private void Run_Click(object sender, RoutedEventArgs e)
        {
            this.RunSelected();
        }

        private void ScriptList_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            this.RunSelected();
        }

        private void RunSelected()
        {
            ScriptInfo scriptInfo = this.ScriptList.SelectedItem as ScriptInfo;
            if (scriptInfo == null)
            {
                return;
            }

            Action<Python.ExecutionItem, Exception> failed = null;
            failed = delegate(Python.ExecutionItem item, Exception e)
            {
                if (!this.CheckAccess())
                {
                    this.Dispatcher.BeginInvoke(failed, item, e);
                    return;
                }

                MessageBox.Show("Script failed: " + e.GetType().ToString() + ": " + e.Message);
            };

            Action<Python.ExecutionItem> completed = null;
            completed = delegate(Python.ExecutionItem item)
            {
                if (!this.CheckAccess())
                {
                    this.Dispatcher.BeginInvoke(completed, item);
                    return;
                }
            };

            Python.Current.Settings = new RunSettings(this.OutputDirectory.Text, this.TestName.Text,
                this.SaveStatistics.IsChecked == true);

            Python.Current.Execute(scriptInfo.Path, completed, failed);
        }

        private void Stop_Click(object sender, RoutedEventArgs e)
        {
            Python.Current.Interrupt();
        }

        private void SelectOutputDirectory(object sender, RoutedEventArgs e)
        {
            System.Windows.Forms.FolderBrowserDialog folderBrowser = new System.Windows.Forms.FolderBrowserDialog();
            if (folderBrowser.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                this.OutputDirectory.Text = folderBrowser.SelectedPath;
            }
        }
    }
}
