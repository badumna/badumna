﻿
//#define PROFILE

using System;
using System.IO;
using System.ComponentModel;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Threading;
using System.Windows.Threading;

using Microsoft.Scripting;
using Microsoft.Scripting.Hosting;

using Badumna.Utilities;
using NetworkSimulator;
using System.Windows.Resources;
using Microsoft.Scripting.Runtime;


namespace ScriptSim
{
    public class Python : INotifyPropertyChanged
    {
        public static string ScriptDirectory;
        public static string LibDirectory;

        public class ExecutionItem
        {
            public string Name;
            public ScriptSource Script;
            public ScriptScope Scope;
            public Action<ExecutionItem> CompletedHandler;
            public Action<ExecutionItem, Exception> FailedHandler;
        }

        static private Python mCurrent;
        static public Python Current { get { return Python.mCurrent; } }
        static public void Create(Dispatcher dispatcher)
        {
            Python.mCurrent = new Python(dispatcher);
        }


        public event PropertyChangedEventHandler PropertyChanged;

        public ScriptEngine Engine { get; private set; }

        private Dispatcher mDispatcher;
        private Thread mScriptThread;
        private Queue<ExecutionItem> mRunQueue;
        private readonly object mRunQueueLock;
        private AutoResetEvent mNewItems;
        private volatile bool mShutdown;

        private volatile bool mIsExecuting;
        public bool IsExecuting { get { return this.mIsExecuting; } }
        private volatile string mCurrentItem = "";
        public string CurrentItem
        {
            get
            {
                return this.mCurrentItem;
            }

            private set
            {
                this.mCurrentItem = value;
                this.OnPropertyChanged("CurrentItem");
            }
        }

        private volatile RunSettings mSettings;
        public RunSettings Settings
        {
            get { return this.mSettings; }
            set { this.mSettings = value; }
        }

        private ScriptSource mInitScript;

        private Python(Dispatcher dispatcher)
        {
            var options = new Dictionary<string, object>();
            
#if PROFILE
            options["EnableProfiler"] = ScriptingRuntimeHelpers.True;
#endif 

            this.Engine = IronPython.Hosting.Python.CreateEngine(options);
            this.LoadInitScript();

            this.mDispatcher = dispatcher;
            this.mRunQueue = new Queue<ExecutionItem>();
            this.mRunQueueLock = new object();
            this.mNewItems = new AutoResetEvent(false);
            this.mScriptThread = new Thread(this.Run);
            this.mScriptThread.IsBackground = true;
            this.mScriptThread.Start();
        }

        private void OnPropertyChanged(string propertyName)
        {
            if (this.mDispatcher != null && !this.mDispatcher.CheckAccess())
            {
                this.mDispatcher.BeginInvoke(new Action<string>(this.OnPropertyChanged), propertyName);
                return;
            }

            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }


        private void LoadInitScript()
        {
            string initScript;
            using (TextReader reader = File.OpenText("Lib/init.py"))
            {
                initScript = reader.ReadToEnd();
            }
            this.mInitScript = this.Engine.CreateScriptSourceFromString(initScript, SourceCodeKind.File);
        }

        public ScriptScope CreateScope()
        {
            ScriptScope newScope = this.Engine.CreateScope();

            this.mInitScript.Execute(newScope);

            this.Engine.Execute("sys.path.append(r'" + Python.ScriptDirectory + "')", newScope);
            this.Engine.Execute("sys.path.append(r'" + Python.LibDirectory + "')", newScope);

            newScope.SetVariable("__name__", "__main__");

            RunSettings settings = this.Settings != null ? new RunSettings(this.Settings) : null;
            newScope.SetVariable("settings", settings);

            return newScope;
        }

        public void Execute(string fileName, Action<ExecutionItem> completed, Action<ExecutionItem, Exception> failed)
        {
            ScriptScope scope = this.CreateScope();
            this.Execute(fileName, scope, completed, failed);
        }

        public void Execute(string fileName, ScriptScope scope, Action<ExecutionItem> completed, Action<ExecutionItem, Exception> failed)
        {
            string fullpath = Path.Combine(Directory.GetCurrentDirectory(), fileName);
            ScriptSource source = 
                this.Engine.CreateScriptSourceFromFile(fullpath, Encoding.UTF8, SourceCodeKind.Statements);
            this.Execute(Path.GetFileNameWithoutExtension(fileName), source, scope, completed, failed);
        }

        public void Execute(ScriptSource source, ScriptScope scope, Action<ExecutionItem> completed, Action<ExecutionItem, Exception> failed)
        {
            this.Execute(null, source, scope, completed, failed);
        }

        public void Execute(string name, ScriptSource source, ScriptScope scope, Action<ExecutionItem> completed, Action<ExecutionItem, Exception> failed)
        {
            if (name == null || name.Length == 0)
            {
                name = "Interactive";
            }

            lock (this.mRunQueueLock)
            {
                ExecutionItem item = new ExecutionItem
                {
                    Name = name,
                    Script = source,
                    Scope = scope,
                    CompletedHandler = completed,
                    FailedHandler = failed
                };
                this.mRunQueue.Enqueue(item);
                this.mNewItems.Set();
            }
        }

        public void Shutdown()
        {
            this.mShutdown = true;
        }

        public bool QueueIsEmpty()
        {
            return this.mRunQueue.Count == 0;
        }

        public void Run()
        {
            while (true)
            {
                try
                {
                    if (MainWindow.Current != null)
                    {
                        //this.Spinner.Stop();
                        MainWindow.Current.SetStatus("Idle");
                    }

                    this.mIsExecuting = false;
                    this.CurrentItem = "";
                    this.mNewItems.WaitOne();

                    if (this.mShutdown)
                    {
                        break;
                    }

                    while (true)
                    {
                        ExecutionItem item;

                        lock (this.mRunQueueLock)
                        {
                            if (this.mRunQueue.Count == 0)
                            {
                                break;
                            }

                            this.mIsExecuting = true;
                            item = this.mRunQueue.Dequeue();
                        }  // Drop the lock before we start a possibly long script execution

                        this.CurrentItem = item.Name;
                        if (MainWindow.Current != null)
                        {
                            //this.Spinner.Start();
                            MainWindow.Current.SetStatus("Executing '" + item.Name + "'");
                        }

                        try
                        {
                            item.Script.Execute(item.Scope);

#if PROFILE
                            IronPython.Runtime.PythonTuple profilerData = this.Engine.Execute("clr.GetProfilerData()", item.Scope);
                            Func<dynamic, string> formatItem = x => string.Format("{0}\t{1}\t{2}\t{3}", x.Name, x.Calls, x.InclusiveTime, x.ExclusiveTime);
                            File.WriteAllLines("sim-profile.csv", profilerData.Select(formatItem));
#endif

                            if (item.CompletedHandler != null)
                            {
                                try
                                {
                                    item.CompletedHandler(item);
                                }
                                catch { }
                            }
                        }
                        catch (Exception e)
                        {
                            try
                            {
                                if (item.FailedHandler != null)
                                {
                                    item.FailedHandler(item, e);
                                }
                            }
                            catch { }
                        }
                    }
                }
                catch (ThreadAbortException)
                {
                    Console.WriteLine("thread abort exception caught. ");
                    Thread.ResetAbort();
                    lock (this.mRunQueueLock)
                    {
                        this.mRunQueue = new Queue<ExecutionItem>();
                        this.mNewItems.Reset();
                    }
                }
            }
        }

        public void Interrupt()
        {
            this.mScriptThread.Abort(new KeyboardInterruptException());
        }
    }
}
