﻿
from ScriptSim import Sim, EyeItem, SummaryFunction
from ScriptSim.EyeConfig import EyeGraphWrapper
from random_walk import WalkerFactory
from agent_tests import AgentPeerInitializer
from pysim import push_repeat

 
inbound = 640
outbound = 640
run_time_seconds = 15 * 60


class OverloadPeerInitializer:
    def __init__(self, helper, stack_type):
        self._helper = helper
        self._stack_type = stack_type
    
    def initialize(self, sender, init_args):
        facade = init_args.SimulatorFacade
        facade.CreateStack(self._stack_type)
        
        peer = BadumnaPeer(facade)
        
        if self._helper is not None:
            self._helper.Add(peer)

        peer.Start()
        
        return peer



def nums():
    k = 0
    while True:
        k += 1
        yield k


def make_initializer():
    simWrapper = SimulatorWrapper(Sim.Current.Simulator)
    timeWrapper = TimeKeeperWrapper(Sim.Current.Simulator)
    ni = NetworkInitializer(simWrapper, timeWrapper)
    ni.SetDefaults()
    ni.NodeInboundBandwidthCapacity = inbound
    ni.NodeOutboundBandwidthCapacity = outbound
    ni.NodeQueueBufferSize = min(inbound, outbound) * 0.25 * 1024 / 8
    ni.Topology.HasFullConectivity = True
    ni.Topology.MessageTracer = Sim.Current.MessageTracer
    ni.Topology.Load("mking-t.txt")
    ni.Reset()
    return ni


def overload_test_1(overload_initializer, peer_initializer):
    ni = make_initializer()
        
    next_node_number = nums().next
    
    # Overload peer (defined in Sim's NetworkConfig.xml)
    ni.InitializeNode(overload_initializer.initialize, 0, next_node_number())
    
    # First peer, lower bandwidth
    ni.NodeInboundBandwidthCapacity = inbound / 10
    ni.NodeOutboundBandwidthCapacity = outbound / 10
    ni.InitializeNode(peer_initializer.initialize, next_node_number())
    ni.NodeInboundBandwidthCapacity = inbound
    ni.NodeOutboundBandwidthCapacity = outbound
        
    def f():
        ni.InitializeNode(peer_initializer.initialize, next_node_number())
    
    push_repeat(f, 15000)



def overload_test_2(overload_initializer, peer_initializer):
    ni = make_initializer()
    
    next_node_number = nums().next
    
    # Overload peer (defined in Sim's NetworkConfig.xml)
    ni.InitializeNode(overload_initializer.initialize, 0, next_node_number())  # fixed domain number so we always get the same IP address

    # Peer 1
    ni.InitializeNode(peer_initializer.initialize, 1, next_node_number())
    
    # Peer 2
    ni.InitializeNode(peer_initializer.initialize, next_node_number())
    
    # Peer 3
    ni.InitializeNode(peer_initializer.initialize, next_node_number())
    
    peer1 = peer_initializer._helper.Peers[1]
    push_repeat(peer1.AddAgent, 10000)


def make_eye_graph(label):
    graph = EyeGraphWrapper()
    graph.Label = label
    return graph

def make_eye_item(label, path, peers, function, graph):
    item = EyeItem()
    item.Label = label
    item.Path = path
    item.Peers = peers
    item.Function = function
    item.Graph = graph
    return item


def setup_graphs():
    eyeconf = MainWindow.Current.EyeConfig
    
#    eyeconf.Graphs.Clear()
#    eyeconf.EyeItems.Clear()
    
    bw_graph = make_eye_graph("Bandwidth")
    eyeconf.Graphs.Add(bw_graph)
    
    count_graph = make_eye_graph("Peer 1")
    eyeconf.Graphs.Add(count_graph)
    
    eyeconf.EyeItems.Add(make_eye_item("0", "mStack.mTransportLayer.OutboundBytesPerSecond", "0", SummaryFunction.Average, bw_graph))
    eyeconf.EyeItems.Add(make_eye_item("1", "mStack.mTransportLayer.OutboundBytesPerSecond", "1", SummaryFunction.Average, bw_graph))

    eyeconf.EyeItems.Add(make_eye_item("Direct", "mStack.mEntityManager.mOriginalWrappers[0].Value.mInterestedPeerList.mDirectPeers.Count", "1", SummaryFunction.Average, count_graph))
    eyeconf.EyeItems.Add(make_eye_item("Overload", "mStack.mEntityManager.mOriginalWrappers[0].Value.mInterestedPeerList.mOverloadPeers.Count", "1", SummaryFunction.Average, count_graph))
    eyeconf.EyeItems.Add(make_eye_item("Total", "mStack.mEntityManager.mOriginalWrappers[0].Value.mInterestedPeerList.mInterestedPeers.Count", "1", SummaryFunction.Average, count_graph))


if __name__ == '__main__':
    sim = Sim.Current
    sim.Reset()
    
    walk_size = 100
    stack_type = SimulatedStackType.DhtIm
    entity_radius = 50
    interest_radius = 100
    payload_size = 100
    
    overload_init = OverloadPeerInitializer(sim.Helper, stack_type)
    peer_init = AgentPeerInitializer(WalkerFactory(walk_size, walk_size).create, stack_type, 0, entity_radius, interest_radius, walk_size, sim.Helper, payload_size)
    
    overload_test_1(overload_init, peer_init)
    MainWindow.Current.Engage()
    
    MainWindow.Current.DispatchSync(setup_graphs)
    
    sim.StartEyeGraphs()
    sim.RunFor(run_time_seconds * 1000)
 