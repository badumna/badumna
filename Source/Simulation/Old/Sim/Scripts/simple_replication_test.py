﻿
from ScriptSim import Sim, AgentPeer
from agent_tests import initialize_run
from Agency import Script, Seeker, Idler
from Badumna.DataTypes import Vector3
from System import TimeSpan


stack_type = SimulatedStackType.DhtIm
num_peers = 2
peer_start_delay = 0
payload_size = 0
radius = 10
interest_radius = 100

x_start_pos = 500
x_seek_pos = 10
travel_time_seconds = 10
wait_time_seconds = 30
run_time_seconds = (travel_time_seconds + wait_time_seconds) * 3


class SimpleReplicationTestBehaviourFactory:
    def __init__(self):
        dist = x_start_pos - x_seek_pos
        self._speed = dist / travel_time_seconds
        
    def create(self, start_position):
        if start_position.X > 0:
            sign = 1
        else:
            sign = -1

        script = Script();
        script.AddCue(TimeSpan.Zero, Seeker(Vector3(x_seek_pos * sign, 0, 0), self._speed))
        time = travel_time_seconds
        script.AddCue(TimeSpan.FromSeconds(time), Idler());
        time += wait_time_seconds
        script.AddCue(TimeSpan.FromSeconds(time), Seeker(Vector3(x_start_pos * sign, 0, 0), self._speed))
        time += travel_time_seconds
        script.AddCue(TimeSpan.FromSeconds(time), Idler());
        time += wait_time_seconds
        script.AddCue(TimeSpan.FromSeconds(time), None)
        return script
    


class SimpleReplicationTestPeerInitializer:
    def __init__(self, helper):
        self._helper = helper
        self._count = 0
        self._behaviour_factory = SimpleReplicationTestBehaviourFactory().create

    
    def initialize(self, sender, init_args):
        facade = init_args.SimulatorFacade
        facade.CreateStack(stack_type)
        
        if self._count == 0:
            x = x_start_pos
        else:
            x = -x_start_pos

        start_position = Vector3(x, 0, 0)
        peer = AgentPeer(facade, radius, interest_radius, start_position, self._behaviour_factory(start_position), payload_size)
        
        if self._helper is not None:
            self._helper.Add(peer)

        self._count += 1
        peer.Start()




def meet_and_part_test(helper):
    peer_init = SimpleReplicationTestPeerInitializer(helper)
    initialize_run(peer_init, num_peers, peer_start_delay, plot_sequence_diagram=True)



if __name__ == '__main__':
    sim = Sim.Current
    sim.Reset()
    meet_and_part_test(sim.Helper)
    MainWindow.Current.Engage()
    sim.RunFor(run_time_seconds * 1000)
