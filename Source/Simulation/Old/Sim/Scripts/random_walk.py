﻿
from ScriptSim import Sim
from agent_tests import AgentPeerInitializer, initialize_run
from Agency import RandomWalker


class WalkerFactory:
    def __init__(self, size, speed=1.0):
        self._count = 0
        self._size = size
        self._speed = speed
        
    def create(self, start_position):
        self._count += 1
        return RandomWalker(self._speed, self._count, -self._size, self._size, -self._size, self._size, -self._size, self._size)
        
        
def random_walk_test(num_peers, stack_type, walk_size, peer_start_delay=5000, entity_radius=1.0, interest_radius=10.0, helper=None, payload_size=0):
    peer_init = AgentPeerInitializer(WalkerFactory(walk_size).create, stack_type, 0, entity_radius,
                                     interest_radius, walk_size, helper, payload_size)
    initialize_run(peer_init, num_peers, peer_start_delay, plot_sequence_diagram=True)



if __name__ == '__main__':
    sim = Sim.Current
    sim.Reset()
    random_walk_test(2, SimulatedStackType.DhtIm, 10, 100.0, 1.0, 10.0, sim.Helper)
    MainWindow.Current.Engage()
    sim.StartEyeGraphs()
    sim.RunFor(600000)
