﻿from ScriptSim import Sim, MainWindow
from pysim import push_repeat, add_graph, graph_parse_bytes, make_standard_report
from random_walk import random_walk_test


def run(num_peers, stack_type, ratio, duration, report, run_title, graphs):
    # ratio defines the interest radius compared to the random walk size.  ratio > 1
    # means all entities will be able to see all others at all times.  ratio = 1
    # specifies that the minimum interest radius should be used so that they can
    # all still see each other (ignores size of entities, but is approx correct).  ratio < 1
    # means that entities may not be able to see each other.
    interest_radius = 300.0
    walk_size = interest_radius / (2.0 * 1.4142 * ratio)
    entity_radius = 1.0
        
    sim = Sim.Current
    sim.Reset()
    
    random_walk_test(num_peers, stack_type, walk_size, 40000, entity_radius, interest_radius, sim.Helper)
    #MainWindow.Current.Engage(str(num_peers) + ", " + str(stack_type) + ", " + str(ratio))
    report.Start(run_title)
    sources = {}
    def tmp(*args):
        graph_parse_bytes(run_title, report, graphs, sources)
    sim.Helper.NewStatistics += tmp
    sim.RepeatGetStatistics(30000)
    sim.RunFor(duration)    


if __name__ == '__main__':
    run_length_ms = 5500000

    for ratio in [1.0, 0.5, 0.1]:
        for num_peers in [100]:
            title = str(num_peers) + " peers , " + str(ratio) + " ratio"
            report = make_standard_report(title)
            graphs = {}
            for stack_type in [SimulatedStackType.GossipIm, SimulatedStackType.DhtIm]:
                run(num_peers, stack_type, ratio, run_length_ms, report, str(stack_type), graphs)

