
from random_walk import random_walk_test
from pysim import push_repeat
from ScriptSim import Graph
from System.Windows import Point


sim = Sim.Current

main = MainWindow.Current
dispatcher = main.Dispatcher

graph = main.Construct[Graph]()
missing_objects_series = graph.AddSeries('Missing Objects')
bandwidth_avg_series = graph.AddSeries('Bandwidth Average')
bandwidth_max_series = graph.AddSeries('Bandwidth Maximum')
dht_ims_avg_series = graph.AddSeries('DHT IMS Avg KB')
gossip_ims_avg_series = graph.AddSeries('Gossip IMS Avg KB')
hallucinated_objects_series = graph.AddSeries('Hallucinated Objects')

main.AddTab('Gossip IM', graph)

helper = DiagnosticsHelper(sim.Now)

def get_stats():
    time = sim.TimeSeconds
    helper.GetStatistics(sim.Now)
    stats = helper.StatsAggregator.Table
    
    missing_objects = stats.Compute('Avg(Value)', "Time = '" + str(time) + "' AND Name = 'MissingObjects'")
    hallucinated_objects = stats.Compute('Max(Value)', "Time = '" + str(time) + "' AND Name = 'HallucinatedObjects'")
    bandwidth_avg = stats.Compute('Avg(Value)', "Time = '" + str(time) + "' AND Name = 'AverageInboundBandwidth'")
    bandwidth_max = stats.Compute('Max(Value)', "Time = '" + str(time) + "' AND Name = 'AverageInboundBandwidth'")        
        
    #dht_ims_avg = stats.Compute('Avg(Value)', "Time = '" + str(time) + "' AND Group = 'LayerBytes' AND (Name = 'DhtProtocol' OR Name = 'TieredRouter' OR Name Like 'ReplicaManager*')")
    #gossip_ims_avg = stats.Compute('Avg(Value)', "Time = '" + str(time) + "' AND Group = 'LayerBytes' AND Name = 'GossipImService'")
    
    if System.Convert.IsDBNull(missing_objects):
        missing_objects = 0
    #if System.Convert.IsDBNull(hallucinated_objects):
    #    hallucinated_objects = 0
    if System.Convert.IsDBNull(bandwidth_avg):
        bandwidth_avg = 0
    if System.Convert.IsDBNull(bandwidth_max):
        bandwidth_max = 0
    #if System.Convert.IsDBNull(dht_ims_avg):
    #    dht_ims_avg = 0
    #if System.Convert.IsDBNull(gossip_ims_avg):
    #    gossip_ims_avg = 0
               
    #dht_ims_avg /= 1000
    #gossip_ims_avg /= 1000
               
    missing_objects_series.AppendAsync(dispatcher, Point(time, missing_objects))
    hallucinated_objects_series.AppendAsync(dispatcher, Point(time, hallucinated_objects))
    bandwidth_avg_series.AppendAsync(dispatcher, Point(time, bandwidth_avg)) 
    bandwidth_max_series.AppendAsync(dispatcher, Point(time, bandwidth_max)) 
    #dht_ims_avg_series.AppendAsync(dispatcher, Point(time, dht_ims_avg)) 
    #gossip_ims_avg_series.AppendAsync(dispatcher, Point(time, gossip_ims_avg)) 
    

random_walk_test(30, SimulatedStackType.GossipIm, 100.0, helper=helper)
push_repeat(get_stats, 30000)
while True:
    sim.RunFor(60000)