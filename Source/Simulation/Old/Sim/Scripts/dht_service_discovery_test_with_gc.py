﻿import System
from ScriptSim import Sim, MainWindow, RunSettings
#from Visualizer import SourceConfig
from NetworkSimulator import ServiceDiscoveryPeer 
from dht_init import Initializer
from pysim import push, push_repeat
from Badumna.ServiceDiscovery import ServiceType

class TestRunner:
    def __init__(self, dht_initializer):
        self._test_message_number = 0
        self._dht_initializer = dht_initializer

    def begin_test(self, start_delay, interval):
	# one random peer will announce the service
        push(start_delay, self._announce_service, ServiceType.Overload)
    
    def _announce_service(self, service_type):
	peer = self._dht_initializer.get_random_peer()
	if peer is not None:
	    peer.AnnounceService(service_type)
	    push(300000, peer.ShutdownService)

    def _print_report(self):
	peer = self._dht_initializer.get_random_peer()
	if peer is not None:
	    peer.GenerateReport()


if __name__ == '__main__':
    #run_length_ms = 50000000
    run_length_ms = 1200000
    peer_start_delay = 2500
    num_peers = 400
    use_sequence_diagram=False

    run_settings = RunSettings("ServiceDiscoveryTests", str(num_peers) + " peers", True) # output directory, testname, save statistics
    intitializer = Initializer(ServiceDiscoveryPeer, SimulatedStackType.ServiceDiscovery, use_sequence_diagram)  
    
    intitializer.set_node_lifetime(360, 360, 180, 180)
    
    test_runner = TestRunner(intitializer)
    sim = intitializer.initialize_run(run_settings, num_peers, peer_start_delay, True, None, str(num_peers) + " peers")
    test_runner.begin_test(peer_start_delay + 1, 5000) # Must be called after first peer has entered (greater than peer_start_dealy)
    sim.RunFor(run_length_ms)
    test_runner._print_report()
