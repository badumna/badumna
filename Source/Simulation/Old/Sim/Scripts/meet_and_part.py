﻿
from ScriptSim import Sim
from agent_tests import AgentPeerInitializer, initialize_run
from Agency import Script, Seeker
from Badumna.DataTypes import Vector3
from System import TimeSpan

class MeetAndPartFactory:
    def __init__(self, size):
        self._travel_time = 30.0  # seconds
        dist = size * 3 / 4  # travel 3 / 4 of the half distance, so objects will be inside 1/4 of the total space
        self._speed = dist / self._travel_time
        
    def create(self, start_position):
        script = Script();
        script.AddCue(TimeSpan.Zero, Seeker(Vector3(0, start_position.Y, start_position.Z), self._speed));
        script.AddCue(TimeSpan.FromSeconds(self._travel_time), Seeker(start_position, self._speed));
        script.AddCue(TimeSpan.FromSeconds(2 * self._travel_time), None);
        return script
    

def meet_and_part_test(num_peers, stack_type, start_zone_size, start_pos_seed, peer_start_delay, entity_radius, interest_radius, helper):
    peer_init = AgentPeerInitializer(MeetAndPartFactory(start_zone_size).create, stack_type, start_pos_seed, 
                                     entity_radius, interest_radius, start_zone_size, helper)
    initialize_run(peer_init, num_peers, peer_start_delay)



if __name__ == '__main__':
    sim = Sim.Current
    sim.Reset()
    meet_and_part_test(20, SimulatedStackType.DhtIm, 400, 0, 100.0, 1.0, 10.0, sim.Helper)
    MainWindow.Current.Engage()
    while True:
        sim.RunFor(120000)