﻿
from pysim import push_repeat, line_graph, validate_stats
from random_walk import random_walk_test

peer_range = [2.0, 3.0, 4.0, 5.0, 8.0, 10.0, 12.0, 15.0, 17.0, 20.0, 25.0]
#peer_range = [2.0, 3.0, 4.0]
in_Bps = []
out_Bps = []

sim = Sim.Current

for num_peers in peer_range:
    sim.Reset()
    helper = DiagnosticsHelper(sim.Now) # TODO: Sim has to be reset before instantiating DiagnosticsHelper, otherwise time offsets are wrong.  Not sure what to do, but it should be nicer/shouldn't necessarly have to remember to call sim.Reset
    random_walk_test(num_peers, SimulatedStackType.DhtIm, 100.0, helper=helper)  
    push_repeat(lambda: helper.GetStatistics(sim.Now), 10000)
    sim.RunFor(120000)   # TODO: Should really run the test until it settles, rather than for some arbitrary time

    stats = helper.StatsAggregator.Table
    
    stat_check = validate_stats(stats)
    for name, value in stat_check:
        print name, '=', value
    
    avg_in_Bps = stats.Compute('Avg(Value)', "Time = '120' AND Name = 'AverageInboundBandwidth'")
    if System.Convert.IsDBNull(avg_in_Bps):
        avg_in_Bps = 0.0
    in_Bps.append(avg_in_Bps)
        
    avg_out_Bps = stats.Compute('Avg(Value)', "Time = '120' AND Name = 'AverageOutboundBandwidth'")
    if System.Convert.IsDBNull(avg_out_Bps):
        avg_out_Bps = 0.0
    out_Bps.append(avg_out_Bps)

MainWindow.Current.AddTab("In kbps", line_graph(peer_range, in_Bps))
MainWindow.Current.AddTab("Out kbps", line_graph(peer_range, out_Bps))
