﻿
from pysim import push_repeat, line_graph, validate_stats
from ScriptSim import Sim
from agent_tests import ChatPeerInitializer, initialize_run

chat_messages_received=[]


def start_chat_peer(peer, peer_name):
    if peer_name == "Node7":
        peer.InitiateChatWith("Node6")

def stop_chat_peer(peer, peer_name):
    if peer_name == "Node7" or peer_name == "Node6":
        peer.AssertReceived(1)
        

def chat_test(num_peers, stack_type, peer_start_delay=5000, helper=None):
    peer_init = ChatPeerInitializer(stack_type, helper, start_chat_peer, stop_chat_peer)
    initialize_run(peer_init, 7, peer_start_delay)


#    enum NatType
#    {
#        Unknown = 0,
#        Blocked = 1,
#        Open = 2,
#        SymmetricFirewall = 3,
#        FullCone = 4,
#        SymmetricNat = 5,
#        RestrictedPort = 6,
#        RestrictedCone = 7,
#        MangledFullCone = 8,
#        Internal = 9,
#        HashAddressable = 10,
#    }


peer_range = [7.0]
in_Bps = []
out_Bps = []


sim = Sim.Current


    
for num_peers in peer_range:
    sim.Reset()
    helper = DiagnosticsHelper(sim.Now) # TODO: Sim has to be reset before instantiating DiagnosticsHelper, otherwise time offsets are wrong.  Not sure what to do, but it should be nicer/shouldn't necessarly have to remember to call sim.Reset
    chat_test(num_peers, SimulatedStackType.Chat, 15000.0, helper=helper) 
    push_repeat(lambda: helper.GetStatistics(sim.Now), 1000)
    sim.RunFor(250000)   # TODO: Should really run the test until it settles, rather than for some arbitrary time

    stats = helper.StatsAggregator.Table
    
    stat_check = validate_stats(stats)
    for name, value in stat_check:
        print name, '=', value
    
    avg_in_Bps = stats.Compute('Avg(Value)', "Time = '120' AND Name = 'AverageInboundBandwidth'")
    if System.Convert.IsDBNull(avg_in_Bps):
        avg_in_Bps = 0.0
    in_Bps.append(avg_in_Bps)
        
    avg_out_Bps = stats.Compute('Avg(Value)', "Time = '120' AND Name = 'AverageOutboundBandwidth'")
    if System.Convert.IsDBNull(avg_out_Bps):
        avg_out_Bps = 0.0
    out_Bps.append(avg_out_Bps)

MainWindow.Current.AddTab("In kbps", line_graph(peer_range, in_Bps))
MainWindow.Current.AddTab("Out kbps", line_graph(peer_range, out_Bps))
