﻿
from agent_tests import run_with_report, interest_radius_to_zone_size
from random_walk import random_walk_test
from pysim import add_graph
from ScriptSim import MainWindow, Sim
from Visualizer import SourceConfig, GraphConfig, Graph

def graph_peers(run_title, report, graphs, sources):
    time = Sim.Current.TimeSeconds
    stats = Sim.Current.Helper.StatsAggregator.Table

    peer_table = Sim.Current.Filter(stats, "Time = '" + str(time) + "' AND Name = 'NetworkQueueLength'", 'Sum', 'Peer')
    peers = []
    for row in peer_table.Rows:
        peer_name = row[0]
        peers.append(peer_name)
    Sim.Current.ClearDataSet(stats)
        
    for peer in peers:
        if peer not in graphs:
            graph = MainWindow.Current.Construct[Graph]()
            graphs[peer] = graph
            def tmp():
                graph.Title = peer
                graph.YAxisLabel = 'kb/s'
                report.AddGraph(graph)
            MainWindow.Current.DispatchSync(tmp)
        
            source = SourceConfig(run_title, 'Avg(Value)', "Time = '$now' AND Name = 'EstimatedBandwidth' AND Peer = '" + peer + "'", 8.0 / 1024.0)
            source.Name = 'Estimated'
            sources.append(source)
            graphs[peer].AddSeries(source.Name, source.Source)
            source = SourceConfig(run_title, 'Avg(Value)', "Time = '$now' AND Name = 'AverageOutboundBandwidth' AND Peer = '" + peer + "'")
            source.Name = 'Instantaneous'
            sources.append(source)
            graphs[peer].AddSeries(source.Name, source.Source)
            
    for source in sources:
        source.Append(time, stats)


if __name__ == '__main__':
    walk_params = {}
    walk_params['num_peers'] = 20
    walk_params['peer_start_delay'] = 0
    walk_params['entity_radius'] = 1.0
    walk_params['interest_radius'] = 300.0
    walk_params['walk_size'] = interest_radius_to_zone_size(walk_params['interest_radius'], 1.0)
    walk_params['stack_type'] = SimulatedStackType.DhtIm

    run_length_ms = 300000
    sample_period_ms = 1000

    title = "Bandwidth Estimator Test"
    report = MainWindow.Current.AddReport(title)
    
    graphs = {}
    sources = []
    def test_init(helper):
        random_walk_test(helper=helper, **walk_params)
        def tmp(*args):
            graph_peers(title, report, graphs, sources)
        helper.NewStatistics += tmp
    run_with_report(test_init, run_length_ms, sample_period_ms, report, str(walk_params['stack_type']), graphs, settings, False, False)
