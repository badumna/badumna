﻿
import System
from ScriptSim import Sim, MainWindow, RunSettings
from Visualizer import SourceConfig
from NetworkSimulator import DhtPeer 
from dht_init import Initializer, ReportRunHelper, make_dht_report
from pysim import add_graph, push, push_repeat

class TestRunner:
    def __init__(self, dht_initializer, report_helper, request_time_log):
        self._test_message_number = 0
        self._dht_initializer = dht_initializer
        self._sources = [SourceConfig("RouteTime", "", ""), SourceConfig("NumberOfHops", "", ""), SourceConfig("AvgTimePerHop", "", "")]
        self._request_time_log = request_time_log
        self._data_key = "some_key"
        self._key_number = 0;
        report_helper.add_graph(self._sources[0], "RouteTime")

    def begin_test(self, start_delay, interval):
        push(start_delay, self._start_put_data)
        push(start_delay, self._start_get_data, interval)
        
    def _start_get_data(self, interval):
        push_repeat(self._get_test_message, interval)
        
    def _start_put_data(self):
        push_repeat(self._put_data, 85000) # 85 seconds (5 less than timeout)
    
    def _put_data(self):
        initial_peer = self._dht_initializer.get_first_peer()
        self._data_key = "key_" + str(self._key_number);
        self._key_number += 1
        initial_peer.PutData(self._data_key, "some_data", 90) # 1.5 min timeout

    def _get_test_message(self):
        peer = self._dht_initializer.get_random_peer()
        if peer is not None:
            peer.GetData(self._data_key, self._get_arrival)
            self._test_message_number = self._test_message_number + 1 # for randomization
        
    def _get_arrival(self, key, values, requestTime):
        self._sources[0].Append(MainWindow.Current.Dispatcher, Sim.Current.TimeSeconds, requestTime.TotalMilliseconds)
        if self._request_time_log:
            self._request_time_log.write(str(Sim.Current.TimeSeconds) + ',' + str(requestTime.TotalMilliseconds) + '\n')
            self._request_time_log.flush()


if __name__ == '__main__':
    run_length_ms = 500000000
    sample_period_ms = 45000
    peer_start_delay = 45000
    num_peers = 25;
    use_sequence_diagram=True

    report = make_dht_report("Dht traffic")
    report_helper = ReportRunHelper(report, sample_period_ms, False, False)
                        
    run_settings = RunSettings("DhtTests", str(num_peers) + " peers", True) # output directory, testname, save statistics
    intitializer = Initializer(DhtPeer, SimulatedStackType.Dht, use_sequence_diagram)  
    
    intitializer.set_node_lifetime(3600, 3600, 1800, 1800)
    
    
    req_time_log = open("DhtTests\\request_time.csv", "w")
    
    test_runner = TestRunner(intitializer, report_helper, req_time_log)
    sim = intitializer.initialize_run(run_settings, num_peers, peer_start_delay, True, report_helper, str(num_peers) + " peers")
    test_runner.begin_test(peer_start_delay + 1, 5000) # Must be called after first peer has entered (greater than peer_start_dealy)
    sim.RunFor(run_length_ms)