﻿
from pysim import make_standard_report
from agent_tests import run_with_report, interest_radius_to_zone_size
from random_walk import random_walk_test


if __name__ == '__main__':
    walk_params = {}
    walk_params['num_peers'] = 45
    walk_params['peer_start_delay'] = 120000
    walk_params['entity_radius'] = 1.0
    walk_params['interest_radius'] = 300.0
    walk_params['payload_size'] = 100;

    run_length_ms = (walk_params['num_peers'] + 5) * walk_params['peer_start_delay']
    sample_period_ms = 30000

    for ratio in [1.0]:
        walk_params['walk_size'] = interest_radius_to_zone_size(walk_params['interest_radius'], ratio)

        title = "IM scalability test (peers = " + str(walk_params['num_peers']) + ", ratio = " + str(ratio) + ")"
        report = make_standard_report(title)
        graphs = {}
        
        for walk_params['stack_type'] in [SimulatedStackType.GossipIm]:
            def test_init(helper):
                random_walk_test(helper=helper, **walk_params)
            run_with_report(test_init, run_length_ms, sample_period_ms, report, str(walk_params['stack_type']), graphs, settings)
