﻿
from ScriptSim import Sim
from NetworkSimulator import BadumnaPeer
from agent_tests import initialize_run
from System.IO import Stream
from Badumna.DataTypes import BadumnaId
from pysim import push


stack_type = SimulatedStackType.DhtIm
num_peers = 2
peer_start_delay = 0
run_time_seconds = 30 * 60
stream_tag = "highly-unique-stream-tag"
stream_length = 200 * 1024 * 1024
bandwidth_in = 200  # kbit/s (k = 1000)
bandwidth_out = 2000  # kbit/s (k = 1000)


class LongStream(Stream):
    def __init__(self, length):
        self._length = length
        self._position = 0

    def Read(self, buffer, offset, count):
        remaining = self._length - self._position
        
        if remaining < count:
            bytes_read = remaining
        else:
            bytes_read = count
            
        self._position += bytes_read
        return bytes_read
        
    Length = property(lambda self: self._length)
    CanRead = property(lambda self: True)


class StreamingTestPeerInitializer:
    def __init__(self, helper):
        self._helper = helper
        self._count = 0
    
    def initialize(self, sender, init_args):
        self._count += 1
        facade = init_args.SimulatorFacade
        facade.CreateStack(stack_type)
        
        peer = BadumnaPeer(facade)
        
        if self._helper is not None:
            self._helper.Add(peer)

        peer.Start()
        
        def handler(sender, args):
            args.AcceptStream(Stream.Null, None, None)
        
        facade.Streaming.SubscribeToReceiveStreamRequests(stream_tag, handler)
        
        if self._count == 1:
            self._id = BadumnaId.GetNextUniqueId()
        else:
            def start_transfer():
                facade.Streaming.BeginSendReliableStream(stream_tag, "very-original-stream-name", LongStream(stream_length), self._id, "username", None, None)
            push(5000, start_transfer)


def streaming_test(helper):
    peer_init = StreamingTestPeerInitializer(helper)
    initialize_run(peer_init, num_peers, peer_start_delay, plot_sequence_diagram=True, inbound=bandwidth_in, outbound=bandwidth_out)



if __name__ == '__main__':
    sim = Sim.Current
    sim.Reset()
    streaming_test(sim.Helper)
    MainWindow.Current.Engage()
    sim.StartEyeGraphs()
    sim.RunFor(run_time_seconds * 1000)
