﻿
from ScriptSim import Sim
from NetworkSimulator import Topology
from Badumna import Vector3
from Agency import RandomWalker


class WalkerFactory:
    def __init__(self, size):
        self._count = 0
        self._size = size
        
    def create(self, start_position):
        self._count += 1
        return RandomWalker(1.0, self._count, -self._size, -self._size, self._size, self._size)

class APInitializer:
    def __init__(self, behaviour_factory, stack_type, position_seed, radius, interest_radius, start_zone_size, helper, payload_size=0):
        self._behaviour_factory = behaviour_factory
        self._stack_type = stack_type
        self._radius = radius
        self._interest_radius = interest_radius
        self._start_zone_size = start_zone_size
        self._payload_size = payload_size
        self._helper = helper
        self._random = System.Random(position_seed)
    
    def initialize(self, sender, init_args):
        facade = init_args.SimulatorFacade
        facade.CreateStack(self._stack_type)
        x = self._start_zone_size * (self._random.NextDouble() - 0.5) * 2.0
        y = self._start_zone_size * (self._random.NextDouble() - 0.5) * 2.0
        z = self._start_zone_size * (self._random.NextDouble() - 0.5) * 2.0
        start_position = Vector3(x, y, z)
        peer = AgentPeer(facade, self._radius, self._interest_radius, start_position, self._behaviour_factory(start_position),
            self._payload_size)
        if self._helper is not None:
            self._helper.Add(peer)


hasher = System.Activator.CreateInstance(System.Type.GetType("Badumna.DistributedHashTable.HashKey, Badumna"))

def test(t,n):
    if not t:
        print "test failed", n


if __name__ == '__main__':
    sim = Sim.Current
    sim.Reset()
    helper = sim.Helper
    peer_init = APInitializer(WalkerFactory(20).create, SimulatedStackType.GossipIm, 0, 1, 100, 20, helper)
    
    simWrapper = SimulatorWrapper(Sim.Current.Simulator)
    timeWrapper = TimeKeeperWrapper(Sim.Current.Simulator)
    ni = NetworkInitializer(simWrapper, timeWrapper)
    ni.SetDefaults()
    ni.Topology = Topology()
    ni.Topology.AddDomain(Topology.DomainType.Domain)
    ni.Topology.HasFullConectivity = True
    ni.NumberOfNodes = 3
    ni.Reset()
    ni.InitializeNodes(peer_init.initialize)
    
    cellName = AgentPeer.SceneName + "<0,0,0>"
    cellHash = hasher.Hash(cellName)

    hashes = [hasher.Hash(helper.Peers[0].LocalAddress), hasher.Hash(helper.Peers[1].LocalAddress), hasher.Hash(helper.Peers[2].LocalAddress)]
    
    imsServerPriority = map(lambda hash, id: (id, hash.AbsoluteDistanceFrom(cellHash)), hashes, range(0, ni.NumberOfNodes))
    imsServerPriority.sort(key=lambda elem: elem[1], reverse=True)
    
    peers = map(lambda ind: helper.Peers[ind[0]], imsServerPriority)
    # 'peers' now has the peers ordered from lowest priority to highest priority.  If we start them
    # up in the order they appear in the list then the cell server will migrate each time.
    
    MainWindow.Current.Engage()
 
    peers[0].Start()
    sim.RunFor(30)    # when this is low both peers might treat the other as quarantined and process their ims insert locally (because the leafset sync that contains the flag indicating the first peer is unquarantined takes some time to travel across the network)
    peers[1].Start()

    sim.RunFor(120000)
    
    test(peers[0].RemoteAgents.Count == 1, 1)
    test(peers[1].RemoteAgents.Count == 1, 2)
    
    peers[2].Start()
 
    sim.RunFor(40000)

    test(peers[0].RemoteAgents.Count == 2, 3)
    test(peers[1].RemoteAgents.Count == 2, 4)
    test(peers[2].RemoteAgents.Count == 2, 5)
