﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Microsoft.Scripting;
using Microsoft.Scripting.Hosting;
using System.ComponentModel;


namespace ScriptSim
{
    /// <summary>
    /// Interaction logic for PythonConsole.xaml
    /// </summary>
    partial class PythonConsole : UserControl
    {
        private ScriptScope mScope;
        public ScriptScope Scope { get { return this.mScope; } }

        public PythonConsole()
        {
            InitializeComponent();

            this.Prompt();

            if (DesignerProperties.GetIsInDesignMode(this))
            {
                return;
            }

            TextBoxOutputStream output = new TextBoxOutputStream(this.Output, Encoding.UTF8);
            Python.Current.Engine.Runtime.IO.SetOutput(output, Encoding.UTF8);
            Python.Current.Engine.Runtime.IO.SetErrorOutput(output, Encoding.UTF8);
            this.mScope = Python.Current.CreateScope();
        }

        private void Write(string text)
        {
            if (!this.CheckAccess())
            {
                this.Dispatcher.BeginInvoke(new Action<string>(this.Write), text);
                return;
            }

            this.Output.AppendText(text);
            this.Output.ScrollToEnd();
        }

        private void WriteLine()
        {
            this.Write("\n");
        }

        private void WriteLine(string text)
        {
            this.Write(text);
            this.WriteLine();
        }

        private void Prompt()
        {
            if (!this.CheckAccess())
            {
                this.Dispatcher.BeginInvoke(new Action(this.Prompt));
                return;
            }

            this.Write(">>> ");

        }

        private void LogException(Exception e)
        {
            this.WriteLine(e.GetType() + ": " + e.Message);
            this.Prompt();
        }

        private void Text_KeyUp(object sender, KeyEventArgs args)
        {
            if (args.Key == Key.Return || args.Key == Key.Enter)
            {
                args.Handled = true;

                string expression = this.CommandLine.Text;
                if (expression.Trim().Length == 0)
                {
                    this.CommandLine.Clear();
                    return;
                }

                this.WriteLine(expression);

                if (expression.StartsWith("#"))
                {
                    if (expression.StartsWith("#reset"))
                    {
                        this.mScope = Python.Current.CreateScope();
                        this.Prompt();
                    }
                    else if (expression.StartsWith("#exec"))
                    {
                        string fileName = expression.Substring("#exec".Length).Trim();
                        Python.Current.Execute(fileName, this.mScope, delegate { this.Prompt(); },
                            delegate(Python.ExecutionItem item, Exception e) { this.LogException(e); });
                    }
                    else
                    {
                        this.WriteLine("I only understand #reset and #exec");
                        this.Prompt();
                    }
                }
                else
                {
                    ScriptSource source = Python.Current.Engine.CreateScriptSourceFromString(expression, SourceCodeKind.InteractiveCode);
                    Python.Current.Execute(source, this.mScope, delegate { this.Prompt(); },
                        delegate(Python.ExecutionItem item, Exception e) { this.LogException(e); });
                }
                this.CommandLine.Clear();
            }
        }

        private void Console_Loaded(object sender, RoutedEventArgs e)
        {
            this.CommandLine.Focus();
        }
    }
}
