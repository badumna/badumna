using System;
using System.Collections.Generic;
using System.Text;

using Badumna.Core;
using Badumna.Utilities;
using NetworkSimulator;
using Badumna.DistributedHashTable;

namespace ExperimentLibrary
{
    public class ReceivedRoutedMessageEventArgs : EventArgs
    {
        private PeerAddress mSourceAddress;
        public PeerAddress SourceAddress { get { return this.mSourceAddress; } }

        private PeerAddress mDestinationAddress;
        public PeerAddress DestinationAddress { get { return this.mDestinationAddress; } }

        private HashKey mKey;
        public HashKey Key { get { return this.mKey; } }

        public ReceivedRoutedMessageEventArgs(HashKey key, PeerAddress destinationAddress, PeerAddress sourceAddress)
        {
            this.mKey = key;
            this.mDestinationAddress = destinationAddress;
            this.mSourceAddress = sourceAddress;
        }
    }


    public class SimpleRouteLayer : DhtProtocol
    {
        static int mNextTimerKey;

        public event EventHandler<ReceivedRoutedMessageEventArgs> RoutedMessageArrivalEvent;
        public event EventHandler<ReceivedRoutedMessageEventArgs> RoutedMessagePathEvent;

        private DhtFacade mFacade;

        public SimpleRouteLayer(DhtFacade dht)
            : base(dht)
        {
            this.mFacade = dht;
            this.RegisterMethodsIn(this);
            this.ForgeMethodList();
        }

        public void RouteToKey(HashKey key)
        {
            DhtEnvelope handle = this.GetMessageFor(key, QualityOfService.Reliable);

            handle.Qos.HandleAlongRoutePath = true;
            Results.Instance.TimerStart(SimpleRouteLayer.mNextTimerKey);
            this.RemoteCall(handle, this, "ReceiveRoutedMessage", SimpleRouteLayer.mNextTimerKey++);
            this.SendMessage(handle);
        }

        [ProtocolMethod]
        private void ReceiveRoutedMessage(int timerKey)
        {
            if (!this.mFacade.MapsToLocalNode(this.CurrentEnvelope.DestinationKey, null))
            {
                if (null != this.RoutedMessagePathEvent)
                {
                    this.RoutedMessagePathEvent.Invoke(this,
                        new ReceivedRoutedMessageEventArgs(this.CurrentEnvelope.DestinationKey,
                        NetworkContext.CurrentContext.PublicAddress, this.CurrentEnvelope.Source));
                }
                return;
            }

            Results.Instance.TimerStop("Routed message delay", timerKey);

            if (null != this.RoutedMessageArrivalEvent)
            {
                this.RoutedMessageArrivalEvent.Invoke(this,
                    new ReceivedRoutedMessageEventArgs(this.CurrentEnvelope.DestinationKey,
                    NetworkContext.CurrentContext.PublicAddress, this.CurrentEnvelope.Source));
            }
        }

    }
}
