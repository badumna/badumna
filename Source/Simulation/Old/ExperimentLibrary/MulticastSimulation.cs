using System;
using System.Collections.Generic;
using System.Text;

using Badumna.DistributedHashTable;
using Badumna.Core;
using Badumna.Utilities;
using Badumna.Transport;
using Badumna.Multicast;
using Badumna.DataTypes;
using Badumna.Diagnostics;

using NetworkSimulator;


namespace ExperimentLibrary
{
    public class MulticastSimulation : DhtSimulation
    {
        private Random mGenerator = new Random((int)DateTime.Now.Ticks);
        private List<SimpleMulticast> mMulticastLayers = new List<SimpleMulticast>();
        private List<MulticastFacade> mMulticastFacades = new List<MulticastFacade>();
        private static UniqueNetworkId mGroupId = Badumna.Chat.ChatChannel.Scene;

        public MulticastSimulation()
        {
            this.DhtTier = 2;
        }

        protected override void NewServiceInstance(DhtFacade dhtFacade, PeerAddress address)
        {
        }

        protected override void ContinueInitialization(INode node, ConnectionTable connectionTable)
        {
            MembershipService memberService = new MembershipService(this.mDhtFacades[node.Address.Address]);
            MulticastFacade multicastFacade = new MulticastFacade(connectionTable, memberService);
            SimpleMulticast simpleLayer = new SimpleMulticast(multicastFacade, MulticastSimulation.mGroupId);

            this.mMulticastFacades.Add(multicastFacade);
            this.mMulticastLayers.Add(simpleLayer);
            lock (this.mCheckPointers)
            {
                this.mCheckPointers[node.Address.Address].Register((MulticastFacade)multicastFacade);
            }
        }

        public override void Run()
        {
            DateTime endTime = Simulator.Instance.Now + TimeSpan.FromSeconds(3600);

            Simulator.Instance.RunFor(TimeSpan.FromSeconds(60));
            Logger.Debug("----------|-----------");
            while (Simulator.Instance.Now < endTime && !Simulator.Instance.Suspend)
            {
                Simulator.Instance.RunFor(TimeSpan.FromSeconds(10));
                this.MulticastTest();
            }

            base.Cleanup();
            this.mMulticastLayers.Clear();
            this.mDhtFacades.Clear();
        }

        public void MulticastTest()
        {
            foreach (SimpleMulticast layer in this.mMulticastLayers)
            {
                layer.Reset();
            }

            int i = this.mGenerator.Next(0, this.mMulticastLayers.Count);

            INode sourceNode = this.mNodes[i];
            SimpleMulticast source = this.mMulticastLayers[i];

            Logger.Debug("---------------------- {0} ---------------------------", sourceNode.Address);

            sourceNode.LockOnline();
            Simulator.Instance.RunFor(TimeSpan.FromSeconds(5));

            sourceNode.SetCurrentContext();
            DateTime routeTime = Simulator.Instance.Now;

            Results.Instance.Increment("Messages multicast");
            source.MulticastMessage();
            Simulator.Instance.RunFor(TimeSpan.FromSeconds(10));

          //  this.ValidateAll();

            sourceNode.UnlockStatus();

            foreach (SimpleMulticast layer in this.mMulticastLayers)
            {
                if (null != layer.Source && !source.MemberId.Address.Equals(layer.Source) && !layer.HasBeenOffline)
                {
                    Logger.Debug("{0} failed to receive message from {1}", layer.MemberId, source.MemberId);
                    Simulator.Instance.Suspend = this.PauseOnFailure;
                }
            }
            //Simulator.Instance.Suspend = true;
            Simulator.Instance.RunFor(TimeSpan.FromSeconds(1));
        }

        private void ValidateAll()
        {
            lock (this.mCheckPointers)
            {
                Checkpoint checkpoint = new Checkpoint();
                for (int i = 0; i < this.mMulticastLayers.Count; i++)
                {
                    PeerAddress address = this.mNodes[i].Address;

                    if (this.mNodes.Count > i && this.mNodes[i].NetworkContext.Status == ConnectivityStatus.Online &&
                        address.IsValid && this.mDhtFacades[address.Address].IsRouting && this.mCheckPointers.ContainsKey(address.Address))
                    {
                        CheckpointItemSet itemSet = this.mCheckPointers[address.Address].GetCheckpointData();

                        if (null != itemSet)
                        {
                            checkpoint.Add(address, itemSet);
                        }
                    }
                }

                List<String> issues = checkpoint.Validate();

                foreach (String issue in issues)
                {
                    Logger.TraceError(issue);
                    Simulator.Instance.Suspend = this.PauseOnFailure;
                }
            }
        }

        public override void Render(System.Drawing.Graphics context, Dictionary<string, object> options)
        {
            base.Render(context, options);

            if (options.ContainsKey("dht radius"))
            {
                float radius = (float)options["dht radius"];

                Dictionary<String, object> multicastRenderOptions = new Dictionary<string, object>();

                multicastRenderOptions.Add("dht radius", radius - 5f);
                multicastRenderOptions.Add("render leafset", null);

                for (int i = 0; i < this.mMulticastLayers.Count; i++)
                {
                    if (this.mNodes[i].NetworkContext.Status == ConnectivityStatus.Online)
                    {
                        PeerAddress address = this.mMulticastLayers[i].Address;
                        String peerView = options.ContainsKey("single peer") ? options["single peer"] as String : null;
                        if (null == peerView || peerView == this.mMulticastLayers[i].Address.ToString())
                        {
                            if (options.ContainsKey("render broadcast path"))
                            {
                                this.mMulticastLayers[i].Render(context, radius);
                            }
                        }
                    }
                }
            }
        }
    }
}

