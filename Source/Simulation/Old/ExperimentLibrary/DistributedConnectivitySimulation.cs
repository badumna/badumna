using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;

using Badumna.Core;
using Badumna.Utilities;
using Badumna.Transport;
using Badumna.DistributedHashTable;
using System.Net;

using NetworkSimulator;

namespace ExperimentLibrary
{
    public class DistributedConnectivitySimulation : DhtSimulation
    {
        private Random mGenerator = new Random((int)DateTime.Now.Ticks);
        private List<SimpleSendLayer> mConnectivityLayers = new List<SimpleSendLayer>();
        private Dictionary<IPAddress, DhtFacade> mTierTwoFacades = new Dictionary<IPAddress, DhtFacade>();

        public DistributedConnectivitySimulation()
            : base()
        {
            this.DhtTier = 1;
            RendezvousModule.Instance.RendezvousServiceTypeName = "Badumna.Transport.DistributedRendezvousService";
        }

        protected override void ContinueInitialization(INode node, ConnectionTable connectionTable)
        {
            SimpleSendLayer connectivityLayer = new SimpleSendLayer(connectionTable);
            this.mConnectivityLayers.Add(connectivityLayer);

            DhtFacade facade = new DhtFacade(connectionTable, 2);

            // Ensure that the first node is always in the recent online peer list.
            // This is because it may be the only peer that is in tier 1.
            if (!this.mRecentOnlinePeers.Contains(this.mNodes[0].Address))
            {
                this.mRecentOnlinePeers.RemoveAt(0);
                this.mRecentOnlinePeers.Add(this.mNodes[0].Address);
            }

            facade.Initialize(this.mRecentOnlinePeers);
            this.mTierTwoFacades.Add(node.Address.Address, facade);
        }

        protected override void HanldeNodeInitialize(SimulatorFacade facade)
        {
            this.mTierTwoFacades[node.Address.Address].Initialize(this.mRecentOnlinePeers);
        }

        public override void Run()
        {
            Debug.Assert(this.mConnectivityLayers.Count == this.mNodes.Count, "The number of services is not equal to the number of nodes.");

            DateTime endTime = Simulator.Instance.Now + TimeSpan.FromSeconds(3600);

            Simulator.Instance.RunFor(TimeSpan.FromSeconds(180));
            Logger.Debug("----------|-----------");
            while (Simulator.Instance.Now < endTime)
            {
                Simulator.Instance.RunFor(TimeSpan.FromSeconds(10));
                this.SendToRandomPairTest();
            }

            base.Cleanup();
            this.mConnectivityLayers.Clear();
            this.mTierTwoFacades.Clear();
        }

        public void SendToRandomPairTest()
        {
            int i = this.mGenerator.Next(0, this.mConnectivityLayers.Count);
            int j = this.mGenerator.Next(0, this.mConnectivityLayers.Count);

            INode sourceNode = this.mNodes[i];
            INode destinationNode = this.mNodes[j];

            sourceNode.LockOnline();
            if (i != j)
            {
                destinationNode.LockOnline();
            }
            Simulator.Instance.RunFor(TimeSpan.FromSeconds(15));

            SimpleSendLayer source = this.mConnectivityLayers[i];
            SimpleSendLayer destination = this.mConnectivityLayers[j];

            sourceNode.SetCurrentContext();
            this.SendMessage(source, destination, destinationNode.Address);

            destinationNode.SetCurrentContext();
            this.SendMessage(destination, source, sourceNode.Address);

            sourceNode.UnlockStatus();

            if (i != j)
            {
                destinationNode.UnlockStatus();
            }
        }

        private void SendMessage(SimpleSendLayer source, SimpleSendLayer destination, PeerAddress destinationAddress)
        {
            Logger.Debug("---------------------- {0} ---------------------------", destinationAddress.HumanReadable());

            destination.NumMessagesArrived = 0;

            source.SendTo(destinationAddress);
            Simulator.Instance.RunFor(TimeSpan.FromSeconds(30));

            if (destination.NumMessagesArrived == 1)
            {
                Results.Instance.Increment("Successful sends");
            }
            else
            {
                if (destination.NumMessagesArrived == 0)
                {
                    Logger.Debug("Failed to send to {0}", destinationAddress.HumanReadable());
                }
                else
                {
                    Logger.Debug("Duplicate messages sent to {0} arrived", destinationAddress.HumanReadable());

                }
                Results.Instance.Increment("Failed sends");
                Simulator.Instance.Suspend = this.PauseOnFailure;
            }
        }

    }
}
