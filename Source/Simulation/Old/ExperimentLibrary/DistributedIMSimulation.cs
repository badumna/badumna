using System;
using System.Collections.Generic;
using System.Text;
using System.Net;

using Badumna.DistributedHashTable;
using Badumna.InterestManagement;
using Badumna.Core;
using Badumna.DataTypes;

using NetworkSimulator;

using Badumna.Utilities;
using Badumna.Transport;

namespace ExperimentLibrary
{
    public class DistributedIMSimulation : DhtSimulation
    {
        List<ImsRegion> mRegions = new List<ImsRegion>();
        Dictionary<IPAddress, List<UniqueNetworkId>> mIntersectionLists = new Dictionary<IPAddress, List<UniqueNetworkId>>();
        Dictionary<IPAddress, NetworkEvent> mHeartbeatEvents = new Dictionary<IPAddress, NetworkEvent>();

        List<IInterestManagementService> mImServices = new List<IInterestManagementService>();

        private Random mGenerator = new Random((int)DateTime.Now.Ticks);

        private const int mMax = 250;
        private const int mMin = -250;

        private const int mMaxRadius = 50;
        private const int mMinRadius = 15;

        public DistributedIMSimulation()
        {
            this.DhtTier = 2;
        }

        protected override void NewServiceInstance(DhtFacade dhtFacade, PeerAddress address)
        {
            DistributedImService imService = new DistributedImService(dhtFacade);
            CellList.CellSpan = new Vector3(50, 50, 50);

            this.mImServices.Add(imService);
            this.mIntersectionLists.Add(address.Address, new List<UniqueNetworkId>());
            imService.Initialize("tester", "huha");
        }

        public override void Run()
        {
            this.IntersectionTest();

            base.Cleanup();
            this.mImServices.Clear();
            this.mIntersectionLists.Clear();
            this.mRegions.Clear();
        }

        private InterestRegion RandomizeRegion(INode node)
        {
            Vector3 centroid = new Vector3((float)this.mGenerator.Next(DistributedIMSimulation.mMin, DistributedIMSimulation.mMax),
                (float)this.mGenerator.Next(DistributedIMSimulation.mMin, DistributedIMSimulation.mMax),
                (float)this.mGenerator.Next(DistributedIMSimulation.mMin, DistributedIMSimulation.mMax));
           float radius = (float)this.mGenerator.Next(DistributedIMSimulation.mMinRadius, DistributedIMSimulation.mMaxRadius);

            node.SetCurrentContext();
            return new InterestRegion(centroid, radius);
        }

        private void CreateRegionForNode(INode node, IInterestManagementService imService)
        {
            System.Diagnostics.Debug.Assert(node.NetworkContext.Status == ConnectivityStatus.Online,
                String.Format("Node {0} must be online in order to create a region", node.Address.HumanReadable()));

            node.SetCurrentContext();
            InterestRegion region = this.RandomizeRegion(node);
            imService.InsertRegion("test scene", region, 30, this.EnterNotificationHandler, this.ExitNotificationHandler, this.RequestNotification);
            Results.Instance.TimerStart(region.RequestInfo.Id);

            Logger.Debug("{0} subscribes to {1}, r{2} -- request id ({3})", node.Address.HumanReadable(),
                region.Centroid, region.Radius, region.RequestInfo.Id);

            this.mRegions.Add(region);

            NetworkEvent heartbeastEvent = Simulator.Instance.Push("Heart beat region", 20000,
                new GenericCallBack<INode, IInterestManagementService>(this.HeartbeatRegionFor), node, imService);

            if (this.mHeartbeatEvents.ContainsKey(node.Address.Address))
            {
                this.mHeartbeatEvents.Remove(node.Address.Address);
            }

            this.mHeartbeatEvents.Add(node.Address.Address, heartbeastEvent);
        }

        private void HeartbeatRegionFor(INode node, IInterestManagementService imService)
        {
            NetworkEvent heartbeastEvent = Simulator.Instance.Push("Heart beat region", 20000,
                new GenericCallBack<INode, IInterestManagementService>(this.HeartbeatRegionFor), node, imService);

            if (this.mHeartbeatEvents.ContainsKey(node.Address.Address))
            {
                this.mHeartbeatEvents.Remove(node.Address.Address);
            }

            this.mHeartbeatEvents.Add(node.Address.Address, heartbeastEvent);

            if (node.NetworkContext.Status == ConnectivityStatus.Online)
            {
                ImsRegion region = this.NodesRegion(node);

                if (null != region)
                {
                    List<String> heartbeatObjects = new List<string>();

                    node.SetCurrentContext();
                    imService.ModifyRegion("test scene", region, 30);                  
                }
            }
        }

        protected override void HanldeNodeOffline(SimulatorFacade facade)
        {
            if (this.mHeartbeatEvents.ContainsKey(node.Address.Address))
            {
                Simulator.Instance.Remove(this.mHeartbeatEvents[node.Address.Address]);
                this.mHeartbeatEvents.Remove(node.Address.Address);
            }

            if (this.mIntersectionLists.ContainsKey(node.Address.Address))
            {
                this.mIntersectionLists[node.Address.Address].Clear();
            }

            for (int i = 0; i < this.mRegions.Count; i++)
            {
                if (this.mRegions[i].Address.Equals(node.Address))
                {
                    Logger.Debug("{0} region {1}", i, this.mRegions[i].Address.HumanReadable());
                    this.mRegions.RemoveAt(i);

                    Logger.Debug("Removing region {0} from simulation list", node.Address.HumanReadable());
                    Logger.Debug("{0} regions", this.mRegions.Count);

                    return;
                }
            }

            System.Diagnostics.Debug.Assert(false, "No region found.");
        }

        protected override void HanldeNodeOnline(SimulatorFacade facade)
        {                
            System.Diagnostics.Debug.Assert(null == this.NodesRegion(node), 
                String.Format("Region {0} found.", node.Address.HumanReadable()));

            this.CreateRegionForNode(node, this.NodesImService(node));
        }

        private ImsRegion NodesRegion(INode node)
        {
            foreach (ImsRegion region in this.mRegions)
            {
                if (region.Address.Equals(node.Address))
                {
                    return region;
                }
            }

            return null;
        }

        private IInterestManagementService NodesImService(INode node)
        {
            return this.mImServices[this.mNodes.IndexOf(node)];
        }

        private void IntersectionTest()
        {
            DateTime endTime = Simulator.Instance.Now + TimeSpan.FromSeconds(3600);

            Results.Instance.Increment("Intersections undected");
            Results.Instance.Decrement("Intersections undected");
            Results.Instance.Increment("Seperations undetected");
            Results.Instance.Decrement("Seperations undetected");

            TimeSpan slice = TimeSpan.FromSeconds(90);

            // Run the sim for a while to give time for initialization.
            Simulator.Instance.RunFor(TimeSpan.FromSeconds(300));

            Logger.Debug(" *** BEGIN *** {0}, {1}", this.mImServices.Count, this.mNodes.Count);
            System.Diagnostics.Debug.Assert(this.mImServices.Count == this.mNodes.Count, "The number of services is not equal to the number of nodes.");

            // Enter the event regions
            for (int i = 0; i < this.mNodes.Count; i++)
            {
                this.mNodes[i].LockOnline();
                Simulator.Instance.RunFor(TimeSpan.FromSeconds(5));
                if (null == this.NodesRegion(this.mNodes[i]))
                {
                    this.CreateRegionForNode(this.mNodes[i], this.mImServices[i]);
                }

                Simulator.Instance.RunFor(slice);
                this.AssertCorrectIntersections();
                this.mNodes[i].UnlockStatus();
            }

            // Continue modifing the event regions
            int j = 0;
            while (Simulator.Instance.Now < endTime)
            {
                j = (j + 1) % this.mNodes.Count;

                this.mNodes[j].LockOnline();
                Simulator.Instance.RunFor(TimeSpan.FromSeconds(5));

                Logger.Debug("---------------------- {0} ---------------------------", this.mNodes[j].Address.HumanReadable());

                this.mNodes[j].SetCurrentContext();
                InterestRegion newRegion = this.RandomizeRegion(this.mNodes[j]);

                System.Diagnostics.Debug.Assert(null != this.NodesRegion(this.mNodes[j]),
                    String.Format("Region {0} not found.", this.mNodes[j].Address.HumanReadable()));

                ImsRegion region = this.NodesRegion(this.mNodes[j]);

                region.Radius = newRegion.Radius;
                region.Centroid = newRegion.Centroid;
                RequestInfo request = this.mImServices[j].ModifyRegion("test scene", region, 100000);
                Results.Instance.TimerStart(request.Id);

                Logger.Debug("{0} modifies subscription {1}, r{2} -- request id ({3})", this.mNodes[j].Address.HumanReadable(),
                    region.Centroid, region.Radius, this.NodesRegion(this.mNodes[j]).RequestInfo.Id);

                Simulator.Instance.RunFor(slice);
                this.AssertCorrectIntersections();
                this.mNodes[j].UnlockStatus();
            }
        }

        private void AssertCorrectIntersections()
        {
            foreach (ImsRegion region in this.mRegions)
            {
                Logger.Debug("{0},{1} {2}", region.Centroid, region.Radius,
                    region.Address);
            }
            Logger.Debug("----------------------*---------------------------");

            foreach (ImsRegion regionOne in this.mRegions)
            {
                foreach (ImsRegion regionTwo in this.mRegions)
                {
                    if (!regionOne.Equals(regionTwo))
                    {
                        if (regionOne.IsIntersecting(regionTwo))
                        {
                            if (!this.mIntersectionLists[regionOne.Address.Address].Contains(regionTwo.Guid))
                            {
                                Logger.Debug("{0}, r{1} ({4}) intersection with {2}, r{3} ({5}) undetected.",
                                    regionOne.Centroid, regionOne.Radius,
                                    regionTwo.Centroid, regionTwo.Radius,
                                    regionOne.Guid, regionTwo.Guid);

                                Results.Instance.Increment("Intersections undected");
                                Simulator.Instance.Suspend = this.PauseOnFailure;
                            }
                        }
                        else
                        {
                            if (this.mIntersectionLists[regionOne.Address.Address].Contains(regionTwo.Guid))
                            {
                                Logger.Debug("{0}, r{1} ({4}) seperation with {2}, r{3} ({5}) undetected.",
                                    regionOne.Centroid, regionOne.Radius,
                                    regionTwo.Centroid, regionTwo.Radius,
                                    regionOne.Guid, regionTwo.Guid);

                                Results.Instance.Increment("Seperations undetected");
                                Simulator.Instance.Suspend = this.PauseOnFailure;
                            }
                        }
                    }
                }
            }
        }

        private void EnterNotificationHandler(object sender, ImsEventArgs e)
        {
            PeerAddress address = NetworkContext.CurrentContext.PublicAddress;

            System.Diagnostics.Debug.Assert(address.Equals(e.Region.Address),
                String.Format("Notification for {0} recieved at incorrect peer {1}", e.Region.Address.HumanReadable(),
                address.HumanReadable()));

            if (!this.mIntersectionLists[address.Address].Contains(e.IntersectingRegion.Guid))
            {
                this.mIntersectionLists[address.Address].Add(e.IntersectingRegion.Guid);
                Results.Instance.Increment("Number of intersections");

                Logger.Debug("Intersection {0} {1}", e.Region.Address, e.IntersectingRegion.Guid);
            }
            else
            {
                Logger.Debug("Intersection between {0} {1} already known",
                    e.Region.Address, e.IntersectingRegion.Address);
            }
        }

        private void ExitNotificationHandler(object sender, ImsEventArgs e)
        {
            PeerAddress address = NetworkContext.CurrentContext.PublicAddress;

            System.Diagnostics.Debug.Assert(address.Equals(e.Region.Address),
                String.Format("Notification for {0} recieved at incorrect peer {1}", e.Region.Address,
                address.HumanReadable()));

            if (this.mIntersectionLists[address.Address].Contains(e.IntersectingRegion.Guid))
            {
                this.mIntersectionLists[address.Address].Remove(e.IntersectingRegion.Guid);
                Results.Instance.Decrement("Number of intersections");

                Logger.Debug("Seperation {0} {1}", e.Region.Address, e.IntersectingRegion.Guid);
            }
            else
            {
                Logger.Debug("Seperation between {0} {1} already known",
                    e.Region.Address, e.IntersectingRegion.Address.HumanReadable());
            }
        }

        private void RequestNotification(object sender, ImsStatusEventArgs e)
        {
            PeerAddress address = NetworkContext.CurrentContext.PublicAddress;

            if (e.Status.IsComplete)
            {
                Logger.Debug("Request {0} status at {1} is complete", e.Status.Id, address.HumanReadable());
            }
            if (e.Status.HasFailed)
            {
                Logger.Debug("Request {0} status at {1} has failed", e.Status.Id, address.HumanReadable());
            }


            Results.Instance.TimerStop("Request time", e.Status.Id);
        }
    }
}
