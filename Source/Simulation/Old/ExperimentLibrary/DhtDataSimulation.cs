using System;
using System.Collections.Generic;
using System.Text;

using Badumna.DistributedHashTable;
using Badumna.Core;
using Badumna.Utilities;
using Badumna.Transport;

using NetworkSimulator;

namespace ExperimentLibrary
{
    public class DhtDataSimulation : DhtSimulation
    {
        protected List<DataService> mServices = new List<DataService>();
        private List<ReplicatedDataObject> mReplicas = new List<ReplicatedDataObject>();
        private Random mGenerator = new Random((int)DateTime.Now.Ticks);

        private String mDataValue = "A test string";
        private int mNumberOfExpectedValues;
        private List<PeerAddress> mNodesThatReceivedAReply = new List<PeerAddress>();
        private List<INode> mNodesThatExpectAReply = new List<INode>();

        private PeerAddress mSourceNodesAddress;

        public DhtDataSimulation() 
        {
            this.DhtTier = 2;
        }

        protected override void NewServiceInstance(DhtFacade dhtFacade, PeerAddress address)
        {
            DataService service = new DataService(dhtFacade);
            this.mServices.Add(service);
        }

        public override void Run()
        {
            DateTime endTime = Simulator.Instance.Now + TimeSpan.FromSeconds(36000);

            while (Simulator.Instance.Now < endTime)
            {
                Simulator.Instance.RunFor(TimeSpan.FromSeconds(180));
                this.PutGetTest();
            }

            base.Cleanup();
            this.mServices.Clear();
        }

        public void PutGetTest()
        {
            System.Diagnostics.Debug.Assert(this.mServices.Count == this.mNodes.Count, "The number of services is not equal to the number of nodes.");

            int nodeIndex = this.mGenerator.Next(0, this.mNodes.Count - 1);
            INode node = this.mNodes[nodeIndex];
            DataService service = this.mServices[nodeIndex];

            this.mSourceNodesAddress = node.Address;

            node.LockOnline();
            Simulator.Instance.RunFor(TimeSpan.FromMilliseconds(Parameters.NodeInitializationDelayMs));
            System.Diagnostics.Debug.Assert(node.NetworkContext.Status == ConnectivityStatus.Online);

            HashKey dataKey = HashKey.Random();

            Logger.Debug("--------------------------------------***PUT***--------------------------------------");
            Logger.Debug("{0}", Simulator.Instance.Now.ToString());

            node.SetCurrentContext();
            service.Put(dataKey.ToString(), this.mDataValue, 300);
            Simulator.Instance.RunFor(TimeSpan.FromSeconds(200));
            this.AssertDataAvailability(dataKey.ToString());

            Simulator.Instance.RunFor(TimeSpan.FromSeconds(300));
            Logger.Debug("--------------------------------------***EXPIRE***--------------------------------------");
            Logger.Debug("{0}", Simulator.Instance.Now.ToString());

            this.AssertDataUnavailability(dataKey.ToString());

            node.UnlockStatus();
        }

        private void AssertDataAvailability(String dataKey)
        {
            this.mNumberOfExpectedValues = 1;
            this.MakeRequests(dataKey);
        }

        private void AssertDataUnavailability(String dataKey)
        {
            this.mNumberOfExpectedValues = 0;
            this.MakeRequests(dataKey);
        }

        private void MakeRequests(String dataKey)
        {
            for (int i = 0; i < this.mNodes.Count; i++)
            {
                if (this.mNodes[i].NetworkContext.Status == ConnectivityStatus.Online)
                {
                    this.mNodes[i].SetCurrentContext();

                    if (!this.mNodes[i].Address.Equals(this.mSourceNodesAddress))
                    {
                        this.mNodes[i].LockOnline();
                    }
                    this.mServices[i].Get(dataKey, new GetReplyHandler(this.GetReplyHandler));
                    this.mNodesThatExpectAReply.Add(this.mNodes[i]);
                    Results.Instance.TimerStart(this.mNodes[i].Address);
                }
            }

            Simulator.Instance.RunFor(TimeSpan.FromSeconds(60));

            foreach (INode node in this.mNodesThatExpectAReply)
            {
                if (!this.mNodesThatReceivedAReply.Contains(node.Address))
                {
                    Logger.Debug("Failed to get data from {0}", node.Address.HumanReadable());

                    Results.Instance.TimerStop("Failed get request", node.Address);

                    Results.Instance.Increment("Get request failure");
                    Simulator.Instance.Suspend = this.PauseOnFailure;
                }
            }

            this.mNodesThatReceivedAReply.Clear();
            this.mNodesThatExpectAReply.Clear();
        }

        void GetReplyHandler(String key, ICollection<String> values)
        {
            PeerAddress address = NetworkContext.CurrentContext.PublicAddress;

            Results.Instance.TimerStop("Get request", address);

            if (!this.mNodesThatReceivedAReply.Contains(address))
            {
                this.mNodesThatReceivedAReply.Add(address);
            }

            String firstValue = String.Empty;

            if (values.Count > 0)
            {
                IEnumerator<String> enumerator = values.GetEnumerator();

                enumerator.MoveNext();
                firstValue = enumerator.Current;
            }

            Logger.Debug("Got {0} values for key {1} : {2}", values.Count, key, firstValue);

            if (this.mNumberOfExpectedValues != values.Count)
            {
                Logger.Debug("Expected {0} values but got {1}", this.mNumberOfExpectedValues, values.Count);                

                Results.Instance.Increment("Get value count failure");
                Simulator.Instance.Suspend = this.PauseOnFailure;
            }

            foreach (INode node in this.mNodesThatExpectAReply)
            {
                if (!node.Address.Equals(this.mSourceNodesAddress) && address.Equals(node.Address))
                {
                    node.UnlockStatus();
                }
            }
        }
    }
}
