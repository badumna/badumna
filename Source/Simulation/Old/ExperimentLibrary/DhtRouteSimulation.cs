using System;
using System.Collections.Generic;
using System.Text;

using Badumna.DistributedHashTable;
using Badumna.Core;
using Badumna.Utilities;
using Badumna.Transport;

using NetworkSimulator;

namespace ExperimentLibrary
{
    public class DhtRouteSimulation : DhtSimulation
    {
        protected List<SimpleRouteLayer> mServices = new List<SimpleRouteLayer>();
        private Random mGenerator = new Random((int)DateTime.Now.Ticks);
        private bool mMessageArrived;

        private HashKey mDestinationKey;

        private List<PeerAddress> mRoutePath = new List<PeerAddress>();

        public DhtRouteSimulation()
        {
            this.DhtTier = 2;
        }

        protected override void NewServiceInstance(DhtFacade dhtFacade, PeerAddress address)
        {
            SimpleRouteLayer service = new SimpleRouteLayer(dhtFacade);

            service.RoutedMessageArrivalEvent += this.RoutedMessageArrivalHandler;
            service.RoutedMessagePathEvent += service_RoutedMessagePathEvent;
            this.mServices.Add(service);
        }

        void service_RoutedMessagePathEvent(object sender, ReceivedRoutedMessageEventArgs e)
        {
            lock (this.mRoutePath)
            {
                if (!this.mRoutePath.Contains(e.DestinationAddress))
                {
                    this.mRoutePath.Add(e.DestinationAddress);
                }
            }
        }

        private void RoutedMessageArrivalHandler(object sender, ReceivedRoutedMessageEventArgs e)
        {
            this.AssertDestinationIsCorrect(e.Key, e.DestinationAddress);
        }

        public override void Run()
        {
            Results.Instance.Decrement("Route failure");
            Results.Instance.Increment("Route failure");

            DateTime endTime = Simulator.Instance.Now + TimeSpan.FromSeconds(3600);

            Simulator.Instance.RunFor(TimeSpan.FromSeconds(180));
            Logger.Debug("----------|-----------");
            while (Simulator.Instance.Now < endTime)
            {
                Simulator.Instance.RunFor(TimeSpan.FromSeconds(10));
                this.RouteTest();
            }

            base.Cleanup();
            this.mServices.Clear();
            this.mDhtFacades.Clear();
        }


        public void RouteTest()
        {
            int i = this.mGenerator.Next(0, this.mServices.Count);

            INode sourceNode = this.mNodes[i];
            SimpleRouteLayer source = this.mServices[i];
            this.mDestinationKey = HashKey.Random();

            Logger.Debug("---------------------- {0} ---------------------------", this.mDestinationKey.HumanReadable());

            sourceNode.LockOnline();
            Simulator.Instance.RunFor(TimeSpan.FromSeconds(5));

            this.mMessageArrived = false;
            sourceNode.SetCurrentContext();
            DateTime routeTime = Simulator.Instance.Now;

            Results.Instance.Increment("Messages routed");
            source.RouteToKey(this.mDestinationKey);

            lock (this.mRoutePath)
            {
                this.mRoutePath.Add(sourceNode.Address);
            }

            Simulator.Instance.RunFor(TimeSpan.FromSeconds(60));

            sourceNode.UnlockStatus();

            if (!this.mMessageArrived)
            {
                Logger.Debug("{0} seconds later message hasn't arrived", (Simulator.Instance.Now - routeTime).TotalSeconds);

                Results.Instance.Increment("Never arrived");
                Results.Instance.Increment("Route failure");
                Simulator.Instance.Suspend = this.PauseOnFailure;
            }

            lock (this.mRoutePath)
            {
                this.mRoutePath.Clear();
            }
        }

        private void ReplaceIfCloser(ref List<HashKey> closestKeys, ref List<PeerAddress> closestAddresses, HashKey key, PeerAddress nodesAddress)
        {
            HashKey nodesKey = HashKey.Hash(nodesAddress);

            for (int i = 0; i < closestKeys.Count; i++)
            {
                if (nodesKey.AbsoluteDistanceFrom(key) < closestKeys[i].AbsoluteDistanceFrom(key))
                {
                    PeerAddress furtherNodesAddress = closestAddresses[i];
                    closestKeys[i] = nodesKey;
                    closestAddresses[i] = nodesAddress;
                    this.ReplaceIfCloser(ref closestKeys, ref closestAddresses, key, furtherNodesAddress);
                    break;
                }
            }
        }

        private void AssertDestinationIsCorrect(HashKey key, PeerAddress address)
        {
            HashKey destinationsKey = HashKey.Hash(address);

            List<HashKey> closestKeys = new List<HashKey>();
            List<PeerAddress> closestAddresses = new List<PeerAddress>();

            if (this.mMessageArrived)
            {
                Results.Instance.Increment("Duplicate arrivals");
                Logger.Debug("Ignoring seconds message");
                return;
            }

            if (!key.Equals(this.mDestinationKey))
            {
                Logger.Debug("Ignoring message arrival for unknown key");
                return;
            }

            this.mMessageArrived = true;

            foreach (INode node in this.mNodes)
            {
                if (this.mDhtFacades[node.Address.Address].IsRouting)
                {
                    if (closestKeys.Count < 5)
                    {
                        HashKey nodesKey = HashKey.Hash(node.Address);

                        closestKeys.Add(nodesKey);
                        closestAddresses.Add(node.Address);
                    }
                    else
                    {
                        this.ReplaceIfCloser(ref closestKeys, ref closestAddresses, key, node.Address);
                    }
                }
            }

            if (!closestAddresses.Contains(address))
            {
                Logger.Debug("Failed to route to correct destination.");
                Logger.Debug("Should have routed to one of :");
                for (int i = 0; i < closestKeys.Count; i++)
                {
                    Logger.Debug(" {0} : {1}", closestAddresses[i].HumanReadable(), closestKeys[i].HumanReadable());
                }
                Logger.Debug("But arrived at {0} : {1}", address.HumanReadable(), destinationsKey.HumanReadable());

                Results.Instance.Increment("Wrong destination");
                Results.Instance.Increment("Route failure");
                Simulator.Instance.Suspend = this.PauseOnFailure;

            }
        }

        public override void Render(System.Drawing.Graphics context, Dictionary<string, object> options)
        {
            base.Render(context, options);

            if (options.ContainsKey("dht radius"))
            {
                float radius = (float)options["dht radius"];

                lock (this.mRoutePath)
                {
                    for (int i = 0; i < this.mRoutePath.Count - 1; i++)
                    {
                        Badumna.Diagnostics.DhtVisualiser.Instance.RenderPath(this.mRoutePath[i], this.mRoutePath[i + 1], context, radius);
                    }
                }                
            }
        }
    }
}
