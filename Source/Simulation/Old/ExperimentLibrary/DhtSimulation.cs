using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Drawing;

using NetworkSimulator;

using Badumna.Utilities;
using Badumna.Transport;
using Badumna.DistributedHashTable;
using Badumna.Core;
using Badumna.Diagnostics;


namespace ExperimentLibrary
{
    public abstract class DhtSimulation : ISimulation
    {
        protected List<SimulatorFacade> mNodes = new List<SimulatorFacade>();
        protected Dictionary<IPAddress, DhtFacade> mDhtFacades = new Dictionary<IPAddress, DhtFacade>();

        protected List<PeerAddress> mRecentOnlinePeers = new List<PeerAddress>();
        protected Dictionary<IPAddress, Checkpointer> mCheckPointers = new Dictionary<IPAddress, Checkpointer>();

        private int mDhtTier;
        protected int DhtTier
        {
            get { return this.mDhtTier; }
            set { this.mDhtTier = value; }
        }

        private bool mPauseOnFailure;
        public bool PauseOnFailure
        {
            get { return this.mPauseOnFailure; }
            set { this.mPauseOnFailure = value; }
        }

        public void InitializeNodeHandler(object sender, NodeInitializationEventArgs e)
        {
            /*
            this.mNodes.Add(e.SimulatorFacade);

            ConnectionTable connectionTable = new ConnectionTable(e.SimulatorFacade);
            DhtFacade dht = new DhtFacade(connectionTable, this.mDhtTier);

            // Have to create a new list because by the time this reference is passed to the
            // router its contents have changes.
            List<PeerAddress> initialPeersList = new List<PeerAddress>();
            initialPeersList.InsertRange(0, e.InitialPeers);

            dht.Initialize(initialPeersList);
            
            //NetworkContext.CurrentContext.DhtTiers = new DhtFacadeCollection(null, dht, null);

            connectionTable.Initialize();

            lock (this.mDhtFacades)
            {
                this.mDhtFacades.Add(e.SimulatorFacade.Address.Address, dht);
            }
            this.AddAddressToRecentList(e.SimulatorFacade.Address);

            lock (this.mCheckPointers)
            {
                Checkpointer checkPointer = new Checkpointer();

                checkPointer.Register((DhtFacade)dht);
                this.mCheckPointers.Add(e.SimulatorFacade.Address.Address, checkPointer);
            }

            this.NewServiceInstance(dht, e.SimulatorFacade.Address);

            e.SimulatorFacade.NetworkContext.NetworkOfflineEvent += new NetworkContextHandler(NetworkContext_NetworkOfflineEvent);
            e.SimulatorFacade.NetworkContext.NetworkOnlineEvent += new NetworkContextHandler(NetworkContext_NetworkOnlineEvent);
            e.SimulatorFacade.NetworkContext.NetworkInitializingEvent += new NetworkContextHandler(NetworkContext_NetworkInitializingEvent);

            lock (this.mDhtFacades)
            {
                this.ContinueInitialization(e.SimulatorFacade, connectionTable);
            }
             */
        }

        private void AddAddressToRecentList(PeerAddress address)
        {
            if (!this.mRecentOnlinePeers.Contains(address))
            {
                if (this.mRecentOnlinePeers.Count > 4)
                {
                    this.mRecentOnlinePeers.RemoveAt(1);
                }

                this.mRecentOnlinePeers.Add(address);
            }
        }

        void NetworkContext_NetworkInitializingEvent()
        {
            PeerAddress address = NetworkContext.CurrentContext.PublicAddress;

            lock (this.mDhtFacades)
            {
                System.Diagnostics.Debug.Assert(this.mDhtFacades.ContainsKey(address.Address), "Failed to find dht facade");
                this.mDhtFacades[address.Address].Initialize(this.mRecentOnlinePeers);
            }

            int i;
            for (i = 0; i < this.mNodes.Count; i++)
            {
                if (this.mNodes[i].Address.Equals(address))
                {
                    break;
                }
            }

            this.AddAddressToRecentList(address);

            if (i < this.mNodes.Count)
            {
                this.HanldeNodeInitialize(this.mNodes[i]);
            }
        }

        void NetworkContext_NetworkOnlineEvent()
        {
            PeerAddress address = NetworkContext.CurrentContext.PublicAddress;

            int i;
            for (i = 0; i < this.mNodes.Count; i++)
            {
                if (this.mNodes[i].Address.Equals(address))
                {
                    break;
                }
            }

            this.AddAddressToRecentList(address);

            if (i < this.mNodes.Count)
            {
                this.HanldeNodeOnline(this.mNodes[i]);
            }
        }

        void NetworkContext_NetworkOfflineEvent()
        {
            PeerAddress address = NetworkContext.CurrentContext.PublicAddress;

            int i;
            for (i = 0; i < this.mNodes.Count; i++)
            {
                if (this.mNodes[i].Address.Equals(address))
                {
                    break;
                }
            }

            if (i < this.mNodes.Count)
            {
                this.HanldeNodeOffline(this.mNodes[i]);
            }
        }

        protected virtual void NewServiceInstance(DhtFacade dhtFacade, PeerAddress address)
        { }

        protected virtual void HanldeNodeOffline(SimulatorFacade facade)
        { }

        protected virtual void HanldeNodeOnline(SimulatorFacade facade)
        { }

        protected virtual void HanldeNodeInitialize(SimulatorFacade facade)
        { }

        public abstract void Run();

        protected void Cleanup()
        {
            lock (this.mDhtFacades)
            {
                this.mNodes.Clear();
                this.mDhtFacades.Clear();
                this.mRecentOnlinePeers.Clear();
            }
            lock (this.mCheckPointers)
            {
                this.mCheckPointers.Clear();
            }
        }

        public virtual void Render(Graphics context, Dictionary<string, object> options)
        {

            lock (this.mCheckPointers)
            {
                String peerView = null;
                if (options.ContainsKey("single peer"))
                {
                    peerView = options["single peer"] as String;
                }

                lock (this.mDhtFacades)
                {
                    foreach (KeyValuePair<IPAddress, DhtFacade> item in this.mDhtFacades)
                    {
                        if (item.Value.IsInitialized && (null == peerView || peerView.Contains(item.Key.ToString())))
                        {
                            CheckpointItemSet checkData = this.mCheckPointers[item.Key].GetCheckpointData();
                            if (null != checkData)
                            {
                                checkData.Render(context, options);
                            }
                        }
                    }
                }
            }

        }
    }
}
