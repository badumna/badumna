using System;
using System.Collections.Generic;
using System.Text;

using Badumna.Core;
using Badumna.DataTypes;
using Badumna.Utilities;
using Badumna.Multicast;
using NetworkSimulator;

namespace ExperimentLibrary
{
    class SimpleMulticast : MulticastProtocol
    {
        static int mNextTimerKey;

        private UniqueNetworkId mMemeberId;
        public UniqueNetworkId MemberId { get { return this.mMemeberId; } }

        private UniqueNetworkId mGroupId;

        private List<PeerAddress> mIntermediatePeers = new List<PeerAddress>();

        private PeerAddress mSource;
        public PeerAddress Source
        {
            get { return this.mSource; }
        }

        private PeerAddress mAddress = PeerAddress.Nowhere;
        public PeerAddress Address { get { return this.mAddress; } }

        private bool mHasBeenOffline;
        public bool HasBeenOffline { get { return this.mHasBeenOffline; } }

        public SimpleMulticast(MulticastFacade facade, UniqueNetworkId groupId)
            : base(facade)
        {
            this.RegisterMethodsIn(this);
            this.ForgeMethodList();
            this.mGroupId = groupId;
            this.mMemeberId = facade.AddGroup(groupId);

            NetworkContext.CurrentContext.NetworkOnlineEvent += this.CurrentContext_NetworkOnlineEvent;
            NetworkContext.CurrentContext.NetworkOfflineEvent += this.CurrentContext_NetworkOfflineEvent;
        }

        void CurrentContext_NetworkOfflineEvent()
        {
            this.mSource = null;
            this.mHasBeenOffline = true;
        }

        void CurrentContext_NetworkOnlineEvent()
        {
        }

        public void MulticastMessage()
        {
            MulticastEnvelope handle = this.GetMessageFor(this.mMemeberId, this.mGroupId, QualityOfService.Reliable);

            Results.Instance.TimerStart(SimpleMulticast.mNextTimerKey);
            this.RemoteCall(handle, this, "ReceiveMulticastMessage", SimpleMulticast.mNextTimerKey++);           
            this.SendMessage(handle);
        }

        public void Render(System.Drawing.Graphics context, float dhtRadius)
        {
            lock (this.mIntermediatePeers)
            {

                foreach (PeerAddress source in this.mIntermediatePeers)
                {
                    Badumna.Diagnostics.DhtVisualiser.Instance.RenderBroadcastRange(this.mAddress, source, context, dhtRadius);
                }
            }
        }
        
        public void Reset()
        {
            lock (this.mIntermediatePeers)
            {
                this.mIntermediatePeers.Clear();
            }
            
            this.mSource = null;
            this.mHasBeenOffline = false;
        }

        [ProtocolMethod]
        private void ReceiveMulticastMessage(int timerKey)
        {
            this.mAddress = NetworkContext.CurrentContext.PublicAddress;
            Results.Instance.TimerStop("Routed message delay", timerKey);
            
            lock (this.mIntermediatePeers)
            {
                this.mIntermediatePeers.Add(this.CurrentEnvelope.ImmediateSource);
            }

            if (null != this.mSource)
            {
                Badumna.Utilities.Simulator.Instance.Suspend = true;
                Logger.TraceError("Multiple receives for the same message from {0} at {1}", this.CurrentEnvelope.Source, NetworkContext.CurrentContext.PublicAddress);
            }

            this.mSource = this.CurrentEnvelope.Source;
        }
    }
}
