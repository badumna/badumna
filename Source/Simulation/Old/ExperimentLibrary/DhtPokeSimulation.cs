using System;
using System.Collections.Generic;
using System.Text;

using Badumna.DistributedHashTable;
using Badumna.Core;
using Badumna.Utilities;
using Badumna.Transport;

using NetworkSimulator;

namespace ExperimentLibrary
{ 
    public class DhtPokeSimulation : DhtSimulation
    {   
        private PeerAddress mPokedAddress = null;
        protected List<DataService> mServices = new List<DataService>();

        public DhtPokeSimulation()
        {
            this.DhtTier = 2;
        }

        protected override void NewServiceInstance(DhtFacade dhtFacade, PeerAddress address)
        {
            DataService service = new DataService(dhtFacade);
            this.mServices.Add(service);
        }

        public override void Run()
        {
            Results.Instance.Increment("Poke failure");
            Results.Instance.Decrement("Poke failure");

            this.PokeTest();

            base.Cleanup();
            this.mServices.Clear();
        }

        public void PokeTest()
        {
            System.Diagnostics.Debug.Assert(this.mServices.Count == this.mNodes.Count, "The number of services is not equal to the number of nodes.");

            for (int i = 0; i < this.mNodes.Count; i++)
            {
                this.mNodes[i].LockOnline();
                Simulator.Instance.RunFor(TimeSpan.FromMilliseconds(Parameters.NodeInitializationDelayMs));
                System.Diagnostics.Debug.Assert(this.mNodes[i].NetworkContext.Status == ConnectivityStatus.Online);

                for (int j = 0; j < this.mNodes.Count - 1; j++)
                {
                    HashKey keyJ = HashKey.Hash(this.mNodes[j].Address.ToString());

                    this.mPokedAddress = null;

                    if (i != j)
                    {
                        this.mNodes[j].LockOnline();
                        Simulator.Instance.RunFor(TimeSpan.FromMilliseconds(Parameters.NodeInitializationDelayMs * 10));
                        System.Diagnostics.Debug.Assert(this.mNodes[j].NetworkContext.Status == ConnectivityStatus.Online);
                    }

                    this.mNodes[i].SetCurrentContext();

                    Results.Instance.TimerStart(this.mNodes[j].Address);
                    this.mServices[i].Poke(keyJ, new DataService.PokeReplyHandler(this.PokeReplyHandler));

                    Simulator.Instance.RunFor(TimeSpan.FromSeconds(60));

                    if (null == this.mPokedAddress || !this.mPokedAddress.Address.Equals(this.mNodes[j].Address.Address))
                    {
                        Logger.Debug("Failed to poke {0} from {1}", this.mNodes[j].Address.HumanReadable(),
                            this.mNodes[i].Address.HumanReadable());

                        Results.Instance.TimerStop("Failed poke", this.mNodes[j].Address);

                        Results.Instance.Increment("Poke failure");
                        Simulator.Instance.Suspend = this.PauseOnFailure;
                    }
                    else
                    {
                        Logger.Debug("-----------------");
                    }

                    if (i != j)
                    {
                        this.mNodes[j].UnlockStatus();
                    }
                }

                this.mNodes[i].UnlockStatus();
            }
        }


        public void PokeReplyHandler(HashKey key, PeerAddress address)
        {
            Results.Instance.TimerStop("Route time", address);
            this.mPokedAddress = address;
        }

    }
}
