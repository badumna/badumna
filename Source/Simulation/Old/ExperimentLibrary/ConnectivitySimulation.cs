using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;

using NetworkSimulator;

using Badumna.Core;
using Badumna.Utilities;
using Badumna.Transport;

namespace ExperimentLibrary
{
    public class ConnectivitySimulation : ISimulation 
    {
        private bool mPauseOnFailure;
        public bool PauseOnFailure
        {
            get { return this.mPauseOnFailure; }
            set { this.mPauseOnFailure = value; }
        }

        private Random mGenerator = new Random((int)DateTime.Now.Ticks);
        private List<SimpleSendLayer> mConnectivityLayers = new List<SimpleSendLayer>();
        private List<INode> mNodes = new List<INode>();


        public ConnectivitySimulation()
        {
        }

        public virtual void InitializeNodeHandler(object sender, NodeInitializationEventArgs e)
        {
            e.SimulatorFacade.SetContext();

            this.mNodes.Add(e.SimulatorFacade);
            this.mConnectivityLayers.Add(connectivityLayer);
        }
        
        public void Run()
        {
            Debug.Assert(this.mConnectivityLayers.Count == this.mNodes.Count, "The number of services is not equal to the number of nodes.");

            DateTime endTime = Simulator.Instance.Now + TimeSpan.FromSeconds(3600);

            Simulator.Instance.RunFor(TimeSpan.FromSeconds(180));
            Logger.Debug("----------|-----------");
            while (Simulator.Instance.Now < endTime)
            {
                Simulator.Instance.RunFor(TimeSpan.FromSeconds(10));
                this.SendToRandomPairTest();
            }

            this.mNodes.Clear();
            this.mConnectivityLayers.Clear();
        }

        public void SendToRandomPairTest()
        {
            int i = this.mGenerator.Next(0, this.mConnectivityLayers.Count);
            int j = this.mGenerator.Next(0, this.mConnectivityLayers.Count);

            INode sourceNode = this.mNodes[i];
            INode destinationNode = this.mNodes[j];

            sourceNode.LockOnline();
            if (i != j)
            {
                destinationNode.LockOnline();
            }
            Simulator.Instance.RunFor(TimeSpan.FromSeconds(15));

            SimpleSendLayer source = this.mConnectivityLayers[i];
            SimpleSendLayer destination = this.mConnectivityLayers[j];

            sourceNode.SetCurrentContext();
            this.SendMessage(source, destination, destinationNode.Address);

            destinationNode.SetCurrentContext();
            this.SendMessage(destination, source, sourceNode.Address);

            sourceNode.UnlockStatus();

            if (i != j)
            {
                destinationNode.UnlockStatus();
            }
        }

        private void SendMessage(SimpleSendLayer source, SimpleSendLayer destination, PeerAddress destinationAddress)
        {
            Logger.Debug("---------------------- {0} ---------------------------", destinationAddress.HumanReadable());

            destination.NumMessagesArrived = 0;

            source.SendTo(destinationAddress);
            Simulator.Instance.RunFor(TimeSpan.FromSeconds(30));

            if (destination.NumMessagesArrived == 1)
            {
                Results.Instance.Increment("Successful sends");
            }
            else
            {
                if (destination.NumMessagesArrived == 0)
                {
                    Logger.Debug("Failed to send to {0}", destinationAddress.HumanReadable());
                }
                else
                {
                    Logger.Debug("Duplicate messages sent to {0} arrived", destinationAddress.HumanReadable());

                }
                Results.Instance.Increment("Failed sends");
                Simulator.Instance.Suspend = this.mPauseOnFailure;
            }
        }

        public void Render(System.Drawing.Graphics context, Dictionary<string, object> options)
        {
        }
    }
}
