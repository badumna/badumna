using System;
using System.Collections.Generic;
using System.Text;

using Badumna.Core;
using Badumna.Utilities;
using NetworkSimulator;
using Badumna.DataTypes;
using Badumna.Transport;

namespace ExperimentLibrary
{
    public class SimpleSendLayer : TransportProtocol
    {
        private int mNumMessagesArrived = 0;
        public int NumMessagesArrived
        {
            get { return this.mNumMessagesArrived; }
            set { this.mNumMessagesArrived = value; }
        }

        public SimpleSendLayer(TransportProtocol layer1)
            : base(layer1)
        {
            this.RegisterMethodsIn(this);
            this.ForgeMethodList();
        }

        public void SendTo(PeerAddress destination)
        {
            TransportEnvelope envelope = this.GetMessageFor(destination, QualityOfService.Reliable);
            this.SendMessage(envelope);
        }

        [ProtocolMethod]
        public void TestMethod(UniqueNetworkId messageID)
        {
            Results.Instance.TimerStop("Send message delay", messageID);
            Results.Instance.Increment("Arrived messages");
            this.mNumMessagesArrived++;

            Logger.Debug("Message arrived at {0}", NetworkContext.CurrentContext.PublicAddress.HumanReadable());
        }

        protected override void MessageDeparture(ref TransportEnvelope envelope)
        {
            UniqueNetworkId messageId = UniqueNetworkId.GetNextUniqueID();

            Results.Instance.TimerStart(messageId);
            Results.Instance.Increment("Sent messages");

            Logger.Debug("Sending message to {0} from {1}", envelope.Destination, NetworkContext.CurrentContext.PublicAddress.HumanReadable());

            this.RemoteCall(envelope, this, "TestMethod", messageId);
            base.MessageDeparture(ref envelope);
        }
    }
}
