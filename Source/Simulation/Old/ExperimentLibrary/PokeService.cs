using System;
using System.Collections.Generic;
using System.Text;

using Badumna.Core;
using Badumna.Utilities;
using Badumna.DistributedHashTable;
using NetworkSimulator;

namespace ExperimentLibrary
{
    public class PokeService : DhtProtocol
    {
        public PokeService(DhtFacade facade)
            : base(facade)
        {
            this.RegisterMethodsIn(this);
            this.ForgeMethodList();
        }

        public void Poke(HashKey key, int id)
        {
            DhtEnvelope handle = this.GetMessageFor(key, QualityOfService.Reliable);

            Results.Instance.Increment("Num requests sent");
            Results.Instance.TimerStart("request" + id);

            this.RemoteCall(handle, this, "PokeRequest", NetworkContext.CurrentContext.PublicAddress, id);
            this.SendMessage(handle);
        }

        [ProtocolMethod]
        public void PokeRequest(PeerAddress requestSource, int id)
        {
            DhtEnvelope handle = this.GetMessageFor(requestSource, QualityOfService.Reliable);

            System.Console.WriteLine("{0} poke request from {1} ({2})", NetworkContext.CurrentContext.PublicAddress, requestSource,
                this.CurrentEnvelope.DestinationKey);

            Results.Instance.Increment("Num requests received");
            Results.Instance.TimerStop("Request route time", "request" + id);
            Results.Instance.TimerStart("reply" + id);

            this.RemoteCall(handle, this, "PokeReply", this.CurrentEnvelope.DestinationKey, NetworkContext.CurrentContext.PublicAddress, id);
            this.SendMessage(handle);
        }

        [ProtocolMethod]
        public void PokeReply(HashKey key, PeerAddress address, int id)
        {
            System.Console.WriteLine("{0} poke reply from {1}", NetworkContext.CurrentContext.PublicAddress, address);

            Results.Instance.Increment("Num replies received");
            Results.Instance.TimerStop("Reply arrive time", "reply" + id);
        }

    }

}

