using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.IO;

using Badumna.Utilities;

#if DEBUG
using NUnit.Framework;
#endif

namespace NetworkSimulator
{

    public class Results
    {
        public static readonly Results Instance = new Results();

        private Dictionary<String, int> mCounts = new Dictionary<string, int>();
        private Dictionary<String, object> mValues = new Dictionary<string, object>();
        private Dictionary<String, List<double>> mCollections = new Dictionary<string, List<double>>();
        private Dictionary<object, TimeSpan> mStartTimes = new Dictionary<object, TimeSpan>();

        private int mMaximumCollectionSize = 1000;
        public int MaximumCollectionSize
        {
            get { return this.mMaximumCollectionSize; }
            set { this.mMaximumCollectionSize = value; }
        }

        public void Clear()
        {
            this.mCollections.Clear();
            this.mCounts.Clear();
            this.mValues.Clear();
            this.mStartTimes.Clear();
        }

        public void Clear(String item)
        {
            if (this.mCounts.ContainsKey(item))
            {
                this.mCounts.Remove(item);
            }

            if (this.mValues.ContainsKey(item))
            {
                this.mValues.Remove(item);
            }

            if (this.mCollections.ContainsKey(item))
            {
                this.mCollections.Remove(item);
            }
        }

        public void ClearTimes()
        {
            this.mStartTimes.Clear();
        }

        public void DumpAll(TextWriter writer)
        {

            foreach (KeyValuePair<String, object> value in this.mValues)
            {
                writer.WriteLine(value.Key + " : " + value.Value.ToString());
            }

            foreach (KeyValuePair<String, int> count in this.mCounts)
            {
                writer.WriteLine(count.Key + " : " + count.Value); 
            }

            foreach (KeyValuePair<String, List<double>> collection in this.mCollections)
            {
                writer.WriteLine(collection.Key + " total : " + this.GetCollectionTotal(collection.Key));
                writer.WriteLine(collection.Key + " average : " + this.GetCollectionAverage(collection.Key));
                writer.WriteLine(collection.Key + " median : " + this.GetCollectionMedian(collection.Key));
                writer.WriteLine(collection.Key + " standard deviation : " + this.GetCollectionStdDev(collection.Key));
            }
        }


        public void Increment(String name)
        {
            this.Increment(name, 1);
        }

        public void Increment(String name, int amount)
        {
            if (!this.mCounts.ContainsKey(name))
            {
                this.mCounts.Add(name, 1);
                return;
            }

            this.mCounts[name] += amount;
        }

        public void Decrement(String name)
        {
            this.Decrement(name, 1);
        }
        public void Decrement(String name, int amount)
        {
            if (!this.mCounts.ContainsKey(name))
            {
                this.mCounts.Add(name, -1);
                return;
            }

            this.mCounts[name] -= amount;
        }

        public void SetCount(String name, int amount)
        {
            if (!this.mCounts.ContainsKey(name))
            {
                this.mCounts.Add(name, amount);
                return;
            }

            this.mCounts[name] = amount;
        }

        public bool IsCount(String name)
        {
            return this.mCounts.ContainsKey(name);
        }

        public bool IsCollection(String name)
        {
            return this.mCollections.ContainsKey(name);
        }

        public double GetCount(String name)
        {
            if (!this.mCounts.ContainsKey(name))
            {
                return 0;
            }

            return this.mCounts[name];
        }

        public void AddValue(String name, object value)
        {
            if (!this.mValues.ContainsKey(name))
            {
                this.mValues.Add(name, value);
                return;
            }

            this.mValues[name] = value;
        }

        public object GetValue(String name)
        {
            if (!this.mValues.ContainsKey(name))
            {
                throw new ArgumentException("Cannot get unknown value " + name);
            }

            return this.mValues[name];
        }


        public void TimerStart(object key)
        {
            if (this.mStartTimes.ContainsKey(key))
            {
                throw new InvalidOperationException(String.Format("Cannot start more than one timer with the same name ({0}).", key));
            }

            this.mStartTimes.Add(key, Simulator.Instance.Time);
        }

        public void TimerStop(String name, object key)
        {
            if (!this.mStartTimes.ContainsKey(key))
            {
                return;
            }

            TimeSpan delay = Simulator.Instance.Time - this.mStartTimes[key];

            this.mStartTimes.Remove(key);
            this.AddToCollection(name, delay.TotalSeconds);
        }

        public void AddToCollection(String name, double value)
        {
            if (!this.mCollections.ContainsKey(name))
            {
                this.mCollections.Add(name, new List<double>());               
            }

            this.mCollections[name].Add(value);


            while (this.mCollections[name].Count > this.mMaximumCollectionSize)
            {
                this.mCollections[name].RemoveAt(0);
            }
        }

        public List<double> GetCollection(String name)
        {
            if (!this.mCollections.ContainsKey(name))
            {
                throw new ArgumentException("Unknown parameter name");
            }

            return this.mCollections[name];
        }

        public double GetCollectionTotal(String name)
        {
            if (!this.mCollections.ContainsKey(name))
            {
                throw new ArgumentException("Unknown parameter name");
            }

            return this.mCollections[name].Count;
        }

        public double GetCollectionAverage(String name)
        {
            if (!this.mCollections.ContainsKey(name))
            {
                throw new ArgumentException("Unknown parameter name");
            }

            double sum = 0;
            List<double> collection = this.mCollections[name];

            foreach (double value in collection)
            {
                sum += value;
            }

            return sum / collection.Count;
        }

        public double GetCollectionMedian(String name)
        {
            if (!this.mCollections.ContainsKey(name))
            {
                throw new ArgumentException("Unknown parameter name");
            }
          
            List<double> collection = this.mCollections[name];
            collection.Sort();

            if (0 == collection.Count % 2)
            {
                double a = collection[collection.Count / 2];
                double b = collection[(collection.Count / 2) - 1];
                return (a + b) / 2;
            }

            return collection[collection.Count/2];            
        }

        public double GetCollectionStdDev(String name)
        {
            if (!this.mCollections.ContainsKey(name))
            {
                throw new ArgumentException("Unknown parameter name");
            }
            
            List<double> collection = this.mCollections[name];
            double mean = this.GetCollectionAverage(name);
            double variance = 0;

            foreach (double value in collection)
            {
                variance += Math.Pow((value - mean), 2);
            }

            variance /= (double)collection.Count;

            return Math.Sqrt(variance);
        }

        public double GetCollectionMaximum(String name)
        {
            if (!this.mCollections.ContainsKey(name))
            {
                throw new ArgumentException("Unknown parameter name");
            }

            List<double> collection = this.mCollections[name];
            double maximum = double.NegativeInfinity;

            foreach (double value in collection)
            {
                if (maximum < value)
                {
                    maximum = value;
                }
            }

            return maximum;
        }

        public double GetCollectionMinimum(String name)
        {
            if (!this.mCollections.ContainsKey(name))
            {
                throw new ArgumentException("Unknown parameter name");
            }

            List<double> collection = this.mCollections[name];
            double minimum = double.MaxValue;

            foreach (double value in collection)
            {
                if (minimum > value)
                {
                    minimum = value;
                }
            }

            return minimum;
        }

        public void CaptureSummary(Results summary)
        {

            foreach (KeyValuePair<String, object> value in this.mValues)
            {
                summary.AddValue(value.Key, value.Value);
            }

            foreach (KeyValuePair<String, int> count in this.mCounts)
            {
                summary.AddToCollection(count.Key, count.Value);
            }

            foreach (KeyValuePair<String, List<double>> collection in this.mCollections)
            {
                summary.AddToCollection(collection.Key + " average", this.GetCollectionAverage(collection.Key));
                //summary.AddToCollection(collection.Key + " median", this.GetCollectionMedian(collection.Key));
                summary.AddToCollection(collection.Key + " standard deviation", this.GetCollectionStdDev(collection.Key));
                summary.AddToCollection(collection.Key + " minimum", this.GetCollectionMinimum(collection.Key));
                summary.AddToCollection(collection.Key + " maximum", this.GetCollectionMaximum(collection.Key));
            }
        }

        public void FillKeyList(ICollection<String> keyList)
        {
            foreach (KeyValuePair<String, int> count in this.mCounts)
            {
                keyList.Add(count.Key);
            }

            foreach (KeyValuePair<String, List<double>> collection in this.mCollections)
            {
                keyList.Add(collection.Key);
            }
        }

    }



    #region Unit tests.
#if DEBUG

    [TestFixture]
    public class ResultsTester
    {
        private Results mTestResults;
        private int[] mIntValues = { 0, 2, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 16, 18 };

        public delegate void TimerHandler(String key, int delay);                

        [SetUp]
        public void Initialize()
        {
            Simulator.Instance.Reset();
            this.mTestResults = new Results();
        }

        [TearDown]
        public void Terminate()
        {
            this.mTestResults.Clear();
            this.mTestResults = null;
        }

        public void TimerStart(String key, int delay) 
        {
            this.mTestResults.TimerStart(key);

            // Set the stop event for the given key for 10 seconds in the future.
            Simulator.Instance.Schedule(delay * 1000, this.TimerStop, key, delay);
        }

        public void TimerStop(String key, int delay)
        {
            // Usually name would not be the same as key.
            this.mTestResults.TimerStop(key, key);

            // Set a new start event for the given key for 0 seconds in the future.
            Simulator.Instance.Push(this.TimerStart, key, delay );
        }

        [Test]
        public void HarnessTest()
        {
            this.mTestResults.DumpAll(Console.Out);
            this.mTestResults.Clear();
        }

        [Test]
        public void IncrementTest()
        {
            String paramNameA = "test param A";            
            String paramNameB = "test param B";

            // Test that we can increment a values.
            this.mTestResults.Increment(paramNameA);
            this.mTestResults.Increment(paramNameA);

            // Test that we can get a value.
            Assert.AreEqual(2, this.mTestResults.GetCount(paramNameA));            

            // Test that we can decrement a value.
            this.mTestResults.Decrement(paramNameA);
            Assert.AreEqual(1, this.mTestResults.GetCount(paramNameA));

            // Test that we can have multiple values.
            this.mTestResults.Increment(paramNameB);
            Assert.AreEqual(1, this.mTestResults.GetCount(paramNameB));
        }

        [Test]
        public void DecrementFailTest()
        {
            // Test that decrementing works.
            this.mTestResults.Decrement("test name");
            Assert.AreEqual(-1, this.mTestResults.GetCount("test name"));            
        }

        [Test]
        public void FailCountTest()
        {
            // Test that attempting to get the value of an unknown parameter returns 0.
            Assert.AreEqual(0, this.mTestResults.GetCount("an unknown parameter name"));
        }

        [Test]
        public void ValueTest()
        {
            String name = "name";
            String value = "val";
            String alternateValue = "different val";

            // Test that we can add a value.
            this.mTestResults.AddValue(name, value);

            // Test that we can get a vlaue.
            Assert.AreEqual(value, this.mTestResults.GetValue(name) as String);

            // Test that we can modify a value.
            this.mTestResults.AddValue(name, alternateValue);
            Assert.AreEqual(alternateValue, this.mTestResults.GetValue(name) as String);
        }

        [Test, ExpectedException(typeof(ArgumentException))]
        public void GetValueFailTest()
        {
            // Test that attempting to get an unknown value throws an exception.
            this.mTestResults.GetValue("unknown name");
        }

        [Test]
        public void CollectionTest()
        {            
            String paramName = "ints";

            // Test that we can add an items to a collection.
            foreach (int value in this.mIntValues)
                this.mTestResults.AddToCollection(paramName, value);

            // Test that we can get a collection.
            List<double> collection = this.mTestResults.GetCollection(paramName);
            Assert.AreEqual(this.mIntValues.Length, collection.Count);
            for (int i = 0; i < this.mIntValues.Length; i++)
                Assert.AreEqual(this.mIntValues[i], collection[i]);

            // Test that we can get the total of a collection.
            Assert.AreEqual(this.mIntValues.Length, this.mTestResults.GetCollectionTotal(paramName));
        }

        [Test]
        public void AverageTest()
        {
            double sum = 0;

            foreach (int value in this.mIntValues)
                sum += value;
           
            // Test that we can get the average of a collection.
            this.CollectionTest();
            Assert.AreEqual(sum / this.mIntValues.Length, this.mTestResults.GetCollectionAverage("ints"));
        }

        [Test]
        public void MedianTest()
        {
            // Test that we can get the median of a collection.
            this.CollectionTest();
            Assert.AreEqual(9.5, this.mTestResults.GetCollectionMedian("ints"));

            this.mTestResults.AddToCollection("ints", 20);
            Assert.AreEqual(10, this.mTestResults.GetCollectionMedian("ints"));
        }


        [Test]
        public void StarndardDeviationTest()
        {
            // Test that we can get the standard deviation of a collection.     
            int[] values = { 2, 4, 4, 4, 4, 4, 4, 6 };

            foreach (int val in values)
                this.mTestResults.AddToCollection("stddev", val);

            Assert.AreEqual(4, this.mTestResults.GetCollectionAverage("stddev"));
            Assert.AreEqual(4, this.mTestResults.GetCollectionMedian("stddev"));
            Assert.AreEqual(1, this.mTestResults.GetCollectionStdDev("stddev"));
        }

        // Test that attempting to get each of the aggregates fails for unknown parameters.
        [Test, ExpectedException(typeof(ArgumentException))]
        public void TotalFailTest()
        {
            this.mTestResults.GetCollectionTotal("none");
        }

        [Test, ExpectedException(typeof(ArgumentException))]
        public void AverageFailTest()
        {
            this.mTestResults.GetCollectionAverage("none");
        }

        [Test, ExpectedException(typeof(ArgumentException))]
        public void MedianFailTest()
        {
            this.mTestResults.GetCollectionMedian("none");
        }

        [Test, ExpectedException(typeof(ArgumentException))]
        public void StdDevFailTest()
        {
            this.mTestResults.GetCollectionStdDev("none");
        }

        [Test, ExpectedException(typeof(ArgumentException))]
        public void GetCollectionFailTest()
        {
            this.mTestResults.GetCollection("none");
        }

        [Test]
        public void TimerTest()
        {
            this.TimerStart("one", 10);
            this.TimerStart("two", 25);

            Assert.AreEqual(2, Simulator.Instance.Count);

            // Test that we can record times as a collection in the form of start stop operations.
            Simulator.Instance.RunFor(new TimeSpan(0, 0, 1001));

            Assert.AreEqual(100, this.mTestResults.GetCollectionTotal("one"));
            Assert.AreEqual(10, this.mTestResults.GetCollectionAverage("one"));
            Assert.AreEqual(10, this.mTestResults.GetCollectionMedian("one"));
            Assert.AreEqual(0, this.mTestResults.GetCollectionStdDev("one"));

            Assert.AreEqual(40, this.mTestResults.GetCollectionTotal("two"));
            Assert.AreEqual(25, this.mTestResults.GetCollectionAverage("two"));
            Assert.AreEqual(25, this.mTestResults.GetCollectionMedian("two"));
            Assert.AreEqual(0, this.mTestResults.GetCollectionStdDev("two"));
        }

        [Test, ExpectedException(typeof(InvalidOperationException))]
        public void StartTimerFailTest()
        {
            // Test that attempting to start a timer with the same key fails
            this.mTestResults.TimerStart("key");
            this.mTestResults.TimerStart("key");
        }

        [Test, ExpectedException(typeof(ArgumentException))]
        public void StopTimerFailTest()
        {            
            // Test that calling TimerStop without a matching TimerStart doesn't add to collection.
            this.mTestResults.TimerStop("name", "key");

            this.mTestResults.GetCollection("name");
        }

    }

#endif
    #endregion


}
