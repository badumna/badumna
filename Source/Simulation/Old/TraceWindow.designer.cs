using FormLogger;

namespace NetworkSimulator
{
    partial class TraceWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.traceControl1 = new FormLogger.TraceControl();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.resultPlotterControl1 = new NetworkSimulator.ResultPlotterControl();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.singlePeerViewCheckBox = new System.Windows.Forms.CheckBox();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.renderBroadcastCheck = new System.Windows.Forms.CheckBox();
            this.renderRoutingCheck = new System.Windows.Forms.CheckBox();
            this.renderLeafsetCheck = new System.Windows.Forms.CheckBox();
            this.dhtVisualiser = new CustomControls.RenderControl();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.SuspendLayout();
            // 
            // checkBox1
            // 
            this.checkBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(13, 13);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(119, 17);
            this.checkBox1.TabIndex = 1;
            this.checkBox1.Text = "Suspend Simulation";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Location = new System.Drawing.Point(13, 37);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(854, 901);
            this.tabControl1.TabIndex = 2;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.traceControl1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(918, 875);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Trace";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // traceControl1
            // 
            this.traceControl1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.traceControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.traceControl1.Location = new System.Drawing.Point(3, 3);
            this.traceControl1.Name = "traceControl1";
            this.traceControl1.Size = new System.Drawing.Size(912, 869);
            this.traceControl1.TabIndex = 0;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.label1);
            this.tabPage2.Controls.Add(this.comboBox1);
            this.tabPage2.Controls.Add(this.checkBox2);
            this.tabPage2.Controls.Add(this.resultPlotterControl1);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(918, 875);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Plot";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Y axis";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(48, 6);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(265, 21);
            this.comboBox1.TabIndex = 3;
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(319, 10);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(68, 17);
            this.checkBox2.TabIndex = 2;
            this.checkBox2.Text = "Inclusive";
            this.checkBox2.UseVisualStyleBackColor = true;
            // 
            // resultPlotterControl1
            // 
            this.resultPlotterControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.resultPlotterControl1.Location = new System.Drawing.Point(6, 32);
            this.resultPlotterControl1.Name = "resultPlotterControl1";
            this.resultPlotterControl1.ScrollGrace = 0;
            this.resultPlotterControl1.ScrollMaxX = 0;
            this.resultPlotterControl1.ScrollMaxY = 0;
            this.resultPlotterControl1.ScrollMaxY2 = 0;
            this.resultPlotterControl1.ScrollMinX = 0;
            this.resultPlotterControl1.ScrollMinY = 0;
            this.resultPlotterControl1.ScrollMinY2 = 0;
            this.resultPlotterControl1.Size = new System.Drawing.Size(568, 552);
            this.resultPlotterControl1.TabIndex = 1;
            this.resultPlotterControl1.Load += new System.EventHandler(this.resultPlotterControl1_Load);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.singlePeerViewCheckBox);
            this.tabPage3.Controls.Add(this.comboBox2);
            this.tabPage3.Controls.Add(this.renderBroadcastCheck);
            this.tabPage3.Controls.Add(this.renderRoutingCheck);
            this.tabPage3.Controls.Add(this.renderLeafsetCheck);
            this.tabPage3.Controls.Add(this.dhtVisualiser);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(846, 875);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "DhtVisualiser";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // singlePeerViewCheckBox
            // 
            this.singlePeerViewCheckBox.AutoSize = true;
            this.singlePeerViewCheckBox.Location = new System.Drawing.Point(496, 6);
            this.singlePeerViewCheckBox.Name = "singlePeerViewCheckBox";
            this.singlePeerViewCheckBox.Size = new System.Drawing.Size(104, 17);
            this.singlePeerViewCheckBox.TabIndex = 5;
            this.singlePeerViewCheckBox.Text = "Single peer view";
            this.singlePeerViewCheckBox.UseVisualStyleBackColor = true;
            // 
            // comboBox2
            // 
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(606, 4);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(113, 21);
            this.comboBox2.TabIndex = 4;
            // 
            // renderBroadcastCheck
            // 
            this.renderBroadcastCheck.AutoSize = true;
            this.renderBroadcastCheck.Checked = true;
            this.renderBroadcastCheck.CheckState = System.Windows.Forms.CheckState.Checked;
            this.renderBroadcastCheck.Location = new System.Drawing.Point(234, 4);
            this.renderBroadcastCheck.Name = "renderBroadcastCheck";
            this.renderBroadcastCheck.Size = new System.Drawing.Size(135, 17);
            this.renderBroadcastCheck.TabIndex = 3;
            this.renderBroadcastCheck.Text = "Render broadcast path";
            this.renderBroadcastCheck.UseVisualStyleBackColor = true;
            this.renderBroadcastCheck.CheckedChanged += new System.EventHandler(this.renderBroadcastCheck_CheckedChanged);
            // 
            // renderRoutingCheck
            // 
            this.renderRoutingCheck.AutoSize = true;
            this.renderRoutingCheck.Location = new System.Drawing.Point(105, 4);
            this.renderRoutingCheck.Name = "renderRoutingCheck";
            this.renderRoutingCheck.Size = new System.Drawing.Size(122, 17);
            this.renderRoutingCheck.TabIndex = 2;
            this.renderRoutingCheck.Text = "Render routing table";
            this.renderRoutingCheck.UseVisualStyleBackColor = true;
            this.renderRoutingCheck.CheckedChanged += new System.EventHandler(this.renderRoutingCheck_CheckedChanged);
            // 
            // renderLeafsetCheck
            // 
            this.renderLeafsetCheck.AutoSize = true;
            this.renderLeafsetCheck.Checked = true;
            this.renderLeafsetCheck.CheckState = System.Windows.Forms.CheckState.Checked;
            this.renderLeafsetCheck.Location = new System.Drawing.Point(3, 4);
            this.renderLeafsetCheck.Name = "renderLeafsetCheck";
            this.renderLeafsetCheck.Size = new System.Drawing.Size(95, 17);
            this.renderLeafsetCheck.TabIndex = 1;
            this.renderLeafsetCheck.Text = "Render leafset";
            this.renderLeafsetCheck.UseVisualStyleBackColor = true;
            this.renderLeafsetCheck.CheckedChanged += new System.EventHandler(this.renderLeafsetCheck_CheckedChanged);
            // 
            // dhtVisualiser
            // 
            this.dhtVisualiser.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dhtVisualiser.Location = new System.Drawing.Point(0, 31);
            this.dhtVisualiser.Name = "dhtVisualiser";
            this.dhtVisualiser.Size = new System.Drawing.Size(843, 841);
            this.dhtVisualiser.TabIndex = 0;
            // 
            // TraceWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(879, 950);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.checkBox1);
            this.Name = "TraceWindow";
            this.Text = "TraceWindow";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private TraceControl traceControl1;
        private System.Windows.Forms.TabPage tabPage2;
        private ResultPlotterControl resultPlotterControl1;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.TabPage tabPage3;
        private CustomControls.RenderControl dhtVisualiser;
        private System.Windows.Forms.CheckBox renderLeafsetCheck;
        private System.Windows.Forms.CheckBox renderBroadcastCheck;
        private System.Windows.Forms.CheckBox renderRoutingCheck;
        private System.Windows.Forms.CheckBox singlePeerViewCheckBox;
        private System.Windows.Forms.ComboBox comboBox2;
    }
}