using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

using ZedGraph;

namespace NetworkSimulator
{

    public partial class ResultPlotterControl : ZedGraphControl, ISampler
    {
        private int mNextPointIndex = 1;
        private Dictionary<int, PointPairList> mLines = new Dictionary<int, PointPairList>();
        private ResultSampler mSampler;

        private Dictionary<String, PointPairList> mParameterLines = new Dictionary<string, PointPairList>();

        public ResultPlotterControl()
        {
            InitializeComponent();                    
        }

        public void SampleEventHandler(object sender, EventArgs e)
        {
            this.Refresh();
        }

        public void SetSize()
        {
            this.Location = new Point(10, 10);
            // Leave a small margin around the outside of the control
            this.Size = new Size(this.ClientRectangle.Width - 20, this.ClientRectangle.Height - 20);
        }

        public void Initialize(ResultSampler sampler, String title, String xAxisLabel)
        {
            this.mSampler = sampler;
            EventCallBack callBack = new EventCallBack(this);
            this.mSampler.SampleEvent += callBack.SampleEventHandler;

            GraphPane myPane = this.GraphPane;

            // Set the titles and axis labels
            myPane.Title.Text = title;
            myPane.XAxis.Title.Text = xAxisLabel;

            // Fill the pane background with a color gradient
            myPane.Fill = new Fill(Color.White, Color.FromArgb(220, 220, 255), 45F);

            // Calculate the Axis Scale Ranges
            this.AxisChange();

            this.Disposed += new EventHandler(ResultPlotterControl_Disposed);
        }

        void ResultPlotterControl_Disposed(object sender, EventArgs e)
        {
            this.mSampler.Shutdown();
        }
      

        public void Reset()
        {
            this.mLines.Clear();
            this.mNextPointIndex = 1;
            if (null != this.mSampler)
            {
                this.mSampler.Reset();
            }
            ResultSampler.Clear();
            this.GraphPane.CurveList.Clear();
        }

        public void Clear()
        {
            ResultSampler.Clear();

            if (null != this.GraphPane && null != this.GraphPane.CurveList)
            {
                foreach (CurveItem item in this.GraphPane.CurveList)
                {
                    item.Clear();
                }
            }

            // Calculate the Axis Scale Ranges
            this.AxisChange();
        }

        public void AddResultListener(Results resultInstance, String resultName, bool isCollection)
        {
            if (null == this.mSampler)
            {
                return;
            }

            PointPairList list = new PointPairList();
            PointPairList list2 = new PointPairList();
            PointPairList list3 = new PointPairList();

            if (isCollection)
            {
                this.mSampler.AddGrabber(resultName, resultInstance.GetCollectionAverage);
                this.mSampler.AddGrabber(resultName, resultInstance.GetCollectionStdDev);
                this.mSampler.AddGrabber(resultName, resultInstance.GetCollectionMaximum);

                this.GraphPane.AddCurve(resultName + " average", list, ColorSymbolRotator.StaticNextColor, ColorSymbolRotator.StaticNextSymbol);
                this.GraphPane.AddErrorBar(resultName + " std dev", list2, ColorSymbolRotator.StaticNextColor);
                this.GraphPane.AddCurve(resultName + " maximum", list3, ColorSymbolRotator.StaticNextColor, ColorSymbolRotator.StaticNextSymbol);
            }
            else
            {
                this.mSampler.AddGrabber(resultName, resultInstance.GetCount);
                this.mSampler.AddGrabber(resultName, ResultSampler.Zero);
                this.mSampler.AddGrabber(resultName, ResultSampler.Zero);

                this.GraphPane.AddCurve(resultName, list, ColorSymbolRotator.StaticNextColor, ColorSymbolRotator.StaticNextSymbol);
            }

            this.mLines.Add(this.mNextPointIndex++, list);
            this.mLines.Add(this.mNextPointIndex++, list2);
            this.mLines.Add(this.mNextPointIndex++, list3);

            this.Invoke(new EventHandler(this.RefreshEvent), this, null);
        }

        public void PlotResults(Dictionary<double, Results> resultList)
        {
            foreach (KeyValuePair<String, PointPairList> pair in this.mParameterLines)
            {
                pair.Value.Clear();
                foreach (KeyValuePair<double, Results> samplePoints in resultList)
                {
                    try
                    {
                        pair.Value.Add(samplePoints.Key, samplePoints.Value.GetCollectionAverage(pair.Key));
                    }
                    catch (ArgumentException)
                    {
                        pair.Value.Add(samplePoints.Key, 0.0);
                    }
                }
            }

            // Calculate the Axis Scale Ranges
            this.Update();
            this.AxisChange();
            base.Refresh();
        }

        public void AddParameter(String parameterName)
        {
            GraphPane myPane = this.GraphPane;
            PointPairList pointList = new PointPairList();

            this.mParameterLines.Add(parameterName, pointList);
            myPane.AddCurve(parameterName, pointList, ColorSymbolRotator.StaticNextColor, ColorSymbolRotator.StaticNextSymbol);

            // Calculate the Axis Scale Ranges
            this.AxisChange();
            base.Refresh();
        }

        public void ClearParameters()
        {
            this.mParameterLines.Clear();
        }


        public bool UpdateGraph()
        {
            GraphPane myPane = this.GraphPane;

            ResultSampler.Lock.WaitOne();

            if (0 == ResultSampler.Samples.Count)
            {
                ResultSampler.Lock.ReleaseMutex();
                return false;
            }

            foreach (List<double> sampleList in ResultSampler.Samples)
            {
                double timeInSeconds = sampleList[0];

                for (int y = 1; y < sampleList.Count; y++)
                {
                    if (this.mLines.ContainsKey(y))
                    {
                        switch (y % 3)
                        {
                            case 1:
                                this.mLines[y].Add(timeInSeconds, sampleList[y], 0);
                                break;
                            case 2:
                                if (0.0 != sampleList[y])
                                {
                                    this.mLines[y].Add(timeInSeconds, sampleList[y - 1] + sampleList[y] / 2, sampleList[y - 1] - sampleList[y] / 2);
                                }
                                break;
                            case 0:

                                if (0.0 != sampleList[y])
                                {
                                    this.mLines[y].Add(timeInSeconds, sampleList[y], 0);
                                }
                                break;
                        }
                    }
                }
            }
            ResultSampler.Lock.ReleaseMutex();
            ResultSampler.Clear();

            // Calculate the Axis Scale Ranges
            this.AxisChange();
            this.Update();
            return true;
        }

        public void RefreshEvent(object sender, EventArgs e)
        {
            this.Refresh();
        }

        public override void Refresh()
        {
            if (this.UpdateGraph())
            {
                base.Refresh();
            }
        }

    }


    class EventCallBack
    {
        ResultPlotterControl mPlotterControl;

        public void SampleEventHandler(object sender, EventArgs e)
        {
            this.mPlotterControl.Invoke(new EventHandler(this.mPlotterControl.SampleEventHandler), sender, e);
        }

        public EventCallBack(ResultPlotterControl plotterControl)
        {
            this.mPlotterControl = plotterControl;
        }
    }

}
