using System;
using System.Collections.Generic;
using System.Text;

namespace NetworkSimulator
{
    public interface ISampler
    {
        void Initialize(ResultSampler sampler, String title, String xAxisLAbel);
        void AddResultListener(Results resultInstance, String resultName, bool isCollection);
        void Clear();
    }
   
}
