using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using Badumna.Core;
using Badumna.Utilities;

using CustomControls;

namespace NetworkSimulator
{
    public partial class TraceWindow : Form, ITracer, ISampler
    {
        public event EventHandler Exit;

        private ISimulation mSimulation;
        public ISimulation Simulation
        {
            get { return this.mSimulation; }
            set { this.mSimulation = value; }
        }

        public TraceWindow()
        {
            InitializeComponent();

            DotNetTraceLogger logger = Logger.Log as DotNetTraceLogger;
            this.traceControl1.Initialize(logger != null ? logger.Source : null);
            this.resultPlotterControl1.Initialize(new ResultSampler(60.0), "Simulation Results", "Time (Seconds)");

            this.checkBox1.CheckedChanged += new EventHandler(checkBox1_CheckedChanged);
            this.comboBox1.SelectedIndexChanged += new EventHandler(comboBox1_SelectedIndexChanged);
            this.comboBox1.MouseDown += new MouseEventHandler(comboBox1_MouseDown);
            this.dhtVisualiser.RenderEvent += new EventHandler<RenderEventArgs>(dhtVisualiser_RenderEvent);

            this.mRenderOptions.Add("dht radius", 20.0f);
            this.mRenderOptions.Add("render node", null);

            this.renderBroadcastCheck_CheckedChanged(this, null);
            this.renderLeafsetCheck_CheckedChanged(this, null);
            this.renderRoutingCheck_CheckedChanged(this, null);

            Application.Idle += new EventHandler(Application_Idle);
            this.singlePeerViewCheckBox.CheckedChanged += new EventHandler(singlePeerViewCheckBox_CheckedChanged);
            this.comboBox2.SelectedValueChanged += new EventHandler(comboBox2_SelectedValueChanged);
        }

        void comboBox2_SelectedValueChanged(object sender, EventArgs e)
        {
            this.singlePeerViewCheckBox_CheckedChanged(sender, e);
        }

        void singlePeerViewCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (singlePeerViewCheckBox.Checked && null != this.comboBox2.SelectedItem)
            {
                if (this.mRenderOptions.ContainsKey("single peer"))
                {
                    this.mRenderOptions["single peer"] = this.comboBox2.SelectedItem;
                }
                else
                {
                    this.mRenderOptions.Add("single peer", this.comboBox2.SelectedItem);
                }
            }
            else
            {
                if (this.mRenderOptions.ContainsKey("single peer"))
                {
                    this.mRenderOptions.Remove("single peer");
                }
            }
        }

        private DateTime mLastRender = DateTime.Now;
        void Application_Idle(object sender, EventArgs e)
        {
            if (null != this.mSimulation)
            {
                TimeSpan span = DateTime.Now - this.mLastRender;

                if (span.TotalMilliseconds > 100)
                {
                    this.mLastRender = DateTime.Now;
                    this.dhtVisualiser.Invalidate();
                }                
            }
        }

        private Dictionary<String, object> mRenderOptions = new Dictionary<string, object>();
        void dhtVisualiser_RenderEvent(object sender, RenderEventArgs e)
        {
            if (null != this.mSimulation)
            {
                e.Context.DrawString(String.Format("{0}", Simulator.Instance.Time.TotalSeconds),
                    new Font("Arial", 10f), Brushes.Black, -e.Radius - 50, -e.Radius - 50);

                Pen pen = new Pen(Color.Gray, 2.0f);
                float radius = e.Radius;
                e.Context.DrawEllipse(pen, -radius, -radius, radius * 2, radius * 2);

                this.mRenderOptions["dht radius"] = e.Radius;
                this.mSimulation.Render(e.Context, this.mRenderOptions);                
            }
        }

        public void RecordNewNode(object sender, NodeInitializationEventArgs e)
        {
            this.comboBox2.Invoke(new GenericCallBack<String>(delegate(String peer){ this.comboBox2.Items.Add(peer); }), e.SimulatorFacade.Address.ToString());
        }

        void comboBox1_MouseDown(object sender, MouseEventArgs e)
        {
            this.SetYAxisOptions();
        }

        void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.SelectParameterToPlot(comboBox1.SelectedItem as String);
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            if (null != this.Exit)
            {
                this.Exit(this, e);
            }
        }

        void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            Simulator.Instance.Suspend = this.checkBox1.CheckState.CompareTo(CheckState.Checked) == 0;
        }

        public void SetYAxisOptions()
        {
            List<String> keyNames = new List<string>();

            Results.Instance.FillKeyList(keyNames);

            this.comboBox1.Items.Clear();
            foreach (String key in keyNames)
            {
                this.comboBox1.Items.Add(key);
            }
        }

        public void ResetTrace()
        {
            this.traceControl1.Reset();
        }

        public String Description
        {
            set { this.traceControl1.Description = value; }
        }

        public void Initialize(ResultSampler sampler, String title, String xAxisLabel)
        {
            this.resultPlotterControl1.Initialize(sampler, title, xAxisLabel);
        }

        public void Clear()
        {
            this.comboBox2.Invoke(new GenericCallBack(delegate() { this.comboBox2.Items.Clear(); }));
            this.resultPlotterControl1.Clear();
        }

        public void AddResultListener(Results resultInstance, string resultName, bool isCollection)
        {
            this.resultPlotterControl1.AddResultListener(resultInstance, resultName, isCollection);
        }

        private void resultPlotterControl1_Load(object sender, EventArgs e)
        {

        }

        private void SelectParameterToPlot(String parameter)
        {
            if (Results.Instance.IsCollection(parameter))
            {
                if (!this.checkBox2.Checked)
                {
                    this.resultPlotterControl1.Reset();
                }
                this.resultPlotterControl1.AddResultListener(Results.Instance, parameter, true);
            }

            if (Results.Instance.IsCount(parameter))
            {
                if (!this.checkBox2.Checked)
                {
                    this.resultPlotterControl1.Reset();
                }
                this.resultPlotterControl1.AddResultListener(Results.Instance, parameter, false);
            }
        }

        private void renderLeafsetCheck_CheckedChanged(object sender, EventArgs e)
        {
            if (this.renderLeafsetCheck.Checked)
            {
                if (!this.mRenderOptions.ContainsKey("render leafset"))
                {
                    this.mRenderOptions.Add("render leafset", null);
                    this.dhtVisualiser.Invalidate();
                }
            }
            else
            {
                if (this.mRenderOptions.ContainsKey("render leafset"))
                {
                    this.mRenderOptions.Remove("render leafset");
                    this.dhtVisualiser.Invalidate();
                }
            }
        }

        private void renderRoutingCheck_CheckedChanged(object sender, EventArgs e)
        {
            if (this.renderRoutingCheck.Checked)
            {
                if (!this.mRenderOptions.ContainsKey("render routing table"))
                {
                    this.mRenderOptions.Add("render routing table", null);
                    this.dhtVisualiser.Invalidate();
                }
            }
            else
            {
                if (this.mRenderOptions.ContainsKey("render routing table"))
                {
                    this.mRenderOptions.Remove("render routing table");
                    this.dhtVisualiser.Invalidate();
                }
            }
        }

        private void renderBroadcastCheck_CheckedChanged(object sender, EventArgs e)
        {
            if (this.renderBroadcastCheck.Checked)
            {
                if (!this.mRenderOptions.ContainsKey("render broadcast path"))
                {
                    this.mRenderOptions.Add("render broadcast path", null);
                    this.dhtVisualiser.Invalidate();
                }
            }
            else
            {
                if (this.mRenderOptions.ContainsKey("render broadcast path"))
                {
                    this.mRenderOptions.Remove("render broadcast path");
                    this.dhtVisualiser.Invalidate();
                }
            }
        }
    }
}