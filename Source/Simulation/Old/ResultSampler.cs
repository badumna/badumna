using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

using Badumna.Core;
using Badumna.Utilities;

#if DEBUG
using NUnit.Framework;
#endif

namespace NetworkSimulator
{
    
    public delegate double SampleGrabber(String name);

    public class ResultSampler 
    {
        private static List<List<double>> mSamples = new List<List<double>>();
        public static List<List<double>> Samples { get { return ResultSampler.mSamples; } }

        private List<KeyValuePair<String, SampleGrabber>> mGrabbers = new List<KeyValuePair<string, SampleGrabber>>();
        private TimeSpan mDelay = TimeSpan.FromSeconds(1.0);

        private static Mutex mLock = new Mutex();
        public static Mutex Lock { get { return ResultSampler.mLock; } }        

        public event EventHandler SampleEvent;

        private bool mClosed;

        public ResultSampler(double delay)
        {
            ResultSampler.mLock.WaitOne();
            this.mDelay = TimeSpan.FromSeconds(delay);

            Simulator.Instance.HasBeenReset += new EventHandler(Instance_HasBeenReset);
            ResultSampler.mLock.ReleaseMutex();

            Simulator.Instance.Push(this.Sample);
        }

        public ResultSampler()
        {

        }

        public void Shutdown()
        {
            this.Reset();
            this.mClosed = true;
        }

        void Instance_HasBeenReset(object sender, EventArgs e)
        {
            if (!this.mClosed)
            {
                Simulator.Instance.Push(this.Sample);
            }
        }

        public static void Clear()
        {
            ResultSampler.mLock.WaitOne();
            ResultSampler.mSamples.Clear();
            ResultSampler.mLock.ReleaseMutex();
        }       

        public static double Zero(String name)
        {
            return 0.0;
        }

        public void Reset()
        {
            ResultSampler.mLock.WaitOne();
            this.mGrabbers.Clear();
            ResultSampler.mLock.ReleaseMutex();
        }

        public void ClearResultsForEachGrabber()
        {
            ResultSampler.mLock.WaitOne();
            foreach (KeyValuePair<String, SampleGrabber> grabber in this.mGrabbers)
            {
                Results.Instance.Clear(grabber.Key);
            }
            ResultSampler.mLock.ReleaseMutex();
        }

        public void AddGrabber(String name, SampleGrabber grabber)
        {
            ResultSampler.mLock.WaitOne();
            this.mGrabbers.Add(new KeyValuePair<string, SampleGrabber>(name, grabber));
            ResultSampler.mLock.ReleaseMutex();
        }

        protected virtual double Xaxis()
        {
            return Simulator.Instance.Time.TotalSeconds;
        }

        public void Sample()
        {
            if (this.mClosed)
            {
                return;
            }

            // Re-push next sample event.            
            Simulator.Instance.Schedule((int)this.mDelay.TotalMilliseconds, this.Sample);

            ResultSampler.mLock.WaitOne();
            double xAxis = this.Xaxis();
            List<double> sample = new List<double>();

            //System.Console.WriteLine("Sample @ " + elapsedSeconds);

            sample.Add(xAxis);

            foreach (KeyValuePair<String, SampleGrabber> grabberPair in this.mGrabbers)
            {
                try
                {
                    sample.Add(grabberPair.Value.Invoke(grabberPair.Key));
                }
                catch (Exception)
                {
                    sample.Add(0);
                }
            }

            ResultSampler.mSamples.Add(sample);

            ResultSampler.mLock.ReleaseMutex();
                
            if (null != this.SampleEvent)
            {
                this.SampleEvent.Invoke(this, null);
            }
        }
    }




    #region Unit tests.
#if DEBUG

    [TestFixture]
    public class ResultSamplerTester
    {
        private ResultSampler mSampler;

        [SetUp]
        public void Initialize()
        {
            Simulator.Instance.Reset();
            Results.Instance.Clear();
            ResultSampler.Clear();

            this.mSampler = new ResultSampler(5.0);

            Simulator.Instance.Push(this.IncrementValue);
        }

        [TearDown]
        public void Terminate()
        {
            this.mSampler.Shutdown();
        }

        public void IncrementValue()
        {
            Results.Instance.Increment("counter");

            int delayMs = (int)TimeSpan.FromSeconds(2).TotalMilliseconds;
            Simulator.Instance.Schedule(delayMs, this.IncrementValue);
        }

        [Test]
        public void HarnessTest()
        {
        }

        [Test]
        public void AddGrabberTest()
        {
            // Test that we can add a grabber to the sampler.
            this.mSampler.AddGrabber("counter", Results.Instance.GetCount);

            // Test that grabber will collect the correct sample.
            Simulator.Instance.RunFor(TimeSpan.FromSeconds(99));

            Assert.AreEqual(20, ResultSampler.Samples.Count);

            double nextSampleTime = 0;
            foreach (List<double> sample in ResultSampler.Samples)
            {
                Assert.AreEqual(2, sample.Count);
                Assert.AreEqual(nextSampleTime, sample[0]);
                Assert.AreEqual(Math.Ceiling(sample[0] / 2), sample[1]);

                nextSampleTime += 5.0;
            }
        }

        [Test]
        public void FailedParamTest()
        {
            this.mSampler.AddGrabber("none", Results.Instance.GetCollectionStdDev);

            Simulator.Instance.RunFor(TimeSpan.FromSeconds(99));
            Assert.AreEqual(20, ResultSampler.Samples.Count);

            double nextSampleTime = 0;
            foreach (List<double> sample in ResultSampler.Samples)
            {
                Assert.AreEqual(2, sample.Count);
                Assert.AreEqual(nextSampleTime, sample[0]);
                Assert.AreEqual(0, sample[1]);

                nextSampleTime += 5.0;
            }
        }
    }

#endif
    #endregion



}
