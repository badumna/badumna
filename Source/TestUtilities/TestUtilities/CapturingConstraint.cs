﻿//-----------------------------------------------------------------------
// <copyright file="CapturingConstraint.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace TestUtilities
{
    using Rhino.Mocks.Constraints;

    /// <summary>
    /// Rhino Mocks constraint for simply capturing an argument of a given type.
    /// </summary>
    /// <typeparam name="T">The type of the argument to catch.</typeparam>
    public class CapturingConstraint<T> : AbstractConstraint where T : class
    {
        /// <summary>
        /// The passed argument.
        /// </summary>
        private T argument;

        /// <summary>
        /// Gets the captured argument.
        /// </summary>
        public T Argument
        {
            get { return this.argument; }
        }

        /// <inheritdoc/>
        public override string Message
        {
            get { return "Argument must be compatible with type " + typeof(T).ToString(); }
        }

        /// <summary>
        /// Determines if the object passes the constraint of type compatibility.
        /// </summary>
        /// <param name="obj">The passed argument.</param>
        /// <returns><c>true</c> if the argument is of compatible type, otherwise <c>false</c></returns>
        public override bool Eval(object obj)
        {
            this.argument = obj as T;
            return this.argument != null;
        }
    }
}
