def postmark_settings_for(host):
	server = "smtp.postmarkapp.com"
	settings = {
		'postmark_server': server
	}
	key = None
	# print repr(host)
	if host.endswith(".badumna.com"):
		# use cloud server settings
		key = "8eacfce5-86d0-459b-aed2-2469f9a69c6d"
	elif host.startswith('badumna-monitor'):
		# monitoring has its own key
		key = "43a22e11-681b-48b0-a504-ae68c0e5f0c9"

	if key:
		settings['postmark_key'] = key
		return settings
	else:
		return None

