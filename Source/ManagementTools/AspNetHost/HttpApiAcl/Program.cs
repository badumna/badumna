﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;
using AspNetHost;

namespace HttpApiAcl
{
    static class Program
    {
        // e.g. The following command will give Everyone permission to listen on the http://+:21257/ prefix:
        // HttpApiAcl add http://+:21257/ D:(A;;GX;;;WD)


        enum Error
        {
            NoError = 0,
            GeneralFailure = 31,
            InvalidParameter = 87,
        }

        static int Main(string[] args)
        {
            if (args.Length < 1)
            {
                return (int)Error.InvalidParameter;
            }

            for (int i = 0; i < args.Length; i++)
            {
                args[i] = TrimArg(args[i]);
            }

            switch (args[0])
            {
                case "add":
                    if (args.Length != 3)
                    {
                        return (int)Error.InvalidParameter;
                    }

                    return Invoke(h => h.SetUrlAcl(args[1], args[2]));

                case "delete":
                    if (args.Length != 2)
                    {
                        return (int)Error.InvalidParameter;
                    }

                    return Invoke(h => h.DeleteUrlAcl(args[1]));
            }

            return (int)Error.InvalidParameter;
        }

        static int Invoke(Action<HttpApi> action)
        {
            try
            {
                using (HttpApi httpApi = new HttpApi())
                {
                    action(httpApi);
                }
            }
            catch (Win32Exception e)
            {
                return e.ErrorCode;
            }
            catch
            {
                return (int)Error.GeneralFailure;
            }

            return (int)Error.NoError;
        }

        static string TrimArg(string arg)
        {
            if (arg.Length >= 2 && arg[0] == '"' && arg[arg.Length - 1] == '"')
            {
                return arg.Trim(new char[] { '"' });
            }

            return arg;
        }
    }
}
