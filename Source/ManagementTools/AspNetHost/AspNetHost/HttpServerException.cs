﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AspNetHost
{
    public class HttpServerException : Exception
    {
        public HttpServerException()
        {
        }

        public HttpServerException(string message)
            : base(message)
        {
        }
    }

    public class PortInUseException : HttpServerException
    {
    }

    public class AccessDeniedException : HttpServerException
    {
    }
}
