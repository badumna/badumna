﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Hosting;
using System.IO;

namespace AspNetHost
{
    public class AspHost : MarshalByRefObject
    {
        private string VirtualDir { get; set; }
        private string PhysicalDir { get; set; }

        public static AspHost Create(string virtualDir, string physicalDir)
        {
            AspHost host = (AspHost)ApplicationHost.CreateApplicationHost(typeof(AspHost), virtualDir, physicalDir);
            host.VirtualDir = virtualDir;
            host.PhysicalDir = physicalDir;
            return host;
        }

        public void ProcessRequest(MarshalableListenerContext marshalableContext)
        {
            HttpRuntime.ProcessRequest(new AspNetHostWorkerRequest(marshalableContext, this.VirtualDir, this.PhysicalDir));
        }

        public override object InitializeLifetimeService()
        {
            return null;  // I want to live FOREVER!
        }
    }
}
