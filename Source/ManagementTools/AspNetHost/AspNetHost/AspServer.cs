﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;

namespace AspNetHost
{
    public class AspServer
    {
        private HttpServer mHttpServer;
        private AspHost mAspHost;


        // TODO: Combine prefix and virtualDir into one parameter if possible
        public AspServer(string prefix, string virtualDir, string physicalDir)
        {
            this.mHttpServer = new HttpServer(prefix, this.HandleRequest);
            this.mAspHost = AspHost.Create(virtualDir, physicalDir);
        }

        private void HandleRequest(HttpListenerContext context)
        {
            MarshalableListenerContext marshableContext = new MarshalableListenerContext(context);
            this.mAspHost.ProcessRequest(marshableContext);

            GC.KeepAlive(marshableContext);
        }

        public void Start()
        {
            this.mHttpServer.Start();
        }

        public void Stop()
        {
            this.mHttpServer.Stop();
        }
    }
}
