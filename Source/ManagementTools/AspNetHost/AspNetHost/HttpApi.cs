﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.ComponentModel;
using System.Diagnostics;

namespace AspNetHost
{
    public partial class HttpApi : IDisposable
    {
        public struct PrefixAcl
        {
            public string Prefix;
            public string Acl;

            public PrefixAcl(string prefix, string acl)
            {
                this.Prefix = prefix;
                this.Acl = acl;
            }

            public override string ToString()
            {
                return this.Prefix + " => " + this.Acl;
            }
        }


        public HttpApi()
        {
            this.HttpInitialize(InitializeFlags.Config);
        }

        public void Dispose()
        {
            this.HttpTerminate(InitializeFlags.Config);
        }

        private void HttpInitialize(InitializeFlags flags)
        {
            Error result = HttpInitialize(HTTPAPI_VERSION_1, flags, IntPtr.Zero);
            if (result != Error.NoError)
            {
                throw new Win32Exception((int)result);
            }
        }

        private void HttpTerminate(InitializeFlags flags)
        {
            Error result = HttpTerminate(flags, IntPtr.Zero);
            if (result != Error.NoError)
            {
                throw new Win32Exception((int)result);
            }
        }

        public void SetUrlAcl(string prefix, string acl)
        {
            HTTP_SERVICE_CONFIG_URLACL_SET set = new HTTP_SERVICE_CONFIG_URLACL_SET();
            set.KeyDesc.pUrlPrefix = prefix;
            set.ParamDesc.pStringSecurityDescriptor = acl;

            Error result = HttpSetUrlAclConfiguration(IntPtr.Zero, HTTP_SERVICE_CONFIG_ID.HttpServiceConfigUrlAclInfo,
                ref set, Marshal.SizeOf(set), IntPtr.Zero);

            if (result != Error.NoError)
            {
                throw new Win32Exception((int)result);
            }
        }

        public void DeleteUrlAcl(string prefix)
        {
            HTTP_SERVICE_CONFIG_URLACL_SET set = new HTTP_SERVICE_CONFIG_URLACL_SET();
            set.KeyDesc.pUrlPrefix = prefix;

            Error result = HttpDeleteUrlAclConfiguration(IntPtr.Zero, HTTP_SERVICE_CONFIG_ID.HttpServiceConfigUrlAclInfo,
                ref set, Marshal.SizeOf(set), IntPtr.Zero);

            if (result != Error.NoError)
            {
                throw new Win32Exception((int)result);
            }
        }

        public IEnumerable<PrefixAcl> GetUrlAcls()
        {
            HTTP_SERVICE_CONFIG_URLACL_QUERY query = new HTTP_SERVICE_CONFIG_URLACL_QUERY();
            query.QueryDesc = HTTP_SERVICE_CONFIG_QUERY_TYPE.HttpServiceConfigQueryNext;
            query.dwToken = 0;

            while (true)
            {
                HTTP_SERVICE_CONFIG_URLACL_SET set;
                Error result = this.QueryUrlAcl(query, out set);
                switch (result)
                {
                    case Error.NoMoreItems:
                        yield break;

                    case Error.NoError:
                        yield return new PrefixAcl(set.KeyDesc.pUrlPrefix, set.ParamDesc.pStringSecurityDescriptor);
                        query.dwToken++;
                        continue;

                    default:
                        throw new Win32Exception((int)result);
                }
            }
        }

        public string QueryUrlAcl(string prefix)
        {
            HTTP_SERVICE_CONFIG_URLACL_QUERY query = new HTTP_SERVICE_CONFIG_URLACL_QUERY();
            query.QueryDesc = HTTP_SERVICE_CONFIG_QUERY_TYPE.HttpServiceConfigQueryExact;
            query.KeyDesc = new HTTP_SERVICE_CONFIG_URLACL_KEY(prefix);

            HTTP_SERVICE_CONFIG_URLACL_SET set;
            Error result = this.QueryUrlAcl(query, out set);

            if (result != Error.NoError)
            {
                throw new Win32Exception((int)result);
            }

            return set.ParamDesc.pStringSecurityDescriptor;
        }

        private Error QueryUrlAcl(HTTP_SERVICE_CONFIG_URLACL_QUERY query, out HTTP_SERVICE_CONFIG_URLACL_SET set)
        {
            int outputSize = 256;

            IntPtr pOutput = Marshal.AllocCoTaskMem(outputSize);
            try
            {
                Error result = HttpQueryUrlAclConfiguration(IntPtr.Zero, HTTP_SERVICE_CONFIG_ID.HttpServiceConfigUrlAclInfo,
                    ref query, Marshal.SizeOf(query), pOutput, outputSize, out outputSize, IntPtr.Zero);

                if (result == Error.InsufficientBuffer)
                {
                    Marshal.FreeCoTaskMem(pOutput);
                    pOutput = Marshal.AllocCoTaskMem(outputSize);
                    result = HttpQueryUrlAclConfiguration(IntPtr.Zero, HTTP_SERVICE_CONFIG_ID.HttpServiceConfigUrlAclInfo,
                        ref query, Marshal.SizeOf(query), pOutput, outputSize, out outputSize, IntPtr.Zero);
                }

                if (result == Error.NoError)
                {
                    set = (HTTP_SERVICE_CONFIG_URLACL_SET)Marshal.PtrToStructure(pOutput, typeof(HTTP_SERVICE_CONFIG_URLACL_SET));
                }
                else
                {
                    set = new HTTP_SERVICE_CONFIG_URLACL_SET();
                }

                return result;
            }
            finally
            {
                Marshal.FreeCoTaskMem(pOutput);
            }
        }


#if DEBUG
        public static void DumpAcls()
        {
            using (HttpApi httpApi = new HttpApi())
            {
                foreach (HttpApi.PrefixAcl acl in httpApi.GetUrlAcls())
                {
                    Debug.WriteLine(acl);
                }
            }
        }
#endif
    }
}
