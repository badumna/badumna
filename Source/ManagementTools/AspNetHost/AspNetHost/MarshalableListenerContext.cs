﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;

namespace AspNetHost
{
    public class MarshalableListenerContext : MarshalByRefObject
    {
        public Stream InputStream { get { return this.mContext.Request.InputStream; } }
        public Stream OutputStream { get { return this.mContext.Response.OutputStream; } }
        
        public string HttpMethod { get { return this.mContext.Request.HttpMethod; } }
        
        public string HttpVersion
        {
            get
            {
                return String.Format("HTTP/{0}.{1}",
                    this.mContext.Request.ProtocolVersion.Major,
                    this.mContext.Request.ProtocolVersion.Minor);
            }
        }

        public string LocalAddress { get { return this.mContext.Request.LocalEndPoint.Address.ToString(); } }
        public int LocalPort { get { return this.mContext.Request.LocalEndPoint.Port; } }
        
        public string RemoteAddress { get { return this.mContext.Request.RemoteEndPoint.Address.ToString(); } }
        public int RemotePort { get { return this.mContext.Request.RemoteEndPoint.Port; } }

        public string LocalPath { get { return this.mContext.Request.Url.LocalPath; } }
        public string Query { get { return this.mContext.Request.Url.Query.Replace("?",""); } }
        public string RawUrl { get { return this.mContext.Request.RawUrl; } }

        
        private HttpListenerContext mContext;


        public MarshalableListenerContext(HttpListenerContext context)
        {
            this.mContext = context;
        }

        public void CloseResponse()
        {
            this.mContext.Response.Close();
        }

        public string GetHeader(string name)
        {
            return this.mContext.Request.Headers[name];
        }

        public void SetHeader(string name, string value)
        {
            if (name.Equals("Transfer-Encoding"))
            {
                throw new Exception("Try to set Transfer-Encoding header directly, this is not allowed in .net framework 4.0");
            }

            this.mContext.Response.Headers[name] = value;
        }

        public void SetResponseStatus(int statusCode, string statusDescription)
        {
            this.mContext.Response.StatusCode = statusCode;
            this.mContext.Response.StatusDescription = statusDescription;
        }
    }
}
