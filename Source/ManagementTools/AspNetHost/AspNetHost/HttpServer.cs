﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Hosting;
using System.IO;
using System.Net;
using System.Threading;
using System.Diagnostics;

namespace AspNetHost
{
    public class HttpServer
    {
        public enum HttpListenerError
        {
            AccessDenied = 5,
            PortInUse = 32,
            PrefixConflict = 183,
            IoAborted = 995,
        }


        private const int ConcurrentRequestLimit = 3;
        private static readonly TimeSpan StopCheckPeriod = TimeSpan.FromMilliseconds(500);
        private static readonly TimeSpan StoppingTimeout = TimeSpan.FromSeconds(2);


        private HttpListener mListener;
        private volatile bool mShouldStop;
        private bool mStarted;
        private ManualResetEvent mStopped = new ManualResetEvent(false);

        private Semaphore mCurrentRequests = new Semaphore(HttpServer.ConcurrentRequestLimit, HttpServer.ConcurrentRequestLimit);

        private Action<HttpListenerContext> mRequestHandler;


        public HttpServer(string prefix, Action<HttpListenerContext> requestHandler)
        {
            if (!HttpListener.IsSupported)
            {
                throw new NotSupportedException("HttpListener not supported");
            }

            this.mRequestHandler = requestHandler;

            this.mListener = new HttpListener();
            this.mListener.Prefixes.Add(prefix);

            try
            {
                this.mListener.Start();
            }
            catch (HttpListenerException e)
            {
                switch ((HttpListenerError)e.ErrorCode)
                {
                    case HttpListenerError.AccessDenied:
                        throw new AccessDeniedException();

                    case HttpListenerError.PrefixConflict:
                    case HttpListenerError.PortInUse:
                        throw new PortInUseException();

                    default:
                        throw;
                }
            }
        }

        public void Start()
        {
            if (this.mListener == null)
            {
                throw new InvalidOperationException();
            }

            if (this.mStarted)
            {
                return;
            }

            new Thread(this.Loop).Start();
            this.mStarted = true;
        }

        public void Stop()
        {
            if (!this.mStarted)
            {
                return;
            }

            this.mShouldStop = true;
            this.mListener.Close();

            bool succeeded = this.mStopped.WaitOne(StoppingTimeout, false);  // TODO: Do something if it fails.

            this.mListener = null;
            this.mStarted = false;
        }

        private void Loop()
        {
            try
            {
                while (!this.mShouldStop)
                {
                    // TODO: Some way of killing run-away requests?  At the moment we can probably get DOSed
                    //       by someone just keeping ConcurrentRequestLimit number of requests open all the time.

                    if (!this.mCurrentRequests.WaitOne(HttpServer.StopCheckPeriod, false))
                    {
                        continue;  // Check stop condition
                    }

                    HttpListenerContext context = this.mListener.GetContext();

                    bool succeeded = false;
                    try
                    {
                        succeeded = ThreadPool.QueueUserWorkItem(this.HandleRequest, context);
                    }
                    catch (NotSupportedException)
                    {
                    }

                    if (!succeeded)
                    {
                        context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                        context.Response.StatusDescription = "Queue work item failed";
                        this.mCurrentRequests.Release();
                    }
                }
            }
            catch (Exception e)
            {
                if (e is HttpListenerException && 
                    ((HttpListenerException)e).ErrorCode == (int)HttpListenerError.IoAborted)
                {
                    // normal exit
                }
                else
                {
                    Debug.WriteLine(String.Format("{0}: {1}", e.GetType().Name, e.Message));
                }
            }

            this.mStopped.Set();
        }


        private void HandleRequest(object state)
        {
            try
            {
                HttpListenerContext context = (HttpListenerContext)state;

                this.mRequestHandler(context);

                context.Response.Close();
            }
            catch
            {
                // Prevent exceptions from taking down the whole system
            }
            finally
            {
                this.mCurrentRequests.Release();
            }
        }
    }
}
