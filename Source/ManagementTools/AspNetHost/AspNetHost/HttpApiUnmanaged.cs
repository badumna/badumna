﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace AspNetHost
{
    partial class HttpApi
    {
        enum Error : uint
        {
            NoError = 0,
            InsufficientBuffer = 122,
            NoMoreItems = 259,
        }

        private static readonly HTTPAPI_VERSION HTTPAPI_VERSION_1 = new HTTPAPI_VERSION(1, 0);
        private static readonly HTTPAPI_VERSION HTTPAPI_VERSION_2 = new HTTPAPI_VERSION(2, 0);

        [StructLayout(LayoutKind.Sequential, Pack = 2)]
        struct HTTPAPI_VERSION
        {
            public ushort HttpApiMajorVersion;
            public ushort HttpApiMinorVersion;

            public HTTPAPI_VERSION(ushort majorVersion, ushort minorVersion)
            {
                HttpApiMajorVersion = majorVersion;
                HttpApiMinorVersion = minorVersion;
            }
        }

        [Flags]
        enum InitializeFlags : uint
        {
            Server = 1,
            Config = 2,
        }

        [DllImport("httpapi.dll")]
        static extern Error HttpInitialize(HTTPAPI_VERSION Version, InitializeFlags Flags, IntPtr pReserved);

        [DllImport("httpapi.dll")]
        static extern Error HttpTerminate(InitializeFlags Flags, IntPtr pReserved);



        enum HTTP_SERVICE_CONFIG_ID
        {
            HttpServiceConfigIPListenList = 0,
            HttpServiceConfigSSLCertInfo,
            HttpServiceConfigUrlAclInfo,
            HttpServiceConfigMax
        }

        enum HTTP_SERVICE_CONFIG_QUERY_TYPE
        {
            HttpServiceConfigQueryExact = 0,
            HttpServiceConfigQueryNext,
            HttpServiceConfigQueryMax
        }



        struct HTTP_SERVICE_CONFIG_URLACL_QUERY
        {
            public HTTP_SERVICE_CONFIG_QUERY_TYPE QueryDesc;
            public HTTP_SERVICE_CONFIG_URLACL_KEY KeyDesc;
            public uint dwToken;
        }

        struct HTTP_SERVICE_CONFIG_URLACL_SET
        {
            public HTTP_SERVICE_CONFIG_URLACL_KEY KeyDesc;
            public HTTP_SERVICE_CONFIG_URLACL_PARAM ParamDesc;
        }

        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
        struct HTTP_SERVICE_CONFIG_URLACL_KEY
        {
            [MarshalAs(UnmanagedType.LPWStr)]
            public string pUrlPrefix;

            public HTTP_SERVICE_CONFIG_URLACL_KEY(string urlPrefix)
            {
                pUrlPrefix = urlPrefix;
            }
        }

        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
        struct HTTP_SERVICE_CONFIG_URLACL_PARAM
        {
            [MarshalAs(UnmanagedType.LPWStr)]
            public string pStringSecurityDescriptor;

            public HTTP_SERVICE_CONFIG_URLACL_PARAM(string securityDescriptor)
            {
                pStringSecurityDescriptor = securityDescriptor;
            }
        }


        [DllImport("httpapi.dll", EntryPoint = "HttpQueryServiceConfiguration")]
        static extern Error HttpQueryUrlAclConfiguration(
            IntPtr ServiceIntPtr,
            HTTP_SERVICE_CONFIG_ID ConfigId,
            ref HTTP_SERVICE_CONFIG_URLACL_QUERY pInputConfigInfo,
            int InputConfigInfoLength,
            IntPtr pOutputConfigInfo,
            int OutputConfigInfoLength,
            out int pReturnLength,
            IntPtr pOverlapped);

        [DllImport("httpapi.dll", EntryPoint = "HttpSetServiceConfiguration")]
        static extern Error HttpSetUrlAclConfiguration(
             IntPtr ServiceIntPtr,
             HTTP_SERVICE_CONFIG_ID ConfigId,
             ref HTTP_SERVICE_CONFIG_URLACL_SET pConfigInformation,
             int ConfigInformationLength,
             IntPtr pOverlapped);

        [DllImport("httpapi.dll", EntryPoint = "HttpDeleteServiceConfiguration")]
        static extern Error HttpDeleteUrlAclConfiguration(
             IntPtr ServiceIntPtr,
             HTTP_SERVICE_CONFIG_ID ConfigId,
             ref HTTP_SERVICE_CONFIG_URLACL_SET pConfigInformation,
             int ConfigInformationLength,
             IntPtr pOverlapped);



        ////struct HTTP_SERVICE_CONFIG_IP_LISTEN_QUERY
        ////{
        ////    public int AddrCount;
        ////    public IntPtr AddrList;
        ////}

        ////struct HTTP_SERVICE_CONFIG_IP_LISTEN_PARAM
        ////{
        ////    public ushort AddrLength;
        ////    public IntPtr pAddress;
        ////}


        ////struct HTTP_SERVICE_CONFIG_SSL_QUERY
        ////{
        ////    public HTTP_SERVICE_CONFIG_QUERY_TYPE QueryDesc;
        ////    public HTTP_SERVICE_CONFIG_SSL_KEY KeyDesc;
        ////    public uint dwToken;
        ////}

        ////struct HTTP_SERVICE_CONFIG_SSL_SET
        ////{
        ////    public HTTP_SERVICE_CONFIG_SSL_KEY KeyDesc;
        ////    public HTTP_SERVICE_CONFIG_SSL_PARAM ParamDesc;
        ////}

        ////struct HTTP_SERVICE_CONFIG_SSL_KEY
        ////{
        ////    public IntPtr pIpPort;
        ////}

        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
        struct HTTP_SERVICE_CONFIG_SSL_PARAM
        {
            public int SslHashLength;
            public IntPtr pSslHash;
            public Guid AppId;
            [MarshalAs(UnmanagedType.LPWStr)]
            public string pSslCertStoreName;
            public uint DefaultCertCheckMode;
            public int DefaultRevocationFreshnessTime;
            public int DefaultRevocationUrlRetrievalTimeout;
            [MarshalAs(UnmanagedType.LPWStr)]
            public string pDefaultSslCtlIdentifier;
            [MarshalAs(UnmanagedType.LPWStr)]
            public string pDefaultSslCtlStoreName;
            public uint DefaultFlags;
        }



        [DllImport("httpapi.dll")]
        static extern Error HttpQueryServiceConfiguration(
             IntPtr ServiceIntPtr,
             HTTP_SERVICE_CONFIG_ID ConfigId,
             IntPtr pInputConfigInfo,
             int InputConfigInfoLength,
             IntPtr pOutputConfigInfo,
             int OutputConfigInfoLength,
             out int pReturnLength,
             IntPtr pOverlapped);

        [DllImport("httpapi.dll")]
        static extern Error HttpSetServiceConfiguration(
             IntPtr ServiceIntPtr,
             HTTP_SERVICE_CONFIG_ID ConfigId,
             IntPtr pConfigInformation,
             int ConfigInformationLength,
             IntPtr pOverlapped);

        [DllImport("httpapi.dll")]
        static extern Error HttpDeleteServiceConfiguration(
             IntPtr ServiceIntPtr,
             HTTP_SERVICE_CONFIG_ID ConfigId,
             IntPtr pConfigInformation,
             int ConfigInformationLength,
             IntPtr pOverlapped);
    }
}
