﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Net;
using System.IO;
using Microsoft.Win32.SafeHandles;

namespace AspNetHost
{
    // Based on sample code from MSDN article "Run ASMX Without IIS", Aaron Skonnard, 
    // http://msdn.microsoft.com/en-au/magazine/cc163879.aspx
    // Retrieved 2009/10/05

    public class AspNetHostWorkerRequest : HttpWorkerRequest
    {
        private MarshalableListenerContext mContext;
        private string mVirtualDir;
        private string mPhysicalDir;


        public AspNetHostWorkerRequest(MarshalableListenerContext context, string virtualDir, string physicalDir)
        {
            if (context == null)
            {
                throw new ArgumentNullException("context");
            }

            if (String.IsNullOrEmpty(virtualDir))
            {
                throw new ArgumentException("virtualDir");
            }

            if (String.IsNullOrEmpty(physicalDir))
            {
                throw new ArgumentException("physicalDir");
            }

            this.mContext = context;
            this.mVirtualDir = virtualDir;
            this.mPhysicalDir = physicalDir;
        }


        public override void EndOfRequest()
        {
            this.mContext.CloseResponse();
        }

        public override void FlushResponse(bool finalFlush)
        {
            this.mContext.OutputStream.Flush();
        }

        public override string GetHttpVerbName()
        {
            return this.mContext.HttpMethod;
        }

        public override string GetHttpVersion()
        {
            return this.mContext.HttpVersion;
        }

        public override string GetLocalAddress()
        {
            return this.mContext.LocalAddress;
        }

        public override int GetLocalPort()
        {
            return this.mContext.LocalPort;
        }

        public override string GetQueryString()
        {
            return this.mContext.Query;
        }

        public override string GetRawUrl()
        {
            return this.mContext.RawUrl;
        }

        public override string GetRemoteAddress()
        {
            return this.mContext.RemoteAddress;
        }

        public override int GetRemotePort()
        {
            return this.mContext.RemotePort;
        }

        public override string GetUriPath()
        {
            return this.mContext.LocalPath;
        }

        public override void SendKnownResponseHeader(int index, string value)
        {
            string headerName = HttpWorkerRequest.GetKnownResponseHeaderName(index);
            this.mContext.SetHeader(headerName, value);
        }

        public override void SendResponseFromMemory(byte[] data, int length)
        {
            this.mContext.OutputStream.Write(data, 0, length);
        }

        public override void SendStatus(int statusCode, string statusDescription)
        {
            this.mContext.SetResponseStatus(statusCode, statusDescription);
        }

        public override void SendUnknownResponseHeader(string name, string value)
        {
            this.mContext.SetHeader(name, value);
        }

        public override void SendResponseFromFile(IntPtr handle, long offset, long length)
        {
            using (FileStream file = new FileStream(new SafeFileHandle(handle, false), FileAccess.Read))
            {
                long originalPosition = file.Position;
                this.SendResponseFromStream(file, offset, length);
                file.Position = originalPosition;
            }
        }

        public override void SendResponseFromFile(string filename, long offset, long length)
        {
            using (FileStream file = new FileStream(filename, FileMode.Open, FileAccess.Read))
            {
                this.SendResponseFromStream(file, offset, length);
            }
        }

        private void SendResponseFromStream(Stream stream, long offset, long length)
        {
            stream.Position = offset;

            byte[] buffer = new byte[32768];
            while (length > 0)
            {
                int toRead = (int)Math.Min(length, buffer.Length);
                int bytesRead = stream.Read(buffer, 0, toRead);

                if (bytesRead == 0)
                {
                    break;
                }

                this.mContext.OutputStream.Write(buffer, 0, bytesRead);
                length -= bytesRead;
            }
        }

        public override void CloseConnection()
        {
            this.mContext.CloseResponse();
        }

        public override string GetAppPath()
        {
            return this.mVirtualDir;
        }

        public override string GetAppPathTranslated()
        {
            return this.mPhysicalDir;
        }

        public override int ReadEntityBody(byte[] buffer, int size)
        {
            return this.mContext.InputStream.Read(buffer, 0, size);
        }

        public override string GetUnknownRequestHeader(string name)
        {
            return this.mContext.GetHeader(name);
        }

        //public override string[][] GetUnknownRequestHeaders()
        //{
        //    var headers = this.mContext.Request.Headers;
        //    int count = headers.Count;
        //    List<string[]> headerPairs = new List<string[]>(count);
        //    for (int i = 0; i < count; i++)
        //    {
        //        string headerName = headers.GetKey(i);
        //        if (GetKnownRequestHeaderIndex(headerName) == -1)
        //        {
        //            string headerValue = headers.Get(i);
        //            headerPairs.Add(new string[] { headerName, headerValue });
        //        }
        //    }

        //    string[][] unknownRequestHeaders = headerPairs.ToArray();
        //    return unknownRequestHeaders;
        //}

        public override string GetKnownRequestHeader(int index)
        {
            string name = HttpWorkerRequest.GetKnownRequestHeaderName(index);
            return this.mContext.GetHeader(name);
        }

        //public override string GetServerVariable(string name)
        //{
        //    switch (name)
        //    {
        //        case "HTTPS":
        //            return this.mContext.Request.IsSecureConnection ? "on" : "off";

        //        case "HTTP_USER_AGENT":
        //            return this.mContext.Request.UserAgent;

        //        default:
        //            return null;
        //    }
        //}

        public override string MapPath(string virtualPath)
        {
            if (!virtualPath.StartsWith(this.mVirtualDir))
            {
                return null;
            }

            virtualPath = virtualPath.Substring(this.mVirtualDir.Length).TrimStart('/');
            if (Path.DirectorySeparatorChar != '/')
            {
                virtualPath = virtualPath.Replace('/', '\\');
            }
        
            return Path.Combine(this.mPhysicalDir, virtualPath);
        }

        public override string GetFilePath()
        {
            return this.GetUriPath();
        }

        public override string GetFilePathTranslated()
        {
            return this.MapPath(this.GetFilePath());
        }

        //public override string GetPathInfo()
        //{
        //    string s1 = GetFilePath();
        //    string s2 = this.mContext.Request.Url.LocalPath;
        //    if (s1.Length == s2.Length)
        //        return "";
        //    else
        //        return s2.Substring(s1.Length);
        //}
    }
}
