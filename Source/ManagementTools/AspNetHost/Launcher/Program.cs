﻿//-----------------------------------------------------------------------
// <copyright file="Program.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2010 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
using System;
using System.Diagnostics;
using System.Net;
using System.Net.Sockets;
using System.Windows.Forms;
using AspNetHost;
using Launcher.Properties;
using Mono.Options;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;

namespace Launcher
{
    /// <summary>
    /// The main entry of the Launcher.exe used to start the aspnet host under a specified hostname (e.g. http://127.0.0.1:21254).
    /// </summary>
    class Program : IDisposable
    {        
        /// <summary>
        /// Use to open a new console output.
        /// See http://stackoverflow.com/questions/472282/show-console-in-windows-application
        /// </summary>
        [DllImport("kernel32.dll", SetLastError = true)]
        static extern bool AllocConsole();

        /// <summary>
        /// Free the console (i.e. closing the console window).
        /// </summary>
        /// <returns></returns>
        [DllImport("kernel32.dll", SetLastError = true)]
        static extern bool FreeConsole();
        
        /// <summary>
        /// Exit codes enumeration.
        /// </summary>
        private enum ExitCodes : int
        {
            Success = 0,
            InitializationFailure,
            UnhandledException
        }

        /// <summary>
        /// The Assembly name.
        /// </summary>
        private static string executable = AppDomain.CurrentDomain.FriendlyName;

        /// <summary>
        /// Port to listen on.
        /// </summary>
        private static int port = 0;
        
        /// <summary>
        /// Ip address used for hosting.
        /// </summary>
        private static string ip = "127.0.0.1";

        /// <summary>
        /// A value indicating whether this program should running as windows application or console application.
        /// </summary>
        private static bool console = false;

        /// <summary>
        /// OptionSet for passing command line arguments
        /// </summary>
        private static OptionSet optionSet = new OptionSet()
        {
            { "h|help", "show this message and exit.", v =>
                {
                    if (v != null)
                    {
                        Program.WriteUsageAndExit();
                    }
                }
            },
            {"p|port=", "the port to listen on.", (ushort v) => port = v},
            {"i|ip=", "the ip address is used for hosting.", v => ip = v}
        };

        /// <summary>
        /// Main function of Launcher.exe project.
        /// </summary>
        /// <param name="args">Command line arguments</param>
        /// <returns>Return with exit codes.</returns>
        [STAThread]
        static int Main(string[] args)
        {
            try
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
            }
            catch
            {
                console = true;
            }

            try
            {
                List<string> extras = optionSet.Parse(args);

                if (extras.Count > 0)
                {
                    Program.HandleUnknownArguments(extras.ToArray());
                }

            }
            catch (OptionException e)
            {
                if (!console)
                {
                    AllocConsole();
                }

                Console.WriteLine("{0}: {1}", Launcher.Program.executable, e.Message);
                Console.WriteLine("Try `{0} --help' for more information.", executable);

                if (!console)
                {
                    Console.WriteLine("press any key to continue ...");
                    Console.ReadKey();
                    FreeConsole();
                }

                return (int)ExitCodes.UnhandledException;
            }

            // Note: use the executable (assembly name as a unique identifier)
            string uniqueIdentifier = string.Format("{0}.AspNetHostLauncher", Launcher.Program.executable);

            if (ip != null && port != 0)
            {
                uniqueIdentifier = string.Format("{0}:{1}.AspNetHostLauncher", ip, port);
            }

            Program program = null;

            bool isFirstInstance = SingletonApp.EnsureSingleton(uniqueIdentifier,
                delegate
                {
                    if (program != null && !console)
                    {
                        program.Open();
                    }
                });

            if (!isFirstInstance)
            {
                return (int)ExitCodes.InitializationFailure;
            }

            try
            {
                program = new Program();

                program.Start(ip,port);
                if (!console)
                {
                    Application.Run();
                    GC.KeepAlive(program);
                }
                else
                {
                    while (true)
                    {
                        System.Threading.Thread.Sleep(1000);
                    }
                }
            }
            catch (Exception e)
            {
                if (!console)
                {
                    MessageBox.Show(string.Format("{0} \n Stacktrace: {1}", e.Message, e.StackTrace));
                }
                else
                {
                    Console.WriteLine(string.Format("{0} \n Stacktrace: {1}", e.Message, e.StackTrace));
                }
            }
            finally
            {
                if (program != null)
                {
                    program.Dispose();
                }
            }

            return (int)ExitCodes.Success;
        }
        
        /// <summary>
        /// Notification icon for windows.
        /// </summary>
        private NotifyIcon notifyIcon;

        /// <summary>
        /// Asp Server.
        /// </summary>
        private AspServer aspServer;

        /// <summary>
        /// Asp server uri.
        /// </summary>
        private Uri uri;

        /// <summary>
        /// Make notification icon for windows only currently.
        /// </summary>
        /// <param name="ip">Ip address used for hosting.</param>
        /// <param name="port">Port to listen on</param>
        /// <returns>Notify icon.</returns>
        private NotifyIcon MakeNotifyIcon(string ip,int port)
        {
            NotifyIcon notifyIcon = new NotifyIcon();
            notifyIcon.Icon = Resources.Generic_Application;
            MenuItem[] menuItems = new MenuItem[]
            {
                new MenuItem("&Open", delegate { this.Open(); }) { DefaultItem = true },
                new MenuItem("E&xit", delegate { this.Exit(); }),                
            };

            notifyIcon.ContextMenu = new ContextMenu(menuItems);
            notifyIcon.DoubleClick += delegate { this.Open(); };
            notifyIcon.Text = string.Format("{2} ({0}:{1})", ip, port, Launcher.Program.executable);
            notifyIcon.Visible = true;
            return notifyIcon;
        }

        /// <summary>
        /// Try to open the asp server uri.
        /// </summary>
        private void Open()
        {
            if (this.uri != null)
            {
                Process.Start(this.uri.ToString());
            }
        }

        /// <summary>
        /// Exit application.
        /// </summary>
        private void Exit()
        {
            Application.Exit();
        }

        /// <summary>
        /// Dispose the asp server and notify icon.
        /// </summary>
        public void Dispose()
        {
            if (this.aspServer != null)
            {
                this.aspServer.Stop();
                this.aspServer = null;
            }
            
            if (this.notifyIcon != null)
            {
                this.notifyIcon.Dispose();
                this.notifyIcon = null;
            }
        }

        /// <summary>
        /// Start asp server with the specified ip address and port number.
        /// </summary>
        /// <param name="ip">Ip address used for hosting.</param>
        /// <param name="port">Port to listen on</param>
        public void Start(string ip, int port)
        {
            // Default port
            if (port == 0)
            {
                if (Launcher.Program.executable.Contains("ControlCenter"))
                {
                    port = Settings.Default.ControlCenterPort;
                }
                else
                {
                    port = Settings.Default.DeiAdminToolPort;
                }
            }

            string physicalDir = AppDomain.CurrentDomain.BaseDirectory;
            string virtualDir = "/";

            try
            {
                using (HttpApi httpApi = new HttpApi())
                {
                    httpApi.SetUrlAcl(string.Format("http://+:{0}/", port), "D:(A;;GX;;;WD)");
                }
            }
            catch
            {
            }

            try
            {
                this.uri = new Uri(String.Format("http://{0}:{1}/", ip, port));

                // use strong wildcard UrlPrefix for the Http Server API (http://msdn.microsoft.com/en-us/library/aa364698(v=VS.85).aspx)
                this.aspServer = new AspServer(String.Format("http://+:{0}/", port), virtualDir, physicalDir);
            }
            catch (Exception e)
            {
                string errorMessage = string.Empty;

                if (e is PortInUseException)
                {
                    errorMessage = string.Format("The port number {0} is being used by other program, please select a different port number", port);                    
                }
                else if (e is AccessDeniedException)
                {
                    // This should only happen on Windows, and then only if a reservation hasn't
                    // been made for this URI.
                    errorMessage = string.Format("Please run as administrator if this is your first time running {0} using port number {1}", executable, port);                    
                }
                else if (e is FileNotFoundException)
                {
                    errorMessage = "Exception: Missing full .NET Framework 4!";
                }
                else
                {
                    errorMessage = string.Format("Exception type: {0}, message: {1}, stacktrace: {2}", e.GetType().ToString(), e.Message, e.StackTrace);                    
                }

                if (!console)
                {
                    MessageBox.Show(errorMessage);
                }
                else
                {
                    Console.WriteLine(errorMessage);
                }

                Environment.Exit(1);
            }

            this.aspServer.Start();
            if (!console)
            {
                this.Open();
                try
                {
                    this.notifyIcon = this.MakeNotifyIcon(ip, port);
                }
                catch (NotImplementedException)
                {
                    // Not implemented for OS X yet
                }
            }
        }

        /// <summary>
        /// Write error message listing unknown arguments.
        /// </summary>
        /// <param name="arguments">Unknown arguments.</param>
        private static void HandleUnknownArguments(string[] arguments)
        {
            if (!console)
            {
                AllocConsole();
            }
            Console.Write("{0}: Unknown argument(s):", Launcher.Program.executable);

            foreach (string arg in arguments)
            {
                Console.Write(" " + arg);
            }

            Console.WriteLine(".");
            Console.WriteLine("Try `{0} --help' for more information.", Launcher.Program.executable);

            if (!console)
            {
                Console.WriteLine("press any key to continue ...");
                Console.ReadKey();
                FreeConsole();
            }

            Environment.Exit((int)ExitCodes.Success);
        }

        /// <summary>
        /// Write usage instructions and exit.
        /// </summary>
        private static void WriteUsageAndExit()
        {
            if (!console)
            {
                AllocConsole();
            }

            Console.WriteLine(string.Format("Usage: {0} [Options]", Launcher.Program.executable));
            Console.WriteLine(string.Format("Start {0} asp.net host", Launcher.Program.executable));
            Console.WriteLine(string.Empty);
            Console.WriteLine("Options:");
            
            Program.optionSet.WriteOptionDescriptions(Console.Out);

            if (!console)
            {
                Console.WriteLine("press any key to continue ...");
                Console.ReadKey();
                FreeConsole();
            }
            
            Environment.Exit((int)ExitCodes.Success);
        }
    }
}
