﻿using System;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Ipc;

namespace Launcher
{
    class SingletonApp : MarshalByRefObject
    {
        private Action mAction;

        public SingletonApp(Action action)
        {
            if (action == null)
            {
                throw new ArgumentNullException("action");
            }
            this.mAction = action;
        }

        public void Invoke()
        {
            this.mAction();
        }

        public override object InitializeLifetimeService()
        {
            return null;  // Infinite lifetime
        }

        /// <summary>
        /// Check to see if there's another instance of this application running.  If there
        /// is then this will cause the onSecondStarted action to be executed in the original
        /// instance.
        /// </summary>
        /// <param name="identifier">A unique identifier for the application</param>
        /// <param name="onSecondStarted">Action to be executed in the original instance when a second is started</param>
        /// <returns>True if this is the first instance, false otherwise</returns>
        public static bool EnsureSingleton(string identifier, Action onSecondStarted)
        {
            string singletonPath = "singleton";
            string singletonUri = "ipc://" + identifier + "/" + singletonPath;

            IpcChannel serverChannel = null;
            try
            {
                serverChannel = new IpcChannel(identifier);
            }
            catch (Exception)  // Thrown under Microsoft.NET if something is already listening
            {
                // If something's already listening Microsoft.NET throws RemotingException
                // and Mono throws a TargetInvokationException containing and InvalidOperationException.
                // We just assume any failure to create the server channel means the server
                // exists.
            }

            if (serverChannel == null)  // Looks like application is already running
            {
                IpcChannel clientChannel = new IpcChannel();
                ChannelServices.RegisterChannel(clientChannel, false);
                SingletonApp singleton = (SingletonApp)Activator.GetObject(typeof(SingletonApp), singletonUri);

                try
                {
                    singleton.Invoke();
                }
                catch (Exception)
                {
                    // This might happen if the original instance is going down.  Hopefully this
                    // won't occur very frequently so we just ignore the failure and continue as
                    // if we're a second instance.
                }

                return false;
            }

            ChannelServices.RegisterChannel(serverChannel, false);
            SingletonApp serverSingleton = new SingletonApp(onSecondStarted);
            RemotingServices.Marshal(serverSingleton, singletonPath);
            return true;
        }
    }
}
