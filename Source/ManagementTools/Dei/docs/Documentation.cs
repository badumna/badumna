﻿
#pragma warning disable 1587  // XML comment is not placed on a valid language element

/// \file
/// 
/// \mainpage
/// 
/// \section introduction Introduction
/// Dei is an authentication and user management service used in conjustion with 
/// the Badumna network library. 
/// 
/// The service is responsible for authenticating users, validating their permission
/// to use the application and issueing certificates that can be used by other users
/// to ensure they are trusted.
/// 
/// \section client Client side usage
/// The client should instansiate an instance of the \link Dei.IdentityProvider IdentityProvider\endlink class, 
/// giving the server address and port to the constructor. The \link Dei.IdentityProvider.Authenticate Authenticate\endlink
/// method should then be called with the user's username and password. If successfull the 
/// returned \link Dei.LoginResult LoginResult \endlink object will have its \link Dei.LoginResult.WasSuccessful WasSuccessful \endlink field set to true. 
/// Once this has occurred the identity provider should be passed to Badumna's NetworkFacade.Login
/// method, which access the security tokens as needed.
/// 
/// \section server Server side usage
/// A default implementation of the server has been implemented using a SQLite database, which will be 
/// adequate for small independant applications. Typically, however, you will want to hook the service 
/// into you pre-existing database setup. This can be done by implementing your own \link Dei.ServerUtilities.IAccountManager IAccountManager \endlink
/// class or deriving from the \link Dei.ServerUtilities.SqlAccountManager SqlAccountManager \endlink.
/// 
/// \section ssl SSL
/// The Dei protocol uses the SSL protocl which requires a certificate containing the domain name of the 
/// server and signed by a trusted authority. If you wish to ignore the ssl validation erros for testing 
/// (for eg. if you are using localhost as the server address) then you can set the \link Dei.IdentityProvider.IgnoreSslErrors IgnoreSSLErrors \endlink
/// property on the IdentityProvider to true or provide your own certificate validation method when 
/// calling the \link Dei.IdentityProvider.Authenticate Authenticate \endlink method.
/// 


#pragma warning restore 1587