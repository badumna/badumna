//-----------------------------------------------------------------------
// <copyright file="VersionExceptionTests.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace DeiTests
{
    using System;
    using Dei;
    using NUnit.Framework;

    [TestFixture]
    internal class VersionExceptionTests
    {
        public VersionExceptionTests()
        {
        }

        [Test]
        public void VersionExceptionDefaultConstructorWorks()
        {
            VersionException versionException = new VersionException();
            Assert.IsNotNull(versionException);
        }

        [Test]
        public void VersionExceptionConstructorWithValidMessageWorks()
        {
            string message = "Version exception message";
            VersionException versionException = new VersionException(message);
            Assert.IsNotNull(versionException);
            Assert.AreEqual(message, versionException.Message);
        }

        [Test]
        public void VersionExceptionConstructorWithValidInnerExceptionWorks()
        {
            string message = "Version exception message";
            string innerExceptionMessage = "Inner Exception";
            Exception innerException = new Exception(innerExceptionMessage);
            Assert.IsNotNull(innerException);
            VersionException versionException = new VersionException(message, innerException);
            Assert.IsNotNull(versionException);
            Assert.AreEqual(message, versionException.Message);
            Assert.IsNotNull(versionException.InnerException);
            Assert.AreEqual(innerExceptionMessage, versionException.InnerException.Message);
        }
    }
}
