//-----------------------------------------------------------------------
// <copyright file="SerializerWrapperFactoryTests.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace DeiTests.Protocol
{
    using System;
    using Dei.Protocol;
    using NUnit.Framework;

    [TestFixture]
    internal class SerializerWrapperFactoryTests
    {
        [Test]
        public void SerializerWrapperFactoryReturnsValidSerializerWrapper()
        {
            SerializerWrapperFactory factory = new SerializerWrapperFactory();
            ISerializerWrapper serializerWrapper = factory.Create(typeof(int));
            Assert.IsTrue(serializerWrapper is SerializerWrapper);
        }
    }
}
