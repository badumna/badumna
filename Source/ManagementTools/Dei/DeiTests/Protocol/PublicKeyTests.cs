//-----------------------------------------------------------------------
// <copyright file="PublicKeyTests.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace DeiTests.Protocol
{
    using System;
    using Dei;
    using Dei.Protocol;
    using NUnit.Framework;

    [TestFixture]
    internal class PublicKeyTests
    {
        private TimeSpan validityPeriod = TimeSpan.FromHours(24);

        private byte[] key = new byte[128];

        public PublicKeyTests()
        {
            Random random = new Random((int)DateTime.Now.Ticks);
            random.NextBytes(this.key);
        }

        [Test]
        public void PublicKeyDefaultConstructorWorks()
        {
            PublicKey publicKey = new PublicKey();
            Assert.IsNotNull(publicKey);
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void PublicKeyConstructorWithNullKeyShouldThrowsArgumentNullExcpeption()
        {
            PublicKey publicKey = new PublicKey(this.validityPeriod, null);
        }

        [Test]
        public void PublicKeyConstructorWithMaxValidityPeriodShouldWorks()
        {
            UtcDateTime expiryTime = new UtcDateTime((new DateTime(DateTime.MaxValue.Ticks, DateTimeKind.Utc)));
            PublicKey publicKey = new PublicKey(TimeSpan.MaxValue, this.key);
            Assert.IsNotNull(publicKey);
            Assert.AreEqual(
                expiryTime.Ticks, 
                publicKey.ExpiryTime.Ticks); 
        }

        [Test]
        public void PublicKeyConstructorWithValidInputsShouldWorks()
        {
            PublicKey publicKey = new PublicKey(this.validityPeriod, this.key);
            Assert.IsNotNull(publicKey);
            Assert.AreEqual(this.key, publicKey.Key);
            Assert.IsFalse(publicKey.HasExpired);
        }

        [Test]
        public void SetsAndGetsPublicPropertiesWorks()
        {
            UtcDateTime expiryTime = new UtcDateTime(DateTime.UtcNow + this.validityPeriod);
            PublicKey publicKey = new PublicKey();
            Assert.IsNotNull(publicKey);

            publicKey.Key = this.key;
            Assert.AreEqual(this.key, publicKey.Key);

            publicKey.ExpiryTime = expiryTime;
            Assert.AreEqual(expiryTime, publicKey.ExpiryTime);

            Assert.IsFalse(publicKey.HasExpired);
        }
    }
}
