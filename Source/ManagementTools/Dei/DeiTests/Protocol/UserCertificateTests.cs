//-----------------------------------------------------------------------
// <copyright file="UserCertificateTests.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace DeiTests.Protocol
{
    using System;
    using System.Security.Cryptography;
    using Dei;
    using Dei.Protocol;
    using Dei.Utilities;
    using NUnit.Framework;
    using Badumna.Security;

    [TestFixture]
    internal class UserCertificateTests
    {
        private const int StateLength = 148;

        private const int SignatureLength = 128;

        private static Random random = new Random((int)DateTime.Now.Ticks);

        private UserProperties user;

        private Character character;

        private UtcDateTime timeStamp = new UtcDateTime(DateTime.UtcNow);

        private UtcDateTime expiryTime = new UtcDateTime(DateTime.UtcNow + TimeSpan.FromHours(24));

        public UserCertificateTests()
        {
        }

        [SetUp]
        public void SetUp()
        {
            this.user = new UserProperties("user", "password");
            this.character = new Character(123, "char1");
        }

        [TearDown]
        public void TearDown()
        {
            this.user = null;
        }

        [Test]
        public void UserCertificateDefaultConstructorWorks()
        {
            UserCertificate userCertificate = new UserCertificate();
            Assert.IsNotNull(userCertificate);
        }

        [Test]
        public void UserCertificateWithValidInputsWorks()
        {
            byte[] badumnaState = new byte[UserCertificateTests.StateLength];
            random.NextBytes(badumnaState);

            UserCertificate userCertificate = UserCertificate.CreateUnsignedClientCertificate(
                this.character,
                badumnaState);
            
            Assert.IsNotNull(userCertificate);
            UserCertificate userCertificate2 = new UserCertificate(
                userCertificate,
                this.timeStamp,
                this.expiryTime);

            Assert.IsNotNull(userCertificate2);
            Assert.AreEqual(this.character, userCertificate2.Character);
            Assert.AreEqual(this.timeStamp, userCertificate2.TimeStamp);
            Assert.AreEqual(this.expiryTime, userCertificate2.ExpiryTime);
            Assert.AreEqual(0, userCertificate2.UserId);
            Assert.IsFalse(userCertificate2.HasExpired);
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void UserCertificateWithInvalidTimeStampInputsShouldThrowsArgumentNullException()
        {
            byte[] badumnaState = new byte[UserCertificateTests.StateLength];
            random.NextBytes(badumnaState);

            UserCertificate userCertificate = UserCertificate.CreateUnsignedClientCertificate(
                this.character,
                badumnaState);

            Assert.IsNotNull(userCertificate);
            UserCertificate userCertificate2 = new UserCertificate(
                userCertificate,
                null,
                this.expiryTime);
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void UserCertificateWithInvalidExpiryTimeInputsShouldThrowsArgumentNullException()
        {
            byte[] badumnaState = new byte[UserCertificateTests.StateLength];
            random.NextBytes(badumnaState);

            UserCertificate userCertificate = UserCertificate.CreateUnsignedClientCertificate(
                this.character,
                badumnaState);

            Assert.IsNotNull(userCertificate);
            UserCertificate userCertificate2 = new UserCertificate(
                userCertificate,
                this.timeStamp,
                null);
        }

        [Test]
        [ExpectedException(typeof(ArgumentException))]
        public void CreateUnsignedClientCertificateWithInvalidAdditionalDataLengthShouldThrowsArgumentException()
        {
            byte[] badumnaState = new byte[UserCertificateTests.StateLength + 1];
            random.NextBytes(badumnaState);
            
            UserCertificate userCertificate = UserCertificate.CreateUnsignedClientCertificate(
                this.character, 
                badumnaState);
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void CreateUnsignedClientCertificateWithNullAdditionalDataShouldThrowsArgumentNullException()
        {
            UserCertificate userCertificate = UserCertificate.CreateUnsignedClientCertificate(this.character, null);
        }

        [Test]
        public void SetsAndGetsUserCertificatePropertiesWorks()
        {
            long userId = 1;
            byte[] signature = new byte[UserCertificateTests.SignatureLength];
            random.NextBytes(signature);

            UserCertificate userCertificate = new UserCertificate();
            Assert.IsNotNull(userCertificate);

            userCertificate.UserId = userId;
            Assert.AreEqual(userId, userCertificate.UserId);

            userCertificate.ExpiryTime = this.expiryTime;
            Assert.AreEqual(this.expiryTime, userCertificate.ExpiryTime);

            userCertificate.Signature = signature;
            Assert.AreEqual(signature, userCertificate.Signature);
        }

        [Test]
        public void IsValidWithValidUserCertificateShouldReturnTrue()
        {
            byte[] badumnaState = new byte[UserCertificateTests.StateLength];
            random.NextBytes(badumnaState);

            UserCertificate userCertificate = UserCertificate.CreateUnsignedClientCertificate(
                this.character,
                badumnaState);

            Assert.IsNotNull(userCertificate);
            Assert.IsTrue(userCertificate.IsValid());
        }


        [Test]
        public void IsValidWithNoBadumnaStateShouldReturnFalse()
        {
            byte[] badumnaState = new byte[UserCertificateTests.StateLength];
            random.NextBytes(badumnaState);

            UserCertificate userCertificate = UserCertificate.CreateUnsignedClientCertificate(
                this.character,
                badumnaState);

            userCertificate.BadumnaState = null;
            Assert.IsFalse(userCertificate.IsValid());
        }

        [Test]
        public void IsValidWithEmptyCharacterNameShouldReturnFalse()
        {
            byte[] badumnaState = new byte[UserCertificateTests.StateLength];
            random.NextBytes(badumnaState);

            UserCertificate userCertificate = UserCertificate.CreateUnsignedClientCertificate(
                new Character(12, ""),
                badumnaState);

            Assert.IsFalse(userCertificate.IsValid());
        }

        [Test]
        public void IsValidWithNegativeCharacterIdShouldReturnFalse()
        {
            byte[] badumnaState = new byte[UserCertificateTests.StateLength];
            random.NextBytes(badumnaState);

            UserCertificate userCertificate = UserCertificate.CreateUnsignedClientCertificate(
                new Character(-1, "bob"),
                badumnaState);

            Assert.IsFalse(userCertificate.IsValid());
        }

        [Test]
        public void IsValidWithNullCharacterShouldReturnTrue()
        {
            byte[] badumnaState = new byte[UserCertificateTests.StateLength];
            random.NextBytes(badumnaState);

            UserCertificate userCertificate = UserCertificate.CreateUnsignedClientCertificate(
                null,
                badumnaState);

            Assert.IsTrue(userCertificate.IsValid());
        }

        [Test]
        public void SetAndGetUserCertificatePropertiesWorks()
        {
            byte[] badumnaState = new byte[UserCertificateTests.StateLength];
            random.NextBytes(badumnaState);

            UserCertificate userCertificate = new UserCertificate();
            Assert.IsNotNull(userCertificate);

            userCertificate.BadumnaState = badumnaState;
            Assert.AreEqual(badumnaState, userCertificate.BadumnaState);

            userCertificate.TimeStamp = this.timeStamp;
            Assert.AreEqual(this.timeStamp, userCertificate.TimeStamp);

            userCertificate.Character = this.character;
            Assert.AreEqual(this.character, userCertificate.Character);
        }


        [Test]
        public void TestXmlSerialzation()
        {
            byte[] badumnaState = new byte[UserCertificateTests.StateLength];
            random.NextBytes(badumnaState);

            XmlTestHelper.AssertCommandXmlSerialization<UserCertificate>(
                UserCertificate.CreateUnsignedClientCertificate(
                    this.character,
                    badumnaState));

            XmlTestHelper.AssertCommandXmlSerialization<UserCertificate>(
                UserCertificate.CreateUnsignedClientCertificate(
                    null,
                    badumnaState));
        }
    }
}
