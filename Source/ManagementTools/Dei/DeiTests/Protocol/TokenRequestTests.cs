//-----------------------------------------------------------------------
// <copyright file="TokenRequestTests.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace DeiTests.Protocol
{
    using System;
    using Dei.Protocol;
    using NUnit.Framework;

    [TestFixture]
    internal class TokenRequestTests
    {
        public TokenRequestTests()
        {
        }

        [Test]
        public void TokenRequestDefaultConstructorWorks()
        {
            TokenRequest tokenRequest = new TokenRequest();
            Assert.IsNotNull(tokenRequest);
        }

        [Test]
        public void TokenRequestConstructorWithValidInputWorks()
        {
            int tokenNumber = 1;
            TokenRequest tokenRequest = new TokenRequest(tokenNumber);
            Assert.IsNotNull(tokenRequest);
            Assert.AreEqual(tokenNumber, tokenRequest.Number);
        }
    }
}
