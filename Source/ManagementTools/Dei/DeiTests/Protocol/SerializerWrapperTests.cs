//-----------------------------------------------------------------------
// <copyright file="SerializerWrapperTests.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace DeiTests.Protocol
{
    using System;
    using System.Xml;
    using Dei.Protocol;
    using NUnit.Framework;
    using Rhino.Mocks;

    [TestFixture]
    internal class SerializerWrapperTests
    {
        private Type type = typeof(int);

        public SerializerWrapperTests()
        {
        }

        [Test]
        public void SerializerWrapperConstructorWorks()
        {
            SerializerWrapper serializerWrapper = new SerializerWrapper(this.type);
            Assert.IsNotNull(serializerWrapper);
        }
    }
}
