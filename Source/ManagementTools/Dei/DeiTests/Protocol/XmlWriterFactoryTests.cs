//-----------------------------------------------------------------------
// <copyright file="XmlWriterFactoryTests.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace DeiTests.Protocol
{
    using System.IO;
    using System.Xml;
    using Dei.Protocol;
    using NUnit.Framework;
    using Rhino.Mocks;

    [TestFixture]
    internal class XmlWriterFactoryTests
    {
        [Test]
        public void XmlWriterFactoryReturnsValidXmlWriter()
        {
            XmlWriterFactory factory = new XmlWriterFactory();
            XmlWriter xmlWriter = factory.Create(MockRepository.GenerateMock<Stream>(), null);
            Assert.IsNotNull(xmlWriter);
        }
    }
}
