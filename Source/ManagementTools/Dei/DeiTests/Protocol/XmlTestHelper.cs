﻿using System;
using System.IO;
using System.Xml;
using System.Xml.Schema;
using NUnit.Framework;
using Dei;
using Dei.Protocol;

namespace DeiTests.Protocol
{
    class XmlTestHelper
    {
        public static T AssertCommandXmlSerialization<T>(T value)
        {
            var stream = new MemoryStream();
            ValidationEventHandler validationEventHandler = delegate(object sender, ValidationEventArgs e)
            {
                throw new Exception("Validation error:", e.Exception);
            };

            XmlWriterSettings writerSettings = new XmlWriterSettings();
            writerSettings.ConformanceLevel = ConformanceLevel.Fragment;

            var protocolSchema = XmlSchema.Read(new StringReader(Resources.DeiProtocol), validationEventHandler);

            var writer = new XmlWriterFactory().Create(stream, writerSettings);
            SerializerWrapper wrapper = new SerializerWrapper(typeof(T));

            writer.WriteStartElement(Resources.ConversationElementName, Resources.ProtocolNameSpace);
            new SerializerWrapper(typeof(Dei.Protocol.Version)).WriteObject(writer, new Dei.Protocol.Version(2, 1));
            writer.WriteStartElement("x");
            new SerializerWrapper(typeof(Commands)).WriteObject(writer, Commands.None);
            wrapper.WriteObject(writer, value);
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.Flush();

            //stream.Position = 0;
            //System.Console.WriteLine("Serialized XML: " + new StreamReader(stream).ReadToEnd());

            // now read it back:
            stream.Position = 0;
            XmlReaderSettings readerSettings = new XmlReaderSettings();
            readerSettings.Schemas.Add(protocolSchema);
            readerSettings.ValidationType = ValidationType.Schema;
            readerSettings.ValidationFlags |= XmlSchemaValidationFlags.ProcessInlineSchema;
            readerSettings.ValidationFlags |= XmlSchemaValidationFlags.ReportValidationWarnings;
            readerSettings.ValidationEventHandler += validationEventHandler;
            var reader = new XmlReaderFactory().Create(stream, readerSettings);

            T result;
            try
            {
                while (!wrapper.CanDeserialize(reader))
                {
                    Assert.IsTrue(reader.Read(), "premature end of XML");
                }

                result = (T)wrapper.ReadObject(reader);
            }
            catch (Exception e)
            {
                stream.Position = 0;
                throw new Exception("couldn't read value from XML: " + new StreamReader(stream).ReadToEnd(), e);
            }
            Assert.IsNotNull(result);
            return result;
        }
    }
}
