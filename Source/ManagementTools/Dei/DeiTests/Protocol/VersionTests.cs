//-----------------------------------------------------------------------
// <copyright file="VersionTests.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace DeiTests.Protocol
{
    using Dei.Protocol;
    using NUnit.Framework;

    [TestFixture]
    internal class VersionTests
    {
        public VersionTests()
        {
        }

        [Test]
        public void VersionTestDefaultConstructorWorks()
        {
            Version version = new Version();
            Assert.IsNotNull(version);
        }

        [Test]
        public void VersionTestCOnstructorWithValidInputsWorks()
        {
            int major = 1;
            int minor = 0;
            Version version = new Version(major, minor);
            Assert.IsNotNull(version);
            Assert.AreEqual(major, version.Major);
            Assert.AreEqual(minor, version.Minor);
        }
    }
}
