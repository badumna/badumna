//-----------------------------------------------------------------------
// <copyright file="UserPropertiesTests.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace DeiTests.Protocol
{
    using System;
    using System.Collections.Generic;
    using Dei;
    using NUnit.Framework;

    [TestFixture]
    internal class UserPropertiesTests
    {
        private string login;

        private string password;

        public UserPropertiesTests()
        {
        }

        [SetUp]
        public void SetUp()
        {
            this.login = "user";
            this.password = "password";
        }

        [TearDown]
        public void TearDown()
        {
            this.login = null;
            this.password = null;
        }

        [Test]
        public void UserPropertiesDefaultConstructorWorks()
        {
            UserProperties userProperties = new UserProperties();
            Assert.IsNotNull(userProperties);
        }

        [Test]
        public void UserPropertiesConstructorWithValidInputWorks()
        {
            UserProperties userProperties = new UserProperties(this.login, this.password);
            Assert.IsNotNull(userProperties);
            Assert.AreEqual(this.login, userProperties.Login);
            Assert.AreEqual(this.password, userProperties.Password);
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void SetsLoginWithNullInputShouldThrowsArgumentException()
        {
            UserProperties userProperties = new UserProperties();
            Assert.IsNotNull(userProperties);
            userProperties.Login = null;
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void SetsPasswordWithNullInputShouldThrowsArgumentException()
        {
            UserProperties userProperties = new UserProperties();
            Assert.IsNotNull(userProperties);
            userProperties.Password = null;
        }

        [Test]
        public void SetsAndGetsPropertiesWithValidInputShouldWorks()
        {
            UserProperties userProperties = new UserProperties();
            Assert.IsNotNull(userProperties);
            userProperties.Login = this.login;
            Assert.AreEqual(this.login, userProperties.Login);
            userProperties.Password = this.password;
            Assert.AreEqual(this.password, userProperties.Password);
        }
    }
}
