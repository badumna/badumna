//-----------------------------------------------------------------------
// <copyright file="RequestCharacterListTests.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2012 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace DeiTests.Protocol
{
    using System;
    using Dei;
    using NUnit.Framework;
    using Generic = System.Collections.Generic;
    using DeiTests.Protocol;
    using Badumna.Security;
    using Dei.Protocol;

    [TestFixture]
    internal class RequestCharacterListTests
    {
        public RequestCharacterListTests()
        {
        }


        [Test]
        public void TestXmlSerialzation()
        {
            Generic.List<Character> characters = new Generic.List<Character> { new Character(1, "1andonly") };

            Generic.List<Character> deserialized = XmlTestHelper.AssertCommandXmlSerialization<Generic.List<Character>>(characters);

            Assert.AreEqual(characters, deserialized);
        }
    }
}
