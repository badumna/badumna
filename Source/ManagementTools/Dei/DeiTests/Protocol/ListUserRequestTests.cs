//-----------------------------------------------------------------------
// <copyright file="ListUserRequestTests.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace DeiTests.Protocol
{
    using System;
    using Dei;
    using NUnit.Framework;

    [TestFixture]
    internal class ListUserRequestTests
    {
        public ListUserRequestTests()
        {
        }

        [Test]
        public void ListUserRequestDefaultConstructorWorks()
        {
            ListUserRequest listUserRequest = new ListUserRequest();
            Assert.IsNotNull(listUserRequest);
        }

        [Test]
        public void ListUserRequestWithValidInputsShouldWorks()
        {
            string usernameGlob = "*";
            int numberOfResults = 10;
            int limitStart = 0;
            ListUserRequest listUserRequest = new ListUserRequest(usernameGlob, numberOfResults, limitStart);
            Assert.IsNotNull(listUserRequest);
            Assert.AreEqual(usernameGlob, listUserRequest.UsernameGlob);
            Assert.AreEqual(numberOfResults, listUserRequest.NumberOfResults);
            Assert.AreEqual(limitStart, listUserRequest.LimitStart);
        }

        [Test]
        public void SetsAndGetsPropertiesWithValidInputShouldWork()
        {
            string usernameGlob = "*";
            int numberOfResults = 10;
            int limitStart = 0;
            string[] result = new string[] { "admin", "user" };

            ListUserRequest listUserRequest = new ListUserRequest();
            Assert.IsNotNull(listUserRequest);

            listUserRequest.UsernameGlob = usernameGlob;
            Assert.AreEqual(usernameGlob, listUserRequest.UsernameGlob);

            listUserRequest.NumberOfResults = numberOfResults;
            Assert.AreEqual(numberOfResults, listUserRequest.NumberOfResults);

            listUserRequest.LimitStart = limitStart;
            Assert.AreEqual(limitStart, listUserRequest.LimitStart);

            listUserRequest.Result = result;
            Assert.AreEqual(result, listUserRequest.Result);
        }
    }
}
