//-----------------------------------------------------------------------
// <copyright file="XmlReaderFactoryTests.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace DeiTests.Protocol
{
    using System.IO;
    using System.Xml;
    using Dei.Protocol;
    using NUnit.Framework;
    using Rhino.Mocks;
   
    [TestFixture]
    internal class XmlReaderFactoryTests
    {
        [Test]
        public void XmlReadearFactoryReturnsValidXmlReader()
        {
            XmlReaderFactory factory = new XmlReaderFactory();
            XmlReader xmlReader = factory.Create(MockRepository.GenerateMock<Stream>(), null);
            Assert.IsNotNull(xmlReader);
        }
    }
}
