//-----------------------------------------------------------------------
// <copyright file="ServerTimeTests.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace DeiTests.Protocol
{
    using System;
    using Dei;
    using Dei.Protocol;
    using NUnit.Framework;

    [TestFixture]
    internal class ServerTimeTests
    {
        private TimeSpan validityPeriod = TimeSpan.FromHours(24);

        public ServerTimeTests()
        {
        }

        [Test]
        public void ServerTimeDefaultConstructorWorks()
        {
            ServerTime serverTime = new ServerTime();
            Assert.IsNotNull(serverTime);
        }

        [Test]
        public void ServerTimeConstructorWithValidInputsWorks()
        {
            UtcDateTime time = new UtcDateTime(DateTime.UtcNow);
            UtcDateTime expiryTime = new UtcDateTime(time.DateTime + this.validityPeriod);
            ServerTime serverTime = new ServerTime(time, this.validityPeriod);
            Assert.IsNotNull(serverTime);
            Assert.AreEqual(time, serverTime.Time);
            Assert.AreEqual(expiryTime.Ticks, serverTime.ExpiryTime.Ticks);
        }

        [Test]
        public void SetandGetServerTimePropertiesWorks()
        {
            UtcDateTime time = new UtcDateTime(DateTime.UtcNow);
            UtcDateTime expiryTime = new UtcDateTime(time.DateTime + this.validityPeriod);

            ServerTime serverTime = new ServerTime();
            Assert.IsNotNull(serverTime);

            serverTime.ExpiryTime = expiryTime;
            Assert.AreEqual(expiryTime, serverTime.ExpiryTime);

            serverTime.Time = time;
            Assert.AreEqual(time, serverTime.Time);
        }
    }
}
