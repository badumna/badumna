//-----------------------------------------------------------------------
// <copyright file="PermissionPropertiesTests.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace DeiTests.Protocol
{
    using Dei;
    using NUnit.Framework;

    [TestFixture]
    internal class PermissionPropertiesTests
    {
        public PermissionPropertiesTests()
        {
        }

        [Test]
        public void PermissionPropertiesDefaultConstructorWorks()
        {
            PermissionProperties permissionProperties = new PermissionProperties();
            Assert.IsNotNull(permissionProperties);
        }

        [Test]
        public void PermissionPropertiesConstructorWithValidTypeWorks()
        {
            PermissionProperties permissionProperties = new PermissionProperties(PermissionTypes.Admin);
            Assert.IsNotNull(permissionProperties);
            Assert.AreEqual((long)PermissionTypes.Admin, permissionProperties.Id);
            Assert.AreEqual(PermissionTypes.Admin.ToString(), permissionProperties.Name);
        }

        [Test]
        public void PermissionPropertiesConstructorWithValidPermissionNameWorks()
        {
            PermissionProperties permissionProperties = new PermissionProperties("NewPermission");
            Assert.IsNotNull(permissionProperties);
            Assert.AreEqual("NewPermission", permissionProperties.Name);
        }

        [Test]
        public void PermissionPropertiesConstructorWithValidIdAndNameWorks()
        {
            PermissionProperties permissionProperties = new PermissionProperties(10, "NewPermission");
            Assert.IsNotNull(permissionProperties);
            Assert.AreEqual(10, permissionProperties.Id);
            Assert.AreEqual("NewPermission", permissionProperties.Name);
        }
    }
}
