//-----------------------------------------------------------------------
// <copyright file="PermissionTests.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace DeiTests.Protocol
{
    using System;
    using Dei;
    using NUnit.Framework;

    [TestFixture]
    internal class PermissionTests
    {
        public PermissionTests()
        {
        }

        [Test]
        public void PermissionTestsDefaultConstructorWorsk()
        {
            Permission permission = new Permission();
            Assert.IsNotNull(permission);
        }

        [Test]
        public void SetsAndGetsPermissionPropertiesWorks()
        {
            Random random = new Random((int)DateTime.Now.Ticks);
            string permissionName = "Participation";
            int stateLength = 160;
            byte[] state = new byte[stateLength];
            random.NextBytes(state);
            
            Permission permission = new Permission();
            Assert.IsNotNull(permission);

            permission.Name = permissionName;
            Assert.AreEqual(permissionName, permission.Name);

            permission.State = state;
            Assert.AreEqual(state, permission.State);

            permission.Signiture = state;
            Assert.AreEqual(state, permission.Signiture);
        }
    }
}
