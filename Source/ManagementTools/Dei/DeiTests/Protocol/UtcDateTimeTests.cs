//-----------------------------------------------------------------------
// <copyright file="UtcDateTimeTests.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace DeiTests.Protocol
{
    using System;
    using Dei.Protocol;
    using NUnit.Framework;

    [TestFixture]
    internal class UtcDateTimeTests
    {
        public UtcDateTimeTests()
        {
        }

        [Test]
        public void UtcDateTimeDefaultConstructorWorks()
        {
            UtcDateTime utcDateTime = new UtcDateTime();
            Assert.IsNotNull(utcDateTime);
        }

        [Test]
        public void UtcDateTimeConsructorWithValidUtcDateTimeInputWorks()
        {
            DateTime time = DateTime.UtcNow;
            UtcDateTime utcDateTime = new UtcDateTime(time);
            Assert.IsNotNull(utcDateTime);
            Assert.AreEqual(time, utcDateTime.DateTime);
            Assert.AreEqual(time.Ticks, utcDateTime.DateTime.Ticks);
        }

        [Test]
        [ExpectedException(typeof(ArgumentException))]
        public void UtcDateTimeConscructorWithInvalidDateTimeInputShouldThrowsArgumentException()
        {
            UtcDateTime utcDateTime = new UtcDateTime(DateTime.Now);
        }

        [Test]
        public void SetsAndGetsTicksPropertyWorks()
        {
            DateTime time = DateTime.UtcNow;
            UtcDateTime utcDateTime = new UtcDateTime();
            Assert.IsNotNull(utcDateTime);

            utcDateTime.Ticks = time.Ticks;
            Assert.AreEqual(time.Ticks, utcDateTime.Ticks);
        }
    }
}
