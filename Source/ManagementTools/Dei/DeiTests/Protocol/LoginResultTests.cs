//-----------------------------------------------------------------------
// <copyright file="LoginResultTests.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace DeiTests.Protocol
{
    using System;
    using Dei;
    using NUnit.Framework;
    using Generic = System.Collections.Generic;
    using DeiTests.Protocol;
    using Badumna.Security;
    using Dei.Protocol;

    [TestFixture]
    internal class LoginResultTests
    {
        public LoginResultTests()
        {
        }

        [Test]
        public void LoginResultSuccessConstructorWorks()
        {
            Generic.List<Character> characters = new Generic.List<Character> { new Character(1, "1andonly") };
            LoginResult loginResult = new LoginResult(characters);
            Assert.IsNotNull(loginResult);
            Assert.IsTrue(loginResult.WasSuccessful);
            Assert.AreEqual(characters, loginResult.Characters);
        }

        [Test]
        public void LoginResultConstructorWithValidInputWorks()
        {
            string failureDescription = "Connection error";
            LoginResult loginResult = new LoginResult(failureDescription);
            Assert.IsNotNull(loginResult);
            Assert.AreEqual(failureDescription, loginResult.ErrorDescription);
        }

        [Test]
        public void UserCancelledShouldReturnLoginResultWasNotSuccessfull()
        {
            LoginResult loginResult = LoginResult.UserCancelled;
            Assert.IsNotNull(loginResult);
            Assert.IsFalse(loginResult.WasSuccessful);
            Assert.AreEqual(Dei.DescriptionResources.UserCancelledLoginResult, loginResult.ErrorDescription);
        }

        [Test]
        public void TestXmlSerialzation()
        {
            Generic.List<Character> characters = new Generic.List<Character> { new Character(1, "1andonly") };
            LoginResult loginResult = new LoginResult(characters);

            LoginResult deserialized = XmlTestHelper.AssertCommandXmlSerialization<LoginResult>(loginResult);

            Assert.AreEqual(loginResult, deserialized);
        }
    }
}
