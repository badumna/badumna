//-----------------------------------------------------------------------
// <copyright file="ParticipationKeyTests.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace DeiTests.Protocol
{
    using System;
    using Dei;
    using Dei.Protocol;
    using NUnit.Framework;

    [TestFixture]
    internal class ParticipationKeyTests
    {
        /// <summary>
        /// Random number generator.
        /// </summary>
        private static Random random = new Random((int)DateTime.Now.Ticks);

        private TimeSpan validityPeriod = TimeSpan.FromHours(24);

        public ParticipationKeyTests()
        {
        }

        [Test]
        public void ParticipationKeyDefaultConstructorWorks()
        {
            ParticipationKey participationKey = new ParticipationKey();
            Assert.IsNotNull(participationKey);
        }

        [Test]
        public void ParticipationKeyConstructorWithValidInputShouldWorks()
        {
            byte[] key = new byte[ParticipationKey.KeyLengthBytes];
            random.NextBytes(key);

            ParticipationKey participationKey = new ParticipationKey(this.validityPeriod, key);
            Assert.IsNotNull(participationKey);
            Assert.AreEqual(key, participationKey.Key);
            Assert.IsFalse(participationKey.HasExpired);
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ParticipationKeyConstructorWithNullKeyShouldThrowsArgumentNullException()
        {
            byte[] key = null;
            ParticipationKey participationKey = new ParticipationKey(this.validityPeriod, key);
        }

        [Test]
        [ExpectedException(typeof(InvalidOperationException))]
        public void ParticipationKeyConstructorWithInvalidKeyLengthShouldThrowsInvalidOperationException()
        {
            byte[] key = new byte[ParticipationKey.KeyLengthBytes - 1];
            random.NextBytes(key);

            ParticipationKey participationKey = new ParticipationKey(this.validityPeriod, key);
        }

        [Test]
        public void GenerateRandomParticipationKeyWithValidValidtyPeriodWorks()
        {
            ParticipationKey participationKey = ParticipationKey.GenerateRandomParticipationKey(
                this.validityPeriod);
            Assert.IsNotNull(participationKey);
        }

        [Test]
        public void GetsAndSetsParticipationKeyPropertiesWorks()
        {
            byte[] key = new byte[ParticipationKey.KeyLengthBytes];
            random.NextBytes(key);
            UtcDateTime expiryTime = new UtcDateTime(DateTime.UtcNow + this.validityPeriod);

            ParticipationKey participationKey = new ParticipationKey();
            Assert.IsNotNull(participationKey);

            participationKey.Key = key;
            Assert.AreEqual(key, participationKey.Key);

            participationKey.ExpiryTime = expiryTime;
            Assert.AreEqual(expiryTime, participationKey.ExpiryTime);
        }
    }
}
