//-----------------------------------------------------------------------
// <copyright file="ClientProtocolTests.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace DeiTests.Protocol
{
    using System.Collections.Generic;
    using System.IO;
    using System.Net.Sockets;
    using System.Threading;
    using System.Xml;
    using Dei;
    using Dei.Protocol;
    using NUnit.Framework;
    using Rhino.Mocks;
    using Badumna.Security;

    [TestFixture]
    internal class ClientProtocolTests
    {
        private readonly Version version = new Version(2, 3);

        private Stream stream;

        private XmlReader xmlReader;

        private XmlWriter xmlWriter;

        private IXmlWriterFactory xmlWriterFactory;

        private IXmlReaderFactory xmlReaderFactory;

        private ISerializerWrapperFactory serializerWrapperFactory;

        private int waitTime = 5000;

        private UserProperties user;

        private System.TimeSpan keyValidityPeriod = System.TimeSpan.FromHours(24);

        private Dictionary<System.Type, ISerializerWrapper> serializers = new Dictionary<System.Type, ISerializerWrapper>();

        private FailureReasons failureReason = FailureReasons.InvalidRequest;

        public ClientProtocolTests()
        {
        }

        [SetUp]
        public void SetUp()
        {
            this.stream = MockRepository.GenerateMock<Stream>();
            this.xmlReader = MockRepository.GenerateMock<XmlReader>();
            this.xmlWriter = MockRepository.GenerateMock<XmlWriter>();

            this.xmlReaderFactory = MockRepository.GenerateMock<IXmlReaderFactory>();
            this.xmlReaderFactory.Stub(f => f.Create(null, null)).IgnoreArguments().Return(this.xmlReader);

            this.xmlWriterFactory = MockRepository.GenerateMock<IXmlWriterFactory>();
            this.xmlWriterFactory.Stub(f => f.Create(null, null)).IgnoreArguments().Return(this.xmlWriter);

            this.serializerWrapperFactory = MockRepository.GenerateMock<ISerializerWrapperFactory>();
            this.user = new UserProperties("user", "password");
        }

        [TearDown]
        public void TearDown()
        {
            this.stream = null;
            this.xmlReader = null;
            this.xmlWriter = null;
            this.xmlReaderFactory = null;
            this.xmlWriterFactory = null;
            this.serializerWrapperFactory = null;
            this.user = null;

            this.serializers.Clear();
        }

        [Test]
        public void ClientProtocolConstructorWithValidInputWorks()
        {
            ISerializerWrapper versionSerializerWrapper = this.GetSerializer(typeof(Version));
            Version version = new Version(2, 3);

            ManualResetEvent read = new ManualResetEvent(false);
            ManualResetEvent readName = new ManualResetEvent(false);
            ManualResetEvent flush = new ManualResetEvent(false);
            ManualResetEvent readVersion = new ManualResetEvent(false);
            ManualResetEvent setEndOfTest = new ManualResetEvent(false);

            this.xmlReader.Stub(s => s.Read()).Return(true).WhenCalled(x => read.Set());
            this.xmlReader.Stub(s => s.Name).Return(Resources.ConversationElementName)
                .WhenCalled(x => readName.Set());
            this.SetStubForEndOfTest(setEndOfTest);

            versionSerializerWrapper.Expect(c => c.WriteObject(this.xmlWriter, null))
                .IgnoreArguments().Repeat.Once();

            this.xmlWriter.Expect(c => c.Flush()).WhenCalled(x => flush.Set());
            versionSerializerWrapper.Stub(s => s.CanDeserialize(this.xmlReader)).Return(true);
            versionSerializerWrapper.Stub(s => s.ReadObject(this.xmlReader))
                .Return((object)version).WhenCalled(x => readVersion.Set());

            ClientProtocol clientProtocol = new ClientProtocol(
                this.stream,
                this.xmlWriterFactory,
                this.xmlReaderFactory,
                this.serializerWrapperFactory);

            Assert.IsNotNull(clientProtocol);
            Assert.IsTrue(read.WaitOne(this.waitTime));
            Assert.IsTrue(readName.WaitOne(this.waitTime));
            Assert.IsTrue(flush.WaitOne(this.waitTime));
            Assert.IsTrue(readVersion.WaitOne(this.waitTime));
            Assert.IsTrue(setEndOfTest.WaitOne(this.waitTime));

            this.xmlWriter.VerifyAllExpectations();
            versionSerializerWrapper.VerifyAllExpectations();
        }

        [Test]
        public void OnLoginAcceptedIsCurrentlyLoggedInPropertyShouldReturnTrue()
        {
            this.SetStubsForClientProtocolInitiation();

            ManualResetEvent setEndOfTest = new ManualResetEvent(false);
            this.SetStubsForReadingFromSerializerWrapper(typeof(Commands), Commands.RequestLoginAccepted, null);
            LoginResult loginResult = new LoginResult(new List<Character>());
            this.SetStubsForReadingFromSerializerWrapper(typeof(LoginResult), loginResult, null);
            this.SetStubForEndOfTest(setEndOfTest);

            ClientProtocol clientProtocol = new ClientProtocol(
                this.stream,
                this.xmlWriterFactory,
                this.xmlReaderFactory,
                this.serializerWrapperFactory);

            Assert.IsTrue(setEndOfTest.WaitOne(this.waitTime));
            Assert.IsTrue(clientProtocol.IsCurrentlyLoggedIn);
        }

        [Test]
        public void OnLoginRejectedIsCurrentlyLoggedInPropertyShouldReturnFalse()
        {
            this.SetStubsForClientProtocolInitiation();

            ManualResetEvent setEndOfTest = new ManualResetEvent(false);
            this.SetStubsForReadingFromSerializerWrapper(typeof(Commands), Commands.RequestLoginRejected, null);
            this.SetExpectationForClosingConnection(setEndOfTest);

            ClientProtocol clientProtocol = new ClientProtocol(
                this.stream,
                this.xmlWriterFactory,
                this.xmlReaderFactory,
                this.serializerWrapperFactory);

            Assert.IsTrue(setEndOfTest.WaitOne(this.waitTime));
            Assert.IsFalse(clientProtocol.IsCurrentlyLoggedIn);
        }

        [Test]
        public void OnLogoutAcceptedIsCurrentlyLoggedInPropertyShouldReturnFalse()
        {
            this.SetStubsForClientProtocolInitiation();

            ManualResetEvent setEndOfTest = new ManualResetEvent(false);
            this.SetStubsForReadingFromSerializerWrapper(typeof(Commands), Commands.RequestLogoutAccepted, null);
            this.SetExpectationForClosingConnection(setEndOfTest);

            ClientProtocol clientProtocol = new ClientProtocol(
                this.stream,
                this.xmlWriterFactory,
                this.xmlReaderFactory,
                this.serializerWrapperFactory);

            Assert.IsTrue(setEndOfTest.WaitOne(this.waitTime));
            Assert.IsFalse(clientProtocol.IsCurrentlyLoggedIn);
        }

        [Test]
        public void OnLogoutFailedWithFailureReasonShouldSetClientProtocolFailureReasonCorrectly()
        {
            this.SetStubsForClientProtocolInitiation();

            ManualResetEvent setEndOfTest = new ManualResetEvent(false);
            this.SetStubsForReadingFromSerializerWrapper(typeof(Commands), Commands.RequestLogoutFailed, null);
            this.SetStubsForReadingFromSerializerWrapper(typeof(FailureReasons), this.failureReason, null);
            this.SetExpectationForClosingConnection(setEndOfTest);

            ClientProtocol clientProtocol = new ClientProtocol(
               this.stream,
               this.xmlWriterFactory,
               this.xmlReaderFactory,
               this.serializerWrapperFactory);

            Assert.IsTrue(setEndOfTest.WaitOne(this.waitTime));
            Assert.IsFalse(clientProtocol.IsCurrentlyLoggedIn);
            Assert.AreEqual(this.failureReason, clientProtocol.FailureReason);
        }

        [Test]
        public void ShouldBeConnectionErrorFailureWhenLogoutCalledOnAnEmptyStream()
        {
            this.SetStubsForClientProtocolInitiation();

            ClientProtocol clientProtocol = new ClientProtocol(
               this.stream,
               this.xmlWriterFactory,
               this.xmlReaderFactory,
               this.serializerWrapperFactory);

            this.TriggerConversationEnd(clientProtocol);

            clientProtocol.SendRequest(Commands.RequestLogin);

            Assert.AreEqual(FailureReasons.ConnectionError, clientProtocol.FailureReason);
        }

        [Test]
        public void OnNewAccountAcceptedShouldBehaveCorrectlyAndThrowsNoException()
        {
            // some of the value can't be checked, only general behaviour can be checked.
            this.SetStubsForClientProtocolInitiation();

            ManualResetEvent setEndOfTest = new ManualResetEvent(false);
            this.SetStubsForReadingFromSerializerWrapper(typeof(Commands), Commands.RequestNewAccountAccepted, null);
            this.SetStubsForReadingFromSerializerWrapper(typeof(UserProperties), this.user, null);
            this.SetStubForEndOfTest(setEndOfTest);

            ClientProtocol clientProtocol = new ClientProtocol(
               this.stream,
               this.xmlWriterFactory,
               this.xmlReaderFactory,
               this.serializerWrapperFactory);

            Assert.IsTrue(setEndOfTest.WaitOne(this.waitTime));
        }

        [Test]
        public void OnNewAccountRejectedWithFailureReasonShouldSetClientProtocolFailureReasonCorrectly()
        {
            this.SetStubsForClientProtocolInitiation();

            ManualResetEvent setEndOfTest = new ManualResetEvent(false);
            this.SetStubsForReadingFromSerializerWrapper(typeof(Commands), Commands.RequestNewAccountFailed, null);
            this.SetStubsForReadingFromSerializerWrapper(typeof(FailureReasons), this.failureReason, null);
            this.SetStubForEndOfTest(setEndOfTest);

            ClientProtocol clientProtocol = new ClientProtocol(
               this.stream,
               this.xmlWriterFactory,
               this.xmlReaderFactory,
               this.serializerWrapperFactory);

            Assert.IsTrue(setEndOfTest.WaitOne(this.waitTime));
            Assert.AreEqual(this.failureReason, clientProtocol.FailureReason);
        }

        [Test]
        public void OnDeleteAccountAcceptedShouldBehaveCorrectlyAndThrowsNoException()
        {
            // some of the value can't be checked, only general behaviour can be checked.
            this.SetStubsForClientProtocolInitiation();

            ManualResetEvent setEndOfTest = new ManualResetEvent(false);
            this.SetStubsForReadingFromSerializerWrapper(typeof(Commands), Commands.RequestDeleteAccountAccepted, null);
            this.SetStubForEndOfTest(setEndOfTest);

            ClientProtocol clientProtocol = new ClientProtocol(
               this.stream,
               this.xmlWriterFactory,
               this.xmlReaderFactory,
               this.serializerWrapperFactory);

            Assert.IsTrue(setEndOfTest.WaitOne(this.waitTime));
        }

        [Test]
        public void OnDeleteAccountFailedWithFailureReasonShouldSetClientProtocolFailureReasonCorrectly()
        {
            this.SetStubsForClientProtocolInitiation();

            ManualResetEvent setEndOfTest = new ManualResetEvent(false);
            this.SetStubsForReadingFromSerializerWrapper(typeof(Commands), Commands.RequestDeleteAccountFailed, null);
            this.SetStubsForReadingFromSerializerWrapper(typeof(FailureReasons), this.failureReason, null);
            this.SetStubForEndOfTest(setEndOfTest);

            ClientProtocol clientProtocol = new ClientProtocol(
               this.stream,
               this.xmlWriterFactory,
               this.xmlReaderFactory,
               this.serializerWrapperFactory);

            Assert.IsTrue(setEndOfTest.WaitOne(this.waitTime));
            Assert.AreEqual(this.failureReason, clientProtocol.FailureReason);
        }

        [Test]
        public void OnChangeAccountAcceptedShouldBehaveCorrectlyAndThrowsNoException()
        {
            this.SetStubsForClientProtocolInitiation();

            ManualResetEvent setEndOfTest = new ManualResetEvent(false);
            this.SetStubsForReadingFromSerializerWrapper(typeof(Commands), Commands.RequestChangeAccountAccepted, null);
            this.SetStubForEndOfTest(setEndOfTest);

            ClientProtocol clientProtocol = new ClientProtocol(
               this.stream,
               this.xmlWriterFactory,
               this.xmlReaderFactory,
               this.serializerWrapperFactory);

            Assert.IsTrue(setEndOfTest.WaitOne(this.waitTime));
        }

        [Test]
        public void OnChangeAccountRejectedWithFailureReasonShouldSetClientProtocolFailureReasonCorrectly()
        {
            this.SetStubsForClientProtocolInitiation();

            ManualResetEvent setEndOfTest = new ManualResetEvent(false);
            this.SetStubsForReadingFromSerializerWrapper(typeof(Commands), Commands.RequestChangeAccountRejected, null);
            this.SetStubsForReadingFromSerializerWrapper(typeof(FailureReasons), this.failureReason, null);
            this.SetStubForEndOfTest(setEndOfTest);

            ClientProtocol clientProtocol = new ClientProtocol(
               this.stream,
               this.xmlWriterFactory,
               this.xmlReaderFactory,
               this.serializerWrapperFactory);

            Assert.IsTrue(setEndOfTest.WaitOne(this.waitTime));
            Assert.AreEqual(this.failureReason, clientProtocol.FailureReason);
        }

        [Test]
        public void OnCreatePermissionAcceptedShouldBehaveCorrectlyAndThrowsNoException()
        {
            PermissionProperties permission = new PermissionProperties(
                (long)PermissionTypes.Admin, 
                PermissionTypes.Admin.ToString());
            
            this.SetStubsForClientProtocolInitiation();

            ManualResetEvent setEndOfTest = new ManualResetEvent(false);
            this.SetStubsForReadingFromSerializerWrapper(typeof(Commands), Commands.RequestCreatePermissionAccepted, null);
            this.SetStubsForReadingFromSerializerWrapper(typeof(PermissionProperties), permission, null);
            this.SetStubForEndOfTest(setEndOfTest);

            ClientProtocol clientProtocol = new ClientProtocol(
               this.stream,
               this.xmlWriterFactory,
               this.xmlReaderFactory,
               this.serializerWrapperFactory);

            Assert.IsTrue(setEndOfTest.WaitOne(this.waitTime));
        }

        [Test]
        public void OnCreatePermissionFailedWithFailureReasonShouldSetClientProtocolFailureReasonCorrectly()
        {
            this.SetStubsForClientProtocolInitiation();

            ManualResetEvent setEndOfTest = new ManualResetEvent(false);
            this.SetStubsForReadingFromSerializerWrapper(typeof(Commands), Commands.RequestCreatePermissionRejected, null);
            this.SetStubsForReadingFromSerializerWrapper(typeof(FailureReasons), this.failureReason, null);
            this.SetStubForEndOfTest(setEndOfTest);

            ClientProtocol clientProtocol = new ClientProtocol(
               this.stream,
               this.xmlWriterFactory,
               this.xmlReaderFactory,
               this.serializerWrapperFactory);

            Assert.IsTrue(setEndOfTest.WaitOne(this.waitTime));
            Assert.AreEqual(this.failureReason, clientProtocol.FailureReason);
        }

        [Test]
        public void OnRemovePermissionAcceptedShouldBehaveCorrectlyAndThrowsNoException()
        {
            this.SetStubsForClientProtocolInitiation();

            ManualResetEvent setEndOfTest = new ManualResetEvent(false);

            this.SetStubsForReadingFromSerializerWrapper(typeof(Commands), Commands.RequestRemovePermissionAccepted, null);
            this.SetStubForEndOfTest(setEndOfTest);

            ClientProtocol clientProtocol = new ClientProtocol(
               this.stream,
               this.xmlWriterFactory,
               this.xmlReaderFactory,
               this.serializerWrapperFactory);
      
            Assert.IsTrue(setEndOfTest.WaitOne(this.waitTime));
        }

        [Test]
        public void OnRemovePermissionFailedWithFailureReasonShouldSetClientProtocolFailureReasonCorrectly()
        {
            this.SetStubsForClientProtocolInitiation();

            ManualResetEvent setEndOfTest = new ManualResetEvent(false);
            this.SetStubsForReadingFromSerializerWrapper(typeof(Commands), Commands.RequestRemovePermissionRejected, null);
            this.SetStubsForReadingFromSerializerWrapper(typeof(FailureReasons), this.failureReason, null);
            this.SetStubForEndOfTest(setEndOfTest);

            ClientProtocol clientProtocol = new ClientProtocol(
               this.stream,
               this.xmlWriterFactory,
               this.xmlReaderFactory,
               this.serializerWrapperFactory);

            Assert.IsTrue(setEndOfTest.WaitOne(this.waitTime));
            Assert.AreEqual(this.failureReason, clientProtocol.FailureReason);
        }

        [Test]
        public void OnGrantPermissionAcceptedShouldBehaveCorrectlyAndThrowsNoException()
        {
            this.SetStubsForClientProtocolInitiation();

            ManualResetEvent setEndOfTest = new ManualResetEvent(false);

            this.SetStubsForReadingFromSerializerWrapper(typeof(Commands), Commands.RequestGrantPermissionAccepted, null);
            this.SetStubForEndOfTest(setEndOfTest);

            ClientProtocol clientProtocol = new ClientProtocol(
               this.stream,
               this.xmlWriterFactory,
               this.xmlReaderFactory,
               this.serializerWrapperFactory);

            Assert.IsTrue(setEndOfTest.WaitOne(this.waitTime));
        }

        [Test]
        public void OnGrantPermissionRejectedWithFailureReasonShouldSetClientProtocolFailureReasonCorrectly()
        {
            this.SetStubsForClientProtocolInitiation();

            ManualResetEvent setEndOfTest = new ManualResetEvent(false);
            this.SetStubsForReadingFromSerializerWrapper(typeof(Commands), Commands.RequestGrantPermissionRejected, null);
            this.SetStubsForReadingFromSerializerWrapper(typeof(FailureReasons), this.failureReason, null);
            this.SetStubForEndOfTest(setEndOfTest);

            ClientProtocol clientProtocol = new ClientProtocol(
               this.stream,
               this.xmlWriterFactory,
               this.xmlReaderFactory,
               this.serializerWrapperFactory);

            Assert.IsTrue(setEndOfTest.WaitOne(this.waitTime));
            Assert.AreEqual(this.failureReason, clientProtocol.FailureReason);
        }

        [Test]
        public void OnRevokePermissionAcceptedShouldBehaveCorrectlyAndThrowsNoException()
        {
            this.SetStubsForClientProtocolInitiation();

            ManualResetEvent setEndOfTest = new ManualResetEvent(false);

            this.SetStubsForReadingFromSerializerWrapper(typeof(Commands), Commands.RequestRevokePermissionAccepted, null);
            this.SetStubForEndOfTest(setEndOfTest);

            ClientProtocol clientProtocol = new ClientProtocol(
               this.stream,
               this.xmlWriterFactory,
               this.xmlReaderFactory,
               this.serializerWrapperFactory);

            Assert.IsTrue(setEndOfTest.WaitOne(this.waitTime));
        }

        [Test]
        public void OnRevokePermissionFailedWithFailureReasonShouldSetClientProtocolFailureReasonCorrectly()
        {
            this.SetStubsForClientProtocolInitiation();

            ManualResetEvent setEndOfTest = new ManualResetEvent(false);
            this.SetStubsForReadingFromSerializerWrapper(typeof(Commands), Commands.RequestRevokePermissionRejected, null);
            this.SetStubsForReadingFromSerializerWrapper(typeof(FailureReasons), this.failureReason, null);
            this.SetStubForEndOfTest(setEndOfTest);

            ClientProtocol clientProtocol = new ClientProtocol(
               this.stream,
               this.xmlWriterFactory,
               this.xmlReaderFactory,
               this.serializerWrapperFactory);

            Assert.IsTrue(setEndOfTest.WaitOne(this.waitTime));
            Assert.AreEqual(this.failureReason, clientProtocol.FailureReason);
        }

        [Test]
        public void OnClientCertificateAcceptedShouldBehaveCorrectlyAndThrowsNoException()
        {
            this.SetStubsForClientProtocolInitiation();

            ManualResetEvent setEndOfTest = new ManualResetEvent(false);

            this.SetStubsForReadingFromSerializerWrapper(typeof(Commands), Commands.RequestClientCertificateAccepted, null);
            this.SetStubsForReadingFromSerializerWrapper(typeof(UserCertificate), new UserCertificate(), null);
            this.SetStubForEndOfTest(setEndOfTest);

            ClientProtocol clientProtocol = new ClientProtocol(
               this.stream,
               this.xmlWriterFactory,
               this.xmlReaderFactory,
               this.serializerWrapperFactory);

            Assert.IsTrue(setEndOfTest.WaitOne(this.waitTime));
        }

        [Test]
        public void OnClientCertificateFailedWithFailureReasonShouldSetClientProtocolFailureReasonCorrectly()
        {
            this.SetStubsForClientProtocolInitiation();

            ManualResetEvent setEndOfTest = new ManualResetEvent(false);
            this.SetStubsForReadingFromSerializerWrapper(typeof(Commands), Commands.RequestClientCertificateFailed, null);
            this.SetStubsForReadingFromSerializerWrapper(typeof(FailureReasons), this.failureReason, null);
            this.SetStubForEndOfTest(setEndOfTest);

            ClientProtocol clientProtocol = new ClientProtocol(
               this.stream,
               this.xmlWriterFactory,
               this.xmlReaderFactory,
               this.serializerWrapperFactory);

            Assert.IsTrue(setEndOfTest.WaitOne(this.waitTime));
            Assert.AreEqual(this.failureReason, clientProtocol.FailureReason);
        }

        [Test]
        public void OnAddPermissionShouldBehaveCorrectlyAndThrowsNoException()
        {
            this.SetStubsForClientProtocolInitiation();

            ManualResetEvent setEndOfTest = new ManualResetEvent(false);

            this.SetStubsForReadingFromSerializerWrapper(typeof(Commands), Commands.AddPermission, null);
            this.SetStubsForReadingFromSerializerWrapper(typeof(Permission), new Permission(), null);
            this.SetStubForEndOfTest(setEndOfTest);

            ClientProtocol clientProtocol = new ClientProtocol(
               this.stream,
               this.xmlWriterFactory,
               this.xmlReaderFactory,
               this.serializerWrapperFactory);

            Assert.IsTrue(setEndOfTest.WaitOne(this.waitTime));
        }

        [Test]
        public void OnPermissionListFailedWithFailureReasonShouldSetClientProtocolFailureReasonCorrectly()
        {
            this.SetStubsForClientProtocolInitiation();

            ManualResetEvent setEndOfTest = new ManualResetEvent(false);
            this.SetStubsForReadingFromSerializerWrapper(typeof(Commands), Commands.RequestPermissionListFailed, null);
            this.SetStubsForReadingFromSerializerWrapper(typeof(FailureReasons), this.failureReason, null);
            this.SetStubForEndOfTest(setEndOfTest);

            ClientProtocol clientProtocol = new ClientProtocol(
               this.stream,
               this.xmlWriterFactory,
               this.xmlReaderFactory,
               this.serializerWrapperFactory);

            Assert.IsTrue(setEndOfTest.WaitOne(this.waitTime));
            Assert.AreEqual(this.failureReason, clientProtocol.FailureReason);
        }

        [Test]
        public void OnAddPermissionTypeShouldBehaveCorrectlyAndThrowsNoException()
        {
            this.SetStubsForClientProtocolInitiation();

            ManualResetEvent setEndOfTest = new ManualResetEvent(false);

            this.SetStubsForReadingFromSerializerWrapper(typeof(Commands), Commands.AddPermissionType, null);
            this.SetStubsForReadingFromSerializerWrapper(
                typeof(PermissionProperties), 
                new PermissionProperties(), 
                null);

            this.SetStubForEndOfTest(setEndOfTest);

            ClientProtocol clientProtocol = new ClientProtocol(
               this.stream,
               this.xmlWriterFactory,
               this.xmlReaderFactory,
               this.serializerWrapperFactory);

            Assert.IsTrue(setEndOfTest.WaitOne(this.waitTime));
        }

        [Test]
        public void OnPermissionListTypesFailedWithFailureReasonShouldSetClientProtocolFailureReasonCorrectly()
        {
            this.SetStubsForClientProtocolInitiation();

            ManualResetEvent setEndOfTest = new ManualResetEvent(false);
            this.SetStubsForReadingFromSerializerWrapper(typeof(Commands), Commands.RequestListPermissionTypesFailed, null);
            this.SetStubsForReadingFromSerializerWrapper(typeof(FailureReasons), this.failureReason, null);
            this.SetStubForEndOfTest(setEndOfTest);

            ClientProtocol clientProtocol = new ClientProtocol(
               this.stream,
               this.xmlWriterFactory,
               this.xmlReaderFactory,
               this.serializerWrapperFactory);

            Assert.IsTrue(setEndOfTest.WaitOne(this.waitTime));
            Assert.AreEqual(this.failureReason, clientProtocol.FailureReason);
        }

        [Test]
        public void OnParticipationKeyAcceptedShouldBehaveCorrectlyAndThrowsNoException()
        {
            this.SetStubsForClientProtocolInitiation();

            ManualResetEvent setEndOfTest = new ManualResetEvent(false);

            this.SetStubsForReadingFromSerializerWrapper(typeof(Commands), Commands.RequestParticipationKeyAccepted, null);
            this.SetStubsForReadingFromSerializerWrapper(typeof(ParticipationKey), new ParticipationKey(), null);
            this.SetStubForEndOfTest(setEndOfTest);

            ClientProtocol clientProtocol = new ClientProtocol(
               this.stream,
               this.xmlWriterFactory,
               this.xmlReaderFactory,
               this.serializerWrapperFactory);

            Assert.IsTrue(setEndOfTest.WaitOne(this.waitTime));
        }

        [Test]
        public void OnParticipationKeyRejectedWithFailureReasonShouldSetClientProtocolFailureReasonCorrectly()
        {
            this.SetStubsForClientProtocolInitiation();

            ManualResetEvent setEndOfTest = new ManualResetEvent(false);
            this.SetStubsForReadingFromSerializerWrapper(typeof(Commands), Commands.RequestParticipationKeyRejected, null);
            this.SetStubsForReadingFromSerializerWrapper(typeof(FailureReasons), this.failureReason, null);
            this.SetStubForEndOfTest(setEndOfTest);

            ClientProtocol clientProtocol = new ClientProtocol(
               this.stream,
               this.xmlWriterFactory,
               this.xmlReaderFactory,
               this.serializerWrapperFactory);

            Assert.IsTrue(setEndOfTest.WaitOne(this.waitTime));
            Assert.AreEqual(this.failureReason, clientProtocol.FailureReason);
        }

        [Test]
        public void OnServerKeyAcceptedShouldBehaveCorrectlyAndThrowsNoException()
        {
            this.SetStubsForClientProtocolInitiation();

            ManualResetEvent setEndOfTest = new ManualResetEvent(false);

            this.SetStubsForReadingFromSerializerWrapper(typeof(Commands), Commands.RequestServersPublicKeyAccepted, null);
            this.SetStubsForReadingFromSerializerWrapper(typeof(PublicKey), new PublicKey(), null);
            this.SetStubForEndOfTest(setEndOfTest);

            ClientProtocol clientProtocol = new ClientProtocol(
               this.stream,
               this.xmlWriterFactory,
               this.xmlReaderFactory,
               this.serializerWrapperFactory);

            Assert.IsTrue(setEndOfTest.WaitOne(this.waitTime));
        }

        [Test]
        public void OnServerKeyRejectedWithFailureReasonShouldSetClientProtocolFailureReasonCorrectly()
        {
            this.SetStubsForClientProtocolInitiation();

            ManualResetEvent setEndOfTest = new ManualResetEvent(false);
            this.SetStubsForReadingFromSerializerWrapper(typeof(Commands), Commands.RequestServersPublicKeyRejected, null);
            this.SetStubsForReadingFromSerializerWrapper(typeof(FailureReasons), this.failureReason, null);
            this.SetStubForEndOfTest(setEndOfTest);

            ClientProtocol clientProtocol = new ClientProtocol(
               this.stream,
               this.xmlWriterFactory,
               this.xmlReaderFactory,
               this.serializerWrapperFactory);

            Assert.IsTrue(setEndOfTest.WaitOne(this.waitTime));
            Assert.AreEqual(this.failureReason, clientProtocol.FailureReason);
        }

        [Test]
        public void OnServerTimeAcceptedShouldBehaveCorrectlyAndThrowsNoException()
        {
            this.SetStubsForClientProtocolInitiation();

            ManualResetEvent setEndOfTest = new ManualResetEvent(false);

            this.SetStubsForReadingFromSerializerWrapper(typeof(Commands), Commands.RequestServerTimeAccepted, null);
            this.SetStubsForReadingFromSerializerWrapper(typeof(ServerTime), new ServerTime(), null);
            this.SetStubForEndOfTest(setEndOfTest);

            ClientProtocol clientProtocol = new ClientProtocol(
               this.stream,
               this.xmlWriterFactory,
               this.xmlReaderFactory,
               this.serializerWrapperFactory);

            Assert.IsTrue(setEndOfTest.WaitOne(this.waitTime));
        }

        [Test]
        public void OnServerTimeRejectedWithFailureReasonShouldSetClientProtocolFailureReasonCorrectly()
        {
            this.SetStubsForClientProtocolInitiation();

            ManualResetEvent setEndOfTest = new ManualResetEvent(false);
            this.SetStubsForReadingFromSerializerWrapper(typeof(Commands), Commands.RequestServerTimeRejected, null);
            this.SetStubsForReadingFromSerializerWrapper(typeof(FailureReasons), this.failureReason, null);
            this.SetStubForEndOfTest(setEndOfTest);

            ClientProtocol clientProtocol = new ClientProtocol(
               this.stream,
               this.xmlWriterFactory,
               this.xmlReaderFactory,
               this.serializerWrapperFactory);

            Assert.IsTrue(setEndOfTest.WaitOne(this.waitTime));
            Assert.AreEqual(this.failureReason, clientProtocol.FailureReason);
        }

        [Test]
        public void OnListUserCompleteShouldBehaveCorrectlyAndThrowsNoException()
        {
            this.SetStubsForClientProtocolInitiation();

            ManualResetEvent setEndOfTest = new ManualResetEvent(false);

            this.SetStubsForReadingFromSerializerWrapper(typeof(Commands), Commands.RequestUserListComplete, null);
            this.SetStubsForReadingFromSerializerWrapper(typeof(ListUserRequest), new ListUserRequest(), null);
            this.SetStubForEndOfTest(setEndOfTest);

            ClientProtocol clientProtocol = new ClientProtocol(
               this.stream,
               this.xmlWriterFactory,
               this.xmlReaderFactory,
               this.serializerWrapperFactory);

            Assert.IsTrue(setEndOfTest.WaitOne(this.waitTime));
        }

        [Test]
        public void OnListUserFailedWithFailureReasonShouldSetClientProtocolFailureReasonCorrectly()
        {
            this.SetStubsForClientProtocolInitiation();

            ManualResetEvent setEndOfTest = new ManualResetEvent(false);
            this.SetStubsForReadingFromSerializerWrapper(typeof(Commands), Commands.RequestUserListFailed, null);
            this.SetStubsForReadingFromSerializerWrapper(typeof(FailureReasons), this.failureReason, null);
            this.SetStubForEndOfTest(setEndOfTest);

            ClientProtocol clientProtocol = new ClientProtocol(
               this.stream,
               this.xmlWriterFactory,
               this.xmlReaderFactory,
               this.serializerWrapperFactory);

            Assert.IsTrue(setEndOfTest.WaitOne(this.waitTime));
            Assert.AreEqual(this.failureReason, clientProtocol.FailureReason);
        }

        [Test]
        public void OnUserCountCompleteShouldBehaveCorrectlyAndThrowsNoException()
        {
            this.SetStubsForClientProtocolInitiation();

            ManualResetEvent setEndOfTest = new ManualResetEvent(false);

            this.SetStubsForReadingFromSerializerWrapper(typeof(Commands), Commands.RequestUserCountComplete, null);
            this.SetStubsForReadingFromSerializerWrapper(typeof(ListUserRequest), new ListUserRequest(), null);
            this.SetStubForEndOfTest(setEndOfTest);

            ClientProtocol clientProtocol = new ClientProtocol(
               this.stream,
               this.xmlWriterFactory,
               this.xmlReaderFactory,
               this.serializerWrapperFactory);

            Assert.IsTrue(setEndOfTest.WaitOne(this.waitTime));
        }

        [Test]
        public void OnUserCountFailedWithFailureReasonShouldSetClientProtocolFailureReasonCorrectly()
        {
            this.SetStubsForClientProtocolInitiation();

            ManualResetEvent setEndOfTest = new ManualResetEvent(false);
            this.SetStubsForReadingFromSerializerWrapper(typeof(Commands), Commands.RequestUserCountFailed, null);
            this.SetStubsForReadingFromSerializerWrapper(typeof(FailureReasons), this.failureReason, null);
            this.SetStubForEndOfTest(setEndOfTest);

            ClientProtocol clientProtocol = new ClientProtocol(
               this.stream,
               this.xmlWriterFactory,
               this.xmlReaderFactory,
               this.serializerWrapperFactory);

            Assert.IsTrue(setEndOfTest.WaitOne(this.waitTime));
            Assert.AreEqual(this.failureReason, clientProtocol.FailureReason);
        }

        private void SetStubsForClientProtocolInitiation()
        {
            ISerializerWrapper versionSerializerWrapper = this.GetSerializer(typeof(Version));

            this.xmlReader.Stub(s => s.Read()).Return(true);
            this.xmlReader.Stub(s => s.Name).Return(Resources.ConversationElementName);
            this.xmlReader.Stub(s => s.EOF).Return(false).Repeat.Once();

            versionSerializerWrapper.Stub(s => s.CanDeserialize(this.xmlReader))
                .Return(true);
            versionSerializerWrapper.Stub(s => s.ReadObject(this.xmlReader))
                .Return((object)this.version);
        }

        private void SetStubsForReadingFromSerializerWrapper(System.Type type, object value, ManualResetEvent eventSet)
        {
            ISerializerWrapper serializer = this.GetSerializer(type);

            serializer.Stub(s => s.CanDeserialize(this.xmlReader)).Return(true);
         
            if (eventSet != null)
            {
                serializer.Stub(s => s.ReadObject(this.xmlReader)).Return(value).WhenCalled(x => eventSet.Set());
            }
            else
            {
                serializer.Stub(s => s.ReadObject(this.xmlReader)).Return(value);
            }
        }

        private void SetStubForEndOfTest(ManualResetEvent eventSet)
        {
            this.xmlReader.Stub(s => s.EOF).Return(true).WhenCalled(x => eventSet.Set());
        }

        private void SetExpectationForClosingConnection(ManualResetEvent eventSet)
        {
            if (eventSet != null)
            {
                this.xmlWriter.Expect(c => c.WriteFullEndElement())
                    .Repeat.Once().WhenCalled(x => eventSet.Set());
            }
            else
            {
                this.xmlWriter.Expect(c => c.WriteFullEndElement()).Repeat.Once();
            }
        }

        private void SetExpectationForWritingObjectToSerializerWrapper(System.Type type, object value, bool ignoreArgument)
        {
            ISerializerWrapper serializer = this.GetSerializer(type);

            if (ignoreArgument)
            {
                serializer.Expect(c => c.WriteObject(this.xmlWriter, value)).IgnoreArguments().Repeat.Once();
            }
            else
            {
                serializer.Expect(c => c.WriteObject(this.xmlWriter, value)).Repeat.Once();
            }
        }

        private void TriggerConversationEnd(ClientProtocol client)
        {
            this.GetSerializer(typeof(Dummy)).Stub(s => s.CanDeserialize(this.xmlReader)).Return(false);
            this.xmlReader.Stub(r => r.Name).Return(Resources.ConversationElementName);
            Assert.IsNull(client.GetParameter<Dummy>(this.xmlReader));
        }

        private class Dummy
        {
        }

        private ISerializerWrapper GetSerializer(System.Type type)
        {
            ISerializerWrapper serializer = null;
            if (!this.serializers.TryGetValue(type, out serializer))
            {
                serializer = MockRepository.GenerateMock<ISerializerWrapper>();
                this.serializers.Add(type, serializer);
                this.serializerWrapperFactory.Stub(f => f.Create(type)).Return(serializer);
            }

            return serializer;
        }
    }
}
