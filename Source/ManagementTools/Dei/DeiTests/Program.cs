//-----------------------------------------------------------------------
// <copyright file="Program.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace DeiTests
{
    using System;
    using System.Reflection;

    /// <summary>
    /// A program to launch the NUnit GUI and run the unit tests in this assembly.
    /// </summary>
    internal class Program
    {
        /// <summary>
        /// The entry point for the program.
        /// </summary>
        /// <param name="args">Command line arguments (pass "GUI" to use GUI).</param>
        [STAThread]
        internal static void Main(string[] args)
        {
            if (args.Length > 0 && args[0].ToUpper() == "GUI")
            {
                NUnit.Gui.AppEntry.Main(
                    new string[]
                    {
                        ////"/fixture:DeiTests.Protocol.XmlReaderFactoryTests",
                        Assembly.GetExecutingAssembly().Location,
                        //// "/exclude:Integration",
                        "/run"
                    });
            }
            else
            {
                NUnit.ConsoleRunner.Runner.Main(
                    new string[] { Assembly.GetExecutingAssembly().Location, "/wait" });
            }
        }
    }
}
