//-----------------------------------------------------------------------
// <copyright file="ProtocolExceptionTests.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace DeiTests
{
    using System;
    using Dei;
    using NUnit.Framework;

    [TestFixture]
    internal class ProtocolExceptionTests
    {
        public ProtocolExceptionTests()
        {
        }

        [Test]
        public void ProtocolExceptionDefaultConstructorWorks()
        {
            ProtocolException protocolException = new ProtocolException();
            Assert.IsNotNull(protocolException);
        }

        [Test]
        public void ProtocolExceptionConstructorWithValidMessageWorks()
        {
            string message = "Protocol exception message";
            ProtocolException protocolException = new ProtocolException(message);
            Assert.IsNotNull(protocolException);
            Assert.AreEqual(message, protocolException.Message);
        }

        [Test]
        public void ProtocolExceptionConstructorWithValidInnerExceptionWorks()
        {
            string message = "Protocol exception message";
            string innerExceptionMessage = "Inner Exception";
            Exception innerException = new Exception(innerExceptionMessage);
            Assert.IsNotNull(innerException);
            ProtocolException protocolException = new ProtocolException(message, innerException);
            Assert.IsNotNull(protocolException);
            Assert.AreEqual(message, protocolException.Message);
            Assert.IsNotNull(protocolException.InnerException);
            Assert.AreEqual(innerExceptionMessage, protocolException.InnerException.Message);
        }
    }
}
