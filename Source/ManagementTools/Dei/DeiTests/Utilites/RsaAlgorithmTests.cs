//-----------------------------------------------------------------------
// <copyright file="RsaAlgorithmTests.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace DeiTests.Utilites
{
    using System;
    using System.IO;
    using Dei.Utilities;
    using NUnit.Framework;

    [TestFixture]
    internal class RsaAlgorithmTests
    {
        private string privateKeyPath = "privateKeyTest.xml";

        private string publicKeyPath = "publicKeyTest.xml";
        
        public RsaAlgorithmTests()
        {
        }

        [Test]
        public void RsaAlgorithmDefaultConstructorAndDestructorWorksNoExceptionThrows()
        {
            RsaAlgorithm rsaAlgorithm = new RsaAlgorithm();
            Assert.IsNotNull(rsaAlgorithm);
            rsaAlgorithm.Dispose();
        }

        [Test]
        public void RsaAlgorithmConstructorWithValidInputPathWorks()
        {
            RsaAlgorithm rsaAlgorithm = new RsaAlgorithm(this.publicKeyPath, this.privateKeyPath);
            Assert.IsNotNull(rsaAlgorithm);
            Assert.AreEqual(this.privateKeyPath, rsaAlgorithm.PrivateKeyPath);
            Assert.AreEqual(this.publicKeyPath, rsaAlgorithm.PublicKeyPath);
        }

        [TestCase(new object[] { null, "privateKeyTest.xml" })]
        [TestCase(new object[] { "publicKeyTest.xml", null })]
        [ExpectedException(typeof(ArgumentNullException))]
        public void RsaAlgorithmConstructorWithInvalidInputPathShouldThrowsArgumentNullException(
            string publicKeyPath, string privateKeyPath)
        {
            RsaAlgorithm rsaAlgorithm = new RsaAlgorithm(publicKeyPath, privateKeyPath);
        }

        [Test]
        public void CreateNewKeysAndDeleteKeysShouldWorks()
        {
            RsaAlgorithm rsaAlgorithm = new RsaAlgorithm(this.publicKeyPath, this.privateKeyPath);
            Assert.IsNotNull(rsaAlgorithm);
            rsaAlgorithm.CreateNewKeys();
            Assert.IsFalse(rsaAlgorithm.PublicOnly);
            Assert.IsNotNull(rsaAlgorithm.GetPrivateKey());
            Assert.IsNotNull(rsaAlgorithm.GetPublicKey());
            rsaAlgorithm.DeleteOldKeys();
            Assert.IsTrue(!File.Exists(this.privateKeyPath));
            Assert.IsTrue(!File.Exists(this.publicKeyPath));
        }

        [Test]
        public void EncryptAndDecryptDataSuccessfully()
        {
            Random random = new Random((int)DateTime.Now.Ticks);
            byte[] rawData = new byte[10];
            random.NextBytes(rawData);

            RsaAlgorithm rsaAlgorithm = new RsaAlgorithm();
            Assert.IsNotNull(rsaAlgorithm);
            byte[] encryptedData = rsaAlgorithm.Encrypt(rawData);
            Assert.IsNotNull(encryptedData);
            byte[] decryptedData = rsaAlgorithm.Decrypt(encryptedData);
            Assert.IsNotNull(decryptedData);
            Assert.AreEqual(rawData, decryptedData);
        }
    }
}
