//-----------------------------------------------------------------------
// <copyright file="IdentityProviderTests.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace DeiTests.Tokens
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Security.Cryptography;
    using Dei;
    using Dei.Protocol;
    using Dei.Utilities;
    using NUnit.Framework;
    using Rhino.Mocks;
    using Badumna.Security;
    using Dei.Tokens;
    using System.Threading;

    [TestFixture, Timeout(2000)]
    internal class IdentityProviderTests
    {

        private Character character;
        private IClientDriver driver;
        private TestIdentityProvider provider;
        private ManualResetEvent refreshing;

        [SetUp]
        public void SetUp()
        {
            this.driver = MockRepository.GenerateStub<IClientDriver>();
            this.character = new Character(1, "Player 1");
            this.provider = new TestIdentityProvider(character, driver);
            this.refreshing = new ManualResetEvent(false);
        }

        [TearDown]
        public void TearDown()
        {
            this.provider.Dispose();
            ((IDisposable)this.refreshing).Dispose();
        }

        [Test]
        public void MonitorThreadIsStartedOnActivateInBackground()
        {
            this.StubNeedsRefresh(false);
            Assert.IsNull(this.provider.MonitorThread);

            this.provider.Activate();
            Assert.IsNotNull(this.provider.MonitorThread);
            Assert.IsTrue(this.provider.MonitorThread.IsAlive);
            Assert.IsTrue(this.provider.MonitorThread.IsBackground);
        }

        [Test]
        public void DisposeStopsMonitorThreadAndDisposesDriver()
        {
            this.StubNeedsRefresh(false);
            this.provider.Activate();
            Thread thread = provider.MonitorThread;
            this.driver.Expect(d => d.Dispose()).Repeat.Once();

            this.provider.Dispose();
            Assert.IsFalse(thread.IsAlive);
            Assert.IsNull(this.provider.MonitorThread);
        }

        [Test]
        public void DisposeSucceedsIfMonitorThreadNotStarted()
        {
            Assert.IsNull(this.provider.MonitorThread);
            this.provider.Dispose();
        }

        [Test]
        public void DisposeIsIdempotent()
        {
            this.StubNeedsRefresh(false);
            this.provider.Activate();
            this.driver.Expect(d => d.Dispose()).Repeat.Once();

            this.provider.Dispose();
            this.provider.Dispose();
            this.provider.Dispose();
        }

        [Test]
        public void MonitorThreadCallsRefreshWhenRequired()
        {
            this.StubNeedsRefresh(true);
            this.driver.Expect(d => d.Refresh(this.character)).Return(new LoginResult(new List<Character> { this.character }));

            this.provider.Activate();
            this.WaitForSingleRefresh();
            this.provider.Dispose();
        }

        [Test]
        public void MonitorThreadOnlyCallsRefreshWhenRequired()
        {
            this.StubNeedsRefresh(false);
            this.driver.Expect(d => d.Refresh(this.character)).Repeat.Never();

            this.provider.Activate();
            this.WaitForSingleRefresh();
            this.provider.Dispose();
        }

        // TODO:
        // Test that the exception (with stacktrace) is logged, and stop
        // the expected messages from going to the console when running the tests.

        [Test]
        public void MonitorThreadContinuesOnException()
        {
            this.StubNeedsRefresh(true);
            this.driver.Expect(d => d.Refresh(this.character)).Throw(new Exception("Expected Error")).Repeat.Twice();

            this.provider.Activate();
            this.WaitForSingleRefresh();
            this.WaitForSingleRefresh();
            this.provider.Dispose();
        }

        [Test]
        public void MonitorThreadContinuesOnRefreshFailure()
        {
            this.StubNeedsRefresh(true);

            this.driver.Expect(d => d.Refresh(this.character)).Return(new LoginResult("Expected Error")).Repeat.Twice();
            this.provider.Activate();
            this.WaitForSingleRefresh();
            this.WaitForSingleRefresh();
            this.provider.Dispose();
        }

        private void StubNeedsRefresh(bool needsRefresh)
        {
            this.driver.Stub(d => d.NeedsRefresh).WhenCalled(invocation => this.refreshing.Set()).Return(needsRefresh);
        }

        private void WaitForSingleRefresh()
        {
            this.refreshing.WaitOne();
            this.refreshing.Reset();
        }

        private class TestIdentityProvider : IdentityProvider
        {
            internal TestIdentityProvider(Character character, IClientDriver driver)
                : base(character, driver)
            {
            }

            public Thread MonitorThread { get { return this.monitorThread; } }
        }
    }
}
