﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DeiTests.Tokens
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Security.Cryptography;
    using Dei;
    using Dei.Protocol;
    using Dei.Utilities;
    using NUnit.Framework;
    using Rhino.Mocks;
    using Badumna.Security;
    using Dei.Tokens;

    class ClientSettingsTests
    {
        private ClientSettings settings;

        [SetUp]
        public void SetUp()
        {
            this.settings = new ClientSettings(null, 1234, null);
        }

        [Test]
        public void ValidateServerCertificate2ShouldIgnoreSslErrorsWhenItSet()
        {
            settings.IgnoreSslErrors = true;

            int[] certificateError = new int[] { (int)SslCertificateError.CertificateUsageError };
            Assert.IsTrue(settings.ValidateServerCertificate2(null, certificateError));
        }

        [Test]
        public void ValidateServerCertificate2ShouldIgnoreCertificateNameMismatchWhenItSet()
        {
            settings.IgnoreSslCertificateNameMismatch = true;

            int[] certificateError = new int[] { (int)SslCertificateError.RemoteCertificateNameMismatch };
            Assert.IsTrue(settings.ValidateServerCertificate2(null, certificateError));
        }

        [Test]
        public void ValidateServerCertificate2ShouldIgnoreSelfSignedCertificateErrorWhenItSet()
        {
            settings.IgnoreSslSelfSignedCertificateErrors = true;

            int[] certificateError = new int[] { (int)SslCertificateError.UntrustedRoot };
            Assert.IsTrue(settings.ValidateServerCertificate2(null, certificateError));
        }

        [Test]
        public void ValidateServerCertificate2ShouldFailedOnCertificateError()
        {
            int[] certificateError = new int[] { (int)SslCertificateError.UntrustedRoot };
            Assert.IsFalse(settings.ValidateServerCertificate2(null, certificateError));
        }
    }
}
