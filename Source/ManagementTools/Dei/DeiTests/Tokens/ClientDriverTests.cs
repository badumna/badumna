//-----------------------------------------------------------------------
// <copyright file="IdentityProviderTests.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace DeiTests.Tokens
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Security.Cryptography;
    using Dei;
    using Dei.Protocol;
    using Dei.Utilities;
    using NUnit.Framework;
    using Rhino.Mocks;
    using Badumna.Security;
    using Dei.Tokens;

    [TestFixture]
    internal class ClientDriverTests
    {
        private static RSACryptoServiceProvider privateKey = new RSACryptoServiceProvider();
        private const int StateLength = 148;

        private LoginResult LoginSuccess = new LoginResult(new List<Character>());
        private IClient client;
        private string username;
        private string password;
        private TestClientDriver driver;
        private ClientSettings settings;
        private Character character;

        class TestClientDriver : ClientDriver
        {
            ClientDriverTests test;
            public int InitializeCount { get; private set; }

            // expose the parent's client property publicly
            public IClient GetClient() { return this.Client; }

            public TestClientDriver(ClientDriverTests test)
                : base(test.username, test.password, test.settings)
            {
                this.test = test;
            }

            protected override IClient InitializeClient()
            {
                this.InitializeCount++;
                return this.test.client;
            }

        }

        [SetUp]
        public void SetUp()
        {
            this.client = MockRepository.GenerateStub<IClient>();
            this.username = "user";
            this.password = "password";
            this.character = new Character(12, "character");
            this.settings = new ClientSettings("hostname", 1, ClientDriverTests.privateKey);
            this.driver = new TestClientDriver(this);
        }

        private void StubFailedTokenRetrieval()
        {
            this.client.Stub(c => c.GetServerTime()).Return(null);
            this.client.Stub(c => c.GetParticipationKey(true)).Return(null);
            this.client.Stub(c => c.GetServersPublicKey(true)).Return(null);
            this.client.Stub(c => c.GetPermissions(null)).IgnoreArguments().Return(new List<Permission>());
            this.client.Stub(c => c.GetCertificate(null, null)).IgnoreArguments().Return(null);
        }

        private void ExpectValidLogin()
        {
            client.Expect(c => c.Login(this.username, this.password)).Return(LoginSuccess);
        }

        private void ExpectValidTokenRetrieval()
        {
            UtcDateTime time = new UtcDateTime(DateTime.UtcNow);
            TimeSpan validityPeriod = TimeSpan.FromHours(24);
            List<Permission> listPermission = new List<Permission>();
            byte[] key = new byte[ParticipationKey.KeyLengthBytes];

            Random random = new Random((int)DateTime.Now.Ticks);
            random.NextBytes(key);

            UserCertificate userCertificate = this.CreateNewUserCertificate(
                random,
                time,
                new UtcDateTime(time.DateTime + validityPeriod));

            this.client.Expect(c => c.GetServerTime())
                .Return(new ServerTime(time, validityPeriod)).Repeat.Once();
            this.client.Expect(c => c.GetParticipationKey(true))
                .Return(new ParticipationKey(validityPeriod, key)).Repeat.Once();
            this.client.Expect(c => c.GetServersPublicKey(true))
                .Return(this.GeneratePublicKey()).Repeat.Once();
            this.client.Expect(c => c.GetPermissions(null)).IgnoreArguments()
                .Return(listPermission).Repeat.Once();
            this.client.Expect(c => c.GetCertificate(null, null)).IgnoreArguments()
                .Return(userCertificate).Repeat.Once();
        }

        [Test]
        public void AuthenticateWithValidInputShouldReturnLoginSuccessful()
        {
            this.ExpectValidLogin();

            LoginResult loginResult = this.driver.OpenSession(null);
            Assert.IsTrue(loginResult.WasSuccessful, loginResult.ErrorDescription);

            this.ExpectValidTokenRetrieval();
            this.driver.Authorize(this.character, loginResult, null);

            Assert.IsNotNull(this.driver.GetNetworkParticipationToken());
            Assert.IsNotNull(this.driver.GetPermissionListToken());
            Assert.IsNotNull(this.driver.GetTimeToken());
            Assert.IsNotNull(this.driver.GetTrustedAuthorityToken());
            Assert.IsNotNull(this.driver.GetUserCertificateToken());

            this.client.VerifyAllExpectations();
        }


        [Test]
        public void AuthorizeWithClientErrorShouldResultFailureReason()
        {
            this.ExpectValidLogin();

            LoginResult loginResult = this.driver.OpenSession(null);
            Assert.IsTrue(loginResult.WasSuccessful, loginResult.ErrorDescription);

            this.StubFailedTokenRetrieval();
            this.client.Stub(c => c.FailureReason).Return(FailureReasons.PermissionDenied);

            loginResult = this.driver.Authorize(this.character, loginResult, null);
            Assert.IsFalse(loginResult.WasSuccessful);
            Assert.AreEqual("Failed to acquire security tokens: PermissionDenied", loginResult.ErrorDescription);
            
            this.client.VerifyAllExpectations();
        }


        private UserCertificate CreateNewUserCertificate(Random random, UtcDateTime timeStamp, UtcDateTime expiryTime)
        {
            byte[] badumnaState = new byte[StateLength];
            random.NextBytes(badumnaState);

            UserCertificate userCertificate = UserCertificate.CreateUnsignedClientCertificate(
                this.character,
                badumnaState);

            Assert.IsNotNull(userCertificate);
            UserCertificate userCertificate2 = new UserCertificate(
                userCertificate,
                timeStamp,
                expiryTime);

            Assert.IsNotNull(userCertificate2);

            return userCertificate2;
        }

        [Test]
        public void CloneShouldNotAliasClient()
        {
            this.ExpectValidLogin();
            this.driver.OpenSession(null);
            var secondInstance = (TestClientDriver)this.driver.Clone();
            Assert.IsNotNull(this.driver.GetClient());
            Assert.IsNull(secondInstance.GetClient());
        }

        [Test]
        public void LoginProgressNotificationCanAbortProcess()
        {
            var stages = new List<LoginStage>();
            this.ExpectValidLogin();
            LoginProgressNotification progressCallback = delegate(LoginStage stage, string description, int percentageComplete)
            {
                stages.Add(stage);
                return stage != LoginStage.ClockSynchronization;
            };

            var result = this.driver.OpenSession(progressCallback);
            Assert.AreEqual(true, result.WasSuccessful);

            result = this.driver.Authorize(this.character, result, progressCallback);
            Assert.AreEqual(new List<LoginStage> { LoginStage.Initializing, LoginStage.ClockSynchronization }, stages);
            Assert.AreEqual(LoginResult.UserCancelled, result);
        }

        [Test]
        public void ExceptionInClientDoesNotLeaveClientInAnUnusableState()
        {
            client.Expect(c => c.Login(this.username, this.password)).Throw(new Exception("Spanner in the works!"));

            Assert.AreEqual(false, this.driver.OpenSession(null).WasSuccessful);

            client.Expect(c => c.Login(this.username, this.password)).Return(LoginSuccess).Repeat.Any();
            var result = this.driver.OpenSession(null);
            Assert.AreEqual(true, result.WasSuccessful, result.ErrorDescription);
        }

        private PublicKey GeneratePublicKey()
        {
            CspParameters cspParameters = new CspParameters(1);
            cspParameters.KeyContainerName = @"Dei_KeyStore";

            using (RSACryptoServiceProvider rsa = new RSACryptoServiceProvider(RsaAlgorithm.KeyLength, cspParameters))
            {
                PublicKey publicKey = new PublicKey(TimeSpan.MaxValue, rsa.ExportCspBlob(false));
                rsa.Clear();
                return publicKey;
            }
        }
    }

}
