//-----------------------------------------------------------------------
// <copyright file="IdentityProviderTests.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace DeiTests.Tokens
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Security.Cryptography;
    using Dei;
    using Dei.Protocol;
    using Dei.Utilities;
    using NUnit.Framework;
    using Rhino.Mocks;
    using Badumna.Security;
    using Dei.Tokens;

    [TestFixture]
    internal class SessionTests
    {
        private const string ServerHost = "localhost";

        private const ushort ServerPort = 21248;

        private LoginResult EmptyLoginResponse = new LoginResult(new List<Character>());

        private IClientDriver driver;

        private UserProperties user;

        private Character character;

        private IIdentityProvider identity;
        
        private Session Session()
        {
            return this.StubDriver(new Session(ServerHost, ServerPort));
        }

        private Session StubDriver(Session session)
        {
            session.ClientDriverFactory = (username, password, settings) => this.driver;
            return session;
        }

        [SetUp]
        public void SetUp()
        {
            this.driver = MockRepository.GenerateStub<IClientDriver>();
            this.user = new UserProperties("user", "password");
            this.character = new Character(33, "character");
        }

        [Test]
        public void SessionConstructorWithValidInputsWorks()
        {
            new Session(ServerHost, ServerPort);
        }

        [Test]
        [ExpectedException(typeof(ArgumentException))]
        public void IdentityProviderConstructorWithInvalidXmlStringShouldThrowArgumentException()
        {
            new Session(ServerHost, ServerPort, string.Empty, (LoginProgressNotification)null);
        }
        
        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void IdentityProviderConstructorWithNullXmlStringShouldThrowArgumentNullException()
        {
            new Session(ServerHost, ServerPort, (string)null, (LoginProgressNotification)null);
        }

        [TestCase(new object[] { null, "password" })]
        [TestCase(new object[] { "user", null })]
        public void AuthenticateWithNullInputShouldThrowsArgumentNullException(
            string username, string password)
        {
            var session = Session();
            Assert.Throws(typeof(ArgumentNullException), () => session.Authenticate(username, password));
        }

        private void AssertValidLogin()
        {
            this.driver.AssertWasCalled(d => d.OpenSession(null), o => o.IgnoreArguments());
        }

        [Test]
        public void AuthenticateWithValidInputShouldReturnLoginSuccessful()
        {
            var progressNotification = MockRepository.GenerateStub<LoginProgressNotification>();
            using (var session = new Session(ServerHost, ServerPort, progressNotification))
            {
                this.StubDriver(session);
                var expectedResult = EmptyLoginResponse;

                this.driver.Stub(d => d.OpenSession(progressNotification)).Return(expectedResult);
                LoginResult loginResult = session.Authenticate(
                    this.user.Login,
                    this.user.Password);
                Assert.AreEqual(expectedResult, loginResult);
                Assert.IsTrue(loginResult.WasSuccessful, loginResult.ErrorDescription);

                this.driver.Stub(d => d.Authorize(this.character, loginResult, progressNotification)).Return(expectedResult);
                Assert.AreEqual(expectedResult, session.SelectIdentity(this.character, out this.identity));
                Assert.IsNotNull(this.identity);
            }

            this.driver.VerifyAllExpectations();
        }

        [Test]
        public void DisposeCallsDisposeOnDriverAndDiscardsIt()
        {
            var session = this.Session();
            session.Authenticate(this.user.Login, this.user.Password);

            // dispose should be idempotent - calling multiple times will only dispose the driver the first time
            session.Dispose();
            this.driver.AssertWasCalled(d => d.Dispose(), o => o.Repeat.Once());

            session.Dispose();
            this.driver.AssertWasCalled(d => d.Dispose(), o => o.Repeat.Once());
        }


        [Test]
        public void AuthenticateDiscardsAnyPreviousDriver()
        {
            using (var session = this.Session())
            {
                session.Authenticate(this.user.Login, this.user.Password);
                this.driver.AssertWasNotCalled(d => d.Dispose());

                session.Authenticate(this.user.Login, this.user.Password);
                this.driver.AssertWasCalled(d => d.Dispose(), o => o.Repeat.Once());
            }
        }

        [Test]
        public void AuthorizeWithEmptyCharacterNameFails()
        {
            using (var session = this.Session())
            {
                this.driver.Stub(d => d.OpenSession(null)).Return(EmptyLoginResponse);
                Assert.IsTrue(session.Authenticate(this.user.Login, this.user.Password).WasSuccessful);
                Assert.Throws<ArgumentException>(() => session.SelectIdentity(new Character(12, ""), out this.identity));
            }
        }

        [Test]
        public void AuthorizeFailureDoesNotProduceIdentityProvider()
        {
            using (var session = this.Session())
            {
                this.driver.Stub(d => d.OpenSession(null)).Return(EmptyLoginResponse);
                this.driver.Stub(d => d.Authorize(this.character, EmptyLoginResponse, null)).Return(new LoginResult("nope"));

                Assert.IsTrue(session.Authenticate(this.user.Login, this.user.Password).WasSuccessful);
                Assert.IsFalse(session.SelectIdentity(this.character, out this.identity).WasSuccessful);
                Assert.IsNull(this.identity);
            }
        }

        [Test]
        public void UserMethodsThrowIfAuthenticateHasntBeenCalled()
        {
            var session = this.Session();

            Assert.Throws<InvalidOperationException>(() => session.SelectIdentity(this.character, out this.identity));
            Assert.Throws<InvalidOperationException>(() => session.CreateCharacter(this.character.Name));
            Assert.Throws<InvalidOperationException>(() => session.ListCharacters());
            Assert.Throws<InvalidOperationException>(() => session.DeleteCharacter(this.character));
        }

        [Test]
        public void UserMethodsThrowIfAuthenticateHasNotSucceeded()
        {
            var session = this.Session();

            this.driver.Stub(d => d.OpenSession(null)).IgnoreArguments().Return(new LoginResult("Nope"));
            Assert.IsFalse(session.Authenticate(this.user.Login, this.user.Password).WasSuccessful);

            Assert.Throws<InvalidOperationException>(() => session.SelectIdentity(this.character, out this.identity));
            Assert.Throws<InvalidOperationException>(() => session.CreateCharacter(this.character.Name));
            Assert.Throws<InvalidOperationException>(() => session.ListCharacters());
            Assert.Throws<InvalidOperationException>(() => session.DeleteCharacter(this.character));
        }
    }
}
