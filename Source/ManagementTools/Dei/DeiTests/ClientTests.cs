//-----------------------------------------------------------------------
// <copyright file="ClientTests.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace DeiTests
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using Dei;
    using Dei.Protocol;
    using NUnit.Framework;
    using Rhino.Mocks;
    using Badumna.Security;

    [TestFixture]
    internal class ClientTests
    {
        private static LoginResult loginOk = new LoginResult(new List<Character>());
        
        private static LoginResult loginFail = new LoginResult("Nope");

        private Stream stream;

        private IClientProtocol clientProtocol;

        private UserProperties user;

        private Client client;

        public ClientTests()
        {
        }

        [SetUp]
        public void SetUp()
        {
            this.stream = MockRepository.GenerateMock<Stream>();
            this.clientProtocol = MockRepository.GenerateMock<IClientProtocol>();
            this.user = new UserProperties("user", "password");
            this.client = new Client(this.stream, this.ClientProtocolFactory);
        }

        [TearDown]
        public void TearDown()
        {
            this.stream = null;
            this.clientProtocol = null;
            this.user = null;
            this.client = null;
        }

        [Test]
        public void ClientConstructorWithMockStreamShouldWorks()
        {
            Client client = new Client(this.stream);
            Assert.IsNotNull(client);
        }

        [Test]
        public void LoginWithNonWritableStreamShouldReturnFalse()
        {
            this.stream.Stub(s => s.CanWrite).Return(false).Repeat.Once();
            Assert.IsFalse(this.client.Login(this.user.Login, this.user.Password).WasSuccessful);
        }

        [Test]
        public void LoginWithValidUserAndStreamShouldReturnTrue()
        {
            this.stream.Stub(s => s.CanWrite).Return(true).Repeat.Once();
            this.clientProtocol.Stub(s => s.Login(this.user))
                .Return(loginOk).IgnoreArguments().Repeat.Once();
            Assert.IsTrue(this.client.Login(this.user.Login, this.user.Password).WasSuccessful);
        }

        [Test]
        public void LoginWithValidUserButFailedLoginToServerShouldReturnFalse()
        {
            this.stream.Stub(s => s.CanWrite).Return(true).Repeat.Once();
            this.clientProtocol.Stub(s => s.Login(this.user))
                .Return(loginFail).IgnoreArguments().Repeat.Once();
            Assert.IsFalse(this.client.Login(this.user.Login, this.user.Password).WasSuccessful);
        }

        [Test]
        public void LogoutShouldCallClientProtocolLogoutMethod()
        {
            this.ClientLogin();

            this.clientProtocol.Stub(s => s.Logout(this.user))
                .Return(true).IgnoreArguments().Repeat.Once();
            this.client.Logout();
        }

        [Test]
        public void DisposeShouldIgnoreProtocolException()
        {
            this.ClientLogin();

            this.clientProtocol.Stub(s => s.Logout(this.user))
                .Throw(new ProtocolException("ouch")).IgnoreArguments().Repeat.Once();
            this.client.Dispose();
        }

        [Test]
        public void GetServerTimeShouldCallClientProtocolRequestServerTime()
        {
            this.ClientLogin();

            ServerTime time = new ServerTime(new UtcDateTime(DateTime.UtcNow), TimeSpan.FromHours(24));
            this.clientProtocol.Stub(s => s.RequestServerTime(this.user))
                .Return(time).IgnoreArguments().Repeat.Once();
            Assert.AreEqual(time, this.client.GetServerTime());
        }

        [Test]
        [ExpectedException(typeof(InvalidOperationException))]
        public void GetParticipationKeyWithoutLoginShouldThrowsInvalidOperationException()
        {
            this.client.GetParticipationKey(true);
        }

        [Test]
        public void GetParticipationKeyWithValidUserShouldReturnValidParticipationKey()
        {
            this.ClientLogin();

            bool isFirstRequest = true;
            ParticipationKey participationKey = new ParticipationKey();
            this.clientProtocol.Stub(s => s.RequestParticipationKey(this.user, isFirstRequest ? 0 : 1))
                .Return(participationKey).IgnoreArguments().Repeat.Once();
            Assert.AreEqual(participationKey, this.client.GetParticipationKey(isFirstRequest));
        }

        [Test]
        [ExpectedException(typeof(InvalidOperationException))]
        public void GetServerPublicKeyWithoutLoginShouldThrowsInvalidOperationException()
        {
            this.client.GetServersPublicKey(true);
        }

        [Test]
        public void GetServerPublicKeyWithValidUserShouldReturnValidPublicKey()
        {
            this.ClientLogin();

            bool isFirstRequest = true;
            PublicKey publicKey = new PublicKey();
            this.clientProtocol.Stub(s => s.RequestServersKey(this.user, isFirstRequest ? 0 : 1))
                .Return(publicKey).IgnoreArguments().Repeat.Once();
            Assert.AreEqual(publicKey, this.client.GetServersPublicKey(isFirstRequest));
        }

        [Test]
        [ExpectedException(typeof(InvalidOperationException))]
        public void GetCertificateWithoutLoginShouldThrowInvalidOperationException()
        {
            this.client.GetCertificate(null, null);
        }

        [Test]
        public void GetCertificateWithInvalidCharacterShouldThrowInvalidOperationException()
        {
            this.ClientLogin();
            Assert.Throws<ArgumentException>(() => this.client.GetCertificate(new Character(2, ""), null));
        }

        [Test]
        public void GetCertificateWithValidUserAndCharacterShouldReturnValidCertificate()
        {
            this.ClientLogin();

            UserCertificate userCertificate = new UserCertificate();
            Character character = new Character(1, "cpt one");
            this.clientProtocol.Stub(s => s.RequestCertificate(this.user, character, null))
                .Return(userCertificate).IgnoreArguments().Repeat.Once();
            Assert.AreEqual(userCertificate, this.client.GetCertificate(character, null));
        }

        [Test]
        public void GetCertificateWithNullCharacterShouldReturnValidCertificate()
        {
            this.ClientLogin();

            UserCertificate userCertificate = new UserCertificate();
            this.clientProtocol.Stub(s => s.RequestCertificate(this.user, null, null))
                .Return(userCertificate).IgnoreArguments().Repeat.Once();
            Assert.AreEqual(userCertificate, this.client.GetCertificate(null, null));
        }

        [Test]
        [ExpectedException(typeof(InvalidOperationException))]
        public void GetPermissionsWithoutLoginShouldThrowsInvalidOperationException()
        {
            this.client.GetPermissions(null);
        }

        [Test]
        public void GetPermissionsWithValidUserShouldReturnValidPermissionList()
        {
            this.ClientLogin();

            List<Permission> permissions = new List<Permission>();
            this.clientProtocol.Stub(s => s.RequestPermissionList(this.user, null))
                .Return(permissions).IgnoreArguments().Repeat.Once();
            Assert.AreEqual(permissions, this.client.GetPermissions(null));
        }

        [Test]
        [ExpectedException(typeof(InvalidOperationException))]
        public void GetPermissionTypesWithoutLoginShouldThrowsInvalidOperationException()
        {
            this.client.GetPermissionTypes(this.user.Login);
        }

        [Test]
        public void GetPermissionTypesWithValidUserShouldReturnValidPermissionTypesList()
        {
            this.ClientLogin();

            List<PermissionProperties> permissionTypes = new List<PermissionProperties>();
            this.clientProtocol.Stub(s => s.RequestPermissionTypes(this.user.Login))
                .Return(permissionTypes).Repeat.Once();
            Assert.AreEqual(permissionTypes, this.client.GetPermissionTypes(this.user.Login));
        }

        [Test]
        [ExpectedException(typeof(InvalidOperationException))]
        public void ChangeAccountWithoutLoginShouldThrowsInvalidOperationException()
        {
            UserProperties modifiedUser = new UserProperties("user", "newPassword");
            this.client.ChangeAccount(modifiedUser.Login, modifiedUser.Password);
        }

        [Test]
        public void ChangeAccountWithValidUserShouldReturnTrue()
        {
            this.ClientLogin();

            UserProperties modifiedUser = new UserProperties("user", "newPassword");
            this.clientProtocol.Stub(s => s.ChangeAccount(modifiedUser))
                .Return(true).IgnoreArguments().Repeat.Once();
            Assert.IsTrue(this.client.ChangeAccount(modifiedUser.Login, modifiedUser.Password));
        }

        [Test]
        [ExpectedException(typeof(InvalidOperationException))]
        public void CreateNewAccountWithoutLoginShouldThrowsInvalidOperationException()
        {
            UserProperties newUser = new UserProperties("newUser", "newPassword");
            this.client.CreateNewAccount(newUser.Login, newUser.Password);
        }

        [Test]
        public void CreateNewAccountWithValidUserShouldReturnTrue()
        {
            this.ClientLogin();

            UserProperties newUser = new UserProperties("user", "newPassword");
            this.clientProtocol.Stub(s => s.CreateNewAccount(newUser))
                .Return(true).IgnoreArguments().Repeat.Once();
            Assert.IsTrue(this.client.CreateNewAccount(newUser.Login, newUser.Password));
        }

        [Test]
        [ExpectedException(typeof(InvalidOperationException))]
        public void DeleteAccountWithoutLoginShouldThrowsInvalidOperationException()
        {
            UserProperties existingUser = new UserProperties("user", "newPassword");
            this.client.DeleteAccount(existingUser.Login);
        }

        [Test]
        public void DeleteAccountWithValidUserShouldReturnTrue()
        {
            this.ClientLogin();

            UserProperties existingUser = new UserProperties("user", "newPassword");
            this.clientProtocol.Stub(s => s.DeleteAccount(existingUser))
                .Return(true).IgnoreArguments().Repeat.Once();
            Assert.IsTrue(this.client.DeleteAccount(existingUser.Login));
        }

        [Test]
        [ExpectedException(typeof(InvalidOperationException))]
        public void CreatePermissionWithoutLoginShouldThrowsInvalidOperationException()
        {
            string permissionName = PermissionTypes.Admin.ToString();
            this.client.CreatePermission(permissionName);
        }

        [Test]
        public void CreatePermissionWithValidUserAndPermissionNameShouldReturnPermissionId()
        {
            this.ClientLogin();

            string permissionName = PermissionTypes.Admin.ToString();
            this.clientProtocol.Stub(s => s.CreatePermission(permissionName))
                .Return((long)PermissionTypes.Admin).Repeat.Once();
            Assert.AreEqual((long)PermissionTypes.Admin, this.client.CreatePermission(permissionName));
        }

        [Test]
        [ExpectedException(typeof(InvalidOperationException))]
        public void RemovePermissionWithoutLoginShouldThrowsInvalidOperationException()
        {
            long permissionId = (long)PermissionTypes.Admin;
            this.client.RemovePermission(permissionId);
        }

        [Test]
        public void RemovePermissionWithValidUserAndPermissionIdShouldReturnTrue()
        {
            this.ClientLogin();

            long permissionId = (long)PermissionTypes.Admin;
            this.clientProtocol.Stub(s => s.RemovePermission(permissionId))
                .Return(true).Repeat.Once();
            Assert.IsTrue(this.client.RemovePermission(permissionId));
        }

        [Test]
        [ExpectedException(typeof(InvalidOperationException))]
        public void GrantPermissionWithoutLoginShouldThrowsInvalidOperationException()
        {
            long permissionId = (long)PermissionTypes.Admin;
            this.client.GrantPermission(this.user.Login, permissionId);
        }

        [Test]
        public void GrantPermissionWithValidUserAndPermissionIdShouldReturnTrue()
        {
            this.ClientLogin();

            long permissionId = (long)PermissionTypes.Participation;
            this.clientProtocol.Stub(s => s.GrantPermission(this.user, permissionId))
                .IgnoreArguments().Return(true).Repeat.Once();
            Assert.IsTrue(this.client.GrantPermission(this.user.Login, permissionId));
        }

        [Test]
        [ExpectedException(typeof(InvalidOperationException))]
        public void RevokePermissionWithoutLoginShouldThrowsInvalidOperationException()
        {
            long permissionId = (long)PermissionTypes.Admin;
            this.client.RevokePermission(this.user.Login, permissionId);
        }

        [Test]
        public void RevokePermissionWithValidUserAndPermissionIdShouldReturnTrue()
        {
            this.ClientLogin();

            long permissionId = (long)PermissionTypes.Participation;
            this.clientProtocol.Stub(s => s.RevokePermission(this.user, permissionId))
                .IgnoreArguments().Return(true).Repeat.Once();
            Assert.IsTrue(this.client.RevokePermission(this.user.Login, permissionId));
        }

        [Test]
        [ExpectedException(typeof(InvalidOperationException))]
        public void ListUsersWithoutLoginShouldThrowsInvalidOperationException()
        {
            this.client.ListUsers("*", 0, 10);
        }

        [TestCase(new object[] { "*", -1 })]
        [TestCase(new object[] { "", 10 })]
        [ExpectedException(typeof(ArgumentException))]
        public void ListUsersWithInvalidArgumentsShouldThrowsArgumentException(
            string usernameGlob, int maxNumberOfResult)
        {
            this.ClientLogin();
            this.client.ListUsers(usernameGlob, 0, maxNumberOfResult);
        }

        [Test]
        public void ListUsersWithValidUserAndArgumentsShouldReturnUserList()
        {
            string usernameGlob = "*";
            int startRange = 0;
            int maxNumberOfResult = 10;
            List<string> userList = new List<string>() { "user", "admin" };
            this.ClientLogin();

            this.clientProtocol.Stub(s => s.RequestUserList(usernameGlob, startRange, maxNumberOfResult))
                .Return(userList).Repeat.Once();
            Assert.AreEqual(userList, this.client.ListUsers(usernameGlob, startRange, maxNumberOfResult));
        }

        [Test]
        [ExpectedException(typeof(InvalidOperationException))]
        public void GetUserCountWithoutLoginShouldThrowsInvalidOperationException()
        {
            string usernameGlob = "*";
            this.client.GetUserCount(usernameGlob);
        }

        [Test]
        [ExpectedException(typeof(ArgumentException))]
        public void GetUserCountWithInvalidUsernameGlobShouldThrowsArgumentException()
        {
            this.ClientLogin();
            this.client.GetUserCount(string.Empty);
        }

        [Test]
        public void GetUserCountWihtValidUserAndUsernameGlobShouldReturnUserCount()
        {
            this.ClientLogin();
            string usernameGlob = "*";
            int result = 25;
            this.clientProtocol.Stub(s => s.RequestUserCount(usernameGlob)).Return(result).Repeat.Once();
            Assert.AreEqual(result, this.client.GetUserCount(usernameGlob));
        }

        internal IClientProtocol ClientProtocolFactory(Stream stream)
        {
            return this.clientProtocol;
        }

        private void ClientLogin()
        {
            this.stream.Stub(s => s.CanWrite).Return(true).Repeat.Once();
            this.clientProtocol.Stub(s => s.Login(this.user))
                .Return(loginOk).IgnoreArguments().Repeat.Once();
            Assert.IsTrue(this.client.Login(this.user.Login, this.user.Password).WasSuccessful);
        }
    }
}
