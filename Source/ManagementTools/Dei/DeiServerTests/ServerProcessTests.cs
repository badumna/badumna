//-----------------------------------------------------------------------
// <copyright file="ServerProcessTests.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace DeiServerTests
{
    using System;
    using System.IO;
    using DeiServer;
    using NUnit.Framework;
    using Rhino.Mocks;
    using X509CertificateCreator;

    [TestFixture]
    internal class ServerProcessTests
    {
        private const string TestCertificateFile = "test_cert.pfx";
        private const string EmptyCertificateFile = "empty_cert.pfx";
        private const string MissingCertificateFile = "missing_cert.pfx";
        private const string TestCertificatePassword = "deinopidae";

        // ServerProcess serverProcess = null;
        private ICertificateCreator certificateCreator = null;

        public ServerProcessTests()
        {
        }

        [SetUp]
        public void SetUp()
        {
            this.certificateCreator = MockRepository.GenerateMock<ICertificateCreator>();
        }

        [TearDown]
        public void TearDown()
        {
            this.certificateCreator = null;
        }

        [Test]
        public void OnInitializeDoesNotCreateCertificateWhenValidOneIsFound()
        {
            Assert.IsTrue(File.Exists(TestCertificateFile));
            ServerProcess serverProcess = new ServerProcess(TestCertificateFile, this.certificateCreator);
            this.certificateCreator.Expect(c => c.CreateCertificate(null, DateTime.Now))
                .IgnoreArguments().Repeat.Never();
            string[] processArgs = new string[] { "--ssl", "--certificate-password:" + TestCertificatePassword };
            serverProcess.OnInitialize(ref processArgs);
            this.certificateCreator.VerifyAllExpectations();
        }

        [Test]
        public void OnInitializeCreatesCertificateWhenCertificateFileMissing()
        {
            ServerProcess serverProcess = new ServerProcess(MissingCertificateFile, this.certificateCreator);
            this.certificateCreator.Expect(c => c.CreateCertificate(null, DateTime.Now))
                .Return(null).IgnoreArguments().Repeat.Once();
            string[] processArgs = new string[] { "--ssl", "--certificate-password:" + TestCertificatePassword };
            serverProcess.OnInitialize(ref processArgs);
            this.certificateCreator.VerifyAllExpectations();
        }

        [Test]
        public void OnInitializeCreatesCertificateWhenCertificateFileInvalid()
        {
            Assert.IsFalse(File.Exists(EmptyCertificateFile));
            ServerProcess serverProcess = new ServerProcess(EmptyCertificateFile, this.certificateCreator);
            this.certificateCreator.Expect(c => c.CreateCertificate(null, DateTime.Now))
                .Return(null).IgnoreArguments().Repeat.Once();
            string[] processArgs = new string[] { "--ssl", "--certificate-password:" + TestCertificatePassword };
            serverProcess.OnInitialize(ref processArgs);
            this.certificateCreator.VerifyAllExpectations();
        }

        [TestCase("--ssl ")]
        [TestCase("--ssl --certificate-password:")]
        [TestCase("--ssl --certificate-password:foo")]
        public void OnInitializeCreatesCertificateWhenExistingCertificatePasswordMissingOrWrong(string argsString)
        {
            Assert.IsTrue(File.Exists(TestCertificateFile));
            ServerProcess serverProcess = new ServerProcess(TestCertificateFile, this.certificateCreator);
            this.certificateCreator.Expect(c => c.CreateCertificate(null, DateTime.Now))
                .Return(null).IgnoreArguments().Repeat.Once();
            string[] processArgs = argsString.Split(' ');
            serverProcess.OnInitialize(ref processArgs);
            this.certificateCreator.VerifyAllExpectations();
        }

        [Test]
        public void NonImplementedOnProcessRequest()
        {
            int requestType = 0;
            byte[] request = null;
            Assert.IsTrue(File.Exists(TestCertificateFile));
            ServerProcess serverProcess = new ServerProcess(TestCertificateFile, this.certificateCreator);

            Assert.IsNull(serverProcess.OnProcessRequest(requestType, request));
        }

        [Test]
        public void NonImplementedOnPerformRegularTasks()
        {
            int delayMiliseconds = 0;
            Assert.IsTrue(File.Exists(TestCertificateFile));
            ServerProcess serverProcess = new ServerProcess(TestCertificateFile, this.certificateCreator);
            Assert.IsTrue(serverProcess.OnPerformRegularTasks(delayMiliseconds));
        }
    }
}
