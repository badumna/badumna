//-----------------------------------------------------------------------
// <copyright file="ServerProtocolTests.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace DeiServerTests
{
    using System.Collections.Generic;
    using System.IO;
    using System.Net.Sockets;
    using System.Security.Cryptography;
    using System.Threading;
    using System.Xml;
    using System.Xml.Schema;
    using Dei;
    using Dei.Protocol;
    using Dei.Utilities;
    using DeiServer;
    using NUnit.Framework;
    using Rhino.Mocks;
    using System.Text;
    using Badumna.Security;

    [TestFixture]
    internal class ServerProtocolTests
    {
        private readonly Version version = new Version(2, 3);

        private IAccountManager accountManager;

        private Stream stream;

        private XmlReader xmlReader;

        private XmlWriter xmlWriter;

        private IXmlWriterFactory xmlWriterFactory;

        private IXmlReaderFactory xmlReaderFactory;

        private ISerializerWrapperFactory serializerWrapperFactory;

        private int waitTime = 5000;

        private TokenGenerator tokenGenerator;

        private UserProperties user;

        private Character character;

        private System.TimeSpan keyValidityPeriod = System.TimeSpan.FromHours(24);

        private Dictionary<System.Type, ISerializerWrapper> serializers = new Dictionary<System.Type, ISerializerWrapper>();
        
        private IList<Character> emptyCharacters;

        delegate ISerializerWrapper SerializerFactory(System.Type type);

        public ServerProtocolTests()
        {
        }

        [SetUp]
        public void SetUp()
        {
            this.accountManager = MockRepository.GenerateMock<IAccountManager>();
            this.stream = MockRepository.GenerateMock<Stream>();
            this.xmlReader = MockRepository.GenerateMock<XmlReader>();
            this.xmlWriter = MockRepository.GenerateMock<XmlWriter>();

            this.xmlReaderFactory = MockRepository.GenerateMock<IXmlReaderFactory>();
            this.xmlReaderFactory.Stub(f => f.Create(null, null)).IgnoreArguments().Return(this.xmlReader);

            this.xmlWriterFactory = MockRepository.GenerateMock<IXmlWriterFactory>();
            this.xmlWriterFactory.Stub(f => f.Create(null, null)).IgnoreArguments().Return(this.xmlWriter);

            // serializerWrapperFactory creates / returns a single mock serializer for each type (on demand)
            this.serializerWrapperFactory = MockRepository.GenerateStub<ISerializerWrapperFactory>();
            this.serializerWrapperFactory.Stub(c => c.Create(Arg<System.Type>.Is.Anything)).Do(
                (SerializerFactory) delegate(System.Type type)
                {
                    ISerializerWrapper serializer = null;
                    if (!this.serializers.TryGetValue(type, out serializer))
                    {
                        serializer = MockRepository.GenerateMock<ISerializerWrapper>();
                        this.serializers.Add(type, serializer);
                    }

                    return serializer;
                });

            this.accountManager.Stub(s => s.GetParticipationValidityPeriod())
                .Return(this.keyValidityPeriod).Repeat.Once();

            this.tokenGenerator = new TokenGenerator(this.accountManager);
            this.user = new UserProperties("user", "password");
            this.character = new Character(1234, "character");
            this.emptyCharacters = new List<Character>();
        }

        [TearDown]
        public void TearDown()
        {
            this.accountManager = null;
            this.stream = null;
            this.xmlReader = null;
            this.xmlWriter = null;
            this.xmlReaderFactory = null;
            this.xmlWriterFactory = null;
            this.serializerWrapperFactory = null;
            this.tokenGenerator = null;
            this.user = null;
            this.serializers.Clear();
        }

        [Test]
        public void ServerProtocolTestConstructorWorks()
        {
            ISerializerWrapper versionSerializerWrapper = this.GetSerializer(typeof(Version));
            Version version = new Version(2, 3);

            ManualResetEvent read = new ManualResetEvent(false);
            ManualResetEvent readName = new ManualResetEvent(false);
            ManualResetEvent flush = new ManualResetEvent(false);
            ManualResetEvent readVersion = new ManualResetEvent(false);
            ManualResetEvent setEndOfTest = new ManualResetEvent(false);

            this.xmlReader.Stub(s => s.Read()).Return(true).WhenCalled(x => read.Set());
            this.xmlReader.Stub(s => s.Name).Return(Resources.ConversationElementName)
                .WhenCalled(x => readName.Set());

            versionSerializerWrapper.Expect(c => c.WriteObject(this.xmlWriter, null))
                .IgnoreArguments().Repeat.Once();

            this.xmlWriter.Expect(c => c.Flush()).WhenCalled(x => flush.Set());
            versionSerializerWrapper.Stub(s => s.CanDeserialize(this.xmlReader)).Return(true);
            versionSerializerWrapper.Stub(s => s.ReadObject(this.xmlReader))
                .Return((object)version).WhenCalled(x => readVersion.Set());

            this.SetStubForEndOfTest(setEndOfTest);

            ServerProtocol serverProtocol = new ServerProtocol(
                this.stream,
                this.tokenGenerator,
                this.xmlWriterFactory,
                this.xmlReaderFactory,
                this.serializerWrapperFactory);

            Assert.IsNotNull(serverProtocol);
            Assert.IsTrue(read.WaitOne(this.waitTime));
            Assert.IsTrue(readName.WaitOne(this.waitTime));
            Assert.IsTrue(flush.WaitOne(this.waitTime));
            Assert.IsTrue(readVersion.WaitOne(this.waitTime));
            Assert.IsTrue(setEndOfTest.WaitOne(this.waitTime));

            versionSerializerWrapper.VerifyAllExpectations();
            this.xmlWriter.VerifyAllExpectations();
        }

        [Test]
        public void ServerProtocolConstructorWithInvalidVersionShouldStopListening()
        {
            ManualResetEvent readVersion = new ManualResetEvent(false);

            Version invalidVersion = new Version(2, 4);
            ISerializerWrapper versionSerializerWrapper = this.GetSerializer(typeof(Version));
            ISerializerWrapper commandSerializerWrapper = this.GetSerializer(typeof(Commands));

            this.xmlReader.Stub(s => s.Read()).Return(true);
            this.xmlReader.Stub(s => s.Name).Return(Resources.ConversationElementName);
            this.xmlReader.Expect(c => c.EOF).Return(false).Repeat.Never();

            versionSerializerWrapper.Stub(s => s.CanDeserialize(this.xmlReader)).Return(true);
            versionSerializerWrapper.Stub(s => s.ReadObject(this.xmlReader))
                .Return((object)invalidVersion).WhenCalled(x => readVersion.Set());

            commandSerializerWrapper.Expect(c => c.CanDeserialize(this.xmlReader)).Return(true).Repeat.Never();
            commandSerializerWrapper.Expect(c => c.ReadObject(this.xmlReader))
                .Return(Commands.RequestLogin).Repeat.Never();

            ServerProtocol serverProtocol = new ServerProtocol(
                this.stream,
                this.tokenGenerator,
                this.xmlWriterFactory,
                this.xmlReaderFactory,
                this.serializerWrapperFactory);

            Assert.IsTrue(readVersion.WaitOne(this.waitTime));

            this.xmlReader.VerifyAllExpectations();
            commandSerializerWrapper.VerifyAllExpectations();
        }

        [Test]
        public void OnLoginWithValidUserShouldReturnWithRequestLoginAccepted()
        {
            this.SetStubsForServerProtocolInitiation();

            ManualResetEvent sendRequestLoginAccepted = new ManualResetEvent(false);
            ManualResetEvent setEndOfTest = new ManualResetEvent(false);

            UserProperties user = new UserProperties("user", "password");
            List<Character> characters = new List<Character> { new Character(12, "cpt. twelvey") };
            LoginResult result = new LoginResult(characters);

            this.SetStubsAndExpectationForProcessCommand(Commands.RequestLogin, Commands.RequestLoginAccepted, null);
            this.SetStubsForReadingFromSerializerWrapper(typeof(UserProperties), user, null);

            ISerializerWrapper resultSerializerWrapper = this.GetSerializer(typeof(LoginResult));
            resultSerializerWrapper.Expect(c => c.WriteObject(this.xmlWriter, (object) result))
                .Repeat.Once().WhenCalled(x => sendRequestLoginAccepted.Set());

            this.accountManager.Stub(s => s.AuthenticateUser(user.Login, user.Password))
                .Return(true).Repeat.Once();

            this.accountManager.Stub(s => s.GetCharacters(user.Login))
                .Return(characters).Repeat.Once();

            this.SetStubForEndOfTest(setEndOfTest);

            ServerProtocol serverProtocol = new ServerProtocol(
                this.stream,
                this.tokenGenerator,
                this.xmlWriterFactory,
                this.xmlReaderFactory,
                this.serializerWrapperFactory);

            Assert.IsTrue(sendRequestLoginAccepted.WaitOne(this.waitTime));
            Assert.IsTrue(setEndOfTest.WaitOne(this.waitTime));

            foreach (ISerializerWrapper serializer in this.serializers.Values)
            {
                serializer.VerifyAllExpectations();
            }
        }
        
        [TestCase(new object[] { "", "password" })]
        [TestCase(new object[] { "user", "" })]
        public void OnLoginWithInvalidUserPropertiesShouldReturnWithRequestLoginRejected(
            string username, string password)
        {
            this.SetStubsForServerProtocolInitiation();

            ManualResetEvent sendRequestLoginRejected = new ManualResetEvent(false);
            ManualResetEvent setEndOfTest = new ManualResetEvent(false);
            
            this.SetStubsAndExpectationForProcessCommand(
                Commands.RequestLogin,
                Commands.RequestLoginRejected,
                sendRequestLoginRejected);

            UserProperties user = new UserProperties(username, password);
            this.SetStubsForReadingFromSerializerWrapper(typeof(UserProperties), user, null);
            this.SetStubForEndOfTest(setEndOfTest);

            ServerProtocol serverProtocol = new ServerProtocol(
                this.stream,
                this.tokenGenerator,
                this.xmlWriterFactory,
                this.xmlReaderFactory,
                this.serializerWrapperFactory);

            Assert.IsTrue(sendRequestLoginRejected.WaitOne(this.waitTime));
            Assert.IsTrue(setEndOfTest.WaitOne(this.waitTime));

            foreach (ISerializerWrapper serializer in this.serializers.Values)
            {
                serializer.VerifyAllExpectations();
            }
        }
        
        [Test]
        public void OnLogoutWithInvalidUsernameShouldReturnWithRequestLogoutFailed()
        {
            this.SetStubsForServerProtocolInitiation();

            ManualResetEvent sendRequestLogoutFailed = new ManualResetEvent(false);
            ManualResetEvent setEndOfTest = new ManualResetEvent(false);

            ISerializerWrapper failureReasonSerializerWrapper = this.GetSerializer(typeof(FailureReasons));
            this.serializerWrapperFactory.Stub(f => f.Create(typeof(FailureReasons)))
                .Return(failureReasonSerializerWrapper);

            this.SetStubsAndExpectationForProcessCommand(Commands.RequestLogout, Commands.RequestLogoutFailed, null);

            UserProperties user = new UserProperties(string.Empty, "password");
            this.SetStubsForReadingFromSerializerWrapper(typeof(UserProperties), user, null);

            failureReasonSerializerWrapper.Expect(c => c.WriteObject(this.xmlWriter, (object)FailureReasons.InvalidRequest))
                .Repeat.Once().WhenCalled(x => sendRequestLogoutFailed.Set());

            this.SetStubForEndOfTest(setEndOfTest);
            
            ServerProtocol serverProtocol = new ServerProtocol(
                this.stream,
                this.tokenGenerator,
                this.xmlWriterFactory,
                this.xmlReaderFactory,
                this.serializerWrapperFactory);

            Assert.IsTrue(sendRequestLogoutFailed.WaitOne(this.waitTime));
            Assert.IsTrue(setEndOfTest.WaitOne(this.waitTime));

            foreach (ISerializerWrapper serializer in this.serializers.Values)
            {
                serializer.VerifyAllExpectations();
            }
        }
        
        [Test]
        public void OnLogoutAfterLogInShouldReturnWithRequestLogoutAccepted()
        {
            this.SetStubsForServerProtocolInitiation();
            this.SetStubsForServerProtocolLogIn();
            
            ManualResetEvent setEndOfTest = new ManualResetEvent(false);
            ManualResetEvent sendRequestLogoutAccepted = new ManualResetEvent(false);

            this.SetStubsAndExpectationForProcessCommand(Commands.RequestLogout, Commands.RequestLogoutAccepted, sendRequestLogoutAccepted);

            UserProperties user = new UserProperties("user", "password");
            this.SetStubsForReadingFromSerializerWrapper(typeof(UserProperties), user, null);
            this.SetStubForEndOfTest(setEndOfTest);
            
            ServerProtocol serverProtocol = new ServerProtocol(
                this.stream,
                this.tokenGenerator,
                this.xmlWriterFactory,
                this.xmlReaderFactory,
                this.serializerWrapperFactory);

            Assert.IsTrue(sendRequestLogoutAccepted.WaitOne(this.waitTime));
            Assert.IsTrue(setEndOfTest.WaitOne(this.waitTime));
            
            foreach (ISerializerWrapper serializer in this.serializers.Values)
            {
                serializer.VerifyAllExpectations();
            }
        }
        
        [TestCase(new object[] { "", "password" })]
        [TestCase(new object[] { "user", "" })]
        public void OnNewAccountWithInvalidUserPropertiesShouldReturnWithRequestNewAccountFailed(
            string username, string password)
        {
            this.SetStubsForServerProtocolInitiation();

            ManualResetEvent sendRequestNewAccountFailed = new ManualResetEvent(false);
            ManualResetEvent setEndOfTest = new ManualResetEvent(false);

            this.SetStubsAndExpectationForProcessCommand(
                Commands.RequestNewAccount,
                Commands.RequestNewAccountFailed,
                sendRequestNewAccountFailed);

            UserProperties user = new UserProperties(username, password);
            this.SetStubsForReadingFromSerializerWrapper(typeof(UserProperties), user, null);

            this.SetExpectationForWritingObjectToSerializerWrapper(
                typeof(FailureReasons),
                FailureReasons.InvalidRequest,
                false);
            
            this.SetStubForEndOfTest(setEndOfTest);
            
            ServerProtocol serverProtocol = new ServerProtocol(
                this.stream,
                this.tokenGenerator,
                this.xmlWriterFactory,
                this.xmlReaderFactory,
                this.serializerWrapperFactory);

            Assert.IsTrue(sendRequestNewAccountFailed.WaitOne(this.waitTime));
            Assert.IsTrue(setEndOfTest.WaitOne(this.waitTime));

            foreach (ISerializerWrapper serializer in this.serializers.Values)
            {
                serializer.VerifyAllExpectations();
            }
        }
        
        [Test]
        public void OnNewAccountWithValidUserLoggedInShouldReturnRequestNewAccountAccepted()
        {
            this.SetStubsForServerProtocolInitiation();
            this.SetStubsForServerProtocolLogIn();

            ManualResetEvent sendRequestNewAccountAccepted = new ManualResetEvent(false);
            ManualResetEvent setEndOfTest = new ManualResetEvent(false);
           
            this.SetStubsAndExpectationForProcessCommand(
                Commands.RequestNewAccount,
                Commands.RequestNewAccountAccepted,
                sendRequestNewAccountAccepted);

            UserProperties newUser = new UserProperties("newUser", "password");
            this.SetStubsForReadingFromSerializerWrapper(typeof(UserProperties), newUser, null);

            this.accountManager.Stub(s => s.HasPermission(this.user.Login, (long)PermissionTypes.CreateUsers))
                .Return(true).Repeat.Once();
            this.accountManager.Stub(s => s.AuthenticateUser(newUser.Login, newUser.Password))
                .Return(false).Repeat.Once();
            this.accountManager.Stub(s => s.CreateAccount(newUser.Login, newUser.Password))
                .Return(true).Repeat.Once();
            this.SetStubForEndOfTest(setEndOfTest);
            
            ServerProtocol serverProtocol = new ServerProtocol(
                this.stream,
                this.tokenGenerator,
                this.xmlWriterFactory,
                this.xmlReaderFactory,
                this.serializerWrapperFactory);

            Assert.IsTrue(sendRequestNewAccountAccepted.WaitOne(this.waitTime));
            Assert.IsTrue(setEndOfTest.WaitOne(this.waitTime));

            foreach (ISerializerWrapper serializer in this.serializers.Values)
            {
                serializer.VerifyAllExpectations();
            }
        }
        
        [Test]
        public void OnNewAccountWithInvalidPermissionShouldReturnWithRequestNewAccountFailed()
        {
            this.SetStubsForServerProtocolInitiation();
            this.SetStubsForServerProtocolLogIn();

            ManualResetEvent sendRequestNewAccountFailed = new ManualResetEvent(false);
            ManualResetEvent setEndOfTest = new ManualResetEvent(false);

            this.SetStubsAndExpectationForProcessCommand(
                Commands.RequestNewAccount,
                Commands.RequestNewAccountFailed,
                sendRequestNewAccountFailed);

            UserProperties newUser = new UserProperties("newUser", "password");
            this.SetStubsForReadingFromSerializerWrapper(typeof(UserProperties), newUser, null);

            this.SetExpectationForWritingObjectToSerializerWrapper(
                typeof(FailureReasons), 
                FailureReasons.PermissionDenied, 
                false);

            this.accountManager.Stub(s => s.HasPermission(this.user.Login, (long)PermissionTypes.CreateUsers))
                .Return(false).Repeat.Once();
            this.accountManager.Expect(c => c.AuthenticateUser(newUser.Login, newUser.Password))
                .Return(false).Repeat.Never();
            this.accountManager.Expect(c => c.CreateAccount(newUser.Login, newUser.Password))
                .Return(true).Repeat.Never();

            this.SetStubForEndOfTest(setEndOfTest);
 
            ServerProtocol serverProtocol = new ServerProtocol(
                this.stream,
                this.tokenGenerator,
                this.xmlWriterFactory,
                this.xmlReaderFactory,
                this.serializerWrapperFactory);

            Assert.IsTrue(sendRequestNewAccountFailed.WaitOne(this.waitTime));
            Assert.IsTrue(setEndOfTest.WaitOne(this.waitTime));

            this.accountManager.VerifyAllExpectations();
            foreach (ISerializerWrapper serializer in this.serializers.Values)
            {
                serializer.VerifyAllExpectations();
            }
        }
        
        [TestCase(new object[] { "", "password" })]
        public void OnDeleteAccountWithInvalidUserPropertiesShouldReturnWithRequestDeleteAccountFailed(
            string username, string password)
        {
            this.SetStubsForServerProtocolInitiation();

            ManualResetEvent sendRequestDeleteAccountFailed = new ManualResetEvent(false);
            ManualResetEvent setEndOfTest = new ManualResetEvent(false);

            this.SetStubsAndExpectationForProcessCommand(
                Commands.RequestDeleteAcount,
                Commands.RequestDeleteAccountFailed,
                sendRequestDeleteAccountFailed);

            UserProperties user = new UserProperties(username, password);
            this.SetStubsForReadingFromSerializerWrapper(typeof(UserProperties), user, null);

            this.SetExpectationForWritingObjectToSerializerWrapper(
                typeof(FailureReasons),
                FailureReasons.InvalidRequest,
                false);

            this.SetStubForEndOfTest(setEndOfTest);

            ServerProtocol serverProtocol = new ServerProtocol(
                this.stream,
                this.tokenGenerator,
                this.xmlWriterFactory,
                this.xmlReaderFactory,
                this.serializerWrapperFactory);

            Assert.IsTrue(sendRequestDeleteAccountFailed.WaitOne(this.waitTime));
            Assert.IsTrue(setEndOfTest.WaitOne(this.waitTime));

            foreach (ISerializerWrapper serializer in this.serializers.Values)
            {
                serializer.VerifyAllExpectations();
            }
        }
        
        [Test]
        public void OnDeleteAccountWithValidUserLoggedInAndPropertiesShouldReturnRequestDeleteAccountAccepted()
        {
            this.SetStubsForServerProtocolInitiation();
            this.SetStubsForServerProtocolLogIn();

            ManualResetEvent sendRequestDeleteAccountAccepted = new ManualResetEvent(false);
            ManualResetEvent setEndOfTest = new ManualResetEvent(false);

            this.SetStubsAndExpectationForProcessCommand(
                Commands.RequestDeleteAcount,
                Commands.RequestDeleteAccountAccepted,
                sendRequestDeleteAccountAccepted);

            UserProperties deletedUser = new UserProperties("oldUser", "password");
            this.SetStubsForReadingFromSerializerWrapper(typeof(UserProperties), deletedUser, null);

            this.accountManager.Stub(s => s.HasPermission(this.user.Login, (long)PermissionTypes.CreateUsers))
                .Return(true).Repeat.Once();
            this.accountManager.Stub(s => s.GetPermissions(deletedUser.Login))
                .Return(new List<PermissionProperties>()).Repeat.Once();
            this.accountManager.Stub(s => s.DeleteAccount(deletedUser.Login))
                .Return(true).Repeat.Once();
            this.SetStubForEndOfTest(setEndOfTest);
            
            ServerProtocol serverProtocol = new ServerProtocol(
                this.stream,
                this.tokenGenerator,
                this.xmlWriterFactory,
                this.xmlReaderFactory,
                this.serializerWrapperFactory);

            Assert.IsTrue(sendRequestDeleteAccountAccepted.WaitOne(this.waitTime));
            Assert.IsTrue(setEndOfTest.WaitOne(this.waitTime));

            foreach (ISerializerWrapper serializer in this.serializers.Values)
            {
                serializer.VerifyAllExpectations();
            }
        }
        
        [TestCase(new object[] { "", "password" })]
        [TestCase(new object[] { "user", "" })]
        public void OnChangeAccountWithInvalidUserPropertiesShouldReturnWithRequestChangeAccountFailed(
            string username, string password)
        {
            this.SetStubsForServerProtocolInitiation();

            ManualResetEvent sendRequestChangeAccountFailed = new ManualResetEvent(false);
            ManualResetEvent setEndOfTest = new ManualResetEvent(false);
            
            this.SetStubsAndExpectationForProcessCommand(
                Commands.RequestChangeAccount,
                Commands.RequestChangeAccountRejected,
                sendRequestChangeAccountFailed);

            UserProperties user = new UserProperties(username, password);
            this.SetStubsForReadingFromSerializerWrapper(typeof(UserProperties), user, null);

            this.SetExpectationForWritingObjectToSerializerWrapper(
                typeof(FailureReasons), 
                FailureReasons.InvalidRequest, 
                false);
            this.SetStubForEndOfTest(setEndOfTest);
            
            ServerProtocol serverProtocol = new ServerProtocol(
                this.stream,
                this.tokenGenerator,
                this.xmlWriterFactory,
                this.xmlReaderFactory,
                this.serializerWrapperFactory);

            Assert.IsTrue(sendRequestChangeAccountFailed.WaitOne(this.waitTime));
            Assert.IsTrue(setEndOfTest.WaitOne(this.waitTime));

            foreach (ISerializerWrapper serializer in this.serializers.Values)
            {
                serializer.VerifyAllExpectations();
            }
        }
        
        [Test]
        public void OnChangeAccountWithValidUserLoggedInAndPropertiesShouldReturnWithRequestChangeAccountAccepted()
        {
            this.SetStubsForServerProtocolInitiation();
            this.SetStubsForServerProtocolLogIn();

            ManualResetEvent sendRequestChangeAccountAccepted = new ManualResetEvent(false);
            ManualResetEvent setEndOfTest = new ManualResetEvent(false);

            this.SetStubsAndExpectationForProcessCommand(
                Commands.RequestChangeAccount,
                Commands.RequestChangeAccountAccepted,
                sendRequestChangeAccountAccepted);

            UserProperties modifiedUser = new UserProperties("user", "newPassword");
            this.SetStubsForReadingFromSerializerWrapper(typeof(UserProperties), modifiedUser, null);

            this.accountManager.Stub(s => s.ChangePassword(modifiedUser.Login, modifiedUser.Password))
                .Return(true).Repeat.Once();
            this.SetStubForEndOfTest(setEndOfTest);

            ServerProtocol serverProtocol = new ServerProtocol(
                this.stream,
                this.tokenGenerator,
                this.xmlWriterFactory,
                this.xmlReaderFactory,
                this.serializerWrapperFactory);

            Assert.IsTrue(sendRequestChangeAccountAccepted.WaitOne(this.waitTime));
            Assert.IsTrue(setEndOfTest.WaitOne(this.waitTime));

            foreach (ISerializerWrapper serializer in this.serializers.Values)
            {
                serializer.VerifyAllExpectations();
            }
        }
        
        [Test]
        public void OnCreatePermissionWithInvalidPermissionNameShouldReturnWithRequestCreatePermissionRejected()
        {
            this.SetStubsForServerProtocolInitiation();

            ManualResetEvent sendRequestCreatePermissionRejected = new ManualResetEvent(false);
            ManualResetEvent setEndOfTest = new ManualResetEvent(false);

            this.SetStubsAndExpectationForProcessCommand(
                Commands.RequestCreatePermission,
                Commands.RequestCreatePermissionRejected,
                sendRequestCreatePermissionRejected);

            PermissionProperties permissionProperties = new PermissionProperties(string.Empty);
            this.SetStubsForReadingFromSerializerWrapper(typeof(PermissionProperties), permissionProperties, null);

            this.SetExpectationForWritingObjectToSerializerWrapper(
                typeof(FailureReasons),
                FailureReasons.InvalidRequest,
                false);

            this.SetStubForEndOfTest(setEndOfTest);

            ServerProtocol serverProtocol = new ServerProtocol(
                this.stream,
                this.tokenGenerator,
                this.xmlWriterFactory,
                this.xmlReaderFactory,
                this.serializerWrapperFactory);

            Assert.IsTrue(sendRequestCreatePermissionRejected.WaitOne(this.waitTime));
            Assert.IsTrue(setEndOfTest.WaitOne(this.waitTime));

            foreach (ISerializerWrapper serializer in this.serializers.Values)
            {
                serializer.VerifyAllExpectations();
            }
        }
        
        [Test]
        public void OnCreatePermissionWithValidPermissionAndUserLoggedInShouldReturnWithRequestCreatePermissionAccepted()
        {
            this.SetStubsForServerProtocolInitiation();
            this.SetStubsForServerProtocolLogIn();

            ManualResetEvent sendRequestCreatePermissionAccepted = new ManualResetEvent(false);
            ManualResetEvent setEndOfTest = new ManualResetEvent(false);

            this.SetStubsAndExpectationForProcessCommand(
                Commands.RequestCreatePermission,
                Commands.RequestCreatePermissionAccepted,
                sendRequestCreatePermissionAccepted);

            PermissionProperties permissionProperties = new PermissionProperties(
                (long)PermissionTypes.Admin,
                PermissionTypes.Admin.ToString());

            this.SetStubsForReadingFromSerializerWrapper(typeof(PermissionProperties), permissionProperties, null);

            this.accountManager.Stub(s => s.HasPermission(this.user.Login, (long)PermissionTypes.CreatePermissions))
               .Return(true).Repeat.Once();
            this.accountManager.Stub(s => s.CreatePermission(permissionProperties.Name))
                .Return(permissionProperties.Id);

            this.SetStubForEndOfTest(setEndOfTest);

            ServerProtocol serverProtocol = new ServerProtocol(
                this.stream,
                this.tokenGenerator,
                this.xmlWriterFactory,
                this.xmlReaderFactory,
                this.serializerWrapperFactory);

            Assert.IsTrue(sendRequestCreatePermissionAccepted.WaitOne(this.waitTime));
            Assert.IsTrue(setEndOfTest.WaitOne(this.waitTime));

            foreach (ISerializerWrapper serializer in this.serializers.Values)
            {
                serializer.VerifyAllExpectations();
            }
        }
        
        [Test]
        public void OnRemovePermissionWithInvalidPermissionIdShouldReturnWithRequestRemovePermissionRejected()
        {
            this.SetStubsForServerProtocolInitiation();

            ManualResetEvent sendRequestRemovePermissionRejected = new ManualResetEvent(false);
            ManualResetEvent setEndOfTest = new ManualResetEvent(false);

            this.SetStubsAndExpectationForProcessCommand(
                Commands.RequestRemovePermission,
                Commands.RequestRemovePermissionRejected,
                sendRequestRemovePermissionRejected);

            PermissionProperties permissionProperties = new PermissionProperties(0, string.Empty);
            this.SetStubsForReadingFromSerializerWrapper(typeof(PermissionProperties), permissionProperties, null);
            
            this.SetExpectationForWritingObjectToSerializerWrapper(
                typeof(FailureReasons),
                FailureReasons.InvalidRequest,
                false);

            this.SetStubForEndOfTest(setEndOfTest);

            ServerProtocol serverProtocol = new ServerProtocol(
                this.stream,
                this.tokenGenerator,
                this.xmlWriterFactory,
                this.xmlReaderFactory,
                this.serializerWrapperFactory);

            Assert.IsTrue(sendRequestRemovePermissionRejected.WaitOne(this.waitTime));
            Assert.IsTrue(setEndOfTest.WaitOne(this.waitTime));

            foreach (ISerializerWrapper serializer in this.serializers.Values)
            {
                serializer.VerifyAllExpectations();
            }
        }
        
        [Test]
        public void OnRemovePermissionWithValidPermissionAndUserLoggedInShouldReturnWithRequestRemovePermissionAccepted()
        {
            this.SetStubsForServerProtocolInitiation();
            this.SetStubsForServerProtocolLogIn();

            ManualResetEvent sendRequestRemovePermissionAccepted = new ManualResetEvent(false);
            ManualResetEvent setEndOfTest = new ManualResetEvent(false);

            this.SetStubsAndExpectationForProcessCommand(
                Commands.RequestRemovePermission,
                Commands.RequestRemovePermissionAccepted,
                sendRequestRemovePermissionAccepted);

            PermissionProperties permissionProperties = new PermissionProperties(
                (long)PermissionTypes.Admin,
                PermissionTypes.Admin.ToString());
            this.SetStubsForReadingFromSerializerWrapper(typeof(PermissionProperties), permissionProperties, null);

            this.accountManager.Stub(s => s.HasPermission(this.user.Login, (long)PermissionTypes.CreatePermissions))
               .Return(true).Repeat.Once();
            this.accountManager.Stub(s => s.DeletePermission(permissionProperties.Id))
                .Return(true);

            this.SetStubForEndOfTest(setEndOfTest);
            
            ServerProtocol serverProtocol = new ServerProtocol(
                this.stream,
                this.tokenGenerator,
                this.xmlWriterFactory,
                this.xmlReaderFactory,
                this.serializerWrapperFactory);

            Assert.IsTrue(sendRequestRemovePermissionAccepted.WaitOne(this.waitTime));
            Assert.IsTrue(setEndOfTest.WaitOne(this.waitTime));

            foreach (ISerializerWrapper serializer in this.serializers.Values)
            {
                serializer.VerifyAllExpectations();
            }
        }
        
        [TestCase(new object[] { 0, "admin" })]
        public void OnGrantPermissionWithInvalidPermissionShouldReturnWithRequestGrantPermissionRejected(
            long id, string permissionName)
        {
            this.SetStubsForServerProtocolInitiation();

            ManualResetEvent sendRequestGrantPermissionRejected = new ManualResetEvent(false);
            ManualResetEvent setEndOfTest = new ManualResetEvent(false);

            this.SetStubsAndExpectationForProcessCommand(
                Commands.RequestGrantPermission,
                Commands.RequestGrantPermissionRejected,
                sendRequestGrantPermissionRejected);

            UserProperties grantedUser = new UserProperties("grantedUser", "password");
            this.SetStubsForReadingFromSerializerWrapper(typeof(UserProperties), grantedUser, null);
            
            PermissionProperties permissionProperties = new PermissionProperties(id, permissionName);
            this.SetStubsForReadingFromSerializerWrapper(typeof(PermissionProperties), permissionProperties, null);
            
            this.SetExpectationForWritingObjectToSerializerWrapper(
                typeof(FailureReasons),
                FailureReasons.InvalidRequest,
                false);

            this.SetStubForEndOfTest(setEndOfTest);

            ServerProtocol serverProtocol = new ServerProtocol(
                this.stream,
                this.tokenGenerator,
                this.xmlWriterFactory,
                this.xmlReaderFactory,
                this.serializerWrapperFactory);

            Assert.IsTrue(sendRequestGrantPermissionRejected.WaitOne(this.waitTime));
            Assert.IsTrue(setEndOfTest.WaitOne(this.waitTime));

            foreach (ISerializerWrapper serializer in this.serializers.Values)
            {
                serializer.VerifyAllExpectations();
            }
        }
        
        [Test]
        public void OnGrantPermissionWithValidPermissionAndUserLoggedInShouldReturnWithRequestGrantPermissionAccepted()
        {
            this.SetStubsForServerProtocolInitiation();
            this.SetStubsForServerProtocolLogIn();

            ManualResetEvent sendRequestGrantPermissionAccepted = new ManualResetEvent(false);
            ManualResetEvent setEndOfTest = new ManualResetEvent(false);

            this.SetStubsAndExpectationForProcessCommand(
                Commands.RequestGrantPermission,
                Commands.RequestGrantPermissionAccepted,
                sendRequestGrantPermissionAccepted);

            PermissionProperties permissionProperties = new PermissionProperties(
                (long)PermissionTypes.Admin,
                PermissionTypes.Admin.ToString());
            this.SetStubsForReadingFromSerializerWrapper(typeof(PermissionProperties), permissionProperties, null);

            UserProperties grantedUser = new UserProperties("grantedUser", "password");
            this.SetStubsForReadingFromSerializerWrapper(typeof(UserProperties), grantedUser, null);

            this.accountManager.Stub(s => s.HasPermission(this.user.Login, (long)PermissionTypes.AssignPermissions))
               .Return(true).Repeat.Once();
            this.accountManager.Stub(s => s.GrantPermission(grantedUser.Login, permissionProperties.Id))
                .Return(true);

            this.SetStubForEndOfTest(setEndOfTest);
            
            ServerProtocol serverProtocol = new ServerProtocol(
                this.stream,
                this.tokenGenerator,
                this.xmlWriterFactory,
                this.xmlReaderFactory,
                this.serializerWrapperFactory);

            Assert.IsTrue(sendRequestGrantPermissionAccepted.WaitOne(this.waitTime));
            Assert.IsTrue(setEndOfTest.WaitOne(this.waitTime));

            foreach (ISerializerWrapper serializer in this.serializers.Values)
            {
                serializer.VerifyAllExpectations();
            }
        }
        
        [TestCase(new object[] { 0, "admin" })]
        public void OnRevokePermissionWithInvalidPermissionShouldReturnWithRequestRevokePermissionRejected(
            long id, string permissionName)
        {
            this.SetStubsForServerProtocolInitiation();

            ManualResetEvent sendRequestRevokePermissionRejected = new ManualResetEvent(false);
            ManualResetEvent setEndOfTest = new ManualResetEvent(false);

            this.SetStubsAndExpectationForProcessCommand(
                Commands.RequestRevokePermission,
                Commands.RequestRevokePermissionRejected,
                sendRequestRevokePermissionRejected);

            UserProperties revokedUser = new UserProperties("revokedUser", "password");
            this.SetStubsForReadingFromSerializerWrapper(typeof(UserProperties), revokedUser, null);

            PermissionProperties permissionProperties = new PermissionProperties(id, permissionName);
            this.SetStubsForReadingFromSerializerWrapper(typeof(PermissionProperties), permissionProperties, null);

            this.SetExpectationForWritingObjectToSerializerWrapper(
                typeof(FailureReasons),
                FailureReasons.InvalidRequest,
                false);

            this.SetStubForEndOfTest(setEndOfTest);

            ServerProtocol serverProtocol = new ServerProtocol(
                this.stream,
                this.tokenGenerator,
                this.xmlWriterFactory,
                this.xmlReaderFactory,
                this.serializerWrapperFactory);

            Assert.IsTrue(sendRequestRevokePermissionRejected.WaitOne(this.waitTime));
            Assert.IsTrue(setEndOfTest.WaitOne(this.waitTime));

            foreach (ISerializerWrapper serializer in this.serializers.Values)
            {
                serializer.VerifyAllExpectations();
            }
        }
        
        [Test]
        public void OnRevokePermissionWithValidPermissionAndUserLoggedInShouldReturnWithRequestRevokePermissionAccepted()
        {
            this.SetStubsForServerProtocolInitiation();
            this.SetStubsForServerProtocolLogIn();

            ManualResetEvent sendRequestRevokePermissionAccepted = new ManualResetEvent(false);
            ManualResetEvent setEndOfTest = new ManualResetEvent(false);

            this.SetStubsAndExpectationForProcessCommand(
                Commands.RequestRevokePermission,
                Commands.RequestRevokePermissionAccepted,
                sendRequestRevokePermissionAccepted);

            PermissionProperties permissionProperties = new PermissionProperties(
                (long)PermissionTypes.Admin,
                PermissionTypes.Admin.ToString());
            this.SetStubsForReadingFromSerializerWrapper(typeof(PermissionProperties), permissionProperties, null);

            UserProperties revokedUser = new UserProperties("revokedUser", "password");
            this.SetStubsForReadingFromSerializerWrapper(typeof(UserProperties), revokedUser, null);

            this.accountManager.Stub(s => s.HasPermission(this.user.Login, (long)PermissionTypes.AssignPermissions))
               .Return(true).Repeat.Once();
            this.accountManager.Stub(s => s.RevokePermission(revokedUser.Login, permissionProperties.Id))
                .Return(true);

            this.SetStubForEndOfTest(setEndOfTest);
            
            ServerProtocol serverProtocol = new ServerProtocol(
                this.stream,
                this.tokenGenerator,
                this.xmlWriterFactory,
                this.xmlReaderFactory,
                this.serializerWrapperFactory);

            Assert.IsTrue(sendRequestRevokePermissionAccepted.WaitOne(this.waitTime));
            Assert.IsTrue(setEndOfTest.WaitOne(this.waitTime));

            foreach (ISerializerWrapper serializer in this.serializers.Values)
            {
                serializer.VerifyAllExpectations();
            }
        }
        
        [Test]
        public void OnClientCertificateWithInvalidUserCertificateShouldReturnWithRequestClientCertificateFailed()
        {
            this.SetStubsForServerProtocolInitiation();

            ManualResetEvent sendRequestClientCertificateFailed = new ManualResetEvent(false);
            ManualResetEvent setEndOfTest = new ManualResetEvent(false);

            this.SetStubsAndExpectationForProcessCommand(
                Commands.RequestClientCertificate,
                Commands.RequestClientCertificateFailed,
                sendRequestClientCertificateFailed);

            // invalid user certificate
            UserCertificate userCertificate = new UserCertificate();
            userCertificate.Character = new Character(-1, "");
            userCertificate.UserId = -1;

            this.SetStubsForReadingFromSerializerWrapper(typeof(UserCertificate), userCertificate, null);

            this.SetExpectationForWritingObjectToSerializerWrapper(
                typeof(FailureReasons),
                FailureReasons.InvalidRequest,
                false);

            this.SetStubForEndOfTest(setEndOfTest);

            ServerProtocol serverProtocol = new ServerProtocol(
                this.stream,
                this.tokenGenerator,
                this.xmlWriterFactory,
                this.xmlReaderFactory,
                this.serializerWrapperFactory);

            Assert.IsTrue(sendRequestClientCertificateFailed.WaitOne(this.waitTime));
            Assert.IsTrue(setEndOfTest.WaitOne(this.waitTime));

            foreach (ISerializerWrapper serializer in this.serializers.Values)
            {
                serializer.VerifyAllExpectations();
            }
        }

        public void OnClientCertificateWithValidUserCertificateAndIncorrectCharacterShouldReturnWithRequestClientCertificateFailed()
        {
            this.SetStubsForServerProtocolInitiation();
            this.SetStubsForServerProtocolLogIn();

            ManualResetEvent sendRequestClientCertificateAccepted = new ManualResetEvent(false);
            ManualResetEvent setEndOfTest = new ManualResetEvent(false);

            this.SetStubsAndExpectationForProcessCommand(
                Commands.RequestClientCertificate,
                Commands.RequestClientCertificateAccepted,
                sendRequestClientCertificateAccepted);

            UserCertificate userCertificate = UserCertificate.CreateUnsignedClientCertificate(
                this.character,
                this.GeneratePublicKey().Key);
            this.SetStubsForReadingFromSerializerWrapper(typeof(UserCertificate), userCertificate, null);

            this.SetExpectationForWritingObjectToSerializerWrapper(
                typeof(FailureReasons),
                FailureReasons.NoneGiven,
                false);

            this.accountManager.Stub(s => s.HasPermission(this.user.Login, (long)PermissionTypes.Participation))
               .Return(true).Repeat.Once();
            this.accountManager.Stub(s => s.GetUserId(this.user.Login)).Return(1);
            this.accountManager.Stub(s => s.AuthorizeCharacter(this.user.Login, this.character)).Return(false).Repeat.Once();

            this.SetStubForEndOfTest(setEndOfTest);

            ServerProtocol serverProtocol = new ServerProtocol(
                this.stream,
                this.tokenGenerator,
                this.xmlWriterFactory,
                this.xmlReaderFactory,
                this.serializerWrapperFactory);

            Assert.IsTrue(sendRequestClientCertificateAccepted.WaitOne(this.waitTime));
            Assert.IsTrue(setEndOfTest.WaitOne(this.waitTime));

            foreach (ISerializerWrapper serializer in this.serializers.Values)
            {
                serializer.VerifyAllExpectations();
            }
        }

        [Test]
        public void OnClientCertificateWithValidUserCertificateShouldReturnWithRequestClientCertificateAccepted()
        {
            this.SetStubsForServerProtocolInitiation();
            this.SetStubsForServerProtocolLogIn();

            ManualResetEvent sendRequestClientCertificateAccepted = new ManualResetEvent(false);
            ManualResetEvent setEndOfTest = new ManualResetEvent(false);

            this.SetStubsAndExpectationForProcessCommand(
                Commands.RequestClientCertificate,
                Commands.RequestClientCertificateAccepted,
                sendRequestClientCertificateAccepted);

            UserCertificate userCertificate = UserCertificate.CreateUnsignedClientCertificate(
                this.character,
                this.GeneratePublicKey().Key);
            this.SetStubsForReadingFromSerializerWrapper(typeof(UserCertificate), userCertificate, null);

            this.accountManager.Stub(s => s.HasPermission(this.user.Login, (long)PermissionTypes.Participation))
               .Return(true).Repeat.Once();
            this.accountManager.Stub(s => s.GetUserId(this.user.Login)).Return(1);
            this.accountManager.Stub(s => s.AuthorizeCharacter(this.user.Login, this.character)).Return(true);

            this.SetStubForEndOfTest(setEndOfTest);

            ServerProtocol serverProtocol = new ServerProtocol(
                this.stream,
                this.tokenGenerator,
                this.xmlWriterFactory,
                this.xmlReaderFactory,
                this.serializerWrapperFactory);

            Assert.IsTrue(sendRequestClientCertificateAccepted.WaitOne(this.waitTime));
            Assert.IsTrue(setEndOfTest.WaitOne(this.waitTime));

            foreach (ISerializerWrapper serializer in this.serializers.Values)
            {
                serializer.VerifyAllExpectations();
            }
        }

        [Test]
        public void OnClientCertificateWithValidUserCertificateAndNoCharacterShouldReturnWithRequestClientCertificateAccepted()
        {
            this.SetStubsForServerProtocolInitiation();
            this.SetStubsForServerProtocolLogIn();

            ManualResetEvent sendRequestClientCertificateAccepted = new ManualResetEvent(false);
            ManualResetEvent setEndOfTest = new ManualResetEvent(false);

            this.SetStubsAndExpectationForProcessCommand(
                Commands.RequestClientCertificate,
                Commands.RequestClientCertificateAccepted,
                sendRequestClientCertificateAccepted);

            UserCertificate userCertificate = UserCertificate.CreateUnsignedClientCertificate(
                null,
                this.GeneratePublicKey().Key);
            this.SetStubsForReadingFromSerializerWrapper(typeof(UserCertificate), userCertificate, null);

            this.accountManager.Stub(s => s.HasPermission(this.user.Login, (long)PermissionTypes.Participation))
               .Return(true).Repeat.Once();
            this.accountManager.Stub(s => s.GetUserId(this.user.Login)).Return(1);

            this.SetStubForEndOfTest(setEndOfTest);

            ServerProtocol serverProtocol = new ServerProtocol(
                this.stream,
                this.tokenGenerator,
                this.xmlWriterFactory,
                this.xmlReaderFactory,
                this.serializerWrapperFactory);

            Assert.IsTrue(sendRequestClientCertificateAccepted.WaitOne(this.waitTime));
            Assert.IsTrue(setEndOfTest.WaitOne(this.waitTime));

            foreach (ISerializerWrapper serializer in this.serializers.Values)
            {
                serializer.VerifyAllExpectations();
            }
        }
        
        [Test]
        public void OnRequestParticipationKeyWithInvalidUserPropertiesShouldReturnWithRequestParticipationKeyRejected()
        {
            this.SetStubsForServerProtocolInitiation();

            ManualResetEvent sendRequestParticipationKeyRejected = new ManualResetEvent(false);
            ManualResetEvent setEndOfTest = new ManualResetEvent(false);

            this.SetStubsAndExpectationForProcessCommand(
                Commands.RequestParticipationKey,
                Commands.RequestParticipationKeyRejected,
                sendRequestParticipationKeyRejected);

            UserProperties invalidUser = new UserProperties(string.Empty, "password");
            this.SetStubsForReadingFromSerializerWrapper(typeof(UserProperties), invalidUser, null);

            TokenRequest tokenRequest = new TokenRequest(1);
            this.SetStubsForReadingFromSerializerWrapper(typeof(TokenRequest), tokenRequest, null);

            this.SetExpectationForWritingObjectToSerializerWrapper(
                typeof(FailureReasons),
                FailureReasons.InvalidRequest,
                false);

            this.SetStubForEndOfTest(setEndOfTest);

            ServerProtocol serverProtocol = new ServerProtocol(
                this.stream,
                this.tokenGenerator,
                this.xmlWriterFactory,
                this.xmlReaderFactory,
                this.serializerWrapperFactory);

            Assert.IsTrue(sendRequestParticipationKeyRejected.WaitOne(this.waitTime));
            Assert.IsTrue(setEndOfTest.WaitOne(this.waitTime));

            foreach (ISerializerWrapper serializer in this.serializers.Values)
            {
                serializer.VerifyAllExpectations();
            }
        }
        
        [Test]
        public void OnRequestParticipationKeyWithValidUserPropertiesShouldReturnWithRequestParticipationKeyAccepted()
        {
            this.SetStubsForServerProtocolInitiation();
            this.SetStubsForServerProtocolLogIn();

            ManualResetEvent sendRequestParticipationKeyAccepted = new ManualResetEvent(false);
            ManualResetEvent setEndOfTest = new ManualResetEvent(false);

            this.SetStubsAndExpectationForProcessCommand(
                Commands.RequestParticipationKey,
                Commands.RequestParticipationKeyAccepted,
                sendRequestParticipationKeyAccepted);

            this.SetStubsForReadingFromSerializerWrapper(typeof(UserProperties), this.user, null);

            TokenRequest tokenRequest = new TokenRequest(1);
            this.SetStubsForReadingFromSerializerWrapper(typeof(TokenRequest), tokenRequest, null);

            this.accountManager.Stub(s => s.HasPermission(this.user.Login, (long)PermissionTypes.Participation))
               .Return(true).Repeat.Once();
            
            this.SetExpectationForWritingObjectToSerializerWrapper(
                typeof(ParticipationKey),
                new ParticipationKey(),
                true);

            this.SetStubForEndOfTest(setEndOfTest);

            ServerProtocol serverProtocol = new ServerProtocol(
                this.stream,
                this.tokenGenerator,
                this.xmlWriterFactory,
                this.xmlReaderFactory,
                this.serializerWrapperFactory);

            Assert.IsTrue(sendRequestParticipationKeyAccepted.WaitOne(this.waitTime));
            Assert.IsTrue(setEndOfTest.WaitOne(this.waitTime));

            foreach (ISerializerWrapper serializer in this.serializers.Values)
            {
                serializer.VerifyAllExpectations();
            }
        }
        
        [Test]
        public void OnRequestPublicKeyWithInvalidUserPropertiesShouldReturnWithRequestServersPublicKeyRejected()
        {
            this.SetStubsForServerProtocolInitiation();

            ManualResetEvent sendRequestPublicKeyRejected = new ManualResetEvent(false);
            ManualResetEvent setEndOfTest = new ManualResetEvent(false);

            this.SetStubsAndExpectationForProcessCommand(
                Commands.RequestServersPublicKey,
                Commands.RequestServersPublicKeyRejected,
                sendRequestPublicKeyRejected);

            UserProperties invalidUser = new UserProperties(string.Empty, "password");
            this.SetStubsForReadingFromSerializerWrapper(typeof(UserProperties), invalidUser, null);

            TokenRequest tokenRequest = new TokenRequest(1);
            this.SetStubsForReadingFromSerializerWrapper(typeof(TokenRequest), tokenRequest, null);

            this.SetExpectationForWritingObjectToSerializerWrapper(
                typeof(FailureReasons),
                FailureReasons.InvalidRequest,
                false);

            this.SetStubForEndOfTest(setEndOfTest);

            ServerProtocol serverProtocol = new ServerProtocol(
                this.stream,
                this.tokenGenerator,
                this.xmlWriterFactory,
                this.xmlReaderFactory,
                this.serializerWrapperFactory);

            Assert.IsTrue(sendRequestPublicKeyRejected.WaitOne(this.waitTime));
            Assert.IsTrue(setEndOfTest.WaitOne(this.waitTime));
            
            foreach (ISerializerWrapper serializer in this.serializers.Values)
            {
                serializer.VerifyAllExpectations();
            }
        }
        
        [Test]
        public void OnRequestPublicKeyWithValidUserPropertiesShouldReturnWithRequestServersPublicKeyAccepted()
        {
            this.SetStubsForServerProtocolInitiation();
            this.SetStubsForServerProtocolLogIn();

            ManualResetEvent sendRequestPublicKeyAccepted = new ManualResetEvent(false);
            ManualResetEvent setEndOfTest = new ManualResetEvent(false);

            this.SetStubsAndExpectationForProcessCommand(
                Commands.RequestServersPublicKey,
                Commands.RequestServersPublicKeyAccepted,
                sendRequestPublicKeyAccepted);

            this.SetStubsForReadingFromSerializerWrapper(typeof(UserProperties), this.user, null);

            TokenRequest tokenRequest = new TokenRequest(1);
            this.SetStubsForReadingFromSerializerWrapper(typeof(TokenRequest), tokenRequest, null);

            this.accountManager.Stub(s => s.HasPermission(this.user.Login, (long)PermissionTypes.Participation))
               .Return(true).Repeat.Once();
            
            this.SetExpectationForWritingObjectToSerializerWrapper(
                typeof(PublicKey),
                new PublicKey(),
                true);
            
            this.SetStubForEndOfTest(setEndOfTest);

            ServerProtocol serverProtocol = new ServerProtocol(
                this.stream,
                this.tokenGenerator,
                this.xmlWriterFactory,
                this.xmlReaderFactory,
                this.serializerWrapperFactory);

            Assert.IsTrue(sendRequestPublicKeyAccepted.WaitOne(this.waitTime));
            Assert.IsTrue(setEndOfTest.WaitOne(this.waitTime));

            foreach (ISerializerWrapper serializer in this.serializers.Values)
            {
                serializer.VerifyAllExpectations();
            }
        }
        
        [Test]
        public void OnRequestTimeWithInvalidUserPropertiesShouldReturnWithRequestServerTimeRejected()
        {
            this.SetStubsForServerProtocolInitiation();

            ManualResetEvent sendRequestServerTimeRejected = new ManualResetEvent(false);
            ManualResetEvent setEndOfTest = new ManualResetEvent(false);

            this.SetStubsAndExpectationForProcessCommand(
                Commands.RequestServerTime,
                Commands.RequestServerTimeRejected,
                sendRequestServerTimeRejected);

            UserProperties invalidUser = new UserProperties(string.Empty, "password");
            this.SetStubsForReadingFromSerializerWrapper(typeof(UserProperties), invalidUser, null);

            this.SetExpectationForWritingObjectToSerializerWrapper(
                typeof(FailureReasons),
                FailureReasons.InvalidRequest,
                false);

            this.SetStubForEndOfTest(setEndOfTest);

            ServerProtocol serverProtocol = new ServerProtocol(
                this.stream,
                this.tokenGenerator,
                this.xmlWriterFactory,
                this.xmlReaderFactory,
                this.serializerWrapperFactory);

            Assert.IsTrue(sendRequestServerTimeRejected.WaitOne(this.waitTime));
            Assert.IsTrue(setEndOfTest.WaitOne(this.waitTime));

            foreach (ISerializerWrapper serializer in this.serializers.Values)
            {
                serializer.VerifyAllExpectations();
            }
        }
        
        [Test]
        public void OnRequestTimeWithValidUserPropertiesShouldReturnWithRequestServerTimeAccepted()
        {
            this.SetStubsForServerProtocolInitiation();
            this.SetStubsForServerProtocolLogIn();

            ManualResetEvent sendRequestServerTimeAccepted = new ManualResetEvent(false);
            ManualResetEvent setEndOfTest = new ManualResetEvent(false);

            this.SetStubsAndExpectationForProcessCommand(
                Commands.RequestServerTime,
                Commands.RequestServerTimeAccepted,
                sendRequestServerTimeAccepted);

            this.SetStubsForReadingFromSerializerWrapper(typeof(UserProperties), this.user, null);

            this.accountManager.Stub(s => s.HasPermission(this.user.Login, (long)PermissionTypes.Participation))
               .Return(true).Repeat.Once();
            
            this.SetExpectationForWritingObjectToSerializerWrapper(
                typeof(UserProperties),
                new UserProperties(),
                true);

            this.SetExpectationForWritingObjectToSerializerWrapper(
                typeof(ServerTime),
                new ServerTime(),
                true);
            
            this.SetStubForEndOfTest(setEndOfTest);

            ServerProtocol serverProtocol = new ServerProtocol(
                this.stream,
                this.tokenGenerator,
                this.xmlWriterFactory,
                this.xmlReaderFactory,
                this.serializerWrapperFactory);

            Assert.IsTrue(sendRequestServerTimeAccepted.WaitOne(this.waitTime));
            Assert.IsTrue(setEndOfTest.WaitOne(this.waitTime));

            foreach (ISerializerWrapper serializer in this.serializers.Values)
            {
                serializer.VerifyAllExpectations();
            }
        }

        [Test]
        public void OnRequestListUserWithInvalidListUserRequestShouldReturnWithRequestUserListFailed()
        {
            this.SetStubsForServerProtocolInitiation();

            ManualResetEvent sendRequestUserListFailed = new ManualResetEvent(false);
            ManualResetEvent setEndOfTest = new ManualResetEvent(false);

            this.SetStubsAndExpectationForProcessCommand(
                Commands.RequestUserList,
                Commands.RequestUserListFailed,
                sendRequestUserListFailed);

            ListUserRequest listUserRequest = new ListUserRequest();
            this.SetStubsForReadingFromSerializerWrapper(typeof(ListUserRequest), listUserRequest, null);

            this.SetExpectationForWritingObjectToSerializerWrapper(
                typeof(FailureReasons),
                FailureReasons.InvalidRequest,
                false);

            this.SetStubForEndOfTest(setEndOfTest);

            ServerProtocol serverProtocol = new ServerProtocol(
                this.stream,
                this.tokenGenerator,
                this.xmlWriterFactory,
                this.xmlReaderFactory,
                this.serializerWrapperFactory);

            Assert.IsTrue(sendRequestUserListFailed.WaitOne(this.waitTime));
            Assert.IsTrue(setEndOfTest.WaitOne(this.waitTime));

            foreach (ISerializerWrapper serializer in this.serializers.Values)
            {
                serializer.VerifyAllExpectations();
            }
        }
        
        [Test]
        public void OnRequestListUserWithValidListUserRequestShouldReturnWithRequestListComplete()
        {
            this.SetStubsForServerProtocolInitiation();
            this.SetStubsForServerProtocolLogIn();

            ManualResetEvent sendRequestUserListComplete = new ManualResetEvent(false);
            ManualResetEvent setEndOfTest = new ManualResetEvent(false);

            this.SetStubsAndExpectationForProcessCommand(
                Commands.RequestUserList,
                Commands.RequestUserListComplete,
                sendRequestUserListComplete);

            ListUserRequest listUserRequest = new ListUserRequest("*", 10, 0);
            this.SetStubsForReadingFromSerializerWrapper(typeof(ListUserRequest), listUserRequest, null);

            List<string> userList = new List<string>() { "admin", "user" };
            this.accountManager.Stub(s => s.HasPermission(this.user.Login, (long)PermissionTypes.ViewUsers))
               .Return(true).Repeat.Once();
            this.accountManager.Stub(s => s.FindUsers(listUserRequest.UsernameGlob))
                .Return(userList).Repeat.Once();

            this.SetStubForEndOfTest(setEndOfTest);

            ServerProtocol serverProtocol = new ServerProtocol(
                this.stream,
                this.tokenGenerator,
                this.xmlWriterFactory,
                this.xmlReaderFactory,
                this.serializerWrapperFactory);

            Assert.IsTrue(sendRequestUserListComplete.WaitOne(this.waitTime));
            Assert.IsTrue(setEndOfTest.WaitOne(this.waitTime));

            foreach (ISerializerWrapper serializer in this.serializers.Values)
            {
                serializer.VerifyAllExpectations();
            }
        }
        
        [Test]
        public void OnRequestUserCountWithInvalidListUserRequestShouldReturnWithRequestUserCountFailed()
        {
            this.SetStubsForServerProtocolInitiation();

            ManualResetEvent sendRequestUserCountFailed = new ManualResetEvent(false);
            ManualResetEvent setEndOfTest = new ManualResetEvent(false);

            this.SetStubsAndExpectationForProcessCommand(
                Commands.RequestUserCount,
                Commands.RequestUserCountFailed,
                sendRequestUserCountFailed);

            ListUserRequest listUserRequest = new ListUserRequest();
            this.SetStubsForReadingFromSerializerWrapper(typeof(ListUserRequest), listUserRequest, null);

            this.SetExpectationForWritingObjectToSerializerWrapper(
                typeof(FailureReasons),
                FailureReasons.InvalidRequest,
                false);

            this.SetStubForEndOfTest(setEndOfTest);

            ServerProtocol serverProtocol = new ServerProtocol(
                this.stream,
                this.tokenGenerator,
                this.xmlWriterFactory,
                this.xmlReaderFactory,
                this.serializerWrapperFactory);

            Assert.IsTrue(sendRequestUserCountFailed.WaitOne(this.waitTime));
            Assert.IsTrue(setEndOfTest.WaitOne(this.waitTime));

            foreach (ISerializerWrapper serializer in this.serializers.Values)
            {
                serializer.VerifyAllExpectations();
            }
        }
        
        [Test]
        public void OnRequestUserCountWithValidListUserRequestShouldReturnWithRequestUserCountComplete()
        {
            this.SetStubsForServerProtocolInitiation();
            this.SetStubsForServerProtocolLogIn();

            ManualResetEvent sendRequestUserCountComplete = new ManualResetEvent(false);
            ManualResetEvent setEndOfTest = new ManualResetEvent(false);

            this.SetStubsAndExpectationForProcessCommand(
                Commands.RequestUserCount,
                Commands.RequestUserCountComplete,
                sendRequestUserCountComplete);

            ListUserRequest listUserRequest = new ListUserRequest("*", 10, 0);
            this.SetStubsForReadingFromSerializerWrapper(typeof(ListUserRequest), listUserRequest, null);

            this.accountManager.Stub(s => s.HasPermission(this.user.Login, (long)PermissionTypes.ViewUsers))
               .Return(true).Repeat.Once();
            this.accountManager.Stub(s => s.GetUserCount(listUserRequest.UsernameGlob))
                .Return(15).Repeat.Once();

            this.SetStubForEndOfTest(setEndOfTest);
            
            ServerProtocol serverProtocol = new ServerProtocol(
                this.stream,
                this.tokenGenerator,
                this.xmlWriterFactory,
                this.xmlReaderFactory,
                this.serializerWrapperFactory);

            Assert.IsTrue(sendRequestUserCountComplete.WaitOne(this.waitTime));
            Assert.IsTrue(setEndOfTest.WaitOne(this.waitTime));

            foreach (ISerializerWrapper serializer in this.serializers.Values)
            {
                serializer.VerifyAllExpectations();
            }
        }
        
        [Test]
        public void OnRequestPermissionListWithInvalidInputShouldReturnWithRequestPermissionListFailed()
        {
            this.SetStubsForServerProtocolInitiation();

            ManualResetEvent sendRequestPermissionListFailed = new ManualResetEvent(false);
            ManualResetEvent setEndOfTest = new ManualResetEvent(false);

            this.SetStubsAndExpectationForProcessCommand(
                Commands.RequestPermissionList,
                Commands.RequestPermissionListFailed,
                sendRequestPermissionListFailed);

            // invalid PublicKey
            PublicKey publicKey = new PublicKey();
            publicKey.Key = null;

            this.SetStubsForReadingFromSerializerWrapper(typeof(UserProperties), this.user, null);
            this.SetStubsForReadingFromSerializerWrapper(typeof(PublicKey), publicKey, null);

            this.SetExpectationForWritingObjectToSerializerWrapper(
                typeof(FailureReasons),
                FailureReasons.InvalidRequest,
                false);

            this.SetStubForEndOfTest(setEndOfTest);
            
            ServerProtocol serverProtocol = new ServerProtocol(
                this.stream,
                this.tokenGenerator,
                this.xmlWriterFactory,
                this.xmlReaderFactory,
                this.serializerWrapperFactory);

            Assert.IsTrue(sendRequestPermissionListFailed.WaitOne(this.waitTime));
            Assert.IsTrue(setEndOfTest.WaitOne(this.waitTime));

            foreach (ISerializerWrapper serializer in this.serializers.Values)
            {
                serializer.VerifyAllExpectations();
            }
        }
        
        [Test]
        public void OnRequestPermissionListWithValidInputShouldReturnWithRequestPermissionListComplete()
        {
            this.SetStubsForServerProtocolInitiation();
            this.SetStubsForServerProtocolLogIn();

            ManualResetEvent sendRequestPermissionListComplete = new ManualResetEvent(false);
            ManualResetEvent setEndOfTest = new ManualResetEvent(false);

            this.SetStubsAndExpectationForProcessCommand(
                Commands.RequestPermissionList,
                Commands.RequestPermissionListComplete,
                sendRequestPermissionListComplete);

            this.SetStubsForReadingFromSerializerWrapper(typeof(UserProperties), this.user, null);
            this.SetStubsForReadingFromSerializerWrapper(typeof(PublicKey), this.GeneratePublicKey(), null);

            List<PermissionProperties> permissions = new List<PermissionProperties>()
            {
                new PermissionProperties((long)PermissionTypes.Admin, PermissionTypes.Admin.ToString())
            };

            this.accountManager.Stub(s => s.GetPermissions(this.user.Login))
                .Return(permissions).Repeat.Once();
            this.accountManager.Stub(s => s.GetParticipationValidityPeriod())
                .Return(this.keyValidityPeriod).Repeat.Once();

            this.SetExpectationForWritingObjectToSerializerWrapper(typeof(Commands), Commands.AddPermission, false);
            this.SetExpectationForWritingObjectToSerializerWrapper(typeof(Permission), null, true);

            this.SetStubForEndOfTest(setEndOfTest);

            ServerProtocol serverProtocol = new ServerProtocol(
                this.stream,
                this.tokenGenerator,
                this.xmlWriterFactory,
                this.xmlReaderFactory,
                this.serializerWrapperFactory);

            Assert.IsTrue(sendRequestPermissionListComplete.WaitOne(this.waitTime));
            Assert.IsTrue(setEndOfTest.WaitOne(this.waitTime));

            foreach (ISerializerWrapper serializer in this.serializers.Values)
            {
                serializer.VerifyAllExpectations();
            }
        }
        
        [Test]
        public void OnRequestListPermissionTypesWithEmptyUsernameShouldReturnWithRequestListPermissionTypesComplete()
        {
            this.SetStubsForServerProtocolInitiation();
            this.SetStubsForServerProtocolLogIn();

            ManualResetEvent sendRequestListPermissionTypesComplete = new ManualResetEvent(false);
            ManualResetEvent setEndOfTest = new ManualResetEvent(false);

            this.SetStubsAndExpectationForProcessCommand(
                Commands.RequestListPermissionTypes,
                Commands.RequestListPermissionTypesComplete,
                sendRequestListPermissionTypesComplete);

            UserProperties emptyUser = new UserProperties(string.Empty, "password");
            this.SetStubsForReadingFromSerializerWrapper(typeof(UserProperties), emptyUser, null);

            List<PermissionProperties> permissions = new List<PermissionProperties>()
            {
                new PermissionProperties(PermissionTypes.Admin.ToString()),
                new PermissionProperties(PermissionTypes.AssignPermissions.ToString()),
                new PermissionProperties(PermissionTypes.CreatePermissions.ToString())
            };

            this.accountManager.Stub(s => s.HasPermission(this.user.Login, (long)PermissionTypes.ViewPermissions))
               .Return(true).Repeat.Once();
            this.accountManager.Stub(s => s.ListAllPermissions()).Return(permissions).Repeat.Once();

            ISerializerWrapper commandSerializerWrapper = this.GetSerializer(typeof(Commands));
            commandSerializerWrapper.Expect(c => c.WriteObject(this.xmlWriter, Commands.AddPermissionType))
                .Repeat.Times(permissions.Count);

            ISerializerWrapper permissionPropertiesSerializerWrapper = this.GetSerializer(typeof(PermissionProperties));
            permissionPropertiesSerializerWrapper.Expect(c => c.WriteObject(this.xmlWriter, null))
                .IgnoreArguments().Repeat.Times(permissions.Count);
            
            this.SetStubForEndOfTest(setEndOfTest);
            
            ServerProtocol serverProtocol = new ServerProtocol(
                this.stream,
                this.tokenGenerator,
                this.xmlWriterFactory,
                this.xmlReaderFactory,
                this.serializerWrapperFactory);

            Assert.IsTrue(sendRequestListPermissionTypesComplete.WaitOne(this.waitTime));
            Assert.IsTrue(setEndOfTest.WaitOne(this.waitTime));

            foreach (ISerializerWrapper serializer in this.serializers.Values)
            {
                serializer.VerifyAllExpectations();
            }
        }
        
        [Test]
        public void OnRequestListPermissionTypesFromLoggedInUserShouldReturnWithRequestListPermissionTypesComplete()
        {
            this.SetStubsForServerProtocolInitiation();
            this.SetStubsForServerProtocolLogIn();

            ManualResetEvent sendRequestListPermissionTypesComplete = new ManualResetEvent(false);
            ManualResetEvent setEndOfTest = new ManualResetEvent(false);

            this.SetStubsAndExpectationForProcessCommand(
                Commands.RequestListPermissionTypes,
                Commands.RequestListPermissionTypesComplete,
                sendRequestListPermissionTypesComplete);

            this.SetStubsForReadingFromSerializerWrapper(typeof(UserProperties), this.user, null);

            List<PermissionProperties> permissions = new List<PermissionProperties>()
            {
                new PermissionProperties(PermissionTypes.Admin.ToString()),
                new PermissionProperties(PermissionTypes.CreateUsers.ToString()),
                new PermissionProperties(PermissionTypes.Participation.ToString())
            };

            this.accountManager.Stub(s => s.GetPermissions(this.user.Login))
                .Return(permissions).Repeat.Once();

            ISerializerWrapper commandSerializerWrapper = this.GetSerializer(typeof(Commands));
            commandSerializerWrapper.Expect(c => c.WriteObject(this.xmlWriter, Commands.AddPermissionType))
                .Repeat.Times(permissions.Count);

            ISerializerWrapper permissionPropertiesSerializerWrapper = this.GetSerializer(typeof(PermissionProperties));
            permissionPropertiesSerializerWrapper.Expect(c => c.WriteObject(this.xmlWriter, null))
                .IgnoreArguments().Repeat.Times(permissions.Count);

            this.SetStubForEndOfTest(setEndOfTest);

            ServerProtocol serverProtocol = new ServerProtocol(
                this.stream,
                this.tokenGenerator,
                this.xmlWriterFactory,
                this.xmlReaderFactory,
                this.serializerWrapperFactory);

            Assert.IsTrue(sendRequestListPermissionTypesComplete.WaitOne(this.waitTime));
            Assert.IsTrue(setEndOfTest.WaitOne(this.waitTime));

            foreach (ISerializerWrapper serializer in this.serializers.Values)
            {
                serializer.VerifyAllExpectations();
            }
        }
        
        private void SetStubsForServerProtocolInitiation()
        {
            ISerializerWrapper versionSerializerWrapper = this.GetSerializer(typeof(Version));

            this.xmlReader.Stub(s => s.Read()).Return(true);
            this.xmlReader.Stub(s => s.Name).Return(Resources.ConversationElementName);

            versionSerializerWrapper.Stub(s => s.CanDeserialize(this.xmlReader)).Return(true);
            versionSerializerWrapper.Stub(s => s.ReadObject(this.xmlReader))
                .Return((object)this.version);
        }

        private void SetStubsForServerProtocolLogIn()
        {
            this.SetStubsAndExpectationForProcessCommand(Commands.RequestLogin, Commands.RequestLoginAccepted, null);
            this.SetStubsForReadingFromSerializerWrapper(typeof(UserProperties), this.user, null);

            this.accountManager.Stub(s => s.AuthenticateUser(this.user.Login, this.user.Password))
                .Return(true).Repeat.Once();
            this.accountManager.Stub(s => s.GetCharacters(this.user.Login))
                .Return(this.emptyCharacters).Repeat.Once();
        }

        private void SetStubsAndExpectationForProcessCommand(
            Commands inputCommand, Commands expectedCommand, ManualResetEvent eventSet)
        {
            this.xmlReader.Stub(s => s.EOF).Return(false).Repeat.Once();

            ISerializerWrapper commandSerializerWrapper = this.GetSerializer(typeof(Commands));

            commandSerializerWrapper.Stub(s => s.CanDeserialize(this.xmlReader)).Return(true);
            commandSerializerWrapper.Expect(s => s.ReadObject(this.xmlReader)).Return(inputCommand);
            if (eventSet != null)
            {
                commandSerializerWrapper.Expect(c => c.WriteObject(this.xmlWriter, expectedCommand))
                    .Repeat.Once().WhenCalled(x => eventSet.Set());
            }
            else
            {
                commandSerializerWrapper.Expect(c => c.WriteObject(this.xmlWriter, expectedCommand))
                   .Repeat.Once();
            }
        }

        private void SetStubsForReadingFromSerializerWrapper(System.Type type, object value, ManualResetEvent eventSet)
        {
            ISerializerWrapper serializer = this.GetSerializer(type);

            serializer.Stub(s => s.CanDeserialize(this.xmlReader)).Return(true);

            if (eventSet != null)
            {
                serializer.Stub(s => s.ReadObject(this.xmlReader))
                    .Return(value).Repeat.Once().WhenCalled(x => eventSet.Set());
            }
            else
            {
                serializer.Stub(s => s.ReadObject(this.xmlReader))
                    .Return(value).Repeat.Once();
            }
        }

        private void SetExpectationForWritingObjectToSerializerWrapper(System.Type type, object value, bool ignoreArgument)
        {
            ISerializerWrapper serializer = this.GetSerializer(type);

            if (ignoreArgument)
            {
                serializer.Expect(c => c.WriteObject(this.xmlWriter, value)).IgnoreArguments().Repeat.Once();
            }
            else
            {
                serializer.Expect(c => c.WriteObject(this.xmlWriter, value)).Repeat.Once();
            }        
        }

        private void SetStubForEndOfTest(ManualResetEvent eventSet)
        {
            this.xmlReader.Stub(s => s.EOF).Return(true).WhenCalled(x => eventSet.Set());
        }

        private ISerializerWrapper GetSerializer(System.Type type)
        {
            return this.serializerWrapperFactory.Create(type);
        }

        private PublicKey GeneratePublicKey()
        {
            CspParameters cspParameters = new CspParameters(1);
            cspParameters.KeyContainerName = @"Dei_KeyStore";

            using (RSACryptoServiceProvider rsa = new RSACryptoServiceProvider(RsaAlgorithm.KeyLength, cspParameters))
            {
                PublicKey publicKey = new PublicKey(System.TimeSpan.MaxValue, rsa.ExportCspBlob(false));
                rsa.Clear();
                return publicKey;
            }
        }
    }
}
