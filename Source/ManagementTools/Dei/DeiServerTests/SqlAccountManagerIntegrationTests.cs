//-----------------------------------------------------------------------
// <copyright file="SqlAccountManagerIntegrationTests.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace DeiServerTests
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Data.Common;
    using Dei;
    using DeiServer;
    using NUnit.Framework;
    using Dei.Protocol;
    //using DeiAccountManager;
    using Badumna.Security;
    using System.Reflection;
    using System.Globalization;
    using System.IO;

    ////[TestFixture("System.Data.SqlClient", "Server=(local)\\SQLEXPRESS;Initial Catalog=DeiAccountsTest;Integrated Security=True;Pooling=False")]
    [TestFixture("System.Data.SQLite", "Data Source=DeiAccountsTest.s3db")]
    [TestFixture("MySql.Data.MySqlClient", "Server=localhost;Port=3306;Database=DeiAccountsTest;Uid=test;Pwd=test_password;")]
    [Category("Integration")]
    internal class SqlAccountManagerIntegrationTests
    {
        private readonly string testUsername;
        private readonly string testCharacterName;
        private readonly string testPassword;
        private readonly int testPermissionId;
        private readonly string testUsernamePattern;
        private readonly string dataProvider;
        private readonly string connectionString;
        private readonly TimeSpan validityPeriod;
        private IAccountManager accountManager;

        public SqlAccountManagerIntegrationTests(string dataProvider, string connectionString)
        {
            try
            {
                this.testUsername = this.GetUniqueString();
                this.testCharacterName = this.GetUniqueString();
                this.testPassword = this.GetUniqueString();
                this.testPermissionId = 98765;
                this.testUsernamePattern = this.testUsername.Substring(
                    0,
                    this.testUsername.Length / 2) + "%";
                this.dataProvider = dataProvider;
                this.connectionString = connectionString;
                this.validityPeriod = TimeSpan.FromHours(24);

                // Since DeiAccountManager is not signed, we need to load it dynamically:
                Assembly DeiAccountManagerDLL = Assembly.LoadFile(new FileInfo("DeiAccountManager.dll").FullName);
                this.accountManager = (IAccountManager) DeiAccountManagerDLL.CreateInstance(
                    "DeiAccountManager.SqlAccountManager",
                    false,
                    BindingFlags.Default,
                    null,
                    new object[] { this.dataProvider, this.connectionString, this.validityPeriod },
                    CultureInfo.CurrentCulture,
                    null);

                this.accountManager.Initialize();
                this.ClearDatabase();
                
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Exception caught: " + e + "\n" + e.StackTrace);
                throw;
            }
            
        }

        [TestFixtureTearDown]
        public void FixtureTearDown()
        {
            try
            {
                DbProviderFactory factory = DbProviderFactories.GetFactory(this.dataProvider);
                using (DbConnection connection = factory.CreateConnection())
                {
                    connection.ConnectionString = this.connectionString;
                    connection.Open();
                    DbCommand command = connection.CreateCommand();
                    command.CommandText = "DROP TABLE users ; DROP TABLE permissions ; DROP TABLE permission_types ; DROP TABLE characters";
                    command.ExecuteNonQuery();
                }
            }
            catch (DbException exception)
            {
                Console.WriteLine("Error removing tables: {0}", exception.Message);
            }
        }

        [SetUp]
        public void SetUp()
        {
            this.accountManager.CreateAccount(this.testUsername, this.testPassword);
        }

        [TearDown]
        public void TearDown()
        {
            this.ClearDatabase();
        }

        [Test]
        public void CreateAccountCreatesAccountWithNonNegativeId()
        {
            Assert.GreaterOrEqual(this.accountManager.GetUserId(this.testUsername), 0);
        }

        [Test]
        public void CreateCharacterCreatesCharacterWithNonNegativeId()
        {
            Character character = this.accountManager.CreateCharacter(this.testUsername, this.testCharacterName);
            Assert.IsNotNull(character);
            Assert.GreaterOrEqual(character.Id, 0);
        }

        [Test]
        public void CreateCharacterAllowsCharacterNamesThatMatchUserName()
        {
            Assert.IsNotNull(this.accountManager.CreateCharacter(this.testUsername, this.testUsername));
        }

        [Test]
        public void ListCharactersReturnsAllCharactersForTheUser()
        {
            string user2 = "user2";
            Assert.IsTrue(this.accountManager.CreateAccount(user2, "password"));

            string name1 = this.testCharacterName + "1";
            string name2 = this.testCharacterName + "2";
            Character char1 = this.accountManager.CreateCharacter(this.testUsername, name1);
            Character char2 = this.accountManager.CreateCharacter(this.testUsername, name2);
            
            Assert.IsNotNull(this.accountManager.CreateCharacter(user2, this.testCharacterName + "3"));

            Assert.IsNotNull(char1);
            Assert.IsNotNull(char2);

            IEnumerable<Character> unsortedChars = this.accountManager.GetCharacters(this.testUsername);
            var returnedCharacters = new List<Character>(unsortedChars);
            returnedCharacters.Sort(delegate(Character a, Character b)
            {
                return a.Name.CompareTo(b.Name);
            });

            Assert.AreEqual(returnedCharacters, new List<Character> { char1, char2 });
        }

        [Test]
        public void CreateCharacterFailsForUnknownUser()
        {
            Assert.IsNull(this.accountManager.CreateCharacter("Noexistant Norris", this.testCharacterName));
        }

        [Test]
        public void CreateCharacterFailsIfTheCharacterNameIsTakenByThisOrAnyOtherUser()
        {
            Assert.IsNotNull(this.accountManager.CreateCharacter(this.testUsername, this.testCharacterName));
            Assert.IsNull(this.accountManager.CreateCharacter(this.testUsername, this.testCharacterName));

            string secondUser = "user2";
            Assert.IsTrue(this.accountManager.CreateAccount(secondUser, "password"));
            Assert.IsNull(this.accountManager.CreateCharacter(secondUser, this.testCharacterName));
        }

        [Test]
        public void GetUserIdReturnsNegativeIdForEmptyUserName()
        {
            Assert.Less(this.accountManager.GetUserId(string.Empty), 0);
        }

        [Test]
        public void GetUserIdReturnsNegativeIdForUnknownUserName()
        {
            Assert.Less(this.accountManager.GetUserId(this.GetUniqueString()), 0);
        }

        [Test]
        public void GetUserIdReturnsNonNegativeIdForKnownUser()
        {
            Assert.GreaterOrEqual(this.accountManager.GetUserId(this.testUsername), 0);
        }

        [Test]
        public void AuthenticateUserFailsForEmptyUsername()
        {
            Assert.IsFalse(this.accountManager.AuthenticateUser(string.Empty, this.GetUniqueString()));
        }

        [Test]
        public void AuthenticateUserFailsForUnkownUsername()
        {
            Assert.IsFalse(this.accountManager.AuthenticateUser(this.GetUniqueString(), this.GetUniqueString()));
        }

        [Test]
        public void AuthenticateUserFailsForEmptyPassword()
        {
            Assert.IsFalse(this.accountManager.AuthenticateUser(this.testUsername, string.Empty));
        }

        [Test]
        public void AuthenticateUserFailsForWrongPassword()
        {
            Assert.IsFalse(this.accountManager.AuthenticateUser(this.testUsername, this.GetUniqueString()));
        }

        [Test]
        public void AuthenticateUserSucceedsForCorrectPassword()
        {
            Assert.IsTrue(this.accountManager.AuthenticateUser(this.testUsername, this.testPassword));
        }

        [Test]
        public void AccountMethodsSucceedWhenGivenSpecialCharacters()
        {
            string username = "Mike's Mischief$%\";--()";
            string password = "$%\";--()";
            string characterName = "$%\";--(player 1)";

            Assert.IsTrue(this.accountManager.CreateAccount(username, password));
            Assert.IsTrue(this.accountManager.AuthenticateUser(username, password));
            Character character = this.accountManager.CreateCharacter(username, characterName);
            Assert.IsTrue(this.accountManager.AuthorizeCharacter(username, character));
            Assert.IsTrue(this.accountManager.DeleteCharacter(username, character));
            Assert.IsTrue(this.accountManager.DeleteAccount(username));
        }

        [Test]
        public void DeleteAccountAlsoDeletesCharacters()
        {
            string secondUser = "user2";
            Assert.IsTrue(this.accountManager.CreateAccount(secondUser, "password"));
            Assert.IsNotNull(this.accountManager.CreateCharacter(this.testUsername, this.testCharacterName));
            
            // can't create a duplicate character name:
            Assert.IsNull(this.accountManager.CreateCharacter(secondUser, this.testCharacterName));
            
            // until we delete the user, and the created character goes with it
            Assert.IsTrue(this.accountManager.DeleteAccount(this.testUsername));
            Assert.IsNotNull(this.accountManager.CreateCharacter(secondUser, this.testCharacterName));
        }

        [Test]
        public void FindUsersFindsNoUsersForEmptyUsername()
        {
            Assert.AreEqual(0, this.accountManager.FindUsers(string.Empty).Count);
        }

        [Test]
        public void FindUsersFindsNoUsersForUnknownUsername()
        {
            Assert.AreEqual(0, this.accountManager.FindUsers(this.GetUniqueString()).Count);
        }

        [Test]
        public void FindUsersFindsMatchingUsername()
        {
            Assert.IsTrue(this.accountManager.FindUsers(this.testUsernamePattern).Contains(this.testUsername));
        }

        [Test]
        public void GetUserCountCountsNoUsersForEmptyUserName()
        {
            Assert.AreEqual(0, this.accountManager.GetUserCount(string.Empty));
        }

        [Test]
        public void GetUserCountCountsNoUsersForUnkownUserName()
        {
            Assert.AreEqual(0, this.accountManager.GetUserCount(this.GetUniqueString()));
        }

        [Test]
        public void GetUserCountCountsKnownUserName()
        {
            Assert.AreEqual(1, this.accountManager.GetUserCount(this.testUsername));
        }

        [Test]
        public void GetUserCountCountsUserMatchingPattern()
        {
            Assert.AreEqual(1, this.accountManager.GetUserCount(this.testUsernamePattern));
        }

        [Test]
        public void HasPermissionReturnsFalseForEmptyUsername()
        {
            Assert.IsFalse(this.accountManager.HasPermission(string.Empty, this.testPermissionId));
        }

        [Test]
        public void HasPermissionReturnsFalseForUnkownUsername()
        {
            Assert.IsFalse(this.accountManager.HasPermission(this.GetUniqueString(), this.testPermissionId));
        }

        [Test]
        public void HasPermissionReturnsFalseForUngrantedPermissionUsername()
        {
            Assert.IsFalse(this.accountManager.HasPermission(this.testUsername, this.testPermissionId));
        }

        [Test]
        public void HasPermissionReturnsTrueForGrantedPermission()
        {
            string permissionName = this.GetUniqueString();
            this.accountManager.CreatePermission(this.testPermissionId, permissionName);
            this.accountManager.GrantPermission(this.testUsername, this.testPermissionId);
            Assert.IsTrue(this.accountManager.HasPermission(this.testUsername, this.testPermissionId));
            this.accountManager.RevokePermission(this.testUsername, this.testPermissionId);
            this.accountManager.DeletePermission(this.testPermissionId);
        }

        [Test]
        public void DeleteCharacter()
        {
            Character character = this.accountManager.CreateCharacter(this.testUsername, this.testCharacterName);
            Assert.IsNotNull(character);
            Assert.IsTrue(this.accountManager.DeleteCharacter(this.testUsername, character));
        }

        [Test]
        public void DeleteCharacterOnlyDeletesCharacterIfItIsOwnedByTheGivenUser()
        {
            string owner = "user2";
            Assert.IsTrue(this.accountManager.CreateAccount(owner, "secret"));
            Character character = this.accountManager.CreateCharacter(owner, this.testCharacterName);
            Assert.IsNotNull(character);
            
            Assert.IsFalse(this.accountManager.DeleteCharacter(this.testUsername, character));
            Assert.IsTrue(this.accountManager.DeleteCharacter(owner, character));
        }

        [Test]
        public void CreateAccountFailsForEmptyUsername()
        {
            Assert.IsFalse(this.accountManager.CreateAccount(string.Empty, this.GetUniqueString()));
        }

        [Test]
        public void CreateAccountFailsForEmptyPassword()
        {
            Assert.IsFalse(this.accountManager.CreateAccount(this.GetUniqueString(), string.Empty));
        }

        [Test]
        public void CreateAccountFailsForExistingUsername()
        {
            Assert.IsFalse(this.accountManager.CreateAccount(this.testUsername, this.GetUniqueString()));
        }

        [Test]
        public void CreateAccountSucceedsForNewValidUsername()
        {
            string newUserName = this.GetUniqueString();
            Assert.IsTrue(this.accountManager.CreateAccount(newUserName, this.GetUniqueString()));
            this.accountManager.DeleteAccount(newUserName);
        }

        [Test]
        public void DeleteAccountFailsForUnkownUserName()
        {
            Assert.IsFalse(this.accountManager.DeleteAccount(this.GetUniqueString()));
        }

        [Test]
        public void DeleteAccountSucceedsForKnownUserName()
        {
            string newUserName = this.GetUniqueString();
            this.accountManager.CreateAccount(newUserName, this.GetUniqueString());
            Assert.IsTrue(this.accountManager.DeleteAccount(newUserName));
        }

        [Test]
        public void ChangePasswordFailsForEmptyUserName()
        {
            Assert.IsFalse(this.accountManager.ChangePassword(string.Empty, this.GetUniqueString()));
        }

        [Test]
        public void ChangePasswordFailsForUnkownUserName()
        {
            Assert.IsFalse(this.accountManager.ChangePassword(this.GetUniqueString(), this.GetUniqueString()));
        }

        [Test]
        public void ChangePasswordFailsForEmptyPassword()
        {
            Assert.IsFalse(this.accountManager.ChangePassword(this.testUsername, string.Empty));
        }

        [Test]
        public void ChangePasswordSucceedsForKnownUserName()
        {
            var tempPassword = this.GetUniqueString();
            Assert.IsTrue(this.accountManager.ChangePassword(this.testUsername, tempPassword));
            
            Assert.IsTrue(this.accountManager.AuthenticateUser(this.testUsername, tempPassword));
            Assert.IsFalse(this.accountManager.AuthenticateUser(this.testUsername, this.testPassword));

            Assert.IsTrue(this.accountManager.ChangePassword(this.testUsername, this.testPassword));

        }

        [TestCase(-1, "")]
        [TestCase(-1, null)]
        [TestCase(1, "")]
        [TestCase(1, null)]
        public void CreatePermissionFailsWithInvalidArguments(int permissionId, string permissionName)
        {
            Assert.IsFalse(this.accountManager.CreatePermission(permissionId, permissionName));
            Assert.Less(this.accountManager.CreatePermission(permissionName), 0);
        }

        [Test]
        public void CreatePermissionSucceedsWithNewValidName()
        {
            string newValidName = this.GetUniqueString();
            long permissionId = this.accountManager.CreatePermission(newValidName);
            Assert.GreaterOrEqual(permissionId, 0);
            this.accountManager.DeletePermission(permissionId);
        }

        [Test]
        public void CreatePermissionSucceedsWithNewValidNameAndId()
        {
            long permissionId = 1;
            string newValidName = this.GetUniqueString();
            Assert.IsTrue(this.accountManager.CreatePermission(permissionId, newValidName));
            this.accountManager.DeletePermission(permissionId);
        }

        [Test]
        public void CreatePermissionFailsWithExistingName()
        {
            string newValidName = this.GetUniqueString();
            long permissionId = this.accountManager.CreatePermission(newValidName);
            Assert.Less(this.accountManager.CreatePermission(newValidName), 0);
            this.accountManager.DeletePermission(permissionId);
        }

        [Test]
        public void CreatePermissionFailsWithExistingId()
        {
            string newValidName = this.GetUniqueString();
            long permissionId = this.accountManager.CreatePermission(newValidName);
            Assert.IsFalse(this.accountManager.CreatePermission(permissionId, this.GetUniqueString()));
            this.accountManager.DeletePermission(permissionId);
        }

        [Test]
        public void DeletePermissionFailsWithUnknownId()
        {
            long knownPermission = 1;
            long unknownPermission = knownPermission + 1;
            this.accountManager.CreatePermission(knownPermission, this.GetUniqueString());
            Assert.IsFalse(this.accountManager.DeletePermission(unknownPermission));
            this.accountManager.DeletePermission(knownPermission);
        }

        [Test]
        public void DeletePermissionSucceedsWithKnownId()
        {
            long knownPermission = 1;
            this.accountManager.CreatePermission(knownPermission, this.GetUniqueString());
            Assert.IsTrue(this.accountManager.DeletePermission(knownPermission));
        }

        [Test]
        public void GrantPermissionFailsWithEmptyUserName()
        {
            long permissionId = 1;
            this.accountManager.CreatePermission(permissionId, this.GetUniqueString());
            Assert.IsFalse(this.accountManager.GrantPermission(string.Empty, permissionId));
            this.accountManager.DeletePermission(permissionId);
        }

        [Test]
        public void GrantPermissionFailsWithUnknownPermission()
        {
            long permissionId = 1;
            this.accountManager.CreatePermission(permissionId, this.GetUniqueString());
            Assert.IsFalse(this.accountManager.GrantPermission(this.testUsername, permissionId + 1));
            this.accountManager.DeletePermission(permissionId);
        }

        [Test]
        public void GrantPermissionSucceedsWithKnownPermission()
        {
            long permissionId = 1;
            this.accountManager.CreatePermission(permissionId, this.GetUniqueString());
            Assert.IsTrue(this.accountManager.GrantPermission(this.testUsername, permissionId));
            this.accountManager.RevokePermission(this.testUsername, permissionId);
            this.accountManager.DeletePermission(permissionId);
        }

        [Test]
        public void RevokePermissionFailsForInvalidPermissionId()
        {
            long permissionId = -1;
            Assert.IsFalse(this.accountManager.RevokePermission(this.testUsername, permissionId));
        }

        [Test]
        public void RevokePermissionFailsForUnknownPermissionId()
        {
            long permissionId = 1;
            Assert.IsFalse(this.accountManager.RevokePermission(this.testUsername, permissionId));
        }

        [Test]
        public void RevokePermissionSucceedsForUngrantedPermissionId()
        {
            long permissionId = 1;
            this.accountManager.CreatePermission(permissionId, this.GetUniqueString());
            Assert.IsTrue(this.accountManager.RevokePermission(this.testUsername, permissionId));
            this.accountManager.DeletePermission(permissionId);
        }

        [Test]
        public void RevokePermissionFailsForEmptyUsername()
        {
            long permissionId = 1;
            this.accountManager.CreatePermission(permissionId, this.GetUniqueString());
            Assert.IsFalse(this.accountManager.RevokePermission(string.Empty, permissionId));
            this.accountManager.DeletePermission(permissionId);
        }

        [Test]
        public void RevokePermissionFailsForUnknownUserName()
        {
            long permissionId = 1;
            this.accountManager.CreatePermission(permissionId, this.GetUniqueString());
            Assert.IsFalse(this.accountManager.RevokePermission(this.GetUniqueString(), permissionId));
            this.accountManager.DeletePermission(permissionId);
        }

        [Test]
        public void RevokePermissionSucceedsForGrantedPermission()
        {
            long permissionId = 1;
            this.accountManager.CreatePermission(permissionId, this.GetUniqueString());
            this.accountManager.GrantPermission(this.testUsername, permissionId);
            Assert.IsTrue(this.accountManager.RevokePermission(this.testUsername, permissionId));
            this.accountManager.DeletePermission(permissionId);
        }

        [Test]
        public void GetPermissionsReturnsNoPermissionsWhenNoneGranted()
        {
            long permissionId = 1;
            this.accountManager.CreatePermission(permissionId, this.GetUniqueString());
            Assert.IsEmpty((ICollection)this.accountManager.GetPermissions(this.testUsername));
            this.accountManager.DeletePermission(permissionId);
        }

        [Test]
        public void GetPermissionsReturnsGrantedPermissions()
        {
            List<string> permissionNames = new List<string>();
            for (long i = 1; i < 10; ++i)
            {
                permissionNames.Add(this.GetUniqueString());
                this.accountManager.CreatePermission(i, permissionNames[(int)i - 1]);
                this.accountManager.GrantPermission(this.testUsername, i);
            }

            List<PermissionProperties> foundPermissions = new List<PermissionProperties>(
                this.accountManager.GetPermissions(this.testUsername));
            Assert.AreEqual(9, foundPermissions.Count);
            List<string> foundPermissionNames = new List<string>();
            foreach (PermissionProperties pp in foundPermissions)
            {
                foundPermissionNames.Add(pp.Name);
            }

            for (long i = 1; i < 10; ++i)
            {
                Assert.IsTrue(foundPermissionNames.Contains(permissionNames[(int)i - 1]));
                this.accountManager.RevokePermission(this.testUsername, i);
                this.accountManager.DeletePermission(i);
            }
        }

        [Test]
        public void GetParticipationValidityPeriodReturnsParticipationValidityPeriod()
        {
            Assert.AreEqual(this.validityPeriod, this.accountManager.GetParticipationValidityPeriod());
        }

        [TestCase(0)]
        [TestCase(1)]
        [TestCase(2)]
        [TestCase(20)]
        public void ListAllPermissionsListsAllPermissions(int numberOfPermissions)
        {
            for (long id = 0; id < numberOfPermissions; ++id)
            {
                this.accountManager.CreatePermission(id, this.GetUniqueString());
            }

            Assert.AreEqual(numberOfPermissions, this.accountManager.ListAllPermissions().Count);
        }

        #region Private Methods

        private string GetUniqueString()
        {
            return Guid.NewGuid().ToString();
        }

        private void ClearDatabase()
        {
            try
            {
                DbProviderFactory factory = DbProviderFactories.GetFactory(this.dataProvider);
                using (DbConnection connection = factory.CreateConnection())
                {
                    connection.ConnectionString = this.connectionString;
                    connection.Open();
                    DbCommand command = connection.CreateCommand();
                    command.CommandText = "DELETE FROM users ; DELETE FROM permissions ; DELETE FROM permission_types; DELETE FROM characters";
                    command.ExecuteNonQuery();
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine("Error clearing database: {0}", exception.Message);
            }
        }

        #endregion // Private methods
    }
}
