//-----------------------------------------------------------------------
// <copyright file="ServerTests.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace DeiServerTests
{
    using System;
    using System.IO;
    using DeiServer;
    using NUnit.Framework;
    using Rhino.Mocks;

    [TestFixture]
    internal class ServerTests
    {
        /// <summary>
        /// IAccountManager used for mocking.
        /// </summary>
        private IAccountManager accountManager;

        /// <summary>
        /// Listening port number.
        /// </summary>
        private int port = 21248;

        /// <summary>
        /// The key validity period. 
        /// </summary>
        private TimeSpan validityPeriod;

        public ServerTests()
        {
        }

        [SetUp]
        public void SetUp()
        {
            this.accountManager = MockRepository.GenerateMock<IAccountManager>();
            this.validityPeriod = TimeSpan.FromMinutes(10);
        }

        [TearDown]
        public void TearDown()
        {
            this.accountManager = null;
        }

        [Test]
        public void ServerConstructorWorks()
        {
            this.accountManager.Expect(c => c.GetParticipationValidityPeriod())
                .Return(this.validityPeriod).Repeat.Once();
            Server server = new Server(this.port, this.accountManager, null);
            
            // Stop the tcp listener
            server.Stop();
            server.Start();
            
            this.accountManager.VerifyAllExpectations();
        }

        [Test]
        public void ServerConstructorWithoutSslWorks()
        {
            bool useSslConnection = false;
            this.accountManager.Expect(c => c.GetParticipationValidityPeriod())
                .Return(this.validityPeriod).Repeat.Once();
            Server server = new Server(this.port, this.accountManager, null, useSslConnection);

            // Stop the tcp listener
            server.Stop();
            server.Start();

            this.accountManager.VerifyAllExpectations();
        }

        [Test]
        public void StopServerSuccessfully()
        {
            bool useSslConnection = false;
            this.accountManager.Expect(c => c.GetParticipationValidityPeriod())
                .Return(this.validityPeriod).Repeat.Once();
            Server server = new Server(this.port, this.accountManager, null, useSslConnection);

            this.accountManager.Expect(c => c.Dispose()).Repeat.Once();
            server.Stop();

            this.accountManager.VerifyAllExpectations();
        }

        [Test]
        public void GenerateKeypairViaServer()
        {
            Server.GenerateNewKeyPair();
            Assert.IsFalse(File.Exists("ParticipationKeys.xml"));
        }
    }
}
