﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Dei.Protocol;
using Dei;
using System.Xml;
using System.IO;
using Badumna.Security;

namespace DeiServerTests
{
    class LoginResultTests
    {
        [Test]
        public void LoginResultSerializesProperly()
        {
            var serializer = new SerializerWrapperFactory().Create(typeof(LoginResult));
            var str = new StringBuilder();
            var writer = XmlWriter.Create(str);
            var original = new LoginResult(new List<Character> { new Character(1, "one") });
            serializer.WriteObject(writer, original);

            object obj = serializer.ReadObject(XmlReader.Create(new StringReader(str.ToString())));
            Assert.IsTrue(original.Equals(obj as LoginResult));
        }
    }
}
