//-----------------------------------------------------------------------
// <copyright file="Program.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace DeiServerTests
{
    using System;
    using System.Reflection;
    using System.Collections.Generic;

    /// <summary>
    /// A program to launch the NUnit GUI and run the unit tests in this assembly.
    /// </summary>
    internal class Program
    {
        /// <summary>
        /// The entry point for the program.
        /// </summary>
        /// <param name="args">Command line arguments (pass "GUI" to use GUI).</param>
        [STAThread]
        internal static void Main(string[] args)
        {
            List<string> nunitArgs = new List<string> { Assembly.GetExecutingAssembly().Location };
            if (args.Length > 0 && args[0].ToUpper() == "GUI")
            {
                int firstArgIdx = nunitArgs.Count;
                nunitArgs.AddRange(args);
                nunitArgs.RemoveAt(firstArgIdx); // remove unneeded "GUI" arg
                NUnit.Gui.AppEntry.Main(nunitArgs.ToArray());
            }
            else
            {
                nunitArgs.Add("/wait");
                nunitArgs.AddRange(args);
                Console.WriteLine("Running nunit-console with args: " + string.Join(" ", nunitArgs.ToArray()));
                NUnit.ConsoleRunner.Runner.Main(nunitArgs.ToArray());
            }
        }
    }
}
