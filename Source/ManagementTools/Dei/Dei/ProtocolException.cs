//-----------------------------------------------------------------------
// <copyright file="ProtocolException.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace Dei
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    using System.Text;

    /// <summary>
    /// Protocol exception class derived from exception.
    /// </summary>
    [Serializable]
    public class ProtocolException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProtocolException"/> class.
        /// </summary>
        internal ProtocolException()
            : base() 
        { 
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ProtocolException"/> class.
        /// </summary>
        /// <param name="message">Exception message.</param>
        internal ProtocolException(string message)
            : base(message) 
        { 
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ProtocolException"/> class.
        /// </summary>
        /// <param name="message">Exception message.</param>
        /// <param name="innerException">Inner exception.</param>
        internal ProtocolException(string message, Exception innerException) 
            : base(message, innerException) 
        { 
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ProtocolException"/> class.
        /// </summary>
        /// <param name="serializationInfo">Serialization info</param>
        /// <param name="streamingContext">Streaming contect</param>
        internal ProtocolException(SerializationInfo serializationInfo, StreamingContext streamingContext)
            : base(serializationInfo, streamingContext) 
        { 
        }
    }
}
