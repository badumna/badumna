using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Dei")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: InternalsVisibleTo("DeiServer, PublicKey=00240000048000009400000006020000002400005253413100040000010001007DE84BBFE3DFB92D8A50A9B0B804EB07A02D28BB5B5665246B5A338A47DF060263EFE25FB2BE507841AA9247A054092558CCE470818F8C6EA295F8714F6E3F7494869E0B14C52F26A41E0E83F280E094E2C54150653DB0B1FFF68EA4FFBA589787839BE3F40BB6C3BC62A251AB54E60B0C1FF6C8E9AFD8BF0BE402929F40F4B6")]
[assembly: InternalsVisibleTo("DeiTests, PublicKey=0024000004800000940000000602000000240000525341310004000001000100df1377a45f64f59ae839a53fdd348317479767a6415214e33a30e776bc530447176d30408676032c907c791197d812214522106435d8a555ccd8fc00426f7248fdb2e8def63712ce0adbdcaa08d3b0e276bb9013f1eb5dcaaad036cbcd7e541d51d6bd1f2f94484a10be51aceee4d7639539870cd5ba5e17ab8baecab54222b3")]
[assembly: InternalsVisibleTo("DeiServerTests, PublicKey=00240000048000009400000006020000002400005253413100040000010001002312ebf1dbb64d30072ad917ad9bb58f112a6c354886999b6d6a36d5389d0b2fb231458d2c6259ab32a220de42bf7a2b7d038b53fb0651a2cdfcf702e8f28da42509d9324813748206815767895a89006a622d65c9381a6454e03e0faf641c3506faa73aeea296412019f98f476cfe966ad7148620f96c357368f799d396b6cb")]
[assembly: InternalsVisibleTo("DynamicProxyGenAssembly2, PublicKey=0024000004800000940000000602000000240000525341310004000001000100c547cac37abd99c8db225ef2f6c8a3602f3b3606cc9891605d02baa56104f4cfc0734aa39b93bf7852f7d9266654753cc297e7d2edfe0bac1cdcf9f717241550e0a7b191195b7667bb4f64bcb8e2121380fd1d9d46ad2d92d2d15605093924cceaf74c4861eff62abf69b9291ed0a340e113be11e6a7d3113e92484cf7045cc7")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("9bd263c1-a6f3-4309-8f52-79f62d3a4742")]
