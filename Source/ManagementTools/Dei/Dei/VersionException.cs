//-----------------------------------------------------------------------
// <copyright file="VersionException.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace Dei
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    using System.Text;

    /// <summary>
    /// Version exception class derived from Exception.
    /// </summary>
    [Serializable]
    public class VersionException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="VersionException"/> class.
        /// </summary>
        internal VersionException() 
            : base() 
        { 
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="VersionException"/> class.
        /// </summary>
        /// <param name="message">Exception message.</param>
        internal VersionException(string message) 
            : base(message) 
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="VersionException"/> class.
        /// </summary>
        /// <param name="message">Exception message.</param>
        /// <param name="innerException">Inner exception.</param>
        internal VersionException(string message, Exception innerException) 
            : base(message, innerException) 
        { 
        }

        /// <summary>
        ///  Initializes a new instance of the <see cref="VersionException"/> class.
        /// </summary>
        /// <param name="serializationInfo">Serialization info.</param>
        /// <param name="streamingContext">Streaming context.</param>
        internal VersionException(SerializationInfo serializationInfo, StreamingContext streamingContext)
            : base(serializationInfo, streamingContext) 
        {
        }                
    }
}
