//-----------------------------------------------------------------------
// <copyright file="IClient.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace Dei
{
    using System;
    using System.Collections.Generic;
    using Badumna.Security;

    /// <summary>
    /// IClient interface.
    /// </summary>
    public interface IClient : IDisposable
    {
        /// <summary>
        /// Gets a value indicating whether the client connection is closed.
        /// </summary>
        bool IsClosed { get; }

        /// <summary>
        /// Gets the Dei failure reason.
        /// </summary>
        FailureReasons FailureReason { get; }

        /// <summary>
        /// Login to the Dei server.
        /// </summary>
        /// <param name="userName">User name.</param>
        /// <param name="password">User password.</param>
        /// <returns>The login result.</returns>
        LoginResult Login(string userName, string password);

        /// <summary>
        /// Logout from the Dei server.
        /// </summary>
        void Logout();

        /// <summary>
        /// Gets the server time.
        /// </summary>
        /// <returns>Server time.</returns>
        ServerTime GetServerTime();

        /// <summary>
        /// Gets the participation key.
        /// </summary>
        /// <param name="isFirstRequest">A value indicating whether this is a first request.</param>
        /// <returns>Return participation key.</returns>
        ParticipationKey GetParticipationKey(bool isFirstRequest);

        /// <summary>
        /// Gets the server public key
        /// </summary>
        /// <param name="isFirstRequest">A value indicating whether this is a first request.</param>
        /// <returns>Return server public key.</returns>
        PublicKey GetServersPublicKey(bool isFirstRequest);

        /// <summary>
        /// Get the certificate based on the serialized public key.
        /// </summary>
        /// <param name="character">The character to identify as.</param>
        /// <param name="serializedPublicKey">Serialized public key.</param>
        /// <returns>Return a user certificate.</returns>
        UserCertificate GetCertificate(Character character, byte[] serializedPublicKey);

        /// <summary>
        /// Gets the permissions given the user public key.
        /// </summary>
        /// <param name="usersPublicKey">User public key.</param>
        /// <returns>List of permission.</returns>
        IList<Permission> GetPermissions(byte[] usersPublicKey);
        
        /// <summary>
        /// Get the permissions types for given username.
        /// </summary>
        /// <param name="username">User name.</param>
        /// <returns>Permission types that user has.</returns>
        IList<PermissionProperties> GetPermissionTypes(string username);

        /// <summary>
        /// Modified the account password.
        /// </summary>
        /// <param name="userName">User name.</param>
        /// <param name="newPassword">New password.</param>
        /// <returns>Return true on success.</returns>
        bool ChangeAccount(string userName, string newPassword);

        /// <summary>
        /// Create a new user account.
        /// </summary>
        /// <param name="userName">User name.</param>
        /// <param name="password">User password.</param>
        /// <returns>Return true on success.</returns>
        bool CreateNewAccount(string userName, string password);

        /// <summary>
        /// Delete an existing account.
        /// </summary>
        /// <param name="username">User name.</param>
        /// <returns>Return true on success.</returns>
        bool DeleteAccount(string username);

        /// <summary>
        /// Create a new character.
        /// </summary>
        /// <param name="characterName">Name of the character.</param>
        /// <returns>The created character, or `null` on failure.</returns>
        Character CreateCharacter(string characterName);
        
        /// <summary>
        /// Delete a character owned by the user.
        /// </summary>
        /// <param name="character">The character to delete.</param>
        /// <returns>Returns true on success.</returns>
        bool DeleteCharacter(Character character);

        /// <summary>
        /// Lists the characters belonging to the given user.
        /// </summary>
        /// <returns>A character list, or `null` on failure.</returns>
        IList<Character> ListCharacters();

        /// <summary>
        /// Create a new permission.
        /// </summary>
        /// <param name="permissionName">Permission name.</param>
        /// <returns>Permission id.</returns>
        long CreatePermission(string permissionName);

        /// <summary>
        /// Remove an existing permission.
        /// </summary>
        /// <param name="permisionType">Permission type.</param>
        /// <returns>Return true on success.</returns>
        bool RemovePermission(long permisionType);

        /// <summary>
        /// Grant a particular permission to a specific user.
        /// </summary>
        /// <param name="username">User name.</param>
        /// <param name="permissionId">Permission id.</param>
        /// <returns>Return true on success.</returns>
        bool GrantPermission(string username, long permissionId);

        /// <summary>
        /// Revoke a particular permission from a specific user.
        /// </summary>
        /// <param name="username">User name.</param>
        /// <param name="permisionId">Permission id.</param>
        /// <returns>Return true on success.</returns>
        bool RevokePermission(string username, long permisionId);

        /// <summary>
        /// List users which match with the given username glob.
        /// </summary>
        /// <param name="usernameGlob">User name glob.</param>
        /// <param name="startRange">Start range.</param>
        /// <param name="maximumNumberOfResults">Maximum number of result</param>
        /// <returns>Return list of user.</returns>
        IList<string> ListUsers(string usernameGlob, long startRange, long maximumNumberOfResults);

        /// <summary>
        /// Count the number of user.
        /// </summary>
        /// <param name="usernameGlob">User name glob.</param>
        /// <returns>Return number of current user.</returns>
        long GetUserCount(string usernameGlob);
    }
}
