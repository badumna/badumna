//-----------------------------------------------------------------------
// <copyright file="IExpirable.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace Dei
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Dei.Protocol;

    /// <summary>
    /// Interface for Expirable class.
    /// </summary>
    internal interface IExpirable
    {
        /// <summary>
        /// Gets or sets expiry time.
        /// </summary>
        UtcDateTime ExpiryTime { get; set; }

        /// <summary>
        /// Gets a value indicating whether is already expired.
        /// </summary>
        bool HasExpired { get; }
    }
}
