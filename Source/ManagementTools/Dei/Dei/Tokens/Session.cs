﻿//-----------------------------------------------------------------------
// <copyright file="Session.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2012 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Net.Security;
using System.Security;
using System.Security.Cryptography;
using Badumna;
using Badumna.Security;
using Badumna.Utilities;
using Dei.Tokens;

namespace Dei
{
    /// <summary>
    /// Ssl certificate error enumeration.
    /// </summary>
    public enum SslCertificateError
    {
        /// <summary>
        /// Certificate usage error.
        /// </summary>
        CertificateUsageError = -2146762490,

        /// <summary>
        /// Certificate name mismatch.
        /// </summary>
        RemoteCertificateNameMismatch = -2146762481,

        /// <summary>
        /// Certificate expired.
        /// </summary>
        NotTimeValid = -2146762495,

        /// <summary>
        /// Certificate have validity periods that are not nested.
        /// </summary>
        NotTimeNested = -2146762494,

        /// <summary>
        /// Specifies that the X509 chain is invalid due to an invalid certificate signature.
        /// </summary>
        NotSignatureValid = -2146869232,

        /// <summary>
        /// Specifies that the X509 chain is invalid due to an untrusted root certificate.
        /// </summary>
        UntrustedRoot = -2146762487,

        /// <summary>
        /// Specifies that the X509 chain is invalid due to invalid basic constraints.
        /// </summary>
        InvalidBasicConstraints = -2146869223,

        /// <summary>
        /// Specifies that the X509 chain could not be built up to the root certificate.
        /// </summary>
        PartialChain = -2146762486,
    }

    /// <summary>
    /// This class is used to authenticate and select a user identity via a Dei server.
    /// After successful calls to Authenticate() and SelectIdentity(), it creates an
    /// IIdentityProvider which can be used to log into a Badumna network securely.
    /// </summary>
    /// Client code should wrap the use of this object in a `using` block, or manually call
    /// its IDisposable.Dispose() method when this object is no longer required.
    public class Session : IDisposable
    {
        /// <summary>
        /// The client settings
        /// </summary>
        private ClientSettings clientSettings;

        /// <summary>
        /// The progress notification delegate
        /// </summary>
        private LoginProgressNotification progressNotification;

        /// <summary>
        /// The dei client driver
        /// </summary>
        private IClientDriver driver;

        /// <summary>
        /// The dei client driver
        /// </summary>
        private ClientDriverFactory clientDriverFactory = ClientDriver.Create;

        /// <summary>
        /// The curretn authentication result
        /// </summary>
        private LoginResult authenticationResult;

        /// <summary>
        /// Initializes a new instance of the <see cref="Session"/> class.
        /// </summary>
        /// <param name="serverHost">The host name (uri or ip address)</param>
        /// <param name="serverPort">The port of the dei server</param>
        /// <param name="keyPairXml">The Rsa key pair information as an XML string.</param>
        /// <param name="progressNotification">A LoginProgressNotificationDelegate to use for thie session (can be null).</param>
        public Session(string serverHost, ushort serverPort, string keyPairXml, LoginProgressNotification progressNotification)
        {
            var privateKey = new RSACryptoServiceProvider(CertificateToken.KeyLengthBits);
            this.progressNotification = progressNotification;
            
            try
            {
                privateKey.FromXmlString(keyPairXml);
            }
            catch (CryptographicException e)
            {
                throw new ArgumentException("Invalid Rsa key pair XML string", e);
            }
            catch (XmlSyntaxException e)
            {
                throw new ArgumentException("Invalid Rsa key pair XML string", e);
            }

            this.clientSettings = new ClientSettings(serverHost, serverPort, privateKey);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Session"/> class.
        /// A private key is generated automatically.
        /// </summary>
        /// <param name="serverHost">The server host.</param>
        /// <param name="serverPort">The server port.</param>
        /// <param name="progressNotification">The progress notification (can be null).</param>
        public Session(string serverHost, ushort serverPort, LoginProgressNotification progressNotification)
            : this(serverHost, serverPort, Session.GenerateKeyPair(), progressNotification)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Session"/> class.
        /// A private key is generated automatically.
        /// </summary>
        /// <param name="serverHost">The server host.</param>
        /// <param name="serverPort">The server port.</param>
        public Session(string serverHost, ushort serverPort)
            : this(serverHost, serverPort, Session.GenerateKeyPair(), null)
        {
        }

        /// <summary>
        /// Gets the failure reason for the most recent method called on this object.
        /// </summary>
        /// <remarks>In most cases this is just for additional information, however note that
        /// if a connection error occurs (in which case this property will return <see cref="FailureReasons.ConnectionError"/>),
        /// you will need to call the Authenticate() method again before any further methods will succeed.</remarks>
        public FailureReasons FailureReason
        {
            get
            {
                if (this.driver == null)
                {
                    return FailureReasons.NoneGiven;
                }

                return this.driver.FailureReason;
            }
        }

        /// <summary>
        /// Sets a value indicating whether to use an SSL connection.
        /// </summary>
        public bool UseSslConnection
        {
            set { this.clientSettings.UseSslConnection = value; }
        }

        /// <summary>
        /// Sets a value indicating whether to ignore SSL errors.
        /// </summary>
        /// Not all hosts have a valid trusted ssl certificate (especially when testing). Setting
        /// this field to true will ignore the certificate errors, which enables an INSECURE connection
        /// to be made anyway.
        public bool IgnoreSslErrors
        {
            set { this.clientSettings.IgnoreSslErrors = value; }
        }

        /// <summary>
        /// Sets a value indicating whether a ssl certificate name mismatch is ignored.
        /// </summary>
        public bool IgnoreSslCertificateNameMismatch
        {
            set { this.clientSettings.IgnoreSslCertificateNameMismatch = value; }
        }

        /// <summary>
        /// Sets a value indicating whether ssl selt signed certificate errors are ignored.
        /// </summary>
        public bool IgnoreSslSelfSignedCertificateErrors
        {
            set { this.clientSettings.IgnoreSslSelfSignedCertificateErrors = value; }
        }
        
        /// <summary>
        /// Sets a custom certificate validation callback in order to override certificate validation errors.
        /// </summary>
        public RemoteCertificateValidationCallback CertificateValidationCallback
        {
            set { this.clientSettings.CertificateValidationCallback = value; }
        }

        /// <summary>
        /// Sets the client driver factory.
        /// used only for tests
        /// </summary>
        /// <value>
        /// The client driver factory.
        /// </value>
        internal ClientDriverFactory ClientDriverFactory
        {
            set { this.clientDriverFactory = value; }
        }

        /// <summary>
        /// Generates a new RSA key pair and returns an XML string containing a newly generated key pair.
        /// </summary>
        /// <returns>Returns an XML string containing a newly generated key pair.</returns>
        public static string GenerateKeyPair()
        {
            return Key.GenerateKeyPair(true);
        }

        /// <summary>
        /// Authenticates the specified user.
        /// </summary>
        /// <para>
        /// Authenticates the user with the server. If the user is unknown, has given the incorrect password, or 
        /// does not have permission to join the network then the returned <see cref="LoginResult"/>
        /// object will have its <see cref="LoginResult.WasSuccessful"/> property set to false and 
        /// its <see cref="LoginResult.ErrorDescription"/> property set appropriately.
        /// </para>
        /// <para>
        /// After calling Authenticate, you must call Activate before this <see cref="IdentityProvider" />
        /// can be passed into the Badumna NetworkFacade's Login method.
        /// </para>
        /// <param name="userName">Name of the user.</param>
        /// <param name="password">The password.</param>
        /// <returns>A <see cref="LoginResult"/> object</returns>
        public LoginResult Authenticate(string userName, string password)
        {
            if (userName == null)
            {
                throw new ArgumentNullException("userName");
            }

            if (password == null)
            {
                throw new ArgumentNullException("password");
            }

            // Authenticate discards any existing login attempt
            this.Dispose();

            this.driver = this.clientDriverFactory(userName, password, this.clientSettings);
            this.authenticationResult = this.driver.OpenSession(this.progressNotification);
            return this.authenticationResult;
        }
        
        /// <inheritdoc/>
        public void Dispose()
        {
            this.authenticationResult = null;
            if (this.driver != null)
            {
                this.driver.Dispose();
                this.driver = null;
            }
        }

        /// <summary>
        /// Activates the user session with the specified character.
        /// </summary>
        /// <para>
        /// A user may have multiple characters, but can only act as one character at a time. This
        /// method sets the active character and triggers the retrieval of the character's security
        /// tokens from the Dei server.
        /// </para>
        /// <para>
        /// You may pass in <See cref="Badumna.Security.Character.None"/> if this client will not need to use any
        /// character-based services (such as Chat).
        /// </para>
        /// <para>
        /// A list of characters for the authenticated user is
        /// contained within the `LoginResult` object returned by <see cref="Authenticate"/>.
        /// Note that if the user has no characters, you will need to call <see cref="CreateCharacter"/>
        /// before you can call this method successfully.
        /// </para>
        /// <para>
        /// Once this method returns success, the <see cref="IdentityProvider" /> out object should be passed
        /// to the Badumna NetworkFacade's Login method.
        /// </para>
        /// <param name="character">The character to authorize as.</param>
        /// <param name="identity">An IIdentityProvider reference, which will be set on success.</param>
        /// <returns>A <see cref="LoginResult"/> object</returns>
        public LoginResult SelectIdentity(Character character, out IIdentityProvider identity)
        {
            this.AssertAuthenticated();

            if (!(character == null || character.IsValid))
            {
                throw new ArgumentException("character");
            }

            Logger.TraceInformation(LogTag.Crypto | LogTag.Event, "authorizing as character: {0}", character == null ? "NULL" : character.ToString());
            LoginResult loginResult = this.driver.Authorize(character, this.authenticationResult, this.progressNotification);

            if (loginResult.WasSuccessful)
            {
                // create a new IdentityProvider with a copy of our ClientDriver
                identity = new IdentityProvider(character, this.driver.Clone());
            }
            else
            {
                identity = null;
            }

            return loginResult;
        }

        /// <summary>
        /// Creates a new character for the currently authenticated user.
        /// </summary>
        /// <param name="characterName">Name of the character.</param>
        /// <returns>The character on success, or `null` if the character could not be created.</returns>
        public Character CreateCharacter(string characterName)
        {
            this.AssertAuthenticated();
            return this.driver.CreateCharacter(characterName);
        }

        /// <summary>
        /// Deleted a character owned by the currently authenticated user.
        /// </summary>
        /// <param name="character">The character.</param>
        /// <returns>Whether the deletion succeeded.</returns>
        public bool DeleteCharacter(Character character)
        {
            this.AssertAuthenticated();
            return this.driver.DeleteCharacter(character);
        }

        /// <summary>
        /// Retrieves a list of the current user's characters from the server.
        /// </summary>
        /// <remarks>
        /// <para>
        /// The same list is returned within the return value of Authenticate(),
        /// so you should only need to call this method if the list of characters may have changed
        /// since the last call to Authenticate()
        /// </para>
        /// </remarks>
        /// <returns>A list of the user's characters</returns>
        public IList<Character> ListCharacters()
        {
            this.AssertAuthenticated();
            return this.driver.ListCharacters();
        }

        /// <summary>
        /// Asserts that this object is connected &amp; authenticated.
        /// </summary>
        private void AssertAuthenticated()
        {
            if (this.authenticationResult == null || !this.authenticationResult.WasSuccessful)
            {
                throw new InvalidOperationException("This session is not authenticated");
            }
        }
    }
}
