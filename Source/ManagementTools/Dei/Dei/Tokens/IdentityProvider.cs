//---------------------------------------------------------------------------------
// <copyright file="IdentityProvider.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

namespace Dei
{
    using System;
    using System.Diagnostics;
    using System.Threading;
    using Badumna.Security;
    using Badumna.Utilities;
    using Dei.Tokens;
    using Badumna;


    /// <summary>
    /// An implementation of a Badumna identity provider
    /// </summary>
    /// <para>
    /// The Dei identity provider stores (and periodically updates) signed security tokens
    /// from a Dei server for use by Badumna to maintain network integrity.
    /// </para>
    public class IdentityProvider : IIdentityProvider
    {
        /// <summary>
        /// Client character.
        /// </summary>
        private readonly Character character;

        /// <summary>
        /// The background monitor thread used to refresh tokens. 
        /// </summary>
        protected Thread monitorThread;

        /// <summary>
        /// The flag used to signal termination. ManualResetEvent should not be used here as its wait one method will
        /// crash the old Unity/Mono 1.2.5.
        /// </summary>
        private volatile bool stopped = false;

        /// <summary>
        /// The client driver
        /// </summary>
        private IClientDriver driver;

        /// <summary>
        /// Initializes a new instance of the <see cref="IdentityProvider"/> class.
        /// </summary>
        /// Create an identity provider that connects to a Dei server.
        /// <param name="character">The identity.</param>
        /// <param name="driver">The Dei ClientDriver</param>
        internal IdentityProvider(Character character, IClientDriver driver)
        {
            this.character = character;
            this.driver = driver;
        }

        #region IIdentityProvider Members

        /// <inheritdoc/>
        public SymmetricKeyToken GetNetworkParticipationToken()
        {
            return this.driver.GetNetworkParticipationToken();
        }

        /// <inheritdoc/>
        public PermissionListToken GetPermissionListToken()
        {
            return this.driver.GetPermissionListToken();
        }

        /// <inheritdoc/>
        public TrustedAuthorityToken GetTrustedAuthorityToken()
        {
            return this.driver.GetTrustedAuthorityToken();
        }

        /// <inheritdoc/>
        public CertificateToken GetUserCertificateToken()
        {
            return this.driver.GetUserCertificateToken();
        }

        /// <inheritdoc/>
        public TimeToken GetTimeToken()
        {
            return this.driver.GetTimeToken();
        }

        /// <inheritdoc/>
        public void Activate()
        {
            if (this.monitorThread == null)
            {
                this.monitorThread = new Thread(this.Monitor);
                this.monitorThread.IsBackground = true;
                this.monitorThread.Start();
            }
        }

        /// <inheritdoc/>
        public void Dispose()
        {
            // note that this.driver is used in the monitor thread, so we
            // must stop the thread before disposing of the driver
            if (this.monitorThread != null)
            {
                this.stopped = true;
                this.monitorThread.Join();
                this.monitorThread = null;
            }

            if (this.driver != null)
            {
                this.driver.Dispose();
                this.driver = null;
            }
        }

        #endregion

        /// <summary>
        /// Monitor runs on different thread.
        /// </summary>
        private void Monitor()
        {
            while (true)
            {
                if (this.stopped)
                {
                    // time to terminate this thread.
                    return;
                }
                else
                {
                    // using Thread.Sleep(100) here because ManualResetEvent.WaitOne is not supported on
                    // Unity 2.6/Mono 1.2.5
                    Thread.Sleep(100);

                    if (this.driver.NeedsRefresh)
                    {
                        // time to refresh tokens
                        try
                        {
                            LoginResult loginResult = this.driver.Refresh(this.character);
                            if (!loginResult.WasSuccessful)
                            {
                                Logger.TraceError(
                                    LogTag.Dei,
                                    "IdentityProvider.Refresh() failed! Error description: {0}",
                                    loginResult.ErrorDescription);
                            }

                        }
                        catch (Exception e)
                        {
                            Logger.TraceException(LogLevel.Error, e, "Exception caught in IdentityProvider's monitor thread :");
                            Console.Error.WriteLine("Exception caught in IdentityProvider's monitor thread : {0}", e);
                        }
                    }
                }
            }
        }
    }
}
