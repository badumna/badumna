﻿//-----------------------------------------------------------------------
// <copyright file="ClientDriver.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2012 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net.Security;
using System.Net.Sockets;
using System.Reflection;
using System.Security.Authentication;
using Badumna;
using Badumna.Security;
using Badumna.Utilities;
using Dei.Protocol;

namespace Dei.Tokens
{
    /// <summary>
    /// Factory type for ClientDriver
    /// </summary>
    /// <param name="username">The username.</param>
    /// <param name="password">The password.</param>
    /// <param name="settings">The settings.</param>
    /// <returns>A client driver</returns>
    internal delegate IClientDriver ClientDriverFactory(string username, string password, ClientSettings settings);

    /// <summary>
    /// A thread-safe object that fetches and stores tokens from Dei.
    /// NOTE that this object gets cloned upon successful authorization,
    /// so all fields must either reference immutable objects
    /// or be explicitly assigned to in the `InitializeOwnedFields()` method.
    /// </summary>
    internal class ClientDriver : IDisposable, IClientDriver
    {
        /// <summary>
        /// Random number source.
        /// </summary>
        private static Random randomSource = new Random((int)DateTime.Now.Ticks);

        /// <summary>
        /// Object session lock.
        /// </summary>
        private object sessionLock;

        /// <summary>
        /// The client settings
        /// </summary>
        private ClientSettings clientSettings;

        /// <summary>
        /// The username
        /// </summary>
        private string userName;

        /// <summary>
        /// User password
        /// </summary>
        private string password;

        /// <summary>
        /// Participation token.
        /// </summary>
        private SymmetricKeyToken participationToken;

        /// <summary>
        /// Trusted authority token
        /// </summary>
        private TrustedAuthorityToken trustedAuthorityKeyToken;

        /// <summary>
        /// Certificate token.
        /// </summary>
        private CertificateToken certificateToken;

        /// <summary>
        /// Permission list token.
        /// </summary>
        private PermissionListToken permissionListToken;

        /// <summary>
        /// Time token.
        /// </summary>
        private TimeToken timeToken;

        /// <summary>
        /// Refresh time.
        /// </summary>
        private DateTime refreshTime = DateTime.Now;

        /// <summary>
        /// The remaining life time of the currently valid credentials.
        /// </summary>
        private TimeSpan remainingLifetime;
        
        /// <summary>
        /// Whether the supplier still has the second chance to refresh its tokens.
        /// </summary>
        private bool secondChanceAvailable;

        /// <summary>
        /// Initializes a new instance of the <see cref="ClientDriver"/> class.
        /// </summary>
        /// <param name="userName">The username.</param>
        /// <param name="password">The password.</param>
        /// <param name="settings">The settings.</param>
        internal ClientDriver(string userName, string password, ClientSettings settings)
        {
            this.InitializeOwnedFields();
            this.userName = userName;
            this.password = password;
            this.clientSettings = settings;

            // second chance is only available once we have successfully fetched tokens
            this.secondChanceAvailable = false;
        }

        /// <summary>
        /// Gets the network time.
        /// </summary>
        public Clock NetworkTime
        {
            get
            {
                return this.clientSettings.NetworkTime;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this object is due for a token refresh.
        /// </summary>
        /// <value>
        ///   <c>true</c> if a refresh is due; otherwise, <c>false</c>.
        /// </value>
        public bool NeedsRefresh
        {
            get { return this.NetworkTime.Now >= this.refreshTime; }
        }

        /// <summary>
        /// Gets the failure reason.
        /// </summary>
        public FailureReasons FailureReason
        {
            get
            {
                if (this.Client == null)
                {
                    return FailureReasons.NoneGiven;
                }

                return this.Client.FailureReason;
            }
        }

        /// <summary>
        /// Gets or sets The Dei client.
        /// </summary>
        protected IClient Client { get; set; }

        /// <summary>
        /// The default ClientDriver factory
        /// </summary>
        /// <param name="username">The username.</param>
        /// <param name="password">The password.</param>
        /// <param name="settings">The settings.</param>
        /// <returns>A client driver</returns>
        public static ClientDriver Create(string username, string password, ClientSettings settings)
        {
            return new ClientDriver(username, password, settings);
        }
        
        /// <summary>
        /// Clones this instance.
        /// Makes a shallow copy of all instance fields, and then calls InitializeOwnedFields()
        /// to reset all mutable or otherwise unshareable fields.
        /// </summary>
        /// <returns>A clone of this object with a copy of all tokens and settings,
        /// but with its own (uninitialized) Dei client</returns>
        public ClientDriver Clone()
        {
            ClientDriver clone = (ClientDriver)this.MemberwiseClone();
            clone.InitializeOwnedFields();
            return clone;
        }

        #region IIdentityProvider Members

        /// <inheritdoc/>
        public SymmetricKeyToken GetNetworkParticipationToken()
        {
            lock (this.sessionLock)
            {
                return this.participationToken;
            }
        }

        /// <inheritdoc/>
        public PermissionListToken GetPermissionListToken()
        {
            lock (this.sessionLock)
            {
                return this.permissionListToken;
            }
        }

        /// <inheritdoc/>
        public TrustedAuthorityToken GetTrustedAuthorityToken()
        {
            lock (this.sessionLock)
            {
                return this.trustedAuthorityKeyToken;
            }
        }

        /// <inheritdoc/>
        public CertificateToken GetUserCertificateToken()
        {
            lock (this.sessionLock)
            {
                return this.certificateToken;
            }
        }

        /// <inheritdoc/>
        public TimeToken GetTimeToken()
        {
            lock (this.sessionLock)
            {
                return this.timeToken;
            }
        }

        /// <inheritdoc/>
        public void Dispose()
        {
            if (this.Client != null)
            {
                Logger.TraceInformation(LogTag.Crypto | LogTag.Event | LogTag.Connection, "IdentityProvider: Closing client session");
                this.Client.Dispose();
                this.Client = null;
            }
        }

        #endregion

        /// <summary>
        /// Authorizes the user as specified character.
        /// </summary>
        /// <param name="character">The character.</param>
        /// <param name="authenticationResult">The authentication result.</param>
        /// <param name="progressCallback">The progress callback.</param>
        /// <returns>The LoginResult</returns>
        public LoginResult Authorize(Character character, LoginResult authenticationResult, LoginProgressNotification progressCallback)
        {
            return this.UpdateTokens(true, character, authenticationResult, progressCallback);
        }

        /// <summary>
        /// Creates a new character.
        /// </summary>
        /// <param name="characterName">Name of the character.</param>
        /// <returns>A value indicating success</returns>
        public Character CreateCharacter(string characterName)
        {
            return this.Client.CreateCharacter(characterName);
        }

        /// <summary>
        /// Deletes a character.
        /// </summary>
        /// <param name="character">The character.</param>
        /// <returns>A value indicating success</returns>
        public bool DeleteCharacter(Character character)
        {
            return this.Client.DeleteCharacter(character);
        }

        /// <summary>
        /// Lists the user's characters.
        /// </summary>
        /// <returns>The user's characters</returns>
        public IList<Character> ListCharacters()
        {
            return this.Client.ListCharacters();
        }

        /// <summary>
        /// Do a refresh, updating all tokens
        /// </summary>
        /// <param name="character">The identity in use.</param>
        /// <returns>
        /// Return the login result.
        /// </returns>
        public LoginResult Refresh(Character character)
        {
            LoginProgressNotification progressNotification = null;
            lock (this.sessionLock)
            {
                try
                {
                    using (this)
                    {
                        LoginResult loginResult = this.OpenSession(progressNotification);
                        if (!loginResult.WasSuccessful)
                        {
                            return loginResult;
                        }

                        loginResult = this.UpdateTokens(false, character, loginResult, progressNotification);

                        // the refresh time is supposed to be updated inside UpdateTokens().
                        Debug.Assert(this.refreshTime >= this.NetworkTime.Now, "assertion failed: this.refreshTime >= this.networkTime.Now");
                        return loginResult;
                    }
                }
                catch (Exception)
                {
                    this.ScheduleRefreshRetry();
                    throw;
                }
            }
        }

        /// <summary>
        /// Open new session with dei server.
        /// </summary>
        /// <param name="progressCallback">Progress callback.</param>
        /// <returns>Return the login result.</returns>
        public LoginResult OpenSession(LoginProgressNotification progressCallback)
        {
            this.Dispose();

            try
            {
                Logger.TraceInformation(LogTag.Crypto, "IdentityProvider: Initializing client");
                this.Client = this.InitializeClient();
            }
            catch (IOException e)
            {
                System.Console.WriteLine("Failed to connect to Dei server : {0}", e.Message);
                System.Console.WriteLine("Failed to connect to Dei server : {0}", e.StackTrace);
                if (e.InnerException != null)
                {
                    System.Console.WriteLine("Failed to connect to authentication server : {0}", e.InnerException.Message);
                    System.Console.WriteLine("Failed to connect to authentication server : {0}", e.InnerException.StackTrace);
                }

                return new LoginResult("Connection error");
            }
            catch (SocketException e)
            {
                System.Console.WriteLine("Failed to connect to authentication server : {0}", e.Message);
                System.Console.WriteLine("Failed to connect to authentication server : {0}", e.StackTrace);
                return new LoginResult("Unable to connect to authentication server");
            }
            catch (AuthenticationException e)
            {
                System.Console.WriteLine("Failed to connect to authentication server : {0}", e.Message);
                System.Console.WriteLine("Failed to connect to authentication server : {0}", e.StackTrace);
                return new LoginResult("Unable to create a secure connection");
            }
            catch (Exception e)
            {
                System.Console.WriteLine("Failed to connect to authentication server : {0}", e.Message);
                System.Console.WriteLine("Failed to connect to authentication server : {0}", e.StackTrace);
                return new LoginResult("Connection related error - unidentified type.");
            }

            this.ProgressFeedback(progressCallback, LoginStage.Initializing, 20, DescriptionResources.LoginAuthenticatingDescription);

            try
            {
                return this.Client.Login(this.userName, this.password);
            }
            catch (VersionException)
            {
                return new LoginResult("Authentication protocol is incompatible");
            }
            catch (ProtocolException)
            {
                return new LoginResult("Authentication protocol error");
            }
            catch
            {
                return new LoginResult("Unknown login error");
            }
        }

        /// <summary>
        /// Initializess the client.
        /// </summary>
        /// <returns>A new client</returns>
        protected virtual IClient InitializeClient()
        {
            TcpClient connection = new TcpClient(this.clientSettings.ServerHost, this.clientSettings.ServerPort);

            if (this.clientSettings.UseSslConnection)
            {
                if (this.IsRunningOnOldMono())
                {
                    Type sslClientStream = Type.GetType("Mono.Security.Protocol.Tls.SslClientStream, Mono.Security");
                    Type validationCallback = Type.GetType("Mono.Security.Protocol.Tls.CertificateValidationCallback, Mono.Security");

                    Stream secureConnection = (Stream)Activator.CreateInstance(sslClientStream, new object[] { connection.GetStream(), this.clientSettings.ServerHost, true });
                    Delegate validationDelegate = Delegate.CreateDelegate(validationCallback, this.clientSettings, typeof(ClientSettings).GetMethod("ValidateServerCertificate2"));

                    System.Reflection.PropertyInfo serverCertValidationDelegate = sslClientStream.GetProperty("ServerCertValidationDelegate");
                    serverCertValidationDelegate.SetValue((object)secureConnection, validationDelegate, null);

                    return new Client(secureConnection);
                }
                else
                {
                    SslStream secureConnection = new SslStream(connection.GetStream(), false, this.clientSettings.ValidateServerCertificate);
                    secureConnection.AuthenticateAsClient(this.clientSettings.ServerHost, null, SslProtocols.Tls, false);

                    return new Client(secureConnection);
                }
            }
            else
            {
                return new Client(connection.GetStream());
            }
        }

        /// <summary>
        /// Initializes mutable or otherwise unshareable instance fields.
        /// </summary>
        private void InitializeOwnedFields()
        {
            this.sessionLock = new object();
            this.Client = null;
        }

        /// <summary>
        /// Fetches all tokens. Assumes an open session.
        /// </summary>
        /// <param name="firstTime">A value indicating whether this is the first time tokens are being fetched.</param>
        /// <param name="character">The selected identity.</param>
        /// <param name="currentState">The current authentication state. This will be `null` when called from the background update task.</param>
        /// <param name="progressCallback">Login progress callback.</param>
        /// <returns>A LoginResult indicating error, or `currentState` on success.</returns>
        private LoginResult UpdateTokens(bool firstTime, Character character, LoginResult currentState, LoginProgressNotification progressCallback)
        {
            int percentageComplete = 40;
            int percentagePerToken = 10;

            if (!this.ProgressFeedback(
                progressCallback,
                LoginStage.ClockSynchronization,
                percentageComplete += percentagePerToken,
                DescriptionResources.LoginClockSynchronization))
            {
                return LoginResult.UserCancelled;
            }

            bool refreshFailed = false;

            if (!this.RefreshTimeToken(firstTime))
            {
                Logger.TraceWarning(LogTag.Crypto | LogTag.Event, "RefreshTimeToken failed");
                refreshFailed = true;
            }

            if (!this.ProgressFeedback(
                progressCallback,
                LoginStage.NetworkParticipationToken,
                percentageComplete += percentagePerToken,
                DescriptionResources.LoginParticipationDescription))
            {
                return LoginResult.UserCancelled;
            }

            if (!this.RefreshParticipationToken(firstTime))
            {
                Logger.TraceWarning(LogTag.Crypto | LogTag.Event, "RefreshParticipationToken failed");
                refreshFailed = true;
            }

            if (!this.ProgressFeedback(
                progressCallback,
                LoginStage.TrustedAuthorityToken,
                percentageComplete += percentagePerToken,
                DescriptionResources.LoginTrustedDescription))
            {
                return LoginResult.UserCancelled;
            }

            if (!this.RefreshTrustedAuthorityToken(firstTime))
            {
                Logger.TraceWarning(LogTag.Crypto | LogTag.Event, "RefreshTrustedAuthorityToken failed");
                refreshFailed = true;
            }

            if (!this.ProgressFeedback(
                progressCallback,
                LoginStage.PermissionListToken,
                percentageComplete += percentagePerToken,
                DescriptionResources.LoginPermissionsDescription))
            {
                return LoginResult.UserCancelled;
            }

            if (!this.RefreshPermissionListToken(firstTime))
            {
                Logger.TraceWarning(LogTag.Crypto | LogTag.Event, "RefreshPermissionListToken failed");
                refreshFailed = true;
            }

            if (!this.ProgressFeedback(
                progressCallback,
                LoginStage.UserCertificateToken,
                percentageComplete += percentagePerToken,
                DescriptionResources.LoginCertificateDescription))
            {
                return LoginResult.UserCancelled;
            }

            if (!this.RefreshCertificateToken(firstTime, character))
            {
                Logger.TraceWarning(LogTag.Crypto | LogTag.Event, "RefreshCertificateToken failed");
                refreshFailed = true;
            }

            if (!this.ProgressFeedback(
                progressCallback,
                LoginStage.Complete,
                100,
                DescriptionResources.LoginCompleteDescription))
            {
                return LoginResult.UserCancelled;
            }

            if (refreshFailed)
            {
                this.ScheduleRefreshRetry();
                return new LoginResult("Failed to acquire security tokens: " + this.Client.FailureReason);
            }
            else
            {
                this.secondChanceAvailable = true;
            }

            Logger.TraceInformation(LogTag.Event | LogTag.Crypto | LogTag.Connection, "Refresh finished");
            return currentState;
        }

        /// <summary>
        /// Send feedback to the given progressCallback, if defined.
        /// </summary>
        /// <param name="progressCallback">The progress callback.</param>
        /// <param name="stage">The stage.</param>
        /// <param name="percentage">The percentage.</param>
        /// <param name="description">The description.</param>
        /// <returns>Whether the progress should proceed</returns>
        private bool ProgressFeedback(LoginProgressNotification progressCallback, LoginStage stage, int percentage, string description)
        {
            if (progressCallback != null)
            {
                return progressCallback(stage, description, percentage);
            }

            return true;
        }

        /// <summary>
        /// Calculate the refresh time.
        /// </summary>
        /// <param name="expiryTime">Expiry time.</param>
        private void CalculateRefreshTime(UtcDateTime expiryTime)
        {
            TimeSpan lifeTime = expiryTime.DateTime - this.clientSettings.NetworkTime.Now;
            double randomFactor = ClientDriver.randomSource.NextDouble();

            // the refresh delay is between 0.5-0.9 of the certificate lifetime. 
            long halfLifeTimeTick = lifeTime.Ticks / 2;
            long upperBoundTick = lifeTime.Ticks * 2 / 5;
            long randomized = (long)(upperBoundTick * randomFactor);
            TimeSpan refreshDelay = new TimeSpan(halfLifeTimeTick + randomized);

            Debug.Assert(refreshDelay < lifeTime, "Assertion failed: refreshDelay < lifeTime");
            this.remainingLifetime = lifeTime - refreshDelay;

            this.refreshTime = this.clientSettings.NetworkTime.Now + refreshDelay;
        }

        /// <summary>
        /// Refresh participation token.
        /// </summary>
        /// <param name="firstTime">A value indicating whether this is the first request to refresh the
        /// participation token.</param>
        /// <returns>Whether the participation token is successfully refreshed.</returns>
        private bool RefreshParticipationToken(bool firstTime)
        {
            ParticipationKey participationKey = this.Client.GetParticipationKey(firstTime);

            if (participationKey != null)
            {
                this.participationToken = new SymmetricKeyToken(participationKey.ExpiryTime.DateTime, participationKey.Key);
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Refresh the permission list token.
        /// </summary>
        /// <param name="firstAttempt">A value indicating whether this is the first attempt.</param>
        /// <returns>Whether the permission list token has been successfully refreshed.</returns>
        private bool RefreshPermissionListToken(bool firstAttempt)
        {
            PermissionListToken permissionToken = new PermissionListToken(new PrivateKey(this.clientSettings.PrivateKey));

            foreach (Permission permission in this.Client.GetPermissions(permissionToken.UsersPublicKey))
            {
                permissionToken.AddPermission(permission.State, permission.Signiture);
            }

            this.permissionListToken = permissionToken;
            return true;
        }

        /// <summary>
        /// Refresh trusted authority token.
        /// </summary>
        /// <param name="firstTime">A value indicating whether this is the first time to refresh trusted authority token.</param>
        /// <returns>Whether the trust authority token has been successfully refreshed.</returns>
        private bool RefreshTrustedAuthorityToken(bool firstTime)
        {
            Dei.PublicKey serversPublicKey = this.Client.GetServersPublicKey(firstTime);

            if (serversPublicKey != null)
            {
                this.trustedAuthorityKeyToken = new TrustedAuthorityToken(serversPublicKey.ExpiryTime.DateTime, serversPublicKey.Key);
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Refresh certificate token.
        /// </summary>
        /// <param name="firstTime">A value indicating whether this is the first time to refresh certificate token.</param>
        /// <param name="character">The selected identity.</param>
        /// <returns>Whether the certificate token has been refreshed.</returns>
        private bool RefreshCertificateToken(bool firstTime, Character character)
        {
            UserCertificate certificate = this.Client.GetCertificate(character, this.clientSettings.PrivateKey.ExportCspBlob(false));
            if (certificate != null)
            {
                this.certificateToken = new CertificateToken(
                    certificate.UserId,
                    certificate.Character,
                    certificate.TimeStamp.DateTime,
                    certificate.ExpiryTime.DateTime,
                    certificate.Signature,
                    this.clientSettings.PrivateKey);

                this.CalculateRefreshTime(certificate.ExpiryTime);

                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Refresh time token.
        /// </summary>
        /// <param name="firstTime">A value indicating whether this is the first time to refresh time token.</param>
        /// <returns>Whether the time token has been refreshed.</returns>
        private bool RefreshTimeToken(bool firstTime)
        {
            ServerTime serverTime = this.Client.GetServerTime();
            if (serverTime != null)
            {
                // TODO: Shouldn't Synchronise be called every time we get the server time?  Though, what effect would this have to stored times like this.refreshTime based on the previous sync?
                if (!this.clientSettings.NetworkTime.IsSynchronized)
                {
                    // network clock has never been synchronized, need to synchronize it before the time token 
                    // can be constructed.
                    this.clientSettings.NetworkTime.Synchronize(serverTime.Time.DateTime);
                }

                this.timeToken = new TimeToken(serverTime.Time.DateTime, serverTime.ExpiryTime.DateTime);
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Schedules to retry the token reresh.
        /// </summary>
        private void ScheduleRefreshRetry()
        {
            if (this.secondChanceAvailable)
            {
                double delayInMs = this.remainingLifetime.TotalMilliseconds / 2;
                this.refreshTime = this.clientSettings.NetworkTime.Now + TimeSpan.FromMilliseconds(delayInMs);
                this.secondChanceAvailable = false;
            }
            else
            {
                Logger.TraceInformation(LogTag.Dei, "IdentityProvider: no secondChanceAvailable");
            }
        }

        /// <summary>
        /// Return a value indicating whether the process is run under old mono version.
        /// </summary>
        /// <returns>True if this process run under mono 1.2.* version.</returns>
        private bool IsRunningOnOldMono()
        {
            try
            {
                Type monoRuntimeType;
                MethodInfo getDisplayNameMethod;
                BindingFlags flags = BindingFlags.NonPublic | BindingFlags.Static | BindingFlags.DeclaredOnly | BindingFlags.ExactBinding;
                if ((monoRuntimeType = typeof(object).Assembly.GetType("Mono.Runtime")) != null &&
                    (getDisplayNameMethod = monoRuntimeType.GetMethod("GetDisplayName", flags, null, Type.EmptyTypes, null)) != null)
                {
                    string displayName = (string)getDisplayNameMethod.Invoke(null, null);

                    System.Diagnostics.Trace.TraceInformation("Mono display name:" + displayName);

                    if (!displayName.Contains("1.2."))
                    {
                        //// not 1.2.* mono
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
                else
                {
                    // not mono
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
