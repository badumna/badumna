//-----------------------------------------------------------------------
// <copyright file="RsaAlgorithm.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Dei.Utilities
{
    using System;
    using System.Collections;
    using System.IO;
    using System.Net.Sockets;
    using System.Security.Cryptography;
    using System.Text;
    using System.Xml;

    /// <summary>
    /// RsaAlgorithm class compute the rsa key.
    /// </summary>
    internal class RsaAlgorithm : IDisposable
    {
        /// <summary>
        /// Rsa key length.
        /// </summary>
        public const int KeyLength = 1024;

        /// <summary>
        /// Rsa crypto service provider.
        /// </summary>
        private RSACryptoServiceProvider rsa;

        /// <summary>
        /// The private key path.
        /// </summary>
        private string privateKeyPath = @"privatekey.xml";

        /// <summary>
        /// The public key path.
        /// </summary>
        private string publicKeyPath = @"publickey.xml";

        /// <summary>
        /// A value indicating whether the object has been disposed.
        /// </summary>
        private bool disposed = false;

        /// <summary>
        /// Initializes a new instance of the <see cref="RsaAlgorithm"/> class.
        /// </summary>
        public RsaAlgorithm()
        {
            this.rsa = new RSACryptoServiceProvider(RsaAlgorithm.KeyLength);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RsaAlgorithm"/> class.
        /// </summary>
        /// <param name="publicKeyPath">Public key path.</param>
        /// <param name="privateKeyPath">Private key path.</param>
        public RsaAlgorithm(string publicKeyPath, string privateKeyPath)
        {
            if (publicKeyPath == null)
            {
                throw new ArgumentNullException("publicKeyPath");
            }

            if (privateKeyPath == null)
            {
                throw new ArgumentNullException("privateKeyPath");
            }

            this.publicKeyPath = publicKeyPath;
            this.privateKeyPath = privateKeyPath;
            this.rsa = new RSACryptoServiceProvider(RsaAlgorithm.KeyLength);
        }

        /// <summary>
        /// Finalizes an instance of the <see cref="RsaAlgorithm"/> class.
        /// </summary>
        ~RsaAlgorithm()
        {
            this.CleanUp(false);
        }

        /// <summary>
        /// Gets the private key path.
        /// </summary>
        public string PrivateKeyPath
        {
            get { return this.privateKeyPath; }
        }

        /// <summary>
        /// Gets the public key path.
        /// </summary>
        public string PublicKeyPath
        {
            get { return this.publicKeyPath; }
        }

        /// <summary>
        /// Gets a value indicating whether a value indicates that rsa object contains only
        /// the public key.
        /// </summary>
        public bool PublicOnly
        {
            get { return this.rsa.PublicOnly; }
        }

        /// <summary>
        /// Encrypt the given data.
        /// </summary>
        /// <param name="data">The data need to be encrypted.</param>
        /// <returns>Return the encripted data.</returns>
        public byte[] Encrypt(byte[] data)
        {
            return this.rsa.Encrypt(data, false);
        }

        /// <summary>
        /// Decrypt the given data.
        /// </summary>
        /// <param name="data">The data need to be decrypted.</param>
        /// <returns>Return the decrypted data.</returns>
        public byte[] Decrypt(byte[] data)
        {
            return this.rsa.Decrypt(data, false);
        }

        /// <summary>
        /// Gets the rsa public key.
        /// </summary>
        /// <returns>Return the public key.</returns>
        public RSA GetPublicKey()
        {
            using (StreamReader reader = new StreamReader(this.publicKeyPath))
            {
                RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();
                rsa.FromXmlString(reader.ReadToEnd());
                return rsa;
            }
        }

        /// <summary>
        /// Gets the rsa private key.
        /// </summary>
        /// <returns>Return the private key.</returns>
        public RSA GetPrivateKey()
        {
            using (StreamReader reader = new StreamReader(this.privateKeyPath))
            {
                RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();
                rsa.FromXmlString(reader.ReadToEnd());
                return rsa;
            }
        }

        /// <summary>
        /// Create a new key pair.
        /// </summary>
        public void CreateNewKeys()
        {
            if (!File.Exists(this.privateKeyPath))
            {
                using (StreamWriter writer = new StreamWriter(this.privateKeyPath))
                {
                    writer.Write(this.rsa.ToXmlString(true));
                }
            }

            if (!File.Exists(this.publicKeyPath))
            {
                using (StreamWriter writer = new StreamWriter(this.publicKeyPath))
                {
                    writer.Write(this.rsa.ToXmlString(false));
                }
            }
        }

        /// <summary>
        /// Deleted the old key.
        /// </summary>
        public void DeleteOldKeys()
        {
            File.Delete(this.publicKeyPath);
            File.Delete(this.privateKeyPath);
        }

        /// <summary>
        /// Clean up the rsa object.
        /// </summary>
        /// <param name="disposing">Disposing is not used on the current implementation.</param>
        public void CleanUp(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    // managed
                }
                
                if (this.rsa != null)
                {
                    this.rsa.Clear();
                }

                this.rsa = null;
            }

            this.disposed = true;
        }

        /// <summary>
        /// Dispose the rsa algorithm class.
        /// </summary>
        public void Dispose()
        {
            this.CleanUp(true);
            GC.SuppressFinalize(this);
        }
    }
}
