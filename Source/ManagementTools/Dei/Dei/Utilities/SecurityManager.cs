//-----------------------------------------------------------------------
// <copyright file="SecurityManager.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace Dei.Utilities
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Security.Cryptography;
    using System.Text;

    /// <summary>
    /// SecurityManager class.
    /// </summary>
    internal class SecurityManager
    {
        /// <summary>
        /// Private key file name.
        /// </summary>
        private static string privateKeyName = "clientprvkey.xml";
        
        /// <summary>
        /// Public key file name.
        /// </summary>
        private static string publicKeyName = "clientpubkey.xml";
               
        /// <summary>
        /// Get the client private key.
        /// </summary>
        /// <returns>Return the client private key.</returns>
        public RSA GetClientPrivateKey() 
        {
            return this.GetPrivateKey(SecurityManager.privateKeyName);
        }

        /// <summary>
        /// Get the client public key.
        /// </summary>
        /// <returns>Return the client public key.</returns>
        public RSA GetClientPublicKey()
        {
            return this.GetPublicKey(SecurityManager.publicKeyName);
        }

        /// <summary>
        /// Generate client keys (public and private keys).
        /// </summary>
        /// <returns>Rsa algorithm instance.</returns>
        internal static RsaAlgorithm GenerateClientKeys()
        {
            RsaAlgorithm key = new RsaAlgorithm(SecurityManager.publicKeyName, SecurityManager.privateKeyName);
            key.CreateNewKeys();
            return key;
        }

        /// <summary>
        /// Generate client keys (public and private key). 
        /// </summary>
        /// <param name="publicKeyName">Public key file name.</param>
        /// <param name="privateKeyName">Private key file name.</param>
        /// <returns>RSA client keys.</returns>
        protected RSA GenerateKeys(string publicKeyName, string privateKeyName)
        {
            SecurityManager.publicKeyName = publicKeyName;
            SecurityManager.privateKeyName = privateKeyName;
            RsaAlgorithm key = new RsaAlgorithm(publicKeyName, privateKeyName);
            key.CreateNewKeys();
            return key.GetPrivateKey();
        }

        /// <summary>
        /// Get public key from given public key file name.
        /// </summary>
        /// <param name="publicKeyName">Public key file name.</param>
        /// <returns>Return the client public key.</returns>
        private RSA GetPublicKey(string publicKeyName)
        {
            using (StreamReader reader = new StreamReader(publicKeyName))
            {
                RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();
                rsa.FromXmlString(reader.ReadToEnd());
                return rsa;
            }
        }

        /// <summary>
        /// Get private key from gicen private key file name.
        /// </summary>
        /// <param name="privateKeyName">Private key file name.</param>
        /// <returns>Return the client private key.</returns>
        private RSA GetPrivateKey(string privateKeyName)
        {
            using (StreamReader reader = new StreamReader(privateKeyName))
            {
                RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();
                rsa.FromXmlString(reader.ReadToEnd());
                return rsa;
            }
        }       
    }
}
