//-----------------------------------------------------------------------
// <copyright file="Client.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace Dei
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Net;
    using System.Net.Sockets;
    using System.Security.Cryptography;
    using System.Text;
    using System.Threading;
    using System.Xml;
    using System.Xml.Serialization;
    using Badumna.Security;
    using Dei.Protocol;
    using Dei.Utilities;
    using Badumna;
    using Badumna.Utilities;
    using Badumna.Utilities.Logging;
    using System.Diagnostics;

    /// <summary>
    /// Client protocol factory delegate.
    /// </summary>
    /// <param name="stream">Client stream</param>
    /// <returns>Return Client protocol.</returns>
    internal delegate IClientProtocol ClientProtocolFactoryDelegate(Stream stream);

    /// <summary>
    /// Client class has responsibility to talk to the server via client protocol class by passing the
    /// client stream.
    /// </summary>
    public class Client : IClient
    {
        /// <summary>
        /// Client stream.
        /// </summary>
        private Stream stream;

        /// <summary>
        /// Client protocol.
        /// </summary>
        private IClientProtocol clientProtocol;

        /// <summary>
        /// User properties
        /// </summary>
        private UserProperties userProperties;

        /// <summary>
        /// Client protocol factory delegate.
        /// </summary>
        private ClientProtocolFactoryDelegate clientProtocolFactoryDelegate;

        /// <summary>
        /// Initializes a new instance of the <see cref="Client"/> class.
        /// </summary>
        /// <param name="stream">Client stream.</param>
        public Client(Stream stream)
            : this(
                stream, 
                delegate(Stream clientStream)
                {
                    return new ClientProtocol(clientStream);
                })
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Client"/> class.
        /// </summary>
        /// <param name="stream">Client stream.</param>
        /// <param name="clientFactoryDelegate">Client factory delegate.</param>
        internal Client(Stream stream, ClientProtocolFactoryDelegate clientFactoryDelegate)
        {
            this.stream = stream;
            this.clientProtocolFactoryDelegate = clientFactoryDelegate;
        }

        /// <summary>
        /// Gets a value indicating whether the client connection is closed.
        /// </summary>
        public bool IsClosed
        {
            get { return ((ClientProtocol)this.clientProtocol).IsClosed; }
        }

        /// <summary>
        /// Gets the Dei failure reason.
        /// </summary>
        public FailureReasons FailureReason
        {
            get
            {
                return this.clientProtocol == null ? FailureReasons.NoneGiven : this.clientProtocol.FailureReason;
            }
        }

        /// <summary>
        /// Login to the Dei server.
        /// </summary>
        /// <param name="userName">User name.</param>
        /// <param name="password">User password.</param>
        /// <returns>Return true on success.</returns>
        public LoginResult Login(string userName, string password)
        {
            if (this.stream == null || !this.stream.CanWrite)
            {
                System.Diagnostics.Trace.TraceError("Cannot login because the connection is not valid.");
                return new LoginResult("Invalid connection");
            }

            try
            {
                this.clientProtocol = this.clientProtocolFactoryDelegate(this.stream);
            }
            catch (Exception e)
            {
                Logger.TraceException(LogLevel.Error, LogTag.Event | LogTag.Crypto | LogTag.Connection, e, "Exception in Login()");
                return new LoginResult("An unknown error occurred");
            }

            this.userProperties = new UserProperties(userName, password);
            LoginResult result = this.clientProtocol.Login(this.userProperties);
            if (!result.WasSuccessful)
            {
                this.userProperties = null;
            }

            return result;
        }

        /// <summary>
        /// Logout from the Dei server.
        /// </summary>
        public void Logout()
        {
            if (this.userProperties == null)
            {
                return;
            }

            this.clientProtocol.Logout(this.userProperties);
        }

        /// <inheritdoc/>
        public void Dispose()
        {
            try
            {
                this.Logout();
            }
            catch (ProtocolException e)
            {
                // Happens if the connection is corrupt / unexpectedly closed, we'll assume the connection is dead and move on
                Logger.TraceException(LogLevel.Error, LogTag.Event | LogTag.Connection, e, "Exception in Dispose()");
            }
        }

        /// <summary>
        /// Gets the server time.
        /// </summary>
        /// <returns>Server time.</returns>
        public ServerTime GetServerTime()
        {
            this.EnsureLoggedIn();
            return this.clientProtocol.RequestServerTime(this.userProperties);
        }

        /// <summary>
        /// Gets the participation key.
        /// </summary>
        /// <param name="isFirstRequest">A value indicating whether this is a first request.</param>
        /// <returns>Return participation key.</returns>
        public ParticipationKey GetParticipationKey(bool isFirstRequest)
        {
            this.EnsureLoggedIn();
            return this.clientProtocol.RequestParticipationKey(this.userProperties, isFirstRequest ? 0 : 1);
        }

        /// <summary>
        /// Gets the server public key
        /// </summary>
        /// <param name="isFirstRequest">A value indicating whether this is a first request.</param>
        /// <returns>Return server public key.</returns>
        public PublicKey GetServersPublicKey(bool isFirstRequest)
        {
            this.EnsureLoggedIn();
            return this.clientProtocol.RequestServersKey(this.userProperties, isFirstRequest ? 0 : 1);
        }

        /// <summary>
        /// Get the certificate based on the serialized public key.
        /// </summary>
        /// <param name="character">The Character to identify as.</param>
        /// <param name="serializedPublicKey">Serialized public key.</param>
        /// <returns>Return a user certificate.</returns>
        public UserCertificate GetCertificate(Character character, byte[] serializedPublicKey)
        {
            if (!(character == null || character.IsValid))
            {
                throw new ArgumentException("invalid character");
            }
            this.EnsureLoggedIn();
            return this.clientProtocol.RequestCertificate(this.userProperties, character, serializedPublicKey);
        }

        /// <summary>
        /// Gets the permissions given the user public key.
        /// </summary>
        /// <param name="usersPublicKey">User public key.</param>
        /// <returns>List of permission.</returns>
        public IList<Permission> GetPermissions(byte[] usersPublicKey)
        {
            this.EnsureLoggedIn();
            return this.clientProtocol.RequestPermissionList(this.userProperties, usersPublicKey);
        }

        /// <summary>
        /// Get the permissions types for given username.
        /// </summary>
        /// <param name="username">User name.</param>
        /// <returns>Permission types that user has.</returns>
        public IList<PermissionProperties> GetPermissionTypes(string username)
        {
            this.EnsureLoggedIn();
            return this.clientProtocol.RequestPermissionTypes(username);
        }

        /// <summary>
        /// Modified the account password.
        /// </summary>
        /// <param name="userName">User name.</param>
        /// <param name="newPassword">New password.</param>
        /// <returns>Return true on success.</returns>
        public bool ChangeAccount(string userName, string newPassword)
        {
            this.EnsureLoggedIn();
            this.userProperties = new UserProperties(userName, newPassword);
            return this.clientProtocol.ChangeAccount(this.userProperties);
        }

        /// <summary>
        /// Create a new user account.
        /// </summary>
        /// <param name="userName">User name.</param>
        /// <param name="password">User password.</param>
        /// <returns>Return true on success.</returns>
        public bool CreateNewAccount(string userName, string password)
        {
            this.EnsureLoggedIn();
            UserProperties newUsersProperties = new UserProperties(userName, password);
            if (this.clientProtocol.CreateNewAccount(newUsersProperties))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Delete an existing account.
        /// </summary>
        /// <param name="username">User name.</param>
        /// <returns>Return true on success.</returns>
        public bool DeleteAccount(string username)
        {
            this.EnsureLoggedIn();
            return this.clientProtocol.DeleteAccount(new UserProperties(username, string.Empty));
        }

        /// <inheritdoc/>
        public Character CreateCharacter(string characterName)
        {
            this.EnsureLoggedIn();
            return this.clientProtocol.CreateCharacter(this.userProperties, characterName);
        }

        /// <inheritdoc/>
        public bool DeleteCharacter(Character character)
        {
            this.EnsureLoggedIn();
            return this.clientProtocol.DeleteCharacter(this.userProperties, character);
        }

        /// <inheritdoc/>
        public IList<Character> ListCharacters()
        {
            this.EnsureLoggedIn();
            return this.clientProtocol.RequestCharacterList(this.userProperties);
        }

        /// <summary>
        /// Create a new permission.
        /// </summary>
        /// <param name="permissionName">Permission name.</param>
        /// <returns>Permission id.</returns>
        public long CreatePermission(string permissionName)
        {
            this.EnsureLoggedIn();
            return this.clientProtocol.CreatePermission(permissionName);
        }

        /// <summary>
        /// Remove an existing permission.
        /// </summary>
        /// <param name="permisionType">Permission type.</param>
        /// <returns>Return true on success.</returns>
        public bool RemovePermission(long permisionType)
        {
            this.EnsureLoggedIn();
            return this.clientProtocol.RemovePermission(permisionType);
        }

        /// <summary>
        /// Grant a particular permission to a specific user.
        /// </summary>
        /// <param name="username">User name.</param>
        /// <param name="permissionId">Permission id.</param>
        /// <returns>Return true on success.</returns>
        public bool GrantPermission(string username, long permissionId)
        {
            this.EnsureLoggedIn();
            return this.clientProtocol.GrantPermission(new UserProperties(username, string.Empty), permissionId);
        }

        /// <summary>
        /// Revoke a particular permission from a specific user.
        /// </summary>
        /// <param name="username">User name.</param>
        /// <param name="permisionId">Permission id.</param>
        /// <returns>Return true on success.</returns>
        public bool RevokePermission(string username, long permisionId)
        {
            this.EnsureLoggedIn();
            return this.clientProtocol.RevokePermission(new UserProperties(username, string.Empty), permisionId);
        }

        /// <summary>
        /// List users which match with the given username glob.
        /// </summary>
        /// <param name="usernameGlob">User name glob.</param>
        /// <param name="startRange">Start range.</param>
        /// <param name="maximumNumberOfResults">Maximum number of result</param>
        /// <returns>Return list of user.</returns>
        public IList<string> ListUsers(string usernameGlob, long startRange, long maximumNumberOfResults)
        {
            this.EnsureLoggedIn();
            if (string.IsNullOrEmpty(usernameGlob))
            {
                throw new ArgumentException("Username glob cannot be null or empty");
            }

            if (maximumNumberOfResults < 1)
            {
                throw new ArgumentException("Doesn't make sense to have 0 as the number of desired results");
            }

            return this.clientProtocol.RequestUserList(usernameGlob, startRange, maximumNumberOfResults);
        }

        /// <summary>
        /// Count the number of user.
        /// </summary>
        /// <param name="usernameGlob">User name glob.</param>
        /// <returns>Return number of current user.</returns>
        public long GetUserCount(string usernameGlob)
        {
            this.EnsureLoggedIn();
            if (string.IsNullOrEmpty(usernameGlob))
            {
                throw new ArgumentException("Username glob cannot be null or empty");
            }

            return this.clientProtocol.RequestUserCount(usernameGlob);
        }

        /// <summary>
        /// Throws InvalidOperationException if the user is not logged in.
        /// </summary>
        private void EnsureLoggedIn()
        {
            if (this.userProperties == null)
            {
                throw new InvalidOperationException("Cannot perform operation until Login() has been called successfully.");
            }
        }
    }
}
