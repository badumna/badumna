//-----------------------------------------------------------------------
// <copyright file="ParticipationKeyGenerator.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace DeiServer
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Text;
    using Dei;

    /// <summary>
    /// ParticipationKeyGenerator class has responsibility to generate the participation key
    /// and renew the participation key in case they are expired.
    /// </summary>
    internal class ParticipationKeyGenerator : SavableGenerator<ParticipationKey>
    {
        /// <summary>
        /// ParticipationKeys.xml filename.
        /// </summary>
        private const string FileName = "ParticipationKeys.xml";

        /// <summary>
        /// The default validity period used for the Participation Key. 
        /// </summary>
        private static readonly TimeSpan MaxValidityPeriod = TimeSpan.MaxValue;

        /// <summary>
        /// Initializes a new instance of the <see cref="ParticipationKeyGenerator"/> class.
        /// </summary>
        public ParticipationKeyGenerator()
            : base(MaxValidityPeriod)
        {
            this.Load();
        }

        /// <summary>
        /// Clear the presisted keys, by deleting the existing ParticipationKeys.xml file.
        /// </summary>
        public static void ClearPersistedKeys()
        {
            try
            {
                if (File.Exists(ParticipationKeyGenerator.FileName))
                {
                    File.Delete(ParticipationKeyGenerator.FileName);
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Trace.TraceError("Exception while trying to clear persisted keys : {0}", e.Message);
            }
        }

        /// <summary>
        /// Generate a new participation key based on the validity period.
        /// </summary>
        /// <param name="validityPeriod">Validity period for a participation key.</param>
        /// <returns>Returns a new participation key.</returns>
        protected override ParticipationKey Generate(TimeSpan validityPeriod)
        {
            System.Console.WriteLine("Generating participationKey with validity period {0}", validityPeriod);
            byte[] key = new byte[ParticipationKey.KeyLengthBytes];
            ParticipationKeyGenerator.RandomSource.NextBytes(key);

            return new ParticipationKey(validityPeriod, key);
        }

        /// <summary>
        /// Save the participation keys to file.
        /// </summary>
        protected override void Save()
        {
            try
            {
                using (FileStream stream = new FileStream(ParticipationKeyGenerator.FileName, FileMode.Create))
                {
                    base.Save(stream);
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Trace.TraceError("Exception while trying to save participation key : {0}", e.Message);
            }
        }

        /// <summary>
        /// Load the exisiting participation keys from file.
        /// </summary>
        private void Load()
        {
            try
            {
                if (File.Exists(ParticipationKeyGenerator.FileName))
                {
                    using (FileStream stream = new FileStream(ParticipationKeyGenerator.FileName, FileMode.Open))
                    {
                        base.Load(stream);
                    }
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Trace.TraceError("Exception while trying to load participation key : {0}", e.Message);
            }
        }
    }
}
