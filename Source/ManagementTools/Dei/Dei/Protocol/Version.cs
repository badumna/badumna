//-----------------------------------------------------------------------
// <copyright file="Version.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace Dei.Protocol
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// Version number for the Dei protocol.
    /// </summary>
    [Serializable]
    public class Version
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Version"/> class.
        /// </summary>
        public Version()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Version"/> class.
        /// </summary>
        /// <param name="major">Major version number.</param>
        /// <param name="minor">Minor version number.</param>
        public Version(int major, int minor)
        {
            this.Major = major;
            this.Minor = minor;
        }

        /// <summary>
        /// Gets and sets major version number.
        /// </summary>
        public int Major { get; set; }

        /// <summary>
        /// Gets and sets minor version number.
        /// </summary>
        public int Minor { get; set; }
    }
}
