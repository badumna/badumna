//-----------------------------------------------------------------------
// <copyright file="PermissionProperties.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace Dei
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// Built in permission types enumeration.
    /// </summary>
    public enum PermissionTypes
    {
        /// <summary>
        /// None (default permission)
        /// </summary>
        None = 0,

        /// <summary>
        /// Admin permission.
        /// </summary>
        Admin = 1,

        /// <summary>
        /// Create user permission.
        /// </summary>
        CreateUsers = 2,

        /// <summary>
        /// View user permission.
        /// </summary>
        ViewUsers = 3,

        /// <summary>
        /// Permission for Create permission. 
        /// </summary>
        CreatePermissions = 4,

        /// <summary>
        /// Permission for assign permission.
        /// </summary>
        AssignPermissions = 5,

        /// <summary>
        /// Permission for viewing list of permissions.
        /// </summary>
        ViewPermissions = 6,

        /// <summary>
        /// Participation permission.
        /// </summary>
        Participation = 7,

        /// <summary>
        /// Announce permission.
        /// </summary>
        Announce = 8,

        /// <summary>
        /// End built in permission.
        /// </summary>
        EndBuiltinMarker_DoNotUseAsPermission = 9,
    }

    /// <summary>
    /// PermissionProperties class store the information about a permission
    /// (i.e. permission id and permission name).
    /// </summary>
    [Serializable]
    public class PermissionProperties
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PermissionProperties"/> class.
        /// </summary>
        public PermissionProperties() 
        { 
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PermissionProperties"/> class.
        /// </summary>
        /// <param name="type">Permission type.</param>
        public PermissionProperties(PermissionTypes type)
        {
            this.Id = (long)type;
            this.Name = type.ToString();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PermissionProperties"/> class.
        /// </summary>
        /// <param name="name">Permission name.</param>
        public PermissionProperties(string name)
        {
            this.Name = name;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PermissionProperties"/> class.
        /// </summary>
        /// <param name="id">Permission id.</param>
        /// <param name="name">Permission name.</param>
        public PermissionProperties(long id, string name)
        {
            this.Id = id;
            this.Name = name;
        }

        /// <summary>
        /// Gets or sets permission id.
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Gets or sets permission name.
        /// </summary>
        public string Name { get; set; }
    }
}
