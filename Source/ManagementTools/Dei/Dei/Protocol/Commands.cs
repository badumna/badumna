//-----------------------------------------------------------------------
// <copyright file="Commands.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Dei
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// Dei commands enumeration.
    /// </summary>
    [Serializable]
    public enum Commands
    {
        /// <summary>
        /// None must be first so that default(Commands) will return it.
        /// </summary>
        None,

        /// <summary>
        /// Request login.
        /// </summary>
        RequestLogin,

        /// <summary>
        /// Request logout.
        /// </summary>
        RequestLogout,

        /// <summary>
        /// Request new account creation.
        /// </summary>
        RequestNewAccount,

        /// <summary>
        /// Request account deletion.
        /// </summary>
        RequestDeleteAcount,

        /// <summary>
        /// Request account modification (i.e. change password).
        /// </summary>
        RequestChangeAccount,

        /// <summary>
        /// Request login is accepted.
        /// </summary>
        RequestLoginAccepted,

        /// <summary>
        /// Request login is rejected.
        /// </summary>
        RequestLoginRejected,

        /// <summary>
        /// Request logout is accepted.
        /// </summary>
        RequestLogoutAccepted,

        /// <summary>
        /// Request logout failed.
        /// </summary>
        RequestLogoutFailed,

        /// <summary>
        /// Request new account creation is accepted.
        /// </summary>
        RequestNewAccountAccepted,

        /// <summary>
        /// Request new account creation failed.
        /// </summary>
        RequestNewAccountFailed,

        /// <summary>
        /// Request account deletion accepted.
        /// </summary>
        RequestDeleteAccountAccepted,

        /// <summary>
        /// Request account deletion failed.
        /// </summary>
        RequestDeleteAccountFailed,

        /// <summary>
        /// Request account modification is accepted.
        /// </summary>
        RequestChangeAccountAccepted,

        /// <summary>
        /// Request account modification rejected.
        /// </summary>
        RequestChangeAccountRejected,

        /// <summary>
        /// Request client certificate.
        /// </summary>
        RequestClientCertificate,

        /// <summary>
        /// Request client certificate is accepted.
        /// </summary>
        RequestClientCertificateAccepted,

        /// <summary>
        /// Request client certificate is rejected.
        /// </summary>
        RequestClientCertificateFailed,

        /// <summary>
        /// Request permission list.
        /// </summary>
        /// Signed permissions (to be used by badumna)
        RequestPermissionList,

        /// <summary>
        /// Request permission list failed.
        /// </summary>
        RequestPermissionListFailed,

        /// <summary>
        /// Request permission list complete.
        /// </summary>
        RequestPermissionListComplete,

        /// <summary>
        /// Add a new permission.
        /// </summary>
        AddPermission,

        /// <summary>
        /// Request a participation key.
        /// </summary>
        RequestParticipationKey,

        /// <summary>
        /// Request a participation key is accepted.
        /// </summary>
        RequestParticipationKeyAccepted,

        /// <summary>
        /// Request a participation key is rejected.
        /// </summary>
        RequestParticipationKeyRejected,

        /// <summary>
        /// Request server public key.
        /// </summary>
        RequestServersPublicKey,

        /// <summary>
        /// Request server public key is accepted.
        /// </summary>
        RequestServersPublicKeyAccepted,

        /// <summary>
        /// Request server public key is rejected.
        /// </summary>
        RequestServersPublicKeyRejected,

        /// <summary>
        /// Request server time.
        /// </summary>
        RequestServerTime,

        /// <summary>
        /// Request server time is accepted.
        /// </summary>
        RequestServerTimeAccepted,

        /// <summary>
        /// Request server time is rejected.
        /// </summary>
        RequestServerTimeRejected,

        /// <summary>
        /// Request permission creation
        /// </summary>
        RequestCreatePermission,

        /// <summary>
        /// Request permission creation is accepted.
        /// </summary>
        RequestCreatePermissionAccepted,

        /// <summary>
        /// Request permission creation is rejected.
        /// </summary>
        RequestCreatePermissionRejected,

        /// <summary>
        /// Request permission deletion.
        /// </summary>
        RequestRemovePermission,

        /// <summary>
        /// Request permission deletion is accepted.
        /// </summary>
        RequestRemovePermissionAccepted,

        /// <summary>
        /// Request permission deletion is rejected.
        /// </summary>
        RequestRemovePermissionRejected,

        /// <summary>
        /// Request grant permission.
        /// </summary>
        RequestGrantPermission,

        /// <summary>
        /// Request grant permission is accepted.
        /// </summary>
        RequestGrantPermissionAccepted,

        /// <summary>
        /// Request grant permission is rejected.
        /// </summary>
        RequestGrantPermissionRejected,

        /// <summary>
        /// Request revoke permission.
        /// </summary>
        RequestRevokePermission,

        /// <summary>
        /// Request revoke permission is accepted.
        /// </summary>
        RequestRevokePermissionAccepted,

        /// <summary>
        /// Request revoke permission is rejected.
        /// </summary>
        RequestRevokePermissionRejected,

        /// <summary>
        /// Request list of permission types.
        /// </summary>
        /// Unsigned permission (just the name and id)
        RequestListPermissionTypes,

        /// <summary>
        /// Request list of permission types failed.
        /// </summary>
        RequestListPermissionTypesFailed,

        /// <summary>
        /// Request list of permission type complete.
        /// </summary>
        RequestListPermissionTypesComplete,

        /// <summary>
        /// Add permission type.
        /// </summary>
        AddPermissionType,

        /// <summary>
        /// Request user list.
        /// </summary>
        RequestUserList,

        /// <summary>
        /// Request user list failed.
        /// </summary>
        RequestUserListFailed,

        /// <summary>
        /// Request user list complete.
        /// </summary>
        RequestUserListComplete,

        /// <summary>
        /// Request user count.
        /// </summary>
        RequestUserCount,

        /// <summary>
        /// Request user count failed.
        /// </summary>
        RequestUserCountFailed,

        /// <summary>
        /// Request user count complete.
        /// </summary>
        RequestUserCountComplete,

        /// <summary>
        ///  Request Create Character
        /// </summary>
        RequestCreateCharacter,

        /// <summary>
        ///  Request Create Character Complete
        /// </summary>
        RequestCreateCharacterComplete,

        /// <summary>
        ///  Request Create Character Failed
        /// </summary>
        RequestCreateCharacterFailed,

        /// <summary>
        ///  Request Delete Character
        /// </summary>
        RequestDeleteCharacter,

        /// <summary>
        ///  Request Delete Character Complete
        /// </summary>
        RequestDeleteCharacterComplete,

        /// <summary>
        ///  Request Delete Character Failed
        /// </summary>
        RequestDeleteCharacterFailed,

        /// <summary>
        ///  Request Character List
        /// </summary>
        RequestCharacterList,

        /// <summary>
        ///  Request Character List Complete
        /// </summary>
        RequestCharacterListComplete,

        /// <summary>
        ///  Request Character List Failed
        /// </summary>
        RequestCharacterListFailed,
    }

    /// <summary>
    /// Dei failure reasons enumeration. 
    /// </summary>
    [Serializable]
    public enum FailureReasons
    {
        /// <summary>
        /// Fialure reason not given.
        /// </summary>
        NoneGiven,

        /// <summary>
        /// Too many character in login.
        /// </summary>
        TooManyCharactersInLogin,

        /// <summary>
        /// Authentication failed.
        /// </summary>
        AuthenticationFailed,

        /// <summary>
        /// Authorization failed.
        /// </summary>
        AuthorizationFailed,

        /// <summary>
        /// Account is already exist.
        /// </summary>
        AccountAlreadyExists,

        /// <summary>
        /// Permission is denied.
        /// </summary>
        PermissionDenied,

        /// <summary>
        /// Invalid request.
        /// </summary>
        InvalidRequest,

        /// <summary>
        /// Connection error.
        /// </summary>
        ConnectionError,
    }
}
