//-----------------------------------------------------------------------
// <copyright file="IXmlReaderFactory.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace Dei.Protocol
{
    using System.IO;
    using System.Xml;

    /// <summary>
    /// Abstract factory for creating XmlReader.
    /// </summary>
    internal interface IXmlReaderFactory
    {
        /// <summary>
        /// Create a xml reader.
        /// </summary>
        /// <param name="stream">Stream used by the xml reader.</param>
        /// <param name="settings">Xml reader settings.</param>
        /// <returns>Return xml reader.</returns>
        XmlReader Create(Stream stream, XmlReaderSettings settings);
    }
}
