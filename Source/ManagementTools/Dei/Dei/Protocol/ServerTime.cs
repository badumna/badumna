//-----------------------------------------------------------------------
// <copyright file="ServerTime.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace Dei
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Dei.Protocol;
    
    /// <summary>
    /// Server time class store the current server time and the expiry time.
    /// </summary>
    [Serializable]
    public class ServerTime
    {
        /// <summary>
        /// Server time.
        /// </summary>
        private UtcDateTime time = new UtcDateTime();
        
        /// <summary>
        /// Server expiry time.
        /// </summary>
        private UtcDateTime expiryTime = new UtcDateTime();

        /// <summary>
        /// Initializes a new instance of the <see cref="ServerTime"/> class.
        /// </summary>
        public ServerTime()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ServerTime"/> class.
        /// </summary>
        /// <param name="time">Utc date time.</param>
        /// <param name="validityPeriod">Validity period.</param>
        public ServerTime(UtcDateTime time, TimeSpan validityPeriod)
        {
            this.time = time;
            this.expiryTime = new UtcDateTime(time.DateTime + validityPeriod);
        }

        /// <summary>
        /// Gets and sets the server time.
        /// </summary>
        public UtcDateTime Time
        {
            get { return this.time; }
            set { this.time = value; }
        }

        /// <summary>
        /// Gets and sets the server expiry time.
        /// </summary>
        public UtcDateTime ExpiryTime
        {
            get { return this.expiryTime; }
            set { this.expiryTime = value; }
        }
    }
}
