//-----------------------------------------------------------------------
// <copyright file="Server.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace DeiServer
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Net;
    using System.Net.Security;
    using System.Net.Sockets;
    using System.Security.Authentication;
    using System.Security.Cryptography;
    using System.Security.Cryptography.X509Certificates;
    using System.Text;
    using System.Threading;
    using System.Xml;
    using System.Xml.Serialization;
    using Dei.Protocol;
    using Dei.Utilities;

    /// <summary>
    /// Server class is used in DeiServer application, this Server class is listening using tcp network protocol.
    /// </summary>
    public class Server 
    {
        /// <summary>
        /// Tcp listener.
        /// </summary>
        private readonly TcpListener listener;

        /// <summary>
        /// Server certificate used.
        /// </summary>
        private X509Certificate serverCertificate;

        /// <summary>
        /// A value indicating whether the server is curently listening.
        /// </summary>
        private volatile bool isListening = true;

        /// <summary>
        /// Token generator <seealso cref="TokenGenerator"/>.
        /// </summary>
        private TokenGenerator tokenGenerator;

        /// <summary>
        /// A value indicating whether the ssl connection is used.
        /// </summary>
        private bool useSslConnection = true;

        /// <summary>
        /// Initializes a new instance of the <see cref="Server"/> class. 
        /// </summary>
        /// Construct a new server which take serverEndPoint, accountManager and certificate as arguments
        /// <param name="serverEndPoint">Server address</param>
        /// <param name="accountManager">a class derived from IAccountManager</param>
        /// <param name="certificate">X509 Certificate</param>
        public Server(IPEndPoint serverEndPoint, IAccountManager accountManager, X509Certificate certificate) 
        {
            this.tokenGenerator = new TokenGenerator(accountManager);
            this.serverCertificate = certificate;

            try
            {
                System.Console.WriteLine("Attempting to listen on port {0}", serverEndPoint.Port);
                this.listener = new TcpListener(serverEndPoint);
                this.listener.Start();
                this.isListening = true;
            }
            catch (SocketException e)
            {
                System.Diagnostics.Trace.TraceError("Socket exception while trying to start server : {0}", e.Message);
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Server"/> class.
        /// </summary>
        /// Construct a new server which take port number, accountManager and certificate as arguments
        /// <param name="port">Port number in which the server listen on</param>
        /// <param name="accountManager"> a class derived from IAccountManager</param>
        /// <param name="certificate">X509 Certificate</param>
        public Server(int port, IAccountManager accountManager, X509Certificate certificate)
            : this(new IPEndPoint(IPAddress.Any, port), accountManager, certificate)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Server"/> class.
        /// </summary>
        /// Construct a new server which take port number, accountManager, certificate and SSLConection as arguments
        /// <param name="port">Port number in which the server listen on</param>
        /// <param name="accountManager">a class derived from IAccountManager</param>
        /// <param name="certificate">X509 Certificate</param>
        /// <param name="useSslConnection">Option to use SSL connection or Tcp connection for authentication, 
        /// for Unity developer set useSslConnection to false</param>
        public Server(int port, IAccountManager accountManager, X509Certificate certificate, bool useSslConnection)
            : this(new IPEndPoint(IPAddress.Any, port), accountManager, certificate)
        {
            this.useSslConnection = useSslConnection;
            System.Console.WriteLine("Listening using SSL connection: {0}", this.useSslConnection);
        }

        /// <summary>
        /// Generate a new key pair
        /// </summary>
        public static void GenerateNewKeyPair()
        {
            TokenGenerator.GenerateNewKeyPair();
        }

        /// <summary>
        /// Start the server
        /// </summary>
        public void Start()
        {
            while (this.isListening)
            {
                this.StartClient(this.listener.AcceptTcpClient());
            }

            this.listener.Stop();
        }

        /// <summary>
        /// Stop the server
        /// </summary>
        public void Stop()
        {
            this.isListening = false;
            this.tokenGenerator.Dispose();
            this.tokenGenerator = null;
        }

        /// <summary>
        /// Start the client by creating a new server protocol, authenticate as server is required when
        /// ssl connection is used.
        /// </summary>
        /// <param name="client">Tcp client.</param>
        private void StartClient(TcpClient client)
        {
            try
            {
                if (this.useSslConnection)
                {
                    SslStream secureStream = new SslStream(client.GetStream(), false);
                    secureStream.AuthenticateAsServer(this.serverCertificate, false, SslProtocols.Tls, false);
                    new ServerProtocol(secureStream, this.tokenGenerator);
                }
                else
                {
                    new ServerProtocol(client.GetStream(), this.tokenGenerator);
                }
            }
            catch (AuthenticationException e)
            {
                System.Diagnostics.Trace.TraceError("Failed to authenticate client : {0}", e.Message);
                client.Close();
            }
            catch (IOException e)
            {
                System.Diagnostics.Trace.TraceError("Failed to authenticate client : {0}", e.Message);
                client.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Trace.TraceError("Failed to start client : {0}", e.Message);
                client.Close();
            }
        }
    }
}
