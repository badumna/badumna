//-----------------------------------------------------------------------
// <copyright file="Permission.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace Dei
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Badumna.Security;
    using Dei.Protocol;
    
    /// <summary>
    /// Dei permission class store the name and the state of the permission. 
    /// </summary>
    [Serializable]
    public class Permission
    {
        /// <summary>
        /// Permission state
        /// </summary>
        private byte[] state;

        /// <summary>
        /// Permission signiture.
        /// </summary>
        private byte[] signiture;

        /// <summary>
        /// Permission name.
        /// </summary>
        private string name = string.Empty;

        /// <summary>
        /// Initializes a new instance of the <see cref="Permission"/> class.
        /// </summary>
        public Permission()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Permission"/> class.
        /// </summary>
        /// <param name="expiryTime">Expiry time.</param>
        /// <param name="type">Permission type.</param>
        /// <param name="serializedPublicKey">Serialized public key.</param>
        /// <param name="permissionName">Permission name.</param>
        public Permission(UtcDateTime expiryTime, long type, byte[] serializedPublicKey, string permissionName)
        {
            this.name = permissionName;
            this.state = PermissionListToken.GeneratePermission(expiryTime.DateTime, type, serializedPublicKey);
        }

        /// <summary>
        /// Gets or sets the permission state.
        /// </summary>
        public byte[] State
        {
            get { return this.state; }
            set { this.state = value; }
        }

        /// <summary>
        /// Gets or sets the permission signature.
        /// </summary>
        public byte[] Signiture
        {
            get { return this.signiture; }
            set { this.signiture = value; }
        }
        
        /// <summary>
        /// Gets or sets permission name.
        /// </summary>
        public string Name
        {
            get { return this.name; }
            set { this.name = value; }
        }
    }
}
