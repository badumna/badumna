//-----------------------------------------------------------------------
// <copyright file="UtcDateTime.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace Dei.Protocol
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Xml.Serialization;

    /// <summary>
    /// A DateTime guaranteed to be in UTC and serialized deserialized correctly.
    /// </summary>
    /// <remarks>
    /// This class has a few purposes:
    ///    i)   Easily identify which dates are supposed to be UTC
    ///    ii)  Warn when a local time is used as a UTC time unintentionally
    ///    iii) Work around issues in XML serialization by doing serialization as a number of ticks
    /// </remarks>
    [Serializable]
    public class UtcDateTime
    {
        /// <summary>
        /// This should always have Kind = Utc.
        /// </summary>
        private DateTime dateTime;

        /// <summary>
        /// Initializes a new instance of the <see cref="UtcDateTime"/> class.
        /// </summary>
        public UtcDateTime()
        {
            this.dateTime = new DateTime(0, DateTimeKind.Utc);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UtcDateTime"/> class.
        /// </summary>
        /// <param name="dateTime">Date time in utc format.</param>
        public UtcDateTime(DateTime dateTime)
        {
            if (dateTime.Kind != DateTimeKind.Utc)
            {
                throw new ArgumentException("Must be in UTC", "dateTime");
            }

            this.dateTime = dateTime;
        }

        /// <summary>
        /// Gets the date time.
        /// </summary>
        [XmlIgnore]
        public DateTime DateTime
        {
            get { return this.dateTime; }
        }

        /// <summary>
        /// Gets or sets the time as a long ticks.
        /// </summary>
        public long Ticks
        {
            get { return this.dateTime.Ticks; }
            set { this.dateTime = new DateTime(value, DateTimeKind.Utc); }
        }
    }
}
