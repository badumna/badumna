﻿//-----------------------------------------------------------------------
// <copyright file="SavableGenerator.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2011 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace DeiServer
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Text;
    using System.Xml;
    using System.Xml.Serialization;
    using Dei;
    using Dei.Protocol;

    /// <summary>
    /// This class maintains a an IExpirable item that can be saved/loaded into/from a xml file. 
    /// </summary>
    /// <typeparam name="ExpirableType">Expirable type.</typeparam>
    internal abstract class SavableGenerator<ExpirableType> where ExpirableType : IExpirable
    {
        /// <summary>
        /// Random number source.
        /// </summary>
        private static Random randomSource = new Random((int)DateTime.Now.Ticks);

        /// <summary>
        /// ExpirableType validity period.
        /// </summary>
        private TimeSpan validityPeriod;

        /// <summary>
        /// Collections of expirable type.
        /// </summary>
        private ExpirableType expirable;

        /// <summary>
        /// Initializes a new instance of the SavableGenerator class.
        /// </summary>
        /// <param name="validityPeriod">Validity period of ExpirableType.</param>
        public SavableGenerator(TimeSpan validityPeriod)
        {
            this.validityPeriod = validityPeriod;
        }

        /// <summary>
        /// Gets the random number source.
        /// </summary>
        protected static Random RandomSource
        {
            get { return SavableGenerator<ExpirableType>.randomSource; }
        }

        /// <summary>
        /// Get the current instance of ExpirableType.
        /// </summary>
        /// <returns>Return the curren instance of ExpirableType.</returns>
        public ExpirableType GetCurrentInstance()
        {
            lock (this)
            {
                if (this.expirable == null)
                {
                    this.expirable = this.Generate(this.validityPeriod);
                    this.Save();
                }

                return this.expirable;
            }
        }

        /// <summary>
        /// Generate a new ExpirableType based on the validity period.
        /// </summary>
        /// <param name="validityPeriod">Validity period for ExpirableType.</param>
        /// <returns>Returns a new ExpirableType instance.</returns>
        protected abstract ExpirableType Generate(TimeSpan validityPeriod);

        /// <summary>
        /// Save the ExpirableTypes to xml file.
        /// </summary>
        protected virtual void Save()
        {
        }

        /// <summary>
        /// Save the ExpirableTypes to xml file.
        /// </summary>
        /// <param name="stream">The file stream where the data will be save into.</param>
        /// <returns>Return true on success.</returns>
        protected bool Save(Stream stream)
        {
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(ExpirableType));
                XmlSerializerNamespaces expirableNamespaces = new XmlSerializerNamespaces(
                    new XmlQualifiedName[] { new XmlQualifiedName(string.Empty, string.Empty) });

                XmlWriterSettings setting = new XmlWriterSettings();
                setting.CloseOutput = false;
                using (XmlWriter writer = XmlWriter.Create(stream, setting))
                {
                    writer.WriteStartElement(string.Empty, "Expirables", string.Empty);
                    serializer.Serialize(writer, this.expirable, expirableNamespaces);
                    writer.WriteEndElement();
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Trace.TraceError("Exception while trying to save expirables : {0}", e.Message);
            }

            return false;
        }

        /// <summary>
        /// Load the ExpirableType from a file stream.
        /// </summary>
        /// <param name="stream">File stream need to be read.</param>
        /// <returns>Return true on success.</returns>
        protected bool Load(Stream stream)
        {
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(ExpirableType));

                using (XmlReader reader = XmlReader.Create(stream))
                {
                    if (reader.ReadToDescendant("Expirables", string.Empty))
                    {
                        using (XmlReader expirableReader = reader.ReadSubtree())
                        {
                            while (reader.Read() && serializer.CanDeserialize(reader))
                            {
                                ExpirableType deserializedExpirable = (ExpirableType)serializer.Deserialize(reader);

                                // Set the validity period of the loaded expirable to be no greater than the
                                // currently set validity period. This prevents saved expirables with large validity periods
                                // preventing new expirabled from being created if the validity period is changed.
                                if (deserializedExpirable.ExpiryTime.DateTime > DateTime.UtcNow + this.validityPeriod)
                                {
                                    deserializedExpirable.ExpiryTime = new UtcDateTime(DateTime.UtcNow + this.validityPeriod);
                                }

                                if (!deserializedExpirable.HasExpired)
                                {
                                    this.expirable = deserializedExpirable;
                                    Console.WriteLine(
                                        "The validity period of the loaded ParticipationKey : {0} UTC", 
                                        this.expirable.ExpiryTime.DateTime);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Trace.TraceError("Exception while trying to load expirables : {0}", e.Message);
            }

            return false;
        }
    }
}
