//-----------------------------------------------------------------------
// <copyright file="ParticipationKey.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace Dei
{
    using System;
    using System.Collections.Generic;
    using System.Security.Cryptography;
    using System.Text;
    using System.Xml;
    using Badumna.Security;
    using Dei.Protocol;

    /// <summary>
    /// Participation key implements IExpirable interface is requested by dei client 
    /// and use this for login to Badumna network.
    /// </summary>
    [Serializable]
    public class ParticipationKey : IExpirable
    {
        /// <summary>
        /// Key bytes length.
        /// </summary>
        [NonSerialized]
        private static int keyLengthBytes = SymmetricKeyToken.KeyLengthBytes;

        /// <summary>
        /// Random number generator.
        /// </summary>
        private static Random random = new Random((int)DateTime.Now.Ticks);

        /// <summary>
        /// Participation key.
        /// </summary>
        private byte[] key = new byte[ParticipationKey.KeyLengthBytes];

        /// <summary>
        /// Expiry time of the participation key
        /// </summary>
        private UtcDateTime expiryTime = new UtcDateTime();

        /// <summary>
        /// Initializes a new instance of the <see cref="ParticipationKey"/> class.
        /// </summary>
        public ParticipationKey()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ParticipationKey"/> class.
        /// </summary>
        /// <param name="validityPeriod">Key validity period.</param>
        /// <param name="key">Key in byte.</param>
        public ParticipationKey(TimeSpan validityPeriod, byte[] key)
        {
            if (key == null)
            {
                throw new ArgumentNullException("key");
            }

            if (key.Length != ParticipationKey.KeyLengthBytes)
            {
                throw new InvalidOperationException("Invalid key length");
            }

            this.key = key;

            if (validityPeriod == TimeSpan.MaxValue)
            {
                this.expiryTime = new UtcDateTime(new DateTime(DateTime.MaxValue.Ticks, DateTimeKind.Utc));
            }
            else
            {
                this.expiryTime = new UtcDateTime(DateTime.UtcNow + validityPeriod);
            }
        }

        /// <summary>
        /// Gets the key bytes length.
        /// </summary>
        public static int KeyLengthBytes
        {
            get { return ParticipationKey.keyLengthBytes; }
        }

        /// <summary>
        /// Gets or sets the participation key in bytes.
        /// </summary>
        public byte[] Key
        {
            get { return this.key; }
            set { this.key = value; }
        }

        /// <summary>
        /// Gets or sets the expiry time of the participation key.
        /// </summary>
        public UtcDateTime ExpiryTime
        {
            get { return this.expiryTime; }
            set { this.expiryTime = value; }
        }

        /// <summary>
        /// Gets a value indicating whether the participation key has expired.
        /// </summary>
        public bool HasExpired 
        { 
            get 
            { 
                return this.expiryTime.DateTime < DateTime.UtcNow; 
            } 
        }

        /// <summary>
        /// Generate random participation key with given validity period.
        /// </summary>
        /// <param name="validityPeriod">Key validity period.</param>
        /// <returns>Return the new random participation key.</returns>
        public static ParticipationKey GenerateRandomParticipationKey(TimeSpan validityPeriod)
        {
            byte[] key = new byte[ParticipationKey.KeyLengthBytes];

            ParticipationKey.random.NextBytes(key);
            return new ParticipationKey(validityPeriod, key);
        }
    }
}
