//-----------------------------------------------------------------------
// <copyright file="SerializerWrapperFactory.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace Dei.Protocol
{
    using System;

    /// <summary>
    /// Factory for creating SerializerWrapper.
    /// </summary>
    internal class SerializerWrapperFactory : ISerializerWrapperFactory
    {
        /// <summary>
        /// Create a SerializerWrapper.
        /// </summary>
        /// <param name="type">Type of serializer.</param>
        /// <returns>Return serializer wrapper.</returns>
        public ISerializerWrapper Create(Type type)
        {
            return new SerializerWrapper(type);
        }
    }
}
