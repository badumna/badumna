//-----------------------------------------------------------------------
// <copyright file="IXmlWriterFactory.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace Dei.Protocol
{
    using System.IO;
    using System.Xml;

    /// <summary>
    /// Abstract factory for creating XmlWriter.
    /// </summary>
    internal interface IXmlWriterFactory
    {
        /// <summary>
        /// Create a xml writer.
        /// </summary>
        /// <param name="stream">Stream used by the xml writer.</param>
        /// <param name="settings">Xml writer settings.</param>
        /// <returns>Return XmlWriter.</returns>
        XmlWriter Create(Stream stream, XmlWriterSettings settings);
    }
}
