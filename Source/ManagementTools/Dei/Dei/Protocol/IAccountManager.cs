//-----------------------------------------------------------------------
// <copyright file="IAccountManager.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace DeiServer
{
    using System;
    using System.Collections.Generic;
    using Badumna.Security;
    using Dei;
 
    /// <summary>
    /// Encapsulates all account management operations.
    /// </summary>
    public interface IAccountManager : IDisposable
    {
        /// <summary>
        /// Initializes this instance.
        /// </summary>
        /// This method will be called as soon as this instance is created. The Dei Server will be terminated if this
        /// method throw any exception. 
        void Initialize();

        /// <summary>
        /// Shutdowns this instance.
        /// </summary>
        /// This method will be called when the Dei server is being shutdown. The Dei Server will catch all exception
        /// thrown from this method and output it to the standard error.
        void Shutdown();

        /// <summary>
        /// Authenticate user credentials. 
        /// </summary>
        /// Authenticate the user if the username and password given are valid, check the validity of username 
        /// and password from database.
        /// <param name="username">User name.</param>
        /// <param name="password">User password.</param>
        /// <returns>Return true if the username and password are valid, otherwise it return false.</returns>
        bool AuthenticateUser(string username, string password);

        /// <summary>
        /// Get the user Id for a particular username
        /// </summary>
        /// Get the userId of a particular username from the database.
        /// <param name="username">User name.</param>
        /// <returns>Return the userId of given username, if the username is a valid username. Otherwise returns -1</returns>
        long GetUserId(string username);

        /// <summary>
        /// Get the character for a particular user's character ID
        /// </summary>
        /// <param name="username">User name.</param>
        /// <param name="character">The character.</param>
        /// <returns>Return true if the character is valid and belongs to user. Otherwise false</returns>
        bool AuthorizeCharacter(string username, Character character);

        /// <summary>
        /// Check whether the user has a particular permission
        /// </summary>
        /// For example, admin has admin permission (privilege), while the normal users only have participation permission.
        /// <param name="username">User name.</param>
        /// <param name="permissionId">Permission Id.</param>
        /// <returns>Return true if username has that permission, otherwise it will return false.</returns>
        bool HasPermission(string username, long permissionId);

        /// <summary>
        /// Find the users which match with the given conditions.
        /// </summary>
        /// For example, "*" is used to obtain all the available users from database.
        /// <param name="usernameGlobExpression">Search conditions.</param>
        /// <returns>Return list of string consist of the username that match with the given conditions.</returns>
        IList<string> FindUsers(string usernameGlobExpression);

        /// <summary>
        /// Get the total number of user that satisfy the given conditions
        /// </summary>
        /// For example, to get the total number of users exist in database, use "*" for usernameGlobExpression.
        /// <param name="usernameGlobExpression">Search conditions.</param>
        /// <returns>Return the number of user that satisfy the given conditions.</returns>
        long GetUserCount(string usernameGlobExpression);

        /// <summary>
        /// Create a new account by providing the new username and password.
        /// </summary>
        /// Create a new account and store it in the database
        /// <param name="username">User name.</param>
        /// <param name="password">User password.</param>
        /// <returns>Return true if it successfully create a new account.</returns>
        bool CreateAccount(string username, string password);

        /// <summary>
        /// Create a new character belonging to the given user.
        /// </summary>
        /// Create a new character and store it in the database
        /// <param name="username">The owner's user name.</param>
        /// <param name="characterName">The new character's name.</param>
        /// <returns>Return the character if it successfully create a new character, otherwise null.</returns>
        Character CreateCharacter(string username, string characterName);

        /// <summary>
        /// Delete an existing account with the given username.
        /// </summary>
        /// Delete an existing account for a particular username from database.
        /// <param name="username">User name.</param>
        /// <returns>Return true if it successfully delete an account from database.</returns>
        bool DeleteAccount(string username);

        /// <summary>
        /// Delete the character belonging to the given user.
        /// </summary>
        /// <param name="username">The owner's user name.</param>
        /// <param name="character">The character.</param>
        /// <returns>Returns true on success.</returns>
        bool DeleteCharacter(string username, Character character);

        /// <summary>
        /// Change the password of a specific username.
        /// </summary>
        /// <param name="username">User name.</param>
        /// <param name="newPassword">New password.</param>
        /// <returns>Return true, if the password is successfully changed.</returns>
        bool ChangePassword(string username, string newPassword);

        /// <summary>
        /// Create a new permission type, it used by Admin only.
        /// </summary>
        /// A user require a creating permission privilage to be able to create a new permission.
        /// <param name="permissionName">Permission name.</param>
        /// <returns>Return the permission id of the new permission.</returns>
        long CreatePermission(string permissionName);

        /// <summary>
        /// Create a new permission type by providing both the permission name and permission id
        /// </summary>
        /// <seealso cref="CreatePermission(string)"/>
        /// <param name="permissionId">Permission Id.</param>
        /// <param name="permissionName">Permission name.</param>
        /// <returns>Return true if the new permission is created successfully.</returns>
        bool CreatePermission(long permissionId, string permissionName);

        /// <summary>
        /// Delete an existing permission according to given permissionId
        /// </summary>
        /// Delete an existing permission from the database based on given permissionId
        /// <param name="permissionId">Permission Id.</param>
        /// <returns>Return true if the permission is removed successfully</returns>
        bool DeletePermission(long permissionId);

        /// <summary>
        /// Grant a particular permission to a particular user
        /// </summary>
        /// <param name="username">User name.</param>
        /// <param name="permissionId">Permission Id</param>
        /// <returns>Return true if the permission is granted successfully to the particular user.</returns>
        bool GrantPermission(string username, long permissionId);

        /// <summary>
        /// Revoke a particular permission from a particular user
        /// </summary>
        /// <param name="username">User name.</param>
        /// <param name="permissionId">Permission Id</param>
        /// <returns>Return true if the permission is revoked successfully from the particular user.</returns>
        bool RevokePermission(string username, long permissionId);

        /// <summary>
        /// Get the list of available permission for a certain user
        /// </summary>
        /// <param name="username">User name.</param>
        /// <returns>Return the list of permission that particular user has</returns>
        IList<PermissionProperties> GetPermissions(string username);

        /// <summary>
        /// Get the list of characters for a certain user
        /// </summary>
        /// <param name="username">User name.</param>
        /// <returns>Return the list of characters that a particular user has</returns>
        IList<Character> GetCharacters(string username);

        /// <summary>
        /// Get the participation validity period
        /// </summary>
        /// <returns>Return the participation validity period</returns>
        TimeSpan GetParticipationValidityPeriod();

        /// <summary>
        /// Get the list of all available permissions from database
        /// </summary>
        /// <returns>Return list of all available permissions.</returns>
        IList<PermissionProperties> ListAllPermissions();
    }
}
