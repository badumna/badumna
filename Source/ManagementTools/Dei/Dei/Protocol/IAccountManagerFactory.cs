﻿//-----------------------------------------------------------------------
// <copyright file="IAccountManagerFactory.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;

namespace DeiServer
{
    /// <summary>
    /// The interface of the account manager factory class.
    /// </summary>
    public interface IAccountManagerFactory
    {
        /// <summary>
        /// Creates the account manager.
        /// </summary>
        /// <param name="keyValidityPeriod">The key validity period.</param>
        /// <returns>The account manager object.</returns>
        IAccountManager Create(TimeSpan keyValidityPeriod);
    }
}
