//-----------------------------------------------------------------------
// <copyright file="ServerProtocol.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Dei.Protocol
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Xml;
    using Badumna.Security;
    using Dei;
    using DeiServer;    

    /// <summary>
    /// Dei Server Protocol
    /// </summary>
    internal class ServerProtocol : BaseProtocol
    {
        /// <summary>
        /// The maximum wrong login tries.
        /// </summary>
        private const int MaxWrongLoginTries = 3;
        
        /// <summary>
        /// User properties
        /// </summary>
        private UserProperties loggedInUser;

        /// <summary>
        /// Token generator
        /// </summary>
        private TokenGenerator tokenGenerator;

        /// <summary>
        /// Initializes a new instance of the <see cref="ServerProtocol"/> class.
        /// </summary>
        /// <param name="clientStream">The client stream.</param>
        /// <param name="tokenGenerator">Token generator.</param>
        public ServerProtocol(Stream clientStream, TokenGenerator tokenGenerator)
            : this(
            clientStream, 
            tokenGenerator, 
            new XmlWriterFactory(), 
            new XmlReaderFactory(), 
            new SerializerWrapperFactory())
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ServerProtocol"/> class.
        /// </summary>
        /// <param name="clientStream">The client stream.</param>
        /// <param name="tokenGenerator">Token generator.</param>
        /// <param name="xmlWriterFactory">Xml writer factory.</param>
        /// <param name="xmlReaderFactory">Xml reader factory.</param>
        /// <param name="serializerWrapperFactory">Serialzer wrapper factory.</param>
        internal ServerProtocol(
            Stream clientStream,
            TokenGenerator tokenGenerator,
            IXmlWriterFactory xmlWriterFactory,
            IXmlReaderFactory xmlReaderFactory,
            ISerializerWrapperFactory serializerWrapperFactory)
            : base(clientStream, xmlWriterFactory, xmlReaderFactory, serializerWrapperFactory)
        {
            this.tokenGenerator = tokenGenerator;
            this.Start();
        }

        #region ProcessCommand

        /// <summary>
        /// OnRequestPermissionList callback is called when the client requesting list of permission
        /// given the user properties.
        /// </summary>
        /// <param name="userProperties">User properties.</param>
        /// <param name="publicKey">Public key.</param>
        public void OnRequestPermissionList(UserProperties userProperties, Dei.PublicKey publicKey)
        {
            System.Diagnostics.Trace.TraceInformation("client: OnRequestPermissionList()");

            if (string.IsNullOrEmpty(userProperties.Login) || !this.IsValidName(userProperties.Login)
                || publicKey.Key == null)
            {
                this.SendRequest(Commands.RequestPermissionListFailed, FailureReasons.InvalidRequest);
            }

            if (this.IsLoggedInAndHasPermission(Commands.RequestPermissionListFailed, userProperties, PermissionTypes.None))
            {
                try
                {
                    IList<PermissionProperties> permissions = this.tokenGenerator.AccountManager.GetPermissions(userProperties.Login);
                    TimeSpan permissionValidityPeriod = this.tokenGenerator.AccountManager.GetParticipationValidityPeriod();

                    if (permissions != null)
                    {
                        foreach (PermissionProperties properties in permissions)
                        {
                            Dei.Permission permission = new Dei.Permission(
                                new UtcDateTime(DateTime.UtcNow + permissionValidityPeriod),
                                properties.Id, 
                                publicKey.Key, 
                                properties.Name);
                            permission.Signiture = this.tokenGenerator.Sign(permission.State);
                            this.SendRequest(Commands.AddPermission, permission);
                        }
                    }

                    this.SendRequest(Commands.RequestPermissionListComplete);
                }
                catch (Exception e)
                {
                    System.Diagnostics.Trace.TraceError("Caught exception : {0}", e.Message);
                    this.SendRequest(Commands.RequestPermissionListFailed, FailureReasons.NoneGiven);
                }
            }
        }

        /// <summary>
        /// OnRequestListPermissionTypes callback is called when the client requesting all available 
        /// permission type. Send the available permission type to the user if and only if the user 
        /// granted with ViewPermissions permission.
        /// </summary>
        /// <param name="userProperties">User properties.</param>
        public void OnRequestListPermissionTypes(UserProperties userProperties)
        {
            System.Diagnostics.Trace.TraceInformation("client: OnRequestListPermissionTypes()");

            if (!this.IsValidName(userProperties.Login))
            {
                this.SendRequest(Commands.RequestListPermissionTypesFailed, FailureReasons.InvalidRequest);
            }

            PermissionTypes permission = PermissionTypes.ViewPermissions;

            if (userProperties.Login == this.loggedInUser.Login)
            {
                permission = PermissionTypes.None;
            }

            if (this.IsLoggedInAndHasPermission(Commands.RequestListPermissionTypesFailed, this.loggedInUser, permission))
            {
                try
                {
                    IList<PermissionProperties> permissionProperties = null;
                    if (string.IsNullOrEmpty(userProperties.Login))
                    {
                        System.Diagnostics.Trace.TraceInformation("Listing all permissions ...");
                        permissionProperties = this.tokenGenerator.AccountManager.ListAllPermissions();
                    }
                    else
                    {
                        System.Diagnostics.Trace.TraceInformation("Listing permissions for {0} ...", userProperties.Login);
                        permissionProperties = this.tokenGenerator.AccountManager.GetPermissions(userProperties.Login);
                    }

                    if (permissionProperties != null)
                    {
                        foreach (PermissionProperties permissionProperty in permissionProperties)
                        {
                            this.SendRequest(Commands.AddPermissionType, permissionProperty);
                        }
                    }

                    this.SendRequest(Commands.RequestListPermissionTypesComplete);
                }
                catch (Exception e)
                {
                    System.Diagnostics.Trace.TraceError("Caught exception : {0}", e.Message);
                    this.SendRequest(Commands.RequestListPermissionTypesFailed, FailureReasons.NoneGiven);
                }
            }
        }

        /// <summary>
        /// Protocol version error.
        /// </summary>
        /// <param name="myVersion">Server version number.</param>
        /// <param name="theirVersion">Client version number.</param>
        protected override void ProtocolVersionError(Version myVersion, Version theirVersion)
        {
            System.Diagnostics.Trace.TraceInformation("client: Incompatable version");
        }

        /// <summary>
        /// Process the commands.
        /// </summary>
        /// <param name="command">Dei commands.</param>
        /// <param name="reader">Xml reader.</param>
        protected override void ProcessCommand(Commands command, XmlReader reader)
        {
            ISerializerWrapper failureReasonsSerializer = this.GetSerializer(typeof(FailureReasons));

            try
            {
                switch (command)
                {
                    case Commands.RequestLogin:
                        this.OnLogin(this.GetParameter<UserProperties>(reader));
                        break;

                    case Commands.RequestLogout:
                        this.OnLogout(this.GetParameter<UserProperties>(reader));
                        break;

                    case Commands.RequestNewAccount:
                        this.OnNewAccount(this.GetParameter<UserProperties>(reader));
                        break;

                    case Commands.RequestDeleteAcount:
                        this.OnDeleteAcount(this.GetParameter<UserProperties>(reader));
                        break;

                    case Commands.RequestChangeAccount:
                        this.OnChangeAccount(this.GetParameter<UserProperties>(reader));
                        break;

                    case Commands.RequestCreatePermission:
                        this.OnCreatePermission(this.GetParameter<PermissionProperties>(reader));
                        break;

                    case Commands.RequestRemovePermission:
                        this.OnRemovePermission(this.GetParameter<PermissionProperties>(reader));
                        break;

                    case Commands.RequestGrantPermission:
                        this.OnGrantPermission(this.GetParameter<UserProperties>(reader), this.GetParameter<PermissionProperties>(reader));
                        break;

                    case Commands.RequestRevokePermission:
                        this.OnRevokePermission(this.GetParameter<UserProperties>(reader), this.GetParameter<PermissionProperties>(reader));
                        break;

                    case Commands.RequestClientCertificate:
                        this.OnClientCertificate(this.GetParameter<UserCertificate>(reader));
                        break;

                    case Commands.RequestPermissionList:
                        this.OnRequestPermissionList(this.GetParameter<UserProperties>(reader), this.GetParameter<Dei.PublicKey>(reader));
                        break;

                    case Commands.RequestListPermissionTypes:
                        this.OnRequestListPermissionTypes(this.GetParameter<UserProperties>(reader));
                        break;

                    case Commands.RequestParticipationKey:
                        this.OnRequestParticipationKey(this.GetParameter<UserProperties>(reader), this.GetParameter<TokenRequest>(reader));
                        break;

                    case Commands.RequestServersPublicKey:
                        this.OnRequestPublicKey(this.GetParameter<UserProperties>(reader), this.GetParameter<TokenRequest>(reader));
                        break;

                    case Commands.RequestServerTime:
                        this.OnRequestTime(this.GetParameter<UserProperties>(reader));
                        break;

                    case Commands.RequestUserCount:
                        this.OnRequestUserCount(this.GetParameter<ListUserRequest>(reader));
                        break;

                    case Commands.RequestUserList:
                        this.OnRequestListUsers(this.GetParameter<ListUserRequest>(reader));
                        break;

                    case Commands.RequestCreateCharacter:
                        this.OnCreateCharacter(this.GetParameter<UserProperties>(reader), this.GetParameter<string>(reader));
                        break;

                    case Commands.RequestDeleteCharacter:
                        this.OnDeleteCharacter(this.GetParameter<UserProperties>(reader), this.GetParameter<Character>(reader));
                        break;

                    case Commands.RequestCharacterList:
                        this.OnRequestCharacterList(this.GetParameter<UserProperties>(reader));
                        break;

                    case Commands.None:
                        break;
                }
            }
            catch (Exception e)
            {
                this.ProtocolError("Exception thrown while processing command : {0}", e.Message);
                this.CloseConnection();
            }
        }

        #endregion

        /// <summary>
        /// OnLogin callback is called when there is a user try to login.
        /// </summary>
        /// <param name="userProperties">User properties.</param>
        private void OnLogin(UserProperties userProperties)
        {
            System.Diagnostics.Trace.TraceInformation("client: OnLogin()");

            if (string.IsNullOrEmpty(userProperties.Login) || string.IsNullOrEmpty(userProperties.Password) || !this.IsValidName(userProperties.Login))
            {
                this.SendRequest(Commands.RequestLoginRejected);
            }

            try
            {
                if (this.tokenGenerator.AccountManager.AuthenticateUser(userProperties.Login, userProperties.Password))
                {
                    this.loggedInUser = userProperties;
                    IList<Character> characters = this.tokenGenerator.AccountManager.GetCharacters(userProperties.Login);
                    if (characters != null)
                    {
                        this.SendRequest(Commands.RequestLoginAccepted, new LoginResult(characters));
                        return;
                    }
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Trace.TraceError("Caught exception : {0}", e.Message);
            }

            this.SendRequest(Commands.RequestLoginRejected);
        }

        /// <summary>
        /// OnLogout callback is called when there is a user request to logout. 
        /// </summary>
        /// <param name="userProperties">User properties</param>
        private void OnLogout(UserProperties userProperties)
        {
            System.Diagnostics.Trace.TraceInformation("client: OnLogout()");

            if (string.IsNullOrEmpty(userProperties.Login))
            {
                this.SendRequest(Commands.RequestLogoutFailed, FailureReasons.InvalidRequest);
            }

            if (this.IsLoggedInAndHasPermission(Commands.RequestLogoutFailed, userProperties, PermissionTypes.None))
            {
                this.SendRequest(Commands.RequestLogoutAccepted);
            }
        }

        /// <summary>
        /// OnNewAccount callback is called when the client request a new account creation.
        /// </summary>
        /// <param name="newUserProperties">New user properties that need to be created.</param>
        private void OnNewAccount(UserProperties newUserProperties)
        {
            System.Diagnostics.Trace.TraceInformation("client: OnNewAccount()");

            if (string.IsNullOrEmpty(newUserProperties.Login) || string.IsNullOrEmpty(newUserProperties.Password)
                || !this.IsValidName(newUserProperties.Login))
            {
                this.SendRequest(Commands.RequestNewAccountFailed, FailureReasons.InvalidRequest);
            }

            if (this.IsLoggedInAndHasPermission(Commands.RequestNewAccountFailed, this.loggedInUser, PermissionTypes.CreateUsers))
            {
                try
                {
                    if (!this.tokenGenerator.AccountManager.AuthenticateUser(newUserProperties.Login, newUserProperties.Password))
                    {
                        System.Diagnostics.Trace.TraceInformation("client: OnNewAccount() creating a new account");
                        if (this.tokenGenerator.AccountManager.CreateAccount(newUserProperties.Login, newUserProperties.Password))
                        {
                            this.SendRequest(Commands.RequestNewAccountAccepted, newUserProperties);
                        }
                        else
                        {
                            this.SendRequest(Commands.RequestNewAccountFailed, FailureReasons.NoneGiven);
                        }
                    }
                    else
                    {
                        System.Diagnostics.Trace.TraceInformation("client: OnNewAccount() an account already exists");
                        this.SendRequest(Commands.RequestNewAccountFailed, FailureReasons.AccountAlreadyExists);
                    }
                }
                catch (Exception e)
                {
                    this.HandleException(e, Commands.RequestNewAccountFailed);
                }
            }
        }

        /// <summary>
        /// OnDeleteAccount callback is called when the client request delete an account
        /// </summary>
        /// <param name="userProperties">User properties</param>
        private void OnDeleteAcount(UserProperties userProperties)
        {
            System.Diagnostics.Trace.TraceInformation("client: OnDeleteAcount()");

            if (string.IsNullOrEmpty(userProperties.Login) || !this.IsValidName(userProperties.Login))
            {
                this.SendRequest(Commands.RequestDeleteAccountFailed, FailureReasons.InvalidRequest);
            }

            if (this.IsLoggedInAndHasPermission(Commands.RequestDeleteAccountFailed, this.loggedInUser, PermissionTypes.CreateUsers))
            {
                // Try and remove the users permissions first.
                try
                {
                    IList<PermissionProperties> usersPermissions = this.tokenGenerator.AccountManager.GetPermissions(userProperties.Login);
                    foreach (PermissionProperties permission in usersPermissions)
                    {
                        try
                        {
                            this.tokenGenerator.AccountManager.RevokePermission(userProperties.Login, permission.Id);
                        }
                        catch (Exception e)
                        {
                            System.Diagnostics.Trace.TraceError("Caught exception : {0}", e.Message);
                        }
                    }
                }
                catch (Exception e)
                {
                    System.Diagnostics.Trace.TraceError("Caught exception : {0}", e.Message);
                }

                try
                {
                    if (this.tokenGenerator.AccountManager.DeleteAccount(userProperties.Login))
                    {
                        this.SendRequest(Commands.RequestDeleteAccountAccepted);
                        return;
                    }
                }
                catch (Exception e)
                {
                    this.LogException(e);
                }

                this.Fail(Commands.RequestDeleteAccountFailed);
            }
        }

        /// <summary>
        /// OnChangeAccount callback is called when the client request update user details. 
        /// </summary>
        /// <param name="userProperties">User properties that need to be change.</param>
        private void OnChangeAccount(UserProperties userProperties)
        {
            System.Diagnostics.Trace.TraceInformation("client: OnChangeAccount()");

            if (string.IsNullOrEmpty(userProperties.Login) || string.IsNullOrEmpty(userProperties.Password)
                || !this.IsValidName(userProperties.Login))
            {
                this.SendRequest(Commands.RequestChangeAccountRejected, FailureReasons.InvalidRequest);
            }

            if (this.IsLoggedInAndHasPermission(Commands.RequestChangeAccountRejected, userProperties, PermissionTypes.None))
            {
                try
                {
                    if (this.tokenGenerator.AccountManager.ChangePassword(userProperties.Login, userProperties.Password))
                    {
                        this.SendRequest(Commands.RequestChangeAccountAccepted);
                    }
                    else
                    {
                        this.SendRequest(Commands.RequestChangeAccountRejected, FailureReasons.NoneGiven);
                    }
                }
                catch (Exception e)
                {
                    this.HandleException(e, Commands.RequestChangeAccountRejected);
                }
            }
        }

        /// <summary>
        /// OnCreatePermission callback is called when the client request a new permission creation.
        /// </summary>
        /// <param name="permissionProperty">Permission type properties.</param>
        private void OnCreatePermission(PermissionProperties permissionProperty)
        {
            System.Diagnostics.Trace.TraceInformation("client: OnCreatePermission()");

            if (string.IsNullOrEmpty(permissionProperty.Name) || !this.IsValidName(permissionProperty.Name))
            {
                this.SendRequest(Commands.RequestCreatePermissionRejected, FailureReasons.InvalidRequest);
            }

            if (this.IsLoggedInAndHasPermission(Commands.RequestCreatePermissionRejected, this.loggedInUser, PermissionTypes.CreatePermissions))
            {
                try
                {
                    long permissionId = this.tokenGenerator.AccountManager.CreatePermission(permissionProperty.Name);
                    if (permissionId > 0)
                    {
                        permissionProperty.Id = permissionId;
                        this.SendRequest(Commands.RequestCreatePermissionAccepted, permissionProperty);
                    }
                    else
                    {
                        this.SendRequest(Commands.RequestCreatePermissionRejected, FailureReasons.NoneGiven);
                    }
                }
                catch (Exception e)
                {
                    this.HandleException(e, Commands.RequestCreatePermissionRejected);
                }
            }
        }

        /// <summary>
        /// OnRemovePermission callback is called when the client request to remove a permission.
        /// </summary>
        /// <param name="permissionProperty">Permission type properties.</param>
        private void OnRemovePermission(PermissionProperties permissionProperty)
        {
            System.Diagnostics.Trace.TraceInformation("client: OnCreatePermission()");

            if (permissionProperty.Id <= 0)
            {
                this.SendRequest(Commands.RequestRemovePermissionRejected, FailureReasons.InvalidRequest);
            }

            if (this.IsLoggedInAndHasPermission(Commands.RequestRemovePermissionRejected, this.loggedInUser, PermissionTypes.CreatePermissions))
            {
                try
                {
                    if (this.tokenGenerator.AccountManager.DeletePermission(permissionProperty.Id))
                    {
                        this.SendRequest(Commands.RequestRemovePermissionAccepted);
                    }
                    else
                    {
                        this.SendRequest(Commands.RequestRemovePermissionRejected, FailureReasons.NoneGiven);
                    }
                }
                catch (Exception e)
                {
                    this.HandleException(e, Commands.RequestRemovePermissionRejected);
                }
            }
        }

        /// <summary>
        /// OnGrantPermission callback is called when the client request permission to be granted to
        /// a specific user.
        /// </summary>
        /// <param name="userProperties">User properties that will be granted with a permission.</param>
        /// <param name="permissionProperty">Permission type properties.</param>
        private void OnGrantPermission(UserProperties userProperties, PermissionProperties permissionProperty)
        {
            System.Diagnostics.Trace.TraceInformation("client: OnGrantPermission()");

            if (string.IsNullOrEmpty(userProperties.Login) || !this.IsValidName(userProperties.Login) || permissionProperty.Id <= 0)
            {
                this.SendRequest(Commands.RequestGrantPermissionRejected, FailureReasons.InvalidRequest);
            }

            if (this.IsLoggedInAndHasPermission(Commands.RequestGrantPermissionRejected, this.loggedInUser, PermissionTypes.AssignPermissions))
            {
                try
                {
                    if (this.tokenGenerator.AccountManager.GrantPermission(userProperties.Login, permissionProperty.Id))
                    {
                        this.SendRequest(Commands.RequestGrantPermissionAccepted);
                    }
                    else
                    {
                        this.SendRequest(Commands.RequestGrantPermissionRejected, FailureReasons.NoneGiven);
                    }
                }
                catch (Exception e)
                {
                    this.HandleException(e, Commands.RequestGrantPermissionRejected);
                }
            }
        }

        /// <summary>
        /// OnRevokePermission callback is called when the client request revoke a permission from specific
        /// user.
        /// </summary>
        /// <param name="userProperties">User properties.</param>
        /// <param name="permissionProperty">Permission type properties.</param>
        private void OnRevokePermission(UserProperties userProperties, PermissionProperties permissionProperty)
        {
            System.Diagnostics.Trace.TraceInformation("client: OnRevokePermission()");

            if (string.IsNullOrEmpty(userProperties.Login) || !this.IsValidName(userProperties.Login) || permissionProperty.Id <= 0)
            {
                this.SendRequest(Commands.RequestRevokePermissionRejected, FailureReasons.InvalidRequest);
            }

            if (this.IsLoggedInAndHasPermission(Commands.RequestRevokePermissionRejected, this.loggedInUser, PermissionTypes.AssignPermissions))
            {
                try
                {
                    if (this.tokenGenerator.AccountManager.RevokePermission(userProperties.Login, permissionProperty.Id))
                    {
                        this.SendRequest(Commands.RequestRevokePermissionAccepted);
                    }
                    else
                    {
                        this.SendRequest(Commands.RequestRevokePermissionRejected, FailureReasons.NoneGiven);
                    }
                }
                catch (Exception e)
                {
                    this.HandleException(e, Commands.RequestRevokePermissionRejected);
                }
            }
        }

        /// <summary>
        /// OnClientCertificate callback is called when client requesting a signed certificate given the 
        /// unsigned certificate.
        /// </summary>
        /// <param name="unsignedCertificate">Certificate need to be signed.</param>
        private void OnClientCertificate(UserCertificate unsignedCertificate)
        {
            System.Diagnostics.Trace.TraceInformation("client: OnClientCertificate()");

            if (!unsignedCertificate.IsValid())
            {
                this.SendRequest(Commands.RequestClientCertificateFailed, FailureReasons.InvalidRequest);
                return;
            }

            if (this.IsLoggedInAndHasPermission(Commands.RequestClientCertificateFailed, this.loggedInUser, PermissionTypes.Participation))
            {
                try
                {
                    unsignedCertificate.UserId = this.tokenGenerator.AccountManager.GetUserId(this.loggedInUser.Login);
                    if (unsignedCertificate.Character == null || this.tokenGenerator.AccountManager.AuthorizeCharacter(this.loggedInUser.Login, unsignedCertificate.Character))
                    {
                        UserCertificate certificate = this.tokenGenerator.GetSignedCertificates(unsignedCertificate);
                        this.SendRequest(Commands.RequestClientCertificateAccepted, certificate);
                        return;
                    }

                    System.Diagnostics.Trace.TraceInformation(String.Format("Could not authorize user '{0}' as character '{1}'", this.loggedInUser.Login, unsignedCertificate.Character));
                    this.SendRequest(Commands.RequestClientCertificateFailed, FailureReasons.AuthorizationFailed);
                    return;
                }
                catch (Exception e)
                {
                    this.HandleException(e, Commands.RequestClientCertificateFailed);
                    return;
                }
            }
        }

        /// <summary>
        /// OnCloseConnection is called when the client requesting close connection.
        /// </summary>
        /// <param name="userProperties">User properties.</param>
        private void OnCloseConnection(UserProperties userProperties)
        {
            System.Diagnostics.Trace.TraceInformation("client: OnCloseConnection()");
        }

        /// <summary>
        /// OnRequestParticipationKey is called when the client is requesting a participation key.
        /// </summary>
        /// <param name="userProperties">User properties.</param>
        /// <param name="request">Token request</param>
        private void OnRequestParticipationKey(UserProperties userProperties, TokenRequest request)
        {
            System.Diagnostics.Trace.TraceInformation("client: OnRequestParticipationKey()");

            if (string.IsNullOrEmpty(userProperties.Login) || !this.IsValidName(userProperties.Login))
            {
                this.SendRequest(Commands.RequestParticipationKeyRejected, FailureReasons.InvalidRequest);
            }

            if (this.IsLoggedInAndHasPermission(Commands.RequestParticipationKeyRejected, userProperties, PermissionTypes.Participation))
            {
                ParticipationKey key = this.tokenGenerator.GetParticipationKey();
                if (key != null)
                {
                    this.SendRequest(Commands.RequestParticipationKeyAccepted, key);
                }
                else
                {
                    this.SendRequest(Commands.RequestParticipationKeyRejected, FailureReasons.InvalidRequest);
                }
            }
        }

        /// <summary>
        /// OnRequestPublicKey is called when the client requesting a public key.
        /// </summary>
        /// <param name="userProperties">User properties.</param>
        /// <param name="request">Token request.</param>
        private void OnRequestPublicKey(UserProperties userProperties, TokenRequest request)
        {
            System.Diagnostics.Trace.TraceInformation("client: OnRequestPublicKey()");

            if (string.IsNullOrEmpty(userProperties.Login) || !this.IsValidName(userProperties.Login))
            {
                this.SendRequest(Commands.RequestServersPublicKeyRejected, FailureReasons.InvalidRequest);
            }

            if (this.IsLoggedInAndHasPermission(Commands.RequestServersPublicKeyRejected, userProperties, PermissionTypes.Participation))
            {
                Dei.PublicKey key = this.tokenGenerator.GetPublicKeys(request.Number);
                if (key != null)
                {
                    this.SendRequest(Commands.RequestServersPublicKeyAccepted, userProperties, key);
                }
                else
                {
                    this.SendRequest(Commands.RequestServersPublicKeyRejected, FailureReasons.InvalidRequest);
                }
            }
        }

        /// <summary>
        /// OnRequestTime is called when the client requesting the server time.
        /// </summary>
        /// <param name="userProperties">User properties.</param>
        private void OnRequestTime(UserProperties userProperties)
        {
            System.Diagnostics.Trace.TraceInformation("client: OnRequestTime()");

            if (string.IsNullOrEmpty(userProperties.Login) || !this.IsValidName(userProperties.Login))
            {
                this.SendRequest(Commands.RequestServerTimeRejected, FailureReasons.InvalidRequest);
            }

            if (this.IsLoggedInAndHasPermission(Commands.RequestServerTimeRejected, userProperties, PermissionTypes.Participation))
            {
                try
                {
                    ServerTime time = new ServerTime(new UtcDateTime(DateTime.UtcNow), this.tokenGenerator.AccountManager.GetParticipationValidityPeriod());
                    this.SendRequest(Commands.RequestServerTimeAccepted, userProperties, time);
                }
                catch (Exception e)
                {
                    this.HandleException(e, Commands.RequestServerTimeRejected);
                }
            }
        }

        /// <summary>
        /// OnRequestListUsers callback is called when the client is requesting list of users.
        /// </summary>
        /// <param name="request">List user request.</param>
        private void OnRequestListUsers(ListUserRequest request)
        {
            System.Diagnostics.Trace.TraceInformation("client: OnRequestListUsers()");

            if (string.IsNullOrEmpty(request.UsernameGlob) || !this.IsExpressionSafe(request.UsernameGlob))
            {
                this.SendRequest(Commands.RequestUserListFailed, FailureReasons.InvalidRequest);
            }

            if (this.IsLoggedInAndHasPermission(Commands.RequestUserListFailed, this.loggedInUser, PermissionTypes.ViewUsers))
            {
                try
                {
                    // limits are ignored, as not supported by MS SQL Server.
                    List<string> userList = new List<string>(this.tokenGenerator.AccountManager.FindUsers(
                        request.UsernameGlob));

                    userList.RemoveRange(0, (int)request.LimitStart);

                    if (userList.Count > request.NumberOfResults)
                    {
                        userList.RemoveRange((int)request.NumberOfResults, (int)(userList.Count - request.NumberOfResults));
                    }

                    request.Result = userList.ToArray();

                    this.SendRequest(Commands.RequestUserListComplete, request);
                }
                catch (Exception e)
                {
                    this.HandleException(e, Commands.RequestUserListFailed);
                }
            }
        }

        /// <summary>
        /// OnRequestUserCount is called when the client requesting the number of users currently stored
        /// in database.
        /// </summary>
        /// <param name="request">List user request.</param>
        private void OnRequestUserCount(ListUserRequest request)
        {
            System.Diagnostics.Trace.TraceInformation("client: OnRequestUserCount()");

            if (string.IsNullOrEmpty(request.UsernameGlob) || !this.IsExpressionSafe(request.UsernameGlob))
            {
                this.SendRequest(Commands.RequestUserCountFailed, FailureReasons.InvalidRequest);
            }

            if (this.IsLoggedInAndHasPermission(Commands.RequestUserCountFailed, this.loggedInUser, PermissionTypes.ViewUsers))
            {
                try
                {
                    long userCount = this.tokenGenerator.AccountManager.GetUserCount(request.UsernameGlob);

                    request.LimitStart = 0;
                    request.NumberOfResults = userCount;

                    this.SendRequest(Commands.RequestUserCountComplete, request);
                }
                catch (Exception e)
                {
                    this.LogException(e);
                }
            }

            this.Fail(Commands.RequestUserCountFailed);
        }

        /// <summary>
        /// Handle a request for the user's character list.
        /// </summary>
        /// <param name="userProperties">The user properties.</param>
        private void OnRequestCharacterList(UserProperties userProperties)
        {
            if (this.IsLoggedInAndHasPermission(Commands.RequestCharacterListFailed, userProperties, PermissionTypes.Participation))
            {
                try
                {
                    List<Character> characters = new List<Character>(this.tokenGenerator.AccountManager.GetCharacters(userProperties.Login));
                    if (characters != null)
                    {
                        this.SendRequest(Commands.RequestCharacterListComplete, characters);
                        return;
                    }
                }
                catch (Exception e)
                {
                    this.LogException(e);
                }
            }

            this.Fail(Commands.RequestCharacterListFailed);
        }

        /// <summary>
        /// Handle a delete character request.
        /// </summary>
        /// <param name="userProperties">The user properties.</param>
        /// <param name="character">The character.</param>
        private void OnDeleteCharacter(UserProperties userProperties, Character character)
        {
            if (this.IsLoggedInAndHasPermission(Commands.RequestCharacterListFailed, userProperties, PermissionTypes.Participation))
            {
                try
                {
                    if (this.tokenGenerator.AccountManager.DeleteCharacter(userProperties.Login, character))
                    {
                        this.SendRequest(Commands.RequestDeleteCharacterComplete);
                        return;
                    }
                }
                catch (Exception e)
                {
                    this.LogException(e);
                }
            }

            this.Fail(Commands.RequestDeleteCharacterFailed);
        }

        /// <summary>
        /// Handle a create character request.
        /// </summary>
        /// <param name="userProperties">The user properties.</param>
        /// <param name="characterName">Name of the character.</param>
        private void OnCreateCharacter(UserProperties userProperties, string characterName)
        {
            if (this.IsLoggedInAndHasPermission(Commands.RequestCharacterListFailed, userProperties, PermissionTypes.Participation))
            {
                try
                {
                    Character createdCharacter = this.tokenGenerator.AccountManager.CreateCharacter(userProperties.Login, characterName);
                    if (createdCharacter != null)
                    {
                        this.SendRequest(Commands.RequestCreateCharacterComplete, createdCharacter);
                        return;
                    }
                }
                catch (Exception e)
                {
                    this.LogException(e);
                }
            }

            this.Fail(Commands.RequestCreateCharacterFailed);
        }

        /// <summary>
        /// Check whether the current user/client has logged in and has a correct permission.
        /// </summary>
        /// <param name="onFailureReply">On failure reply command.</param>
        /// <param name="userProperties">User propertis.</param>
        /// <param name="requiredPermission">Required permission.</param>
        /// <returns>Return true if the user is valid.</returns>
        private bool IsLoggedInAndHasPermission(Commands onFailureReply, UserProperties userProperties, PermissionTypes requiredPermission)
        {
            try
            {
                if (this.loggedInUser != null && this.loggedInUser.Login == userProperties.Login)
                {
                    if (requiredPermission == PermissionTypes.None
                        || this.tokenGenerator.AccountManager.HasPermission(userProperties.Login, (long)requiredPermission))
                    {
                        return true;
                    }
                    else
                    {
                        System.Diagnostics.Trace.TraceInformation("   User doesn't have permission");
                        this.SendRequest(onFailureReply, FailureReasons.PermissionDenied);
                        return false;
                    }
                }
            }
            catch (Exception e)
            {
                this.HandleException(e, onFailureReply);
                return false;
            }

            System.Diagnostics.Trace.TraceInformation("   User NOT logged in");
            this.SendRequest(onFailureReply, FailureReasons.AuthenticationFailed);
            return false;
        }

        /// <summary>
        /// Check whether the sql query/expression is safe (i.e. not a SQL injection) 
        /// </summary>
        /// <param name="expression">Sql query/exrpession.</param>
        /// <returns>Currently unused, it will return true always.</returns>
        private bool IsExpressionSafe(string expression)
        {
            return true;
        }

        /// <summary>
        /// Check whether the given name is valid.
        /// </summary>
        /// <param name="name">User/permission name</param>
        /// <returns>Always return true.</returns>
        private bool IsValidName(string name) // username or permission name
        {
            return true;
        }

        /// <summary>
        /// Logs an exception.
        /// </summary>
        /// <param name="e">The exception.</param>
        private void LogException(Exception e)
        {
            System.Diagnostics.Trace.TraceError("Caught exception : {0}", e.Message);
        }

        /// <summary>
        /// Send the specified failure response, with reason "FailureReasons.NoneGiven".
        /// </summary>
        /// <param name="response">The response.</param>
        private void Fail(Commands response)
        {
            this.SendRequest(response, FailureReasons.NoneGiven);
        }

        /// <summary>
        /// Handles an unexpected exception by sending FailureReason.NoneGiven as the response.
        /// </summary>
        /// <param name="e">The exception.</param>
        /// <param name="response">The response command.</param>
        private void HandleException(Exception e, Commands response)
        {
            this.LogException(e);
            this.Fail(response);
        }
    }
}
