//-----------------------------------------------------------------------
// <copyright file="UserCertificate.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace Dei
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Security.Cryptography;
    using System.Text;
    using Badumna.Security;
    using Dei.Protocol;
    using Dei.Utilities;

    /// <summary>
    /// User certificate implements IExpirable interface.
    /// </summary>
    [Serializable]
    public class UserCertificate : IExpirable
    {
        /// <summary>
        /// Signature length bytes.
        /// </summary>
        private const int SignatureLength = 128;

        /// <summary>
        /// State length bytes.
        /// </summary>
        private const int StateLength = 148;

        /// <summary>
        /// User certificate id.
        /// </summary>
        private long userId;

        /// <summary>
        /// The authorized Character.
        /// </summary>
        private Character character;

        /// <summary>
        /// Badumna state.
        /// </summary>
        private byte[] badumnaState = new byte[UserCertificate.StateLength];

        /// <summary>
        /// User certificate signature.
        /// </summary>
        private byte[] signature = new byte[UserCertificate.SignatureLength];

        /// <summary>
        /// User certificate time stamp.
        /// </summary>
        private UtcDateTime timeStamp = new UtcDateTime();

        /// <summary>
        /// User certificate expiry time.
        /// </summary>
        private UtcDateTime expiryTime = new UtcDateTime();

        /// <summary>
        /// Initializes a new instance of the <see cref="UserCertificate"/> class.
        /// </summary>
        public UserCertificate()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UserCertificate"/> class.
        /// </summary>
        /// <param name="userCertificate">Usert certificate.</param>
        /// <param name="timeStamp">Time stamp.</param>
        /// <param name="expiryTime">Expiry time.</param>
        public UserCertificate(UserCertificate userCertificate, UtcDateTime timeStamp, UtcDateTime expiryTime)
            : this(userCertificate.UserId, userCertificate.Character, userCertificate.badumnaState, timeStamp, expiryTime)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UserCertificate"/> class.
        /// </summary>
        /// <param name="userId">User certificate id.</param>
        /// <param name="character">The Character.</param>
        /// <param name="badumnaState">Badumna state.</param>
        /// <param name="timeStamp">Time stamp.</param>
        /// <param name="expiryTime">Expiry time.</param>
        private UserCertificate(long userId, Character character, byte[] badumnaState, UtcDateTime timeStamp, UtcDateTime expiryTime)
        {
            if (badumnaState == null)
            {
                throw new ArgumentNullException("badumnaState");
            }

            if (badumnaState.Length != UserCertificate.StateLength)
            {
                throw new ArgumentException("badumnaState.Length != 148");
            }

            if (timeStamp == null)
            {
                throw new ArgumentNullException("timeStamp");
            }

            if (expiryTime == null)
            {
                throw new ArgumentNullException("expiryTime");
            }

            this.userId = userId;
            this.character = character;
            this.badumnaState = badumnaState;
            this.timeStamp = timeStamp;
            this.expiryTime = expiryTime;
        }

        /// <summary>
        /// Gets or sets the user certificate expiry time.
        /// </summary>
        public UtcDateTime ExpiryTime
        {
            get { return this.expiryTime; }
            set { this.expiryTime = value; }
        }

        /// <summary>
        /// Gets a value indicating whether the user certificate has expired.
        /// </summary>
        public bool HasExpired 
        { 
            get { return this.expiryTime.DateTime < DateTime.UtcNow; } 
        }

        /// <summary>
        /// Gets or sets the user id.
        /// </summary>
        public long UserId
        {
            get { return this.userId; }
            set { this.userId = value; }
        }

        /// <summary>
        /// Gets or sets the character name.
        /// </summary>
        public Character Character
        {
            get { return this.character; }
            set { this.character = value; }
        }

        /// <summary>
        /// Gets or sets the user certificate signature.
        /// </summary>
        public byte[] Signature
        {
            get { return this.signature; }
            set { this.signature = value; }
        }

        /// <summary>
        /// Gets or sets the user certificate time stamp.
        /// </summary>
        public UtcDateTime TimeStamp
        {
            get { return this.timeStamp; }
            set { this.timeStamp = value; }
        }

        /// <summary>
        /// Gets or sets the badumna state.
        /// </summary>
        public byte[] BadumnaState
        {
            get { return this.badumnaState; }
            set { this.badumnaState = value; }
        }

        /// <summary>
        /// Create unsigned client certificate.
        /// </summary>
        /// <param name="character">The Character.</param>
        /// <param name="additionalData">Additional data.</param>
        /// <returns>Return unsigned user certificate.</returns>
        public static UserCertificate CreateUnsignedClientCertificate(Character character, byte[] additionalData)
        {
            if (additionalData == null)
            {
                throw new ArgumentNullException("additionalData");
            }

            return new UserCertificate(0, character, additionalData, new UtcDateTime(), new UtcDateTime());
        }

        /// <summary>
        /// Sign the user certificate with given private key.
        /// </summary>
        /// <param name="privateKey">Private key.</param>
        /// <returns>Signature of user certificate.</returns>
        public byte[] Sign(RSACryptoServiceProvider privateKey)
        {
            return CertificateToken.SignCertificate(this.UserId, this.character, this.TimeStamp.DateTime, this.expiryTime.DateTime, this.badumnaState, privateKey.ExportCspBlob(true));
        }

        /// <summary>
        /// A value indicating whether the user certificate is valid.
        /// </summary>
        /// <returns>Return true if this user certificate is valid.</returns>
        internal bool IsValid()
        {
            // note that `null` is a valid character, but if you *have* a character it must be valid
            bool invalidCharacter = character == null ? false : !character.IsValid;

            return !(invalidCharacter || this.UserId < 0 || this.badumnaState == null);
        }
    }
}
