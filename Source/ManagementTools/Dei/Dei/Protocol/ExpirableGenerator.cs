//-----------------------------------------------------------------------
// <copyright file="ExpirableGenerator.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace DeiServer
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Text;
    using System.Xml;
    using System.Xml.Serialization;
    using Dei;
    using Dei.Protocol;

    /// <summary>
    /// This class maintains a set of IExpirable items so that there is at least one valid item
    /// at any one time. It does this by generating new items (with a fixed given validity period)
    /// when an item is older than half its validity period.
    /// </summary>
    /// <typeparam name="ExpirableType">Expirable type.</typeparam>
    internal abstract class ExpirableGenerator<ExpirableType> where ExpirableType : IExpirable
    {
        /// <summary>
        /// Maximum number of ExpirableType instances that allowed.
        /// </summary>
        private const int NumberOfInstances = 2;

        /// <summary>
        /// Random number source.
        /// </summary>
        private static Random randomSource = new Random((int)DateTime.Now.Ticks);

        /// <summary>
        /// ExpirableType validity period.
        /// </summary>
        private TimeSpan validityPeriod;

        /// <summary>
        /// Collections of expirable type.
        /// </summary>
        private List<ExpirableType> expirables = new List<ExpirableType>();

        /// <summary>
        /// Initializes a new instance of the ExpirableGenerator class.
        /// </summary>
        /// <param name="validityPeriod">Validity period of ExpirableType.</param>
        public ExpirableGenerator(TimeSpan validityPeriod)
        {
            this.validityPeriod = validityPeriod;
        }

        /// <summary>
        /// Gets the random number source.
        /// </summary>
        protected static Random RandomSource
        {
            get { return ExpirableGenerator<ExpirableType>.randomSource; }
        }

        /// <summary>
        /// Get the current instances of ExpirableType.
        /// </summary>
        /// <returns>Return the curren instances of ExpirableType.</returns>
        public List<ExpirableType> GetCurrentInstances()
        {
            //// Remove old instances
            while (this.expirables.Count > 0 && this.expirables[0].HasExpired)
            {
                this.expirables.RemoveAt(0);
            }

            //// Create new instances if no instances are available or the current instance is half expired already
            while (this.expirables.Count < ExpirableGenerator<ExpirableType>.NumberOfInstances &&
                (this.expirables.Count == 0 || this.IsHalfwayExpired(this.expirables[0])))
            {
                try
                {
                    ExpirableType expirable = this.Generate(this.validityPeriod);
                    if (expirable != null)
                    {
                        this.expirables.Add(expirable);
                        this.Save();
                    }
                    else
                    {
                        System.Diagnostics.Trace.TraceError("Failed to generate expirable of type {0}", typeof(ExpirableType).Name);
                        break;
                    }
                }
                catch (Exception e)
                {
                    System.Diagnostics.Trace.TraceError("Exception thrown whilst attempting to generate expirable : {0}", e);
                    break;
                }
            }

            return this.expirables;
        }

        /// <summary>
        /// Generate a new ExpirableType based on the validity period.
        /// </summary>
        /// <param name="validityPeriod">Validity period for ExpirableType.</param>
        /// <returns>Returns a new ExpirableType instance.</returns>
        protected abstract ExpirableType Generate(TimeSpan validityPeriod);

        /// <summary>
        /// Save the ExpirableTypes to xml file.
        /// </summary>
        protected virtual void Save()
        { 
        }

        /// <summary>
        /// Save the ExpirableTypes to xml file.
        /// </summary>
        /// <param name="stream">The file stream where the data will be save into.</param>
        /// <returns>Return true on success.</returns>
        protected bool Save(Stream stream)
        {
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(ExpirableType));
                XmlSerializerNamespaces expirableNamespaces = new XmlSerializerNamespaces(
                    new XmlQualifiedName[] { new XmlQualifiedName(string.Empty, string.Empty) });

                XmlWriterSettings setting = new XmlWriterSettings();
                setting.CloseOutput = false;
                using (XmlWriter writer = XmlWriter.Create(stream, setting))
                {
                    writer.WriteStartElement(string.Empty, "Expirables", string.Empty);
                    foreach (ExpirableType expirable in this.expirables)
                    {
                        serializer.Serialize(writer, expirable, expirableNamespaces);
                    }

                    writer.WriteEndElement();
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Trace.TraceError("Exception while trying to save expirables : {0}", e.Message);
            }

            return false;
        }

        /// <summary>
        /// Load the ExpirableType from a file stream.
        /// </summary>
        /// <param name="stream">File stream need to be read.</param>
        /// <returns>Return true on success.</returns>
        protected bool Load(Stream stream)
        {
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(ExpirableType));

                using (XmlReader reader = XmlReader.Create(stream))
                {
                    if (reader.ReadToDescendant("Expirables", string.Empty))
                    {
                        using (XmlReader expirableReader = reader.ReadSubtree())
                        {
                            while (reader.Read() && serializer.CanDeserialize(reader))
                            {
                                ExpirableType expirable = (ExpirableType)serializer.Deserialize(reader);

                                // Set the validity period of the loaded expirable to be no greater than the
                                // currently set validity period. This prevents saved expirables with large validity periods
                                // preventing new expirabled from being created if the validity period is changed.
                                if (expirable.ExpiryTime.DateTime > DateTime.UtcNow + this.validityPeriod)
                                {
                                    expirable.ExpiryTime = new UtcDateTime(DateTime.UtcNow + this.validityPeriod);
                                }

                                if (!expirable.HasExpired)
                                {
                                    this.expirables.Add(expirable);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Trace.TraceError("Exception while trying to load expirables : {0}", e.Message);
            }

            return false;
        }

        /// <summary>
        /// Check whether the Expirable type is halfway expired.
        /// </summary>
        /// <param name="expirable">Expirable type</param>
        /// <returns>Return true if it expirable type halfway to expired.</returns>
        private bool IsHalfwayExpired(IExpirable expirable)
        {
            return expirable.ExpiryTime.DateTime < DateTime.UtcNow + new TimeSpan(this.validityPeriod.Ticks / 2);
        }
    }
}
