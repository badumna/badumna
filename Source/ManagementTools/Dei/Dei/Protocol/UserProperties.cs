//-----------------------------------------------------------------------
// <copyright file="UserProperties.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace Dei
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    using System.Text;

    /// <summary>
    /// The UserProperties class holds information about a user.
    /// </summary>
    [Serializable]
    public class UserProperties
    {
        /// <summary>
        /// Minimum password length.
        /// </summary>
        public const int MinPasswordLength = 8;

        /// <summary>
        /// Maximum password length.
        /// </summary>
        public const int MaxPasswordLength = 255;
        
        /// <summary>
        /// Minimum login length.
        /// </summary>
        public const int MinLoginLength = 0;

        /// <summary>
        /// Maximum login length.
        /// </summary>
        public const int MaxLoginLength = 255;

        /// <summary>
        /// User login name.
        /// </summary>
        private string login;

        /// <summary>
        /// User password.
        /// </summary>
        private string password;

        /// <summary>
        /// Initializes a new instance of the <see cref="UserProperties"/> class.
        /// </summary>
        public UserProperties() 
        { 
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UserProperties"/> class.
        /// </summary>
        /// <param name="login">User login name.</param>
        /// <param name="password">User password.</param>
        public UserProperties(string login, string password)
        {
            if (this.AllowedLoginSymbols(login) && this.AllowedPasswordSymbols(password))
            {
                this.login = login;
                this.password = password;
            }
            else
            {
                throw new ArgumentException("Not an allowed login and password combination");
            }
        }

        /// <summary>
        /// Gets or sets the user login name.
        /// </summary>
        public string Login
        {
            get
            {
                return this.login;
            }

            set
            {
                if (this.AllowedLoginSymbols(value))
                {
                    this.login = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets the user password.
        /// </summary>
        public string Password
        {
            get
            {
                return this.password;
            }

            set
            {
                if (this.AllowedPasswordSymbols(value))
                {
                    this.password = value;
                }
            }
        }

        /// <summary>
        /// Check whether the user login name has a valid length.
        /// </summary>
        /// <param name="login">User login name.</param>
        /// <returns>Return true of the login name has valid length.</returns>
        private bool AllowedLoginSymbols(string login)
        {
            if (login == null)
            {
                throw new ArgumentNullException("login");
            }

            if (login.Length >= UserProperties.MinLoginLength && login.Length <= UserProperties.MaxLoginLength)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Check whether the user password has a valid length.
        /// </summary>
        /// <param name="password">User password.</param>
        /// <returns>Return true if the password has valid length.</returns>
        private bool AllowedPasswordSymbols(string password)
        {
            if (password == null)
            {
                throw new ArgumentNullException("password");
            }

            if (password.Length >= UserProperties.MaxPasswordLength)
            {
                return false;
            }

            return true;
        }
    }
}
