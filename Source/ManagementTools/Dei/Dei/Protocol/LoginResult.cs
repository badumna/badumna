//-----------------------------------------------------------------------
// <copyright file="LoginResult.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using Badumna.Utilities;
using Dei.Protocol;
using System.Collections.ObjectModel;
using Badumna.Security;
using System.Xml.Serialization;
using System.Xml;
using System.Xml.Schema;

namespace Dei
{
    /// <summary>
    /// A callback that provides notification of the progress of the login process and allows for its cancellation.
    /// </summary>
    /// <param name="stage">The stage of the process</param>
    /// <param name="description">A human readable description of the stage</param>
    /// <param name="percentageComplete">The percentage (0-100) of completion of the process</param>
    /// <returns>False indicates that the user has requested to cancel the login process, all additional stages will be abandoned and 
    /// the login process will fail.</returns>
    public delegate bool LoginProgressNotification(LoginStage stage, string description, int percentageComplete);

    /// <summary>
    /// The various stages of the login process.
    /// </summary>
    public enum LoginStage
    {
        /// <summary>
        /// The login process is initializing
        /// </summary>
        Initializing,

        /// <summary>
        /// Synchronizing time with the server.
        /// </summary>
        ClockSynchronization,

        /// <summary>
        /// Obtaining a network participation token from the identity provider.
        /// </summary>
        NetworkParticipationToken,

        /// <summary>
        /// Obtaining the trusted authority token.
        /// </summary>
        TrustedAuthorityToken,

        /// <summary>
        /// Obtaining the user's permissions.
        /// </summary>
        PermissionListToken,

        /// <summary>
        /// Waiting for STUN to complete.
        /// </summary>
        Stun,

        /// <summary>
        /// Obtaining the user's certificate token.
        /// </summary>
        UserCertificateToken,

        /// <summary>
        /// Initializing the badumna protocol stack.
        /// </summary>
        ProtocolInitialization,

        /// <summary>
        /// The login process is complete.
        /// </summary>
        Complete
    }

    /// <summary>
    /// A sumary of the result of the login process.
    /// </summary>
    public sealed class LoginResult : IEquatable<LoginResult>
    {
        /// <summary>
        /// User cancelled the login result.
        /// </summary>
        private static LoginResult userCancelled = new LoginResult(DescriptionResources.UserCancelledLoginResult);

        /// <summary>
        /// Initializes a new instance of the <see cref="LoginResult"/> class.
        /// Not indended to be called by user code, used only for deserialization.
        /// </summary>
        public LoginResult()
        {
        }

        private LoginResult(string errorDescription, bool wasSuccessful, List<Character> characters)
        {
            this.Init(errorDescription, wasSuccessful, characters);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LoginResult"/> class.
        /// </summary>
        /// <param name="failureDescription">Failure description.</param>
        internal LoginResult(string failureDescription)
            : this(failureDescription, false, null)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LoginResult"/> class.
        /// </summary>
        internal LoginResult(IEnumerable<Character> characters)
            : this("None", true, new List<Character>(characters))
        {
            Debug.Assert(characters != null);
        }

        /// <summary>
        /// Gets a value indicating whether the login operation is completed succesfully.
        /// </summary>
        /// <remarks>This property should never be set manually, it is used by XML Deserialization</remarks>
        public bool WasSuccessful
        {
            get; set;
        }

        /// <summary>
        /// Gets the description of the error that occured.
        /// </summary>
        /// <remarks>This property should never be set manually, it is used by XML Deserialization</remarks>
        public string ErrorDescription
        {
            get; set;
        }

        /// <summary>
        /// Gets a list of this user's available characters.
        /// </summary>
        /// <remarks>This property should never be set manually, it is used by XML Deserialization</remarks>
        public List<Character> Characters
        {
            get; set;
        }

        /// <summary>
        /// Gets User cancelled the login result.
        /// </summary>
        internal static LoginResult UserCancelled
        {
            get { return LoginResult.userCancelled; }
        }

        /// <inheritdoc/>
        public bool Equals(LoginResult other)
        {
            return other != null
                && CollectionUtils.SequenceEqual(other.Characters, this.Characters)
                && other.ErrorDescription == this.ErrorDescription
                && other.WasSuccessful == this.WasSuccessful;
        }

        /// <summary>
        /// Determines whether the specified <see cref="System.Object"/> is equal to this instance.
        /// </summary>
        /// <param name="other">The <see cref="System.Object"/> to compare with this instance.</param>
        public override bool Equals(object other)
        {
            return Object.ReferenceEquals(other, this) || this.Equals(other as LoginResult);
        }

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// It's not a very good hash code, but we should rarely need to hash these objects.
        public override int GetHashCode()
        {
            return this.WasSuccessful.GetHashCode();
        }

        /// <summary>
        /// Does the work of the canonical constructor,
        /// extracted into a method so that XML serialization can share it.
        /// </summary>
        private void Init(string errorDescription, bool wasSuccessful, List<Character> characters)
        {
            this.ErrorDescription = errorDescription;
            this.WasSuccessful = wasSuccessful;
            this.Characters = characters;
        }
    }
}
