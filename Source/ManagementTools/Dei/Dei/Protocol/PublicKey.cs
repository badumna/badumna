//-----------------------------------------------------------------------
// <copyright file="PublicKey.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace Dei
{
    using System;
    using System.Collections.Generic;
    using System.Security.Cryptography;
    using System.Text;
    using System.Xml;
    using Badumna.Security;
    using Dei.Protocol;

    /// <summary>
    /// Dei public key implements IExpirable interface.
    /// </summary>
    [Serializable]
    public class PublicKey : IExpirable
    {
        /// <summary>
        /// Key length bits.
        /// </summary>
        private const int KeyLengthBits = 1024;

        /// <summary>
        /// Public key.
        /// </summary>
        private byte[] key = new byte[PublicKey.KeyLengthBits / 8];

        /// <summary>
        /// Public key expiry time.
        /// </summary>
        private UtcDateTime expiryTime = new UtcDateTime();

        /// <summary>
        /// Initializes a new instance of the <see cref="PublicKey"/> class.
        /// </summary>
        public PublicKey()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PublicKey"/> class.
        /// </summary>
        /// <param name="validityPeriod">Public key validity period.</param>
        /// <param name="key">Public key.</param>
        public PublicKey(TimeSpan validityPeriod, byte[] key)
        {
            if (key == null)
            {
                throw new ArgumentNullException("key");
            }

            this.key = key;

            if (validityPeriod == TimeSpan.MaxValue)
            {
                this.expiryTime = new UtcDateTime(new DateTime(DateTime.MaxValue.Ticks, DateTimeKind.Utc));
            }
            else
            {
                this.expiryTime = new UtcDateTime(DateTime.UtcNow + validityPeriod);
            }
        }

        /// <summary>
        /// Gets or sets public key.
        /// </summary>
        public byte[] Key 
        { 
            get { return this.key; }
            set { this.key = value; }
        }

        /// <summary>
        /// Gets or sets the public key expiry time.
        /// </summary>
        public UtcDateTime ExpiryTime 
        { 
            get { return this.expiryTime; }
            set { this.expiryTime = value; }
        }

        /// <summary>
        /// Gets a value indicating whether the public key has expired.
        /// </summary>
        public bool HasExpired 
        { 
            get { return this.expiryTime.DateTime < DateTime.UtcNow; } 
        }
    }  
}
