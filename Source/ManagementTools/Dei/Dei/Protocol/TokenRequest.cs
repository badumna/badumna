//-----------------------------------------------------------------------
// <copyright file="TokenRequest.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace Dei.Protocol
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// TokenRequest class hold the request token number.
    /// </summary>
    [Serializable]
    public class TokenRequest
    {
        /// <summary>
        /// Token request number.
        /// </summary>
        private int number;

        /// <summary>
        /// Initializes a new instance of the <see cref="TokenRequest"/> class.
        /// </summary>
        public TokenRequest()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TokenRequest"/> class.
        /// </summary>
        /// <param name="tokenNumber">Token number.</param>
        public TokenRequest(int tokenNumber)
        {
            this.number = tokenNumber;
        }

        /// <summary>
        /// Gets or sets the token request number.
        /// </summary>
        public int Number
        {
            get { return this.number; }
            set { this.number = value; }
        }
    }
}
