//-----------------------------------------------------------------------
// <copyright file="BaseProtocol.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace Dei.Protocol
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Text;
    using System.Threading;
    using System.Xml;
    using System.Xml.Schema;
    using Dei.Utilities;
    using Badumna.Utilities;
    using System.Diagnostics;
    using Badumna;
    using Badumna.Utilities.Logging;

    /// <summary>
    /// Abstract base protocol class has responsibility for sending and processing request both 
    /// from server and client side.
    /// </summary>
    internal abstract class BaseProtocol
    {
        /// <summary>
        /// Protocol version number
        /// </summary>
        private readonly Version version = new Version(2, 3);

        /// <summary>
        /// Gets the Failure reason.
        /// </summary>
        public FailureReasons FailureReason { get; private set; }
        
        /// <summary>
        /// Underlying stream used.
        /// </summary>
        private Stream stream;

        /// <summary>
        /// Thread used by base protocol to listen the incoming messages.
        /// </summary>
        private Thread thread;

        /// <summary>
        /// Xml writer.
        /// </summary>
        private XmlWriter writer;

        /// <summary>
        /// Xml protocol schema
        /// </summary>
        private XmlSchema protocolSchema;

        /// <summary>
        /// Collections of SerializerWrapper
        /// </summary>
        private Dictionary<Type, ISerializerWrapper> serializers = new Dictionary<Type, ISerializerWrapper>();

        /// <summary>
        /// A value indicating whether it still reading from stream.
        /// </summary>
        private volatile bool isReading;

        /// <summary>
        /// A value indicating whether the conversation has been initiated.
        /// </summary>
        private bool isInitiated = false;

        /// <summary>
        /// Serializer wrapper factory.
        /// </summary>
        private ISerializerWrapperFactory serializerWrapperFactory;

        /// <summary>
        /// XmlReader factory.
        /// </summary>
        private IXmlReaderFactory xmlReaderFactory;

        /// <summary>
        /// The last request sent (used for context in error reporting)
        /// </summary>
        protected Commands ActiveRequest { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="BaseProtocol"/> class.
        /// </summary>
        /// <param name="stream">Stream used.</param>
        /// <param name="xmlWriterFactory">Xml writer factory.</param>
        /// <param name="xmlReaderFactory">Xml reader factory.</param>
        /// <param name="serializerWrapperFactory">Serializer wrapper factory.</param>
        public BaseProtocol(
            Stream stream,
            IXmlWriterFactory xmlWriterFactory,
            IXmlReaderFactory xmlReaderFactory,
            ISerializerWrapperFactory serializerWrapperFactory)
        {
            this.stream = stream;
            this.serializerWrapperFactory = serializerWrapperFactory;

            XmlWriterSettings writerSettings = new XmlWriterSettings();
            writerSettings.ConformanceLevel = ConformanceLevel.Fragment;
            this.writer = xmlWriterFactory.Create(stream, writerSettings);

            this.protocolSchema = XmlSchema.Read(new StringReader(Resources.DeiProtocol), this.ValidationEventHandler);
            this.xmlReaderFactory = xmlReaderFactory;
        }

        /// <summary>
        /// Gets a value indicating whether it still reading from stream.
        /// </summary>
        public bool IsClosed
        {
            get { return !this.isReading; }
        }

        /// <summary>
        /// Gets the underlying stream used.
        /// </summary>
        protected Stream Stream
        {
            get { return this.stream; }
        }

        /// <summary>
        /// Gets serializer wrapper factory.
        /// </summary>
        protected ISerializerWrapperFactory SerializerWrapperFactory
        {
            get { return this.serializerWrapperFactory; }
        }

        protected Commands LastRequest
        {
            get { return this.ActiveRequest; }
        }

        /// <summary>
        /// Start reading from xml reader by creating a new thread.
        /// </summary>
        public void Start()
        {
            this.thread = new Thread(this.RunLoop);
            this.thread.IsBackground = true;
            this.thread.Start();
        }

        /// <summary>
        /// Initiate conversation with server or client side.
        /// </summary>
        public void InitiateConversation()
        {
            this.isInitiated = true;
            Logger.TraceInformation(LogTag.Event | LogTag.Crypto | LogTag.Connection, "Initiating conversation");
            this.writer.WriteStartElement(Resources.ConversationElementName, Resources.ProtocolNameSpace);

            try
            {
                this.GetSerializer(typeof(Version)).WriteObject(this.writer, this.version);
                this.writer.Flush();
            }
            catch (InvalidOperationException e)
            {
                this.ProtocolError(String.Format("Failed to send version : {0}", e.Message));
            }
        }

        /// <summary>
        /// Close connection with server or client side.
        /// </summary>
        public void CloseConnection()
        {
            try
            {
                Logger.TraceInformation(LogTag.Event | LogTag.Crypto | LogTag.Connection, "BaseProtocol: Closing connection");
                this.isReading = false;
                this.writer.WriteFullEndElement();
                this.writer.Flush();
            }
            catch (InvalidOperationException)
            {
                // Token EndElement in state TopLevel would result in an invalid XML document.
            }
        }

        /// <summary>
        /// Send a request to server or client side.
        /// </summary>
        /// <param name="command">Command type.</param>
        /// <param name="parameters">Parameters need to be sent with the request.</param>
        public void SendRequest(Commands command, params object[] parameters)
        {
            this.FailureReason = FailureReasons.NoneGiven;
            this.ActiveRequest = command;
            ISerializerWrapper commandSerializer = this.GetSerializer(typeof(Commands));

            if (this.stream == null)
            {
                this.SetFailureReason("SendRequest", FailureReasons.ConnectionError);
                return;
            }

            lock (this.stream)
            {
                try
                {
                    // Add an element around the parameter. Delimits it so the reader returns immediately
                    this.writer.WriteStartElement("x");
                    Logger.TraceInformation(LogTag.Event | LogTag.Crypto, "Sending command: {0}", command);
                    commandSerializer.WriteObject(this.writer, command);

                    foreach (object parameter in parameters)
                    {
                        Logger.TraceInformation(LogTag.Event | LogTag.Crypto, "Serializing param {0} ({1})", parameter, parameter.GetType());
                        ISerializerWrapper serializer = this.GetSerializer(parameter.GetType());
                        serializer.WriteObject(this.writer, parameter);
                    }

                    this.writer.WriteEndElement();
                    this.writer.Flush();
                    Logger.TraceInformation(LogTag.Event | LogTag.Crypto, "Command sent.");
                }
                catch (InvalidOperationException e)
                {
                    Logger.TraceException(LogLevel.Error, LogTag.Event | LogTag.Connection, e, "Failed to send command {0}: ", command);
                    this.ProtocolError(String.Format("Failed to send command : {0}", e.Message));
                }
                catch (IOException e)
                {
                    Logger.TraceException(LogLevel.Error, LogTag.Event | LogTag.Connection, e, "IO Error: ");
                    this.ProtocolError("IO error.");
                }
            }
        }

        /// <summary>
        /// Get parameter
        /// </summary>
        /// <typeparam name="T">Return type.</typeparam>
        /// <param name="reader">Xml reader.</param>
        /// <returns>Return object with type T.</returns>
        public T GetParameter<T>(XmlReader reader)
        {
            ISerializerWrapper serializer = this.GetSerializer(typeof(T));

            while (!serializer.CanDeserialize(reader))
            {
                if (!reader.Read())
                {
                    this.ProtocolError(String.Format("Failed to read {0} paramter.", typeof(T)));
                    return default(T);
                }

                // End of element
                if (reader.Name == Resources.ConversationElementName) 
                {
                    Logger.TraceInformation(LogTag.Event | LogTag.Crypto | LogTag.Connection, "Conversation end tag received - closing stream.");
                    this.CloseConnection();
                    this.stream.Close();
                    this.stream = null;
                    return default(T);
                }
            }

            return (T)serializer.ReadObject(reader);
        }

        /// <summary>
        /// Abstract process command method.
        /// </summary>
        /// <param name="command">Command type.</param>
        /// <param name="reader">Xml reader.</param>
        protected abstract void ProcessCommand(Commands command, XmlReader reader);

        /// <summary>
        /// Virtual protocol version error.
        /// </summary>
        /// <param name="myVersion">My version number.</param>
        /// <param name="theirVersion">Their version number.</param>
        protected virtual void ProtocolVersionError(Version myVersion, Version theirVersion)
        {
        }

        /// <summary>
        /// Virtual on closure.
        /// </summary>
        protected virtual void OnClosure()
        {
        }

        /// <summary>
        /// Virtual protocol error.
        /// </summary>
        /// <param name="error">Error message.</param>
        /// <param name="args">Error arguments.</param>
        protected virtual void ProtocolError(string error, params object[] args)
        {
            System.Diagnostics.Trace.TraceError(error, args);
        }

        /// <summary>
        /// Clears the failure reason.
        /// </summary>
        protected void ClearFailureReason()
        {
            this.FailureReason = FailureReasons.NoneGiven;
        }

        /// <summary>
        /// Sets the failure reason.
        /// </summary>
        /// <param name="methodName">Name of the method.</param>
        /// <param name="reason">The reason.</param>
        protected void SetFailureReason(string methodName, FailureReasons reason)
        {
            System.Diagnostics.Trace.TraceInformation("Dei protocol: {0}() {1}", methodName, reason);
            this.FailureReason = reason;
            this.ActiveRequest = Commands.None;
        }

        /// <summary>
        /// Get serializer wrapper with a given type.
        /// </summary>
        /// <param name="type">Given type.</param>
        /// <returns>Return serializer wrapper.</returns>
        protected ISerializerWrapper GetSerializer(Type type)
        {
            ISerializerWrapper serializer = null;

            lock (this.serializers)
            {
                if (!this.serializers.TryGetValue(type, out serializer))
                {
                    serializer = this.serializerWrapperFactory.Create(type);
                    this.serializers.Add(type, serializer);
                }
            }

            return serializer;
        }

        /// <summary>
        /// Check the protocol version number.
        /// </summary>
        /// <param name="reader">Xml reader.</param>
        /// <returns>Return true if version number is matched.</returns>
        private bool CheckVersion(XmlReader reader)
        {
            Version version = this.GetParameter<Version>(reader);

            if (version.Major != this.version.Major || version.Minor != this.version.Minor)
            {
                this.ProtocolVersionError(this.version, version);
                return false;
            }

            return true;
        }

        /// <summary>
        /// Run loop wait for incoming message to be read.
        /// </summary>
        private void RunLoop()
        {
            if (this.stream == null)
            {
                throw new InvalidOperationException("RunLoop() called on a null stream!");
            }

            XmlReaderSettings readerSettings = new XmlReaderSettings();
            readerSettings.Schemas.Add(this.protocolSchema);
            readerSettings.ValidationType = ValidationType.Schema;
            readerSettings.ValidationFlags |= XmlSchemaValidationFlags.ProcessInlineSchema;
            readerSettings.ValidationFlags |= XmlSchemaValidationFlags.ReportValidationWarnings;
            readerSettings.ValidationEventHandler += this.ValidationEventHandler;
                
            try
            {
                XmlReader reader = this.xmlReaderFactory.Create(this.stream, readerSettings);

                // Check that the first element read is a conversation start element.
                // If so reply by sending an equivalent conversation element. 
                if (reader.Read())
                {
                    if (reader.Name == Resources.ConversationElementName)
                    {
                        if (!this.isInitiated)
                        {
                            this.InitiateConversation();
                        }
                        Logger.TraceInformation(LogTag.Event | LogTag.Crypto | LogTag.Connection, "Conversation started");
                    }
                    else
                    {
                        this.ProtocolError("Failed to initialize conversation.");
                        return;
                    }
                }
                else
                {
                    this.ProtocolError("Failed to initialize conversation.");
                    return;
                }

                if (!this.CheckVersion(reader))
                {
                    Logger.TraceInformation(LogTag.Event | LogTag.Crypto | LogTag.Connection, "Version number does not match");
                    this.CloseConnection();
                    return;
                }

                this.isReading = true;

                // Process commands until the conversation has ended.
                while (this.isReading && !reader.EOF)
                {
                    Commands command = this.GetParameter<Commands>(reader);
                    Logger.TraceInformation(LogTag.Event | LogTag.Crypto | LogTag.Connection, "Received command: {0}", command);

                    if (command != default(Commands))
                    {
                        this.ProcessCommand(command, reader);
                    }
                    else
                    {
                        return; // The connection has failed or ended.
                    }
                }
            }
            catch (IOException e)
            {
                Logger.TraceException(LogLevel.Error, e, "Failed to process command:");
                this.ProtocolError(String.Format("Failed to process command : {0}", e.Message));
            }
            catch (Exception e)
            {
                this.ProtocolError(String.Format("Protocol Error : {0}", e.Message));
            }
            this.isReading = false;
        }

        /// <summary>
        /// Validation event handler.
        /// </summary>
        /// <param name="sender">Object sender.</param>
        /// <param name="e">Validation event arguments</param>
        private void ValidationEventHandler(object sender, ValidationEventArgs e)
        {
            System.Diagnostics.Trace.TraceWarning(e.Message);

            if (e.Severity == XmlSeverityType.Error)
            {
                this.ProtocolError("Fatal protocol error. Closing connection");
                this.CloseConnection();
            }
        }
    }
}
