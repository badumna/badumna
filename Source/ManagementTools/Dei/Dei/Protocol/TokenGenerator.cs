//-----------------------------------------------------------------------
// <copyright file="TokenGenerator.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace DeiServer
{
    using System;
    using System.Collections.Generic;
    using System.Security.Cryptography;
    using System.Text;
    using Dei;
    using Dei.Protocol;
    using Dei.Utilities;

    /// <summary>
    /// TokenGenerator class has responsibility to generate token, get signed certificate,
    /// generate new key pair, get the participation key, and get the public key.
    /// </summary>
    internal class TokenGenerator : IDisposable
    {
        /// <summary>
        /// Provider rsa full index.
        /// </summary>
        private const int ProviderRsaFull = 1;

        /// <summary>
        /// The key store name.
        /// </summary>
        private const string KeyStoreName = @"Dei_KeyStore";

        /// <summary>
        /// Default admin password.
        /// </summary>
        private const string DefaultAdminPassword = "admin_password";

        /// <summary>
        /// The database account manager.
        /// </summary>
        private IAccountManager accountManager;

        /// <summary>
        /// The validity period for the participation key.
        /// </summary>
        private TimeSpan keyValidityPeriod;

        /// <summary>
        /// Participation key generator.
        /// </summary>
        private ParticipationKeyGenerator participationKeyGenerator;

        /// <summary>
        /// Initializes a new instance of the <see cref="TokenGenerator"/> class.
        /// </summary>
        /// <param name="accountManager">Database account manager.</param>
        public TokenGenerator(IAccountManager accountManager)
        {
            this.accountManager = accountManager;
            this.keyValidityPeriod = accountManager.GetParticipationValidityPeriod();
            this.participationKeyGenerator = new ParticipationKeyGenerator();

            this.InitializeAdministratorUserAndPermissions();
        }

        /// <summary>
        /// Gets the database account manager.
        /// </summary>
        public IAccountManager AccountManager
        {
            get { return this.accountManager; }
        }

        /// <summary>
        /// Generate a new rsa key pair.
        /// </summary>
        public static void GenerateNewKeyPair()
        {
            try
            {
                ParticipationKeyGenerator.ClearPersistedKeys();
                CspParameters cspParameters = new CspParameters(TokenGenerator.ProviderRsaFull);
                cspParameters.KeyContainerName = TokenGenerator.KeyStoreName;

                RSACryptoServiceProvider rsa = new RSACryptoServiceProvider(RsaAlgorithm.KeyLength, cspParameters);

                // Remove any keys present from the store.
                rsa.PersistKeyInCsp = false;
                rsa.Clear();
            }
            catch (CryptographicException e)
            {
                System.Diagnostics.Trace.TraceError("Exception while trying to generate new key pair : {0}", e.Message);
            }
        }

        /// <summary>
        /// Return the signature for the specified hash value by encrypting it with the private key.
        /// </summary>
        /// <param name="content">Hash value.</param>
        /// <returns>Return the signature for the specifiec hash value.</returns>
        public byte[] Sign(byte[] content)
        {
            try
            {
                using (RSACryptoServiceProvider rsa = new RSACryptoServiceProvider(RsaAlgorithm.KeyLength, TokenGenerator.GetCspParameters()))
                {
                    SHA1Managed sha = new SHA1Managed();

                    byte[] hash = sha.ComputeHash(content);
                    byte[] signiture = rsa.SignHash(hash, CryptoConfig.MapNameToOID("SHA1"));
                    rsa.Clear();
                    return signiture;
                }
            }
            catch (CryptographicException e)
            {
                System.Diagnostics.Trace.TraceError("Exception while trying to sign certificate : {0}", e.Message);
            }

            return null;
        }

        /// <summary>
        /// Get the signed certificate for given unsigned certificate.
        /// </summary>
        /// <param name="unsignedCertificate">Unsigned certificate.</param>
        /// <returns>Return the signed certificate.</returns>
        public UserCertificate GetSignedCertificates(UserCertificate unsignedCertificate)
        {
            DateTime now = DateTime.UtcNow;
            UserCertificate signedCertificate = new UserCertificate(
                unsignedCertificate,
                new UtcDateTime(now),
                new UtcDateTime(now + this.accountManager.GetParticipationValidityPeriod()));

            try
            {
                using (RSACryptoServiceProvider rsa = new RSACryptoServiceProvider(RsaAlgorithm.KeyLength, TokenGenerator.GetCspParameters()))
                {
                    signedCertificate.Signature = signedCertificate.Sign(rsa);
                    rsa.Clear();
                    return signedCertificate;
                }
            }
            catch (CryptographicException e)
            {
                System.Diagnostics.Trace.TraceError("Exception while trying to get public key : {0}", e.Message);
            }

            return null;
        }

        /// <summary>
        /// Get the participation key with given index.
        /// </summary>
        /// <returns>Returns the participation key.</returns>
        public ParticipationKey GetParticipationKey()
        {
            return this.participationKeyGenerator.GetCurrentInstance();
        }

        /// <summary>
        /// Get the public key pf the rsa key.
        /// </summary>
        /// <param name="index">Index is currently unused.</param>
        /// <returns>Returns the public key.</returns>
        public PublicKey GetPublicKeys(int index)
        {
            try
            {
                using (RSACryptoServiceProvider rsa = new RSACryptoServiceProvider(RsaAlgorithm.KeyLength, TokenGenerator.GetCspParameters()))
                {
                    PublicKey publicKey = new PublicKey(TimeSpan.MaxValue, rsa.ExportCspBlob(false));
                    rsa.Clear();
                    return publicKey;
                }
            }
            catch (CryptographicException e)
            {
                System.Diagnostics.Trace.TraceError("Exception while trying to get public key : {0}", e.Message);
            }

            return null;
        }

        /// <summary>
        /// Dispose TokenGenerator.
        /// </summary>
        public void Dispose()
        {
            if (this.accountManager != null)
            {
                this.accountManager.Dispose();
            }
        }

        /// <summary>
        /// Get the csp parameter.
        /// </summary>
        /// <returns>Return the csp parameters.</returns>
        private static CspParameters GetCspParameters()
        {
            CspParameters cspParameters = new CspParameters(TokenGenerator.ProviderRsaFull);
            cspParameters.KeyContainerName = TokenGenerator.KeyStoreName;
            return cspParameters;
        }

        /// <summary>
        /// Initialize administrator user and permissions.
        /// </summary>
        /// This functionality rather not reasonable, the token generator will always use default admin password.
        private void InitializeAdministratorUserAndPermissions()
        {
            try
            {
                UserProperties adminUser = new UserProperties("admin", TokenGenerator.DefaultAdminPassword);
                if (this.accountManager.GetUserId("admin") > 0)
                {
                    if (this.accountManager.AuthenticateUser(adminUser.Login, adminUser.Password))
                    {
                        System.Diagnostics.Trace.TraceWarning("Admin user is using default password. Please change it.");
                    }
                }
                else
                {
                    if (!this.accountManager.CreateAccount("admin", TokenGenerator.DefaultAdminPassword))
                    {
                        System.Diagnostics.Trace.TraceError("Failed to create 'admin' account. Some functionality will be missing");
                    }
                    else
                    {
                        this.CreateAndGrantTo(adminUser, PermissionTypes.Admin, "Admin");
                        this.CreateAndGrantTo(adminUser, PermissionTypes.CreateUsers, "Create users");
                        this.CreateAndGrantTo(adminUser, PermissionTypes.ViewUsers, "View users");
                        this.CreateAndGrantTo(adminUser, PermissionTypes.CreatePermissions, "Create permissions");
                        this.CreateAndGrantTo(adminUser, PermissionTypes.AssignPermissions, "Assign permissions");
                        this.CreateAndGrantTo(adminUser, PermissionTypes.ViewPermissions, "ViewPermissions");
                    }
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Trace.TraceError("Failed to initialize 'admin' account : {0}", e.Message);
            }

            try
            {
                if (!this.FindPermission(PermissionTypes.Participation))
                {
                    this.CreatePermission(PermissionTypes.Participation, "Paticipation");
                }

                if (!this.FindPermission(PermissionTypes.Announce))
                {
                    this.CreatePermission(PermissionTypes.Announce, "Announce");
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Trace.TraceError("Failed to create permisssions : {0}", e.Message);
            }
        }

        /// <summary>
        /// Find a given permission type.
        /// </summary>
        /// <param name="type">Permission type.</param>
        /// <returns>Return true if the given permission type is found.</returns>
        private bool FindPermission(PermissionTypes type)
        {
            foreach (PermissionProperties permission in this.accountManager.ListAllPermissions())
            {
                if (permission.Id == (long)type)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Create permission of type.
        /// </summary>
        /// <param name="type">Permission type.</param>
        /// <param name="name">Permission name.</param>
        private void CreatePermission(PermissionTypes type, string name)
        {
            if (!this.accountManager.CreatePermission((int)type, name))
            {
                System.Diagnostics.Trace.TraceError("Failed to create permission '{0}'", name);
            }
        }

        /// <summary>
        /// Create permission and gran it to.
        /// </summary>
        /// <param name="user">User name.</param>
        /// <param name="type">Permission type.</param>
        /// <param name="name">Permission name.</param>
        private void CreateAndGrantTo(UserProperties user, PermissionTypes type, string name)
        {
            if (this.accountManager.CreatePermission((int)type, name))
            {
                if (!this.accountManager.GrantPermission(user.Login, (int)type))
                {
                    System.Diagnostics.Trace.TraceError("Failed to assign permission '{0}' to 'admin' user", name);
                }
            }
            else
            {
                System.Diagnostics.Trace.TraceError("Failed to create permission '{0}'", name);
            }
        }
    }
}
