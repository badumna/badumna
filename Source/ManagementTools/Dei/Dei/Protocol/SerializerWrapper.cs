//-----------------------------------------------------------------------
// <copyright file="SerializerWrapper.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace Dei.Protocol
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Text;
    using System.Threading;
    using System.Xml;
    using System.Xml.Schema;
    using Serializer = System.Xml.Serialization.XmlSerializer;

    /// <summary>
    /// Wrapper class to allow easy use of different serializers 
    /// </summary>
    public class SerializerWrapper : ISerializerWrapper
    {
        /// <summary>
        /// Xml serializer.
        /// </summary>
        private Serializer serializer;

        internal const string Namespace = "http://badumna.com/DeiProtocol";

        /// <summary>
        /// Initializes a new instance of the <see cref="SerializerWrapper"/> class.
        /// </summary>
        /// <param name="type">Object type.</param>
        public SerializerWrapper(Type type)
        {
            this.serializer = new System.Xml.Serialization.XmlSerializer(type, SerializerWrapper.Namespace);
        }

        /// <summary>
        /// Check whether the xml reader can be deserialized.
        /// </summary>
        /// <param name="reader">Xml reader.</param>
        /// <returns>Return true if xml reader can be deserialized.</returns>
        public bool CanDeserialize(XmlReader reader)
        {
            return this.serializer.CanDeserialize(reader);
        }

        /// <summary>
        /// Write objet to xml serializer.
        /// </summary>
        /// <param name="writer">Xml writer.</param>
        /// <param name="obj">Object need to be serialized.</param>
        public void WriteObject(XmlWriter writer, object obj)
        {
            this.serializer.Serialize(writer, obj);
        }

        /// <summary>
        /// Read object from xml reader.
        /// </summary>
        /// <param name="reader">Xml reader.</param>
        /// <returns>Return the object.</returns>
        public object ReadObject(XmlReader reader)
        {
            return this.serializer.Deserialize(reader);
        }
    }
}
