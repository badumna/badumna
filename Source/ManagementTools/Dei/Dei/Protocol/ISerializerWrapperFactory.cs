//-----------------------------------------------------------------------
// <copyright file="ISerializerWrapperFactory.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Dei.Protocol
{
    using System;

    /// <summary>
    /// Abstract factory for creating SerializerWrapper.
    /// </summary>
    internal interface ISerializerWrapperFactory
    {
        /// <summary>
        /// Create a SerializerWrapper.
        /// </summary>
        /// <param name="type">Type of serializer.</param>
        /// <returns>Return serializer wrapper.</returns>
        ISerializerWrapper Create(Type type);
    }
}
