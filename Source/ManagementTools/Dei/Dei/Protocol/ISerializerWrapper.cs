//-----------------------------------------------------------------------
// <copyright file="ISerializerWrapper.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Dei.Protocol
{
    using System.Xml;

    /// <summary>
    /// Wrapper interface to allow easy use of different serializers 
    /// </summary>
    public interface ISerializerWrapper
    {
        /// <summary>
        /// Check whether the xml reader can be deserialized.
        /// </summary>
        /// <param name="reader">Xml reader.</param>
        /// <returns>Return true if xml reader can be deserialized.</returns>
        bool CanDeserialize(XmlReader reader);

        /// <summary>
        /// Write objet to xml serializer.
        /// </summary>
        /// <param name="writer">Xml writer.</param>
        /// <param name="obj">Object need to be serialized.</param>
        void WriteObject(XmlWriter writer, object obj);

        /// <summary>
        /// Read object from xml reader.
        /// </summary>
        /// <param name="reader">Xml reader.</param>
        /// <returns>Return the object.</returns>
        object ReadObject(XmlReader reader);
    }
}
