//-----------------------------------------------------------------------
// <copyright file="IClientProtocol.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Dei.Protocol
{
    using System.Collections.Generic;
    using Badumna.Security;

    /// <summary>
    /// Client protocol interface.
    /// </summary>
    internal interface IClientProtocol
    {
        /// <summary>
        /// Gets the server public key.
        /// </summary>
        Dei.PublicKey ServerPublicKey { get; }

        /// <summary>
        /// Gets the Failure reason.
        /// </summary>
        FailureReasons FailureReason { get; }

        /// <summary>
        /// Gets the most recent login result.
        /// </summary>
        LoginResult LoginResult { get; }

        /// <summary>
        /// Request login to dei server.
        /// </summary>
        /// <param name="userProperties">User properties.</param>
        /// <returns>Return true on success.</returns>
        LoginResult Login(UserProperties userProperties);

        /// <summary>
        /// Request a new account creation.
        /// </summary>
        /// <param name="newUserProperties">New user properties.</param>
        /// <returns>Return true on success.</returns>
        bool CreateNewAccount(UserProperties newUserProperties);

        /// <summary>
        /// Request a new character creation for the current user.
        /// </summary>
        /// <param name="userProperties">User properties.</param>
        /// <param name="characterName">The new character's name.</param>
        /// <returns>Return the created character, or `null` if creation failed.</returns>
        Badumna.Security.Character CreateCharacter(UserProperties userProperties, string characterName);

        /// <summary>
        /// Request logout.
        /// </summary>
        /// <param name="userProperties">User properties who request logout.</param>
        /// <returns>Return true on success.</returns>
        bool Logout(UserProperties userProperties);

        /// <summary>
        /// Request deleting an account.
        /// </summary>
        /// <param name="userProperties">User properties need to be deleted.</param>
        /// <returns>Return true on success.</returns>
        bool DeleteAccount(UserProperties userProperties);

        /// <summary>
        /// Request the deletion of a character owned by the current user.
        /// </summary>
        /// <param name="userProperties">User properties.</param>
        /// <param name="character">The character to delete.</param>
        /// <returns>Return true on success.</returns>
        bool DeleteCharacter(UserProperties userProperties, Character character);

        /// <summary>
        /// Request account modification (e.g. change user password).
        /// </summary>
        /// <param name="userProperties">User properties require modification.</param>
        /// <returns>Return true on success.</returns>
        bool ChangeAccount(UserProperties userProperties);

        /// <summary>
        /// Request character list for a given user.
        /// </summary>
        /// <param name="userProperties">User properties.</param>
        /// <returns>Returna a list of characters.</returns>
        IList<Badumna.Security.Character> RequestCharacterList(UserProperties userProperties);

        /// <summary>
        /// Request for creating a new permission.
        /// </summary>
        /// <param name="permissionName">New permission name.</param>
        /// <returns>Return the new permission id.</returns>
        long CreatePermission(string permissionName);

        /// <summary>
        /// Request for removing a permission.
        /// </summary>
        /// <param name="permissionId">Permission id.</param>
        /// <returns>Return true on success.</returns>
        bool RemovePermission(long permissionId);

        /// <summary>
        /// Request for granting permission to a given user.
        /// </summary>
        /// <param name="userProperties">User properties.</param>
        /// <param name="permissionId">Permission id.</param>
        /// <returns>Return true on success.</returns>
        bool GrantPermission(UserProperties userProperties, long permissionId);

        /// <summary>
        /// Request for revoking permission from a given user.
        /// </summary>
        /// <param name="userProperties">User properties.</param>
        /// <param name="permissionId">Permission id.</param>
        /// <returns>Return true on success.</returns>
        bool RevokePermission(UserProperties userProperties, long permissionId);

        /// <summary>
        /// Requesting server time.
        /// </summary>
        /// <param name="userProperties">User properties.</param>
        /// <returns>Return the server time.</returns>
        ServerTime RequestServerTime(UserProperties userProperties);

        /// <summary>
        /// Request participation key from server.
        /// </summary>
        /// <param name="userProperties">User properties.</param>
        /// <param name="tokenNumber">Token number</param>
        /// <returns>Return the participation key for this user on success.</returns>
        ParticipationKey RequestParticipationKey(UserProperties userProperties, int tokenNumber);

        /// <summary>
        /// Request servers key.
        /// </summary>
        /// <param name="userProperties">User properties.</param>
        /// <param name="tokenNumber">Token number.</param>
        /// <returns>Return the server public key on success.</returns>
        Dei.PublicKey RequestServersKey(UserProperties userProperties, int tokenNumber);

        /// <summary>
        /// Request permission list for a given user.
        /// </summary>
        /// <param name="userProperties">User properties.</param>
        /// <param name="serializedPublicKey">Serialized public key.</param>
        /// <returns>Return list of permission.</returns>
        IList<Dei.Permission> RequestPermissionList(UserProperties userProperties, byte[] serializedPublicKey);

        /// <summary>
        /// Request all available permission types
        /// </summary>
        /// <param name="username">User name.</param>
        /// <returns>Return all available permission types.</returns>
        IList<PermissionProperties> RequestPermissionTypes(string username);

        /// <summary>
        /// Request user list given the username glob expression.
        /// </summary>
        /// <param name="usernameGlob">Username glob expression.</param>
        /// <param name="startRange">Start range.</param>
        /// <param name="maximumNumberOfResults">Maximum number of result.</param>
        /// <returns>Return list of users.</returns>
        IList<string> RequestUserList(string usernameGlob, long startRange, long maximumNumberOfResults);

        /// <summary>
        /// Request number of user that match with given username glob expression.
        /// </summary>
        /// <param name="usernameGlob">Username glob expression.</param>
        /// <returns>Return the number of user that match with given glob expression on success. 
        /// Otherwise it return -1.</returns>
        long RequestUserCount(string usernameGlob);

        /// <summary>
        /// Request certificate from server.
        /// </summary>
        /// <param name="userProperties">User properties.</param>
        /// <param name="character">The character to identify as.</param>
        /// <param name="serializedPublicKey">Serialized public key.</param>
        /// <returns>Return the signed user certificate.</returns>
        UserCertificate RequestCertificate(UserProperties userProperties, Badumna.Security.Character character, byte[] serializedPublicKey);
    }
}
