//-----------------------------------------------------------------------
// <copyright file="ListUserRequest.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace Dei
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Xml.Serialization;

    /// <summary>
    /// ListUserRequest class is used to store the information about list user request,
    /// the search expression (i.e. glob expression) and also the result of that query.
    /// </summary>
    [Serializable]
    public class ListUserRequest
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ListUserRequest"/> class.
        /// </summary>
        public ListUserRequest()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ListUserRequest"/> class.
        /// </summary>
        /// <param name="usernameGlob">Username glob.</param>
        /// <param name="numberOfResults">Number of result.</param>
        /// <param name="limitStart">Limit start.</param>
        public ListUserRequest(string usernameGlob, long numberOfResults, long limitStart)
        {
            this.UsernameGlob = usernameGlob;
            this.NumberOfResults = numberOfResults;
            this.LimitStart = limitStart;
            this.Result = new string[0];
        }

        /// <summary>
        /// Gets or sets the username glob expression.
        /// </summary>
        public string UsernameGlob { get; set; }

        /// <summary>
        /// Gets or sets the number of result.
        /// </summary>
        public long NumberOfResults { get; set; }

        /// <summary>
        /// Gets or sets the limit start.
        /// </summary>
        public long LimitStart { get; set; }

        /// <summary>
        /// Gets or sets the result (i.e list of users which match with the username glob).
        /// </summary>
        [XmlArrayItem("Username")]
        public string[] Result { get; set; }
    }
}
