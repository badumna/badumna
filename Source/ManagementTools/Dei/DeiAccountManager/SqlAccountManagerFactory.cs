﻿using System;
using DeiServer;
using System.Configuration;

namespace DeiAccountManager
{
    /// <summary>
    /// Sql Account Manager Factory.
    /// </summary>
    public class SqlAccountManagerFactory : IAccountManagerFactory
    {
        #region IAccountManagerFactory Members

        /// <summary>
        /// Creates the account manager.
        /// </summary>
        /// <param name="keyValidityPeriod">The key validity period.</param>
        /// <returns>
        /// The account manager object.
        /// </returns>
        public IAccountManager Create(TimeSpan keyValidityPeriod)
        {
            string connectionString = string.Empty;
            string dataProvider = ConfigurationManager.AppSettings["dataProvider"];

            try
            {
                connectionString = ConfigurationManager.ConnectionStrings[dataProvider].ConnectionString;
            }
            catch (NullReferenceException)
            {
                Console.WriteLine(
                    "Fatal Error: Could not find connection string for use with data provider {0} in the application config file.",
                    dataProvider);
                return null;
            }

            return new SqlAccountManager(dataProvider, connectionString, keyValidityPeriod);
        }

        #endregion
    }
}
