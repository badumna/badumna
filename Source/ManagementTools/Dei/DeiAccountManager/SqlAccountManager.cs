﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text;
using Badumna.Security;
using Dei;
using DeiServer;

namespace DeiAccountManager
{
    /// <summary>
    /// SqlAccountManager implements the IAccountManager interface backed by an SQL database.
    /// Developers may override some of the query methods (e.g. FindUserQuery) in order to use
    /// a different database schema or to perform additional checks / actions.
    /// </summary>
    public class SqlAccountManager : IAccountManager, IDisposable
    {
        private static Random mRandom = new Random((int)DateTime.Now.Ticks);
        private DbProviderFactory mFactory;
        private string mConnectionString;
        private DbConnection mConnection;
        private TimeSpan mKeyValidityPeriod;

        /// <summary>
        /// SqlAccountManager constructor
        /// </summary>
        /// <param name="keyValidityPeriod">Validity Period for this sqlAccountManager class instance</param>
        public SqlAccountManager(string dataProvider, string connectionString, TimeSpan keyValidityPeriod)
        {
            this.mConnectionString = connectionString;
            try
            {
                this.mFactory = DbProviderFactories.GetFactory(dataProvider);
            }
            catch (System.Configuration.ConfigurationErrorsException ex)
            {
                Console.WriteLine(
                    "Error using data provider {0}{1}({2})",
                    dataProvider,
                    Environment.NewLine,
                    ex.Message);
                Environment.Exit(1);
            }
            this.mKeyValidityPeriod = keyValidityPeriod;
        }

        private void InitializeDatabase()
        {
            if (!this.TableExists("users"))
            {
                this.PerformQuery(QueryResources.CreateUsers);
            }

            if (!this.TableExists("characters"))
            {
                this.PerformQuery(QueryResources.CreateCharacters);
            }

            if (!this.TableExists("permissions"))
            {
                this.PerformQuery(QueryResources.CreatePermissions);
            }

            if (!this.TableExists("permission_types"))
            {
                this.PerformQuery(QueryResources.CreatePermissionTypes);
            }
        }

        /// <summary>
        /// Check to see if the table exists.
        /// </summary>
        /// <param name="tableName">The name of the table to check for.</param>
        /// <returns>A value indicating whether the table exists.</returns>
        private bool TableExists(string tableName)
        {
            DataTable dataTable = this.GetConnection().GetSchema(
                "Tables",
                new string[] { null, null, tableName });
            return dataTable.Rows.Count > 0;
        }

        private void PerformQuery(string sql)
        {
            using (IDbCommand command = this.GetSqlCommand())
            {
                command.CommandText = sql;
                command.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Initializes this instance.
        /// </summary>
        /// This method will be called as soon as this instance is created.
        public void Initialize()
        {
            this.InitializeDatabase();
        }

        /// <summary>
        /// Shutdowns this instance.
        /// </summary>
        /// This method will be called when the Dei server is being shutdown.
        public void Shutdown()
        {
        }

        /// <summary>
        /// Find the users which match with the given conditions, by executing the SQL query
        /// </summary>
        /// For example, "*" is used to obtain all the available users from database.
        /// <param name="usernameGlobExpression">Search conditions.</param>
        /// <returns>Return list of string consist of the username that match with the given conditions.</returns>
        /// <seealso cref="IAccountManager.FindUsers"/>
        public IList<string> FindUsers(string usernameGlobExpression)
        {
            List<string> resultsList = new List<string>();

            if (string.IsNullOrEmpty(usernameGlobExpression))
            {
                return resultsList;
            }

            try
            {
                using (IDbCommand command = this.GetSqlCommand())
                {
                    if (command != null)
                    {
                        this.FindUsersQuery(resultsList, command, usernameGlobExpression);
                    }
                }
            }
            catch (Exception e)
            {
                this.HandleException("find user ('" + usernameGlobExpression + "')", e);
            }

            return resultsList;
        }

        /// <summary>
        /// Get the total number of user that satisfy the given conditions, by executing the SQL query
        /// </summary>
        /// For example, to get the total number of users exist in database, use "*" for usernameGlobExpression.
        /// <param name="usernameGlobExpression">Search conditions.</param>
        /// <returns>Return the number of user that satisfy the given conditions.</returns>
        /// <seealso cref="IAccountManager.GetUserCount"/>
        public long GetUserCount(string usernameGlobExpression)
        {
            if (string.IsNullOrEmpty(usernameGlobExpression))
            {
                return 0;
            }

            try
            {
                using (IDbCommand command = this.GetSqlCommand())
                {
                    if (command != null)
                    {
                        return this.CountUsersQuery(command, usernameGlobExpression);
                    }
                }
            }
            catch (Exception e)
            {
                this.HandleException("count users ('" + usernameGlobExpression + "')", e);
            }

            return -1;
        }

        /// <summary>
        /// Create a new account by providing the new username and unecryptedPassword.
        /// </summary>
        /// Encrypt the password before adding it into database (before executing the SQL query)
        /// <param name="username">Username</param>
        /// <param name="unencryptedPassword">Unecrypted password</param>
        /// <returns>Return true if it successfully create a new account, otherwise it will return false.</returns>
        /// <seealso cref="IAccountManager.CreateAccount"/>
        public bool CreateAccount(string username, string unencryptedPassword)
        {
            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(unencryptedPassword))
            {
                return false;
            }

            if (this.GetUserId(username) >= 0)
            {
                return false; // Already exists.
            }

            try
            {
                byte[] saltBytes = new byte[20];
                SqlAccountManager.mRandom.NextBytes(saltBytes);
                string salt = SqlAccountManager.GetString(saltBytes);
                string cryptedPassword = this.GetCryptedPassword(salt, unencryptedPassword);

                using (DbTransaction tx = this.GetConnection().BeginTransaction(IsolationLevel.Serializable))
                {
                    long newId = this.GetMaxId("users", tx);

                    // TODO: Use parameters (need to make vendor independent though (see example code in DbParameterUtility.cs).
                    using (IDbCommand insertCommand = this.GetSqlCommand())
                    {
                        this.PopulateCommand(insertCommand, @"INSERT INTO users (id, login, crypted_password, salt) VALUES(@id, @login, @crypted_password, @salt)",
                            "id", newId,
                            "login", username,
                            "crypted_password", cryptedPassword,
                            "salt", salt);
                        insertCommand.Transaction = tx;
                        int rowsUpdated = insertCommand.ExecuteNonQuery();
                        tx.Commit();
                        return rowsUpdated == 1;
                    }
                }
            }
            catch (Exception e)
            {
                this.HandleException("create account ('" + username + "')", e);
            }

            return false;
        }


        /// <summary>
        /// Delete an existing account with the given username, by executing the SQL query
        /// </summary>
        /// Delete an existing account for a particular username from database.
        /// <param name="username">Username</param>
        /// <returns>Return true if it successfully delete an account from database.</returns>
        /// <seealso cref="IAccountManager.DeleteAccount"/>
        public bool DeleteAccount(string username)
        {
            try
            {
                using (DbTransaction tx = this.GetConnection().BeginTransaction(IsolationLevel.Serializable))
                {
                    // Sqlite requires you to jump through hoops to have deletes
                    // cascade to child records, so we'll do it explicitly:
                    long userId;
                    bool success;
                    using (IDbCommand command = this.GetSqlCommand(tx))
                    {
                        userId = this.GetUserIdQuery(command, username);
                    }

                    using (IDbCommand command = this.GetSqlCommand(tx))
                    {
                        this.DeleteCharactersQuery(command, userId);
                    }

                    using (IDbCommand command = this.GetSqlCommand(tx))
                    {
                        success = this.DeleteAccountQuery(command, userId);
                    }

                    if (success)
                    {
                        tx.Commit();
                    }

                    return success;
                }
            }
            catch (Exception e)
            {
                this.HandleException("delete user ('" + username + "')", e);
            }

            return false;
        }

        /// <summary>
        /// Change the password of a specific username
        /// </summary>
        /// Encrypt the new password before changing it to database
        /// <param name="username">Username</param>
        /// <param name="unencryptedPassword">New unencrypted password</param>
        /// <returns>Return true, if the password is successfully changed.</returns>
        /// <seealso cref="IAccountManager.ChangePassword"/>
        public bool ChangePassword(string username, string unencryptedPassword)
        {
            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(unencryptedPassword))
            {
                return false;
            }

            try
            {
                byte[] saltBytes = new byte[20];
                SqlAccountManager.mRandom.NextBytes(saltBytes);
                string salt = SqlAccountManager.GetString(saltBytes);
                string cryptedPassword = this.GetCryptedPassword(salt, unencryptedPassword);

                using (IDbCommand command = this.GetSqlCommand())
                {
                    if (command != null)
                    {
                        return this.ChangePasswordQuery(command, username, cryptedPassword, salt);
                    }
                }
            }
            catch (Exception e)
            {
                this.HandleException("change password ('" + username + "')", e);
            }

            return false;
        }

        /// <summary>
        /// Authenticate user 
        /// </summary>
        /// Authenticate the user if the username and password given are valid, check the validity of username and password from 
        /// database. This function will automatically encrypt the uncrypted password first.
        /// <param name="username">Username</param>
        /// <param name="unencryptedPassword"> Unencrypted password</param>
        /// <returns>Return true is the username and password are valid, otherwise it return false.</returns>
        /// <seealso cref="IAccountManager.AuthenticateUser"/>
        public bool AuthenticateUser(string username, string unencryptedPassword)
        {
            try
            {
                using (IDbCommand command = this.GetSqlCommand())
                {
                    return this.AuthenticateUserQuery(command, username, unencryptedPassword);
                }
            }
            catch (Exception e)
            {
                this.HandleException("authenticate user ('" + username + "')", e);
            }

            return false;
        }

        /// <summary>
        /// Get the user Id for a particular username by using the SQL query
        /// </summary>
        /// Get the userId of a particular username from the database.
        /// <param name="username">Username</param>
        /// <returns>Return the userId of given username, if the username is a valid username</returns>
        /// <seealso cref="IAccountManager.GetUserId"/>
        public long GetUserId(string username)
        {
            try
            {
                using (IDbCommand command = this.GetSqlCommand())
                {
                    return this.GetUserIdQuery(command, username);
                }
            }
            catch (Exception e)
            {
                this.HandleException("find user id ('" + username + "')", e);
            }

            return -1;
        }

        /// <summary>
        /// Get the list of all available permissions from database
        /// </summary>
        /// <returns>Return list of all available permissions.</returns>
        /// <seealso cref="IAccountManager.ListAllPermissions"/>
        public IList<PermissionProperties> ListAllPermissions()
        {
            List<PermissionProperties> permissions = new List<PermissionProperties>();

            try
            {
                using (IDbCommand command = this.GetSqlCommand())
                {
                    this.ListPermissionTypesQuery(command, permissions);
                }
            }
            catch (Exception e)
            {
                this.HandleException("get permission list", e);
            }

            return permissions;
        }

        /// <summary>
        /// Get the list of available permission for a particular user
        /// </summary>
        /// <param name="username">Username</param>
        /// <returns>Return the list of permission that particular user has</returns>
        /// <seealso cref="IAccountManager.GetPermissions"/>
        public IList<PermissionProperties> GetPermissions(string username)
        {
            List<PermissionProperties> permissions = new List<PermissionProperties>();

            try
            {
                using (IDbCommand command = this.GetSqlCommand())
                {
                    this.ListUsersPermissionsQuery(command, permissions, username);
                }
            }
            catch (Exception e)
            {
                this.HandleException("get permission list for user ('" + username + "')", e);
            }

            return permissions;
        }

        /// <summary>
        /// Get the participation validity period
        /// </summary>
        /// <returns>Return the participation validity period</returns>
        /// <seealso cref="IAccountManager.GetParticipationValidityPeriod"/>
        public virtual TimeSpan GetParticipationValidityPeriod()
        {
            return this.mKeyValidityPeriod;
        }

        /// <summary>
        /// Create a new permission type, it used by Admin only
        /// </summary>
        /// A user require a creating permission privilage to be able to create a new permission.
        /// <param name="permissionName">Permission Name</param>
        /// <returns>Return the permission id of the new permission.</returns>
        /// <seealso cref="IAccountManager.CreatePermission(string permissionName)"/>
        public long CreatePermission(string permissionName)
        {
            if (string.IsNullOrEmpty(permissionName))
            {
                return -1;
            }

            try
            {
                using (DbTransaction tx = this.GetConnection().BeginTransaction(IsolationLevel.Serializable))
                {
                    long newId = this.GetMaxId("permission_types", tx);
                    using (IDbCommand insertCommand = this.GetSqlCommand(tx))
                    {
                        if (this.CreatePermissionQuery(insertCommand, newId, permissionName))
                        {
                            tx.Commit();
                            return newId;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                this.HandleException("create permission " + permissionName, e);
            }

            return -1;
        }

        /// <summary>
        /// Create a new permission type by providing both the permission name and permission id
        /// </summary>
        /// <seealso cref="CreatePermission(string permissionName)"/>
        /// <param name="permissionId">Permission Id</param>
        /// <param name="permissionName">Permission Name</param>
        /// <returns>Return true if the new permission is created successfully.</returns>
        /// <seealso cref="IAccountManager.CreatePermission(long permissionId, string permissionName)"/>
        public bool CreatePermission(long permissionId, string permissionName)
        {
            if (permissionId < 0)
            {
                return false;
            }

            if (string.IsNullOrEmpty(permissionName))
            {
                return false;
            }

            try
            {
                using (IDbCommand command = this.GetSqlCommand())
                {
                    return this.CreatePermissionQuery(command, permissionId, permissionName);
                }
            }
            catch (Exception e)
            {
                this.HandleException("create permission " + permissionName, e);
            }

            return false;
        }

        /// <summary>
        /// Delete an existing permission according to given permissionId
        /// </summary>
        /// Delete an existing permission from the database based on given permissionId
        /// <param name="permissionId">Permission Id</param>
        /// <returns>Return true if the permission is removed successfully</returns>
        /// <seealso cref="IAccountManager.DeletePermission"/>
        public bool DeletePermission(long permissionId)
        {
            if (permissionId < 0)
            {
                return false;
            }

            try
            {
                using (IDbCommand command = this.GetSqlCommand())
                {
                    return this.DeletePermissionQuery(command, permissionId);
                }

            }
            catch (Exception e)
            {
                this.HandleException("delete permission " + permissionId, e);
            }

            return false;
        }

        /// <summary>
        /// Check whether the user has a particular permission
        /// </summary>
        /// For example, admin has admin permission (privilege), while the normal users only have participation permission.
        /// <param name="username">Username</param>
        /// <param name="permissionId">Permission Id</param>
        /// <returns>Return true if username has that permission, otherwise it will return false.</returns>
        /// <seealso cref="IAccountManager.HasPermission"/>
        public bool HasPermission(string username, long permissionId)
        {
            try
            {
                using (IDbCommand command = this.GetSqlCommand())
                {
                    return this.HasPermissionQuery(command, username, permissionId);
                }
            }
            catch (Exception e)
            {
                this.HandleException(string.Format("check user {0} has permission {1}", username, permissionId), e);
            }

            return false;
        }

        /// <summary>
        /// Grant a particular permission to a particular user
        /// </summary>
        /// <param name="username">Username</param>
        /// <param name="permissionId">Permission Id</param>
        /// <returns>Return true if the permission is granted successfully to the particular user.</returns>
        /// <seealso cref="IAccountManager.GrantPermission"/>
        public bool GrantPermission(string username, long permissionId)
        {
            if (string.IsNullOrEmpty(username))
            {
                return false;
            }

            long userId = this.GetUserId(username);
            if (userId < 0)
            {
                return false;
            }

            if (!this.DoesPermissionExist(permissionId))
            {
                return false;
            }

            if (this.HasPermission(username, permissionId))
            {
                return true;
            }

            try
            {
                using (IDbCommand command = this.GetSqlCommand())
                {
                    return this.GrantPermissionQuery(command, userId, permissionId);
                }
            }
            catch (Exception e)
            {
                this.HandleException(string.Format("grant user {0} permission {1}", username, permissionId), e);
            }

            return false;
        }

        /// <summary>
        /// Revoke a particular permission from a particular user
        /// </summary>
        /// <param name="username">Username</param>
        /// <param name="permissionId">Permission Id</param>
        /// <returns>Return true if the permission is revoked successfully from the particular user or the user does not have the permission.</returns>
        /// <seealso cref="IAccountManager.RevokePermission"/>
        public bool RevokePermission(string username, long permissionId)
        {
            if (string.IsNullOrEmpty(username) || permissionId < 0)
            {
                return false;
            }

            long userId = this.GetUserId(username);
            if (userId < 0)
            {
                return false;
            }

            if (!this.DoesPermissionExist(permissionId))
            {
                return false;
            }

            if (!this.HasPermission(username, permissionId))
            {
                return true;
            }

            try
            {
                using (IDbCommand command = this.GetSqlCommand())
                {
                    return this.RevokePermissionQuery(command, userId, permissionId);
                }
            }
            catch (Exception e)
            {
                this.HandleException(string.Format("revoke permission {0} from user {1}", permissionId, username), e);
            }

            return false;
        }

        /// <inheritdoc/>
        public bool AuthorizeCharacter(string username, Character character)
        {
            if (string.IsNullOrEmpty(username) || character == null || !character.IsValid)
            {
                return false;
            }

            try
            {
                using (IDbCommand command = this.GetSqlCommand())
                {
                    return this.CharacterExistsQuery(command, username, character);
                }
            }
            catch (Exception e)
            {
                this.HandleException("authorize character ('" + character + "')", e);
                return false;
            }
        }

        /// <summary>
        /// Query the datbase for a character belonging to a specific user.
        /// </summary>
        /// <param name="command">The command.</param>
        /// <param name="userName">Name of the user.</param>
        /// <param name="id">The character.</param>
        /// <returns>Whether the character exists and is owned by the user.</returns>
        protected bool CharacterExistsQuery(IDbCommand command, string userName, Character character)
        {
            this.PopulateCommand(command, @"
                SELECT users.id
                FROM characters
                    inner join users on characters.user_id = users.id
                WHERE characters.id = @id
                  AND characters.name = @name
                  AND users.login = @user",
                "id", character.Id,
                "name", character.Name,
                "user", userName);
            using (IDataReader reader = command.ExecuteReader())
            {
                if (reader.Read())
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Query the datbase for a character name belonging to any user.
        /// </summary>
        /// <param name="command">The command.</param>
        /// <param name="id">The character name.</param>
        /// <returns>Whether a character with the given name exists.</returns>
        protected bool CharacterExistsQuery(IDbCommand command, string characterName)
        {
            this.PopulateCommand(command, @"
                SELECT id
                FROM characters
                WHERE name = @name",
                "name", characterName);
            using (IDataReader reader = command.ExecuteReader())
            {
                if (reader.Read())
                {
                    return true;
                }
            }
            return false;
        }

        /// <inheritdoc/>
        public Badumna.Security.Character CreateCharacter(string username, string characterName)
        {
            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(characterName))
            {
                return null;
            }

            try
            {
                using (DbTransaction tx = this.GetConnection().BeginTransaction(IsolationLevel.Serializable))
                {
                    long userId;
                    using (IDbCommand command = this.GetSqlCommand(tx))
                    {
                        if (this.CharacterExistsQuery(command, characterName))
                        {
                            return null; // character name is already taken
                        }
                    }

                    using (IDbCommand command = this.GetSqlCommand(tx))
                    {
                        userId = this.GetUserIdQuery(command, username);
                        if (userId < 0)
                        {
                            return null; // No such user.
                        }
                    }

                    long characterId = this.GetMaxId("characters", tx);

                    using (IDbCommand insertCommand = this.GetSqlCommand(tx))
                    {
                        Character character = this.CreateCharacterQuery(insertCommand, characterId, userId, characterName);
                        tx.Commit();
                        return character;
                    }
                }
            }
            catch (Exception e)
            {
                this.HandleException("create character ('" + characterName + "')", e);
            }

            return null;
        }

        /// <summary>
        /// Creates a character in the database.
        /// </summary>
        /// <param name="command">The command.</param>
        /// <param name="characterId">The character id.</param>
        /// <param name="userId">The user id.</param>
        /// <param name="characterName">Name of the character.</param>
        /// <returns>The created character</returns>
        protected Badumna.Security.Character CreateCharacterQuery(IDbCommand command, long characterId, long userId, string characterName)
        {
            this.PopulateCommand(command,
                @"INSERT INTO characters (id, user_id, name) VALUES(@id, @user_id, @name)",
                "id", characterId,
                "user_id", userId,
                "name", characterName);
            int rowsUpdated = command.ExecuteNonQuery();
            return new Character(characterId, characterName);
        }

        /// <inheritdoc/>
        public bool DeleteCharacter(string username, Character character)
        {
            try
            {
                using (IDbCommand deleteCommand = this.GetSqlCommand())
                {
                    bool success = this.DeleteCharacterQuery(deleteCommand, username, character);
                    return success;
                }
            }
            catch (Exception e)
            {
                this.HandleException("delete character (" + character + ")", e);
            }

            return false;
        }

        /// <summary>
        /// Deletes the character from the database.
        /// </summary>
        /// <param name="command">The command.</param>
        /// <param name="character">The character.</param>
        /// <returns></returns>
        protected bool DeleteCharacterQuery(IDbCommand command, string userName, Character character)
        {
            this.PopulateCommand(command,
                @"DELETE FROM characters WHERE id = @id and name = @name and user_id in
                    (SELECT id from users where login = @user_name)",
                "id", character.Id,
                "name", character.Name,
                "user_name", userName);
            int rowsUpdated = command.ExecuteNonQuery();
            return rowsUpdated == 1;
        }

        /// <summary>
        /// Deletes all characters belonging to the given user ID from the database.
        /// </summary>
        /// <param name="command">The command.</param>
        /// <param name="character">The user ID.</param>
        protected void DeleteCharactersQuery(IDbCommand command, long userId)
        {
            this.PopulateCommand(command,
                @"DELETE FROM characters WHERE user_id = @id",
                "id", userId);
            command.ExecuteNonQuery();
        }

        /// <inheritdoc/>
        public IList<Badumna.Security.Character> GetCharacters(string username)
        {
            try
            {
                using (IDbCommand command = this.GetSqlCommand())
                {
                    return this.GetCharactersQuery(command, username);
                }
            }
            catch (Exception e)
            {
                this.HandleException("get character list ('" + username + "')", e);
                return null;
            }
        }

        /// <summary>
        /// Query to return the characters for a given username from the database.
        /// </summary>
        /// <param name="command">The command.</param>
        /// <param name="username">The username.</param>
        /// <returns>A list of characters.</returns>
        protected IList<Badumna.Security.Character> GetCharactersQuery(IDbCommand command, string username)
        {
            List<Character> characters = new List<Character>();
            this.PopulateCommand(command,
                @"SELECT characters.id, characters.name
                    FROM characters inner join users
                         on characters.user_id = users.id
                    WHERE login = @user",
                "user", username);

            using (IDataReader reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    long id = reader.GetInt64(0);
                    string name = reader.GetString(1);

                    characters.Add(new Character(id, name));
                }
            }
            return characters;
        }

        /// <summary>
        /// Creating a connection using the data provider and conneciton string..
        /// </summary>
        /// <returns>A newly created connecion.</returns>
        protected DbConnection CreateConnection()
        {
            this.mConnection = mFactory.CreateConnection();
            this.mConnection.ConnectionString = mConnectionString;
            return this.mConnection;
        }

        /// <summary>
        /// GetCryptedPassword is a virtual method and could be overriden in a derived class.
        /// </summary>
        /// Create a custom password encrytpted by overriden this method if neccessary
        /// <param name="salt">salt</param>
        /// <param name="unencryptedPassword">Unencrypted password</param>
        /// <returns>Return the encrypted password</returns>
        protected virtual string GetCryptedPassword(string salt, string unencryptedPassword)
        {
            string digestString = String.Format("--{0}--{1}--", salt, unencryptedPassword);
            string crypted_password = SqlAccountManager.GetHashOfString(digestString);

            return crypted_password;
        }

        /// <summary>
        /// This method use the standard SQL query to find the user from the database based on the given conditions.
        /// </summary>
        /// This method will be called by <see cref="FindUsers"/>, override this method if it require a complex
        /// query for a certain conditions
        /// <param name="resultsList">Result from the query execution, in this case the list of the user that match with the conditions</param>
        /// <param name="command">IDbCommand passed from <see cref="FindUsers"/> method</param>
        /// <param name="usernameGlobExpression">Search conditions, passed from <see cref="FindUsers"/> method</param>
        protected virtual void FindUsersQuery(IList<string> resultsList, IDbCommand command,
            string usernameGlobExpression)
        {
            this.PopulateCommand(command,
                @"SELECT login FROM users WHERE login LIKE @q",
                "q", usernameGlobExpression);

            using (IDataReader reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    resultsList.Add((string)reader["login"]);
                }
            }
        }

        /// <summary>
        /// This method use the Standard SQL query to count the number of user based on the given conditions
        /// </summary>
        /// This method will be called by <see cref="GetUserCount"/>, override this method if more complex query is required
        /// <param name="command">IDbCommand passed from <see cref="GetUserCount"/></param>
        /// <param name="usernameGlobExpression">Search condition passed from <see cref="GetUserCount"/></param>
        /// <returns>Return the number of user that satisfy the given conditions. </returns>
        protected virtual long CountUsersQuery(IDbCommand command, string usernameGlobExpression)
        {
            PopulateCommand(command, @"SELECT COUNT(id) FROM users WHERE login LIKE @q",
                "q", usernameGlobExpression);

            using (IDataReader reader = command.ExecuteReader())
            {
                if (reader.Read())
                {
                    if (reader[0] is System.Int32)
                    {
                        return reader.GetInt32(0);
                    }
                    else
                    {
                        return reader.GetInt64(0);
                    }
                }
            }

            return -1;
        }

        /// <summary>
        /// This method use standard SQL query to insert a new account into the database
        /// </summary>
        /// This method will be called by <see cref="CreateAccount"/>, ovveride this method if neccessary
        /// <param name="command">IDbCommand passed by <see cref="CreateAccount"/></param>
        /// <param name="userId">UserID passed by <see cref="CreateAccount"/></param>
        /// <param name="username">Username passed by <see cref="CreateAccount"/></param>
        /// <param name="cryptedPassword">Crypted password passed by <see cref="CreateAccount"/></param>
        /// <param name="salt">Salt use for the password encryption, passed by <see cref="CreateAccount"/></param>
        /// <returns>Return true if it successfully create a new account, otherwise it will return false.</returns>
        protected virtual bool CreateAccountQuery(IDbCommand command, long userId, string username, string cryptedPassword, string salt)
        {
            this.PopulateCommand(command,
                @"INSERT INTO users (id, login, crypted_password, salt) VALUES(@id, @login, @crypted_password, @salt)",
                "id", userId,
                "login", username,
                "crypted_password", cryptedPassword,
                "salt", salt);
            int rowsUpdated = command.ExecuteNonQuery();
            return rowsUpdated == 1;
        }

        /// <summary>
        /// This method deletes a user from the database by userId.
        /// </summary>
        /// This method is called by <see cref="DeleteAccount"/>, ovveride this method if neccessary
        /// <param name="command">IDbCommand passed by <see cref="DeleteAccount"/></param>
        /// <param name="id">User ID passed by <see cref="DeleteAccount"/></param>
        /// <returns>Return true if it successfully delete an account from database.</returns>
        protected virtual bool DeleteAccountQuery(IDbCommand command, long id)
        {
            this.PopulateCommand(command, @"DELETE FROM users WHERE id = @id",
                "id", id);

            int rowsUpdated = command.ExecuteNonQuery();
            return rowsUpdated == 1;
        }

        /// <summary>
        /// This method use standard SQL query to change the password of an exsisting user
        /// </summary>
        /// This method is called by <see cref="ChangePassword"/>, ovveride this method if neccessary
        /// <param name="command">IDbCommand passed by <see cref="ChangePassword"/></param>
        /// <param name="username">Username passed by <see cref="ChangePassword"/></param>
        /// <param name="cryptedPassword">Crypted password passed by <see cref="ChangePassword"/></param>
        /// <param name="salt">Salt use for the password encryption, passed by <see cref="ChangePassword"/></param>
        /// <returns>Return true, if the password is successfully changed.</returns>
        protected virtual bool ChangePasswordQuery(IDbCommand command, string username, string cryptedPassword, string salt)
        {
            this.PopulateCommand(command, @"UPDATE users SET crypted_password=@crypted_password, salt=@salt WHERE login=@user;",
                "user", username,
                "crypted_password", cryptedPassword,
                "salt", salt);

            int rowsUpdated = command.ExecuteNonQuery();
            return rowsUpdated == 1;
        }

        /// <summary>
        /// This method is used to authenticate the user using the standard SQL query
        /// </summary>
        /// This method is called by <see cref="AuthenticateUser"/>, ovveride this method if neccessary
        /// <param name="command">IDbCimmand passed by <see cref="AuthenticateUser"/></param>
        /// <param name="username">Username passed by <see cref="AuthenticateUser"/></param>
        /// <param name="unencryptedPassword">Unencrypted password passed by <see cref="AuthenticateUser"/></param>
        /// <returns>Return true is the username and password are valid, otherwise it return false.</returns>
        protected virtual bool AuthenticateUserQuery(IDbCommand command, string username, string unencryptedPassword)
        {
            this.PopulateCommand(command, "SELECT salt, crypted_password FROM users WHERE login=@user",
                "user", username);

            using (IDataReader reader = command.ExecuteReader())
            {
                if (reader.Read())
                {
                    string salt = (string)reader["salt"];
                    string storedCryptedPassword = (string)reader["crypted_password"];

                    string givenPasswordCrypted = this.GetCryptedPassword(salt, unencryptedPassword);
                    return String.Compare(storedCryptedPassword, givenPasswordCrypted, true) == 0;
                }
            }

            return false;
        }

        /// <summary>
        /// This method use standard SQL query to get the user id of a given username
        /// </summary>
        /// This method is called by <see cref="GetUserId"/>, overide this method if neccessary
        /// <param name="username">Command passed by <see cref="GetUserId"/></param>
        /// <param name="username">Username passed by <see cref="GetUserId"/></param>
        /// <returns>Return the userId of given username, if the username is a valid username</returns>
        protected virtual long GetUserIdQuery(IDbCommand command, string username)
        {
            this.PopulateCommand(command, "SELECT id FROM users WHERE login = @user",
                "user", username);
            using (IDataReader reader = command.ExecuteReader())
            {
                if (reader.Read())
                {
                    return reader.GetInt64(0);
                }
            }

            return -1;
        }

        /// <summary>
        /// This method use standard SQL query to obtain all available permission types from database
        /// </summary>
        /// This method is called by <see cref="ListAllPermissions"/>, ovveride this method if neccessary
        /// <param name="command">IDbCommand passed by <see cref="ListAllPermissions"/></param>
        /// <param name="resultsList">The list of all available permissions</param>
        protected virtual void ListPermissionTypesQuery(IDbCommand command, IList<PermissionProperties> resultsList)
        {
            command.CommandText = "SELECT id, name FROM permission_types;";
            using (IDataReader reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    long permissionId;
                    if (reader[0] is Int32)
                    {
                        permissionId = reader.GetInt32(0);
                    }
                    else
                    {
                        permissionId = reader.GetInt64(0);
                    }
                    string permissionName = reader.GetString(1);

                    resultsList.Add(new PermissionProperties(permissionId, permissionName));
                }
            }
        }

        /// <summary>
        /// This method use standard SQL query to obtain the list of permissions that a particular user has
        /// </summary>
        /// This method is called by <see cref="GetPermissions"/>, ovveride this method if neccessary
        /// <param name="command">IDbCommand passed by <see cref="GetPermissions"/></param>
        /// <param name="resultsList">List of permissions that a particular user has</param>
        /// <param name="username">Username passed by <see cref="GetPermissions"/></param>
        protected virtual void  ListUsersPermissionsQuery(IDbCommand command, IList<PermissionProperties> resultsList, string username)
        {
            this.PopulateCommand(command,
                @"SELECT a.id, a.name FROM (SELECT pt.id, pt.name, u.login
                FROM permission_types pt INNER JOIN permissions p ON pt.id = p.permission_type_id INNER JOIN users u ON p.id = u.id) a
                WHERE login=@user",
                "user", username);

            using (IDataReader reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    long permissionId;
                    if (reader[0] is Int32)
                    {
                        permissionId = reader.GetInt32(0);
                    }
                    else
                    {
                        permissionId = reader.GetInt64(0);
                    }
                    string permissionName = reader.GetString(1);

                    resultsList.Add(new PermissionProperties(permissionId, permissionName));
                }
            }
        }

        /// <summary>
        /// This method use standard SQL query to insert a new permission into database
        /// </summary>
        /// This method is called by <see cref="CreatePermission(string permissionName)"/>, ovveride this method if neccessary
        /// <param name="command">IDbCommand passed by <see cref="CreatePermission(string permissionName)"/></param>
        /// <param name="permissionName">New permission name passed by <see cref="CreatePermission(string permissionName)"/></param>
        /// <returns>Return the permission id of the new permission.</returns>
        protected virtual long CreatePermissionQuery(IDbCommand command, string permissionName)
        {
            this.PopulateCommand(command, @"INSERT INTO permission_types (name) VALUES(@name)", "name", permissionName);

            int rowsUpdated = command.ExecuteNonQuery();
            if (rowsUpdated == 1)
            {
                // Get the id 
                this.PopulateCommand(command, @"SELECT id FROM permission_types WHERE name=@name", "name", permissionName);
                using (IDataReader reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        return reader.GetInt64(0);
                    }
                }
            }

            return -1;
        }

        /// <summary>
        /// This method is similar to CreatePermissionQuery, create a new permission with a particular permission id into database
        /// </summary>
        /// This method is called by <see cref="CreatePermission(long permissionId, string permissionName)"/>, ovveride this method if neccessary
        /// <param name="command">IDbCommand passed by <see cref="CreatePermission(long permissionId, string permissionName)"/></param>
        /// <param name="permissionId">Permission id passed by <see cref="CreatePermission(long permissionId, string permissionName)"/></param>
        /// <param name="permissionName">Permission name passed by <see cref="CreatePermission(long permissionId, string permissionName)"/></param>
        /// <returns>Return true if the query execution is successfull.</returns>
        protected virtual bool CreatePermissionQuery(IDbCommand command, long permissionId, string permissionName)
        {
            this.PopulateCommand(command, @"INSERT INTO permission_types (id, name) VALUES(@id, @name)",
                    "id", permissionId,
                    "name", permissionName);
            return command.ExecuteNonQuery() == 1;
        }

        /// <summary>
        /// Delete an existing permission from database using standard SQL query
        /// </summary>
        /// This method is called by <see cref="DeletePermission"/>, ovveride this method if neccessary
        /// <param name="command">IDbCommand passed by <see cref="DeletePermission"/></param>
        /// <param name="permissionId">Permission Id passed by <see cref="DeletePermission"/></param>
        /// <returns>Return true if the permission is removed successfully</returns>
        protected virtual bool DeletePermissionQuery(IDbCommand command, long permissionId)
        {
            this.PopulateCommand(command, @"DELETE FROM permission_types WHERE id=@id", "id", permissionId);
            return command.ExecuteNonQuery() == 1;
        }

        /// <summary>
        /// Check whether a particular username has been granted to particular permission use standard SQL query
        /// </summary>
        /// This method is called by <see cref="HasPermission"/>, ovveride this method if neccessary
        /// <param name="command">IDbCommand passed by <see cref="HasPermission"/></param>
        /// <param name="username">Username passed by <see cref="HasPermission"/></param>
        /// <param name="permissionId">Permission Id passed by <see cref="HasPermission"/></param>
        /// <returns>Return true if username has that permission, otherwise it will return false</returns>
        protected virtual bool HasPermissionQuery(IDbCommand command, string username, long permissionId)
        {
            this.PopulateCommand(command,
                "select permission_type_id from users u inner join permissions p on u.id = p.id where login=@user and permission_type_id=@id;",
                "user", username,
                "id", permissionId);
            using (IDataReader reader = command.ExecuteReader())
            {
                return reader.Read();
            }
        }

        /// <summary>
        /// This method use standard SQL query which is used for granting a user with a specific permission, by adding the user id and the permission id into
        /// permissions table
        /// </summary>
        /// This method is called by <see cref="GrantPermission"/>, ovveride this method if neccessary
        /// <param name="command">IDbCommand passed by <see cref="GrantPermission"/></param>
        /// <param name="userId">User id passed by <see cref="GrantPermission"/></param>
        /// <param name="permissionId">Permission Id passed by <see cref="GrantPermission"/></param>
        /// <returns>Return true if the query execution is successfull</returns>
        protected virtual bool GrantPermissionQuery(IDbCommand command, long userId, long permissionId)
        {
            this.PopulateCommand(command, @"INSERT INTO permissions (id, permission_type_id) VALUES(@user_id, @permission_id)",
                "user_id",userId,
                "permission_id", permissionId);

            return command.ExecuteNonQuery() == 1;
        }

        /// <summary>
        /// This method use standard SQL query and have opposite responsibility from GrantPermissionQuery. This method is used to revoke the a particular
        /// permission from a particular user, by removing the userId and permissionId from permissions table
        /// </summary>
        /// This method is called by <see cref="RevokePermission"/>, ovveride this method if neccessary
        /// <param name="command">IDbCommand passed by <see cref="RevokePermission"/></param>
        /// <param name="userId">User Id passed by <see cref="RevokePermission"/></param>
        /// <param name="permissionId">Permission Id passed by <see cref="RevokePermission"/></param>
        /// <returns>Return true if the query execution is successfull.</returns>
        protected virtual bool RevokePermissionQuery(IDbCommand command, long userId, long permissionId)
        {
            this.PopulateCommand(command, @"DELETE FROM permissions WHERE id = @user_id AND permission_type_id = @permission_id",
                "user_id", userId,
                "permission_id", permissionId);

            return command.ExecuteNonQuery() == 1;
        }

        /// <summary>
        /// This method is used to check whether a particular permission Id is exist on the database.
        /// </summary>
        /// <param name="command">IDbCommand</param>
        /// <param name="permissionId">Permisison Id</param>
        /// <returns>Return true if the permission is exist.</returns>
        protected virtual bool DoesPermissionExistQuery(IDbCommand command, long permissionId)
        {
            this.PopulateCommand(command, @"SELECT id FROM permission_types WHERE id=@id", "id", permissionId);
            using (IDataReader reader = command.ExecuteReader())
            {
                return reader.Read();
            }
        }

        /// <summary>
        /// Gets the maximum id field for the given table.
        /// </summary>
        /// <param name="table">The table name.</param>
        /// <param name="tx">The DB transaction to execute within.</param>
        /// <returns>The maximum ID, (0 if the table is empty).</returns>
        protected long GetMaxId(string table, DbTransaction tx)
        {
            using (IDbCommand maxIdCommand = this.GetSqlCommand())
            {
                // note that we can't use paramaters here - so `table` should only ever be
                // hard-coded values, not taken from user input
                this.PopulateCommand(maxIdCommand, "SELECT MAX(id) FROM " + table);
                maxIdCommand.Transaction = tx;
                long newId = 0;
                using (IDataReader maxIdReader = maxIdCommand.ExecuteReader())
                {
                    if (maxIdReader.Read())
                    {
                        if (!maxIdReader.IsDBNull(0))
                        {
                            if (maxIdReader[0] is Int32)
                            {
                                newId = maxIdReader.GetInt32(0);
                                newId++;
                            }
                            else
                            {
                                newId = maxIdReader.GetInt64(0);
                                newId++;
                            }
                        }
                    }
                }
                return newId;
            }
        }

        private static String GetHashOfString(String data)
        {
            SHA1 sha = new SHA1Managed();
            byte[] result;

            sha.Initialize();

            result = sha.ComputeHash(Encoding.ASCII.GetBytes(data));
            return SqlAccountManager.GetString(result);
        }

        private static string GetString(byte[] bytes)
        {
            int length = bytes.Length * 2;
            StringBuilder builder = new StringBuilder(length);

            foreach (byte b in bytes)
            {
                builder.AppendFormat("{0:X2}", b);
            }

            return builder.ToString(0, length);
        }

        private bool DoesPermissionExist(long permissionId)
        {
            if (permissionId < 0)
            {
                return false;
            }

            try
            {
                using (IDbCommand command = this.GetSqlCommand())
                {
                    if (command != null)
                    {
                        return this.DoesPermissionExistQuery(command, permissionId);
                    }
                }
            }
            catch (Exception e)
            {
                Trace.TraceError("Exception whilst trying to query for permission existance : {0}", e.Message);
            }

            return false;
        }

        private IDbCommand GetSqlCommand(IDbTransaction transaction)
        {
            IDbCommand command = this.GetSqlCommand();
            command.Transaction = transaction;
            return command;
        }

        private IDbCommand GetSqlCommand()
        {
            DbConnection connection = this.GetConnection();
            if (connection != null)
            {
                IDbCommand queryCommand = connection.CreateCommand();

                return queryCommand;
            }

            throw new ExternalException("Failed to get sql command");
        }


        /// <summary>
        /// Populates the command text of an IDbCommand, using parameters to protect against SQL injection.
        /// </summary>
        /// <param name="command">The command.</param>
        /// <param name="query">The query.</param>
        /// <param name="args">A series of alternating key and value pairs to use as query parameters.</param>
        ///
        /// Example usage:
        /// PopulateCommand(command, "SELECT * from users where id = @id" and name = @name",
        ///     "id", userId,
        ///     "name", userName);
        protected void PopulateCommand(IDbCommand command, string query, params object[] args)
        {
            if (args.Length % 2 != 0)
            {
                throw new ArgumentException("PopulateCommand received an odd number of parameter arguments (key-value pairs are required)");
            }

            command.CommandText = query;
            for (int i = 0; i < args.Length; i += 2)
            {
                string name = args[i] as string;
                if (string.IsNullOrEmpty(name))
                {
                    throw new ArgumentException("parameter name must be a non-empty string");
                }

                var param = command.CreateParameter();
                param.ParameterName = name;
                param.Value = args[i + 1];
                command.Parameters.Add(param);
            }
        }

        private DbConnection GetConnection()
        {
            if (this.mConnection != null && !(this.mConnection.State == ConnectionState.Closed || this.mConnection.State == ConnectionState.Broken))
            {
                return this.mConnection;
            }

            DbConnection connection = this.CreateConnection();
            if (connection == null)
            {
                throw new ExternalException("Failed to get sql connection");
            }

            connection.Open();
            this.mConnection = connection;
            return this.mConnection;
        }

        /// <summary>
        /// Dispose SqlAccountManager instance, close the connection to database, if it still connected to database.
        /// </summary>
        public void Dispose()
        {
            if (this.mConnection != null)
            {
                this.mConnection.Close();
                this.mConnection = null;
            }
        }

        private void HandleException(string desc, Exception e)
        {
            // Uncomment the line below to throw all exceptions up to caller - useful for debugging.
            //throw new Exception("Exception whilst trying to " + desc, e);
            Trace.TraceError("Exception whilst trying to {0} : {1}", desc, e.Message);
        }
    }
}
