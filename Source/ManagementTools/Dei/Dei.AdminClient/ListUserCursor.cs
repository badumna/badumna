//-----------------------------------------------------------------------
// <copyright file="ListUserCursor.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace Dei.AdminClient
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// ListUserCursor has responsibility to display the user list from the dei server.
    /// Mostly used as part as the search user query.
    /// </summary>
    public class ListUserCursor
    {
        /// <summary>
        /// Dei client.
        /// </summary>
        private IClient client;

        /// <summary>
        /// Glob expression.
        /// </summary>
        private string globExpression;

        /// <summary>
        /// Initializes a new instance of the <see cref="ListUserCursor"/> class.
        /// </summary>
        /// <param name="client">Dei client.</param>
        /// <param name="globExpression">Glob expression.</param>
        /// <param name="maximumNumberOfResults">Maximum number of result.</param>
        internal ListUserCursor(IClient client, string globExpression, long maximumNumberOfResults)
        {
            this.client = client;
            this.RangeStart = 0;
            this.globExpression = globExpression;
            this.MaximumNumberOfResults = maximumNumberOfResults;

            try
            {
                this.Count = this.client.GetUserCount(globExpression);
            }
            catch (Exception e)
            {
                System.Diagnostics.Trace.TraceError("Exception while trying to get user count : {0}", e.Message);
            }
        }

        /// <summary>
        /// Gets the number of user count. 
        /// </summary>
        public long Count { get; private set; }

        /// <summary>
        /// Gets the maximum number of result.
        /// </summary>
        public long MaximumNumberOfResults { get; private set; }

        /// <summary>
        /// Gets the range start.
        /// </summary>
        public long RangeStart { get; private set; }

        /// <summary>
        /// Gets a value indicating whether there is a next value.
        /// </summary>
        public bool HasNext
        {
            get { return this.Count > this.RangeStart + this.MaximumNumberOfResults; }
        }

        /// <summary>
        /// Gets a value indicating whether there is a previous value.
        /// </summary>
        public bool HasPrevious
        {
            get { return this.RangeStart > 0; }
        }

        /// <summary>
        /// Get the list of users from database that match with the glob expression given.
        /// </summary>
        /// <returns>Return the list of users that match with the glob expression.</returns>
        public IList<string> GetResults()
        {
             return this.client.ListUsers(this.globExpression, this.RangeStart, this.MaximumNumberOfResults);
        }

        /// <summary>
        /// Go to the next page.
        /// </summary>
        public void Next()
        {
            this.RangeStart += this.MaximumNumberOfResults;
            if (this.RangeStart > this.Count)
            {
                this.RangeStart = 0;
            }
        }

        /// <summary>
        /// Go to the previous page.
        /// </summary>
        public void Previous()
        {
            this.RangeStart = Math.Max(0, this.RangeStart - this.MaximumNumberOfResults);
        }
    }
}
