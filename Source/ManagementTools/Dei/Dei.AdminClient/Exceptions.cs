//-----------------------------------------------------------------------
// <copyright file="Exceptions.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace Dei.AdminClient
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// Dei exception class.
    /// </summary>
    public class DeiException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DeiException"/> class.
        /// </summary>
        /// <param name="message">Exception message.</param>
        internal DeiException(string message)
            : base(message) 
        { 
        }
    }

    /// <summary>
    /// User name conflict exception derived from Dei exception.
    /// </summary>
    /// Exception is thrown when the given username conflicts with another.
    public class UserNameConflictException : DeiException
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UserNameConflictException"/> class.
        /// </summary>
        public UserNameConflictException()
            : base("The given username conflicts with another")
        { 
        }
    }

    /// <summary>
    /// No such user exception derived from Dei exception.
    /// </summary>
    /// Exception is thrown when the username cannot be found.
    public class NoSuchUserException : DeiException
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="NoSuchUserException"/> class.
        /// </summary>
        public NoSuchUserException()
            : base("No such username can be found")
        { 
        }
    }

    /// <summary>
    /// Permission exception derived from Dei excpetion.
    /// </summary>
    /// Exception is thrown when permission was denied.
    public class PermissionException : DeiException
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PermissionException"/> class.
        /// </summary>
        public PermissionException()
            : base("Permission was denied")
        { 
        }
    }

    /// <summary>
    /// No such permission exception derived from Dei exception.
    /// </summary>
    /// Exception is thrown when the specified permission cannot be found.
    public class NoSuchPermissionException : DeiException
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="NoSuchPermissionException"/> class.
        /// </summary>
        public NoSuchPermissionException()
            : base("The specified permission was not found")
        { 
        }
    }

    /// <summary>
    /// Permission conflict exception derived from Dei exception.
    /// </summary>
    /// Exception is thrown when the given permission conflicts with another.
    public class PermissionConflictException : DeiException
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PermissionConflictException"/> class.
        /// </summary>
        public PermissionConflictException()
            : base("The given permission conflicts with another")
        { 
        }
    }
}
