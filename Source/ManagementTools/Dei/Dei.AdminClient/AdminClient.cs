//-----------------------------------------------------------------------
// <copyright file="AdminClient.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace Dei.AdminClient
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Net;
    using System.Net.Security;
    using System.Net.Sockets;
    using System.Security.Authentication;
    using System.Security.Cryptography;
    using System.Security.Cryptography.X509Certificates;
    using System.Text;
    using Dei;

    /// <summary>
    /// AdminClient class has responsibility to talk to the Dei Server and do some account administration 
    /// stuff including account creation, modification and deletion.
    /// </summary>
    public class AdminClient : IDisposable
    {
        /// <summary>
        /// Ignore Ssl errors.
        /// </summary>
        private bool ignoreSslErrors = false;

        /// <summary>
        /// Ignore Ssl certificate name mismatch.
        /// </summary>
        private bool ignoreSslCertificateNameMismatch = false;

        /// <summary>
        /// Ignore Ssl self signed certificate errors.
        /// </summary>
        private bool ignoreSslSelfSignedCertificateErrors = false;

        /// <summary>
        /// Client user name.
        /// </summary>
        private string username;

        /// <summary>
        /// Dei client.
        /// </summary>
        private IClient client;

        /// <summary>
        /// A value indicating whether ssl connection is used.
        /// </summary>
        private bool useSslConnection;
        
        /// <summary>
        /// The most recent login result.
        /// </summary>
        private LoginResult loginResult;

        /// <summary>
        /// Initializes a new instance of the <see cref="AdminClient"/> class.
        /// </summary>
        public AdminClient()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AdminClient"/> class.
        /// </summary>
        /// This is used for dependency injection for unit test purposes.
        /// <param name="client">Dei client.</param>
        internal AdminClient(IClient client)
        {
            this.client = client;
        }

        /// <summary>
        /// Gets or sets a value indicating whether Ssl errors is ignored.
        /// </summary>
        public bool IgnoreSslErrors 
        {
            get { return this.ignoreSslErrors; }
            set { this.ignoreSslErrors = value; } 
        }

        /// <summary>
        /// Gets or sets a value indicating whether the Ssl certificate name mismatch is ignored.
        /// </summary>
        public bool IgnoreSslCertificateNameMismatch 
        {
            get { return this.ignoreSslCertificateNameMismatch; }
            set { this.ignoreSslCertificateNameMismatch = value; } 
        }

        /// <summary>
        /// Gets or sets a value indicating whether the Ssl self signed certificate errors is ignored.
        /// </summary>
        public bool IgnoreSslSelfSignedCertificateErrors 
        {
            get { return this.ignoreSslSelfSignedCertificateErrors; }
            set { this.ignoreSslSelfSignedCertificateErrors = value; } 
        }

        /// <summary>
        /// Gets or sets the remote certificate validation callback.
        /// </summary>
        public RemoteCertificateValidationCallback CertificateValidationCallback { get; set; }
        
        /// <summary>
        /// Gets the failure reason.
        /// </summary>
        public FailureReasons FailureReason { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the connection is closed.
        /// </summary>
        public bool IsClosed 
        { 
            get { return this.client == null || this.client.IsClosed; } 
        }

        /// <summary>
        /// Gets a value indicating whether the client is logged in.
        /// </summary>
        private bool IsLoggedIn
        {
            get { return this.loginResult == null ? false : this.loginResult.WasSuccessful; }
        }

        /// <summary>
        /// Login to the dei server.
        /// </summary>
        /// <param name="hostname">Dei server hostname.</param>
        /// <param name="port">Dei server port number.</param>
        /// <param name="username">User name.</param>
        /// <param name="password">User password.</param>
        /// <param name="useSslConnection">A value indicating whether Ssl connection is used.</param>
        /// <returns>Return true on success.</returns>
        public bool Login(string hostname, int port, string username, string password, bool useSslConnection)
        {
            this.useSslConnection = useSslConnection;
            return this.Login(hostname, port, username, password);
        }

        /// <summary>
        /// Login to the dei server.
        /// </summary>
        /// <param name="hostname">Dei server hostname.</param>
        /// <param name="port">Dei server port number.</param>
        /// <param name="username">User name.</param>
        /// <param name="password">User password.</param>
        /// <returns>Return true on success.</returns>
        public bool Login(string hostname, int port, string username, string password)
        {
            if (this.IsLoggedIn)
            {
                return true;
            }

            this.ResetErrorState();
            try
            {
                TcpClient connection = new TcpClient(hostname, port);
                if (this.useSslConnection)
                {
                    SslStream secureConnection = new SslStream(connection.GetStream(), false, this.ValidateServerCertificate);

                    secureConnection.AuthenticateAsClient(hostname, null, SslProtocols.Tls, false);

                    // When the class under test, do not create a new client intance.
                    if (this.client == null)
                    {
                        this.client = new Client(secureConnection);
                    }
                }
                else
                {
                    if (this.client == null)
                    {
                        this.client = new Client(connection.GetStream());
                    }
                }
            }
            catch (IOException e)
            {
                if (e.InnerException != null)
                {
                    Trace.TraceError("Failed to connect to Dei server : {0}", e.InnerException.Message);
                }
                else
                {
                    Trace.TraceError("Failed to connect to Dei server : {0}", e.Message);
                }

                this.HandleFailure("Login", FailureReasons.ConnectionError);
                return false;
            }
            catch (SocketException e)
            {
                if (e.InnerException != null)
                {
                    Trace.TraceError("Failed to connect to Dei server : {0}", e.InnerException.Message);
                }
                else
                {
                    Trace.TraceError("Failed to connect to Dei server : {0}", e.Message);
                }

                this.HandleFailure("Login", FailureReasons.ConnectionError);
                return false;
            }
            catch (AuthenticationException e)
            {
                if (e.InnerException != null)
                {
                    Trace.TraceError("Failed to connect to Dei server : {0}", e.InnerException.Message);
                }
                else
                {
                    Trace.TraceError("Failed to connect to Dei server : {0}", e.Message);
                }

                this.HandleFailure("Login", FailureReasons.ConnectionError);
                return false;
            }

            try
            {
                this.loginResult = this.client.Login(username, password);
                if(this.loginResult.WasSuccessful)
                {
                    this.username = username;
                }
                else
                {
                    this.HandleFailure("Login", this.client.FailureReason);
                }

                return this.loginResult.WasSuccessful;
            }
            catch (VersionException)
            {
                Trace.TraceError("Failed to login to Dei : Protocol version is incompatible");
            }
            catch (ProtocolException)
            {
                Trace.TraceError("Failed to login to Dei : Authentication protocol error.");
            }
            catch
            {
                this.client.Logout();
                Trace.TraceError("Failed to login to Dei : Unknown login error");
            }

            this.HandleFailure("Login", FailureReasons.ConnectionError);
            this.client = null;
            return false;
        }

        /// <summary>
        /// Close the connection to dei server.
        /// </summary>
        public void Close()
        {
            this.loginResult = null;
            try
            {
                if (this.client != null)
                {
                    this.client.Logout();
                }
            }
            catch 
            {
            }

            this.client = null;
        }

        /// <summary>
        /// Create a new dei user.
        /// </summary>
        /// <param name="username">User name.</param>
        /// <param name="password">User password.</param>
        /// <returns>Return true on success.</returns>
        public bool CreateUser(string username, string password)
        {
            if (string.IsNullOrEmpty(username))
            {
                throw new ArgumentException("Username is invalid");
            }

            if (string.IsNullOrEmpty(password))
            {
                throw new ArgumentException("Password is invalid");
            }

            if (this.client == null)
            {
                throw new InvalidOperationException("Must be logged in before performing any Dei operations.");
            }

            this.ResetErrorState();
            if (this.client.CreateNewAccount(username, password))
            {
                return true;
            }

            this.HandleFailure("Add user", this.client.FailureReason);
            return false;
        }

        /// <summary>
        /// Remove user from database.
        /// </summary>
        /// <param name="username">User name.</param>
        /// <returns>Return true on success.</returns>
        public bool RemoveUser(string username)
        {
            if (string.IsNullOrEmpty(username))
            {
                throw new ArgumentException("Username is invalid");
            }

            if (this.client == null)
            {
                throw new InvalidOperationException("Must be logged in before performing any Dei operations.");
            }

            this.ResetErrorState();
            if (!this.client.DeleteAccount(username))
            {
                this.HandleFailure("Remove user", this.client.FailureReason);
                return false;
            }

            return true;
        }

        /// <summary>
        /// Change user password.
        /// </summary>
        /// <param name="password">New password.</param>
        /// <returns>Return true on success.</returns>
        public bool ChangePassword(string password)
        {
            if (string.IsNullOrEmpty(password))
            {
                throw new ArgumentException("Password is invalid");
            }

            if (this.client == null)
            {
                throw new InvalidOperationException("Must be logged in before performing any Dei operations.");
            }

            this.ResetErrorState();
            if (this.client.ChangeAccount(this.username, password))
            {
                return true;
            }

            this.HandleFailure("Change password", this.client.FailureReason);
            return false;
        }

        /// <summary>
        /// Blacklist given user name (currently unused, it will always return false and do nothing).
        /// </summary>
        /// <param name="username">User name.</param>
        /// <returns>Return true on success.</returns>
        public bool BlacklistUser(string username)
        {
            this.ResetErrorState();
            return false;
        }

        /// <summary>
        /// Find users from database.
        /// </summary>
        /// <param name="usernameGlobExpression">User name glob expression.</param>
        /// <param name="maximumNumberOfResults">Maximum number of result.</param>
        /// <returns>Return the list user cursor object.</returns>
        public ListUserCursor FindUsers(string usernameGlobExpression, long maximumNumberOfResults)
        {
            if (this.client == null)
            {
                throw new InvalidOperationException("Must be logged in before performing any Dei operations.");
            }

            if (string.IsNullOrEmpty(usernameGlobExpression))
            {
                throw new ArgumentException("usernameGlobExpression must not be null or empty");
            }

            if (maximumNumberOfResults < 1)
            {
                throw new ArgumentException("maximumNumberOfResults must be greater than 0");
            }

            this.ResetErrorState();
            return new ListUserCursor(this.client, usernameGlobExpression, maximumNumberOfResults);
        }

        /// <summary>
        /// Count the number of users.
        /// </summary>
        /// <returns>Return the current number of users.</returns>
        public long UserCount()
        {
            if (this.client == null)
            {
                throw new InvalidOperationException("Must be logged in before performing any Dei operations.");
            }

            this.ResetErrorState();
            return this.client.GetUserCount("*");
        }

        /// <summary>
        /// List all available permission.
        /// </summary>
        /// <returns>Return the list of permission.</returns>
        public IList<PermissionProperties> ListAllPermissions()
        {
            if (this.client == null)
            {
                throw new InvalidOperationException("Must be logged in before performing any Dei operations.");
            }

            this.ResetErrorState();
            List<PermissionProperties> returnList = new List<PermissionProperties>(this.client.GetPermissionTypes(string.Empty));

            return returnList;
        }

        /// <summary>
        /// List of permission that a specific user has.
        /// </summary>
        /// <param name="username">User name.</param>
        /// <returns>Return the list of permission of the given user.</returns>
        public IList<PermissionProperties> ListPermissions(string username)
        {
            if (this.client == null)
            {
                throw new InvalidOperationException("Must be logged in before performing any Dei operations.");
            }

            if (string.IsNullOrEmpty(username))
            {
                throw new ArgumentException("Username is invalid");
            }

            this.ResetErrorState();
            return this.client.GetPermissionTypes(username);
        }

        /// <summary>
        /// Check whether a specific user has a particular permission.
        /// </summary>
        /// <param name="username">User name.</param>
        /// <param name="permissionId">Permission id.</param>
        /// <returns>Return true if the user has permission.</returns>
        public bool HasPermission(string username, int permissionId)
        {
            this.ResetErrorState();
            foreach (PermissionProperties permission in this.ListPermissions(username))
            {
                if (permission.Id == permissionId)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Create a new permission.
        /// </summary>
        /// <param name="permissionName">New permission name.</param>
        /// <returns>Return the permission properties.</returns>
        public PermissionProperties CreatePermission(string permissionName)
        {
            if (this.client == null)
            {
                throw new InvalidOperationException("Must be logged in before performing any Dei operations.");
            }

            this.ResetErrorState();

            if (string.IsNullOrEmpty(permissionName))
            {
                throw new ArgumentException("permission name cannot be null or empty");
            }

            long permissionId = this.client.CreatePermission(permissionName);
            if (permissionId > 0)
            {
                return new PermissionProperties(permissionId, permissionName);
            }

            this.HandleFailure("Create permission", this.client.FailureReason);
            return null;
        }

        /// <summary>
        /// Remove existing permission.
        /// </summary>
        /// <param name="permissionId">Permission id.</param>
        /// <returns>Return true on success.</returns>
        public bool RemovePermission(long permissionId)
        {
            if (this.client == null)
            {
                throw new InvalidOperationException("Must be logged in before performing any Dei operations.");
            }

            this.ResetErrorState();

            if (permissionId < (long)PermissionTypes.EndBuiltinMarker_DoNotUseAsPermission)
            {
                this.HandleFailure("Remove permission", FailureReasons.InvalidRequest); // Do not change built in permissions from here.
                return false;
            }

            if (this.client.RemovePermission(permissionId))
            {
                return true;
            }

            this.HandleFailure("Remove permission", this.client.FailureReason);
            return false;
        }

        /// <summary>
        /// Grant a particular permission to a specific user.
        /// </summary>
        /// <param name="username">User name.</param>
        /// <param name="permissionId">Permission id.</param>
        /// <returns>Return true on success.</returns>
        public bool GrantPermission(string username, long permissionId)
        {
            if (this.client == null)
            {
                throw new InvalidOperationException("Must be logged in before performing any Dei operations.");
            }

            if (string.IsNullOrEmpty(username))
            {
                throw new ArgumentException("Username is invalid");
            }

            if (permissionId < 1)
            {
                throw new ArgumentException("Permission Id is invalid");
            }

            this.ResetErrorState();

            if (!this.client.GrantPermission(username, permissionId))
            {
                this.HandleFailure("GrantPermission", this.client.FailureReason);
                return false;
            }

            return true;
        }

        /// <summary>
        /// Revoke a particular permission from a specific user.
        /// </summary>
        /// <param name="username">User name.</param>
        /// <param name="permissionId">Permission id.</param>
        /// <returns>Return true on success.</returns>
        public bool RevokePermission(string username, long permissionId)
        {
            if (this.client == null)
            {
                throw new InvalidOperationException("Must be logged in before performing any Dei operations.");
            }

            if (string.IsNullOrEmpty(username))
            {
                throw new ArgumentException("Username is invalid");
            }

            if (permissionId < 1)
            {
                throw new ArgumentException("Permission Id is invalid");
            }

            this.ResetErrorState();

            if (!this.client.RevokePermission(username, permissionId))
            {
                this.HandleFailure("RevokePermission", this.client.FailureReason);
                return false;
            }

            return true;
        }

        /// <summary>
        /// Dispose the admin client object.
        /// </summary>
        public void Dispose()
        {
            this.Close();
        }

        /// <summary>
        /// Validate the server certificate.
        /// </summary>
        /// <param name="sender">Object sender.</param>
        /// <param name="certificate">X509 certificate.</param>
        /// <param name="chain">X509 chain.</param>
        /// <param name="sslPolicyErrors">Ssl policy errors.</param>
        /// <returns>Return true when the certificate is valid.</returns>
        private bool ValidateServerCertificate(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            // Supplying a certification validation callback overrides any other options
            if (this.CertificateValidationCallback != null)
            {
                return this.CertificateValidationCallback(sender, certificate, chain, sslPolicyErrors);
            }

            // Succeed if no errors, or errors are ignored.
            if ((sslPolicyErrors == SslPolicyErrors.None) || this.ignoreSslErrors)
            {
                return true;
            }

            // Fail if remote certificate is not available
            if ((sslPolicyErrors & SslPolicyErrors.RemoteCertificateNotAvailable) == SslPolicyErrors.RemoteCertificateNotAvailable)
            {
                return false;
            }

            // Fail if certificate name does not match (unless this error is ignored).
            if ((sslPolicyErrors & SslPolicyErrors.RemoteCertificateNameMismatch) == SslPolicyErrors.RemoteCertificateNameMismatch &&
                this.ignoreSslCertificateNameMismatch == false)
            {
                return false;
            }

            // Fail if there are certificate chain errors, unless self-signed certificates are permitted
            // and the certificate is self-signed.
            if ((sslPolicyErrors & SslPolicyErrors.RemoteCertificateChainErrors) == SslPolicyErrors.RemoteCertificateChainErrors)
            {
                if (this.ignoreSslSelfSignedCertificateErrors != true ||
                    chain.ChainStatus.Length != 1 ||
                    certificate.Subject != certificate.Issuer ||
                    chain.ChainStatus[0].Status != X509ChainStatusFlags.UntrustedRoot)
                {
                    return false;
                }
            }

            // If none of the above conditions triggered failure, validation succeeds
            // (i.e. it was permitted to ignore any errors found).
            return true;
        }

        /// <summary>
        /// Reset the error state.
        /// </summary>
        private void ResetErrorState()
        {
            this.FailureReason = FailureReasons.NoneGiven;
        }

        /// <summary>
        /// Handle or intepret the failure reason.
        /// </summary>
        /// <param name="operationDescription">Operation description.</param>
        /// <param name="reason">Failure reason.</param>
        private void HandleFailure(string operationDescription, FailureReasons reason)
        {
            switch (reason)
            {
                case FailureReasons.NoneGiven:
                    Trace.TraceError("Dei {0} error : no reason given", operationDescription);
                    break;

                case FailureReasons.PermissionDenied:
                    Trace.TraceError("Dei {0} error : permission denied", operationDescription);
                    break;

                case FailureReasons.TooManyCharactersInLogin:
                    Trace.TraceError("Dei {0} error : username is too long", operationDescription);
                    break;

                case FailureReasons.InvalidRequest:
                    Trace.TraceError("Dei {0} error : invalid request", operationDescription);
                    break;

                case FailureReasons.AuthenticationFailed:
                    Trace.TraceError("Dei {0} error : authentication failed", operationDescription);
                    break;

                case FailureReasons.AccountAlreadyExists:
                    Trace.TraceError("Dei {0} error : account already exists", operationDescription);
                    break;

                case FailureReasons.ConnectionError:
                    Trace.TraceError("Dei {0} error : connection error.", operationDescription);
                    break;
            }

            this.FailureReason = reason;
        }
    }
}
