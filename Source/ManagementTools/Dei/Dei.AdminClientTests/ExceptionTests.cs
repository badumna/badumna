//-----------------------------------------------------------------------
// <copyright file="ExceptionTests.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace Dei.AdminClientTests
{
    using System;
    using System.Collections.Generic;
    using Dei.AdminClient;
    using NUnit.Framework;

    [TestFixture]
    internal class ExceptionTests
    {
        public ExceptionTests()
        {
        }

        [Test]
        [ExpectedException(typeof(UserNameConflictException))]
        public void UsernameConflictExceptionShouldWorks()
        {
            throw new UserNameConflictException();
        }

        [Test]
        [ExpectedException(typeof(NoSuchUserException))]
        public void NoSuchUserExceptionShouldWorks()
        {
            throw new NoSuchUserException();
        }

        [Test]
        [ExpectedException(typeof(PermissionException))]
        public void PermissionExceptionShouldWorks()
        {
            throw new PermissionException();
        }

        [Test]
        [ExpectedException(typeof(NoSuchPermissionException))]
        public void NoSuchPermissionExceptionShouldWorks()
        {
            throw new NoSuchPermissionException();
        }

        [Test]
        [ExpectedException(typeof(PermissionConflictException))]
        public void PermissionConflictExceptionShouldWorks()
        {
            throw new PermissionConflictException();
        }
    }
}
