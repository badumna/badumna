//-----------------------------------------------------------------------
// <copyright file="AdminClientTests.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Dei.AdminClientTests
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using Dei;
    using Dei.AdminClient;
    using NUnit.Framework;
    using Rhino.Mocks;

    [TestFixture]
    internal class AdminClientTests
    {
        private IClient client;

        private UserProperties user;

        private List<PermissionProperties> expectedPermissionList = 
            new List<PermissionProperties>() 
            { 
                new PermissionProperties(PermissionTypes.Participation),
                new PermissionProperties(PermissionTypes.Admin)
            };

        public AdminClientTests()
        {
        }

        [SetUp]
        public void SetUp()
        {
            this.client = MockRepository.GenerateMock<IClient>();
            this.user = new UserProperties("user", "password");
        }

        [TearDown]
        public void TearDown()
        {
            this.client = null;
            this.user = null;
        }

        [Test]
        public void AdminClientConstructorWorks()
        {
            AdminClient adminClient = new AdminClient();
            Assert.IsNotNull(adminClient);
        }

        [Test]
        public void AdminClientConstructorWithClientParameterWorks()
        {
            AdminClient adminClient = new AdminClient(this.client);
            Assert.IsNotNull(adminClient);
        }

        [Test]
        public void DisposeAdminClientAndMakeSureTheConnectionIsClosed()
        {
            AdminClient adminClient = new AdminClient();
            adminClient.Dispose();

            Assert.IsTrue(adminClient.IsClosed);
        }

        [Test]
        public void IsClosedWithDependencyInjectionWorks()
        {
            AdminClient adminClient = new AdminClient(this.client);

            this.client.Expect(c => c.IsClosed).Return(true).Repeat.Once();
            Assert.IsTrue(adminClient.IsClosed);
            this.client.VerifyAllExpectations();
        }

        [Test]
        public void CloseShouldCallClientLogout()
        {
            AdminClient adminClient = new AdminClient(this.client);

            this.client.Expect(c => c.Logout()).Repeat.Once();
            adminClient.Close();
            this.client.VerifyAllExpectations();
        }

        [TestCase(new object[] { "", "password" })]
        [TestCase(new object[] { "user", "" })]
        [ExpectedException(typeof(ArgumentException))]
        public void CreateUserWithInvalidInputsShouldThrowArgumentException(string username, string password)
        {
            AdminClient adminClient = new AdminClient(this.client);
            adminClient.CreateUser(username, password);
        }

        [Test]
        public void CreateUserWithValidInputsShouldWorksAndReturnTrue()
        {
            AdminClient adminClient = new AdminClient(this.client);
            this.client.Expect(c => c.CreateNewAccount(this.user.Login, this.user.Password))
                .Return(true).Repeat.Once();
            Assert.IsTrue(adminClient.CreateUser(this.user.Login, this.user.Password));
            this.client.VerifyAllExpectations();
        }

        [Test]
        [ExpectedException(typeof(InvalidOperationException))]
        public void CreateUserWithValidInputAndNullClientShouldThrowInvalidOperationException()
        {
            AdminClient adminClient = new AdminClient(null);
            adminClient.CreateUser(this.user.Login, this.user.Password);
        }

        [Test]
        public void CreateUserWithValidInputButFailToCreateUserShouldReturnFalse()
        {
            FailureReasons failureReason = FailureReasons.AccountAlreadyExists;
            AdminClient adminClient = new AdminClient(this.client);
            this.client.Expect(c => c.CreateNewAccount(this.user.Login, this.user.Password))
                .Return(false).Repeat.Once();
            this.client.Expect(c => c.FailureReason)
                .Return(failureReason).Repeat.Once();
            Assert.IsFalse(adminClient.CreateUser(this.user.Login, this.user.Password));
            Assert.AreEqual(failureReason, adminClient.FailureReason);
            this.client.VerifyAllExpectations();
        }

        [Test]
        [ExpectedException(typeof(ArgumentException))]
        public void RemoveUserWithInvalidInputShouldThrowArgumentException()
        {
            AdminClient adminClient = new AdminClient(this.client);
            adminClient.RemoveUser(string.Empty);
        }

        [Test]
        [ExpectedException(typeof(InvalidOperationException))]
        public void RemoveUserWithValidInputAndNullClientShouldThrowInvalidOperationException()
        {
            AdminClient adminClient = new AdminClient(null);
            adminClient.RemoveUser(this.user.Login);
        }

        [Test]
        public void RemoveUserWithValidInputAndShouldReturnTrue()
        {
            AdminClient adminClient = new AdminClient(this.client);
            this.client.Expect(c => c.DeleteAccount(this.user.Login)).Return(true).Repeat.Once();
            Assert.IsTrue(adminClient.RemoveUser(this.user.Login));
            this.client.VerifyAllExpectations();
        }

        [Test]
        public void RemoveUserWithValidInputButFailToDeleteAccountShouldReturnFalse()
        {
            FailureReasons failureReason = FailureReasons.PermissionDenied;
            AdminClient adminClient = new AdminClient(this.client);
            this.client.Expect(c => c.DeleteAccount(this.user.Login)).Return(false).Repeat.Once();
            this.client.Expect(c => c.FailureReason).Return(failureReason).Repeat.Once();
            Assert.IsFalse(adminClient.RemoveUser(this.user.Login));
            Assert.AreEqual(failureReason, adminClient.FailureReason);
            this.client.VerifyAllExpectations();
        }

        [Test]
        [ExpectedException(typeof(ArgumentException))]
        public void ChangePasswordWithInvalidInputShouldThrowArgumentException()
        {
            AdminClient adminClient = new AdminClient(this.client);
            adminClient.ChangePassword(string.Empty);
        }

        [Test]
        [ExpectedException(typeof(InvalidOperationException))]
        public void ChangePasswordWithValidInputAndNullClientShouldThrowInvalidOperationException()
        {
            AdminClient adminClient = new AdminClient(null);
            adminClient.ChangePassword(this.user.Password);
        }

        [Test]
        public void ChangePasswordWithValidInputAndShouldReturnTrue()
        {
            AdminClient adminClient = new AdminClient(this.client);
            this.client.Expect(c => c.ChangeAccount(this.user.Login, this.user.Password))
                .IgnoreArguments().Return(true).Repeat.Once();
            Assert.IsTrue(adminClient.ChangePassword(this.user.Password));
            this.client.VerifyAllExpectations();
        }

        [Test]
        public void ChangePasswordWithValidInputButFailToDeleteAccountShouldReturnFalse()
        {
            FailureReasons failureReason = FailureReasons.AuthenticationFailed;
            AdminClient adminClient = new AdminClient(this.client);
            this.client.Expect(c => c.ChangeAccount(this.user.Login, this.user.Password))
                .IgnoreArguments().Return(false).Repeat.Once();
            this.client.Expect(c => c.FailureReason).Return(failureReason).Repeat.Once();
            Assert.IsFalse(adminClient.ChangePassword(this.user.Password));
            Assert.AreEqual(failureReason, adminClient.FailureReason);
            this.client.VerifyAllExpectations();
        }

        [Test]
        public void BlacklistUsertIsNotImplementedShouldReturnFalse()
        {
            AdminClient adminClient = new AdminClient(this.client);
            Assert.IsFalse(adminClient.BlacklistUser(this.user.Login));
        }

        [TestCase(new object[] { "", 1 })]
        [TestCase(new object[] { "*", 0 })]
        [ExpectedException(typeof(ArgumentException))]
        public void FindUsersWithInvalidInputsShouldThrowsArgumentException(
            string usernameGlobExpression, long maximumNumberOfResults)
        {
            AdminClient adminClient = new AdminClient(this.client);
            adminClient.FindUsers(usernameGlobExpression, maximumNumberOfResults);
        }

        [Test]
        [ExpectedException(typeof(InvalidOperationException))]
        public void FindUsersWithValdInputsAndNullClientShouldThrowsArgumentException()
        {
            AdminClient adminClient = new AdminClient(null);
            adminClient.FindUsers("*", 10);
        }

        [Test]
        public void FindUsersWithValidInputsShouldReturnValidListUserCursor()
        {
            string globExpression = "*";
            int numberOfUser = 10;
            AdminClient adminClient = new AdminClient(this.client);
            this.client.Expect(c => c.GetUserCount(globExpression)).Return(numberOfUser).Repeat.Once();
            Assert.IsNotNull(adminClient.FindUsers(globExpression, numberOfUser));
            this.client.VerifyAllExpectations();
        }

        [Test]
        [ExpectedException(typeof(InvalidOperationException))]
        public void UserCountWithNullClientShouldThrowsInvalidOperationException()
        {
            AdminClient adminClient = new AdminClient(null);
            adminClient.UserCount();
        }

        [Test]
        public void UserCountShouldReturnTheExpectedNumberOfUser()
        {
            int expectedUserCount = 10;
            AdminClient adminClient = new AdminClient(this.client);
            this.client.Expect(c => c.GetUserCount("*")).Return(expectedUserCount).Repeat.Once();
            Assert.AreEqual(expectedUserCount, adminClient.UserCount());
            this.client.VerifyAllExpectations();
        }

        [Test]
        [ExpectedException(typeof(InvalidOperationException))]
        public void ListAllPermissionsWithNullClientShouldThrowsInvalidOperationException()
        {
            AdminClient adminClient = new AdminClient(null);
            adminClient.ListAllPermissions();
        }

        [Test]
        public void ListAllPermissionsShouldReturnExpectedPermissionList()
        {
            AdminClient adminClient = new AdminClient(this.client);
            this.client.Expect(c => c.GetPermissionTypes(string.Empty))
                .Return(this.expectedPermissionList).Repeat.Once();
            Assert.AreEqual(this.expectedPermissionList, adminClient.ListAllPermissions());
            this.client.VerifyAllExpectations();
        }

        [Test]
        [ExpectedException(typeof(ArgumentException))]
        public void ListPermissionsWithInvalidInputShouldThrowsArgumentException()
        {
            AdminClient adminClient = new AdminClient(this.client);
            adminClient.ListPermissions(string.Empty);
        }

        [Test]
        [ExpectedException(typeof(InvalidOperationException))]
        public void ListPermissionsWithNullClientShouldThrowsInvalidOperationException()
        {
            AdminClient adminClient = new AdminClient(null);
            adminClient.ListPermissions(this.user.Login);
        }

        [Test]
        public void ListPermissionsShouldReturnExpectedPermissionList()
        {
            AdminClient adminClient = new AdminClient(this.client);
            this.client.Expect(c => c.GetPermissionTypes(this.user.Login))
                .Return(this.expectedPermissionList).Repeat.Once();
            Assert.AreEqual(this.expectedPermissionList, adminClient.ListPermissions(this.user.Login));
            this.client.VerifyAllExpectations();
        }

        [Test]
        public void HasPermissionShouldReturnTrueWhenTheUserDoesHasPermission()
        {
            AdminClient adminClient = new AdminClient(this.client);
            this.client.Expect(c => c.GetPermissionTypes(this.user.Login))
                .Return(this.expectedPermissionList).Repeat.Once();
            Assert.IsTrue(adminClient.HasPermission(this.user.Login, (int)PermissionTypes.Participation));
            this.client.VerifyAllExpectations();
        }

        [Test]
        public void HasPermissionShouldReturnFalseWhenTheUserDoesNotHasPermission()
        {
            AdminClient adminClient = new AdminClient(this.client);
            this.client.Expect(c => c.GetPermissionTypes(this.user.Login))
                .Return(this.expectedPermissionList).Repeat.Once();
            Assert.IsFalse(adminClient.HasPermission(this.user.Login, (int)PermissionTypes.ViewUsers));
            this.client.VerifyAllExpectations();
        }

        [Test]
        [ExpectedException(typeof(ArgumentException))]
        public void CreatePermissionWithInvalidInputShouldThrowsArgumentException()
        {
            AdminClient adminClient = new AdminClient(this.client);
            adminClient.CreatePermission(string.Empty);
        }

        [Test]
        [ExpectedException(typeof(InvalidOperationException))]
        public void CreatePermissionWithNullClientShouldThrowsInvalidOperationException()
        {
            AdminClient adminClient = new AdminClient(null);
            adminClient.CreatePermission(PermissionTypes.Participation.ToString());
        }

        [Test]
        public void CreatePermissionWithValidInputShouldReturnTheExpectedPermissionProperties()
        {
            AdminClient adminClient = new AdminClient(this.client);
            long permissionId = (long)PermissionTypes.Participation;
            string permissionName = PermissionTypes.Participation.ToString();
            this.client.Expect(c => c.CreatePermission(permissionName)).Return(permissionId).Repeat.Once();
            PermissionProperties permissionProperties = adminClient.CreatePermission(permissionName);

            Assert.IsNotNull(permissionProperties);
            Assert.AreEqual(permissionName, permissionProperties.Name);
            Assert.AreEqual(permissionId, permissionProperties.Id);
            this.client.VerifyAllExpectations();
        }

        [Test]
        [ExpectedException(typeof(InvalidOperationException))]
        public void RemovePermissionWithNullClientShouldThrowsInvalidOperationException()
        {
            long permissionId = (long)PermissionTypes.EndBuiltinMarker_DoNotUseAsPermission + 1;
            AdminClient adminClient = new AdminClient(null);
            adminClient.RemovePermission(permissionId);
        }

        [Test]
        public void RemovePermissionWithInvalidPermissionIdShouldReturnFalse()
        {
            AdminClient adminClient = new AdminClient(this.client);
            Assert.IsFalse(adminClient.RemovePermission((long)PermissionTypes.None));
            Assert.AreEqual(FailureReasons.InvalidRequest, adminClient.FailureReason);
            this.client.VerifyAllExpectations();
        }

        [Test]
        public void RemovePermissionOnSuccessShouldReturnTrue()
        {
            long permissionId = (long)PermissionTypes.EndBuiltinMarker_DoNotUseAsPermission + 1;
            AdminClient adminClient = new AdminClient(this.client);
            this.client.Expect(c => c.RemovePermission(permissionId)).Return(true).Repeat.Once();
            Assert.IsTrue(adminClient.RemovePermission(permissionId));
            this.client.VerifyAllExpectations();
        }

        [Test]
        public void RemovePermissionOnFailureShouldReturnFalse()
        {
            long permissionId = (long)PermissionTypes.EndBuiltinMarker_DoNotUseAsPermission + 1;
            FailureReasons failureReason = FailureReasons.InvalidRequest;
            AdminClient adminClient = new AdminClient(this.client);
            this.client.Expect(c => c.RemovePermission(permissionId)).Return(false).Repeat.Once();
            this.client.Expect(c => c.FailureReason).Return(failureReason).Repeat.Once();
            Assert.IsFalse(adminClient.RemovePermission(permissionId));
            this.client.VerifyAllExpectations();
        }

        [Test]
        [ExpectedException(typeof(InvalidOperationException))]
        public void GrantPermissionWithNullClientShouldThrowsInvalidOperationException()
        {
            AdminClient adminClient = new AdminClient(null);
            adminClient.GrantPermission(this.user.Login, (long)PermissionTypes.CreateUsers);
        }

        [TestCase(new object[] { "", 1 })]
        [TestCase(new object[] { "user", 0 })]
        [ExpectedException(typeof(ArgumentException))]
        public void GrantPermissionWithInvalidInputsShouldThrowsArgumentException(
            string username, long permissionId)
        {
            AdminClient adminClient = new AdminClient(this.client);
            adminClient.GrantPermission(username, permissionId);
        }

        [Test]
        public void GrantPermissionOnSuccessShouldReturnTrue()
        {
            long permissionId = (long)PermissionTypes.Admin;
            AdminClient adminClient = new AdminClient(this.client);
            this.client.Expect(c => c.GrantPermission(this.user.Login, permissionId))
                .Return(true).Repeat.Once();
            Assert.IsTrue(adminClient.GrantPermission(this.user.Login, permissionId));
            this.client.VerifyAllExpectations();
        }

        [Test]
        public void GrantPermissionOnFailureShouldReturnFalse()
        {
            long permissionId = (long)PermissionTypes.Admin;
            FailureReasons failureReason = FailureReasons.ConnectionError;
            AdminClient adminClient = new AdminClient(this.client);
            this.client.Expect(c => c.GrantPermission(this.user.Login, permissionId))
                .Return(false).Repeat.Once();
            this.client.Expect(c => c.FailureReason).Return(failureReason).Repeat.Once();
            Assert.IsFalse(adminClient.GrantPermission(this.user.Login, permissionId));
            Assert.AreEqual(failureReason, adminClient.FailureReason);
            this.client.VerifyAllExpectations();
        }

        [Test]
        [ExpectedException(typeof(InvalidOperationException))]
        public void RevokePermissionWithNullClientShouldThrowsInvalidOperationException()
        {
            AdminClient adminClient = new AdminClient(null);
            adminClient.RevokePermission(this.user.Login, (long)PermissionTypes.CreateUsers);
        }

        [TestCase(new object[] { "", 1 })]
        [TestCase(new object[] { "user", 0 })]
        [ExpectedException(typeof(ArgumentException))]
        public void RevokePermissionWithInvalidInputsShouldThrowsArgumentException(
            string username, long permissionId)
        {
            AdminClient adminClient = new AdminClient(this.client);
            adminClient.RevokePermission(username, permissionId);
        }

        [Test]
        public void RevokePermissionOnSuccessShouldReturnTrue()
        {
            long permissionId = (long)PermissionTypes.Admin;
            AdminClient adminClient = new AdminClient(this.client);
            this.client.Expect(c => c.RevokePermission(this.user.Login, permissionId))
                .Return(true).Repeat.Once();
            Assert.IsTrue(adminClient.RevokePermission(this.user.Login, permissionId));
            this.client.VerifyAllExpectations();
        }

        [Test]
        public void RevokePermissionOnFailureShouldReturnFalse()
        {
            long permissionId = (long)PermissionTypes.Admin;
            FailureReasons failureReason = FailureReasons.ConnectionError;
            AdminClient adminClient = new AdminClient(this.client);
            this.client.Expect(c => c.RevokePermission(this.user.Login, permissionId))
                .Return(false).Repeat.Once();
            this.client.Expect(c => c.FailureReason).Return(failureReason).Repeat.Once();
            Assert.IsFalse(adminClient.RevokePermission(this.user.Login, permissionId));
            Assert.AreEqual(failureReason, adminClient.FailureReason);
            this.client.VerifyAllExpectations();
        }

        [Test]
        public void LoginOnConnectionErrorCatchInSocketException()
        {
            AdminClient adminClient = new AdminClient(this.client);
            Assert.IsFalse(adminClient.Login("localhost", 21248, this.user.Login, this.user.Password));
            Assert.AreEqual(FailureReasons.ConnectionError, adminClient.FailureReason);
        }

        [Test]
        public void LoginWithouSslOnConnectionErrorCatchInSocketException()
        {
            AdminClient adminClient = new AdminClient(this.client);
            Assert.IsFalse(adminClient.Login("localhost", 21248, this.user.Login, this.user.Password, false));
            Assert.AreEqual(FailureReasons.ConnectionError, adminClient.FailureReason);
        }

        [Test]
        public void SetGetIgnoreSslErrorsPropertiesWorks()
        {
            AdminClient adminClient = new AdminClient(this.client);
            bool ignoreSslErrors = true;
            adminClient.IgnoreSslErrors = ignoreSslErrors;
            Assert.AreEqual(ignoreSslErrors, adminClient.IgnoreSslErrors);
        }

        [Test]
        public void SetGetIgnoreSslCertificateNameMismatchPropertiesWorks()
        {
            AdminClient adminClient = new AdminClient(this.client);
            bool ignoreSslCertificateNameMismatch = true;
            adminClient.IgnoreSslCertificateNameMismatch = ignoreSslCertificateNameMismatch;
            Assert.AreEqual(
                ignoreSslCertificateNameMismatch,
                adminClient.IgnoreSslCertificateNameMismatch);
        }

        [Test]
        public void SetGetIgnoreSslSelfSignedCertificateErrorsPropertiesWorks()
        {
            AdminClient adminClient = new AdminClient(this.client);
            bool ignoreSslSelfSignedCertificateErrors = true;
            adminClient.IgnoreSslSelfSignedCertificateErrors = ignoreSslSelfSignedCertificateErrors;
            Assert.AreEqual(
                ignoreSslSelfSignedCertificateErrors,
                adminClient.IgnoreSslSelfSignedCertificateErrors);
        }
    }
}
