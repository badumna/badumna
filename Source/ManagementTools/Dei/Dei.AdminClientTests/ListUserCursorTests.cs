//-----------------------------------------------------------------------
// <copyright file="ListUserCursorTests.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace Dei.AdminClientTests
{
    using System;
    using System.Collections.Generic;
    using Dei;
    using Dei.AdminClient;
    using NUnit.Framework;
    using Rhino.Mocks;

    internal class ListUserCursorTests
    {
        private IClient client;

        private ListUserCursor listUserCursor;

        private string globExpression = "*";

        private int numberOfUsers = 25;
      
        public ListUserCursorTests()
        {
        }

        [SetUp]
        public void SetUp()
        {
            this.client = MockRepository.GenerateMock<IClient>();
            int maxNumberOfResult = 10;
            this.client.Expect(c => c.GetUserCount(this.globExpression))
                .Return(this.numberOfUsers).Repeat.Once();
            this.listUserCursor = new ListUserCursor(this.client, this.globExpression, maxNumberOfResult);
            Assert.IsNotNull(this.listUserCursor);
        }

        [TearDown]
        public void TearDown()
        {
            this.client = null;
            this.listUserCursor = null;
        }

        [Test]
        public void GetResultsShouldReturnTheExpectedListOfuser()
        {
            List<string> userList = new List<string>() { "admin", "user1", "user2" };
            this.client.Expect(
                c => c.ListUsers(
                    this.globExpression, 
                    this.listUserCursor.RangeStart, 
                    this.listUserCursor.MaximumNumberOfResults))
                .Return(userList).Repeat.Once();
            
            Assert.AreEqual(userList, this.listUserCursor.GetResults());
            this.client.VerifyAllExpectations();
        }

        [Test]
        public void HasNextShouldReturnTrueIfRangeStartLessThanCount()
        {
            Assert.IsTrue(this.listUserCursor.HasNext);
        }

        [Test]
        public void HasPreviousShouldReturnFalseIfTheRangeStartIsZero()
        {
            Assert.IsFalse(this.listUserCursor.HasPrevious);
        }

        [Test]
        public void HasNextShouldReturnFalseIfRangeStartGreaterThanCount()
        {
            this.listUserCursor.Next();
            this.listUserCursor.Next();
            Assert.IsFalse(this.listUserCursor.HasNext);
        }

        [Test]
        public void HasPreviousShouldReturnTrueIfRangeStartGreaterThanZero()
        {
            this.listUserCursor.Next();
            this.listUserCursor.Next();
            Assert.IsTrue(this.listUserCursor.HasPrevious);
            this.listUserCursor.Previous();
            Assert.IsTrue(this.listUserCursor.HasPrevious);
        }

        [Test]
        public void NextShouldResetTheStartRangeWhenStartRangeGreaterThanCount()
        {
            this.listUserCursor.Next();
            this.listUserCursor.Next();
            this.listUserCursor.Next();
            Assert.IsTrue(this.listUserCursor.HasNext);
        }
    }
}
