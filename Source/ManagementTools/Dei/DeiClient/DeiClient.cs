using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Text;
using System.Net.Sockets;
using System.Net;
using System.IO;
using System.Security.Cryptography;
using System.Security.Authentication;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;

using Dei;
using Dei.Utilities;
using Badumna.Security;

namespace DeiClient
{
    class DeiClient
    {
        static void Main(string[] args)
        {
            TextWriterTraceListener traceListener = new TextWriterTraceListener(System.Console.Out);
            Trace.Listeners.Add(traceListener);

            RSACryptoServiceProvider privateKey = new RSACryptoServiceProvider(128 * 8);

            try
            {
                Session session = new Session("localhost", 21248, DeiClient.LoginProgressNotification);

                session.IgnoreSslErrors = true;

                System.Console.WriteLine("Please enter username: ");
                String username = System.Console.ReadLine();

                System.Console.WriteLine("Please enter password: ");
                String password = System.Console.ReadLine();

                LoginResult result = session.Authenticate(username, password);
                if (result.WasSuccessful)
                {
                    IIdentityProvider identity;
                    session.SelectIdentity(Character.None, out identity);
                    for (int i = 0; i < 10; i++)
                    {
                        CertificateToken certificate = identity.GetUserCertificateToken();
                        TrustedAuthorityToken serverKey = identity.GetTrustedAuthorityToken();

                        if (!certificate.Validate(serverKey))
                        {
                            System.Console.WriteLine("Failed to validate certificate");
                        }

                        System.Threading.Thread.Sleep(37000);
                    }
                }
                else
                {
                    System.Console.WriteLine("Failed to login : {0}", result.ErrorDescription);
                }
            }
            catch (IOException e)
            {
                System.Console.WriteLine("Failed to connect to server : {0}", e.Message);
            }
            catch (SocketException e)
            {
                System.Console.WriteLine("Failed to connect to server : {0}", e.Message);
            }
            catch (AuthenticationException e)
            {
                System.Console.WriteLine("Failed to connect to server : {0}", e.Message);
            }
                        
            System.Console.ReadKey();          
        }

        public static bool ValidateServerCertificate(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            return true; //sslPolicyErrors == SslPolicyErrors.None;
        }

        private static bool LoginProgressNotification(LoginStage stage, string description, int percentage)
        {
            System.Console.WriteLine("{0,3}% : {1}", percentage, description);
            return true;
        }
    }
}
