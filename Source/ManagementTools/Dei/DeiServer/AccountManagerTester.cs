﻿using System;
using System.Collections.Generic;
using System.Text;
using Dei;

namespace DeiServer
{
    class AccountManagerTester
    {
        private IAccountManager mAccountManager;
        private List<string> mFailures = new List<string>();
        private string mTestUsername = "__tester_username__";
        private string mTestPassword = "__tester_password__";
        private int mTestPermissionId = 98762;

        public AccountManagerTester(IAccountManager accountManager)
        {
            this.mAccountManager = accountManager;
        }

        public bool RunTests()
        {
            DateTime start = DateTime.Now;
            this.mFailures.Clear();

            if (!this.AddTestUser())
            {
                this.PresentErrors();
                return false;
            }

            try
            {
                this.TestGetUserId();
                this.TestAuthenticateUser();
                this.TestFindUser();
                this.TestGetUserCount();
                this.TestHasPermission();
                this.TestCreateAccount();
                this.TestDeleteAccount();
                this.TestChangePassword();
                this.TestCreatePermission();
                this.TestDeletePermission();
                this.TestGrantPermission();
                this.TestRevokePermission();
                this.TestGetPermissions();
                this.TestValidityPeriods();
                this.TestListAllPermissions();
            }
            catch
            { }
            finally
            {
                this.PresentErrors();
                this.RemoveTestUser();

                this.DeleteTestPermission();
                this.RemoveTestUser();
            }

            TimeSpan executionTime = DateTime.Now - start;
            System.Diagnostics.Trace.TraceInformation("Tests completed in {0:0.00} seconds", executionTime.TotalSeconds);

            return this.mFailures.Count == 0;
        }

        private void PresentErrors()
        {
            if (this.mFailures.Count > 0)
            {
                System.Diagnostics.Trace.TraceInformation("Tests failed with {0} errors", this.mFailures.Count);
                foreach (string failure in this.mFailures)
                {
                    System.Diagnostics.Trace.TraceError(failure);
                }
            }
            else
            {
                System.Diagnostics.Trace.TraceInformation("All tests passed");
            }
        }

        // Must add an error message if returning false.
        private bool AddTestUser()
        {
            try
            {
                bool foundFreeUsername = false;
                for (int i = 0; i < 3; i++)
                {
                    if (this.mAccountManager.GetUserId(this.mTestUsername) < 0)
                    {
                        foundFreeUsername = true;
                        break;
                    }
                    this.mTestUsername += "q";
                }

                if (!foundFreeUsername)
                {
                    this.mFailures.Add("Failed to find free username for testing. Please remove users '__tester_username__*' and try again");
                    return false;
                }
            }
            catch (Exception e)
            {
                this.AddExceptionToFailures("GetUserId", e);
                return false;
            }
            try
            {
                if (!this.mAccountManager.CreateAccount(this.mTestUsername, this.mTestPassword))
                {
                    this.mFailures.Add("Failed to add test user when a previous call to GetUserId indicated it wasn't found (returned -1)");
                    return false;
                }
            }
            catch (Exception e)
            {
                this.AddExceptionToFailures("CreateAccount", e);
                return false;
            }
            return true;
        }

        // Must add an error message if returning false.
        private bool RemoveTestUser()
        {
            try
            {
                if (!this.mAccountManager.DeleteAccount(this.mTestUsername))
                {
                    this.mFailures.Add(string.Format("Failed to remove test user ({0}). Please remove it manually.", this.mTestUsername));
                    return false;
                }
            }
            catch (Exception e)
            {
                this.AddExceptionToFailures("DeleteAccount", e);
                return false;
            }

            try
            {
                if (this.mAccountManager.GetUserId(this.mTestUsername) >= 0)
                {
                    this.mFailures.Add("DeleteUser succeeded but GetUserId now returns a positive Id for the same user.");
                    return false;
                }
            }
            catch (Exception e)
            {
                this.AddExceptionToFailures("GetUserId", e);
                return false;
            }

            return true;
        }

        // Must add an error message if returning false.
        private bool GrantPermissionToTestUser()
        {
            try
            {
                if (!this.mAccountManager.GrantPermission(this.mTestUsername, this.mTestPermissionId))
                {
                    this.mFailures.Add("GrantPermission failed for test user and test permission");
                    return false;
                }

                if (!this.mAccountManager.HasPermission(this.mTestUsername, this.mTestPermissionId))
                {
                    this.mFailures.Add("HasPermission returned false for newly granted test permission for test user");
                    return false;
                }
            }
            catch (Exception e)
            {
                this.AddExceptionToFailures("GrantPermission", e);
                return false;
            }

            return true;
        }

        // Must add an error message if returning false.
        private bool RevokePermissionFromTestUser()
        {
            try
            {
                if (!this.mAccountManager.RevokePermission(this.mTestUsername, this.mTestPermissionId))
                {
                    this.mFailures.Add("RevokePermission failed for test user and test permission. Please revoke it manually");
                    return false;
                }

                if (this.mAccountManager.HasPermission(this.mTestUsername, this.mTestPermissionId))
                {
                    this.mFailures.Add("HasPermission returned true for newly revoked test permission for test user");
                    return false;
                }
            }
            catch (Exception e)
            {
                this.AddExceptionToFailures("RevokePermission", e);
                return false;
            }

            return true;
        }

        // Must add an error message if returning false.
        private bool CreateTestPermission()
        {
            try
            {
                if (this.mAccountManager.CreatePermission(this.mTestPermissionId, "__test_permission__"))
                {
                    return true;
                }
            }
            catch (Exception e)
            {
                this.AddExceptionToFailures("CreatePermission", e);
            }

            return false;
        }

        // Must add an error message if returning false.
        private bool DeleteTestPermission()
        {
            try
            {
                if (this.mAccountManager.DeletePermission(this.mTestPermissionId))
                {
                    return true;
                }
                this.mFailures.Add(String.Format("WARNING : Failed to remove test permission {0}. Please remove it manually", this.mTestPermissionId));
            }
            catch (Exception e)
            {
                this.AddExceptionToFailures("DeletePermission", e);
            }

            return false;
        }

        #region AuthenticateUser

        private void TestAuthenticateUser()
        {
            this.TestAuthenticateUser_InvalidUsername();
            this.TestAuthenticateUser_UnknownUser();
            this.TestAuthenticateUser_IncorrectPassword();
            this.TestAuthenticateUser_CorrectPassword();
        }

        private void TestAuthenticateUser_InvalidUsername()
        {
            try
            {
                if (this.mAccountManager.AuthenticateUser("", ""))
                {
                    this.mFailures.Add("AuthenticateUser succeeded with empty username");
                }
            }
            catch (Exception e)
            {
                this.AddExceptionToFailures("AuthenticateUser", e);
            }
        }

        private void TestAuthenticateUser_IncorrectPassword()
        {
            try
            {
                if (this.mAccountManager.AuthenticateUser(this.mTestUsername, this.mTestPassword + "incorrect"))
                {
                    this.mFailures.Add("AuthenticateUser succeeded with incorrect password");
                }
            }
            catch (Exception e)
            {
                this.AddExceptionToFailures("AuthenticateUser", e);
            }
        }

        private void TestAuthenticateUser_CorrectPassword()
        {
            try
            {
                if (!this.mAccountManager.AuthenticateUser(this.mTestUsername, this.mTestPassword))
                {
                    this.mFailures.Add("AuthenticateUser failed with correct password");
                }
            }
            catch (Exception e)
            {
                this.AddExceptionToFailures("AuthenticateUser", e);
            }
        }

        private void TestAuthenticateUser_UnknownUser()
        {
            if (!this.RemoveTestUser())
            {
                throw new Exception("Failed to remove test user");
            }

            try
            {
                if (this.mAccountManager.AuthenticateUser(this.mTestUsername, ""))
                {
                    this.mFailures.Add("AuthenticateUser succeeded for previously removed test user");
                }
            }
            catch (Exception e)
            {
                this.AddExceptionToFailures("AuthenticateUser", e);
            }
            finally
            {
                if (!this.AddTestUser())
                {
                    throw new Exception("Failed to add test user");
                }
            }
        }

        #endregion

        #region GetUserId

        private void TestGetUserId()
        {
            this.TestGetUserId_InvalidUsername();
            this.TestGetUserId_MissingUser();
            this.TestGetUserId_TestUser();
        }

        public void TestGetUserId_InvalidUsername()
        {
            try
            {
                if (this.mAccountManager.GetUserId("") >= 0)
                {
                    this.mFailures.Add("GetUserId returned a positive Id for an empty username");
                }
            }
            catch (Exception e)
            {
                this.AddExceptionToFailures("GetUserId", e);
            }
        }

        public void TestGetUserId_TestUser()
        {
            try
            {
                if (this.mAccountManager.GetUserId(this.mTestUsername) < 0)
                {
                    this.mFailures.Add("GetUserId returned a negative Id for previously inserted test user");
                }
            }
            catch (Exception e)
            {
                this.AddExceptionToFailures("GetUserId", e);
            }
        }

        public void TestGetUserId_MissingUser()
        {
            if (!this.RemoveTestUser())
            {
                throw new Exception("Failed to remove test user");
            }

            try
            {
                if (this.mAccountManager.GetUserId(this.mTestUsername) >= 0)
                {
                    this.mFailures.Add("GetUserId returned a positive Id for previously removed test user");
                }
            }
            catch (Exception e)
            {
                this.AddExceptionToFailures("GetUserId", e);
            }
            finally
            {
                if (!this.AddTestUser())
                {
                    throw new Exception("Failed to add test user");
                }
            }
        }

        #endregion

        #region FindUser

        private void TestFindUser()
        {
            this.TestFindUser_InvalidUsername();
            this.TestFindUser_MissingUser();
            this.TestFindUser_TestUser();
            this.TestFindUser_TestUserGlob();
        }

        public void TestFindUser_InvalidUsername()
        {
            try
            {
                IList<string> users = this.mAccountManager.FindUsers("");
                if (users != null && users.Count > 0)
                {
                    this.mFailures.Add("FindUser returned a positive result for an empty username");
                }
            }
            catch (Exception e)
            {
                this.AddExceptionToFailures("FindUser", e);
            }
        }

        public void TestFindUser_TestUser()
        {
            try
            {
                IList<string> users = this.mAccountManager.FindUsers(this.mTestUsername);
                if (users == null || users.Count == 0)
                {
                    this.mFailures.Add("FindUser returned a negative result for previously inserted test user");
                }
                if (!users.Contains(this.mTestUsername))
                {
                    this.mFailures.Add("FindUser result did not contain test user");
                }
            }
            catch (Exception e)
            {
                this.AddExceptionToFailures("FindUser", e);
            }
        }

        public void TestFindUser_MissingUser()
        {
            if (!this.RemoveTestUser())
            {
                throw new Exception("Failed to remove test user");
            }

            try
            {
                IList<string> users = this.mAccountManager.FindUsers(this.mTestUsername);
                if (users != null && users.Count > 0)
                {
                    this.mFailures.Add("FindUser returned a positive result for previously removed test user");
                }
            }
            catch (Exception e)
            {
                this.AddExceptionToFailures("FindUser", e);
            }
            finally
            {
                if (!this.AddTestUser())
                {
                    throw new Exception("Failed to add test user");
                }
            }
        }

        private void TestFindUser_TestUserGlob()
        {
            try
            {
                IList<string> users = this.mAccountManager.FindUsers("__tester*__*");
                if (users == null || users.Count == 0)
                {
                    this.mFailures.Add("FindUser returned a negative result for previously inserted test user glob ('__tester*__')");
                }
                if (!users.Contains(this.mTestUsername))
                {
                    this.mFailures.Add("FindUser ('__tester*__*') result did not contain test user");
                }
            }
            catch (Exception e)
            {
                this.AddExceptionToFailures("FindUser", e);
            }
        }

        #endregion

        #region GetUserCount

        private void TestGetUserCount()
        {
            this.TestGetUserCount_InvalidUsername();
            this.TestGetUserCount_MissingUser();
            this.TestGetUserCount_TestUser();
            this.TestGetUserCount_TestUserGlob();
        }

        public void TestGetUserCount_InvalidUsername()
        {
            try
            {
                if (this.mAccountManager.GetUserCount("") > 0)
                {
                    this.mFailures.Add("GetUserCount returned a positive result for an empty username");
                }
            }
            catch (Exception e)
            {
                this.AddExceptionToFailures("GetUserCount", e);
            }
        }

        public void TestGetUserCount_TestUser()
        {
            try
            {
                if (this.mAccountManager.GetUserCount(this.mTestUsername) != 1)
                {
                    this.mFailures.Add("GetUserCount returned a negative result for previously inserted test user");
                }
            }
            catch (Exception e)
            {
                this.AddExceptionToFailures("GetUserCount", e);
            }
        }

        public void TestGetUserCount_MissingUser()
        {
            if (!this.RemoveTestUser())
            {
                throw new Exception("Failed to remove test user");
            }

            try
            {
                if (this.mAccountManager.GetUserCount(this.mTestUsername) != 0)
                {
                    this.mFailures.Add("GetUserCount returned a positive result for previously removed test user");
                }
            }
            catch (Exception e)
            {
                this.AddExceptionToFailures("GetUserCount", e);
            }
            finally
            {
                if (!this.AddTestUser())
                {
                    throw new Exception("Failed to add test user");
                }
            }
        }

        private void TestGetUserCount_TestUserGlob()
        {
            try
            {
                if (this.mAccountManager.GetUserCount("__tester*__*") <= 0)
                {
                    this.mFailures.Add("GetUserCount returned a negative result for previously inserted test user glob ('__tester*__')");
                }
            }
            catch (Exception e)
            {
                this.AddExceptionToFailures("GetUserCount", e);
            }
        }

        #endregion

        #region HasPermission

        private void TestHasPermission()
        {
            this.TestHasPermission_InvalidUser();
            this.TestHasPermission_MissingUser();
            this.TestHasPermission_TestUserNoPermission();
            this.TestHasPermission_TestUserWithPermission();
        }

        private void TestHasPermission_InvalidUser()
        {
            try
            {
                if (this.mAccountManager.HasPermission("", (int)PermissionTypes.Admin))
                {
                    this.mFailures.Add("HasPermission returned a positive result for an empty username");
                }
            }
            catch (Exception e)
            {
                this.AddExceptionToFailures("HasPermission", e);
            }
        }

        private void TestHasPermission_MissingUser()
        {
            if (!this.RemoveTestUser())
            {
                throw new Exception("Failed to remove test user");
            }

            try
            {
                if (this.mAccountManager.HasPermission(this.mTestUsername, (int)PermissionTypes.Admin))
                {
                    this.mFailures.Add("HasPermission returned a positive result for previously removed test user");
                }
            }
            catch (Exception e)
            {
                this.AddExceptionToFailures("HasPermission", e);
            }
            finally
            {
                if (!this.AddTestUser())
                {
                    throw new Exception("Failed to add test user");
                }
            }
        }

        private void TestHasPermission_TestUserNoPermission()
        {
            try
            {
                if (this.mAccountManager.HasPermission(this.mTestUsername, this.mTestPermissionId))
                {
                    this.mFailures.Add("HasPermission returned a positive result for (un granted) test permission on test user");
                }
            }
            catch (Exception e)
            {
                this.AddExceptionToFailures("HasPermission", e);
            }
        }

        private void TestHasPermission_TestUserWithPermission()
        {
            if (!this.CreateTestPermission())
            {
                throw new Exception("Failed to create test permission");
            }

            if (!this.GrantPermissionToTestUser())
            {
                throw new Exception("Failed to grant permission to test user");
            }

            try
            {
                if (!this.mAccountManager.HasPermission(this.mTestUsername, this.mTestPermissionId))
                {
                    this.mFailures.Add("HasPermission returned a negative result for test permission on test user after it was granted");
                }
            }
            catch (Exception e)
            {
                this.AddExceptionToFailures("HasPermission", e);
            }
            finally
            {
                if (!this.RevokePermissionFromTestUser())
                {
                    throw new Exception("Failed to revoke permission from test user");
                }
                if (!this.DeleteTestPermission())
                {
                    throw new Exception("Failed to remove test permission");
                }
            }
        }

        #endregion

        #region CreateAccount

        private void TestCreateAccount()
        {
            this.TestCreateAccount_InvalidUserName();
            this.TestCreateAccount_InvalidPassword();
            this.TestCreateAccount_ExistingUser();
            this.TestCreateAccount_ValidUser();
        }

        private void TestCreateAccount_InvalidUserName()
        {
            if (!this.RemoveTestUser())
            {
                throw new Exception("Failed to remove test user");
            }

            try
            {
                if (this.mAccountManager.CreateAccount("", this.mTestPassword))
                {
                    this.mFailures.Add("CreateAccount succeeded with empty username");

                    // Try and remove the account if possible so it doen't interfere with other tests
                    try
                    {
                        if (!this.mAccountManager.DeleteAccount(""))
                        {
                            this.mFailures.Add("DeleteAccount failed with empty username (previously created). Please remove manually.");
                            throw new Exception("Failed to remove empty account");
                        }
                    }
                    catch (Exception de)
                    {
                        this.mFailures.Add("DeleteAccount failed with empty username (previously created). Please remove manually.");
                        this.AddExceptionToFailures("DeleteAccount", de);
                        throw new Exception("Failed to remove empty account");
                    }
                }
            }
            catch (Exception e)
            {
                this.AddExceptionToFailures("AuthenticateUser", e);
            }
            finally
            {
                if (!this.AddTestUser())
                {
                    throw new Exception("Failed to add test user");
                }
            }
        }

        private void TestCreateAccount_InvalidPassword()
        {
            if (!this.RemoveTestUser())
            {
                throw new Exception("Failed to remove test user");
            }

            try
            {
                if (this.mAccountManager.CreateAccount(this.mTestUsername, ""))
                {
                    this.mFailures.Add("CreateAccount succeeded with empty password");

                    if (!this.RemoveTestUser())
                    {
                        throw new Exception("Failed to remove test user account after it was added with invalid password.");
                    }
                }
            }
            catch (Exception e)
            {
                this.AddExceptionToFailures("AuthenticateUser", e);
            }
            finally
            {
                if (!this.AddTestUser())
                {
                    throw new Exception("Failed to add test user");
                }
            }
        }

        private void TestCreateAccount_ExistingUser()
        {
            try
            {
                if (this.mAccountManager.GetUserId(this.mTestUsername) > 0)
                {
                    if (this.mAccountManager.CreateAccount(this.mTestUsername, this.mTestPassword + "something different"))
                    {
                        this.mFailures.Add("CreateAccount succeeded with the same username as one that already exists");
                    }
                }
                else
                {
                    this.mFailures.Add("Test should exist but doesn't");
                }
            }
            catch (Exception e)
            {
                this.AddExceptionToFailures("AuthenticateUser", e);
            }
        }

        private void TestCreateAccount_ValidUser()
        {
            // Tested in AddTestUser already
        }

        #endregion

        #region DeleteAccount

        private void TestDeleteAccount()
        {
            this.TestDeleteAccount_ExistingUser();
            this.TestDeleteAccount_MissingUser();
        }

        private void TestDeleteAccount_MissingUser()
        {
            if (!this.RemoveTestUser())
            {
                throw new Exception("Failed to remove test user");
            }

            try
            {
                if (this.mAccountManager.DeleteAccount(this.mTestUsername))
                {
                    this.mFailures.Add("DeleteAccount succeeded with user that does not exist");
                }
            }
            catch (Exception e)
            {
                this.AddExceptionToFailures("DeleteAccount", e);
            }
            finally
            {
                if (!this.AddTestUser())
                {
                    throw new Exception("Failed to re-add test user");
                }
            }
        }

        private void TestDeleteAccount_ExistingUser()
        {
            // Testes in RemoveTestUser already
        }

        #endregion

        #region ChangePassword

        private void TestChangePassword()
        {
            this.TestChangePassword_InvalidUser();
            this.TestChangePassword_InvalidPassword();
            this.TestChangePassword_MissingUser();
            this.TestChangePassword_TestUser();
        }

        private void TestChangePassword_InvalidUser()
        {
            try
            {
                if (this.mAccountManager.ChangePassword("", this.mTestPassword))
                {
                    this.mFailures.Add("ChangePassword succeeded with empty username");
                }
            }
            catch (Exception e)
            {
                this.AddExceptionToFailures("ChangePassword", e);
            }
        }

        private void TestChangePassword_MissingUser()
        {
            if (!this.RemoveTestUser())
            {
                throw new Exception("Failed to remove test user");
            }

            try
            {
                if (this.mAccountManager.ChangePassword(this.mTestUsername, this.mTestPassword + "X"))
                {
                    this.mFailures.Add("ChangePassword succeeded for previously removed test user");
                }
            }
            catch (Exception e)
            {
                this.AddExceptionToFailures("ChangePassword", e);
            }
            finally
            {
                if (!this.AddTestUser())
                {
                    throw new Exception("Failed to add test user");
                }
            }
        }

        private void TestChangePassword_InvalidPassword()
        {
            try
            {
                if (this.mAccountManager.ChangePassword(this.mTestUsername, ""))
                {
                    this.mFailures.Add("ChangePassword succeeded with empty paswword");
                }
            }
            catch (Exception e)
            {
                this.AddExceptionToFailures("ChangePassword", e);
            }
        }

        private void TestChangePassword_TestUser()
        {
            try
            {
                if (!this.mAccountManager.ChangePassword(this.mTestUsername, this.mTestPassword + "X"))
                {
                    this.mFailures.Add("ChangePassword failed for valid username and password");
                    return;
                }

                // Try to authenticate using the new password
                try
                {
                    if (this.mAccountManager.AuthenticateUser(this.mTestUsername, this.mTestPassword + "X"))
                    {
                        try
                        {
                            if (!this.mAccountManager.ChangePassword(this.mTestUsername, this.mTestPassword))
                            {
                                this.mFailures.Add("WARNING: ChangePassword failed fto set password back to previous value.");
                                return;
                            }
                        }
                        catch (Exception ce)
                        {
                            this.AddExceptionToFailures("ChangePassword", ce);
                        }
                    }
                    else
                    {
                        this.mFailures.Add("Failed to  Authenticate User after password was changed");
                    }
                }
                catch (Exception ae)
                {
                    this.AddExceptionToFailures("AuthenticateUser", ae);
                }
            }
            catch (Exception e)
            {
                this.AddExceptionToFailures("ChangePassword", e);
            }
        }

        #endregion

        #region CreatePermission

        private void TestCreatePermission()
        {
            this.TestCreatePermission_InvalidId();
            this.TestCreatePermission_EmptyName();
            this.TestCreatePermission_EmptyNameWithoutId();
            this.TestCreatePermission_NullName();
            this.TestCreatePermission_NullNameWithoutId();
            this.TestCreatePermission_IdAlreadyExists();
            this.TestCreatePermission_NameAlreadyExists();
            this.TestCreatePermission_AnyId();
            this.TestCreatePermission_ValidWithId();
        }

        private void TestCreatePermission_InvalidId()
        {
            try
            {
                if (this.mAccountManager.CreatePermission(-1, "invalid_test_permission"))
                {
                    this.mFailures.Add("CreatePermission succeeded for invalid permission Id (nagative number)");

                    try
                    {
                        if (!this.mAccountManager.DeletePermission(-1))
                        {
                            this.mFailures.Add("WARNING : Failed to remove invalidp ermission Id (-1) please remove it manually");
                        }
                    }
                    catch (Exception de)
                    {
                        this.AddExceptionToFailures("RemovePermission", de);
                    }
                }
            }
            catch (Exception e)
            {
                this.AddExceptionToFailures("CreatePermission", e);
            }
        }

        private void TestCreatePermission_EmptyName()
        {
            try
            {
                if (this.mAccountManager.CreatePermission(this.mTestPermissionId, ""))
                {
                    this.mFailures.Add("CreatePermission succeeded for empty permission name");

                    try
                    {
                        if (!this.mAccountManager.DeletePermission(this.mTestPermissionId))
                        {
                            this.mFailures.Add(String.Format("WARNING : Failed to remove test permission Id ({0}) please remove it manually", this.mTestPermissionId));
                        }
                    }
                    catch (Exception de)
                    {
                        this.AddExceptionToFailures("RemovePermission", de);
                    }
                }

            }
            catch (Exception e)
            {
                this.AddExceptionToFailures("CreatePermission", e);
            }
        }

        private void TestCreatePermission_NullName()
        {
            try
            {
                if (this.mAccountManager.CreatePermission(this.mTestPermissionId, null))
                {
                    this.mFailures.Add("CreatePermission succeeded for null permission name");

                    try
                    {
                        if (!this.mAccountManager.DeletePermission(this.mTestPermissionId))
                        {
                            this.mFailures.Add(String.Format("WARNING : Failed to remove test permission Id ({0}) please remove it manually", this.mTestPermissionId));
                        }
                    }
                    catch (Exception de)
                    {
                        this.AddExceptionToFailures("RemovePermission", de);
                    }
                }

            }
            catch (Exception e)
            {
                this.AddExceptionToFailures("CreatePermission", e);
            }
        }

        private void TestCreatePermission_EmptyNameWithoutId()
        {
            try
            {
                long permissionId = this.mAccountManager.CreatePermission("");
                if (permissionId > 0)
                {
                    this.mFailures.Add("CreatePermission (without id) succeeded for empty permission name");

                    try
                    {
                        if (!this.mAccountManager.DeletePermission(permissionId))
                        {
                            this.mFailures.Add(String.Format("WARNING : Failed to remove test permission Id ({0}) please remove it manually", permissionId));
                        }
                    }
                    catch (Exception de)
                    {
                        this.AddExceptionToFailures("RemovePermission", de);
                    }
                }

            }
            catch (Exception e)
            {
                this.AddExceptionToFailures("CreatePermission", e);
            }
        }

        private void TestCreatePermission_NullNameWithoutId()
        {
            try
            {
                long permissionId = this.mAccountManager.CreatePermission("");
                if (permissionId > 0)
                {
                    this.mFailures.Add("CreatePermission (without id) succeeded for null permission name");

                    try
                    {
                        if (!this.mAccountManager.DeletePermission(permissionId))
                        {
                            this.mFailures.Add(String.Format("WARNING : Failed to remove test permission Id ({0}) please remove it manually", permissionId));
                        }
                    }
                    catch (Exception de)
                    {
                        this.AddExceptionToFailures("RemovePermission", de);
                    }
                }

            }
            catch (Exception e)
            {
                this.AddExceptionToFailures("CreatePermission", e);
            }
        }

        private void TestCreatePermission_IdAlreadyExists()
        {
            try
            {
                long permissionId = this.mAccountManager.CreatePermission("__test_permission__");
                if (permissionId > 0)
                {
                    if (this.mAccountManager.CreatePermission(permissionId, "__another_test_permission_name__"))
                    {
                        this.mFailures.Add("CreatePermission succeeded for conflicting permission Id");
                    }

                    try
                    {
                        if (!this.mAccountManager.DeletePermission(permissionId))
                        {
                            this.mFailures.Add(String.Format("WARNING : Failed to remove test permission Id ({0}) please remove it manually", permissionId));
                        }
                    }
                    catch (Exception de)
                    {
                        this.AddExceptionToFailures("RemovePermission", de);
                    }
                }
                else
                {
                    this.mFailures.Add("CreatePermission failed for valid permission name");
                }

            }
            catch (Exception e)
            {
                this.AddExceptionToFailures("CreatePermission", e);
            }
        }

        private void TestCreatePermission_NameAlreadyExists()
        {
            try
            {
                long permissionId = this.mAccountManager.CreatePermission("__test_permission__");
                if (permissionId > 0)
                {
                    if (this.mAccountManager.CreatePermission(permissionId, "__test_permission__"))
                    {
                        this.mFailures.Add("CreatePermission succeeded for conflicting permission name");
                    }

                    try
                    {
                        if (!this.mAccountManager.DeletePermission(permissionId))
                        {
                            this.mFailures.Add(String.Format("WARNING : Failed to remove test permission Id ({0}) please remove it manually", permissionId));
                        }
                    }
                    catch (Exception de)
                    {
                        this.AddExceptionToFailures("RemovePermission", de);
                    }
                }
                else
                {
                    this.mFailures.Add("CreatePermission failed for valid permission name");
                }

            }
            catch (Exception e)
            {
                this.AddExceptionToFailures("CreatePermission", e);
            }
        }

        private void TestCreatePermission_AnyId()
        {
            try
            {
                long permissionId = this.mAccountManager.CreatePermission("__test_permission__");
                if (permissionId > 0)
                {
                    try
                    {
                        if (!this.mAccountManager.DeletePermission(permissionId))
                        {
                            this.mFailures.Add(String.Format("WARNING : Failed to remove test permission Id ({0}) please remove it manually", permissionId));
                        }
                    }
                    catch (Exception de)
                    {
                        this.AddExceptionToFailures("RemovePermission", de);
                    }
                }
                else
                {
                    this.mFailures.Add("CreatePermission failed for valid permission name");
                }
            }
            catch (Exception e)
            {
                this.AddExceptionToFailures("CreatePermission", e);
            }
        }

        private void TestCreatePermission_ValidWithId()
        {
            try
            {
                if (this.mAccountManager.CreatePermission(this.mTestPermissionId, "__test_permission__"))
                {
                    try
                    {
                        if (!this.mAccountManager.DeletePermission(this.mTestPermissionId))
                        {
                            this.mFailures.Add(String.Format("WARNING : Failed to remove test permission Id ({0}) please remove it manually", this.mTestPermissionId));
                        }
                    }
                    catch (Exception de)
                    {
                        this.AddExceptionToFailures("RemovePermission", de);
                    }
                }
                else
                {
                    this.mFailures.Add("CreatePermission failed for valid permission name and Id");
                }
            }
            catch (Exception e)
            {
                this.AddExceptionToFailures("CreatePermission", e);
            }
        }

        #endregion

        #region DeletePermission

        private void TestDeletePermission()
        {
            this.TestDeletePermission_MissingId();
            this.TestDeletePermission_ValidId();
        }

        private void TestDeletePermission_MissingId()
        {
            try
            {
                if (this.mAccountManager.DeletePermission(this.mTestPermissionId))
                {
                    this.mFailures.Add("DeletePermission succeeded for missing permission Id");
                }
            }
            catch (Exception e)
            {
                this.AddExceptionToFailures("DeletePermission", e);
            }
        }

        private void TestDeletePermission_ValidId()
        {
            try
            {
                if (this.mAccountManager.CreatePermission(this.mTestPermissionId, "__test_permission__"))
                {
                    try
                    {
                        if (!this.mAccountManager.DeletePermission(this.mTestPermissionId))
                        {
                            this.mFailures.Add("DeletePermission failed to remove existing permission Id");
                            this.mFailures.Add(String.Format("WARNING : Please remove permission Id {0} manually", this.mTestPermissionId));
                        }
                    }
                    catch (Exception de)
                    {
                        this.AddExceptionToFailures("RemovePermission", de);
                    }
                }
                else
                {
                    this.mFailures.Add("CreatePermission failed for valid permission name and Id");
                }
            }
            catch (Exception e)
            {
                this.AddExceptionToFailures("CreatePermission", e);
            }
        }

        #endregion

        #region GrantPermission

        private void TestGrantPermission()
        {
            this.TestGrantPermission_InvalidUsername();
            this.TestGrantPermission_MissingPermissionId();
            this.TestGrantPermission_MissingUser();
            this.TestGrantPermission_Valid();
        }

        private void TestGrantPermission_InvalidUsername()
        {
            try
            {
                if (this.mAccountManager.GrantPermission("", this.mTestPermissionId))
                {
                    this.mFailures.Add("GrantPermission succeeded for empty username");

                    try
                    {
                        if (!this.mAccountManager.RevokePermission("", this.mTestPermissionId))
                        {
                            this.mFailures.Add(string.Format("WARNING: Failed to revoke invalid permission {0} for empty user", this.mTestPermissionId));
                        }
                    }
                    catch (Exception re)
                    {
                        this.AddExceptionToFailures("RevokePermission", re);
                    }
                }
            }
            catch (Exception e)
            {
                this.AddExceptionToFailures("GrantPermission", e);
            }
        }

        private void TestGrantPermission_MissingUser()
        {
            if (!this.RemoveTestUser())
            {
                throw new Exception("Failed to remove test user");
            }

            if (!this.CreateTestPermission())
            {
                throw new Exception("Failed to create test permission");
            }

            try
            {
                if (this.mAccountManager.GrantPermission(this.mTestUsername, this.mTestPermissionId))
                {
                    this.mFailures.Add("GrantPermission succeeded for missing username and existing permission");
                }
            }
            catch (Exception e)
            {
                this.AddExceptionToFailures("GrantPermission", e);
            }
            finally
            {
                if (!this.AddTestUser())
                {
                    throw new Exception("Failed to re-add test user");
                }

                if (!this.DeleteTestPermission())
                {
                    throw new Exception("Failed to delete test permission");
                }
            }
        }

        private void TestGrantPermission_MissingPermissionId()
        {
            try
            {
                if (this.mAccountManager.GrantPermission(this.mTestUsername, this.mTestPermissionId))
                {
                    this.mFailures.Add("GrantPermission succeeded for missing permission id");

                    try
                    {
                        if (!this.mAccountManager.RevokePermission(this.mTestUsername, this.mTestPermissionId))
                        {
                            this.mFailures.Add(string.Format("WARNING: Failed to revoke invalid permission {0} for empty user", this.mTestPermissionId));
                        }
                    }
                    catch (Exception re)
                    {
                        this.AddExceptionToFailures("RevokePermission", re);
                    }
                }
            }
            catch (Exception e)
            {
                this.AddExceptionToFailures("GrantPermission", e);
            }

        }

        private void TestGrantPermission_Valid()
        {
            if (!this.CreateTestPermission())
            {
                throw new Exception("Failed to create test permission");
            }

            try
            {
                if (!this.mAccountManager.GrantPermission(this.mTestUsername, this.mTestPermissionId))
                {
                    this.mFailures.Add("GrantPermission failed for existing username and existing permission");
                }

                try
                {
                    if (!this.mAccountManager.RevokePermission(this.mTestUsername, this.mTestPermissionId))
                    {
                        this.mFailures.Add(string.Format("WARNING: Failed to revoke test permission {0} for empty user", this.mTestPermissionId));
                    }
                }
                catch (Exception re)
                {
                    this.AddExceptionToFailures("RevokePermission", re);
                }
            }
            catch (Exception e)
            {
                this.AddExceptionToFailures("GrantPermission", e);
            }
            finally
            {
                if (!this.DeleteTestPermission())
                {
                    throw new Exception("Failed to delete test permission");
                }
            }
        }

        #endregion

        #region RevokePermission

        private void TestRevokePermission()
        {
            this.TestRevokePermission_InvalidUser();
            this.TestRevokePermission_InvalidId();
            this.TestRevokePermission_MissingId();
            this.TestRevokePermission_MissingUser();
            this.TestRevokePermission_Valid();
        }

        private void TestRevokePermission_InvalidUser()
        {
            try
            {
                if (this.mAccountManager.RevokePermission("", this.mTestPermissionId))
                {
                    this.mFailures.Add("RevokePermission succeeded with empty username");
                }
            }
            catch (Exception e)
            {
                this.AddExceptionToFailures("RevokePermission", e);
            }
        }

        private void TestRevokePermission_InvalidId()
        {
            try
            {
                if (this.mAccountManager.RevokePermission(this.mTestUsername, -1))
                {
                    this.mFailures.Add("RevokePermission succeeded with invalid permission id (a negative number)");
                }
            }
            catch (Exception e)
            {
                this.AddExceptionToFailures("RevokePermission", e);
            }
        }

        private void TestRevokePermission_MissingId()
        {
            try
            {
                if (this.mAccountManager.RevokePermission(this.mTestUsername, this.mTestPermissionId))
                {
                    this.mFailures.Add("RevokePermission succeeded for missing permissionId");
                }
            }
            catch (Exception e)
            {
                this.AddExceptionToFailures("RevokePermission", e);
            }
        }

        private void TestRevokePermission_MissingUser()
        {
            if (!this.RemoveTestUser())
            {
                throw new Exception("Failed to remove test user");
            }

            if (!this.CreateTestPermission())
            {
                this.AddTestUser();
                throw new Exception("Failed to add test permission");
            }

            try
            {
                if (this.mAccountManager.RevokePermission(this.mTestUsername, this.mTestPermissionId))
                {
                    this.mFailures.Add("RevokePermission succeeded with username that does not exist");
                }
            }
            catch (Exception e)
            {
                this.AddExceptionToFailures("RevokePermission", e);
            }
            finally
            {
                if (!this.AddTestUser())
                {
                    throw new Exception("Failed to re-add test user");
                }
                if (!this.DeleteTestPermission())
                {
                    throw new Exception("Failed to remove test permission");
                }
            }
        }

        private void TestRevokePermission_Valid()
        {
            // Already tested elsewhere
        }

        #endregion

        #region GetPermissions

        private void TestGetPermissions()
        {
            this.TestGetPermissions_ValidNoPermissions();
            this.TestGetPermissions_ValidWithPermissions();
        }

        private void TestGetPermissions_ValidNoPermissions()
        {
            try
            {
                IList<PermissionProperties> permissions = this.mAccountManager.GetPermissions(this.mTestUsername);
                if (permissions != null && permissions.Count > 0)
                {
                    this.mFailures.Add("GetPermissions returned permissions for test user which has not been granted any");
                }
            }
            catch (Exception e)
            {
                this.AddExceptionToFailures("GetPermissions", e);
            }
        }

        private void TestGetPermissions_ValidWithPermissions()
        {
            if (!this.CreateTestPermission())
            {
                throw new Exception("Failed to create test permission");
            }

            if (!this.GrantPermissionToTestUser())
            {
                this.DeleteTestPermission();
                throw new Exception("Failed to grant test user test permission");
            }

            try
            {
                IList<PermissionProperties> permissions = this.mAccountManager.GetPermissions(this.mTestUsername);
                if (permissions == null || permissions.Count != 1)
                {
                    this.mFailures.Add("GetPermissions failed to return the correct number of permissions");
                }
                else if (permissions[0].Id != this.mTestPermissionId)
                {
                    this.mFailures.Add("GetPermissions failed to return the correct test permission");
                }
            }
            catch (Exception e)
            {
                this.AddExceptionToFailures("GetPermissions", e);
            }
            finally
            {
                if (!this.RevokePermissionFromTestUser())
                {
                    throw new Exception("Failed to revoke test permission from test user");
                }
                if (!this.DeleteTestPermission())
                {
                    throw new Exception("Failed to remove test permission");
                }
            }

        }

        #endregion

        #region Get*ValidityPeriod

        private void TestValidityPeriods()
        {
            try
            {
                TimeSpan particpationValidityPeriod = this.mAccountManager.GetParticipationValidityPeriod();

                if (particpationValidityPeriod == null)
                {
                    this.mFailures.Add("Failed to get participation validity period");
                    return;
                }

                if (particpationValidityPeriod < TimeSpan.FromHours(1))
                {
                    this.mFailures.Add("Participation validity period is too short (Must be greater that 1 hour, preferably greater than 1 day)");
                }
            }
            catch (Exception e)
            {
                this.AddExceptionToFailures("Get*ValidityPeriod", e);
            }
        }

        #endregion

        #region ListAllPermissions

        private void TestListAllPermissions()
        {
            if (!this.CreateTestPermission())
            {
                throw new Exception("Failed to create test permission");
            }

            try
            {
                IList<PermissionProperties> permissions = this.mAccountManager.ListAllPermissions();
                if (permissions == null || permissions.Count < 1)
                {
                    this.mFailures.Add("ListAllPermissions failed to return the correct number of permissions");
                }
                else
                {
                    foreach (PermissionProperties permission in permissions)
                    {
                        if (permission.Id == this.mTestPermissionId)
                        {
                            return;
                        }
                    }

                    this.mFailures.Add("ListAllPermissions failed to return the test permission");
                }
            }
            catch (Exception e)
            {
                this.AddExceptionToFailures("ListAllPermissions", e);
            }
            finally
            {
                if (!this.DeleteTestPermission())
                {
                    throw new Exception("Failed to remove test permission");
                }
            }
        }

        #endregion


        private void AddExceptionToFailures(string testName, Exception e)
        {
            if (e.InnerException != null)
            {
                e = e.InnerException;
            }

            this.mFailures.Add(string.Format("{1} threw exception : {0}", e.Message, testName));
            System.Diagnostics.Trace.TraceError("{1} threw exception : {0}", e.Message, testName);
            System.Diagnostics.Trace.TraceError(e.StackTrace);

            return;
        }
    }
}
