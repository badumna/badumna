﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("DeiServer")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("25b10bd7-869c-4058-90ee-6afe077baaa1")]

[assembly: InternalsVisibleTo("DeiServerTests, PublicKey=00240000048000009400000006020000002400005253413100040000010001002312ebf1dbb64d30072ad917ad9bb58f112a6c354886999b6d6a36d5389d0b2fb231458d2c6259ab32a220de42bf7a2b7d038b53fb0651a2cdfcf702e8f28da42509d9324813748206815767895a89006a622d65c9381a6454e03e0faf641c3506faa73aeea296412019f98f476cfe966ad7148620f96c357368f799d396b6cb")]
