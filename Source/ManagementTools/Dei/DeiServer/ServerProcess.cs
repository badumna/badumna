//-----------------------------------------------------------------------
// <copyright file="ServerProcess.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace DeiServer
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.IO;
    using System.Security.Cryptography.X509Certificates;
    using System.Threading;
    using GermHarness;
    using Mono.Options;
    using X509CertificateCreator;

    /// <summary>
    /// Server process implements IHostedProcess has responsibility to initialize, start and stoping the 
    /// server process.
    /// </summary>
    internal class ServerProcess : IHostedProcess
    {
        /// <summary>
        /// Default certificate file name.
        /// </summary>
        private const string DefaultCertificateFile = "certificate.pfx";
        
        /// <summary>
        /// Default certificate common name.
        /// </summary>
        private const string DefaultCertificateCommonName = "DeiServer auto-generated self-signed certificate.";
        
        /// <summary>
        /// X509 certificate path.
        /// </summary>
        private readonly string certificatePath;
        
        /// <summary>
        /// Certificate creator.
        /// </summary>
        private readonly ICertificateCreator certificateCreator;

        /// <summary>
        /// Certificate password.
        /// </summary>
        private string certificatePassword = null;

        /// <summary>
        /// Certificate common name.
        /// </summary>
        private string certificateCommonName = DefaultCertificateCommonName;

        /// <summary>
        /// Server that need to be handled.
        /// </summary>
        private Server server;

        /// <summary>
        /// Server thread.
        /// </summary>
        private Thread serverThread;

        /// <summary>
        /// Port number where the server is listening on.
        /// </summary>
        private int port = 21248;

        /// <summary>
        /// X509 certificate.
        /// </summary>
        private X509Certificate2 certificate;

        /// <summary>
        /// The key validity period.
        /// </summary>
        private TimeSpan keyValidityPeriod = TimeSpan.FromHours(24);

        /// <summary>
        /// A value indicating whether the ssl connection is used.
        /// </summary>
        private bool useSslConnection = false;

        /// <summary>
        /// Option set (i.e. list all of the possible command line arguments).
        /// </summary>
        private OptionSet optionSet;

        /// <summary>
        /// The account manager to use.
        /// </summary>
        private IAccountManager accountManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="ServerProcess"/> class.
        /// </summary>
        /// <param name="certificatePath">X509 certificate path</param>
        /// <param name="certificateCreator">Certificate creator instance.</param>
        internal ServerProcess(string certificatePath, ICertificateCreator certificateCreator)
        {
            if (certificatePath == null)
            {
                throw new ArgumentNullException("x509CertificatePath");
            }

            if (certificateCreator == null)
            {
                throw new ArgumentNullException("certificateCreator");
            }

            this.certificatePath = certificatePath;
            this.certificateCreator = certificateCreator;

            this.optionSet = new OptionSet()
            {
                { "p|port=", "the port to listen on.", (ushort v) => this.port = v },
                { "s|ssl", "use SSL.", v => 
                    { 
                        if (v != null) 
                        {
                            this.useSslConnection = true; 
                        } 
                    } 
                },
                { "g|generate-keys", "generate a new key pair.", v => Server.GenerateNewKeyPair() },
                { "c|certificate-password=", "password for certificate file.", v => this.certificatePassword = v },
                { "n|common-name=", "common name for auto-generated certificate.", v => this.certificateCommonName = v },
                { "k|key-expiry=", "expiry time for the participation key in minutes.", v => this.keyValidityPeriod = TimeSpan.FromMinutes(Convert.ToDouble(v)) }
            };
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ServerProcess"/> class.
        /// </summary>
        internal ServerProcess() :
            this(ServerProcess.DefaultCertificateFile, new CertificateCreator())
        {
        }

        /// <summary>
        /// OnInitialize implementation.
        /// </summary>
        /// <param name="arguments">Command line arguments.</param>
        public void OnInitialize(ref string[] arguments)
        {
            List<string> extras = this.optionSet.Parse(arguments);

            arguments = extras.ToArray();

            if (this.useSslConnection)
            {
                if (File.Exists(this.certificatePath) && this.certificatePassword != null)
                {
                    try
                    {
                        this.certificate = new X509Certificate2(this.certificatePath, this.certificatePassword);
                        return;
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(
                            "Warning: Could not use certificate {0}, generating self-certified certificate.",
                            this.certificatePath);
                        Console.WriteLine(ex.Message);
                    }
                }

                this.certificate = this.certificateCreator.CreateCertificate(
                    this.certificateCommonName,
                    new DateTime(2099, 12, 31));
            }
        }

        /// <summary>
        /// OnPerformRegularTasks implementation is not implemented in this class.
        /// </summary>
        /// <param name="delayMilliseconds">Delay in milliseconds.</param>
        /// <returns>Always return true.</returns>
        public bool OnPerformRegularTasks(int delayMilliseconds)
        {
            return true;
        }

        /// <summary>
        /// OnProcessRequest is not implemented in this class.
        /// </summary>
        /// <param name="requestType">Request type.</param>
        /// <param name="request">Request message.</param>
        /// <returns>Return null (not implemented).</returns>
        public byte[] OnProcessRequest(int requestType, byte[] request)
        {
            return null;
        }

        /// <summary>
        /// OnStart implementation, start the server using the apropriate data provider 
        /// given on the AppSettings.
        /// </summary>
        /// <returns>Return true on success.</returns>
        public bool OnStart()
        {
            this.server = null;
            this.serverThread = null;

            string dllName = ConfigurationManager.AppSettings["accountManagerDllName"];
            if (dllName == null)
            {
                Console.Error.WriteLine("Configuration error: no accountManagerDllName given.");
                return false;
            }

            AccountManagerLoader creator = new AccountManagerLoader(dllName, this.keyValidityPeriod);

            try
            {
                this.accountManager = creator.Create();
            }
            catch (FileNotFoundException)
            {
                Console.Error.WriteLine("File {0} not found.", dllName);
                return false;
            }
            catch (FileLoadException)
            {
                Console.Error.WriteLine("File {0} can not be loaded.", dllName);
                return false;
            }
            catch (BadImageFormatException)
            {
                Console.Error.WriteLine("File {0} is not a valid Dll.", dllName);
                return false;
            }
            catch (MissingMemberException)
            {
                Console.Error.WriteLine("File {0} doesn't implement the required factory class.", dllName);
                return false;
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Exception caught when creating the account manager: " + e);
                return false;
            }

            if (this.accountManager == null)
            {
                Console.Error.WriteLine("Configuration error: Account manager factory returned null");
                return false;
            }
            
            Console.WriteLine("Using the account manager: " + accountManager.GetType().ToString());

            // initilize the account manager;
            this.accountManager.Initialize();

            Console.WriteLine("Key expiry time: {0} minutes", this.keyValidityPeriod.TotalMinutes);

            this.server = new Server(this.port, accountManager, this.certificate, this.useSslConnection);

            this.serverThread = new Thread(this.server.Start);
            this.serverThread.IsBackground = true;
            this.serverThread.Start();

            return true;
        }

        /// <summary>
        /// OnShutdown implementaton, stop the server and abort the server thread.
        /// </summary>
        public void OnShutdown()
        {
            if (this.server != null)
            {
                this.server.Stop();

                // TODO : This is pretty ugly - make it play nice!
                try
                {
                    this.serverThread.Abort();
                    this.serverThread = null;
                }
                catch 
                { 
                }

                this.server = null;
            }

            if(this.accountManager != null)
            {
                this.accountManager.Shutdown();
            }
        }

        /// <summary>
        /// Write options description back to the console.
        /// </summary>
        /// <param name="tw">Text writer.</param>
        public void WriteOptionsDescription(TextWriter tw)
        {
            this.optionSet.WriteOptionDescriptions(tw);
        }
    }
}
