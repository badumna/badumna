﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Net.Sockets;
using System.Net.Security;
using System.Xml;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.IO;

using Dei;
using Dei.ServerUtilities;
using Dei.Utilities;

namespace Dei.Protocol
{
    class ServerProtocol : BaseProtocol
    {
        private const int MaxWrongLoginTries = 3;
        private UserProperties mLoggedInUser;
        private TokenGenerator mTokenGenerator;

        public ServerProtocol(Stream clientStream, TokenGenerator tokenGenerator)
            : base(clientStream)
        {
            this.mTokenGenerator = tokenGenerator;
        }



        #region ProcessCommand

        protected override void ProtocolVersionError(Version myVersion, Version theirVersion)
        {
            System.Diagnostics.Trace.TraceInformation("client: Incompatable version");
        }

        protected override void ProcessCommand(Commands command, XmlReader reader)
        {
            SerializerWrapper failureReasonsSerializer = new SerializerWrapper(typeof(FailureReasons));

            try
            {
                switch (command)
                {
                    case Commands.RequestLogin:
                        this.OnLogin(this.GetParamter<UserProperties>(reader));
                        break;

                    case Commands.RequestLogout:
                        this.OnLogout(this.GetParamter<UserProperties>(reader));
                        break;

                    case Commands.RequestNewAccount:
                        this.OnNewAccount(this.GetParamter<UserProperties>(reader));
                        break;

                    case Commands.RequestDeleteAcount:
                        this.OnDeleteAcount(this.GetParamter<UserProperties>(reader));
                        break;

                    case Commands.RequestChangeAccount:
                        this.OnChangeAccount(this.GetParamter<UserProperties>(reader));
                        break;

                    case Commands.RequestCreatePermission:
                        this.OnCreatePermission(this.GetParamter<PermissionProperties>(reader));
                        break;

                    case Commands.RequestRemovePermission:
                        this.OnRemovePermission(this.GetParamter<PermissionProperties>(reader));
                        break;

                    case Commands.RequestGrantPermission:
                        this.OnGrantPermission(this.GetParamter<UserProperties>(reader), this.GetParamter<PermissionProperties>(reader));
                        break;

                    case Commands.RequestRevokePermission:
                        this.OnRevokePermission(this.GetParamter<UserProperties>(reader), this.GetParamter<PermissionProperties>(reader));
                        break;


                    case Commands.RequestClientCertificate:
                        this.OnClientCertificate(this.GetParamter<UserCertificate>(reader));
                        break;

                    case Commands.RequestPermissionList:
                        this.OnRequestPermissionList(this.GetParamter<UserProperties>(reader), this.GetParamter<PublicKey>(reader));
                        break;

                    case Commands.RequestListPermissionTypes:
                        this.OnRequestListPermissionTypes(this.GetParamter<UserProperties>(reader));
                        break;

                    case Commands.RequestParticipationKey:
                        this.OnRequestParticipationKey(this.GetParamter<UserProperties>(reader), this.GetParamter<TokenRequest>(reader));
                        break;

                    case Commands.RequestServersPublicKey:
                        this.OnRequestPublicKey(this.GetParamter<UserProperties>(reader), this.GetParamter<TokenRequest>(reader));
                        break;

                    case Commands.RequestServerTime:
                        this.OnRequestTime(this.GetParamter<UserProperties>(reader));
                        break;

                    case Commands.RequestUserCount:
                        this.OnRequestUserCount(this.GetParamter<ListUserRequest>(reader));
                        break;

                    case Commands.RequestUserList:
                        this.OnRequestListUsers(this.GetParamter<ListUserRequest>(reader));
                        break;

                    case Commands.None:
                        break;
                }
            }
            catch (Exception e)
            {
                this.ProtocolError("Exception thrown while processing command : {0}", e.Message);
                this.CloseConnection();
            }
        }

        #endregion

        private void OnLogin(UserProperties userProperties)
        {
            System.Diagnostics.Trace.TraceInformation("client: OnLogin()");

            if (string.IsNullOrEmpty(userProperties.Login) || string.IsNullOrEmpty(userProperties.Password) || !this.IsValidName(userProperties.Login))
            {
                this.SendRequest(Commands.RequestLoginRejected);
            }

            try
            {
                if (this.mTokenGenerator.AccountManager.AuthenticateUser(userProperties.Login, userProperties.Password))
                {
                    this.mLoggedInUser = userProperties;
                    this.SendRequest(Commands.RequestLoginAccepted, userProperties);
                    return;
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Trace.TraceError("Caught exception : {0}", e.Message);
            }
            this.SendRequest(Commands.RequestLoginRejected);
        }

        private void OnLogout(UserProperties userProperties)
        {
            System.Diagnostics.Trace.TraceInformation("client: OnLogout()");

            if (string.IsNullOrEmpty(userProperties.Login))
            {
                this.SendRequest(Commands.RequestLogoutFailed, FailureReasons.InvalidRequest);
            }

            if (this.IsLoggedInAndHasPermission(Commands.RequestLogoutFailed, userProperties, PermissionTypes.None))
            {
                this.SendRequest(Commands.RequestLogoutAccepted);
            }
        }

        private void OnNewAccount(UserProperties newUserProperties)
        {
            System.Diagnostics.Trace.TraceInformation("client: OnNewAccount()");

            if (string.IsNullOrEmpty(newUserProperties.Login) || string.IsNullOrEmpty(newUserProperties.Password)
                || !this.IsValidName(newUserProperties.Login))
            {
                this.SendRequest(Commands.RequestNewAccountFailed, FailureReasons.InvalidRequest);
            }

            if (this.IsLoggedInAndHasPermission(Commands.RequestNewAccountFailed, this.mLoggedInUser, PermissionTypes.CreateUsers))
            {
                try
                {
                    if (!this.mTokenGenerator.AccountManager.AuthenticateUser(newUserProperties.Login, newUserProperties.Password))
                    {
                        System.Diagnostics.Trace.TraceInformation("client: OnNewAccount() creating a new account");
                        if (this.mTokenGenerator.AccountManager.CreateAccount(newUserProperties.Login, newUserProperties.Password))
                        {
                            this.SendRequest(Commands.RequestNewAccountAccepted, newUserProperties);
                        }
                        else
                        {
                            this.SendRequest(Commands.RequestNewAccountFailed, FailureReasons.NoneGiven);
                        }
                    }
                    else
                    {
                        System.Diagnostics.Trace.TraceInformation("client: OnNewAccount() an account already exists");
                        this.SendRequest(Commands.RequestNewAccountFailed, FailureReasons.AccountAlreadyExists);
                    }
                }
                catch (Exception e)
                {
                    System.Diagnostics.Trace.TraceError("Caught exception : {0}", e.Message);
                    this.SendRequest(Commands.RequestNewAccountFailed, FailureReasons.NoneGiven);
                }
            }
        }

        private void OnDeleteAcount(UserProperties userProperties)
        {
            System.Diagnostics.Trace.TraceInformation("client: OnDeleteAcount()");

            if (string.IsNullOrEmpty(userProperties.Login) || !this.IsValidName(userProperties.Login))
            {
                this.SendRequest(Commands.RequestDeleteAccountFailed, FailureReasons.InvalidRequest);
            }

            if (this.IsLoggedInAndHasPermission(Commands.RequestDeleteAccountFailed, this.mLoggedInUser, PermissionTypes.CreateUsers))
            {
                // Try and remove the users permissions first.
                try
                {
                    IList<PermissionProperties> usersPermissions = this.mTokenGenerator.AccountManager.GetPermissions(userProperties.Login);
                    foreach (PermissionProperties permission in usersPermissions)
                    {
                        try
                        {
                            this.mTokenGenerator.AccountManager.RevokePermission(userProperties.Login, permission.Id);
                        }
                        catch (Exception e)
                        {
                            System.Diagnostics.Trace.TraceError("Caught exception : {0}", e.Message);
                        }
                    }
                }
                catch (Exception e)
                {
                    System.Diagnostics.Trace.TraceError("Caught exception : {0}", e.Message);
                }

                try
                {
                    if (this.mTokenGenerator.AccountManager.DeleteAccount(userProperties.Login))
                    {
                        this.SendRequest(Commands.RequestDeleteAccountAccepted);
                        return;
                    }
                }
                catch (Exception e)
                {
                    System.Diagnostics.Trace.TraceError("Caught exception : {0}", e.Message);
                }

                this.SendRequest(Commands.RequestDeleteAccountFailed, FailureReasons.NoneGiven);
            }
        }

        private void OnChangeAccount(UserProperties userProperties)
        {
            System.Diagnostics.Trace.TraceInformation("client: OnChangeAccount()");

            if (string.IsNullOrEmpty(userProperties.Login) || string.IsNullOrEmpty(userProperties.Password)
                || !this.IsValidName(userProperties.Login))
            {
                this.SendRequest(Commands.RequestChangeAccountRejected, FailureReasons.InvalidRequest);
            }

            if (this.IsLoggedInAndHasPermission(Commands.RequestChangeAccountRejected, userProperties, PermissionTypes.None))
            {
                try
                {
                    if (this.mTokenGenerator.AccountManager.ChangePassword(userProperties.Login, userProperties.Password))
                    {
                        this.SendRequest(Commands.RequestChangeAccountAccepted);
                    }
                    else
                    {
                        this.SendRequest(Commands.RequestChangeAccountRejected, FailureReasons.NoneGiven);
                    }
                }
                catch (Exception e)
                {
                    System.Diagnostics.Trace.TraceError("Caught exception : {0}", e.Message);
                    this.SendRequest(Commands.RequestChangeAccountRejected, FailureReasons.NoneGiven);
                }
            }
        }

        private void OnCreatePermission(PermissionProperties permissionProperty)
        {
            System.Diagnostics.Trace.TraceInformation("client: OnCreatePermission()");

            if (string.IsNullOrEmpty(permissionProperty.Name) || !this.IsValidName(permissionProperty.Name))
            {
                this.SendRequest(Commands.RequestCreatePermissionRejected, FailureReasons.InvalidRequest);
            }

            if (this.IsLoggedInAndHasPermission(Commands.RequestCreatePermissionRejected, this.mLoggedInUser, PermissionTypes.CreatePermissions))
            {
                try
                {
                    long permissionId = this.mTokenGenerator.AccountManager.CreatePermission(permissionProperty.Name);
                    if (permissionId > 0)
                    {
                        permissionProperty.Id = permissionId;
                        this.SendRequest(Commands.RequestCreatePermissionAccepted, permissionProperty);
                    }
                    else
                    {
                        this.SendRequest(Commands.RequestCreatePermissionRejected, FailureReasons.NoneGiven);
                    }
                }
                catch (Exception e)
                {
                    System.Diagnostics.Trace.TraceError("Caught exception : {0}", e.Message);
                    this.SendRequest(Commands.RequestCreatePermissionRejected, FailureReasons.NoneGiven);
                }
            }
        }

        private void OnRemovePermission(PermissionProperties permissionProperty)
        {
            System.Diagnostics.Trace.TraceInformation("client: OnCreatePermission()");

            if (permissionProperty.Id <= 0)
            {
                this.SendRequest(Commands.RequestRemovePermissionRejected, FailureReasons.InvalidRequest);
            }

            if (this.IsLoggedInAndHasPermission(Commands.RequestRemovePermissionRejected, this.mLoggedInUser, PermissionTypes.CreatePermissions))
            {
                try
                {
                    if (this.mTokenGenerator.AccountManager.DeletePermission(permissionProperty.Id))
                    {
                        this.SendRequest(Commands.RequestRemovePermissionAccepted);
                    }
                    else
                    {
                        this.SendRequest(Commands.RequestRemovePermissionRejected, FailureReasons.NoneGiven);
                    }
                }
                catch (Exception e)
                {
                    System.Diagnostics.Trace.TraceError("Caught exception : {0}", e.Message);
                    this.SendRequest(Commands.RequestRemovePermissionRejected, FailureReasons.NoneGiven);
                }
            }
        }

        private void OnGrantPermission(UserProperties userProperties, PermissionProperties permissionProperty)
        {
            System.Diagnostics.Trace.TraceInformation("client: OnGrantPermission()");

            if (string.IsNullOrEmpty(userProperties.Login) || !this.IsValidName(userProperties.Login) || permissionProperty.Id <= 0)
            {
                this.SendRequest(Commands.RequestGrantPermissionRejected, FailureReasons.InvalidRequest);
            }

            if (this.IsLoggedInAndHasPermission(Commands.RequestGrantPermissionRejected, this.mLoggedInUser, PermissionTypes.AssignPermissions))
            {
                try
                {
                    if (this.mTokenGenerator.AccountManager.GrantPermission(userProperties.Login, permissionProperty.Id))
                    {
                        this.SendRequest(Commands.RequestGrantPermissionAccepted);
                    }
                    else
                    {
                        this.SendRequest(Commands.RequestGrantPermissionRejected, FailureReasons.NoneGiven);
                    }
                }
                catch (Exception e)
                {
                    System.Diagnostics.Trace.TraceError("Caught exception : {0}", e.Message);
                    this.SendRequest(Commands.RequestGrantPermissionRejected, FailureReasons.NoneGiven);
                }
            }
        }

        private void OnRevokePermission(UserProperties userProperties, PermissionProperties permissionProperty)
        {
            System.Diagnostics.Trace.TraceInformation("client: OnRevokePermission()");

            if (string.IsNullOrEmpty(userProperties.Login) || !this.IsValidName(userProperties.Login) || permissionProperty.Id <= 0)
            {
                this.SendRequest(Commands.RequestRevokePermissionRejected, FailureReasons.InvalidRequest);
            }

            if (this.IsLoggedInAndHasPermission(Commands.RequestRevokePermissionRejected, this.mLoggedInUser, PermissionTypes.AssignPermissions))
            {
                try
                {
                    if (this.mTokenGenerator.AccountManager.RevokePermission(userProperties.Login, permissionProperty.Id))
                    {
                        this.SendRequest(Commands.RequestRevokePermissionAccepted);
                    }
                    else
                    {
                        this.SendRequest(Commands.RequestRevokePermissionRejected, FailureReasons.NoneGiven);
                    }
                }
                catch (Exception e)
                {
                    System.Diagnostics.Trace.TraceError("Caught exception : {0}", e.Message);
                    this.SendRequest(Commands.RequestRevokePermissionRejected, FailureReasons.NoneGiven);
                }
            }
        }

        private void OnClientCertificate(UserCertificate unsignedCertificate)
        {
            System.Diagnostics.Trace.TraceInformation("client: OnClientCertificate()");

            if (!unsignedCertificate.IsValid())
            {
                this.SendRequest(Commands.RequestClientCertificateFailed, FailureReasons.InvalidRequest);
            }

            UserProperties userProperties = new UserProperties(unsignedCertificate.UserName, "");
            if (this.IsLoggedInAndHasPermission(Commands.RequestClientCertificateFailed, userProperties, PermissionTypes.Participation))
            {
                try
                {
                    unsignedCertificate.UserId = this.mTokenGenerator.AccountManager.GetUserId(this.mLoggedInUser.Login);
                    UserCertificate certificate = this.mTokenGenerator.GetSignedCertificates(unsignedCertificate);

                    this.SendRequest(Commands.RequestClientCertificateAccepted, certificate);
                    return;
                }
                catch (Exception e)
                {
                    System.Diagnostics.Trace.TraceInformation(String.Format("Failed to sign certificate : {0}", e.Message));
                    this.SendRequest(Commands.RequestClientCertificateFailed, FailureReasons.NoneGiven);
                    return;
                }
            }
        }

        public void OnRequestPermissionList(UserProperties userProperties, PublicKey publicKey)
        {
            System.Diagnostics.Trace.TraceInformation("client: OnRequestPermissionList()");

            if (string.IsNullOrEmpty(userProperties.Login) || !this.IsValidName(userProperties.Login) || publicKey.Key == null
                || publicKey.Key == null)
            {
                this.SendRequest(Commands.RequestPermissionListFailed, FailureReasons.InvalidRequest);
            }

            if (this.IsLoggedInAndHasPermission(Commands.RequestPermissionListFailed, userProperties, PermissionTypes.None))
            {
                try
                {
                    IList<PermissionProperties> permissions = this.mTokenGenerator.AccountManager.GetPermissions(userProperties.Login);
                    TimeSpan permissionValidityPeriod = this.mTokenGenerator.AccountManager.GetParticipationValidityPeriod();

                    if (permissions != null)
                    {
                        foreach (PermissionProperties properties in permissions)
                        {
                            Permission permission = new Permission(new UtcDateTime(DateTime.UtcNow + permissionValidityPeriod),
                                    properties.Id, publicKey.Key, properties.Name);
                            permission.Signiture = this.mTokenGenerator.Sign(permission.State);
                            this.SendRequest(Commands.AddPermission, permission);
                        }
                    }

                    this.SendRequest(Commands.RequestPermissionListComplete);
                }
                catch (Exception e)
                {
                    System.Diagnostics.Trace.TraceError("Caught exception : {0}", e.Message);
                    this.SendRequest(Commands.RequestPermissionListFailed, FailureReasons.NoneGiven);
                }
            }
        }

        public void OnRequestListPermissionTypes(UserProperties userProperties)
        {
            System.Diagnostics.Trace.TraceInformation("client: OnRequestListPermissionTypes()");

            if (!this.IsValidName(userProperties.Login))
            {
                this.SendRequest(Commands.RequestListPermissionTypesFailed, FailureReasons.InvalidRequest);
            }
            PermissionTypes permission = PermissionTypes.ViewPermissions;
            if (userProperties.Login == this.mLoggedInUser.Login)
            {
                permission = PermissionTypes.None;
            }

            if (this.IsLoggedInAndHasPermission(Commands.RequestListPermissionTypesFailed, this.mLoggedInUser, permission))
            {
                try
                {
                    IList<PermissionProperties> permissionProperties = null;
                    if (string.IsNullOrEmpty(userProperties.Login))
                    {
                        System.Diagnostics.Trace.TraceInformation("Listing all permissions ...");
                        permissionProperties = this.mTokenGenerator.AccountManager.ListAllPermissions();
                    }
                    else
                    {
                        System.Diagnostics.Trace.TraceInformation("Listing permissions for {0} ...", userProperties.Login);
                        permissionProperties = this.mTokenGenerator.AccountManager.GetPermissions(userProperties.Login);
                    }

                    if (permissionProperties != null)
                    {
                        foreach (PermissionProperties permissionProperty in permissionProperties)
                        {
                            this.SendRequest(Commands.AddPermissionType, permissionProperty);
                        }
                    }

                    this.SendRequest(Commands.RequestListPermissionTypes);
                }
                catch (Exception e)
                {
                    System.Diagnostics.Trace.TraceError("Caught exception : {0}", e.Message);
                    this.SendRequest(Commands.RequestListPermissionTypesFailed, FailureReasons.NoneGiven);
                }
            }
        }

        private void OnCloseConnection(UserProperties up)
        {
            System.Diagnostics.Trace.TraceInformation("client: OnCloseConnection()");
        }

        private void OnRequestParticipationKey(UserProperties userProperties, TokenRequest request)
        {
            System.Diagnostics.Trace.TraceInformation("client: OnRequestParticipationKey()");

            if (string.IsNullOrEmpty(userProperties.Login) || !this.IsValidName(userProperties.Login))
            {
                this.SendRequest(Commands.RequestParticipationKeyRejected, FailureReasons.InvalidRequest);
            }

            if (this.IsLoggedInAndHasPermission(Commands.RequestParticipationKeyRejected, userProperties, PermissionTypes.Participation))
            {
                ParticipationKey key = this.mTokenGenerator.GetParticipationKey(request.Number);
                if (key != null)
                {
                    this.SendRequest(Commands.RequestParticipationKeyAccepted, key);
                }
                else
                {
                    this.SendRequest(Commands.RequestParticipationKeyRejected, FailureReasons.InvalidRequest);
                }
            }
        }

        private void OnRequestPublicKey(UserProperties userProperties, TokenRequest request)
        {
            System.Diagnostics.Trace.TraceInformation("client: OnRequestPublicKey()");

            if (string.IsNullOrEmpty(userProperties.Login) || !this.IsValidName(userProperties.Login))
            {
                this.SendRequest(Commands.RequestServersPublicKeyRejected, FailureReasons.InvalidRequest);
            }

            if (this.IsLoggedInAndHasPermission(Commands.RequestServersPublicKeyRejected, userProperties, PermissionTypes.Participation))
            {
                PublicKey key = this.mTokenGenerator.GetPublicKeys(request.Number);
                if (key != null)
                {
                    this.SendRequest(Commands.RequestServersPublicKeyAccepted, userProperties, key);
                }
                else
                {
                    this.SendRequest(Commands.RequestServersPublicKeyRejected, FailureReasons.InvalidRequest);
                }
            }
        }

        private void OnRequestTime(UserProperties userProperties)
        {
            System.Diagnostics.Trace.TraceInformation("client: OnRequestTime()");

            if (string.IsNullOrEmpty(userProperties.Login) || !this.IsValidName(userProperties.Login))
            {
                this.SendRequest(Commands.RequestServerTimeRejected, FailureReasons.InvalidRequest);
            }

            if (this.IsLoggedInAndHasPermission(Commands.RequestServerTimeRejected, userProperties, PermissionTypes.Participation))
            {
                try
                {
                    ServerTime time = new ServerTime(new UtcDateTime(DateTime.UtcNow), this.mTokenGenerator.AccountManager.GetParticipationValidityPeriod());
                    this.SendRequest(Commands.RequestServerTimeAccepted, userProperties, time);
                }
                catch (Exception e)
                {
                    System.Diagnostics.Trace.TraceError("Caught exception : {0}", e.Message);
                    this.SendRequest(Commands.RequestServerTimeRejected, FailureReasons.NoneGiven);
                }
            }
        }

        private void OnRequestListUsers(ListUserRequest request)
        {
            System.Diagnostics.Trace.TraceInformation("client: OnRequestListUsers()");

            if (string.IsNullOrEmpty(request.UsernameGlob) || !this.IsExpressionSafe(request.UsernameGlob))
            {
                this.SendRequest(Commands.RequestUserListFailed, FailureReasons.InvalidRequest);
            }

            if (this.IsLoggedInAndHasPermission(Commands.RequestUserListFailed, this.mLoggedInUser, PermissionTypes.ViewUsers))
            {
                try
                {
                    List<string> userList = new List<string>(this.mTokenGenerator.AccountManager.FindUsers(
                        request.UsernameGlob, request.LimitStart, request.NumberOfResults));

                    if (userList.Count > request.NumberOfResults)
                    {
                        userList.RemoveRange((int)request.NumberOfResults, (int)(userList.Count - request.NumberOfResults));
                    }
                    request.Result = userList.ToArray();

                    this.SendRequest(Commands.RequestUserListComplete, request);
                }
                catch (Exception e)
                {
                    System.Diagnostics.Trace.TraceError("Caught exception : {0}", e.Message);
                    this.SendRequest(Commands.RequestUserListFailed, FailureReasons.NoneGiven);
                }
            }
        }

        private void OnRequestUserCount(ListUserRequest request)
        {
            System.Diagnostics.Trace.TraceInformation("client: OnRequestUserCount()");

            if (string.IsNullOrEmpty(request.UsernameGlob) || !this.IsExpressionSafe(request.UsernameGlob))
            {
                this.SendRequest(Commands.RequestUserCountFailed, FailureReasons.InvalidRequest);
            }

            if (this.IsLoggedInAndHasPermission(Commands.RequestUserCountFailed, this.mLoggedInUser, PermissionTypes.ViewUsers))
            {
                try
                {
                    long userCount = this.mTokenGenerator.AccountManager.GetUserCount(request.UsernameGlob);

                    request.LimitStart = 0;
                    request.NumberOfResults = userCount;

                    this.SendRequest(Commands.RequestUserCountComplete, request);
                }
                catch (Exception e)
                {
                    System.Diagnostics.Trace.TraceError("Caught exception : {0}", e.Message);
                    this.SendRequest(Commands.RequestUserCountFailed, FailureReasons.NoneGiven);
                }
            }
        }


        private bool IsLoggedInAndHasPermission(Commands onFailureReply, UserProperties userProperties, PermissionTypes requiredPermission)
        {
            try
            {
                if (this.mLoggedInUser != null && this.mLoggedInUser.Login == userProperties.Login)
                {
                    if (requiredPermission == PermissionTypes.None
                        || this.mTokenGenerator.AccountManager.HasPermission(userProperties.Login, (long)requiredPermission))
                    {
                        return true;
                    }
                    else
                    {
                        System.Diagnostics.Trace.TraceInformation("   User doesn't have permission");
                        this.SendRequest(onFailureReply, FailureReasons.PermissionDenied);
                        return false;
                    }
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Trace.TraceError("Caught exception : {0}", e.Message);
                this.SendRequest(onFailureReply, FailureReasons.NoneGiven);
                return false;
            }

            System.Diagnostics.Trace.TraceInformation("   User NOT logged in");
            this.SendRequest(onFailureReply, FailureReasons.AuthenticationFailed);
            return false;
        }


        private bool IsExpressionSafe(string expression)
        {

            return true;
        }

        private bool IsValidName(string name) // username or permission name
        {

            return true;
        }
    }
}
