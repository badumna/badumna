//-----------------------------------------------------------------------
// <copyright file="Program.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace DeiServer
{
    using System;
    using System.Collections.Generic;
    using GermHarness;
    using Mono.Options;

    /// <summary>
    /// The entry point of the DeiServer application.
    /// </summary>
    internal class Program
    {
        /// <summary>
        /// Server process.
        /// </summary>
        private static ServerProcess process = new ServerProcess();

        /// <summary>
        /// Process harness.
        /// </summary>
        private static ProcessHarness harness = new ProcessHarness();

        /// <summary>
        /// Option set for parsing command line arguments.
        /// </summary>
        private static OptionSet optionSet = new OptionSet()
            {
                { "h|help", "show this message and exit.", v =>
                    {
                        if (v != null)
                        {
                            Program.WriteUsageAndExit();
                        }
                    }
                }
            };

        /// <summary>
        /// Exit codes enumeration.
        /// </summary>
        private enum ExitCodes : int
        {
            /// <summary>
            /// Applicatio exit with no error.
            /// </summary>
            Success = 0,

            /// <summary>
            /// Application exit due to initalization failure.
            /// </summary>
            InitializationFailure,

            /// <summary>
            /// Application exit due to unhandled exception.
            /// </summary>
            UnhandledException
        }

        /// <summary>
        /// Entry point of DeiServer application.
        /// </summary>
        /// <param name="args">Command line arguments.</param>
        /// <returns>Exit code.</returns>
        internal static int Main(string[] args)
        {
            try
            {
                List<string> extras = optionSet.Parse(args);

                if (Program.harness.Initialize(ref args, Program.process))
                {
                    if (args.Length > 0)
                    {
                        Program.HandleUnknownArguments(args);
                    }
                    else
                    {
                        harness.Start();
                    }
                }
                else
                {
                    Console.WriteLine("Could not initialize harness.");
                    return (int)ExitCodes.InitializationFailure;
                }
            }
            catch (OptionException ex)
            {
                Console.WriteLine("DeiServer: {0}", ex.Message);
                Console.WriteLine("Try `DeiServer --help' for more information.");
                return (int)ExitCodes.UnhandledException;
            }

            return (int)ExitCodes.Success;
        }

        /// <summary>
        /// Write error message listing unknown arguments.
        /// </summary>
        /// <param name="arguments">Unknown arguments.</param>
        private static void HandleUnknownArguments(string[] arguments)
        {
            Console.Write("DeiServer: Unknown argument(s):");
            foreach (string arg in arguments)
            {
                Console.Write(" " + arg);
            }

            Console.WriteLine(".");
            Console.WriteLine("Try `DeiServer --help' for more information.");
        }

        /// <summary>
        /// Write usage instructions and exit.
        /// </summary>
        private static void WriteUsageAndExit()
        {
            string assemblyName = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name;
            Console.WriteLine("Usage: DeiServer [Options]");
            Console.WriteLine("Start a DeiServer on the local machine.");
            Console.WriteLine(string.Empty);
            Console.WriteLine("Options:");
            Program.harness.WriteOptionDescriptions(Console.Out);
            Program.process.WriteOptionsDescription(Console.Out);
            Program.optionSet.WriteOptionDescriptions(Console.Out);
            Environment.Exit((int)ExitCodes.Success);
        }
    }
}
