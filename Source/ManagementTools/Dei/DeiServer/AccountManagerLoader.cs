﻿//-----------------------------------------------------------------------
// <copyright file="AccountManagerLoader.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.IO;
using System.Reflection;

namespace DeiServer
{
    /// <summary>
    /// The class used to create the account manager.
    /// </summary>
    public class AccountManagerLoader
    {
        /// <summary>
        /// The data provider string.
        /// </summary>
        private string dllName;

        /// <summary>
        /// The validity period of tokens.
        /// </summary>
        private TimeSpan keyValidityPeriod;

        /// <summary>
        /// Initializes a new instance of the <see cref="AccountManagerLoader"/> class.
        /// </summary>
        /// <param name="dllName">The name of the dll.</param>
        /// <param name="keyValidityPeriod">The key validity period.</param>
        public AccountManagerLoader(string dllName, TimeSpan keyValidityPeriod)
        {
            this.dllName = dllName;
            this.keyValidityPeriod = keyValidityPeriod;
        }

        /// <summary>
        /// Creates the IAccountManager object.
        /// </summary>
        /// <returns>The IAccountManager object.</returns>
        public IAccountManager Create()
        {
            string assemblyPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, this.dllName);
            Assembly assembly = Assembly.LoadFrom(assemblyPath);

            foreach (Type type in assembly.GetTypes())
            {
                if (typeof(IAccountManagerFactory).IsAssignableFrom(type))
                {
                    object target = Activator.CreateInstance(type); 
                    object ret = type.GetMethod("Create").Invoke(target, new object[] { this.keyValidityPeriod });
                    return ret as IAccountManager;
                }
            }

            throw new MissingMemberException("IAccountManagerFactory is missing.");
        }
    }
}
