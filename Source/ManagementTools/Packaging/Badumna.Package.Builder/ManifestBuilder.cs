using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.IO.Compression;
using System.Runtime.Serialization;
using System.Security.Cryptography;

using ICSharpCode.SharpZipLib.Zip;
using Badumna.Package;

namespace Badumna.Package.Builder
{

    public class ManifestBuilder : Manifest
    {
        public delegate void EntryDelegate(String relativeFilename, String hashKey);

        public enum UpdateTypeValues
        {
            Complete,
            Partial
        }

        #region Progress fields

        private String mCurrentTask = "Collecting data";
        public String CurrentTask { get { return this.mCurrentTask; } }

        private int mProgress;
        public int Progress { get { return this.mProgress; } }

        private const int DefaultStages = 8;

        private int mStages = ManifestBuilder.DefaultStages;
        public int ProgressStages { get { return this.mStages; } }

        public event EventHandler ProgressEvent;

        #endregion

        [NonSerialized]
        private String mBaseDirectory = String.Empty;
        public String BaseDirectory
        {
            get { return this.mBaseDirectory; }
        }

        [NonSerialized]
        private String mPublishDirectory = String.Empty;
        public String PublishDirectory
        {
            get { return this.mPublishDirectory; }
            set
            {
                this.mPublishDirectory = value;

                String speacialPublishDirectoryKey = this.GetSpecialKey("PublishDirectory");
                if (!this.FileList.Contains(speacialPublishDirectoryKey))
                {
                    this.FileList.Add(speacialPublishDirectoryKey, this.mPublishDirectory);
                }
                else
                {
                    this.mFileList[speacialPublishDirectoryKey] = this.mPublishDirectory;
                }
            }
        }

        private string GetSpecialKey(String key)
        {
            return '"' + key + '"';
        }

        private bool IsSpecialKey(String key)
        {
            return key.Length > 1 && key[0] == '"' && key[key.Length - 1] == '"';
        }

        public void SetBaseDirectory(String directory)
        {
            this.mBaseDirectory = directory;
        }

        public bool AddFile(String fullPathFileName)
        {
            if (!Path.IsPathRooted(fullPathFileName))
            {
                throw new ArgumentException("Files name must be a full path");
            }

            if (File.Exists(fullPathFileName))
            {
                String relativeFileName = fullPathFileName.Replace(this.BaseDirectory, "");
                relativeFileName = relativeFileName.TrimStart(Path.DirectorySeparatorChar);

                if (!this.mFileList.Contains(fullPathFileName) && !this.mFileList.Contains(relativeFileName))
                {
                    this.mFileList.Add(fullPathFileName, "");
                    this.mStages += 2;

                    return true;
                }
            }

            return false;
        }

        public void RemoveFile(String fileName)
        {
            if (this.mFileList.Contains(fileName))
            {
                this.mFileList.Remove(fileName);
                this.mStages -= 2;
            }
        }

        public void SetExecutable(String executable)
        {
            this.AddFile(executable);

            if (File.Exists(executable))
            {
                this.mExecutable = executable;
            }
        }

        public void SetVersion(String versionString)
        {
            String[] split = versionString.Split(new char[] { '.' });

            if (split.Length != 4)
            {
                throw new ArgumentException("Invalid version string. Must have format #.#.#.#");
            }

            try
            {
                this.mMajor = int.Parse(split[0]);
                this.mMinor = int.Parse(split[1]);
                this.mBuild = int.Parse(split[2]);
                this.mRevision = int.Parse(split[3]);
            }
            catch (FormatException)
            {
                throw new ArgumentException("Version must contain only integers");
            }
        }

        public void SetProtocolVersion(int protocolVersion)
        {
            this.mProtocolVersion = protocolVersion;
        }

        public void SetSourceUri(Uri packageUri)
        {
            this.mPackageBaseUri = packageUri.AbsoluteUri;
        }

        public void SetUpdateType(UpdateTypeValues type)
        {
            this.mUpdateType = type.ToString();
        }

        public bool CompareProtocolManifest(String filename)
        {
            if (!File.Exists(filename))
            {
                throw new BuildException(String.Format("Failed to find protocol manifest {0}", filename));
            }

            String protocolHash = this.GetHashOfFile(filename);

            if (this.ProtocolHash.Length > 0 && this.ProtocolHash != protocolHash)
            {
                this.mProtocolVersion++;
                this.mProtocolHash = protocolHash;
                return true;
            }

            this.mProtocolHash = protocolHash;
            return false;
        }

        public bool IsFileChanged(String fullPath)
        {
            if (this.mFileList.ContainsKey(fullPath))
            {
                // The file has not been converted to relative name so it must be new.
                return true;
            }
            String relativeFileName = fullPath.Remove(0, this.BaseDirectory.Length);
            relativeFileName = relativeFileName.TrimStart(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);

            if (!File.Exists(relativeFileName))
            {
                return false;
            }

            if (this.mFileList.ContainsKey(relativeFileName))
            {
                return this.mFileList[relativeFileName] as String != this.GetHashOfFile(relativeFileName);
            }

            return true;
        }

        public void Reset()
        {
            this.Copy(new Manifest());
            this.mBaseDirectory = String.Empty;
            this.SetFilename(String.Empty);
            this.mStages = ManifestBuilder.DefaultStages;
        }

        private Manifest BuildManifest(bool isComplete)
        {
            SortedList changedFilesList = new SortedList();

            this.NextTask("Finding common directory");
            String commonDirectory = this.BaseDirectory;

            if (null == commonDirectory)
            {
                throw new BuildException("No common directory found. Have you Added files?");
            }

            this.NextTask("Setting executable field");
            if (null == this.mExecutable)
            {
                throw new BuildException("Please specify a valid executable.");
            }

            // Make the executable relative
            if (this.mExecutable.Contains(commonDirectory))
            {
                this.mExecutable = this.mExecutable.Remove(0, commonDirectory.Length);
                this.mExecutable = this.mExecutable.TrimStart(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
            }

            try
            {
                Directory.SetCurrentDirectory(commonDirectory);
            }
            catch (FileNotFoundException)
            {
                throw new BuildException(String.Format("Common directory {0} was not found", commonDirectory));
            }
            catch
            {
                throw new BuildException(String.Format("Unable to access directory {0}", commonDirectory));
            }

            this.NextTask("Adding files");

            DictionaryEntry[] fileList = new DictionaryEntry[this.mFileList.Count];

            this.mFileList.CopyTo(fileList, 0);
            this.mFileList.Clear();

            // Add the files to the manifest
            String uniqueManifestKey = this.Version;
            foreach (DictionaryEntry item in fileList)
            {
                String relativeFileName = String.Empty;
                String absoluteFileName = item.Key as String;

                // Remove the directory base if present
                if (absoluteFileName.Contains(commonDirectory))
                {
                    relativeFileName = absoluteFileName.Remove(0, commonDirectory.Length);
                    relativeFileName = relativeFileName.TrimStart(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                }
                else
                {
                    relativeFileName = item.Key as String;
                }

                if (File.Exists(relativeFileName))
                {
                    this.NextTask(String.Format("Adding {0}", relativeFileName));

                    String fileHash = this.GetHashOfFile(relativeFileName);

                    if (isComplete)
                    {
                        uniqueManifestKey += fileHash;
                        changedFilesList.Add(relativeFileName, fileHash);
                        this.mFileList.Add(relativeFileName, fileHash);
                    }
                    else
                    {
                        if (fileHash != (String)item.Value)
                        {
                            uniqueManifestKey += fileHash;
                            changedFilesList.Add(relativeFileName, fileHash);
                        }

                        this.mFileList.Add(item.Key as String, item.Value as String);
                    }
                }
            }

            if (!isComplete && changedFilesList.Count > 0)
            {
                this.mRevision++;
            }

            this.NextTask("Adding other data");
            this.SetPackageKey(this.GetHashOfString(uniqueManifestKey));

            Manifest manifest = new Manifest();

            manifest.Copy(this);

            manifest.FileList.Clear();
            foreach (DictionaryEntry item in changedFilesList)
            {
                manifest.FileList.Add(item.Key, item.Value);
            }

            this.mStages -= (this.mFileList.Count - changedFilesList.Count) * 2;
            return manifest;
        }

        public void BuildPackage()
        {
            bool isComplete = this.UpdateType == UpdateTypeValues.Complete.ToString();

            if (isComplete)
            {
                this.mRevision = 0;
                this.mBuild++;
            }

            this.mProgress = 0;
            this.mStages = ManifestBuilder.DefaultStages + (2 * this.mFileList.Count);
            Manifest manifest = this.BuildManifest(isComplete);

            if (isComplete)
            {
                this.mLastCompletePackageKey = manifest.PackageKey;
                manifest.SetLastCompletePackageKey(manifest.PackageKey);
            }

            if (manifest.FileList.Count == 0)
            {
                this.mStages = this.mProgress + 1;
                this.NextTask("No files require updating");
                return;
            }

            this.NextTask("Creating output path");
            // Remove the package file if it exists already
            if (File.Exists(manifest.PackageFilename))
            {
                File.Delete(manifest.PackageFilename);
            }

            // Create the output path if required
            if (this.mPublishDirectory.Length == 0)
            {
                this.mPublishDirectory = Path.Combine(this.BaseDirectory, "publish");
            }
            if (!Directory.Exists(this.mPublishDirectory))
            {
                try
                {
                    Directory.CreateDirectory(this.mPublishDirectory);
                }
                catch (Exception e)
                {
                    throw new BuildException(String.Format("Unable to create directory {0} : {1}", this.mPublishDirectory, e.Message));
                }
            }

            // Save the manifest file.
            this.NextTask("Writing the manifest file");
            manifest.Save(Manifest.DefaultFileName);
            manifest.SetInternalEntry(this.GetHashOfFile(manifest.FileName));

            // Make the package.
            String temporaryPackagePath = Path.Combine(this.mPublishDirectory, "tmp-eggsac-package.zip");
            long packageSize;
            packageSize = this.Compress(manifest.FileList, temporaryPackagePath);

            // Remove manifest file
            if (File.Exists(manifest.FileName))
            {
                File.Delete(manifest.FileName);
            }

            String finalPackageName = Path.Combine(this.mPublishDirectory, manifest.PackageFilename);
            if (File.Exists(finalPackageName))
            {
                File.Delete(finalPackageName);
            }

            this.NextTask("Copying final package");
            try
            {
                File.Move(temporaryPackagePath, finalPackageName);

                // Try to copy a manifest xslt file to the output path
                try
                {
                    String xsltFile = Path.Combine(Path.GetDirectoryName(this.FileName), Path.GetFileNameWithoutExtension(this.FileName) + ".xslt");

                    if (File.Exists(xsltFile))
                    {
                        File.Copy(xsltFile, Path.Combine(this.mPublishDirectory, "Manifest.xslt"));
                    }
                    else
                    {
                        File.Copy(Path.Combine(Path.GetDirectoryName(this.FileName), "Manifest.xslt"), Path.Combine(this.mPublishDirectory, "Manifest.xslt"));
                    }
                }
                catch { }

                this.NextTask("Cleanup");
                manifest.SetPackageSize(packageSize);
                manifest.SetPackageCheckSum(this.GetHashOfFile(finalPackageName));
                manifest.Save(Path.Combine(this.mPublishDirectory, manifest.FileName));

                if (isComplete) // Set last complete package info. It will be saved by Save() for inclusion in the next update build
                {
                    this.mLatestCompletePackageSizeKB = packageSize;
                    this.SetLastCompletePackageChecksum(manifest.PackageChecksum);
                }

                Updater.TryCleanDirectory(this.mPublishDirectory);
            }
            catch (FileNotFoundException)
            {
                throw new BuildException(String.Format("Failed to find built package"));
            }
            catch
            {
                throw new BuildException(String.Format("Failed to move built package"));
            }

            this.NextTask("Complete");
            return;
        }

        private String FindCommonDirectory()
        {
            String commonDirectory = null;

            foreach (DictionaryEntry entry in this.mFileList)
            {
                String directory = Path.GetDirectoryName(entry.Key as String);
                if (null == commonDirectory || commonDirectory.Contains(directory))
                {
                    commonDirectory = directory;
                }
            }

            return commonDirectory;
        }

        private void NextTask(String taskDescription)
        {
            this.mCurrentTask = taskDescription;
            this.mProgress++;

            if (null != this.ProgressEvent)
            {
                this.ProgressEvent(this, new EventArgs());
            }
        }

        public new void Load(String filename)
        {
            if (!Path.IsPathRooted(filename))
            {
                filename = Path.GetFullPath(filename);
            }

            Manifest manifest = Manifest.Load(filename);

            if (null != manifest)
            {
                this.Copy(manifest);
                String specialBaseDirectoryKey = this.GetSpecialKey("BaseDirectory");
                if (this.FileList.Contains(specialBaseDirectoryKey))
                {
                    this.mBaseDirectory = this.FileList[specialBaseDirectoryKey] as String;
                    this.FileList.Remove(specialBaseDirectoryKey);
                }

                String specialPublishDirectoryKey = this.GetSpecialKey("PublishDirectory");
                if (this.FileList.Contains(specialPublishDirectoryKey))
                {
                    this.mPublishDirectory = this.FileList[specialPublishDirectoryKey] as String;
                    this.FileList.Remove(specialPublishDirectoryKey);
                }

                this.mStages = ManifestBuilder.DefaultStages + (this.FileList.Count * 2);
            }
        }

        public void ForeachFile(EntryDelegate function)
        {
            foreach (DictionaryEntry entry in this.mFileList)
            {
                function(entry.Key as String, entry.Value as String);
            }
        }

        private Manifest ProduceFixedManifest()
        {
            String commonDirectory = this.BaseDirectory;
            String currentDirectory = Directory.GetCurrentDirectory();

            if (null == commonDirectory)
            {
                throw new BuildException("No common directory found. Have you Added files?");
            }

            if (null == this.mExecutable)
            {
                throw new BuildException("Please specify a valid executable.");
            }

            // Make the executable relative
            if (this.mExecutable.Contains(commonDirectory))
            {
                this.mExecutable = this.mExecutable.Remove(0, commonDirectory.Length);
                this.mExecutable = this.mExecutable.TrimStart(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
            }

            try
            {
                //// Note: this will cause the problem, as it never set back the original directory.
                //// Fix : Set the directory back to the current directory afterward.
                Directory.SetCurrentDirectory(commonDirectory);
            }
            catch (FileNotFoundException)
            {
                throw new BuildException(String.Format("Common directory {0} was not found", commonDirectory));
            }
            catch
            {
                throw new BuildException(String.Format("Unable to access directory {0}", commonDirectory));
            }

            DictionaryEntry[] fileList = new DictionaryEntry[this.mFileList.Count];

            this.mFileList.CopyTo(fileList, 0);
            this.mFileList.Clear();

            this.mFileList.Add(this.GetSpecialKey("BaseDirectory"), commonDirectory);
            this.mFileList.Add(this.GetSpecialKey("PublishDirectory"), this.mPublishDirectory);

            // Add the files to the manifest
            String uniqueManifestKey = this.Version;
            foreach (DictionaryEntry item in fileList)
            {
                String relativeFileName = String.Empty;
                String absoluteFileName = item.Key as String;

                // Remove the directory base if present
                if (absoluteFileName.Contains(commonDirectory))
                {
                    relativeFileName = absoluteFileName.Remove(0, commonDirectory.Length);
                    relativeFileName = relativeFileName.TrimStart(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);

                    if (File.Exists(relativeFileName))
                    {
                        String fileHash = this.GetHashOfFile(relativeFileName);
                        this.mFileList.Add(relativeFileName, fileHash);
                    }
                }
                else
                {
                    if (!this.mFileList.ContainsKey(item.Key as String))
                    {
                        this.mFileList.Add(item.Key as String, item.Value as String);
                    }
                }
            }           

            Manifest manifest = new Manifest();

            manifest.Copy(this);
            
            List<String> filesNotIncluded = new List<string>();
            foreach (DictionaryEntry file in this.mFileList)
            {
                if (this.IsSpecialKey((string)file.Key))
                {
                    filesNotIncluded.Add((string)file.Key);
                }
            }

            foreach (String excludedFile in filesNotIncluded)
            {
                this.mFileList.Remove(excludedFile);
            }

            //// Set back the current directory, to where it was
            Directory.SetCurrentDirectory(currentDirectory);

            return manifest;
        }


        public override void Save(string fileName)
        {
            Manifest manifest = this.ProduceFixedManifest();

            manifest.Save(fileName);
            this.Load(fileName);
        }

        internal String GetHashOfFile(String fileName)
        {
            SHA1 sha = new SHA1Managed();
            byte[] result;

            sha.Initialize();

            result = sha.ComputeHash(File.ReadAllBytes(fileName));

            int length = result.Length * 2;
            StringBuilder builder = new StringBuilder(length);

            foreach (byte b in result)
            {
                builder.AppendFormat("{0:X2}", b);
            }

            return builder.ToString(0, length);
        }

        internal String GetHashOfString(String data)
        {
            SHA1 sha = new SHA1Managed();
            byte[] result;

            sha.Initialize();

            MemoryStream stream = new MemoryStream(10000);
            BinaryWriter writer = new BinaryWriter(stream);

            writer.Write(data);

            stream.Flush();
            stream.Position = 0;
            result = sha.ComputeHash(stream);
            stream.Close();

            int length = result.Length * 2;
            StringBuilder builder = new StringBuilder(length);

            foreach (byte b in result)
            {
                builder.AppendFormat("{0:X2}", b);
            }

            return builder.ToString(0, length);
        }

        private long Compress(SortedList fileList, String zipFileName)
        {
            long fileSize = 0;

            try
            {
                // 'using' statements gaurantee the stream is closed properly which is a big source
                // of problems otherwise.  Its exception safe as well which is great.
                using (ZipOutputStream stream = new ZipOutputStream(File.Create(zipFileName)))
                {
                    stream.SetLevel(9); // 0 - store only to 9 - means best compression                    

                    byte[] buffer = new byte[4096];

                    foreach (DictionaryEntry listEntry in fileList)
                    {
                        String file = listEntry.Key as String;

                        if (File.Exists(file))
                        {
                            // Using GetFileName makes the result compatible with XP
                            // as the resulting path is not absolute.
                            ZipEntry entry = new ZipEntry(file);

                            // Setup the entry data as required.

                            // Crc and size are handled by the library for seakable streams
                            // so no need to do them here.

                            // Could also use the last write time or similar for the file.
                            entry.DateTime = DateTime.Now;
                            stream.PutNextEntry(entry);

                            this.NextTask(String.Format("Compressing {0}", file));

                            using (FileStream fs = File.OpenRead(file))
                            {
                                // Using a fixed size buffer here makes no noticeable difference for output
                                // but keeps a lid on memory usage.
                                int sourceBytes;
                                do
                                {
                                    sourceBytes = fs.Read(buffer, 0, buffer.Length);
                                    stream.Write(buffer, 0, sourceBytes);
                                } while (sourceBytes > 0);

                                fs.Close();
                            }
                        }
                    }

                    fileSize = stream.Length / 1000;
                    stream.Flush();

                    // Finish/Close arent needed strictly as the using statement does this automatically

                    // Finish is important to ensure trailing information for a Zip file is appended.  Without this
                    // the created file would be invalid.
                    stream.Finish();

                    // Close is important to wrap things up and unlock the file.
                    stream.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception during processing {0}", ex);

                // No need to rethrow the exception as for our purposes its handled.
            }

            return fileSize;
        }
    }


    [Serializable]
    public class BuildException : ApplicationException
    {
        public BuildException(String message)
            : base(message)
        { }

        public BuildException(String message, Exception innerException)
            : base(message, innerException)
        { }

        public BuildException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

    }

}

