<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html" indent="yes"/>

  <xsl:template match="/">

    <html>
      <body >
        <div align="center">

          <xsl:choose>

            <xsl:when test="Manifest/ShowDescription &lt; 1">
              <div class="description" style="margin-left: auto; margin-right: auto; width: 300px;">
                <p>
                  Preliminary package download required (<xsl:value-of select="Manifest/LatestCompletePackageSizeKB"/>KB)
                </p>
              </div>
            </xsl:when>

            <xsl:otherwise>

              <div class="details" style="font-size: small; border-spacing: 2px;">
                <table border="0" style="border: 1px groove #C0C0C0">
                  <tr>
                    <td>Application version</td>
                    <td>
                      <xsl:value-of select="Manifest/Major"/>.<xsl:value-of select="Manifest/Minor"/>.<xsl:value-of select="Manifest/Build"/>.<xsl:value-of select="Manifest/Revision"/>
                    </td>
                  </tr>
                  <tr>
                    <td>Protocol version</td>
                    <td>
                      0.<xsl:value-of select="Manifest/ProtocolVersion"/>
                    </td>
                  </tr>
                  <tr>
                    <td>Date</td>
                    <td>
                      <xsl:value-of select="Manifest/Date"/>
                    </td>
                  </tr>
                  <tr>
                    <td>File size</td>
                    <td>
                      <xsl:value-of select="Manifest/PackageSizeKB"/> KB
                    </td>
                  </tr>
                </table>
              </div>

              <h4>
                <xsl:apply-templates select="Manifest/Description/Header"/>
              </h4>
              <div class="description" style="font-size: x-small; margin-left: 25px; margin-right: auto; width: 300px; text-align: left;">
                <xsl:apply-templates select="Manifest/Description/Comments"/>
                <ul>
                  <xsl:for-each select="Manifest/Description/Item">
                    <li>
                      <xsl:value-of select="."/>
                    </li>
                  </xsl:for-each>
                </ul>
              </div>

            </xsl:otherwise>
          </xsl:choose>
        </div>
      </body>
    </html>

  </xsl:template>

</xsl:stylesheet>