﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.Linq;
using System.IO;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;


namespace Badumna.Package.Builder
{
    /// <summary>
    /// Interaction logic for PackageControl.xaml
    /// </summary>
    public partial class PackageControl : UserControl
    {
        private DirectoryView mDirectoryView;
        private ManifestBuilder mManifestBuilder;
        private PackageData mPackage;
        public PackageData Package { get { return this.mPackage; } }

        private String mBasePackageDirectory = AppDomain.CurrentDomain.BaseDirectory;
        public String BasePackageDirectory
        {
            get { return this.mBasePackageDirectory; }
            set { this.mBasePackageDirectory = value; }
        }

        public PackageControl()
        {
            InitializeComponent();
        }

        public bool Load(string packageFile)
        {
            PackageData application = PackageData.Load(packageFile);

            if (application != null)
            {
                this.ApplicationNameText.Text = application.Name;
                this.ApplicationNameText.IsEnabled = false;
                this.LoadPackage(application);

                return true;
            }

            return false;
        }

        public void Reset()
        {
            this.mPackage = null;
            this.mManifestBuilder = null;
            if (this.mDirectoryView != null)
            {
                this.mDirectoryView.Visibility = Visibility.Hidden;
            }
            this.mDirectoryView = null;
            this.ApplicationNameText.Text = "Unknown";
            this.ApplicationNameText.IsEnabled = true;
            this.Details.IsEnabled = false;
        }

        public bool Save(String fileName)
        {
            if (this.mPackage != null && this.mDirectoryView != null)
            {
                this.mPackage.Files = this.mDirectoryView.GetContents();

                if (this.mManifestBuilder != null)
                {
                    this.SyncFilesToBuilder(false);
                    try
                    {
                        this.mManifestBuilder.Save(this.mPackage.ManifestFile);
                    }
                    catch (Exception ex)
                    {
                        return false;
                    }
                }
            }

            this.mPackage.Save(fileName);

            return true;
        }

        public void Delete()
        {
            if (this.mPackage != null)
            {
                if (File.Exists(this.mPackage.ManifestFile))
                {
                    File.Delete(this.mPackage.ManifestFile);
                }
            }

            if (this.mManifestBuilder != null)
            {
                string directory = this.mManifestBuilder.BaseDirectory;
                this.mManifestBuilder.Reset();
                this.mManifestBuilder = null;
                if (Directory.Exists(directory))
                {
                    Directory.Delete(directory, true);
                }
            }
        }


        private void ApplicationNameText_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                if (this.StartNewPackage(new PackageData(this.mBasePackageDirectory, this.ApplicationNameText.Text)))
                {
                    this.mPackage.ManifestFile = Path.GetFullPath(this.mManifestBuilder.FileName);
                    this.Details.IsEnabled = true;
                    this.ApplicationNameText.IsEnabled = false;

                    e.Handled = true;
                }
            }
        }

        private void ToggleBuildButtonIfNecessary()
        {
            if (this.mManifestBuilder != null)
            {
                if (!File.Exists(Path.Combine(this.mManifestBuilder.BaseDirectory, this.mManifestBuilder.Executable)))
                {
                    this.BuildButton.IsEnabled = false;
                    return;
                }

                this.BuildButton.IsEnabled = true;
                return;
            }

            this.BuildButton.IsEnabled = false;
            return;
        }

        private void BuildButton_Click(object sender, RoutedEventArgs e)
        {
            Window descriptionDialog = new Window();

            descriptionDialog.Width = 600;
            descriptionDialog.Height = 600;
            PackageDescriptionControl descriptionControl = new PackageDescriptionControl(descriptionDialog, this.mManifestBuilder);
            descriptionDialog.Content = descriptionControl;

            if (descriptionDialog.ShowDialog() == true)
            {
                this.mManifestBuilder.SetDescription(descriptionControl.Description);

                this.BuildPackage();
            }
        }

        private void LoadPackage(PackageData application)
        {
            this.mPackage = application;
            this.Details.IsEnabled = true;
            this.ApplicationNameText.IsEnabled = false;
            this.ApplicationNameText.Text = application.Name;
            this.mDirectoryView = new DirectoryView(this.mBasePackageDirectory, application.Name);
            this.DirectoryViewContent.Content = this.mDirectoryView;
            String path = application.Path;

            this.mManifestBuilder = new ManifestBuilder();
            this.mManifestBuilder.PublishDirectory = Path.GetFullPath(Path.Combine(this.mBasePackageDirectory, Path.Combine(application.Name, "publish")));
            this.mManifestBuilder.SetBaseDirectory(Path.Combine(Path.GetFullPath(this.mBasePackageDirectory), application.Name));
            this.mManifestBuilder.ProgressEvent += this.mManifestBuilder_ProgressEvent;
            try
            {
                this.mManifestBuilder.Load(application.ManifestFile);
            }
            catch (Exception)
            {
                this.mManifestBuilder.SetFilename(application.ManifestFile);
            }

            this.mManifestBuilder.SetApplicationName(application.Name);
            this.MajorText.Text = this.mManifestBuilder.Major.ToString();
            this.MinorText.Text = this.mManifestBuilder.Minor.ToString();
            this.BuildText.Text = this.mManifestBuilder.Build.ToString();
            this.RevisionText.Text = this.mManifestBuilder.Revision.ToString();
            this.ExecutableText.Text = this.mManifestBuilder.Executable;
            this.CompleteCheckBox.IsChecked = this.mManifestBuilder.UpdateType == "Complete";

            foreach (KeyValuePair<string, string> filePair in application.Files)
            {
                try
                {
                    this.mDirectoryView.AddFile(filePair.Key, filePair.Value);
                }
                catch (Exception e)
                {
                    ;
                }
            }

            this.AddCommonFiles(application.Name);

            this.ToggleBuildButtonIfNecessary();
        }

        private bool StartNewPackage(PackageData application)
        {
            String applicationName = application.Name;

            try
            {
                if (applicationName.Length > 0)
                {
                    String path = application.Path;

                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(path);
                    }

                    this.mDirectoryView = new DirectoryView(this.mBasePackageDirectory, applicationName);
                    this.DirectoryViewContent.Content = this.mDirectoryView;

                    this.AddCommonFiles(applicationName);

                    if (this.mManifestBuilder == null)
                    {
                        this.mManifestBuilder = new ManifestBuilder();

                        this.mManifestBuilder.SetApplicationName(applicationName);
                        this.mManifestBuilder.SetFilename(String.Format("{0}Manifest.xml", applicationName));
                        this.mManifestBuilder.SetBaseDirectory(Path.Combine(Path.GetFullPath(this.mBasePackageDirectory), applicationName));
                        this.mManifestBuilder.PublishDirectory = Path.GetFullPath(Path.Combine(this.mBasePackageDirectory, Path.Combine(applicationName, "publish")));
                        this.mManifestBuilder.ProgressEvent += this.mManifestBuilder_ProgressEvent;
                    }

                    this.MajorText.Text = this.mManifestBuilder.Major.ToString();
                    this.MinorText.Text = this.mManifestBuilder.Minor.ToString();
                    this.BuildText.Text = this.mManifestBuilder.Build.ToString();
                    this.RevisionText.Text = this.mManifestBuilder.Revision.ToString();
                    this.ExecutableText.Text = this.mManifestBuilder.Executable;
                    this.CompleteCheckBox.IsChecked = this.mManifestBuilder.UpdateType == "Complete";

                    // Add all the files in the default package directory to the new package directory view
                    string defaultPackageDirectory = Path.GetFullPath(Path.Combine(this.mBasePackageDirectory, "Default"));
                    if (Directory.Exists(defaultPackageDirectory))
                    {
                        foreach (string file in Directory.GetFiles(defaultPackageDirectory, "*", SearchOption.AllDirectories))
                        {
                            String relativePath = file.Replace(defaultPackageDirectory + Path.DirectorySeparatorChar, String.Empty);

                            this.mDirectoryView.AddFile(relativePath, Path.GetFullPath(file));
                        }
                    }
                }
            }
            catch (Exception e)
            {
                ;
            }

            this.ToggleBuildButtonIfNecessary();
            this.mPackage = application;
            return true;
        }

        private void SyncFilesToBuilder(bool copy)
        {
            if (this.mManifestBuilder != null && this.mDirectoryView != null)
            {
                string packageDirectory = Path.GetFullPath(this.mBasePackageDirectory);

                List<String> packagedFiles = new List<string>();

                foreach (string file in this.mManifestBuilder.FileList.Keys)
                {
                    if (File.Exists(file))
                    {
                        packagedFiles.Add(Path.Combine(this.mManifestBuilder.BaseDirectory, file));
                    }
                }

                //    this.mManifestBuilder.FileList.Clear();

                int i = 0;
                foreach (KeyValuePair<string, string> pair in this.mPackage.Files)
                {
                    string destinationPath = Path.GetFullPath(Path.Combine(packageDirectory, pair.Key));

                    if (copy)
                    {
                        string directory = Path.GetDirectoryName(destinationPath);

                        if (File.Exists(pair.Value))
                        {
                            if (File.Exists(directory))
                            {
                                File.Delete(directory);
                            }

                            if (!Directory.Exists(directory))
                            {
                                Directory.CreateDirectory(directory);
                            }

                            File.Copy(pair.Value, destinationPath, true);
                        }

                        this.Dispatcher.Invoke(new UpdateProgressDelegate(this.UpdateProgress),
                           i++, this.mPackage.Files.Count, String.Format("Copying {0}", pair.Key), false);
                    }

                    if (!packagedFiles.Contains(destinationPath))
                    {
                        this.mManifestBuilder.AddFile(destinationPath);
                    }
                    else
                    {
                        packagedFiles.Remove(destinationPath);
                    }
                }

                foreach (String removedFile in packagedFiles)
                {
                    this.mManifestBuilder.RemoveFile(removedFile);
                }
            }
        }

        private void AddCommonFiles(string applicationName)
        {
            if (this.mDirectoryView != null)
            {
                String commonDirectory = AppDomain.CurrentDomain.BaseDirectory;
                String originalProcessMonitorPathName = Path.Combine(commonDirectory, "ProcessMonitor.exe");
                String newProcessMonitorPathName = Path.Combine(commonDirectory, String.Format("{0}PM.exe", applicationName));

                if (File.Exists(originalProcessMonitorPathName))
                {
                    File.Copy(originalProcessMonitorPathName, newProcessMonitorPathName, true);
                    this.mDirectoryView.AddFile(Path.GetFileName(newProcessMonitorPathName), newProcessMonitorPathName);
                }

                this.mDirectoryView.AddFile("Badumna.Package.dll", Path.Combine(commonDirectory, "Badumna.Package.dll"));
                this.mDirectoryView.AddFile("ICSharpCode.SharpZipLib.dll", Path.Combine(commonDirectory, "ICSharpCode.SharpZipLib.dll"));
            }
        }

        private void BuildPackage()
        {
            if (this.mManifestBuilder != null && this.mDirectoryView != null)
            {
                this.ProgressBar.Maximum = this.mManifestBuilder.ProgressStages;
                this.ProgressBar.Value = 0;
                this.mPackage.Files = this.mDirectoryView.GetContents();

                System.Threading.Thread thread = new System.Threading.Thread(this.BuildThreadStart);
                thread.Start();
            }
        }

        private void BuildThreadStart()
        {
            if (this.mManifestBuilder != null)
            {
                this.Dispatcher.Invoke(new EnableDetailsDelegate(this.EnableDetails), false);
                this.SyncFilesToBuilder(true);

                try
                {
                    this.mManifestBuilder.BuildPackage();
                    this.mManifestBuilder.Save(this.mManifestBuilder.FileName);
                }
                catch (BuildException e)
                {
                    this.Dispatcher.Invoke(new UpdateProgressDelegate(this.UpdateProgress), 0, this.mManifestBuilder.ProgressStages, e.Message, true);
                }

                this.Dispatcher.Invoke(new EnableDetailsDelegate(this.EnableDetails), true);
            }
        }

        private delegate void UpdateProgressDelegate(int stage, int totalStages, string description, bool isError);
        private void UpdateProgress(int stage, int totalStages, string description, bool isError)
        {
            this.ProgressText.Text = description;
            this.ProgressBar.Value = stage;
            this.ProgressBar.Maximum = totalStages;

            if (isError)
            {
                this.ProgressText.Foreground = Brushes.Red;
            }
        }

        private delegate void EnableDetailsDelegate(bool enable);
        private void EnableDetails(bool enable)
        {
            this.Details.IsEnabled = enable;

            if (enable)
            {
                this.MajorText.Text = this.mManifestBuilder.Major.ToString();
                this.MinorText.Text = this.mManifestBuilder.Minor.ToString();
                this.BuildText.Text = this.mManifestBuilder.Build.ToString();
                this.RevisionText.Text = this.mManifestBuilder.Revision.ToString();
                this.ExecutableText.Text = this.mManifestBuilder.Executable;
                this.CompleteCheckBox.IsChecked = this.mManifestBuilder.UpdateType == "Complete";
            }
        }

        void mManifestBuilder_ProgressEvent(object sender, EventArgs e)
        {
            if (this.mManifestBuilder != null)
            {
                this.Dispatcher.Invoke(new UpdateProgressDelegate(this.UpdateProgress),
                    this.mManifestBuilder.Progress, this.mManifestBuilder.ProgressStages, this.mManifestBuilder.CurrentTask, false);
            }
        }

        private void ExecutableButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.mManifestBuilder != null)
            {
                System.Windows.Forms.OpenFileDialog openFileDialog = new System.Windows.Forms.OpenFileDialog();

                openFileDialog.Filter = "Executables (*.exe)|*.exe";
                openFileDialog.Multiselect = false;


                openFileDialog.InitialDirectory = this.mManifestBuilder.BaseDirectory;

                if (openFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    string fileName = Path.GetFileName(openFileDialog.FileName);
                    string sourcePath = Path.GetFullPath(openFileDialog.FileName);
                    string relativeDestinationPath = Path.Combine(this.mPackage.Name, fileName);
                    string fullDestinationPath = Path.Combine(this.mPackage.Path, fileName);

                    if (sourcePath.Contains(this.mManifestBuilder.BaseDirectory))
                    {
                        // The source path is in the package directory already.

                        if (System.Windows.MessageBox.Show("Do you really intend to use the executable in the package already?",
                            "Relative executable file warning", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                        {
                            fullDestinationPath = sourcePath;
                        }
                        else
                        {
                            return;
                        }
                    }
                    else
                    {
                        this.mDirectoryView.AddFile(relativeDestinationPath, sourcePath);

                        if (!Directory.Exists(Path.GetDirectoryName(fullDestinationPath)))
                        {
                            Directory.CreateDirectory(Path.GetDirectoryName(fullDestinationPath));
                        }

                        File.Copy(openFileDialog.FileName, fullDestinationPath, true);
                    }

                    this.ExecutableText.Text = fileName;
                    this.mManifestBuilder.SetExecutable(fullDestinationPath);
                }

                this.ToggleBuildButtonIfNecessary();
            }
        }

        private void CompleteCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            if (this.mManifestBuilder != null)
            {
                this.mManifestBuilder.SetUpdateType(ManifestBuilder.UpdateTypeValues.Complete);
            }
        }

        private void CompleteCheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            if (this.mManifestBuilder != null)
            {
                this.mManifestBuilder.SetUpdateType(ManifestBuilder.UpdateTypeValues.Partial);
            }
        }
    }

}
