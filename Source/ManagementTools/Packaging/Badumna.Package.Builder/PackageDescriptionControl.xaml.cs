﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Xml;
using System.Xml.Xsl;
using System.Xml.XPath;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Badumna.Package;
using Badumna.Package.Builder;

namespace Badumna.Package.Builder
{
    /// <summary>
    /// Interaction logic for PackageDescriptionControl.xaml
    /// </summary>
    public partial class PackageDescriptionControl : UserControl
    {
        private XslCompiledTransform mXslTransform = new XslCompiledTransform();
        private Manifest mManifest;
        private Window mWindowParent;

        private string mDescription = String.Empty;
        public string Description { get { return this.mDescription; } }

        internal PackageDescriptionControl(Window parent, ManifestBuilder manifest)
        {
            InitializeComponent();

            this.mXslTransform.Load(new XmlTextReader(new System.IO.StringReader(Badumna.Package.Builder.Resources.Default)));
            this.mManifest = new Manifest();
            this.mManifest.Copy(manifest);
            this.mWindowParent = parent;
            this.HeaderText.Text = manifest.ApplicationName;
            this.CommentText.Loaded += new RoutedEventHandler(CommentText_Loaded);

            this.UpdateManifest();
        }

        void CommentText_Loaded(object sender, RoutedEventArgs e)
        {
            this.CommentText.Focus();
        }

        private void ItemText_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return && this.ItemText.Text.Length > 0)
            {
                this.ItemList.Items.Add(this.ItemText.Text);
                this.UpdateView();
            }
        }

        private void RemoveMenuItem_Click(object sender, RoutedEventArgs e)
        {
            if (this.ItemList.SelectedIndex >= 0)
            {
                this.ItemList.Items.RemoveAt(this.ItemList.SelectedIndex);
                this.UpdateView();
            }
        }

        private void Add_Click(object sender, RoutedEventArgs e)
        {
            if (this.ItemText.Text.Length > 0)
            {
                this.ItemList.Items.Add(this.ItemText.Text);
                this.UpdateView();
            }
        }

        private void HeaderText_TextChanged(object sender, TextChangedEventArgs e)
        {
            this.UpdateView();
        }

        private void CommentText_TextChanged(object sender, TextChangedEventArgs e)
        {
            this.UpdateView();
        }

        private void UpdateManifest()
        {
            XmlDocument descriptionDocument = new XmlDocument();

            XmlNode descriptionNode = descriptionDocument.CreateElement("Description");
            XmlNode headerNode = descriptionNode.AppendChild(descriptionDocument.CreateElement("Header"));
            XmlNode commentNode = descriptionNode.AppendChild(descriptionDocument.CreateElement("Comments"));

            headerNode.InnerXml = System.Security.SecurityElement.Escape(this.HeaderText.Text);
            commentNode.InnerXml = System.Security.SecurityElement.Escape(this.CommentText.Text);

            foreach (String itemText in this.ItemList.Items)
            {
                XmlNode itemNode = descriptionNode.AppendChild(descriptionDocument.CreateElement("Item"));
                itemNode.InnerXml = System.Security.SecurityElement.Escape(itemText);
            }

            this.mManifest.SetDescription(descriptionNode.InnerXml);
            this.mDescription = descriptionNode.InnerXml;
        }

        private void UpdateView()
        {
            this.UpdateManifest();

            try
            {
                MemoryStream inputStream = new MemoryStream();
                MemoryStream outputStream = new MemoryStream();

                this.mManifest.Save(inputStream);
                inputStream.Position = 0;

                XmlDocument document = new XmlDocument();

                document.Load(inputStream);
                this.mXslTransform.Transform(document, null, outputStream);
                outputStream.Flush();
                outputStream.Position = 0;

                String htmlString = new StreamReader(outputStream).ReadToEnd();

                inputStream.Position = 0;
                String xmlString = new StreamReader(inputStream).ReadToEnd();

                this.Preview.NavigateToString(htmlString);
                    
                inputStream.Close();
                outputStream.Close();
            }
            catch { }            
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            this.mWindowParent.DialogResult = false;
            this.mWindowParent.Close();
        }

        private void Apply_Click(object sender, RoutedEventArgs e)
        {
            this.mWindowParent.DialogResult = true;
            this.mWindowParent.Close();
        }
    }
}
