﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.IO;

namespace Badumna.Package.Builder
{
    [DataContract(Name = "Application")]
    public class PackageData
    {
        [DataMember(Name = "Name")]
        private string mName = string.Empty;
        public string Name { get { return this.mName; } }

        [DataMember(Name = "ManifestFile")]
        private string mManifestFile = string.Empty;
        public string ManifestFile
        {
            get { return this.mManifestFile; }
            set { this.mManifestFile = value; }
        }

        [DataMember(Name = "Files")]
        private Dictionary<string, string> mFiles = new Dictionary<string, string>();

        [DataMember(Name = "Path")]
        private string mPath;
        public string Path { get { return this.mPath; } }

        public Dictionary<string, string> Files
        {
            get { return this.mFiles; }
            set { this.mFiles = value; }
        }

        public PackageData(String packagePath, String name)
        {
            this.mName = name;
            this.mPath = System.IO.Path.Combine(packagePath, name);
        }

        public void Save(String fileName)
        {
            using (FileStream fileStream = new FileStream(fileName, FileMode.Create))
            {
                DataContractSerializer serializer = new DataContractSerializer(typeof(PackageData));

                serializer.WriteObject(fileStream, this);
            }
        }

        public static PackageData Load(String fileName)
        {
            if (File.Exists(fileName))
            {
                using (FileStream fileStream = new FileStream(fileName, FileMode.Open))
                {
                    DataContractSerializer deserializer = new DataContractSerializer(typeof(PackageData));

                    return deserializer.ReadObject(fileStream) as PackageData;
                }
            }

            return null;
        }
    }
}
