﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.IO;

namespace Badumna.Package.Builder
{
    /// <summary>
    /// Interaction logic for DirectoryView.xaml
    /// </summary>
    public partial class DirectoryView : UserControl
    {
        private string mRootPath = string.Empty;

        public DirectoryView(String rootPath, string baseDirectory)
        {
            InitializeComponent();

            this.mRootPath = rootPath;
            this.AddDirectoryItem(this.TreeView.Items, baseDirectory, string.Empty);
            this.Dispatcher.Hooks.DispatcherInactive += new EventHandler(Hooks_DispatcherInactive);
        }

        void Hooks_DispatcherInactive(object sender, EventArgs e)
        {
            if (this.TreeView.Items.Count > 0)
            {
                DirectoryViewItem root = this.TreeView.Items[0] as DirectoryViewItem;

                foreach (DirectoryViewItem item in root.Items)
                {
                    this.CheckPath(Path.Combine(this.mRootPath, root.Path), item);
                }
            }
        }

        private void CheckPath(string parentPath, DirectoryViewItem item)
        {
            if (item.SourcePath != null)
            {
                if (!File.Exists(item.SourcePath) && !Directory.Exists(Path.Combine(parentPath, item.Path)))
                {
                    item.ChangeColor(Brushes.Red);
                }
                else 
                {
                    item.ChangeColor(Brushes.Black);
                }
            }
        }


        private DirectoryViewItem AddDirectoryItem(ItemCollection collection, String relativePath, String sourcePath)
        {
            DirectoryViewItem item = new DirectoryViewItem(relativePath, sourcePath);

            item.MouseRightButtonDown += this.Item_MouseRightButtonDown;
            collection.Add(item);

            return item;
        }

        public void AddFile(String relativePath, String fullPath)
        {
            TreeViewItem rootPathItem = this.TreeView.Items[0] as TreeViewItem;

            if (rootPathItem == null)
            {
                return;
            }

            ItemCollection collection = this.TreeView.Items;
            bool traverse = true;

            while (traverse && collection != null && collection.Count > 0 && relativePath.Length > 0)
            {
                traverse = false;
                foreach (DirectoryViewItem pathItem in collection)
                {
                    string parentPath = pathItem.Path;

                    if (parentPath == relativePath)
                    {
                        return;
                    }

                    if (parentPath != null && relativePath.StartsWith(parentPath + Path.DirectorySeparatorChar))
                    {
                        rootPathItem = pathItem;
                        relativePath = relativePath.Remove(0, parentPath.Length + 1);
                        collection = pathItem.Items;
                        traverse = true;
                        break;
                    }
                }
            }

            string[] directories = relativePath.Split(Path.DirectorySeparatorChar);

            foreach (string path in directories)
            {
                if (path.Length > 0)
                {
                    if (Path.GetFileName(path) == path)
                    {
                        rootPathItem = this.AddDirectoryItem(rootPathItem.Items, path, fullPath);
                    }
                    else
                    {
                        rootPathItem = this.AddDirectoryItem(rootPathItem.Items, path, string.Empty); // is a directory
                    }
                }
            }
        }

        private void AddFile(TreeViewItem parentItem, String fullPath)
        {
            TreeViewItem rootPathItem = parentItem;
            string fileName = Path.GetFileName(fullPath);

            if (rootPathItem == null)
            {
                return;
            }

            foreach (TreeViewItem childItem in rootPathItem.Items)
            {
                if (childItem.Header as string == fileName)
                {
                    return;
                }
            }

            TreeViewItem child = this.AddDirectoryItem(rootPathItem.Items, fileName, fullPath);

            child.ToolTip = fullPath;
        }

        private void Item_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (sender is TreeViewItem)
            {
                (sender as TreeViewItem).IsSelected = true;
                e.Handled = true;
            }
        }

        private void Remove(string file)
        {

        }

        private void RemoveItem_Click(object sender, RoutedEventArgs e)
        {
            TreeViewItem item = this.TreeView.SelectedItem as TreeViewItem;
            TreeViewItem parent = (item.Parent as TreeViewItem);

            if (parent != null && this.TreeView.Items[0] != item)
            {
                parent.Items.Remove(item);
            }

            e.Handled = true;
        }

        private void AddItem_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Forms.OpenFileDialog openFileDialog = new System.Windows.Forms.OpenFileDialog();

            openFileDialog.Filter = "All files (*.*)|*.*";
            openFileDialog.Multiselect = true;

            if (openFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                TreeViewItem selectedItem = this.TreeView.SelectedItem as TreeViewItem;
                selectedItem.IsExpanded = true;

                foreach (String path in openFileDialog.FileNames)
                {
                    this.AddFile(selectedItem, Path.GetFullPath(path));
                }
            }
        }

        private void NewItem_Click(object sender, RoutedEventArgs e)
        {
            TreeViewItem selectedItem = this.TreeView.SelectedItem as TreeViewItem;

            if (selectedItem != null)
            {
                selectedItem.IsExpanded = true;
                DirectoryViewItem child = this.AddDirectoryItem(selectedItem.Items, "New Folder", String.Empty);

                child.Focus();
            }
        }

        private void RenameItem_Click(object sender, RoutedEventArgs e)
        {
            DirectoryViewItem selectedItem = this.TreeView.SelectedItem as DirectoryViewItem;

            if (selectedItem != null)
            {
                selectedItem.IsExpanded = true;
                selectedItem.Focus();
            }
        }

        public Dictionary<string, string> GetContents()
        {
            Dictionary<string, string> paths = new Dictionary<string, string>();

            this.GetContents(ref paths, string.Empty, this.TreeView.Items);

            return paths;
        }

        public void GetContents(ref Dictionary<string, string> paths, string directory, ItemCollection collection)
        {
            foreach (DirectoryViewItem item in collection)
            {
                string relativePath = Path.Combine(directory, item.Path);
                if (!paths.ContainsKey(relativePath))
                {
                    paths.Add(relativePath, item.SourcePath);
                }
                if (item.Items.Count > 0)
                {
                    this.GetContents(ref paths, Path.Combine(directory, item.Path), item.Items);
                }
            }
        }
    }

    class DirectoryViewItem : TreeViewItem
    {
        private TextBox mTextBox;
        private bool mIsDirectory;
        public bool IsDirectory { get { return this.mIsDirectory; } }

        private string mPath = string.Empty;
        public string Path { get { return (this.mPath); } }
        public string SourcePath { get { return (this.ToolTip as string); } }

        public DirectoryViewItem(String relativePath, string sourcePath)
        {
            this.mTextBox = new TextBox();
            this.Header = new TextBlock(new Run(relativePath));
            this.mIsDirectory = string.IsNullOrEmpty(sourcePath);
            if (!this.mIsDirectory)
            {
                this.ToolTip = sourcePath;
            }
            this.mPath = relativePath;

            this.mTextBox.LostFocus += this.text_LostFocus;
            this.mTextBox.KeyDown += this.text_KeyDown;
        }

        void text_LostFocus(object sender, RoutedEventArgs e)
        {
            this.mPath = this.mTextBox.Text;
            this.Header = new TextBlock(new Run(this.mPath));
        }

        void text_Loaded(object sender, RoutedEventArgs e)
        {
            this.mTextBox.Focus();
        }

        void text_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                this.mPath = this.mTextBox.Text;
                this.Header = new TextBlock(new Run(this.mPath));
            }
        }

        public void ChangeColor(Brush brush)
        {
            if (this.Header is TextBlock)
            {
                (this.Header as TextBlock).Foreground = brush;
            }
        }

        public new void Focus()
        {
            if (this.Header is TextBlock)
            {
                this.mTextBox.Text = this.mPath;
                this.Header = this.mTextBox;

                if (this.mTextBox.IsLoaded)
                {
                    this.mTextBox.Focus();
                }
                else
                {
                    this.mTextBox.Loaded += this.text_Loaded;
                }
            }
        }
    }

}
