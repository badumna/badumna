﻿using SimplexPrestoManifesto;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace SimplexPrestoManifestoUnitTest
{
    
    
    /// <summary>
    ///This is a test class for ProgramTest and is intended
    ///to contain all ProgramTest Unit Tests
    ///</summary>
    [TestClass()]
    public class ProgramTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for ParseArguments
        ///</summary>
        [TestMethod()]
        public void ParseArgumentsTest()
        {
            string[] arguments = new string[]{"applicationName:application_name", @"projectFile:../projectFile.zip",
                "updateType:complete", "includeExtractionTools", "executableFile:test.exe", "description:description", @"output:../output"}; 
            Dictionary<string, string> expected = new Dictionary<string,string>(){{"applicationName","application_name"},
                                    {"projectFile",@"../projectFile.zip"},{"updateType","complete"},{"includeExtractionTools",string.Empty},
                                    {"executableFile","test.exe"},{"description","description"},{"output",@"../output"}}; 
            Dictionary<string, string> actual;
            actual = Program.ParseArguments(arguments);
            
            //// This method should be enough to check whether two dictionaries are the same.
            Assert.AreEqual(expected.Count, actual.Count);
            
            bool isEqual = true;
            foreach(KeyValuePair<string,string> pair in actual)
            {
                isEqual = expected.ContainsKey(pair.Key) && expected[pair.Key].Equals(pair.Value);
            }
            Assert.IsTrue(isEqual);
        }

        /// <summary>
        ///A test for Main
        ///</summary>
        [TestMethod()]
        public void MainTest()
        {
            string[] args = new string[]{"applicationName:application_name", @"projectFile:../projectFile.zip",
                "updateType:complete", "includeExtractionTools", "executableFile:test.exe", "description:description", @"output:../output"};
            Program.Main(args);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for Program Constructor
        ///</summary>
        [TestMethod()]
        public void ProgramConstructorTest()
        {
            Program target = new Program();
            Assert.IsNotNull(target);
        }
    }
}
