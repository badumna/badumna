﻿using SimplexPrestoManifesto;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
namespace SimplexPrestoManifestoUnitTest
{
    
    
    /// <summary>
    ///This is a test class for SimplexPrestoManifestoProcessTest and is intended
    ///to contain all SimplexPrestoManifestoProcessTest Unit Tests
    ///</summary>
    [TestClass()]
    public class SimplexPrestoManifestoProcessTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for UploadProjectFile
        ///</summary>
        [TestMethod()]
        public void UploadProjectFileTest()
        {
            SimplexPrestoManifestoProcess target = new SimplexPrestoManifestoProcess(); // TODO: Initialize to an appropriate value
            string fullPath = string.Empty; // TODO: Initialize to an appropriate value
            target.UploadProjectFile(fullPath);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for SaveProject
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SimplexPrestoManifesto.exe")]
        public void SaveProjectTest()
        {
            SimplexPrestoManifestoProcess_Accessor target = new SimplexPrestoManifestoProcess_Accessor(); // TODO: Initialize to an appropriate value
            string projectFullPath = Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory,"test.xml"); // TODO: Initialize to an appropriate value
            target.SaveProject(projectFullPath);
            Assert.IsTrue(File.Exists(projectFullPath));
        }

        /// <summary>
        ///A test for LoadProject
        ///</summary>
        [TestMethod()]
        public void LoadProjectTest()
        {
            SimplexPrestoManifestoProcess target = new SimplexPrestoManifestoProcess(); // TODO: Initialize to an appropriate value
            string projectFullPath = string.Empty; // TODO: Initialize to an appropriate value
            target.LoadProject(projectFullPath);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for AddFiles
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SimplexPrestoManifesto.exe")]
        public void AddFilesTest()
        {
            SimplexPrestoManifestoProcess_Accessor target = new SimplexPrestoManifestoProcess_Accessor(); // TODO: Initialize to an appropriate value
            string sourcePath = string.Empty; // TODO: Initialize to an appropriate value
            target.AddFiles(sourcePath);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for AddFile
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SimplexPrestoManifesto.exe")]
        public void AddFileTest()
        {
            SimplexPrestoManifestoProcess_Accessor target = new SimplexPrestoManifestoProcess_Accessor(); // TODO: Initialize to an appropriate value
            string fileFullpath = string.Empty; // TODO: Initialize to an appropriate value
            target.AddFile(fileFullpath);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for AddApplication
        ///</summary>
        [TestMethod()]
        public void AddApplicationTest()
        {
            SimplexPrestoManifestoProcess target = new SimplexPrestoManifestoProcess(); 
            string applicationName = "Test"; 
            string packagePath = Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory,@"Packages/"); // TODO: Initialize to an appropriate value
            target.AddApplication(applicationName, packagePath);
            Assert.IsTrue(File.Exists(Path.Combine(packagePath,string.Format("{0}.Project.xml",applicationName))));
        }

        /// <summary>
        ///A test for SimplexPrestoManifestoProcess Constructor
        ///</summary>
        [TestMethod()]
        public void SimplexPrestoManifestoProcessConstructorTest()
        {
            SimplexPrestoManifestoProcess target = new SimplexPrestoManifestoProcess();
            Assert.IsNotNull(target);
        }
    }
}
