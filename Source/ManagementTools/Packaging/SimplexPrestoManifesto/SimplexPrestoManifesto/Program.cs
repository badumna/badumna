﻿//---------------------------------------------------------------------------------
// <copyright file="Program.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
// <summary>The main function of SimplexPrestoManifesto application.</summary>
//---------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace SimplexPrestoManifesto
{
    /// <summary>
    /// SimplexPrestoManifesto is the simplification from PrestoManifesto application. This
    /// is a console application that build a package using the command line argument. This will
    /// be useful for the default services which will be built automatically using the python
    /// installer script.
    /// Requirement :
    /// 1) Have the 7z.exe set as an environment variable in Windows.
    /// </summary>
    public class Program
    {
        /// <summary>
        /// Build Process task delegate used to start async package build process.
        /// </summary>
        /// <param name="description">Package description.</param>
        private delegate void BuildProcessTask(string description);

        /// <summary>
        /// Main function of SimplexPrestoManifesto application.
        /// Command line argument
        /// SimplexPrestoManifesto.exe applicationName:[application name] projectFile:[relative project file Path] 
        ///     updateType:[complete/partial] includeExtractionTools executableFile:[.exe filename] description:[package description]
        ///     output:[relative output path]
        /// </summary>
        /// <param name="args">Command line arguments.</param>
        public static void Main(string[] args)
        {
            Dictionary<string, string> argDictionary = ParseArguments(args);
            SimplexPrestoManifestoProcess simplexPrestoManifesto = new SimplexPrestoManifestoProcess();
            string packagePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"Packages\");
            string outputPath = string.Empty;
            string packageDescription = string.Empty;
            string applicationName = string.Empty;

            if (argDictionary.ContainsKey("applicationName"))
            {
                applicationName = argDictionary["applicationName"];
                
                //// Try to load the project file first
                string projectFullPath = string.Format("{0}{1}.Project.xml", packagePath, applicationName);

                if (File.Exists(projectFullPath))
                {
                    simplexPrestoManifesto.LoadProject(projectFullPath);
                }
                else
                {
                    simplexPrestoManifesto.AddApplication(applicationName, packagePath);
                }
            }

            if (argDictionary.ContainsKey("projectFile"))
            {
                Directory.SetCurrentDirectory(AppDomain.CurrentDomain.BaseDirectory);
                string projectSourceFile = Path.GetFullPath(argDictionary["projectFile"]);
                if (File.Exists(projectSourceFile))
                {
                    if (Path.GetExtension(projectSourceFile).Equals(".zip"))
                    {
                        simplexPrestoManifesto.UploadProjectFile(projectSourceFile);
                    }
                    else
                    {
                        Console.WriteLine("{0} is an invalid extension", Path.GetExtension(projectSourceFile));
                    }
                }
                else
                {
                    Console.WriteLine("{0} doesn't exist", Path.GetFileName(projectSourceFile));
                }
            }

            if (argDictionary.ContainsKey("updateType"))
            {
                string updateType = argDictionary["updateType"];
                simplexPrestoManifesto.SetUpdateType(updateType.ToLower().Equals("complete"));
            }

            if (argDictionary.ContainsKey("includeExtractionTools"))
            {
                simplexPrestoManifesto.SetIncludeExtractionTools(true);
            }

            if (argDictionary.ContainsKey("executableFile"))
            {
                string exeFile = argDictionary["executableFile"];
                if (File.Exists(Path.Combine(Path.Combine(packagePath, applicationName), exeFile)))
                {
                    simplexPrestoManifesto.SetExecutableFile(Path.Combine(Path.Combine(packagePath, applicationName), exeFile));
                }
                else
                {
                    Console.WriteLine("{0} file cannot be found", exeFile);
                }
            }

            if (argDictionary.ContainsKey("description"))
            {
                packageDescription = argDictionary["description"];
            }

            if (argDictionary.ContainsKey("output"))
            {
                outputPath = Path.GetFullPath(argDictionary["output"]);
                simplexPrestoManifesto.SetOutput(outputPath);
            }

            //// Build a package
            BuildProcessTask buildProcessTask = new BuildProcessTask(simplexPrestoManifesto.BuildPackage);
            IAsyncResult result = buildProcessTask.BeginInvoke(packageDescription, null, null);
            while (!result.IsCompleted)
            {
                result.AsyncWaitHandle.WaitOne(500);
                simplexPrestoManifesto.GetBuildDescription();
            }

            simplexPrestoManifesto.CopyOutputPackage();
        }

        /// <summary>
        /// Parse the arguments given from the command line and put it into argDictionary, this method 
        /// has been used in the old PrestoManifesto.
        /// </summary>
        /// <param name="arguments">Command line arguments.</param>
        /// <returns>Return the argument dictionary.</returns>
        public static Dictionary<string, string> ParseArguments(string[] arguments)
        {
            Dictionary<string, string> argDictionary = new Dictionary<string, string>();
            foreach (string argument in arguments)
            {
                string[] parts = argument.Split(new char[] { ':' }, 2, StringSplitOptions.RemoveEmptyEntries);
                if (parts.Length == 1)
                {
                    argDictionary.Add(parts[0], string.Empty);
                }
                else
                {
                    argDictionary.Add(parts[0], parts[1]);
                }
            }

            return argDictionary;
        }

        /// <summary>
        /// Finish Build async method is called after the build process is finished.
        /// </summary>
        /// <param name="result">IAsyncResult content simplexPrestoManifesto instance.</param>
        private static void FinishBuild(IAsyncResult result)
        {
        }
    }
}
