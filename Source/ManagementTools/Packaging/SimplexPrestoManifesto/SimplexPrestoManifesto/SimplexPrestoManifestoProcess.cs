﻿//---------------------------------------------------------------------------------
// <copyright file="SimplexPrestoManifestoProcess.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
// <summary>SimplexPrestoManifestoProcess class.</summary>
//---------------------------------------------------------------------------------

#define DEBUG

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

using Germinator.PrestoManifesto;
using System.ComponentModel;

namespace SimplexPrestoManifesto
{
    /// <summary>
    /// SimplexPrestoManifestoProcess class handle the request from the command line argument, it serve likes a configuration class in ControlCenter.
    /// </summary>
    public class SimplexPrestoManifestoProcess
    {
        /// <summary>
        /// PrestoManifesto project, in this case one project contains one specific application as 
        /// this program will be used to create a default services only.
        /// </summary>
        private Project prestoManifestoProject;

        /// <summary>
        /// The current application name.
        /// </summary>
        private string applicationName;

        /// <summary>
        /// The package path.
        /// </summary>
        private string packagePath;

        /// <summary>
        /// Project file path.
        /// </summary>
        private string projectFullPath;

        /// <summary>
        /// The output package path.
        /// </summary>
        private string outputPath;

        /// <summary>
        /// Initializes a new instance of the <see cref="SimplexPrestoManifestoProcess"/> class.
        /// </summary>
        public SimplexPrestoManifestoProcess()
        {
            this.prestoManifestoProject = new Project();
        }

        /// <summary>
        /// Add an application to presto manifesto project then save the project.
        /// </summary>
        /// <param name="applicationName">Application/Service name.</param>
        /// <param name="packagePath">The application package path.</param>
        internal void AddApplication(string applicationName, string packagePath)
        {
            //// ServiceType is not used in this application, leave it as NonBadumnaService.
            PackageData packageData = new PackageData(applicationName, packagePath, ServiceType.NonBadumnaService);
            PackageControl packageControl = new PackageControl(packageData, this.prestoManifestoProject, AppDomain.CurrentDomain.BaseDirectory);
          
            this.prestoManifestoProject.Applications.Add(packageData);
            this.prestoManifestoProject.PackageControls.Add(applicationName, packageControl);
            this.applicationName = applicationName;
            this.packagePath = packagePath;
#if DEBUG
            Console.WriteLine("Successfully add a new application");
#endif
            //// Automatically save the project using this format {applicationName}.Project.xml
            this.projectFullPath = string.Format("{0}{1}.Project.xml", packagePath, applicationName);
            this.SaveProject(this.projectFullPath);
        }

        /// <summary>
        /// Load the exsisting project by providing the project file path.
        /// </summary>
        /// <param name="projectFullPath">Project file path.</param>
        internal void LoadProject(string projectFullPath)
        {
            this.projectFullPath = projectFullPath;
            this.packagePath = Path.GetDirectoryName(projectFullPath);
            using (FileStream fileStream = new FileStream(projectFullPath, FileMode.Open))
            {
                DataContractSerializer deserializer = new DataContractSerializer(typeof(Project));

                try
                {
                    this.prestoManifestoProject = deserializer.ReadObject(fileStream) as Project;
                    if (this.prestoManifestoProject.PackageControls == null)
                    {
                        this.prestoManifestoProject.PackageControls = new Dictionary<string, PackageControl>();
                    }

                    foreach (PackageData application in this.prestoManifestoProject.Applications)
                    {
                        PackageControl packageControl = new PackageControl(application, this.prestoManifestoProject, Path.GetDirectoryName(AppDomain.CurrentDomain.FriendlyName));
                        this.prestoManifestoProject.PackageControls.Add(application.Name, packageControl);

                        //// Assumption there will be only one application per project
                        this.applicationName = application.Name;
                    }
#if DEBUG
                    Console.WriteLine("Successfully load {0}", Path.GetFileName(projectFullPath));
#endif
                }
                catch
                {
#if DEBUG
                    Console.WriteLine("Failed to load {0} file", projectFullPath);
#endif
                    throw;
                }
            }
        }

        /// <summary>
        /// Upload a service zip file to the current project.
        /// </summary>
        /// <param name="fullPath">The file full path.</param>
        internal void UploadProjectFile(string fullPath)
        {
            if (File.Exists(fullPath))
            {
                string tempDirectory = Path.Combine(this.packagePath, "Temp");

                //// Check if package path directory is exist
                if (!Directory.Exists(this.packagePath))
                {
                    Directory.CreateDirectory(this.packagePath);
                }

                //// Check if temp directory is exist
                if (!Directory.Exists(tempDirectory))
                {
                    Directory.CreateDirectory(tempDirectory);
                }

                FileInfo fileInfo = new FileInfo(fullPath);
                fileInfo.CopyTo(Path.Combine(tempDirectory, fileInfo.Name),true);

                Directory.SetCurrentDirectory(tempDirectory);
                ProcessStartInfo processStartInfo = new ProcessStartInfo("7z.exe", string.Format("x -tzip \"{0}\"", Path.GetFileName(fullPath)));
                processStartInfo.CreateNoWindow = true;
                processStartInfo.UseShellExecute = false;

                try
                {
                    Process process = Process.Start(processStartInfo);
                    process.WaitForExit();
                }
                catch (Win32Exception e)
                {
                    int fileNotFoundErrorCode = 2;
                    if (e.NativeErrorCode == fileNotFoundErrorCode)
                    {
                        Console.WriteLine("Error: SimplexPrestoManifesto requires 7z.exe (7-zip) to be on the path");
                        Environment.Exit(1);
                    }

                    throw;
                }

                File.Delete(Path.GetFileName(fullPath));

                this.AddFiles(tempDirectory);
                this.SaveProject(this.projectFullPath);
                Directory.SetCurrentDirectory(AppDomain.CurrentDomain.BaseDirectory);
                Directory.Delete(tempDirectory, true);
#if DEBUG 
                Console.WriteLine("Successfully upload {0}", Path.GetFileName(fullPath));
#endif
            }
            else
            {
                Console.WriteLine("{0} is not exist", fullPath);
            }
        }

        /// <summary>
        /// Set the package update type "complete" or "partial".
        /// </summary>
        /// <param name="isSet">Update type is complete if it is set.</param>
        internal void SetUpdateType(bool isSet)
        {
             PackageControl packageControl;
             if (this.prestoManifestoProject.PackageControls.TryGetValue(this.applicationName, out packageControl))
             {
                 packageControl.UpdateType = isSet ? "complete" : "partial";
             }
        }

        /// <summary>
        /// Set the include extraction tools of package control property.
        /// </summary>
        /// <param name="isSet">Include the extraction tool if it is set.</param>
        internal void SetIncludeExtractionTools(bool isSet)
        {
            PackageControl packageControl;
            if (this.prestoManifestoProject.PackageControls.TryGetValue(this.applicationName, out packageControl))
            {
                packageControl.IncludeExtractionTools = isSet;
            }
        }

        /// <summary>
        /// Set the package executable file.
        /// </summary>
        /// <param name="fullPath">Executable file path.</param>
        internal void SetExecutableFile(string fullPath)
        {
            PackageControl packageControl;
            if (this.prestoManifestoProject.PackageControls.TryGetValue(this.applicationName, out packageControl))
            {
                packageControl.SetExecutable(fullPath);
            }
        }

        /// <summary>
        /// Set the output file
        /// </summary>
        /// <param name="output">Output path.</param>
        internal void SetOutput(string output)
        {
            this.outputPath = output;
        }

        /// <summary>
        /// Build a package with the given description.
        /// </summary>
        /// <param name="description">Build description.</param>
        internal void BuildPackage(string description)
        {
            PackageControl packageControl;
            if (this.prestoManifestoProject.PackageControls.TryGetValue(this.applicationName, out packageControl))
            {
                //// Check whether the executable file has been set
                if (!string.IsNullOrEmpty(packageControl.GetExecutable()))
                {
                    packageControl.BuildPackage(description);
                }
                else
                {
                    Console.WriteLine("Error: executable file has not been set properly");
                }
            }
        }

        /// <summary>
        /// Get build progress description.
        /// </summary>
        internal void GetBuildDescription()
        {
            PackageControl packageControl;
            if (this.prestoManifestoProject.PackageControls.TryGetValue(this.applicationName, out packageControl))
            {
                Console.WriteLine(packageControl.Description);
            }
        }

        /// <summary>
        /// Copy the output files to the given output path.
        /// </summary>
        internal void CopyOutputPackage()
        {
            if (!string.IsNullOrEmpty(this.outputPath))
            {
                PackageControl packageControl;
                if (this.prestoManifestoProject.PackageControls.TryGetValue(this.applicationName, out packageControl))
                {
                    packageControl.CopyBuiltPackage(this.outputPath);
                }
            }
        }

        /// <summary>
        /// Save the project to the given project file path.
        /// </summary>
        /// <param name="projectFullPath">Project file path.</param>
        private void SaveProject(string projectFullPath)
        {
            this.prestoManifestoProject.Save(projectFullPath);
#if DEBUG
            Console.WriteLine("Save the {0}", Path.GetFileName(projectFullPath));
#endif
        }

        /// <summary>
        /// Add file to the current project.
        /// </summary>
        /// <param name="fileFullpath">File full path.</param>
        private void AddFile(string fileFullpath)
        {
            PackageControl packageControl;
            if (this.prestoManifestoProject.PackageControls.TryGetValue(this.applicationName, out packageControl))
            {
                //// Set the modification time to last access time for the specific file. 
                FileInfo fileInfo = new FileInfo(fileFullpath);
                string destFileName = Path.Combine(Path.Combine(this.packagePath, this.applicationName), Path.GetFileName(fileFullpath));
                if (packageControl.AddFile(destFileName, fileInfo.LastAccessTime))
                {
                    File.Copy(fileFullpath, destFileName, true);
                }
            }
        }

        /// <summary>
        /// Recursively Add files to the current project.
        /// </summary>
        /// <param name="sourcePath">The directory path.</param>
        private void AddFiles(string sourcePath)
        {
            string[] files = Directory.GetFiles(sourcePath);
            string[] directories = Directory.GetDirectories(sourcePath);

            foreach (string file in files)
            {
                this.AddFile(file);
            }

            foreach (string directory in directories)
            {
                this.AddFiles(directory);
            }
        }
    }
}
