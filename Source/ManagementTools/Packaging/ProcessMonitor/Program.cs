﻿using System;
using System.Text;
using System.IO;

using Badumna.Package;

namespace ProcessMonitor
{
    class Program
    {
        static string ProcessArguments(string[] args)
        {
            StringBuilder applicationArguments = new StringBuilder();

            foreach (string arg in args)
            {
                applicationArguments.AppendFormat("{0} ", arg);
            }

            return applicationArguments.ToString();
        }

        static void Main(string[] args)
        {
            string applicationArguments = Program.ProcessArguments(args);
            string directory = Path.GetDirectoryName(AppDomain.CurrentDomain.FriendlyName);

            Updater updater = new Updater(directory, null);

            switch (updater.UpdateIfAvailable())
            {
                case Updater.Result.ArgumentError:
                    return;

                case Updater.Result.PackageError:
                    return;

                default:
                    break;
            }

            if (!string.IsNullOrEmpty(updater.Executable) && File.Exists(updater.Executable))
            {
                ProcessMonitor monitor = new ProcessMonitor();

                monitor.Monitor(updater.Executable, Path.GetDirectoryName(updater.Executable), applicationArguments);
            }
        }
    }
}
