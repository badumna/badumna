using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.Xml;
using System.Xml.XPath;
using System.IO;
using System.Runtime.Serialization;

namespace Badumna.Package
{
    internal class Persistable<T> where T : new()
    {
        public static T Load(String filename)
        {
            if (!File.Exists(filename))
            {
                throw new LoadErrorException(String.Format("Unable to find file {0}", filename));
            }

            try
            {
                using (Stream stream = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.Read))
                {
                    return Persistable<T>.CustomLoad(stream);
                }
            }
            catch (Exception e)
            {
                StreamWriter writer = new StreamWriter("Persistable-error.log", true);

                writer.WriteLine("Exception caught while attempting to parse persitable object {0} from {1}", typeof(T).FullName, filename);
                writer.WriteLine("Exception : {0}", e.Message);
                writer.WriteLine("{0}", e.StackTrace);
                writer.Flush();
                writer.Close();

                throw new LoadErrorException(String.Format("Failed to parse {0}", filename), e);
            }
        }

        public static T Load(Stream stream)
        {
            return Persistable<T>.CustomLoad(stream);
        }

        public static void Save(String filename, T persitableObject)
        {
            using (Stream stream = new FileStream(filename, FileMode.Create, FileAccess.Write))
            {
                Persistable<T>.CustomSave(stream, persitableObject);
                stream.Close();
            }
        }

        public static void Save(Stream stream, T persitableObject)
        {
            Persistable<T>.CustomSave(stream, persitableObject);
        }

        private static bool LoadObject(ref object persistable, XPathNavigator navigator)
        {
            Type type = persistable.GetType();
            MemberInfo[] members = type.GetMembers(BindingFlags.NonPublic | BindingFlags.Instance);

            foreach (MemberInfo memberInfo in members)
            {
                object memberValue = null;
                bool isField = true;

                if (memberInfo.MemberType == MemberTypes.Field)
                {
                    memberValue = ((FieldInfo)(memberInfo)).GetValue(persistable);
                }
                if (memberInfo.MemberType == MemberTypes.Property)
                {
                    if (null != ((PropertyInfo)(memberInfo)).GetSetMethod())
                    {
                        memberValue = ((PropertyInfo)(memberInfo)).GetValue(persistable, null);
                    }
                    isField = false;
                }

                if (null != memberValue)
                {
                    String memberName = Persistable<T>.TrimPrefix(memberInfo.Name);

                    if (memberValue is SortedList)
                    {
                        XPathNavigator collectionNav = navigator.SelectSingleNode("child::" + memberName);

                        if (null != collectionNav)
                        {
                            XPathNodeIterator navigators = collectionNav.SelectChildren("Item", "");
                            foreach (XPathNavigator itemNav in navigators)
                            {
                                Object item = new DictionaryEntry(String.Empty, String.Empty);

                                if (Persistable<T>.LoadObject(ref item, itemNav))
                                {
                                    (memberValue as SortedList).Add(((DictionaryEntry)item).Key, ((DictionaryEntry)item).Value);
                                }
                            }
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                    {
                        XPathNavigator memberNode = navigator.SelectSingleNode(String.Format("child::{0}", memberName));

                        if (null != memberNode)
                        {
                            if (memberValue.GetType() != typeof(bool))
                            {
                                try
                                {
                                    if (memberValue.GetType() == typeof(String))
                                    {
                                        memberValue = String.Copy((memberNode.InnerXml));
                                    }
                                    else
                                    {
                                        memberValue = memberNode.ValueAs(memberValue.GetType());
                                    }
                                }
                                catch (NotImplementedException)
                                {
                                    // For Mono
                                    if (memberValue.GetType() == typeof(int))
                                    {
                                        memberValue = memberNode.ValueAsInt;
                                    }
                                    else if (memberValue.GetType() == typeof(long))
                                    {
                                        memberValue = memberNode.ValueAsLong;
                                    }
                                    else if (memberValue.GetType() == typeof(String))
                                    {
                                        memberValue = String.Copy((memberNode.InnerXml));
                                    }
                                    else if (memberValue.GetType() == typeof(int))
                                    {
                                        memberValue = memberNode.ValueAsInt;
                                    }
                                    else
                                    {
                                        memberValue = (object)String.Copy(memberNode.InnerXml);
                                    }
                                }
                            }
                            else
                            {
                                memberValue = memberNode.Value == "True";
                            }
                        }
                    }

                    try
                    {
                        if (isField)
                        {
                            ((FieldInfo)(memberInfo)).SetValue(persistable, memberValue);
                        }
                        else
                        {
                            ((PropertyInfo)(memberInfo)).SetValue(persistable, memberValue, null);
                        }
                    }
                    catch 
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        private static String TrimPrefix(String memberName)
        {
            String result = memberName;

            if (memberName[0] == 'm' || memberName[0] == '_')
            {
                result = memberName.Remove(0, 1);
            }

            return result;
        }


        private static T CustomLoad(Stream stream)
        {
            Object persistable = new T();

            XmlDocument document = new XmlDocument();

            document.Load(stream);

            XPathNavigator navigator = document.CreateNavigator();
            if (null == navigator)
            {
                return default(T);
            }

            navigator = navigator.SelectSingleNode("child::Manifest");
            if (null != navigator && Persistable<T>.LoadObject(ref persistable, navigator))
            {
                return (T)persistable;
            }

            return default(T);
        }



        private static XmlNode SaveObject(String name, Object persistable, XmlDocument document)
        {
            Type type = persistable.GetType();
            MemberInfo[] members = type.GetMembers(BindingFlags.NonPublic | BindingFlags.GetField | BindingFlags.Instance);

            XmlElement namedNode = document.CreateElement(name);

            foreach (MemberInfo memberInfo in members)
            {
                object memberValue = null;
                String memberName = Persistable<T>.TrimPrefix(memberInfo.Name);

                if (memberInfo.MemberType == MemberTypes.Field)
                {
                    memberValue = ((FieldInfo)(memberInfo)).GetValue(persistable);
                }

                if (null != memberValue && !memberInfo.IsDefined(typeof(NonSerializedAttribute), true))
                {
                    XmlElement memberNode = null;

                    if (memberValue is ICollection)
                    {
                        memberNode = document.CreateElement(memberName, "");

                        foreach (object obj in (memberValue as ICollection))
                        {
                            XmlNode node = Persistable<T>.SaveObject("Item", obj, document);
                            if (null != node)
                            {
                                memberNode.AppendChild(node);
                            }
                        }
                    }
                    else if (null != memberValue)
                    {
                        if ((memberValue is String) && (memberValue as String).Length == 0)
                        {
                            continue;
                        }

                        memberNode = document.CreateElement(memberName, "");
                        memberNode.InnerXml = memberValue.ToString();         
                    }
                    else
                    {
                        continue;
                    }

                    if (null != memberNode)
                    {
                        namedNode.AppendChild(memberNode);
                    }
                }
            }

            return namedNode;
        }

        private static void CustomSave(Stream stream, T persistable)
        {
            XmlDocument document = new XmlDocument();

            XmlDeclaration declaration = document.CreateXmlDeclaration("1.0", "utf-8", null);
            XmlProcessingInstruction instruction = document.CreateProcessingInstruction("xml-stylesheet", "type='text/xsl' href='Manifest.xslt'");

            document.InsertBefore(declaration, document.DocumentElement);
            document.InsertBefore(instruction, document.DocumentElement);

            XmlNode manifestNode = Persistable<T>.SaveObject("Manifest", persistable, document);

            if (null != manifestNode)
            {
                document.AppendChild(manifestNode);
            }

            XmlTextWriter writer = new XmlTextWriter(stream, Encoding.ASCII);

            writer.Formatting = Formatting.Indented;
            writer.Indentation = 4;

            document.WriteTo(writer);
            writer.Flush();
        }
    }

    [Serializable]
    public class LoadErrorException : ApplicationException
    {
        public LoadErrorException(String message)
            : base(message)
        { }

        public LoadErrorException(String message, Exception innerException)
            : base(message, innerException)
        { }


        public LoadErrorException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }

    [Serializable]
    public class SaveErrorException : ApplicationException
    {
        public SaveErrorException(String message)
            : base(message)
        { }

        public SaveErrorException(String message, Exception innerException)
            : base(message, innerException)
        { }

        public SaveErrorException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

    }
}
