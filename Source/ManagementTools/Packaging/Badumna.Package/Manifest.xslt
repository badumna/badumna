<?xml version="1.0" encoding="utf-8"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html" indent="yes" />

  <xsl:param name="version">1</xsl:param>
  
  <xsl:template match="/">

          <html>
            <body>
              <h2>
                <xsl:value-of select="Manifest/ApplicationName"/>
              </h2>

              <xsl:value-of select="Manifest/Description"/>
              <hr></hr>
              
              <table border="0">
                <tr>
                  <td>Application version</td>
                  <td>
                    <xsl:value-of select="Manifest/Major"/>.<xsl:value-of select="Manifest/Minor"/>.<xsl:value-of select="Manifest/Build"/>.<xsl:value-of select="Manifest/Revision"/>
                  </td>
                </tr>
                <tr>
                  <td>Protocol version</td>
                  <td>
                    0.<xsl:value-of select="Manifest/ProtocolVersion"/>
                  </td>
                </tr>
                <tr>
                  <td>Executable</td>
                  <td>
                    <xsl:value-of select="Manifest/Executable"/>
                  </td>
                </tr>
                <tr>
                  <td>Date</td>
                  <td>
                    <xsl:value-of select="Manifest/Date"/>
                  </td>
                </tr>
                <tr>
                  <td>File size</td>
                  <td>
                    <xsl:value-of select="Manifest/PackageSizeKB"/> KB
                  </td>
                </tr>
              </table>

              <hr></hr>

              <table border="0">
                <th height="10" align="left">Contents</th>
                <xsl:for-each select="Manifest/FileList/Item">
                  <tr>
                    <td>
                      <xsl:value-of select="key"/>
                    </td>
                  </tr>
                </xsl:for-each>
              </table>

              <hr></hr>

              <a href="../AppInstaller.msi">Download</a>

            </body>
          </html>
        
  </xsl:template>

</xsl:stylesheet>
