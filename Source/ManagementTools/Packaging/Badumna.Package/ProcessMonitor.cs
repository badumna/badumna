﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.IO;

namespace Badumna.Package
{
    public class ProcessMonitor
    {
        private bool mHasRequestedRestart;
        private int mToken;
        private UdpClient mClient;

        public void Monitor(string executablePath, string executableDirectory, string executableArguments)
        {
            Random random = new Random((int)DateTime.Now.Ticks);

            random.Next();
            int port = this.StartListening(random.Next(1024, 30000));

            if (port < 0)
            {
                return;
            }

            do
            {
                this.mHasRequestedRestart = false;

                this.mToken = random.Next();
                ProcessStartInfo processInfo = new ProcessStartInfo(executablePath, executableArguments);

                processInfo.WorkingDirectory = executableDirectory;
                this.AddEnvironmentVariable(processInfo, "ProcessMonitorPort", port.ToString());
                this.AddEnvironmentVariable(processInfo, "ProcessMonitorToken", this.mToken.ToString());
                processInfo.UseShellExecute = false;

                Process process = Process.Start(processInfo);

                // Sleep until the process exits
                do
                {
                    System.Threading.Thread.Sleep(500);
                }
                while (!process.HasExited);
            }
            while (this.mHasRequestedRestart);

            if (this.mClient != null)
            {
                try
                {
                    this.mClient.Close();
                }
                catch { }
            }
        }

        private void AddEnvironmentVariable(ProcessStartInfo processInfo, string variable, string value)
        {
            if (processInfo.EnvironmentVariables.ContainsKey(variable))
            {
                processInfo.EnvironmentVariables[variable] = value;
            }
            else
            {
                processInfo.EnvironmentVariables.Add(variable, value);
            }
        }

        private int StartListening(int port)
        {
            try
            {
                this.mClient = new UdpClient(new IPEndPoint(new IPAddress(new byte[] { 127, 0, 0, 1 }), port));
                this.mClient.BeginReceive(this.ReceiveMessage, null);
                return port;
            }
            catch (SocketException)
            {
                return this.StartListening(port + 1);
            }
            catch
            { }

            return -1;
        }

        private void ReceiveMessage(IAsyncResult ar)
        {
            try
            {
                IPEndPoint endPoint = new IPEndPoint(IPAddress.Any, 0);
                byte[] message = this.mClient.EndReceive(ar, ref endPoint);

                if (message.Length == 4 && BitConverter.ToInt32(message, 0) == this.mToken)
                {
                    this.mHasRequestedRestart = true;
                }
            }
            catch (SocketException)
            {
                return;
            }

            try
            {
                this.mClient.BeginReceive(this.ReceiveMessage, null);
            }
            catch { }
        }       

    }
}
