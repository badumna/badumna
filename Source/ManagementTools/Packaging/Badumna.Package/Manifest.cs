using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Badumna.Package
{
    [Serializable]
    public class Manifest
    {
        public const string DefaultFileName = "Manifest.xml";

        #region Versioning

        protected int mMajor;
        public int Major { get { return this.mMajor; } }

        protected int mMinor;
        public int Minor { get { return this.mMinor; } }

        protected int mBuild;
        public int Build { get { return this.mBuild; } }

        protected int mRevision;
        public int Revision { get { return this.mRevision; } }

        protected int mProtocolVersion;
        public int ProtocolVersion { get { return this.mProtocolVersion; } }

        protected String mProtocolHash = String.Empty;
        public String ProtocolHash { get { return this.mProtocolHash; } }

        public String Version
        {
            get { return String.Format("{0}.{1}.{2}.{3}", this.Major, this.Minor, this.Build, this.Revision); }
        }

        #endregion

        #region Update information

        protected String mUpdateType = String.Empty;
        public String UpdateType { get { return this.mUpdateType; } }

        protected bool mIsCritical;
        public bool IsCriticial { get { return this.mIsCritical; } }

        protected String mDate = DateTime.Now.ToString();
        public String Date { get { return this.mDate; } }

        protected String mApplicationName = String.Empty;
        public String ApplicationName
        {
            get { return this.mApplicationName; }
        }

        private String mDescription = String.Empty;
        public String Description
        {
            get { return this.mDescription; }
            set { this.mDescription = value; }
        }

        #endregion

        #region Package location

        protected String mPackageBaseUri = String.Empty;
        public String PackageBaseUri { get { return this.mPackageBaseUri; } }

        #endregion

        #region Local data

        [NonSerialized]
        private String mFileName = String.Empty;
        public String FileName { get { return this.mFileName; } }

        [NonSerialized]
        private String mDeploymentDirectory = String.Empty;
        public String DeploymentDirectory { get { return this.mDeploymentDirectory; } }
        public bool IsDeployed { get { return !String.IsNullOrEmpty(this.mDeploymentDirectory); } }

        public String PackageFilename { get { return this.PackageKey + ".zip"; } }
        public String PackageUri { get { return this.mPackageBaseUri + this.PackageFilename; } }

        public String LastCompletePackageFileName { get { return this.LastCompletePackageKey + ".zip"; } }
        public String LastCompletePackageUri { get { return this.mPackageBaseUri + this.LastCompletePackageFileName; } }

        public bool IsComplete { get { return this.mUpdateType == "Complete"; } }

        #endregion

        #region Package contents

        private String mPackageKey = String.Empty;
        public String PackageKey { get { return this.mPackageKey; } }

        private String mPackageChecksum = String.Empty;
        public String PackageChecksum { get { return this.mPackageChecksum; } }

        protected String mLastCompletePackageKey = String.Empty;
        public String LastCompletePackageKey { get { return this.mLastCompletePackageKey; } }

        protected String mExecutable = String.Empty;
        public String Executable { get { return this.mExecutable; } }

        protected long mPackageSizeKB;
        public long PackageSizeKB { get { return this.mPackageSizeKB; } }

        protected long mLatestCompletePackageSizeKB;
        public long LatestCompletePackageSizeKb { get { return this.mLatestCompletePackageSizeKB; } }

        private String mLastCompletePackageChecksum = String.Empty;
        public String LastCompletePackageChecksum { get { return this.mLastCompletePackageChecksum; } }

        private String mSignature = "None";
        public String Signature { get { return this.mSignature; } }

        private String mLastCompletePackageSignature = "None";
        public String LastCompletePackageSignature { get { return this.mLastCompletePackageSignature; } }

        protected SortedList mFileList = new SortedList();
        public SortedList FileList { get { return this.mFileList; } }


        #endregion

        public void Copy(Manifest copy)
        {
            this.mMajor = copy.mMajor;
            this.mMinor = copy.mMinor;
            this.mBuild = copy.mBuild;
            this.mRevision = copy.mRevision;
            this.mProtocolVersion = copy.mProtocolVersion;
            this.mProtocolHash = copy.mProtocolHash;

            this.mUpdateType = copy.mUpdateType;
            this.mIsCritical = copy.mIsCritical;
            this.mApplicationName = copy.ApplicationName;
            this.mDescription = copy.mDescription;

            this.mPackageKey = copy.mPackageKey;
            this.mExecutable = copy.mExecutable;

            this.mFileList.Clear();
            foreach (DictionaryEntry pair in copy.FileList)
            {
                this.FileList.Add(pair.Key, pair.Value);
            }

            this.mPackageChecksum = copy.mPackageChecksum;
            this.mPackageBaseUri = copy.mPackageBaseUri;
            this.mLastCompletePackageKey = copy.mLastCompletePackageKey;
            this.mLatestCompletePackageSizeKB = copy.mLatestCompletePackageSizeKB;            
            this.mLastCompletePackageChecksum = copy.mLastCompletePackageChecksum;

            this.mFileName = copy.mFileName;
            this.mPackageSizeKB = copy.mPackageSizeKB;
        }

        public static Manifest Load(String fileName)
        {
            Manifest manifest = Persistable<Manifest>.Load(fileName);

            if (null != manifest)
            {
                manifest.mFileName = fileName;
                manifest.mDeploymentDirectory = Path.GetDirectoryName(Path.GetFullPath(fileName));
            }

            return manifest;
        }

        public static Manifest Load(Stream stream)
        {
            return Persistable<Manifest>.Load(stream);
        }

        public virtual void Save(String fileName)
        {
            this.mFileName = fileName;
            Persistable<Manifest>.Save(fileName, this);
        }

        public virtual void Save(Stream stream)
        {
            Persistable<Manifest>.Save(stream, this);
        }

        public bool IsOlderThan(Manifest other)
        {
            if (this.Major == other.Major)
            {
                if (this.Minor == other.Minor)
                {
                    if (this.Build == other.Build)
                    {
                        return this.Revision < other.Revision;
                    }
                    else
                    {
                        return this.Build < other.Build;
                    }
                }
                else
                {
                    return this.Minor < other.Minor;
                }
            }

            return this.Major < other.Major;
        }

        public void SetInternalEntry(String manifestHash)
        {
            if (null != manifestHash)
            {
                this.mFileList.Add(this.FileName, manifestHash);
            }
        }

        public void SetFilename(String filename)
        {
            if (null != filename)
            {
                this.mFileName = filename;
            }
        }

        public void SetApplicationName(String applicationName)
        {
            if (null != applicationName)
            {
                this.mApplicationName = applicationName;
            }
        }

        public void SetDescription(String comments)
        {
            if (null != comments)
            {
                this.mDescription = comments;
            }
        }

        protected void SetPackageKey(String packageKey)
        {
            if (null != packageKey)
            {
                this.mPackageKey = packageKey;
            }
        }

        public void SetLastCompletePackageKey(String packageKey)
        {
            if (null != packageKey)
            {
                this.mLastCompletePackageKey = packageKey;
            }
        }

        public void SetLastCompletePackageChecksum(String checksum)
        {
            if (null != checksum)
            {
                this.mLastCompletePackageChecksum = checksum;
            }
        }

        public void SetPackageSize(long packageSize)
        {
            this.mPackageSizeKB = packageSize;

            if (this.mUpdateType == "Complete")
            {
                this.mLatestCompletePackageSizeKB = packageSize;
            }
        }

        public void SetPackageCheckSum(String checksum)
        {
            this.mPackageChecksum = checksum;
        }
    }
}
