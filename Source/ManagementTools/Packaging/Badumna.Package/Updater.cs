﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Diagnostics;
using System.Runtime.Serialization;
using System.Security.Cryptography;
using System.Net;
using System.Reflection;

using ICSharpCode.SharpZipLib.Zip;
using ICSharpCode.SharpZipLib.Zip.Compression.Streams;

namespace Badumna.Package
{
    public class Updater
    {
        private const string DefaultPackageDirectory = "Packages";
        private const string DefaultApplicationDirectory = "";

        private string mBaseDirectory;
        private string mPackagePath;
        private string mApplicationPath;
        private string mManifestUri;
        private string mExecutable;
        private string mRunningExecutable;

        public string Executable { get { return this.mExecutable; } }
        public string ExecutableDirectory { get { return this.mApplicationPath; } }

        public enum Result
        {
            PackageError,
            ArgumentError,
            NoUpdateAvailable,
            Updated,
            UpToDate,
            IOConflict
        }

        public Updater(string baseDirectory, string manifestUri)
        {
            this.mBaseDirectory = baseDirectory;
            this.mManifestUri = manifestUri;
            this.mRunningExecutable = Environment.GetCommandLineArgs()[0];
        }

        public Result UpdateIfAvailable()
        {
            if (String.IsNullOrEmpty(this.mBaseDirectory))
            {
                Trace.TraceError("Invalid package directory");
                return Result.ArgumentError;
            }

            if (!Path.IsPathRooted(this.mBaseDirectory))
            {
                Trace.TraceError("Package directory {0} is not full path", this.mBaseDirectory);
                return Result.ArgumentError;
            }

            if (!Directory.Exists(this.mBaseDirectory))
            {
                try
                {
                    Directory.CreateDirectory(this.mBaseDirectory);
                }
                catch (IOException)
                {
                    Trace.TraceError("Cannot create directory {0}", this.mBaseDirectory);
                    return Result.PackageError;
                }
                catch (UnauthorizedAccessException)
                {
                    Trace.TraceError("Cannot create directory {0}. Unauthorized", this.mBaseDirectory);
                    return Result.PackageError;
                }
            }

            if (String.IsNullOrEmpty(this.mPackagePath))
            {
                this.mPackagePath = Path.Combine(this.mBaseDirectory, Updater.DefaultPackageDirectory);
            }

            if (!Directory.Exists(this.mPackagePath))
            {
                try
                {
                    Directory.CreateDirectory(this.mPackagePath);
                }
                catch (IOException)
                {
                    Trace.TraceError("Cannot create package directory {0}", this.mPackagePath);
                    return Result.PackageError;
                }
                catch (UnauthorizedAccessException)
                {
                    Trace.TraceError("Cannot create package directory {0}. Unauthorized", this.mPackagePath);
                    return Result.PackageError;
                }
            }

            if (String.IsNullOrEmpty(this.mApplicationPath))
            {
                this.mApplicationPath = Path.Combine(this.mBaseDirectory, Updater.DefaultApplicationDirectory);
            }

            if (!Directory.Exists(this.mApplicationPath))
            {
                try
                {
                    Directory.CreateDirectory(this.mApplicationPath);
                }
                catch (IOException)
                {
                    Trace.TraceError("Cannot create application directory {0}", this.mApplicationPath);
                    return Result.PackageError;
                }
                catch (UnauthorizedAccessException)
                {
                    Trace.TraceError("Cannot create application directory {0}. Unauthorized", this.mApplicationPath);
                    return Result.PackageError;
                }
            }

            Manifest manifest = null;
            string defaultManifestFileName = Path.Combine(this.mBaseDirectory, Manifest.DefaultFileName);

            if (String.IsNullOrEmpty(this.mManifestUri))
            {
                this.mManifestUri = defaultManifestFileName;
            }

            if (manifest == null)
            {
                try
                {
                    Uri manifestUri = new Uri(this.mManifestUri);

                    if (manifestUri.IsFile)
                    {
                        if (File.Exists(this.mManifestUri))
                        {
                            try
                            {
                                manifest = Manifest.Load(this.mManifestUri);
                            }
                            catch (Exception e)
                            {
                                Trace.TraceError("Failed to load manifest {0} : {1}", manifestUri, e.Message);
                            }
                        }
                    }
                    else
                    {
                        try
                        {
                            if (this.DownloadFile(manifestUri.AbsoluteUri, defaultManifestFileName))
                            {
                                manifest = Manifest.Load(defaultManifestFileName);
                            }
                        }
                        catch
                        {
                            Trace.TraceError("Failed to load manifest {0}", defaultManifestFileName);
                        }
                    }
                }
                catch (UriFormatException e)
                {
                    Trace.TraceError("Invalid manifest uri : {0}", e.Message);
                }
            }

            Manifest latestManifest = null;

            if (this.TryFindNewerManifest(manifest, out latestManifest))
            {
                try
                {
                    latestManifest.Save(defaultManifestFileName);
                }
                catch (IOException)
                {
                    Trace.TraceError("Failed to save manifest.");
                    return Result.IOConflict;
                }
                catch (Exception e)
                {
                    Trace.TraceError("Failed to save manifest : {0}", e.Message);
                    return Result.PackageError;
                }

                this.mExecutable = Path.Combine(this.mApplicationPath, latestManifest.Executable);
                return this.Update(latestManifest);
            }

            if (manifest == null)
            {
                Trace.TraceError("Cannot find update manifest. Giving up.");
                return Result.PackageError;
            }
            else
            {
                this.mExecutable = Path.Combine(this.mApplicationPath, manifest.Executable);
            }

            return Result.UpToDate;
        }

        public static void TryCleanDirectory(string directory)
        {
            if (string.IsNullOrEmpty(directory) || !Directory.Exists(directory))
            {
                return;
            }

            try
            {
                Manifest latestManifest = null;
                string[] files = Directory.GetFiles(directory, "*.zip");
                List<Manifest> packageManifests = new List<Manifest>();

                // Find the latest manifest
                foreach (string file in files)
                {
                    Manifest manifest = Updater.ExtractManifest(file);
                    if (manifest != null)
                    {
                        manifest.SetFilename(file);
                        packageManifests.Add(manifest);
                        if (latestManifest == null || latestManifest.IsOlderThan(manifest))
                        {
                            latestManifest = manifest;
                        }
                    }
                }

                foreach (Manifest manifest in packageManifests)
                {
                    if (latestManifest.PackageFilename != manifest.PackageFilename
                        && (latestManifest.IsComplete || latestManifest.LastCompletePackageFileName != manifest.PackageFilename))
                    {
                        try
                        {
                            File.Delete(Path.Combine(directory, manifest.PackageFilename));
                        }
                        catch { }
                    }
                }
            }
            catch { }
        }

        private bool TryFindNewerManifest(Manifest currentManifest, out Manifest latestManifest)
        {
            String[] zipFiles = Directory.GetFiles(this.mPackagePath, "*.zip");
            List<String> packages = new List<string>();

            latestManifest = currentManifest;
            bool foundNewer = false;

            foreach (String file in zipFiles)
            {
                Manifest manifest = Updater.ExtractManifest(file);

                if (null != manifest)
                {
                    packages.Add(file);
                    if (null == latestManifest || latestManifest.IsOlderThan(manifest))
                    {
                        latestManifest = manifest;
                        foundNewer = true;
                    }
                }
            }

            if (!foundNewer && currentManifest != null)
            {
                string tempManifestFilename = Path.GetTempFileName();
                Uri manifestUri = null;

                try
                {
                    if (Uri.TryCreate(new Uri(currentManifest.PackageBaseUri), Manifest.DefaultFileName, out manifestUri))
                    {
                        if (this.DownloadFile(manifestUri.AbsoluteUri, tempManifestFilename))
                        {
                            try
                            {
                                latestManifest = Manifest.Load(tempManifestFilename);
                            }
                            catch (Exception)
                            {
                                Trace.TraceError("Failed to download manifest from {0}", manifestUri);
                            }
                            finally
                            {
                                if (File.Exists(tempManifestFilename))
                                {
                                    File.Delete(tempManifestFilename);
                                }
                            }
                        }
                    }
                }
                catch (UriFormatException)
                { }
            }

            // Remove old packages
            foreach (String file in packages)
            {
                string relativeFilePath = Path.GetFileName(file);
                if (relativeFilePath != latestManifest.PackageFilename && relativeFilePath != latestManifest.LastCompletePackageFileName)
                {
                    try
                    {
                        if (File.Exists(file))
                        {
                            File.Delete(file);
                        }
                    }
                    catch { }
                }
            }

            return foundNewer;
        }

        private Result Update(Manifest manifest)
        {
            string packageFile = Path.Combine(this.mPackagePath, manifest.PackageFilename);

            if (!File.Exists(packageFile))
            {
                if (!this.DownloadPackage(manifest))
                {
                    Trace.TraceError("Package {0} is not available.", packageFile);
                    return Result.NoUpdateAvailable;
                }
            }

            if (!manifest.IsComplete)
            {
                string lastCompletePackageFile = Path.Combine(this.mPackagePath, manifest.LastCompletePackageFileName);

                if (!File.Exists(lastCompletePackageFile))
                {
                    if (!this.DownloadPackage(manifest.LastCompletePackageUri, lastCompletePackageFile,
                        manifest.LastCompletePackageChecksum, manifest.LastCompletePackageSignature))
                    {
                        Trace.TraceError("Prerequisite package {0} is not available.", lastCompletePackageFile);
                        return Result.NoUpdateAvailable;
                    }
                }

                if (!this.ExtractPackageContents(lastCompletePackageFile))
                {
                    return Result.IOConflict;
                }
            }

            if (!this.ExtractPackageContents(packageFile))
            {
                return Result.IOConflict;
            }

            Updater.TryCleanDirectory(this.mPackagePath);
            return Result.Updated;
        }

        private bool DownloadFile(String url, string destinationFile)
        {
            try
            {
                WebClient client = new WebClient();

                client.DownloadFile(url, destinationFile);
                return true;
            }
            catch (NotSupportedException)
            {
                Trace.TraceError("Download not supported.");
            }
            catch (WebException)
            {
                Trace.TraceError("Failed to download file {0}", url);
            }
            catch (IOException)
            {
                Trace.TraceError("Cannot download file {1} to {0}. Does it already exists ?", destinationFile, url);
            }

            return false;
        }

        private bool DownloadPackage(Manifest manifest)
        {
            string packageFile = Path.Combine(this.mPackagePath, manifest.PackageFilename);

            return this.DownloadPackage(manifest.PackageUri, packageFile, manifest.PackageChecksum, manifest.Signature);
        }

        private bool DownloadPackage(string url, string fileName, string checksum, string signature)
        {
            if (!this.DownloadFile(url, fileName))
            {
                return false;
            }

            if (!this.IsPackageValid(fileName, checksum, signature))
            {
                this.RemovePackage(fileName);
                Trace.TraceError("Package at {0} could not be validated.", url);
                return false;
            }

            return true;
        }

        private bool IsPackageValid(string packageFileName, string checksum, string signature)
        {
            // Check the check sum
            // Check the signiture

            return true;
        }

        private void RemovePackage(String packageFile)
        {
            if (String.IsNullOrEmpty(packageFile))
            {
                return;
            }

            if (File.Exists(packageFile))
            {
                try
                {
                    File.Delete(packageFile);
                }
                catch
                { }
            }
        }

        private bool ExtractPackageContents(string packageFileName)
        {
            return this.ExtractAll(packageFileName);
        }

        private static Manifest ExtractManifest(String fileName)
        {
            try
            {
                using (ZipInputStream stream = new ZipInputStream(File.OpenRead(fileName)))
                {
                    ZipEntry theEntry;
                    while ((theEntry = stream.GetNextEntry()) != null)
                    {
                        string entryFileName = Path.GetFileName(theEntry.Name);

                        if (entryFileName == Manifest.DefaultFileName)
                        {
                            using (MemoryStream streamWriter = new MemoryStream())
                            {
                                int size = 2048;
                                byte[] data = new byte[2048];
                                while (true)
                                {
                                    size = stream.Read(data, 0, data.Length);
                                    if (size > 0)
                                    {
                                        streamWriter.Write(data, 0, size);
                                    }
                                    else
                                    {
                                        break;
                                    }
                                }

                                streamWriter.Position = 0;
                                return Persistable<Manifest>.Load(streamWriter);
                            }
                        }
                    }

                    return null;
                }
            }
            catch (ZipException)
            {
                return null;
            }
            catch (Exception)
            {
                return null;
            }
        }

        private bool ExtractAll(String packageFileName)
        {
            if (!File.Exists(packageFileName))
            {
                return false;
            }

            try
            {
                using (ZipInputStream s = new ZipInputStream(File.OpenRead(packageFileName)))
                {
                    ZipEntry theEntry;
                    while ((theEntry = s.GetNextEntry()) != null)
                    {
                        if (Path.IsPathRooted(theEntry.Name))
                        {
                            Console.WriteLine("Skipping file with absolute path: " + theEntry.Name);
                            continue;
                        }

                        string entryFileName = Path.Combine(this.mApplicationPath, theEntry.Name);
                        string directoryName = Path.GetDirectoryName(entryFileName);

                        // create directory
                        if (!Directory.Exists(directoryName))
                        {
                            Directory.CreateDirectory(directoryName);
                        }

                        try
                        {
                            using (FileStream streamWriter = File.Create(entryFileName))
                            {
                                int size = 16048;
                                byte[] data = new byte[size];
                                while (true)
                                {
                                    size = s.Read(data, 0, data.Length);
                                    if (size > 0)
                                    {
                                        streamWriter.Write(data, 0, size);
                                    }
                                    else
                                    {
                                        break;
                                    }
                                }
                            }

                        }
                        catch (IOException)
                        {
                            Trace.TraceError("Failed to extract file {0}", entryFileName);
                            //    return false;
                        }
                        catch (UnauthorizedAccessException)
                        {
                            Trace.TraceError("Failed to extract file {0}", entryFileName);
                            //    return false;
                        }
                    }
                }
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }
    }
}
