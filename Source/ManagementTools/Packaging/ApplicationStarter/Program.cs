using System;
using System.IO;
using System.Configuration;

namespace Starter
{
    class Program
    {
        static void Main(string[] args)
        {
            string executable = "ProcessMonitor.exe";            
            string executablePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, executable);

            AppDomainSetup newDomainSetupInfo = new AppDomainSetup();

            newDomainSetupInfo.ShadowCopyFiles = "true";
            newDomainSetupInfo.ApplicationBase = Path.GetDirectoryName(executablePath);

            AppDomain newAppDomain = AppDomain.CreateDomain(executablePath, AppDomain.CurrentDomain.Evidence, newDomainSetupInfo);

            Environment.Exit(newAppDomain.ExecuteAssembly(executablePath, AppDomain.CurrentDomain.Evidence, args));
        }
    }
}
