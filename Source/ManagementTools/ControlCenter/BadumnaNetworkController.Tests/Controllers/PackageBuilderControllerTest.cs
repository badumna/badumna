﻿//-----------------------------------------------------------------------
// <copyright file="PackageBuilderControllerTest.cs" company="National ICT Australia Ltd">
//     Copyright (c) 2012 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BadumnaNetworkController.Tests.Controllers
{
    using System;
    using System.IO;
    using System.Web.Mvc;
    using System.Web.SessionState;
    using BadumnaNetworkController.Controllers;
    using Germinator;
    using Germinator.PrestoManifesto;
    using MvcFakes;
    using NUnit.Framework;

    [TestFixture]
    public class PackageBuilderControllerTest
    {
        private SessionStateItemCollection sessionItems;

        [SetUp]
        public void SetUp()
        {
            // Copy the UnitTest.xml configuration.
            File.Copy("UnitTest.xml", Path.Combine(SetUpClass.BaseDirectory, @"App_Data\Configurations\UnitTest.xml"), true);

            // Initialize the germinator facade.
            XmlConfigurationProvider configurationProvider = new XmlConfigurationProvider(Path.Combine(SetUpClass.BaseDirectory, @"App_Data\Configurations"));
            GerminatorFacade.Initialize(configurationProvider, null, null);

            this.sessionItems = new SessionStateItemCollection();
            this.sessionItems[HomeController.ConfigurationSessionKey] = GerminatorFacade.Instance.GetConfiguration("UnitTest");
            Assert.IsNotNull(this.sessionItems[HomeController.ConfigurationSessionKey]);
        }

        [TearDown]
        public void TearDown()
        {
            GerminatorFacade.Instance = null;
            File.Delete(Path.Combine(SetUpClass.BaseDirectory, @"App_Data\Configurations\UnitTest.xml"));
        }

        [Test]
        public void AddCustomServiceWithInvalidNameShouldReturnErrorMessage()
        {
            PackageBuilderController controller = new PackageBuilderController();
            controller.ControllerContext = new FakeControllerContext(controller, this.sessionItems);

            string serviceName = "*&test";

            ActionResult result = controller.AddService(serviceName, ServiceType.ArbitrationService.ToString());

            this.ValidateModelState("name", 1, Resources.InvalidName, controller);
        }

        private void ValidateModelState(string key, int count, string errorMesage, Controller controller)
        {
            ModelState modelState;

            Assert.That(controller.ModelState.TryGetValue(key, out modelState));
            Assert.AreEqual(count, modelState.Errors.Count);
            Assert.AreEqual(errorMesage, modelState.Errors[0].ErrorMessage);
        }
    }
}
