﻿//-----------------------------------------------------------------------
// <copyright file="GermControllerTest.cs" company="National ICT Australia Ltd">
//     Copyright (c) 2012 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BadumnaNetworkController.Tests.Controllers
{
    using System;
    using System.IO;
    using System.Web.SessionState;
    using BadumnaNetworkController.Controllers;
    using Germinator;
    using MvcFakes;
    using NUnit.Framework;
    using System.Web.Mvc;

    [TestFixture]
    public class GermControllerTest
    {
        private SessionStateItemCollection sessionItems;

        [SetUp]
        public void SetUp()
        {
            // Copy the UnitTest.xml configuration.
            File.Copy("UnitTest.xml", Path.Combine(SetUpClass.BaseDirectory, @"App_Data\Configurations\UnitTest.xml"), true);

            // Initialize the germinator facade.
            XmlConfigurationProvider configurationProvider = new XmlConfigurationProvider(Path.Combine(SetUpClass.BaseDirectory, @"App_Data\Configurations"));
            GerminatorFacade.Initialize(configurationProvider, null, null);

            this.sessionItems = new SessionStateItemCollection();
            this.sessionItems[HomeController.ConfigurationSessionKey] = GerminatorFacade.Instance.GetConfiguration("UnitTest");
            Assert.IsNotNull(this.sessionItems[HomeController.ConfigurationSessionKey]);
        }

        [TearDown]
        public void TearDown()
        {
            GerminatorFacade.Instance = null;
            File.Delete(Path.Combine(SetUpClass.BaseDirectory, @"App_Data\Configurations\UnitTest.xml"));
        }

        [Test]
        public void ConstructingGermControllerWithNoError()
        {
            GermController controller = new GermController();
            Assert.IsNotNull(controller);
        }

        [Test]
        public void DetailsActionRedirectToHomeWhenHostNameIsInvalid()
        {
            GermController controller = new GermController();
            string hostName = null;

            ActionResult result = controller.Details(hostName);
            this.AssertRedirectToHome(result);
        }

        [Test]
        public void DetailsActionRedirectToHomeWhenConfigurationIsNull()
        {
            GermController controller = new GermController();
            controller.ControllerContext = new FakeControllerContext(controller, new SessionStateItemCollection());

            string hostName = "localhost:21253";

            ActionResult result = controller.Details(hostName);
            this.AssertRedirectToHome(result);
        }

        [Test]
        public void DetailsActionReturnsErrorMessageWhenHostNameIsNotAvailable()
        {
            GermController controller = new GermController();
            controller.ControllerContext = new FakeControllerContext(controller, this.sessionItems);

            string hostName = "localhost:21253";

            ActionResult result = controller.Details(hostName);

            ModelState modelState;
            Assert.That(controller.ModelState.TryGetValue("Name", out modelState));
            Assert.That(modelState.Errors.Count == 1);
            Assert.That(String.Format(Resources.NothingKnownAboutFormat, hostName).Equals(modelState.Errors[0].ErrorMessage));
        }

        [Test]
        public void DetailsActionReturnsDetailsViewWhenHostnameIsValid()
        {
            GermController controller = new GermController();
            controller.ControllerContext = new FakeControllerContext(controller, this.sessionItems);

            string hostName = "localhost:21253";

            (this.sessionItems[HomeController.ConfigurationSessionKey] as Configuration).AddGerm(hostName);

            ActionResult result = controller.Details(hostName);

            Assert.That(result as ViewResult != null);
            Assert.That((result as ViewResult).Model != null);
            Assert.That(hostName.Equals(((GermClient)(result as ViewResult).Model).Name));
        }

        [Test]
        public void ProcessesActionRedirectToHomeWhenHostnameIsInvalid()
        {
            GermController controller = new GermController();
            controller.ControllerContext = new FakeControllerContext(controller, this.sessionItems);

            string hostName = null;

            ActionResult result = controller.Processes(hostName);
            this.AssertRedirectToHome(result);
        }

        [Test]
        public void ProcessesActionReturnsErrorMessageWhenGermClientIsNotConnected()
        {
            // Note: at the moment we cannot inject the germ client instance,
            // i.e. can not test the RunningProcesses method in unit test.

            GermController controller = new GermController();
            controller.ControllerContext = new FakeControllerContext(controller, this.sessionItems);

            string hostName = "localhost:21253";

            ActionResult result = controller.Processes(hostName);

            Assert.That(result as ContentResult != null);
            Assert.That("Not connected".Equals((result as ContentResult).Content));
        }

        [Test]
        public void IsConnectedActionReturnErrorMessageWhenHostnameIsInvalid()
        {
            GermController controller = new GermController();
            string hostname = null;

            ActionResult result = controller.IsConnected(hostname, 0);

            Assert.That(result as ContentResult != null);
            Assert.That("0:Invalid Germ name".Equals((result as ContentResult).Content));
        }

        [Test]
        public void IsConnectedActionReturnCorrectMessageWhenHostnameIsValid()
        {
            GermController controller = new GermController();
            string hostname = "localhost:21253";

            ActionResult result = controller.IsConnected(hostname, 0);

            Assert.That(result as ContentResult != null);
            Assert.That("0:Germ offline".Equals((result as ContentResult).Content));
        }

        [Test]
        public void GetMachineStatusActionReturnsErrorMessageWhenHostnameIsInvalid()
        {
            GermController controller = new GermController();
            string hostname = null;

            ActionResult result = controller.GetMachineStatus(hostname, 1);

            Assert.That(result as ContentResult != null);
            Assert.That("Error:Invalid Germ name".Equals((result as ContentResult).Content));
        }

        [Test]
        public void GetMachineStatusActionReturnCorrectMessageWhenHostnameIsValid()
        {
            GermController controller = new GermController();
            controller.ControllerContext = new FakeControllerContext(controller, this.sessionItems);

            string hostname = "localhost:21253";

            ActionResult result = controller.GetMachineStatus(hostname, 1);

            Assert.That(result as ContentResult != null);
            Assert.That("Germ offline:0:N/A:N/A|".Equals((result as ContentResult).Content));
        }

        [Test]
        public void MachineStatusDetailsActionRedirectToNetworkIndexViewWhenHostnameIsInvalid()
        {
            GermController controller = new GermController();
            controller.ControllerContext = new FakeControllerContext(controller, this.sessionItems);

            string hostname = null;

            ActionResult result = controller.MachineStatusDetails(hostname);

            Assert.That(result as RedirectToRouteResult != null);
            Assert.That("Network".Equals((result as RedirectToRouteResult).RouteValues["controller"]));
            Assert.That("Index".Equals((result as RedirectToRouteResult).RouteValues["action"]));
        }

        [Test]
        public void MachineStatusDetailsActionReturnsCorrectMessageWhenHostnameIsValid()
        {
            GermController controller = new GermController();
            controller.ControllerContext = new FakeControllerContext(controller, this.sessionItems);

            string hostname = "localhost:21253";

            ActionResult result = controller.MachineStatusDetails(hostname);

            Assert.That(result as ContentResult != null);
            Assert.That("Machine status is not available".Equals((result as ContentResult).Content));
        }

        [Test]
        public void CreateActionShouldReturnCreateViewWithNoError()
        {
            GermController controller = new GermController();
            ActionResult result = controller.Create();

            Assert.IsNotNull(result);
        }

        [Test]
        public void CreatePostActionReturnsErrorMessageWhenHostnameIsInvalid()
        {
            GermController controller = new GermController();
            string hostName = null;

            ActionResult result = controller.Create(hostName, new Nullable<int>(-100), new Nullable<bool>(false));

            ModelState modelState;

            Assert.That(controller.ModelState.TryGetValue("Host", out modelState));
            Assert.AreEqual(2, modelState.Errors.Count);
            Assert.That(Resources.MustGiveHostName.Equals(modelState.Errors[0].ErrorMessage));
            Assert.That(controller.ModelState.TryGetValue("port", out modelState));
            Assert.AreEqual(1, modelState.Errors.Count);
            Assert.That(Resources.InvalidPort.Equals(modelState.Errors[0].ErrorMessage));
        }

        [Test]
        public void CreatePostActionRedirectToHomeWhenConfigurationIsNull()
        {
            GermController controller = new GermController();
            controller.ControllerContext = new FakeControllerContext(controller, new SessionStateItemCollection());
            
            string hostName = "localhost";

            ActionResult result = controller.Create(hostName, new Nullable<int>(21253), new Nullable<bool>(false));
            this.AssertRedirectToHome(result);
        }
        
        [Test]
        public void CreatePostActionSucceedToAddGermWithValidArguments()
        {
            GermController controller = new GermController();
            controller.ControllerContext = new FakeControllerContext(controller, this.sessionItems);

            string hostName = "localhost";

            ActionResult result = controller.Create(hostName, new Nullable<int>(21253), new Nullable<bool>(false));
            
            Assert.That(result as RedirectToRouteResult != null);
            Assert.That("Network".Equals((result as RedirectToRouteResult).RouteValues["controller"]));
            Assert.That("Index".Equals((result as RedirectToRouteResult).RouteValues["action"]));
            Assert.That((this.sessionItems[HomeController.ConfigurationSessionKey] as Configuration).GetGermClient(string.Format("{0}:21253", hostName)) != null);
        }

        private void AssertRedirectToHome(ActionResult result)
        {
            Assert.That(result as RedirectToRouteResult != null);
            Assert.That("Home".Equals((result as RedirectToRouteResult).RouteValues["controller"]));
            Assert.That("Index".Equals((result as RedirectToRouteResult).RouteValues["action"]));
        }
    }
}
