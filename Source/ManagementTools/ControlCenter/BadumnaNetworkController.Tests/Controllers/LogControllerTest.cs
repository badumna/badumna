﻿//-----------------------------------------------------------------------
// <copyright file="LogControllerTest.cs" company="National ICT Australia Ltd">
//     Copyright (c) 2012 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BadumnaNetworkController.Tests.Controllers
{
    using System;
    using NUnit.Framework;
    using BadumnaNetworkController.Controllers;
    using System.IO;
    using Germinator;
    using System.Web.Mvc;

    [TestFixture]
    public class LogControllerTest
    {
        [SetUp]
        public void SetUp()
        {
            // Copy the UnitTest.xml configuration.
            File.Copy("UnitTest.xml", Path.Combine(SetUpClass.BaseDirectory, @"App_Data\Configurations\UnitTest.xml"), true);

            // Initialize the germinator facade.
            XmlConfigurationProvider configurationProvider = new XmlConfigurationProvider(Path.Combine(SetUpClass.BaseDirectory, @"App_Data\Configurations"));
            GerminatorFacade.Initialize(configurationProvider, null, null);
        }

        [TearDown]
        public void TearDown()
        {
            GerminatorFacade.Instance = null;
            File.Delete(Path.Combine(SetUpClass.BaseDirectory, @"App_Data\Configurations\UnitTest.xml"));
        }

        [Test]
        public void ConstructingLogControllerWithNoError()
        {
            LogController controller = new LogController();
            Assert.IsNotNull(controller);
        }

        [Test]
        public void IndexActionReturnsCorrectView()
        {
            LogController controller = new LogController();
            ActionResult result = controller.Index();

            Assert.That(result as ViewResult != null);
            Assert.That((result as ViewResult).Model != null);
        }
    }
}
