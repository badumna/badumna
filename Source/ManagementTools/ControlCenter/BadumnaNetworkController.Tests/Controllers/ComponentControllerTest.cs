﻿//-----------------------------------------------------------------------
// <copyright file="ComponentControllerTest.cs" company="National ICT Australia Ltd">
//     Copyright (c) 2012 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BadumnaNetworkController.Tests.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Web.Mvc;
    using System.Web.SessionState;
    using BadumnaNetworkController.Controllers;
    using Germinator;
    using MvcFakes;
    using NUnit.Framework;

    [TestFixture]
    public class ComponentControllerTest
    {
        private SessionStateItemCollection sessionItems;

        [SetUp]
        public void SetUp()
        {
            // Copy the UnitTest.xml configuration.
            File.Copy("UnitTest.xml", Path.Combine(SetUpClass.BaseDirectory, @"App_Data\Configurations\UnitTest.xml"), true);

            // Initialize the germinator facade.
            XmlConfigurationProvider configurationProvider = new XmlConfigurationProvider(Path.Combine(SetUpClass.BaseDirectory, @"App_Data\Configurations"));
            GerminatorFacade.Initialize(configurationProvider, null, null);

            this.sessionItems = new SessionStateItemCollection();
            this.sessionItems[HomeController.ConfigurationSessionKey] = GerminatorFacade.Instance.GetConfiguration("UnitTest");
            Assert.IsNotNull(this.sessionItems[HomeController.ConfigurationSessionKey]);
        }

        [TearDown]
        public void TearDown()
        {
            GerminatorFacade.Instance = null;
            File.Delete(Path.Combine(SetUpClass.BaseDirectory, @"App_Data\Configurations\UnitTest.xml"));
        }

        [Test]
        public void ConstructingComponentControllerWithNoError()
        {
            ComponentController controller = new ComponentController();
            Assert.IsNotNull(controller);
        }

        [Test]
        public void FailedToGetGermListWhenConfigurationIsNull()
        {
            ComponentController controller = new ComponentController();
            controller.ControllerContext = new FakeControllerContext(controller, new SessionStateItemCollection());
            string serviceName = "SeedPeer";

            ActionResult actual = controller.GetGermList(serviceName);

            Assert.That(actual as ContentResult != null);
            Assert.That("Failed to get the germ hosts list".Equals((actual as ContentResult).Content));
        }
        
        [Test]
        public void GetEmptyGermListWhenNoGermsAdded()
        {
            ComponentController controller = new ComponentController();
            string serviceName = "SeedPeer";
            
            controller.ControllerContext = new FakeControllerContext(controller, this.sessionItems);

            ActionResult actual = controller.GetGermList(serviceName);
            
            Assert.That(actual as PartialViewResult != null);
            Assert.That("DropDownGermsList".Equals((actual as PartialViewResult).ViewName));

            object model = (actual as PartialViewResult).ViewData.Model;
            Assert.That(model != null);

            int counter = 0;
            foreach( var client in (model as IEnumerable<GermClient>))
            {
                counter ++;
            }

            Assert.That(counter == 0);
        }

        [Test]
        public void GetCorrectNumberOfGermInTheList()
        {
            ComponentController controller = new ComponentController();
            
            string serviceName = "SeedPeer";
            string hostName = "localhost:21253";            

            (this.sessionItems[HomeController.ConfigurationSessionKey] as Configuration).AddGerm(hostName);

            controller.ControllerContext = new FakeControllerContext(controller, this.sessionItems);
            ActionResult actual = controller.GetGermList(serviceName);

            Assert.That(actual as PartialViewResult != null);
            Assert.That("DropDownGermsList".Equals((actual as PartialViewResult).ViewName));

            object model = (actual as PartialViewResult).ViewData.Model;
            Assert.That(model != null);

            int counter = 0;
            foreach (var client in (model as IEnumerable<GermClient>))
            {
                Assert.AreEqual(hostName, client.Name);
                counter++;
            }

            Assert.That(counter == 1);
        }

        [Test]
        public void GetCorrectGermListViewWhenStartingDeiService()
        {
            ComponentController controller = new ComponentController();

            string serviceName = "Dei";
            string hostName = "localhost:21253";
            
            (this.sessionItems[HomeController.ConfigurationSessionKey] as Configuration).AddGerm(hostName);
            controller.ControllerContext = new FakeControllerContext(controller, this.sessionItems);

            ActionResult actual = controller.GetGermList(serviceName);

            Assert.That(actual as PartialViewResult != null);
            Assert.That("DropDownGermsList".Equals((actual as PartialViewResult).ViewName));
            Assert.That((actual as PartialViewResult).ViewBag.useSsl != null);

            object model = (actual as PartialViewResult).ViewData.Model;
            Assert.That(model != null);

            int counter = 0;
            foreach (var client in (model as IEnumerable<GermClient>))
            {
                Assert.AreEqual(hostName, client.Name);
                counter++;
            }

            Assert.That(counter == 1);
        }

        [Test]
        public void EditActionRedirectToHomeWhenConfigurationIsNull()
        {
            ComponentController controller = new ComponentController();
            controller.ControllerContext = new FakeControllerContext(controller, new SessionStateItemCollection());
            
            string serviceName = "SeedPeer";

            ActionResult actual = controller.Edit(serviceName);
            this.AssertRedirectToHome(actual);
        }

        [Test]
        public void EditActionRedirectToHomeWhenIndexIsNull()
        {
            ComponentController controller = new ComponentController();
            controller.ControllerContext = new FakeControllerContext(controller, this.sessionItems);

            string serviceName = "";

            ActionResult actual = controller.Edit(serviceName);
            this.AssertRedirectToHome(actual);
        }

        [Test]
        public void EditActionRedirectToHomeWhenNonExistingIndexIsUsed()
        {
            ComponentController controller = new ComponentController();
            controller.ControllerContext = new FakeControllerContext(controller, this.sessionItems);

            string serviceName = "non-existing-service";

            ActionResult actual = controller.Edit(serviceName);
            this.AssertRedirectToHome(actual);            
        }

        [Test]
        public void EditActionReturnCorrectViewWhenValidParameterIsPassed()
        {
            ComponentController controller = new ComponentController();
            controller.ControllerContext = new FakeControllerContext(controller, this.sessionItems);

            string serviceName = "SeedPeer";

            ActionResult actual = controller.Edit(serviceName);

            Assert.That(actual as ViewResult != null);
            Assert.That((actual as ViewResult).Model != null);
            Assert.That((actual as ViewResult).ViewBag.SettingChangeAllowed);
        }

        [Test]
        public void EditPostActionRedirectToHomeWhenConfigurationIsNull()
        {
            ComponentController controller = new ComponentController();
            controller.ControllerContext = new FakeControllerContext(controller, new SessionStateItemCollection());
            
            string serviceName = "SeedPeer";

            ActionResult actual = controller.Edit(serviceName, new Nullable<bool>(false), null);
            this.AssertRedirectToHome(actual);            
        }

        [Test]
        public void EditPostActionRedirectToHomeWhenIndexIsNull()
        {
            ComponentController controller = new ComponentController();
            controller.ControllerContext = new FakeControllerContext(controller, this.sessionItems);

            string serviceName = null;

            ActionResult actual = controller.Edit(serviceName, new Nullable<bool>(false), null);
            this.AssertRedirectToHome(actual);
        }

        [Test]
        public void EditPostActionRedirectToHomeWhenNonExistingIndexIsUsed()
        {
            ComponentController controller = new ComponentController();
            controller.ControllerContext = new FakeControllerContext(controller, this.sessionItems);

            string serviceName = "non-existing-service";

            ActionResult actual = controller.Edit(serviceName, new Nullable<bool>(false), null);
            this.AssertRedirectToHome(actual);
        }

        [Test]
        public void EditPostActionReturnErrorMessageWhenOneOfTheCollectionValueIsNull()
        {
            ComponentController controller = new ComponentController();
            controller.ControllerContext = new FakeControllerContext(controller, this.sessionItems);

            string serviceName = "SeedPeer";
            FormCollection collection = new FormCollection();
            collection.Add("ListeningPort", null);

            ActionResult actual = controller.Edit(serviceName, new Nullable<bool>(false), collection);

            this.ValidateModelState("ListeningPort", 1, Resources.Invalid, controller);
        }

        [Test]
        public void EditPostActionReturnErrorMessageWhenInvalidApplicationNameIsPassed()
        {
            ComponentController controller = new ComponentController();
            controller.ControllerContext = new FakeControllerContext(controller, this.sessionItems);

            string componentName = "ApplicationComponent";
            FormCollection collection = new FormCollection();
            collection.Add("ApplicationName", "&test");

            ActionResult actual = controller.Edit(componentName, new Nullable<bool>(false), collection);

            this.ValidateModelState("ApplicationName", 1, Resources.InvalidName, controller);
        }

        [Test]
        public void DefaultActionReturnErrorMessageWhenConfigurationIsNull()
        {
            ComponentController controller = new ComponentController();
            controller.ControllerContext = new FakeControllerContext(controller, new SessionStateItemCollection());

            string serviceName = "SeedPeer";

            ActionResult actual = controller.Default(serviceName);

            Assert.That(actual as ContentResult != null);
            Assert.That(string.Format("Failed to set {0} back to default value", serviceName).Equals((actual as ContentResult).Content));
        }

        [Test]
        public void DefaultActionShouldRedirectToEditWithValidArguments()
        {
            ComponentController controller = new ComponentController();
            controller.ControllerContext = new FakeControllerContext(controller, this.sessionItems);

            string serviceName = "SeedPeer";

            ActionResult actual = controller.Default(serviceName);

            Assert.That(actual as RedirectToRouteResult != null);
            Assert.That("Edit".Equals((actual as RedirectToRouteResult).RouteValues["action"]));
            Assert.That(serviceName.Equals((actual as RedirectToRouteResult).RouteValues["index"]));
        }

        private void AssertRedirectToHome(ActionResult result)
        {
            Assert.That(result as RedirectToRouteResult != null);
            Assert.That("Home".Equals((result as RedirectToRouteResult).RouteValues["controller"]));
            Assert.That("Index".Equals((result as RedirectToRouteResult).RouteValues["action"]));
        }

        private void ValidateModelState(string key, int count, string errorMesage, Controller controller)
        {
            ModelState modelState;

            Assert.That(controller.ModelState.TryGetValue(key, out modelState));
            Assert.AreEqual(count, modelState.Errors.Count);
            Assert.AreEqual(errorMesage, modelState.Errors[0].ErrorMessage);
        }
    }
}
