﻿//-----------------------------------------------------------------------
// <copyright file="HomeControllerTest.cs" company="National ICT Australia Ltd">
//     Copyright (c) 2012 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BadumnaNetworkController.Tests.Controllers
{
    using System;
    using System.Collections.Specialized;
    using System.IO;
    using System.Web.Mvc;
    using BadumnaNetworkController.Controllers;
    using Germinator;
    using MvcFakes;
    using NUnit.Framework;

    [TestFixture]
    public class HomeControllerTest
    {
        [SetUp]
        public void SetUp()
        {
            // Copy the UnitTest.xml configuration.
            File.Copy("UnitTest.xml", Path.Combine(SetUpClass.BaseDirectory, @"App_Data\Configurations\UnitTest.xml"), true);

            // Initialize the germinator facade.
            XmlConfigurationProvider configurationProvider = new XmlConfigurationProvider(Path.Combine(SetUpClass.BaseDirectory, @"App_Data\Configurations"));
            GerminatorFacade.Initialize(configurationProvider, null, null);
        }

        [TearDown]
        public void TearDown()
        {
            GerminatorFacade.Instance = null;
            File.Delete(Path.Combine(SetUpClass.BaseDirectory, @"App_Data\Configurations\UnitTest.xml"));
        }

        [Test]
        public void ConstructingHomeControllerWithNoError()
        {
            HomeController controller = new HomeController();
            Assert.IsNotNull(controller);
        }

        [Test]
        public void IndexReturnToNetworkDetailsPageIfGerminatorFacadeIsNotNull()
        {
            HomeController controller = new HomeController();
            controller.ControllerContext = new FakeControllerContext(controller, null, new NameValueCollection() { { "name", "localhost:21253" } });

            RedirectToRouteResult result = controller.Index() as RedirectToRouteResult;

            Assert.IsNotNull(result);
            Assert.That("Details".Equals(result.RouteValues["action"]));
            Assert.That("Network".Equals(result.RouteValues["controller"]));
            Assert.That("UnitTest".Equals(result.RouteValues["name"]));
        }

        [Test]
        public void GetCreateCertificateViewReturnCorrectView()
        {
            HomeController controller = new HomeController();

            ViewResult result = controller.CreateCertificate() as ViewResult;

            Assert.That(result, Is.Not.Null);
        }

        [Test]
        public void GetCertificateAuthenticationViewReturnCorrectView()
        {
            HomeController controller = new HomeController();
            string message = "test message.";
            ViewResult result = controller.CertificateAuthentication(message, string.Empty) as ViewResult;

            Assert.That(result, Is.Not.Null);
            Assert.That(message.Equals(result.ViewBag.Message));
            Assert.That(string.IsNullOrEmpty(result.ViewBag.ErrorMessage));
        }

        [Test]
        public void PostCertificateAuthenticationReturnAnErrorMessageWhenInvalidPassphraseIsGiven()
        {
            HomeController controller = new HomeController();
            ViewResult result = controller.CertificateAuthentication(string.Empty) as ViewResult;

            Assert.That(result, Is.Not.Null);
            Assert.That(string.IsNullOrEmpty(result.ViewBag.ErrorMessage));
            Assert.That(BadumnaNetworkController.Resources.PasswordInvalid.Equals(result.ViewData.ModelState["passphrase"].Errors[0].ErrorMessage));
        }

        [Test]
        public void PostCertificateAuthenticationRedirectToIndexWithValidPassphrase()
        {
            HomeController controller = new HomeController();

            RedirectToRouteResult result = controller.CertificateAuthentication("password") as RedirectToRouteResult;
            
            Assert.That("Index".Equals(result.RouteValues["action"]));
        }
    }
}
