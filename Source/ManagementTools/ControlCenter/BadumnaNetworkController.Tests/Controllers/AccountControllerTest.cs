﻿//-----------------------------------------------------------------------
// <copyright file="AccountControllerTest.cs" company="National ICT Australia Ltd">
//     Copyright (c) 2012 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BadumnaNetworkController.Tests.Controllers
{
    using System;
    using System.IO;
    using System.Web.Mvc;
    using System.Web.SessionState;
    using BadumnaNetworkController.Controllers;
    using Germinator;
    using MvcFakes;
    using NUnit.Framework;

    [TestFixture]
    public class AccountControllerTest
    {
        [SetUp]
        public void SetUp()
        {
            // Copy the UnitTest.xml configuration.
            File.Copy("UnitTest.xml", Path.Combine(SetUpClass.BaseDirectory, @"App_Data\Configurations\UnitTest.xml"), true);

            // Initialize the germinator facade.
            XmlConfigurationProvider configurationProvider = new XmlConfigurationProvider(Path.Combine(SetUpClass.BaseDirectory, @"App_Data\Configurations"));
            GerminatorFacade.Initialize(configurationProvider, null, null);
        }

        [TearDown]
        public void TearDown()
        {
            GerminatorFacade.Instance = null;
            File.Delete(Path.Combine(SetUpClass.BaseDirectory, @"App_Data\Configurations\UnitTest.xml"));
        }

        [Test]
        public void RegisterNewUserWithInvalidUsernameReturnErrorMessage()
        {
            AccountController controller = new AccountController();

            ActionResult result = controller.Register("&test*", "test@scalify.com", "password", "password", "securityQuestion", "securityAnswer");
            this.ValidateModelState("username", 1, new string[] { Resources.InvalidName }, controller);

            Assert.That(controller.ModelState.IsValidField("email"));
        }

        [Test]
        public void RegisterNewUserWithInvalidEmailReturnErrorMessage()
        {
            AccountController controller = new AccountController();

            ActionResult result = controller.Register("username", "test", "password", "password", "securityQuestion", "securityAnswer");
            
            this.ValidateModelState("email", 1, new string[] { Resources.InvalidEmail }, controller);
        }

        private void ValidateModelState(string key, int count, string[] errorMesages, Controller controller)
        {
            ModelState modelState;

            Assert.That(controller.ModelState.TryGetValue(key, out modelState));
            Assert.AreEqual(count, modelState.Errors.Count);

            for (int i = 0; i < count; i++)
            {
                Assert.AreEqual(errorMesages[i], modelState.Errors[i].ErrorMessage);
            }
        }
    }
}
