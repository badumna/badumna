﻿//-----------------------------------------------------------------------
// <copyright file="NetworkControllerTest.cs" company="National ICT Australia Ltd">
//     Copyright (c) 2012 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BadumnaNetworkController.Tests.Controllers
{
    using System;
    using System.IO;
    using System.Web.Mvc;
    using System.Web.SessionState;
    using BadumnaNetworkController.Controllers;
    using Germinator;
    using MvcFakes;
    using NUnit.Framework;

    [TestFixture]
    public class NetworkControllerTest
    {
        private SessionStateItemCollection sessionItems;

        [SetUp]
        public void SetUp()
        {
            // Copy the UnitTest.xml configuration.
            File.Copy("UnitTest.xml", Path.Combine(SetUpClass.BaseDirectory, @"App_Data\Configurations\UnitTest.xml"), true);

            // Initialize the germinator facade.
            XmlConfigurationProvider configurationProvider = new XmlConfigurationProvider(Path.Combine(SetUpClass.BaseDirectory, @"App_Data\Configurations"));
            GerminatorFacade.Initialize(configurationProvider, null, null);

            this.sessionItems = new SessionStateItemCollection();
            this.sessionItems[HomeController.ConfigurationSessionKey] = GerminatorFacade.Instance.GetConfiguration("UnitTest");
            Assert.IsNotNull(this.sessionItems[HomeController.ConfigurationSessionKey]);
        }

        [TearDown]
        public void TearDown()
        {
            GerminatorFacade.Instance = null;
            File.Delete(Path.Combine(SetUpClass.BaseDirectory, @"App_Data\Configurations\UnitTest.xml"));
        }

        [Test]
        public void ConstructingNetworkControllerWithNoError()
        {
            NetworkController controller = new NetworkController();
            Assert.IsNotNull(controller);
        }

        [Test]
        public void IndexActionRedirectToHomeWhenConfigurationNameIsNull()
        {
            NetworkController controller = new NetworkController();
            controller.ControllerContext = new FakeControllerContext(controller, new SessionStateItemCollection());

            ActionResult result = controller.Index();
            this.AssertRedirectToHome(result);
        }

        [Test]
        public void IndexActionRedirectToNetworkDetailsPageWhenConfigurationNameIsValid()
        {
            NetworkController controller = new NetworkController();
            string configurationName = "UnitTest";

            this.sessionItems[HomeController.ConfigurationNameSessionKey] = configurationName;
            controller.ControllerContext = new FakeControllerContext(controller, this.sessionItems);

            ActionResult result = controller.Index();
            Assert.That(result as RedirectToRouteResult != null);
            Assert.That("Network".Equals((result as RedirectToRouteResult).RouteValues["controller"]));
            Assert.That("Details".Equals((result as RedirectToRouteResult).RouteValues["action"]));
            Assert.That(configurationName.Equals((result as RedirectToRouteResult).RouteValues["name"]));
        }

        [Test]
        public void DetailsActionShouldRedirectToIndexWhenConfigurationNameIsInvalid()
        {
            NetworkController controller = new NetworkController();
            controller.ControllerContext = new FakeControllerContext(controller, this.sessionItems);

            string configurationName = null;

            ActionResult result = controller.Details(configurationName);

            Assert.That(result as RedirectToRouteResult != null);
            Assert.That("Index".Equals((result as RedirectToRouteResult).RouteValues["action"]));
        }

        [Test]
        public void DetailsActionShouldSetTheSessionKeyCorrectlyWhenConfigurationNameIsValid()
        {
            NetworkController controller = new NetworkController();
            SessionStateItemCollection collection = new SessionStateItemCollection();
            controller.ControllerContext = new FakeControllerContext(controller, collection);

            string configurationName = "UnitTest";

            ActionResult result = controller.Details(configurationName);

            Assert.That(result as ViewResult != null);
            Assert.That((result as ViewResult).Model != null);
            Assert.That((result as ViewResult).ViewBag.RunningServices != null);
            Assert.That(collection[HomeController.ConfigurationSessionKey] != null);
            Assert.AreEqual(configurationName, collection[HomeController.ConfigurationNameSessionKey]);
        }

        ////[Test]
        ////public void RemoveGermActionShouldSucceedRemoveTheGerm()
        ////{
        ////    NetworkController controller = new NetworkController();
        ////    controller.ControllerContext = new FakeControllerContext(controller, this.sessionItems);

        ////    string hostName = "localhost:21253";

        ////    (this.sessionItems[HomeController.ConfigurationSessionKey] as Configuration).AddGerm(hostName);

        ////    ActionResult result = controller.RemoveGerm(hostName);

        ////    Assert.That(result as PartialViewResult != null);
        ////}

        [Test]
        public void CreateActionShouldReturnCorrectView()
        {
            NetworkController controller = new NetworkController();
            ActionResult result = controller.Create();

            Assert.That(result as ViewResult != null);
        }

        [Test]
        public void CreatePostActionReturnErrorMessageWhenInvalidInputIsUsed()
        {
            NetworkController controller = new NetworkController();
            controller.ControllerContext = new FakeControllerContext(controller, this.sessionItems);

            ActionResult result = controller.Create(string.Empty, string.Empty);

            this.ValidateModelState("name", 1, new string[] { Resources.MustGiveName }, controller);
            this.ValidateModelState("protocolHash", 1, new string[] { Resources.MustGiveName }, controller);
        }

        [Test]
        public void CreatePostActionReturnErrorMessageWhenProtocolHashContainsWhiteSpace()
        {
            NetworkController controller = new NetworkController();
            controller.ControllerContext = new FakeControllerContext(controller, this.sessionItems);

            ActionResult result = controller.Create("NewUnitTest", "Unit Test App");

            this.ValidateModelState("protocolHash", 1, new string[] { Resources.InvalidName }, controller);            
        }

        [Test]
        public void CreatePostActionReturnErrorMessageWhenInvalidProtocolHashIsUsed()
        {
            NetworkController controller = new NetworkController();
            controller.ControllerContext = new FakeControllerContext(controller, this.sessionItems);

            ActionResult result = controller.Create("NewUnitTest", "*&App");

            this.ValidateModelState("protocolHash", 1, new string[] { Resources.InvalidName }, controller);
        }

        [Test]
        public void PrestartServiceActionReturnEmptyStringWhenConfigurationIsNull()
        {
            NetworkController controller = new NetworkController();
            controller.ControllerContext = new FakeControllerContext(controller, new SessionStateItemCollection());

            string serviceName = "SeedPeer";
            ActionResult result = controller.PrestartService(serviceName);
            
            Assert.That(result is ContentResult);
            Assert.That(string.IsNullOrEmpty((result as ContentResult).Content));
        }

        [Test]
        public void PrestartServiceActionReturnEmptyStringWhenStartingDeiButNoRunningServicesFound()
        {
            NetworkController controller = new NetworkController();
            controller.ControllerContext = new FakeControllerContext(controller, this.sessionItems);

            string serviceName = "Dei";
            ActionResult result = controller.PrestartService(serviceName);
            
            Assert.That(result is ContentResult);
            Assert.That(string.IsNullOrEmpty((result as ContentResult).Content));
        }

        [Test]
        public void FailedToStopServiceWhenServiceNameIsInvalid()
        {
            NetworkController controller = new NetworkController();
            controller.ControllerContext = new FakeControllerContext(controller, this.sessionItems);

            string serviceName = "non-existing-service";
            string hostName = "localhost:21253";

            ActionResult result = controller.StopService(serviceName, hostName, true);
            
            Assert.That(result is ContentResult);
            Assert.AreEqual("Failed", (result as ContentResult).Content);
            
        }

        [Test]
        public void StopServiceActionRedirectToHomeWhenServiceNameIsValid()
        {
            NetworkController controller = new NetworkController();
            controller.ControllerContext = new FakeControllerContext(controller, this.sessionItems);

            string serviceName = "SeedPeer";
            string hostName = "localhost:21253";

            ActionResult result = controller.StopService(serviceName, hostName, true);
            this.AssertRedirectToHome(result);
        }

        [Test]
        public void FailedToGetPingWhenServiceNameIsInvalid()
        {
            NetworkController controller = new NetworkController();
            controller.ControllerContext = new FakeControllerContext(controller, this.sessionItems);

            string serviceName = "non-existing-service";
            string hostName = "localhost:21253";

            ActionResult result = controller.Ping(serviceName, hostName);

            Assert.That(result is ContentResult);
            Assert.AreEqual("Failed", (result as ContentResult).Content);
        }

        [Test]
        public void PingActionShouldReturnsListRunninServicesPartialViewWhenServiceNameIsValid()
        {
            NetworkController controller = new NetworkController();
            controller.ControllerContext = new FakeControllerContext(controller, this.sessionItems);

            string serviceName = "SeedPeer";
            string hostName = "localhost:21253";

            ActionResult result = controller.Ping(serviceName, hostName);

            Assert.That(result is PartialViewResult);
            Assert.AreEqual("RunningServices", (result as PartialViewResult).ViewName);
        }

        [Test]
        public void FailedToRestartServiceWhenServiceNameIsInvalid()
        {
            NetworkController controller = new NetworkController();
            controller.ControllerContext = new FakeControllerContext(controller, this.sessionItems);

            string serviceName = "non-existing-service";
            string hostName = "localhost:21253";

            ActionResult result = controller.RestartService(serviceName, hostName, false);

            Assert.That(result is ContentResult);
            Assert.AreEqual("Failed", (result as ContentResult).Content);
        }

        [Test]
        public void RestartServiceRedirectToHomeWhenServiceNameIsValid()
        {
            NetworkController controller = new NetworkController();
            controller.ControllerContext = new FakeControllerContext(controller, this.sessionItems);

            string serviceName = "SeedPeer";
            string hostName = "localhost:21253";

            ActionResult result = controller.RestartService(serviceName, hostName, true);
            this.AssertRedirectToHome(result);
        }

        [Test]
        public void ProcessesActionShouldReturnListRunningServicesPartialView()
        {
            NetworkController controller = new NetworkController();
            controller.ControllerContext = new FakeControllerContext(controller, this.sessionItems);

            string hostName = "localhost:21253";

            ActionResult result = controller.Processes(hostName);

            Assert.That(result is PartialViewResult);
            Assert.AreEqual("RunningServices", (result as PartialViewResult).ViewName);
        }

        private void AssertRedirectToHome(ActionResult result)
        {
            Assert.That(result as RedirectToRouteResult != null);
            Assert.That("Home".Equals((result as RedirectToRouteResult).RouteValues["controller"]));
            Assert.That("Index".Equals((result as RedirectToRouteResult).RouteValues["action"]));
        }

        private void ValidateModelState(string key, int count, string[] errorMesages, Controller controller)
        {
            ModelState modelState;

            Assert.That(controller.ModelState.TryGetValue(key, out modelState));
            Assert.AreEqual(count, modelState.Errors.Count);

            for (int i = 0; i < count; i++)
            {
                Assert.AreEqual(errorMesages[i], modelState.Errors[i].ErrorMessage);
            }
        }
    }
}
