﻿//-----------------------------------------------------------------------
// <copyright file="NotificationControllerTest.cs" company="National ICT Australia Ltd">
//     Copyright (c) 2012 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BadumnaNetworkController.Tests.Controllers
{
    using System;
    using System.IO;
    using System.Web.Mvc;
    using System.Web.SessionState;
    using BadumnaNetworkController.Controllers;
    using Germinator;
    using Germinator.Notification;
    using MvcFakes;
    using NUnit.Framework;

    [TestFixture]
    public class NotificationControllerTest
    {
        private SessionStateItemCollection sessionItems;

        [SetUp]
        public void SetUp()
        {
            // Copy the UnitTest.xml configuration.
            File.Copy("UnitTest.xml", Path.Combine(SetUpClass.BaseDirectory, @"App_Data\Configurations\UnitTest.xml"), true);

            // Initialize the germinator facade.
            XmlConfigurationProvider configurationProvider = new XmlConfigurationProvider(Path.Combine(SetUpClass.BaseDirectory, @"App_Data\Configurations"));
            GerminatorFacade.Initialize(configurationProvider, null, null);

            this.sessionItems = new SessionStateItemCollection();
            this.sessionItems[HomeController.ConfigurationSessionKey] = GerminatorFacade.Instance.GetConfiguration("UnitTest");
            Assert.IsNotNull(this.sessionItems[HomeController.ConfigurationSessionKey]);
        }

        [TearDown]
        public void TearDown()
        {
            GerminatorFacade.Instance = null;
            File.Delete(Path.Combine(SetUpClass.BaseDirectory, @"App_Data\Configurations\UnitTest.xml"));
        }

        [Test]
        public void ConstructingNotificationControllerWithNoError()
        {
            NotificationController controller = new NotificationController();
            Assert.IsNotNull(controller);
        }

        [Test]
        public void IndexActionRedirectToHomeWhenConfigurationIsNull()
        {
            NotificationController controller = new NotificationController();
            controller.ControllerContext = new FakeControllerContext(controller, new SessionStateItemCollection());

            ActionResult result = controller.Index();
            this.AssertRedirectToHome(result);
        }

        [Test]
        public void IndexActionShouldReturnsIndexViewWhenConfigurationIsNotNull()
        {
            NotificationController controller = new NotificationController();
            controller.ControllerContext = new FakeControllerContext(controller, this.sessionItems);

            ActionResult result = controller.Index();

            Assert.That(result is ViewResult);
            Assert.AreEqual(0, (result as ViewResult).ViewBag.Count);
        }

        [Test]
        public void GetNotificationCountShouldReturnZeroWhenConfigurationIsNull()
        {
            NotificationController controller = new NotificationController();
            controller.ControllerContext = new FakeControllerContext(controller, new SessionStateItemCollection());

            ActionResult result = controller.GetNotificationsCount();

            Assert.That(result is ContentResult);
            Assert.AreEqual("0", (result as ContentResult).Content);
        }

        [Test]
        public void GetNotificationCountShouldGetTheValueCorrectlyWhenConfigurationIsNotNull()
        {
            NotificationController controller = new NotificationController();
            controller.ControllerContext = new FakeControllerContext(controller, this.sessionItems);

            ActionResult result = controller.GetNotificationsCount();

            Assert.That(result is ContentResult);
            // In this case, there is no notification as well.
            Assert.AreEqual("0", (result as ContentResult).Content);
        }

        [Test]
        public void SetupEmailNotificationActionRedirectToHomeWhenConfigurationIsNull()
        {
            NotificationController controller = new NotificationController();
            controller.ControllerContext = new FakeControllerContext(controller, new SessionStateItemCollection());

            ActionResult result = controller.SetupEmailNotification();
            this.AssertRedirectToHome(result);
        }

        [Test]
        public void SetupEmailNotificationReturnsCorrectViewWhenConfigurationIsNotNull()
        {
            NotificationController controller = new NotificationController();
            controller.ControllerContext = new FakeControllerContext(controller, this.sessionItems);

            ActionResult result = controller.SetupEmailNotification();

            Assert.That(result is ViewResult);
            Assert.That((result as ViewResult).Model != null);
        }

        [Test]
        public void SetupEmailNotificationPostActionRedirectToHomeWhenConfigurationIsNull()
        {
            NotificationController controller = new NotificationController();
            controller.ControllerContext = new FakeControllerContext(controller, new SessionStateItemCollection());

            ActionResult result = controller.SetupEmailNotification(this.GetEmailSetupFormCollection());
            this.AssertRedirectToHome(result);
        }

        [Test]
        public void SetupEmailNotificationPostActionReturnsErrorMessageWhenInvalidInputIsUsed()
        {
            NotificationController controller = new NotificationController();
            controller.ControllerContext = new FakeControllerContext(controller, this.sessionItems);

            FormCollection collection = this.GetEmailSetupFormCollection();
            collection["IsSend"] = null;
            collection["SmtpClient"] = null;

            ActionResult result = controller.SetupEmailNotification(collection);

            this.ValidateModelState("IsSend", 1, Resources.Invalid, controller);
            this.ValidateModelState("SmtpClient", 1, Resources.Invalid, controller);
        }

        [Test]
        public void SetupEmailNotificationPostActionRedirectToIndexOnSuccess()
        {
            NotificationController controller = new NotificationController();
            controller.ControllerContext = new FakeControllerContext(controller, this.sessionItems);

            ActionResult result = controller.SetupEmailNotification(this.GetEmailSetupFormCollection());
            Assert.That(result as RedirectToRouteResult != null);
            Assert.That("Index".Equals((result as RedirectToRouteResult).RouteValues["action"]));
        }

        [Test]
        public void ClearNotificationActionRedirectToHomeWhenConfigurationIsNull()
        {
            NotificationController controller = new NotificationController();
            controller.ControllerContext = new FakeControllerContext(controller, new SessionStateItemCollection());

            ActionResult result = controller.ClearNotification();
            this.AssertRedirectToHome(result);
        }

        [Test]
        public void ClearNotificationActionRedirectToNotificationIndexWhenConfigurationIsNotNull()
        {
            NotificationController controller = new NotificationController();
            controller.ControllerContext = new FakeControllerContext(controller, this.sessionItems);

            ActionResult result = controller.ClearNotification();
            Assert.That(result as RedirectToRouteResult != null);
            Assert.That("Index".Equals((result as RedirectToRouteResult).RouteValues["action"]));
        }

        [Test]
        public void DetailsActionRedirectToHomeWhenConfigurationIsNull()
        {
            NotificationController controller = new NotificationController();
            controller.ControllerContext = new FakeControllerContext(controller, new SessionStateItemCollection());

            ActionResult result = controller.Details(string.Empty, 0);
            this.AssertRedirectToHome(result);
        }

        [Test]
        public void DetailsActionRedirectToNotificationIndexWhenNotificationIsNotFound()
        {
            NotificationController controller = new NotificationController();
            controller.ControllerContext = new FakeControllerContext(controller, this.sessionItems);

            ActionResult result = controller.Details(string.Empty, 0);
            Assert.That(result as RedirectToRouteResult != null);
            Assert.That("Index".Equals((result as RedirectToRouteResult).RouteValues["action"]));
        }

        [Test]
        public void DetailsActionReturnNotificationDetailsIfNotificationExist()
        {
            NotificationController controller = new NotificationController();
            controller.ControllerContext = new FakeControllerContext(controller, this.sessionItems);

            int processId = 1;
            string time;

            // Inject one notification.
            (this.sessionItems[HomeController.ConfigurationSessionKey] as Configuration).AddNotification("SeedPeer", processId, "localhost:21253", TypeOfNotification.Error, string.Empty, PriorityLevel.High);
            time = (this.sessionItems[HomeController.ConfigurationSessionKey] as Configuration).GetNotifications()[0].Time;

            ActionResult result = controller.Details(time, processId);

            Assert.That(result is ViewResult);
            Assert.That((result as ViewResult).Model != null);
        }

        [Test]
        public void DeleteActionRedirectToHomeWhenConfigurationIsNull()
        {
            NotificationController controller = new NotificationController();
            controller.ControllerContext = new FakeControllerContext(controller, new SessionStateItemCollection());

            ActionResult result = controller.Delete(string.Empty, 0);
            this.AssertRedirectToHome(result);
        }

        [Test]
        public void DeleteActionShouldDeleteIfNotificationExist()
        {
            NotificationController controller = new NotificationController();
            controller.ControllerContext = new FakeControllerContext(controller, this.sessionItems);

            int processId = 1;
            string time;

            // Inject two notification.
            (this.sessionItems[HomeController.ConfigurationSessionKey] as Configuration).AddNotification("SeedPeer", processId, "localhost:21253", TypeOfNotification.Error, string.Empty, PriorityLevel.High);
            (this.sessionItems[HomeController.ConfigurationSessionKey] as Configuration).AddNotification("SeedPeer", 2, "localhost:21253", TypeOfNotification.Error, string.Empty, PriorityLevel.High);
            time = (this.sessionItems[HomeController.ConfigurationSessionKey] as Configuration).GetNotifications()[0].Time;

            ActionResult result = controller.Delete(time, processId);
            Assert.That(result as RedirectToRouteResult != null);
            Assert.That("Index".Equals((result as RedirectToRouteResult).RouteValues["action"]));

            // Check that the notification left should be 1.
            ActionResult notificationCount = controller.GetNotificationsCount();

            Assert.That(notificationCount is ContentResult);
            Assert.AreEqual("1", (notificationCount as ContentResult).Content);
        }

        private void AssertRedirectToHome(ActionResult result)
        {
            Assert.That(result as RedirectToRouteResult != null);
            Assert.That("Home".Equals((result as RedirectToRouteResult).RouteValues["controller"]));
            Assert.That("Index".Equals((result as RedirectToRouteResult).RouteValues["action"]));
        }

        private FormCollection GetEmailSetupFormCollection()
        {
            FormCollection collection = new FormCollection();
            collection.Add("IsSend", "true");
            collection.Add("SmtpClient", "domain.com");
            collection.Add("SmtpPort", "123");
            collection.Add("username", "test_user");
            collection.Add("password", "test_password");
            collection.Add("from", "admin@domain.com");
            collection.Add("to", "user@domain.com");

            return collection;
        }

        private void ValidateModelState(string key, int count, string errorMesage, Controller controller)
        {
            ModelState modelState;

            Assert.That(controller.ModelState.TryGetValue(key, out modelState));
            Assert.AreEqual(count, modelState.Errors.Count);
            Assert.AreEqual(errorMesage, modelState.Errors[0].ErrorMessage);
        }
    }
}
