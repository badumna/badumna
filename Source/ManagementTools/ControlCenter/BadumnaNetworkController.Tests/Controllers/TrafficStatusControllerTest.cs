﻿//-----------------------------------------------------------------------
// <copyright file="TrafficStatusControllerTest.cs" company="National ICT Australia Ltd">
//     Copyright (c) 2012 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BadumnaNetworkController.Tests.Controllers
{
    using System;
    using System.IO;
    using System.Web.Mvc;
    using System.Web.SessionState;
    using BadumnaNetworkController.Controllers;
    using Germinator;
    using MvcFakes;
    using NUnit.Framework;

    [TestFixture]
    public class TrafficStatusControllerTest
    {
        private SessionStateItemCollection sessionItems;

        [SetUp]
        public void SetUp()
        {
            // Copy the UnitTest.xml configuration.
            File.Copy("UnitTest.xml", Path.Combine(SetUpClass.BaseDirectory, @"App_Data\Configurations\UnitTest.xml"), true);

            // Initialize the germinator facade.
            XmlConfigurationProvider configurationProvider = new XmlConfigurationProvider(Path.Combine(SetUpClass.BaseDirectory, @"App_Data\Configurations"));
            GerminatorFacade.Initialize(configurationProvider, null, null);

            this.sessionItems = new SessionStateItemCollection();
            this.sessionItems[HomeController.ConfigurationSessionKey] = GerminatorFacade.Instance.GetConfiguration("UnitTest");
            Assert.IsNotNull(this.sessionItems[HomeController.ConfigurationSessionKey]);
        }

        [TearDown]
        public void TearDown()
        {
            GerminatorFacade.Instance = null;
            File.Delete(Path.Combine(SetUpClass.BaseDirectory, @"App_Data\Configurations\UnitTest.xml"));
        }

        [Test]
        public void ConstructingTrafficStatusControllerWithNoError()
        {
            TrafficStatusController controller = new TrafficStatusController();
            Assert.IsNotNull(controller);
        }

        [Test]
        public void FetchDataActionShouldReturnNotNullObject()
        {
            TrafficStatusController controller = new TrafficStatusController();
            controller.ControllerContext = new FakeControllerContext(controller, this.sessionItems);

            string serviceName = "SeedPeer";
            string hostName = "localhost:21253";

            ActionResult result = controller.FetchData(serviceName, hostName);
            Assert.IsNotNull(result);
            Assert.That(result is JsonResult);
        }
    }
}
