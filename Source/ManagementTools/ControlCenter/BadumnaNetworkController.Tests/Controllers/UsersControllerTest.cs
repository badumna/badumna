﻿//-----------------------------------------------------------------------
// <copyright file="UsersControllerTest.cs" company="National ICT Australia Ltd">
//     Copyright (c) 2012 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BadumnaNetworkController.Tests.Controllers
{
    using System;
    using System.IO;
    using System.Web.Mvc;
    using System.Web.SessionState;
    using BadumnaNetworkController.Controllers;
    using Germinator;
    using MvcFakes;
    using NUnit.Framework;

    [TestFixture]
    public class UsersControllerTest
    {
        [SetUp]
        public void SetUp()
        {
            // Copy the UnitTest.xml configuration.
            File.Copy("UnitTest.xml", Path.Combine(SetUpClass.BaseDirectory, @"App_Data\Configurations\UnitTest.xml"), true);

            // Initialize the germinator facade.
            XmlConfigurationProvider configurationProvider = new XmlConfigurationProvider(Path.Combine(SetUpClass.BaseDirectory, @"App_Data\Configurations"));
            GerminatorFacade.Initialize(configurationProvider, null, null);
        }

        [TearDown]
        public void TearDown()
        {
            GerminatorFacade.Instance = null;
            File.Delete(Path.Combine(SetUpClass.BaseDirectory, @"App_Data\Configurations\UnitTest.xml"));
        }

        [Test]
        public void CreateDeiAccountWithInvalidUsernameShouldReturnErrorMessage()
        {
            UsersController controller = new UsersController();
             
            FormCollection collection = new FormCollection();
            collection.Add("accountType", "0");
            
            ActionResult result = controller.Create("&test", "password", "password", collection);

            this.ValidateModelState("username", 1, new string[] { Resources.InvalidName }, controller);
        }

        [Test]
        public void CreateDeiAccountWithInvalidPasswordShouldReturnErrorMessage()
        {
            UsersController controller = new UsersController();

            FormCollection collection = new FormCollection();
            collection.Add("accountType", "0");

            ActionResult result = controller.Create("user", "", "password", collection);

            this.ValidateModelState("password", 1, new string[] { Resources.PasswordInvalid }, controller);
            this.ValidateModelState("all", 1, new string[] { Resources.PasswordMismatch }, controller);
        }

        private void ValidateModelState(string key, int count, string[] errorMesages, Controller controller)
        {
            ModelState modelState;

            Assert.That(controller.ModelState.TryGetValue(key, out modelState));
            Assert.AreEqual(count, modelState.Errors.Count);

            for (int i = 0; i < count; i++)
            {
                Assert.AreEqual(errorMesages[i], modelState.Errors[i].ErrorMessage);
            }
        }
    }
}
