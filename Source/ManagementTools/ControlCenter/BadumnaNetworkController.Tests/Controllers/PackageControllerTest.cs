﻿//-----------------------------------------------------------------------
// <copyright file="PackageControllerTest.cs" company="National ICT Australia Ltd">
//     Copyright (c) 2012 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BadumnaNetworkController.Tests.Controllers
{
    using System;
    using System.IO;
    using System.Web.Mvc;
    using BadumnaNetworkController.Controllers;
    using Germinator;
    using NUnit.Framework;

    [TestFixture]
    public class PackageControllerTest
    {
        [SetUp]
        public void SetUp()
        {
            // Copy the UnitTest.xml configuration.
            File.Copy("UnitTest.xml", Path.Combine(SetUpClass.BaseDirectory, @"App_Data\Configurations\UnitTest.xml"), true);

            // Initialize the germinator facade.
            XmlConfigurationProvider configurationProvider = new XmlConfigurationProvider(Path.Combine(SetUpClass.BaseDirectory, @"App_Data\Configurations"));
            GerminatorFacade.Initialize(configurationProvider, null, null);
        }

        [TearDown]
        public void TearDown()
        {
            GerminatorFacade.Instance = null;
            File.Delete(Path.Combine(SetUpClass.BaseDirectory, @"App_Data\Configurations\UnitTest.xml"));
        }

        [Test]
        public void ConstructingPackageControllerWithNoError()
        {
            PackageController controller = new PackageController();
            Assert.IsNotNull(controller);
        }

        [Test]
        public void DetailsActionRedirectToHomeWhenPackageNameIsInvalid()
        {
            PackageController controller = new PackageController();

            ActionResult result = controller.Details(string.Empty, "SeedPeer");
            Assert.That(result as RedirectToRouteResult != null);
            Assert.That("Network".Equals((result as RedirectToRouteResult).RouteValues["controller"]));
            Assert.That("Index".Equals((result as RedirectToRouteResult).RouteValues["action"]));
        }

        [Test]
        public void DetailsActionReturnDetailsViewWhenPackageNameIsValid()
        {
            PackageController controller = new PackageController();
            string serviceName = "SeedPeer";

            ActionResult result = controller.Details(serviceName, serviceName);

            Assert.That(result is ViewResult);
            Assert.AreEqual(serviceName, (result as ViewResult).ViewBag.index);
            Assert.AreEqual(serviceName, (result as ViewResult).ViewBag.DisplayName);
        }
    }
}
