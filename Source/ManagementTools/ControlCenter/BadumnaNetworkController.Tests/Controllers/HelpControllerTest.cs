﻿//-----------------------------------------------------------------------
// <copyright file="HelpControllerTest.cs" company="National ICT Australia Ltd">
//     Copyright (c) 2012 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BadumnaNetworkController.Tests.Controllers
{
    using System;
    using System.IO;
    using System.Web.Mvc;
    using System.Web.SessionState;
    using BadumnaNetworkController.Controllers;
    using BadumnaNetworkController.Models;
    using Germinator;
    using NUnit.Framework;

    [TestFixture]
    public class HelpControllerTest
    {
        private SessionStateItemCollection sessionItems;

        [SetUp]
        public void SetUp()
        {
            // Copy the UnitTest.xml configuration.
            File.Copy("UnitTest.xml", Path.Combine(SetUpClass.BaseDirectory, @"App_Data\Configurations\UnitTest.xml"), true);

            // Initialize the germinator facade.
            XmlConfigurationProvider configurationProvider = new XmlConfigurationProvider(Path.Combine(SetUpClass.BaseDirectory, @"App_Data\Configurations"));
            GerminatorFacade.Initialize(configurationProvider, null, null);

            this.sessionItems = new SessionStateItemCollection();
            this.sessionItems[HomeController.ConfigurationSessionKey] = GerminatorFacade.Instance.GetConfiguration("UnitTest");
            Assert.IsNotNull(this.sessionItems[HomeController.ConfigurationSessionKey]);

            HelpTopics.Instance = new HelpTopics(Path.Combine(SetUpClass.BaseDirectory, "App_Data"));
        }

        [TearDown]
        public void TearDown()
        {
            GerminatorFacade.Instance = null;
            HelpTopics.Instance = null;
            File.Delete(Path.Combine(SetUpClass.BaseDirectory, @"App_Data\Configurations\UnitTest.xml"));
        }

        [Test]
        public void ConstructingHelpControllerWithNoError()
        {
            HelpController controller = new HelpController();
            Assert.IsNotNull(controller);
        }

        [Test]
        public void IndexActionShouldReturnsCorrectView()
        {
            HelpController controller = new HelpController();

            ActionResult result = controller.Index();

            Assert.That(result as ViewResult != null);
            Assert.That((result as ViewResult).Model != null);
        }

        [Test]
        public void GetHelpItemActionReturnsCorrectHelpTopic()
        {
            HelpController controller = new HelpController();

            Assert.IsNotNull(HelpTopics.Instance);
            
            foreach(string topicName in HelpTopics.Instance.Topics)
            {
                foreach (HelpItem item in HelpTopics.Instance.GetItems(topicName))
                {
                    string itemName = item.Name;

                    ActionResult result = controller.GetHelpItem(topicName, itemName);

                    Assert.That(result as PartialViewResult != null);
                    Assert.That((result as PartialViewResult).Model != null);
                    Assert.AreEqual("HelpItem", (result as PartialViewResult).ViewName);
                    Assert.AreEqual(topicName, (result as PartialViewResult).ViewBag.Topic);
                    Assert.AreEqual(itemName, (result as PartialViewResult).ViewBag.ItemName);
                }
            }
        }

        [Test]
        public void AddItemActionShouldReturnCorrectView()
        {
            HelpController controller = new HelpController();

            Assert.IsNotNull(HelpTopics.Instance);

            foreach (string topicName in HelpTopics.Instance.Topics)
            {
                foreach (HelpItem item in HelpTopics.Instance.GetItems(topicName))
                {
                    string itemName = item.Name;

                    ActionResult result = controller.AddItem(topicName, itemName);

                    Assert.That(result as ViewResult != null);
                    Assert.AreEqual(topicName, (result as ViewResult).ViewBag.Topic);
                    Assert.AreEqual(itemName, (result as ViewResult).ViewBag.Item);
                    Assert.AreEqual(item.Title, (result as ViewResult).ViewBag.HelpTitle);
                    Assert.AreEqual(item.Text, (result as ViewResult).ViewBag.Text);
                }
            }
        }

        [Test]
        public void AddItemPostActionReturnsCorrectErrorMessageWhenInvalidInputIsUsed()
        {
            HelpController controller = new HelpController();
            ActionResult result = controller.AddItem(string.Empty, string.Empty, string.Empty, string.Empty);

            this.ValidateModelState("Topic", 1, "Must include a valid topic", controller);
            this.ValidateModelState("Item", 1, "Must include a valid item name", controller);
            this.ValidateModelState("Title", 1, "Must include a valid title", controller);
            this.ValidateModelState("Text", 1, "Must include help text", controller);
        }

        [Test]
        public void AddTestReturnCorrectView()
        {
            HelpController controller = new HelpController();
            ActionResult result = controller.Add();

            Assert.That(result as ViewResult != null);
            Assert.That((result as ViewResult).ViewBag.TopicList != null);
        }

        private void ValidateModelState(string key, int count, string errorMesage, Controller controller)
        {
            ModelState modelState;

            Assert.That(controller.ModelState.TryGetValue(key, out modelState));
            Assert.AreEqual(count, modelState.Errors.Count);
            Assert.AreEqual(errorMesage, modelState.Errors[0].ErrorMessage);
        }
    }
}
