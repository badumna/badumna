﻿//-----------------------------------------------------------------------
// <copyright file="SetUpClass.cs" company="National ICT Australia Ltd">
//     Copyright (c) 2012 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BadumnaNetworkController.Tests
{
    using System;
    using System.IO;
    using NUnit.Framework;
    using X509CertificateCreator;

    [SetUpFixture]
    public class SetUpClass
    {
        public static string BaseDirectory = @"..\..\..\BadumnaNetworkController";

        [SetUp]
        public void RunBeforeAnyTests()
        {
            string certificateDirectory = Path.Combine(BaseDirectory, "Certificate");
            string passphrase = "password";
            string certificateFilename = "certificate.pfx";

            if (!File.Exists(Path.Combine(certificateDirectory, certificateFilename)))
            {
                string currentDirectory = Directory.GetCurrentDirectory();
                Directory.SetCurrentDirectory(certificateDirectory);
                new CertificateCreator().CreateCertificate(System.Net.Dns.GetHostName(), DateTime.Now + TimeSpan.FromDays(365), certificateFilename, passphrase);
                Directory.SetCurrentDirectory(currentDirectory);
            }
        }
    }
}
