﻿//------------------------------------------------------------------------------
// <copyright file="BadumnaCommandLineOptions.cs" company="National ICT Australia Limited">
//     Copyright (c) 2012 National ICT Australia Limited. All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics.CodeAnalysis;
using Badumna;
using Badumna.Core;
using Mono.Options;

namespace GermHarness
{
    /// <summary>
    /// Command line option handler for options related to Badumna configuration.
    /// </summary>
    public class BadumnaCommandLineOptions
    {
        /// <summary>
        /// The application name to use for the Badumna network.
        /// </summary>
        private string appName;

        /// <summary>
        /// File to read configuration options from.
        /// </summary>
        private string badumnaConfigFile;

        /// <summary>
        /// File to save configuration options into.
        /// </summary>
        private string badumnaConfigSaveFile;

        /// <summary>
        /// Port number to override Badumna config options.
        /// </summary>
        private ushort? badumnaConfigPort;

        /// <summary>
        /// Seed peer address to override Badumna config options.
        /// </summary>
        private string badumnaConfigSeedPeer;

        /// <summary>
        /// Flag indicating whether to use this peer as a matchmaking server.
        /// </summary>
        private bool isMatchmakingServer;

        /// <summary>
        /// Initializes a new instance of the BadumnaCommandLineOptions class.
        /// </summary>
        /// <param name="optionSet">The option set to add Badumna command line options to.</param>
        [SuppressMessage("Microsoft.StyleCop.CSharp.ReadabilityRules", "SA1118:ParameterMustNotSpanMultipleLines", Justification = "Centered")]
        public BadumnaCommandLineOptions(OptionSet optionSet)
        {
            optionSet.Add(
                "application-name=",
                "the application name to give the Badumna network.",
                (string v) => this.appName = v);

            optionSet.Add(
                "badumna-config-file=",
                "the name of the file containing the Badumna configuration.",
                (string v) => this.badumnaConfigFile = v);

            optionSet.Add(
                "badumna-config-save=",
                "the name of a file to save the configuration into.  The saved configuration " +
                "matches the configuration actually used, including any commandline override options " +
                "specified.  If the specified file already exists it will be overwritten without warning.",
                (string v) => this.badumnaConfigSaveFile = v);

            optionSet.Add(
                "badumna-config-port=",
                "the port for Badumna to listen for incoming connections on. " +
                "Overrides any port configuration provided using the badumna-config-file option.",
                (string v) =>
                {
                    ushort port = 0;
                    if (ushort.TryParse(v, out port))
                    {
                        this.badumnaConfigPort = port;
                    }
                    else
                    {
                        throw new OptionException(
                            string.Format(
                                "badumna-config-port must be in the range {0}-{1}",
                                UInt16.MinValue,
                                UInt16.MaxValue),
                            "badumna-config-port");
                    }
                });

            optionSet.Add(
                "badumna-config-seedpeer=",
                "the address and port of the seedpeer to connect to (in format ADDRESS:PORT). " +
                "Added to any seedpeer configuration provided using the badumna-config-file option.",
                (string v) => this.badumnaConfigSeedPeer = v); // TODO: Validate address:port format.

            optionSet.Add(
                "mm|matchmaking-server",
                "use this peer as matchmaking server.",
                var => this.isMatchmakingServer = true);
        }

        /// <summary>
        /// Applies the Badumna-related command line options and returns the resulting <see cref="Options"/>
        /// instance.
        /// </summary>
        /// <param name="initialOptions">The initial options.  The options specified on the command line are applied over these 
        /// (unless the "badumna-config-file" option has been specified and <paramref name="allowConfigLoad"/> is true, in which
        /// case the initial options will be loaded from the specified file).</param>
        /// <param name="allowConfigLoad">Indicates whether the "badumna-config-file" option should be processed.</param>
        /// <returns>The final <see cref="Options"/>.  This may be the same instance as <paramref name="initialOptions"/>
        /// or may be a new instance.</returns>
        public Options ApplyOptions(Options initialOptions, bool allowConfigLoad)
        {
            Options options = initialOptions;

            if (allowConfigLoad && this.badumnaConfigFile != null)
            {
                try
                {
                    options = new Options(this.badumnaConfigFile);
                }
                catch (ConfigurationException ex)
                {
                    Console.WriteLine(
                        string.Format(
                            "Could not load configuration from file {0}. {1}",
                            this.badumnaConfigFile,
                            ex.Message));
                    Environment.Exit((int)ExitCode.ConfigurationError);
                }
            }

            if (this.badumnaConfigPort.HasValue)
            {
                options.Connectivity.ConfigureForSpecificPort(this.badumnaConfigPort.Value);
            }

            if (!string.IsNullOrEmpty(this.badumnaConfigSeedPeer))
            {
                options.Connectivity.SeedPeers.Insert(0, this.badumnaConfigSeedPeer);
            }

            if (!string.IsNullOrEmpty(this.appName))
            {
                options.Connectivity.ApplicationName = this.appName;
            }

            if (this.isMatchmakingServer)
            {
                options.Matchmaking.ActiveMatchLimit = 999;
            }

            return options;
        }

        /// <summary>
        /// Saves the Badumna config options to a file if "badumna-config-save" has been specified.  This should
        /// be called after the options have been finalised.
        /// </summary>
        /// <param name="finalOptions">Options to be saved</param>
        public void SaveIfRequired(Options finalOptions)
        {
            if (this.badumnaConfigSaveFile == null)
            {
                return;
            }

            try
            {
                finalOptions.Save(this.badumnaConfigSaveFile);
                Console.WriteLine("Options saved to '{0}'", this.badumnaConfigSaveFile);
            }
            catch (Exception e)
            {
                Console.WriteLine("Saving options failed: {0}", e.Message);
                Environment.Exit((int)ExitCode.ConfigurationError);
            }
        }
    }
}
