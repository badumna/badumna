﻿//-----------------------------------------------------------------------
// <copyright file="IDeiConfigManager.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2010 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace GermHarness
{
    using System;

    /// <summary>
    /// Interface for DeiConfigManager (to allow mocking)
    /// </summary>
    public interface IDeiConfigManager
    {
        /// <summary>
        /// Configure a PeerHarness with an authenticated IdentityProvider.
        /// </summary>
        /// <param name="harness">The PeerHarness to configure.</param>
        /// <param name="configFileName">The name of a config file, or empty if none.</param>
        /// <param name="configString">A configuration string, or empty if none.</param>
        /// <exception cref="DeiConfigException">Thrown when there is an error in the Dei config.</exception>
        void ConfigHarness(IPeerHarness harness, string configFileName, string configString);
    }
}
