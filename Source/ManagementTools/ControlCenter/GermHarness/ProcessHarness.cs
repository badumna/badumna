﻿//-----------------------------------------------------------------------
// <copyright file="ProcessHarness.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2010 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace GermHarness
{
    using System.IO;

    /// <summary>
    /// Harness for hosting a process under remote control.
    /// </summary>
    public sealed class ProcessHarness : HarnessBase, IProcessHarness
    {
        /// <summary>
        /// A flag indicating if the hosted process is active.
        /// </summary>
        private bool isActive;

        /// <summary>
        /// Initializes a new instance of the ProcessHarness class.
        /// </summary>
        public ProcessHarness()
        {
            this.ProcessNetworkStateIntervalMs = 25;
        }

        /// <summary>
        /// Called when the hosted process is started.
        /// </summary>
        /// <param name="config">The configuration to use.</param>
        /// <returns>True if successful, otherwise false.</returns>
        protected override bool OnStart(string config)
        {
            this.isActive = true;
            return true;
        }

        /// <summary>
        /// Called when the hosted process is stopped.
        /// </summary>
        protected override void OnStop()
        {
            this.isActive = false;
        }

        /// <summary>
        /// Do regular processing.
        /// </summary>
        protected override void OnProcessRegularEvents()
        {
        }

        /// <summary>
        /// Write current status for reporting to controller.
        /// </summary>
        /// <param name="stream">The stream to write to.</param>
        /// <returns>True if successful, otherwise false.</returns>
        protected override bool OnWriteStatus(Stream stream)
        {
            return true;
        }

        /// <summary>
        /// Report the ping state.
        /// </summary>
        /// <remarks>Ping state is active if the process has been started and not stopped.</remarks>
        /// <returns>The ping state.</returns>
        protected override PingState OnGetPing()
        {
            if (this.isActive)
            {
                return PingState.Active;
            }

            return PingState.None;
        }

        /// <summary>
        /// Check for updates.
        /// </summary>
        protected override void OnCheckForUpdates()
        {
        }
    }
}
