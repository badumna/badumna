﻿//-----------------------------------------------------------------------
// <copyright file="DeiConfigException.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2011 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;

namespace GermHarness
{
    /// <summary>
    /// An exception indicating an error encountered during Dei configuration.
    /// </summary>
    [global::System.Serializable]
    public class DeiConfigException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the DeiConfigException class.
        /// </summary>
        public DeiConfigException()
        {
        }

        /// <summary>
        /// Initializes a new instance of the DeiConfigException class with a
        /// specified message.
        /// </summary>
        /// <param name="message">A message describing the reason for the exception.</param>
        public DeiConfigException(string message) : base(message)
        {
        }

        /// <summary>
        /// Initializes a new instance of the DeiConfigException class with a
        /// specified error message and a reference to the inner exception that
        /// is the cause of this exception..
        /// </summary>
        /// <param name="message">The error message that explains the reason for
        /// the exception.</param>
        /// <param name="inner">The exception that is the cause of the current
        /// exception, or a null reference (Nothing in Visual Basic) if no inner
        /// exception is specified.</param>
        public DeiConfigException(string message, Exception inner) : base(message, inner)
        {
        }
        
        /// <summary>
        /// Initializes a new instance of the DeiConfigException class.
        /// </summary>
        /// <param name="info">The System.Runtime.Serialization.SerializationInfo that holds the
        /// serialized object data about the exception being thrown.</param>
        /// <param name="context">The System.Runtime.Serialization.StreamingContext that contains
        /// contextual information about the source or destination.</param>
        protected DeiConfigException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context)
            : base(info, context)
        {
        }
    }
}

