﻿//-----------------------------------------------------------------------
// <copyright file="IHarnessBase.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2010 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace GermHarness
{
    using System.IO;

    /// <summary>
    /// Interface for base class for Harnesses used to host processes under remote control.
    /// </summary>
    public interface IHarnessBase
    {
        /// <summary>
        /// Gets or sets the sleep time beween processing network state.
        /// </summary>
        int ProcessNetworkStateIntervalMs { get; set; }

        /// <summary>
        /// Initialize the harness with command line arguments.
        /// </summary>
        /// <param name="args">Command line arguments.</param>
        /// <returns>True if successful, otherwise false.</returns>
        bool Initialize(ref string[] args);

        /// <summary>
        /// Initialize the harness and hosted process with command line arguments.
        /// </summary>
        /// <param name="args">Command line arguments.</param>
        /// <param name="process">The hosted process.</param>
        /// <returns>True if successful, otherwise false.</returns>
        bool Initialize(ref string[] args, IHostedProcess process);

        /// <summary>
        /// Start the harness and hosted process.
        /// </summary>
        /// <returns>True if successful, false otherwise</returns>
        bool Start();

        /// <summary>
        /// Write out the list of supported options.
        /// </summary>
        /// <param name="tw">The text writer to use.</param>
        void WriteOptionDescriptions(TextWriter tw);
    }
}
