﻿//---------------------------------------------------------------------------------
// <copyright file="DeiConfigManager.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

namespace GermHarness
{
    using System;
    using System.IO;
    using GermHarness;

    /// <summary>
    /// The Dei config type.
    /// </summary>
    public enum DeiConfigType
    {
        /// <summary>
        /// Dei is configured using a configuration file.
        /// </summary>
        DeiConfigFile,

        /// <summary>
        /// Dei is configured using a configuration string. 
        /// </summary>
        DeiConfigString,

        /// <summary>
        /// Does not need to configure Dei.
        /// </summary>
        NoDei,
    }

    /// <summary>
    /// DeiConfigManager is used to parse the Dei config file or Dei config string, 
    /// then use the specified configuration to initialize the peer harness object.
    /// </summary>
    internal class DeiConfigManager : GermHarness.IDeiConfigManager
    {
        /// <summary>
        /// Provides file system access for checking and reading config files.
        /// </summary>
        private readonly IFileSystem fileSystem;

        /// <summary>
        /// The host of the Dei.
        /// </summary>
        private string deiHost;

        /// <summary>
        /// The string containing the port of the Dei.
        /// </summary>
        private string deiPortString;

        /// <summary>
        /// The port of the Dei.
        /// </summary>
        private ushort deiPort;

        /// <summary>
        /// The username of the Dei account.
        /// </summary>
        private string deiUsername;

        /// <summary>
        /// The password of the Dei account.
        /// </summary>
        private string deiPassword;

        /// <summary>
        /// A string containing a flag indicating whether to use ssl connection.
        /// </summary>
        private string useSslConnectionString;

        /// <summary>
        /// A flag indicating whether to use ssl connection.
        /// </summary>
        private bool useSslConnection;

        /// <summary>
        /// Initializes a new instance of the <see cref="DeiConfigManager"/> class.
        /// </summary>
        public DeiConfigManager() :
            this(new FileSystem())
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DeiConfigManager"/> class.
        /// </summary>
        /// <param name="fileSystem">The <see cref="IFileSystem"/> to use.</param>
        public DeiConfigManager(IFileSystem fileSystem)
        {
            if (fileSystem == null)
            {
                throw new ArgumentNullException("fileSystem");
            }

            this.fileSystem = fileSystem;
        }

        /// <summary>
        /// Configs the peer harness object.
        /// </summary>
        /// <param name="harness">The peer harness.</param>
        /// <param name="configFileName">Name of the config file.</param>
        /// <param name="configString">The config string.</param>
        /// <exception cref="DeiConfigException">Thrown when an error is encountered in the Dei config.</exception>
        public void ConfigHarness(IPeerHarness harness, string configFileName, string configString)
        {
            this.deiHost = null;
            this.deiPortString = null;
            this.deiUsername = null;
            this.deiPassword = null;
            this.useSslConnectionString = null;

            if (harness == null)
            {
                throw new ArgumentNullException();
            }

            DeiConfigType type = this.GetDeiConfigType(configString, configFileName);
            if (type == DeiConfigType.NoDei)
            {
                return;
            }

            if (type == DeiConfigType.DeiConfigFile)
            {
                this.ReadDeiConfigFile(configFileName);
            }
            else if (type == DeiConfigType.DeiConfigString)
            {
                this.ReadDeiConfigString(configString);
            }

            // config the peer harness object.
            harness.SetIdentityProvider(
                this.deiHost,
                this.deiPort,
                this.deiUsername,
                this.deiPassword,
                this.useSslConnection);
        }

        /// <summary>
        /// Gets the type of the dei config.
        /// </summary>
        /// <param name="deiConfigString">The dei config string.</param>
        /// <param name="deiConfigFileName">Name of the dei config file.</param>
        /// <returns>The dei config type. </returns>
        private DeiConfigType GetDeiConfigType(string deiConfigString, string deiConfigFileName)
        {
            if (!string.IsNullOrEmpty(deiConfigString) && !string.IsNullOrEmpty(deiConfigFileName))
            {
                throw new DeiConfigException("A Dei config string or file may be specified, but not both.");
            }

            if (string.IsNullOrEmpty(deiConfigString) && string.IsNullOrEmpty(deiConfigFileName))
            {
                return DeiConfigType.NoDei;
            }
            else if (!string.IsNullOrEmpty(deiConfigString))
            {
                return DeiConfigType.DeiConfigString;
            }
            else
            {
                return DeiConfigType.DeiConfigFile;
            }
        }

        /// <summary>
        /// Reads the dei config file.
        /// </summary>
        /// <param name="fileName">Name of the file.</param>
        /// <exception cref="DeiConfigException">Thrown when the file name is missing or
        /// there is an error reading the file.</exception>
        private void ReadDeiConfigFile(string fileName)
        {
            if (!this.fileSystem.FileExists(fileName))
            {
                throw new DeiConfigException("Cannot find Dei config file: " + fileName);
            }

            string[] lines;
            try
            {
                lines = this.fileSystem.ReadAllLines(fileName);
            }
            catch (System.IO.IOException ex)
            {
                throw new DeiConfigException("Error reading Dei config file: " + ex.Message);
            }

            if (lines.Length != 5)
            {
                throw new DeiConfigException("Dei config file must contain five lines.");
            }

            foreach (string line in lines)
            {
                string[] parts = line.Split(new char[] { ':' }, 2);
                if (parts == null || parts.Length < 2)
                {
                    throw new DeiConfigException("File contains badly formatted line: \"" + line + "\"");
                }

                switch (parts[0])
                {
                    case "deiHost":
                        this.deiHost = parts[1];
                        break;
                    case "deiPort":
                        this.deiPortString = parts[1];
                        break;
                    case "deiUsername":
                        this.deiUsername = parts[1];
                        break;
                    case "deiPassword":
                        this.deiPassword = parts[1];
                        break;
                    case "useSslConnection":
                        this.useSslConnectionString = parts[1];
                        break;
                    default:
                        throw new DeiConfigException("File contains invalid entry: \"" + parts[0] + "\"");
                }
            }

            this.ValidateFields();
        }

        /// <summary>
        /// Reads the dei config from the Dei config string.
        /// </summary>
        /// <param name="deiConfigString">The dei config string.</param>
        /// <exception cref="DeiConfigException">Thrown when the Dei config string is invalid.</exception>
        private void ReadDeiConfigString(string deiConfigString)
        {
            //// Note: deiConfigString should have a fix order, assuming that ControlCenter will not give a random order.
            string[] parts = deiConfigString.Split(new char[] { ';' }, 5);
            if (parts.Length != 5)
            {
                throw new DeiConfigException("The Dei config string must be in the format " +
                    "\"address;port;use_ssl(true/false);username;password\"");
            }

            this.deiHost = parts[0];
            this.deiPortString = parts[1];
            this.useSslConnectionString = parts[2];
            this.deiUsername = parts[3];
            this.deiPassword = parts[4];

            this.ValidateFields();
        }

        /// <summary>
        /// Alls the fields set.
        /// </summary>
        /// <exception cref="DeiConfigException">Thrown if one or more fields are invalid.</exception>
        private void ValidateFields()
        {
            if (string.IsNullOrEmpty(this.deiHost))
            {
                throw new DeiConfigException("Host address is missing.");
            }

            if (string.IsNullOrEmpty(this.deiUsername))
            {
                throw new DeiConfigException("Username is missing.");
            }

            if (string.IsNullOrEmpty(this.deiPassword))
            {
                throw new DeiConfigException("Password is missing.");
            }

            if (string.IsNullOrEmpty(this.deiPortString))
            {
                throw new DeiConfigException("Port number is missing.");
            }

            if (string.IsNullOrEmpty(this.useSslConnectionString))
            {
                throw new DeiConfigException("SSL flag is missing.");
            }

            this.ParsePortNumber(this.deiPortString);
            this.ParseSslFlag(this.useSslConnectionString);

            // Enforce no spaces to discourage accidentally including spaces in password.
            if (this.deiHost.Contains(" ") ||
                this.deiUsername.Contains(" ") ||
                this.deiPortString.Contains(" ") ||
                this.useSslConnectionString.Contains(" "))
            {
                throw new DeiConfigException("Dei configuration should not include whitespace.");
            }
        }

        /// <summary>
        /// Parse a string representing an SSL usage flag.
        /// </summary>
        /// <param name="sslFlag">The string to parse.</param>
        /// <exception cref="DeiConfigException">Thrown when the SSL usage flag is not true or false.</exception>
        private void ParseSslFlag(string sslFlag)
        {
            try
            {
                this.useSslConnection = bool.Parse(sslFlag);
            }
            catch (FormatException)
            {
                throw new DeiConfigException("SSL usage flag must be \"true\" or \"false\".");
            }
        }

        /// <summary>
        /// Parse a string representing a port.
        /// </summary>
        /// <param name="port">The string to parse containing a port number.</param>
        /// <exception cref="DeiConfigException">Thrown when the port not an integer in the correct range.</exception>
        private void ParsePortNumber(string port)
        {
            string errorMessage = string.Format(
                "Port number must be an integer between {0} to {1}.",
                ushort.MinValue,
                ushort.MaxValue);
            try
            {
                this.deiPort = UInt16.Parse(port);
            }
            catch (FormatException)
            {
                throw new DeiConfigException(errorMessage);
            }
            catch (OverflowException)
            {
                throw new DeiConfigException(errorMessage);
            }
        }
    }
}