﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Tcp;
using System.Runtime.Serialization.Formatters;

namespace PeerHarness
{
    public class HarnessServer
    {
        private TcpChannel mChannel;
        private Harness mHarness;

        public HarnessServer()
        {
        }

        public void Start(string[] args)
        {
            this.Start(args, null);
        }

        public void Start(string[] args, IPeer peer)
        {
            int port = 0;
            bool harness = false;
            string name = AppDomain.CurrentDomain.FriendlyName;

            try
            {
                foreach (string arg in args)
                {
                    if (arg.Contains("port"))
                    {
                        port = int.Parse(arg.Split(':')[1]);
                    }

                    if (arg.Contains("name"))
                    {
                        name = arg.Split(':')[1];
                    }

                    if (arg.Contains("harness"))
                    {
                        harness = true;
                    }
                }
            }
            catch
            {
                return;
            }

            if (harness)
            {
                this.Start(port, name, peer);
            }
            else
            {
                this.StartSynchronously(name, peer);
            }

            // Sleep for a bit before exiting to allow the socket to close.
            System.Threading.Thread.Sleep(500);
        }

        private void StartSynchronously(string name, IPeer peer)
        {
            Harness harness = new Harness("BotRunner");

            String config = System.IO.File.ReadAllText("NetworkConfig.xml");

            harness.Peer = peer;
            harness.StartPeer(name, 89, 0, config, "x", "d");

            int i = 0;
            while (harness.IsRunning && i < 290)
            {
                System.Threading.Thread.Sleep(1000);
                i++;
            }

            harness.StopPeer();
        }

        public void Start(int port, string peerName, IPeer peer)
        {
            BinaryServerFormatterSinkProvider serverProvider = new BinaryServerFormatterSinkProvider();
            serverProvider.TypeFilterLevel = TypeFilterLevel.Full;

            BinaryClientFormatterSinkProvider clientProvider = new BinaryClientFormatterSinkProvider();

            IDictionary properties = new Hashtable();
            properties["port"] = port;
      //      properties["bindTo"] = "127.0.0.1"; 

            this.mChannel = new TcpChannel(properties, clientProvider, serverProvider);
            ChannelServices.RegisterChannel(this.mChannel, false);

            this.mHarness = new Harness(peerName);
            this.mHarness.Peer = peer;
            RemotingServices.Marshal(this.mHarness, "Harness");

            try
            {
                this.mHarness.DoSomething(); // Interact with the object to ensure that it exists
                while (this.mHarness.IsRunning)
                {
                    System.Threading.Thread.Sleep(100);
                }
            }
            catch { }

            RemotingServices.Disconnect(this.mHarness);
            this.mHarness = null;

            ChannelServices.UnregisterChannel(this.mChannel);
            this.mChannel = null;
        }
    }
}
