using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("GermHarness")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("79285249-8615-4eb5-9700-b1bf0eed5031")]

// Make internals visible for unit tests.
[assembly: InternalsVisibleTo("GermHarnessTests, PublicKey=00240000048000009400000006020000002400005253413100040000010001007de84bbfe3dfb92d8a50a9b0b804eb07a02d28bb5b5665246b5a338a47df060263efe25fb2be507841aa9247a054092558cce470818f8c6ea295f8714f6e3f7494869e0b14c52f26a41e0e83f280e094e2c54150653db0b1fff68ea4ffba589787839be3f40bb6c3bc62a251ab54e60b0c1ff6c8e9afd8bf0be402929f40f4b6")]
[assembly: InternalsVisibleTo("DynamicProxyGenAssembly2, PublicKey=0024000004800000940000000602000000240000525341310004000001000100c547cac37abd99c8db225ef2f6c8a3602f3b3606cc9891605d02baa56104f4cfc0734aa39b93bf7852f7d9266654753cc297e7d2edfe0bac1cdcf9f717241550e0a7b191195b7667bb4f64bcb8e2121380fd1d9d46ad2d92d2d15605093924cceaf74c4861eff62abf69b9291ed0a340e113be11e6a7d3113e92484cf7045cc7")]
