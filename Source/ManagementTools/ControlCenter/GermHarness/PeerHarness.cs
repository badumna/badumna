﻿//-----------------------------------------------------------------------
// <copyright file="PeerHarness.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2010 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace GermHarness
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Net;
    using System.Net.Security;
    using System.Xml;
    using Badumna;
    using Badumna.Core;
    using Badumna.Security;
    using Badumna.ServiceDiscovery;
    using Dei;
    using Mono.Options;

    /// <summary>
    /// Delegate for PeerHarness factory method.
    /// </summary>
    /// <param name="options">Badumna configuration options.</param>
    /// <returns>A new instance of the PeerHarness class.</returns>
    public delegate IPeerHarness PeerHarnessFactory(Options options);

    /// <summary>
    /// Harness for hosting peer processes under remote control.
    /// </summary>
    public sealed class PeerHarness : HarnessBase, IPeerHarness
    {
        /// <summary>
        /// The network facade factory.
        /// </summary>
        private readonly NetworkFacadeFactory networkFacadeFactory;

        /// <summary>
        /// Parses Dei configuration, and configures harness with IdentityProvider.
        /// </summary>
        private readonly IDeiConfigManager deiConfigManager;

        /// <summary>
        /// Option set for parsing command line options.
        /// </summary>
        private readonly OptionSet optionSet;

        /// <summary>
        /// Badumna-related command line option handler.
        /// </summary>
        private readonly BadumnaCommandLineOptions badumnaCommandLineOptions;

        /// <summary>
        /// Badumna configuration options.
        /// </summary>
        private readonly Options configurationOptions;

        /// <summary>
        /// The network facade.
        /// </summary>
        private INetworkFacade networkFacade;

        /// <summary>
        /// The identity provider for secure log in.
        /// </summary>
        private IIdentityProvider identityProvider;

        /// <summary>
        /// The type of server the peer is (for service discovery).
        /// </summary>
        private ServerType serverType;

        /// <summary>
        /// Flag indicating whether the peer has a service that should be registered.
        /// </summary>
        private bool hasRegisteredService;

        /// <summary>
        /// The name of the file containing the Dei configuration.
        /// </summary>
        private string deiConfigFile = null;

        /// <summary>
        /// A string containing the Dei configuration.
        /// </summary>
        private string deiConfigString = null;
        
        /// <summary>
        /// Flag indicating whether the peer should rejoin an existing network.
        /// </summary>
        private bool rejoin = false;

        /// <summary>
        /// Peers in any existing network to rejoin.
        /// </summary>
        private List<IPEndPoint> initialPeers;

        /// <summary>
        /// Total amount of time run (used for calculating periodic tasks).
        /// </summary>
        private int totalDelayMilliseconds = 0;

        /// <summary>
        /// A value indicating whether the peer is running in the cloud.
        /// </summary>
        private bool onCloud;

        /// <summary>
        /// Initializes a new instance of the PeerHarness class.
        /// </summary>
        /// <param name="networkFacadeFactory">A factory method for creating the network facade.</param>
        /// <param name="configurationOptions">The Badumna configuration options.</param>
        /// <param name="deiConfigManager">The dei configuration manager to use.</param>
        public PeerHarness(
            NetworkFacadeFactory networkFacadeFactory,
            Options configurationOptions,
            IDeiConfigManager deiConfigManager)
        {
            if (networkFacadeFactory == null)
            {
                throw new ArgumentNullException("networkFacadeFactory");
            }

            if (configurationOptions == null)
            {
                throw new ArgumentNullException("configurationOptions");
            }

            if (deiConfigManager == null)
            {
                throw new ArgumentNullException("deiConfigManager");
            }

            this.networkFacadeFactory = networkFacadeFactory;
            this.configurationOptions = configurationOptions;
            this.deiConfigManager = deiConfigManager;
            
            this.optionSet = new OptionSet()
            {
                { "dei-config-file=",
                    "the name of the file containing the Dei configuration.",
                    (string v) => this.deiConfigFile = v },
                { "dei-config-string=",
                    "a string containing the Dei configuration.",
                    (string v) => this.deiConfigString = v },
                { "cloud",
                    "Indicating whether the service is running in the cloud.",
                    var => this.onCloud = true }
            };

            this.badumnaCommandLineOptions = new BadumnaCommandLineOptions(this.optionSet);

            this.ProcessNetworkStateIntervalMs = 25;
        }

        /// <summary>
        /// Initializes a new instance of the PeerHarness class.
        /// </summary>
        /// <param name="options">Badumna configuration options.</param>
        public PeerHarness(Options options) :
            this(o => NetworkFacade.Create(o), options, new DeiConfigManager())
        {
        }

        /// <inheritdoc/>
        public INetworkFacade NetworkFacadeInstance
        {
            get
            {
                return this.networkFacade;
            }
        }

        /// <summary>
        /// Initialize the harness and hosted process with command line arguments.
        /// </summary>
        /// <param name="args">Initialization arguments.</param>
        /// <param name="process">The process to host.</param>
        /// <exception cref="OptionException">Thrown when there ia an error parsing command line options.</exception>
        /// <exception cref="DeiConfigException">Thrown when there ia an error in the Dei configuration.</exception>
        /// <returns>True if succssful, otherwise false.</returns>
        public override bool Initialize(ref string[] args, IHostedProcess process)
        {
            // Parse BaseHarness specific command line arguments.
            List<string> extras = this.optionSet.Parse(args);

            this.deiConfigManager.ConfigHarness(this, this.deiConfigFile, this.deiConfigString);

            args = extras.ToArray();

            return base.Initialize(ref args, process);
        }

        /// <summary>
        /// Write a list of supported command line options.
        /// </summary>
        /// <param name="tw">The text writer to use.</param>
        public override void WriteOptionDescriptions(TextWriter tw)
        {
            this.optionSet.WriteOptionDescriptions(tw);
            base.WriteOptionDescriptions(tw);
        }

        /// <summary>
        /// Store list of peers to join.
        /// </summary>
        /// <param name="peers">A list of the addresses of the peers.</param>
        public void AddInitialPeers(List<IPEndPoint> peers)
        {
            this.initialPeers = peers;
            this.rejoin = true;
        }

        /// <summary>
        /// Create a Dei Identity Provider to use.
        /// </summary>
        /// <param name="deiHost">The address of the Dei Server.</param>
        /// <param name="deiPort">The port to connect to the Dei Server on.</param>
        /// <param name="userName">The user account to connect to the Dei Server with.</param>
        /// <param name="password">The password for the account.</param>
        /// <param name="useSSL">A flag indicating whether SSL should be used when connecting to the Dei Server.</param>
        public void SetIdentityProvider(string deiHost, ushort deiPort, string userName, string password, bool useSSL)
        {
            this.SetIdentityProvider(deiHost, deiPort, userName, password, useSSL, false, null);
        }

        /// <summary>
        /// Create a Dei Identity Provider to use.
        /// </summary>
        /// <param name="deiHost">The address of the Dei Server.</param>
        /// <param name="deiPort">The port to connect to the Dei Server on.</param>
        /// <param name="userName">The user account to connect to the Dei Server with.</param>
        /// <param name="password">The password for the account.</param>
        /// <param name="useSSL">A flag indicating whether SSL should be used when connecting to the Dei Server.</param>
        /// <param name="ignoreSSLErrors">A flag indicating whether SSL errors should be ignored when connecting to the Dei Server.</param>
        /// <param name="certificateValidationCallback">A delegate for validating the SSL certificate.</param>
        public void SetIdentityProvider(
            string deiHost,
            ushort deiPort,
            string userName,
            string password,
            bool useSSL,
            bool ignoreSSLErrors,
            RemoteCertificateValidationCallback certificateValidationCallback)
        {
            using (Session session = new Session(deiHost, deiPort))
            {
                session.UseSslConnection = useSSL;
                session.IgnoreSslErrors = ignoreSSLErrors;
                session.CertificateValidationCallback = certificateValidationCallback;

                LoginResult result = session.Authenticate(userName, password);
                if (result.WasSuccessful)
                {
                    result = session.SelectIdentity(Character.None, out this.identityProvider);
                }

                if (!result.WasSuccessful)
                {
                    throw new DeiAuthenticationException(String.Format("User authentication failed: {0}.", result.ErrorDescription));
                }
            }
        }

        /// <summary>
        /// Register a service for service discovery.
        /// </summary>
        /// <param name="type">The type of the server.</param>
        public void RegisterService(ServerType type)
        {
            this.serverType = type;
            this.hasRegisteredService = true;
        }

        /// <summary>
        /// Start the peer.
        /// </summary>
        /// <param name="config">The network configuration.</param>
        /// <returns>True if successful, otherwise false.</returns>
        protected override bool OnStart(string config)
        {
            Options options = this.configurationOptions;

            bool haveConfigFromController = !string.IsNullOrEmpty(config);
            if (haveConfigFromController)
            {
                XmlDocument configDocument = new System.Xml.XmlDocument();

                try
                {
                    configDocument.LoadXml(config);
#if DEBUG
                    configDocument.Save(Console.Out);
#endif
                }
                catch (XmlException ex)
                {
                    Console.WriteLine("Error in configuration options: " + ex.Message);
                    Environment.Exit((int)ExitCode.ConfigurationError);
                }

                options = new Options(configDocument);
            }

            options = this.badumnaCommandLineOptions.ApplyOptions(options, !haveConfigFromController);

            // a peer harness process should always has its HostedService flag set to be true. 
            options.Connectivity.IsHostedService = true;

            // if running in cloud
            if (this.onCloud)
            {
                options.CloudIdentifier = "cloud-service";
            }

            Console.WriteLine("Starting the peer using the application name : \"{0}\"", options.Connectivity.ApplicationName);

            this.badumnaCommandLineOptions.SaveIfRequired(options);

            this.networkFacade = this.networkFacadeFactory.Invoke(options);

            this.networkFacade.RequestShutdown += this.RequestShutdown;
            this.networkFacade.RequestShutdownForUpdate += this.RequestShutdown;

            bool success;
            if (this.identityProvider != null)
            {
                success = this.networkFacade.Login(this.identityProvider);
            }
            else
            {
                success = this.networkFacade.Login();
            }

            if (!success)
            {
                Console.WriteLine("Login failed (maybe the port is already in use or UDP is blocked?).");
                return false;
            }

            //// whether the peer is running on a machine with open connection
            if (!this.PeerHasOpenConnection())
            {
                Console.WriteLine("Warning: running behind NAT, not all users can directly connect to this computer.");
            }

            if (this.hasRegisteredService)
            {
                Console.WriteLine("Announce Service has been requested");
                this.networkFacade.AnnounceService(this.serverType);
            }

            if (this.rejoin)
            {
                Console.WriteLine("Rejoin is set, going to add {0} initial peers.", this.initialPeers.Count);
                ((INetworkFacadeInternal)this.networkFacade).AddInitialConnections(this.initialPeers);
            }

            return true;
        }

        /// <summary>
        /// Stop the peer.
        /// </summary>
        protected override void OnStop()
        {
            this.networkFacade.Shutdown(true);
        }

        /// <summary>
        /// Do regular processing.
        /// </summary>
        protected override void OnProcessRegularEvents()
        {
            // networkFacade is not available until OnStart is called.
            if (this.networkFacade != null)
            {
                if (this.networkFacade.IsLoggedIn)
                {
                    this.networkFacade.ProcessNetworkState();
                }
            }

            if (this.Verbose)
            {
                this.totalDelayMilliseconds += this.ProcessNetworkStateIntervalMs;

                // dump open peer connections every 60 seconds.
                if (this.totalDelayMilliseconds >= (long)TimeSpan.FromSeconds(5).TotalMilliseconds)
                {
                    this.totalDelayMilliseconds = 0;
                    if (this.networkFacade != null)
                    {
                        Console.WriteLine(this.networkFacade.GetNetworkStatus().ToString());
                    }
                    else
                    {
                        Console.WriteLine("NetworkFacade not instantiated.");
                    }
                }
            }
        }

        /// <summary>
        /// Write the peer's current status (for reporting to controller).
        /// </summary>
        /// <param name="stream">The stream to write to.</param>
        /// <returns>True if the status is successfully written, otherwise false.</returns>
        protected override bool OnWriteStatus(Stream stream)
        {
            if (this.networkFacade != null)
            {
                if (this.networkFacade.IsLoggedIn)
                {
                    this.networkFacade.GetNetworkStatus().ToXml().Save(stream);
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Get the peer's current ping state (for reporting to controller).
        /// </summary>
        /// <returns>The ping state.</returns>
        protected override PingState OnGetPing()
        {
            PingState state = PingState.Initialized;

            if (this.networkFacade != null)
            {
                if (this.networkFacade.IsLoggedIn)
                {
                    state = PingState.LoggedIn;
                }

                if (this.networkFacade.IsOnline)
                {
                    state = PingState.Active;
                }
            }

            return state;
        }

        /// <summary>
        /// Check for any updates.
        /// </summary>
        protected override void OnCheckForUpdates()
        {
            ////NetworkFacade.CheckForUpdates();
        }

        /// <summary>
        /// Handle shutdown request events.
        /// </summary>
        /// <param name="sender">The event sender.</param>
        /// <param name="e">The event arguments.</param>
        private void RequestShutdown(object sender, EventArgs e)
        {
            this.Stop();
        }

        /// <summary>
        /// Whether the local peer has open connection.
        /// </summary>
        /// <returns>True if running with open connection, or false.</returns>
        private bool PeerHasOpenConnection()
        {
            if (this.networkFacade != null)
            {
                if (this.networkFacade.IsLoggedIn)
                {
                    string publicAddress = this.networkFacade.GetNetworkStatus().PublicAddress.ToLower();

                    return publicAddress.Contains("open") || publicAddress.Contains("full cone");
                }
            }

            throw new InvalidOperationException("NAT type is only available after login.");
        }
    }
}
