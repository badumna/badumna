﻿//-----------------------------------------------------------------------
// <copyright file="DeiAuthenticationException.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace GermHarness
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// An exception indicating an error encountered during Dei authentication.
    /// </summary>
    public class DeiAuthenticationException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DeiAuthenticationException"/> class.
        /// </summary>
        public DeiAuthenticationException()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DeiAuthenticationException"/> class with a
        /// specified message.
        /// </summary>
        /// <param name="message">A message describing the reason for the exception.</param>
        public DeiAuthenticationException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DeiAuthenticationException"/> class with a
        /// specified error message and a reference to the inner exception that
        /// is the cause of this exception..
        /// </summary>
        /// <param name="message">The error message that explains the reason for
        /// the exception.</param>
        /// <param name="inner">The exception that is the cause of the current
        /// exception, or a null reference (Nothing in Visual Basic) if no inner
        /// exception is specified.</param>
        public DeiAuthenticationException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
