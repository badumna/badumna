﻿//-----------------------------------------------------------------------
// <copyright file="IHostedProcess.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2010 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace GermHarness
{
    using System.IO;

    /// <summary>
    /// A process that can be hosted by a GermHarness.
    /// </summary>
    public interface IHostedProcess
    { 
        /// <summary>
        /// Called when the peer harness has started, before any other calls are made.
        /// </summary>
        /// <param name="arguments">An array of arguments that were passed to the process.</param>
        void OnInitialize(ref string[] arguments);

        /// <summary>
        /// The peer is shutting down. Called when either the process is about to end
        /// or a remote user has requested the process shutdown.
        /// </summary>
        void OnShutdown();

        /// <summary>
        /// Called when the process has received a request to start.
        /// Is called after OnInitialize() and before any other methods in the IPeer interface.
        /// </summary>
        /// <returns>True if successful, otherwise false.</returns>
        bool OnStart();

        /// <summary>
        /// Called at regular intervals (<see cref="IHarnessBase.ProcessNetworkStateIntervalMs">ProcessNetworkStateIntervalMs</see>).
        /// </summary>
        /// <param name="delayMilliseconds">The delay since the last call to this method</param>
        /// <returns>True if the wishes to continue in the run loop. If false the process will shutdown and exit.</returns>
        bool OnPerformRegularTasks(int delayMilliseconds);

        /// <summary>
        /// Process any remote requests. 
        /// </summary>
        /// <param name="requestType">An id for the type of request</param>
        /// <param name="request">Any data associated with the request</param>
        /// <returns>The reply to the request</returns>
        byte[] OnProcessRequest(int requestType, byte[] request);

        /// <summary>
        /// Write a description of the command line options supported.
        /// </summary>
        /// <param name="tw">A text writer to use.</param>
        void WriteOptionsDescription(TextWriter tw);
    }
}
