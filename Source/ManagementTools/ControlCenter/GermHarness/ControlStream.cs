﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Threading;
using System.Diagnostics;

namespace GermHarness
{
    class CustomRequest
    {
        public int Type { get; set; }
        public byte[] Message { get; set; }
    }

    class ControlStream
    {
        private const int mBufferSize = 8096;

        private TcpClient mClient;
        private Thread mReadThread;
        private Queue<CustomRequest> mRequestQueue = new Queue<CustomRequest>();
        private object mRequestQueueLock = new object();
        private Queue<byte[]> mResponseQueue = new Queue<byte[]>();
        private object mResponseLock = new object();
        private AutoResetEvent mResponseAvailableSignal = new AutoResetEvent(false);

        private bool IsRunning;

        public ControlStream()
        {
        }

        public bool Start(int port, int controlid)
        {
            if (port > 0)
            {
                try
                {
                    this.mClient = new TcpClient();
                    this.mClient.Connect(new IPEndPoint(IPAddress.Loopback, port));
                    if (this.mClient.Connected)
                    {
                        NetworkStream stream = this.mClient.GetStream();
                        int processId = System.Diagnostics.Process.GetCurrentProcess().Id;

                        // Write preamble to negotiate connection with Germ
                        ControlStream.WriteVersion(stream, 1, 0);
                        ControlStream.WriteInt(stream, controlid); // The controlId passed to this process by command line argument from the Germ
                        ControlStream.WriteInt(stream, processId); // The proceesId of this process which could be otherwise unknown by the Germ

                        // Start the request read loop thread
                        this.mReadThread = new Thread(this.ReadLoop);
                        this.mReadThread.IsBackground = true;
                        this.mReadThread.Start();
                        return true;
                    }
                }
                catch
                {
                }
            }

            return false;
        }

        /// <summary>
        /// Get any pending requests from the queue. NOTE: For EVERY request popped from the queue
        /// a corresponding response MUST be pushed back on the queue in the same order.
        /// </summary>
        /// <returns>The next request in the queue or null if no request is available</returns>
        public CustomRequest PopRequest()
        {
            lock (this.mRequestQueueLock)
            {
                if (this.mRequestQueue.Count > 0)
                {
                    return this.mRequestQueue.Dequeue();
                }
            }

            return null;
        }

        /// <summary>
        /// Push a response to a previously popped request.
        /// </summary>
        /// <param name="response">The response, can be null.</param>
        public void PushResponse(byte[] response)
        {
            lock (this.mResponseLock)
            {
                this.mResponseQueue.Enqueue(response);
                this.mResponseAvailableSignal.Set();
            }
        }

        protected static void WriteVersion(Stream stream, int major, int minor)
        {
            ControlStream.WriteInt(stream, major); 
            ControlStream.WriteInt(stream, minor); 
        }

        /// <summary>
        /// Continously read requests from the stream and push them on the request queue then block until a
        /// response can be popped off the response queue and send it.
        /// </summary>
        private void ReadLoop()
        {
            try
            {
                if (this.mClient.Connected)
                {
                    this.IsRunning = true;

                    byte[] buffer = new byte[ControlStream.mBufferSize];
                    Stream clientStream = this.mClient.GetStream();
                    using (BinaryReader reader = new BinaryReader(clientStream))
                    {
                        do
                        {
                            CustomRequest request = new CustomRequest();

                            request.Type = reader.ReadInt32();
                            int length = reader.ReadInt32();
                            if (length > 0)
                            {
                                request.Message = reader.ReadBytes(length);
                            }
                            else
                            {
                                request.Message = new byte[] { };
                            }

                            lock (this.mRequestQueueLock)
                            {
                                Trace.TraceInformation("Harness : Received request {0} ", request.Type);
                                this.mRequestQueue.Enqueue(request);
                            }

                            if (request.Type == (int)CustomRequestType.StopPeer)
                            {
                                this.IsRunning = false;
                            }

                            this.mResponseAvailableSignal.WaitOne();
                            lock (this.mResponseLock)
                            {
                                if (this.mResponseQueue.Count > 0)
                                {
                                    byte[] response = this.mResponseQueue.Dequeue();
                                    int responseLength = 0;
                                    if (response == null)
                                    {
                                        clientStream.Write(BitConverter.GetBytes((int)0), 0, 4);
                                    }
                                    else
                                    {
                                        responseLength = response.Length;
                                        clientStream.Write(BitConverter.GetBytes(responseLength), 0, 4);
                                        clientStream.Write(response, 0, responseLength);
                                    }
                                    clientStream.Flush();
                                    Trace.TraceInformation("Harness : Sent response ({1} bytes) to request {0} ", request.Type, responseLength);
                                }
                                else
                                {
                                    Trace.TraceWarning("Harness : No response available for request {0}", request.Type);
                                    this.IsRunning = false;
                                }
                            }
                        }
                        while (this.IsRunning);
                    }
                }
            }
            catch (IOException)
            {
                Trace.TraceInformation("Harness : Connection to server closed unexpectedly");
            }
            finally 
            {
                if (this.mClient != null && this.mClient.Connected)
                {
                    this.mClient.Close();
                }
                this.IsRunning = false;
            }

            Trace.TraceInformation("Harness : Control stream closed");
        }

        private static void WriteInt(Stream stream, int value)
        {
            stream.Write(BitConverter.GetBytes(value), 0, 4);
        }
    }
}
