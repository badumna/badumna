﻿//-----------------------------------------------------------------------
// <copyright file="IProcessHarness.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2010 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace GermHarness
{
    using System.IO;

    /// <summary>
    /// Interface for harness for hosting a process under remote control.
    /// </summary>
    public interface IProcessHarness : IHarnessBase
    {
    }
}
