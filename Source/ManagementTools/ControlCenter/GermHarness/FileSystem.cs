﻿//-----------------------------------------------------------------------
// <copyright file="FileSystem.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace GermHarness
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// Default FileSystem implementation to be used when not mocking.
    /// </summary>
    internal class FileSystem : IFileSystem
    {
        /// <summary>
        /// Determines whethe the specified file exists.
        /// </summary>
        /// <param name="path">The path to check.</param>
        /// <returns>True if the file exists, otherwise false.</returns>
        public bool FileExists(string path)
        {
            return System.IO.File.Exists(path);
        }

        /// <summary>
        /// Read all the lines in a given file.
        /// </summary>
        /// <param name="path">The path of the file to read.</param>
        /// <returns>An array of the lines in the file.</returns>
        /// <exception cref="System.IO.IOException">Thrown when there is an error reading the file.</exception>
        public string[] ReadAllLines(string path)
        {
            try
            {
                return System.IO.File.ReadAllLines(path);
            }
            catch (System.IO.IOException ex)
            {
                throw new System.IO.IOException(ex.Message, ex);
            }
            catch (ArgumentException ex)
            {
                throw new System.IO.IOException(ex.Message, ex);
            }
            catch (UnauthorizedAccessException ex)
            {
                throw new System.IO.IOException(ex.Message, ex);
            }
            catch (NotSupportedException ex)
            {
                throw new System.IO.IOException(ex.Message, ex);
            }
            catch (System.Security.SecurityException ex)
            {
                throw new System.IO.IOException(ex.Message, ex);
            }
        }
    }
}
