﻿//-----------------------------------------------------------------------
// <copyright file="IPeerHarness.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2010 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace GermHarness
{
    using System.Collections.Generic;
    using System.Net;
    using System.Net.Security;
    using Badumna;
    using Badumna.ServiceDiscovery;

    /// <summary>
    /// Interface for harness for hosting peer processes under remote control.
    /// </summary>
    public interface IPeerHarness : IHarnessBase
    {
        /// <summary>
        /// Gets the network facade.
        /// </summary>
        INetworkFacade NetworkFacadeInstance { get; }

        /// <summary>
        /// Store list of peers to join.
        /// </summary>
        /// <param name="peers">A list of the addresses of the peers.</param>
        void AddInitialPeers(List<IPEndPoint> peers);

        /// <summary>
        /// Create a Dei Identity Provider to use.
        /// </summary>
        /// <param name="deiHost">The address of the Dei Server.</param>
        /// <param name="deiPort">The port to connect to the Dei Server on.</param>
        /// <param name="userName">The user account to connect to the Dei Server with.</param>
        /// <param name="password">The password for the account.</param>
        /// <param name="useSSL">A flag indicating whether SSL should be used when connecting to the Dei Server.</param>
        void SetIdentityProvider(
            string deiHost,
            ushort deiPort,
            string userName,
            string password,
            bool useSSL);

        /// <summary>
        /// Create a Dei Identity Provider to use.
        /// </summary>
        /// <param name="deiHost">The address of the Dei Server.</param>
        /// <param name="deiPort">The port to connect to the Dei Server on.</param>
        /// <param name="userName">The user account to connect to the Dei Server with.</param>
        /// <param name="password">The password for the account.</param>
        /// <param name="useSSL">A flag indicating whether SSL should be used when connecting to the Dei Server.</param>
        /// <param name="ignoreSslErrors">A flag indicating whether SSL errors should be ignored when connecting to the Dei Server.</param>
        /// <param name="certificateValidationCallback">A delegate for validating the SSL certificate.</param>
        void SetIdentityProvider(
            string deiHost,
            ushort deiPort,
            string userName,
            string password,
            bool useSSL,
            bool ignoreSslErrors,
            RemoteCertificateValidationCallback certificateValidationCallback);

        /// <summary>
        /// Register a service for service discovery.
        /// </summary>
        /// <param name="type">The type of the server.</param>
        void RegisterService(ServerType type);
        
        /// <summary>
        /// Stop the hosted process.
        /// </summary>
        void Stop();        
    }
}
