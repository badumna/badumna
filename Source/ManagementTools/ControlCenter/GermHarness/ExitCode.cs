﻿//-----------------------------------------------------------------------
// <copyright file="ExitCode.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace GermHarness
{
    /// <summary>
    /// GermHarness exit codes.
    /// <para>
    /// Control Center used this enumaration to determine whether the service is still
    /// running or have exited with an error.
    /// </para>
    /// </summary>
    public enum ExitCode
    {
        /// <summary>
        /// Process exit after displaying help messages (i.e. when the process is started with arguments --help or -h).
        /// </summary>
        DisplayHelp = 1,

        /// <summary>
        /// Invalid arguments exit code.
        /// </summary>
        InvalidArguments,

        /// <summary>
        /// Dei authentication failed exit code.
        /// </summary>
        DeiAuthenticationFailed,

        /// <summary>
        /// Announce service failed exit code.
        /// </summary>
        AnnounceServiceFailed,

        /// <summary>
        /// Cannot load/save the configuration options.
        /// </summary>
        ConfigurationError,

        /// <summary>
        /// Others exceptions.
        /// </summary>
        Others
    }
}
