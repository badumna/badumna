﻿//-----------------------------------------------------------------------
// <copyright file="HarnessBase.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2010 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace GermHarness
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Reflection;
    using System.Text;
    using System.Threading;
    using System.Xml;
    using Badumna.Core;
    using Badumna.Security;
    using Mono.Options;

    #region Enumerations

    /// <summary>
    /// Enumeration identifying types custom request receiveable from controller.
    /// </summary>
    public enum CustomRequestType : int
    {
        /// <summary>
        /// Check for updates to the harness.
        /// </summary>
        Update = int.MaxValue - 1,

        /// <summary>
        /// Start the peer.
        /// </summary>
        StartPeer = int.MaxValue - 2,
        
        /// <summary>
        /// Stop the peer.
        /// </summary>
        StopPeer = int.MaxValue - 3,

        /// <summary>
        /// Report status.
        /// </summary>
        Status = int.MaxValue - 4,

        /// <summary>
        /// Report ping state.
        /// </summary>
        Ping = int.MaxValue - 5
    }

    /// <summary>
    /// Enumeration of states reportable in response to a ping request.
    /// </summary>
    public enum PingState
    {
        /// <summary>
        /// Process is not in any other state.
        /// </summary>
        None,

        /// <summary>
        /// Process has been initialized.
        /// </summary>
        Initialized,

        /// <summary>
        /// Process is logged in to Badumna network (only valid for peers).
        /// </summary>
        LoggedIn,

        /// <summary>
        /// The process is active (peer is online, or process has started).
        /// </summary>
        Active,
    }

    #endregion // Enumerations

    /// <summary>
    /// Base class for Harnesses used to host processes under remote control.
    /// </summary>
    public abstract class HarnessBase : IHarnessBase
    {
        #region Fields

        /// <summary>
        /// OptionSet for parsing command line arguments.
        /// </summary>
        private readonly OptionSet optionSet;

        /// <summary>
        /// The name of the assemlby (for error messages).
        /// </summary>
        private string assemblyName;

        /// <summary>
        /// Flag indicating whether verbose output should be written.
        /// </summary>
        private bool verbose = false;

        /// <summary>
        /// Flag indicating whether a port was set using command line option.
        /// </summary>
        private bool portIsSet = false;
        
        /// <summary>
        /// The port to connect to the harness server on.
        /// </summary>
        private ushort port;

        /// <summary>
        /// Flag indicating whether a control ID was set using command line option.
        /// </summary>
        private bool controlIdIsSet = false;

        /// <summary>
        /// The ID to identify this harness to the server.
        /// </summary>
        private int controlId;

        /// <summary>
        /// A value indicating whether the hosted process is running.
        /// </summary>
        private volatile bool isRunning;

        /// <summary>
        /// The hosted process.
        /// </summary>
        private IHostedProcess process;

        /// <summary>
        /// The stream used to receive control requests and pass replies back.
        /// </summary>
        private ControlStream controlStream;

        /// <summary>
        /// Last caught exception to be notified to controller.
        /// </summary>
        private Exception lastException;

        #endregion // Fields

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the HarnessBase class.
        /// </summary>
        public HarnessBase()
        {
            this.optionSet = new OptionSet()
            {
                { "v|verbose",
                    "write status output while running.",
                    v => { this.verbose = true; } },
                { "harness-port=",
                    "the port to connect to the harness server on: must be between 0 and 65535. Required if running from Control Center.",
                    (ushort v) => { this.port = v; this.portIsSet = true; } },
                { "control-id=",
                    "the ID to identify this harness to the server: must be an integer. Required if running from Control Center.",
                    (int v) => { this.controlId = v; this.controlIdIsSet = true; } }
            };

            this.ProcessNetworkStateIntervalMs = 25;
        }

        #endregion // Constructors

        #region Properties

        /// <summary>
        /// Gets or sets the sleep time beween processing network state.
        /// </summary>
        public int ProcessNetworkStateIntervalMs { get; set; }

        /// <summary>
        /// Gets a value indicating whether verbose output should be printed.
        /// </summary>
        protected bool Verbose
        {
            get { return this.verbose;  }
        }

        #endregion // Properties

        #region Public methods

        /// <summary>
        /// Initialize the harness with command line arguments.
        /// </summary>
        /// <param name="args">Command line arguments.</param>
        /// <returns>True if successful, otherwise false.</returns>
        public virtual bool Initialize(ref string[] args)
        {
            return this.Initialize(ref args, null);
        }

        /// <summary>
        /// Initialize the harness and hosted process with command line arguments.
        /// </summary>
        /// <param name="args">Command line arguments.</param>
        /// <param name="process">The hosted process.</param>
        /// <returns>True if successful, otherwise false.</returns>
        /// <exception cref="Mono.Options.OptionException">Thrown when there is an error parsing arguments.</exception>
        public virtual bool Initialize(ref string[] args, IHostedProcess process)
        {
            this.process = process;

            // Parse BaseHarness specific command line arguments.
            // May throw OptionException.
            List<string> extras = this.optionSet.Parse(args);

            args = extras.ToArray();

            // Initialize process with remaining arguments;
            if (this.process != null)
            {
                this.process.OnInitialize(ref args);
            }

            return true;
        }

        /// <inheritdoc />
        public virtual bool Start()
        {
            if (this.portIsSet && this.controlIdIsSet)
            {
                if (this.port > 0)
                {
                    this.controlStream = new ControlStream();
                    if (!this.controlStream.Start(this.port, this.controlId))
                    {
                        // Failed to connect. 
                        // Set to null becuase it is used as a check to determine if connected.
                        this.controlStream = null;
                    }
                }
            }
            else
            {
                if (!this.StartProcess(string.Empty))
                {
                    return false;
                }
            }

            this.RunLoop();
            return true;
        }

        /// <summary>
        /// Write out the list of supported options.
        /// </summary>
        /// <param name="tw">The text writer to use.</param>
        public virtual void WriteOptionDescriptions(TextWriter tw)
        {
            this.optionSet.WriteOptionDescriptions(tw);
        }

        /// <summary>
        /// Stop the hosted process.
        /// </summary>
        public void Stop()
        {
            this.isRunning = false;
        }

        #endregion // Public methods

        #region Protected methods

        /// <summary>
        /// Override this method to trigger stuff when Start() is called.
        /// </summary>
        /// <param name="config">A coniguration string.</param>
        /// <returns>A flag indicating whether there were errors.</returns>
        protected abstract bool OnStart(string config);

        /// <summary>
        /// Override this method to trigger stuff when StopProcess() is called.
        /// </summary>
        protected abstract void OnStop();

        /// <summary>
        /// Override this method to perform regular processing.
        /// </summary>
        protected abstract void OnProcessRegularEvents();
        
        /// <summary>
        /// Override this method to write status reports for sending to controller.
        /// </summary>
        /// <param name="stream">The stream to write to.</param>
        /// <returns>True if status successfuly written, otherwise, false.</returns>
        protected abstract bool OnWriteStatus(Stream stream);
        
        /// <summary>
        /// Override this method to report the state for a ping response.
        /// </summary>
        /// <returns>The current ping state.</returns>
        protected abstract PingState OnGetPing();

        /// <summary>
        /// Override this method to check for updates.
        /// </summary>
        protected abstract void OnCheckForUpdates();

        /// <summary>
        /// Get the name of the current executable (for error messages).
        /// </summary>
        /// <returns>the name of the current executable.</returns>
        protected string GetExecutableName()
        {
            if (this.assemblyName == null)
            {
                this.assemblyName = string.Empty;
                Assembly entryAssembly = Assembly.GetEntryAssembly();
                if (entryAssembly != null)
                {
                    this.assemblyName = entryAssembly.GetName().Name;
                }
            }

            return this.assemblyName;
        }
        #endregion // Protected methods

        #region Private methods

        /// <summary>
        /// Start the process with a given configuration.
        /// </summary>
        /// <remarks>Used by controller.</remarks>
        /// <param name="config">The configuration to use (UTF8 encoded byte array).</param>
        /// <returns>True if successful, otherwise false.</returns>
        private bool StartProcess(byte[] config)
        {
            string configString = null;

            if (config != null && config.Length > 0)
            {
                try
                {
                    configString = Encoding.UTF8.GetString(config);
                }
                catch
                {
                    return false;
                }
            }

            return this.StartProcess(configString);
        }

        /// <summary>
        /// Start the process with a given configuration.
        /// </summary>
        /// <remarks>Used by controller.</remarks>
        /// <param name="config">The configuration to use.</param>
        /// <returns>True if successful, otherwise false.</returns>
        private bool StartProcess(string config)
        {
            try
            {
                if (!this.OnStart(config))
                {
                    return false;
                }

                if (this.process != null)
                {
                    try
                    {
                        return this.process.OnStart();
                    }
                    catch (Exception e)
                    {
                        this.ProcessException(e);
                        return false;
                    }
                }
            }
            catch (SecurityException e)
            {
                throw e;
            } 
            catch (ConfigurationException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception caught when starting process: {0}", e.Message);
                return false;
            }

            return true;
        }

        /// <summary>
        /// Stop the hosted process.
        /// </summary>
        /// <remarks>Subclasses should override OnStop() to trigger stuff at this point.</remarks>
        private void StopProcess()
        {
            this.OnStop();
        }

        /// <summary>
        /// Run the hosted process.
        /// </summary>
        private void RunLoop()
        {
            try
            {
                this.isRunning = true;
                bool continueMakingCallsToPeer = true;

                do
                {
                    Thread.Sleep(this.ProcessNetworkStateIntervalMs);

                    if (continueMakingCallsToPeer)
                    {
                        if (this.process != null)
                        {
                            try
                            {
                                if (!this.process.OnPerformRegularTasks(this.ProcessNetworkStateIntervalMs))
                                {
                                    this.isRunning = false;
                                    break;
                                }
                            }
                            catch (Exception e)
                            {
                                continueMakingCallsToPeer = false;
                                this.ProcessException(e);
                            }
                        }

                        this.OnProcessRegularEvents();
                    }

                    // If we are not connected to a harness server and we are no longer making calls to
                    // the peer becuase of a previous exception then we should exit.
                    if (this.controlStream == null && !continueMakingCallsToPeer)
                    {
                        this.isRunning = false;
                        break;
                    }

                    this.ProcessRequest();
                }
                while (this.isRunning);

                if (this.process != null && continueMakingCallsToPeer)
                {
                    try
                    {
                        this.process.OnShutdown();
                    }
                    catch (Exception e)
                    {
                        this.ProcessException(e);
                    }
                }

                this.StopProcess();
            }
            finally
            {
                // Ensure the peer stops correctly and that the IsRunning flag is not set.
                if (this.isRunning)
                {
                    try
                    {
                        this.StopProcess();
                    }
                    catch
                    {
                    }
                }

                this.isRunning = false;
            }
        }

        /// <summary>
        /// Report and store the innermost exception of an exception.
        /// </summary>
        /// <param name="e">The exception being processed.</param>
        private void ProcessException(Exception e)
        {
            if (e.InnerException != null)
            {
                this.ProcessException(e.InnerException);
            }
            else
            {
                System.Console.WriteLine("Exception caught : {0}", e.Message);
                System.Console.WriteLine(e.StackTrace);
                this.lastException = e;

                // GermHarness process should just exit as soon as the exception is caught.
                Environment.Exit((int)ExitCode.Others);
            }
        }

        /// <summary>
        /// Process request received from the controller.
        /// </summary>
        private void ProcessRequest()
        {
            if (this.controlStream != null)
            {
                CustomRequest request = this.controlStream.PopRequest();

                if (request != null)
                {
                    byte[] reply = null;
                    switch (request.Type)
                    {
                        case (int)CustomRequestType.Status:
                            using (MemoryStream stream = new MemoryStream())
                            {
                                if (this.OnWriteStatus(stream))
                                {
                                    stream.Flush();
#if DEBUG
                                    System.Console.WriteLine("Status response {0} bytes", stream.Length);
#endif
                                    reply = stream.ToArray();
                                }
                                else
                                {
#if DEBUG
                                    System.Console.WriteLine("Status response : Not logged in");
#endif
                                }
                            }
                    
                            break;

                        case (int)CustomRequestType.Update:
                            this.OnCheckForUpdates();
                            break;

                        case (int)CustomRequestType.StopPeer:
                            this.isRunning = false;
                            break;

                        case (int)CustomRequestType.StartPeer:
                            System.Console.WriteLine("Starting peer ... ");
                            bool successful = this.StartProcess(request.Message);
                            reply = new byte[] { successful ? (byte)1 : (byte)0 };
                            break;

                        case (int)CustomRequestType.Ping:
                            reply = this.WritePingReply();
                            break;

                        default:
                            if (this.process != null)
                            {
                                try
                                {
                                    reply = this.process.OnProcessRequest(request.Type, request.Message);
                                }
                                catch (Exception e)
                                {
                                    this.ProcessException(e);
                                }
                            }

                            break;
                    }

                    this.controlStream.PushResponse(reply);
                }
            }
        }

        /// <summary>
        /// Write the reply to a ping request.
        /// </summary>
        /// <returns>The reply to a ping request.</returns>
        private byte[] WritePingReply()
        {
            try
            {
                using (MemoryStream stream = new MemoryStream())
                {
                    using (XmlWriter writer = XmlWriter.Create(stream))
                    {
                        writer.WriteStartDocument();
                        writer.WriteStartElement("Status", string.Empty);
                        writer.WriteAttributeString("Version", "1.1");
                        writer.WriteAttributeString("State", ((int)this.OnGetPing()).ToString());

                        if (this.lastException != null)
                        {
                            writer.WriteStartElement("Exception", string.Empty);

                            writer.WriteStartElement(string.Empty, "Message", string.Empty);
                            writer.WriteCData(this.lastException.Message);
                            writer.WriteFullEndElement();

                            writer.WriteStartElement(string.Empty, "StackTrace", string.Empty);
                            writer.WriteCData(this.lastException.StackTrace);
                            writer.WriteFullEndElement();

                            writer.WriteFullEndElement();
                        }

                        writer.WriteFullEndElement();
                        writer.WriteEndDocument();
                        writer.Flush();
                    }

                    return stream.ToArray();
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Trace.TraceError("Exception while writing ping reply : {0}", e.Message);
            }

            return null;
        }

        /// <summary>
        /// Set the port to connect to the controller on.
        /// </summary>
        /// <param name="port">The port to use.</param>
        private void SetPort(ushort port)
        {
            this.port = port;
            this.portIsSet = true;
        }

        /// <summary>
        /// Set the control ID to identify the harness to the controller.
        /// </summary>
        /// <param name="controlId">The control ID to use.</param>
        private void SetControlId(int controlId)
        {
            this.controlId = controlId;
            this.controlIdIsSet = true;
        }

        #endregion // Private methods
    }
}
