﻿//-----------------------------------------------------------------------
// <copyright file="IFileSystem.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2010 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace GermHarness
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// Interface for abstracting the file system so it can be mocked in unit tests.
    /// </summary>
    internal interface IFileSystem
    {
        /// <summary>
        /// Determines whethe the specified file exists.
        /// </summary>
        /// <param name="path">The path to check.</param>
        /// <returns>True if the file exists, otherwise false.</returns>
        bool FileExists(string path);

        /// <summary>
        /// Read all the lines in a given file.
        /// </summary>
        /// <param name="path">The path of the file to read.</param>
        /// <returns>An array of the lines in the file.</returns>
        string[] ReadAllLines(string path);
    }
}
