﻿//-----------------------------------------------------------------------
// <copyright file="Program.cs" company="National ICT Australia Ltd">
//     Copyright (c) 2010 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace LaunchGerm
{
    using System;
    using System.Collections.Generic;
    using System.ServiceProcess;
    using System.Text;
    using System.Threading;

    using GermServer;

    using Mono.Options;

    /// <summary>
    /// The main entry of the LaunchGerm.exe used to start the Germ that listen on the specified port.
    /// </summary>
    internal class Program
    {
        /// <summary>
        /// Port to listen on.
        /// </summary>
        private static int port = 21253;

        /// <summary>
        /// Control center address.
        /// </summary>
        private static string controlCenterAddress;

        /// <summary>
        /// OptionSet for passing command line arguments
        /// </summary>
        private static OptionSet optionSet = new OptionSet()
        {
            { "h|help", "show this message and exit.", v =>
                {
                    if (v != null)
                    {
                        Program.WriteUsageAndExit();
                    }
                }
            },
            { "p|port=", "the port to listen on.", (ushort v) => port = v },
            { "c|connect-to=", "connect to given control center address automatically, e.g. 192.168.0.12:21200", v => controlCenterAddress = v } 
        };

        /// <summary>
        /// Exit codes enumeration.
        /// </summary>
        private enum ExitCodes : int
        {
            /// <summary>
            /// Succeed exit code.
            /// </summary>
            Success = 0,

            /// <summary>
            /// Exit due to initialization failure.
            /// </summary>
            InitializationFailure,

            /// <summary>
            /// Unhandled exception occured.
            /// </summary>
            UnhandledException
        }

        /// <summary>
        /// Main function of LaunchGerm.exe project.
        /// </summary>
        /// <param name="args">Command line arguments</param>
        /// <returns>Return with exit codes.</returns>
        public static int Main(string[] args)
        {
            try
            {
                List<string> extras = optionSet.Parse(args);

                if (extras.Count > 0)
                {
                    Program.HandleUnknownArguments(extras.ToArray());
                }
            }
            catch (OptionException e)
            {
                string executable = AppDomain.CurrentDomain.FriendlyName;
                Console.WriteLine("{0}: {1}", executable, e.Message);
                Console.WriteLine("Try `{0} --help' for more information.", executable);

                return (int)ExitCodes.UnhandledException;
            }

            GermService service = new GermService(port, controlCenterAddress);
            service.Start();

            Thread.Sleep(Timeout.Infinite);

            return (int)ExitCodes.Success;
        }

        /// <summary>
        /// Write error message listing unknown arguments.
        /// </summary>
        /// <param name="arguments">Unknown arguments.</param>
        private static void HandleUnknownArguments(string[] arguments)
        {
            string executable = AppDomain.CurrentDomain.FriendlyName;
            Console.Write("{0}: Unknown argument(s):", executable);
            foreach (string arg in arguments)
            {
                Console.Write(" " + arg);
            }

            Console.WriteLine(".");
            Console.WriteLine("Try `{0} --help' for more information.", executable);
        }

        /// <summary>
        /// Write usage instructions and exit.
        /// </summary>
        private static void WriteUsageAndExit()
        {
            string executable = AppDomain.CurrentDomain.FriendlyName;
            Console.WriteLine(string.Format("Usage: {0} [Options]", executable));
            Console.WriteLine(string.Format("Start {0} asp.net host", executable));
            Console.WriteLine(string.Empty);
            Console.WriteLine("Options:");
            Program.optionSet.WriteOptionDescriptions(Console.Out);

            Environment.Exit((int)ExitCodes.Success);
        }
    }
}
