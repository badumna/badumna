﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl"
>
  <xsl:output method="xml" indent="yes"/>

  <xsl:template match="/">
    <div>

      <xsl:choose>

        <xsl:when test="Manifest/ShowDescription &lt; 1">
          <div class="description">
            <p>
              Preliminary package download required (<xsl:value-of select="Manifest/LatestCompletePackageSizeKB"/>KB)
            </p>
          </div>
        </xsl:when>

        <xsl:otherwise>

          <div class="details" >
            <table border="0" >
              <tr>
                <td>Application version</td>
                <td>
                  <xsl:value-of select="Manifest/Major"/>.<xsl:value-of select="Manifest/Minor"/>.<xsl:value-of select="Manifest/Build"/>.<xsl:value-of select="Manifest/Revision"/>
                </td>
              </tr>
              <tr>
                <td>Protocol version</td>
                <td>
                  0.<xsl:value-of select="Manifest/ProtocolVersion"/>
                </td>
              </tr>
              <tr>
                <td>Date</td>
                <td>
                  <xsl:value-of select="Manifest/Date"/>
                </td>
              </tr>
              <tr>
                <td>File size</td>
                <td>
                  <xsl:value-of select="Manifest/PackageSizeKB"/> KB
                </td>
              </tr>
            </table>
          </div>

          <h4>
            <xsl:apply-templates select="Manifest/Description/Header"/>
          </h4>
          <div class="description" >
            <xsl:apply-templates select="Manifest/Description/Comments"/>
            <ul>
              <xsl:for-each select="Manifest/Description/Item">
                <li>
                  <xsl:value-of select="."/>
                </li>
              </xsl:for-each>
            </ul>
          </div>

        </xsl:otherwise>
      </xsl:choose>
    </div>
  </xsl:template>
</xsl:stylesheet>
