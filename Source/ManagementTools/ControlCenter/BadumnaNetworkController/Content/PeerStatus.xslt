﻿<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html" indent="yes"/>

  <xsl:template match="/">
    <div id='peerStatus'>
      <xsl:apply-templates ></xsl:apply-templates>
    </div>
  </xsl:template>

  <xsl:template match="Exception">
    <p>
      <span>
        Exception message : <xsl:value-of select="Message"/>
      </span>
    </p>
    <p>
      Exception message : <xsl:value-of select="StackTrace"/>
    </p>
  </xsl:template>

  <xsl:template match="Status">
    <div>
      <table class="generalListTable">
        <tr>
          <th colspan="2">Connectivity :</th>
        </tr>
        <tr>
          <td>Public address</td>
          <td>
            <xsl:value-of select="Connectivity/PublicAddress"/>
          </td>
        </tr>
        <tr>
          <td>Private address</td>
          <td>
            <xsl:value-of select="Connectivity/PrivateAddress"/>
          </td>
        </tr>
        <tr>
          <td>Active connections</td>
          <td>
            <xsl:value-of select="Connectivity/ActiveConnections"/>
          </td>
        </tr>
        <tr>
          <td>Initializing connections</td>
          <td>
            <xsl:value-of select="Connectivity/InitializingConnections"/>
          </td>
        </tr>
        <tr>
          <th colspan="2">Discovery :</th>
        </tr>
        <tr>
          <td>Method</td>
          <td>
            <xsl:value-of select="Discovery/Method" />
          </td>
        </tr>
      </table>
    </div>
  </xsl:template>

</xsl:stylesheet>
