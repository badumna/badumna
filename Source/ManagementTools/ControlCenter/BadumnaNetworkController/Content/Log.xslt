﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl"
    xmlns:e2e="http://schemas.microsoft.com/2004/06/E2ETraceEvent"
    xmlns:sys="http://schemas.microsoft.com/2004/06/windows/eventlog/system"
>
  <xsl:output method="html" indent="yes"/>

  <xsl:template match="/Log">
    <html>
      <body>
        <div id='Log'>
          <textarea cols='110' rows='20' readonly='true'>
              <xsl:for-each select="e2e:E2ETraceEvent" >
                <xsl:value-of select="sys:System/sys:TimeCreated/@SystemTime"/>
                <xsl:text> -&#x09;</xsl:text>                
                <xsl:value-of select="e2e:ApplicationData"/>
                <xsl:text>&#x0a;</xsl:text>
              </xsl:for-each>
            </textarea>          
        </div>
      </body>
    </html>
  </xsl:template>

</xsl:stylesheet>
