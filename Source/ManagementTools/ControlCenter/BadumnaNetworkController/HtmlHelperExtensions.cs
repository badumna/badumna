﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Routing;
using System.Xml;
using System.Xml.Xsl;
using System.IO;

namespace System.Web.Mvc
{
    public static class HtmlHelperExtensions
    {
        private static int UniqueIdCounter = 0;

        public static string UniqueId(this HtmlHelper htmlHelper)
        {
            return string.Format("id_{0}", HtmlHelperExtensions.UniqueIdCounter++);
        }

        /// <summary>
        /// Returns a file input element by using the specified HTML helper and the name of the form field.
        /// </summary>
        /// <param name="htmlHelper">The HTML helper instance that this method extends.</param>
        /// <param name="name">The name of the form field and the <see cref="member">System.Web.Mvc.ViewDataDictionary</see> key that is used to look up the validation errors.</param>
        /// <returns>An input element that has its type attribute set to "file".</returns>
        public static string FileBox(this HtmlHelper htmlHelper, string name)
        {
            return htmlHelper.FileBox(name, (object)null);
        }

        /// <summary>
        /// Returns a file input element by using the specified HTML helper, the name of the form field, and the HTML attributes.
        /// </summary>
        /// <param name="htmlHelper">The HTML helper instance that this method extends.</param>
        /// <param name="name">The name of the form field and the <see cref="member">System.Web.Mvc.ViewDataDictionary</see> key that is used to look up the validation errors.</param>
        /// <param name="htmlAttributes">An object that contains the HTML attributes for the element. The attributes are retrieved through reflection by examining the properties of the object. The object is typically created by using object initializer syntax.</param>
        /// <returns>An input element that has its type attribute set to "file".</returns>
        public static string FileBox(this HtmlHelper htmlHelper, string name, object htmlAttributes)
        {
            return htmlHelper.FileBox(name, new RouteValueDictionary(htmlAttributes));
        }

        /// <summary>
        /// Returns a file input element by using the specified HTML helper, the name of the form field, and the HTML attributes.
        /// </summary>
        /// <param name="htmlHelper">The HTML helper instance that this method extends.</param>
        /// <param name="name">The name of the form field and the <see cref="member">System.Web.Mvc.ViewDataDictionary</see> key that is used to look up the validation errors.</param>
        /// <param name="htmlAttributes">An object that contains the HTML attributes for the element. The attributes are retrieved through reflection by examining the properties of the object. The object is typically created by using object initializer syntax.</param>
        /// <returns>An input element that has its type attribute set to "file".</returns>
        public static string FileBox(this HtmlHelper htmlHelper, string name, IDictionary<String, Object> htmlAttributes)
        {
            var tagBuilder = new TagBuilder("input");
            tagBuilder.MergeAttributes(htmlAttributes);
            tagBuilder.MergeAttribute("type", "file", true);
            tagBuilder.MergeAttribute("name", name, true);
            tagBuilder.GenerateId(name);

            ModelState modelState;
            if (htmlHelper.ViewData.ModelState.TryGetValue(name, out modelState))
            {
                if (modelState.Errors.Count > 0)
                {
                    tagBuilder.AddCssClass(HtmlHelper.ValidationInputCssClassName);
                }
            }

            return tagBuilder.ToString(TagRenderMode.SelfClosing);
        }

        public static HtmlString HelpLabel(this HtmlHelper htmlHelper, string labeltext)
        {
            return htmlHelper.RenderHtml(string.Format(@"<lablel onmouseover=""showInfo('#{0}Info');"" >{0}</label>", labeltext));
        }

        public static HtmlString HelpLabel(this HtmlHelper htmlHelper, string labelTarget, string labeltext)
        {
            return htmlHelper.RenderHtml(string.Format(@"<lablel for='{0}' onmouseover=""showInfo('#{0}Info');"" >{1}</label>", 
                labelTarget.Trim().Replace(" ", "_"), labeltext));
        }

        public static HtmlString RenderHtml(this HtmlHelper htmlHelper, string html)
        {
            ViewContext context = htmlHelper.ViewContext;
            if (!string.IsNullOrEmpty(html))
            {
                return new HtmlString(html);
            }

            return null;
        }

        public static HtmlString RenderXML(this HtmlHelper htmlHelper, string xml, string XSLTPath, Dictionary<string, string> xslArgParams)
        {
            if (!string.IsNullOrEmpty(xml))
            {
                XmlDocument xmlDocument = new XmlDocument();

                xmlDocument.LoadXml(xml);
                return htmlHelper.RenderXML(xmlDocument, XSLTPath, xslArgParams);
            }

            return null;
        }

        public static HtmlString RenderXML(this HtmlHelper htmlHelper, XmlDocument xmlDocument, string XSLTPath, Dictionary<string, string> xslArgParams)
        {
            ViewContext context = htmlHelper.ViewContext;
            XsltArgumentList xslArgs = new XsltArgumentList();

            if (xslArgParams != null)
            {
                foreach (string key in xslArgParams.Keys)
                {
                    xslArgs.AddParam(key, null, xslArgParams[key]);
                }
            }

            XslCompiledTransform transform = new XslCompiledTransform();
            transform.Load(XSLTPath);

            StringWriter writer = new StringWriter();
            transform.Transform(xmlDocument, xslArgs, writer);

            return new HtmlString(writer.ToString());
        }

    }
}
