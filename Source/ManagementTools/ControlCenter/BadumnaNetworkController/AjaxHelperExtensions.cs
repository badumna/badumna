﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Routing;
using System.Xml;
using System.Xml.Xsl;
using System.Web.Mvc.Ajax;

namespace System.Web.Mvc
{
    public static class AjaxHelperExtensions
    {
        private const string DefaultBustImageSource = "/Content/wait16.gif";

        private static int UniqueCounter = 0;

        public static string BusyActionLink(this AjaxHelper ajaxHelper, string linkText, string updateTarget, string actionName)
        {
            return ajaxHelper.BusyActionLink(linkText, updateTarget, actionName, new object { }, AjaxHelperExtensions.DefaultBustImageSource);
        }

        public static string BusyActionLink(this AjaxHelper ajaxHelper, string linkText, string updateTarget, string actionName, object routeValues)
        {
            return ajaxHelper.BusyActionLink(linkText, updateTarget, actionName, routeValues, AjaxHelperExtensions.DefaultBustImageSource);
        }

        public static string BusyActionLink(this AjaxHelper ajaxHelper, string linkText, string updateTarget, string actionName,
            string controller)
        {
            return ajaxHelper.BusyActionLink(linkText, updateTarget, actionName, controller, new object { }, AjaxHelperExtensions.DefaultBustImageSource);
        }

        public static string BusyActionLink(this AjaxHelper ajaxHelper, string linkText, string updateTarget, string actionName,
            string controller, object routeValues)
        {
            return ajaxHelper.BusyActionLink(linkText, updateTarget, actionName, controller, routeValues, AjaxHelperExtensions.DefaultBustImageSource);
        }

        public static string BusyActionLink(this AjaxHelper ajaxHelper, string linkText, string updateTarget, string actionName,
            string controller, object routeValues, string busyImageSource)
        {
            string updateTag = String.Format("upd_{0}", AjaxHelperExtensions.UniqueCounter);
            string enableTag = String.Format("enb_{0}", AjaxHelperExtensions.UniqueCounter++);

            StringBuilder builder = new StringBuilder();

            builder.AppendFormat(@"<img id='{0}' src='{1}' style='display: none;' alt='Updating ...' />", updateTag, busyImageSource);
            builder.AppendFormat(@"<span id='{0}'>", enableTag);
            builder.Append(
                ajaxHelper.ActionLink(linkText, actionName, controller, routeValues,
                new AjaxOptions()
                    {
                        UpdateTargetId = updateTarget,
                        LoadingElementId = updateTag,
                        OnBegin = "function(){ return $('#" + enableTag + "').hide(); }",
                        OnComplete = "function(){ return $('#" + enableTag + "').show(); }"
                    }));
            builder.Append("</span>");

            return builder.ToString();
        }

        public static string BusyActionLink(this AjaxHelper ajaxHelper, string linkText, string updateTarget, string actionName,
            object routeValues, string busyImageSource)
        {
            string updateTag = String.Format("upd_{0}", AjaxHelperExtensions.UniqueCounter);
            string enableTag = String.Format("enb_{0}", AjaxHelperExtensions.UniqueCounter++);

            StringBuilder builder = new StringBuilder();

            builder.AppendFormat(@"<img id='{0}' src='{1}' style='display: none;' alt='Updating ...' />", updateTag, busyImageSource);
            builder.AppendFormat(@"<span id='{0}'>", enableTag);
            builder.Append(
                ajaxHelper.ActionLink(linkText, actionName, routeValues,
                new AjaxOptions()
                {
                    UpdateTargetId = updateTarget,
                    LoadingElementId = updateTag,
                    OnBegin = "function(){ return $('#" + enableTag + "').hide(); }",
                    OnComplete = "function(){ return $('#" + enableTag + "').show(); }"
                }));
            builder.Append("</span>");

            return builder.ToString();
        }

    }
}
