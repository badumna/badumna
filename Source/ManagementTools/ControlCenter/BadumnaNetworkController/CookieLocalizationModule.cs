﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Threading;
using System.Globalization;

namespace BadumnaNetworkController
{
    public class CookieLocalizationModule : IHttpModule
    {
        public void Dispose()
        {
        }

        public void Init(HttpApplication context)
        {
            context.BeginRequest += new EventHandler(context_BeginRequest);
        }

        void context_BeginRequest(object sender, EventArgs e)
        {
            // eat the cookie (if any) and set the culture
            if (HttpContext.Current.Request.Cookies["lang"] != null)
            {
                try
                {
                    HttpCookie cookie = HttpContext.Current.Request.Cookies["lang"];
                    string lang = cookie.Value;
                    var culture = new CultureInfo(lang);
                    Thread.CurrentThread.CurrentCulture = culture;
                    Thread.CurrentThread.CurrentUICulture = culture;
                }
                catch { }
            }
        }
    }
}
