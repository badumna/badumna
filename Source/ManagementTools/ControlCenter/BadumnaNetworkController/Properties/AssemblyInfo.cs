using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("BadumnaNetworkController")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("05430d80-e264-46d0-b1c9-24398851a0da")]

[assembly: System.Runtime.CompilerServices.InternalsVisibleTo("BadumnaNetworkController.Tests, PublicKey=00240000048000009400000006020000002400005253413100040000010001007de84bbfe3dfb92d8a50a9b0b804eb07a02d28bb5b5665246b5a338a47df060263efe25fb2be507841aa9247a054092558cce470818f8c6ea295f8714f6e3f7494869e0b14c52f26a41e0e83f280e094e2c54150653db0b1fff68ea4ffba589787839be3f40bb6c3bc62a251ab54e60b0c1ff6c8e9afd8bf0be402929f40f4b6")]
