﻿using System;
using System.Xml.Serialization;
using System.Collections.Generic;

using Germinator;

namespace BadumnaNetworkController.Models
{
    [XmlRoot(ElementName = "Germinators")]
    public class Germinators
    {
        public static Germinators Instance = new Germinators();

        [XmlElement("PrivateKeyFilename", Order = 0)]
        public string PrivateKeyFilename;

        [XmlArray("Clients", Order = 1)]
        [XmlArrayItem(ElementName = "Client", Type = typeof(GermClient))]
        public GermClient[] Clients                      // Just for serialization
        {
            get { return new List<GermClient>(this.mGerms.Values).ToArray(); }
            set
            {
                foreach (GermClient client in value)
                {
                    this.mGerms.Add(client.Name, new GermClient(client.Name, this.PrivateKeyFilename));
                }
            }
        }

        [XmlIgnore]
        public int Count { get { return this.mGerms.Count; } }

        private Dictionary<string, GermClient> mGerms = new Dictionary<string, GermClient>();

        public Germinators()
        {
        }

        public void SetPrivateKey(string privateKeyFile)
        {
            this.PrivateKeyFilename = privateKeyFile;
        }

        public IEnumerable<GermClient> ListGerms()
        {
            return this.mGerms.Values;
        }

        public GermClient AddGerm(string hostAndPort)
        {
            GermClient client = new GermClient(hostAndPort, this.PrivateKeyFilename);

            if (!this.mGerms.ContainsKey(client.Name))
            {
                this.mGerms.Add(client.Name, client);
            }
            return client;
        }

        public GermClient AddGerm(string host, int port)
        {
            return this.AddGerm(string.Format("{0}:{1}", host, port));
        }

        public bool Contains(string name)
        {
            return this.mGerms.ContainsKey(name);
        }

        public GermClient GetGerm(string name)
        {
            GermClient client = null;
            if (this.mGerms.TryGetValue(name, out client))
            {
                return client;
            }

            return null;
        }

        public void RemoveGerm(string name)
        {
            if (this.mGerms.ContainsKey(name))
            {
                this.mGerms.Remove(name);
            }
        }

    }
}
