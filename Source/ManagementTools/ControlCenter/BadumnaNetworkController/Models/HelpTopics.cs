﻿//---------------------------------------------------------------------------------
// <copyright file="HelpTopics.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
// <summary>Help topic class.</summary>
//---------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

using Germinator;

using Germinator.Components;

namespace BadumnaNetworkController.Models
{
    /// <summary>
    /// Help Topic class has responsibility to add a new help topic and retrive a help topic from the xml file.
    /// </summary>
    public class HelpTopics
    {
        /// <summary>
        /// An instance of <see cref="HelpTopic"/>.
        /// </summary>
        private static HelpTopics instance;

        /// <summary>
        /// List of help topics.
        /// </summary>
        private List<string> topics;

        /// <summary>
        /// A xml document.
        /// </summary>
        private XDocument document;

        /// <summary>
        /// The directory path where the help.xml file is stored.
        /// </summary>
        private string directory;

        /// <summary>
        /// Initializes a new instance of the <see cref="HelpTopics"/> class.
        /// </summary>
        /// <param name="directory">The directory path where the help.xml file is stored.</param>
        public HelpTopics(string directory)
        {
            this.topics = new List<string>(new string[] { "Badumna", "Components", "Germs", "InstallGerms", "Networks", "Services" });

            foreach (ComponentView component in GerminatorFacade.Instance.GetAllComponents())
            {
                this.topics.Add(component.Index);
            }

            this.directory = directory;

            try
            {
                XDocument document = XDocument.Load(System.IO.Path.Combine(directory, Resources.HelpDocumentFilename));
                this.document = document;
            }
            catch (Exception e)
            {
                DebugConsole.Instance.EnqueueTraceInfo(string.Format("Failed to load help file : {0}", e.Message), System.Diagnostics.TraceLevel.Error);
            }
        }

        /// <summary>
        /// Gets or sets the instance of <see cref="HelpTopics"/>.
        /// </summary>
        /// <value>Instance of <see cref="HelpTopics"/>.</value>
        public static HelpTopics Instance 
        { 
            get { return instance; }
            set { instance = value; }
        }

        /// <summary>
        /// Gets the list of help topics.
        /// </summary>
        /// <value>List of help topics.</value>
        public List<string> Topics 
        { 
            get { return this.topics; } 
        }

        /// <summary>
        /// Get the list of help items given the help topic name.
        /// </summary>
        /// <param name="topic">Help topic name.</param>
        /// <returns>Return list of help items for a given topic name.</returns>
        public IEnumerable<HelpItem> GetItems(string topic)
        {
            if (this.document == null)
            {
                return new HelpItem[] { };
            }

            return from item in this.document.Descendants("Item") where (string)item.Parent.Attribute("Name") == topic select new HelpItem(item);
        }

        /// <summary>
        /// Get a specific help item from the topic name mentioned in the parameter.
        /// </summary>
        /// <param name="topic">Help topic name.</param>
        /// <param name="itemName">Help item name.</param>
        /// <returns>Return a specific help item.</returns>
        public HelpItem GetItem(string topic, string itemName)
        {
            if (this.document == null)
            {
                return null;
            }

            XElement itemElement =
                (from item in this.document.Descendants("Item")
                 where (string)item.Attribute("Name") == itemName && (string)item.Parent.Attribute("Name") == topic
                 select item).FirstOrDefault();

            if (itemElement != null)
            {
                return new HelpItem(itemElement);
            }

            return null;
        }

        /// <summary>
        /// Get the xml format for the corresponding topic name.
        /// </summary>
        /// <param name="topicName">Help topic name.</param>
        /// <returns>Return the help topinc in xml format.</returns>
        public string GetTopicXml(string topicName)
        {
            if (this.document == null)
            {
                return string.Empty;
            }

            return
                (from topic in this.document.Descendants("Topic")
                 where (string)topic.Attribute("Name") == topicName
                 select topic.Value).FirstOrDefault();
        }

        /// <summary>
        /// Add a new help item to the specified topic name.
        /// </summary>
        /// <param name="topicName">Help topic name.</param>
        /// <param name="itemName">A new help item name.</param>
        /// <param name="title">Help title.</param>
        /// <param name="text">help information.</param>
        /// <returns>Return the HelpItem class represent a new item that just been added.</returns>
        public HelpItem AddItem(string topicName, string itemName, string title, string text)
        {
            if (this.document == null)
            {
                this.document = new XDocument();
                this.document.Add(new XElement("Topics"));
            }

            XElement topicElement = (from topic in this.document.Descendants("Topic") where (string)topic.Attribute("Name") == topicName select topic).FirstOrDefault();

            if (topicElement == null)
            {
                topicElement = new XElement("Topic", new XAttribute("Name", topicName));
                this.document.Element("Topics").Add(topicElement);
            }

            XElement itemElement =
                (from item in topicElement.Descendants("Item") where (string)item.Attribute("Name") == itemName select item).FirstOrDefault();

            if (itemElement != null)
            {
                itemElement.RemoveNodes();
                itemElement.Add(new XText(text));

                XAttribute titleAttribute = itemElement.Attribute("Title");
                if (titleAttribute == null)
                {
                    itemElement.Add(new XAttribute("Title", title));
                }
                else
                {
                    titleAttribute.Value = title;
                }
            }
            else
            {
                itemElement = new XElement(
                                        "Item",
                                        new XAttribute("Name", itemName),
                                        new XAttribute("Title", title),
                                        new XText(text));
                topicElement.Add(itemElement);
            }

            this.document.Save(System.IO.Path.Combine(this.directory, Resources.HelpDocumentFilename));
            return new HelpItem() { Name = itemName, Title = title, Text = text };
        }

        /// <summary>
        /// Get all topics from the current help document.
        /// </summary>
        /// <returns>Returns all topic items.</returns>
        public IEnumerable<string> GetAllTopics()
        {
            if (this.document == null)
            {
                return new string[] { };
            }

            return from topic in this.document.Descendants("Topic") select topic.Attribute("Name").Value;
        }
    }
}
