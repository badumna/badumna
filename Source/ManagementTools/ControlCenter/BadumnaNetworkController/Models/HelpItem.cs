﻿//---------------------------------------------------------------------------------
// <copyright file="HelpItem.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
// <summary>Help item class.</summary>
//---------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;

namespace BadumnaNetworkController.Models
{
    /// <summary>
    /// Help item class is used to store help information including the title, and topic name.
    /// </summary>
    public class HelpItem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="HelpItem"/> class.
        /// </summary>
        internal HelpItem()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="HelpItem"/> class.
        /// </summary>
        /// <param name="element">XElement (Represent a xml element).</param>
        internal HelpItem(XElement element)
        {
            XAttribute titleAttribute = element.Attribute("Title");
            XAttribute nameAttribute = element.Attribute("Name");

            this.Title = titleAttribute == null ? string.Empty : titleAttribute.Value;
            this.Text = element == null ? string.Empty : element.Value;
            this.Name = nameAttribute == null ? string.Empty : nameAttribute.Value;
        }

        /// <summary>
        /// Gets or sets the Help title.
        /// </summary>
        /// <value>Help title.</value>
        public string Title { get; set; }
        
        /// <summary>
        /// Gets or sets the help information.
        /// </summary>
        /// <value>Help information.</value>
        public string Text { get; set; }

        /// <summary>
        /// Gets or sets the topic name.
        /// </summary>
        /// <value>Topic name.</value>
        public string Name { get; set; }
    }
}
