﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

using Germinator.Components;

namespace BadumnaNetworkController.Models
{
    [XmlRoot(ElementName="Configuration")]
    public class NetworkConfiguration
    {
        [XmlAttribute(AttributeName="Name")]
        public string Name { get; set; }

        [XmlElement("Germinators")]
        public Germinators Germinators { get; set; }

        [XmlElement(ElementName="Options")]
        public BadumnaComponent Configuration { get; set; }
        
        public ComponentContainer Components = new ComponentContainer();

        public NetworkConfiguration()
        {
            this.Name = "Default";
        }

        public NetworkConfiguration(string name)
        {
            this.Name = name;
            this.Germinators = new Germinators();
            foreach (string componentName in ComponentContainer.Instance.ComponentNames)
            {
                this.Components.Add(componentName);
            }
        }

        internal void EnableComponent(string componentName, bool isEnabled)
        {
            ComponentView component = this.Components.GetComponent(componentName);
            if (component != null)
            {
                component.Instance.IsEnabled = isEnabled;
            }
        }

        public bool StartService(Germinator.Services.BaseService service)
        {
            if (service == null)
            {
                return false;
            }

            Germinator.Services.PeerService peerService = service as Germinator.Services.PeerService;
            if (peerService != null)
            {
                BadumnaComponent configComponent = this.Components.GetComponent("Network configuration").Instance as BadumnaComponent;
                peerService.StartPeerService(this.Germinators.ListGerms(), configComponent);
            }
            else
            {
                service.Start(this.Germinators.ListGerms(), "");
            }

            return true;
        }
    }
}
