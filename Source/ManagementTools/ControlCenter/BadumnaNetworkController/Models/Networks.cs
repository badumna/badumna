﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using System.Xml;
using System.IO;

using GermServer;
using Germinator;
using Germinator.Services;
using Germinator.Components;

namespace BadumnaNetworkController.Models
{
    public class Networks
    {
        public static Networks Instance;
        public static string SessionKey { get { return "Network"; } }

        public int ConfigurationCount { get { return this.mConfigurations.Count; } }
        public IEnumerable<NetworkConfiguration> Configurations { get { return this.mConfigurations.Values; } }
        public NetworkConfiguration this[string name]
        {
            get
            {
                NetworkConfiguration configuration = null;
                this.mConfigurations.TryGetValue(name, out configuration);
                return configuration;
            }
        }

        private string mFilename;
        private object mLoadLock = new object();
        private Dictionary<string, NetworkConfiguration> mConfigurations;


        public Networks(string directory)
        {
            this.mFilename = Path.Combine(directory, "Networks.xml");
            this.mConfigurations = new Dictionary<string, NetworkConfiguration>();
        }

        public void Add(NetworkConfiguration configuration)
        {
            if (!this.mConfigurations.ContainsKey(configuration.Name))
            {
                this.mConfigurations.Add(configuration.Name, configuration);
                this.Save();
            }
        }

        public void Remove(String configurationName)
        {
            if (this.mConfigurations.ContainsKey(configurationName))
            {
                this.mConfigurations.Remove(configurationName);
            }
        }


        public void Save()
        {
            lock (this.mLoadLock)
            {
                if (string.IsNullOrEmpty(this.mFilename))
                {
                    throw new InvalidOperationException("File name of Networks class must be set before save can be called.");
                }

                XmlSerializerNamespaces namespaces = new XmlSerializerNamespaces(new XmlQualifiedName[] 
            {
                new XmlQualifiedName("", "")
            });

                XmlWriterSettings settings = new XmlWriterSettings();
                settings.Indent = true;

                try
                {
                    using (XmlWriter writer = XmlWriter.Create(this.mFilename, settings))
                    {
                        XmlSerializer serializer = new XmlSerializer(typeof(NetworkConfiguration));
                        XmlSerializer germinatorSerializer = new XmlSerializer(typeof(Germinators));

                        writer.WriteStartDocument();
                        writer.WriteStartElement("Data");
                        writer.WriteAttributeString("Version", "1.0");
                        germinatorSerializer.Serialize(writer, Germinators.Instance, namespaces);

                        foreach (NetworkConfiguration configuration in this.mConfigurations.Values)
                        {
                            serializer.Serialize(writer, configuration, namespaces);
                        }

                        writer.WriteEndElement();
                    }
                }
                catch (Exception e)
                {
                    ;
                }
            }
        }

        public void Load()
        {
            lock (this.mLoadLock)
            {
                if (string.IsNullOrEmpty(this.mFilename))
                {
                    throw new InvalidOperationException("File name of Networks class must be set before load can be called.");
                }

                try
                {
                    if (File.Exists(this.mFilename))
                    {
                        using (XmlReader reader = XmlReader.Create(this.mFilename))
                        {
                            XmlSerializer serializer = new XmlSerializer(typeof(NetworkConfiguration));
                            XmlSerializer germinatorSerializer = new XmlSerializer(typeof(Germinators));

                            while (!reader.EOF && reader.Read())
                            {
                                while (germinatorSerializer.CanDeserialize(reader))
                                {
                                    Germinators.Instance = (Germinators)germinatorSerializer.Deserialize(reader);
                                }

                                while (serializer.CanDeserialize(reader))
                                {
                                    NetworkConfiguration configuration = (NetworkConfiguration)serializer.Deserialize(reader);
                                    this.mConfigurations.Add(configuration.Name, configuration);
                                }

                            }
                        }
                        this.PostLoad();
                    }
                }
                catch (Exception e)
                {
                    ;
                }
            }
        }

        public static void Load(string directory)
        {
            string dataDirectory = Path.Combine(directory, "App_Data");
            string packagesDirectory = Path.Combine(directory, "Packages");

            Germinator.Components.ComponentContainer.Instance.FindComponents(dataDirectory);
            Germinator.Services.BaseService.FindPackages(packagesDirectory);
            Germinator.Maintenance.MaintenanceTaskManager.Instance.StartMaintenanceThread();

            Networks.Instance = new Networks(dataDirectory);
            Networks.Instance.Load();
        }


        private void PostLoad()
        {
            // Get the list of running processes on each germ and update the service list accordingly
            foreach (NetworkConfiguration configuration in this.mConfigurations.Values)
            {
                if (configuration.Germinators != null)
                {
                    foreach (GermClient client in configuration.Germinators.ListGerms())
                    {
                        Operation op = client.QueueProcessListOpeation(
                            delegate(IAsyncResult ar)
                            {
                                ProcessListOperation operation = ar as ProcessListOperation;
                                if (operation != null && operation.IsCompleted)
                                {
                                    foreach (ProcessDescription process in operation.RunningProcesses)
                                    {
                                        ComponentView component = configuration.Components.GetComponent(process.Name);
                                        if (component != null)
                                        {
                                            BaseService service = component.Instance as BaseService;
                                            if (service != null)
                                            {
                                                service.ConfirmInstance(client, process);
                                            }
                                        }
                                    }
                                }
                            }, null);

                        op.AsyncWaitHandle.WaitOne(GermClient.DefaultOperationTimeout);
                    }
                }

                foreach (ComponentView component in configuration.Components.ListComponents())
                {
                    if (component.Instance != null && component.Instance.IsEnabled)
                    {
                        configuration.StartService(component.Instance as BaseService);
                    }
                }

            }
        }
    }
}
