﻿//---------------------------------------------------------------------------------
// <copyright file="PrestoManifesto.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Germinator.PrestoManifesto;

namespace BadumnaNetworkController.Models
{
    /// <summary>
    /// PrestoManifesto store the detail information of an application from the 
    /// package builder (i.e. Presto Manifesto).
    /// </summary>
    public class PrestoManifesto
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PrestoManifesto"/> class.
        /// </summary>
        public PrestoManifesto()
        {
            // Default Constructor
        }

        /// <summary>
        /// Gets or sets the name of the service / application.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the package version.
        /// </summary>
        public string PackageVersion { get; set; }

        /// <summary>
        /// Gets or sets the executable file for this application
        /// </summary>
        public string Executable { get; set; }

        /// <summary>
        /// Gets or sets the Base directory where this package is store.
        /// </summary>
        public string BaseDirectory { get; set; }

        /// <summary>
        /// Gets or sets the tree view Html code, it used to display the files
        /// in the client browser.
        /// </summary>
        public string TreeView { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the type of package update is complete or partial.
        /// </summary>
        public bool CompleteUpdate { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the pakcage extraction tools is included (i.e. ProcessMonitor.exe, ApplicationStarter.exe, etc.).
        /// </summary>
        public bool IncludeExtractionTools { get; set; }

        /// <summary>
        /// Gets or sets list of files included in this package.
        /// </summary>
        public Dictionary<string, FileData> Contents { get; set; }
    }
}
