﻿//---------------------------------------------------------------------------------
// <copyright file="RemoteProcess.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

using System;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using GermServer;

namespace BadumnaNetworkController.Models
{
    /// <summary>
    /// Remote Process information are store within this class.
    /// </summary>
    public class RemoteProcess
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RemoteProcess"/> class.
        /// </summary>
        /// <param name="description">Process description.</param>
        public RemoteProcess(ProcessDescription description)
        {
            this.Name = description.Name;
            this.ProcessId = description.ProcessId;
            this.Arguments = description.Args;
        }

        /// <summary>
        /// Gets or sets the name of the remote process.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the process id of the remote process.
        /// </summary>
        public int ProcessId { get; set; }

        /// <summary>
        /// Gets or sets the command line argument used by the remote process.
        /// </summary>
        public string Arguments { get; set; }
    }
}
