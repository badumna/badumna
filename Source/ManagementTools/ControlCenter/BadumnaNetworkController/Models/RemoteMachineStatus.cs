﻿//---------------------------------------------------------------------------------
// <copyright file="RemoteMachineStatus.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BadumnaNetworkController.Models
{
    /// <summary>
    /// RemoteMachineStatus class store the remote machine information (Note: remote machine
    /// thar run Germ on it.
    /// </summary>
    public class RemoteMachineStatus
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RemoteMachineStatus"/> class.
        /// </summary>
        public RemoteMachineStatus()
        {
            // Default Constructor
        }

        /// <summary>
        /// Gets or sets the total CPU load in percent.
        /// </summary>
        public string CPULoad { get; set; }

        /// <summary>
        /// Gets or sets the total free memory (RAM) in Mega bytes.
        /// </summary>
        public string MemoryAvailable { get; set; }

        /// <summary>
        /// Gets or sets total bytes sent by badumna peers per second. 
        /// </summary>
        public int TotalBytesSent { get; set; }

        /// <summary>
        /// Gets or sets total bytes received by badumna peers per second.
        /// </summary>
        public int TotalBytesReceived { get; set; }

        /// <summary>
        /// Gets or sets the time where these machine status are obtained.
        /// </summary>
        public DateTime Timestamp { get; set; }
    }
}
