﻿//-----------------------------------------------------------------------
// <copyright file="TestDriver.cs" company="National ICT Australia Ltd">
//     Copyright (c) 2012 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BadumnaNetworkController
{
    using System;
    using System.IO;
    using System.Web;
    using System.Web.Hosting;

    /// <summary>
    /// Test Driver class (this should be used for integration testing).
    /// </summary>
    /// <remarks>
    /// The idea of this class was taken from
    /// http://dotnetslackers.com/articles/aspnet/Unit-testing-an-HttpModule-with-NUnit-and-RhinoMock.aspx
    /// </remarks>
    public class TestDriver : MarshalByRefObject
    {
        /// <summary>
        /// Start the test.
        /// </summary>
        /// <param name="hostname">Host name.</param>
        /// <param name="page">Page name.</param>
        /// <param name="queryString">Query string.</param>
        public void Go(string hostname, string page, string queryString)
        {
            StringWriter writer = new StringWriter();
            SimpleWorkerRequest workerRequest = new SimpleWorkerRequest(page, queryString, writer);

            HttpRuntime.ProcessRequest(workerRequest);
        }

        /// <summary>
        /// Done test, close the http runtime.
        /// </summary>
        public void Done()
        {
            HttpRuntime.Close();
        }
    }
}
