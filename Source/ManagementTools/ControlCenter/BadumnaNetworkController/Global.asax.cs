﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

using BadumnaNetworkController.Models;

namespace BadumnaNetworkController
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        public static string BaseDirectory = HttpRuntime.AppDomainAppPath;

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            string defaultController = "Home";
            string defaultAction = "Index";

            routes.MapRoute(
                "Default",                                                               // Route name
                "{controller}/{action}/{id}",                                            // URL with parameters
                new { controller = defaultController, action = defaultAction, id = "" }  // Parameter defaults
            );
        }

        protected void Application_Start()
        {
            MvcApplication.RegisterRoutes(RouteTable.Routes);
            //RouteDebug.RouteDebugger.RewriteRoutesForTesting(RouteTable.Routes);
        }

        protected void Session_Start(Object sender, EventArgs e)
        {
        }

        protected void Session_End(Object sender, EventArgs e)
        {
            foreach (object sessionObject in this.Session)
            {
                IDisposable asDisposable = sessionObject as IDisposable;
                if (asDisposable != null)
                {
                    asDisposable.Dispose();
                }
            }
        }

        

        
    }
}