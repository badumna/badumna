﻿//-----------------------------------------------------------------------
// <copyright file="JsonHelper.cs" company="National ICT Australia Ltd">
//     Copyright (c) 2012 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BadumnaNetworkController
{
    using System;
    using System.Web.Script.Serialization;

    /// <summary>
    /// JsonHelper class is a helper class used to produced a json object for the flot plotting library.
    /// </summary>
    public class JsonHelper
    {
        /// <summary>
        /// Produced the data plot output in json format.
        /// </summary>
        /// <param name="data">Data need to be plotted.</param>
        /// <param name="label">Data labels.</param>
        /// <param name="extraOptions">Any additional plot options.</param>
        /// <returns>Return a json object with flot format.</returns>
        public static object DataPlotFormatter(string[] data, string[] label, string[] extraOptions)
        {
            return DataPlotFormatter(data, label, extraOptions, null);
        }

        /// <summary>
        /// Produced the data plot output in json format with any extra additional properties.
        /// </summary>
        /// <param name="data">Data need to be plotted.</param>
        /// <param name="label">Data labels.</param>
        /// <param name="extraOptions">Any additional plot options.</param>
        /// <param name="additionalProperties">Any additional properties.</param>
        /// <returns>Return a json object with flot format</returns>
        public static object DataPlotFormatter(string[] data, string[] label, string[] extraOptions, string additionalProperties)
        {
            if (label.Length != data.Length || extraOptions.Length != data.Length)
            {
                throw new Exception("Data, label and extra options have to be the same length.");
            }

            string result = string.Format("{{ \"plot\": [{0}]", GenerateStringFlotFormat(data, label, extraOptions));

            if (!string.IsNullOrEmpty(additionalProperties))
            {
                result += string.Format(", {0}", additionalProperties);
            }

            result += "}";

            JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
            return jsonSerializer.DeserializeObject(result);
        }

        /// <summary>
        /// Generating the data plot output for two different graph in json format.
        /// </summary>
        /// <remarks>
        /// Make this method more general if more than two graphs output need to be generated.
        /// </remarks>
        /// <param name="data1">Data for plot 1.</param>
        /// <param name="label1">Labels for plot 1.</param>
        /// <param name="data2">Data for plot 2.</param>
        /// <param name="label2">Labels for plot 2.</param>
        /// <param name="additionalProperties">Any additional properties.</param>
        /// <returns>Return a json object with flot format.</returns>
        public static object DataPlotFormatter(string[] data1, string[] label1, string[] data2, string[] label2, string additionalProperties)
        {
            if (label1.Length != data1.Length || label2.Length != data2.Length)
            {
                throw new Exception("Data, label have to be the same length.");
            }

            string result = string.Format("{{ \"plot\": [{0}]", GenerateStringFlotFormat(data1, label1, new string[data1.Length]));
            result += string.Format(", \"plot2\": [{0}]", GenerateStringFlotFormat(data2, label2, new string[data2.Length]));

            if (!string.IsNullOrEmpty(additionalProperties))
            {
                result += string.Format(", {0}", additionalProperties);
            }

            result += "}";

            JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
            return jsonSerializer.DeserializeObject(result);
        }

        /// <summary>
        /// Convert json string into json object.
        /// </summary>
        /// <param name="jsonString">Json string.</param>
        /// <returns>Returns a json object for given json string.</returns>
        public static object StringToJson(string jsonString)
        {
            JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
            return jsonSerializer.DeserializeObject(jsonString);
        }

        /// <summary>
        /// Generates the flot format data for a given plot data, label and extra options.
        /// </summary>
        /// <param name="data">Data need to be plotted.</param>
        /// <param name="label">Labels for each data.</param>
        /// <param name="extraOptions">Any extra flot options.</param>
        /// <returns>Returns a string in flot format for the given data.</returns>
        private static string GenerateStringFlotFormat(string[] data, string[] label, string[] extraOptions)
        {
            string result = string.Empty;

            for (int i = 0; i < data.Length; i++)
            {
                if (!string.IsNullOrEmpty(extraOptions[i]))
                {
                    result += string.Format("{{ \"data\": [{0}], \"label\": \"{1}\", {2} }}", data[i], label[i], extraOptions[i]);
                }
                else
                {
                    result += string.Format("{{ \"data\": [{0}], \"label\": \"{1}\" }}", data[i], label[i]);
                }

                if (i < data.Length - 1)
                {
                    result += ",";
                }
            }

            return result;
        }
    }
}