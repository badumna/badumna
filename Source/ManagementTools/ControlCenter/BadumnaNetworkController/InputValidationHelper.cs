﻿//-----------------------------------------------------------------------
// <copyright file="InputValidationHelper.cs" company="National ICT Australia Ltd">
//     Copyright (c) 2012 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BadumnaNetworkController
{
    using System.Text.RegularExpressions;

    /// <summary>
    /// Input validation class helper.
    /// </summary>
    public class InputValidationHelper
    {
        /// <summary>
        /// Validate the given input, only return true if it is alphanumeric, dash, underscore but no
        /// white spaces.
        /// </summary>
        /// <param name="name">Given name that need to be checked.</param>
        /// <returns>Return true if the given name is alphanumeric, dash, underscore but no space.</returns>
        public static bool ValidateAlphanumeric(string name)
        {
            Regex regex = new Regex(@"^[a-zA-Z0-9-_]*$");

            return regex.IsMatch(name);
        }

        /// <summary>
        /// Validate the given email address.
        /// </summary>
        /// <param name="email">Given email address.</param>
        /// <returns>Return true if email adrress is valid.</returns>
        public static bool ValidateEmail(string email)
        {
            // regex taken from http://www.codeproject.com/Articles/22777/Email-Address-Validation-Using-Regular-Expression
            Regex regex = new Regex(@"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
                                    + @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
                                    + @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                                    + @"([a-zA-Z]+[\w-]+\.)+[a-zA-Z]{2,4})$");
            return regex.IsMatch(email);
        }
    }
}