//---------------------------------------------------------------------------------
// <copyright file="PackageBuilderController.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;

using BadumnaNetworkController.Models;

using Germinator;
using Germinator.PrestoManifesto;

namespace BadumnaNetworkController.Controllers
{
    /// <summary>
    /// PackageBuilder controller has the same functionality as PrestoManifesto application to build packages for services 
    /// that run in the ControlCenter (Note : application and service are the same in this context).
    /// </summary>
    public class PackageBuilderController : Controller
    {
        /// <summary>
        /// List of the current available applications on PrestoManifesto.
        /// </summary>
        private static Dictionary<string, PrestoManifesto> applicationList = new Dictionary<string, PrestoManifesto>();

        /// <summary>
        /// ProcessTask delegate used to start asycn package build process.
        /// </summary>
        /// <param name="applicationName">Service name.</param>
        /// <param name="description">Package description.</param>
        /// <returns>Return the service name.</returns>
        private delegate string ProcessTask(string applicationName, string description);

        /// <summary>
        /// PackageBuilder index page, displaying all available services in the current project file.
        /// </summary>
        /// <returns>Retrun the PackageBuilder index page.</returns>
        public ActionResult Index()
        {
            // get the list of applications
            Configuration configuration = this.Session[HomeController.ConfigurationSessionKey] as Configuration;

            if (configuration != null && configuration.PrestoManifestoProject != null)
            {
                this.ViewData.Model = configuration.PrestoManifestoProject.Applications.AsEnumerable();
            }

            return View();
        }

        /// <summary>
        /// Add a new service to the current project.
        /// </summary>
        /// <returns>Return AddService.aspx page.</returns>
        public ActionResult AddService()
        {
            ViewData.Model = (IEnumerable<string>)Enum.GetNames(typeof(ServiceType));
            return View();
        }

        /// <summary>
        /// Post method of AddService, get the service name from the user and use it to create a new service and 
        /// add it to the current project.
        /// </summary>
        /// <param name="name">Service Name.</param>
        /// <param name="type">Service type.</param>
        /// <returns>Redirect to PackageBuilder index page.</returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AddService(string name, string type)
        {
            Configuration configuration = this.Session[HomeController.ConfigurationSessionKey] as Configuration;

            try
            {
                if (string.IsNullOrEmpty(name))
                {
                    this.ModelState.AddModelError("name", Resources.MustGiveName);
                }

                IEnumerable<PackageData> packages = configuration.PrestoManifestoProject.Applications.AsEnumerable();
                foreach (PackageData package in packages)
                {
                    if (package.Name.Equals(name))
                    {
                        this.ModelState.AddModelError("name", Resources.NameInUse);
                    }
                }

                if (!InputValidationHelper.ValidateAlphanumeric(name))
                {
                    this.ModelState.AddModelError("name", Resources.InvalidName);
                }

                if (string.IsNullOrEmpty(type))
                {
                    this.ModelState.AddModelError("type", Resources.MustGivenType);
                }

                if (!this.ModelState.IsValid)
                {
                    ViewData.Model = (IEnumerable<string>)Enum.GetNames(typeof(ServiceType));
                    return View();
                }
            }
            catch (Exception)
            {
                return View();
            }

            if (configuration != null)
            {
                string appPath = Request.PhysicalApplicationPath;
                string saveDir = string.Empty;
                if (!configuration.IsMono)
                {
                    saveDir = @"Packages\";
                }
                else
                {
                    saveDir = @"Packages/";
                }

                string fullPath = Path.Combine(appPath, saveDir);
                configuration.AddNewService(name, fullPath, (ServiceType)Enum.Parse(typeof(ServiceType), type, true));
            }

            return RedirectToAction("Details", new { name = name });
        }

        /// <summary>
        /// Remove a particular service based on the given service name.
        /// </summary>
        /// <param name="name">Service name.</param>
        /// <returns>Redirect to PackageBuilder index page.</returns>
        public ActionResult Remove(string name)
        {
            if (this.Request.IsAjaxRequest())
            {
                Configuration configuration = this.Session[HomeController.ConfigurationSessionKey] as Configuration;

                if (configuration != null)
                {
                    configuration.RemoveService(name);

                    // Remove the application from the application list
                    applicationList.Remove(name);

                    return PartialView("List", configuration.PrestoManifestoProject.Applications.AsEnumerable());
                }
            }
         
            return RedirectToAction("Index");
        }

        /// <summary>
        /// Display the details of a particular service name, includes the information about the current version of 
        /// the built package, and all files included for this particular service.
        /// </summary>
        /// <param name="name">Service name.</param>
        /// <returns>Return the Details.aspx page fot a particular service based on the given service name.</returns>
        public ActionResult Details(string name)
        {
            Configuration configuration = this.Session[HomeController.ConfigurationSessionKey] as Configuration;

            if (configuration != null)
            {
                PrestoManifesto prestoManifesto = null;
                string treeView = string.Empty;

                if (!applicationList.TryGetValue(name, out prestoManifesto))
                {
                    prestoManifesto = new PrestoManifesto();
                    prestoManifesto.Name = name;
                    prestoManifesto.PackageVersion = configuration.GetPackageVersion(name);
                    prestoManifesto.IncludeExtractionTools = configuration.IncludeExtractionTools(name);
                    prestoManifesto.CompleteUpdate = configuration.GetUpdateType(name).Equals("complete");
                    prestoManifesto.Contents = configuration.GetContents(name);
                    prestoManifesto.Executable = configuration.GetExecutable(name);
                    prestoManifesto.BaseDirectory = configuration.GetBaseDirectory(name);

                    this.GenerateTreeViewRecursive(prestoManifesto.Contents, prestoManifesto.BaseDirectory, ref treeView);
                    prestoManifesto.TreeView = treeView;

                    applicationList.Add(name, prestoManifesto);
                }

                // TODO : should fix this, currently it will always check the package version number.
                prestoManifesto.PackageVersion = configuration.GetPackageVersion(name);

                this.ViewBag.name = name;
                this.ViewBag.confName = configuration.Name;
                this.ViewData.Model = prestoManifesto;
            }

            return View();
        }

        /// <summary>
        /// Upload the package from the client side to the server side.
        /// </summary>
        /// <param name="name">Service name.</param>
        /// <param name="config">Current configuration name.</param>
        /// <returns>Redirect to the details for a given sercive name.</returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Upload(string name, string config)
        {
            Configuration configuration = GerminatorFacade.Instance.GetConfiguration(config);
            if (configuration != null)
            {
                string appPath = Request.PhysicalApplicationPath;
                string saveDir = string.Empty;
                if (!configuration.IsMono)
                {
                    saveDir = @"Packages\" + name + @"\";
                }
                else
                {
                    saveDir = @"Packages/" + name + @"/";
                }
                
                HttpFileCollectionBase hfc = Request.Files;
                for (int i = 0; i < hfc.Count; i++)
                {
                    HttpPostedFileBase file = hfc[i];
                    string fileName = file.FileName;
                    
                    if (!Directory.Exists(Path.Combine(appPath, saveDir)))
                    {
                        Directory.CreateDirectory(Path.Combine(appPath, saveDir));
                    }

                    if (!string.IsNullOrEmpty(fileName))
                    {
                        string savePath = Path.Combine(Path.Combine(appPath, saveDir), fileName);

                        if (Path.GetExtension(fileName).Equals(".zip"))
                        {
                            // Save .zip file, extract it and remove the original file
                            file.SaveAs(savePath);

                            // Change the directory back afterward.
                            string currentDirectory = Directory.GetCurrentDirectory();
                            
                            Directory.SetCurrentDirectory(Path.Combine(appPath, saveDir));
                            ProcessStartInfo processStartInfo = null;

                            if (!configuration.IsMono)
                            {
                                processStartInfo = new ProcessStartInfo(Path.Combine(HomeController.BaseDirectory, @"bin\unzip.exe"), string.Format("-u -o \"{0}\"", fileName));
                                processStartInfo.CreateNoWindow = true;
                                processStartInfo.UseShellExecute = false;
                            }
                            else
                            {
                                processStartInfo = new ProcessStartInfo("unzip", string.Format("-u -o \"{0}\"", fileName));
                                processStartInfo.CreateNoWindow = true;
                                processStartInfo.UseShellExecute = false;
                            }

                            Process process = Process.Start(processStartInfo);
                            process.WaitForExit();

                            System.IO.File.Delete(savePath);

                            // Set back the current directory
                            Directory.SetCurrentDirectory(currentDirectory);

                            // Recursively add all files in the save directory to the Project
                            this.AddFiles(name, configuration, Path.Combine(appPath, saveDir));
                        }
                        else
                        {    
                            bool sucess = configuration.AddFile(name, savePath, DateTime.Now.ToUniversalTime());
                            if (sucess)
                            {
                                file.SaveAs(savePath);
                            }
                        }
                    }
                }
            }

            // Check if this application is already in the applicationList.
            PrestoManifesto prestoManifesto = null;
            if (applicationList.TryGetValue(name, out prestoManifesto))
            {
                // Update the application tree view
                string treeView = string.Empty;
                this.GenerateTreeViewRecursive(prestoManifesto.Contents, prestoManifesto.BaseDirectory, ref treeView);
                prestoManifesto.TreeView = treeView;

                return PartialView("ContentView", prestoManifesto);
            }
            else
            {
                // Redirect to details page ( it shouldn't go through here, this should happen only if there 
                // is an error occurs).
                this.Session[HomeController.ConfigurationSessionKey] = configuration;
                return RedirectToAction("Details", new { name = name });
            }
        }

        /// <summary>
        /// Add a file from the client to server side into a specific directory.
        /// </summary>
        /// <param name="name">Service name.</param>
        /// <param name="config">Current configuration name.</param>
        /// <param name="directory">Directory name where the file should be added to.</param>
        /// <returns>Return partial view of "ContentView".</returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AddFile(string name, string config, string directory)
        {
            Configuration configuration = GerminatorFacade.Instance.GetConfiguration(config);
            if (configuration != null)
            {
                HttpFileCollectionBase hfc = Request.Files;

                for (int i = 0; i < hfc.Count; i++)
                {
                    HttpPostedFileBase file = hfc[i];
                    string fileName = file.FileName;

                    if (!Directory.Exists(directory))
                    {
                        Directory.CreateDirectory(directory);
                    }

                    if (!string.IsNullOrEmpty(fileName))
                    {
                        string savePath = Path.Combine(directory, fileName);
                        bool sucess = configuration.AddFile(name, savePath, DateTime.Now.ToUniversalTime());
                        if (sucess)
                        {
                            file.SaveAs(savePath);
                            PrestoManifesto prestoManifesto = null;
                            
                            if (applicationList.TryGetValue(name, out prestoManifesto))
                            {
                                string treeView = string.Empty;
                                this.GenerateTreeViewRecursive(prestoManifesto.Contents, prestoManifesto.BaseDirectory, ref treeView);
                                prestoManifesto.TreeView = treeView;

                                return PartialView("ContentView", prestoManifesto);
                            }
                            else
                            {
                                return RedirectToAction("Details", new { name = name });
                            }         
                        }
                        else
                        {
                            return Content(string.Format("Error: Filed to upload {0} to {1}", fileName, directory));
                        }
                    }
                }
            }

            return Content(string.Format("Error: There is an error occured when upload a file to {0} directory", directory));
        }

        /// <summary>
        /// Remove a specific file from the package given the service name and the full path of the file.
        /// </summary>
        /// <param name="applicationName">Service name.</param>
        /// <param name="fullPath">File full path.</param>
        /// <returns>Return the partial view of "ContentView".</returns>
        public ActionResult RemoveFile(string applicationName, string fullPath)
        {
            Configuration configuration = this.Session[HomeController.ConfigurationSessionKey] as Configuration;

            if (configuration != null)
            {
                if (System.IO.File.Exists(fullPath))
                {
                    configuration.RemoveFile(applicationName, fullPath);
                    System.IO.File.Delete(fullPath);

                    PrestoManifesto prestoManifesto = null;
                    if (applicationList.TryGetValue(applicationName, out prestoManifesto))
                    {
                        string treeView = string.Empty;
                        this.GenerateTreeViewRecursive(prestoManifesto.Contents, prestoManifesto.BaseDirectory, ref treeView);
                        prestoManifesto.TreeView = treeView;

                        return PartialView("ContentView", prestoManifesto);
                    }
                    else
                    {
                        return RedirectToAction("Details", new { name = applicationName });
                    }                    
                }
                else if (Directory.Exists(fullPath))
                {
                    return Content(string.Format("Error: Currently cannot remove a directory through Control Center")); 
                }
            }

            return Content(string.Format("Error: There is an error occured during the removal of {0}", Path.GetFileName(fullPath)));
        }

        /// <summary>
        /// Set the update type of a package.
        /// </summary>
        /// <param name="applicationName">Service name.</param>
        /// <param name="isSet">Boolean is set.</param>
        public void UpdateType(string applicationName, bool isSet)
        {
            Configuration configuration = this.Session[HomeController.ConfigurationSessionKey] as Configuration;
            string type = string.Empty;

            if (configuration != null)
            {
                if (isSet)
                {
                    type = "complete";
                }
                else
                {
                    type = "partial";
                }

                configuration.SetUpdateType(applicationName, type);
                try
                {
                    PrestoManifesto prestoManifesto = applicationList[applicationName];
                    prestoManifesto.CompleteUpdate = isSet;
                }
                catch
                {
                    DebugConsole.Instance.EnqueueTraceInfo(string.Format("Error: Failed to load {0} from the application list", applicationName), TraceLevel.Error);
                }
            }

            DebugConsole.Instance.EnqueueTraceInfo(isSet ? "Complete update type is set" : "Partial update type is set", TraceLevel.Info);
        }

        /// <summary>
        /// Set the extraction tools.
        /// </summary>
        /// <param name="applicationName">Service name.</param>
        /// <param name="isSet">Boolean is set.</param>
        public void IncludeExtractionTools(string applicationName, bool isSet)
        {
            Configuration configuration = this.Session[HomeController.ConfigurationSessionKey] as Configuration;

            if (configuration != null)
            {
                configuration.SetIncludeExtractionTools(applicationName, isSet);
                try
                {
                    PrestoManifesto prestoManifesto = applicationList[applicationName];
                    prestoManifesto.IncludeExtractionTools = isSet;
                }
                catch
                {
                    DebugConsole.Instance.EnqueueTraceInfo(string.Format("Error: Failed to load {0} from the application list", applicationName), TraceLevel.Error);
                }
            }

            DebugConsole.Instance.EnqueueTraceInfo(isSet ? "Extraction tools is included" : "Extraction tools is not included", TraceLevel.Info);
        }

        /// <summary>
        /// Build a package with a given description for a particular service.
        /// </summary>
        /// <param name="applicationName">Service name.</param>
        /// <param name="description">Package description.</param>
        public void Build(string applicationName, string description)
        {
            Configuration configuration = this.Session[HomeController.ConfigurationSessionKey] as Configuration;

            if (configuration != null)
            {
                ProcessTask processTask = new ProcessTask(configuration.BuildPackage);
                processTask.BeginInvoke(applicationName, description, new AsyncCallback(this.FinishBuild), processTask);
            }
        }

        /// <summary>
        /// Finish build callback function currently unused.
        /// </summary>
        /// <param name="result">The async result from the build async call.</param>
        public void FinishBuild(IAsyncResult result)
        {
            ProcessTask processTask = (ProcessTask)result.AsyncState;
            string applicationName = processTask.EndInvoke(result);

            // Check whether the build process has alter the current working directory
            if (!Directory.GetCurrentDirectory().Equals(HomeController.BaseDirectory))
            {
                Directory.SetCurrentDirectory(HomeController.BaseDirectory);
            }
        }

        /// <summary>
        /// Add a particular service to the Control Center, copy the required packages file to the Service directroy. Thus,
        /// the service will be available in and can be run from Control Center.
        /// </summary>
        /// <param name="applicationName">Service name.</param>
        public void AddServiceToControlCenter(string applicationName)
        {
            // Add the package to control center automatically
            Configuration configuration = this.Session[HomeController.ConfigurationSessionKey] as Configuration;

            if (configuration != null)
            {
                configuration.AddServiceToControlCenter(applicationName);
            }
        }

        /// <summary>
        /// Get the current package build progress description for a specific services.
        /// </summary>
        /// <remarks>http://weblogs.asp.net/seanmcalinden/archive/2009/11/15/asynchronous-processing-in-asp-net-mvc-with-ajax-progress-bar.aspx</remarks>
        /// <param name="applicationName">Service name.</param>
        /// <returns>Is an ajax request, return the progress description and javascript will display it on the client browser.</returns>
        public ContentResult GetCurrentDescription(string applicationName)
        {
            Configuration configuration = this.Session[HomeController.ConfigurationSessionKey] as Configuration;

            if (configuration != null)
            {
                this.ControllerContext.HttpContext.Response.AddHeader("cache-control", "no-cache");
                var currentDescription = configuration.GetBuildDescription(applicationName);
                var currentProgress = configuration.GetBuildProgress(applicationName);
                
                return Content(string.Format("{0}:{1}", currentProgress, currentDescription));
            }

            return Content("Failed to get the build description from the server");
        }

        /// <summary>
        /// Set the executable file for particular service, it has to be .exe file otherwise return an 
        /// error message and display it to the user.
        /// </summary>
        /// <param name="applicationName">Service name.</param>
        /// <param name="fullPath">Executable file full path.</param>
        /// <returns>Return the name of the executable file if success otherwiser retrun an error message.</returns>
        public ContentResult SetExecutableFile(string applicationName, string fullPath)
        {
            Configuration configuration = this.Session[HomeController.ConfigurationSessionKey] as Configuration;

            if (configuration != null)
            {
                if (System.IO.File.Exists(fullPath))
                {
                    if (Path.GetExtension(fullPath).Equals(".exe"))
                    {
                        configuration.SetExecutable(applicationName, fullPath);

                        try
                        {
                            PrestoManifesto prestoManifesto = applicationList[applicationName];
                            prestoManifesto.Executable = Path.GetFileName(fullPath);
                        }
                        catch
                        {
                            return Content(string.Format("Error: Failed to load {0} from the application list", applicationName));
                        }

                        return Content(Path.GetFileName(fullPath));
                    }
                    else
                    {
                        return Content(string.Format("Error: {0} is not executable, failed to set executable file", Path.GetFileName(fullPath)));
                    }
                }
            }

            return Content("Error: Failed to set executable file");
        }

        /// <summary>
        /// Add files recursively to the current project.
        /// </summary>
        /// <param name="projectName">Project name and Service name is the same in this case.</param>
        /// <param name="configuration">The current configuration.</param>
        /// <param name="sourcePath">The directory path.</param>
        private void AddFiles(string projectName, Configuration configuration, string sourcePath)
        {
            string[] files = Directory.GetFiles(sourcePath);
            string[] directories = Directory.GetDirectories(sourcePath);

            foreach (string file in files)
            {
                configuration.AddFile(projectName, file, DateTime.Now.ToUniversalTime());
            }

            foreach (string directory in directories)
            {
                this.AddFiles(projectName, configuration, directory);
            }
        }

        /// <summary>
        /// Gnerate an html codes used to display the tree view of service files on the client browser.
        /// </summary>
        /// <param name="contents">Contents need to be displayed.</param>
        /// <param name="baseDirectory">The service base directory.</param>
        /// <param name="treeViewCode">Html code (output).</param>
        private void GenerateTreeViewRecursive(Dictionary<string, FileData> contents, string baseDirectory, ref string treeViewCode)
        {
            string[] files = Directory.GetFiles(baseDirectory);
            string[] directories = Directory.GetDirectories(baseDirectory);

            foreach (string file in files)
            {
                if (contents.ContainsKey(file))
                {
                    treeViewCode += string.Format(@"<li><span class=""file"" id=""{0}"">{1}</span></li>", file, Path.GetFileName(file));
                }
            }

            foreach (string directory in directories)
            {
                DirectoryInfo dirInfo = new DirectoryInfo(directory);
                if (!dirInfo.Name.Equals("publish"))
                {
                    treeViewCode += string.Format(@"<li><span class=""folder"" id=""{0}"">{1}</span><ul>", directory, dirInfo.Name);
                    this.GenerateTreeViewRecursive(contents, directory, ref treeViewCode);
                    treeViewCode += string.Format(@"</ul></li>");
                }
            }
        }
    }
}