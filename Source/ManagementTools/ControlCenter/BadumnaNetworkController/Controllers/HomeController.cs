﻿//-----------------------------------------------------------------------
// <copyright file="HomeController.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
 
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Web.Mvc;

using BadumnaNetworkController.Models;

using Germinator;

using X509CertificateCreator;

namespace BadumnaNetworkController.Controllers
{
    /// <summary>
    /// HomeController class has responsibility to intialize the ControlCenter and load both configuration and germ from files.
    /// The current design can handle multiple configurations within one ControlCenter, however having multiple configurations
    /// in one Control Center seems not appropriate in the developer point of view.
    /// </summary>
    [HandleError]
    public class HomeController : Controller
    {
        /// <summary>
        /// Configuration Session Key.
        /// </summary>
        public const string ConfigurationSessionKey = "Configuration";

        /// <summary>
        /// Current Configuration Name.
        /// </summary>
        public const string ConfigurationNameSessionKey = "ConfigurationName";

        /// <summary>
        /// Certificate default filename.
        /// </summary>
        private const string CertificateFilename = "certificate.pfx";

        /// <summary>
        /// Control Center certificate.
        /// </summary>
        private static X509Certificate2 certificate;       

        /// <summary>
        /// Certificate passphrase.
        /// </summary>
        private static string certificatePassphrase = string.Empty;

        /// <summary>
        /// Gets the base directory.
        /// </summary>
        internal static string BaseDirectory
        {
            get { return MvcApplication.BaseDirectory; }
        }
        
        /// <summary>
        /// Redirect the home controller index page to current network configuration index page (redirect to Network/Details).
        /// The user authentication will be prompted here, currently are deactivated, however it should be activated when the control
        /// center will run remotely and only allowing certain users to access it. It will load the exsisting configuration and germ 
        /// file, otherwise it will ask the user to create a new configuration. The original and current implementation allows
        /// multiple configurations exist in one Control Center, the design should be changed as the current design only allows one
        /// configuration for each Control Center.
        /// </summary>
        /// <returns>Redirect to Network details page or create network page.</returns>
        public ActionResult Index()
        {
            // Load the required data
            if (GerminatorFacade.Instance == null)
            {
                string dataDirectory = Path.Combine(BaseDirectory, "App_Data");
                string certificateDirectory = Path.Combine(BaseDirectory, "Certificate");
                string germDirectory = Path.Combine(BaseDirectory, "Germ");
                string packagesDirectory = Path.Combine(BaseDirectory, "Packages");

                this.CheckExistingDirectory(dataDirectory);
                this.CheckExistingDirectory(Path.Combine(dataDirectory, "Configurations"));
                this.CheckExistingDirectory(certificateDirectory);
                this.CheckExistingDirectory(germDirectory);
                this.CheckExistingDirectory(packagesDirectory);

                Directory.SetCurrentDirectory(BaseDirectory);

                // Try to get the certificate
                if (!System.IO.File.Exists(Path.Combine(certificateDirectory, CertificateFilename)))
                {
                    return RedirectToAction("CreateCertificate");
                }
                else if (string.IsNullOrEmpty(certificatePassphrase))
                {
                    return RedirectToAction("CertificateAuthentication");
                }

                try
                {
                    // NOTE: create a certificate once on the CC lifetime.
                    if (certificate == null)
                    {
                        certificate = new X509Certificate2(Path.Combine(certificateDirectory, CertificateFilename), certificatePassphrase);
                    }

                    // For safety, always rewrite the public key.
                    string publicKeyString = certificate.GetPublicKeyString();
                    System.IO.File.WriteAllText(Path.Combine(germDirectory, "publicKey.key"), publicKeyString);
                }
                catch (CryptographicException e)
                {
                    return RedirectToAction("CertificateAuthentication", new { errorMessage = e.Message });
                }

                XmlConfigurationProvider configurationProvider = new XmlConfigurationProvider(Path.Combine(dataDirectory, "Configurations"));

                GerminatorFacade.Initialize(configurationProvider, null, certificate);

                // TODO: update the processes that currently running in each germ
                HelpTopics.Instance = new HelpTopics(System.IO.Path.Combine(BaseDirectory, "App_Data"));
            }

            if (!Request.IsAuthenticated)
            {        
                return RedirectToAction("LogOn", "Account");   
            }
            else if (GerminatorFacade.Instance.ConfigurationCount == 0)
            {
                return RedirectToAction("Create", "Network");
            }
            else
            {
                // TODO : revise this method... since we can only have one configuration
                // we might want to change the code in GerminatorFacade that will only 
                // give one configuration, currently force the Control Center to just read the
                // first element of exsisting network configurations from Germinator Facade
                IEnumerable<Configuration> configurations = GerminatorFacade.Instance.GetAllConfigurations();
                string configurationName = configurations.First<Configuration>().Name;
                
                return RedirectToAction("Details", "Network", new { name = configurationName });
            }   
        }

        /// <summary>
        /// Get the log messages from the DebugConsole if the queue is not empty and display it in the client browser.
        /// Use this to give user more feedback about the current processes that happening in the Control Center.
        /// </summary>
        /// <returns>Returns the first log message from the trace queue.</returns>
        public ContentResult GetLog()
        {
            return Content(DebugConsole.Instance.DequeueTraceInfo());
        }

        /// <summary>
        /// Auto generate certificate page.
        /// </summary>
        /// <returns>Return the create certificate page.</returns>
        public ActionResult CreateCertificate()
        {
            return View();
        }

        /// <summary>
        /// Generate a new certificate with given passphrase.
        /// </summary>
        /// <remarks>TODO: use the appropriate tool to auto generate the certificate.</remarks>
        /// <param name="passphrase">Certificate passphrase.</param>
        /// <param name="passphraseConfirmation">Certificate passphrase confirmation.</param>
        /// <returns>Back to home index page.</returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CreateCertificate(string passphrase, string passphraseConfirmation)
        {
            if (string.IsNullOrEmpty(passphrase))
            {
                this.ModelState.AddModelError("passphrase", Resources.PasswordInvalid);
            }

            if (string.IsNullOrEmpty(passphraseConfirmation))
            {
                this.ModelState.AddModelError("passphraseConfirmation", Resources.PasswordInvalid);
            }

            if (passphrase != passphraseConfirmation)
            {
                this.ModelState.AddModelError("all", Resources.PasswordMismatch);
            }

            if (!this.ModelState.IsValid)
            {
                return View();
            }

            string openSSLDirectory = Path.Combine(BaseDirectory, "OpenSSL");
            string certificateDirectory = Path.Combine(BaseDirectory, "Certificate");

            // 1. Create Self Signed Certificate.
            Directory.SetCurrentDirectory(certificateDirectory);
            new X509CertificateCreator.CertificateCreator().CreateCertificate(System.Net.Dns.GetHostName(), DateTime.Now + TimeSpan.FromDays(365), CertificateFilename, passphrase);

            // 2. Store the certificate passphrase.
            certificatePassphrase = passphrase;

            return RedirectToAction("Index");
        }

        /// <summary>
        /// Try to get the certificate passphrase from the user.
        /// </summary>
        /// <param name="message">System message.</param>
        /// <param name="errorMessage">Error message.</param>
        /// <returns>Return the certificate authentication.</returns>
        public ActionResult CertificateAuthentication(string message, string errorMessage)
        {
            this.ViewBag.Message = message;
            this.ViewBag.ErrorMessage = errorMessage;
            return View();
        }

        /// <summary>
        /// Obtains the certificae passphrase from the user.
        /// </summary>
        /// <param name="passphrase">Certificate passphrase</param>
        /// <returns>Back to home index page.</returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CertificateAuthentication(string passphrase)
        {
            if (string.IsNullOrEmpty(passphrase))
            {
                this.ModelState.AddModelError("passphrase", Resources.PasswordInvalid);
            }

            if (!this.ModelState.IsValid)
            {
                return View();
            }

            certificatePassphrase = passphrase;

            return RedirectToAction("Index");
        }

        /// <summary>
        /// Check whether the specify directory is exist, otherwise create one.
        /// </summary>
        /// <param name="fullPath">Directory full path.</param>
        private void CheckExistingDirectory(string fullPath)
        {
            if (!Directory.Exists(fullPath))
            {
                Directory.CreateDirectory(fullPath);
            }
        }
    }
}
