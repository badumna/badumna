//-----------------------------------------------------------------------
// <copyright file="HelpController.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
 
using System.Web.Mvc;

using BadumnaNetworkController.Models;

namespace BadumnaNetworkController.Controllers
{
    /// <summary>
    /// HelpController class is used for displaying the help information into the client browser, it obtained help information from
    /// HelpTopic instance. 
    /// </summary>
    public class HelpController : Controller
    {
        /// <summary>
        /// Display all the help information into a single page.
        /// </summary>
        /// <returns>Return help index page.</returns>
        public ActionResult Index()
        {
            this.ViewData.Model = HelpTopics.Instance.GetAllTopics();
            return View();
        }

        /// <summary>
        /// Get the help information with a given topic and item name and return the result back to the client browser.
        /// </summary>
        /// <param name="topicName">Topic name.</param>
        /// <param name="itemName">Item name.</param>
        /// <returns>The help information for given topic and item name.</returns>
        public ActionResult GetHelpItem(string topicName, string itemName)
        {
            HelpItem help = HelpTopics.Instance.GetItem(topicName, itemName);

            this.ViewBag.Topic = topicName;
            this.ViewBag.ItemName = itemName;
            this.ViewData.Model = help;
#if DEBUG
            this.ViewBag.Debug = true;
#else
            this.ViewBag.Debug = false;
#endif
            return PartialView("HelpItem");
        }

        /// <summary>
        /// Add a new help topic, it should be used internally (development purposes only), the deploy
        /// ControlCenter shouldn't have this functionality.
        /// </summary>
        /// <param name="topic">Topic name.</param>
        /// <param name="item">Item name.</param>
        /// <returns>Return AddItem.aspx page.</returns>
        public ActionResult AddItem(string topic, string item)
        {
            this.ViewBag.TopicList = new SelectList(HelpTopics.Instance.Topics, 0);
            this.ViewBag.Topic = topic;
            this.ViewBag.Item = item;
            this.ViewBag.HelpTitle = string.Empty;
            this.ViewBag.Text = string.Empty;

            if (!string.IsNullOrEmpty(topic) && !string.IsNullOrEmpty(item))
            {
                HelpItem helpItem = HelpTopics.Instance.GetItem(topic, item);
                if (helpItem != null)
                {
                    this.ViewBag.HelpTitle = helpItem.Title;
                    this.ViewBag.Text = helpItem.Text;
                }
            }

            return View();
        }

        /// <summary>
        /// Post method of AddItem page, collect the help information and save it to help.en.xml file.
        /// </summary>
        /// <param name="topic">Topic name.</param>
        /// <param name="item">Item name.</param>
        /// <param name="title">Title of the new topic.</param>
        /// <param name="text">Help information.</param>
        /// <returns>Redirect to Add method <see cref="Add(string topic, string item, string title, string text)"/>.</returns>
        [ValidateInput(false)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AddItem(string topic, string item, string title, string text)
        {
            return this.Add(topic, item, title, text);
        }

        /// <summary>
        /// Add a new help infomation into an exisiting topic by specifying the item name, title and text manually.
        /// </summary>
        /// <returns>Returnt Add.aspx page.</returns>
        public ActionResult Add()
        {
            this.ViewBag.TopicList = new SelectList(HelpTopics.Instance.Topics, 0);
            return View();
        }

        /// <summary>
        /// Post method from Add.aspx page, this method has exact functionality as <see cref="AddItem(string topic, string item, string title, string text)"/>,
        /// which is collecting the help information and saving it to help.en.xml file. 
        /// </summary>
        /// <remarks>Remove one of the method, Add and Add Item methods are too similar.</remarks>
        /// <param name="topic">Topic name.</param>
        /// <param name="item">Item name.</param>
        /// <param name="title">Title of the new topic.</param>
        /// <param name="text">Help information.</param>
        /// <returns>Return the Show.aspx page, showing the submited help information.</returns>
        [ValidateInput(false)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Add(string topic, string item, string title, string text)
        {
            if (string.IsNullOrEmpty(topic))
            {
                this.ModelState.AddModelError("Topic", "Must include a valid topic");
            }

            if (string.IsNullOrEmpty(item))
            {
                this.ModelState.AddModelError("Item", "Must include a valid item name");
            }

            if (string.IsNullOrEmpty(title))
            {
                this.ModelState.AddModelError("Title", "Must include a valid title");
            }

            if (string.IsNullOrEmpty(text))
            {
                this.ModelState.AddModelError("Text", "Must include help text");
            }

            if (!this.ModelState.IsValid)
            {
                return View();
            }

            this.ViewData.Model = HelpTopics.Instance.AddItem(topic, item, title, text);

            return this.View("Show");
        }
    }
}
