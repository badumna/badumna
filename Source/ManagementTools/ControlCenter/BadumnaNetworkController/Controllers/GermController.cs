//-----------------------------------------------------------------------
// <copyright file="GermController.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using System.Web.Script.Serialization;

using BadumnaNetworkController.Models;

using Germinator;

namespace BadumnaNetworkController.Controllers
{
    /// <summary>
    /// GermController class control all of the germs transaction and query, including the remote machine status where the germ is 
    /// running, the number of processes running on the specific germ and the germ list for the current configuration.
    /// </summary>
    public class GermController : Controller
    {
        /// <summary>
        /// Maintance the list of remote machine status foreach online germ.
        /// </summary>
        private static Dictionary<string, RemoteMachineStatus> machineStatus = new Dictionary<string, RemoteMachineStatus>();

        /// <summary>
        /// Record the last time control center request a remote machine status, make sure control center is not requesting status
        /// too often.
        /// </summary>
        private static Dictionary<string, DateTime> lastTimeGetMachineStatus = new Dictionary<string, DateTime>();

        /// <summary>
        /// Keep the records of the last 20 data of the remote machines cpu load.
        /// </summary>
        private static Dictionary<string, List<string>> cpuLoadHistory = new Dictionary<string, List<string>>();

        /// <summary>
        /// Keep the records of the last 20 data of the remote machines free memory.
        /// </summary>
        private static Dictionary<string, List<string>> memAvailableHistory = new Dictionary<string, List<string>>();

        /// <summary>
        /// Keep the records of the total traffic bandwidth received an sent from/to remote machines.
        /// </summary>
        private static Dictionary<string, List<string>> trafficBandwidthHistory = new Dictionary<string, List<string>>();
        
        /// <summary>
        /// Return the details of the specific germ mention in the parameter name.
        /// </summary>
        /// <remarks>GET: /Germ/Details/5</remarks>
        /// <param name="name">Germ host name, including the port number, e.g. www.germ.com:21257.</param> 
        /// <returns>Return Details.aspx page.</returns>
        public ActionResult Details(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                return RedirectToAction("Index", "Home");
            }

            Configuration configuration = this.Session[HomeController.ConfigurationSessionKey] as Configuration;
            
            if (configuration == null)
            {
                return RedirectToAction("Index", "Home");
            }

            GermClient client = (from germClient in configuration.GermClients where germClient.Name == name select germClient).SingleOrDefault();
            
            if (client == null)
            {
                this.ModelState.AddModelError("Name", String.Format(Resources.NothingKnownAboutFormat, name));
            }

            if (!this.ModelState.IsValid)
            {
                return this.RedirectToAction("Index", "Home");            
            }

            this.ViewData.Model = client;
            return View();
        }

        /// <summary>
        /// Get all processes that currently running on the a specific germ mention in the parameter name.
        /// </summary>
        /// <remarks>This method is obsolete (use Network/Processes).</remarks>
        /// <param name="name">Germ host name, including the port number, e.g. www.germ.com:21257.</param>
        /// <returns>Return Processes partial view.</returns>
        public ActionResult Processes(string name)
        {
            if (!string.IsNullOrEmpty(name))
            {
                IEnumerable<RemoteProcess> processes = this.RunningProcesses(name);
                if (processes != null)
                {
                    this.ViewBag.Host = name;
                    return this.PartialView("Processes", processes);
                }
                else
                {
                    return Content("Not connected");
                }
            }

            return RedirectToAction("Index", "Home");
        }

        /// <summary>
        /// Get the information about the germ status, either the germ is online or offline.
        /// </summary>
        /// <param name="name">Germ host name, including the port number, e.g. www.germ.com:21257.</param>
        /// <param name="index">Index which used in the client side to identified a particular germ.</param>
        /// <returns>Return the status of that particular germ.</returns>
        public ContentResult IsConnected(string name, int index)
        {
            if (!string.IsNullOrEmpty(name))
            {
                // Note: germ client shouldn't be initiate connection too often, otherwise it will create too
                // much traffic on the Control Center               
                if (machineStatus.ContainsKey(name))
                {
                    // Check whether the host name connection just based on the machine status,
                    // means if we still be able to collect the machine status information then 
                    // the host is still online.
                    return Content(index + ":Germ online");
                }
                else
                {            
                    return Content(index + ":Germ offline");
                }
            }

            return Content(index + ":Invalid Germ name");
        }

        /// <summary>
        /// Get the machine status and return the result back to the network details page, it different to 
        /// <see cref="MachineStatusDetails(string name)"/>.
        /// </summary>
        /// <remarks>
        /// TODO: the machine status shouldn't be called from many different places. Let this
        /// function to always take the latest status from the machineStatus dictionary. 
        /// However, the server has to request the machine status regularly for each germ.
        /// </remarks>
        /// <param name="name">Germ host name, including the port number, e.g. www.germ.com:21257.</param>
        /// <param name="count">The number of available germs.</param>
        /// <returns>Return the machine status of that particular germ.</returns>
        public ContentResult GetMachineStatus(string name, int count)
        {
            if (!string.IsNullOrEmpty(name))
            {
                // Split the name to list of string
                string response = string.Empty;
                string[] hostNames = name.Split(new char[] { ',' }, count, StringSplitOptions.RemoveEmptyEntries);
                DateTime time = DateTime.Now;
                int index = 0;
                
                foreach (string hostName in hostNames)
                {
                    RemoteMachineStatus status = null;
                    DateTime lastTime;
                    
                    // Check whether this is the first request
                    if (!lastTimeGetMachineStatus.TryGetValue(hostName, out lastTime))
                    {
                        // make sure to request machine status to remote machine at the first time.
                        lastTime = DateTime.Now - TimeSpan.FromMinutes(6);
                        lastTimeGetMachineStatus.Add(hostName, lastTime);
                    }

                    if ((time - lastTime).TotalMinutes < 5)
                    {
                        // Try to get the value from machineStatus dictionary
                        if (machineStatus.TryGetValue(hostName, out status))
                        {
                            response += string.Format("Germ online:{0}:{1}:{2}|", index, status.CPULoad, status.MemoryAvailable);
                        }
                        else
                        {
                            response += string.Format("Germ offline:{0}:N/A:N/A|", index);
                        }
                    }
                    else
                    {
                        Configuration configuration = this.Session[HomeController.ConfigurationSessionKey] as Configuration;
                        if (configuration != null)
                        {
                            GermClient germ = configuration.GetGermClient(hostName);
                            status = this.MachineStatus(hostName);

                            // Note: keep trying to ping the host when the status is null
                            if (status != null)
                            {
                                lastTimeGetMachineStatus[hostName] = time;
                                this.StoreMachineStatus(status, hostName);
                                if (machineStatus.ContainsKey(hostName))
                                {
                                    machineStatus[hostName] = status;
                                }
                                else
                                {
                                    machineStatus.Add(hostName, status);
                                }

                                response += string.Format("Germ online:{0}:{1}:{2}|", index, status.CPULoad, status.MemoryAvailable);
                            }
                            else
                            {
                                if (machineStatus.ContainsKey(hostName))
                                {
                                    // the machine status are not relevant anymore for this germ
                                    machineStatus.Remove(hostName);
                                    lastTimeGetMachineStatus.Remove(hostName);
                                }

                                response += string.Format("Germ offline:{0}:N/A:N/A|", index);
                            }
                        }
                        else
                        {
                            // TODO: should give a meaningfull information instead.
                            response += string.Format("Germ offline:{0}:N/A:N/A|", index);
                        }
                    }

                    index++;
                }

                return Content(response);
            }

            return Content("Error:Invalid Germ name");
        }

        /// <summary>
        /// Get the remote machine status where a particular germ is running, it will give information
        /// about the cpu load, memory available and bandwidth consumption.
        /// </summary>
        /// <param name="name">Germ host name, including the port number, e.g. www.germ.com:21257.</param>
        /// <returns>Remote machine status.</returns>
        public ActionResult MachineStatusDetails(string name)
        {
            if (!string.IsNullOrEmpty(name))
            {
                RemoteMachineStatus status = this.MachineStatus(name);

                if (status != null)
                {
                    this.StoreMachineStatus(status, name);
                    this.ViewBag.Host = name;
                    if (machineStatus.ContainsKey(name))
                    {
                        machineStatus[name] = status;
                    }
                    else
                    {
                        machineStatus.Add(name, status);
                    }

                    return this.PartialView("MachineStatus", status);
                }
                else
                {
                    if (machineStatus.ContainsKey(name))
                    {
                        // the machine status are not relevant anymore for this germ
                        machineStatus.Remove(name);
                        lastTimeGetMachineStatus.Remove(name);
                    }

                    return Content("Machine status is not available");
                }
            }

            return RedirectToAction("Index", "Network");
        }

        /// <summary>
        /// Get the cpu load and free memory data of a particular remote machine.
        /// </summary>
        /// <param name="name">Germ host name, including the port number, e.g. www.germ.com:21257.</param>
        /// <returns>Returns a json format of the machine status.</returns>
        public ActionResult GetMachineStatusData(string name)
        {
            List<string> cpuLoadData = null;
            List<string> freeMemData = null;
            string cpuLoad = string.Empty;
            string freeMem = string.Empty;

            if (cpuLoadHistory.TryGetValue(name, out cpuLoadData))
            {
                foreach (string data in cpuLoadData)
                {
                    cpuLoad += string.Format("{0},", data);
                }
            }

            if (memAvailableHistory.TryGetValue(name, out freeMemData))
            {
                foreach (string data in freeMemData)
                {
                    freeMem += string.Format("{0},", data);
                }
            }

            return Json(
                JsonHelper.DataPlotFormatter(
                    new string[] { cpuLoad.TrimEnd(','), freeMem.TrimEnd(',') }, 
                    new string[] { "cpu load (%)", "free memory (Mb)" }, 
                    new string[] { string.Empty, "\"yaxis\" : 2" }));                        
        }

        /// <summary>
        /// Get the total traffic bandwidth of a particular remote machine.
        /// </summary>
        /// <param name="name">Germ host name, including the port number, e.g. www.germ.com:21257.</param>
        /// <returns>Returns a json format of the total traffic status.</returns>
        public ActionResult GetTrafficData(string name)
        {
            List<string> trafficReceivedData = null;
            List<string> trafficSentData = null;
            string trafficReceived = string.Empty;
            string trafficSent = string.Empty;

            if (trafficBandwidthHistory.TryGetValue(string.Format("{0}-received", name), out trafficReceivedData))
            {
                foreach (string data in trafficReceivedData)
                {
                    trafficReceived += string.Format("{0},", data);
                }
            }

            if (trafficBandwidthHistory.TryGetValue(string.Format("{0}-sent", name), out trafficSentData))
            {
                foreach (string data in trafficSentData)
                {
                    trafficSent += string.Format("{0},", data);
                }
            }
            
            return Json(
                JsonHelper.DataPlotFormatter(
                    new string[] { trafficReceived.TrimEnd(','), trafficSent.TrimEnd(',') }, 
                    new string[] { "Total received (kB/s)", "Total sent (kB/s)" }, 
                    new string[] { null, null }));            
        }

        /// <summary>
        /// Create a new germ and added to the list of the germ.
        /// </summary>
        /// <remarks> GET: /Germ/Create </remarks>
        /// <returns>Create germ page.</returns>
        public ActionResult Create()
        {
            return View();
        }

        /// <summary>
        /// POST method, submitted form from client side, contains a new germ information including the address
        /// of the germ and port number in which this germ is listening to.
        /// </summary>
        /// <remarks>POST: /Germ/Create</remarks>
        /// <param name="host">Germ address, e.g. www.germ.com.</param>
        /// <param name="port">Germ port.</param>
        /// <param name="isUndercover">Undercover germ (bool).</param>
        /// <returns>Redirect to ConttolCenter index page.</returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Create(string host, int? port, bool? isUndercover)
        {
            if (string.IsNullOrEmpty(host))
            {
                this.ModelState.AddModelError("Host", Resources.MustGiveHostName);
            }

            try
            {
                System.Net.IPAddress[] addresses = System.Net.Dns.GetHostAddresses(host);
                if (addresses.Length == 0)
                {
                    this.ModelState.AddModelError("host", Resources.ResolveError);
                }
            }
            catch
            {
                this.ModelState.AddModelError("host", Resources.InvalidHost);
            }

            if (!port.HasValue || port.Value <= 0)
            {
                this.ModelState.AddModelError("port", Resources.InvalidPort);
            }

            if (!isUndercover.HasValue)
            {
                isUndercover = false;
            }

            if (!this.ModelState.IsValid)
            {
                return View();
            }

            try
            {
                Configuration configuration = this.Session[HomeController.ConfigurationSessionKey] as Configuration;

                if (configuration == null)
                {
                    return RedirectToAction("Index", "Home");
                }

                GermClient client = configuration.AddGerm(String.Format("{0}:{1}", host, port.Value));

                client.IsUndercover = isUndercover.Value;
                return RedirectToAction("Index", "Network");
            }
            catch (Exception)
            {
                return View();
            }
        }

        /// <summary>
        /// Get the list of running processes in the particular germ.
        /// </summary>
        /// <param name="name">Germ host name, including the port number, e.g. www.germs.com:21257.</param>
        /// <returns>The list of running processes.</returns>
        private IEnumerable<RemoteProcess> RunningProcesses(string name)
        {
            try
            {
                Configuration configuration = this.Session[HomeController.ConfigurationSessionKey] as Configuration;

                GermClient client = configuration.GetGermClient(name);
                
                if (client != null)
                {
                    ProcessListOperation listOperation = client.QueueProcessListOperation(null, null);

                    listOperation.AsyncWaitHandle.WaitOne(GermClient.DefaultOperationTimeout);
                    if (listOperation.RunningProcesses != null)
                    {
                        return from description in listOperation.RunningProcesses select new RemoteProcess(description);
                    }
                }
            }
            catch 
            { 
                // It will catch the error if the configuration is null.
            }

            return null;
        }

        /// <summary>
        /// Get the machine status of the remote machine of the specified address (name).
        /// </summary>
        /// <param name="name">Germ host name, including the port number, e.g. www.germ.com:21257.</param>
        /// <returns>Return a RemoteMachineStatus object that contains machine status information.</returns>
        private RemoteMachineStatus MachineStatus(string name)
        {
            try
            {
                Configuration configuration = this.Session[HomeController.ConfigurationSessionKey] as Configuration;

                GermClient client = configuration.GetGermClient(name);
                if (client != null)
                {
                    CustomMessageOperation operation = client.QueueGetMachineStatusOperation(new byte[] { }, null, null);
                    operation.AsyncWaitHandle.WaitOne(5000);
                    if (operation.WasSuccessful)
                    {
                        try
                        {
                            // TODO: using XML to represent the status of the machine will be a better idea
                            BinaryReader reader = new BinaryReader(operation.Response);
                            RemoteMachineStatus status = new RemoteMachineStatus();
                            status.CPULoad = reader.ReadString();
                            status.MemoryAvailable = reader.ReadString();
                            status.TotalBytesSent = reader.ReadInt32();
                            status.TotalBytesSent = reader.ReadInt32();
                            status.Timestamp = DateTime.Now;
                            return status;
                        }
                        catch 
                        {
                        }
                    }
                    else if (!operation.WasSuccessful)
                    {
                        return null;
                    }
                }
            }
            catch 
            {
                // It will catch the error if the configuration is null.
            }

            return null;
        }

        /// <summary>
        /// Store the machine status history.
        /// </summary>
        /// <param name="status">Remote machine status.</param>
        /// <param name="name">Germ host name, including the port number, e.g. www.germ.com:21257.</param>
        private void StoreMachineStatus(RemoteMachineStatus status, string name)
        {
            List<string> cpuLoadData = null;
            List<string> memAvailableData = null;
            List<string> trafficSentData = null;
            List<string> trafficReceivedData = null;
            long timeStamp = this.GetJavascriptTimestamp(status.Timestamp);
            
            if (!cpuLoadHistory.TryGetValue(name, out cpuLoadData))
            {
                cpuLoadData = new List<string>();
                cpuLoadHistory.Add(name, cpuLoadData);
            }

            cpuLoadData.Add(string.Format("[{0},{1}]", timeStamp, status.CPULoad.Split((char)32)[0]));

            if (cpuLoadData.Count > 20)
            {
                cpuLoadData.RemoveAt(0);
            }

            cpuLoadHistory[name] = cpuLoadData;

            if (!memAvailableHistory.TryGetValue(name, out memAvailableData))
            {
                memAvailableData = new List<string>();
                memAvailableHistory.Add(name, memAvailableData);
            }

            memAvailableData.Add(string.Format("[{0},{1}]", timeStamp, status.MemoryAvailable.Split((char)32)[0]));

            if (memAvailableData.Count > 20)
            {
                memAvailableData.RemoveAt(0);
            }

            memAvailableHistory[name] = memAvailableData;

            if (!trafficBandwidthHistory.TryGetValue(string.Format("{0}-received", name), out trafficReceivedData))
            {
                trafficReceivedData = new List<string>();
                trafficBandwidthHistory.Add(string.Format("{0}-received", name), trafficReceivedData);
            }

            trafficReceivedData.Add(string.Format("[{0},{1}]", timeStamp, status.TotalBytesReceived / 1024.0f));

            if (trafficReceivedData.Count > 20)
            {
                trafficReceivedData.RemoveAt(0);
            }

            trafficBandwidthHistory[string.Format("{0}-received", name)] = trafficReceivedData;
            
            if (!trafficBandwidthHistory.TryGetValue(string.Format("{0}-sent", name), out trafficSentData))
            {
                trafficSentData = new List<string>();
                trafficBandwidthHistory.Add(string.Format("{0}-sent", name), trafficSentData);
            }

            trafficSentData.Add(string.Format("[{0},{1}]", timeStamp, status.TotalBytesSent / 1024.0f));

            if (trafficSentData.Count > 20)
            {
                trafficSentData.RemoveAt(0);
            }

            trafficBandwidthHistory[string.Format("{0}-sent", name)] = trafficSentData;
        }

        /// <summary>
        /// Get JavaScriptTimeStamp from a given DateTime object.
        /// </summary>
        /// <param name="input">Date time object.</param>
        /// <returns>Return long ticks.</returns>
        private long GetJavascriptTimestamp(DateTime input)
        {
            TimeSpan span = new TimeSpan(DateTime.Parse("1/1/1970").Ticks);
            DateTime time = input.Subtract(span);
            return (long)(time.Ticks / 10000);
        }
    }
}
