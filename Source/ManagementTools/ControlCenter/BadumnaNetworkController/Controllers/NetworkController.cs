//-----------------------------------------------------------------------
// <copyright file="NetworkController.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web.Mvc;

using Germinator;
using Germinator.Components;
using Germinator.Services;

namespace BadumnaNetworkController.Controllers
{
    /// <summary>
    /// The NetworkController class is the main class in the ControlCenter, almost all the functionality of Control Center are implemented
    /// in this class, is also act as "Home page" for Control Center. It displays the germ list, the badumna components (badumna network 
    /// configuration), the available services and the current running proccesses.
    /// </summary>
    public class NetworkController : Controller
    {
        /// <summary>
        /// StartProcessTask delegate is used to start a process asynchronously.
        /// </summary>
        /// <param name="service">Service instance.</param>
        /// <param name="customName">Service custom name.</param>
        /// <param name="passphrase">Certificate passphrase for dei service.</param>
        /// <returns>Return true on success.</returns>
        private delegate bool StartProcessTask(BaseService service, string customName, string passphrase);

        /// <summary>
        /// The index page is used to display all the available configurations, since current implementation only allows one configuration,
        /// the index page will be automatically redirected to the network details page. The index page should be used to display all the 
        /// network configuration details and remove the details page from the Control Center (requirement: only one configuration for each
        /// Control Center).
        /// </summary>
        /// <returns>Redirect to network detail page.</returns>
        public ActionResult Index()
        {
            try
            {
                string configurationName = this.Session[HomeController.ConfigurationNameSessionKey] as string;

                if (configurationName == null)
                {
                    return RedirectToAction("Index", "Home");
                }

                return RedirectToAction("Details", "Network", new { name = configurationName });
            }
            catch (Exception)
            {
                return RedirectToAction("Index", "Home");
            }
        }

        /// <summary>
        /// The Details network page act as the home page for Control Center and has all the ControlCenter core/important components and
        /// functionality.
        /// </summary>
        /// <param name="name">Name of the configuration.</param>
        /// <returns>The details page of given configuration name.</returns>
        public ActionResult Details(string name)
        {
            if (string.IsNullOrEmpty(name) || GerminatorFacade.Instance == null)
            {
                return this.RedirectToAction("Index");
            }

            Configuration configuration = GerminatorFacade.Instance.GetConfiguration(name);
            if (configuration == null)
            {
                this.ModelState.AddModelError("Name", String.Format(Resources.NothingKnownAboutFormat, name));
                return this.View("Index", GerminatorFacade.Instance.GetAllConfigurations());
            }
            else
            {
                this.Session[HomeController.ConfigurationSessionKey] = configuration;
                this.Session[HomeController.ConfigurationNameSessionKey] = configuration.Name;
                this.ViewData.Model = configuration;
                this.ViewBag.RunningServices = configuration.RunningServices;
            }

            return View();
        }

        /// <summary>
        /// Remove a specific germ given the germ name from the list.
        /// </summary>
        /// <param name="name">Germ host name, including the port number.</param>
        /// <returns>Return to network index page.</returns>
        public ActionResult RemoveGerm(string name)
        {
            if (this.Request.IsAjaxRequest())
            {
                if (!string.IsNullOrEmpty(name))
                {
                    GerminatorFacade.Instance.RemoveGerm(name);
                }

                return PartialView("Germs", GerminatorFacade.Instance.GetAllGermClients());
            }

            return RedirectToAction("Index");
        }

        /// <summary>
        /// Create a new network configuration, it will be called only when there is no existing configuration
        /// in the Control Center.
        /// </summary>
        /// <returns>Return the create network configuration page.</returns>
        public ActionResult Create()
        {
            return View();
        }

        /// <summary>
        /// The POST method of create network configuration page. Collect he information from the client/user and 
        /// create the new configuration based on the given information. The only information obtained is the 
        /// configuration name.
        /// </summary>
        /// <param name="name">Name of the new network configuration.</param>
        /// <param name="protocolHash">Name of the protocol hash for this corresponding application.</param>
        /// <returns>Redirect to network detail page.</returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Create(string name, string protocolHash)
        {
            try
            {
                if (string.IsNullOrEmpty(name))
                {
                    this.ModelState.AddModelError("name", Resources.MustGiveName);
                }

                if (string.IsNullOrEmpty(protocolHash))
                {
                    this.ModelState.AddModelError("protocolHash", Resources.MustGiveName);
                }

                if (!InputValidationHelper.ValidateAlphanumeric(protocolHash))
                {
                    this.ModelState.AddModelError("protocolHash", Resources.InvalidName);
                }

                Configuration configuration = GerminatorFacade.Instance.GetConfiguration(name);
                if (configuration != null)
                {
                    this.ModelState.AddModelError("name", Resources.NameInUse);
                }

                if (!this.ModelState.IsValid)
                {
                    return View();
                }

                configuration = GerminatorFacade.Instance.AddConfiguration(name, protocolHash);
                configuration.Save();

                return RedirectToAction("Details", new { name = name });
            }
            catch (Exception)
            {
                return View();
            }
        }

        /// <summary>
        /// Prestart service is used to check some special condition to be met, for example in this case it will be use
        /// to check whether the dei server should be started with or without warning
        /// </summary>
        /// <param name="serviceName">service name to be started</param>
        /// <returns>Response text need to be displayed on the client browser.</returns>
        public ContentResult PrestartService(string serviceName)
        {
            string response = string.Empty;
            Configuration configuration = this.Session[HomeController.ConfigurationSessionKey] as Configuration;

            if (configuration != null && !string.IsNullOrEmpty(serviceName))
            {
                if (serviceName.Equals("Dei") && configuration.RunningServices.Count > 0)
                {
                    response += "<p>There are services running in your network. Once you start Dei server, you will have to restart those services with Dei configuration in order to work properly. <br/>";
                    response += "Do you want to continue ? </p>";
                }
            }

            return Content(response);
        }

        /// <summary>
        /// Start a service on the specified germ host.
        /// </summary>
        /// <param name="serviceName">Service name.</param>
        /// <param name="germName">Germ host name, including the port number.</param>
        /// <param name="customName">Service custom name.</param>
        /// <param name="passphrase">The certificate passphrase (special case for start dei server).</param>
        public void StartService(string serviceName, string germName, string customName, string passphrase)
        {
            DebugConsole.Instance.EnqueueTraceInfo("Loading ...");
            Configuration configuration = this.Session[HomeController.ConfigurationSessionKey] as Configuration;
            BaseService service = null;
            ComponentView componentView = null;

            if (configuration != null)
            {
                this.ViewBag.ApplicationName = configuration.Name;
                service = configuration.GetService(serviceName);
                componentView = configuration.GetComponent(serviceName);
            }

            // Update the component property of "CompatibleHosts" before starting a service
            // TODO: it should done in the lower level (configuration.cs)
            if (service != null && componentView != null)
            {
                componentView.SetProperty("CompatibleHosts", germName);
                DebugConsole.Instance.EnqueueTraceInfo(string.Format("Starting {0} service in {1}", serviceName, germName));
                StartProcessTask startProcessTask = new StartProcessTask(configuration.StartService);
                startProcessTask.BeginInvoke(service, customName, passphrase, new AsyncCallback(this.OnFinishStartProcess), startProcessTask);

                return;
            }

            DebugConsole.Instance.EnqueueTraceInfo(string.Format("Failed to start {0} service in {1}", serviceName, germName));
        }

        /// <summary>
        /// Stop or kill a running process based on the service name and the germ name where the process is running on.
        /// </summary>
        /// <param name="serviceName">Service name.</param>
        /// <param name="germName">Germ host name, including the port number.</param>
        /// <param name="redirectToHome">The value indicating whether redirect to home is required</param>
        /// <returns>Return the partial view of 'RunningServices'.</returns>
        public ActionResult StopService(string serviceName, string germName, bool redirectToHome)
        {
            Configuration configuration = this.Session[HomeController.ConfigurationSessionKey] as Configuration;
            BaseService service = null;
            if (configuration != null)
            {
                this.ViewBag.ApplicationName = configuration.Name;
                if (serviceName.Contains("Dei"))
                {
                    service = configuration.GetService("Dei");
                }
                else
                {
                    service = configuration.GetService(serviceName);
                }
            }

            if (service != null)
            {
                DebugConsole.Instance.EnqueueTraceInfo(string.Format("Stopping {0} service from {1}", serviceName, germName));
                configuration.StopService(service, germName);
                DebugConsole.Instance.EnqueueTraceInfo(string.Format("Successfully stopped {0} service from {1}", serviceName, germName));

                foreach (KeyValuePair<string, RemoteServiceProcess> pair in configuration.RunningServices)
                {
                    if (pair.Value.Process.Name.Equals(serviceName) && pair.Value.Germ.Name.Equals(germName))
                    {
                        configuration.RunningServices.Remove(pair.Key);
                        configuration.Save();
                        break;
                    }
                }

                if (!redirectToHome)
                {
                    this.ViewData.Model = configuration.RunningServices;
                    return PartialView("RunningServices");
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }

            return Content("Failed");
        }

        /// <summary>
        /// Get the ping status for a specific process based on the service name and the germ where this process is running on.
        /// </summary>
        /// <param name="serviceName">Service name.</param>
        /// <param name="host">Germ host name, including the port number.</param>
        /// <returns>Return the partial view of 'RunningServices'.</returns>
        public ActionResult Ping(string serviceName, string host)
        {
            Configuration configuration = this.Session[HomeController.ConfigurationSessionKey] as Configuration;
            BaseService service = null;
            if (configuration != null)
            {
                this.ViewBag.ApplicationName = configuration.Name;
                service = configuration.GetService(serviceName.Contains("Dei") ? "Dei" : serviceName);
            }

            if (service != null)
            {
                service.BlockingPing(host);
                this.ViewData.Model = configuration.RunningServices;
                return PartialView("RunningServices");
            }

            return Content("Failed");
        }

        /// <summary>
        /// Restart the specific processes given the service name and the germ name.
        /// </summary>
        /// <param name="serviceName">Service name.</param>
        /// <param name="germName">Germ host name, including the port number.</param>
        /// <param name="redirectToHome">The value indicating whether redirect to home is required</param>
        /// <returns>Return the partial view of 'RunningServices'.</returns>
        public ActionResult RestartService(string serviceName, string germName, bool redirectToHome)
        {
            Configuration configuration = this.Session[HomeController.ConfigurationSessionKey] as Configuration;
            BaseService service = null;
            if (configuration != null)
            {
                this.ViewBag.ApplicationName = configuration.Name;
                if (serviceName.Contains("Dei"))
                {
                    service = configuration.GetService("Dei");
                }
                else
                {
                    service = configuration.GetService(serviceName);
                }
            }

            if (service != null)
            {
                DebugConsole.Instance.EnqueueTraceInfo(string.Format("Restart {0} service from {1}", serviceName, germName));
                configuration.RestartService(service, germName);
                DebugConsole.Instance.EnqueueTraceInfo(string.Format("Successfully restart {0} service from {1}", serviceName, germName));

                if (!redirectToHome)
                {
                    this.ViewData.Model = configuration.RunningServices;
                    return PartialView("RunningServices");
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }

            return Content("Failed");
        }

        /// <summary>
        /// Get all processes that currently running on the a specific germ mention in the parameter name.
        /// </summary>
        /// <param name="germName">Germ host name, including the port number.</param>
        /// <returns>Return the partial view of 'RunningServices'.</returns>
        public ActionResult Processes(string germName)
        {
            Configuration configuration = this.Session[HomeController.ConfigurationSessionKey] as Configuration;
            if (configuration != null)
            {
                Dictionary<string, RemoteServiceProcess> processes = new Dictionary<string, RemoteServiceProcess>();

                foreach (KeyValuePair<string, RemoteServiceProcess> pair in configuration.RunningServices)
                {
                    if (pair.Value.Germ.Name.Equals(germName))
                    {
                        processes.Add(pair.Key, pair.Value);
                    }
                }

                return PartialView("RunningServices", processes);
            }

            return Content("Failed");
        }

        /// <summary>
        /// AsyncCallback method, which will be called when StartProcessTask delegate has finished.
        /// </summary>
        /// <param name="result">IAsyncResult object.</param>
        private void OnFinishStartProcess(IAsyncResult result)
        {
            StartProcessTask startProcessTask = (StartProcessTask)result.AsyncState;
            if (startProcessTask.EndInvoke(result))
            {
                DebugConsole.Instance.EnqueueTraceInfo("location.reload(true)");
            }
        }
    }
}
