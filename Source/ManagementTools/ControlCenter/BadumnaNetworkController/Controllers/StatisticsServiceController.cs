﻿//---------------------------------------------------------------------------------
// <copyright file="StatisticsServiceController.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
// <summary>Statistics service class.</summary>
//---------------------------------------------------------------------------------

namespace BadumnaNetworkController.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Web.Mvc;
    using System.Xml;

    using Germinator;
    using Germinator.Services;

    /// <summary>
    /// StatisticsServiceController is used to display the scene status from the statistics server.
    /// </summary>
    public class StatisticsServiceController : Controller
    {
        /// <summary>
        /// Maximum plot points to store for each scene.
        /// </summary>
        private readonly int plotCountMax = 20;

        /// <summary>
        /// Collection of plots for each scene.
        /// </summary>
        private static Dictionary<string, List<string>> sceneData = new Dictionary<string, List<string>>();

        /// <summary>
        /// Total users across all scenes.
        /// </summary>
        private static List<string> totalData = new List<string>();

        /// <summary>
        /// Statistics query offset.
        /// </summary>
        private static int statsQueryOffset = 0;

        /// <summary>
        /// Selected range type.
        /// </summary>
        private static RangeType rangeType = RangeType.Day;

        /// <summary>
        /// Range type of the Session starts and Concurrent Users line graphs.
        /// </summary>
        private enum RangeType : int
        {
            /// <summary>
            /// Range of one day.
            /// </summary>
            Day = 0,

            /// <summary>
            /// Range of one week.
            /// </summary>
            Week,

            /// <summary>
            /// Range of one month.
            /// </summary>
            Month
        }

        /// <summary>
        /// The session start activity graph page.
        /// </summary>
        /// <param name="host">Host name.</param>
        /// <param name="direction">Whether to request current, next or previous day session activity data.</param>
        /// <returns>The default view.</returns>
        public ActionResult StatisticsSessionGraph(string host, int direction)
        {
            Configuration configuration = this.Session[HomeController.ConfigurationSessionKey] as Configuration;
            StatisticsService service = null;

            if (configuration != null)
            {
                service = configuration.GetService("StatisticsServer") as StatisticsService;
            }

            if (service != null)
            {
                StatisticsServiceController.statsQueryOffset = Math.Min(StatisticsServiceController.statsQueryOffset + direction, 0);
                DateTime startTime = this.GetSessionQueryStartTime();
                string status = service.GetSessionActivity(host, startTime, (int)StatisticsServiceController.rangeType);

                if (status != string.Empty)
                {
                    string response = this.GetSessionData(status);
                    string[] statistics = response.Split('|');

                    if (statistics.Length == 4)
                    {
                        this.ViewBag.SessionStarts = JsonHelper.StringToJson(string.Format("{{ \"plot\": {0} }}", statistics[0]));
                        this.ViewBag.ConcurrentUsers = JsonHelper.StringToJson(string.Format("{{ \"plot\": {0} }}", statistics[1]));
                        this.ViewBag.UniqueUsers = statistics[2];
                        this.ViewBag.AverageSessionTime = statistics[3];
                    }
                }

                this.ViewBag.Date = this.GetSessionQueryTimeSpanString(startTime);
                this.ViewBag.RangeType = (int)StatisticsServiceController.rangeType;
            }

            this.ViewData.Model = host;

            return View();
        }

        /// <summary>
        /// Display the scene status for the statistics server running in the given germ.
        /// </summary>
        /// <param name="host">Germ host name, including the port number.</param>
        /// <returns>Return the scene status for the statistics server running in the given germ.</returns>
        public ActionResult QueryScenes(string host)
        {
            Configuration configuration = this.Session[HomeController.ConfigurationSessionKey] as Configuration;
            BaseService service = null;

            if (configuration != null)
            {
                service = configuration.GetService("StatisticsServer");
            }

            if (service != null)
            {
                string status = service.GetStatus(host);

                if (status != string.Empty)
                {
                    string response = this.GetSceneData(status);

                    // TODO: Ideally, it should use Json format output.
                    return Content(response);
                }
            }

            return View();
        }

        /// <summary>
        /// Display the session activity for the statistics server running in the given germ.
        /// </summary>
        /// <param name="host">Germ host name, including the port number.</param>
        /// <param name="rangeType">Range type.</param>
        /// <returns>Return the session activity for the statistics server running in the given germ.</returns>
        public ActionResult QuerySessionActivity(string host, int rangeType)
        {
            this.DetermineNewRange((RangeType)rangeType);

            Configuration configuration = this.Session[HomeController.ConfigurationSessionKey] as Configuration;
            StatisticsService service = null;

            if (configuration != null)
            {
                service = configuration.GetService("StatisticsServer") as StatisticsService;
            }

            if (service != null)
            {
                DateTime startTime = this.GetSessionQueryStartTime();
                string status = service.GetSessionActivity(host, startTime, rangeType);

                if (status != string.Empty)
                {
                    string response = this.GetSessionData(status);
                    response += "|" + this.GetSessionQueryTimeSpanString(startTime);

                    // TODO: Ideally, it should use Json format output.
                    return Content(response);
                }
            }

            return View();
        }

        /// <summary>
        /// Updates and returns a string describing scene plot data.
        /// </summary>
        /// <param name="status">Status string from the Statistics Server.</param>
        /// <returns>String describing plot data.</returns>
        private string GetSceneData(string status)
        {
            long time = this.GetJavascriptTimestamp(DateTime.Now);
            this.UpdateSceneData(status, time);
            this.UpdateTotalData(status, time);
            this.RemoveEmptySceneData();

            string data = "[";
            string totalPoints = StatisticsServiceController.totalData[0];

            for (int i = 1; i < StatisticsServiceController.totalData.Count; ++i)
            {
                totalPoints += "," + StatisticsServiceController.totalData[i];
            }

            data += "{data: [" + totalPoints + "], label: \"Total Users\"}";

            Dictionary<string, List<string>>.Enumerator sceneEnum = StatisticsServiceController.sceneData.GetEnumerator();

            while (sceneEnum.MoveNext())
            {
                string name = sceneEnum.Current.Key;
                List<string> plots = sceneEnum.Current.Value;

                string points = plots[0];

                for (int i = 1; i < plots.Count; ++i)
                {
                    points += "," + plots[i];
                }

                data += ",{data: [" + points + "], label: \"" + name + "\"}";
            }

            XmlDocument document = new XmlDocument();
            document.LoadXml(status);

            string numSessions = document.SelectSingleNode("Statistics/Statistics/NumberOfSessions").InnerText;

            data += "]|" + numSessions;

            return data;
        }

        /// <summary>
        /// Update the collection of scene plot points.
        /// </summary>
        /// <param name="status">Status string from the Statistics Server.</param>
        /// <param name="time">Javascript time stamp.</param>
        private void UpdateSceneData(string status, long time)
        {
            XmlDocument document = new XmlDocument();
            document.LoadXml(status);

            XmlNodeList sceneNodeList = document.SelectNodes("Statistics/Scenes/Scene");
            Dictionary<string, string> currScenes = new Dictionary<string, string>();

            for (int i = 0; i < sceneNodeList.Count; ++i)
            {
                XmlNode sceneNode = sceneNodeList[i];
                string name = sceneNode.SelectSingleNode("Name").InnerText;
                string users = sceneNode.SelectSingleNode("Users").InnerText;
                string plot = string.Format("[{0}, {1}]", time, users);
                currScenes.Add(name, plot);

                if (!StatisticsServiceController.sceneData.ContainsKey(name))
                {
                    StatisticsServiceController.sceneData.Add(name, new List<string>());
                }
            }

            string zeroPlot = string.Format("[{0}, 0]", time);
            List<string> scenesToRemove = new List<string>();
            Dictionary<string, List<string>>.Enumerator sceneEnum = StatisticsServiceController.sceneData.GetEnumerator();

            while (sceneEnum.MoveNext())
            {
                string name = sceneEnum.Current.Key;
                List<string> plots = sceneEnum.Current.Value;

                if (currScenes.ContainsKey(name))
                {
                    plots.Add(currScenes[name]);
                }
                else
                {
                    plots.Add(zeroPlot);
                }

                if (plots.Count > this.plotCountMax)
                {
                    plots.RemoveAt(0);
                }
            }
        }

        /// <summary>
        /// Update the total users across all scenes.
        /// </summary>
        /// <param name="status">Status string from the Statistics Server.</param>
        /// <param name="time">Javascript time stamp.</param>
        private void UpdateTotalData(string status, long time)
        {
            XmlDocument document = new XmlDocument();
            document.LoadXml(status);

            XmlNodeList sceneNodeList = document.SelectNodes("Statistics/Scenes/Scene");
            int totalUsers = 0;

            for (int i = 0; i < sceneNodeList.Count; ++i)
            {
                XmlNode sceneNode = sceneNodeList[i];
                string users = sceneNode.SelectSingleNode("Users").InnerText;
                totalUsers += int.Parse(users);
            }

            string totalPlot = string.Format("[{0}, {1}]", time, totalUsers);
            StatisticsServiceController.totalData.Add(totalPlot);

            if (StatisticsServiceController.totalData.Count > this.plotCountMax)
            {
                StatisticsServiceController.totalData.RemoveAt(0);
            }
        }

        /// <summary>
        /// Remove empty scene data.
        /// </summary>
        private void RemoveEmptySceneData()
        {
            List<string> scenesToRemove = new List<string>();
            Dictionary<string, List<string>>.Enumerator sceneEnum = StatisticsServiceController.sceneData.GetEnumerator();

            while (sceneEnum.MoveNext())
            {
                string name = sceneEnum.Current.Key;
                List<string> plots = sceneEnum.Current.Value;
                bool removeScene = true;

                for (int i = 0; i < plots.Count; ++i)
                {
                    if (!plots[i].Contains(", 0]"))
                    {
                        removeScene = false;
                        break;
                    }
                }

                if (removeScene)
                {
                    scenesToRemove.Add(name);
                }
            }

            for (int i = 0; i < scenesToRemove.Count; ++i)
            {
                StatisticsServiceController.sceneData.Remove(scenesToRemove[i]);
            }
        }

        /// <summary>
        /// Get JavaScriptTimeStamp from a given DateTime object.
        /// </summary>
        /// <param name="input">Date time object.</param>
        /// <returns>Return long ticks.</returns>
        private long GetJavascriptTimestamp(DateTime input)
        {
            TimeSpan span = new TimeSpan(DateTime.Parse("1/1/1970").Ticks);
            DateTime time = input.Subtract(span);
            return (long)(time.Ticks / 10000);
        }

        /// <summary>
        /// Returns a string describing session plot data.
        /// </summary>
        /// <param name="status">Status string from the Statistics Server.</param>
        /// <returns>String describing plot data.</returns>
        private string GetSessionData(string status)
        {
            string data = string.Empty;

            XmlDocument document = new XmlDocument();
            document.LoadXml(status);

            XmlNodeList sessionNodeList = document.SelectNodes("Statistics/Statistics/SessionStarts");
            XmlNodeList concurrentNodeList = document.SelectNodes("Statistics/Statistics/ConcurrentUsers");
            string uniqueUsers = document.SelectSingleNode("Statistics/Statistics/UniqueUsers").InnerText;
            string averageSessionTime = document.SelectSingleNode("Statistics/Statistics/AverageSessionTime").InnerText;

            if ((StatisticsServiceController.rangeType == RangeType.Week) && (sessionNodeList.Count == 28) && (concurrentNodeList.Count == 28))
            {
                data += "[{data: [[0, " + sessionNodeList[0].InnerText + "], [1, " + sessionNodeList[1].InnerText + "], [2, " +
                    sessionNodeList[2].InnerText + "], [3, " + sessionNodeList[3].InnerText + "]], label: \"Sunday\"}";
                data += ", {data: [[4, " + sessionNodeList[4].InnerText + "], [5, " + sessionNodeList[5].InnerText + "], [6, " +
                    sessionNodeList[6].InnerText + "], [7, " + sessionNodeList[7].InnerText + "]], label: \"Monday\"}";
                data += ", {data: [[8, " + sessionNodeList[8].InnerText + "], [9, " + sessionNodeList[9].InnerText + "], [10, " +
                    sessionNodeList[10].InnerText + "], [11, " + sessionNodeList[11].InnerText + "]], label: \"Tuesday\"}";
                data += ", {data: [[12, " + sessionNodeList[12].InnerText + "], [13, " + sessionNodeList[13].InnerText + "], [14, " +
                    sessionNodeList[14].InnerText + "], [15, " + sessionNodeList[15].InnerText + "]], label: \"Wednesday\"}";
                data += ", {data: [[16, " + sessionNodeList[16].InnerText + "], [17, " + sessionNodeList[17].InnerText + "], [18, " +
                    sessionNodeList[18].InnerText + "], [19, " + sessionNodeList[19].InnerText + "]], label: \"Thursday\"}";
                data += ", {data: [[20, " + sessionNodeList[20].InnerText + "], [21, " + sessionNodeList[21].InnerText + "], [22, " +
                    sessionNodeList[22].InnerText + "], [23, " + sessionNodeList[23].InnerText + "]], label: \"Friday\"}";
                data += ", {data: [[24, " + sessionNodeList[24].InnerText + "], [25, " + sessionNodeList[25].InnerText + "], [26, " +
                    sessionNodeList[26].InnerText + "], [27, " + sessionNodeList[27].InnerText + "]], label: \"Saturday\"}]";

                data += "|";

                data += "[{data: [[0, " + concurrentNodeList[0].InnerText + "], [1, " + concurrentNodeList[1].InnerText + "], [2, " +
                    concurrentNodeList[2].InnerText + "], [3, " + concurrentNodeList[3].InnerText + "]], label: \"Sunday\"}";
                data += ", {data: [[4, " + concurrentNodeList[4].InnerText + "], [5, " + concurrentNodeList[5].InnerText + "], [6, " +
                    concurrentNodeList[6].InnerText + "], [7, " + concurrentNodeList[7].InnerText + "]], label: \"Monday\"}";
                data += ", {data: [[8, " + concurrentNodeList[8].InnerText + "], [9, " + concurrentNodeList[9].InnerText + "], [10, " +
                    concurrentNodeList[10].InnerText + "], [11, " + concurrentNodeList[11].InnerText + "]], label: \"Tuesday\"}";
                data += ", {data: [[12, " + concurrentNodeList[12].InnerText + "], [13, " + concurrentNodeList[13].InnerText + "], [14, " +
                    concurrentNodeList[14].InnerText + "], [15, " + concurrentNodeList[15].InnerText + "]], label: \"Wednesday\"}";
                data += ", {data: [[16, " + concurrentNodeList[16].InnerText + "], [17, " + concurrentNodeList[17].InnerText + "], [18, " +
                    concurrentNodeList[18].InnerText + "], [19, " + concurrentNodeList[19].InnerText + "]], label: \"Thursday\"}";
                data += ", {data: [[20, " + concurrentNodeList[20].InnerText + "], [21, " + concurrentNodeList[21].InnerText + "], [22, " +
                    concurrentNodeList[22].InnerText + "], [23, " + concurrentNodeList[23].InnerText + "]], label: \"Friday\"}";
                data += ", {data: [[24, " + concurrentNodeList[24].InnerText + "], [25, " + concurrentNodeList[25].InnerText + "], [26, " +
                    concurrentNodeList[26].InnerText + "], [27, " + concurrentNodeList[27].InnerText + "]], label: \"Saturday\"}]";
            }
            else
            {
                data += "[{data: [";

                for (int i = 0; i < sessionNodeList.Count; ++i)
                {
                    string plot = string.Format("[{0}, {1}]", i.ToString(), sessionNodeList[i].InnerText);
                    data += (i > 0) ? (", " + plot) : plot;
                }

                data += "], label: \"Session Starts\"}]|[{data: [";

                for (int i = 0; i < concurrentNodeList.Count; ++i)
                {
                    string plot = string.Format("[{0}, {1}]", i.ToString(), concurrentNodeList[i].InnerText);
                    data += (i > 0) ? (", " + plot) : plot;
                }

                data += "], label: \"Concurrent Users\"}]";
            }

            data += "|" + uniqueUsers + "|" + double.Parse(averageSessionTime).ToString("#0.00");

            return data;
        }

        /// <summary>
        /// Gets the start time for a session activity query.
        /// </summary>
        /// <returns>Session query start time.</returns>
        private DateTime GetSessionQueryStartTime()
        {
            DateTime startTime;
            DateTime currentDay = DateTime.Today;

            switch (StatisticsServiceController.rangeType)
            {
                default:
                case RangeType.Day:
                    startTime = currentDay.AddDays(StatisticsServiceController.statsQueryOffset);
                    break;

                case RangeType.Week:
                    DateTime startOfWeek = currentDay.AddDays(-(int)currentDay.DayOfWeek);
                    startTime = startOfWeek.AddDays(StatisticsServiceController.statsQueryOffset * 7);
                    break;

                case RangeType.Month:
                    DateTime startOfMonth = currentDay.AddDays(1 - currentDay.Day);
                    startTime = startOfMonth.AddMonths(StatisticsServiceController.statsQueryOffset);
                    break;
            }

            return startTime;
        }

        /// <summary>
        /// Gets the time span string for a session activity query.
        /// </summary>
        /// <param name="startTime">The start time.</param>
        /// <returns>Session query time span string.</returns>
        private string GetSessionQueryTimeSpanString(DateTime startTime)
        {
            string timeSpan = string.Empty;
            string dateTimeFormat = "d MMMM yyyy";

            switch (StatisticsServiceController.rangeType)
            {
                default:
                case RangeType.Day:
                    {
                        timeSpan = startTime.ToString(dateTimeFormat);
                    }

                    break;

                case RangeType.Week:
                    {
                        DateTime endTime = startTime.AddDays(6);
                        timeSpan = string.Format("{0} - {1}", startTime.ToString(dateTimeFormat), endTime.ToString(dateTimeFormat));
                    }

                    break;

                case RangeType.Month:
                    {
                        DateTime startOfNextMonth = startTime.AddMonths(1);
                        DateTime endTime = startOfNextMonth.AddDays(-1);
                        timeSpan = string.Format("{0} - {1}", startTime.ToString(dateTimeFormat), endTime.ToString(dateTimeFormat));
                    }

                    break;
            }

            return timeSpan;
        }

        /// <summary>
        /// Determine new stats query offset when range type changes.
        /// </summary>
        /// <param name="newRangeType">New range type.</param>
        private void DetermineNewRange(RangeType newRangeType)
        {
            if (StatisticsServiceController.rangeType == newRangeType)
            {
                return;
            }

            DateTime currentDay = DateTime.Today;
            DateTime oldStartTime = this.GetSessionQueryStartTime();

            switch (newRangeType)
            {
                default:
                case RangeType.Day:
                    {
                        StatisticsServiceController.statsQueryOffset = Math.Min((int)(oldStartTime - currentDay).TotalDays, 0);
                    }

                    break;

                case RangeType.Week:
                    {
                        DateTime startOfThisWeek = currentDay.AddDays(-(int)currentDay.DayOfWeek);
                        DateTime startOfThatWeek = oldStartTime.AddDays(-(int)oldStartTime.DayOfWeek);
                        StatisticsServiceController.statsQueryOffset = Math.Min((int)(startOfThatWeek - startOfThisWeek).TotalDays / 7, 0);
                    }

                    break;

                case RangeType.Month:
                    {
                        DateTime startOfThisMonth = currentDay.AddDays(1 - currentDay.Day);
                        DateTime startOfThatMonth = oldStartTime.AddDays(1 - oldStartTime.Day);

                        StatisticsServiceController.statsQueryOffset = 0;

                        while (startOfThisMonth.AddMonths(StatisticsServiceController.statsQueryOffset).Month != startOfThatMonth.Month)
                        {
                            StatisticsServiceController.statsQueryOffset--;
                        }
                    }

                    break;
            }

            StatisticsServiceController.rangeType = newRangeType;
        }
    }
}