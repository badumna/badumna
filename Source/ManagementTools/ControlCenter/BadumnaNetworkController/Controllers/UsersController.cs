//---------------------------------------------------------------------------------
// <copyright file="UsersController.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.SessionState;

using Dei;
using Dei.AdminClient;

using Germinator;

namespace BadumnaNetworkController.Controllers
{
    /// <summary>
    /// UserController is only used when there is a Dei type of service in the ControlCenter. It used to maintain the user account,
    /// (i.e. grant and revoke permission, find a user from the database, create a new account, etc.).
    /// </summary>
    public class UsersController : Controller
    {
        /// <summary>
        /// Admin client session key.
        /// </summary>
        internal const string AdminClientSessionKey = "AdminClient";

        /// <summary>
        /// Permission list session key.
        /// </summary>
        private const string PermissionListSessionKey = "UsersPermissionList";
        
        /// <summary>
        /// Username session key.
        /// </summary>
        private const string UsernameSessionKey = "DeiUsername";
        
        /// <summary>
        /// Cursor session key.
        /// </summary>
        private const string CursorSessionKey = "Cursor";

        /// <summary>
        /// Permission list session key.
        /// </summary>
        private const string AllPermissionListSessionKey = "AllPermissionList";

        /// <summary>
        /// List of all available account type.
        /// </summary>
        public enum AccountTypes
        {
            /// <summary>
            /// User account.
            /// </summary>
            User,

            /// <summary>
            /// Admin account.
            /// </summary>
            Admin,

            /// <summary>
            /// Service account.
            /// </summary>
            HostedService,

            /// <summary>
            /// Custom account.
            /// </summary>
            Custom
        }

        /// <summary>
        /// Check whether a session has a specific permission.
        /// </summary>
        /// <param name="session">Http Session.</param>
        /// <param name="permissionType">Permission type.</param>
        /// <returns>Return true if a session has that permission.</returns>
        public static bool HasPermission(HttpSessionState session, PermissionTypes permissionType)
        {
            IList<PermissionProperties> permissions = (IList<PermissionProperties>)session[UsersController.PermissionListSessionKey];
            if (permissions != null)
            {
                foreach (PermissionProperties permission in permissions)
                {
                    if ((long)permissionType == permission.Id)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// Check whether a session has logged in.
        /// </summary>
        /// <param name="session">Http session.</param>
        /// <returns>Return true if it has logged in.</returns>
        public static bool IsLoggedIn(HttpSessionState session)
        {
            return session[UsersController.AdminClientSessionKey] != null;
        }

        /// <summary>
        /// Get the error message description from AdminClient Failure reasons.
        /// </summary>
        /// <param name="client">Admin client.</param>
        /// <returns>Retrun the error message.</returns>
        public static string GetErrorDescription(AdminClient client)
        {
            switch (client.FailureReason)
            {
                case FailureReasons.NoneGiven:
                    return Resources.DeiError_NoReason;

                case FailureReasons.PermissionDenied:
                    return Resources.DeiError_PermissionDenied;

                case FailureReasons.TooManyCharactersInLogin:
                    return Resources.DeiError_LoginTooLong;

                case FailureReasons.InvalidRequest:
                    return Resources.DeiError_InvalidRequest;

                case FailureReasons.AuthenticationFailed:
                    return Resources.DeiError_AuthenticationFailed;

                case FailureReasons.AccountAlreadyExists:
                    return Resources.DeiError_AlreadyExists;

                case FailureReasons.ConnectionError:
                    return Resources.DeiError_ConnectionError;
            }

            return Resources.DeiError_UnknownError;
        }

        /// <summary>
        /// Return the index page of the user controller. It will require an admin login before the user can use it. 
        /// </summary>
        /// <returns>Return user  account index page.</returns>
        public ActionResult Index()
        {
            if (!this.IsLoggedIn())
            {
                return this.RedirectToAction("Login");
            }

            this.ViewData.Model = this.Session[UsersController.UsernameSessionKey];
            return PartialView("Index");
        }

        /// <summary>
        /// Login page to the Dei server.
        /// </summary>
        /// <returns>Return the login page.</returns>
        public ActionResult Login()
        {
            if (this.IsLoggedIn())
            {
                return PartialView("LoggedIn");
            }

            return PartialView("Login");
        }

        /// <summary>
        /// Post method of Login page, collect the user information and used it to login to the Dei server.
        /// </summary>
        /// <param name="username">Username (usually admin username).</param>
        /// <param name="password">The corresponding password.</param>
        /// <returns>Redirect to the index page.</returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Login(string username, string password)
        {
            try
            {
                if (string.IsNullOrEmpty(username))
                {
                    this.ModelState.AddModelError("username", Resources.MustGiveName);
                }

                if (string.IsNullOrEmpty(password))
                {
                    this.ModelState.AddModelError("password", Resources.PasswordInvalid);
                }

                if (!this.ModelState.IsValid)
                {
                    return PartialView("Login");
                }
            }
            catch
            {
                return PartialView("Login");                
            }

            try
            {
                string errorDescription;
                if (this.LoginAndSaveSession(username, password, out errorDescription))
                {
                    return PartialView("LoggedIn");
                }
                else
                {
                    this.ModelState.AddModelError("all", String.Format("Failed to login : {0}", errorDescription));
                    return PartialView("Login");
                }
            }
            catch
            {
                return PartialView("Index");
            }
        }

        /// <summary>
        /// Logout from the current session.
        /// </summary>
        /// <returns>Redirect to login page.</returns>
        public ActionResult Logout()
        {
            this.LogoutAndReleaseSession();
            return this.RedirectToAction("Login");
        }

        /// <summary>
        /// Change the password. 
        /// </summary>
        /// <returns>Return ChangePassword.aspx page.</returns>
        public ActionResult ChangePassword()
        {
            string username = (string)this.Session[UsersController.UsernameSessionKey];
            if (string.IsNullOrEmpty(username))
            {
                return this.RedirectToAction("Login");
            }

            this.ViewBag.username = username;
            return PartialView("ChangePassword");
        }

        /// <summary>
        /// Post method of Change password, collect the information from the user, validate the old and the new password for 
        /// a given username.
        /// </summary>
        /// <param name="username">Dei username.</param>
        /// <param name="oldPassword">Old password.</param>
        /// <param name="newPassword">New password.</param>
        /// <param name="passwordConfirmation">New password confirmation.</param>
        /// <returns>Redirect to index page.</returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ChangePassword(string username, string oldPassword, string newPassword, string passwordConfirmation)
        {
            if (string.IsNullOrEmpty(username))
            {
                return this.RedirectToDefault();
            }

            this.ViewBag.username = username;
            try
            {
                if (string.IsNullOrEmpty(oldPassword))
                {
                    this.ModelState.AddModelError("oldPassword", Resources.PasswordInvalid);
                }

                if (string.IsNullOrEmpty(newPassword))
                {
                    this.ModelState.AddModelError("newPassword", Resources.PasswordInvalid);
                }

                if (string.IsNullOrEmpty(passwordConfirmation))
                {
                    this.ModelState.AddModelError("passwordConfirmation", Resources.PasswordInvalid);
                }

                if (newPassword != passwordConfirmation)
                {
                    this.ModelState.AddModelError("all", Resources.PasswordMismatch);
                }

                if (!this.ModelState.IsValid)
                {
                    return PartialView("ChangePassword");
                }
            }
            catch
            {
                return PartialView("ChangePassword");
            }

            try
            {
                using (AdminClient client = new AdminClient())
                {
                    client.IgnoreSslErrors = true;                 // TODO : Not sure what to do about this.
                    string hostName;
                    int port;

                    Configuration configuration = this.Session[HomeController.ConfigurationSessionKey] as Configuration;
                    if (configuration != null && configuration.GetDeiHost(out hostName, out port))
                    {
                        if (client.Login(hostName, port, username, oldPassword))
                        {
                            if (client.ChangePassword(newPassword))
                            {
                                string errorDescription;
                                if (this.LoginAndSaveSession(username, newPassword, out errorDescription))
                                {
                                    this.ViewBag.Message = String.Format("The password for '{0}' has been successfully changed", username);
                                    return PartialView("LoggedIn");
                                }
                                else
                                {
                                    this.ModelState.AddModelError("all", string.Format("Failed to login with new password : {0}", errorDescription));
                                }
                            }
                            else
                            {
                                this.ModelState.AddModelError("all", String.Format("Failed to change the password : {0},", UsersController.GetErrorDescription(client)));
                            }
                        }
                        else
                        {
                            this.ModelState.AddModelError("all", string.Format("Failed to login with old pasword : {0}", UsersController.GetErrorDescription(client)));
                        }
                    }
                    else
                    {
                        this.ModelState.AddModelError("all", Resources.FailedToFindDei);
                    }
                }
            }
            catch
            {
                this.ModelState.AddModelError("all", "An unexpected error has occurred. Please try again.");
            }

            return PartialView("ChangePassword");
        }

        /// <summary>
        /// Create a new user/account, only available when the user is logged in. 
        /// </summary>
        /// <returns>Return the create.aspx page when the user is logged in.</returns>
        public ActionResult Create()
        {
            if (!this.IsLoggedIn())
            {
                return this.RedirectToAction("Login");
            }

            return View();
        }

        /// <summary>
        /// Post method of Create a new user/account, collect the information from the user including the 
        /// username, password and the password confirmation.
        /// </summary>
        /// <param name="username">Dei username.</param>
        /// <param name="password">Corresponding password.</param>
        /// <param name="passwordConfirmation">Password confirmation.</param>
        /// <param name="collection">The form collection obtained from the client.</param>
        /// <returns>Return to index page.</returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Create(string username, string password, string passwordConfirmation, FormCollection collection)
        {
            try
            {
                if (string.IsNullOrEmpty(username))
                {
                    this.ModelState.AddModelError("username", Resources.MustGiveName);
                }

                if (!InputValidationHelper.ValidateAlphanumeric(username))
                {
                    this.ModelState.AddModelError("username", Resources.InvalidName);
                }

                if (string.IsNullOrEmpty(password))
                {
                    this.ModelState.AddModelError("password", Resources.PasswordInvalid);
                }

                if (string.IsNullOrEmpty(passwordConfirmation))
                {
                    this.ModelState.AddModelError("passwordConfirmation", Resources.PasswordInvalid);
                }

                if (password != passwordConfirmation)
                {
                    this.ModelState.AddModelError("all", Resources.PasswordMismatch);
                }

                List<string> keys = new List<string>(collection.AllKeys.Length);
                keys.AddRange(collection.AllKeys);

                if (keys.IndexOf("accountType") == -1)
                {
                    this.ModelState.AddModelError("accountType", "Account type is invalid"); 
                }

                if (!this.ModelState.IsValid)
                {
                    return PartialView("LoggedIn");
                }

                AdminClient client = this.GetClient();

                if (client == null)
                {
                    return this.RedirectToAction("Login");
                }

                if (client.CreateUser(username, password))
                {
                    int index = keys.IndexOf("accountType");

                    switch ((AccountTypes)Enum.Parse(typeof(AccountTypes), collection[index]))
                    {
                        case AccountTypes.User:
                            client.GrantPermission(username, (long)PermissionTypes.Participation);
                            break;
                        case AccountTypes.Admin:
                            client.GrantPermission(username, (long)PermissionTypes.Admin);
                            client.GrantPermission(username, (long)PermissionTypes.AssignPermissions);
                            client.GrantPermission(username, (long)PermissionTypes.CreatePermissions);
                            client.GrantPermission(username, (long)PermissionTypes.CreateUsers);
                            client.GrantPermission(username, (long)PermissionTypes.ViewPermissions);
                            client.GrantPermission(username, (long)PermissionTypes.ViewUsers);
                            break;
                        case AccountTypes.HostedService:
                            client.GrantPermission(username, (long)PermissionTypes.Announce);
                            client.GrantPermission(username, (long)PermissionTypes.Participation);
                            break;
                        case AccountTypes.Custom:
                            for (int i = 0; i < collection.Count; i++)
                            {
                                PermissionTypes type = PermissionTypes.None;

                                try
                                {
                                    type = (PermissionTypes)Enum.Parse(typeof(PermissionTypes), collection.GetKey(i));
                                }
                                catch (ArgumentException)
                                {
                                }

                                if (type != PermissionTypes.None && collection[i].ToLower().StartsWith("true"))
                                {
                                    switch (type)
                                    {
                                        case PermissionTypes.Admin:
                                            client.GrantPermission(username, (long)PermissionTypes.Admin);
                                            break;
                                        case PermissionTypes.CreateUsers:
                                            client.GrantPermission(username, (long)PermissionTypes.CreateUsers);
                                            break;
                                        case PermissionTypes.ViewUsers:
                                            client.GrantPermission(username, (long)PermissionTypes.ViewUsers);
                                            break;
                                        case PermissionTypes.CreatePermissions:
                                            client.GrantPermission(username, (long)PermissionTypes.CreatePermissions);
                                            break;
                                        case PermissionTypes.AssignPermissions:
                                            client.GrantPermission(username, (long)PermissionTypes.AssignPermissions);
                                            break;
                                        case PermissionTypes.ViewPermissions:
                                            client.GrantPermission(username, (long)PermissionTypes.ViewPermissions);
                                            break;
                                        case PermissionTypes.Participation:
                                            client.GrantPermission(username, (long)PermissionTypes.Participation);
                                            break;
                                        case PermissionTypes.Announce:
                                            client.GrantPermission(username, (long)PermissionTypes.Announce);
                                            break;
                                    }
                                }
                            }

                            break;
                    }

                    this.ViewBag.Message = String.Format("User account '{0}' has been created, WITH {1} account type.", username, collection[index]);
                    DebugConsole.Instance.EnqueueTraceInfo(String.Format("User account '{0}' has been created, WITH {1} account type.", username, collection[index]));
                    return PartialView("LoggedIn");
                }
                else
                {
                    this.ModelState.AddModelError("all", string.Format("Failed to create account for '{0}' : {1}", username, UsersController.GetErrorDescription(client)));
                    return PartialView("LoggedIn");
                }
            }
            catch
            {
                return PartialView("LoggedIn");
            }
        }

        /// <summary>
        /// Remove a particular account based on the given username.
        /// </summary>
        /// <param name="username">Dei username.</param>
        /// <returns>Return to the index page.</returns>
        public ActionResult Remove(string username)
        {
            try
            {
                if (string.IsNullOrEmpty(username))
                {
                    this.ViewBag.Message = "Failed to delete account: username is invalid";
                    DebugConsole.Instance.EnqueueTraceInfo("Failed to delete account: username is invalid");
                    return this.Index();
                }

                AdminClient client = this.GetClient();
                if (client == null)
                {
                    return this.RedirectToAction("Login");
                }

                if (client.RemoveUser(username))
                {
                    this.ViewBag.Message = String.Format("User account '{0}' has been deleted", username);
                    DebugConsole.Instance.EnqueueTraceInfo(String.Format("User account '{0}' has been deleted", username));      
                }
                else
                {
                    this.ViewBag.Message = String.Format("Failed to delete account'{0}' : {1}", username, UsersController.GetErrorDescription(client));
                    DebugConsole.Instance.EnqueueTraceInfo(String.Format("Failed to delete account'{0}' : {1}", username, UsersController.GetErrorDescription(client)));
                }

                if (this.Session[UsersController.CursorSessionKey] == null)
                {
                    // by default display all users.
                    ListUserCursor cursor = client.FindUsers("%", 10);
                    if (cursor != null)
                    {
                        this.Session[UsersController.CursorSessionKey] = cursor;
                        return this.PartialView("ListUsers", cursor);
                    }
                }

                return PartialView("ListUsers", this.Session[UsersController.CursorSessionKey]);
            }
            catch
            {
                return Content(String.Format("Exception occurs when try to remove: {0}", username));
            }
        }

        /// <summary>
        /// Requesting all available permission type.
        /// </summary>
        /// <returns>Return the permission types.</returns>
        public ActionResult GetPermissions()
        {
            // currently we can use the PermissionController to get list of all permission
            List<PermissionProperties> permissions = this.GetPermissionList(this.GetClient());
            
            this.ViewBag.PermissionsTypes = permissions;
            this.ViewBag.AccountTypes = (AccountTypes[])Enum.GetValues(typeof(AccountTypes));
            return PartialView("Permissions");
        }

        /// <summary>
        /// Search the user from the database for a given username, or use '*' to display all users from database.
        /// </summary>
        /// <param name="username">Searched username.</param>
        /// <returns>Return the partial view of "ListUsers".</returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Search(string username)
        {
            if (!this.Request.IsAjaxRequest())
            {
                return this.RedirectToDefault();
            }

            try
            {
                if (string.IsNullOrEmpty(username))
                {
                    return this.Content("No results found");
                }

                AdminClient client = this.GetClient();
                if (client == null)
                {
                    return this.Content("Not logged in!");
                }

                ListUserCursor cursor = client.FindUsers(username, 10);
                if (cursor != null)
                {
                    this.Session[UsersController.CursorSessionKey] = cursor;
                    return this.PartialView("ListUsers", cursor);
                }

                return this.Content("No results found");
            }
            catch
            {
                return this.Content("Unexpected error while searching");
            }
        }

        /// <summary>
        /// Get all users that are available from Dei server.
        /// </summary>
        /// <returns>Return the partial view of list users.</returns>
        public ActionResult AllUsers()
        {
            try
            {
                AdminClient client = this.GetClient();
                ListUserCursor cursor = client.FindUsers("%", 10);
                if (cursor != null)
                {
                    this.Session[UsersController.CursorSessionKey] = cursor;
                    return this.PartialView("ListUsers", cursor);
                }

                return this.Content("No users found");
            }
            catch
            {
                return this.Content("Unexpected error occurs.");
            }
        }

        /// <summary>
        /// Next action is used in paging. It is used to display the next page if applicable.
        /// </summary>
        /// <returns>Return the partial view of "ListUsers".</returns>
        public ActionResult Next()
        {
            if (!this.Request.IsAjaxRequest())
            {
                return this.RedirectToDefault();
            }

            try
            {
                ListUserCursor cursor = (ListUserCursor)this.Session[UsersController.CursorSessionKey];
                if (cursor != null)
                {
                    cursor.Next();
                    return this.PartialView("ListUsers", cursor);
                }

                return this.Content("No results found");
            }
            catch
            {
                return this.Content("Unexpected error while searching");
            }
        }

        /// <summary>
        /// Previous is the opposite of next (display the previous page if applicable).
        /// </summary>
        /// <returns>Return the partial view of "ListUsers".</returns>
        public ActionResult Previous()
        {
            if (!this.Request.IsAjaxRequest())
            {
                return this.RedirectToDefault();
            }

            try
            {
                ListUserCursor cursor = (ListUserCursor)this.Session[UsersController.CursorSessionKey];
                if (cursor != null)
                {
                    cursor.Previous();
                    return this.PartialView("ListUsers", cursor);
                }

                return this.Content("No results found");
            }
            catch
            {
                return this.Content("Unexpected error while searching");
            }
        }

        /// <summary>
        /// Edit a particular account based on the given username, it only available after logging in.
        /// </summary>
        /// <param name="username">Edited username.</param>
        /// <returns>Return the Edit.aspx page.</returns>
        public ActionResult Edit(string username)
        {
            AdminClient client = this.GetClient();
            if (client == null)
            {
                return this.RedirectToAction("Login");
            }

            if (string.IsNullOrEmpty(username))
            {
                return this.RedirectToDefault();
            }

            try
            {
                this.PrepareViewDataForUsersPermissions(client, username);
                return PartialView("EditUser");
            }
            catch
            {
                return this.RedirectToDefault();
            }
        }

        /// <summary>
        /// Post method for editing the user permissions/account type.
        /// </summary>
        /// <param name="username">User name.</param>
        /// <param name="collection">The form collection collected from client browser.</param>
        /// <returns>Return "succeed" content if the user is edited successfully.</returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Edit(string username, FormCollection collection)
        {
            List<string> keys = new List<string>(collection.AllKeys.Length);
            keys.AddRange(collection.AllKeys);

            if (keys.IndexOf(username + ":accountType") == -1)
            {
                this.ModelState.AddModelError("accountType", "Account type is invalid");
            }

            if (!this.ModelState.IsValid)
            {
                return Content("Failed");
            }

            AdminClient client = this.GetClient();

            if (client == null)
            {
                return this.RedirectToAction("Login");
            }

            // need to revoke all the existing permission first.
            foreach (PermissionProperties permission in this.GetPermissionList(client))
            {
                client.RevokePermission(username, permission.Id);
            }

            int index = keys.IndexOf(username + ":accountType");

            switch ((AccountTypes)Enum.Parse(typeof(AccountTypes), collection[index]))
            {
                case AccountTypes.User:
                    client.GrantPermission(username, (long)PermissionTypes.Participation);
                    break;
                case AccountTypes.Admin:
                    client.GrantPermission(username, (long)PermissionTypes.Admin);
                    client.GrantPermission(username, (long)PermissionTypes.AssignPermissions);
                    client.GrantPermission(username, (long)PermissionTypes.CreatePermissions);
                    client.GrantPermission(username, (long)PermissionTypes.CreateUsers);
                    client.GrantPermission(username, (long)PermissionTypes.ViewPermissions);
                    client.GrantPermission(username, (long)PermissionTypes.ViewUsers);
                    break;
                case AccountTypes.HostedService:
                    client.GrantPermission(username, (long)PermissionTypes.Announce);
                    client.GrantPermission(username, (long)PermissionTypes.Participation);
                    break;
                case AccountTypes.Custom:
                    for (int i = 0; i < collection.Count; i++)
                    {
                        PermissionTypes type = PermissionTypes.None;

                        try
                        {
                            string key = collection.GetKey(i);
                            if (key.Contains(":"))
                            {
                                key = key.Split(':')[1];
                            }

                            type = (PermissionTypes)Enum.Parse(typeof(PermissionTypes), key);
                        }
                        catch (ArgumentException)
                        {
                        }

                        if (type != PermissionTypes.None && collection[i].ToLower().StartsWith("true"))
                        {
                            switch (type)
                            {
                                case PermissionTypes.Admin:
                                    client.GrantPermission(username, (long)PermissionTypes.Admin);
                                    break;
                                case PermissionTypes.CreateUsers:
                                    client.GrantPermission(username, (long)PermissionTypes.CreateUsers);
                                    break;
                                case PermissionTypes.ViewUsers:
                                    client.GrantPermission(username, (long)PermissionTypes.ViewUsers);
                                    break;
                                case PermissionTypes.CreatePermissions:
                                    client.GrantPermission(username, (long)PermissionTypes.CreatePermissions);
                                    break;
                                case PermissionTypes.AssignPermissions:
                                    client.GrantPermission(username, (long)PermissionTypes.AssignPermissions);
                                    break;
                                case PermissionTypes.ViewPermissions:
                                    client.GrantPermission(username, (long)PermissionTypes.ViewPermissions);
                                    break;
                                case PermissionTypes.Participation:
                                    client.GrantPermission(username, (long)PermissionTypes.Participation);
                                    break;
                                case PermissionTypes.Announce:
                                    client.GrantPermission(username, (long)PermissionTypes.Announce);
                                    break;
                            }
                        }
                    }

                    break;
            }

            this.ViewBag.Message = String.Format("Successfully changed account type for '{0}'  WITH {1} account.", username, collection[index]);
            DebugConsole.Instance.EnqueueTraceInfo(String.Format("Successfully changed account type for '{0}'  WITH {1} account.", username, collection[index]));
            return Content("Succeed");
        }

        /// <summary>
        /// Post method for granting a permssion to a specific user.
        /// </summary>
        /// <param name="username">Dei username.</param>
        /// <param name="permissionId">Permission id.</param>
        /// <returns>Return the partial view of "UserPermisisons".</returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Grant(string username, long permissionId)
        {
            try
            {
                if (string.IsNullOrEmpty(username))
                {
                    return this.RedirectToDefault();
                }

                if (permissionId < 0)
                {
                    return this.RedirectToDefault();
                }

                AdminClient client = this.GetClient();
                if (client != null)
                {
                    client.GrantPermission(username, permissionId);

                    if (!this.Request.IsAjaxRequest())
                    {
                        return this.RedirectToAction("Edit", new { username = username });
                    }
                    else
                    {
                        this.PrepareViewDataForUsersPermissions(client, username);
                        return this.PartialView("UserPermissions");
                    }
                }
            }
            catch
            {
            }

            return this.RedirectToDefault();
        }

        /// <summary>
        /// Revoke is the opposite of Grant, Revoking a permission from a specific user.
        /// </summary>
        /// <param name="username">Dei username.</param>
        /// <param name="permissionId">Permission id.</param>
        /// <returns>Return the partial view of "UserPermissions".</returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Revoke(string username, long permissionId)
        {
            try
            {
                if (string.IsNullOrEmpty(username))
                {
                    return this.RedirectToDefault();
                }

                if (permissionId < 0)
                {
                    return this.RedirectToDefault();
                }

                AdminClient client = this.GetClient();
                if (client != null)
                {
                    client.RevokePermission(username, permissionId);

                    if (!this.Request.IsAjaxRequest())
                    {
                        return this.RedirectToAction("Edit", new { username = username });
                    }
                    else
                    {
                        this.PrepareViewDataForUsersPermissions(client, username);
                        return this.PartialView("UserPermissions");
                    }
                }
            }
            catch
            {
            }

            return this.RedirectToDefault();
        }

        /// <summary>
        /// Prepare the view data for a user permission.
        /// </summary>
        /// <param name="client">Admin client.</param>
        /// <param name="username">Dei username.</param>
        private void PrepareViewDataForUsersPermissions(AdminClient client, string username)
        {
            IList<PermissionProperties> allPermissions = this.GetPermissionList(client);
            IList<PermissionProperties> userPermissions = client.ListPermissions(username);
            
            if (userPermissions == null)
            {
                userPermissions = new List<PermissionProperties>();
            }
            
            this.ViewBag.PermissionsTypes = allPermissions;
            this.ViewBag.AccountTypes = (AccountTypes[])Enum.GetValues(typeof(AccountTypes));
            
            // determine the user account type based on the user permissions.
            List<long> permissionIds = this.ConvertUserPermissions(userPermissions);
            if (userPermissions.Count == 1 && permissionIds.Contains((long)PermissionTypes.Participation))
            {
                this.ViewBag.AccountType = AccountTypes.User;
            }
            else if (userPermissions.Count == 2 
                && permissionIds.Contains((long)PermissionTypes.Participation)
                && permissionIds.Contains((long)PermissionTypes.Announce))
            {
                this.ViewBag.AccountType = AccountTypes.HostedService;                
            }
            else if (userPermissions.Count == 6
                && !permissionIds.Contains((long)PermissionTypes.Participation)
                && !permissionIds.Contains((long)PermissionTypes.Announce))
            {
                this.ViewBag.AccountType = AccountTypes.Admin;
            }
            else
            {
                this.ViewBag.AccountType = AccountTypes.Custom;
            }

            this.ViewBag.UserPermissions = userPermissions;
            this.ViewBag.Username = username;
        }

        /// <summary>
        /// Check if a session has logged in, <seealso cref="IsLoggedIn(HttpSessionState session)"/>.
        /// </summary>
        /// <returns>Return true if is logged in.</returns>
        private bool IsLoggedIn()
        {
            return this.Session[UsersController.AdminClientSessionKey] != null;
        }

        /// <summary>
        /// Get the admin client.
        /// </summary>
        /// <returns>Return Admin client.</returns>
        private AdminClient GetClient()
        {
            AdminClient client = (AdminClient)this.Session[UsersController.AdminClientSessionKey];
            if (client == null || client.IsClosed)
            {
                if (client != null)
                {
                    client.Close();
                }

                this.Session.Remove(UsersController.AdminClientSessionKey);
                return null;
            }

            return client;
        }

        /// <summary>
        /// Log out and realease current session.
        /// </summary>
        private void LogoutAndReleaseSession()
        {
            try
            {
                this.Session.Remove(UsersController.PermissionListSessionKey);
                this.Session.Remove(UsersController.UsernameSessionKey);

                try
                {
                    this.Session.Remove(UsersController.CursorSessionKey);
                }
                catch 
                { 
                }

                AdminClient oldClient = (AdminClient)this.Session[UsersController.AdminClientSessionKey];
                if (oldClient != null)
                {
                    this.Session.Remove(UsersController.AdminClientSessionKey);
                    oldClient.Dispose();
                }
            }
            catch 
            {
            }
        }

        /// <summary>
        /// Login and save session for a given username.
        /// </summary>
        /// <param name="username">Dei username.</param>
        /// <param name="password">Corresponding password.</param>
        /// <param name="errorDescription">Error description (output) if applicable.</param>
        /// <returns>Return true on success login.</returns>
        private bool LoginAndSaveSession(string username, string password, out string errorDescription)
        {
            try
            {
                AdminClient oldClient = this.GetClient();
                if (oldClient != null)
                {
                    oldClient.Dispose();
                    this.Session.Remove(UsersController.AdminClientSessionKey);
                }
            }
            catch 
            {
            }

            AdminClient client = new AdminClient();
            try
            {
                client.IgnoreSslErrors = true;          // TODO : Not sure what to do about this.
                string hostName;
                int port;
                bool useSslConnection;

                Configuration configuration = this.Session[HomeController.ConfigurationSessionKey] as Configuration;
                if (configuration == null || !configuration.GetDeiHost(out hostName, out port, out useSslConnection))
                {
                    errorDescription = Resources.FailedToFindDei;
                    return false;
                }

                if (client.Login(hostName, port, username, password, useSslConnection))
                {
                    this.Session[UsersController.AdminClientSessionKey] = client;
                }
                else
                {
                    errorDescription = UsersController.GetErrorDescription(client);
                    return false;
                }

                this.Session[UsersController.PermissionListSessionKey] = client.ListPermissions(username);
                this.Session[UsersController.UsernameSessionKey] = username;

                errorDescription = string.Empty;
                return true;
            }
            catch
            {
                client.Close();
                this.Session.Remove(UsersController.PermissionListSessionKey);
                this.Session.Remove(UsersController.UsernameSessionKey);
                throw;
            }
        }

        /// <summary>
        /// Redirect to default will redirect the page to index page.
        /// </summary>
        /// <returns>Redirect to index page.</returns>
        private ActionResult RedirectToDefault()
        {
            return this.RedirectToAction("Index");
        }

        /// <summary>
        /// Convert list of permission properties to list of permission ids.
        /// </summary>
        /// <remarks>This is a helper method for checking the permission type.</remarks>
        /// <param name="permissions">List of permission properties.</param>
        /// <returns>Returns a list of permission ids.</returns>
        private List<long> ConvertUserPermissions(IList<PermissionProperties> permissions)
        {
            List<long> permissionIds = new List<long>(permissions.Count);

            foreach (PermissionProperties permission in permissions)
            {
                permissionIds.Add(permission.Id);
            }

            return permissionIds;
        }

        /// <summary>
        /// Get the permission list from database.
        /// </summary>
        /// <param name="client">Admin client.</param>
        /// <param name="session">Http Session.</param>
        /// <returns>List of permissions.</returns>
        private List<PermissionProperties> GetPermissionList(AdminClient client)
        {
            List<PermissionProperties> permissionList = (List<PermissionProperties>)this.Session[UsersController.AllPermissionListSessionKey];
            
            if (permissionList == null)
            {
                permissionList = new List<PermissionProperties>(client.ListAllPermissions());
                this.Session[UsersController.AllPermissionListSessionKey] = permissionList;
            }

            return permissionList;
        }
    }
}
