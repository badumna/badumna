//-----------------------------------------------------------------------
// <copyright file="ComponentController.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Web.Mvc;

using Germinator;
using Germinator.Components;
using Germinator.Services;

namespace BadumnaNetworkController.Controllers
{
    /// <summary>
    /// ComponentController class is used for displaying and modifying the properties for each individual 
    /// components and services that are available in ControlCenter. 
    /// </summary>
    public class ComponentController : Controller
    {
        /// <summary>
        /// Edit the component or service basic properties.
        /// </summary>
        /// <remarks>GET: /Component/Edit?index=service_name</remarks>
        /// <param name="index">Name of the component or service.</param>
        /// <returns>Return the details form contains component or service basic properties.</returns>
        public ActionResult Edit(string index)
        {
            // Checking for any existance of ModelState in temp data.
            if (this.TempData["ModelState"] != null && !this.ModelState.Equals(this.TempData["ModelState"]))
            {
                this.ModelState.Merge((ModelStateDictionary)this.TempData["ModelState"]);
            }

            Configuration configuration = this.Session[HomeController.ConfigurationSessionKey] as Configuration;
            if (configuration == null || String.IsNullOrEmpty(index))
            {
                return RedirectToAction("Index", "Home");
            }            

            ComponentView component = configuration.GetComponent(index);
            if (component == null)
            {
                return this.RedirectToAction("Index", "Home");
            }

            this.ViewData.Model = component;

            if ((component.Instance as BaseService) == null && configuration.RunningServices.Count > 0)
            {
                this.ViewBag.SettingChangeAllowed = false;
            }
            else
            {
                this.ViewBag.SettingChangeAllowed = true;
            }

            string hostname;
            int port;

            configuration.GetDeiHost(out hostname, out port);

            if (!string.IsNullOrEmpty(hostname))
            {
                this.ViewBag.DeiServerAddress = string.Format("{0}:{1}", hostname, port);
            }
            else
            {
                this.ViewBag.DeiServerAddress = null;
            }

            return View();
        }

        /// <summary>
        /// Post method from the edit component, update the component or service properties using 
        /// the submitted form received.
        /// </summary>
        /// <remarks>POST: /Component/Edit/</remarks>
        /// <param name="name">Name of the component or service.</param>
        /// <param name="isEnabled">This parameter is no longer used.</param>
        /// <param name="collection">The form collection obtained from the client.</param>
        /// <returns>Redirect to Edit action.</returns>
        /// <seealso cref="Edit(string)"/>
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Edit(string name, bool? isEnabled, FormCollection collection)
        {
            try
            {
                Configuration configuration = this.Session[HomeController.ConfigurationSessionKey] as Configuration;
                if (configuration == null || string.IsNullOrEmpty(name))
                {
                    return RedirectToAction("Index", "Home");
                }
               
                ComponentView componentView = configuration.GetComponent(name);
                if (componentView == null)
                {
                    return RedirectToAction("Index", "Home");
                }

                if (!isEnabled.HasValue)
                {
                    isEnabled = false;
                }

                for (int i = 0; i < collection.Count; i++)
                {
                    if (collection[i] == null)
                    {                        
                        this.ModelState.AddModelError(collection.GetKey(i), Resources.Invalid);
                    }

                    // extra checking for application name.
                    if (collection.GetKey(i) == "ApplicationName")
                    {
                        if (!InputValidationHelper.ValidateAlphanumeric(collection[i] as string))
                        {
                            this.ModelState.AddModelError(collection.GetKey(i), Resources.InvalidName);
                        }
                    }
                }

                if (!this.ModelState.IsValid)
                {
                    this.TempData["ModelState"] = this.ModelState;
                    return this.RedirectToAction("Edit", new { index = name });
                }

                for (int i = 0; i < collection.Count; i++)
                {
                    componentView.SetProperty(collection.GetKey(i), collection[i]);
                }

                if (componentView.Instance != null)
                {
                    configuration.EnableComponent(componentView, isEnabled.Value);
                }

                // TODO: check this out, to upload the setting from external file or something ??
                foreach (string propertyIndex in Request.Files)
                {
                    HttpPostedFileBase file = Request.Files[propertyIndex];
                    if (file.ContentLength > 0)
                    {
                        string directory = Path.GetFullPath(this.Server.MapPath(new string(Path.DirectorySeparatorChar, 1)));
                        string destinationPath = componentView.UploadFile(propertyIndex, directory, file.FileName);
                        if (!string.IsNullOrEmpty(destinationPath))
                        {
                            file.SaveAs(destinationPath);
                        }
                    }
                }

                configuration.Save();
                DebugConsole.Instance.EnqueueTraceInfo(string.Format("Successfully save setting for {0}", name));

                // NOTE: when there is a corupted application on the presto manifesto, the badumna package builder will
                // try to set the current directory to point to the application base directory and never set it back
                // Should do check here
                if (!Directory.GetCurrentDirectory().Equals(HomeController.BaseDirectory))
                {
                    Directory.SetCurrentDirectory(HomeController.BaseDirectory);
                }

                return this.RedirectToAction("Edit", new { index = name });
            }
            catch (Exception)
            {
                return RedirectToAction("Index", "Home");
            }
        }

        /// <summary>
        /// Get the list of the germ and display it under drop down list.
        /// </summary>
        /// <param name="name">Name of the service.</param>
        /// <returns>ListGermSimple partial view.</returns>
        public ActionResult GetGermList(string name)
        {
            Configuration configuration = this.Session[HomeController.ConfigurationSessionKey] as Configuration;

            if (configuration == null)
            {
                return Content("Failed to get the germ hosts list");
            }

            this.ViewData.Model = configuration.GermClients;
            this.ViewBag.name = name;

            // Extra check for dei server
            if (name.Equals("Dei"))
            {
                ComponentView componentView = configuration.GetComponent(name);
                if (componentView != null)
                {
                    object value;
                    componentView.GetProperty("ListenWithSsl", out value);
                    this.ViewBag.useSsl = (bool)value;
                }
            }

            return PartialView("DropDownGermsList", this.ViewData.Model);
        }

        /// <summary>
        /// Set the components back to the default value.
        /// </summary>
        /// <param name="name">>Name of the component or service.</param>
        /// <returns>Redirect to Edit action.</returns>
        public ActionResult Default(string name)
        {
            Configuration configuration = this.Session[HomeController.ConfigurationSessionKey] as Configuration;
            if (configuration != null)
            {
                configuration.SetToDefault(name);
                return RedirectToAction("Edit", new { index = name });
            }

            return Content(string.Format("Failed to set {0} back to default value", name));
        }
    }
}
