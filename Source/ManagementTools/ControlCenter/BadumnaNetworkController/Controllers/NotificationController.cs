//-----------------------------------------------------------------------
// <copyright file="NotificationController.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Web.Mvc;

using Germinator;
using Germinator.Notification;

namespace BadumnaNetworkController.Controllers
{
    /// <summary>
    /// Notification controller class has responsibility to handle all notifications from running processes, a process can push a notification
    /// if the maintenance task interval not equal to 0, the notification can also be sent via e-mail, if send notification to email is enabled.
    /// </summary>
    public class NotificationController : Controller
    {
        /// <summary>
        /// Return the notification index page containing all notificaitons from running processes.
        /// </summary>
        /// <returns>Return the index page.</returns>
        public ActionResult Index()
        {
            Configuration configuration = this.Session[HomeController.ConfigurationSessionKey] as Configuration;
            if (configuration == null)
            {
                return RedirectToAction("Index", "Home");
            }

            this.ViewBag.Count = configuration.GetNotifications().Count;
            this.ViewData.Model = configuration.Notifications;
            if (this.ViewData.Model == null)
            {
                return this.RedirectToAction("Index", "Home");
            }

            this.ViewBag.EmailNotificationEnabled = configuration.IsEmailNotificationEnabled();
            return View();
        }

        /// <summary>
        /// Get the number of current notifications.
        /// </summary>
        /// <returns>Return the number of current notifications.</returns>
        public ContentResult GetNotificationsCount()
        {
            Configuration configuration = this.Session[HomeController.ConfigurationSessionKey] as Configuration;
            if (configuration != null)
            {
                return Content(configuration.GetNotifications().Count.ToString());
            }

            return Content("0");
        }

        /// <summary>
        /// Configure the e-mail notification properties such as smtp client,smtp port, username, password and destination
        /// address.
        /// </summary>
        /// <returns>Return the SetupEmailNotification page.</returns>
        public ActionResult SetupEmailNotification()
        {
            Configuration configuration = this.Session[HomeController.ConfigurationSessionKey] as Configuration;
            if (configuration == null)
            {
                return RedirectToAction("Index", "Home");
            }

            this.ViewData.Model = configuration.GetEmailNotificationConfig();
            if (this.ViewData.Model == null)
            {
                return this.RedirectToAction("Index");
            }

            return View();
        }

        /// <summary>
        /// Post method of SetupEmailNotification, it collect the information from the client.
        /// </summary>
        /// <param name="collection">Form collection contains the email notification information from user.</param>
        /// <returns>Redirect to the notification index page.</returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SetupEmailNotification(FormCollection collection)
        {
            try
            {
                Configuration configuration = this.Session[HomeController.ConfigurationSessionKey] as Configuration;
                if (configuration == null)
                {
                    return RedirectToAction("Index", "Home");
                }

                EmailNotificationView emailNotificationConfig = configuration.GetEmailNotificationConfig();
                if (emailNotificationConfig == null)
                {
                    return RedirectToAction("Index");
                }

                for (int i = 0; i < collection.Count; i++)
                {
                    if (collection[i] == null)
                    {
                        this.ModelState.AddModelError(collection.GetKey(i), Resources.Invalid);
                    }
                }

                if (!this.ModelState.IsValid)
                {
                    return View();
                }

                for (int i = 0; i < collection.Count; i++)
                {
                    emailNotificationConfig.SetProperty(collection.GetKey(i), collection[i]);
                }

                configuration.Save();
                return this.RedirectToAction("Index");
            }
            catch (Exception)
            {
                return RedirectToAction("Index");
            }
        }

        /// <summary>
        /// Clear the notifications from the list.
        /// </summary>
        /// <returns>Redirect to notification index page.</returns>
        public ActionResult ClearNotification()
        {
            Configuration configuration = this.Session[HomeController.ConfigurationSessionKey] as Configuration;
            if (configuration == null)
            {
                return RedirectToAction("Index", "Home");
            }

            configuration.RemoveNotification();
            return this.RedirectToAction("Index");
        }

        /// <summary>
        /// Display the details of a particular notification given the notificaiton timestamp and process id.
        /// </summary>
        /// <param name="time">The notification time stamp.</param>
        /// <param name="processId">Process id of a process that generate a particular notification.</param>
        /// <returns>Return the Details.aspx page for a particular notification.</returns>
        public ActionResult Details(string time, int processId)
        {
            Configuration configuration = this.Session[HomeController.ConfigurationSessionKey] as Configuration;
            if (configuration == null)
            {
                return RedirectToAction("Index", "Home");
            }

            List<Notification> notifications = configuration.GetNotifications();
            foreach (Notification notification in notifications)
            {
                if (notification.ProcessId == processId && notification.Time == time)
                {
                    this.ViewData.Model = notification;
                    return View();
                }
            }

            return RedirectToAction("Index");
        }
        
        /// <summary>
        /// Delete a particular notification, <seealso cref=" ClearNotification()"/>.
        /// </summary>
        /// <param name="time">The notification time stamp.</param>
        /// <param name="processId">Process id of a process that generate a particular notification.</param>
        /// <returns>Redirect to notification index page.</returns>
        public ActionResult Delete(string time, int processId)
        {
            Configuration configuration = this.Session[HomeController.ConfigurationSessionKey] as Configuration;
            if (configuration == null)
            {
                return RedirectToAction("Index", "Home");
            }

            configuration.RemoveNotification(time, processId);
            return RedirectToAction("Index");
        }
    }
}
