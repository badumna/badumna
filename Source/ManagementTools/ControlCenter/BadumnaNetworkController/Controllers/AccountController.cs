﻿//---------------------------------------------------------------------------------
// <copyright file="AccountController.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

using System;
using System.Globalization;
using System.Security.Principal;
using System.Web.Mvc;
using System.Web.Security;
using Germinator;

namespace BadumnaNetworkController.Controllers
{
    /// <summary>
    /// The FormsAuthentication type is sealed and contains static members, so it is difficult to
    /// unit test code that calls its members. The interface and helper class below demonstrate
    /// how to create an abstract wrapper around such a type in order to make the AccountController
    /// code unit testable.
    /// </summary>
    public interface IFormsAuthentication
    {
        /// <summary>
        /// Sign in method which take two arguments, username and createPresistanceCookie.
        /// </summary>
        /// <param name="userName">Control Center username.</param>
        /// <param name="createPersistentCookie">Boolean persistance cookie.</param>
        void SignIn(string userName, bool createPersistentCookie);

        /// <summary>
        /// Sign out method.
        /// </summary>
        void SignOut();
    }

    /// <summary>
    /// IMembershipService interface.
    /// </summary>
    public interface IMembershipService
    {
        /// <summary>
        /// Gets the minimum password length.
        /// </summary>
        /// <value>Minimum password length.</value>
        int MinPasswordLength { get; }

        /// <summary>
        /// Validate user with a given username and password.
        /// </summary>
        /// <param name="userName">Control Center username.</param>
        /// <param name="password">Corresponding password.</param>
        /// <returns>Return true on success.</returns>
        bool ValidateUser(string userName, string password);

        /// <summary>
        /// Create a new user account for ControlCenter (it is used when there is a new user register it to the system).
        /// </summary>
        /// <param name="userName">Control Center username.</param>
        /// <param name="password">Corresponding password.</param>
        /// <param name="email">User e-mail.</param>
        /// <param name="securityQuestion">Security question.</param>
        /// <param name="securityAnswer">Answer for the securtiy question.</param>
        /// <returns>Return the MembershipCreateStatus instance.</returns>
        MembershipCreateStatus CreateUser(string userName, string password, string email, string securityQuestion, string securityAnswer);

        /// <summary>
        /// Change the user password, this method is called after the valdiation of new password.
        /// </summary>
        /// <param name="userName">Control Center username.</param>
        /// <param name="oldPassword">Old password.</param>
        /// <param name="newPassword">New password.</param>
        /// <returns>Return true on success.</returns>
        bool ChangePassword(string userName, string oldPassword, string newPassword);
    }

    /// <summary>
    /// AccountController is used to handle the login and logout from ControlCenter. It is not used on the release version 1.3.
    /// </summary>
    [HandleError]
    public class AccountController : Controller
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AccountController"/> class.
        /// This constructor is used by the MVC framework to instantiate the controller using
        /// the default forms authentication and membership providers.
        /// </summary>
        public AccountController()
            : this(null, null)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AccountController"/> class.
        /// This constructor is not used by the MVC framework but is instead provided for ease
        /// of unit testing this type. See the comments at the end of this file for more
        /// information.
        /// </summary>
        /// <param name="formsAuth">IForms authentication.</param>
        /// <param name="service">IMembership service.</param>
        public AccountController(IFormsAuthentication formsAuth, IMembershipService service)
        {
            this.FormsAuth = formsAuth ?? new FormsAuthenticationService();
            this.MembershipService = service ?? new AccountMembershipService();
        }

        /// <summary>
        /// Gets formsAuth public property.
        /// </summary>
        /// <value>Forms auth.</value>
        public IFormsAuthentication FormsAuth
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets membershipService public property.
        /// </summary>
        /// <value>Membership service.</value>
        public IMembershipService MembershipService
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets Log On page of Control Center.
        /// </summary>
        /// <returns>Return the logon page.</returns>
        public ActionResult LogOn()
        {
            return View();
        }

        /// <summary>
        /// Post method of Log on page of the ControlCenter, this is used for the security reason as the ControlCenter will run remotely and 
        /// mutliple admins can access it from other machine.
        /// </summary>
        /// <param name="userName">Admin usernames.</param>
        /// <param name="password">Corresponding password.</param>
        /// <param name="rememberMe">Remember the username and password option.</param>
        /// <param name="returnUrl">Redirect to this given url.</param>
        /// <returns>Redirect to given url on success login.</returns>
        [AcceptVerbs(HttpVerbs.Post)]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1054:UriParametersShouldNotBeStrings",
            Justification = "Needs to take same parameter type as Controller.Redirect()")]
        public ActionResult LogOn(string userName, string password, bool rememberMe, string returnUrl)
        {
            if (!this.ValidateLogOn(userName, password))
            {
                return View();
            }

            this.FormsAuth.SignIn(userName, rememberMe);
            if (!String.IsNullOrEmpty(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        /// <summary>
        /// LogOff is the opposite of <see cref="LogOn(string userName, string password, bool rememberMe, string returnUrl"/>.
        /// </summary>
        /// <returns>Redirect to home index page.</returns>
        public ActionResult LogOff()
        {
            this.FormsAuth.SignOut();
            return RedirectToAction("Index", "Home");
        }

        /// <summary>
        /// Registering a new account to MembershipService. 
        /// </summary>
        /// <returns>Return the register page.</returns>
        public ActionResult Register()
        {
            this.ViewBag.PasswordLength = this.MembershipService.MinPasswordLength;
            return View();
        }

        /// <summary>
        /// The post method of <see cref="Register()"/>, collect the information from the new user including username, email,
        /// security qeuestion and answer.
        /// </summary>
        /// <param name="userName">ControlCenter username.</param>
        /// <param name="email">User email.</param>
        /// <param name="password">New password.</param>
        /// <param name="confirmPassword">Password confirmation.</param>
        /// <param name="securityQuestion">Security question.</param>
        /// <param name="securityAnswer">Answer for the corresponding security quesiton.</param>
        /// <returns>Redirect to home index page.</returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Register(string userName, string email, string password, string confirmPassword, string securityQuestion, string securityAnswer)
        {
            this.ViewBag.PasswordLength = this.MembershipService.MinPasswordLength;

            if (this.ValidateRegistration(userName, email, password, confirmPassword))
            {
                // Attempt to register the user
                MembershipCreateStatus createStatus = this.MembershipService.CreateUser(userName, password, email, securityQuestion, securityAnswer);

                if (createStatus == MembershipCreateStatus.Success)
                {
                    // this.FormsAuth.SignIn(userName, false /* createPersistentCookie */);
                    AdministrationToolController.AllRegisterUsers = Membership.GetAllUsers();
                    DebugConsole.Instance.EnqueueTraceInfo(string.Format("Successfully created user account: {0}", userName));
                    
                    // Note: if the username is admin, then automatically signed this user with Admin role.
                    // Special case, redirect to index home
                    if (userName.Equals("admin"))
                    {
                        Roles.AddUserToRole(userName, RoleNames.Admin);
                        return RedirectToAction("Index", "Home");
                    }

                    return RedirectToAction("Index", "AdministrationTool");
                }
                else
                {
                    ModelState.AddModelError("_FORM", ErrorCodeToString(createStatus));
                }
            }

            // If we got this far, something failed, redisplay form
            return View();
        }

        /// <summary>
        /// Change ControlCenter password, it only available when is logged in.
        /// </summary>
        /// <returns>Returnt the change password page.</returns>
        [Authorize]
        public ActionResult ChangePassword()
        {
            this.ViewBag.PasswordLength = this.MembershipService.MinPasswordLength;
            return View();
        }

        /// <summary>
        /// Post method of ChangePassword method, collect the old and new password from the user, and change the password
        /// with the new password if the password validation return successfully.
        /// </summary>
        /// <param name="currentPassword">Old/current password.</param>
        /// <param name="newPassword">New password.</param>
        /// <param name="confirmPassword">New password confirmation.</param>
        /// <returns>Redirect to ChangePasswordSuccess page.</returns>
        [Authorize]
        [AcceptVerbs(HttpVerbs.Post)]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes",
            Justification = "Exceptions result in password not being changed.")]
        public ActionResult ChangePassword(string currentPassword, string newPassword, string confirmPassword)
        {
            this.ViewBag.PasswordLength = this.MembershipService.MinPasswordLength;

            if (!this.ValidateChangePassword(currentPassword, newPassword, confirmPassword))
            {
                return View();
            }

            try
            {
                if (this.MembershipService.ChangePassword(User.Identity.Name, currentPassword, newPassword))
                {
                    return RedirectToAction("ChangePasswordSuccess");
                }
                else
                {
                    ModelState.AddModelError("_FORM", "The current password is incorrect or the new password is invalid.");
                    return View();
                }
            }
            catch
            {
                ModelState.AddModelError("_FORM", "The current password is incorrect or the new password is invalid.");
                return View();
            }
        }

        /// <summary>
        /// Change password success page.
        /// </summary>
        /// <returns>Return the ChangePasswordSuccess.aspx page.</returns>
        public ActionResult ChangePasswordSuccess()
        {
            return View();
        }

        /// <summary>
        /// On action executing.
        /// </summary>
        /// <param name="filterContext">Action executing context.</param>
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (filterContext.HttpContext.User.Identity is WindowsIdentity)
            {
                throw new InvalidOperationException("Windows authentication is not supported.");
            }
        }

        /// <summary>
        /// Create an error message based on the MembershipCreateStatus given. 
        /// </summary>
        /// <param name="createStatus">MembershipCreateStatus instance.</param>
        /// <returns>Return the error message.</returns>
        private static string ErrorCodeToString(MembershipCreateStatus createStatus)
        {
            // See http://msdn.microsoft.com/en-us/library/system.web.security.membershipcreatestatus.aspx for
            // a full list of status codes.
            switch (createStatus)
            {
                case MembershipCreateStatus.DuplicateUserName:
                    return "Username already exists. Please enter a different user name.";

                case MembershipCreateStatus.DuplicateEmail:
                    return "A username for that e-mail address already exists. Please enter a different e-mail address.";

                case MembershipCreateStatus.InvalidPassword:
                    return "The password provided is invalid. Please enter a valid password value.";

                case MembershipCreateStatus.InvalidEmail:
                    return "The e-mail address provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidAnswer:
                    return "The password retrieval answer provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidQuestion:
                    return "The password retrieval question provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidUserName:
                    return "The user name provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.ProviderError:
                    return "The authentication provider returned an error. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                case MembershipCreateStatus.UserRejected:
                    return "The user creation request has been canceled. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                default:
                    return "An unknown error occurred. Please verify your entry and try again. If the problem persists, please contact your system administrator.";
            }
        }

        /// <summary>
        /// Change password validation, check the validity of current and new password.
        /// </summary>
        /// <param name="currentPassword">Old/current password.</param>
        /// <param name="newPassword">New password.</param>
        /// <param name="confirmPassword">New password confirmation.</param>
        /// <returns>Return true if successfully validate current and new password.</returns>
        private bool ValidateChangePassword(string currentPassword, string newPassword, string confirmPassword)
        {
            if (String.IsNullOrEmpty(currentPassword))
            {
                ModelState.AddModelError("currentPassword", "You must specify a current password.");
            }

            if (newPassword == null || newPassword.Length < this.MembershipService.MinPasswordLength)
            {
                ModelState.AddModelError(
                    "newPassword",
                    String.Format(CultureInfo.CurrentCulture, "You must specify a new password of {0} or more characters.", this.MembershipService.MinPasswordLength));
            }

            if (!String.Equals(newPassword, confirmPassword, StringComparison.Ordinal))
            {
                ModelState.AddModelError("_FORM", "The new password and confirmation password do not match.");
            }

            return ModelState.IsValid;
        }

        /// <summary>
        /// Validate the log on information, make sure username and password are given, also both the data are 
        /// valid.
        /// </summary>
        /// <param name="userName">Control Center username.</param>
        /// <param name="password">Corresponding password.</param>
        /// <returns>Return true if success.</returns>
        private bool ValidateLogOn(string userName, string password)
        {
            if (String.IsNullOrEmpty(userName))
            {
                ModelState.AddModelError("username", "You must specify a username.");
            }

            if (String.IsNullOrEmpty(password))
            {
                ModelState.AddModelError("password", "You must specify a password.");
            }

            if (!this.MembershipService.ValidateUser(userName, password))
            {
                ModelState.AddModelError("_FORM", "The username or password provided is incorrect.");
            }

            return ModelState.IsValid;
        }

        /// <summary>
        /// Validate Registration information, make sure all the informations are complete (i.e. username, email, password).
        /// </summary>
        /// <param name="userName">Control Center username.</param>
        /// <param name="email">User e-mail.</param>
        /// <param name="password">Corresponding password.</param>
        /// <param name="confirmPassword">Password confirmation.</param>
        /// <returns>Return true if success.</returns>
        private bool ValidateRegistration(string userName, string email, string password, string confirmPassword)
        {
            if (String.IsNullOrEmpty(userName))
            {
                ModelState.AddModelError("username", "You must specify a username.");
            }

            if (!InputValidationHelper.ValidateAlphanumeric(userName))
            {
                ModelState.AddModelError("username", Resources.InvalidName);
            }

            if (String.IsNullOrEmpty(email))
            {
                ModelState.AddModelError("email", "You must specify an email address.");
            }

            if (!InputValidationHelper.ValidateEmail(email))
            {
                ModelState.AddModelError("email", Resources.InvalidEmail);
            }

            if (password == null || password.Length < this.MembershipService.MinPasswordLength)
            {
                ModelState.AddModelError(
                    "password",
                    String.Format(CultureInfo.CurrentCulture, "You must specify a password of {0} or more characters.", this.MembershipService.MinPasswordLength));
            }

            if (!String.Equals(password, confirmPassword, StringComparison.Ordinal))
            {
                ModelState.AddModelError("_FORM", "The new password and confirmation password do not match.");
            }

            return ModelState.IsValid;
        }
    }

    /// <summary>
    /// FormAuthenticationService class implement from IFormAuthentication interface.
    /// </summary>
    public class FormsAuthenticationService : IFormsAuthentication
    {
        /// <summary>
        /// Implementation of SignIn method. 
        /// </summary>
        /// <param name="userName">ControlCenter username.</param>
        /// <param name="createPersistentCookie">Boolean persistance cookie.</param>
        public void SignIn(string userName, bool createPersistentCookie)
        {
            FormsAuthentication.SetAuthCookie(userName, createPersistentCookie);
        }

        /// <summary>
        /// Implementation of SignOut method.
        /// </summary>
        public void SignOut()
        {
            FormsAuthentication.SignOut();
        }
    }

    /// <summary>
    /// AccountMembershipService class implement the IMembershipService interface.
    /// </summary>
    public class AccountMembershipService : IMembershipService
    {
        /// <summary>
        /// Provider (private field). 
        /// </summary>
        private MembershipProvider provider;

        /// <summary>
        /// Initializes a new instance of the <see cref="AccountMembershipService"/> class.
        /// </summary>
        public AccountMembershipService()
            : this(null)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AccountMembershipService"/> class.
        /// </summary>
        /// <param name="provider">Membership provider (<see cref="System.Web.Security.MembershipProvider"/>).</param>
        public AccountMembershipService(MembershipProvider provider)
        {
            this.provider = provider ?? Membership.Provider;
        }

        /// <summary>
        /// Gets the minimum password length.
        /// </summary>
        /// <value>Minimum password length.</value>
        public int MinPasswordLength
        {
            get
            {
                return this.provider.MinRequiredPasswordLength;
            }
        }

        /// <summary>
        /// Implementation of ValidateUser method.
        /// </summary>
        /// <param name="userName">Control Center username.</param>
        /// <param name="password">Corresponding password.</param>
        /// <returns>Return true on success.</returns>
        public bool ValidateUser(string userName, string password)
        {
            return this.provider.ValidateUser(userName, password);
        }

        /// <summary>
        /// Implementation of CreateUser method.
        /// </summary>
        /// <param name="userName">Control Center username.</param>
        /// <param name="password">Corresponding password.</param>
        /// <param name="email">User e-mail.</param>
        /// <param name="securityQuestion">Security question.</param>
        /// <param name="securityAnswer">Answer for the securtiy question.</param>
        /// <returns>Return the MembershipCreateStatus instance.</returns>
        public MembershipCreateStatus CreateUser(string userName, string password, string email, string securityQuestion, string securityAnswer)
        {
            MembershipCreateStatus status;
            this.provider.CreateUser(userName, password, email, securityQuestion, securityAnswer, true, null, out status);
            return status;
        }

        /// <summary>
        /// Implementation of ChangePassword method.
        /// </summary>
        /// <param name="userName">Control Center username.</param>
        /// <param name="oldPassword">Old password.</param>
        /// <param name="newPassword">New password.</param>
        /// <returns>Return true on success.</returns>
        public bool ChangePassword(string userName, string oldPassword, string newPassword)
        {
            MembershipUser currentUser = this.provider.GetUser(userName, true /* userIsOnline */);
            return currentUser.ChangePassword(oldPassword, newPassword);
        }
    }
}
