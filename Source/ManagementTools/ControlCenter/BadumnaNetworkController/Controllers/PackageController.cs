//-----------------------------------------------------------------------
// <copyright file="PackageController.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System.IO;
using System.Web;
using System.Web.Mvc;

using Badumna.Package;

using Germinator;
using System.Collections.Generic;

namespace BadumnaNetworkController.Controllers
{
    /// <summary>
    /// PackageController class display the service package details information, and also used to update 
    /// the package for the default services if neccessary.
    /// </summary>
    public class PackageController : Controller
    {       
        /// <summary>
        /// Display the details information about a particular package for a particular service.
        /// </summary>
        /// <remarks> GET: /Package/Details/ </remarks>
        /// <param name="name">The name of the package.</param>
        /// <param name="displayName">The display name of the service (i.e. service name may be different with display name).</param>
        /// <returns>The package details information.</returns>
        public ActionResult Details(string name, string displayName)
        {
            if (string.IsNullOrEmpty(name))
            {
                return RedirectToAction("Index", "Network");
            }

            this.ViewBag.index = name.Contains("Dei") ? "Dei" : name;
            this.ViewBag.DisplayName = displayName;
            ViewData.Model = GerminatorFacade.Instance.GetPackage(name);
            return View();
        }

        /// <summary>
        /// Post method for details is used to update the current package with the newer package, upload
        /// two files (manifest.xml and the new package itself), this function will be available only for 
        /// default services package (i.e. PublicPeer, Dei, HttpTunneling and Overload peer).
        /// </summary>
        /// <param name="displayName">The display name of the service.</param>
        /// <returns>Redirect to package details information.</returns>
        /// <seealso cref="Details(string,string)"/>
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Details(string displayName, IEnumerable<HttpPostedFileBase> files)
        {
            // TODO : Check the current package version has to be newer than the previous file
            string appPath = Request.PhysicalApplicationPath;
            string saveDir = @"\Services\";
            string packageName = string.Empty;

            foreach (HttpPostedFileBase file in files)
            {
                if (file.ContentLength > 0)
                {
                    // Note: Manifest.xml has to be uploaded first
                    if (file.FileName.Equals(Manifest.DefaultFileName))
                    {
                        Manifest manifest = Manifest.Load(file.InputStream);
                        packageName = manifest.ApplicationName;
                        saveDir += packageName + @"\";
                        if (!Directory.Exists(appPath + saveDir))
                        {
                            DebugConsole.Instance.EnqueueTraceInfo(string.Format("Failed to update {0} package, the service doesn't exist", displayName));
                            return Content("Failed to update the package, the service doesn't exist");
                        }
                    }

                    string fileName = file.FileName;
                    string savePath = appPath + saveDir + fileName;
                    file.SaveAs(savePath);
                }
            }

            DebugConsole.Instance.EnqueueTraceInfo(string.Format("Update {0} package successfully", displayName));
            return RedirectToAction("Details", new { name = packageName, displayName = displayName });
        }
    }
}
