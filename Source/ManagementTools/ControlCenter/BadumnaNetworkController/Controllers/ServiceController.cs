//-----------------------------------------------------------------------
// <copyright file="ServiceController.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

using Badumna.Package;

using Germinator;
using Germinator.Components;
using Germinator.Services;

namespace BadumnaNetworkController.Controllers
{
    /// <summary>
    /// ServiceController class shouldn't be used anymore. It should be handle it in NetworkController class.
    /// </summary>
    public class ServiceController : Controller
    {
        /// <summary>
        /// Display the status of the specific process.
        /// </summary>
        /// <param name="serviceName">Service name.</param>
        /// <param name="host">Germ host name, including the port number.</param>
        /// <param name="customName">Service custom name.</param>
        /// <returns>Return the status.aspx page.</returns>
        public ActionResult Status(string serviceName, string host, string customName)
        {
            Configuration configuration = this.Session[HomeController.ConfigurationSessionKey] as Configuration;
            BaseService service = null;
            if (configuration != null)
            {
                this.ViewBag.ApplicationName = configuration.Name;
                service = configuration.GetService(serviceName);
            }

            if (service != null)
            {
                this.ViewData.Model = service.GetStatus(host);

                foreach (RemoteServiceProcess process in service.Instances)
                {
                    if (process.Germ.Name.Equals(host))
                    {
                        this.ViewBag.ProcessInfo = process;
                    }
                }

                if (service as PeerService != null)
                {
                    this.ViewBag.Process = this.ViewBag.ProcessInfo;                    
                }

                this.ViewBag.Service = serviceName;
                this.ViewBag.Host = host;
                this.ViewBag.CustomName = customName;
                return PartialView("Status");
            }

            return Content("Failed");
        }

        /// <summary>
        /// Remove a particular service from the control center.
        /// (Note: this can be done using an ajax request, and just return a partial view).
        /// </summary>
        /// <param name="serviceName">Service name need to be removed.</param>
        /// <returns>Redirect to network index page.</returns>
        public ActionResult Remove(string serviceName)
        {
            Configuration configuration = this.Session[HomeController.ConfigurationSessionKey] as Configuration;
            BaseService service = null;

            if (configuration != null)
            {
                service = configuration.GetService(serviceName);
            }

            if (service != null)
            {
                // Remove the package from list
                PackageManager.Instance.RemovePacakage(serviceName);

                // Remove component from ComponentContainer
                ComponentContainer.Instance.Remove(service.Index);

                // Remove component from the current configuration
                configuration.RemoveComponent(service.Index);
            }

            return RedirectToAction("Index", "Network");
        }
    }
}
