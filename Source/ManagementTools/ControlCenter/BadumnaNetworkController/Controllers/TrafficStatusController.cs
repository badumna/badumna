//-----------------------------------------------------------------------
// <copyright file="TrafficStatusController.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Xml;

using Germinator;
using Germinator.Services;

namespace BadumnaNetworkController.Controllers
{
    /// <summary>
    /// TrafficStatusController is used to display the traffice status for a specific running process, specifically the total bytes
    /// sent and received per second. (TODO: autmatically requesting data to all running processes every certain amount of time).
    /// </summary>
    public class TrafficStatusController : Controller
    {
        /// <summary>
        /// Collection of traffic data for each process.
        /// </summary>
        private static Dictionary<string, List<string>> trafficData = new Dictionary<string, List<string>>();

        /// <summary>
        /// Data type enumeration.
        /// </summary>
        private enum DataType
        {
            /// <summary>
            /// Total bytes sent per second from a given peer.
            /// </summary>
            TotalBytesSentPerSecond,

            /// <summary>
            /// Total bytes received per second by a given peer.
            /// </summary>
            TotalBytesReceivedPerSecond,

            /// <summary>
            /// The total number of clients currently connected to overlod server.
            /// </summary>
            NumberOfOverloadClients,

            /// <summary>
            /// The total number of receiving peers currently sending update to overload server.
            /// </summary>
            NumberOfReceivingPeers,

            /// <summary>
            /// Total numbers of Incoming updates received from the connected client.
            /// </summary>
            IncomingUpdates,

            /// <summary>
            /// Total numbers of Outgoing updates sent by overload server to replicas.
            /// </summary>
            OutgoingUpdates,

            /// <summary>
            /// The total number of clients currently connected to arbitration server.
            /// </summary>
            NumberOfArbitratorClients,

            /// <summary>
            /// Total number of requests received by arbitration server.
            /// </summary>
            NumberOfReceivedRequests,

            /// <summary>
            /// Total number of request handled by arbitration server.
            /// </summary>
            NumberOfHandledRequests,

            /// <summary>
            /// Total number of messages sent by arbitration server.
            /// </summary>
            NumberOfSentMessages
        }

        /// <summary>
        /// Service type enumeration.
        /// </summary>
        private enum ServiceType
        {
            /// <summary>
            /// Normal Badumna peer service.
            /// </summary>
            Normal,

            /// <summary>
            /// Seed peer service.
            /// </summary>
            Seed,

            /// <summary>
            /// Overload server service.
            /// </summary>
            Overload,

            /// <summary>
            /// Arbitration server service.
            /// </summary>
            Arbitration
        }        

        /// <summary>
        /// Fetch a process traffic from the germ.  
        /// </summary>
        /// <param name="serviceName">Service name.</param>
        /// <param name="host">Germ host name, including the port number.</param>
        /// <returns>Write the response to the user and shoudn't return anything.</returns>
        public ActionResult FetchData(string serviceName, string host)
        {
            Configuration configuration = this.Session[HomeController.ConfigurationSessionKey] as Configuration;

            BaseService service = null;

            if (configuration != null)
            {
                service = configuration.GetService(serviceName);
            }

            if (service != null)
            {
                ServiceType serviceType = this.GetServiceType(service);
                string status = service.GetStatus(host);

                string dataSent = this.GetData(status, serviceName, host, DataType.TotalBytesSentPerSecond);
                string dataReceived = this.GetData(status, serviceName, host, DataType.TotalBytesReceivedPerSecond);

                switch (serviceType)
                {
                    case ServiceType.Overload:
                        string numberOfClientsConnected = this.GetData(status, serviceName, host, DataType.NumberOfOverloadClients);
                        string numberOfReceivingPeers = this.GetData(status, serviceName, host, DataType.NumberOfReceivingPeers);
                        string incomingUpdates = this.GetData(status, serviceName, host, DataType.IncomingUpdates);
                        string outgoingUpdates = this.GetData(status, serviceName, host, DataType.OutgoingUpdates);
                        
                        return Json(
                            JsonHelper.DataPlotFormatter(
                                new string[] { dataSent.TrimEnd(','), dataReceived.TrimEnd(',') },
                                new string[] { serviceName + " sent (kB/s)", serviceName + " received (kB/s)" },
                                new string[] { numberOfClientsConnected.TrimEnd(','), numberOfReceivingPeers.TrimEnd(','), incomingUpdates.TrimEnd(','), outgoingUpdates.TrimEnd(',') },
                                new string[] { "Number of clients", "Number of receiving peers", "Total incoming updates", "Total outgoing updates" },
                                string.Format("\"type\": \"{0}\"", serviceType.ToString())));
                    case ServiceType.Arbitration:
                        numberOfClientsConnected = this.GetData(status, serviceName, host, DataType.NumberOfArbitratorClients);
                        string numberOfReceivedRequests = this.GetData(status, serviceName, host, DataType.NumberOfReceivedRequests);
                        string numberOfHandledRequest = this.GetData(status, serviceName, host, DataType.NumberOfHandledRequests);
                        string numberOfSentMessages = this.GetData(status, serviceName, host, DataType.NumberOfSentMessages);

                        return Json(
                            JsonHelper.DataPlotFormatter(
                                new string[] { dataSent.TrimEnd(','), dataReceived.TrimEnd(',') },
                                new string[] { serviceName + " sent (kB/s)", serviceName + " received (kB/s)" },
                                new string[] { numberOfClientsConnected.TrimEnd(','), numberOfReceivedRequests.TrimEnd(','), numberOfHandledRequest.TrimEnd(','), numberOfSentMessages.TrimEnd(',') },
                                new string[] { "Number of clients", "Number of received requests", "Number of handled requests", "Number of sent messages" },
                                string.Format("\"type\": \"{0}\"", serviceType.ToString())));
                    default:
                        return Json(
                            JsonHelper.DataPlotFormatter(
                                new string[] { dataSent.TrimEnd(','), dataReceived.TrimEnd(',') },
                                new string[] { serviceName + " sent (kB/s)", serviceName + " received (kB/s)" },
                                new string[] { null, null },
                                string.Format("\"type\": \"{0}\"", serviceType.ToString())));
                }
            }

            return null;
        }

        /// <summary>
        /// Extracting the given data type value from the network status in xml format.
        /// </summary>
        /// <param name="networkStatus">Network status string in xml format.</param>
        /// <param name="dataType">Data type need to be extracted.</param>
        /// <returns>Returns the data point representing the value in JSON format.</returns>
        private string ExtractValueFromStatus(string networkStatus, DataType dataType)
        {
            try
            {
                string dataPoint = string.Empty;
                XmlDocument networkStatusXml = new XmlDocument();

                if (!string.IsNullOrEmpty(networkStatus))
                {
                    networkStatusXml.LoadXml(networkStatus);

                    long time = this.GetJavascriptTimestamp(DateTime.Now);
                    string value = string.Empty;

                    switch (dataType)
                    {
                        case DataType.TotalBytesReceivedPerSecond:
                            value = networkStatusXml.SelectSingleNode("/Status/TransferRate/TotalBytesReceivedPerSecond").InnerText;
                            
                            // convert to kilobyte
                            value = (float.Parse(value) / 1024.0f).ToString();
                            break;
                        case DataType.TotalBytesSentPerSecond:
                            value = networkStatusXml.SelectSingleNode("/Status/TransferRate/TotalBytesSentPerSecond").InnerText;
                            
                            // convert to kilobyte
                            value = (float.Parse(value) / 1024.0f).ToString();
                            break;
                        case DataType.NumberOfOverloadClients:
                            value = networkStatusXml.SelectSingleNode("/Status/Overload/NumberOfClients").InnerText;
                            break;
                        case DataType.NumberOfReceivingPeers:
                            value = networkStatusXml.SelectSingleNode("/Status/Overload/NumberOfReceivingPeers").InnerText;
                            break;
                        case DataType.IncomingUpdates:
                            value = networkStatusXml.SelectSingleNode("/Status/Overload/IncomingUpdates").InnerText;
                            break;
                        case DataType.OutgoingUpdates:
                            value = networkStatusXml.SelectSingleNode("/Status/Overload/OutgoingUpdates").InnerText;
                            break;
                        case DataType.NumberOfArbitratorClients:
                            value = networkStatusXml.SelectSingleNode("/Status/Arbitration/NumberOfClients").InnerText;
                            break;
                        case DataType.NumberOfReceivedRequests:
                            value = networkStatusXml.SelectSingleNode("/Status/Arbitration/NumberOfReceivedRequests").InnerText;
                            break;
                        case DataType.NumberOfHandledRequests:
                            value = networkStatusXml.SelectSingleNode("/Status/Arbitration/NumberOfHandledRequests").InnerText;
                            break;
                        case DataType.NumberOfSentMessages:
                            value = networkStatusXml.SelectSingleNode("/Status/Arbitration/NumberOfSentMessaages").InnerText;
                            break;
                    }

                    dataPoint = string.Format("[{0},{1}]", time, value);
                }

                return dataPoint;
            }
            catch (XmlException e)
            {
                throw new InvalidOperationException(e.Message);
            }
        }

        /// <summary>
        /// Get JavaScriptTimeStamp from a given DateTime object.
        /// </summary>
        /// <param name="input">Date time object.</param>
        /// <returns>Return long ticks.</returns>
        private long GetJavascriptTimestamp(DateTime input)
        {
            TimeSpan span = new TimeSpan(DateTime.Parse("1/1/1970").Ticks);
            DateTime time = input.Subtract(span);
            return (long)(time.Ticks / 10000);
        }

        /// <summary>
        /// Get and added the new data from/into TrafficData dictionary, we should keep track the latest 20 data from each
        /// process.
        /// </summary>
        /// <param name="status">Status in xml format.</param>
        /// <param name="serviceName">Service name.</param>
        /// <param name="host">Germ host name, including the port number.</param>
        /// <param name="type">Type of data required.</param>
        /// <returns>Return the requested data.</returns>
        private string GetData(string status, string serviceName, string host, DataType type)
        {
            string data = string.Empty;
            List<string> dataHistory;

            if (!trafficData.TryGetValue(serviceName + host + type.ToString(), out dataHistory))
            {
                dataHistory = new List<string>();
                trafficData.Add(serviceName + host + type.ToString(), dataHistory);
            }

            dataHistory.Add(this.ExtractValueFromStatus(status, type));

            if (dataHistory.Count >= 20)
            {
                dataHistory.RemoveAt(0);
            }

            foreach (string point in dataHistory)
            {
                data += point + ",";
            }

            trafficData[serviceName + host + type.ToString()] = dataHistory;
            return data;
        }

        /// <summary>
        /// Get the type of the given service.
        /// </summary>
        /// <param name="service">A given service.</param>
        /// <returns>Returns the service type.</returns>
        private ServiceType GetServiceType(BaseService service)
        {
            if (service is ArbitrationService)
            {
                return ServiceType.Arbitration;
            }
            else if (service is OverloadPeerService)
            {
                return ServiceType.Overload;
            }
            else if (service is PublicPeerService)
            {
                return ServiceType.Seed;
            }
            else
            {
                return ServiceType.Normal;
            }
        }
    }
}
