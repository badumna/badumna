//-----------------------------------------------------------------------
// <copyright file="AdministrationToolController.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using System.Web.Security;
using Germinator;

namespace BadumnaNetworkController.Controllers
{
    /// <summary>
    /// Administration Tool Controller class is a website administration tools that would be used
    /// in Control Center, so the user will easily manage their account.
    /// </summary>
    public class AdministrationToolController : Controller
    {
        /// <summary>
        /// MembershipUser collections of all registered users.
        /// </summary>
        private static MembershipUserCollection allRegisterUsers;

        /// <summary>
        /// List of all available roles.
        /// </summary>
        private static string[] allRoles;

        /// <summary>
        /// Gets or sets the all register users list.
        /// TODO: shouldn't be set from outside class
        /// </summary>
        public static MembershipUserCollection AllRegisterUsers
        {
            get 
            {
                if (allRegisterUsers == null)
                {
                    allRegisterUsers = Membership.GetAllUsers();
                }

                return allRegisterUsers; 
            }

            set 
            { 
                allRegisterUsers = value; 
            }
        }

        /// <summary>
        /// Gets or sets list of all available roles.
        /// <remarks>
        /// This get and set property is required for unit test.
        /// </remarks>
        /// </summary>
        private static string[] AllRoles
        {
            get
            {
                if (allRoles == null)
                {
                    allRoles = Roles.GetAllRoles();
                }

                return allRoles;
            }

            set
            {
                allRoles = value;
            }
        }

        /// <summary>
        /// Administration Index page.
        /// </summary>
        /// <returns>Returh the administration tool index page.</returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Manage the user account.
        /// </summary>
        /// <returns>Return the manage user page.</returns>
        public ActionResult ManageUser()
        {
            ViewBag.TotalUser = AllRegisterUsers.Count;            
            ViewData.Model = AllRegisterUsers;
            return View();
        }

        /// <summary>
        /// Edit a specific user's role.
        /// </summary>
        /// <param name="userName">The account username.</param>
        /// <returns>The edit role page.</returns>
        public ActionResult EditRole(string userName)
        {
            ViewData.Model = AllRoles;
            ViewBag.CurrentRoles = Roles.GetRolesForUser(userName);
            ViewBag.Username = userName;
            return View();
        }

        /// <summary>
        /// Delete a selected user from the account database (Control Center database).
        /// </summary>
        /// <param name="userName">The account username.</param>
        /// <returns>Update the ListOfUsers partial view.</returns>
        public ActionResult DeleteUser(string userName)
        {
            if (!string.IsNullOrEmpty(userName))
            {
                DebugConsole.Instance.EnqueueTraceInfo(string.Format("Delete {0} account from database", userName));
                Membership.DeleteUser(userName);
                DebugConsole.Instance.EnqueueTraceInfo(string.Format("Successfully deleted {0} user account from database", userName));
                AllRegisterUsers = Membership.GetAllUsers();

                return PartialView("ListOfUsers", AllRegisterUsers);
            }

            return Content("Failed");
        }

        /// <summary>
        /// Create or delete roles from the account database (Control Center database)
        /// </summary>
        /// <returns>Returns the manage role page.</returns>
        public ActionResult ManageRole()
        {
            ViewData.Model = AllRoles;
            return View();
        }

        /// <summary>
        /// Add a new role into account database.
        /// </summary>
        /// <param name="newRole">New role name.</param>
        /// <returns>Returns the manage role page.</returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManageRole(string newRole)
        {
            if (!InputValidationHelper.ValidateAlphanumeric(newRole))
            {
                this.ModelState.AddModelError("newRole", Resources.InvalidName);
            }

            if (this.ModelState.IsValid && !string.IsNullOrEmpty(newRole) && !Roles.RoleExists(newRole))
            {
                DebugConsole.Instance.EnqueueTraceInfo(string.Format("Create a new role '{0}'", newRole));
                Roles.CreateRole(newRole);
                DebugConsole.Instance.EnqueueTraceInfo(string.Format("Successfully created '{0}'", newRole));
                AllRoles = Roles.GetAllRoles();
            }

            ViewData.Model = AllRoles;
            return View();
        }

        /// <summary>
        /// Delete a role from database.
        /// </summary>
        /// <param name="role">The role name.</param>
        /// <returns>Returns to manage role page.</returns>
        public ActionResult DeleteRole(string role)
        {
            if (!string.IsNullOrEmpty(role))
            {
                string[] users = Roles.GetUsersInRole(role);
                foreach (string user in users)
                {
                    DebugConsole.Instance.EnqueueTraceInfo(string.Format("Remove the {0} from {1} first", user, role));
                    Roles.RemoveUserFromRole(user, role);
                }

                DebugConsole.Instance.EnqueueTraceInfo(string.Format("Delete role '{0}'", role));
                Roles.DeleteRole(role);
                DebugConsole.Instance.EnqueueTraceInfo(string.Format("Successfully deleted '{0}'", role));
                AllRoles = Roles.GetAllRoles();
            }

            return PartialView("ListOfRoles", AllRoles);
        }

        /// <summary>
        /// Manage a particular role, list all of the user in the specific role.
        /// </summary>
        /// <param name="role">The role name.</param>
        /// <returns>Returns the manage single role page.</returns>
        public ActionResult ManageSingleRole(string role)
        {
            if (!string.IsNullOrEmpty(role))
            {
                ViewBag.UsersInRole = Roles.GetUsersInRole(role);
                ViewBag.Role = role;
                ViewData.Model = AllRegisterUsers;
            }
             
            return View();
        }

        /// <summary>
        /// Grant or revoke a specific user for a particular role.
        /// </summary>
        /// <param name="userName">The account username.</param>
        /// <param name="role">The role name.</param>
        /// <param name="isSet">A valude indicating whether is the role should be granted or revoke.</param>
        public void SetRole(string userName, string role, bool isSet)
        {
            if (isSet)
            {
                Roles.AddUserToRole(userName, role);
                DebugConsole.Instance.EnqueueTraceInfo(string.Format("Successfully added {0} to {1} role", userName, role));
            }
            else
            {
                Roles.RemoveUserFromRole(userName, role);
                DebugConsole.Instance.EnqueueTraceInfo(string.Format("Successfully removed {0} from {1} role", userName, role));
            }
        }
    }
}
