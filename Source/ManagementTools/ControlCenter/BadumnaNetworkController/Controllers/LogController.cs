//-----------------------------------------------------------------------
// <copyright file="LogController.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
using System;
using System.Web.Mvc;
using System.Xml;

using Germinator;

namespace BadumnaNetworkController.Controllers
{
    /// <summary>
    /// LogController class has responsibility to display the messages from the system.diagnostic.trace. It is used for 
    /// the development and debuging purposes only and should be removed in the future.  
    /// </summary>
    public class LogController : Controller
    {
        /// <summary>
        /// The log index page, displaying all the log messages.
        /// </summary>
        /// <returns>Return log index page.</returns>
        public ActionResult Index()
        {
            // replace the UTC time to the local time (make user easier to read it)
            XmlDocument log = new XmlDocument();
            log.LoadXml(GerminatorFacade.Instance.GetLog());
            XmlNodeList traceEventNodes = log.LastChild.ChildNodes;

            foreach (XmlNode traceEventNode in traceEventNodes)
            {           
                System.Collections.IEnumerator systemNodes = traceEventNode.FirstChild.GetEnumerator();
                while (systemNodes.MoveNext())
                {
                    XmlNode node = (XmlNode)systemNodes.Current;
                    if (node.LocalName.Equals("TimeCreated"))
                    {
                        XmlNode systemTimeNode = node.Attributes.GetNamedItem("SystemTime");
                        DateTime time = DateTime.Parse(systemTimeNode.InnerText);
                        systemTimeNode.InnerText = "(" + time.ToShortDateString() + ") " + time.ToLongTimeString();
                    }
                }
            }

            this.ViewData.Model = log.SelectSingleNode("/Log").OuterXml;
            return View();
        }
    }
}
