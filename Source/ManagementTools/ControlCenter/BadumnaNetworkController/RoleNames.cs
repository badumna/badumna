﻿//------------------------------------------------------------------------------
// <copyright file="RoleNames.cs" company="National ICT Australia Limited">
//     Copyright (c) 2014 National ICT Australia Limited. All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BadumnaNetworkController
{
    /// <summary>
    /// Names of roles used in the control center app
    /// </summary>
    public static class RoleNames
    {
        /// <summary>
        /// The administration role
        /// </summary>
        public const string Admin = "Admin";
    }
}