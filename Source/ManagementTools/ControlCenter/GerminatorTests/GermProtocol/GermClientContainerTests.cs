﻿//-----------------------------------------------------------------------
// <copyright file="GermClientContainerTests.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace GerminatorTests.GermProtocol
{
    using System;
    using Germinator;
    using NUnit.Framework;
    using Rhino.Mocks;
    using System.Xml;

    [TestFixture]
    internal class GermClientContainerTests
    {
        [SetUp]
        public void SetUp()
        {
        }

        [Test]
        public void GermClientContainerLoadXmlEmptyElementSuccessfully()
        {
            GermClientContainer container = new GermClientContainer();

            Assert.IsNotNull(container);

            // load the xml input file, and make sure it loaded properly.
            XmlReader xmlReader = XmlReader.Create("GermProtocol/EmptyGermClient.xml");
            container.Load(xmlReader);

            Assert.IsTrue(xmlReader.EOF);
        }

        [Test]
        public void GermClientContainerLoadValidXmlInputSuccessfully()
        {
            string hostname = "localhost:21253";
            GermClientContainer container = new GermClientContainer();

            Assert.IsNotNull(container);

            // load the xml input file
            XmlReader xmlReader = XmlReader.Create("GermProtocol/GermClient.xml");
            container.Load(xmlReader);

            Assert.IsTrue(container.Count == 1);
            Assert.IsTrue(container.Contains(hostname));

            GermClient germ = container.GetGerm(hostname);
            Assert.IsNotNull(germ);
            Assert.IsFalse(germ.IsUndercover);
        }
    }
}
