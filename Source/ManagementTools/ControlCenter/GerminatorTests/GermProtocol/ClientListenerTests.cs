﻿//-----------------------------------------------------------------------
// <copyright file="ClientListenerTests.cs" company="National ICT Australia Ltd">
//     Copyright (c) 2012 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace GerminatorTests.GermProtocol
{
    using System;
    using Germinator;
    using NUnit.Framework;
    using Germinator.GermProtocol;
    using System.Security.Cryptography.X509Certificates;

    [TestFixture]
    internal class ClientListenerTests
    {
        private int sequenceNumber = 0;

        [SetUp]
        public void SetUp()
        {
        }

        [Test]
        public void DefaultConstructorShouldThrowNoError()
        {
            ClientListener listener = new ClientListener(new X509Certificate2());
            Assert.IsNotNull(listener);
        }

        [Test]
        public void GetsAndSetsAddGermPropertyWorkProperly()
        {
            ClientListener listener = new ClientListener(new X509Certificate2());
            listener.AddGerm = this.AddGerm;
            listener.AddGerm.Invoke(null);

            Assert.AreEqual(99, this.sequenceNumber);
        }

        [Test]
        public void GetsAndSetsPortWorks()
        {
            int newPort = 21254;
            ClientListener listener = new ClientListener(new X509Certificate2(), 21200);
            listener.Port = newPort;

            Assert.AreEqual(listener.Port, newPort);
        }

        private void AddGerm(GermClient client)
        {
            Assert.IsNull(client);
            this.sequenceNumber = 99;
        }
    }
}
