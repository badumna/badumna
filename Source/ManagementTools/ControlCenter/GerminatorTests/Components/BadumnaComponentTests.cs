﻿//-----------------------------------------------------------------------
// <copyright file="BadumnaComponentTests.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2011 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace GerminatorTests.Components
{
    using System;
    using Germinator.Components;
    using NUnit.Framework;
    using Rhino.Mocks;

    [TestFixture]
    internal class BadumnaComponentTests
    {
        private BadumnaComponent badumnaComponent;

        [Test]
        public void CreateBadumnaComponentAndSucceed()
        {
            this.badumnaComponent = new BadumnaComponent();

            Assert.IsNotNull(this.badumnaComponent);
            Assert.AreEqual(BadumnaComponent.ComponentIndex, this.badumnaComponent.Index);
            Assert.AreEqual("Connectivity Setting", this.badumnaComponent.DisplayName);
            Assert.IsFalse(this.badumnaComponent.IsDetachable);
            Assert.IsTrue(this.badumnaComponent.IsEnabled);
        }
    }
}
