﻿//-----------------------------------------------------------------------
// <copyright file="ComponentViewTests.cs" company="National ICT Australia Ltd">
//     Copyright (c) 2012 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace GerminatorTests.Components
{
    using NUnit.Framework;
    using Germinator.Components;

    [TestFixture]
    public class ComponentViewTests
    {
        private bool callbackIsCalled;
        
        [SetUp]
        public void SetUp()
        {
            this.callbackIsCalled = false;
        }

        [Test]
        public void PropertChangedEventShouldBeTriggered()
        {
            TestableComponent component = new TestableComponent();

            Assert.IsNotNull(component);
            Assert.AreEqual(component.Index, TestableComponent.ComponentIndex);

            ComponentView compView = new ComponentView(null, "Testable component", component);
            compView.PropertyChangedEvent += this.PropertyChangedHandler;

            compView.SetProperty("IsTestable", "true");

            Assert.IsTrue(this.callbackIsCalled);
            Assert.IsTrue(component.IsTestable);
        }

        private void PropertyChangedHandler(string propertyIndex)
        {
            Assert.AreEqual(propertyIndex, "IsTestable");
            this.callbackIsCalled = true;
        }
    }
}
