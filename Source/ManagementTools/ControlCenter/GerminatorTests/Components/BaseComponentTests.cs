﻿//-----------------------------------------------------------------------
// <copyright file="BaseComponentTests.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2011 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace GerminatorTests.Components
{
    using System;
    using Germinator.Components;
    using NUnit.Framework;
    using Rhino.Mocks;

    [TestFixture]
    internal class BaseComponentTests
    {
        private BaseComponent baseComponent;

        [Test]
        public void CreateBaseComponentSucceed()
        {
            this.baseComponent = new BaseComponent();

            Assert.IsNotNull(this.baseComponent);
            Assert.IsTrue(this.baseComponent.IsDetachable);
        }
    }
}
