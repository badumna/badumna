﻿//-----------------------------------------------------------------------
// <copyright file="TestableComponent.cs" company="National ICT Australia Ltd">
//     Copyright (c) 2012 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace GerminatorTests.Components
{
    using Germinator.Components;
    using System.Xml.Serialization;

    public class TestableComponent : BaseComponent
    {
        private static string componentIndex = "TestableComponent";

        public TestableComponent()
            : base(TestableComponent.componentIndex, "Testable component")
        {
            this.IsTestable = false;
        }

        public static string ComponentIndex
        {
            get { return componentIndex; }
        }

        [XmlElement("IsTestable")]
        [ComponentProperty("Is testable")]
        public bool IsTestable { get; set; }
    }
}
