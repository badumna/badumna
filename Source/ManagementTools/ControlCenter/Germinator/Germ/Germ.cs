//---------------------------------------------------------------------------------
// <copyright file="Germ.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2010 NICTA Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Management;
using System.Net.Sockets;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Xml;
using Utilities;

namespace GermServer
{
    /// <summary>
    /// Germ class handle the ControlCenter request.
    /// </summary>
    internal class Germ
    {
        /// <summary>
        /// Current germ base directory.
        /// </summary>
        private readonly string Directory = AppDomain.CurrentDomain.BaseDirectory;

        /// <summary>
        /// Generate a random number.
        /// </summary>
        private static Random random = new Random((int)DateTime.Now.Ticks);

        /// <summary>
        /// Client name.
        /// </summary>
        private string client;

        /// <summary>
        /// Harness listener.
        /// </summary>
        private HarnessListener harnessListener;

        /// <summary>
        /// Harness port.
        /// </summary>
        private int harnessPort;

        /// <summary>
        /// List of running processes.
        /// </summary>
        private Dictionary<int, ProcessDescription> processes;

        /// <summary>
        /// Performance monitor.
        /// </summary>
        private PerformanceMonitor performanceMonitor;

        /// <summary>
        /// A value indicating whether the germ running under mono.
        /// </summary>
        private bool isMono;

        /// <summary>
        /// Initializes a new instance of the <see cref="Germ"/> class.
        /// </summary>
        public Germ()
        {
            this.processes = new Dictionary<int, ProcessDescription>();
            this.harnessListener = new HarnessListener(this);
            this.harnessPort = this.harnessListener.Begin();

            this.isMono = Type.GetType("Mono.Runtime") != null;

            this.performanceMonitor = new PerformanceMonitor(System.Environment.OSVersion);
            
            // Note: shouldn't be a static string.
            this.client = "Germinator client"; 
        }

        /// <summary>
        /// Get the hash value of a file.
        /// </summary>
        /// <param name="fileName">File name.</param>
        /// <returns>Return the hash string.</returns>
        public static string GetHashOfFile(string fileName)
        {
            SHA1 sha = new SHA1Managed();
            byte[] result;

            sha.Initialize();

            try
            {
                result = sha.ComputeHash(File.ReadAllBytes(fileName));
            }
            catch (IOException)
            {
                return string.Empty;
            }

            int length = result.Length * 2;
            StringBuilder builder = new StringBuilder(length);

            foreach (byte b in result)
            {
                builder.AppendFormat("{0:X2}", b);
            }

            return builder.ToString(0, length);
        }

        /// <summary>
        /// Upload a file handler
        /// </summary>
        /// <param name="relativeDirectory">Relative directory.</param>
        /// <param name="fileName">File name.</param>
        /// <param name="hash">Hash value.</param>
        /// <returns>Return the uploader.</returns>
        public Uploader Upload(string relativeDirectory, string fileName, string hash)
        {
            try
            {
                Uploader uploader = new Uploader();

                if (uploader.Open(relativeDirectory, fileName, hash))
                {
                    return uploader;
                }
            }
            catch (Exception e)
            {
                Trace.TraceError("Failed to open uploader : {0}", e.Message);
            }

            return null;
        }

        /// <summary>
        /// Get the running applications.
        /// </summary>
        /// <returns>Retunt the list of running applications</returns>
        public List<ProcessDescription> GetRunningApplications()
        {
            List<ProcessDescription> applicationList = new List<ProcessDescription>();

            foreach (KeyValuePair<int, ProcessDescription> pair in this.processes)
            {
                try
                {
                    if (!Process.GetProcessById(pair.Key).HasExited)
                    {
                        applicationList.Add(pair.Value);
                    }
                    else
                    {
                        Trace.TraceInformation("Process {0} is not running", pair.Key);
                    }
                }
                catch (ArgumentException e)
                {
                    Trace.TraceError(e.Message);
                }
            }

            this.processes.Clear();
            foreach (ProcessDescription description in applicationList)
            {
                this.processes.Add(description.ProcessId, description);
            }

            Trace.TraceInformation("{0} : Listing running processes.", this.client);
            return applicationList;
        }

        /// <summary>
        /// Start an application.
        /// </summary>
        /// <param name="name">Service name.</param>
        /// <param name="applicationDirectory">Application directory.</param>
        /// <param name="executable">Executable file.</param>
        /// <param name="args">Process line argument.</param>
        /// <returns>Return the process id.</returns>
        public int StartApplication(string name, string applicationDirectory, string executable, string args)
        {
            ProcessDescription description = new ProcessDescription(0, name, args);
            bool isMono = Type.GetType("Mono.Runtime") != null;
            ProcessStartInfo startInfo = new ProcessStartInfo();

            object harnessLock = new object();
            bool isHarnessed = false;

            if (this.harnessPort > 0)
            {
                try
                {
                    args += this.harnessListener.OnConnectionCallback(
                        delegate(HarnessController controller)
                        {
                            description.Controller = controller;
                            lock (harnessLock)
                            {
                                isHarnessed = true;
                            }
                        });
                }
                catch 
                { 
                }
            }

            startInfo.UseShellExecute = true;
            startInfo.WorkingDirectory = Path.Combine(this.Directory, applicationDirectory);
            if (isMono)
            {
                startInfo.FileName = "mono";
                startInfo.Arguments = String.Format("\"{0}\" {1}", Path.Combine(startInfo.WorkingDirectory, executable), args);
            }
            else
            {
                startInfo.FileName = Path.Combine(startInfo.WorkingDirectory, executable);
                startInfo.Arguments = args;
            }

            Process processInfo = null;

            try
            {
                Trace.TraceInformation("{0} : Starting application ... {1} {2}.", this.client, executable, args);
                Trace.TraceInformation(" Start information .. : {0}, {1}, {2}", startInfo.FileName, startInfo.Arguments, startInfo.WorkingDirectory);
                processInfo = Process.Start(startInfo);
                description.ProcessId = processInfo.Id;
                description.ProcessInfo = processInfo;
                Trace.TraceInformation("{0} : Start process sucessfull with id {1}, {2}", DateTime.Now.ToString(), description.ProcessId, processInfo.ToString());
            }
            catch (Exception e)
            {
                Trace.TraceError("Failed to start process {0} : {1}", executable, e.Message);
                return -1;
            }

            try
            {
                if (this.processes.ContainsKey(description.ProcessId))
                {
                    // Old process must have terminated
                    this.processes[description.ProcessId] = description;
                }
                else
                {
                    this.processes.Add(description.ProcessId, description);
                }

                // Wait a while for the harness to connect. Not using WaitOne because it is not implemented in mono 2.2
                // It can take a while to unpack the package if it has been updated (should wait longer then 15 s, 
                // current version will be set to 30 seconds)
                DateTime endWait = DateTime.Now + TimeSpan.FromSeconds(30);

                while (DateTime.Now < endWait)
                {
                    lock (harnessLock)
                    {
                        if (isHarnessed)
                        {
                            break;
                        }
                    }
                }

                return description.ProcessId;
            }
            catch (Exception e)
            {
                Trace.TraceError("Failed to index process {0} : {1}", processInfo.Id, e.Message);
                try
                {
                    processInfo.Kill();
                }
                catch (Exception e2)
                {
                    Trace.TraceError("Failed to kill rogue process {0} : {1}", processInfo.Id, e2.Message);
                }
            }

            return -1;
        }

        /// <summary>
        /// Kill an application given its process id.
        /// </summary>
        /// <param name="processId">Process id.</param>
        /// <returns>Return true on success.</returns>
        public bool KillApplication(int processId)
        {
            ProcessDescription description = null;
            if (this.processes.TryGetValue(processId, out description))
            {
                try
                {
                    Process applicationProcess = Process.GetProcessById(processId);

                    if (applicationProcess != null && !applicationProcess.HasExited)
                    {
                        Trace.TraceInformation("{0} : Killing process {1}.", this.client, processId);
                        applicationProcess.Kill();
                    }

                    if (description.Controller != null)
                    {
                        Trace.TraceInformation("{0} : Killing harnessed process ", this.client);
                        description.Controller.KillClient();
                    }
                    else
                    {
                        // TODO: kill the unharness process
                        Process[] processes = Process.GetProcesses();
                        foreach (Process process in processes)
                        {
                            try
                            {
                                // TODO: Revise this method, and make sure it will work in any platform
                                PerformanceCounter pc = new PerformanceCounter("Process", "Creating Process Id", process.ProcessName);
                                long parentProcessId = pc.RawValue;
                                if (parentProcessId == processId)
                                {
                                    Process unharnessProcess = Process.GetProcessById(process.Id);
                                    unharnessProcess.Kill();
                                    Trace.TraceInformation("{0} : Killing unharnessed process ", this.client);
                                    break;
                                }
                            }
                            catch (Exception e)
                            {
                                Trace.TraceError("Exception in killing un-harnessed process: {0} ", e.Message);
                            }
                        }
                    }
                }
                catch (ArgumentException)
                {
                    // Process is not running!
                    return false;
                }

                this.processes.Remove(processId);
                return true;
            }

            return false;
        }

        /// <summary>
        /// Forward message to the harness process.
        /// </summary>
        /// <param name="processId">Process id.</param>
        /// <param name="messageType">Message type.</param>
        /// <param name="message">Custom message.</param>
        /// <param name="reply">Reply message need to be sent back to Control Center.</param>
        /// <returns>Return the custom message reply type.</returns>
        public CustomMessageReply ForwardMessage(int processId, int messageType, byte[] message, out byte[] reply)
        {
            ProcessDescription description = null;
            if (this.processes.TryGetValue(processId, out description))
            {
                // Check the process status
                Process process = description.ProcessInfo;

                if (process != null && !process.HasExited)
                {
                    if (description.Controller != null)
                    {
                        if (description.Controller.SendMessage(messageType, message, out reply))
                        {
                            return CustomMessageReply.OkWithData;
                        }
                        else
                        {
                            return CustomMessageReply.UnspecifiedError;
                        }
                    }
                    else
                    {
                        Trace.TraceWarning("Process {0} is not harnessed", processId);
                        reply = null;
                        return CustomMessageReply.ProcessUnharnessed;
                    }

                }
                else if (process != null && process.HasExited)
                {
                    reply = Encoding.UTF8.GetBytes(description.Name);
                    switch (process.ExitCode)
                    {
                        case 2:
                            return CustomMessageReply.ProcessExitArgumentError;
                        case 3:
                            return CustomMessageReply.ProcessExitDeiAuthenticationFailed;
                        case 4:
                            return CustomMessageReply.ProcessExitAnnounceServiceFailed;
                        case 5:
                            return CustomMessageReply.ProcessExitConfigurationError;
                        default:
                            return CustomMessageReply.ProcessExitWithUnknownReason;
                    }
                }
                else
                {
                    reply = Encoding.UTF8.GetBytes(description.Name);
                    return CustomMessageReply.ProcessExitWithUnknownReason;
                }
            }
            else
            {
                Trace.TraceWarning("Cannot find process {0}", processId);
                reply = null;
                return CustomMessageReply.ProcessUnknown;
            }
        }

        /// <summary>
        /// Get machine status
        /// </summary>
        /// <param name="message">Custom message.</param>
        /// <param name="reply">Reply message contains the machine status if success.</param>
        /// <returns>Retutn custom message reply.</returns>
        public CustomMessageReply GetMachineStatus(byte[] message, out byte[] reply)
        {
            MemoryStream response = new MemoryStream();
            BinaryWriter writer = new BinaryWriter(response);
            int totalBytesSent = 0;
            int totalBytesReceived = 0;

            string[] status = this.performanceMonitor.GetMachineStatus();

            // Current Cpu Usage
            writer.Write(status[0]);
            // Available RAM
            writer.Write(status[1]);

            // get the total bytes sent and received per second
            foreach (KeyValuePair<int, ProcessDescription> pair in this.processes)
            {
                ProcessDescription description = pair.Value;
                if (description.Controller != null)
                {
                    byte[] replyMessage;
                    if (description.Controller.SendMessage(int.MaxValue - 4, new byte[] { }, out replyMessage))
                    {
                        // TODO: Fix this, currently germ doesn't have information about the message type
                        // int.Maxvalue - 4 represent CustomRequestType.Status
                        try
                        {
                            XmlDocument document = new XmlDocument();

                            document.Load(new MemoryStream(replyMessage));
                            document.LoadXml(document.SelectSingleNode("/Status").OuterXml);
                            totalBytesSent += int.Parse(document.SelectSingleNode("/Status/TransferRate/TotalBytesSentPerSecond").InnerText);
                            totalBytesReceived += int.Parse(document.SelectSingleNode("/Status/TransferRate/TotalBytesReceivedPerSecond").InnerText);
                        }
                        catch 
                        { 
                        }
                    }
                }
            }

            writer.Write(totalBytesSent);
            writer.Write(totalBytesReceived);

            reply = response.ToArray();
            return CustomMessageReply.OkWithData;
        }
    }
}