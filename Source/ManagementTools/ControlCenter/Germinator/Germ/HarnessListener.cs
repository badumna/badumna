﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Diagnostics;

namespace GermServer
{
    class HarnessListener
    {
        private Germ mGerm;
        private TcpListener mListener;

        public HarnessListener(Germ germ)
        {
            this.mGerm = germ;
        }

        public int Begin()
        {
            try
            {
                this.mListener = new TcpListener(new IPEndPoint(IPAddress.Loopback, 0));
                this.mListener.Start();
                this.mListener.BeginAcceptTcpClient(HarnessListener.ReceiveCallback, this);
                return (this.mListener.LocalEndpoint as IPEndPoint).Port;
            }
            catch (SocketException e)
            {
                Trace.TraceError("Failed to start harness listener : {0}", e.Message);
            }

            return -1;
        }

        public void Stop()
        {
            this.mListener.Stop();
        }

        private object mConnectionLock = new object();
        private Dictionary<int, ConnectionCallback> mCallbacks = new Dictionary<int, ConnectionCallback>();
        private static int mNextCallbackId;
        public delegate void ConnectionCallback(HarnessController controller);
        public string OnConnectionCallback(ConnectionCallback callback)
        {
            lock (this.mConnectionLock)
            {
                int callbackId = HarnessListener.mNextCallbackId++;
                this.mCallbacks[callbackId] = callback;

                string arguments = String.Format(" --harness-port={0} --control-id={1}", (this.mListener.LocalEndpoint as IPEndPoint).Port, callbackId);
                return arguments;
            }
        }

        private void HandleNewConnection(TcpClient client)
        {
            lock (this.mConnectionLock)
            {
                HarnessController controller = HarnessController.CreateHarnessController(client);
                if (controller != null)
                {
                    ConnectionCallback callback = null;
                    if (this.mCallbacks.TryGetValue(controller.ControllerId, out callback))
                    {
                        callback(controller);
                    }
                    else
                    {
                        controller.Close();
                    }
                }
            }
        }



        static private void ReceiveCallback(IAsyncResult ar)
        {
            HarnessListener listener = (HarnessListener)ar.AsyncState;
            TcpClient client = null;

            if (listener == null)
            {
                return;
            }

            try
            {
                client = listener.mListener.EndAcceptTcpClient(ar);
            }
            catch
            {
            }

            if (null != client && client.Connected)
            {
                try
                {
                    listener.HandleNewConnection(client);
                }
                catch (Exception)
                {
                }
            }

            listener.mListener.BeginAcceptTcpClient(HarnessListener.ReceiveCallback, listener);
        }

    }

}
