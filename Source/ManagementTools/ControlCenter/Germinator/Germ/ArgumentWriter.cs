﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Diagnostics;

namespace GermServer
{
    class ArgumentWriter : IDisposable
    {
        private MemoryStream mStream;
        private BinaryWriter mWriter;

        public ArgumentWriter()
        {
            this.mStream = new MemoryStream();
            this.mWriter = new BinaryWriter(this.mStream);
            this.WriteHeader(0);
        }

        public void WriteHeader(int type)
        {
            int length = Math.Max(0, (int)this.mStream.Length - 8);

            this.mStream.Position = 0;
            this.mWriter.Write(type);
            this.mWriter.Write(length);

            this.mStream.Position = this.mStream.Length;
        }

        public void Write(byte[] buffer)
        {
            this.mWriter.Write(buffer.Length);
            this.mWriter.Write(buffer, 0, buffer.Length);
        }

        public void Write(byte[] buffer, int length)
        {
            this.mWriter.Write(length);
            this.mWriter.Write(buffer, 0, length); 
        }

        public void Write(bool value)
        {
            this.mWriter.Write((byte)(value ? 1 : 0));
        }

        public void Write(int value)
        {
            this.mWriter.Write(value);
        }

        public void Write(string value)
        {
            this.mWriter.Write(value);
        }

        public byte[] GetBytes()
        {
            return this.mStream.ToArray();
        }

        public void ToStream(Stream stream)
        {
            try
            {
                byte[] buffer = this.mStream.ToArray();
                stream.Write(buffer, 0, buffer.Length);
                stream.Flush();
            }
            catch (IOException e)
            {
                Trace.TraceError("Germ protocol error : Failed to write argument to stream : {0}", e.Message);
            }
        }

        public void Dispose()
        {
            if (this.mWriter != null)
            {
                this.mWriter.Close();
                this.mWriter = null;
            }

            if (this.mStream != null)
            {
                this.mStream.Close();
                this.mStream = null;
            }
        }

    }
}

