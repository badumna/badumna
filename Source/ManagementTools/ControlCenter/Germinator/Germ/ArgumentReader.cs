﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace GermServer
{
    class ArgumentReader : IDisposable
    {
        private MemoryStream mStream;
        private BinaryReader mReader;

        public ArgumentReader(byte[] value)
        {
            this.mStream = new MemoryStream(value);
            this.mReader = new BinaryReader(this.mStream);
        }

        public byte[] ReadBytes()
        {
            return this.mReader.ReadBytes(this.mReader.ReadInt32());
        }
        
        public bool ReadBool()
        {
            return this.mReader.ReadByte() != 0;
        }

        public int ReadInt()
        {
            return this.mReader.ReadInt32();
        }

        public string ReadString()
        {
            return this.mReader.ReadString();
        }


        public void Dispose()
        {
            if (this.mReader != null)
            {
                this.mReader.Close();
                this.mReader = null;
            }

            if (this.mStream != null)
            {
                this.mStream.Close();
                this.mStream = null;
            }
        }

    }
}
