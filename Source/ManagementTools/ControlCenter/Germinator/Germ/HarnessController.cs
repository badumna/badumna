﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Net.Sockets;
using System.Diagnostics;

namespace GermServer
{
    class HarnessController
    {
        public int ControllerId { get; set; }

        private int mProcessId;
        private TcpClient mClient;

        private HarnessController(TcpClient client)
        {
            this.mClient = client;
            this.Begin();
        }

        public static HarnessController CreateHarnessController(TcpClient client)
        {
            if (!client.Connected)
            {
                return null;
            }

            try
            {
                Stream clientStream = client.GetStream();
                int major = HarnessController.ReadInt(clientStream);
                int minor = HarnessController.ReadInt(clientStream);

                // TODO : Check version matches and create the appropriate version

                if (major == 1 && minor == 0)
                {
                    return new HarnessController(client);
                }

                client.Close();
            }
            catch { }

            return null;
        }

        public void Close()
        {
            if (this.mClient != null)
            {
                this.mClient.Close();
            }
        }

        public virtual void KillClient()
        {
            try
            {
                this.Close();

                Process harnessedProcess = Process.GetProcessById(this.mProcessId);
                if (harnessedProcess != null && !harnessedProcess.HasExited)
                {
                    harnessedProcess.Kill();
                }
            }
            catch { }
        }

        public virtual bool SendMessage(int messageType, byte[] message, out byte[] reply)
        {
            if (!this.mClient.Connected)
            {
                reply = null;
                return false;
            }

            try
            {
                Trace.TraceInformation("Forwarding message {0} ({1} bytes)", messageType, message.Length);
                Stream clientStream = this.mClient.GetStream();
                BinaryWriter writer = new BinaryWriter(clientStream);
                BinaryReader reader = new BinaryReader(clientStream);

                writer.Write(messageType);
                writer.Write(message.Length);
                writer.Write(message);
                writer.Flush();

                int length = reader.ReadInt32();
                if (length > 0)
                {
                    reply = reader.ReadBytes(length);
                }
                else
                {
                    reply = new byte[] { };
                }

                Trace.TraceInformation("Got reply ({0} bytes)", reply.Length);

                return true;
            }
            catch (Exception e)
            {
                Trace.TraceError("Error forwarding message : {0}", e.Message);
            }

            reply = null;
            return false;
        }

        protected virtual void Begin()
        {
            this.ControllerId = HarnessController.ReadInt(this.mClient.GetStream());
            this.mProcessId = HarnessController.ReadInt(this.mClient.GetStream());
        }

        private static int ReadInt(Stream clientStream)
        {
            byte[] buffer = new byte[4];
            clientStream.Read(buffer, 0, 4);
            return BitConverter.ToInt32(buffer, 0);
        }

    }
}
