﻿//-----------------------------------------------------------------------
// <copyright file="ProcessDescription.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace GermServer
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Net.Sockets;
    using System.Text;

    /// <summary>
    /// ProcessDescription describe about a process.
    /// </summary>
    public class ProcessDescription
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProcessDescription"/> class.
        /// </summary>
        /// <param name="processId">Process id.</param>
        /// <param name="name">Process name.</param>
        /// <param name="args">Process arguments.</param>
        public ProcessDescription(int processId, string name, string args)
        {
            this.ProcessId = processId;
            this.Name = name;
            this.Args = args;
        }

        /// <summary>
        /// Gets the process id.
        /// </summary>
        public int ProcessId { get; internal set; }

        /// <summary>
        /// Gets the process name.
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// Gets the process arguments.
        /// </summary>
        public string Args { get; private set; }

        /// <summary>
        /// Gets the process info.
        /// </summary>
        public Process ProcessInfo { get; internal set; }

        /// <summary>
        /// Gets a value indicating whether the process is harness.
        /// </summary>
        public bool IsHarness
        {
            get { return this.Controller != null; }
        }

        /// <summary>
        /// Gets or sets the harness controller.
        /// </summary>
        internal HarnessController Controller { get; set; }
    }
}
