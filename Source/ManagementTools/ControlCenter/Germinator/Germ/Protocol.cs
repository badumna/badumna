﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;

namespace GermServer
{
    public enum RequestType : int
    {
        Close = 0,
        StartConversation = 1,
        AcceptConversation = 2,
        GetAutheticateTokenRequest = 3,
        GetAutheticateTokenReply = 4,
        AuthenticateRequest = 5,
        AutheticateReply = 6,
        StartProcessRequest = 7,
        StartProcessReply = 8,
        KillProcessRequest = 9,
        KillProcessReply = 10,
        UploadFileRequest = 11,
        UploadFileBegin = 12,
        UploadFileWriteSegment = 13,
        UploadFileAckSegment = 14,
        UploadFileClose = 15,
        UploadFileComplete = 16,
        ListProcessRequest = 17,
        ListProcessReply = 18,
        ForwardCustomRequest = 19,
        ForwardCustomReply = 20,
        GetMachineStatusRequest = 21,
        GetMachineStatusReply = 22
    }

    public enum CustomMessageReply : int
    {
        NotConnected = 0,
        UnspecifiedError = 1,
        OkWithoutData = 2,
        OkWithData = 3,
        ProcessUnknown = 4,
        ProcessUnharnessed = 5,
        NotAuthorized = 6,
        ProcessExitWithUnknownReason = 7,
        ProcessExitArgumentError = 8,
        ProcessExitDeiAuthenticationFailed = 9,
        ProcessExitAnnounceServiceFailed = 10,
        ProcessExitConfigurationError = 11
    }

    /// <summary>
    /// This prococol follows the TLV style (Type, Length, Value)
    /// </summary>
    /// TODO : Check network order.
    public abstract class Protocol
    {
        private const int mMajor = 1;
        private const int mMinor = 1;

        private delegate ArgumentWriter RequestHandler(ArgumentReader reader);
       
        private List<RequestHandler> mHandlers;

        private Stream mStream;
        private Thread mListenThread;
        private bool mContinueListening;
        private string mPeerDescription;

        public bool ConnectionNegotiated { get { return this.mContinueListening; } }

        // Replies
        private byte[] mAuthenticationToken;
        private bool mAuthenticationSuccessful;
        private List<ProcessDescription> mProcessList = new List<ProcessDescription>();
        private int mFileId = -1;
        private int mProcessId = -1;
        private byte[] mCustomMessageReply;
        private CustomMessageReply mCustomMessageReplyType;

        public Protocol()
        {
            this.mHandlers = new List<RequestHandler>(new RequestHandler[] 
            {
                this.Close,
                this.StartConversation,
                this.AcceptConversation,
                this.GetAuthenticateTokenRequest,
                this.GetAutheticateTokenReply,
                this.AuthenticateRequest,
                this.AutheticateReply,
                this.StartProcessRequest,
                this.StartProcessReply,
                this.KillProcessRequest,
                this.KillProcessReply,
                this.UploadFileRequest,
                this.UploadFileBegin,
                this.UploadFileWriteSegment,
                this.UploadFileAckSegment,
                this.UploadFileClose,
                this.UploadFileComplete,
                this.ListProcessRequest,
                this.ListProcessReply,
                this.ForwardCustomRequest,
                this.ForwardCustomReply,
                this.GetMachineStatusRequest,
                this.GetMachineStatusReply
            });
        }

        protected bool ConnectToServer(BinaryReader reader, string serverDescription)
        {
            using (ArgumentWriter writer = new ArgumentWriter())
            {
                writer.Write(Protocol.mMajor);
                writer.Write(Protocol.mMinor);
                this.WriteRequestOrReply(writer, RequestType.StartConversation);
                writer.ToStream(reader.BaseStream);
            }

            this.ProcessOneRequest(reader);
            if (!this.ConnectionNegotiated)
            {
                Trace.TraceError("Failed to negetiate connection with {0}", serverDescription);
            }

            return this.ConnectionNegotiated;
        }

        public void BeginListenThread(Stream stream, IPEndPoint clientAddress)
        {
            if (this.mStream == null)
            {
                this.mPeerDescription = clientAddress.ToString();
                this.mStream = stream;
                this.mListenThread = new Thread(this.Listen);
                this.mListenThread.IsBackground = true;
                this.mListenThread.Start();
            }
        }

        protected void EndConversation(BinaryReader reader)
        {
            this.mContinueListening = false;

            using (ArgumentWriter writer = new ArgumentWriter())
            {
                this.WriteRequestOrReply(writer, RequestType.Close);
                writer.ToStream(reader.BaseStream);
            }
        }

        protected byte[] GetAuthenticationToken(BinaryReader reader)
        {
            using (ArgumentWriter writer = new ArgumentWriter())
            {
                this.WriteRequestOrReply(writer, RequestType.GetAutheticateTokenRequest);
                writer.ToStream(reader.BaseStream);
            }

            this.mAuthenticationToken = null;
            this.ProcessOneRequest(reader);
            return this.mAuthenticationToken;
        }

        protected bool Authenticate(BinaryReader reader, byte[] signature, string clientDescription)
        {
            using (ArgumentWriter writer = new ArgumentWriter())
            {
                writer.Write(signature);
                writer.Write(clientDescription);
                this.WriteRequestOrReply(writer, RequestType.AuthenticateRequest);
                writer.ToStream(reader.BaseStream);
            }

            this.mAuthenticationSuccessful = false;
            this.ProcessOneRequest(reader);
            return this.mAuthenticationSuccessful;
        }

        protected List<ProcessDescription> GetRunningApplications(BinaryReader reader)
        {
            using (ArgumentWriter writer = new ArgumentWriter())
            {
                this.WriteRequestOrReply(writer, RequestType.ListProcessRequest);
                writer.ToStream(reader.BaseStream);
            }

            this.mProcessList.Clear();
            this.ProcessOneRequest(reader);
            return this.mProcessList;
        }

        protected int SendOpenFile(BinaryReader reader, string directory, string file, string hash)
        {
            using (ArgumentWriter writer = new ArgumentWriter())
            {
                writer.Write(directory);
                writer.Write(file);
                writer.Write(hash);

                this.WriteRequestOrReply(writer, RequestType.UploadFileRequest);
                writer.ToStream(reader.BaseStream);
            }

            this.mFileId = -1;
            this.ProcessOneRequest(reader);
            return this.mFileId;
        }

        protected bool SendFileSegment(BinaryReader reader, int id, byte[] segment, int length)
        {
            using (ArgumentWriter writer = new ArgumentWriter())
            {
                writer.Write(id);
                writer.Write(segment, length);
                this.WriteRequestOrReply(writer, RequestType.UploadFileWriteSegment);
                writer.ToStream(reader.BaseStream);
            }

            this.mFileId = -1;
            this.ProcessOneRequest(reader);
            return this.mFileId == id;
        }

        protected bool SendCloseFile(BinaryReader reader, int id)
        {
            using (ArgumentWriter writer = new ArgumentWriter())
            {
                writer.Write(id);
                this.WriteRequestOrReply(writer, RequestType.UploadFileClose);
                writer.ToStream(reader.BaseStream);
            }

            this.mFileId = -1;
            this.ProcessOneRequest(reader);
            return this.mFileId == id;
        }

        protected int SendStartProcess(BinaryReader reader, string name, string directory, string file, string args)
        {
            using (ArgumentWriter writer = new ArgumentWriter())
            {
                writer.Write(name);
                writer.Write(directory);
                writer.Write(file);
                writer.Write(args);
                this.WriteRequestOrReply(writer, RequestType.StartProcessRequest);
                writer.ToStream(reader.BaseStream);
            }

            this.mProcessId = -1;
            this.ProcessOneRequest(reader);
            return this.mProcessId;
        }

        protected bool SendKillProcess(BinaryReader reader, int processId)
        {
            using (ArgumentWriter writer = new ArgumentWriter())
            {
                writer.Write(processId);
                this.WriteRequestOrReply(writer, RequestType.KillProcessRequest);
                writer.ToStream(reader.BaseStream);
            }

            this.mProcessId = -1;
            this.ProcessOneRequest(reader);
            return this.mProcessId == processId;
        }

        protected CustomMessageReply ForwardCustomMessage(BinaryReader reader, int processId, int messageType, byte[] message, out byte[] reply)
        {
            using (ArgumentWriter writer = new ArgumentWriter())
            {
                writer.Write(processId);
                writer.Write(messageType);
                writer.Write(message);
                this.WriteRequestOrReply(writer, RequestType.ForwardCustomRequest);
                writer.ToStream(reader.BaseStream);
            }

            this.mCustomMessageReply = null;
            this.ProcessOneRequest(reader);
            reply = this.mCustomMessageReply;
            
            if (reply == null)
            {
                return CustomMessageReply.UnspecifiedError;
            }

            return this.mCustomMessageReplyType;
        }

        protected CustomMessageReply GetMachineStatus(BinaryReader reader, byte[] message, out byte[] reply)
        {
            using (ArgumentWriter writer = new ArgumentWriter())
            {
                writer.Write(message);
                this.WriteRequestOrReply(writer, RequestType.GetMachineStatusRequest);
                writer.ToStream(reader.BaseStream);
            }

            this.mCustomMessageReply = null;
            this.ProcessOneRequest(reader);
            reply = this.mCustomMessageReply;
            
            if (reply == null)
            {
                return CustomMessageReply.UnspecifiedError;
            }

            return this.mCustomMessageReplyType;
        }

        protected bool ProcessOneRequest(BinaryReader reader)
        {
            try
            {
                int type = reader.ReadInt32();
                int length = reader.ReadInt32();
                byte[] value = new byte[] { };
                if (length > 0)
                {
                    value = reader.ReadBytes(length);
                }

                if (this.mHandlers.Count <= type)
                {
                    this.Error("Request {0} is unknown", type);
                    return false;
                }

                RequestHandler handler = this.mHandlers[type];
                try
                {
                    ArgumentWriter writer = null;
                    if (value.Length > 0)
                    {
                        using (ArgumentReader requestReader = new ArgumentReader(value))
                        {
                            writer = handler(requestReader);
                        }
                    }
                    else
                    {
                        writer = handler(null);
                    }

                    if (writer != null)
                    {
                        writer.ToStream(reader.BaseStream);
                        writer.Dispose();
                    }
                }
                catch (Exception e)
                {
                    this.Error("Germ protocol error : Handler for request {0} threw an exception {1}", type, e.Message);
                    this.Error("   {0}", e.StackTrace);
                    return false;
                }

                if ((RequestType)type == RequestType.Close)
                {
                    this.EndConversation(reader);
                    return false;
                }

                return true;
            }
            catch (IOException)
            {
                this.Error("Conversation with {0} ended abruptly", this.mPeerDescription);
            }

            return false;
        }

        protected void Error(string message, params object[] args)
        {
            Trace.TraceError(message, args);
            System.Console.WriteLine(message, args);
        }

        protected virtual void OnClose()
        {
        }

        protected virtual byte[] OnGetAuthenticateTokenRequest()
        {
            this.Error("GetAuthenticateTokenRequest not implemented");
            return null;
        }

        protected virtual bool OnAuthenticateRequest(byte[] signiture, string clientDescription)
        {
            this.Error("AuthenticateRequest not implemented");
            return false;
        }

        protected virtual int OnStartProcessRequest(string name, string directory, string executable, string arguments)
        {
            this.Error("StartProcessRequest not implemented");
            return -1;
        }

        protected virtual int OnKillProcessRequest(int processId)
        {
            this.Error("KillProcessRequest not implemented");
            return -1;
        }

        protected virtual int OnUploadFileRequest(string directory, string file, string hash)
        {
            this.Error("UploadFileRequest not implemented");
            return -1;
        }

        protected virtual int OnUploadFileWriteSegment(int uploaderId, byte[] segment)
        {
            this.Error("UploadFileWriteSegment not implemented");
            return -1;
        }

        protected virtual int OnUploadFileClose(int uploaderId)
        {
            this.Error("UploadFileClose not implemented");
            return -1;
        }

        protected virtual IList<ProcessDescription> OnListProcesses()
        {
            this.Error("ListProcessRequest not implemented");
            return new List<ProcessDescription>();
        }

        protected virtual CustomMessageReply OnCustomMessage(int processId, int messageType, byte[] message, out byte[] reply)
        {
            this.Error("CustomMessage not implemented");
            reply = new byte[] { };
            return CustomMessageReply.UnspecifiedError;
        }

        protected virtual CustomMessageReply OnGetMachineStatus(byte[] message, out byte[] reply)
        {
            this.Error("GetMachineStatus not implemented");
            reply = new byte[] { };
            return CustomMessageReply.UnspecifiedError;
        }

        private void Listen()
        {
            using (BinaryReader reader = new BinaryReader(this.mStream))
            {
                do
                {
                    try
                    {
                        this.mContinueListening = this.ProcessOneRequest(reader);
                    }
                    catch (Exception e)
                    {
                        this.Error("Conversation ended with exception : {0}", e.Message);
                        this.mContinueListening = false;
                    }
                } 
                while (this.mContinueListening);
            }
            this.mStream = null;
        }

        private void WriteRequestOrReply(ArgumentWriter writer, RequestType type)
        {
            writer.WriteHeader((int)type);
        }

        private bool AcceptConnection(ref int major, ref int minor, string peerDescription)
        {
            bool isAcceptable = major == Protocol.mMajor && minor == Protocol.mMinor; // TODO : Actually negotiate this
            if (!isAcceptable)
            {
                Trace.TraceWarning("Failed to negotiate connection with {0}", peerDescription);
            }

            return isAcceptable;
        }

        private ArgumentWriter StartConversation(ArgumentReader reader)
        {
            int major = reader.ReadInt();
            int minor = reader.ReadInt();

            ArgumentWriter writer = new ArgumentWriter();
            if (this.AcceptConnection(ref major, ref minor, this.mPeerDescription))
            {
                writer.Write(major);
                writer.Write(minor);
                this.WriteRequestOrReply(writer, RequestType.AcceptConversation);

                this.mContinueListening = true;
            }
            else
            {
                this.Error("Incompatible protocol version. Closing conversation");
                this.WriteRequestOrReply(writer, RequestType.Close);
            }

            return writer;
        }

        private ArgumentWriter AcceptConversation(ArgumentReader reader)
        {
            int major = reader.ReadInt();
            int minor = reader.ReadInt();

            ArgumentWriter writer = null;
            if (this.AcceptConnection(ref major, ref minor, this.mPeerDescription))
            {
                this.mContinueListening = true;
            }
            else
            {
                writer = new ArgumentWriter();
                this.WriteRequestOrReply(writer, RequestType.Close);
                this.Error("Incompatible protocol version. Closing conversation");
            }

            return writer;
        }

        private ArgumentWriter Close(ArgumentReader reader)
        {
            this.OnClose();
            return null;
        }

        private ArgumentWriter GetAuthenticateTokenRequest(ArgumentReader reader)
        {
            byte[] token = this.OnGetAuthenticateTokenRequest();
            ArgumentWriter writer = new ArgumentWriter();

            if (token == null)
            {
                this.WriteRequestOrReply(writer, RequestType.Close);
                this.Error("Failed to get authentication token. Closing conversation");
            }
            else
            {
                writer.Write(token);
                this.WriteRequestOrReply(writer, RequestType.GetAutheticateTokenReply);
            }

            return writer;
        }

        private ArgumentWriter GetAutheticateTokenReply(ArgumentReader reader)
        {
            this.mAuthenticationToken = reader.ReadBytes();
            return null;
        }

        private ArgumentWriter AuthenticateRequest(ArgumentReader reader)
        {
            ArgumentWriter writer = new ArgumentWriter();

            bool successful = this.OnAuthenticateRequest(reader.ReadBytes(), reader.ReadString());
            writer.Write(successful);
            this.WriteRequestOrReply(writer, RequestType.AutheticateReply);

            return writer;
        }

        private ArgumentWriter AutheticateReply(ArgumentReader reader)
        {
            this.mAuthenticationSuccessful = reader.ReadBool();
            return null;
        }

        private ArgumentWriter StartProcessRequest(ArgumentReader reader)
        {
            ArgumentWriter writer = new ArgumentWriter();
            int processId = this.OnStartProcessRequest(reader.ReadString(), reader.ReadString(), reader.ReadString(), reader.ReadString());

            writer.Write(processId);
            this.WriteRequestOrReply(writer, RequestType.StartProcessReply);

            return writer;
        }

        private ArgumentWriter StartProcessReply(ArgumentReader reader)
        {
            this.mProcessId = reader.ReadInt();
            return null;
        }

        private ArgumentWriter KillProcessRequest(ArgumentReader reader)
        {
            ArgumentWriter writer = new ArgumentWriter();
            int processId = this.OnKillProcessRequest(reader.ReadInt());
            writer.Write(processId);
            this.WriteRequestOrReply(writer, RequestType.KillProcessReply);

            return writer;
        }

        private ArgumentWriter KillProcessReply(ArgumentReader reader)
        {
            this.mProcessId = reader.ReadInt();
            return null;
        }

        private ArgumentWriter UploadFileRequest(ArgumentReader reader)
        {
            int uploaderId = this.OnUploadFileRequest(reader.ReadString(), reader.ReadString(), reader.ReadString());
            ArgumentWriter writer = new ArgumentWriter();

            writer.Write(uploaderId);
            this.WriteRequestOrReply(writer, RequestType.UploadFileBegin);

            return writer;
        }

        private ArgumentWriter UploadFileBegin(ArgumentReader reader)
        {
            this.mFileId = reader.ReadInt();
            return null;
        }

        private ArgumentWriter UploadFileWriteSegment(ArgumentReader reader)
        {
            ArgumentWriter writer = new ArgumentWriter();
            int uploaderId = this.OnUploadFileWriteSegment(reader.ReadInt(), reader.ReadBytes());

            writer.Write(uploaderId);
            this.WriteRequestOrReply(writer, RequestType.UploadFileAckSegment);

            return writer;
        }

        private ArgumentWriter UploadFileAckSegment(ArgumentReader reader)
        {
            this.mFileId = reader.ReadInt();
            return null;
        }

        private ArgumentWriter UploadFileClose(ArgumentReader reader)
        {
            ArgumentWriter writer = new ArgumentWriter();
            int uploaderId = this.OnUploadFileClose(reader.ReadInt());

            writer.Write(uploaderId);
            this.WriteRequestOrReply(writer, RequestType.UploadFileComplete);

            return writer;
        }

        private ArgumentWriter UploadFileComplete(ArgumentReader reader)
        {
            this.mFileId = reader.ReadInt();
            return null;
        }

        private ArgumentWriter ListProcessRequest(ArgumentReader reader)
        {
            IList<ProcessDescription> processDescriptions = this.OnListProcesses();
            ArgumentWriter writer = new ArgumentWriter();

            writer.Write(processDescriptions.Count);
            foreach (ProcessDescription description in processDescriptions)
            {
                writer.Write(description.ProcessId);
                writer.Write(description.Name);
                writer.Write(description.Args);
            }

            this.WriteRequestOrReply(writer, RequestType.ListProcessReply);
            return writer;
        }

        private ArgumentWriter ListProcessReply(ArgumentReader reader)
        {
            int count = reader.ReadInt();

            for (int i = 0; i < count; i++)
            {
                int processId = reader.ReadInt();
                string name = reader.ReadString();
                string args = reader.ReadString();

                this.mProcessList.Add(new ProcessDescription(processId, name, args));
            }

            return null;
        }

        private ArgumentWriter ForwardCustomRequest(ArgumentReader reader)
        {
            int processId = reader.ReadInt();
            int messageType = reader.ReadInt();

            byte[] message = reader.ReadBytes();
            byte[] replyMessage = null;
            CustomMessageReply replyType = this.OnCustomMessage(processId, messageType, message, out replyMessage);

            ArgumentWriter writer = new ArgumentWriter();

            if (replyMessage != null)
            {
                writer.Write((int)replyType);
                writer.Write(replyMessage);
            }
            else
            {
                writer.Write((int)replyType);
                writer.Write(new byte[] { });
            }

            this.WriteRequestOrReply(writer, RequestType.ForwardCustomReply);
            return writer;
        }

        private ArgumentWriter ForwardCustomReply(ArgumentReader reader)
        {
            this.mCustomMessageReplyType = (CustomMessageReply)reader.ReadInt();
            this.mCustomMessageReply = reader.ReadBytes();

            return null;
        }

        private ArgumentWriter GetMachineStatusRequest(ArgumentReader reader)
        {
            byte[] message = reader.ReadBytes();
            byte[] replyMessage = null;
            CustomMessageReply replyType = this.OnGetMachineStatus(message, out replyMessage);

            ArgumentWriter writer = new ArgumentWriter();

            if (replyMessage != null)
            {
                writer.Write((int)replyType);
                writer.Write(replyMessage);
            }
            else
            {
                writer.Write((int)replyType);
                writer.Write(new byte[] { });
            }

            this.WriteRequestOrReply(writer, RequestType.GetMachineStatusReply);
            return writer;
        }

        private ArgumentWriter GetMachineStatusReply(ArgumentReader reader)
        {
            this.mCustomMessageReplyType = (CustomMessageReply)reader.ReadInt();
            this.mCustomMessageReply = reader.ReadBytes();

            return null;
        }
    }
}
