﻿//-----------------------------------------------------------------------
// <copyright file="GermService.cs" company="National ICT Australia Ltd">
//     Copyright (c) 2012 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace GermServer
{
    using System;
    using System.Diagnostics;
    using System.IO;
    using System.Net;
    using System.Net.Security;
    using System.Net.Sockets;
    using System.Security.Authentication;
    using System.Security.Cryptography.X509Certificates;
    using System.ServiceProcess;

    /// <summary>
    /// Germ service class responsible for forming an initial connection to or from Control center.
    /// </summary>
    public partial class GermService : ServiceBase
    {        
        /// <summary>
        /// A default port number in which this germ service should listen on.
        /// </summary>
        private int port = 21253;

        /// <summary>
        /// Control center address.
        /// </summary>
        private string controlCenterAddress;

        /// <summary>
        /// Tcp listener.
        /// </summary>
        private TcpListener listener;

        /// <summary>
        /// Germ server protocol.
        /// </summary>
        private ServerProtocol protocol;

        /// <summary>
        /// Initializes a new instance of the <see cref="GermService"/> class.
        /// </summary>
        public GermService()
        {
            InitializeComponent();
            this.protocol = new ServerProtocol();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GermService"/> class.
        /// It should listen on the given port number.
        /// </summary>
        /// <param name="port">Port number where this service should listen on.</param>
        public GermService(int port)
            : this()
        {
            this.port = port;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GermService"/> class.
        /// </summary>
        /// <param name="port">Port number where this service should listen on.</param>
        /// <param name="controlCenterAddress">Control center address.</param>
        public GermService(int port, string controlCenterAddress)
            : this(port)
        {
            this.controlCenterAddress = controlCenterAddress;
        }

        /// <summary>
        /// Starting the germ service.
        /// </summary>
        public void Start()
        {
            this.OnStart(new string[] { });
        }

        /// <summary>
        /// Start the tcp listener to begin listen for any tcp client.
        /// When the control center address is not null, germ service should try to contact a given Control center first.
        /// </summary>
        /// <param name="args">Arguments array (currently is not used).</param>
        protected override void OnStart(string[] args)
        {
            // Germ shouldn't start the listener when it ask to connect automatically to given control center.
            if (!string.IsNullOrEmpty(this.controlCenterAddress))
            {
                TcpClient client = null; ;

                try
                {
                    Console.WriteLine("Connecting to Control Center with address '{0}' ...", this.controlCenterAddress);

                    // connecting to given control center address first.
                    client = new TcpClient(new IPEndPoint(IPAddress.Any, this.port));
                    client.Connect(this.controlCenterAddress.Split(':')[0], int.Parse(this.controlCenterAddress.Split(':')[1]));
                    
                    SslStream secureStream = new SslStream(client.GetStream(), false, ValidateClientCertificate);
                    secureStream.AuthenticateAsClient(client.Client.RemoteEndPoint.ToString(), null, SslProtocols.Tls, false);

                    if (null != client && client.Connected && secureStream.IsAuthenticated)
                    {
                        try
                        {
                            secureStream.Write(new byte[] { 0 });
                            this.protocol.BeginListenThread(secureStream, client.Client.RemoteEndPoint as IPEndPoint);
                        }
                        catch (Exception e)
                        {
                            Trace.TraceError("Failed to begin listening from secure stream : {0}", e.Message);
                        }
                    }
                }
                catch (SocketException e)
                {
                    Trace.TraceError("Failed to connect : {0}", e.Message);
                    Console.WriteLine("Failed to connect : {0}", e.Message);
                    
                    if (client != null)
                    {
                        client.Close();
                    }

                    this.StartListening();
                }
            }
            else
            {
                this.StartListening();
            }
        }

        /// <summary>
        /// Stop the tcp listener.
        /// </summary>
        protected override void OnStop()
        {
            if (this.listener != null)
            {
                this.listener.Stop();
            }
        }

        /// <summary>
        /// Validate the client certificate.
        /// </summary>
        /// <param name="sender">Sender object.</param>
        /// <param name="certificate">Certificate that need to be validated.</param>
        /// <param name="chain">X509 chain for X509 certificate.</param>
        /// <param name="sslPolicyErrors">Ssl policy errors enumeration.</param>
        /// <returns>Return true if the certificate is valid.</returns>
        private static bool ValidateClientCertificate(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            string clientPublicKey = certificate.GetPublicKeyString();
            string publicKeyFilename = "publicKey.key";
            if (!File.Exists(publicKeyFilename))
            {
                throw new ArgumentException(String.Format("Cannot find file {0}", publicKeyFilename));
            }

            // Read the private key from the given file name.
            string publicKeyContents = File.ReadAllText(publicKeyFilename);

            if (string.IsNullOrEmpty(publicKeyContents))
            {
                throw new InvalidDataException(String.Format("Failed to read public key from {0}", publicKeyFilename));
            }
            else if (publicKeyContents.Equals(clientPublicKey))
            {
                //// RemoteCertificateChainErrors and RemoteCertificateNameMismatch will occurs since the certificate on the server side
                //// is a self signed certificate.
                //// http://social.msdn.microsoft.com/forums/en-US/netfxnetcom/thread/8aeadd67-16ba-40e0-b9d6-262e6e7f5a94
                //// Refering from this forum, how to ignore those two ssl policy errors

                if (sslPolicyErrors == SslPolicyErrors.None)
                {
                    return true;
                }

                //// If a certificate is a self signed certificate or if certificate name does not match, ignore it.
                //// Since this will always the case for the control center. (The germs will need to validate just the
                //// public key of the certificate).
                if (((sslPolicyErrors & SslPolicyErrors.RemoteCertificateNameMismatch) == SslPolicyErrors.RemoteCertificateNameMismatch) ||
                    ((sslPolicyErrors & SslPolicyErrors.RemoteCertificateChainErrors) == SslPolicyErrors.RemoteCertificateChainErrors
                    && chain.ChainStatus.Length == 1 && certificate.Subject == certificate.Issuer
                    && chain.ChainStatus[0].Status == X509ChainStatusFlags.UntrustedRoot))
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// ReceivedCallback will be called when there is an incoming connection from Control Center.
        /// </summary>
        /// <param name="ar">Asynchronise result.</param>
        private static void ReceiveCallback(IAsyncResult ar)
        {
            GermService listener = (GermService)ar.AsyncState;
            TcpClient client = null;
            SslStream secureStream = null;

            if (listener == null)
            {
                return;
            }

            try
            {
                client = listener.listener.EndAcceptTcpClient(ar);
                secureStream = new SslStream(client.GetStream(), false, ValidateClientCertificate);
                secureStream.AuthenticateAsClient(client.Client.RemoteEndPoint.ToString(), null, SslProtocols.Tls, false);
            }
            catch (Exception e)
            {
                Trace.TraceError("Failed to authenticate secure stream : {0}", e.Message);
            }

            if (null != client && client.Connected && secureStream.IsAuthenticated)
            {
                try
                {
                    //// Note: workaround from the Mono bug 457120, it should be removed when the bug is fixed.
                    //// https://bugzilla.novell.com/show_bug.cgi?id=457120
                    secureStream.Write(new byte[] { 0 });
                    listener.protocol.BeginListenThread(secureStream, client.Client.RemoteEndPoint as IPEndPoint);
                }
                catch (Exception e)
                {
                    Trace.TraceError("Failed to begin listening from secure stream : {0}", e.Message);
                }
            }

            listener.listener.BeginAcceptTcpClient(GermService.ReceiveCallback, listener);
        }

        /// <summary>
        /// Start the tcp listener and listening on the given port.
        /// </summary>
        private void StartListening()
        {
            try
            {
                Console.WriteLine("Germ listening in port {0}", this.port);
                this.listener = new TcpListener(IPAddress.Any, this.port);
                this.listener.Start(1);
                this.listener.BeginAcceptTcpClient(GermService.ReceiveCallback, this);
            }
            catch (SocketException e)
            {
                Trace.TraceError("Failed to start listening : {0}", e.Message);
                Console.WriteLine("Failed to start listening : {0}", e.Message);
            }
        }
    }
}
