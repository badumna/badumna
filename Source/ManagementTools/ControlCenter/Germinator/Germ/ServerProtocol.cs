﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Diagnostics;


namespace GermServer
{
    class ServerProtocol : Protocol
    {
        private Germ mGerm;
        private Dictionary<int, Uploader> mUploaders;

        private static int mNextUploaderId;

        public ServerProtocol()
        {
            this.mGerm = new Germ();
            this.mUploaders = new Dictionary<int, Uploader>();
        }

        protected override void OnClose()
        {
        }

        protected override int OnStartProcessRequest(string name, string directory, string executable, string arguments)
        {
            return this.mGerm.StartApplication(name, directory, executable, arguments);
        }

        protected override int OnKillProcessRequest(int processId)
        {
            if (this.mGerm.KillApplication(processId))
            {
                return processId;
            }
            return -1;
        }

        protected override IList<ProcessDescription> OnListProcesses()
        {
            return this.mGerm.GetRunningApplications();
        }

        protected override int OnUploadFileRequest(string directory, string file, string hash)
        {
            Uploader uploader = this.mGerm.Upload(directory, file, hash);
            int uploaderId = ServerProtocol.mNextUploaderId++;
            if (ServerProtocol.mNextUploaderId == int.MaxValue)
            {
                ServerProtocol.mNextUploaderId = 0;
            }

            if (uploader != null)
            {
                this.mUploaders.Add(uploaderId, uploader);
                return uploaderId;
            }

            return -1;
        }

        protected override int OnUploadFileWriteSegment(int uploaderId, byte[] segment)
        {
            Uploader uploader = null;
            if (this.mUploaders.TryGetValue(uploaderId, out uploader))
            {
                uploader.WriteSegment(segment);
            }
            else
            {
                uploaderId = -1;
            }

            return uploaderId;
        }

        protected override int OnUploadFileClose(int uploaderId)
        {
            Uploader uploader = null;
            if (!this.mUploaders.TryGetValue(uploaderId, out uploader) || !uploader.Close())
            {
                uploaderId = -1;
            }

            return uploaderId;
        }

        protected override CustomMessageReply OnCustomMessage(int processId, int messageType, byte[] message, out byte[] reply)
        {
            CustomMessageReply replyType = this.mGerm.ForwardMessage(processId, messageType, message, out reply);
            if (replyType == CustomMessageReply.OkWithData)
            {
                if (reply == null)
                {
                    return CustomMessageReply.OkWithoutData;
                }

                return CustomMessageReply.OkWithData;
            }

            return replyType;
        }

        protected override CustomMessageReply OnGetMachineStatus(byte[] message, out byte[] reply)
        {
            CustomMessageReply replyType = this.mGerm.GetMachineStatus(message, out reply);
            if (replyType == CustomMessageReply.OkWithData)
            {
                if (reply == null)
                {
                    return CustomMessageReply.OkWithoutData;
                }

                return CustomMessageReply.OkWithData;
            }

            return replyType;
        }
    }
}
