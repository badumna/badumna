﻿namespace GermServer
{
    partial class ProjectInstaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.GermServiceProcessInstaller = new System.ServiceProcess.ServiceProcessInstaller();
            this.GermServiceInstaller = new System.ServiceProcess.ServiceInstaller();
            // 
            // GermServiceProcessInstaller
            // 
            this.GermServiceProcessInstaller.Password = null;
            this.GermServiceProcessInstaller.Username = null;
            // 
            // GermServiceInstaller
            // 
            this.GermServiceInstaller.DisplayName = "GermInstaller";
            this.GermServiceInstaller.ServiceName = "GermService";
            // 
            // ProjectInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.GermServiceProcessInstaller,
            this.GermServiceInstaller});

        }

        #endregion

        private System.ServiceProcess.ServiceProcessInstaller GermServiceProcessInstaller;
        private System.ServiceProcess.ServiceInstaller GermServiceInstaller;
    }
}