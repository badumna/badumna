﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.IO;
using System.Diagnostics;

namespace GermServer
{
    class Uploader
    {
        public bool IsOutOfDate { get; internal set; }

        private FileStream mFileStream;
        private String mFileHash;
        private String mAbsolutePath;
        private readonly string mDirectory = AppDomain.CurrentDomain.BaseDirectory;

        public Uploader()
        {
        }

        private static bool IsFileOutOfDate(string relativeDirectory, string fileName, string hash)
        {
            string directory = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, relativeDirectory);
            string absoultePath = Path.Combine(directory, Path.GetFileName(fileName));

            if (File.Exists(absoultePath))
            {
                string currentHash = Germ.GetHashOfFile(absoultePath);
                if (currentHash == hash)
                {
                    Trace.TraceInformation("File {0} is up to date", fileName);
                    return false;
                }

                Trace.TraceInformation("File {0} is out of date ({1} vs {2})", fileName, currentHash, hash);
            }
            else
            {
                Trace.TraceInformation("File {0} does not exist", absoultePath);
            }

            return true;
        }

        internal bool Open(string relativeDirectory, string fileName, string hash)
        {
            relativeDirectory = relativeDirectory.Replace('/', Path.DirectorySeparatorChar).Replace('\\', Path.DirectorySeparatorChar);
            this.IsOutOfDate = Uploader.IsFileOutOfDate(relativeDirectory, fileName, hash);
            if (!this.IsOutOfDate)
            {
                return false;
            }

            string directory = Path.Combine(this.mDirectory, relativeDirectory);
            this.mAbsolutePath = Path.Combine(directory, Path.GetFileName(fileName));

            foreach (char invalidChar in Path.GetInvalidPathChars())
            {
                this.mAbsolutePath = this.mAbsolutePath.Replace(invalidChar, '_');
            }

            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }

            Trace.TraceInformation("  Uploading {0} to {1}", fileName, directory);

            this.mFileHash = hash;
            this.mFileStream = new FileStream(this.mAbsolutePath, FileMode.Create, FileAccess.Write);
            return true;
        }

        public void WriteSegment(byte[] segment)
        {
            if (this.mFileStream != null)
            {
                this.mFileStream.Write(segment, 0, segment.Length);
            }
        }

        public bool Close()
        {
            if (this.mFileStream != null)
            {
                Trace.TraceInformation("  Upload of {0} complete", this.mAbsolutePath);
                this.mFileStream.Close();
                this.mFileStream = null;
            }

            if (File.Exists(this.mAbsolutePath))
            {
                if (Germ.GetHashOfFile(this.mAbsolutePath) != this.mFileHash)
                {
                    File.Delete(this.mAbsolutePath);
                    return false;
                }
            }
            else
            {
                return false;
            }

            return true;
        }

    }
}
