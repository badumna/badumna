﻿using System;
using System.Collections.Generic;
using System.Text;

using Germinator.Components;
using Germinator.Services;

namespace Dei
{
    public class DeiService : BaseService
    {
        [ComponentProperty("Time (in hours) that the public key is valid for. (Not Active)")]
        public int PublicKeyValidityPeriod { get; set; }

        [ComponentProperty("Time (in hours) that the participation key is valid for. (Not Active)")]
        public int ParticipationValidityPeriod { get; set; }

        [ComponentProperty("The port the dei server listens on")]
        public int Port { get { return this.mPort; } set { this.SetPort(value); } }

        private static readonly string[] PackageFlavours = new string[] { "SQLite" }; // Will need to change the PackageFlavour attibute too if changed.
        [ComponentProperty("Account storage type", "SQLite")]
        public string PackageFlavour
        {
            get { return this.mPackageFlavour; }
            set { this.SetPackageFlavour(value); }
        }

        private string mPackageFlavour = DeiService.PackageFlavours[0];
        private int mPort;

        public DeiService()
            : base("Dei", "Dei account management")
        {
            this.SetPort(21260); // default port
            this.MaximumNumberOfInstances = 1;
            this.PublicKeyValidityPeriod = (int)TimeSpan.FromDays(30).TotalHours;
            this.ParticipationValidityPeriod = (int)TimeSpan.FromDays(1).TotalHours;
        }

        private void SetPackageFlavour(string flavour)
        {
            bool isValid = false;
            foreach (string knownFlavour in DeiService.PackageFlavours)
            {
                if (knownFlavour == flavour)
                {
                    isValid = true;
                    break;
                }
            }

            if (!isValid)
            {
                return;
            }

            this.mPackageFlavour = flavour;
            this.PackageIndex = String.Format("Dei-{0}", flavour);
        }

        private void SetPort(int port)
        {
            this.mPort = port;
            this.AppendProcessArgument(string.Format("port:{0}", port));
        }
    }
}
