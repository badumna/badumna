﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Germinator.Services;

namespace Tunnel
{
    public class TunnelService : PublicPeerService
    {
        public TunnelService()
            : base("Tunnel", "Tunnel")
        {
        }
    }
}
