﻿using System;
using System.Collections.Generic;
using System.Text;

using Germinator.Components;
using Germinator.Services;

namespace UnityComponents
{
    public class UnityService : PublicPeerService
    {
        public UnityService()
            : base("UnityTest", "Unity Testing")
        {
        }
    }
}
