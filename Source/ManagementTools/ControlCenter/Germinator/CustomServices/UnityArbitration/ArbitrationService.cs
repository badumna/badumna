﻿using System;
using System.Collections.Generic;
using System.Text;

using Germinator.Components;
using Germinator.Services;

namespace UnityArbitration
{
    public class ArbitrationService : PublicPeerService
    {
        public ArbitrationService()
            : base("UnityArbitration", "Unity Arbitration")
        {
        }
    }
}
