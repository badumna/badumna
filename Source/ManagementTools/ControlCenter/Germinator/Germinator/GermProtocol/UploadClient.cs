﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Threading;
using System.Net.Sockets;
using System.Security.Cryptography;

namespace Germinator
{
    class UploadClient
    {
        public delegate void UploadProgressDelegate(long bytesSent, long bytesTotal);

        private ClientProtocol mClient;
        private const int mSegmentSize = 8096;

        public UploadClient(ClientProtocol client)
        {
            this.mClient = client;
        }

        public void UploadFile(String destinationDirectory, String file, UploadProgressDelegate progressDelegate)
        {
            Dictionary<String, String> files = new Dictionary<string, string>();

            files.Add(file, "");
            this.UploadFiles(destinationDirectory, files, progressDelegate);
        }

        public void UploadFiles(string destinationDirectory, Dictionary<String, String> files, UploadProgressDelegate progressDelegate)
        {
            long totalLength = 0;
            foreach (String file in files.Keys)
            {
                if (File.Exists(file))
                {
                    totalLength += new FileInfo(file).Length;
                }
            }

            long bytesSent = 0;
            foreach (KeyValuePair<String, String> pair in files)
            {
                string file = pair.Key;
                string relativeDirectory = pair.Value;

                if (string.IsNullOrEmpty(file))
                {
                    continue;
                }

                if (File.Exists(file))
                {
                    try
                    {
                        String hash = UploadClient.GetHashOfFile(file);
                        int uploadId = this.mClient.SendOpenFile(Path.Combine(destinationDirectory, relativeDirectory), Path.GetFileName(file), hash);

                        if (uploadId >= 0) // negative indicates failure or the file is already up to date
                        {
                            using (FileStream fileStream = new FileStream(file, FileMode.Open, FileAccess.Read, FileShare.Read))
                            {
                                byte[] buffer = new byte[UploadClient.mSegmentSize];
                                int length = 0;

                                while ((length = fileStream.Read(buffer, 0, UploadClient.mSegmentSize)) != 0)
                                {
                                    if (!this.mClient.SendFileSegment(uploadId, buffer, length))
                                    {
                                        break;
                                    }
                                    bytesSent += length;
                                    if (progressDelegate != null)
                                    {
                                        progressDelegate(bytesSent, totalLength);
                                    }
                                }
                            }
                            this.mClient.SendCloseFile(uploadId);
                        }
                        else
                        {
                            totalLength -= new FileInfo(file).Length;
                            if (totalLength > 0 && bytesSent == totalLength)
                            {
                                progressDelegate(totalLength, totalLength); // 100 % 
                            }
                        }
                    }
                    catch (IOException)
                    {
                        DebugConsole.Instance.EnqueueTraceInfo(string.Format("Failed to upload file {1}", Path.GetFileName(file)), System.Diagnostics.TraceLevel.Error);
                    }
                    catch (SocketException)
                    {
                        DebugConsole.Instance.EnqueueTraceInfo(string.Format("Failed to upload file {1}", Path.GetFileName(file)), System.Diagnostics.TraceLevel.Error);
                    }
                }
                else
                {
                    DebugConsole.Instance.EnqueueTraceInfo(string.Format("Cannot upload file {0} because it does not exist", Path.GetFileName(file)), System.Diagnostics.TraceLevel.Warning);
                }
            }

            if (totalLength == 0 || bytesSent != totalLength)
            {
                progressDelegate(totalLength, totalLength); // 100 % 
            }
        }



        public static String GetHashOfFile(String fileName)
        {
            SHA1 sha = new SHA1Managed();
            byte[] result;

            sha.Initialize();

            try
            {
                result = sha.ComputeHash(File.ReadAllBytes(fileName));
            }
            catch (IOException)
            {
                return String.Empty;
            }

            int length = result.Length * 2;
            StringBuilder builder = new StringBuilder(length);

            foreach (byte b in result)
            {
                builder.AppendFormat("{0:X2}", b);
            }

            return builder.ToString(0, length);
        }
    }
}
