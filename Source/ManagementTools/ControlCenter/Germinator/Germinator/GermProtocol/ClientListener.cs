﻿//-----------------------------------------------------------------------
// <copyright file="ClientListener.cs" company="National ICT Australia Ltd">
//     Copyright (c) 2012 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Germinator.GermProtocol
{
    using System;
    using System.Net;
    using System.Net.Security;
    using System.Net.Sockets;
    using System.Security.Authentication;
    using System.Security.Cryptography.X509Certificates;

    /// <summary>
    /// ClientListener class responsible to listen on a specific port for an join request from any remote germs.
    /// This would be used to implements the new functionality in CC to add the remote germ automatically.
    /// </summary>
    internal class ClientListener
    {                
        /// <summary>
        /// A default port number in which this class should listen on.
        /// </summary>
        private int port = 21253;

        /// <summary>
        /// Tcp listener.
        /// </summary>
        private TcpListener listener;

        /// <summary>
        /// X509 certificate use on ssl connection.
        /// </summary>
        private X509Certificate2 certificate;

        /// <summary>
        /// Add germ to the configuration list.
        /// </summary>
        private AddGermClient addGerm;

        /// <summary>
        /// A value indicating whether the tcp listener is currently listening on. 
        /// </summary>
        private bool isListening;
        
        /// <summary>
        /// Initializes a new instance of the <see cref="ClientListener"/> class.
        /// Default constructor.
        /// </summary>
        /// <param name="certificate">X509 certificate use for ssl connection.</param>
        public ClientListener(X509Certificate2 certificate)
        {
            this.certificate = certificate;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ClientListener"/> class.
        /// </summary>
        /// <param name="certificate">X509 certificate use for ssl connection.</param>
        /// <param name="port">Port number in which this class should listen on.</param>
        public ClientListener(X509Certificate2 certificate, int port)
            : this(certificate)
        {
            this.port = port;
        }
        
        /// <summary>
        /// Add germ client delegate.
        /// </summary>
        /// <param name="client">Germ client.</param>
        public delegate void AddGermClient(GermClient client);

        /// <summary>
        /// Gets or sets the add germ delegate.
        /// </summary>
        public AddGermClient AddGerm
        {
            get
            {
                return this.addGerm;
            }

            set
            {
                this.addGerm = value;
            }
        }

        /// <summary>
        /// Gets or sets the client listener port number.
        /// </summary>
        public int Port
        {
            get
            {
                return this.port;
            }

            set
            {
                this.port = value;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the tcp listener is currently listening on.
        /// </summary>
        public bool IsListening
        {
            get
            {
                return this.isListening;
            }
        }

        /// <summary>
        /// Start the client listener.
        /// </summary>
        public void Start()
        {
            try
            {
                this.listener = new TcpListener(IPAddress.Any, this.port);
                this.listener.Start();
                this.listener.BeginAcceptTcpClient(this.ReceiveCallback, null);
                this.isListening = true;
            }
            catch (SocketException e)
            {
                DebugConsole.Instance.EnqueueTraceInfo(e.Message);
            }
        }

        /// <summary>
        /// Restart the client listener.
        /// </summary>
        public void Restart()
        {
            if (this.isListening)
            {
                this.Stop();
            }

            this.Start();
        }

        /// <summary>
        /// Stop the client listener.
        /// </summary>
        public void Stop()
        {
            this.listener.Stop();
            this.isListening = false;
        }

        /// <summary>
        /// Received callback async handler should be called when there is a remote germ try to connect.
        /// </summary>
        /// <param name="ar">Asynchronise result.</param>
        private void ReceiveCallback(IAsyncResult ar)
        {
            TcpClient client = null;
            SslStream secureStream = null;

            try
            {
                if (this.isListening)
                {
                    client = this.listener.EndAcceptTcpClient(ar);
                    secureStream = new SslStream(client.GetStream(), false);
                    secureStream.AuthenticateAsServer(this.certificate, false, SslProtocols.Tls, false);

                    string remoteHost = client.Client.RemoteEndPoint.ToString();
                    ClientProtocol clientProtocol = new ClientProtocol(this.certificate, client, secureStream);
                    clientProtocol.Initialize(remoteHost);

                    this.listener.BeginAcceptTcpClient(this.ReceiveCallback, null);

                    GermClient germClient = new GermClient(remoteHost, this.certificate, clientProtocol);

                    if (this.addGerm != null)
                    {
                        this.addGerm.Invoke(germClient);
                    }
                }
            }
            catch (Exception e)
            {
                DebugConsole.Instance.EnqueueTraceInfo(e.Message);
            }
        }
    }
}
