﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;

using GermServer;
namespace Germinator
{
    public class ExecuteOperation : Operation
    {
        public ProcessDescription ProcessDescription { get; private set; }

        private GermClient mGermClient;
        private string mName;
        private string mDirectory;
        private string mExecutable;
        private string mArguments;

        internal ExecuteOperation(GermClient client, string name, string directory, string executable, string arguments, AsyncCallback callback, object callbackState)
            : base(null, callback, callbackState)
        {
            this.mGermClient = client;
            this.mName = name;
            this.mDirectory = directory;
            this.mExecutable = executable;
            this.mArguments = arguments;
        }

        internal override void Perform()
        {
            ProcessDescription description = this.mGermClient.Execute(this.mName, this.mDirectory, this.mExecutable, this.mArguments);

            if (description != null)
            {
                this.ProcessDescription = description;
                base.Perform();
            }
            else
            {
                this.Fail(String.Format("Cannot start {0}", this.mExecutable));
            }
        }

        protected override void OnFailure(string reason)
        {
            DebugConsole.Instance.EnqueueTraceInfo(string.Format("Failed to start {1} process : {0}", reason, this.mName), System.Diagnostics.TraceLevel.Error);
        }

    }
}
