﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Diagnostics;

namespace Germinator
{
    public class Operation : IAsyncResult
    {
        public delegate void OperationDelegate();

        #region IAsyncResult Members

        private object mAsyncState;
        public object AsyncState { get { return this.mAsyncState; } }

        private ManualResetEvent mWaitHandle = new ManualResetEvent(false);
        public WaitHandle AsyncWaitHandle { get { return this.mWaitHandle; } }

        public bool CompletedSynchronously { get { return false; } }

        protected bool mIsCompleted;
        public bool IsCompleted { get { return this.mIsCompleted; } }

        #endregion

        internal OperationDelegate OperationTask { private get; set; }

        private AsyncCallback mCallback;

        internal Operation(OperationDelegate operation, AsyncCallback callback, object callbackState)
        {
            this.OperationTask = operation;
            this.mCallback = callback;
            this.mAsyncState = callbackState;
        }

        internal virtual void Perform()
        {
            if (this.OperationTask != null)
            {
                this.OperationTask();
            }
            
            this.mIsCompleted = true;
            
            if (this.mCallback != null)
            {
                this.mCallback(this);
            }
            this.mWaitHandle.Set();
        }

        internal void Fail(string reason)
        {
            this.OnFailure(reason);
            this.mIsCompleted = false;
            if (this.mCallback != null)
            {
                this.mCallback(this);
            }
            this.mWaitHandle.Set();
        }

        protected virtual void OnFailure(string reason)
        {
            Trace.TraceError(String.Format("An asynchronous germ operation has failed : {0}", reason));
            DebugConsole.Instance.EnqueueTraceInfo(string.Format("An asynchronous germ operation has failed : {0}", reason));
        }
    }
}
