﻿//-----------------------------------------------------------------------
// <copyright file="CustomMessageOperation.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Germinator
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Text;

    /// <summary>
    /// CustomMessageOperation class stored the custom message response.
    /// </summary>
    public class CustomMessageOperation : Operation
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CustomMessageOperation"/> class.
        /// </summary>
        /// <param name="operation">Operation delegate.</param>
        /// <param name="callback">Callback method.</param>
        /// <param name="callbackState">Callback state.</param>
        internal CustomMessageOperation(OperationDelegate operation, AsyncCallback callback, object callbackState)
            : base(operation, callback, callbackState)
        {
        }

        /// <summary>
        /// Gets or sets the custom message response.
        /// </summary>
        public MemoryStream Response { get; set; }

        /// <summary>
        /// Gets the message type.
        /// </summary>
        public int MessageType { get; internal set; }

        /// <summary>
        /// Gets the process id.
        /// </summary>
        public int ProcessId { get; internal set; }

        /// <summary>
        /// Gets a value indicating whether the custom message operation was successful.
        /// </summary>
        public bool WasSuccessful { get; internal set; }

        /// <summary>
        /// Gets a value indicating whether the custom message operation is forwarded to unharness process.
        /// </summary>
        public bool IsUnharnessed { get; internal set; }

        /// <summary>
        /// Override on operation failure callback method.
        /// </summary>
        /// <param name="reason">Failure reason.</param>
        protected override void OnFailure(string reason)
        {
            DebugConsole.Instance.EnqueueTraceInfo(string.Format("Failed to forward message to process : {0}", reason), System.Diagnostics.TraceLevel.Error);
        }
    }
}
