﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;

namespace Germinator
{
    public class UploadProgressEventArgs : EventArgs
    {
        private long mBytesSent;
        private long mBytesTotal;

        public long BytesSent { get { return this.mBytesSent; } }
        public long BytesTotal { get { return this.mBytesTotal; } }
        public int Percentage { get { return this.mBytesTotal == 0 ? 100 : (int)(this.mBytesSent / this.mBytesTotal) * 100; } }

        internal UploadProgressEventArgs(long bytesSent, long bytesTotal)
        {
            this.mBytesSent = bytesSent;
            this.mBytesTotal = bytesTotal;
        }
    }

    public class UploadOperation : Operation
    {
        public event EventHandler<UploadProgressEventArgs> ProgressChangedEvent;

        private string mDestinationDirectory;
        private Dictionary<String, String> mSourceDestinationPairs;
        private GermClient mGermClient;

        internal UploadOperation(string destinationDirectory, Dictionary<String, String> sourceDestinationPairs, GermClient client, AsyncCallback callback, object callbackState)
            : base(null, callback, callbackState)
        {
            this.mGermClient = client;
            this.mDestinationDirectory = destinationDirectory;
            this.mSourceDestinationPairs = sourceDestinationPairs;
        }

        internal override void Perform()
        {
            UploadClient uploadClient = this.mGermClient.MakeUploadClient();

            uploadClient.UploadFiles(this.mDestinationDirectory, this.mSourceDestinationPairs, this.ProgressChangedHandler);
            base.Perform();
        }

        internal void ProgressChangedHandler(long bytesSent, long bytesTotal)
        {
            if (this.ProgressChangedEvent != null)
            {
                this.ProgressChangedEvent(this, new UploadProgressEventArgs(bytesSent, bytesTotal));
            }
        }

        protected override void OnFailure(string reason)
        {
            DebugConsole.Instance.EnqueueTraceInfo(string.Format("Failed to upload the following files to germ : {0}", reason), System.Diagnostics.TraceLevel.Error);
            foreach (string file in this.mSourceDestinationPairs.Keys)
            {
                DebugConsole.Instance.EnqueueTraceInfo(string.Format("    {0}", System.IO.Path.GetFileName(file)), System.Diagnostics.TraceLevel.Error);
            }
        }
    }
}
