﻿using System;
using System.Collections.Generic;
using System.Text;

using GermServer;
namespace Germinator
{
    public class ProcessListOperation : Operation
    {
        public List<ProcessDescription> RunningProcesses { get; private set; }
        private GermClient mGermClient;        

        internal ProcessListOperation(GermClient client, AsyncCallback callback, object callbackState)
            : base(null, callback, callbackState)
        {
            this.mGermClient = client;
        }

        internal override void Perform()
        {
            this.RunningProcesses = this.mGermClient.GetProcessList();
            if (this.RunningProcesses == null)
            {
                this.RunningProcesses = new List<ProcessDescription>();
            }

            base.Perform();
        }

        protected override void OnFailure(string reason)
        {
            DebugConsole.Instance.EnqueueTraceInfo(string.Format("Failed to list germs processes : {0}", reason), System.Diagnostics.TraceLevel.Error);
        }
    }
}
