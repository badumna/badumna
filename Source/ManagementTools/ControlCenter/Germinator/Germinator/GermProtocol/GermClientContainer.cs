﻿//---------------------------------------------------------------------------------
// <copyright file="GermClientContainer.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2010 NICTA Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Germinator
{
    /// <summary>
    /// A class that store a collections of germ clients.
    /// </summary>
    internal class GermClientContainer
    {
        /// <summary>
        /// Object lock.
        /// </summary>
        private static object loadLock = new object();

        /// <summary>
        /// Germ client collection.
        /// </summary>
        private Dictionary<string, GermClient> germs = new Dictionary<string, GermClient>();

        /// <summary>
        /// Initializes a new instance of the <see cref="GermClientContainer"/> class.
        /// </summary>
        public GermClientContainer()
        {
        }

        /// <summary>
        /// Gets or sets X509 Certificate use in secure connection with the remote germs.
        /// </summary>
        public static X509Certificate2 Certificate { get; set; }

        /// <summary>
        /// Gets the numbers of germ clients currently on the class.
        /// </summary>
        public int Count
        {
            get { return this.germs.Count; }
        }

        /// <summary>
        /// Gets the enumerable of germs host name.
        /// </summary>
        public IEnumerable<string> Hosts
        {
            get { return this.germs.Keys; }
        }

        /// <summary>
        /// Return the collection of germ clients as enumerable object.
        /// </summary>
        /// <returns>Return the enumerable object of germ clients.</returns>
        public IEnumerable<GermClient> AsEnumerable()
        {
            return this.germs.Values;
        }

        /// <summary>
        /// Add a germ given the germ host name to the container.
        /// </summary>
        /// <param name="hostAndPort">Germ host name.</param>
        /// <returns>Return the germ client correspond to the new germ.</returns>
        public GermClient AddGerm(string hostAndPort)
        {
            GermClient client = new GermClient(hostAndPort, GermClientContainer.Certificate);

            this.AddGerm(client);

            return client;
        }

        /// <summary>
        /// Add a germ given a host name and the port number
        /// </summary>
        /// <param name="host">The host name (e.g. 127.0.0.1)</param>
        /// <param name="port">The port number.</param>
        /// <returns>Return the germ client correspond to the new germ.</returns>
        public GermClient AddGerm(string host, int port)
        {
            return this.AddGerm(string.Format("{0}:{1}", host, port));
        }

        /// <summary>
        /// Add a germ given the germ client instance.
        /// </summary>
        /// <param name="client">Germ client instance.</param>
        public void AddGerm(GermClient client)
        {
            if (!this.germs.ContainsKey(client.Name))
            {
                this.germs.Add(client.Name, client);
            }
            else
            {
                // Need to update the client with the new one.
                // Need this for remote germ to reconnect after lost connection.
                GermClient oldClient = this.germs[client.Name];
                oldClient.Client = client.Client;
            }
        }

        /// <summary>
        /// Check whether the germ hostname is exist on the collections.
        /// </summary>
        /// <param name="name">Germ host name.</param>
        /// <returns>Return true on success.</returns>
        public bool Contains(string name)
        {
            return this.germs.ContainsKey(name);
        }

        /// <summary>
        /// Get the germ client given the germ host name.
        /// </summary>
        /// <param name="name">Germ host name.</param>
        /// <returns>Return the germ client given the germ host name exist.</returns>
        public GermClient GetGerm(string name)
        {
            GermClient client = null;
            if (this.germs.TryGetValue(name, out client))
            {
                return client;
            }

            return null;
        }

        /// <summary>
        /// Remove germ from the Germ client collection.
        /// </summary>
        /// <param name="name">Germ host name.</param>
        public void RemoveGerm(string name)
        {
            if (this.germs.ContainsKey(name))
            {
                this.germs.Remove(name);
            }
        }

        /// <summary>
        /// Save the germ clients into xml format, this method will be called by XmlGermProvider.Save() method.
        /// </summary>
        /// <param name="writer">Xml writer.</param>
        internal void Save(XmlWriter writer)
        {
            lock (GermClientContainer.loadLock)
            {
                try
                {
                    writer.WriteStartElement("GermClientList");
                    writer.WriteAttributeString("Version", "1.0");

                    foreach (GermClient client in this.germs.Values)
                    {
                        writer.WriteStartElement("GermClient");
                        writer.WriteAttributeString("name", client.Name);
                        writer.WriteAttributeString("undercover", client.IsUndercover.ToString());
                        writer.WriteEndElement();
                    }

                    writer.WriteEndElement();
                }
                catch (Exception e)
                {
                    DebugConsole.Instance.EnqueueTraceInfo(string.Format("Failed to save germ client list : {0}", e.Message), System.Diagnostics.TraceLevel.Error);
                }
            }
        }

        /// <summary>
        /// Load the germ from the xml file given from the XmlGermProvider, opposite functionality to Save() method.
        /// </summary>
        /// <param name="reader">Xml reader.</param>
        internal void Load(XmlReader reader)
        {
            lock (GermClientContainer.loadLock)
            {
                try
                {
                    if (reader.IsStartElement() && reader.LocalName == "GermClientList" && reader.IsEmptyElement)
                    {
                        reader.Read();
                        return;
                    }
                    
                    //// TODO : Check version
                    while (reader.IsStartElement() && reader.LocalName == "GermClientList")
                    {
                        reader.Read();
                    }

                    while (reader.LocalName == "GermClient")
                    {
                        string hostName = reader.GetAttribute("name");
                        string isUndercover = reader.GetAttribute("undercover");

                        if (!string.IsNullOrEmpty(hostName))
                        {
                            GermClient germClient = new GermClient(hostName, Certificate);
                            germClient.IsUndercover = string.IsNullOrEmpty(isUndercover) || isUndercover.ToLower() == "true";
                            this.germs.Add(hostName, germClient);
                        }

                        reader.Read();
                        reader.MoveToContent();
                    }

                    reader.ReadEndElement();
                }
                catch (XmlException e)
                {
                    DebugConsole.Instance.EnqueueTraceInfo(string.Format("Failed to load germ client list : {0}", e.Message), System.Diagnostics.TraceLevel.Error);
                }
                catch (Exception e)
                {
                    DebugConsole.Instance.EnqueueTraceInfo(string.Format("Failed to load germ client list : {0}", e.Message), System.Diagnostics.TraceLevel.Error);
                }
            }
        }
    }
}
