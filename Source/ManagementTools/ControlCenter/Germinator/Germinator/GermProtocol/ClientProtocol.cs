﻿//---------------------------------------------------------------------------------
// <copyright file="ClientProtocol.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2010 NICTA Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net.Security;
using System.Net.Sockets;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;

using GermServer;

namespace Germinator
{
    /// <summary>
    /// Germ client protocol.
    /// </summary>
    internal class ClientProtocol : Protocol
    {
        /// <summary>
        /// Binary reader.
        /// </summary>
        private BinaryReader reader;

        /// <summary>
        /// Stream object lock.
        /// </summary>
        private object streamLock = new object();

        /// <summary>
        /// Tcp client.
        /// </summary>
        private TcpClient client;

        /// <summary>
        /// Secure stream.
        /// </summary>
        private SslStream sslStream;

        /// <summary>
        /// X509 certificate use on ssl connection.
        /// </summary>
        private X509Certificate2 certificate;

        /// <summary>
        /// Initializes a new instance of the <see cref="ClientProtocol"/> class.
        /// </summary>
        /// <param name="certificate">X509 certificate.</param>
        public ClientProtocol(X509Certificate2 certificate)
        {
            this.certificate = certificate;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ClientProtocol"/> class.
        /// </summary>
        /// <param name="certificate">X509 certificate.</param>
        /// <param name="client">Tcp client.</param>
        /// <param name="secureStream">Ssl stream.</param>
        public ClientProtocol(X509Certificate2 certificate, TcpClient client, SslStream secureStream)
            : this(certificate)
        {
            this.client = client;
            this.sslStream = secureStream;
        }

        /// <summary>
        /// Gets a value indicating whether the client is currently connected.
        /// </summary>
        public bool IsConnected
        {
            get
            {
                return this.client != null && this.client.Connected;
            }
        }

        /// <summary>
        /// Initialize a connection to the remote germ.
        /// </summary>
        /// <param name="host">Germ host name.</param>
        /// <returns>Return true on success.</returns>
        public bool Initialize(string host)
        {
            try
            {
                if (this.client == null)
                {
                    this.client = new TcpClient();
                    this.client.Connect(host.Split(':')[0], int.Parse(host.Split(':')[1]));

                    this.sslStream = new SslStream(this.client.GetStream(), false);
                    this.sslStream.AuthenticateAsServer(this.certificate, false, SslProtocols.Tls, false);
                }

                BinaryReader reader = new BinaryReader(this.sslStream);

                //// Note: workaround from the Mono bug 457120, it should be removed when the bug is fixed.
                //// https://bugzilla.novell.com/show_bug.cgi?id=457120
                reader.ReadByte();
                
                if (this.ConnectToServer(reader, host))
                {
                    this.reader = reader;

                    return true;
                }
            }
            catch (Exception e) 
            {
                string error = e.ToString();
            }

            return false;
        }
        
        /// <summary>
        /// Shutdown the connection with the remote germ.
        /// </summary>
        public void Shutdown()
        {
            lock (this.streamLock)
            {
                if (this.reader != null)
                {
                    this.EndConversation(this.reader);
                    this.reader.Close();
                    this.reader = null;
                }

                this.client.Close();
            }
        }

        /// <summary>
        /// Get authentication token.
        /// </summary>
        /// <returns>Returns byte of authentication token.</returns>
        [Obsolete("This method is obsolete; use Initialize() method instead")]
        public byte[] GetAuthenticationToken()
        {
            if (this.reader == null)
            {
                throw new InvalidOperationException("Must begin before performing any operations.");
            }

            lock (this.streamLock)
            {
                return base.GetAuthenticationToken(this.reader);
            }
        }

        /// <summary>
        /// Authenticate the incoming stream.
        /// </summary>
        /// <param name="signature">Signature in bytes.</param>
        /// <returns>Return true on success.</returns>
        [Obsolete("This method is obsolete; use Initialize() method instead")]
        public bool Authenticate(byte[] signature)
        {
            if (this.reader == null)
            {
                throw new InvalidOperationException("Must begin before performing any operations.");
            }

            string clientDescription = "GerminatorClient";
            lock (this.streamLock)
            {
                return base.Authenticate(this.reader, signature, clientDescription);
            }
        }

        /// <summary>
        /// Get the list of running applications.
        /// </summary>
        /// <returns>Return the list of running services.</returns>
        public List<ProcessDescription> GetRunningApplications()
        {
            if (this.reader == null)
            {
                throw new InvalidOperationException("Must begin before performing any operations.");
            }

            lock (this.streamLock)
            {
                return base.GetRunningApplications(this.reader);
            }
        }

        /// <summary>
        /// Send the opend file.
        /// </summary>
        /// <param name="directory">Directory name.</param>
        /// <param name="file">File name.</param>
        /// <param name="hash">Hash value.</param>
        /// <returns>Return the upload id.</returns>
        public int SendOpenFile(string directory, string file, string hash)
        {
            if (this.reader == null)
            {
                throw new InvalidOperationException("Must begin before performing any operations.");
            }

            lock (this.streamLock)
            {
                return base.SendOpenFile(this.reader, directory, file, hash);
            }
        }

        /// <summary>
        /// Send segment of a file
        /// </summary>
        /// <param name="id">Upload id.</param>
        /// <param name="segment">Segement of a file.</param>
        /// <param name="length">Length of the segment.</param>
        /// <returns>Return true on success.</returns>
        public bool SendFileSegment(int id, byte[] segment, int length)
        {
            if (this.reader == null)
            {
                throw new InvalidOperationException("Must begin before performing any operations.");
            }

            lock (this.streamLock)
            {
                return base.SendFileSegment(this.reader, id, segment, length);
            }
        }

        /// <summary>
        /// Send a close file.
        /// </summary>
        /// <param name="id">The upload id.</param>
        /// <returns>Return true on success.</returns>
        public bool SendCloseFile(int id)
        {
            if (this.reader == null)
            {
                throw new InvalidOperationException("Must begin before performing any operations.");
            }

            lock (this.streamLock)
            {
                return base.SendCloseFile(this.reader, id);
            }
        }

        /// <summary>
        /// Start the application.
        /// </summary>
        /// <param name="name">Service name.</param>
        /// <param name="applicationDirectory">Application directory.</param>
        /// <param name="executable">Executable file.</param>
        /// <param name="args">Process line argument.</param>
        /// <returns>Return the process description.</returns>
        public ProcessDescription StartApplication(string name, string applicationDirectory, string executable, string args)
        {
            if (this.reader == null)
            {
                throw new InvalidOperationException("Must begin before performing any operations.");
            }

            lock (this.streamLock)
            {
                int processId = this.SendStartProcess(this.reader, name, applicationDirectory, executable, args);

                return new ProcessDescription(processId, applicationDirectory, args);
            }
        }

        /// <summary>
        /// Kill or stop a service given the process id.
        /// </summary>
        /// <param name="processId">Process id.</param>
        public void KillApplication(int processId)
        {
            if (this.reader == null)
            {
                throw new InvalidOperationException("Must begin before performing any operations.");
            }

            lock (this.streamLock)
            {
                this.SendKillProcess(this.reader, processId);
            }
        }

        /// <summary>
        /// Send a custom message.
        /// </summary>
        /// <param name="processId">Process id.</param>
        /// <param name="messageType">Type of message.</param>
        /// <param name="message">Custom message.</param>
        /// <param name="reply">Reply message received.</param>
        /// <returns>Return custom message reply.</returns>
        public CustomMessageReply SendCustomMessage(int processId, int messageType, byte[] message, out byte[] reply)
        {
            lock (this.streamLock)
            {
                if (this.reader == null)
                {
                    throw new InvalidOperationException("Must begin before performing any operations.");
                }

                return this.ForwardCustomMessage(this.reader, processId, messageType, message, out reply);
            }
        }

        /// <summary>
        /// Send get machine status message
        /// </summary>
        /// <param name="message">Custom message.</param>
        /// <param name="reply">Reply message received.</param>
        /// <returns>Return custom message reply.</returns>
        public CustomMessageReply SendGetMachineStatus(byte[] message, out byte[] reply)
        {
            lock (this.streamLock)
            {
                if (this.reader == null)
                {
                    throw new InvalidOperationException("Must begin before performing any operations.");
                }

                return this.GetMachineStatus(this.reader, message, out reply);
            }
        }
    }
}
