﻿//-----------------------------------------------------------------------
// <copyright file="GermClient.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Remoting;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;
using System.Xml.Serialization;

using Germinator.Services;
using GermServer;

namespace Germinator
{
    /// <summary>
    /// Responsible for communication with the germ. Primarily upload files, and start/stop applications
    /// </summary>
    public class GermClient
    {
        /// <summary>
        /// The default time out is 5 seconds.
        /// </summary>
        private static int defaultOperationTimeout = 5000;

        /// <summary>
        /// A client protocol (Germ protocol).
        /// </summary>
        private ClientProtocol client;

        /// <summary>
        /// X509Certificate use to communicate with the remote germ using ssl connection.
        /// </summary>
        private X509Certificate2 certificate;

        /// <summary>
        /// Operation lock object.
        /// </summary>
        private object operationsLock = new object();

        /// <summary>
        /// Connection lock object.
        /// </summary>
        private object connectionLock = new object();

        /// <summary>
        /// Operations queue.
        /// </summary>
        private Queue<Operation> operations = new Queue<Operation>();

        /// <summary>
        /// Operation thread.
        /// </summary>
        private Thread operationThread;

        /// <summary>
        /// The germ host address.
        /// </summary>
        private string germHost;

        /// <summary>
        /// Initializes a new instance of the <see cref="GermClient"/> class.
        /// For xml serialization.
        /// </summary>
        public GermClient() 
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GermClient"/> class.
        /// </summary>
        /// <param name="remoteHost">Germ host name.</param>
        /// <param name="certificate">Certificate use for ssl connection.</param>
        public GermClient(string remoteHost, X509Certificate2 certificate)
        {
            if (string.IsNullOrEmpty(remoteHost))
            {
                throw new ArgumentException(String.Format("{0} is not a valid host name", remoteHost), "remoteHost");
            }

            this.certificate = certificate;
            this.germHost = remoteHost;
            this.Name = remoteHost;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GermClient"/> class.
        /// </summary>
        /// <param name="remoteHost">Germ host name.</param>
        /// <param name="certificate">Certificate use for ssl connection.</param>
        /// <param name="client">Protocol client instance.</param>
        internal GermClient(string remoteHost, X509Certificate2 certificate, ClientProtocol client)
            : this(remoteHost, certificate)
        {
            this.client = client;
        }

        /// <summary>
        /// Gets the default operation timeout.
        /// </summary>
        public static int DefaultOperationTimeout
        {
            get { return defaultOperationTimeout; }
        }

        /// <summary>
        /// Gets or sets the germ name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the germ is undercover.
        /// </summary>
        public bool IsUndercover { get; set; }

        /// <summary>
        /// Gets a value indicating whether this germ is currently connected.
        /// </summary>
        public bool IsConnected
        {
            get
            {
                lock (this.connectionLock)
                {
                    //// TODO:It should return connect
                    //// return this.mClient != null;
                    return this.Connect();
                }
            }
        }

        /// <summary>
        /// Gets or sets the client protocol.
        /// </summary>
        internal ClientProtocol Client
        {
            get { return this.client; }
            set { this.client = value; }
        }

        /// <summary>
        /// Ulpoad operation.
        /// </summary>
        /// <param name="destinationDirectory">Destination directory.</param>
        /// <param name="sourceFullPath">Full path of source file.</param>
        /// <param name="destinationRelativeDirectory">Destination relative directory.</param>
        /// <param name="callback">Asynchronize callback function.</param>
        /// <param name="callbackState">A callback state.</param>
        /// <returns>Return Upload operation.</returns>
        public UploadOperation QueueUploadOperation(string destinationDirectory, string sourceFullPath, string destinationRelativeDirectory, AsyncCallback callback, object callbackState)
        {
            Dictionary<string, string> files = new Dictionary<string, string>();

            files.Add(sourceFullPath, destinationRelativeDirectory);
            return this.QueueUploadOperation(destinationDirectory, files, callback, callbackState);
        }

        /// <summary>
        /// Upload operation.
        /// </summary>
        /// <param name="destinationDirectory">Destination directory.</param>
        /// <param name="sourceDestinationPairs">Source destination pairs.</param>
        /// <param name="callback">Asynchronize callback function.</param>
        /// <param name="callbackState">A callback state.</param>
        /// <returns>Returns the upload operation.</returns>
        public UploadOperation QueueUploadOperation(string destinationDirectory, Dictionary<string, string> sourceDestinationPairs, AsyncCallback callback, object callbackState)
        {
            UploadOperation uploadOperation = new UploadOperation(destinationDirectory, sourceDestinationPairs, this, callback, callbackState);

            this.QueueOperation(uploadOperation);

            return uploadOperation;
        }

        /// <summary>
        /// Start service operation
        /// </summary>
        /// <param name="name">Service name.</param>
        /// <param name="applicationDirectory">The application directory.</param>
        /// <param name="executable">Executable file.</param>
        /// <param name="args">Process line argument.</param>
        /// <param name="callback">Asynchronize callback function.</param>
        /// <param name="callbackState">A callback state.</param>
        /// <returns>Return operation.</returns>
        public ExecuteOperation QueueStartOperation(string name, string applicationDirectory, string executable, string args, AsyncCallback callback, object callbackState)
        {
            ExecuteOperation operation = new ExecuteOperation(this, name, applicationDirectory, executable, args, callback, callbackState);

            this.QueueOperation(operation);
            return operation;
        }

        /// <summary>
        /// Stop service operation
        /// </summary>
        /// <param name="processId">Process Id.</param>
        /// <param name="callback">Asynchronize callback function.</param>
        /// <param name="callbackState">A callback state.</param>
        /// <returns>Return asynchronize result.</returns>
        public IAsyncResult QueueKillOperation(int processId, AsyncCallback callback, object callbackState)
        {
            Operation killOperation = new Operation(delegate { this.client.KillApplication(processId); }, callback, callbackState);

            this.QueueOperation(killOperation);
            return killOperation;
        }

        /// <summary>
        /// Process list operation.
        /// </summary>
        /// <param name="callback">Asynchronize callback function.</param>
        /// <param name="callbackState">A callback state.</param>
        /// <returns>Return process list operation.</returns>
        public ProcessListOperation QueueProcessListOperation(AsyncCallback callback, object callbackState)
        {
            ProcessListOperation listOperation = new ProcessListOperation(this, callback, callbackState);

            this.QueueOperation(listOperation);
            return listOperation;
        }

        /// <summary>
        /// Queue the custom request operation.
        /// </summary>
        /// <param name="processId">Process Id.</param>
        /// <param name="messageType">Message type.</param>
        /// <param name="message">Custom message.</param>
        /// <param name="callback">Asynchronize callback function.</param>
        /// <param name="callbackState">A callback state.</param>
        /// <returns>Return custom message operation.</returns>
        public CustomMessageOperation QueueCustomRequestOperation(int processId, int messageType, byte[] message, AsyncCallback callback, object callbackState)
        {
            CustomMessageOperation operation = new CustomMessageOperation(null, callback, callbackState);

            operation.ProcessId = processId;
            operation.MessageType = messageType;
            operation.OperationTask = delegate
            {
                byte[] response = null;
                CustomMessageReply replyType = this.client.SendCustomMessage(processId, messageType, message, out response);

                switch (replyType)
                {
                    case CustomMessageReply.OkWithData:
                        operation.Response = new MemoryStream(response);
                        operation.WasSuccessful = true;
                        break;
                    case CustomMessageReply.OkWithoutData:
                        operation.Response = new MemoryStream();
                        operation.WasSuccessful = true;
                        break;
                    case CustomMessageReply.NotAuthorized:
                        operation.WasSuccessful = false;
                        DebugConsole.Instance.EnqueueTraceInfo(string.Format("Failed to perform custom request operation to {0} because authorization failed", this.germHost), TraceLevel.Warning);
                        break;
                    case CustomMessageReply.ProcessUnharnessed:
                        operation.WasSuccessful = false;
                        operation.IsUnharnessed = true;
                        DebugConsole.Instance.EnqueueTraceInfo(string.Format("Failed to perform custom request operation to {0} because the process is un-harnessed", this.germHost), TraceLevel.Warning);
                        break;
                    case CustomMessageReply.ProcessUnknown:
                        operation.WasSuccessful = false;
                        DebugConsole.Instance.EnqueueTraceInfo(string.Format("Failed to perform custom request operation to {0} because the process is unknown", this.germHost), TraceLevel.Warning);
                        break;
                    case CustomMessageReply.UnspecifiedError:
                        operation.WasSuccessful = false;
                        DebugConsole.Instance.EnqueueTraceInfo(string.Format("Failed to perform custom request operation to {0}", this.germHost), TraceLevel.Warning);
                        break;
                    case CustomMessageReply.ProcessExitWithUnknownReason:
                        operation.Response = new MemoryStream(this.ForgeCustomMessageResponse(PingStatus.ExitWithError, "Process has exited with unknown reason."));
                        operation.WasSuccessful = false;
                        string processName = Encoding.UTF8.GetString(response);
                        DebugConsole.Instance.EnqueueTraceInfo(string.Format("Process {0} on {1} has exited with unknown reason.", processName, this.germHost), TraceLevel.Error);
                        break;
                    case CustomMessageReply.ProcessExitArgumentError:
                        operation.Response = new MemoryStream(this.ForgeCustomMessageResponse(PingStatus.ExitWithError, "Invalid arguments: Process has exited."));
                        operation.WasSuccessful = false;
                        processName = Encoding.UTF8.GetString(response);
                        DebugConsole.Instance.EnqueueTraceInfo(string.Format("Invalid arguments: Process {0} on {1} has exited.", processName, this.germHost), TraceLevel.Error);
                        break;
                    case CustomMessageReply.ProcessExitDeiAuthenticationFailed:
                        operation.Response = new MemoryStream(this.ForgeCustomMessageResponse(PingStatus.ExitWithError, "Dei authentication failed: Process has exited."));
                        operation.WasSuccessful = false;
                        processName = Encoding.UTF8.GetString(response);
                        DebugConsole.Instance.EnqueueTraceInfo(string.Format("Dei authentication failed: Process {0} on {1} has exited.", processName, this.germHost), TraceLevel.Error);
                        break;
                    case CustomMessageReply.ProcessExitAnnounceServiceFailed:
                        operation.Response = new MemoryStream(this.ForgeCustomMessageResponse(PingStatus.ExitWithError, "Announce service failed: Process has exited."));
                        operation.WasSuccessful = false;
                        processName = Encoding.UTF8.GetString(response);
                        DebugConsole.Instance.EnqueueTraceInfo(string.Format("Announce service failed: Process {0} on {1} has exited.", processName, this.germHost), TraceLevel.Error);
                        break;
                    case CustomMessageReply.ProcessExitConfigurationError:
                        operation.Response = new MemoryStream(this.ForgeCustomMessageResponse(PingStatus.ExitWithError, "Failed to load/save configuration options: Process has exited."));
                        operation.WasSuccessful = false;
                        processName = Encoding.UTF8.GetString(response);
                        DebugConsole.Instance.EnqueueTraceInfo(string.Format("Failed to load/save configuration options: Process {0} on {1} has exited.", processName, this.germHost), TraceLevel.Error);
                        break;
                    default:
                        operation.WasSuccessful = false;
                        break;
                }
            };

            this.QueueOperation(operation);
            return operation;
        }

        /// <summary>
        /// Get machine status operation.
        /// </summary>
        /// <remarks>Should use CustomMessageOperation instead.</remarks>
        /// <param name="message">Custom message.</param>
        /// <param name="callback">Asynchronize callback function.</param>
        /// <param name="callbackState">A callback state.</param>
        /// <returns>Returns custom message operation.</returns>
        public CustomMessageOperation QueueGetMachineStatusOperation(byte[] message, AsyncCallback callback, object callbackState)
        {
            CustomMessageOperation operation = new CustomMessageOperation(null, callback, callbackState);

            operation.OperationTask = delegate
            {
                byte[] response = null;
                CustomMessageReply replyType = this.client.SendGetMachineStatus(message, out response);

                switch (replyType)
                {
                    case CustomMessageReply.OkWithData:
                        operation.Response = new MemoryStream(response);
                        operation.WasSuccessful = true;
                        break;
                    case CustomMessageReply.OkWithoutData:
                        operation.Response = new MemoryStream();
                        operation.WasSuccessful = true;
                        break;
                    case CustomMessageReply.NotAuthorized:
                        operation.WasSuccessful = false;
                        Trace.TraceWarning("Failed to get machine status operation to {0} because authorization failed", this.germHost);
                        DebugConsole.Instance.EnqueueTraceInfo(string.Format("Failed to get machine status operation to {0} because authorization failed", this.germHost));
                        break;
                    case CustomMessageReply.UnspecifiedError:
                        operation.WasSuccessful = false;
                        Trace.TraceWarning("Failed to get machine status operation to {0}", this.germHost);
                        DebugConsole.Instance.EnqueueTraceInfo(string.Format("Failed to get machine status operation to {0}", this.germHost));
                        break;
                    default:
                        operation.WasSuccessful = false;
                        break;
                }
            };

            this.QueueOperation(operation);

            return operation;
        }

        /// <summary>
        /// Return the name of the germ (i.e. Germ host name).
        /// </summary>
        /// <returns>Return the name of the germ.</returns>
        public override string ToString()
        {
            return this.Name;
        }

        /// <summary>
        /// Make an upload client.
        /// </summary>
        /// <returns>Return an upload client.</returns>
        internal UploadClient MakeUploadClient()
        {
            lock (this.connectionLock)
            {
                if (this.client == null)
                {
                    if (!this.Connect())
                    {
                        return null;
                    }
                }

                return new UploadClient(this.client);
            }
        }

        /// <summary>
        /// Execute a command.
        /// </summary>
        /// <param name="name">Service name.</param>
        /// <param name="applicationDirectory">Application directory.</param>
        /// <param name="executable">The executable file.</param>
        /// <param name="args">Process line argument.</param>
        /// <returns>Return process description.</returns>
        internal ProcessDescription Execute(string name, string applicationDirectory, string executable, string args)
        {
            lock (this.connectionLock)
            {
                if (this.client == null)
                {
                    if (!this.Connect())
                    {
                        return null;
                    }
                }

                try
                {
                    return this.client.StartApplication(name, applicationDirectory, executable, args);
                }
                catch
                {
                    return null;
                }
            }
        }

        /// <summary>
        /// Get the process list running on the remote germ.
        /// </summary>
        /// <returns>Return list of process descriptions.</returns>
        internal List<ProcessDescription> GetProcessList()
        {
            lock (this.connectionLock)
            {
                if (this.client == null)
                {
                    if (!this.Connect())
                    {
                        return null;
                    }
                }

                try
                {
                    return this.client.GetRunningApplications();
                }
                catch
                {
                    return null;
                }
            }
        }

        /// <summary>
        /// Disconnect from the germ.
        /// </summary>
        internal void Disconnect()
        {
            lock (this.connectionLock)
            {
                if (this.client != null)
                {
                    this.client.Shutdown();
                    this.client = null;
                }
            }
        }

        /// <summary>
        /// Queue operation.
        /// </summary>
        /// <param name="operation">Operation need to be queued.</param>
        private void QueueOperation(Operation operation)
        {
            bool needToStartThread = false;

            lock (this.operationsLock)
            {
                needToStartThread = this.operations.Count == 0 || !this.operationThread.IsAlive;
                this.operations.Enqueue(operation);
            }

            if (needToStartThread)
            {
                try
                {
                    this.operationThread = new Thread(this.PerformOperations);
                    this.operationThread.IsBackground = true;
                    this.operationThread.Start();
                }
                catch (System.Threading.ThreadAbortException)
                {
                    // Terminate the thread properly.
                    this.operationThread.Join();
                }
            }
        }

        /// <summary>
        /// Perform operations.
        /// </summary>
        private void PerformOperations()
        {
            if (!this.IsConnected && !this.Connect())
            {
                lock (this.operationsLock)
                {
                    foreach (Operation failedOperation in this.operations)
                    {
                        failedOperation.Fail("Cannot connect with germ");
                    }

                    this.operations.Clear();
                }

                return;
            }

            Operation operation = null;
            lock (this.operationsLock)
            {
                if (this.operations.Count == 0)
                {
                    return;
                }

                operation = this.operations.Peek();
            }

            do
            {
                operation.Perform();
                operation = null;

                lock (this.operationsLock)
                {
                    this.operations.Dequeue();

                    if (this.operations.Count > 0)
                    {
                        operation = this.operations.Peek();
                    }
                    else
                    {
                        operation = null;
                    }
                }
            }
            while (operation != null);
        }

        /// <summary>
        /// Connect to the remote germ.
        /// </summary>
        /// <returns>Return true on success.</returns>
        private bool Connect()
        {
            if (this.client != null && this.client.IsConnected)
            {
                return true;
            }

            lock (this.connectionLock)
            {
                ClientProtocol client = new ClientProtocol(this.certificate);

                // Use SslStream to communicate with the remote germs.
                try
                {
                    if (client.Initialize(this.germHost))
                    {
                        this.client = client;
                        return true;
                    }

                    Trace.TraceError("Failed to intitialize connection with Germ {0}", this.germHost);
                    DebugConsole.Instance.EnqueueTraceInfo(string.Format("Failed to intitialize connection with Germ {0}", this.germHost));

                    client.Shutdown();
                    return false;
                }
                catch (RemotingException e)
                {
                    Trace.TraceError("Failed to contact remote germ : {0}", e.Message);
                    DebugConsole.Instance.EnqueueTraceInfo(string.Format("Failed to contact remote germ : {0}", e.Message));
                }
                catch (ArgumentException)
                {
                    Trace.TraceError("Failed to sign authentication token");
                    DebugConsole.Instance.EnqueueTraceInfo(string.Format("Failed to sign authentication token"));
                }
                catch (SocketException)
                {
                    Trace.TraceError("Connection with {0} ended abruptly", this.germHost);
                    DebugConsole.Instance.EnqueueTraceInfo(string.Format("Connection with {0} ended abruptly", this.germHost));
                }
                catch (Exception e)
                {
                    Trace.TraceError("Exception thrown at {0} : {1}", this.germHost, e.Message);
                    DebugConsole.Instance.EnqueueTraceInfo(string.Format("Exception thrown at {0} : {1}", this.germHost, e.Message));
                }

                client.Shutdown();
                return false;
            }
        }

        /// <summary>
        /// Forging a custom message response, is a local message response, should make this more 
        /// general.
        /// </summary>
        /// <param name="state">Ping status.</param>
        /// <param name="errorText">Error text if there is one.</param>
        /// <returns>Return the response in byte array.</returns>
        private byte[] ForgeCustomMessageResponse(PingStatus state, string errorText)
        {
            MemoryStream response = new MemoryStream();
            BinaryWriter writer = new BinaryWriter(response);
            writer.Write((int)state);
            writer.Write(errorText);
            
            return response.ToArray();
        }
    }
}
