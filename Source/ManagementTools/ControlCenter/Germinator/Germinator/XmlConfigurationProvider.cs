﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace Germinator
{
    public class XmlConfigurationProvider : IProvider<Configuration>
    {
        private string mDirectory;

        public XmlConfigurationProvider(string directory)
        {
            if (!Directory.Exists(directory))
            {
                throw new InvalidOperationException(String.Format("Directory '{0}' does not exist", directory));
            }

            this.mDirectory = directory;
        }

        public void Add(Configuration configuration)
        {
            this.Save(configuration);
        }

        public void Remove(String configurationName)
        {
            string configurationFilename = this.ConfigurationFilename(configurationName);
            if (configurationFilename != null && File.Exists(configurationFilename))
            {
                try
                {
                    File.Delete(configurationFilename);
                }
                catch { }
            }
        }

        public IEnumerable<Configuration> Load()
        {
            List<Configuration> configurations = new List<Configuration>();

            foreach (string filename in Directory.GetFiles(this.mDirectory, "*.xml"))
            {
                Configuration configuration = this.Load(filename);
                if (configuration != null)
                {
                    configurations.Add(configuration);
                }
            }

            return configurations;
        }

        public void Save(Configuration configuration)
        {
            if (configuration == null)
            {
                throw new ArgumentNullException("configuration");
            }
            if (configuration.Name == null)
            {
                throw new InvalidOperationException("Cannot save configurations without a name");
            }

            string filename = this.ConfigurationFilename(configuration.Name);

            try
            {
                XmlWriterSettings settings = new XmlWriterSettings();
                settings.Indent = true;

                using (XmlWriter writer = XmlWriter.Create(filename, settings))
                {
                    writer.WriteStartDocument();
                    configuration.Save(writer);
                }
            }
            catch (Exception e)
            {
                DebugConsole.Instance.EnqueueTraceInfo(string.Format("Failed to save configuration {1} : {0}", e.Message, configuration.Name), System.Diagnostics.TraceLevel.Error);
            }
        }

        private string ConfigurationFilename(string configurationName)
        {
            try
            {
                return Path.Combine(this.mDirectory, string.Format("{0}.xml", configurationName));
            }
            catch { }
            return null;
        }

        private Configuration Load(string filename)
        {
            if (string.IsNullOrEmpty(filename))
            {
                throw new InvalidOperationException("Cannot load configuration becuase file name is invalid.");
            }

            try
            {
                if (File.Exists(filename))
                {
                    using (XmlReader reader = XmlReader.Create(filename))
                    {
                        Configuration configuration = new Configuration();
                        configuration.Load(reader, this);
                        return configuration;
                    }
                }
            }
            catch (Exception e)
            {
                DebugConsole.Instance.EnqueueTraceInfo(string.Format("Failed to load configuration from '{1}' : {0}, Corrupted file :'{1}' removed", e.Message, Path.GetFileName(filename)), System.Diagnostics.TraceLevel.Error);
                File.Delete(filename);
            }

            return null;
        }

    }
}
