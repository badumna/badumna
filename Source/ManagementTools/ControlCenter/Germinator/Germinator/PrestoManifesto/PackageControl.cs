﻿//---------------------------------------------------------------------------------
// <copyright file="PackageControl.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;

using Badumna.Package.Builder;

namespace Germinator.PrestoManifesto
{
    /// <summary>
    /// PackageControl class is used in PrestoManifesto, one service/application in presto manifesto project will be handle by
    /// each PackageControl class. PackageControl class track the package version and all the files in the package.
    /// </summary>
    public class PackageControl
    {
        /// <summary>
        /// Update progress delegate is used during package buliding process.
        /// </summary>
        /// <param name="stage">Current building stage.</param>
        /// <param name="totalStage">Total building stage.</param>
        /// <param name="description">Progress description.</param>
        /// <param name="isError">Boolean, is set if error occurs.</param>
        private delegate void UpdateProgressDelegate(int stage, int totalStage, string description, bool isError);

        /// <summary>
        /// List of package extraction tools path (full path).
        /// </summary>
        private static string[] packageToolPaths;

        /// <summary>
        /// Manifest Builder (private field).
        /// </summary>
        private ManifestBuilder manifestBuilder;
        
        /// <summary>
        /// Package Data (private field) <seealso cref="PrestoManifesto.PackageData"/>. 
        /// </summary>
        private PackageData package;

        /// <summary>
        /// Project <seealso cref="PrestoManifesto.Project"/> (private field).
        /// </summary>
        private Project project;

        /// <summary>
        /// Boolean set to true if CC is running in mono (private field).
        /// </summary>
        private bool isMono;

        /// <summary>
        /// Package version : Major number.
        /// </summary>
        private string major;

        /// <summary>
        /// Pacakge version : Minor number.
        /// </summary>
        private string minor;

        /// <summary>
        /// Package version : Build number.
        /// </summary>
        private string build;

        /// <summary>
        /// Package version : Revision number.
        /// </summary>
        private string revision;

        /// <summary>
        /// Package executable file (private field).
        /// </summary>
        private string executable;

        /// <summary>
        /// Package update type (partial or complete update).
        /// </summary>
        private string updateType;

        /// <summary>
        /// Package output path.
        /// </summary>
        private string output;

        /// <summary>
        /// Include or exclude the package extraction tools.
        /// </summary>
        private bool includeExtractionTools;

        /// <summary>
        /// Build max progress bar stages.
        /// </summary>
        private int maxProgressStages;

        /// <summary>
        /// Current progress bar stages.
        /// </summary>
        private int currProgressStages;

        /// <summary>
        /// Package description.
        /// </summary>
        private string description;

        /// <summary>
        /// Gets package data.
        /// </summary>
        /// <value>Package Data.</value>
        public PackageData Package
        {
            get
            {
                return this.package;
            }
        }

        /// <summary>
        /// Gets or sets the package update type.
        /// </summary>
        /// <value>Package update type.</value>
        public string UpdateType
        {
            get { return this.updateType; }
            set { this.updateType = value; }
        }

        /// <summary>
        /// Gets or sets the inlcude ectraction tools field.
        /// </summary>
        /// <value>Include or exlude the package extraction tools (boolean).</value>
        public bool IncludeExtractionTools
        {
            get { return this.includeExtractionTools; }
            set { this.includeExtractionTools = value; }
        }

        /// <summary>
        /// Gets the max progress bar stages.
        /// </summary>
        /// <value>Max progrss bar stages.</value>
        public int MaxProgressStages
        {
            get
            {
                return this.maxProgressStages;
            }
        }

        /// <summary>
        /// Gets the current progress bar stages.
        /// </summary>
        /// <value>Current progress bar stages.</value>
        public int CurrProgressStages
        {
            get
            {
                return this.currProgressStages;
            }
        }

        /// <summary>
        /// Gets the package description.
        /// </summary>
        /// <value>Package description.</value>
        public string Description
        {
            get
            {
                return this.description;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PackageControl"/> class.
        /// It takes 0 argument.
        /// </summary>
        public PackageControl(string appPath)
        {
            string baseDir = string.Empty;
            this.isMono = Type.GetType("Mono.Runtime") != null;
            if (!this.isMono)
            {
                baseDir = @"Common\";
            }
            else
            {
                baseDir = @"Common/";
            }

            string appBaseDirectory = Path.Combine(appPath, baseDir);
            PackageControl.packageToolPaths = new string[] 
            {
                Path.Combine(appBaseDirectory, "ApplicationStarter.exe"), 
                Path.Combine(appBaseDirectory, "Badumna.Package.dll"),
                Path.Combine(appBaseDirectory, "ProcessMonitor.exe"), 
                Path.Combine(appBaseDirectory, "ICSharpCode.SharpZipLib.dll")
            };
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PackageControl"/> class.
        /// It takes two arguments, application package data and the presto manifesto project.
        /// </summary>
        public PackageControl(PackageData application, Project project, string appPath)
            : this(appPath)
        {
            this.project = project;

            if (!this.LoadPackage(application))
            {
                // new package
                this.StartNewPackage(application);
            }

            this.project.SaveEvent += new EventHandler(this.Current_SaveEvent);
        }

        /// <summary>
        /// Add a file to package conrol class
        /// </summary>
        /// <param name="fullPath">File full path.</param>
        /// <param name="modificationTime">Date time represent the file modification time.</param>
        /// <returns>Return true on success.</returns>
        public bool AddFile(string fullPath, DateTime modificationTime)
        {
            if (this.package != null)
            {
                //// Remove the base directory path from the file name full path, the remaining path will be
                //// the file relative path 
                FileData fileData = new FileData() { Name = fullPath.Remove(0, this.manifestBuilder.BaseDirectory.Length), ModificationTime = modificationTime };
                FileData oldFileData;
                bool replaceOrAdd = false;

                if (this.package.Files.TryGetValue(fullPath, out oldFileData))
                {
                    if (oldFileData.ModificationTime.Ticks < fileData.ModificationTime.Ticks)
                    {
                        ////replace the old file, with the new file
                        this.package.Files[fullPath] = fileData;
                        replaceOrAdd = true;
                    }
                }
                else
                {
                    this.package.Files.Add(fullPath, fileData);
                    replaceOrAdd = true;
                }

                //// Note: Let the user to choose the exe file manually.
                ////if (Path.GetExtension(fullPath).Equals(".exe"))
                ////{
                ////    this.executable = fullPath;
                ////}

                return replaceOrAdd;
            }

            return false;
        }

        public void RemoveFile(string fullPath)
        {
            if (this.package != null)
            {
                if (this.manifestBuilder != null)
                {
                    this.manifestBuilder.RemoveFile(fullPath);
                }

                if (this.executable.Equals(fullPath) || this.executable.Equals(Path.GetFileName(fullPath)))
                {
                    this.executable = string.Empty;
                }

                this.package.Files.Remove(fullPath);
            }
        }

        public Dictionary<string, FileData> GetContents()
        {
            return this.package.Files;
        }

        public string GetPackageVersion()
        {
            return string.Format("{0}.{1}.{2}.{3}", this.major, this.minor, this.build, this.revision);
        }

        public string GetExecutable()
        {
            if (this.executable != null)
            {
                return Path.GetFileName(this.executable);
            }
            else
            {
                return string.Empty;
            }
        }

        public void SetExecutable(string fullPath)
        {
            this.executable = fullPath;
        }

        public string GetBaseDirectory()
        {
            if (this.package != null)
            {
                return this.package.Path;
            }
            else
            {
                return string.Empty;
            }
        }

        public void RemoveApplication()
        {
            if (this.package != null)
            {
                if (File.Exists(this.package.ManifestFile))
                {
                    File.Delete(this.package.ManifestFile);
                }
            }

            // will not work for unit test.
            Directory.SetCurrentDirectory(System.Web.HttpRuntime.AppDomainAppPath);

            this.project.SaveEvent -= this.Current_SaveEvent;
            if (this.project.Applications.Contains(this.package))
            {
                this.project.Applications.Remove(this.package);
            }

            if (this.manifestBuilder != null)
            {
                string directory = this.manifestBuilder.BaseDirectory;
                this.manifestBuilder.Reset();
                this.manifestBuilder = null;
                try
                {
                    if (Directory.Exists(directory))
                    {
                        // ASP.NET 2.0 feature, it will restart all season variable when one of the directoris is deleted
                        // programmatically, refers to http://www.vikramlakhotia.com/Deleting_Directory_in_ASPnet_20.aspx
                        // or http://forums.asp.net/p/1056323/1504793.aspx for more information

                        // Directory.Delete(directory);
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
            }
        }

        public void BuildPackage(string description)
        {
            if (this.manifestBuilder != null)
            {
                this.SetDescription(description);

                this.maxProgressStages = this.manifestBuilder.ProgressStages;
                this.currProgressStages = 0;

                if (this.package != null)
                {
                    this.BuildThreadStart();
                }
            }
        }

        public bool CopyBuiltPackage(string destinationPath)
        {
            if (this.manifestBuilder != null)
            {
                try
                {
                    DirectoryInfo dirInfo = new DirectoryInfo(this.manifestBuilder.PublishDirectory);
                    FileInfo[] files = dirInfo.GetFiles();

                    foreach (FileInfo file in files)
                    {
                        if (!Directory.Exists(destinationPath))
                        {
                            Directory.CreateDirectory(destinationPath);
                        }

                        file.CopyTo(Path.Combine(destinationPath, file.Name), true);
                    }

                    return true;
                }
                catch
                {
                    return false;
                }
            }

            return false;
        }

        public ServiceType GetPackageType()
        {
            return this.package.ServiceType;
        }

        public string GetPublishDirectory()
        {
            return this.package.PublishFolder;
        }

        private void Current_SaveEvent(object sender, EventArgs e)
        {
            if (this.package != null)
            {
                if (this.manifestBuilder != null)
                {
                    try
                    {
                        this.manifestBuilder.Save(this.package.ManifestFile);
                    }
                    catch (Exception)
                    {
                    }
                }
            }
        }

        private bool LoadPackage(PackageData application)
        {
            this.package = application;

            if (Directory.Exists(application.Path))
            {
                this.manifestBuilder = new ManifestBuilder();
                this.manifestBuilder.PublishDirectory = application.PublishFolder;
                this.manifestBuilder.SetBaseDirectory(application.Path); // Not sure
                this.manifestBuilder.ProgressEvent += this.ManifestBuilder_ProgressEvent;

                try
                {
                    this.manifestBuilder.Load(application.ManifestFile);
                }
                catch (Exception)
                {
                    this.manifestBuilder.SetFilename(application.ManifestFile);
                }

                this.manifestBuilder.SetApplicationName(application.Name);
                this.major = this.manifestBuilder.Major.ToString();
                this.minor = this.manifestBuilder.Minor.ToString();
                this.build = this.manifestBuilder.Build.ToString();
                this.revision = this.manifestBuilder.Revision.ToString();
                
                this.executable = this.manifestBuilder.Executable;
                this.updateType = this.manifestBuilder.UpdateType;

                int packageToolsIncluded = 0;
                foreach (string path in PackageControl.packageToolPaths)
                {
                    string fileName = Path.GetFileName(path);
                    if (this.manifestBuilder.FileList.Contains(fileName))
                    {
                        packageToolsIncluded++;
                    }
                }

                this.includeExtractionTools = packageToolsIncluded == PackageControl.packageToolPaths.Length;

                // TODO: output of the build process should automatically save in the Services directory
                this.output = this.manifestBuilder.BaseDirectory;

                return true;
            }
            else
            {
                return false;
            }
        }

        private void StartNewPackage(PackageData application)
        {
            string applicationName = application.Name;

            try
            {
                if (applicationName.Length > 0)
                {
                    string path = application.Path;

                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(path);
                    }

                    if (this.manifestBuilder == null)
                    {
                        this.manifestBuilder = new ManifestBuilder();

                        this.manifestBuilder.SetApplicationName(application.Name);
                        this.manifestBuilder.SetFilename(Path.Combine(application.Path, String.Format("{0}Manifest.xml", applicationName)));
                        this.manifestBuilder.SetBaseDirectory(application.Path);
                        this.manifestBuilder.PublishDirectory = application.PublishFolder;
                        this.manifestBuilder.ProgressEvent += this.ManifestBuilder_ProgressEvent;
                    }

                    this.major = this.manifestBuilder.Major.ToString();
                    this.minor = this.manifestBuilder.Minor.ToString();
                    this.build = this.manifestBuilder.Build.ToString();
                    this.revision = this.manifestBuilder.Revision.ToString();

                    this.executable = this.manifestBuilder.Executable;
                    this.updateType = this.manifestBuilder.UpdateType;
                }
            }
            catch (Exception)
            {
            }

            this.package = application;
            this.package.ManifestFile = Path.GetFullPath(this.manifestBuilder.FileName);
        }

        private void ManifestBuilder_ProgressEvent(object sender, EventArgs e)
        {
            if (this.manifestBuilder != null)
            {
                UpdateProgressDelegate updateDelegate = new UpdateProgressDelegate(this.UpdateProgress);
                updateDelegate.Invoke(this.manifestBuilder.Progress, this.manifestBuilder.ProgressStages, this.manifestBuilder.CurrentTask, false);
            }
        }


        private void UpdateProgress(int stage, int totalStages, string description, bool isError)
        {
            this.description = description;
            this.currProgressStages = stage;
            this.maxProgressStages = totalStages;
        }

        private void BuildThreadStart()
        {
            if (this.manifestBuilder != null)
            {
                // Synchronise the file list of manifest builder with the current package file list before build a new package
                foreach (KeyValuePair<string, FileData> pair in this.package.Files)
                {
                    this.manifestBuilder.AddFile(pair.Key);
                }

                // If the extraction tools is required, then copy the files to application directory first and then
                // add the files to manifest builder as well, otherwise we might need to remove the files from the 
                // manifest builder
                foreach (string path in packageToolPaths)
                {
                    string destinationPath = Path.Combine(this.package.Path, Path.GetFileName(path));
                    
                    if (this.includeExtractionTools)
                    {
                        File.Copy(path, destinationPath, true);
                        this.manifestBuilder.AddFile(destinationPath);
                    }
                    else
                    {
                        this.manifestBuilder.RemoveFile(Path.GetFileName(path));
                    }
                }
                
                if (string.IsNullOrEmpty(this.manifestBuilder.Executable))
                {
                    this.manifestBuilder.SetExecutable(this.executable);
                }

                this.manifestBuilder.SetUpdateType(this.updateType.Equals("complete") ? ManifestBuilder.UpdateTypeValues.Complete : ManifestBuilder.UpdateTypeValues.Partial);
                
                try
                {
                    this.manifestBuilder.BuildPackage();
                    this.manifestBuilder.Save(this.manifestBuilder.FileName);

                    // update the package version
                    this.build = this.manifestBuilder.Build.ToString();
                    this.revision = this.manifestBuilder.Revision.ToString();
                }
                catch (BuildException)
                {
                    // TODO: Call the updateprogress delegate here
                }
            }
        }

        private void SetDescription(string description)
        {
            XmlDocument descriptionDocument = new XmlDocument();

            XmlNode descriptionNode = descriptionDocument.CreateElement("Description");
            XmlNode headerNode = descriptionNode.AppendChild(descriptionDocument.CreateElement("Header"));
            XmlNode commentNode = descriptionNode.AppendChild(descriptionDocument.CreateElement("Comments"));

            headerNode.InnerXml = this.package.Name;
            commentNode.InnerXml = description;

            this.manifestBuilder.SetDescription(descriptionNode.InnerXml);
        }
    }
}
