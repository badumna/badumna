﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Germinator.PrestoManifesto
{
    [DataContract(Name = "Project")]
    public class Project
    {
        [DataMember(Name = "Applications")]
        private ObservableCollection<PackageData> mApplications = new ObservableCollection<PackageData>();
        public ObservableCollection<PackageData> Applications { get { return this.mApplications; } }

        private Dictionary<string, PackageControl> mPackageControls = new Dictionary<string, PackageControl>();
        public Dictionary<string, PackageControl> PackageControls
        {
            get { return this.mPackageControls; }
            set { this.mPackageControls = value; }
        }

        public event EventHandler SaveEvent;

        public Project() { }

        public void Save(string fileName)
        {
            if (this.SaveEvent != null)
            {
                this.SaveEvent(this, new EventArgs());
            }

            using (FileStream fileStream = new FileStream(fileName, FileMode.Create))
            {
                DataContractSerializer serializer = new DataContractSerializer(typeof(Project));

                serializer.WriteObject(fileStream, this);
            }
        }        
    }
}
