﻿//---------------------------------------------------------------------------------
// <copyright file="FileData.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
// <summary>File data class (used in PrestoManifesto).</summary>
//---------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Germinator.PrestoManifesto
{
    /// <summary>
    /// FileData class store the file name and the file time modification which is used in 
    /// Presto Manifesto.
    /// </summary>
    [DataContract(Name = "File")]
    public class FileData
    {
        /// <summary>
        /// Gets or sets the file name.
        /// </summary>
        /// <value>File name.</value>
        [DataMember(Name = "Name")]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the file modification time.
        /// </summary>
        /// <value>File modification time.</value>
        [DataMember(Name = "ModificationTime")]
        public DateTime ModificationTime { get; set; }
    }
}
