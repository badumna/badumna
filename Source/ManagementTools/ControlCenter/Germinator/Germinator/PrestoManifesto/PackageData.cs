﻿//---------------------------------------------------------------------------------
// <copyright file="PackageData.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
// <summary>Package data class (used in PrestoManifesto).</summary>
//---------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Germinator.PrestoManifesto
{
    /// <summary>
    /// Type of services, a package can be an arbitration service or non badumna service, it can be added later on if there is a new service
    /// to be serve by Control Center.
    /// </summary>
    public enum ServiceType
    {
        /// <summary>
        /// Arbitration service.
        /// </summary>
        ArbitrationService,

        /// <summary>
        /// Non badumna service (i.e. Unity headless application).
        /// </summary>
        NonBadumnaService
    }

    /// <summary>
    /// PackageData class store the package information such as the package name, package manifest file, list of files included in the 
    /// package, package directory path and output path. 
    /// </summary>
    [DataContract(Name = "Application")]
    public class PackageData
    {
        /// <summary>
        /// Package name.
        /// </summary>
        [DataMember(Name = "Name")]
        private string name = string.Empty;

        /// <summary>
        /// The package manifest file name.
        /// </summary>
        [DataMember(Name = "ManifestFile")]
        private string manifestFile = string.Empty;

        /// <summary>
        /// List of the files included in the package.
        /// </summary>
        [DataMember(Name = "Files")]
        private Dictionary<string, FileData> files = new Dictionary<string, FileData>();

        /// <summary>
        /// The package directory path.
        /// </summary>
        [DataMember(Name = "Path")]
        private string path;

        /// <summary>
        /// The package output directory path.
        /// </summary>
        [DataMember(Name = "PublishFolder")]
        private string publishFolder;

        /// <summary>
        /// The service type for this package.
        /// </summary>
        [DataMember(Name = "ServiceType")]
        private ServiceType serviceType;

        /// <summary>
        /// Initializes a new instance of the <see cref="PackageData"/> class.
        /// </summary>
        /// <param name="name">Package name.</param>
        /// <param name="path">Package directory path.</param>
        /// <param name="type">Package service type.</param>
        public PackageData(string name, string path, ServiceType type)
        {
            this.name = name;
            this.serviceType = type;
            this.path = System.IO.Path.Combine(path, name);
            this.publishFolder = System.IO.Path.GetFullPath(System.IO.Path.Combine(path, System.IO.Path.Combine(name, "publish")));
        }

        /// <summary>
        /// Gets the package name.
        /// </summary>
        /// <value>Package name.</value>
        public string Name
        {
            get { return this.name; }
        }
        
        /// <summary>
        /// Gets or sets the package manifest file name.
        /// </summary>
        /// <value>The package manifest file name.</value>
        public string ManifestFile
        {
            get { return this.manifestFile; }
            set { this.manifestFile = value; }
        }

        /// <summary>
        /// Gets the package directory path.
        /// </summary>
        /// <value>The package directory path.</value>
        public string Path 
        { 
            get { return this.path; }
        }

        /// <summary>
        /// Gets or sets the package output directory path.
        /// </summary>
        /// <value>The package output directory path.</value>
        public string PublishFolder
        {
            get { return this.publishFolder; }
            set { this.publishFolder = value; }
        }

        /// <summary>
        /// Gets or sets the list of files included in the package.
        /// </summary>
        /// <value>List of the files included in the package.</value>
        public Dictionary<string, FileData> Files
        {
            get { return this.files; }
            set { this.files = value; }
        }

        /// <summary>
        /// Gets or sets the package service type.
        /// </summary>
        /// <value>The package service type.</value>
        public ServiceType ServiceType
        {
            get { return this.serviceType; }
            set { this.serviceType = value; }
        }
    }
}
