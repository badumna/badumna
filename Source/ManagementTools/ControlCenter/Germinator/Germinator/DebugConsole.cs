﻿//-----------------------------------------------------------------------
// <copyright file="DebugConsole.cs" company="National ICT Australia Ltd">
//     Copyright (c) 2012 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Germinator
{
    using System.Collections.Generic;
    using System.Diagnostics;

    /// <summary>
    /// DebugConsole class is used to display the trace messages into the browser, 
    /// give the users more information about what is currently happening. 
    /// </summary>
    public class DebugConsole
    {
        /// <summary>
        /// Queue lock object.
        /// </summary>
        private object queueLock = new object();

        /// <summary>
        /// Log queue.
        /// </summary>
        private Queue<string> logQueue;

        /// <summary>
        /// Initializes static members of the <see cref="DebugConsole"/> class.
        /// </summary>
        /// <remarks>Static constructor.</remarks>
        static DebugConsole()
        {
            DebugConsole.Instance = new DebugConsole();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DebugConsole"/> class.
        /// </summary>
        /// <remarks>Default constructor.</remarks>
        public DebugConsole()
        {
            this.logQueue = new Queue<string>();
        }

        /// <summary>
        /// Gets the DebugConsole singleton.
        /// </summary>
        public static DebugConsole Instance { get; private set; }

        /// <summary>
        /// Enqueue the trace info into the log queue.
        /// </summary>
        /// <param name="traceInformation">Trace message.</param>
        public void EnqueueTraceInfo(string traceInformation)
        {
            this.EnqueueTraceInfo(traceInformation, TraceLevel.Info);
        }

        /// <summary>
        /// Enqueue the trace info to the log queue and trace it using System.Diagnostic.Trace.
        /// </summary>
        /// <param name="traceInformation">Trace message.</param>
        /// <param name="traceLevel">Trace level (use only for System.Diagnostic.Trace).</param>
        public void EnqueueTraceInfo(string traceInformation, TraceLevel traceLevel)
        {
            lock (this.queueLock)
            {
                this.logQueue.Enqueue(traceInformation);
            }

            switch (traceLevel)
            {
                case TraceLevel.Info:
                    Trace.TraceInformation(traceInformation);
                    break;
                case TraceLevel.Warning:
                    Trace.TraceWarning(traceInformation);
                    break;
                case TraceLevel.Error:
                    Trace.TraceError(traceInformation);
                    break;
            }
        }

        /// <summary>
        /// Dequeu the trace message from the queue.
        /// </summary>
        /// <returns>Return a message at the beginning of the queue.</returns>
        public string DequeueTraceInfo()
        {
            if (this.logQueue.Count > 0)
            {
                string log = string.Empty;
                lock (this.queueLock)
                {
                    log = this.logQueue.Dequeue();
                }

                return log;
            }
            else
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Count method return the number of log file currently on the queue.
        /// </summary>
        /// <returns>Returns the number of log file currently on the queue.</returns>
        public int Count()
        {
            return this.logQueue.Count;
        }
    }
}
