using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Germinator.Maintenance
{
    class BinaryHeap<T> where T : IComparable<T>
    {
        private ArrayList mItemArray;        
        public int Count
        {
            get { return this.mItemArray.Count -1; }
        }

        public BinaryHeap()
        {
            this.mItemArray = new ArrayList(100);
            this.mItemArray.Add(default(T));
        }

        public BinaryHeap(int capacity)
        {
            this.mItemArray = new ArrayList(capacity);
            this.mItemArray.Add(default(T));
        }

        public void Clear()
        {
            this.mItemArray.Clear();
            this.mItemArray.Add(default(T));
        }

        public void Push(T item)
        {
            this.mItemArray.Add(item);

            this.BubbleUp(this.mItemArray.Count - 1);
        }

        protected int BubbleUp(int bubbleIndex)
        {
            while (bubbleIndex != 1)
            {
                int parentIndex = bubbleIndex / 2;

                if (((T)this.mItemArray[bubbleIndex]).CompareTo((T)this.mItemArray[parentIndex]) <= 0)
                {
                    T tmpItem = (T)this.mItemArray[parentIndex];

                    this.mItemArray[parentIndex] = this.mItemArray[bubbleIndex];
                    this.mItemArray[bubbleIndex] = tmpItem;
                    bubbleIndex = parentIndex;
                }
                else
                {
                    break;
                }
            }

            return bubbleIndex;
        }

        public T Pop()
        {
            if (this.Count <= 0)
            {
                return default(T);
            }
           
            T returnItem = (T)this.mItemArray[1];

            this.mItemArray[1] = this.mItemArray[this.mItemArray.Count-1];
            this.mItemArray.RemoveAt(this.mItemArray.Count - 1);

            if (this.Count > 0)
            {
                this.BubbleDown(0, 1);
            }

            return returnItem;
        }

        protected int BubbleDown(int parentIndex, int swapIndex)
        {
            int childIndex;
            int nextChildIndex;
            int itemCount = this.mItemArray.Count;

            while (parentIndex != swapIndex)
            {
                parentIndex = swapIndex;
                childIndex = parentIndex * 2;
                nextChildIndex = childIndex + 1;

                T swapItem = default(T);

                if (childIndex < itemCount)
                {
                    swapItem = (T)this.mItemArray[parentIndex];

                    if (swapItem.CompareTo((T)this.mItemArray[childIndex]) >= 0)
                    {
                        swapIndex = childIndex;
                    }
                }

                if (nextChildIndex < itemCount)
                {
                    if (((T)this.mItemArray[swapIndex]).CompareTo((T)this.mItemArray[nextChildIndex]) >= 0)
                    {
                        swapIndex = nextChildIndex;
                    }
                }

                if (parentIndex != swapIndex)  // If this is true then we entered the first block above and swapItem has been set (nextChildIndex < itemCount) => (childIndex < itemCount)
                {
                    this.mItemArray[parentIndex] = this.mItemArray[swapIndex];
                    this.mItemArray[swapIndex] = swapItem;
                }
            }

            return swapIndex;
        }

        public T Peek()
        {
            if (this.Count <= 0)
            {
                return default(T);
            }

            return (T)this.mItemArray[1];
        }

#if DEBUG
        public bool Contains(T obj)
        {
            for (int i = 1; i < this.mItemArray.Count; i++)
            {
                if (((T)this.mItemArray[i]).Equals(obj))
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// This is very inefficient and should be used only for debug purposes.
        /// </summary>
        public List<T> GetQueue()
        {
            List<T> list = new List<T>();


            for (int i = 1; i < this.mItemArray.Count; i++)  // Skip inital default(T) value
            {
                list.Add((T)this.mItemArray[i]);
            }
            
            list.Sort();
            
            return list;
        }

#endif

    }
}
