﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Germinator.Maintenance
{
    class MaintenanceTask : IComparable<MaintenanceTask>
    {
        public delegate bool MaintanceDelegate();
        public TimeSpan Interval
        {
            get { return this.mInterval; }
        }

        internal DateTime TimeOfNextTask { get; private set; }
        internal volatile bool IsEnabled;

        private TimeSpan mInterval;
        private MaintanceDelegate mTask;

        internal MaintenanceTask(MaintanceDelegate task, TimeSpan interval)
        {
            this.mTask = task;
            this.mInterval = interval;
            this.IsEnabled = true;
        }

        internal MaintenanceTask(MaintenanceTask clone, TimeSpan interval)
        {
            this.mTask = clone.mTask;
            this.mInterval = interval;
            this.IsEnabled = clone.IsEnabled;
        }

        public bool PerformTask()
        {
            this.TimeOfNextTask = DateTime.Now + this.mInterval;
            if (this.mTask != null)
            {
                return this.mTask();
            }

            return false;
        }

        public int CompareTo(MaintenanceTask other)
        {
            return this.TimeOfNextTask.CompareTo(other.TimeOfNextTask);
        }

    }
}
