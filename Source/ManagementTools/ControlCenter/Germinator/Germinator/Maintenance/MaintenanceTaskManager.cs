﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace Germinator.Maintenance
{
    public class MaintenanceTaskManager
    {
        public static MaintenanceTaskManager Instance = new MaintenanceTaskManager();

        private BinaryHeap<MaintenanceTask> mTaskHeap;
        private object mHeapLock = new object();
        private Thread mThread;
        private volatile bool mContinue;

        private MaintenanceTaskManager()
        {
            this.mTaskHeap = new BinaryHeap<MaintenanceTask>();
        }

        internal MaintenanceTask AddTask(MaintenanceTask.MaintanceDelegate task, TimeSpan interval)
        {
            MaintenanceTask maintenaceTask = new MaintenanceTask(task, interval);
            lock (this.mHeapLock)
            {
                this.mTaskHeap.Push(maintenaceTask);
            }
            return maintenaceTask;
        }

        internal MaintenanceTask SetInterval(MaintenanceTask task, TimeSpan interval)
        {
            MaintenanceTask clonedTask = new MaintenanceTask(task, interval);
            lock (this.mHeapLock)
            {
                task.IsEnabled = false;
                this.mTaskHeap.Push(clonedTask);
            }
            return clonedTask;
        }

        internal void RemoveTask(MaintenanceTask task)
        {
            task.IsEnabled = false;
        }

        public void StartMaintenanceThread()
        {
            if (this.mThread != null && this.mThread.IsAlive)
            {
                return;
            }

            this.mContinue = true;
            this.mThread = new Thread(this.MaintenanceLoop);
            this.mThread.IsBackground = true;
            this.mThread.Start();
        }

        public void StopMaintenanceThread()
        {
            this.mContinue = false;
        }

        private void MaintenanceLoop()
        {
            while (this.mContinue)
            {
                int heapCount = 0;
                lock (this.mHeapLock)
                {
                    heapCount = this.mTaskHeap.Count;
                }
                if (heapCount > 0)
                {
                    MaintenanceTask nextTask = null;

                    lock (this.mHeapLock)
                    {
                        nextTask = this.mTaskHeap.Pop();
                    }
                    if (nextTask.IsEnabled)
                    {
                        TimeSpan delay = nextTask.TimeOfNextTask - DateTime.Now;
                        if (delay > TimeSpan.Zero)
                        {
                            for (int i = 0; i < (int)delay.TotalSeconds; i++)
                            {
                                Thread.Sleep(1000);
                                if (!nextTask.IsEnabled)
                                {
                                    break;
                                }
                            }
                        }

                        if (nextTask.IsEnabled)
                        {
                            bool pushBackOnHeap = nextTask.PerformTask();
                            if (pushBackOnHeap)
                            {
                                lock (this.mHeapLock)
                                {
                                    this.mTaskHeap.Push(nextTask);
                                }
                            }
                        }
                    }
                }
                else
                {
                    Thread.Sleep(TimeSpan.FromSeconds(1));
                }
            }
        }
    }
}
