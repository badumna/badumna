﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Germinator
{
    public interface IProvider<T> 
    {
        void Add(T dataItem);
        void Remove(string dataItemIndex);

        void Save(T dataItem);
        IEnumerable<T> Load();
    }
}
