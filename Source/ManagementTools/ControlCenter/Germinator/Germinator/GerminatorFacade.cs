﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.IO;

using Germinator.Components;
using Germinator.Maintenance;
using Germinator.Services;
using GermServer;
using System.Security.Cryptography.X509Certificates;

namespace Germinator
{
    public class GerminatorFacade
    {
        public static GerminatorFacade Instance;

        public int ConfigurationCount { get { return this.mConfigurations.Count; } }
        public int GermClientCount 
        { 
            get 
            {
                if (this.ConfigurationCount != 1)
                {
                    return 0;
                }
                else
                {
                    return this.GetAllConfigurations().First<Configuration>().Germs.Count;
                }
            } 
        }

        private IProvider<Configuration> mConfigurationProvider;
        private Dictionary<string, Configuration> mConfigurations;

        private MemoryStream mLogFileStream;

        private GerminatorFacade(IProvider<Configuration> configurationProvider, IGermProvider germProvider)
        {
            this.EnableLogging();
            this.mConfigurations = new Dictionary<string, Configuration>();
            this.mConfigurationProvider = configurationProvider;
            foreach (Configuration configuration in configurationProvider.Load())
            {
                this.mConfigurations.Add(configuration.Name, configuration);
            }

            // HACK: to enforce only one germ client container for the entire Control Center, 
            // remove the mGerms from GerminatorFacade and use mGerms from Configuration instead. 
            // TODO: for the next release we need to get rid of GerminatorFacade.

            //this.PostLoad(); // Call the PostLoad method after successfully created GerminatorFacade.Instance
        }

        public static void Initialize(IProvider<Configuration> configurationProvider, IGermProvider germProvider, X509Certificate2 certificate)//, string packageDirectory, string componentDirectory)
        {
            if (configurationProvider == null)
            {
                throw new ArgumentNullException("configurationProvider");
            }

            // TODO: we no longer need GermProvider.
            ////if (germProvider == null)
            ////{
            ////    throw new ArgumentNullException("germProvider");
            ////}

            GermClientContainer.Certificate = certificate;

            ComponentContainer.Instance.FindDefaultComponents();
            MaintenanceTaskManager.Instance.StartMaintenanceThread();

            GerminatorFacade.Instance = new GerminatorFacade(configurationProvider, germProvider);
            GerminatorFacade.Instance.PostLoad();
        }

        public void EnableLogging() // TODO : Need to collect log stream so it doesn't consume too much memory.
        {
            Trace.Listeners.Clear();

            this.mLogFileStream = new MemoryStream();
            XmlWriterTraceListener traceListener = new XmlWriterTraceListener(this.mLogFileStream);
            traceListener.TraceOutputOptions = TraceOptions.DateTime | TraceOptions.Timestamp;
            Trace.Listeners.Add(traceListener);
            Trace.AutoFlush = true; 
        }

        public string GetLog()
        {
            if (this.mLogFileStream.CanRead)
            {
                StreamReader reader = new StreamReader(this.mLogFileStream);
                this.mLogFileStream.Position = 0;
                return String.Format(@"<?xml version=""1.0"" encoding=""utf-8""?><Log>{0}</Log>", reader.ReadToEnd());
            }

            return "";
        }

        public Configuration AddConfiguration(string name, string protocolHash)
        {
            Configuration configuration = null;
            if (!this.mConfigurations.TryGetValue(name, out configuration))
            {
                configuration = new Configuration(name, protocolHash, this.mConfigurationProvider);
                this.mConfigurations.Add(name, configuration);
                this.mConfigurationProvider.Save(configuration);
            }

            return configuration;
        }

        public void RemoveConfiguration(string name)
        {
            if (this.mConfigurations.ContainsKey(name))
            {
                this.mConfigurations.Remove(name);
                this.mConfigurationProvider.Remove(name);
            }
        }

        public Configuration GetConfiguration(string name)
        {
            Configuration configuration = null;
            this.mConfigurations.TryGetValue(name, out configuration);
            return configuration;
        }

        public IEnumerable<Configuration> GetAllConfigurations()
        {
            return this.mConfigurations.Values;
        }

        public GermClient AddGerm(string hostname)
        {
            if(this.ConfigurationCount != 1)
            {
                 return null;
            }


            GermClient germ = this.GetAllConfigurations().First<Configuration>().AddGerm(hostname);
            return germ;
        }

        public void RemoveGerm(string hostname)
        {
            if (this.ConfigurationCount == 1)
            {
                this.GetAllConfigurations().First<Configuration>().RemoveGerm(hostname);
            }
        }        

        [Obsolete("Shouldn't use this method anymore, use Configuration.GetGermClient() method instead")]
        public GermClient GetGermClient(string hostname)
        {
            if (this.ConfigurationCount != 1)
            {
                return null;
            }

            return this.GetAllConfigurations().First<Configuration>().Germs.GetGerm(hostname);
        }

        public IEnumerable<GermClient> GetAllGermClients()
        {
            if (this.ConfigurationCount != 1)
            {
                return null;
            }

            return this.GetAllConfigurations().First<Configuration>().Germs.AsEnumerable();
        }

        public ComponentView GetComponent(string name)
        {
            return ComponentContainer.Instance[name];
        }

        public IEnumerable<ComponentView> GetAllComponents()
        {
            return ComponentContainer.Instance.AsEnumerable();
        }

        public ServicePackageDescription GetPackage(string name)
        {
            return PackageManager.Instance.GetPackage(name);
        }

        public IEnumerable<ServicePackageDescription> GetAllPackages()
        {
            return PackageManager.Instance.GetAllPackages();
        }

        internal void CollectEscapedProcesses(Configuration configuration, GermClient client)
        {
            try
            {
                Dictionary<string, List<ProcessDescription>> alreadyAttachedProcesses = this.ListAllProcesses(client);
                if (alreadyAttachedProcesses != null)
                {
                    this.ReconnectGerms(configuration, alreadyAttachedProcesses);
                }
            }
            catch (Exception e)
            {
                DebugConsole.Instance.EnqueueTraceInfo(string.Format("Failed to attach running processes : {0}", e.Message), TraceLevel.Error);
            }
        }

        //TODO :recheck the protection level for this function
        public void PostLoad()
        {
            // TODO: move this check somewhere else -- this.updateGerm()
            // Make sure that all germs in global list are added into the network configuration
            //foreach (Configuration configuration in this.mConfigurations.Values)
            //{
            //    foreach (GermClient client in this.mGerms.AsEnumerable())
            //    {
            //        if (!configuration.ContainGerm(client))
            //        {
            //            configuration.AddGerm(client.Name);
            //        }
            //    }
            //}

            Dictionary<string, List<ProcessDescription>> runningProcesses = this.ListAllProcesses();

            foreach (Configuration configuration in this.mConfigurations.Values)
            {
                // Check if any new components have been found. If so, add them (dis-abled) to each configuration
                foreach (ComponentView component in ComponentContainer.Instance.AsEnumerable())
                {
                    if (configuration.GetComponent(component.Index) == null)
                    {
                        ComponentView addedComponent = configuration.AddComponent(component.Index);
                        if (addedComponent != null)
                        {
                            configuration.EnableComponent(addedComponent, false);
                        }
                    }
                }

                this.ReconnectGerms(configuration, runningProcesses);
                configuration.UpdateRunningServices();
            }
        }

        private void ReconnectGerms(Configuration configuration, Dictionary<string, List<ProcessDescription>> runningProcesses)
        {
            // For each running process try to attach it to a particular service 
            if (configuration.GermClients != null)
            {
                foreach (GermClient client in configuration.GermClients)
                {
                    List<ProcessDescription> processes = null;
                    if (runningProcesses.TryGetValue(client.Name, out processes))
                    {
                        foreach (ProcessDescription process in processes)
                        {
                            BaseService service = configuration.GetService(process.Name);
                            if (service != null)
                            {
                                service.CommandeerInstance(client, process);
                            }
                        }
                    }
                }
            }

            //// Call start for all remaining enabled services. 
            //// Start a remote process only if one is not already running.
            //// (Obselete)
            
            //foreach (ComponentView component in configuration.Components)
            //{
            //    BaseService service = component.Instance as BaseService;
            //    if (service != null && service.IsEnabled)
            //    {
            //        configuration.StartService(service);
            //    }
            //}
        }

        private Dictionary<string, List<ProcessDescription>> ListAllProcesses()
        {
            bool addToListFlag = true;
            object processListLock = new object();
            Dictionary<string, List<ProcessDescription>> runningProcesses = new Dictionary<string, List<ProcessDescription>>();

            // Contact all the germs and get their list of running processes.
            List<IAsyncResult> operationResults = new List<IAsyncResult>();

            if (this.ConfigurationCount == 1)
            {
                foreach (GermClient client in this.GetAllConfigurations().First<Configuration>().Germs.AsEnumerable())
                {
                    ProcessListOperation operation = client.QueueProcessListOperation(
                        delegate(IAsyncResult ar)
                        {
                            ProcessListOperation listProcessesOperation = ar as ProcessListOperation;
                            if (listProcessesOperation != null && listProcessesOperation.IsCompleted && listProcessesOperation.RunningProcesses != null)
                            {
                                lock (processListLock)
                                {
                                    if (addToListFlag)
                                    {
                                        List<ProcessDescription> processes = new List<ProcessDescription>(listProcessesOperation.RunningProcesses);
                                        runningProcesses[(string)ar.AsyncState] = processes;
                                    }
                                }
                            }
                        }, client.Name);
                    operationResults.Add(operation);
                }

                // Wait for all the operations to complete.
                int totalWaitTimeRemainingMs = GermClient.DefaultOperationTimeout * 2;
                foreach (IAsyncResult result in operationResults)
                {
                    if (result != null && !result.IsCompleted)
                    {
                        DateTime startWait = DateTime.Now;
                        if (!result.AsyncWaitHandle.WaitOne(totalWaitTimeRemainingMs))
                        {
                            // The wait timed out, don't wait for any longer.
                            totalWaitTimeRemainingMs = 0;
                            break;
                        }
                        totalWaitTimeRemainingMs -= (int)(DateTime.Now - startWait).TotalMilliseconds;
                        if (totalWaitTimeRemainingMs <= 0)
                        {
                            break;
                        }
                    }
                }

                lock (processListLock)
                {
                    addToListFlag = false; // Set this flag so that any operations that unexpectedly finish do not add to the process list.
                }
            }

            return runningProcesses;
        }

        private Dictionary<string, List<ProcessDescription>> ListAllProcesses(GermClient client)
        {
            ProcessListOperation operation = client.QueueProcessListOperation(null, null);
            if (operation != null)
            {
                operation.AsyncWaitHandle.WaitOne(GermClient.DefaultOperationTimeout);
                if (operation.IsCompleted && operation.RunningProcesses != null)
                {
                    Dictionary<string, List<ProcessDescription>> runningProcesses = new Dictionary<string, List<ProcessDescription>>();
                    runningProcesses.Add(client.Name, operation.RunningProcesses);
                    return runningProcesses;
                }
            }

            return null;
        }


    }
}
