﻿//---------------------------------------------------------------------------------
// <copyright file="LoggingSubComponent.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
// <summary>Logging subcomponent class.</summary>
//---------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;

using Germinator;

namespace Germinator.Components
{
    /// <summary>
    /// Logging subcomponent is part of the badumna network configruation module.
    /// </summary>
    public class LoggingSubComponent : BaseComponent
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LoggingSubComponent"/> class.
        /// </summary>
        public LoggingSubComponent()
            : base("Logging", "Logging")
        {
            this.IsEnabled = false;
            this.IsLogToConsoleEnabled = false;
            this.IsLogToFileEnabled = false;
            this.IsLog4NetEnabled = false;
            this.Log4NetHost = string.Empty;
        }

        /// <summary>
        /// Gets or sets the logging verbosity.
        /// </summary>
        [ComponentProperty("Logging verbosity", "Error", "Warning", "Information", "Verbose")]
        public string Verbosity { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the default trace is enabled.
        /// </summary>
        [ComponentProperty("Trace default trace messages")]
        public bool IsDefaultTraceEnabled { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the log to file is enabled.
        /// </summary>
        [ComponentProperty("Log to file")]
        public bool IsLogToFileEnabled { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the log to console is enabled.
        /// </summary>
        [ComponentProperty("Log to console")]
        public bool IsLogToConsoleEnabled { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the log4net is used.
        /// </summary>
        [ComponentProperty("Use Log4Net")]
        public bool IsLog4NetEnabled { get; set; }

        /// <summary>
        /// Gets or sets log4net host name.
        /// </summary>
        [ComponentProperty("Log4Net host name")]
        public string Log4NetHost { get; set; }

        /// <summary>
        /// Gets or sets the log4net port number.
        /// </summary>
        [ComponentProperty("Log4Net port")]
        public int Log4NetPort { get; set; }

        /// <summary>
        /// Build the the network config file for the logging module.
        /// </summary>
        /// <returns>Rerurns the configuraiton of logging module.</returns>
        internal string BuildConfigXml()
        {
            return string.Format(
                Germinator_Resource.LogModuleFormat,
                this.IsEnabled.ToString().ToLower(),
                this.IsDefaultTraceEnabled,
                this.Verbosity,
                this.IsLogToFileEnabled,
                string.Empty, // Use default file name
                this.IsLogToConsoleEnabled,
                "false", // Log to chat
                this.IsLog4NetEnabled,
                this.Log4NetHost,
                this.Log4NetPort);
        }

        /// <summary>
        /// Set the component back to the default value.
        /// </summary>
        internal override void DefaultValue()
        {
            this.IsEnabled = false;
            this.IsLogToConsoleEnabled = false;
            this.IsLogToFileEnabled = false;
            this.IsLog4NetEnabled = false;
            this.Log4NetHost = string.Empty;
        }
    }
}
