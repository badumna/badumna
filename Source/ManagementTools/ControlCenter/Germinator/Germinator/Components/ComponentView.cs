﻿//-----------------------------------------------------------------------
// <copyright file="ComponentView.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;

namespace Germinator.Components
{
    /// <summary>
    /// Delegate that should be called when there is a property change in the component view.
    /// </summary>
    /// <param name="propertyIndex">Property index name.</param>
    public delegate void OnPropertyChangeDelegate(string propertyIndex);

    /// <summary>
    /// Property type.
    /// </summary>
    public enum PropertyType
    {
        /// <summary>
        /// Property type: int.
        /// </summary>
        Int,

        /// <summary>
        /// Property type: string.
        /// </summary>
        String,

        /// <summary>
        /// Property type: string option.
        /// </summary>
        StringOption,

        /// <summary>
        /// Property type: string list.
        /// </summary>
        StringList,

        /// <summary>
        /// Property type: boolean.
        /// </summary>
        Bool,

        /// <summary>
        /// Property type: file.
        /// </summary>
        File,

        /// <summary>
        /// Property type: subcomponent.
        /// </summary>
        SubComponent
    }

    /// <summary>
    /// PropertyView class store information a property of a component.
    /// </summary>
    public class PropertyView
    {
        /// <summary>
        /// Types of the property.
        /// </summary>
        public PropertyType Type;

        /// <summary>
        /// Property display name.
        /// </summary>
        public string Name;

        /// <summary>
        /// Property index name.
        /// </summary>
        public string Index;

        /// <summary>
        /// Possible values.
        /// </summary>
        public IEnumerable<string> PossibleValues;

        /// <summary>
        /// Property default value.
        /// </summary>
        public object Default;

        /// <summary>
        /// Property value.
        /// </summary>
        public object Value;

        /// <summary>
        /// Property info.
        /// </summary>
        internal PropertyInfo PropertyInfo;
    }

    /// <summary>
    /// ComponentView class store the information of all the component properties.
    /// </summary>
    public class ComponentView : IEnumerable<PropertyView>
    {
        /// <summary>
        /// Component properties.
        /// </summary>
        private Dictionary<string, PropertyView> properties = new Dictionary<string, PropertyView>();

        /// <summary>
        /// BaseComponent instance.
        /// </summary>
        private BaseComponent instance;

        /// <summary>
        /// Initializes a new instance of the <see cref="ComponentView"/> class.
        /// </summary>
        /// <param name="manager">Component container.</param>
        /// <param name="name">Component name.</param>
        /// <param name="instance">Component as base component.</param>
        internal ComponentView(ComponentContainer manager, string name, BaseComponent instance)
            : this(manager, string.Empty, name, instance)
        { 
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ComponentView"/> class.
        /// </summary>
        /// <param name="manager">Component container.</param>
        /// <param name="parentName">Component parent name.</param>
        /// <param name="displayName">Component display name.</param>
        /// <param name="instance">Component as base component.</param>
        internal ComponentView(ComponentContainer manager, string parentName, string displayName, BaseComponent instance)
        {
            this.DisplayName = displayName;
            this.Index = instance.Index;
            this.ParentName = parentName;
            this.instance = instance;
            Type componentType = instance.GetType();

            PropertyInfo[] properties = componentType.GetProperties();
            this.ConstructProperty(properties, manager);
        }

        /// <summary>
        /// Property changed event.
        /// </summary>
        public event OnPropertyChangeDelegate PropertyChangedEvent;

        /// <summary>
        /// Gets component display name.
        /// </summary>
        public string DisplayName { get; private set; }

        /// <summary>
        /// Gets component index name.
        /// </summary>
        public string Index { get; private set; }

        /// <summary>
        /// Gets component parent name.
        /// </summary>
        public string ParentName { get; private set; }

        /// <summary>
        /// Gets the BaseComponent instance.
        /// </summary>
        public BaseComponent Instance
        {
            get { return this.instance; }
        }
        
        /// <summary>
        /// Upload a file.
        /// </summary>
        /// <param name="propertyIndex">Property index.</param>
        /// <param name="directory">Directory name.</param>
        /// <param name="fileName">File name.</param>
        /// <returns>Return the upload path.</returns>
        public string UploadFile(string propertyIndex, string directory, string fileName)
        {
            PropertyView property = null;
            if (this.properties.TryGetValue(propertyIndex, out property))
            {
                if (property.Type == PropertyType.File)
                {
                    FileUploader uploader = (FileUploader)property.PropertyInfo.GetValue(this.instance, null);
                    if (uploader != null)
                    {
                        string uploadPath = uploader.CanUpload(directory, fileName);
                        if (!string.IsNullOrEmpty(uploadPath))
                        {
                            try
                            {
                                if (!Directory.Exists(Path.GetDirectoryName(uploadPath)))
                                {
                                    Directory.CreateDirectory(Path.GetDirectoryName(uploadPath));
                                }

                                return uploadPath;
                            }
                            catch 
                            { 
                            }
                        }
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Get the component view as enumerator.
        /// </summary>
        /// <returns>Returns the Componen properties as enumerator.</returns>
        public IEnumerator<PropertyView> GetEnumerator()
        {
            return this.properties.Values.GetEnumerator();
        }

        /// <summary>
        /// Get the component view as enumerator.
        /// </summary>
        /// <returns>Returns the Componen properties as enumerator.</returns>
        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return this.properties.Values.GetEnumerator();
        }

        /// <summary>
        /// Set the component property.
        /// </summary>
        /// <param name="propertyIndex">Property index name.</param>
        /// <param name="value">Property value.</param>
        public void SetProperty(string propertyIndex, object value)
        {
            if (value == null || string.IsNullOrEmpty(propertyIndex))
            {
                DebugConsole.Instance.EnqueueTraceInfo("Invalid property index or value.", System.Diagnostics.TraceLevel.Error);
                return;
            }

            PropertyView property = null;
            if (this.properties.TryGetValue(propertyIndex, out property))
            {
                try
                {
                    if (property.Type == PropertyType.Int)
                    {
                        value = int.Parse(value as string);
                    }

                    if (property.Type == PropertyType.Bool)
                    {
                        value = (value as string).ToLower().StartsWith("true");
                    }

                    if (property.Type == PropertyType.StringList)
                    {
                        value = (value as string).Split(new char[] { ',', '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries);
                        
                        // HACK: update the possible values. 
                        property.PossibleValues = (string[])value;
                    }

                    property.PropertyInfo.SetValue(this.instance, value, null);
                    property.Value = value;

                    if (this.PropertyChangedEvent != null)
                    {
                        this.PropertyChangedEvent.Invoke(propertyIndex);
                    }
                }
                catch (Exception e)
                {
                    DebugConsole.Instance.EnqueueTraceInfo(string.Format("Failed to set property {1} : {0}", e.Message, propertyIndex), System.Diagnostics.TraceLevel.Error);
                }
            }
        }

        /// <summary>
        /// Get the component property.
        /// </summary>
        /// <param name="propertyIndex">Property index name.</param>
        /// <param name="value">Out the property value.</param>
        public void GetProperty(string propertyIndex, out object value)
        {
            value = null;

            if (string.IsNullOrEmpty(propertyIndex))
            {
                DebugConsole.Instance.EnqueueTraceInfo("Invalid property index.", System.Diagnostics.TraceLevel.Error);
                return;
            }

            PropertyView propertyView = null;
            if (this.properties.TryGetValue(propertyIndex, out propertyView))
            {
                try
                {
                    value = propertyView.Value;
                }
                catch (Exception e)
                {
                    DebugConsole.Instance.EnqueueTraceInfo(string.Format("Failed to get property {1} : {0}", e.Message, propertyIndex), System.Diagnostics.TraceLevel.Error);
                }
            }
        }

        /// <summary>
        /// Construct a component view given the component container.
        /// </summary>
        /// <param name="container">Component container.</param>
        /// <returns>Return the component view.</returns>
        internal ComponentView Construct(ComponentContainer container)
        {
            string name = this.DisplayName;
            ////BaseComponent instance = Activator.CreateInstance(this.mInstance.GetType()) as BaseComponent;
            BaseComponent instance = this.Instance as BaseComponent;
            if (instance != null)
            {
                return new ComponentView(container, name, instance);
            }

            return null;
        }

        /// <summary>
        /// Set possible values.
        /// </summary>
        /// <param name="propertyIndex">Property index name.</param>
        /// <param name="possibleValues">Possible property values.</param>
        internal void SetPossibleValues(string propertyIndex, IEnumerable<string> possibleValues)
        {
            PropertyView property = null;
            if (this.properties.TryGetValue(propertyIndex, out property))
            {
                property.PossibleValues = new List<string>(possibleValues);
            }
        }

        /// <summary>
        /// Update the property.
        /// </summary>
        internal void UpdateProperty()
        {
            Type componentType = this.instance.GetType();

            PropertyInfo[] properties = componentType.GetProperties();
            this.ConstructProperty(properties, null);            
        }

        /// <summary>
        /// Construct property
        /// </summary>
        /// <param name="properties">Property info.</param>
        /// <param name="manager">Component container.</param>
        internal void ConstructProperty(PropertyInfo[] properties, ComponentContainer manager)
        {
            foreach (PropertyInfo propertyInfo in properties)
            {
                PropertyView view = new PropertyView();

                object[] propertyAttributes = propertyInfo.GetCustomAttributes(typeof(ComponentPropertyAttribute), true);
                if (propertyAttributes.Length > 0)
                {
                    ComponentPropertyAttribute attribute = propertyAttributes[0] as ComponentPropertyAttribute;

                    view.PropertyInfo = propertyInfo;
                    view.Index = propertyInfo.Name;
                    if (propertyInfo.PropertyType == typeof(int))
                    {
                        view.Type = PropertyType.Int;
                    }

                    if (propertyInfo.PropertyType == typeof(string))
                    {
                        if (attribute.Values != null)
                        {
                            view.Type = PropertyType.StringOption;
                            view.PossibleValues = attribute.Values;
                            if (attribute.Values.Count > 0)
                            {
                                if (propertyInfo.GetValue(this.instance, null) == null)
                                {
                                    propertyInfo.SetValue(this.instance, attribute.Values[0], null);
                                }
                            }
                        }
                        else
                        {
                            view.Type = PropertyType.String;
                        }
                    }

                    if (propertyInfo.PropertyType == typeof(string[]))
                    {
                        view.Type = PropertyType.StringList;

                        string[] values = (string[])propertyInfo.GetValue(this.instance, null);
                        if (values != null)
                        {
                            view.PossibleValues = new List<string>(values);
                        }
                        else
                        {
                            view.PossibleValues = new List<string>();
                        }
                    }

                    if (propertyInfo.PropertyType == typeof(bool))
                    {
                        view.Type = PropertyType.Bool;
                    }

                    if (propertyInfo.PropertyType == typeof(FileUploader))
                    {
                        view.Type = PropertyType.File;
                    }

                    if (attribute.Name != null)
                    {
                        view.Name = attribute.Name;
                    }
                    else
                    {
                        view.Name = propertyInfo.Name;
                    }

                    view.Default = propertyInfo.GetValue(this.instance, null);
                    view.Value = view.Default;

                    if (this.properties.ContainsKey(view.Index))
                    {
                        this.properties[view.Index] = view;
                    }
                    else
                    {
                        this.properties.Add(view.Index, view);
                    }
                }

                if (manager != null)
                {
                    propertyAttributes = propertyInfo.GetCustomAttributes(typeof(SubComponentAttribute), true);
                    if (propertyAttributes.Length > 0)
                    {
                        SubComponentAttribute attribute = propertyAttributes[0] as SubComponentAttribute;

                        view.PropertyInfo = propertyInfo;
                        view.Type = PropertyType.SubComponent;

                        if (attribute.Name != null)
                        {
                            view.Name = attribute.Name;
                        }
                        else
                        {
                            view.Name = propertyInfo.Name;
                        }

                        view.Default = propertyInfo.GetValue(this.instance, null);
                        view.Value = view.Default;

                        BaseComponent subComponent = view.Value as BaseComponent;
                        if (subComponent != null)
                        {
                            view.Index = subComponent.Index;
                            this.properties.Add(view.Index, view);
                            manager.AddSubComponent(this.DisplayName, view.Name, subComponent);
                        }
                    }
                }
            }
        }
    }
}
