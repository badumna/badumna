﻿//-----------------------------------------------------------------------
// <copyright file="ApplicationComponent.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Germinator.Components
{
    /// <summary>
    /// Currently ApplicationComponent only store the application name which will be used to 
    /// initialize the badumna network. Hence, Only badumna peers initialized with the same
    /// application name will be able to connect to each other. 
    /// </summary>
    public class ApplicationComponent : BaseComponent
    {
        /// <summary>
        /// The component index.
        /// </summary>
        private static string componentIndex = "ApplicationComponent";
        
        /// <summary>
        /// Initializes a new instance of the <see cref="ApplicationComponent"/> class.
        /// </summary>
        public ApplicationComponent()
            : base(ApplicationComponent.componentIndex, "Application Setting")
        {
            this.ApplicationName = string.Empty;
        }

        /// <summary>
        /// Gets the ComponentIndex property.
        /// </summary>
        public static string ComponentIndex
        {
            get { return componentIndex; }
        }

        /// <summary>
        /// Gets or sets the application name.
        /// </summary>
        [XmlElement("ApplicationName")]
        [ComponentProperty("Application Name")]
        public string ApplicationName { get; set; }

        /// <summary>
        /// Set the component back to the default value.
        /// </summary>
        internal override void DefaultValue()
        {
            this.ApplicationName = string.Empty;
        }
    }
}
