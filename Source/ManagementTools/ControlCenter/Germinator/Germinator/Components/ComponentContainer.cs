﻿//-----------------------------------------------------------------------
// <copyright file="ComponentContainer.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using Germinator.Services;

namespace Germinator.Components
{
    /// <summary>
    /// ComponentContainer class contains a collection of all components that are available.
    /// </summary>
    [XmlRoot(ElementName = "Components", Namespace = "")]
    public class ComponentContainer : IXmlSerializable
    {
        /// <summary>
        /// Instances of ComponentContainer.
        /// </summary>
        private static ComponentContainer instance = new ComponentContainer();

        /// <summary>
        /// Collection of components.
        /// </summary>
        private Dictionary<string, ComponentView> components;

        /// <summary>
        /// Collection of all subcomponent.
        /// </summary>
        private Dictionary<string, ComponentView> subComponents;

        /// <summary>
        /// Initializes a new instance of the <see cref="ComponentContainer"/> class.
        /// </summary>
        internal ComponentContainer()
        {
            this.components = new Dictionary<string, ComponentView>();
            this.subComponents = new Dictionary<string, ComponentView>();
        }

        /// <summary>
        /// Gets the component container instance.
        /// </summary>
        public static ComponentContainer Instance
        {
            get { return ComponentContainer.instance; }
        }

        /// <summary>
        /// Gets the all components name.
        /// </summary>
        public IEnumerable<string> ComponentNames
        {
            get { return this.components.Keys; }
        }

        /// <summary>
        /// Get the component view of given component name.
        /// </summary>
        /// <param name="componentName">Component name.</param>
        /// <returns>Return the component view of given component name.</returns>
        public ComponentView this[string componentName]
        {
            get
            {
                if (string.IsNullOrEmpty(componentName))
                {
                    return null;
                }

                ComponentView view = null;
                if (this.components.TryGetValue(componentName, out view))
                {
                    return view;
                }

                if (this.subComponents.TryGetValue(componentName, out view))
                {
                    return view;
                }

                return null;
            }
        }

        /// <summary>
        /// Add components to the list.
        /// </summary>
        /// <param name="componentName">Component name.</param>
        /// <returns>Return the component view of the new component.</returns>
        public ComponentView Add(string componentName)
        {
            if (string.IsNullOrEmpty(componentName))
            {
                return null;
            }

            ComponentView view = null;
            if (this.components.TryGetValue(componentName, out view))
            {
                return view;
            }

            ComponentView staticView = ComponentContainer.Instance[componentName];
            if (staticView == null)
            {
                return null;
            }

            view = staticView.Construct(this);
            if (view != null)
            {
                this.components.Add(view.Index, view);
            }

            return view;
        }

        /// <summary>
        /// Remove component from the list.
        /// </summary>
        /// <param name="componentIndex">Component index.</param>
        public void Remove(string componentIndex)
        {
            if (string.IsNullOrEmpty(componentIndex))
            {
                return;
            }

            if (this.components.ContainsKey(componentIndex))
            {
                this.components.Remove(componentIndex);
            }

            if (this.subComponents.ContainsKey(componentIndex))
            {
                this.subComponents.Remove(componentIndex);
            }

            return;
        }

        /// <summary>
        /// Check whether the container contains a given component name.
        /// </summary>
        /// <param name="name">Component name.</param>
        /// <returns>Return true if component is exist.</returns>
        public bool Contains(string name)
        {
            return this.components.ContainsKey(name) || this.subComponents.ContainsKey(name);
        }

        /// <summary>
        /// Return the collection of component as enumerable.
        /// </summary>
        /// <returns>Return the enumerable of all the components.</returns>
        public IEnumerable<ComponentView> AsEnumerable()
        {
            return this.components.Values;
        }

        /// <summary>
        /// Add component/service to the container.
        /// </summary>
        /// <param name="service">Service need to be added.</param>
        public void AddComponent(BaseService service)
        {
            BaseComponent componentInstance = service as BaseComponent;
            if (componentInstance != null)
            {
                if (!string.IsNullOrEmpty(componentInstance.DisplayName) && !string.IsNullOrEmpty(componentInstance.Index))
                {
                    this.components.Remove(componentInstance.Index); // always remove the old one first...
                    this.components.Add(componentInstance.Index, new ComponentView(this, componentInstance.DisplayName, componentInstance));
                }
            }
        }

        /// <summary>
        /// Get the xml schema.
        /// <remarks>Not currently implemented.</remarks>
        /// </summary>
        /// <returns>Return null.</returns>
        public System.Xml.Schema.XmlSchema GetSchema()
        {
            return null;
        }

        /// <summary>
        /// Read from the xml reader (load the component from the xml file).
        /// </summary>
        /// <param name="reader">Xml reader.</param>
        public void ReadXml(XmlReader reader)
        {
            if (reader.IsStartElement("Components", string.Empty))
            {
                reader.Read();
                reader.MoveToContent();
                if (reader.IsStartElement("Component"))
                {
                    do
                    {
                        string index = reader.GetAttribute("Index");
                        string displayName = reader.GetAttribute("DisplayName");
                        string parent = reader.GetAttribute("Parent");
                        string typeName = reader.GetAttribute("Type");
                        string assemblyName = reader.GetAttribute("Assembly");
                        if (string.IsNullOrEmpty(typeName))
                        {
                            break;
                        }

                        Type type = Type.GetType(typeName, false);

                        if (type == null)
                        {
                            foreach (Assembly assembly in AppDomain.CurrentDomain.GetAssemblies())
                            {
                                if (assembly.FullName == assemblyName)
                                {
                                    type = assembly.GetType(typeName, false);
                                    break;
                                }
                            }
                        }

                        reader.Read();
                        reader.MoveToContent();
                        using (XmlReader innerReader = reader.ReadSubtree())
                        {
                            if (type != null)
                            {
                                XmlSerializer componentSerializer = new XmlSerializer(type, string.Empty);
                                BaseComponent componentInstance = componentSerializer.Deserialize(innerReader) as BaseComponent;
                                if (componentInstance != null)
                                {
                                    if (string.IsNullOrEmpty(parent))
                                    {
                                        this.components.Add(index, new ComponentView(this, displayName, componentInstance));
                                    }
                                    else
                                    {
                                        this.subComponents.Add(index, new ComponentView(this, parent, displayName, componentInstance));
                                    }
                                }
                            }
                            else
                            {
                            }

                            reader.Read();
                            reader.MoveToContent();
                            if (reader.LocalName == "Component" && !reader.IsStartElement())
                            {
                                reader.ReadEndElement();
                            }
                        }
                    } 
                    while (reader.ReadToNextSibling("Component", string.Empty));
                }

                while (reader.LocalName != "Components")
                {
                    reader.Read();
                }

                if (reader.LocalName == "Components" && !reader.IsStartElement())
                {
                    reader.ReadEndElement();
                }
            }
        }

        /// <summary>
        /// Write the component container in xml format (save it in xml format).
        /// </summary>
        /// <param name="writer">Xml writer.</param>
        public void WriteXml(System.Xml.XmlWriter writer)
        {
            XmlSerializerNamespaces namespaces = new XmlSerializerNamespaces(new XmlQualifiedName[] 
            {
                new XmlQualifiedName(string.Empty, string.Empty)
            });

            foreach (ComponentView component in this.components.Values)
            {
                XmlSerializer componentSerializer = new XmlSerializer(component.Instance.GetType());

                writer.WriteStartElement(string.Empty, "Component", string.Empty);
                writer.WriteAttributeString("Index", component.Index);
                writer.WriteAttributeString("DisplayName", component.DisplayName);
                writer.WriteAttributeString("Type", component.Instance.GetType().FullName);
                writer.WriteAttributeString("Assembly", component.Instance.GetType().Assembly.FullName);
                componentSerializer.Serialize(writer, component.Instance, namespaces);
                writer.WriteEndElement();
            }
        }

        /// <summary>
        /// Find the component from a specific directory.
        /// </summary>
        /// <param name="pluginsDirectory">Plugins directory.</param>
        internal void FindComponents(string pluginsDirectory)
        {
            this.FindComponents(Assembly.GetAssembly(typeof(ComponentContainer)));
            if (Directory.Exists(pluginsDirectory))
            {
                string[] assemblyFiles = Directory.GetFiles(pluginsDirectory, "*.dll", SearchOption.TopDirectoryOnly);

                foreach (string assemblyFile in assemblyFiles)
                {
                    try
                    {
                        this.FindComponents(Assembly.LoadFile(assemblyFile));
                    }
                    catch (Exception e)
                    {
                        DebugConsole.Instance.EnqueueTraceInfo(string.Format("Exception while trying to find components in {1} : {0}", e.Message, assemblyFile), System.Diagnostics.TraceLevel.Warning);
                    }
                }
            }
        }

        /// <summary>
        /// Find the defaukt component such as dei service, seed peer, overload peer and http tunneling service.
        /// </summary>
        internal void FindDefaultComponents()
        {
            this.FindComponents(Assembly.GetAssembly(typeof(ComponentContainer)));
        }

        /// <summary>
        /// Add sub component.
        /// </summary>
        /// <param name="parentName">Component parent name.</param>
        /// <param name="name">Subcomponent name.</param>
        /// <param name="subComponent">Subcomponent as BaseComponent.</param>
        internal void AddSubComponent(string parentName, string name, BaseComponent subComponent)
        {
            if (subComponent == null)
            {
                return;
            }

            if (!this.subComponents.ContainsKey(name))
            {
                this.subComponents.Add(subComponent.Index, new ComponentView(this, parentName, name, subComponent));
            }
        }

        /// <summary>
        /// Find components from given assembly.
        /// </summary>
        /// <param name="assembly">Given assembly.</param>
        private void FindComponents(Assembly assembly)
        {
            try
            {
                foreach (Type type in assembly.GetTypes())
                {
                    if (typeof(BaseComponent).IsAssignableFrom(type))
                    {
                        BaseComponent componentInstance = Activator.CreateInstance(type) as BaseComponent;
                        if (componentInstance != null && !(componentInstance is ArbitrationService))
                        {
                            if (!string.IsNullOrEmpty(componentInstance.DisplayName) && !string.IsNullOrEmpty(componentInstance.Index))
                            {
                                this.components.Add(componentInstance.Index, new ComponentView(this, componentInstance.DisplayName, componentInstance));
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                DebugConsole.Instance.EnqueueTraceInfo(string.Format("Failed to find components in {1} : {0}", e.Message, assembly.FullName), System.Diagnostics.TraceLevel.Error);
            }
        }
    }
}
