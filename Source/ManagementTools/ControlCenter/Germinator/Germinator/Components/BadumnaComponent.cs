﻿//-----------------------------------------------------------------------
// <copyright file="BadumnaComponent.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Germinator.Components
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Text;
    using System.Xml;
    using System.Xml.Serialization;
    using Badumna;

    /// <summary>
    /// BadumnaComponent class store the information about badumna network configuration.
    /// </summary>
    public class BadumnaComponent : BaseComponent
    {
        /// <summary>
        /// Component index.
        /// </summary>
        private static string componentIndex = "BadumnaComponent";

        /// <summary>
        /// Seed peer object lock.
        /// </summary>
        private object seedPeerLock = new object();

        /// <summary>
        /// Object lock.
        /// </summary>
        private object objectLock = new object();

        /// <summary>
        /// Badumna options class.
        /// </summary>
        private Options badumnaOptions;

        /// <summary>
        /// Initializes a new instance of the <see cref="BadumnaComponent"/> class.
        /// </summary>
        public BadumnaComponent()
            : base(BadumnaComponent.ComponentIndex, "Connectivity Setting")
        {
            this.badumnaOptions = new Options();
            this.IsDetachable = false;
            this.IsEnabled = true;  
        }

        /// <summary>
        /// Gets component index.
        /// </summary>
        public static string ComponentIndex
        {
            get { return BadumnaComponent.componentIndex; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether Advanced options is enabled.
        /// </summary>
        [XmlElement("AdvancedOptions")]
        [ComponentProperty("Advanced options")]
        public bool AdvancedOptions { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the broadcast is enabled.
        /// </summary>
        [XmlElement("IsBroadcastEnabled")]
        [ComponentProperty("Is broadcast enabled")]
        public bool IsBroadcastEnabled
        {
            get
            {
                try
                {
                    return this.badumnaOptions.Connectivity.IsBroadcastEnabled;
                }
                catch (NullReferenceException e)
                {
                    DebugConsole.Instance.EnqueueTraceInfo(e.Message);
                    return false;
                }
            }

            set
            {
                try
                {
                    this.badumnaOptions.Connectivity.IsBroadcastEnabled = value;
                }
                catch (NullReferenceException e)
                {
                    DebugConsole.Instance.EnqueueTraceInfo(e.Message);
                }
            }
        }

        /// <summary>
        /// Gets or sets the broadcast port.
        /// </summary>
        [XmlElement("BroadcastPort")]
        [ComponentProperty("Broadcast port")]
        public int BroadcastPort 
        {
            get
            {
                try
                {
                    return this.badumnaOptions.Connectivity.BroadcastPort;
                }
                catch (NullReferenceException e)
                {
                    DebugConsole.Instance.EnqueueTraceInfo(e.Message);
                    return -1;
                }
            }

            set
            {
                try
                {
                    this.badumnaOptions.Connectivity.BroadcastPort = value;
                }
                catch (NullReferenceException e)
                {
                    DebugConsole.Instance.EnqueueTraceInfo(e.Message);
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the portforwarding is enabled.
        /// </summary>
        [XmlElement("IsPortForwardingEnabled")]
        [ComponentProperty("Is PortForwarding enabled")]
        public bool IsPortForwardingEnabled 
        {
            get
            {
                try
                {
                    return this.badumnaOptions.Connectivity.IsPortForwardingEnabled;
                }
                catch (NullReferenceException e)
                {
                    DebugConsole.Instance.EnqueueTraceInfo(e.Message);
                    return false;
                }
            }

            set
            {
                try
                {
                    this.badumnaOptions.Connectivity.IsPortForwardingEnabled = value;
                }
                catch (NullReferenceException e)
                {
                    DebugConsole.Instance.EnqueueTraceInfo(e.Message);
                }
            }
        }

        /// <summary>
        /// Gets or sets the list of stun servers.
        /// </summary>
        [XmlArray(ElementName = "StunServers")]
        [ComponentProperty("Stun server list")]
        public string[] StunServers
        {
            get
            {
                try
                {
                    string[] stunServers = new string[this.badumnaOptions.Connectivity.StunServers.Count];

                    for (int i = 0; i < stunServers.Length; i++)
                    {
                        stunServers[i] = this.badumnaOptions.Connectivity.StunServers[i];
                    }

                    return stunServers;
                }
                catch (NullReferenceException e)
                {
                    DebugConsole.Instance.EnqueueTraceInfo(e.Message);
                    return null;
                }
            }

            set
            {
                try
                {
                    this.badumnaOptions.Connectivity.StunServers.Clear();

                    foreach (string stunServer in value)
                    {
                        this.badumnaOptions.Connectivity.StunServers.Add(stunServer);
                    }
                }
                catch (NullReferenceException e)
                {
                    DebugConsole.Instance.EnqueueTraceInfo(e.Message);
                }
            }
        }

        /// <summary>
        /// Monitor action result.
        /// </summary>
        /// <param name="germClient">Germ client.</param>
        /// <param name="component">The component.</param>
        /// <param name="additionalState">Additional state.</param>
        /// <returns>Return the monitor action result</returns>
        [MonitorAction]
        public MonitorActionResult TestIfActive(GermClient germClient, object component, object additionalState)
        {
            return MonitorActionResult.Error;
        }

        /// <summary>
        /// Build the badumna network configuration in xml format.
        /// </summary>
        /// <param name="listeningPort">Listening port.</param>
        /// <param name="stream">Stream where the network configuration is written.</param>
        public void BuildConfigXml(int listeningPort, MemoryStream stream)
        {
            try
            {
                this.badumnaOptions.Connectivity.ConfigureForSpecificPort(listeningPort);

                XmlWriter xmlWriter = XmlWriter.Create(stream, new XmlWriterSettings() { Encoding = new UTF8Encoding() });
                this.badumnaOptions.Save(xmlWriter);
            }
            catch (NullReferenceException e)
            {
                DebugConsole.Instance.EnqueueTraceInfo(e.Message);
            }
        }

        /// <summary>
        /// Clear the seed peer list.
        /// </summary>
        internal void ClearSeedPeers()
        {
            lock (this.seedPeerLock)
            {
                try
                {
                    this.badumnaOptions.Connectivity.SeedPeers.Clear();
                }
                catch (NullReferenceException e)
                {
                    DebugConsole.Instance.EnqueueTraceInfo(e.Message);
                }
            }
        }

        /// <summary>
        /// Add a seed peer to seed peer list.
        /// </summary>
        /// <param name="seedPeer">SeedPeer address.</param>
        internal void AddSeedPeer(string seedPeer)
        {
            lock (this.seedPeerLock)
            {
                try
                {
                    if (!this.badumnaOptions.Connectivity.SeedPeers.Contains(seedPeer))
                    {
                        this.badumnaOptions.Connectivity.SeedPeers.Add(seedPeer);
                    }
                }
                catch (Exception e)
                {
                    DebugConsole.Instance.EnqueueTraceInfo(e.Message);
                }
            }
        }

        /// <summary>
        /// Remove a particular seed peer from the list.
        /// </summary>
        /// <param name="seedPeer">SeedPeer address.</param>
        internal void RemoveSeedPeer(string seedPeer)
        {
            lock (this.seedPeerLock)
            {
                try
                {
                    if (this.badumnaOptions.Connectivity.SeedPeers.Contains(seedPeer))
                    {
                        this.badumnaOptions.Connectivity.SeedPeers.Remove(seedPeer);
                    }
                }
                catch (Exception e)
                {
                    DebugConsole.Instance.EnqueueTraceInfo(e.Message);
                }
            }
        }

        /// <summary>
        /// Set the overload server to true (i.e. set a peer to be an overload server).
        /// </summary>
        /// <param name="serverAddress">Overload server address.</param>
        internal void EnabledOverloadServer(string serverAddress)
        {
            try
            {
                lock (this.objectLock)
                {
                    this.badumnaOptions.Overload.IsServer = true;
                    this.badumnaOptions.Overload.ServerAddress = serverAddress;
                }
            }
            catch (NullReferenceException e)
            {
                DebugConsole.Instance.EnqueueTraceInfo(e.Message);
            }
        }

        /// <summary>
        /// Set the overload server to false (i.e. this peer is a normal peer).
        /// </summary>
        internal void DisabledOverloadServer()
        {
            try
            {
                lock (this.objectLock)
                {
                    this.badumnaOptions.Overload.IsServer = false;
                }
            }
            catch (NullReferenceException e)
            {
                DebugConsole.Instance.EnqueueTraceInfo(e.Message);
            }
        }

        /// <summary>
        /// Build the arbitration configuration for an arbitration server.
        /// </summary>
        /// <param name="arbitrationName">Arbitration name.</param>
        /// <param name="enabledDistributedLookup">Enable distributed lookup.</param>
        /// <param name="address">Arbitration server address.</param>
        internal void EnabledArbitrationServer(string arbitrationName, bool enabledDistributedLookup, string address)
        {
            try
            {
                lock (this.objectLock)
                {
                    ArbitrationServerDetails serverDetail;

                    if (enabledDistributedLookup)
                    {
                        serverDetail = new ArbitrationServerDetails(arbitrationName);
                    }
                    else
                    {
                        serverDetail = new ArbitrationServerDetails(arbitrationName, address);
                    }

                    // always clear the server list first.
                    this.badumnaOptions.Arbitration.Servers.Clear();
                    this.badumnaOptions.Arbitration.Servers.Add(serverDetail);
                }
            }
            catch (NullReferenceException e)
            {
                DebugConsole.Instance.EnqueueTraceInfo(e.Message);
            }
        }

        /// <summary>
        /// Cleared the arbitration configuration.
        /// </summary>
        internal void DisabledArbitrationServer()
        {
            try
            {
                lock (this.objectLock)
                {
                    this.badumnaOptions.Arbitration.Servers.Clear();
                }
            }
            catch (NullReferenceException e)
            {
                DebugConsole.Instance.EnqueueTraceInfo(e.Message);
            }
        }

        /// <summary>
        /// Set all field back to default value.
        /// </summary>
        internal override void DefaultValue()
        {
            this.badumnaOptions = new Options();
            this.IsDetachable = false;
            this.IsEnabled = true;  
        }
    }
}
