﻿//-----------------------------------------------------------------------
// <copyright file="ComponentPropertyAttribute.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
using System;
using System.Collections.Generic;
using System.Text;

namespace Germinator.Components
{
    /// <summary>
    /// ComponentPropertyAttribute class.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, Inherited = true)]
    public class ComponentPropertyAttribute : Attribute
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ComponentPropertyAttribute"/> class.
        /// </summary>
        public ComponentPropertyAttribute()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ComponentPropertyAttribute"/> class.
        /// </summary>
        /// <param name="values">Component property values.</param>
        public ComponentPropertyAttribute(params string[] values)
            : this()
        {
            this.Values = new List<string>(values);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ComponentPropertyAttribute"/> class.
        /// </summary>
        /// <param name="name">Component property attribute name.</param>
        public ComponentPropertyAttribute(string name)
        {
            this.Name = name;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ComponentPropertyAttribute"/> class.
        /// </summary>
        /// <param name="name">Component property attribute name.</param>
        /// <param name="values">Component property values.</param>
        public ComponentPropertyAttribute(string name, params string[] values)
            : this(name)
        {
            this.Values = new List<string>(values);
        }

        /// <summary>
        /// Gets the component property name.
        /// </summary>
        internal string Name { get; private set; }

        /// <summary>
        /// Gets the component property values.
        /// </summary>
        internal List<string> Values { get; private set; }
    }
}
