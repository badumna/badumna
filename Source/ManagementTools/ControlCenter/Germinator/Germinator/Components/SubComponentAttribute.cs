﻿//-----------------------------------------------------------------------
// <copyright file="SubComponentAttribute.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
using System;
using System.Collections.Generic;
using System.Text;

namespace Germinator.Components
{
    /// <summary>
    /// SubComponentAttribute class contains the subcomponent name.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, Inherited = true)]
    public class SubComponentAttribute : Attribute
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SubComponentAttribute"/> class.
        /// </summary>
        /// <param name="name">Sub component attribute name.</param>
        public SubComponentAttribute(string name)
        {
            this.Name = name;
        }

        /// <summary>
        /// Gets the subcomponent attribute name.
        /// </summary>
        internal string Name { get; private set; }
    }
}
