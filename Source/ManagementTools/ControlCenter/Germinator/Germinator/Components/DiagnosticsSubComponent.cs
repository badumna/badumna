﻿//-----------------------------------------------------------------------
// <copyright file="DiagnosticsSubComponent.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
using System;
using System.Collections.Generic;
using System.Text;

namespace Germinator.Components
{
    /// <summary>
    /// Diagnostic subcomponent is part of the badumna network configruation module.
    /// </summary>
    public class DiagnosticsSubComponent : BaseComponent
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DiagnosticsSubComponent"/> class.
        /// </summary>
        public DiagnosticsSubComponent()
            : base("Diagnostics", "Diagnostics")
        {
            this.DiagnosticsServer = string.Empty;
        }

        /// <summary>
        /// Gets or sets the diagnostic server address.
        /// </summary>
        [ComponentProperty("Diagnostics server")]
        public string DiagnosticsServer { get; set; }

        /// <summary>
        /// Build the the network config file for the diagnostic server module.
        /// </summary>
        /// <returns>Rerurns the diagnostic server module.</returns>
        internal string BuildConfigXml()
        {
            return string.Format(Germinator_Resource.DiagnosticsModuleFormat, this.IsEnabled, this.DiagnosticsServer);
        }

        /// <summary>
        /// Set the component back to the default value.
        /// </summary>
        internal override void DefaultValue()
        {
            this.DiagnosticsServer = string.Empty;
        }
    }
}
