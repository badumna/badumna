﻿//-----------------------------------------------------------------------
// <copyright file="FileUploader.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
using System;
using System.Collections.Generic;
using System.Text;

namespace Germinator.Components
{
    /// <summary>
    /// File uploader functionality is handled by this class.
    /// </summary>
    public class FileUploader
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FileUploader"/> class.
        /// </summary>
        public FileUploader()
        {
            this.DestinationDirectory = "Uploads";
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FileUploader"/> class.
        /// </summary>
        /// <param name="directory">Directory name.</param>
        public FileUploader(string directory)
        {
            this.DestinationDirectory = directory;
        }

        /// <summary>
        /// Gets or sets the File name.
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// Gets or sets the Destination directory.
        /// </summary>
        public string DestinationDirectory { get; set; }

        /// <summary>
        /// Return the upload path.
        /// </summary>
        /// <param name="directory">Directory name.</param>
        /// <param name="fileName">File name.</param>
        /// <returns>Upload path.</returns>
        public virtual string CanUpload(string directory, string fileName)
        {
            this.FileName = fileName;
            return System.IO.Path.Combine(System.IO.Path.Combine(directory, this.DestinationDirectory), fileName);
        }

        /// <summary>
        /// Print out the file name.
        /// </summary>
        /// <returns>Return the file name.</returns>
        public override string ToString()
        {
            if (string.IsNullOrEmpty(this.FileName))
            {
                return string.Empty;
            }

            return this.FileName.ToString();
        }
    }
}
