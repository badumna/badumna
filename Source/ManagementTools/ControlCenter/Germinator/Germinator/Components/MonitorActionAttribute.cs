﻿//-----------------------------------------------------------------------
// <copyright file="MonitorActionAttribute.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
using System;
using System.Collections.Generic;
using System.Text;

namespace Germinator.Components
{
    /// <summary>
    /// MonitorAction delegate method. 
    /// </summary>
    /// <param name="germClient">Germ client.</param>
    /// <param name="component">Component object.</param>
    /// <param name="additionalState">Additional state.</param>
    /// <returns>Return the monitor action result.</returns>
    public delegate MonitorActionResult MonitorAction(GermClient germClient, object component, object additionalState);

    /// <summary>
    /// MonitorActionResult enumeration.
    /// </summary>
    public enum MonitorActionResult
    {
        /// <summary>
        /// Action : success.
        /// </summary>
        Success,

        /// <summary>
        /// Action : Failure.
        /// </summary>
        Failure,

        /// <summary>
        /// Action : Error.
        /// </summary>
        Error
    }

    /// <summary>
    /// MonitorActionAttribute class.
    /// </summary>
    [AttributeUsage(AttributeTargets.Method, Inherited = true)]
    public class MonitorActionAttribute : Attribute
    {
    }
}
