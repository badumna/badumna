﻿//-----------------------------------------------------------------------
// <copyright file="ClientListenerComponent.cs" company="National ICT Australia Ltd">
//     Copyright (c) 2012 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Germinator.Components
{
    using System;
    using System.Xml.Serialization;

    /// <summary>
    /// ClientListenerComponent stores the ClientListener's options.
    /// Specifically the ClientListener port number and whether it should start the 
    /// ClientListener automatically.
    /// </summary>
    public class ClientListenerComponent : BaseComponent
    {
        /// <summary>
        /// The component index.
        /// </summary>
        private static string componentIndex = "ClientListenerComponent";

        /// <summary>
        /// Initializes a new instance of the <see cref="ClientListenerComponent"/> class.
        /// </summary>
        public ClientListenerComponent()
            : base(ClientListenerComponent.componentIndex, "Germs listener Setting")
        {
            this.PortNumber = 21200;
            this.UseClientListener = false;
        }

        /// <summary>
        /// Gets the ComponentIndex property.
        /// </summary>
        public static string ComponentIndex
        {
            get { return componentIndex; }
        }

        /// <summary>
        /// Gets or sets the port number used by ClientListener class.
        /// </summary>
        [XmlElement("PortNumber")]
        [ComponentProperty("Port Number")]
        public int PortNumber { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the client listener should be started.
        /// </summary>
        [XmlElement("UseClientListener")]
        [ComponentProperty("Start germs listener")]
        public bool UseClientListener { get; set; }
    }
}
