﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Germinator.Components
{
    [AttributeUsage(AttributeTargets.Class, Inherited = true)]
    public class NetworkComponentAttribute : Attribute
    {
        public string Name { get; private set; }

        public NetworkComponentAttribute(string name)
        {
            this.Name = name;
        }
    }
}
