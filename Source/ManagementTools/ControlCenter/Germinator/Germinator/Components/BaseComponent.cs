﻿//-----------------------------------------------------------------------
// <copyright file="BaseComponent.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace Germinator.Components
{
    /// <summary>
    /// BaseComponent class store the base information components.
    /// </summary>
    public class BaseComponent
    {
        /// <summary>
        /// Is enabled.
        /// </summary>
        [XmlAttribute("IsEnabled")]
        public volatile bool IsEnabled;

        /// <summary>
        /// Initializes a new instance of the <see cref="BaseComponent"/> class.
        /// For serialization.
        /// </summary>
        public BaseComponent()
        {
            this.IsDetachable = true;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BaseComponent"/> class.
        /// </summary>
        /// <param name="index">Component index.</param>
        /// <param name="displayName">Component display name.</param>
        protected BaseComponent(string index, string displayName)
        {
            this.Index = index;
            this.DisplayName = displayName;
            this.IsDetachable = true;
        }

        /// <summary>
        /// Gets or sets the component index.
        /// </summary>
        [XmlAttribute("Index")]
        public string Index { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the component is detachable.
        /// </summary>
        [XmlAttribute("IsDetachable")]
        public bool IsDetachable { get; set; }

        /// <summary>
        /// Gets or sets the component display name.
        /// </summary>
        [XmlAttribute("DisplayName")]
        public string DisplayName { get; set; }

        /// <summary>
        /// Check whether the component can be execute on a certain germ.
        /// In the current implementation it will always return true.
        /// </summary>
        /// <param name="germ">Germ name.</param>
        /// <returns>Return true if it allowed to run on given germ.</returns>
        public virtual bool CanExecuteOn(GermClient germ)
        {
            return true;
        }

        /// <summary>
        /// Set all field back to default value.
        /// </summary>
        internal virtual void DefaultValue() 
        { 
        }
    }
}
