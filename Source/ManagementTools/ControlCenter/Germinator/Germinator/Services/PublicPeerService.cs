﻿//---------------------------------------------------------------------------------
// <copyright file="PublicPeerService.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
// <summary>Public peer service class.</summary>
//---------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading;
using System.Xml;
using System.Xml.Serialization;

using GermHarness;
using Germinator.Components;

namespace Germinator.Services
{
    /// <summary>
    /// PublicPeer aka SeedPeer class derived from PeerService. It used to run the SeedPeer process from the Control
    /// Center and should be able to start the SeedPeer using the dei configuration as well.
    /// </summary>
    public class PublicPeerService : PeerService
    {
        /// <summary>
        /// PublicPeerService start signal.
        /// </summary>
        private AutoResetEvent startSignal = new AutoResetEvent(false);

        /// <summary>
        /// Seed peer address.
        /// </summary>
        private string seedPeerAddress;

        /// <summary>
        /// Initializes a new instance of the <see cref="PublicPeerService"/> class.
        /// </summary>
        public PublicPeerService()
            : base("SeedPeer", "Seed peer")
        {
            this.ListeningPort = 21251;
            this.UseDei = false;
            this.seedPeerAddress = string.Empty;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PublicPeerService"/> class.
        /// </summary>
        /// <param name="serviceIndex">Service index name.</param>
        /// <param name="displayName">Service display name.</param>
        public PublicPeerService(string serviceIndex, string displayName)
            : base(serviceIndex, displayName)
        {
        }

        /// <summary>
        /// Gets or sets a value indicating whether the PublicPeerService start with or without Dei.
        /// </summary>
        [ComponentProperty("Use Dei Authentication Server")]
        public bool UseDei { get; set; }

        /// <summary>
        /// Gets or sets Dei Server address.
        /// </summary>
        [ComponentProperty("Dei Server address")]
        public string DeiServerAddress { get; set; }

        /// <summary>
        /// Gets or sets the Dei username for SeedPeer.
        /// </summary>
        [ComponentProperty("Username")]
        public string DeiUsername { get; set; }

        /// <summary>
        /// Gets or sets the password for SeedPeer.
        /// </summary>
        [ComponentProperty("Password")]
        public string DeiPassword { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the SSL connection is used or not.
        /// </summary>
        [ComponentProperty("Use Ssl connection")]
        public bool SslConnection { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the seed peer should start a new network.
        /// <remarks>The value of rejoin should always the opposite from the user input.</remarks>
        /// </summary>
        [XmlElement("NewNetwork")]
        [ComponentProperty("Start a new network")]
        public bool NewNetwork { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the seed peer is the matchmaking server.
        /// </summary>
        [ComponentProperty("Use seedpeer as a matchmaking server")]
        public bool IsMatchmakingServer { get; set; }

        /// <summary>
        /// Override the StartPeerService method, it will be called before starting a process.
        /// </summary>
        /// <param name="potentialGerms">List of potentials germ.</param>
        /// <param name="configurationComponent">Badumna configuration component.</param>
        /// <param name="applicationComponent">Application configuration component.</param>
        internal override void StartPeerService(IEnumerable<GermClient> potentialGerms, BadumnaComponent configurationComponent, ApplicationComponent applicationComponent)
        {
            OverloadPeerService overloadPeer = this as OverloadPeerService;
            if (overloadPeer == null)
            {
                // Clear the OverloadPeerConfig, because this class is a seed peer;
                configurationComponent.DisabledOverloadServer();
            }

            //// Note: we don't want to clear seedpeer to support multiple seed peer on the network,
            //// TODO: Add mechanism to check whether the seed peer list are up to date.
            //// configurationComponent.ClearSeedPeers();
            configurationComponent.DisabledArbitrationServer();

            // Clear the process argument
            this.ProcessArguments = string.Empty;

            if (this.UseDei)
            {
                this.BuildDeiConfigString();
            }

            if (!this.NewNetwork && overloadPeer == null)
            {
                // SeedPeer should rejoin to the existing network but not overload peer.
                this.ProcessArguments += "-r ";
            }

            if (this.IsMatchmakingServer && overloadPeer == null)
            {
                this.ProcessArguments += "-mm ";
            }

            base.StartPeerService(potentialGerms, configurationComponent, applicationComponent);
        }

        /// <summary>
        /// Set the component back to the default value.
        /// </summary>
        internal override void DefaultValue()
        {
            base.DefaultValue();

            this.ListeningPort = 21251;
            this.UseDei = false;
            this.DeiServerAddress = string.Empty;
            this.DeiUsername = string.Empty;
            this.DeiPassword = string.Empty;
            this.SslConnection = false;
            this.IsMatchmakingServer = false;
        }

        /// <summary>
        /// Override OnInstanceStarted method, this function will be called when the process is successfully started
        /// on the given germ host.
        /// </summary>
        /// <param name="instance">Remote process instance.</param>
        protected override void OnInstanceStarted(RemoteServiceProcess instance)
        {
            lock (this.mConfigurationLock)
            {
                if (instance != null && this.mConfiguration != null)
                {
                    byte[] config = null;
                    using (System.IO.MemoryStream stream = new System.IO.MemoryStream())
                    {
                        this.mConfiguration.BuildConfigXml(this.ListeningPort, stream);
                        stream.Flush();
                        config = stream.ToArray();
                    }

                    if (config != null)
                    {
                        instance.Germ.QueueCustomRequestOperation(instance.Process.ProcessId, (int)CustomRequestType.StartPeer, config, null, null);

                        // Sleep for a while so that the peer has time to come online so that we can access its public address.
                        Thread.Sleep(10000);

                        instance.Germ.QueueCustomRequestOperation(
                            instance.Process.ProcessId, 
                            (int)CustomRequestType.Status,
                            new byte[] { },
                            delegate(IAsyncResult ar)
                            {
                                CustomMessageOperation operation = ar as CustomMessageOperation;
                                if (operation.WasSuccessful)
                                {
                                    XmlDocument document = new XmlDocument();

                                    try
                                    {
                                        document.Load(operation.Response);
                                        XmlNode publicAddressNode = document.SelectSingleNode("/Status/Connectivity/PublicAddress/text()");
                                        if (publicAddressNode != null)
                                        {
                                            OverloadPeerService overloadPeer = this as OverloadPeerService;
                                            if (overloadPeer == null)
                                            {
                                                //// Only add the seed peer address not the overload address.
                                                this.mConfiguration.AddSeedPeer(publicAddressNode.Value.Split('|')[1]);
                                                this.seedPeerAddress = publicAddressNode.Value.Split('|')[1];
                                            }
                                        }
                                    }
                                    catch
                                    {
                                        Trace.TraceWarning("Failed to start {0} process", this.DisplayName);
                                        DebugConsole.Instance.EnqueueTraceInfo(string.Format("Failed to start {0} process", this.DisplayName));
                                        instance.SetPingStatus(PingStatus.ExitWithError, "Failed to start.");
                                        this.Stop(instance.Germ.Name);
                                    }
                                }

                                this.OnInstanceStarted(instance.Process.ProcessId, operation.WasSuccessful);
                            },
                            null);
                    }
                }
            }
        }

        /// <summary>
        /// Override OnInstanceStarting method.
        /// </summary>
        /// <param name="instance">Remote process instance.</param>
        protected override void OnInstanceStarting(RemoteServiceProcess instance)
        {
            this.startSignal.WaitOne(GermClient.DefaultOperationTimeout);
        }

        /// <summary>
        /// Override OnInstanceStarted method.
        /// </summary>
        /// <param name="processId">Process Id.</param>
        /// <param name="successful">The value indicating whether the process is successfully started.</param>
        protected override void OnInstanceStarted(int processId, bool successful)
        {
            this.startSignal.Set();
        }

        /// <summary>
        /// Override OnInstanceStopped method.
        /// </summary>
        /// <param name="instance">Remote instance process.</param>
        protected override void OnInstanceStopped(RemoteServiceProcess instance)
        {
            if (!string.IsNullOrEmpty(this.seedPeerAddress))
            {
                this.mConfiguration.RemoveSeedPeer(this.seedPeerAddress);
            }
        }
        
        /// <summary>
        /// Build the dei configuration when start the process using dei
        /// </summary>
        private void BuildDeiConfigString()
        {
            string deiConfigString = "--dei-config-string=";

            // Split the host name and port number from the dei server address.
            deiConfigString += string.Format("{0};", this.DeiServerAddress.Split(':')[0]); // DeiHostName
            deiConfigString += string.Format("{0};", this.DeiServerAddress.Split(':')[1]); // DeiPortNumber
            deiConfigString += string.Format("{0};", this.SslConnection.ToString()); // Type of connection
            deiConfigString += string.Format("{0};", this.DeiUsername); // DeiUsername
            deiConfigString += string.Format("{0} ", this.DeiPassword); // DeiPassword
            
            this.ProcessArguments += deiConfigString; 
        }
    }
}
