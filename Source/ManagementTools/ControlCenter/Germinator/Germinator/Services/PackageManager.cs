﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Germinator.Services
{
    // TODO : check the protection level for PackageManager
    public class PackageManager
    {
        public static PackageManager Instance = new PackageManager();

        private Dictionary<string, ServicePackageDescription> mPackages = new Dictionary<string, ServicePackageDescription>();

        /// <summary>
        /// Find and reference all packages found in the given directory.
        /// </summary>
        /// <param name="packagesDirectory"></param>
        internal void FindPackages(string packagesDirectory)
        {
            if (Directory.Exists(packagesDirectory))
            {
                foreach (string subDirectory in Directory.GetDirectories(packagesDirectory))
                {
                    string packageDirectory = Path.Combine(packagesDirectory, subDirectory);
                    string manifestFilename = Path.Combine(packageDirectory, Badumna.Package.Manifest.DefaultFileName);

                    if (File.Exists(manifestFilename))
                    {
                        ServicePackageDescription package = new ServicePackageDescription();

                        string commonDirectory = Path.Combine(packagesDirectory, "Common");
                        if (package.LoadPackageDetails(manifestFilename, commonDirectory))
                        {
                            this.mPackages[package.ApplicationName] = package;
                        }
                    }
                }
            }
        }

        public void AddPackage(string packageDirectory, string manifestFilename)
        {
            if (File.Exists(manifestFilename))
            {
                ServicePackageDescription package = new ServicePackageDescription();
                DirectoryInfo packageDirectoryInfo = new DirectoryInfo(packageDirectory);
                
                string commonDirectory = Path.Combine(packageDirectoryInfo.Parent.FullName, "Common");
                if (package.LoadPackageDetails(manifestFilename, commonDirectory))
                {
                    this.mPackages[package.ApplicationName] = package;
                }
            }
        }

        public ServicePackageDescription GetPackage(string name)
        {
            ServicePackageDescription package = null;
            if (this.mPackages.TryGetValue(name, out package))
            {
                // Check to see if there is a newer version available.
                if (package.IsOutOfDate())
                {
                    package.ReLoad();
                }
            }

            return package;
        }

        public void RemovePacakage(string applicationName)
        {
            if (this.mPackages.ContainsKey(applicationName))
            {
                this.mPackages.Remove(applicationName);
            }
        }

        public IEnumerable<ServicePackageDescription> GetAllPackages()
        {
            return this.mPackages.Values;
        }
    }
}
