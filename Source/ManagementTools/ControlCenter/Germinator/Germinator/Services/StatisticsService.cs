﻿//---------------------------------------------------------------------------------
// <copyright file="StatisticsService.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
// <summary>Statistics service class.</summary>
//---------------------------------------------------------------------------------

namespace Germinator.Services
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Xml;

    using Components;

    /// <summary>
    /// Statistics service is one of default services provided in Control Center. This service is not 
    /// a Badumna peer process thus it derived from BaseService not a PeerService class.
    /// </summary>
    public class StatisticsService : BaseService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="StatisticsService"/> class.
        /// </summary>
        public StatisticsService()
            : base("StatisticsServer", "Statistics Server")
        {
            this.ListeningPort = 21256;
            this.MessageInterval = 300.0f;
        }

        /// <summary>
        /// Statistics message types.
        /// </summary>
        public enum StatisticsMessageType : int
        {
            /// <summary>
            /// Get the statistics and scene data.
            /// </summary>
            Status = 0,

            /// <summary>
            /// Get the session activity data.
            /// </summary>
            Session,
        }

        /// <summary>
        /// Gets or sets the listening port.
        /// </summary>
        [ComponentProperty("Listening port")]
        public int ListeningPort { get; set; }

        /// <summary>
        /// Gets or sets the message interval.
        /// </summary>
        [ComponentProperty("Message interval (sec)")]
        public double MessageInterval { get; set; }

        /// <summary>
        /// Request the session activity.
        /// </summary>
        /// <param name="host">Name of the host.</param>
        /// <param name="startTime">The start time.</param>
        /// <param name="rangeType">The range type.</param>
        /// <returns>Session activity.</returns>
        public string GetSessionActivity(string host, DateTime startTime, int rangeType)
        {
            RemoteServiceProcess instance = null;
            List<RemoteServiceProcess> processes = this.Instances as List<RemoteServiceProcess>;

            if (processes != null)
            {
                for (int i = 0; i < processes.Count; ++i)
                {
                    if (processes[i].Germ.Name == host)
                    {
                        if (processes[i].PingStatus != PingStatus.Error)
                        {
                            instance = processes[i];
                        }

                        break;
                    }
                }
            }

            if ((instance != null) && (instance.Process != null))
            {
                byte[] message;

                using (MemoryStream stream = new MemoryStream())
                {
                    using (BinaryWriter writer = new BinaryWriter(stream))
                    {
                        writer.Write(startTime.ToBinary());
                        writer.Write(rangeType);
                        writer.Flush();
                        message = stream.ToArray();
                    }
                }

                CustomMessageOperation operation = instance.Germ.QueueCustomRequestOperation(
                    instance.Process.ProcessId,
                    (int)StatisticsMessageType.Session,
                    message,
                    null,
                    null);
                operation.AsyncWaitHandle.WaitOne(5000);

                if (operation.WasSuccessful)
                {
                    XmlDocument document = new XmlDocument();
                    using (StreamReader reader = new StreamReader(operation.Response, System.Text.Encoding.UTF8))
                    {
                        document.Load(reader);
                    }

                    return document.OuterXml;
                }
                else if (!operation.WasSuccessful && operation.IsUnharnessed)
                {
                    // Since the process is un-harnessed, then the status is not
                    // available, so instead return the status of the process it
                    // will return useful information to the user.
                    return "Network Status is not available for un-harnessed process";
                }
            }

            return string.Empty;
        }

        /// <summary>
        /// Start the service after building the process arguments.
        /// </summary>
        /// <param name="potentialGerms">Potential germs.</param>
        /// <param name="startEvenIfAlreadyRunning">Start even if already running.</param>
        internal override void Start(IEnumerable<GermClient> potentialGerms, bool startEvenIfAlreadyRunning)
        {
            this.BuildProcessArguments();
            base.Start(potentialGerms, startEvenIfAlreadyRunning);
        }

        /// <summary>
        /// Reset to default values.
        /// </summary>
        internal override void DefaultValue()
        {
            base.DefaultValue();
            this.ListeningPort = 21256;
            this.MessageInterval = 300.0f;
        }

        /// <summary>
        /// Called immediately after status has been requested.
        /// </summary>
        /// <param name="instance">Remote service process.</param>
        /// <returns>Remote instance status.</returns>
        protected override string OnGetStatus(RemoteServiceProcess instance)
        {
            if ((instance != null) && (instance.Process != null))
            {
                CustomMessageOperation operation = instance.Germ.QueueCustomRequestOperation(
                    instance.Process.ProcessId,
                    (int)StatisticsMessageType.Status,
                    new byte[] { },
                    null,
                    null);
                operation.AsyncWaitHandle.WaitOne(5000);

                if (operation.WasSuccessful)
                {
                    XmlDocument document = new XmlDocument();
                    using (StreamReader reader = new StreamReader(operation.Response, System.Text.Encoding.UTF8))
                    {
                        document.Load(reader);
                    }

                    return document.OuterXml;
                }
                else if (!operation.WasSuccessful && operation.IsUnharnessed)
                {
                    // Since the process is un-harnessed, then the status is not
                    // available, so instead return the status of the process it
                    // will return useful information to the user.
                    return "Network Status is not available for un-harnessed process";
                }
            }

            return string.Empty;
        }

        /// <summary>
        /// Build the process arguments for the Statistics service.
        /// </summary>
        private void BuildProcessArguments()
        {
            this.ProcessArguments = string.Empty;

            if (!this.ProcessArguments.Contains("--port-number="))
            {
                this.ProcessArguments += " --port-number=" + this.ListeningPort.ToString();
            }

            if (!this.ProcessArguments.Contains("--message-interval="))
            {
                this.ProcessArguments += " --message-interval=" + this.MessageInterval.ToString();
            }
        }
    }
}
