﻿//---------------------------------------------------------------------------------
// <copyright file="OverloadPeerService.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
// <summary>Overload peer service class.</summary>
//---------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Threading;
using System.Xml;

using GermHarness;

using Germinator.Components;

namespace Germinator.Services
{
    /// <summary>
    /// OverloadPeerService class derived from PublicPeerService. Overload peer is one of the default service provided 
    /// in Control Center. It has one additional property, DistributedLookup. It will used DistributedLookup when the 
    /// the property is set.
    /// </summary>
    public class OverloadPeerService : PublicPeerService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="OverloadPeerService"/> class.
        /// </summary>
        public OverloadPeerService()
            : base("OverloadPeer", "Overload peer")
        {
            this.DistributedLookup = true;
            this.ListeningPort = 21252;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the DistributedLookup functionality is used.
        /// </summary>
        /// <value>Distributed Lookup.</value>
        [ComponentProperty("Distributed Lookup")]
        public bool DistributedLookup { get; set; }

        /// <summary>
        /// Overide the StartPeerService method, make sure to add the overload module to the service NetworkConfig.xml.
        /// </summary>
        /// <param name="potentialGerms">List of potential germs.</param>
        /// <param name="configurationComponent">Badumna configuration components.</param>
        /// <param name="applicationComponent">Application configuration components.</param>
        internal override void StartPeerService(IEnumerable<GermClient> potentialGerms, BadumnaComponent configurationComponent, ApplicationComponent applicationComponent)
        {
            string serverAddress = null;
            if (!this.DistributedLookup)
            {
                // Note: get the server address from the compatible host (germ host) 
                serverAddress = this.CompatibleHosts[0] + ":" + this.ListeningPort;
            }

            configurationComponent.EnabledOverloadServer(serverAddress);
            base.StartPeerService(potentialGerms, configurationComponent, applicationComponent);
        }

        /// <summary>
        /// Set the component back to the default value.
        /// </summary>
        internal override void DefaultValue()
        {
            base.DefaultValue();
            this.DistributedLookup = true;
            this.ListeningPort = 21252;
        }
    }
}
