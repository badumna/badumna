﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

using GermServer;

namespace Germinator.Services
{
    public enum PingStatus
    {
        Unknown,
        HostOffline,
        HostOnlineButUnactive,
        ActiveButOffline,
        ActiveButNotInitialized,
        ActiveButNotLoggedIn,
        OnlineAndActive,
        Error,
        ParseError,
        HostOnlineButUnharnessed,
        ExitWithError,
        ServiceStarted
    }

    public class RemoteServiceProcess
    {
        public GermClient Germ { get; set; }
        public ProcessDescription Process { get; set; }
        public PingStatus PingStatus { get; private set; }
        public DateTime TimeOfLastPing { get; set; }
        public string ErrorText { get; private set; }

        internal void SetPingStatus(PingStatus status, string errorText)
        {
            this.PingStatus = status;
            this.ErrorText = errorText;
        }

        internal void SetPingStatus(XmlNode node)
        {
            PingStatus status = PingStatus.HostOffline;

            if (node != null)
            {
                try
                {
                    int pingState = int.Parse(node.Attributes["State"].Value);
                    switch ((GermHarness.PingState)pingState)
                    {
                        case GermHarness.PingState.Active:
                            status = PingStatus.OnlineAndActive;
                            break;

                        case GermHarness.PingState.Initialized:
                            status = PingStatus.ActiveButNotLoggedIn;
                            break;

                        case GermHarness.PingState.LoggedIn:
                            status = PingStatus.ActiveButOffline;
                            break;

                        case GermHarness.PingState.None:
                            status = PingStatus.ActiveButNotInitialized;
                            break;
                    }

                    XmlNode exceptionNode = node.SelectSingleNode("Exception");
                    if (exceptionNode != null)
                    {
                        status = PingStatus.Error;
                        this.ErrorText = exceptionNode.OuterXml;
                    }
                }
                catch
                {
                    status = PingStatus.ParseError;
                }
            }

            this.PingStatus = status;
        }

    }
}
