﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.IO;
using System.Diagnostics;
using System.Xml.Serialization;

using GermHarness;
using Germinator.Components;
using GermServer;
using Germinator.Maintenance;

namespace Germinator.Services
{
    public class BaseService : BaseComponent
    {
        [XmlElement("MaximumNumberOfInstances")]
        [ComponentProperty("Maximum number of instances")]
        public int MaximumNumberOfInstances { get; set; }

        [XmlArray(ElementName = "CompatibleHosts")]
        [ComponentProperty("Which host can this service run on?")]
        public string[] CompatibleHosts { get; set; }

        /// <summary>
        /// Gets or sets the Maintenance task interval in seconds.
        /// </summary>
        [XmlElement("MaintenanceTaskIntervalSeconds")]
        [ComponentProperty("Monitor Service Performance")]
        public int MaintenanceTaskIntervalSeconds
        {
            get { return this.mMaintenanceIntervalSeconds; }
            set { this.SetMaintenanceInterval(value); }
        }
        private int mMaintenanceIntervalSeconds;


        [XmlAttribute("IsClandestine")]
        public volatile bool IsClandestine;

        [XmlElement("ProcessArguments")]
        [ComponentProperty("Arguments passed to the process when started.")]
        public string ProcessArguments { get; set; }

        private string mAdditionalArguments = "";

        [XmlIgnore]
        public IEnumerable<RemoteServiceProcess> Instances
        {
            get
            {
                lock (this.mProcessesLock)
                {
                    return new List<RemoteServiceProcess>(this.mProcesses.Values);
                }
            }
        }

        protected string PackageIndex { get; set; }

        private Dictionary<string, RemoteServiceProcess> mProcesses = new Dictionary<string, RemoteServiceProcess>();
        private object mProcessesLock = new object();
        private MaintenanceTask mMaintenanceTask;


        public BaseService() // For serialization
        {
        }

        public BaseService(string index, string displayName)
            : base(index, displayName)
        {
            this.PackageIndex = index;
            this.MaximumNumberOfInstances = 1;
            this.IsDetachable = true;
            this.ProcessArguments = string.Empty;
        }

        public string GetStatus(string host)
        {
            RemoteServiceProcess serviceInstance = null;
            lock (this.mProcessesLock)
            {
                this.mProcesses.TryGetValue(host, out serviceInstance);
            }

            if (serviceInstance != null)
            {
                if (serviceInstance.PingStatus == PingStatus.Error)
                {
                    return serviceInstance.ErrorText;
                }

                return this.OnGetStatus(serviceInstance);
            }
            return null;
        }

        public PingStatus BlockingPing(string host)
        {
            RemoteServiceProcess serviceInstance = null;
            lock (this.mProcessesLock)
            {
                this.mProcesses.TryGetValue(host, out serviceInstance);
            }

            if (serviceInstance != null && serviceInstance.Process != null)
            {
                CustomMessageOperation operation = serviceInstance.Germ.QueueCustomRequestOperation(serviceInstance.Process.ProcessId,
                            (int)CustomRequestType.Ping, new byte[] { }, null, null);

                if (!operation.AsyncWaitHandle.WaitOne(GermClient.DefaultOperationTimeout))
                {
                    serviceInstance.SetPingStatus(PingStatus.HostOffline, "");
                }
                else
                {
                    if (operation.WasSuccessful && operation.Response != null && operation.Response.Length > 0)
                    {
                        try
                        {
                            XmlDocument document = new XmlDocument();

                            document.Load(operation.Response);
                            serviceInstance.SetPingStatus(document.ChildNodes[1]);
                            serviceInstance.TimeOfLastPing = DateTime.Now;
                            this.IsEnabled = true;
                            return serviceInstance.PingStatus;
                        }
                        catch (Exception e)
                        {
                            Trace.TraceWarning("Failed to parse ping reply : {0}", e.Message);
                            DebugConsole.Instance.EnqueueTraceInfo(string.Format("Failed to parse ping reply : {0}", e.Message));
                            serviceInstance.SetPingStatus(PingStatus.ParseError, e.Message);
                            return serviceInstance.PingStatus;
                        }
                    }
                    else if (!operation.WasSuccessful && operation.IsUnharnessed)
                    {
                        // if the process is un-harnessed then set the ping status to
                        // HostOnlineButUnharnessed to inform the user that the process
                        // is un-harnessed
                        serviceInstance.SetPingStatus(PingStatus.HostOnlineButUnharnessed, "");
                        return serviceInstance.PingStatus;
                    }
                    else if (!operation.WasSuccessful && operation.Response == null)
                    {
                        // if the operation was not successful and the response is null
                        // most likely due to offline host
                        serviceInstance.SetPingStatus(PingStatus.HostOffline, "Both operation and response are failed");
                        return serviceInstance.PingStatus;
                    }
                    else if (!operation.WasSuccessful && operation.Response != null && operation.Response.Length > 0)
                    {
                        try
                        {
                            using (BinaryReader reader = new BinaryReader(operation.Response))
                            {
                                PingStatus status = (PingStatus)reader.ReadInt32();
                                serviceInstance.SetPingStatus(status, reader.ReadString());
                                serviceInstance.TimeOfLastPing = DateTime.Now;
                            }

                            return serviceInstance.PingStatus;
                        }
                        catch (Exception e)
                        {
                            Trace.TraceWarning("Failed to parse ping reply : {0}", e.Message);
                            DebugConsole.Instance.EnqueueTraceInfo(string.Format("Failed to parse ping reply : {0}", e.Message));
                            serviceInstance.SetPingStatus(PingStatus.ParseError, e.Message);
                            return serviceInstance.PingStatus;
                        }
                    }

                    serviceInstance.SetPingStatus(PingStatus.HostOnlineButUnactive, "");
                }
                return serviceInstance.PingStatus;
            }

            return PingStatus.Error;
        }

        public override bool CanExecuteOn(GermClient germ)
        {
            return (this.IsClandestine && germ.IsUndercover) || (!this.IsClandestine && !germ.IsUndercover);
        }

        internal override void DefaultValue()
        {
            this.MaximumNumberOfInstances = 1;
            this.MaintenanceTaskIntervalSeconds = 0;
            this.IsDetachable = true;
            this.ProcessArguments = string.Empty;
        }

        /// <summary>
        /// Restart a particular remote instance of this service.
        /// </summary>
        /// <param name="service"></param>
        protected void RestartInstance(RemoteServiceProcess service)
        {   
            // always check the inconsistency between the index and package index
            this.CheckPackageIndex();
            ServicePackageDescription package = PackageManager.Instance.GetPackage(this.PackageIndex);
            if (package == null)
            {
                Trace.TraceError("Cannot re-start {0} process becuase no suitable package was found", this.DisplayName);
                DebugConsole.Instance.EnqueueTraceInfo(string.Format("Cannot re-start {0} process becuase no suitable package was found", this.DisplayName));
                return;
            }

            if (service.Process == null)
            {
                Trace.TraceError("Cannot re-start {0} process becuase no existing process id is known", this.DisplayName);
                DebugConsole.Instance.EnqueueTraceInfo(string.Format("Cannot re-start {0} process becuase no existing process id is known", this.DisplayName));
                return; // May not have been commandeered yet.
            }

            lock (this.mProcessesLock)
            {
                if (!this.mProcesses.ContainsKey(service.Germ.Name))
                {
                    Trace.TraceError("Cannot re-start {0} process becuase no existing process id is known", this.DisplayName);
                    DebugConsole.Instance.EnqueueTraceInfo(string.Format("Cannot re-start {0} process becuase no existing process id is known", this.DisplayName));
                    return; // Don't restart if it is not known
                }
            }

            this.Stop(service);

            bool wasStarted = this.UploadAndStartService(service.Germ, service, service.Process.Args, package);
            if (!this.IsEnabled && wasStarted)
            {
                this.IsEnabled = true;
            }
        }

        protected void StartInstance(RemoteServiceProcess service)
        {
            this.CheckPackageIndex();
            ServicePackageDescription package = PackageManager.Instance.GetPackage(this.PackageIndex);
            if (package == null)
            {
                Trace.TraceError("Cannot re-start {0} process becuase no suitable package was found", this.DisplayName);
                DebugConsole.Instance.EnqueueTraceInfo(string.Format("Cannot re-start {0} process becuase no suitable package was found", this.DisplayName));
                return;
            }

            if (service.Process == null)
            {
                Trace.TraceError("Cannot start {0} process becuase no existing process id known", this.DisplayName);
                DebugConsole.Instance.EnqueueTraceInfo(string.Format("Cannot start {0} process becuase no existing process id known", this.DisplayName));
                return; // May not have been commandeered yet.
            }

            lock (this.mProcessesLock)
            {
                if (!this.mProcesses.ContainsKey(service.Germ.Name))
                {
                    Trace.TraceError("Cannot start {0} process becuase no existing process id known", this.DisplayName);
                    DebugConsole.Instance.EnqueueTraceInfo(string.Format("Cannot start {0} process becuase no existing process id known", this.DisplayName));
                    return; // Don't start if it is not known
                }
            }

            bool wasStarted = this.StartService(service.Germ, service, package.RemoteDirectory, package.Executable, service.Process.Args);
            if (!this.IsEnabled && wasStarted)
            {
                this.IsEnabled = true;
            }
        }

        /// <summary>
        /// Called once for each remote service instance every maintenance interval.
        /// </summary>
        /// <param name="instance"></param>
        protected virtual void OnRegularTask(RemoteServiceProcess instance)
        {
        }

        /// <summary>
        /// Called immediately prior to a remote instance is started
        /// </summary>
        /// <param name="instance"></param>
        protected virtual void OnInstanceStarting(RemoteServiceProcess instance)
        {
        }

        /// <summary>
        /// Called immediately after a remote instance hase been successfully started.
        /// </summary>
        /// <param name="instance">Remote service process.</param>
        protected virtual void OnInstanceStarted(RemoteServiceProcess instance)
        {
            if (instance != null)
            {
                byte[] config = new byte[0];
                instance.Germ.QueueCustomRequestOperation(instance.Process.ProcessId, (int)CustomRequestType.StartPeer, config, null, null);
            }
        }

        /// <summary>
        /// Called immediately prior to stop a remote instance.
        /// </summary>
        /// <param name="instance">Remote service process.</param>
        protected virtual void OnInstanceStopped(RemoteServiceProcess instance)
        {
        }

        protected virtual string OnGetStatus(RemoteServiceProcess instance)
        {
            return null;
        }

        protected virtual void OnCommandeered(RemoteServiceProcess instance)
        {
        }

        /// <summary>
        /// Add an argument or space seperated list of arguments to the process.
        /// </summary>
        /// <param name="argument"></param>
        protected void AppendProcessArgument(string argument)
        {
            this.mAdditionalArguments = " " + argument.TrimStart(' ');
        }

        /// <summary>
        /// Start the service. 
        /// </summary>
        /// <param name="potentialGerms"></param>
        /// <param name="arguments"></param>
        internal virtual void Start(IEnumerable<GermClient> potentialGerms, bool startEvenIfAlreadyRunning)
        {
            this.CheckPackageIndex();
            ServicePackageDescription package = PackageManager.Instance.GetPackage(this.PackageIndex);
            if (package == null)
            {
                throw new System.NullReferenceException(string.Format("Cannot start {0} becuase no suitable package was found", this.DisplayName));
            }

            foreach (GermClient client in potentialGerms)
            {
                bool constranitsSatisfied = false;
                RemoteServiceProcess service = null;

                lock (this.mProcessesLock)
                {
                    if (!this.mProcesses.TryGetValue(client.Name, out service))
                    {
                        service = new RemoteServiceProcess();
                        service.Germ = client;
                    }
                    else if (!startEvenIfAlreadyRunning)
                    {
                        continue;
                    }
                }

                if (this.CompatibleHosts != null)
                {
                    foreach (string host in this.CompatibleHosts)
                    {
                        if (host == client.Name)
                        {
                            constranitsSatisfied = true;
                            break;
                        }
                    }
                }
                else
                {
                    constranitsSatisfied = true;
                }

                if (this.IsClandestine)
                {
                    constranitsSatisfied = constranitsSatisfied && (client.IsUndercover && service.Process == null);
                }
                else
                {
                    constranitsSatisfied = constranitsSatisfied && (!client.IsUndercover && service.Process == null);
                }

                //// TODO: fix this,
                //// Possibly a bug, it should check the maximum number of instances that already running on the corresponding germ name

                //// constranitsSatisfied = constranitsSatisfied && this.mProcesses.Count < this.MaximumNumberOfInstances;
                
                //// At this point we will assume that each germ can only run one particular service (e.g. we can't start two seed peer in
                //// one remote machine)

                constranitsSatisfied = constranitsSatisfied && !this.mProcesses.ContainsKey(client.Name);

                if (constranitsSatisfied)
                {
                    this.UploadAndStartService(client, service, this.ProcessArguments, package);
                }
            }

            //// IsEnabled shouldn't be use anymore to identify the running or stoping services, except for Dei Service
            //// TODO: fix this, is should be set to true if and only if the service is sucessfully started
            if (this.Index.Equals("Dei"))
            {
                this.IsEnabled = true;
            }

            this.StartMaintenanceTask();
        }

        /// <summary>
        /// Restart the service
        /// </summary>
        /// <param name="germName"></param>
        internal void Restart(string germName)
        {
            RemoteServiceProcess instance = null;
            if (this.mProcesses.TryGetValue(germName, out instance))
            {
                this.RestartInstance(instance);
            }
        }


        /// <summary>
        /// Stop the service
        /// </summary>
        internal void Stop()
        {
            lock (this.mProcessesLock)
            {
                foreach (RemoteServiceProcess service in this.mProcesses.Values)
                {
                    this.Stop(service);
                }
                this.mProcesses.Clear();
            }

            this.IsEnabled = false;
            this.StopMaintenanceTask();

            Trace.TraceInformation("All {0} processes stopped.", this.DisplayName);
            DebugConsole.Instance.EnqueueTraceInfo(string.Format("All {0} processes stopped.", this.DisplayName));
        }

        internal void Stop(string germName)
        {
            lock (this.mProcessesLock)
            {
                RemoteServiceProcess service;
                if(this.mProcesses.TryGetValue(germName, out service))
                {
                    this.Stop(service);
                    this.mProcesses.Remove(germName);
                }

                if (this.mProcesses.Count == 0)
                {
                    // there is no active processes
                    this.IsEnabled = false;
                    this.StopMaintenanceTask();

                    Trace.TraceInformation("All {0} processes stopped.", this.DisplayName);
                    DebugConsole.Instance.EnqueueTraceInfo(string.Format("All {0} processes stopped.", this.DisplayName));
                }
            }
        }
        
        internal void CommandeerInstance(GermClient client, ProcessDescription description)
        {
            //// This is not relevant anymore in the current implementation, is enabled doesn't mean anything

            //if (!this.IsEnabled)
            //{
            //    return; // Do not take conrol of processes if disabled. TODO : // should we kill the process ?
            //}

            lock (this.mProcessesLock)
            {
                if (!this.mProcesses.ContainsKey(client.Name))
                {
                    RemoteServiceProcess serviceInstance = new RemoteServiceProcess() { Germ = client, Process = description };
                    this.mProcesses[client.Name] = serviceInstance;

                    Trace.TraceInformation("Discovered {0} remote process ({1}) on {2}", this.DisplayName, description.ProcessId, client.Name);
                    DebugConsole.Instance.EnqueueTraceInfo(string.Format("Discovered {0} remote process ({1}) on {2}", this.DisplayName, description.ProcessId, client.Name));

                    this.StartMaintenanceTask();
                    this.IsEnabled = true;
                    this.OnCommandeered(serviceInstance);
                }
            }

            return;
        }



        private void StartMaintenanceTask()
        {
            if (this.mMaintenanceTask == null && this.mMaintenanceIntervalSeconds > 0)
            {
                this.SetMaintenanceInterval(this.mMaintenanceIntervalSeconds);
            }
        }

        protected void StopMaintenanceTask()
        {
            if (this.mMaintenanceTask != null)
            {
                MaintenanceTaskManager.Instance.RemoveTask(this.mMaintenanceTask);
                this.mMaintenanceTask = null;
            }
        }

        private void SetMaintenanceInterval(int intervalSeconds)
        {
            this.mMaintenanceIntervalSeconds = intervalSeconds;
            if (intervalSeconds > 0)
            {
                if (this.mMaintenanceTask != null)
                {
                    this.mMaintenanceTask = MaintenanceTaskManager.Instance.SetInterval(this.mMaintenanceTask, TimeSpan.FromSeconds(intervalSeconds));
                }
                else
                {
                    this.mMaintenanceTask = MaintenanceTaskManager.Instance.AddTask(this.PerformMaintenanceTask, TimeSpan.FromSeconds(intervalSeconds));
                }
            }
            else
            {
                if (this.mMaintenanceTask != null)
                {
                    MaintenanceTaskManager.Instance.RemoveTask(this.mMaintenanceTask);
                    this.mMaintenanceTask = null;
                }
            }
        }

        private bool PerformMaintenanceTask()
        {
            lock (this.mProcessesLock)
            {
                // Copied to list to avoid collection modified exception.
                List<RemoteServiceProcess> instances = new List<RemoteServiceProcess>(this.mProcesses.Values);

                foreach (RemoteServiceProcess instance in instances)
                {
                    this.OnRegularTask(instance);
                }
            }

            return true;
        }

        protected void StopOn(string host)
        {
            lock (this.mProcessesLock)
            {
                RemoteServiceProcess serviceInstance = null;
                if (this.mProcesses.TryGetValue(host, out serviceInstance))
                {
                    this.Stop(serviceInstance);
                    this.mProcesses.Remove(host);
                }
            }
        }

        private void Stop(RemoteServiceProcess serviceInstance)
        {
            if (serviceInstance.Process != null)
            {
                this.OnInstanceStopped(serviceInstance);
                serviceInstance.Germ.QueueCustomRequestOperation(serviceInstance.Process.ProcessId, (int)CustomRequestType.StopPeer, new byte[] { }, null, null);
                serviceInstance.Germ.QueueKillOperation(serviceInstance.Process.ProcessId, null, null);
            }
        }

        private bool UploadAndStartService(GermClient client, RemoteServiceProcess process, string arguments, ServicePackageDescription package)
        {
            if (package == null)
            {
                return false;
            }

            if (!package.AreAllFilesAvailable())
            {
                Trace.TraceWarning("Failed to start {0} process : Files missing", this.DisplayName);
                DebugConsole.Instance.EnqueueTraceInfo(string.Format("Failed to start {0} process : Files missing", this.DisplayName));
                return false;
            }

            if (arguments == null)
            {
                arguments = "";
            }

            // Note: only add the additional arguments if it is not added yet.
            if (!arguments.Contains(this.mAdditionalArguments))
            {
                arguments = string.Concat(arguments, " ", this.mAdditionalArguments);
            }

            UploadOperation uploadOperation = client.QueueUploadOperation(package.RemoteDirectory, package.DependancyFiles,
                delegate(IAsyncResult ar)
                {
                    if (ar.IsCompleted)
                    {
                        this.StartService(client, process, package.RemoteDirectory, package.Executable, arguments);
                    }
                }, null);

            return true;
        }

        private bool StartService(GermClient client, RemoteServiceProcess process, string remoteDirectory, string executable, string arguments)
        {
            if (arguments == null)
            {
                arguments = "";
            }

            lock (this.mProcessesLock)
            {
                this.mProcesses[client.Name] = process;
            }

            this.OnInstanceStarting(process);
            ExecuteOperation executeOperation = client.QueueStartOperation(this.Index, remoteDirectory, executable, arguments,
                delegate(IAsyncResult ar2)
                {
                    ExecuteOperation startOperation = ar2 as ExecuteOperation;
                    if (startOperation != null && startOperation.IsCompleted && startOperation.ProcessDescription.ProcessId > 0)
                    {
                        process.Process = startOperation.ProcessDescription;
                        this.OnInstanceStarted(process);
                        Trace.TraceInformation("{0} process {1} ({2}) started.", this.DisplayName, process.Process.Name, process.Process.ProcessId);
                        DebugConsole.Instance.EnqueueTraceInfo(string.Format("{0} process {1} ({2}) started.", this.DisplayName, process.Process.Name, process.Process.ProcessId));
                    }
                    else
                    {
                        Trace.TraceWarning("Failed to start {0} process", this.DisplayName);
                        DebugConsole.Instance.EnqueueTraceInfo(string.Format("Failed to start {0} process", this.DisplayName));
                        lock (this.mProcessesLock)
                        {
                            this.mProcesses.Remove(client.Name);
                        }
                    }

                }, null);

            return true;
        }

        private void CheckPackageIndex()
        {
            //// TODO: Fix this, package index should be part of XML deserializer
            if (this.PackageIndex == null)
            {
                this.PackageIndex = this.Index;
            }
            else if (this.PackageIndex.Equals("Arbitration") && !this.PackageIndex.Equals("Dei") 
                && this.PackageIndex != this.Index)
            {
                this.PackageIndex = this.Index;
            }
        }
    }
}
