﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

using GermHarness;
using Germinator.Components;
using Germinator.Notification;

namespace Germinator.Services
{

    public class PeerService : BaseService
    {
        [ComponentProperty("Listening port")]
        public int ListeningPort { get; set; }

        protected BadumnaComponent mConfiguration;
        protected object mConfigurationLock = new object();

        private Configuration.NotificationHandler[] mNotificationHandlers;

        public PeerService() // For serialization
        { }

        public PeerService(string name, string displayName)
            : base(name, displayName)
        {
            this.ListeningPort = 20000;
        }

        internal virtual void StartPeerService(IEnumerable<GermClient> potentialGerms, BadumnaComponent configurationComponent, ApplicationComponent applicationComponent)
        {
            lock (this.mConfigurationLock)
            {
                this.mConfiguration = configurationComponent;
            }

            // If the application name is set then add --app_name=ApplicationName into the process arguments
            if (!string.IsNullOrEmpty(applicationComponent.ApplicationName) && !this.ProcessArguments.Contains("--application-name"))
            {
                this.ProcessArguments += string.Format(" --application-name={0}", applicationComponent.ApplicationName);
            }

            base.Start(potentialGerms, false);
        }

        internal virtual void RestartPeerService(string germName)
        {
            IEnumerable<RemoteServiceProcess> instances = this.Instances;
            foreach (RemoteServiceProcess instance in instances)
            {
                if (instance.Germ.Name.Equals(germName))
                {
                    this.RestartInstance(instance);
                    break;
                }
            }
        }

        internal override void DefaultValue()
        {
            base.DefaultValue();
            this.ListeningPort = 20000;
        }

        protected override void OnInstanceStarted(RemoteServiceProcess instance)
        {
            lock (this.mConfigurationLock)
            {
                if (instance != null && this.mConfiguration != null)
                {
                    byte[] config = null;
                    using (System.IO.MemoryStream stream = new System.IO.MemoryStream())
                    {
                        this.mConfiguration.BuildConfigXml(this.ListeningPort, stream);
                        stream.Flush();
                        config = stream.ToArray();
                    }

                    if (config != null)
                    {
                        instance.Germ.QueueCustomRequestOperation(instance.Process.ProcessId, (int)CustomRequestType.StartPeer, config,
                            delegate(IAsyncResult ar)
                            {
                                CustomMessageOperation operation = ar as CustomMessageOperation;
                                this.OnInstanceStarted(instance.Process.ProcessId, operation != null && operation.WasSuccessful);
                                if (operation.WasSuccessful)
                                {
                                    DebugConsole.Instance.EnqueueTraceInfo(string.Format("{0} peer has beed successfully started", this.DisplayName));
                                }
                                else
                                {
                                    DebugConsole.Instance.EnqueueTraceInfo(string.Format("Failed to start {0} peer", this.DisplayName), System.Diagnostics.TraceLevel.Warning);
                                }
                            }
                            , null);
                    }
                }
            }
        }

        protected override string OnGetStatus(RemoteServiceProcess instance)
        {
            if (instance != null && instance.Process != null)
            {
                CustomMessageOperation operation = instance.Germ.QueueCustomRequestOperation(instance.Process.ProcessId, (int)CustomRequestType.Status,
                    new byte[] { }, null, null);

                operation.AsyncWaitHandle.WaitOne(5000);
                if (operation.WasSuccessful)
                {
                    try
                    {
                        XmlDocument document = new XmlDocument();

                        document.Load(operation.Response);
                        return document.SelectSingleNode("/Status").OuterXml;
                    }
                    catch { }
                }
                else if (!operation.WasSuccessful && operation.IsUnharnessed)
                {
                    // Since the process is un-harnessed, then the status is not
                    // available, so instead return the status of the process it
                    // will return useful infromation to the user
                    return "Network Status is not available for un-harnessed process";
                }
            }

            return "";
        }

        protected virtual void OnInstanceStarted(int processId, bool successful)
        {
        }

        protected override void OnRegularTask(RemoteServiceProcess instance)
        {
            PingStatus status = base.BlockingPing(instance.Germ.Name);

            switch (status)
            {
                case PingStatus.OnlineAndActive:
                    break; // Do nothing;

                case PingStatus.ActiveButOffline:
                    // Do not attempt a restart just yet
                    // base.RestartInstance(instance);
                    break;

                case PingStatus.HostOnlineButUnactive:
                    // Start the process 
                    // base.RestartInstance(instance);
                    break;

                case PingStatus.HostOnlineButUnharnessed:
                    break; // Do nothing

                case PingStatus.ActiveButNotInitialized:
                    // this.RestartPeerInstance(instance);
                    break;
                    
                case PingStatus.ActiveButNotLoggedIn:
                    // TODO : Do something to fix it !
                    if (mNotificationHandlers[(int)TypeOfNotification.ActiveButNotLoggedIn] != null && instance.Process != null)
                    {
                        Notification.Notification notification =
                            new Notification.Notification(instance.Process.Name, instance.Process.ProcessId,
                                instance.Germ.Name, TypeOfNotification.ActiveButNotLoggedIn, instance.PingStatus.ToString(),
                                PriorityLevel.Medium);

                        mNotificationHandlers[(int)TypeOfNotification.ActiveButNotLoggedIn](notification);
                    }
                    break;

                case PingStatus.HostOffline:
                    // TODO : HostOffline is no longer a valid status, as now we have the ExitWithError status.
                    ////if (mNotificationHandlers[(int)TypeOfNotification.HostOffline] != null && instance.Process != null)
                    ////{
                    ////    Notification.Notification notification =
                    ////        new Notification.Notification(instance.Process.Name, instance.Process.ProcessId,
                    ////            instance.Germ.Name, TypeOfNotification.HostOffline, instance.PingStatus.ToString(),
                    ////            PriorityLevel.High);

                    ////    mNotificationHandlers[(int)TypeOfNotification.HostOffline](notification);
                    ////}
                    break;

                case PingStatus.Error:
                    // TODO : Signal ping error event...
                    if (mNotificationHandlers[(int)TypeOfNotification.Error] != null && instance.Process != null)
                    {
                        Notification.Notification notification =
                            new Notification.Notification(instance.Process.Name, instance.Process.ProcessId,
                                instance.Germ.Name, TypeOfNotification.Error, instance.PingStatus.ToString(), PriorityLevel.High);

                        mNotificationHandlers[(int)TypeOfNotification.Error](notification);
                    }
                    //base.RestartInstance(instance);
                    break;
                
                case PingStatus.ExitWithError:
                    if(mNotificationHandlers[(int)TypeOfNotification.ExitWithError] != null && instance.Process != null)
                    {
                        Notification.Notification notification = new Notification.Notification(
                                                                    instance.Process.Name,
                                                                    instance.Process.ProcessId,
                                                                    instance.Germ.Name,
                                                                    TypeOfNotification.ExitWithError,
                                                                    instance.ErrorText,
                                                                    PriorityLevel.High);
                        mNotificationHandlers[(int)TypeOfNotification.ExitWithError](notification);

                        // requesting stop doing maintenance task as the process has exited.
                        this.StopMaintenanceTask();
                    }
                    break;
            }
        }


        private void RestartPeerInstance(RemoteServiceProcess instance)
        {
            this.OnInstanceStarted(instance); // Sends a request to start peer.
        }

        public void SubscribeNotificationEvent(Configuration.NotificationHandler[] eventHandlers)
        {
            mNotificationHandlers = eventHandlers;
        }
    }
}
