﻿using System;
using System.Collections.Generic;
using System.Text;

using Germinator.Components;

namespace Germinator.Services
{
    [Component("Public peer")]
    public class PublicPeerComponent : PeerService
    {
        public PublicPeerComponent()
            : base("PublicPeer")
        {
        }
    }
}
