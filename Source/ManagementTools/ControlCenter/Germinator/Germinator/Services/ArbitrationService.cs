﻿//---------------------------------------------------------------------------------
// <copyright file="ArbitrationService.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
// <summary>Arbitration service class.</summary>
//---------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Germinator.Components;

namespace Germinator.Services
{
    /// <summary>
    /// Arbitration service class derived from PeerService class, the ArbitrationService will be application specific, the developer 
    /// should create their own arbitration service and added into ControlCenter via PackageBuilder (Presto Manifesto) mechanism.
    /// </summary>
    public class ArbitrationService : PeerService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ArbitrationService"/> class.
        /// </summary>
        public ArbitrationService()
            : base("Arbitration", "Arbitration service")
        { 
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ArbitrationService"/> class.
        /// </summary>
        /// <param name="index">Service index name.</param>
        /// <param name="displayName">Service display name.</param>
        public ArbitrationService(string index, string displayName)
            : base(index, displayName)
        {
        }

        /// <summary>
        /// Gets or sets a value indicating whether the DistributedLookup functionality is used.
        /// </summary>
        /// <value>Distributed Lookup.</value>
        [ComponentProperty("Distributed Lookup")]
        public bool DistributedLookup { get; set; }

        /// <summary>
        /// Gets or sets the Arbitration name property.
        /// </summary>
        /// <value>Arbitration Name.</value>
        [ComponentProperty("Arbitration Name")]
        public string ArbitrationName { get; set; }

        /// <summary>
        /// Overide the StartPeerService method, make sure to add the arbitration module to the Arbitration service NetworkConfig.xml.
        /// </summary>
        /// <param name="potentialGerms">List of potential germs.</param>
        /// <param name="configurationComponent">Badumna configuration components.</param>
        /// <param name="applicationComponent">Application configuration components.</param>
        internal override void StartPeerService(IEnumerable<GermClient> potentialGerms, BadumnaComponent configurationComponent, ApplicationComponent applicationComponent)
        {
            configurationComponent.EnabledArbitrationServer(
                this.ArbitrationName,
                this.DistributedLookup,
                string.Format("{0}:{1}", this.CompatibleHosts.Single(), this.ListeningPort));
            configurationComponent.DisabledOverloadServer();

            // Note : arbitration should use a seedpeer.
            // configurationComponent.ClearSeedPeers();
            base.StartPeerService(potentialGerms, configurationComponent, applicationComponent);
        }

        /// <summary>
        /// Set the component back to the default value.
        /// </summary>
        internal override void DefaultValue()
        {
            base.DefaultValue();
            this.DistributedLookup = false;
            this.ArbitrationName = string.Empty;
        }
    }
}
