﻿//---------------------------------------------------------------------------------
// <copyright file="HttpTunnelingService.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
// <summary>Http Tunneling service class.</summary>
//---------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Germinator.Components;

namespace Germinator.Services
{
    /// <summary>
    /// Http Tunneling service is one of default services provided in Control Center. This service is not 
    /// a Badumna peer process thus it derived from BaseService not a PeerService class.
    /// </summary>
    public class HttpTunnelingService : BaseService
    {
        /// <summary>
        /// The http prefix which the http tunneling server listen on.
        /// </summary>
        private string prefix;

        /// <summary>
        /// Initializes a new instance of the <see cref="HttpTunnelingService"/> class.
        /// </summary>
        public HttpTunnelingService()
            : base("HttpTunneling", "Http Tunneling")
        {
            //// Default http prefix.
            this.SetPrefix("http://+:21247/");
        }

        /// <summary>
        /// Gets or sets the port the http prefix which the http tunneling server listen on.
        /// </summary>
        /// <value>The http prefix which the http tunneling server listen on.</value>
        [ComponentProperty("The http prefix for tunneling server")]
        public string Prefix
        {
            get { return this.prefix; }
            set { this.SetPrefix(value); }
        }

        /// <summary>
        /// Set the component back to the default value.
        /// </summary>
        internal override void DefaultValue()
        {
            base.DefaultValue();
            this.SetPrefix("http://+:21247/");
        }

        /// <summary>
        /// Set the http prefix which the http tunneling server listen on and included in 
        /// the process argument.
        /// </summary>
        /// <param name="prefix">The http prefix which the http tunneling server listen on.</param>
        private void SetPrefix(string prefix)
        {
            this.prefix = prefix;
            this.AppendProcessArgument(prefix);
        }
    }
}
