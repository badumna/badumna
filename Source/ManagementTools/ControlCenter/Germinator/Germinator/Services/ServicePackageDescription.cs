﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

using Badumna.Package;

namespace Germinator.Services
{
    public class ServicePackageDescription
    {
        public Dictionary<string, string> DependancyFiles { get; private set; }
        public String Executable { get; private set; }
        public String ApplicationName { get; private set; }
        public String RemoteDirectory { get; private set; }
        public String ManifestXml { get; set; }

        private string mManifestFilename;
        private string mCommonDirectory;
        private DateTime mManifestModificationTime;

        public ServicePackageDescription()
        {
            this.DependancyFiles = new Dictionary<string, string>();
        }

        public bool AreAllFilesAvailable()
        {
            bool isComplte = true;
            foreach (string file in this.DependancyFiles.Keys)
            {
                if (!File.Exists(file))
                {
                    DebugConsole.Instance.EnqueueTraceInfo(string.Format("File '{0}' required for {1} is missing", file, this.ApplicationName), System.Diagnostics.TraceLevel.Warning);
                    isComplte = false;
                }
            }

            return isComplte;
        }

        public bool IsOutOfDate()
        {
            if (string.IsNullOrEmpty(this.mManifestFilename) || this.mManifestModificationTime == null)
            {
                return false;
            }

            return File.GetLastWriteTime(this.mManifestFilename) > this.mManifestModificationTime;
        }

        public void ReLoad()
        {
            this.DependancyFiles.Clear();
            this.LoadPackageDetails(this.mManifestFilename, this.mCommonDirectory);
        }

        public bool LoadPackageDetails(string manifestFilename, string commonDirectory)
        {
            if (!File.Exists(manifestFilename))
            {
                return false;
            }

            // Include all files in the common directory.
            if (!string.IsNullOrEmpty(commonDirectory) && Directory.Exists(commonDirectory))
            {
                this.mCommonDirectory = commonDirectory;
                foreach (string filename in Directory.GetFiles(commonDirectory))
                {
                    this.DependancyFiles.Add(filename, "");
                }
            }

            // Include all the necessary package files.
            this.mManifestFilename = manifestFilename;
            Manifest manifest = Manifest.Load(manifestFilename);
            if (manifest != null)
            {
                this.ManifestXml = File.ReadAllText(manifestFilename);
                string manifestDirectory = Path.GetDirectoryName(manifestFilename);
                this.DependancyFiles.Add(Path.Combine(manifestDirectory, manifest.PackageFilename), "Packages");
                if (!manifest.IsComplete && manifest.PackageFilename != manifest.LastCompletePackageFileName && !manifest.LastCompletePackageFileName.Equals(".zip"))
                {
                    this.DependancyFiles.Add(Path.Combine(manifestDirectory, manifest.LastCompletePackageFileName), "Packages");
                }

                this.Executable = "ApplicationStarter.exe";
                this.ApplicationName = manifest.ApplicationName;
                this.RemoteDirectory = this.ApplicationName;
                this.mManifestModificationTime = File.GetLastWriteTime(manifestFilename);

                foreach (char invalidChar in Path.GetInvalidPathChars())
                {
                    this.RemoteDirectory = this.RemoteDirectory.Replace(invalidChar, '_');
                }

                return true;
            }

            return false;
        }

    }
}
