﻿//---------------------------------------------------------------------------------
// <copyright file="DeiService.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
// <summary>Dei account management service class.</summary>
//---------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;

using Germinator.Components;

namespace Germinator.Services
{
    /// <summary>
    /// DeiService class responsible to run the dei service, (note: there are two types of dei service which are
    /// Dei-SQLite and Dei-MySql, depends on the storage type).
    /// </summary>
    public class DeiService : BaseService
    {
        /// <summary>
        /// Dei port number.
        /// </summary>
        private int port;

        /// <summary>
        /// Dei server host name.
        /// </summary>
        private string hostname;

        /// <summary>
        /// Initializes a new instance of the <see cref="DeiService"/> class.
        /// </summary>
        public DeiService()
            : base("Dei", "Dei account management")
        {
            this.SetPort(21248); //// default port
            this.MaximumNumberOfInstances = 1;
            this.PublicKeyValidityPeriod = (int)TimeSpan.FromDays(30).TotalHours;
            this.ParticipationValidityPeriod = (int)TimeSpan.FromDays(1).TotalMinutes;
        }

        /// <summary>
        /// Gets or sets the public key validity period.
        /// </summary>
        [ComponentProperty("Time (in hours) that the public key is valid for. (Not Active)")]
        public int PublicKeyValidityPeriod { get; set; }

        /// <summary>
        /// Gets or sets the participation validity period.
        /// </summary>
        [ComponentProperty("Time (in minutes) that the participation key is valid for")]
        public int ParticipationValidityPeriod { get; set; }

        /// <summary>
        /// Gets or sets the port the dei server listens on.
        /// </summary>
        [ComponentProperty("The port the dei server listens on")]
        public int Port 
        { 
            get { return this.port; } 
            set { this.SetPort(value); } 
        }

        /// <summary>
        /// Gets or sets a value indicating whether the ssl connection is used or not.
        /// </summary>
        [ComponentProperty("Listening with Ssl connection")]
        public bool ListenWithSsl { get; set; }

        /// <summary>
        /// Gets or sets the certificate passphrase.
        /// </summary>
        internal string Passphrase { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the custom certificate is used.
        /// </summary>
        internal bool UseCustomCertificate { get; set; }

        /// <summary>
        /// Gets the Dei server host name.
        /// </summary>
        /// <returns>Returns the dei server hostname.</returns>
        internal string GetHostname()
        {
            return this.hostname;
        }

        /// <summary>
        /// Override the Start method, build a proccess argument for dei service.
        /// </summary>
        /// <param name="potentialGerms">Potentials germ host.</param>
        /// <param name="startEvenIfAlreadyRunning">The value indicating whether the service is already running.</param>
        internal override void Start(IEnumerable<GermClient> potentialGerms, bool startEvenIfAlreadyRunning)
        {
            //// Note: build process argument first.
            this.BuildProcessArgument();
            base.Start(potentialGerms, startEvenIfAlreadyRunning);
        }

        /// <summary>
        /// Set the component back to the default value.
        /// </summary>
        internal override void DefaultValue()
        {
            base.DefaultValue();
            this.SetPort(21248); //// default port
            this.PublicKeyValidityPeriod = (int)TimeSpan.FromDays(30).TotalHours;
            this.ParticipationValidityPeriod = (int)TimeSpan.FromDays(1).TotalMinutes;
            this.ListenWithSsl = false;
        }

        /// <summary>
        /// Override the OnCommandeered method.
        /// </summary>
        /// <param name="instance">Remote process instance.</param>
        protected override void OnCommandeered(RemoteServiceProcess instance)
        {
            base.OnCommandeered(instance);
            this.SetHostname(instance.Germ);
        }

        /// <summary>
        /// Override the OnInstanceStarted method.
        /// </summary>
        /// <param name="instance">Remote process instance.</param>
        protected override void OnInstanceStarted(RemoteServiceProcess instance)
        {
            base.OnInstanceStarted(instance);
            this.SetHostname(instance.Germ);
        }

        /// <summary>
        /// Set the dei server host name.
        /// </summary>
        /// <param name="client">Germ client.</param>
        private void SetHostname(GermClient client)
        {
            if (client != null)
            {
                string[] germHostnameParts = client.Name.Split(':');
                if (germHostnameParts != null && germHostnameParts.Length > 0)
                {
                    this.hostname = germHostnameParts[0];
                }
            }
        }

        /// <summary>
        /// Set Dei port number.
        /// </summary>
        /// <param name="port">Dei port number.</param>
        private void SetPort(int port)
        {
            this.port = port;
            this.AppendProcessArgument(string.Format("--port={0}", port));
        }

        /// <summary>
        /// Build a process argument for dei service.
        /// </summary>
        private void BuildProcessArgument()
        {
            this.ProcessArguments = string.Empty; //// Reset the process argument

            if (this.ListenWithSsl)
            {
                this.ProcessArguments += " --ssl";
            }

            if (this.UseCustomCertificate && !string.IsNullOrEmpty(this.Passphrase))
            {
                this.ProcessArguments += string.Format(" --certificate-password={0}", this.Passphrase);
            }

            this.ProcessArguments += string.Format(" -k={0}", this.ParticipationValidityPeriod);
        }
    }
}
