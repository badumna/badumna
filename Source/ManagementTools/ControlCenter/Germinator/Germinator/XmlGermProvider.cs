﻿//---------------------------------------------------------------------------------
// <copyright file="XmlGermProvider.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2010 NICTA Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Germinator
{
    /// <summary>
    /// This class handle the Germs.xml configuration file, uploaded the existing germs client to
    /// the control center, when the control center start at the first time.
    /// </summary>
    public class XmlGermProvider : IGermProvider
    {
        /// <summary>
        /// Full path of germs.xml file.
        /// </summary>
        private string filename;

        /// <summary>
        /// Germ client container.
        /// </summary>
        private GermClientContainer clients;
        
        /// <summary>
        /// Object lock.
        /// </summary>
        private object loadLock = new object();

        /// <summary>
        /// Initializes a new instance of the <see cref="XmlGermProvider"/> class.
        /// </summary>
        /// <param name="filename">Germ.xml file path.</param>
        public XmlGermProvider(string filename)
        {
            if (string.IsNullOrEmpty(filename))
            {
                throw new ArgumentException("filename");
            }

            this.filename = filename;
            this.clients = new GermClientContainer();
        }

        /// <summary>
        /// Add a new germ.
        /// </summary>
        /// <param name="host">Germ host name.</param>
        public void Add(string host)
        {
            this.clients.AddGerm(host);
            this.Save();
        }

        /// <summary>
        /// Remove a germ.
        /// </summary>
        /// <param name="hostname">Germ host name.</param>
        public void Remove(string hostname)
        {
            this.clients.RemoveGerm(hostname);
            this.Save();
        }

        /// <summary>
        /// Exactly the same as Add method. 
        /// TODO: should remove this, use Add() instead of Save().
        /// </summary>
        /// <param name="hostname">Germ host name.</param>
        public void Save(string hostname)
        {
            this.Add(hostname);
        }

        /// <summary>
        /// Load the list of germs from the file (i.e. Germs.xml file)
        /// </summary>
        /// <returns>Return the list of germs.</returns>
        public IEnumerable<string> Load()
        {
            List<string> hosts = new List<string>();

            this.clients = new GermClientContainer();
            try
            {
                using (XmlReader reader = XmlReader.Create(new StreamReader(this.filename)))
                {
                    if (reader.Read())
                    {
                        this.clients.Load(reader);
                    }
                }
            }
            catch (FileNotFoundException)
            {
                this.Save();
                DebugConsole.Instance.EnqueueTraceInfo(string.Format("Germ file not found, creating a new germ file {0}", Path.GetFileName(this.filename)), System.Diagnostics.TraceLevel.Warning);
            }

            foreach (GermClient client in this.clients.AsEnumerable())
            {
                hosts.Add(client.Name);
            }

            return hosts;
        }

        /// <summary>
        /// Save the list of germs to Germs.xml file (or can be any filename given in filename variaable).
        /// </summary>
        private void Save()
        {
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;

            using (XmlWriter writer = XmlWriter.Create(this.filename, settings))
            {
                writer.WriteStartDocument();
                this.clients.Save(writer);
            }
        }
    }
}
