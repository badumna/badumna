﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Reflection;

namespace Germinator.Notification
{
    public enum PropertyType
    {
        Int,
        String,
        Bool,
        Password,
        StringList
    }
    public class PropertyView
    {
        public PropertyType Type;
        public string Name;
        public string Index;
        public object Value;
        //internal PropertyInfo PropertyInfo; // TODO: is not used at the moment
    }
    public class EmailNotificationView : IEnumerable<PropertyView>
    {
        private Dictionary<string, PropertyView> mProperties = new Dictionary<string, PropertyView>();
        public int Count { get { return this.mProperties.Count; } }

        public EmailNotificationView() { }

        internal void SetDefaultProperty()
        {
            // TODO: use the xml atrribute to set up all property
            PropertyView IsSend = new PropertyView();
            IsSend.Type = PropertyType.Bool;
            IsSend.Value = false;
            IsSend.Name = "Send Notification via email";
            IsSend.Index = "IsSend";
            mProperties.Add("IsSend", IsSend);

            PropertyView SmtpClient = new PropertyView();
            SmtpClient.Type = PropertyType.String;
            SmtpClient.Value = "";
            SmtpClient.Name = "Smtp Client";
            SmtpClient.Index = "SmtpClient";
            mProperties.Add("SmtpClient", SmtpClient);

            PropertyView SmtpPort = new PropertyView();
            SmtpPort.Type = PropertyType.Int;
            SmtpPort.Value = "0";
            SmtpPort.Name = "Smtp Port";
            SmtpPort.Index = "SmtpPort";
            mProperties.Add("SmtpPort", SmtpPort);

            PropertyView Username = new PropertyView();
            Username.Type = PropertyType.String;
            Username.Value = "";
            Username.Name = "Username";
            Username.Index = "username";
            mProperties.Add("username", Username);

            PropertyView Password = new PropertyView();
            Password.Type = PropertyType.Password;
            Password.Value = "";
            Password.Name = "Password";
            Password.Index = "password";
            mProperties.Add("password", Password);

            PropertyView From = new PropertyView();
            From.Type = PropertyType.String;
            From.Value = "";
            From.Name = "From";
            From.Index = "from";
            mProperties.Add("from", From);

            PropertyView To = new PropertyView();
            To.Type = PropertyType.String;
            To.Value = "";
            To.Name = "To";
            To.Index = "to";
            mProperties.Add("to", To);
        }

        public void SetProperty(string propertyIndex, object value)
        {
            if (value == null || string.IsNullOrEmpty(propertyIndex))
            {
                DebugConsole.Instance.EnqueueTraceInfo("Invalid property index or value.", System.Diagnostics.TraceLevel.Error);
                return;
            }

            PropertyView property = null;
            if (this.mProperties.TryGetValue(propertyIndex, out property))
            {
                try
                {
                    if (property.Type == PropertyType.Int)        
                    {
                        value = int.Parse(value as string);
                    }
                    if (property.Type == PropertyType.Bool)
                    {
                        value = (value as string).ToLower().StartsWith("true");
                    }

                    property.Value = value;
                }
                catch (Exception e)
                {
                    DebugConsole.Instance.EnqueueTraceInfo(string.Format("Failed to set property {1} : {0}", e.Message, propertyIndex), System.Diagnostics.TraceLevel.Error);
                }
            }
        }

        public void addProperty(string Type, string Index, string Name, object Value)
        {
            PropertyView property = new PropertyView();
            property.Index = Index;
            property.Name = Name;
            property.Type = (PropertyType)Enum.Parse(typeof(PropertyType), Type);
            try
            {
                if (property.Type == PropertyType.Int)
                {
                    Value = int.Parse(Value as string);
                }
                if (property.Type == PropertyType.Bool)
                {
                    Value = (Value as string).ToLower().StartsWith("true");
                }

                property.Value = Value;
            }
            catch (Exception e)
            {
                DebugConsole.Instance.EnqueueTraceInfo(string.Format("Failed to add property {1} : {0}", e.Message, Index), System.Diagnostics.TraceLevel.Error);
            }
            mProperties.Add(property.Index, property);
        }

        internal object getProperty(string indexProperty)
        {
            PropertyView property;
            if(this.mProperties.TryGetValue(indexProperty,out property))
            {
                return property.Value;
            }

            return null;
        }

        #region IEnumerable<PropertyView> Members

        public IEnumerator<PropertyView> GetEnumerator()
        {
            return this.mProperties.Values.GetEnumerator();
        }

        #endregion

        #region IEnumerable Members

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return this.mProperties.Values.GetEnumerator();
        }

        #endregion
    }
}
