﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Germinator.Notification
{
    public class EmailNotificationContainer
    {
        private EmailNotificationView mEmailNotificationConfig = new EmailNotificationView();
        private static object mLoadLock = new object();

        // Default constructor
        public EmailNotificationContainer() {}

        public EmailNotificationView getEmailNotificationConfig()
        {
            return this.mEmailNotificationConfig;
        }

        internal void CreateNewConfiguration()
        {
            this.mEmailNotificationConfig.SetDefaultProperty();
        }

        internal void Save(XmlWriter writer)
        {
            //TODO: save the email notification config
            lock (EmailNotificationContainer.mLoadLock)
            {
                try
                {
                    writer.WriteStartElement("EmailNotificationConfig");
                    writer.WriteAttributeString("Version", "1.0");
                    IEnumerator<PropertyView> properties =  mEmailNotificationConfig.GetEnumerator();

                    for (int i = 0; i < mEmailNotificationConfig.Count; i++)
                    {
                        properties.MoveNext();
                        PropertyView property = properties.Current; 

                        writer.WriteStartElement("EmailNotificationProperty");
                        writer.WriteAttributeString("Type", property.Type.ToString());
                        writer.WriteAttributeString("Name", property.Name);
                        writer.WriteAttributeString("Index", property.Index);
                        writer.WriteAttributeString("Value", property.Value.ToString());
                        writer.WriteEndElement();
                    }

                    writer.WriteEndElement();
                }
                catch (Exception e)
                {
                    DebugConsole.Instance.EnqueueTraceInfo(string.Format("Failed to save email notification configuration : {0}", e.Message), System.Diagnostics.TraceLevel.Error);
                }
            }
        }

        internal void Load(XmlReader reader)
        {
            lock (EmailNotificationContainer.mLoadLock)
            {
                try
                {
                    while (reader.IsStartElement() && reader.LocalName == "EmailNotificationConfig")
                    {
                        reader.Read();
                    }

                    while (reader.LocalName == "EmailNotificationProperty")
                    {
                        string Type = reader.GetAttribute("Type");
                        string Name = reader.GetAttribute("Name");
                        string Index = reader.GetAttribute("Index");
                        object Value = reader.GetAttribute("Value");
                        
                        if (mEmailNotificationConfig != null)
                        {
                            mEmailNotificationConfig.addProperty(Type, Index, Name, Value);
                        }

                        reader.Read();
                        reader.MoveToContent();
                    }

                    reader.ReadEndElement();
                }
                catch (Exception e)
                {
                    DebugConsole.Instance.EnqueueTraceInfo(string.Format("Failed to load email notification configuration : {0}", e.Message), System.Diagnostics.TraceLevel.Error);
                }
            }
        }
    }
}
