﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Germinator.Notification
{
    /// <summary>
    /// Notification Container class will be used to store all notifications
    /// from any services in any germ
    /// </summary>
    public class NotificationContainer
    {
        private List<Notification> mNotifications = new List<Notification>();
        public int Count { get { return this.mNotifications.Count; } }
        private static object mLoadLock = new object();

        // Default constructor
        public NotificationContainer() { }

        public List<Notification> getNotifications()
        {
            return this.mNotifications;
        }

        public Notification AddNotification(string serviceName, int processId, string germName,
            TypeOfNotification typeOfNotification, string errorMessage, PriorityLevel priority)
        {
            Notification newNotification = new Notification(serviceName, processId, germName,
                typeOfNotification, errorMessage, priority);
            this.mNotifications.Add(newNotification);

            return newNotification;
        }

        public Notification AddNotification(string time, string serviceName, int processId, string germName,
            TypeOfNotification typeOfNotification, string errorMessage, PriorityLevel priority)
        {
            Notification newNotification = new Notification(time, serviceName, processId, germName,
                typeOfNotification, errorMessage, priority);
            this.mNotifications.Add(newNotification);

            return newNotification;
        }

        public void AddNotification(Notification notification)
        {
            this.mNotifications.Add(notification);
        }

        public void RemoveNotification()
        {
            //TODO: revise this method, at the moment remove all notification from the list
            this.mNotifications.Clear();
        }

        /// <summary>
        /// Remove a specific Notification from the notifications list
        /// </summary>
        /// <param name="Time"></param>
        /// <param name="processId"></param>
        public void RemoveNotification(string Time, int processId)
        {
            foreach (Notification notification in this.mNotifications)
            {
                if (notification.ProcessId == processId && notification.Time == Time)
                {
                    this.mNotifications.Remove(notification);
                    return;
                }
            }
        }

        public IEnumerable<Notification> AsEnumerable()
        {
            return this.mNotifications;
        }

        // TODO: load and save method may be required in the future 

        internal void Save(XmlWriter writer)
        {
            lock (NotificationContainer.mLoadLock)
            {
                try
                {
                    writer.WriteStartElement("Notifications");
                    writer.WriteAttributeString("Version", "1.0");

                    foreach (Notification notification in this.mNotifications)
                    {
                        writer.WriteStartElement("Notification");
                        writer.WriteAttributeString("ServiceName", notification.ServiceName);
                        writer.WriteAttributeString("ProcessId", notification.ProcessId.ToString());
                        writer.WriteAttributeString("GermName", notification.GermName);
                        writer.WriteAttributeString("Type", notification.TypeOfNotification.ToString());
                        writer.WriteAttributeString("ErrorMessage", notification.ErrorMessage);
                        writer.WriteAttributeString("Time", notification.Time);
                        writer.WriteAttributeString("Priority", notification.Priority.ToString());
                        writer.WriteEndElement();
                    }
                }

                catch (Exception e)
                {
                    DebugConsole.Instance.EnqueueTraceInfo(string.Format("Failed to save notifications : {0}", e.Message), System.Diagnostics.TraceLevel.Error);
                }
            }
        }

        internal void Load(XmlReader reader)
        {
            lock (NotificationContainer.mLoadLock)
            {
                try
                {
                    if (reader.IsStartElement() && reader.LocalName == "Notifications" && reader.IsEmptyElement)
                    {
                        reader.Read();
                        return;
                    }

                    while (reader.IsStartElement() && reader.LocalName == "Notifications")
                    {
                        reader.Read();
                    }
                    while (reader.LocalName == "Notification")
                    {
                        if (this.mNotifications != null)
                        {
                            this.AddNotification(reader.GetAttribute("Time"), reader.GetAttribute("ServiceName"),
                                int.Parse(reader.GetAttribute("ProcessId")), reader.GetAttribute("GermName"),
                                (TypeOfNotification) Enum.Parse(typeof(TypeOfNotification), reader.GetAttribute("Type"), true),
                                reader.GetAttribute("ErrorMessage"), 
                                (PriorityLevel) Enum.Parse(typeof(PriorityLevel), reader.GetAttribute("Priority"), true));
                        }

                        reader.Read();
                        reader.MoveToContent();
                    }

                    reader.ReadEndElement();
                }
                catch (Exception e)
                {
                    DebugConsole.Instance.EnqueueTraceInfo(string.Format("Failed to load notifications : {0}", e.Message), System.Diagnostics.TraceLevel.Error);
                }
            }
        }
    }
}
