﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Germinator.Notification
{
    public enum TypeOfNotification
    {
        ActiveButNotLoggedIn,
        HostOffline,
        Error,
        ExitWithError
    }

    public enum PriorityLevel
    {
        Low,
        Medium,
        High
    }
    /// <summary>
    /// Notification class, will handle the notification information from the services 
    /// running on the specific germs
    /// </summary>
    public class Notification
    {
        // TODO: find all useful information that can be included
        private string mServiceName;
        public string ServiceName
        {
            get { return this.mServiceName; }
        }
        
        private int mProcessId;
        public int ProcessId
        {
            get { return this.mProcessId; }
        }

        private string mGermName;
        public string GermName
        {
            get { return this.mGermName; }
        }

        private TypeOfNotification mTypeOfNotification; // we might use enumeration for this case
        public TypeOfNotification TypeOfNotification
        {
            get { return this.mTypeOfNotification; }
        }

        private string mErrorMessage;
        public string ErrorMessage
        {
            get { return this.mErrorMessage; }
            set { this.mErrorMessage = value; }
        }

        private string mTime;
        public string Time
        {
            get { return this.mTime; }
        }

        private PriorityLevel mPriority;
        public PriorityLevel Priority
        {
            get { return this.mPriority; }
        }

        public Notification (string serviceName, int processId, string germName, 
            TypeOfNotification typeOfNotification, string errorMessage,  PriorityLevel Priority)
        {
            this.mServiceName = serviceName;
            this.mProcessId = processId;
            this.mGermName = germName;
            this.mTypeOfNotification = typeOfNotification;
            this.mErrorMessage = errorMessage;
            this.mPriority = Priority;

            DateTime currTime = DateTime.Now;
            this.mTime = "(" +currTime.ToShortDateString() + ") " + currTime.ToShortTimeString();
        }

        public Notification(string time, string serviceName, int processId, string germName,
            TypeOfNotification typeOfNotification, string errorMessage, PriorityLevel Priority)
        {
            this.mServiceName = serviceName;
            this.mProcessId = processId;
            this.mGermName = germName;
            this.mTypeOfNotification = typeOfNotification;
            this.mErrorMessage = errorMessage;
            this.mPriority = Priority;
            this.mTime = time;
        }

        public override string ToString()
        {
            return "Time: " + this.mTime + "\n" +
                   "Service Name: " + this.mServiceName + "\n" +
                   "Process Id: " + this.mProcessId + "\n" +
                   "Germn Name: " + this.mGermName + "\n" +
                   "Problem: " + this.mTypeOfNotification + "\n" +
                   "Error Message: " + this.mErrorMessage.Replace("<br/>","\n") + "\n";
        }
    }
}
