﻿//---------------------------------------------------------------------------------
// <copyright file="NotificationController.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2010 NICTA Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Xml;
using System.Xml.Serialization;

using Badumna.Package;

using GermHarness;

using Germinator.Components;
using Germinator.Notification;
using Germinator.PrestoManifesto;
using Germinator.Services;
using Germinator.GermProtocol;
using System.Web;
using System.Diagnostics;

namespace Germinator
{
    /// <summary>
    /// Configuration class is used to communicate between the controller (client side) and the germintaor (server side).
    /// </summary>
    public class Configuration
    {
        /// <summary>
        /// Load lock used when loading the configuration.
        /// </summary>
        private static object loadLock = new object();

        /// <summary>
        /// Components container.
        /// </summary>
        private ComponentContainer componentContainer;

        /// <summary>
        /// Collection of germ hosts.
        /// </summary>
        private GermClientContainer germs;

        /// <summary>
        /// The configuration provider.
        /// </summary>
        private IProvider<Configuration> provider;

        /// <summary>
        /// Email notification container.
        /// </summary>
        private EmailNotificationContainer emailNotificationContainer;

        /// <summary>
        /// Collections of notifications.
        /// </summary>
        private NotificationContainer notificationContainer;

        /// <summary>
        /// Presto manifesto project.
        /// </summary>
        private Project prestoManifestoProject;

        /// <summary>
        /// List of all running services.
        /// </summary>
        private Dictionary<string, RemoteServiceProcess> runningServices;

        /// <summary>
        /// Client listener, listen on any incoming connection from remote germs.
        /// </summary>
        private ClientListener clientListener;

        /// <summary>
        /// Gets or sets the name of the configuration.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets the germ clients as an IEnumerable object.
        /// </summary>
        public IEnumerable<GermClient> GermClients
        {
            get { return this.germs.AsEnumerable(); }
        }

        /// <summary>
        /// Gets and sets the badumna component.
        /// </summary>
        public BadumnaComponent BadumnaComponent { get; private set; }

        /// <summary>
        /// Gets the components as an IEnumerable object.
        /// </summary>
        public IEnumerable<ComponentView> Components
        {
            get { return this.componentContainer.AsEnumerable(); }
        }

        /// <summary>
        /// Gets the notifications as IEnumerable object.
        /// </summary>
        public IEnumerable<Notification.Notification> Notifications
        {
            get { return this.notificationContainer.AsEnumerable(); }
        }

        /// <summary>
        /// Gets the presto manifesto project.
        /// </summary>
        public Project PrestoManifestoProject
        {
            get { return this.prestoManifestoProject; }
        }

        /// <summary>
        /// The collections of all running services.
        /// </summary>
        public Dictionary<string, RemoteServiceProcess> RunningServices
        {
            get { return this.runningServices; }
        }

        /// <summary>
        /// Gets the germ client container.
        /// </summary>
        internal GermClientContainer Germs
        {
            get { return this.germs; }
        }

        /// <summary>
        /// Gets the runtime asp.net base directory .
        /// </summary>
        internal string BaseDirectory
        {
            get
            {
                try
                {
                    return HttpRuntime.AppDomainAppPath;
                }
                catch
                {
                    // run for unit testing.
                    return Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"..\..\..\BadumnaNetworkController");
                }
            }
        }

        /// <summary>
        /// Delegate method for handling notification.
        /// </summary>
        /// <param name="notification">Notirication instance.</param>
        public delegate void NotificationHandler(Notification.Notification notification);

        /// <summary>
        /// A value indication whether the ControlCenter is running in mono.
        /// </summary>
        public bool IsMono;

        /// <summary>
        /// Initializes a new instance of the <see cref="Configuration"/> class.
        /// </summary>
        internal Configuration()
        {
            this.Name = "Default";
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Configuration"/> class.
        /// </summary>
        /// <remarks>this function will be called and new network configuration is initiated</remarks>
        /// <param name="name">Configuration name.</param>
        /// <param name="protocolHash">Application protocol hash.</param>
        /// <param name="provider">The configuration provider.</param>
        internal Configuration(string name, string protocolHash, IProvider<Configuration> provider)
        {
            this.Name = name;
            this.provider = provider;
            this.IsMono = Type.GetType("Mono.Runtime") != null;
            this.germs = new GermClientContainer();
            this.componentContainer = new ComponentContainer();

            foreach (string componentName in ComponentContainer.Instance.ComponentNames)
            {
                this.componentContainer.Add(componentName);
            }

            // Add the protocol hash to the application component
            if (this.componentContainer.Contains(ApplicationComponent.ComponentIndex))
            {
                ComponentView compView = this.componentContainer[ApplicationComponent.ComponentIndex];
                compView.SetProperty("ApplicationName", protocolHash);
            }

            this.emailNotificationContainer = new EmailNotificationContainer();
            this.emailNotificationContainer.CreateNewConfiguration();

            this.notificationContainer = new NotificationContainer();

            this.prestoManifestoProject = new Project();

            this.runningServices = new Dictionary<string, RemoteServiceProcess>();

            this.InitializeClientListener();
            
            this.LoadPackage();
            this.LoadProject();
        }

        /// <summary>
        /// Enabled or disabled the selected/specified component.
        /// </summary>
        /// <param name="componentName">Component name.</param>
        /// <param name="isEnabled">A value indicating whether the component should be enabled.</param>
        internal void EnableComponent(string componentName, bool isEnabled)
        {
            ComponentView component = this.componentContainer[componentName];
            if (component != null)
            {
                component.Instance.IsEnabled = isEnabled;
            }

            this.Save();
        }

        /// <summary>
        /// Add component to the component container
        /// </summary>
        /// <param name="name">Component name.</param>
        /// <returns>Return the component view of this component.</returns>
        internal ComponentView AddComponent(string name)
        {
            if (this.componentContainer != null)
            {
                ComponentView newComponent = this.componentContainer.Add(name);
                this.Save();
                return newComponent;
            }

            return null;
        }

        /// <summary>
        /// Remove component from the component container given the component name.
        /// </summary>
        /// <param name="name">Component name.</param>
        public void RemoveComponent(string name)
        {
            if (this.componentContainer != null)
            {
                this.componentContainer.Remove(name);
                this.Save();
            }
        }

        /// <summary>
        /// Add notification.
        /// <remarks>
        /// TODO: this can be done inside the event handler</remarks>
        /// </summary>
        /// <param name="serviceName">Service name.</param>
        /// <param name="processId">Process id.</param>
        /// <param name="germName">Germ host name.</param>
        /// <param name="typeOfNotification">Type of notification.</param>
        /// <param name="errorMessage">Error message.</param>
        /// <param name="priority">Notification priority.</param>
        internal void AddNotification(string serviceName, int processId, string germName, TypeOfNotification typeOfNotification, string errorMessage, PriorityLevel priority)
        {
            Notification.Notification notification = null;
            if (this.notificationContainer != null)
            {
                notification = this.notificationContainer.AddNotification(serviceName, processId, germName, typeOfNotification, errorMessage, priority);
            }

            if (notification.Priority.Equals(PriorityLevel.High) && notification != null)
            {
                this.SendEmailNotification(notification);
            }
        }

        /// <summary>
        /// Return a value indicating whether the email notification is enabled.
        /// </summary>
        /// <returns>Return true it is enabled.</returns>
        public bool IsEmailNotificationEnabled()
        {
            return (bool)this.emailNotificationContainer.getEmailNotificationConfig().getProperty("IsSend");
        }

        /// <summary>
        /// Remove all the notifcations.
        /// </summary>
        public void RemoveNotification()
        {
            this.notificationContainer.RemoveNotification();
            this.Save();
        }

        /// <summary>
        /// Remove a particular notification given the process id and time.
        /// </summary>
        /// <param name="time">Time of notification.</param>
        /// <param name="processId">Process id.</param>
        public void RemoveNotification(string time, int processId)
        {
            this.notificationContainer.RemoveNotification(time, processId);
            this.Save();
        }

        /// <summary>
        /// Send an email notification, only available if the email notification is enabled.
        /// </summary>
        /// <param name="notification">The notification need to be sent.</param>
        private void SendEmailNotification(Notification.Notification notification)
        {
            // TODO: sent the notification through email
            // Do we need this to be more general ??
            EmailNotificationView emailNotificationView =
                    this.emailNotificationContainer.getEmailNotificationConfig();
            if ((bool)emailNotificationView.getProperty("IsSend"))
            {
                string username = emailNotificationView.getProperty("username") as string;
                string password = emailNotificationView.getProperty("password") as string;
                string from = emailNotificationView.getProperty("from") as string;
                string to = emailNotificationView.getProperty("to") as string;
                string smtpClient = emailNotificationView.getProperty("SmtpClient") as string;
                int smtpPort = (int)emailNotificationView.getProperty("SmtpPort");

                System.Net.NetworkCredential loginInfo = new System.Net.NetworkCredential(username, password);
                System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();
                mail.From = new System.Net.Mail.MailAddress(from);
                if (to.Contains(","))
                {
                    // send to more than one email addresses
                    string[] emailAddreses = to.Split(',');
                    foreach (string emailAddress in emailAddreses)
                    {
                        mail.To.Add(new System.Net.Mail.MailAddress(emailAddress));
                    }
                }
                else
                {
                    mail.To.Add(new System.Net.Mail.MailAddress(to));
                }

                mail.Subject = notification.TypeOfNotification.ToString();
                mail.Body = notification.ToString();

                System.Net.Mail.SmtpClient client = new System.Net.Mail.SmtpClient(smtpClient, smtpPort);
                client.Credentials = loginInfo;
                client.Send(mail);
            }
        }

        /// <summary>
        /// Return a value indicating whether the dei service is enabled.
        /// </summary>
        /// <returns>Return true if it is enabled.</returns>
        public bool IsDeiEnabled()
        {
            DeiService deiService = this.GetService("Dei") as DeiService;
            //// TODO: IsEnabled shouldn't be used to indicate the running service
            //// Note: reimplement this method so it will check the Dei Server on the running services
            //// First check whether DeiService is available in CC (it shouldn't be null)
            if (deiService != null)
            {
                //// Check the running services
                if (this.RunningServices.Count > 0)
                {
                    foreach (KeyValuePair<string, RemoteServiceProcess> pair in this.RunningServices)
                    {
                        if (pair.Value.Process.Name.Equals("Dei"))
                        {
                            return true;
                        }
                    }
                }
                else
                {
                    //// no running services.
                    return false;
                }
            }

            return false;
        }

        /// <summary>
        /// Get the hostname of the dei server.
        /// </summary>
        /// <param name="hostname">Germ host name.</param>
        /// <param name="port">Port where the dei server is listen on.</param>
        /// <param name="useSslConnection">The connection used by Dei server.</param>
        /// <returns>Return true if the hostname and port number can be retreived succssfully.</returns>
        public bool GetDeiHost(out string hostname, out int port, out bool useSslConnection)
        {
            DeiService deiService = this.GetService("Dei") as DeiService;
            if (deiService != null && deiService.IsEnabled)
            {
                hostname = deiService.GetHostname();
                port = deiService.Port;

                //// Note: this is only work if the user does not change the dei server option
                //// after the dei service started. Also, assuming one CC can only have a max
                //// one dei service. 
                //// TODO: should show a warning page if the dei server option is changed, 
                //// while the service is already running.
                if (!deiService.ListenWithSsl)
                {
                    useSslConnection = false;
                }
                else
                {
                    useSslConnection = true;
                }

                if (string.IsNullOrEmpty(hostname))
                {
                    return false;
                }

                return true;
            }

            hostname = null;
            port = 0;
            useSslConnection = true; // It doesn't matter in this case
            return false;
        }

        /// <summary>
        /// Get the hostname of the dei server.
        /// </summary>
        /// <param name="hostname">Germ host name.</param>
        /// <param name="port">Port where the dei server is listen on.</param>
        /// <returns>Return true if the hostname and port number can be retreived succssfully.</returns>
        public bool GetDeiHost(out string hostname, out int port)
        {
            DeiService deiService = this.GetService("Dei") as DeiService;
            if (deiService != null && deiService.IsEnabled)
            {
                hostname = deiService.GetHostname();
                port = deiService.Port;

                if (string.IsNullOrEmpty(hostname))
                {
                    return false;
                }

                return true;
            }

            hostname = null;
            port = 0;
            return false;
        }

        /// <summary>
        /// Set the default value of a component or service given the name of the component/service.
        /// </summary>
        /// <param name="name">Component name.</param>
        public void SetToDefault(string name)
        {
            ComponentView component = this.GetComponent(name);
            if (component != null)
            {
                component.Instance.DefaultValue();
                component.UpdateProperty();
            }
        }

        /// <summary>
        /// Gets a particular component from Component Container.
        /// </summary>
        /// <param name="name">Component name.</param>
        /// <returns>If the component is exist, return the component view of this component.</returns>
        public ComponentView GetComponent(string name)
        {
            if (this.componentContainer != null)
            {
                ComponentView component = this.componentContainer[name];

                //// Set the possible values for germs in services. 
                //// TODO: shouldn't be set here, is no longer use in the current version
                //if (component != null && component.Instance is BaseService)
                //{
                //    component.SetPossibleValues("CompatibleHosts", this.mGerms.Hosts);
                //}

                return component;
            }

            return null;
        }

        /// <summary>
        /// Get the service from the component container.
        /// </summary>
        /// <param name="name">Service name.</param>
        /// <returns>If the service is exist, return the service as BaseService.</returns>
        public BaseService GetService(string name)
        {
            ComponentView component = this.GetComponent(name);
            if (component != null)
            {
                BaseService service = component.Instance as BaseService;
                if (service != null)
                {
                    return service;
                }
            }

            return null;
        }

        /// <summary>
        /// Get the email notification config.
        /// </summary>
        /// <returns>Return the email notification config.</returns>
        public EmailNotificationView GetEmailNotificationConfig()
        {
            return this.emailNotificationContainer.getEmailNotificationConfig();
        }

        /// <summary>
        /// Get all the notifications from notification container.
        /// </summary>
        /// <returns>Return the list of notifications.</returns>
        public List<Notification.Notification> GetNotifications()
        {
            return this.notificationContainer.getNotifications();
        }

        /// <summary>
        /// Save the configuration.
        /// </summary>
        public void Save()
        {
            if (this.provider != null)
            {
                this.provider.Save(this);
            }

            // Save the project into the default project name
            if (this.prestoManifestoProject != null)
            {
                string appPath = this.BaseDirectory;
                string saveDir = string.Empty;

                if (!this.IsMono)
                {
                    saveDir = @"Packages\Project.xml";
                }
                else
                {
                    saveDir = @"Packages/Project.xml";
                }

                string fullPath = Path.Combine(appPath, saveDir);
                this.prestoManifestoProject.Save(fullPath);
            }
        }

        /// <summary>
        /// Add new germ.
        /// </summary>
        /// <param name="hostname">Germ hostname.</param>
        /// <returns>Return a new germ client.</returns>
        public GermClient AddGerm(string hostname)
        {
            GermClient client = this.germs.AddGerm(hostname);
            this.Save();
            GerminatorFacade.Instance.CollectEscapedProcesses(this, client);
            return client;
        }

        /// <summary>
        /// Remove germ from the germs client container, and remove it from the configuration.
        /// </summary>
        /// <param name="hostname">Germ host name.</param>
        public void RemoveGerm(string hostname)
        {
            foreach (ComponentView component in this.Components)
            {
                if (component != null)
                {
                    BaseService service = component.Instance as BaseService;
                    if (service != null)
                    {
                        service.Stop(hostname);
                    }
                }
            }

            GermClient germ = this.germs.GetGerm(hostname);

            if (germ != null)
            {
                germ.Disconnect();
            }

            this.germs.RemoveGerm(hostname);
            this.Save();
        }

        public bool ContainGerm(GermClient client)
        {
            return this.germs.Contains(client.Name);
        }

        public void EnableComponent(ComponentView component, bool value)
        {
            if (component.Instance.IsEnabled == value)
            {
                return;
            }

            BaseService asService = component.Instance as BaseService;
            if (asService != null)
            {
                // Note: enabled a component is not allowed, start and stop process only from the network controller.
                //if (value)
                //{
                //    this.StartService(asService);
                //}
                //else
                //{
                //    this.StopService(asService);
                //}
            }
            else
            {
                component.Instance.IsEnabled = value;
            }

            this.Save();
        }

        public bool StartService(BaseService service, string customName, string passphrase)
        {
            if (service == null)
            {
                return false;
            }

            // Check if the service has already been started.
            foreach (RemoteServiceProcess p in service.Instances)
            {
                if (p.Germ.Name.Equals(service.CompatibleHosts[0]))
                {
                    DebugConsole.Instance.EnqueueTraceInfo(string.Format("{0} is already started.", service.DisplayName), TraceLevel.Warning);
                    return false;
                }
            }

            PeerService peerService = service as PeerService;
            
            try
            {
                if (peerService != null)
                {
                    ComponentView badumnaComponentView = this.componentContainer[BadumnaComponent.ComponentIndex];
                    ComponentView applicationComponentView = this.componentContainer[ApplicationComponent.ComponentIndex];
                    if (badumnaComponentView != null && applicationComponentView != null)
                    {
                        BadumnaComponent configurationComponent = badumnaComponentView.Instance as BadumnaComponent;
                        ApplicationComponent applicationComponent = applicationComponentView.Instance as ApplicationComponent;
                        peerService.StartPeerService(this.GermClients, configurationComponent, applicationComponent);
                        peerService.SubscribeNotificationEvent(new NotificationHandler[] 
                    {
                        this.OnActiveButNotLoggedIn,
                        this.OnHostOffline,
                        this.OnErrorStatus,
                        this.OnExitWithErrorStatus
                    });
                    }
                }
                else
                {
                    // Note: Dei server should be run from the base service, not from the peer service.
                    // Check whether the passphrase is not empty or null and if the service is dei.
                    // Special case when starting a dei service.0
                    DeiService deiService = service as DeiService;
                    if (deiService != null)
                    {
                        if (!string.IsNullOrEmpty(passphrase))
                        {
                            deiService.UseCustomCertificate = true;
                            deiService.Passphrase = passphrase;
                        }
                        else
                        {
                            deiService.UseCustomCertificate = false;
                        }

                        deiService.Start(this.GermClients, false);
                    }
                    else
                    {
                        service.Start(this.GermClients, false);
                    }

                    // TODO: wo we need to setup the event handlers as well
                }
            }
            catch (NullReferenceException e)
            {
                DebugConsole.Instance.EnqueueTraceInfo(e.Message, TraceLevel.Error);
                return false;
            }

            //// Try to register the new processes to the list of all runing processes
            //// TODO: Use delegate callback function instead waiting in the loop, but it will require to modify all
            //// the codes from base service and configuration classes.

            //// This method can cause an infinite loop.... 
            //// Set the maximum wait time to be 2 minutes, it should be an enough time to start a process, otherwise
            //// There should be an error occurs

            IEnumerable<RemoteServiceProcess> processes = null;
            RemoteServiceProcess process = null;
            DateTime startTime = DateTime.Now;
            bool found = false;

            while (!found && (DateTime.Now - startTime).TotalMinutes < 2)
            {
                processes = service.Instances;
                foreach (RemoteServiceProcess proc in processes)
                {
                    // Note: there will be only one compatible host every time start a new instance.
                    if (proc.Germ.Name.Equals(service.CompatibleHosts[0]))
                    {
                        process = proc;
                        found = true;
                        break;
                    }
                }

                if (!found)
                {
                    DebugConsole.Instance.EnqueueTraceInfo(string.Format("Waiting to get RemoteServiceProcess of {0} at {1}", service.DisplayName, DateTime.Now.ToLongTimeString()));
                    System.Threading.Thread.Sleep(5000);
                }
            }

            while (process != null && process.Process == null && (DateTime.Now - startTime).TotalMinutes < 3)
            {
                // Waiting until the service is successfully started,
                // Beware that this can cause an infinite loop, set the time out to be 3 minutes from the start time.
                DebugConsole.Instance.EnqueueTraceInfo(string.Format("Waiting {0} at {1}", service.DisplayName, DateTime.Now.ToLongTimeString()));
                System.Threading.Thread.Sleep(5000);
            }

            if (process != null && process.Process != null)
            {
                DebugConsole.Instance.EnqueueTraceInfo(string.Format("Successfully start {0}", service.DisplayName));

                process.TimeOfLastPing = System.DateTime.Now;
                process.SetPingStatus(PingStatus.ServiceStarted, string.Empty);

                string uniqueName = customName;

                // Check whether the customName has been used, (make sure the custom name is unique).
                if (this.RunningServices.ContainsKey(uniqueName))
                {
                    int uniqueIdentifier = 1;
                    uniqueName = string.Format("{0}{1}", customName, uniqueIdentifier);

                    while (this.RunningServices.ContainsKey(uniqueName))
                    {
                        uniqueIdentifier++;
                        uniqueName = string.Format("{0}{1}", customName, uniqueIdentifier);
                    }
                }

                this.RunningServices.Add(uniqueName, process);
            }
            else
            {
                DebugConsole.Instance.EnqueueTraceInfo(string.Format("Unsucessfully start {0} : Time out", service.DisplayName));
                return false;
            }

            this.Save();

            return true;
        }

        public bool RestartService(BaseService service, string germName)
        {
            if (service == null)
            {
                return false;
            }

            PeerService peerService = service as PeerService;
            if (peerService != null)
            {
                ComponentView component = this.componentContainer[BadumnaComponent.ComponentIndex];
                if (component != null)
                {
                    peerService.RestartPeerService(germName);
                }
            }
            else
            {
                service.Restart(germName);
            }

            this.Save();

            return true;
        }

        public bool StopService(BaseService service)
        {
            if (service == null)
            {
                return false;
            }

            service.Stop();
            this.Save();
            return true;
        }

        public bool StopService(BaseService service, string germName)
        {
            if (service == null)
            {
                return false;
            }

            service.Stop(germName);
            this.Save();
            return true;
        }


        #region PrestoManifesto Members

        public void AddNewService(string applicationName, string fullPath, ServiceType type)
        {
            PackageData packageData = new PackageData(applicationName, fullPath, type);
            PackageControl packageControl = new PackageControl(packageData, this.prestoManifestoProject, this.BaseDirectory + "Services");
            this.prestoManifestoProject.Applications.Add(packageData);
            this.prestoManifestoProject.PackageControls.Add(applicationName, packageControl);
            this.Save();
        }

        public void RemoveService(string applicationName)
        {
            PackageControl packageControl;
            if (this.prestoManifestoProject.PackageControls.TryGetValue(applicationName, out packageControl))
            {
                this.prestoManifestoProject.Applications.Remove(packageControl.Package);
                this.prestoManifestoProject.PackageControls.Remove(applicationName);
                packageControl.RemoveApplication();
                this.Save();
            }
        }

        public bool AddFile(string applicationName, string fileFullPath, DateTime modificationTime)
        {
            bool sucess = false;
            PackageControl packageControl;

            if (this.prestoManifestoProject.PackageControls.TryGetValue(applicationName, out packageControl))
            {
                sucess = packageControl.AddFile(fileFullPath, modificationTime);
            }

            this.Save();
            return sucess;
        }

        public void RemoveFile(string applicationName, string fileFullPath)
        {
            PackageControl packageControl;

            if (this.prestoManifestoProject.PackageControls.TryGetValue(applicationName, out packageControl))
            {
                packageControl.RemoveFile(fileFullPath);
            }

            this.Save();
        }

        public Dictionary<string, FileData> GetContents(string applicationName)
        {
            PackageControl packageControl;
            if (this.prestoManifestoProject != null && this.prestoManifestoProject.PackageControls.TryGetValue(applicationName, out packageControl))
            {
                return packageControl.GetContents();
            }
            else
            {
                return null;
            }
        }

        public string GetPackageVersion(string applicationName)
        {
            PackageControl packageControl;
            if (this.prestoManifestoProject != null && this.prestoManifestoProject.PackageControls.TryGetValue(applicationName, out packageControl))
            {
                return packageControl.GetPackageVersion();
            }
            else
            {
                return string.Empty;
            }
        }

        public bool IncludeExtractionTools(string applicationName)
        {
            PackageControl packageControl;
            if (this.prestoManifestoProject != null && this.prestoManifestoProject.PackageControls.TryGetValue(applicationName, out packageControl))
            {
                return packageControl.IncludeExtractionTools;
            }
            else
            {
                // By default, it returns false;
                return false;
            }
        }

        public void SetIncludeExtractionTools(string applicationName, bool isSet)
        {
            PackageControl packageControl;
            if (this.prestoManifestoProject != null && this.prestoManifestoProject.PackageControls.TryGetValue(applicationName, out packageControl))
            {
                packageControl.IncludeExtractionTools = isSet;
            }
        }

        public string GetUpdateType(string applicationName)
        {
            PackageControl packageControl;
            if (this.prestoManifestoProject != null && this.prestoManifestoProject.PackageControls.TryGetValue(applicationName, out packageControl))
            {
                return packageControl.UpdateType;
            }
            else
            {
                return string.Empty;
            }
        }

        public void SetUpdateType(string applicationName, string updateType)
        {
            PackageControl packageControl;
            if (this.prestoManifestoProject != null && this.prestoManifestoProject.PackageControls.TryGetValue(applicationName, out packageControl))
            {
                packageControl.UpdateType = updateType;
            }

            this.Save();
        }

        public string GetExecutable(string applicationName)
        {
            PackageControl packageControl;
            if (this.prestoManifestoProject != null && this.prestoManifestoProject.PackageControls.TryGetValue(applicationName, out packageControl))
            {
                return packageControl.GetExecutable();
            }
            else
            {
                return string.Empty;
            }
        }

        public bool SetExecutable(string applicationName, string fullPath)
        {
            PackageControl packageControl;
            if (this.prestoManifestoProject != null && this.prestoManifestoProject.PackageControls.TryGetValue(applicationName, out packageControl))
            {
                packageControl.SetExecutable(fullPath);
                return true;
            }
            else
            {
                return false;
            }
        }

        public string GetBaseDirectory(string applicationName)
        {
            PackageControl packageControl;
            if (this.prestoManifestoProject != null && this.prestoManifestoProject.PackageControls.TryGetValue(applicationName, out packageControl))
            {
                return packageControl.GetBaseDirectory();
            }
            else
            {
                return string.Empty;
            }
        }

        public string BuildPackage(string applicationName, string description)
        {
            PackageControl packageControl;
            if (this.prestoManifestoProject != null && this.prestoManifestoProject.PackageControls.TryGetValue(applicationName, out packageControl))
            {
                packageControl.BuildPackage(description);
            }

            return applicationName;
        }

        public float GetBuildProgress(string applicationName)
        {
            float buildProgress = 0f;
            PackageControl packageControl;
            if (this.prestoManifestoProject != null && this.prestoManifestoProject.PackageControls.TryGetValue(applicationName, out packageControl))
            {
                //// TODO: 600 is a magic number ( length of the progress bar), we should have some property to set this up.
                //// Probably add this property inside the package Control class or do it on the upper level.
                buildProgress = packageControl.CurrProgressStages / (float)packageControl.MaxProgressStages * 600f;
            }

            return buildProgress;
        }

        public string GetBuildDescription(string applicationName)
        {
            PackageControl packageControl;
            if (this.prestoManifestoProject != null && this.prestoManifestoProject.PackageControls.TryGetValue(applicationName, out packageControl))
            {
                return packageControl.Description;
            }

            return string.Empty;
        }

        public int GetMaxBuildProgress(string applicationName)
        {
            PackageControl packageControl;
            if (this.prestoManifestoProject != null && this.prestoManifestoProject.PackageControls.TryGetValue(applicationName, out packageControl))
            {
                return packageControl.MaxProgressStages;
            }

            return 0;
        }

        public void AddServiceToControlCenter(string applicationName)
        {
            string appPath = this.BaseDirectory;
            string saveDir = string.Empty;
            if (!this.IsMono)
            {
                saveDir = @"Services\" + applicationName + @"\";
            }
            else
            {
                saveDir = @"Services/" + applicationName + @"/";
            }

            string appFullPath = Path.Combine(appPath, saveDir);

            PackageControl packageControl;
            ServiceType packageType = ServiceType.NonBadumnaService; //// By default 
            bool successCopyFiles = false;
            BaseService service;

            if (this.prestoManifestoProject != null && this.prestoManifestoProject.PackageControls.TryGetValue(applicationName, out packageControl))
            {
                //// copy Manifest.xml and package to the Services directory
                successCopyFiles = packageControl.CopyBuiltPackage(appFullPath);
                packageType = packageControl.GetPackageType();
            }

            if (successCopyFiles)
            {
                //// Add the package into package list
                PackageManager.Instance.AddPackage(appFullPath, Path.Combine(appFullPath, Manifest.DefaultFileName));

                if (packageType.Equals(ServiceType.ArbitrationService))
                {

                    service = new ArbitrationService(applicationName, applicationName);
                }
                else
                {
                    service = new BaseService(applicationName, applicationName);
                }

                //// Add the new service into component list
                ComponentContainer.Instance.AddComponent(service);

                //// update the component list in the current configuration
                GerminatorFacade.Instance.PostLoad();
            }
        }

        #endregion

        internal void Save(XmlWriter writer)
        {
            lock (Configuration.loadLock)
            {
                XmlSerializerNamespaces namespaces = new XmlSerializerNamespaces(
                    new XmlQualifiedName[] { new XmlQualifiedName(string.Empty, string.Empty) });

                try
                {
                    writer.WriteStartElement("Configuration");
                    writer.WriteAttributeString("Version", "1.0");
                    writer.WriteAttributeString("Name", this.Name);

                    if (this.germs != null)
                    {
                        this.germs.Save(writer);
                    }

                    if (this.componentContainer != null)
                    {
                        XmlSerializer componentSerializers = new XmlSerializer(typeof(ComponentContainer), new XmlRootAttribute("Components"));
                        componentSerializers.Serialize(writer, this.componentContainer, namespaces);
                    }

                    if (this.emailNotificationContainer != null)
                    {
                        this.emailNotificationContainer.Save(writer);
                    }

                    if (this.notificationContainer != null)
                    {
                        this.notificationContainer.Save(writer);
                    }

                    writer.WriteEndElement();

                    this.SaveRunningServices(writer);
                }
                catch (Exception e)
                {
                    DebugConsole.Instance.EnqueueTraceInfo(string.Format("Failed to save configuration {1} : {0}", e.Message, this.Name), System.Diagnostics.TraceLevel.Error);
                }
            }
        }

        private void SaveRunningServices(XmlWriter writer)
        {
            //// Save the list of running processes
            object saveLock = new object();
            lock (saveLock)
            {
                try
                {
                    writer.WriteStartElement("RunningServices");
                    writer.WriteAttributeString("Version", "1.0");
                    foreach (KeyValuePair<string, RemoteServiceProcess> pair in this.runningServices)
                    {
                        writer.WriteStartElement("Service");
                        writer.WriteAttributeString("CustomName", pair.Key);
                        writer.WriteAttributeString("ServiceName", pair.Value.Process.Name);
                        writer.WriteAttributeString("GermName", pair.Value.Germ.Name);
                        writer.WriteEndElement();
                    }
                }
                catch (Exception e)
                {
                    DebugConsole.Instance.EnqueueTraceInfo(string.Format("Failed to save configuration {1} : {0}", e.Message, this.Name), System.Diagnostics.TraceLevel.Error);
                }
            }
        }

        internal void Load(XmlReader reader, IProvider<Configuration> provider)
        {
            this.provider = provider;
            lock (Configuration.loadLock)
            {
                // Note: shouldn't catch the exception in this level, let the XmlConfigurationProvider catch any exception.

                if (reader.Read())
                {
                    while (reader.LocalName != "Configuration")
                    {
                        reader.Read();
                    }

                    // TODO : Check version
                    if (reader.LocalName == "Configuration")
                    {
                        string name = reader.GetAttribute("Name");
                        if (string.IsNullOrEmpty(name))
                        {
                            return;
                        }

                        this.Name = name;

                        while (reader.IsStartElement() && reader.LocalName == "Configuration")
                        {
                            reader.Read();
                        }

                        this.germs = new GermClientContainer();
                        this.germs.Load(reader);
                        reader.MoveToContent();
                        
                        XmlSerializer componentSerializer = new XmlSerializer(typeof(ComponentContainer), string.Empty);
                        if (componentSerializer.CanDeserialize(reader))
                        {
                            ComponentContainer components = (ComponentContainer)componentSerializer.Deserialize(reader);
                            if (components != null)
                            {
                                this.componentContainer = components;
                                this.LoadPackage();
                            }
                        }

                        if (this.componentContainer == null)
                        {
                            this.componentContainer = new ComponentContainer();
                        }

                        if (this.prestoManifestoProject == null)
                        {
                            this.prestoManifestoProject = new Project();
                        }

                        if (this.clientListener == null)
                        {
                            this.InitializeClientListener();
                        }

                        this.LoadProject();

                        this.emailNotificationContainer = new EmailNotificationContainer();
                        this.emailNotificationContainer.Load(reader);

                        // TODO: check whether we need to load the notification history from somewhere
                        this.notificationContainer = new NotificationContainer();
                        this.notificationContainer.Load(reader);

                        this.runningServices = new Dictionary<string, RemoteServiceProcess>();
                        this.LoadRunningServices(reader);
                    }
                }
            }
        }

        public void LoadPackage()
        {
            if (this.componentContainer != null)
            {
                IEnumerable<ComponentView> components = this.Components;
                foreach (ComponentView component in components)
                {
                    BaseService service = component.Instance as BaseService;
                    if (service != null)
                    {
                        string appPath = this.BaseDirectory;
                        string saveDir = string.Empty;

                        // Check whether is the server run in mono or windows environment
                        if (!this.IsMono)
                        {
                            saveDir = @"\Services\" + service.Index + @"\";
                        }
                        else
                        {
                            saveDir = @"/Services/" + service.Index + @"/";
                        }

                        PackageManager.Instance.AddPackage(appPath + saveDir, appPath + saveDir + Badumna.Package.Manifest.DefaultFileName);
                    }
                }
            }
        }

        private void LoadProject()
        {
            if (this.prestoManifestoProject != null)
            {
                string appPath = this.BaseDirectory;
                string saveDir = string.Empty;

                if (!this.IsMono)
                {
                    saveDir = @"Packages\Project.xml";
                }
                else
                {
                    saveDir = @"Packages/Project.xml";
                }

                string fullPath = Path.Combine(appPath, saveDir);

                if (File.Exists(fullPath))
                {
                    using (FileStream fileStream = new FileStream(fullPath, FileMode.Open))
                    {
                        DataContractSerializer deserializer = new DataContractSerializer(typeof(Project));

                        try
                        {
                            this.prestoManifestoProject = deserializer.ReadObject(fileStream) as Project;

                            if (this.prestoManifestoProject.PackageControls == null)
                            {
                                this.prestoManifestoProject.PackageControls = new Dictionary<string, PackageControl>();
                            }

                            foreach (PackageData application in this.prestoManifestoProject.Applications)
                            {
                                PackageControl packageControl = new PackageControl(application, this.prestoManifestoProject, appPath + "Services");
                                this.prestoManifestoProject.PackageControls.Add(application.Name, packageControl);
                            }
                        }
                        catch { }
                    }
                }
            }
        }

        private void LoadRunningServices(XmlReader reader)
        {
            object readLock = new object();
            lock (readLock)
            {
                try
                {
                    if (reader.IsStartElement() && reader.LocalName == "RunningServices" && reader.IsEmptyElement)
                    {
                        reader.Read();
                        return;
                    }

                    while (reader.IsStartElement() && reader.LocalName == "RunningServices")
                    {
                        reader.Read();
                    }

                    while (reader.LocalName == "Service")
                    {
                        string customName = reader.GetAttribute("CustomName");
                        string serviceName = reader.GetAttribute("ServiceName");
                        string germName = reader.GetAttribute("GermName");

                        RemoteServiceProcess process = new RemoteServiceProcess();
                        if (this.germs.Contains(germName))
                        {
                            process.Germ = this.germs.GetGerm(germName);
                        }

                        //// Create a dummy process on load and add the actual process later on
                        process.Process = new GermServer.ProcessDescription(0, serviceName, string.Empty);

                        if (this.runningServices == null)
                        {
                            this.runningServices = new Dictionary<string, RemoteServiceProcess>();
                        }

                        this.runningServices.Add(customName, process);

                        reader.Read();
                        reader.MoveToContent();
                    }

                    reader.ReadEndElement();
                }
                catch (Exception e)
                {
                    DebugConsole.Instance.EnqueueTraceInfo(string.Format("Failed to load list of running services : {0}", e.Message), System.Diagnostics.TraceLevel.Error);
                }
            }
        }


        public void UpdateRunningServices()
        {
            object updateLock = new object();
            lock (updateLock)
            {
                try
                {
                    //// Copy the list of running services to a temporary list
                    Dictionary<string, RemoteServiceProcess> tempList = new Dictionary<string, RemoteServiceProcess>();
                    foreach (KeyValuePair<string, RemoteServiceProcess> pair in this.runningServices)
                    {
                        tempList.Add(pair.Key, pair.Value);
                    }

                    this.runningServices.Clear();

                    foreach (ComponentView component in this.componentContainer.AsEnumerable())
                    {
                        BaseService service = component.Instance as BaseService;
                        if (service != null)
                        {
                            foreach (RemoteServiceProcess process in service.Instances)
                            {
                                //// TODO; find a better way to update the list, other than by using the linear search
                                //// (current way, store the loaded list to temporary list and update it, it will be less efficient)
                                //// Update and Remove the death service from the running services list
                                //// (Remove that still have the dummy process from LoadRunningServices())
                                foreach (KeyValuePair<string, RemoteServiceProcess> pair in tempList)
                                {
                                    if (pair.Value.Process.Name.Equals(process.Process.Name) && pair.Value.Germ.Name.Equals(process.Germ.Name))
                                    {
                                        process.TimeOfLastPing = DateTime.Now;
                                        service.BlockingPing(process.Germ.Name);
                                        this.runningServices.Add(pair.Key, process);
                                        tempList.Remove(pair.Key);
                                        break;
                                    }
                                }
                            }
                        }
                    }


                }
                catch (Exception e)
                {
                    DebugConsole.Instance.EnqueueTraceInfo(string.Format("Failed to update list of running services : {0}", e.Message), System.Diagnostics.TraceLevel.Error);
                }
            }
        }

        #region Germs helper members

        /// <summary>
        /// Get the specific germ client given the hostname.
        /// </summary>
        /// <remarks>
        /// This method is used to replace the GetGermClient method from GerminatorFacade.
        /// </remarks>
        /// <param name="hostname">Germ host name.</param>
        /// <returns>Return the germ client for a given hostname if exist.</returns>
        public GermClient GetGermClient(string hostname)
        {
            return this.germs.GetGerm(hostname);
        }

        /// <summary>
        /// Add germ delegate, it should only be called from the client listener instance.
        /// </summary>
        /// <param name="client">Germ client.</param>
        internal void AddGerm(GermClient client)
        {
            this.germs.AddGerm(client);
            this.Save();
            GerminatorFacade.Instance.CollectEscapedProcesses(this, client);
            DebugConsole.Instance.EnqueueTraceInfo(string.Format("New germ has been added to the control center : {0}, please refresh the page.", client.Name));
        }

        #endregion

        #region NotificationHandler members

        internal void OnActiveButNotLoggedIn(Notification.Notification notification)
        {
            // TODO: implement this event handler
            // try to find the reason why the peer cannot logged in
            this.notificationContainer.AddNotification(notification);
            this.Save();

            if (notification.Priority.Equals(PriorityLevel.High) && notification != null)
            {
                this.SendEmailNotification(notification);
            }
        }

        internal void OnHostOffline(Notification.Notification notification)
        {
            // TODO: implement this event handler
            // We need to check whether the hostoffline due to time out request
            // or something else
            GermClient germClient = this.germs.GetGerm(notification.GermName);
            CustomMessageOperation operation = germClient.QueueCustomRequestOperation(notification.ProcessId, (int)CustomRequestType.Ping, new byte[] { }, null, null);
            if (!operation.AsyncWaitHandle.WaitOne(GermClient.DefaultOperationTimeout))
            {
                notification.ErrorMessage += " <br/> Highly possible the hostoffline status due to time out request.";
            }
            else
            {
                if (!operation.WasSuccessful && operation.Response == null)
                {
                    notification.ErrorMessage += "<br/> The host is off line, check the germ connection manually to solve this problem";
                }
                else
                {
                    notification.ErrorMessage += " <br/> Recheck the status again"; // TODO: revise this implementation
                }
            }

            this.notificationContainer.AddNotification(notification);
            this.Save();

            if (notification.Priority.Equals(PriorityLevel.High) && notification != null)
            {
                this.SendEmailNotification(notification);
            }
        }

        internal void OnErrorStatus(Notification.Notification notification)
        {
            // TODO: implement this event handler
            // Try to figure out why the error is occuring
            GermClient germClient = this.germs.GetGerm(notification.GermName);
            CustomMessageOperation operation = germClient.QueueCustomRequestOperation(notification.ProcessId, (int)CustomRequestType.Ping, new byte[] { }, null, null);
            if (!operation.AsyncWaitHandle.WaitOne(GermClient.DefaultOperationTimeout))
            {
                notification.ErrorMessage += "<br/> Operation time out when try to get the ping status from the " + notification.GermName;
            }
            else
            {
                if (operation.WasSuccessful && operation.Response != null)
                {
                    try
                    {
                        XmlDocument document = new XmlDocument();
                        document.Load(operation.Response);
                        XmlNode exceptionNode = document.ChildNodes[1].SelectSingleNode("Exception");
                        if (exceptionNode != null)
                        {
                            notification.ErrorMessage += "<br/> Exception occurs:";
                            notification.ErrorMessage += "<br/>" + exceptionNode.Value;
                        }
                    }
                    catch (Exception) { }
                }
            }

            this.notificationContainer.AddNotification(notification);
            this.Save();

            if (notification.Priority.Equals(PriorityLevel.High) && notification != null)
            {
                this.SendEmailNotification(notification);
            }
        }

        internal void OnExitWithErrorStatus(Notification.Notification notification)
        {
            this.notificationContainer.AddNotification(notification);
            this.Save();

            if (notification.Priority.Equals(PriorityLevel.High) && notification != null)
            {
                this.SendEmailNotification(notification);
            }
        }

        #endregion

        #region ClientListener members

        /// <summary>
        /// Initialize client listener.
        /// </summary>
        private void InitializeClientListener()
        {
            if (!this.componentContainer.Contains(ClientListenerComponent.ComponentIndex))
            {
                DebugConsole.Instance.EnqueueTraceInfo("Client listener component not found! Client listener will not be started.");
                return;
            }

            ComponentView compView = this.componentContainer[ClientListenerComponent.ComponentIndex];
            compView.PropertyChangedEvent += new OnPropertyChangeDelegate(ClientListenerComponentPropertyChangedHandler);
            ClientListenerComponent component = compView.Instance as ClientListenerComponent;

            this.clientListener = new ClientListener(GermClientContainer.Certificate);
            this.clientListener.AddGerm = this.AddGerm;
            this.clientListener.Port = component.PortNumber;

            if (component.UseClientListener)
            {
                this.clientListener.Start();
            }
        }

        /// <summary>
        /// Handled client listener component property changed.
        /// </summary>
        private void ClientListenerComponentPropertyChangedHandler(string propertyIndex)
        {
            ClientListenerComponent component = this.componentContainer[ClientListenerComponent.ComponentIndex].Instance as ClientListenerComponent;

            if (this.clientListener == null)
            {
                this.clientListener = new ClientListener(GermClientContainer.Certificate);
                this.clientListener.AddGerm = this.AddGerm;
            }

            switch (propertyIndex)
            {
                case "PortNumber":
                    this.clientListener.Port = component.PortNumber;
                    break;
                case "UseClientListener":
                    if (component.UseClientListener)
                    {
                        this.clientListener.Restart();
                    }
                    else
                    {
                        this.clientListener.Stop();
                    }
                    break;
            }
        }

        #endregion
    }
}
