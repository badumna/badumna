﻿//-----------------------------------------------------------------------
// <copyright file="DeiConfigManagerTests.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace GermHarnessTests
{
    using System;
    using GermHarness;
    using NUnit.Framework;
    using Rhino.Mocks;

    [TestFixture]
    internal class DeiConfigManagerTests
    {
        private IPeerHarness harness;
        private IFileSystem fileSystem;
        private DeiConfigManager manager;

        [SetUp]
        public void SetUp()
        {
            this.harness = MockRepository.GenerateMock<IPeerHarness>();
            this.fileSystem = MockRepository.GenerateMock<IFileSystem>();
            this.manager = new DeiConfigManager(this.fileSystem);
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void DeiConfigManagerThrowsForNullFileSystem()
        {
            this.manager = new DeiConfigManager(null);
        }

        [Test]
        public void DeiConfigManagerCreatesDefaultFileSystem()
        {
            // Not throwing indicates success.
            this.manager = new DeiConfigManager();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void DeiConfigManagerThrowsForNullPeerHarness()
        {
            this.manager.ConfigHarness(null, null, null);
        }

        [TestCase("", "")]
        [TestCase(null, null)]
        [TestCase("", null)]
        [TestCase(null, "")]
        public void ConfigHarnessDoesNotSetHarnessIdentityProviderWhenNoConfigSupplied(string configFile, string configString)
        {
            this.harness.Expect(
                h => h.SetIdentityProvider(null, 0, null, null, false))
                .IgnoreArguments()
                .Repeat.Never();
            this.manager.ConfigHarness(this.harness, configFile, configString);
            this.harness.VerifyAllExpectations();
        }

        [TestCase("foo;1;true;;baz", "Username is missing.")]
        [TestCase("foo;;true;bar;baz", "Port number is missing.")]
        [TestCase("foo;1;true;bar;", "Password is missing.")]
        [TestCase("foo;1;;bar;baz", "SSL flag is missing.")]
        [TestCase(";1;true;bar;baz", "Host address is missing.")]
        public void ConfigHarnessRejectsConfigStringsWithEmptyParts(string configString, string expectedMessage)
        {
            this.BadConfigStringRejected(configString, expectedMessage);
        }

        [TestCase("foo;-1;true;bar;baz")]
        [TestCase("foo;port;true;bar;baz")]
        [TestCase("foo;1234567;true;bar;baz")]
        public void ConfigHarnessRejectsConfigStringsWithInvalidPort(string configString)
        {
            string expectedMessage = string.Format(
                    "Port number must be an integer between {0} to {1}.",
                    ushort.MinValue,
                    ushort.MaxValue);
            this.BadConfigStringRejected(configString, expectedMessage);
        }

        [TestCase("foo;1;yes;bar;baz")]
        [TestCase("foo;1;0;bar;baz")]
        [TestCase("foo;1;1;bar;baz")]
        [TestCase("foo;1;no;bar;baz")]
        public void ConfigHarnessRejectsConfigStringsWithNonBoolSslFlag(string configString)
        {
            this.BadConfigStringRejected(configString, "SSL usage flag must be \"true\" or \"false\".");
        }

        [TestCase("foo;1;true;")]
        [TestCase("foo;1;true;bar")]
        [TestCase("foo")]
        [TestCase("foo;1")]
        public void ConfigHarnessRejectsConfigStringsWithWrongNumberOfParts(string configString)
        {
            this.BadConfigStringRejected(
                configString,
                "The Dei config string must be in the format \"address;port;use_ssl(true/false);username;password\"");
        }

        [Test]
        public void ConfigHarnessRejectsConfigFileItCannotFind()
        {
            string filename = Guid.NewGuid().ToString();
            this.fileSystem.Stub(f => f.FileExists(filename)).Return(false);
            var ex = Assert.Throws<DeiConfigException>(
                () => this.manager.ConfigHarness(this.harness, filename, null));
            Assert.AreEqual(
                "Cannot find Dei config file: " + filename,
                ex.Message);
        }

        [Test]
        public void ConfigHarnessRejectsConfigFileItCannotRead()
        {
            string filename = Guid.NewGuid().ToString();
            this.fileSystem.Stub(f => f.FileExists(filename)).Return(true);
            this.fileSystem.Stub(f => f.ReadAllLines(filename))
                .Throw(new System.IO.IOException("Fake."));
            var ex = Assert.Throws<DeiConfigException>(
                () => this.manager.ConfigHarness(this.harness, filename, null));
            Assert.AreEqual("Error reading Dei config file: Fake.", ex.Message);
        }

        [TestCase("deiHost:foo\tdeiPort:a\tuseSslConnection:true\tdeiUsername:bar\tdeiPassword:baz")]
        [TestCase("deiHost:foo\tdeiPort:65536\tuseSslConnection:true\tdeiUsername:bar\tdeiPassword:baz")]
        [TestCase("deiHost:foo\tdeiPort:-1\tuseSslConnection:true\tdeiUsername:bar\tdeiPassword:baz")]
        public void ConfigHarnessRejectsConfgFileContainingInvalidPort(string configFileContent)
        {
            string expectedMessage = string.Format(
                "Port number must be an integer between {0} to {1}.",
                ushort.MinValue,
                ushort.MaxValue);
            this.BadConfigFileContentRejected(configFileContent, expectedMessage);
        }

        [TestCase("deiHost:foo\tdeiPort: 1\tuseSslConnection:true\tdeiUsername:bar\tdeiPassword:baz")]
        [TestCase("deiHost:foo\tdeiPort:1\tuseSslConnection: true\tdeiUsername:bar\tdeiPassword:baz")]
        [TestCase("deiHost:foo\tdeiPort:1\tuseSslConnection:true\tdeiUsername: bar\tdeiPassword:baz")]
        [TestCase("deiHost: foo\tdeiPort:1\tuseSslConnection:true\tdeiUsername:bar\tdeiPassword:baz")]
        public void ConfigHarnessRejectsConfgFileContainingInvalidSpaces(string configFileContent)
        {
            this.BadConfigFileContentRejected(configFileContent, "Dei configuration should not include whitespace.");
        }

        [TestCase("deiHost:foo\tdeiPort:1\tuseSslConnection:yes\tdeiUsername:bar\tdeiPassword:baz")]
        [TestCase("deiHost:foo\tdeiPort:1\tuseSslConnection:no\tdeiUsername:bar\tdeiPassword:baz")]
        [TestCase("deiHost:foo\tdeiPort:1\tuseSslConnection:1\tdeiUsername:bar\tdeiPassword:baz")]
        [TestCase("deiHost:foo\tdeiPort:1\tuseSslConnection:0\tdeiUsername:bar\tdeiPassword:baz")]
        public void ConfigHarnessRejectsConfgFileContainingInvalidSslFlag(string configFileContent)
        {
            this.BadConfigFileContentRejected(configFileContent, "SSL usage flag must be \"true\" or \"false\".");
        }

        [TestCase("deiPort:1\tuseSslConnection:true\tdeiUsername:bar\tdeiPassword:baz")]
        [TestCase("deiHost:foo\tuseSslConnection:true\tdeiUsername:bar\tdeiPassword:baz")]
        [TestCase("deiHost:foo\tdeiPort:1\tdeiUsername:bar\tdeiPassword:baz")]
        [TestCase("deiHost:foo\tdeiPort:1\tuseSslConnection:true\tdeiPassword:baz")]
        [TestCase("deiHost:foo\tdeiPort:1\tuseSslConnection:true\tdeiUsername:bar")]
        [TestCase("")]
        public void ConfigHarnessRejectsConfigFileWithMissingLines(string configFileContent)
        {
            this.BadConfigFileContentRejected(configFileContent, "Dei config file must contain five lines.");
        }

        [TestCase("deiHost:foo\tdeiPort:1\tuseSslConnection:true\tdeiUsername:\tdeiPassword:baz", "Username is missing.")]
        [TestCase("deiHost:foo\tdeiPort:1\tuseSslConnection:\tdeiUsername:bar\tdeiPassword:baz", "SSL flag is missing.")]
        [TestCase("deiHost:\tdeiPort:1\tuseSslConnection:true\tdeiUsername:bar\tdeiPassword:baz", "Host address is missing.")]
        [TestCase("deiHost:foo\tdeiPort:1\tuseSslConnection:true\tdeiUsername:bar\tdeiPassword:", "Password is missing.")]
        [TestCase("deiHost:foo\tdeiPort:\tuseSslConnection:true\tdeiUsername:bar\tdeiPassword:baz", "Port number is missing.")]
        public void ConfigHarnessRejectsConfgFileWithMissingValues(string configFileContent, string expectedMessage)
        {
            this.BadConfigFileContentRejected(configFileContent, expectedMessage);
        }

        [TestCase("deiHost=foo\tdeiPort:1\tuseSslConnection:true\tdeiUsername:bar\tdeiPassword:baz", "deiHost=foo")]
        [TestCase("deiHost:foo\tdeiPort-1\tuseSslConnection:true\tdeiUsername:bar\tdeiPassword:baz", "deiPort-1")]
        [TestCase("deiHost:foo\tdeiPort:1\tuseSslConnection true\tdeiUsername:bar\tdeiPassword:baz", "useSslConnection true")]
        [TestCase("deiHost:foo\tdeiPort:1\tuseSslConnection:true\tdeiUsername/bar\tdeiPassword:baz", "deiUsername/bar")]
        [TestCase("deiHost:foo\tdeiPort:1\tuseSslConnection:true\tdeiUsername:bar\tdeiPassword~baz", "deiPassword~baz")]
        public void ConfigHarnessRejectsConfgFileWithBadlyFormattedLines(string configFileContent, string badLine)
        {
            this.BadConfigFileContentRejected(configFileContent, "File contains badly formatted line: \"" + badLine + "\"");
        }

        [TestCase("deiAddress:foo\tdeiPort:1\tuseSslConnection:true\tdeiUsername:bar\tdeiPassword:baz", "deiAddress")]
        [TestCase("deiHost:foo\tdeiPortNumber:1\tuseSslConnection:true\tdeiUsername:bar\tdeiPassword:baz", "deiPortNumber")]
        [TestCase("deiHost:foo\tdeiPort:1\tuseSsl:true\tdeiUsername:bar\tdeiPassword:baz", "useSsl")]
        [TestCase("deiHost:foo\tdeiPort:1\tuseSslConnection:true\tUsername:bar\tdeiPassword:baz", "Username")]
        [TestCase("deiHost:foo\tdeiPort:1\tuseSslConnection:true\tdeiUsername:bar\tPassword:baz", "Password")]
        public void ConfigHarnessRejectsConfgFileWithUnknownItem(string configFileContent, string unknownItem)
        {
            this.BadConfigFileContentRejected(configFileContent, "File contains invalid entry: \"" + unknownItem + "\"");
        }

        [TestCase("foo", "bar")]
        ////[ExpectedException("System.ArgumentException")]
        public void ConfigHarnessThrowsExceptionIfNeitherArgumentIsNullorEmpty(string configString, string configFileName)
        {
            Assert.Throws<DeiConfigException>(
                () => this.manager.ConfigHarness(this.harness, configFileName, configString));
        }

        [TestCase("deiHost:foo\tdeiPort:1\tuseSslConnection:true\tdeiUsername:bar\tdeiPassword:\"", "foo", 1, true, "bar", "\"")]
        [TestCase("deiHost:foo\tdeiPort:1\tuseSslConnection:true\tdeiUsername:bar\tdeiPassword: ", "foo", 1, true, "bar", " ")]
        [TestCase("deiHost:foo\tdeiPort:1\tuseSslConnection:true\tdeiUsername:bar\tdeiPassword:;", "foo", 1, true, "bar", ";")]
        [TestCase("deiHost:foo\tdeiPort:1\tuseSslConnection:true\tdeiUsername:bar\tdeiPassword::", "foo", 1, true, "bar", ":")]
        public void ConfigHarnessUsesCorrectDataFromConfigFile(string configFileContent, string host, int port, bool useSSL, string username, string password)
        {
            string configFileName = Guid.NewGuid().ToString("N");
            this.fileSystem.Stub(f => f.FileExists(configFileName)).Return(true);
            this.fileSystem.Stub(f => f.ReadAllLines(configFileName)).Return(configFileContent.Split('\t'));
            this.harness.Expect(
                h => h.SetIdentityProvider(host, (ushort)port, username, password, useSSL));
            this.manager.ConfigHarness(this.harness, configFileName, null);
            this.harness.VerifyAllExpectations();
        }

        [TestCase("foo;1;true;bar; ", "foo", 1, true, "bar", " ")]
        [TestCase("foo;1;true;bar; ; ; ; ;    ", "foo", 1, true, "bar", " ; ; ; ;    ")]
        [TestCase("foo;1;true;bar;;", "foo", 1, true, "bar", ";")]
        [TestCase("foo;1;true;bar;\'", "foo", 1, true, "bar", "\'")]
        [TestCase("foo;1;true;bar;baz", "foo", 1, true, "bar", "baz")]
        [TestCase("foo;1;true;bar;\"", "foo", 1, true, "bar", "\"")]
        [TestCase("foo;1;true;bar;\\", "foo", 1, true, "bar", "\\")]
        public void ConfigHarnessUsesConfigStringDataToSetIdentityProvider(string configString, string host, int port, bool useSSL, string username, string password)
        {
            this.harness.Expect(
                h => h.SetIdentityProvider(host, (ushort)port, username, password, useSSL));
            this.manager.ConfigHarness(this.harness, null, configString);
            this.harness.VerifyAllExpectations();
        }

        private void BadConfigFileContentRejected(string configFileContent, string expectedMessage)
        {
            string configFileName = Guid.NewGuid().ToString("N");
            this.fileSystem.Stub(f => f.FileExists(configFileName)).Return(true);
            this.fileSystem.Stub(f => f.ReadAllLines(configFileName)).Return(configFileContent.Split('\t'));
            this.harness.Expect(
                h => h.SetIdentityProvider(null, 0, null, null, false))
                .IgnoreArguments()
                .Repeat.Never();
            var ex = Assert.Throws<DeiConfigException>(() => this.manager.ConfigHarness(this.harness, configFileName, null));
            Assert.AreEqual(expectedMessage, ex.Message);
            this.harness.VerifyAllExpectations();
        }

        private void BadConfigStringRejected(string configString, string expectedMessage)
        {
            this.harness.Expect(
                h => h.SetIdentityProvider(null, 0, null, null, false))
                .IgnoreArguments()
                .Repeat.Never();
            var ex = Assert.Throws<DeiConfigException>(() => this.manager.ConfigHarness(this.harness, null, configString));
            Assert.AreEqual(expectedMessage, ex.Message);
            this.harness.VerifyAllExpectations();
        }
    }
}
