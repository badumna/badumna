﻿//-----------------------------------------------------------------------
// <copyright file="PeerHarnessTests.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace GermHarnessTests
{
    using System;
    using Badumna;
    using GermHarness;
    using NUnit.Framework;
    using Rhino.Mocks;
    using TestUtilities;

    [TestFixture]
    internal class PeerHarnessTests
    {
        private PeerHarness peerHarness;
        private NetworkFacadeFactory networkFacadeFactory;
        private Options options;
        private IDeiConfigManager deiConfigManager;
        private IHostedProcess hostedProcess;

        [SetUp]
        public void SetUp()
        {
            this.networkFacadeFactory = MockRepository.GenerateMock<NetworkFacadeFactory>();
            this.options = new Options();
            this.deiConfigManager = MockRepository.GenerateMock<IDeiConfigManager>();
            this.hostedProcess = MockRepository.GenerateMock<IHostedProcess>();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ConstructorThrowsForNullNetworkFacadeFactory()
        {
            this.networkFacadeFactory = null;
            this.ConstructHarness();
        }

        [Test]
        [Category("Integration")]
        public void PeerHarnessUsesApplicationNameFromConfigFile()
        {
            // Arrange
            this.ConstructHarness();
            string[] arguments = new string[] { "--badumna-config-file=NetworkConfig.xml" };
            this.peerHarness.Initialize(ref arguments, this.hostedProcess);
            var cc = new CapturingConstraint<Options>();
            this.networkFacadeFactory.Stub(f => f.Invoke(Arg<Options>.Matches(cc)));
            this.hostedProcess.Stub(p => p.OnPerformRegularTasks(Arg<int>.Is.Anything))
                .Return(false);

            // Act
            this.peerHarness.Start();

            // Assert
            Assert.AreEqual("test-application-name", cc.Argument.Connectivity.ApplicationName);
        }

        [Test]
        [Category("Integration")]
        public void PeerHarnessOverridesApplicationNameFromConfigFileWithCommandLineOption()
        {
            // Arrange
            this.ConstructHarness();
            string[] arguments = new string[] { "--badumna-config-file=NetworkConfig.xml", "--application-name=foo" };
            this.peerHarness.Initialize(ref arguments, this.hostedProcess);
            var cc = new CapturingConstraint<Options>();
            this.networkFacadeFactory.Stub(f => f.Invoke(Arg<Options>.Matches(cc)));
            this.hostedProcess.Stub(p => p.OnPerformRegularTasks(Arg<int>.Is.Anything))
                .Return(false);

            // Act
            this.peerHarness.Start();

            // Assert
            Assert.AreEqual("foo", cc.Argument.Connectivity.ApplicationName);
        }

        private void ConstructHarness()
        {
            this.peerHarness = new PeerHarness(
                this.networkFacadeFactory,
                this.options,
                this.deiConfigManager);
        }
    }
}
