﻿//-----------------------------------------------------------------------
// <copyright file="HarnessBaseTests.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace GermHarnessTests
{
    using GermHarness;
    using NUnit.Framework;
    using Rhino.Mocks;

    [TestFixture]
    internal class HarnessBaseTests
    {
        private Harness harness;
        private IHostedProcess process;

        [SetUp]
        public void SetUp()
        {
            this.harness = new Harness();
            this.process = MockRepository.GenerateMock<IHostedProcess>();
        }

        [TearDown]
        public void TearDown()
        {
            this.harness = null;
            this.process = null;
        }

        [TestCase("-foo -bar -harness-port=345", "-foo -bar")]
        [TestCase("-control-id=345 -foo -bar", "-foo -bar")]
        [TestCase("-foo -control-id=345 -bar", "-foo -bar")]
        [TestCase("-foo -bar -control-id=345", "-foo -bar")]
        [TestCase("-foo", "-foo")]
        [TestCase("--foo", "--foo")]
        [TestCase("/foo", "/foo")]
        [TestCase("-harness-port=99", "")]
        [TestCase("-harness-port 99", "")]
        [TestCase("-control-id=99", "")]
        [TestCase("-control-id=-99", "")]
        [TestCase("-control-id 99", "")]
        [TestCase("-control-id -99", "")]
        [TestCase("-foo -bar", "-foo -bar")]
        [TestCase("-harness-port=345 -foo -bar", "-foo -bar")]
        [TestCase("-foo -harness-port=345 -bar", "-foo -bar")]
        [TestCase("", "")]
        public void InitializeInitializesProcessAfterStrippingHarnessArgs(string argsString, string expectedProcessArgsString)
        {
            string[] args = argsString.Split(
                new char[1] { ' ' },
                System.StringSplitOptions.RemoveEmptyEntries);
            string[] expectedProcessArgs = expectedProcessArgsString.Split(
                new char[1] { ' ' },
                System.StringSplitOptions.RemoveEmptyEntries);
            this.process.Expect(
                p => p.OnInitialize(ref expectedProcessArgs));
            this.harness.Initialize(ref args, this.process);
            this.process.VerifyAllExpectations();
        }

        [TestCase("-control-id=foo")]
        [TestCase("-harness-port foo")]
        [TestCase("-harness-port 1.23")]
        [TestCase("-harness-port=1.23")]
        [TestCase("-harness-port=foo")]
        [TestCase("-harness-port=-99")]
        [TestCase("-control-id 1.23")]
        [TestCase("-control-id=1.23")]
        [TestCase("-control-id foo")]
        [TestCase("-harness-port -99")]
        [ExpectedException("Mono.Options.OptionException")]
        public void InitializeThrowsExceptionForBadHarnessOptionValues(string argsString)
        {
            string[] args = argsString.Split(
                new char[1] { ' ' },
                System.StringSplitOptions.RemoveEmptyEntries);
            this.harness.Initialize(ref args);
        }

        internal class Harness : HarnessBase
        {
            protected override bool OnStart(string config)
            {
                throw new System.NotImplementedException();
            }

            protected override void OnStop()
            {
                throw new System.NotImplementedException();
            }

            protected override void OnProcessRegularEvents()
            {
                throw new System.NotImplementedException();
            }

            protected override bool OnWriteStatus(System.IO.Stream stream)
            {
                throw new System.NotImplementedException();
            }

            protected override PingState OnGetPing()
            {
                throw new System.NotImplementedException();
            }

            protected override void OnCheckForUpdates()
            {
                throw new System.NotImplementedException();
            }
        }
    }
}
