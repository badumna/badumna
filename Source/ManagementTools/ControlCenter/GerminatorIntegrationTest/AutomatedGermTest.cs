﻿//-----------------------------------------------------------------------
// <copyright file="AutomatedGermTest.cs" company="National ICT Australia Ltd">
//     Copyright (c) 2012 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace GerminatorIntegrationTest
{
    using System.Security.Cryptography.X509Certificates;
    using System.Threading;
    using NUnit.Framework;

    [TestFixture]
    public class AutomatedGermTest
    {
        private AutoResetEvent testTrigger;

        private X509Certificate2 certificate;

        private ClientListenerRunner clRunner;

        private GermServiceRunner gsRunner;

        [SetUp]
        public void SetUp()
        {
            this.certificate = new X509Certificate2("certificate.pfx", "password");
            this.testTrigger = new AutoResetEvent(false);

            Assert.IsNotNull(this.certificate);
        }

        [TearDown]
        public void TearDown()
        {
            if (this.clRunner != null)
            {
                this.clRunner.Stop();
                this.clRunner = null;
            }

            if (this.gsRunner != null)
            {
                this.gsRunner.Stop();
                this.gsRunner = null;
            }
        }

        [Test]
        public void GermAndClientListenerConnectivityTest()
        {           
            int clientListenerPort = 21200;
            int germPort = 21253;

            this.clRunner = new ClientListenerRunner(this.testTrigger, this.certificate, clientListenerPort);
            this.gsRunner = new GermServiceRunner(germPort, string.Format("127.0.0.1:{0}", clientListenerPort));

            this.clRunner.Run();
            this.gsRunner.Run();

            this.testTrigger.WaitOne();

            Assert.IsTrue(clRunner.TestSucceed);
        }

        [Test]
        public void GermShouldStillConnectAfterRestartingClientListener()
        {
            int clientListenerPort = 21200;
            int germPort = 21253;

            this.clRunner = new ClientListenerRunner(this.testTrigger, this.certificate, clientListenerPort);
            this.gsRunner = new GermServiceRunner(germPort, string.Format("127.0.0.1:{0}", clientListenerPort));

            this.clRunner.Run();
            this.clRunner.Restart();

            this.gsRunner.Run();

            this.testTrigger.WaitOne();

            Assert.IsTrue(clRunner.TestSucceed);
        }

        [Test]
        public void GermShouldStillConnectAfterClientListenerChangePort()
        {
            int clientListenerPort = 21200;
            int germPort = 21253;

            this.clRunner = new ClientListenerRunner(this.testTrigger, this.certificate, clientListenerPort);

            this.clRunner.Run();

            clientListenerPort = 21201;
            this.clRunner.ChangePort(clientListenerPort);
            this.clRunner.Restart();

            this.gsRunner = new GermServiceRunner(germPort, string.Format("127.0.0.1:{0}", clientListenerPort));
            this.gsRunner.Run();

            this.testTrigger.WaitOne();

            Assert.IsTrue(clRunner.TestSucceed);
        }
    }
}
