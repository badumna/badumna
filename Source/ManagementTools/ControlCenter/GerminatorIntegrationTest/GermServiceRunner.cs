﻿//-----------------------------------------------------------------------
// <copyright file="GermServiceRunner.cs" company="National ICT Australia Ltd">
//     Copyright (c) 2012 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace GerminatorIntegrationTest
{
    using GermServer;

    internal class GermServiceRunner
    {
        private GermService germService;

        public GermServiceRunner(int germPort, string controlCenterAddress)
        {
            this.germService = new GermService(germPort, controlCenterAddress);
        }

        public void Run()
        {
            this.germService.Start();
        }

        public void Stop()
        {
            if (this.germService.CanStop)
            {
                this.germService.Stop();
            }
        }
    }
}
