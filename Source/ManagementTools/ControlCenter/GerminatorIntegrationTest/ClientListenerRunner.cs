﻿//-----------------------------------------------------------------------
// <copyright file="ClientListenerRunner.cs" company="National ICT Australia Ltd">
//     Copyright (c) 2012 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace GerminatorIntegrationTest
{
    using System.Security.Cryptography.X509Certificates;
    using System.Threading;
    using Germinator;
    using Germinator.GermProtocol;

    internal class ClientListenerRunner
    {
        private AutoResetEvent testTrigger;

        private ClientListener clientListener;

        private GermClient client;

        private bool testSucceed;

        public ClientListenerRunner(AutoResetEvent testTrigger, X509Certificate2 certificate, int portNumber)
        {
            this.testTrigger = testTrigger;
            this.clientListener = new ClientListener(certificate, portNumber);
        }

        public bool TestSucceed
        {
            get { return this.testSucceed; }
        }

        public void Run()
        {
            this.clientListener.AddGerm = this.TestDelegate;
            this.clientListener.Start();
        }

        public void Restart()
        {
            this.clientListener.Restart();
        }

        public void ChangePort(int newPort)
        {
            this.clientListener.Port = newPort;
        }

        public void Stop()
        {
            this.clientListener.Stop();

            if (this.client != null)
            {
                this.client.Disconnect();
            }
        }

        private void TestDelegate(GermClient client)
        {
            this.client = client;
            this.testSucceed = client != null;
            this.testTrigger.Set();
        }
    }
}
