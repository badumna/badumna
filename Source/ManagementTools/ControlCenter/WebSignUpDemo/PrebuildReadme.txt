﻿
This project exists as a placeholder for prebuild steps for the DeiAdministrationTool project.  The DeiAdministrationTool
project is distributed with the installer and we want it to look exactly like it should in an install so it can just be
exported directly when building the installer.  However it has dependencies such as Dei.dll which need to be copied in
so it builds correctly.