﻿//---------------------------------------------------------------------------------
// <copyright file="HomeController.cs" company="National ICT Australia Ltd">
//     Copyright (c) 2010 National ICT Australia Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.SessionState;
using System.Xml;

using Dei;
using Dei.AdminClient;

namespace WebSignUpDemo.Controllers
{
    /// <summary>
    /// Home Controller for Dei Administration tool application. It should contains all information about
    /// the dei server and what username that will be used to create a new account for user.
    /// </summary>
    [HandleError]
    public class HomeController : Controller
    {
        /// <summary>
        /// Admin client session key.
        /// </summary>
        internal const string AdminClientSessionKey = "AdminClient";

        /// <summary>
        /// Dei server address.
        /// </summary>
        private static string deiServer;

        /// <summary>
        /// Dei username that will be used for creating a new account (Make sure this username has 
        /// creating account permission).
        /// </summary>
        private static string username;

        /// <summary>
        /// The corresponding password for the above username.
        /// </summary>
        private static string password;

        /// <summary>
        /// A value indicating whether the ssl connection is used to connect to Dei Server.
        /// </summary>
        private static bool sslConnection;

        /// <summary>
        /// A value indicating whether the configuration has been load.
        /// </summary>
        private static bool isConfigured = false;

        /// <summary>
        /// Get the error message description from AdminClient Failure reasons.
        /// </summary>
        /// <param name="client">Admin client.</param>
        /// <returns>Retrun the error message.</returns>
        public static string GetErrorDescription(AdminClient client)
        {
            switch (client.FailureReason)
            {
                case FailureReasons.NoneGiven:
                    return Resources.Resources.DeiError_NoReason;

                case FailureReasons.PermissionDenied:
                    return Resources.Resources.DeiError_PermissionDenied;

                case FailureReasons.TooManyCharactersInLogin:
                    return Resources.Resources.DeiError_LoginTooLong;

                case FailureReasons.InvalidRequest:
                    return Resources.Resources.DeiError_InvalidRequest;

                case FailureReasons.AuthenticationFailed:
                    return Resources.Resources.DeiError_AuthenticationFailed;

                case FailureReasons.AccountAlreadyExists:
                    return Resources.Resources.DeiError_AlreadyExists;

                case FailureReasons.ConnectionError:
                    return Resources.Resources.DeiError_ConnectionError;
            }

            return Resources.Resources.DeiError_UnknownError;
        }

        /// <summary>
        /// The Home index page.
        /// </summary>
        /// <returns>Return home index page.</returns>
        public ActionResult Index()
        {
            string baseDirectory = System.Web.HttpRuntime.AppDomainAppPath;
            string dataDirectory = Path.Combine(baseDirectory, "App_Data");
            string errorDescription = string.Empty;

            if (!isConfigured)
            {
                if (this.LoadConfiguration(Path.Combine(dataDirectory, "Configuration.xml")))
                {
                    if (!this.IsLoggedIn())
                    {
                        this.Login(out errorDescription);
                    }
                }
            }
            else
            {
                if (!this.IsLoggedIn())
                {
                    this.Login(out errorDescription);
                }
            }

            if (!string.IsNullOrEmpty(errorDescription))
            {
                ViewData["ErrorMessage"] = errorDescription;
            }
            
            return View();
        }

        /// <summary>
        /// Create a new account page.
        /// </summary>
        /// <returns>Return the create account page.</returns>
        public ActionResult Create()
        {
            return View();
        }

        /// <summary>
        /// The post method of create a new account page, collecting the information from the client and 
        /// use it to create a new account.
        /// </summary>
        /// <param name="username">New user name.</param>
        /// <param name="password">Password corresponding to this user name.</param>
        /// <param name="passwordConfirmation">Password confirmation.</param>
        /// <returns>Return an error message or successful message back to the client.</returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Create(string username, string password, string passwordConfirmation)
        {
            try
            {
                if (string.IsNullOrEmpty(username))
                {
                    this.ModelState.AddModelError("username", Resources.Resources.MustGiveName);
                }

                if (string.IsNullOrEmpty(password))
                {
                    this.ModelState.AddModelError("password", Resources.Resources.PasswordInvalid);
                }

                if (string.IsNullOrEmpty(passwordConfirmation))
                {
                    this.ModelState.AddModelError("passwordConfirmation", Resources.Resources.PasswordInvalid);
                }

                if (!password.Equals(passwordConfirmation))
                {
                    this.ModelState.AddModelError("all", Resources.Resources.PasswordMismatch);
                }

                if (!this.ModelState.IsValid)
                {
                    return this.View();
                }

                AdminClient client = this.GetClient();
                string errorDescription = string.Empty;

                if (client == null)
                {
                    this.Login(out errorDescription);
                }

                if (string.IsNullOrEmpty(errorDescription))
                {
                    if (client.CreateUser(username, password))
                    {
                        if (client.GrantPermission(username, (long)PermissionTypes.Participation))
                        {
                            this.ViewData["Message"] = String.Format("User account '{0}' has been created, with participation permission.", username);
                        }
                        else
                        {
                            this.ViewData["Message"] = String.Format("User account '{0}' has been created, without participation permission.", username);
                        }

                        return View();
                    }
                    else
                    {
                        this.ModelState.AddModelError("all", string.Format("Failed to create account for '{0}' : {1}", username, GetErrorDescription(client)));
                        return View();
                    }
                }
                else
                {
                    this.ViewData["Message"] = errorDescription;
                    return View();
                }
            }
            catch
            {
                return View();
            }
        }

        /// <summary>
        /// Change the password.
        /// </summary>
        /// <returns>Return the change password page.</returns>
        public ActionResult ChangePassword()
        {
            return View();
        }

        /// <summary>
        /// Post method of change password, collect the information from the user, validate the old and the new password for 
        /// a given username.
        /// </summary>
        /// <param name="username">User name.</param>
        /// <param name="oldPassword">Old password.</param>
        /// <param name="newPassword">New password.</param>
        /// <param name="passwordConfirmation">New password confiramtion.</param>
        /// <returns>Display the message whether the process is successful.</returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ChangePassword(string username, string oldPassword, string newPassword, string passwordConfirmation)
        {
            try
            {
                if (string.IsNullOrEmpty(username))
                {
                    this.ModelState.AddModelError("username", Resources.Resources.MustGiveName);
                }

                if (string.IsNullOrEmpty(oldPassword))
                {
                    this.ModelState.AddModelError("oldPassword", Resources.Resources.PasswordInvalid);
                }

                if (string.IsNullOrEmpty(newPassword))
                {
                    this.ModelState.AddModelError("newPassword", Resources.Resources.PasswordInvalid);
                }

                if (string.IsNullOrEmpty(passwordConfirmation))
                {
                    this.ModelState.AddModelError("passwordConfirmation", Resources.Resources.PasswordInvalid);
                }

                if (!newPassword.Equals(passwordConfirmation))
                {
                    this.ModelState.AddModelError("all", Resources.Resources.PasswordMismatch);
                }

                if (!this.ModelState.IsValid)
                {
                    return this.View();
                }
            }
            catch
            {
                return View();
            }

            try
            {
                using (AdminClient client = new AdminClient())
                {
                    client.IgnoreSslErrors = true;
                    string hostname = deiServer.Split(':')[0];
                    int port = int.Parse(deiServer.Split(':')[1]);

                    if (client.Login(hostname, port, username, oldPassword, sslConnection))
                    {
                        if (client.ChangePassword(newPassword))
                        {
                            this.ViewData["Message"] = string.Format("The password for '{0}' has been successfully changed", username);
                        }
                        else
                        {
                            this.ModelState.AddModelError("all", string.Format("Failed to change the password: {0},", GetErrorDescription(client)));
                        }
                    }
                    else
                    {
                        this.ModelState.AddModelError("all", string.Format("Failed to login with old password : {0}", GetErrorDescription(client)));
                    }
                }
            }
            catch
            {
                this.ModelState.AddModelError("all", "An unexpected error has occured. Please try again.");
            }

            return View();
        }

        /// <summary>
        /// The about page.
        /// </summary>
        /// <returns>Returns the about page.</returns>
        public ActionResult About()
        {
            return View();
        }

        /// <summary>
        /// Load the configuration from the xml file.
        /// </summary>
        /// <param name="fileName">Configuration file name.</param>
        /// <returns>Return true on success.</returns>
        private bool LoadConfiguration(string fileName)
        {
            try
            {
                using (XmlReader reader = XmlReader.Create(new StreamReader(fileName)))
                {
                    if (reader.Read())
                    {
                        while (reader.LocalName != "Configuration")
                        {
                            reader.Read();
                        }

                        if (reader.LocalName.Equals("Configuration"))
                        {
                            deiServer = reader.GetAttribute("DeiServer");
                            username = reader.GetAttribute("Username");
                            password = reader.GetAttribute("Password");
                            sslConnection = bool.Parse(reader.GetAttribute("SslConnection"));
                        }

                        isConfigured = true;
                        return true;
                    }
                }
            }
            catch (FileNotFoundException)
            {
                System.Diagnostics.Trace.TraceWarning("Failed to load server configuration file {0}", fileName);
                return false;
            }

            return false;
        }

        /// <summary>
        /// Check whether a session has logged in.
        /// </summary>
        /// <returns>Return true if it has logged in.</returns>
        private bool IsLoggedIn()
        {
            if (this.Session != null)
            {
                return this.Session[HomeController.AdminClientSessionKey] != null;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Login using the provided dei username and password.
        /// </summary>
        /// <param name="errorDescription">Error description if exist.</param>
        private void Login(out string errorDescription)
        {
            errorDescription = string.Empty;

            try
            {    
                AdminClient oldClient = this.GetClient();
                if (oldClient != null)
                {
                    oldClient.Dispose();
                    this.Session.Remove(HomeController.AdminClientSessionKey);
                }
            }
            catch
            {
            }

            AdminClient client = new AdminClient();
            try
            {
                client.IgnoreSslErrors = true;
                string hostname = deiServer.Split(':')[0];
                int port = int.Parse(deiServer.Split(':')[1]);

                if (client.Login(hostname, port, username, password, sslConnection))
                {
                    this.Session[HomeController.AdminClientSessionKey] = client;
                }
                else
                {
                    errorDescription = HomeController.GetErrorDescription(client);
                }   
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Get the admin client.
        /// </summary>
        /// <returns>Return Admin client.</returns>
        private AdminClient GetClient()
        {
            if (this.Session != null)
            {
                AdminClient client = (AdminClient)this.Session[HomeController.AdminClientSessionKey];
                if (client == null || client.IsClosed)
                {
                    if (client != null)
                    {
                        client.Close();
                    }

                    this.Session.Remove(HomeController.AdminClientSessionKey);
                    return null;
                }

                return client;
            }
            else
            {
                return null;
            }
        }
    }
}
