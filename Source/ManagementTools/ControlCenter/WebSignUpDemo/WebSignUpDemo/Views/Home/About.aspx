﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="aboutTitle" ContentPlaceHolderID="TitleContent" runat="server">
    About Us
</asp:Content>

<asp:Content ID="aboutContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2>About</h2>
    <p>
        WebSignUpDemo is used mainly for  account creation and modification (i.e. change user account password) using the Dei server. 
        This is just a simple example, you should modify this web application or create your own according to your need.
    </p>
</asp:Content>
