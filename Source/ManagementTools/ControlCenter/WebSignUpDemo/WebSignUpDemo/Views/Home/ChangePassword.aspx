<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	ChangePassword
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2>ChangePassword</h2>
    
     <%if (ViewData["Message"] != null)
      { %>
        <p>
            <%= Html.Encode(ViewData["Message"]) %>
        </p>
    <%}
      else
      {%>
    <%= Html.ValidationSummary(Resources.Resources.ErrorSummary)%>
    <%  using (Html.BeginForm())
        { %>
    <fieldset>
        <table>
            <tr>
                <td>
                    <label for="Username">
                        Username :</label>
                </td>
                <td>
                    <%= Html.TextBox("Username", string.Empty)%>
                    <%= Html.ValidationMessage("username", "*")%>
                </td>
            </tr>
            <tr>
                <td>
                    <label for="OldPassword">
                        Old Password :</label>
                </td>
                <td>
                    <%= Html.Password("OldPassword")%>
                    <%= Html.ValidationMessage("oldPassword", "*")%>
                </td>
            </tr>
            <tr>
                <td>
                    <label for="NewPassword">
                        New Password :</label>
                </td>
                <td>
                    <%= Html.Password("NewPassword")%>
                    <%= Html.ValidationMessage("newPassword", "*")%>
                </td>
            </tr>
            <tr>
                <td>
                    <label for="PasswordConfirmation">
                        Password Confirmation :</label>
                </td>
                <td>
                    <%= Html.Password("PasswordConfirmation")%>
                    <%= Html.ValidationMessage("passwordConfirmation", "*")%>
                </td>
            </tr>
            <tr>
                <td colspan='2'>
                    <input type="submit" value="<%= Resources.Resources.Submit %>" />
                </td>
            </tr>
        </table>
    </fieldset>
    <%  } %>
    <%} %>
    <div>
        <p> <%= Html.ActionLink("Home", "Index") %></p>
    </div>

</asp:Content>
