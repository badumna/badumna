<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Create
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2>
        Create a New Account</h2>
    <%if (ViewData["Message"] != null)
      { %>
        <p>
            <%= Html.Encode(ViewData["Message"]) %>
        </p>
    <%}
      else
      {%>
    <%= Html.ValidationSummary(Resources.Resources.ErrorSummary)%>
    <%  using (Html.BeginForm())
        { %>
    <fieldset>
        <table>
            <tr>
                <td>
                    <label for="Username">
                        Username :</label>
                </td>
                <td>
                    <%= Html.TextBox("Username", string.Empty)%>
                    <%= Html.ValidationMessage("username", "*")%>
                </td>
            </tr>
            <tr>
                <td>
                    <label for="Password">
                        Password :</label>
                </td>
                <td>
                    <%= Html.Password("Password")%>
                    <%= Html.ValidationMessage("password", "*")%>
                </td>
            </tr>
            <tr>
                <td>
                    <label for="PasswordConfirmation">
                        Password Confirmation :</label>
                </td>
                <td>
                    <%= Html.Password("PasswordConfirmation")%>
                    <%= Html.ValidationMessage("passwordConfirmation", "*")%>
                </td>
            </tr>
            <tr>
                <td colspan='2'>
                    <input type="submit" value="<%= Resources.Resources.Submit %>" />
                </td>
            </tr>
        </table>
    </fieldset>
    <%  } %>
    <%} %>
    <div>
        <p> <%= Html.ActionLink("Home", "Index") %></p>
    </div>
</asp:Content>
