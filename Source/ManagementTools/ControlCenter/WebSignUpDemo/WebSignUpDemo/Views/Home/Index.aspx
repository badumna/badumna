﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="indexTitle" ContentPlaceHolderID="TitleContent" runat="server">
    Home Page
</asp:Content>
<asp:Content ID="indexContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2>
        Welcome to Web Sign Up Demo
    </h2>
    <p>
        <%= Html.Encode(ViewData["Message"]) %></p>
    <%if (ViewData["ErrorMessage"] != null)
      { %>
    <p>
        <%= Html.Encode(ViewData["ErrorMessage"])%>
    </p>
    <%}
      else
      {%>
    <p>
        <%= Html.ActionLink("Create Account", "Create") %>
        <br />
        <%= Html.ActionLink("Change Password", "ChangePassword") %>
    </p>
    <%} %>
</asp:Content>
