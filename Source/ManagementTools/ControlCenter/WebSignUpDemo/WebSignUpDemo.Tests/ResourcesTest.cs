//-----------------------------------------------------------------------
// <copyright file="ResourcesTest.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace WebSignUpDemo.Tests
{
    using System.Globalization;
    using System.Resources;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Microsoft.VisualStudio.TestTools.UnitTesting.Web;
    using Resources;

    /// <summary>
    /// This is a test class for ResourcesTest and is intended
    /// to contain all ResourcesTest Unit Tests
    /// </summary>
    [TestClass()]
    public class ResourcesTest
    {
        /// <summary>
        /// Test context.
        /// </summary>
        private TestContext testContextInstance;

        /// <summary>
        /// Gets or sets the test context which provides
        /// information about and functionality for the current test run.
        /// </summary>
        public TestContext TestContext
        {
            get
            {
                return this.testContextInstance;
            }

            set
            {
                this.testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        /// A test for Submit
        /// </summary>
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("%PathToWebRoot%\\WebSignUpDemo", "/")]
        [UrlToTest("http://localhost:2169/")]
        public void SubmitTest()
        {
            string actual;
            actual = Resources.Submit;
            Assert.AreEqual("Submit", actual);
        }

        /// <summary>
        /// A test for ResourceManager
        /// </summary>
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("%PathToWebRoot%\\WebSignUpDemo", "/")]
        [UrlToTest("http://localhost:2169/")]
        public void ResourceManagerTest()
        {
            ResourceManager actual;
            actual = Resources.ResourceManager;
            Assert.IsNotNull(actual);
        }

        /// <summary>
        /// A test for PasswordMismatch
        /// </summary>
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("%PathToWebRoot%\\WebSignUpDemo", "/")]
        [UrlToTest("http://localhost:2169/")]
        public void PasswordMismatchTest()
        {
            string actual;
            actual = Resources.PasswordMismatch;
            Assert.AreEqual("Passwords do not match", actual);
        }

        /// <summary>
        /// A test for PasswordInvalid
        /// </summary>
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("%PathToWebRoot%\\WebSignUpDemo", "/")]
        [UrlToTest("http://localhost:2169/")]
        public void PasswordInvalidTest()
        {
            string actual;
            actual = Resources.PasswordInvalid;
            Assert.AreEqual("Password is invalid", actual);
        }

        /// <summary>
        /// A test for MustGiveName
        /// </summary>
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("%PathToWebRoot%\\WebSignUpDemo", "/")]
        [UrlToTest("http://localhost:2169/")]
        public void MustGiveNameTest()
        {
            string actual;
            actual = Resources.MustGiveName;
            Assert.AreEqual("Must specify a name.",actual);
        }

        /// <summary>
        /// A test for ErrorSummary
        /// </summary>
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("%PathToWebRoot%\\WebSignUpDemo", "/")]
        [UrlToTest("http://localhost:2169/")]
        public void ErrorSummaryTest()
        {
            string actual;
            actual = Resources.ErrorSummary;
            Assert.AreEqual("Please correct the following errors and try again.", actual);
        }

        /// <summary>
        /// A test for DeiError_UnknownError
        /// </summary>
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("%PathToWebRoot%\\WebSignUpDemo", "/")]
        [UrlToTest("http://localhost:2169/")]
        public void DeiError_UnknownErrorTest()
        {
            string actual;
            actual = Resources.DeiError_UnknownError;
            Assert.AreEqual("unknown error", actual);
        }

        /// <summary>
        /// A test for DeiError_PermissionDenied
        /// </summary>
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("%PathToWebRoot%\\WebSignUpDemo", "/")]
        [UrlToTest("http://localhost:2169/")]
        public void DeiError_PermissionDeniedTest()
        {
            string actual;
            actual = Resources.DeiError_PermissionDenied;
            Assert.AreEqual("permission denied", actual);
        }

        /// <summary>
        /// A test for DeiError_NoReason
        /// </summary>
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("%PathToWebRoot%\\WebSignUpDemo", "/")]
        [UrlToTest("http://localhost:2169/")]
        public void DeiError_NoReasonTest()
        {
            string actual;
            actual = Resources.DeiError_NoReason;
            Assert.AreEqual("no reason given", actual);
        }

        /// <summary>
        /// A test for DeiError_LoginTooLong
        /// </summary>
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("%PathToWebRoot%\\WebSignUpDemo", "/")]
        [UrlToTest("http://localhost:2169/")]
        public void DeiError_LoginTooLongTest()
        {
            string actual;
            actual = Resources.DeiError_LoginTooLong;
            Assert.AreEqual("username is too long", actual);
        }

        /// <summary>
        /// A test for DeiError_InvalidRequest
        /// </summary>
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("%PathToWebRoot%\\WebSignUpDemo", "/")]
        [UrlToTest("http://localhost:2169/")]
        public void DeiError_InvalidRequestTest()
        {
            string actual;
            actual = Resources.DeiError_InvalidRequest;
            Assert.AreEqual("invalid request", actual);
        }

        /// <summary>
        /// A test for DeiError_ConnectionError
        /// </summary>
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("%PathToWebRoot%\\WebSignUpDemo", "/")]
        [UrlToTest("http://localhost:2169/")]
        public void DeiError_ConnectionErrorTest()
        {
            string actual;
            actual = Resources.DeiError_ConnectionError;
            Assert.AreEqual("connection error", actual);
        }

        /// <summary>
        /// A test for DeiError_AuthenticationFailed
        /// </summary>
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("%PathToWebRoot%\\WebSignUpDemo", "/")]
        [UrlToTest("http://localhost:2169/")]
        public void DeiError_AuthenticationFailedTest()
        {
            string actual;
            actual = Resources.DeiError_AuthenticationFailed;
            Assert.AreEqual("authentication failed", actual);
        }

        /// <summary>
        /// A test for DeiError_AlreadyExists
        /// </summary>
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("%PathToWebRoot%\\WebSignUpDemo", "/")]
        [UrlToTest("http://localhost:2169/")]
        public void DeiError_AlreadyExistsTest()
        {
            string actual;
            actual = Resources.DeiError_AlreadyExists;
            Assert.AreEqual("account already exists", actual);
        }

        /// <summary>
        /// A test for Resources Constructor
        /// </summary>
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("%PathToWebRoot%\\WebSignUpDemo", "/")]
        [UrlToTest("http://localhost:2169/")]
        public void ResourcesConstructorTest()
        {
            Resources target = new Resources();
            Assert.IsNotNull(target);
        }
    }
}
