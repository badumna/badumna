//-----------------------------------------------------------------------
// <copyright file="HomeControllerTest.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace WebSignUpDemo.Tests
{
    using System.Collections.Generic;
    using System.IO;
    using System.Web.Mvc;
    using System.Web.SessionState;
    using Dei.AdminClient;
    using WebSignUpDemo.Controllers;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Microsoft.VisualStudio.TestTools.UnitTesting.Web;
    using MvcFakes;

    /// <summary>
    /// This is a test class for HomeControllerTest and is intended
    /// to contain all HomeControllerTest Unit Tests
    /// </summary>
    [TestClass()]
    public class HomeControllerTest
    {
        /// <summary>
        /// Test Context.
        /// </summary>
        private TestContext testContextInstance;

        /// <summary>
        /// Gets or sets the test context which provides
        /// information about and functionality for the current test run.
        /// </summary>
        public TestContext TestContext
        {
            get
            {
                return this.testContextInstance;
            }

            set
            {
                this.testContextInstance = value;
            }
        }

        #region Additional test attributes
        //// 
        ////You can use the following additional attributes as you write your tests:
        ////
        ////Use ClassInitialize to run code before running the first test in the class
        ////[ClassInitialize()]
        ////public static void MyClassInitialize(TestContext testContext)
        ////{
        ////}
        ////
        ////Use ClassCleanup to run code after all tests in a class have run
        ////[ClassCleanup()]
        ////public static void MyClassCleanup()
        ////{
        ////}
        ////
        ////Use TestInitialize to run code before running each test
        ////[TestInitialize()]
        ////public void MyTestInitialize()
        ////{
        ////}
        ////
        ////Use TestCleanup to run code after each test has run
        ////[TestCleanup()]
        ////public void MyTestCleanup()
        ////{
        ////}
        ////
        #endregion

        /// <summary>
        /// A test for Login
        /// </summary>
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("%PathToWebRoot%\\WebSignUpDemo", "/")]
        [UrlToTest("http://localhost:2169/")]
        [DeploymentItem("WebSignUpDemo.dll")]
        public void LoginTest()
        {
            HomeController_Accessor target = new HomeController_Accessor();
            HomeController_Accessor.deiServer = "localhost:21248";
            HomeController_Accessor.username = "admin";
            HomeController_Accessor.password = "admin_password";
            HomeController_Accessor.sslConnection = false;
            
            string errorDescription = string.Empty; 
            string errorDescriptionExpected = Resources.Resources.DeiError_ConnectionError; 
            target.Login(out errorDescription);
            Assert.AreEqual(errorDescriptionExpected, errorDescription);
        }

        /// <summary>
        /// A test for Login (case: oldClient is not null).
        /// </summary>
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("%PathToWebRoot%\\WebSignUpDemo", "/")]
        [UrlToTest("http://localhost:2169/")]
        [DeploymentItem("WebSignUpDemo.dll")]
        public void LoginTest1()
        {
            HomeController_Accessor target = new HomeController_Accessor();
            HomeController_Accessor.deiServer = "localhost:21248";
            HomeController_Accessor.username = "admin";
            HomeController_Accessor.password = "admin_password";
            HomeController_Accessor.sslConnection = false;

            HomeController controller = (HomeController)target.Target;

            SessionStateItemCollection sessionItems = new SessionStateItemCollection();
            sessionItems[HomeController_Accessor.AdminClientSessionKey] = new AdminClient();
            controller.ControllerContext = new FakeControllerContext(controller, sessionItems);

            string errorDescription = string.Empty;
            string errorDescriptionExpected = Resources.Resources.DeiError_ConnectionError;
            target.Login(out errorDescription);
            Assert.AreEqual(errorDescriptionExpected, errorDescription);
        }

        /// <summary>
        /// A test for LoadConfiguration
        /// </summary>
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("%PathToWebRoot%\\WebSignUpDemo", "/")]
        [UrlToTest("http://localhost:2169/")]
        [DeploymentItem("WebSignUpDemo.dll")]
        public void LoadConfigurationTest()
        {
            HomeController_Accessor target = new HomeController_Accessor(); 

            string fileName = "Configuration.xml";
            string app_DataDir = Path.Combine(System.Web.HttpRuntime.AppDomainAppPath, "App_Data");
            
            Assert.IsTrue(Directory.Exists(app_DataDir));
            Assert.IsTrue(File.Exists(Path.Combine(app_DataDir, fileName)));

            bool expected = true;
            bool actual;
            
            actual = target.LoadConfiguration(Path.Combine(app_DataDir, fileName));
            
            Assert.AreEqual(expected, actual);
            Assert.AreEqual(HomeController_Accessor.deiServer, "localhost:21248");
            Assert.AreEqual(HomeController_Accessor.username, "admin");
            Assert.AreEqual(HomeController_Accessor.password, "admin_password");
            Assert.AreEqual(HomeController_Accessor.sslConnection, false);
        }

        /// <summary>
        /// A test for IsLoggedIn (when Session is null)
        /// </summary>
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("%PathToWebRoot%\\WebSignUpDemo", "/")]
        [UrlToTest("http://localhost:2169/")]
        [DeploymentItem("WebSignUpDemo.dll")]
        public void IsLoggedInTest()
        {
            HomeController_Accessor target = new HomeController_Accessor();

            bool expected = false;
            bool actual;
            actual = target.IsLoggedIn();
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        /// A test for IsLoggedIn (when Session and the AdminClient are not null)
        /// </summary>
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("%PathToWebRoot%\\WebSignUpDemo", "/")]
        [UrlToTest("http://localhost:2169/")]
        [DeploymentItem("WebSignUpDemo.dll")]
        public void IsLoggedInTest1()
        {
            HomeController_Accessor target = new HomeController_Accessor();

            bool expected = true;
            bool actual;

            HomeController controller = (HomeController)target.Target;

            SessionStateItemCollection sessionItems = new SessionStateItemCollection();
            sessionItems[HomeController_Accessor.AdminClientSessionKey] = new AdminClient();
            controller.ControllerContext = new FakeControllerContext(controller, sessionItems);
            actual = target.IsLoggedIn();

            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        /// A test for IsLoggedIn (when Session is not null and  the AdminClient is null)
        /// </summary>
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("%PathToWebRoot%\\WebSignUpDemo", "/")]
        [UrlToTest("http://localhost:2169/")]
        [DeploymentItem("WebSignUpDemo.dll")]
        public void IsLoggedInTest2()
        {
            HomeController_Accessor target = new HomeController_Accessor();

            bool expected = false;
            bool actual;

            HomeController controller = (HomeController)target.Target;

            SessionStateItemCollection sessionItems = new SessionStateItemCollection();
            sessionItems[HomeController_Accessor.AdminClientSessionKey] = null;
            controller.ControllerContext = new FakeControllerContext(controller, sessionItems);
            actual = target.IsLoggedIn();

            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        /// A test for Index
        /// </summary>
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("%PathToWebRoot%\\WebSignUpDemo", "/")]
        [UrlToTest("http://localhost:2169/")]
        public void IndexTest()
        {
            HomeController target = new HomeController(); 
            ActionResult actual;
            actual = target.Index();
            Assert.IsNotNull(actual);
        }

        /// <summary>
        /// A test for GetErrorDescription
        /// </summary>
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("%PathToWebRoot%\\WebSignUpDemo", "/")]
        [UrlToTest("http://localhost:2169/")]
        public void GetErrorDescriptionTest()
        {
            AdminClient client = new AdminClient();
            
            string expected = Resources.Resources.DeiError_NoReason;
            string actual;
            actual = HomeController.GetErrorDescription(client);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        /// A test for GetErrorDescription (Account already exist)
        /// </summary>
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("%PathToWebRoot%\\WebSignUpDemo", "/")]
        [UrlToTest("http://localhost:2169/")]
        public void GetErrorDescriptionTest1()
        {
            AdminClient_Accessor target = new AdminClient_Accessor();
            target.FailureReason = Dei.FailureReasons.AccountAlreadyExists;
            AdminClient client = (AdminClient)target.Target;

            string expected = Resources.Resources.DeiError_AlreadyExists;
            string actual;
            actual = HomeController.GetErrorDescription(client);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        /// A test for GetErrorDescription (NoneGiven)
        /// </summary>
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("%PathToWebRoot%\\WebSignUpDemo", "/")]
        [UrlToTest("http://localhost:2169/")]
        public void GetErrorDescriptionTest2()
        {
            AdminClient_Accessor target = new AdminClient_Accessor();
            target.FailureReason = Dei.FailureReasons.NoneGiven;
            AdminClient client = (AdminClient)target.Target;

            string expected = Resources.Resources.DeiError_NoReason;
            string actual;
            actual = HomeController.GetErrorDescription(client);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        /// A test for GetErrorDescription (ConnectionError)
        /// </summary>
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("%PathToWebRoot%\\WebSignUpDemo", "/")]
        [UrlToTest("http://localhost:2169/")]
        public void GetErrorDescriptionTest3()
        {
            AdminClient_Accessor target = new AdminClient_Accessor();
            target.FailureReason = Dei.FailureReasons.ConnectionError;
            AdminClient client = (AdminClient)target.Target;

            string expected = Resources.Resources.DeiError_ConnectionError;
            string actual;
            actual = HomeController.GetErrorDescription(client);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        /// A test for GetErrorDescription (PermissionDenied)
        /// </summary>
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("%PathToWebRoot%\\WebSignUpDemo", "/")]
        [UrlToTest("http://localhost:2169/")]
        public void GetErrorDescriptionTest4()
        {
            AdminClient_Accessor target = new AdminClient_Accessor();
            target.FailureReason = Dei.FailureReasons.PermissionDenied;
            AdminClient client = (AdminClient)target.Target;

            string expected = Resources.Resources.DeiError_PermissionDenied;
            string actual;
            actual = HomeController.GetErrorDescription(client);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        /// A test for GetErrorDescription (TooManyCharactersInLogin)
        /// </summary>
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("%PathToWebRoot%\\WebSignUpDemo", "/")]
        [UrlToTest("http://localhost:2169/")]
        public void GetErrorDescriptionTest5()
        {
            AdminClient_Accessor target = new AdminClient_Accessor();
            target.FailureReason = Dei.FailureReasons.TooManyCharactersInLogin;
            AdminClient client = (AdminClient)target.Target;

            string expected = Resources.Resources.DeiError_LoginTooLong;
            string actual;
            actual = HomeController.GetErrorDescription(client);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        /// A test for GetErrorDescription (InvalidRequest)
        /// </summary>
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("%PathToWebRoot%\\WebSignUpDemo", "/")]
        [UrlToTest("http://localhost:2169/")]
        public void GetErrorDescriptionTest6()
        {
            AdminClient_Accessor target = new AdminClient_Accessor();
            target.FailureReason = Dei.FailureReasons.InvalidRequest;
            AdminClient client = (AdminClient)target.Target;

            string expected = Resources.Resources.DeiError_InvalidRequest;
            string actual;
            actual = HomeController.GetErrorDescription(client);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        /// A test for GetErrorDescription (AuthenticationFailed)
        /// </summary>
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("%PathToWebRoot%\\WebSignUpDemo", "/")]
        [UrlToTest("http://localhost:2169/")]
        public void GetErrorDescriptionTest7()
        {
            AdminClient_Accessor target = new AdminClient_Accessor();
            target.FailureReason = Dei.FailureReasons.AuthenticationFailed;
            AdminClient client = (AdminClient)target.Target;

            string expected = Resources.Resources.DeiError_AuthenticationFailed;
            string actual;
            actual = HomeController.GetErrorDescription(client);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        /// A test for GetClient
        /// </summary>
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("%PathToWebRoot%\\WebSignUpDemo", "/")]
        [UrlToTest("http://localhost:2169/")]
        [DeploymentItem("WebSignUpDemo.dll")]
        public void GetClientTest()
        {
            HomeController_Accessor target = new HomeController_Accessor(); 
            AdminClient expected = null;
            AdminClient actual;
            actual = target.GetClient();
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        /// A test for GetClient (Session is not null but AdminClient is null).
        /// </summary>
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("%PathToWebRoot%\\WebSignUpDemo", "/")]
        [UrlToTest("http://localhost:2169/")]
        [DeploymentItem("WebSignUpDemo.dll")]
        public void GetClientTest1()
        {
            HomeController_Accessor target = new HomeController_Accessor();
            
            AdminClient expected = null;
            AdminClient actual;

            HomeController controller = (HomeController)target.Target;

            SessionStateItemCollection sessionItems = new SessionStateItemCollection();
            sessionItems[HomeController_Accessor.AdminClientSessionKey] = null;
            controller.ControllerContext = new FakeControllerContext(controller, sessionItems);

            actual = target.GetClient();
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        /// A test for GetClient (Both Session and AdminClient are not null, and the dei client is null).
        /// <remarks>Note: not sure how to test when the dei server is actually running (integtration test)</remarks>
        /// </summary>
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("%PathToWebRoot%\\WebSignUpDemo", "/")]
        [UrlToTest("http://localhost:2169/")]
        [DeploymentItem("WebSignUpDemo.dll")]
        public void GetClientTest2()
        {
            HomeController_Accessor target = new HomeController_Accessor();
            AdminClient client = new AdminClient();
        
            AdminClient expected = null;    
            AdminClient actual;

            HomeController controller = (HomeController)target.Target;

            SessionStateItemCollection sessionItems = new SessionStateItemCollection();
            sessionItems[HomeController_Accessor.AdminClientSessionKey] = client;
            controller.ControllerContext = new FakeControllerContext(controller, sessionItems);

            actual = target.GetClient();
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        /// A test for Create
        /// </summary>
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("%PathToWebRoot%\\WebSignUpDemo", "/")]
        [UrlToTest("http://localhost:2169/")]
        public void CreateTest1()
        {
            HomeController target = new HomeController(); 
            string username = "admin";
            string password = "admin_password"; 
            string passwordConfirmation = "admin_password"; 
            string expected = Resources.Resources.DeiError_ConnectionError; 
            ActionResult actual;
            actual = target.Create(username, password, passwordConfirmation);
            Assert.AreEqual(expected, ((ViewResult)actual).ViewData["Message"] as string);
        }

        /// <summary>
        /// A test for Create (password mismatch and empty password).
        /// </summary>
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("%PathToWebRoot%\\WebSignUpDemo", "/")]
        [UrlToTest("http://localhost:2169/")]
        public void CreateTest2()
        {
            HomeController target = new HomeController();
            string username = "admin";
            string password = string.Empty;
            string passwordConfirmation = "admin_password";

            ActionResult actual;
            actual = target.Create(username, password, passwordConfirmation);

            ModelState modelState;
            IEnumerator<ModelError> enumerator;
            
            //// Password mismatch.
            Assert.IsTrue(target.ModelState.TryGetValue("all", out modelState));
            Assert.AreEqual(modelState.Errors.Count, 1);

            enumerator = modelState.Errors.GetEnumerator();
            enumerator.MoveNext();

            Assert.AreEqual(enumerator.Current.ErrorMessage, Resources.Resources.PasswordMismatch);

            //// Password empty.
            Assert.IsTrue(target.ModelState.TryGetValue("password", out modelState));
            Assert.AreEqual(modelState.Errors.Count, 1);

            enumerator = modelState.Errors.GetEnumerator();
            enumerator.MoveNext();

            Assert.AreEqual(enumerator.Current.ErrorMessage, Resources.Resources.PasswordInvalid);
        }

        /// <summary>
        /// A test for Create (empty username).
        /// </summary>
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("%PathToWebRoot%\\WebSignUpDemo", "/")]
        [UrlToTest("http://localhost:2169/")]
        public void CreateTest3()
        {
            HomeController target = new HomeController();
            string username = string.Empty;
            string password = "admin_password";
            string passwordConfirmation = "admin_password";
            ActionResult actual;
            actual = target.Create(username, password, passwordConfirmation);

            ModelState modelState;
            IEnumerator<ModelError> enumerator;

            //// empty username.
            Assert.IsTrue(target.ModelState.TryGetValue("username", out modelState));
            Assert.AreEqual(modelState.Errors.Count, 1);

            enumerator = modelState.Errors.GetEnumerator();
            enumerator.MoveNext();

            Assert.AreEqual(enumerator.Current.ErrorMessage, Resources.Resources.MustGiveName);
        }

        /// <summary>
        /// A test for Create (case: Session is not null)
        /// </summary>
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("%PathToWebRoot%\\WebSignUpDemo", "/")]
        [UrlToTest("http://localhost:2169/")]
        public void CreateTest4()
        {
            HomeController_Accessor target = new HomeController_Accessor();
            HomeController controller = (HomeController)target.Target;

            SessionStateItemCollection sessionItems = new SessionStateItemCollection();
            sessionItems[HomeController_Accessor.AdminClientSessionKey] = null;
            controller.ControllerContext = new FakeControllerContext(controller, sessionItems);

            HomeController_Accessor.deiServer = "localhost:21248";
            HomeController_Accessor.username = "admin";
            HomeController_Accessor.password = "admin_password";
            HomeController_Accessor.sslConnection = false;

            string username = "user";
            string password = "user_password";
            string passwordConfirmation = "user_password";

            string expected = Resources.Resources.DeiError_ConnectionError;
            ActionResult actual;

            actual = target.Create(username, password, passwordConfirmation);
            Assert.IsNotNull((ViewResult)actual);
            Assert.AreEqual(expected, ((ViewResult)actual).ViewData["Message"]);
        }

        /// <summary>
        /// A test for Create
        /// </summary>
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("%PathToWebRoot%\\WebSignUpDemo", "/")]
        [UrlToTest("http://localhost:2169/")]
        public void CreateTest()
        {
            HomeController target = new HomeController(); 
            ActionResult actual;
            actual = target.Create();
            Assert.IsNotNull(actual);
        }

        /// <summary>
        /// A test for ChangePassword
        /// </summary>
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("%PathToWebRoot%\\WebSignUpDemo", "/")]
        [UrlToTest("http://localhost:2169/")]
        public void ChangePasswordTest1()
        {
            HomeController target = new HomeController(); 
            string username = "admin";
            string oldPassword = "admin_password"; 
            string newPassword = "admin_test"; 
            string passwordConfirmation = "admin_test"; 

            ActionResult actual;
            actual = target.ChangePassword(username, oldPassword, newPassword, passwordConfirmation);
            
            ModelState modelState;
            Assert.IsTrue(target.ModelState.TryGetValue("all", out modelState));
        }

        /// <summary>
        /// A test for ChangePassword (password mismatch)
        /// </summary>
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("%PathToWebRoot%\\WebSignUpDemo", "/")]
        [UrlToTest("http://localhost:2169/")]
        public void ChangePasswordTest2()
        {
            HomeController target = new HomeController();
            string username = "admin";
            string oldPassword = "admin_password";
            string newPassword = "admin_password";
            string passwordConfirmation = "admin_test";
            ActionResult actual;
            actual = target.ChangePassword(username, oldPassword, newPassword, passwordConfirmation);
      
            ModelState modelState;
            Assert.IsTrue(target.ModelState.TryGetValue("all", out modelState));
            Assert.AreEqual(modelState.Errors.Count, 1);

            IEnumerator<ModelError> enumerator = modelState.Errors.GetEnumerator();
            enumerator.MoveNext();

            Assert.AreEqual(enumerator.Current.ErrorMessage, Resources.Resources.PasswordMismatch);
        }

        /// <summary>
        /// A test for ChangePassword (empty username)
        /// </summary>
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("%PathToWebRoot%\\WebSignUpDemo", "/")]
        [UrlToTest("http://localhost:2169/")]
        public void ChangePasswordTest3()
        {
            HomeController target = new HomeController();
            string username = string.Empty;
            string oldPassword = "admin_password";
            string newPassword = "admin_password";
            string passwordConfirmation = "admin_test";
            
            ActionResult actual;
            actual = target.ChangePassword(username, oldPassword, newPassword, passwordConfirmation);

            ModelState modelState;
            Assert.IsTrue(target.ModelState.TryGetValue("username", out modelState));
            Assert.AreEqual(modelState.Errors.Count, 1);

            IEnumerator<ModelError> enumerator = modelState.Errors.GetEnumerator();
            enumerator.MoveNext();

            Assert.AreEqual(enumerator.Current.ErrorMessage, Resources.Resources.MustGiveName);
        }

        /// <summary>
        /// A test for ChangePassword (empty oldpassword and new password)
        /// </summary>
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("%PathToWebRoot%\\WebSignUpDemo", "/")]
        [UrlToTest("http://localhost:2169/")]
        public void ChangePasswordTest4()
        {
            HomeController target = new HomeController();
            string username = "admin";
            string oldPassword = string.Empty;
            string newPassword = string.Empty;
            string passwordConfirmation = "admin_test";

            ActionResult actual;
            actual = target.ChangePassword(username, oldPassword, newPassword, passwordConfirmation);

            ModelState modelState;
            IEnumerator<ModelError> enumerator;

            Assert.IsTrue(target.ModelState.TryGetValue("oldPassword", out modelState));
            Assert.AreEqual(modelState.Errors.Count, 1);

            enumerator = modelState.Errors.GetEnumerator();
            enumerator.MoveNext();

            Assert.AreEqual(enumerator.Current.ErrorMessage, Resources.Resources.PasswordInvalid);

            Assert.IsTrue(target.ModelState.TryGetValue("newPassword", out modelState));
            Assert.AreEqual(modelState.Errors.Count, 1);

            enumerator = modelState.Errors.GetEnumerator();
            enumerator.MoveNext();

            Assert.AreEqual(enumerator.Current.ErrorMessage, Resources.Resources.PasswordInvalid);

            Assert.IsTrue(target.ModelState.TryGetValue("all", out modelState));
            Assert.AreEqual(modelState.Errors.Count, 1);

            enumerator = modelState.Errors.GetEnumerator();
            enumerator.MoveNext();

            Assert.AreEqual(enumerator.Current.ErrorMessage, Resources.Resources.PasswordMismatch);
        }

        /// <summary>
        /// A test for ChangePassword
        /// </summary>
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("%PathToWebRoot%\\WebSignUpDemo", "/")]
        [UrlToTest("http://localhost:2169/")]
        public void ChangePasswordTest()
        {
            HomeController target = new HomeController(); 
            ActionResult actual;
            actual = target.ChangePassword();
            Assert.IsNotNull(actual);
        }

        /// <summary>
        /// A test for About
        /// </summary>
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("%PathToWebRoot%\\WebSignUpDemo", "/")]
        [UrlToTest("http://localhost:2169/")]
        public void AboutTest()
        {
            HomeController target = new HomeController(); 
            ActionResult actual;
            actual = target.About();
            Assert.IsNotNull(actual);
        }

        /// <summary>
        /// A test for HomeController Constructor
        /// </summary>
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("%PathToWebRoot%\\WebSignUpDemo", "/")]
        [UrlToTest("http://localhost:2169/")]
        public void HomeControllerConstructorTest()
        {
            HomeController target = new HomeController();
            Assert.IsNotNull(target);
        }
    }
}
