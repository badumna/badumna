//-----------------------------------------------------------------------
// <copyright file="MvcApplicationTest.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace WebSignUpDemo.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Web;
    using System.Web.Mvc;
    using System.Web.Routing;
    using WebSignUpDemo;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Microsoft.VisualStudio.TestTools.UnitTesting.Web;
    using MvcFakes;
    
    /// <summary>
    /// This is a test class for MvcApplicationTest and is intended
    /// to contain all MvcApplicationTest Unit Tests
    /// </summary>
    [TestClass()]
    public class MvcApplicationTest
    {
        /// <summary>
        /// Test context.
        /// </summary>
        private TestContext testContextInstance;

        /// <summary>
        /// Gets or sets the test context which provides
        /// information about and functionality for the current test run.
        /// </summary>
        public TestContext TestContext
        {
            get
            {
                return this.testContextInstance;
            }

            set
            {
                this.testContextInstance = value;
            }
        }

        #region Additional test attributes
        //// 
        ////You can use the following additional attributes as you write your tests:
        ////
        ////Use ClassInitialize to run code before running the first test in the class
        ////[ClassInitialize()]
        ////public static void MyClassInitialize(TestContext testContext)
        ////{
        ////}
        ////
        ////Use ClassCleanup to run code after all tests in a class have run
        ////[ClassCleanup()]
        ////public static void MyClassCleanup()
        ////{
        ////}
        ////
        ////Use TestInitialize to run code before running each test
        ////[TestInitialize()]
        ////public void MyTestInitialize()
        ////{
        ////}
        ////
        ////Use TestCleanup to run code after each test has run
        ////[TestCleanup()]
        ////public void MyTestCleanup()
        ////{
        ////}
        ////
        #endregion

        /// <summary>
        /// A test for RegisterRoutes
        /// </summary>
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("%PathToWebRoot%\\WebSignUpDemo", "/")]
        [UrlToTest("http://localhost:2169/")]
        public void RegisterRoutesTest()
        {
            RouteCollection routes = new RouteCollection(); // TODO: Initialize to an appropriate value
            MvcApplication.RegisterRoutes(routes);
            Assert.IsTrue(routes.Count > 0);
        }

        /// <summary>
        /// A test for Application_Start
        /// </summary>
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("%PathToWebRoot%\\WebSignUpDemo", "/")]
        [UrlToTest("http://localhost:2169/")]
        [DeploymentItem("WebSignUpDemo.dll")]
        public void Application_StartTest()
        {
            MvcApplication_Accessor target = new MvcApplication_Accessor();
            RouteTable.Routes.Clear();
            target.Application_Start();
            Assert.IsTrue(RouteTable.Routes.Count > 0);
        }

        /// <summary>
        /// A test for MvcApplication Constructor
        /// </summary>
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("%PathToWebRoot%\\WebSignUpDemo", "/")]
        [UrlToTest("http://localhost:2169/")]
        public void MvcApplicationConstructorTest()
        {
            MvcApplication target = new MvcApplication();
            Assert.IsNotNull(target);
        }
    }
}
