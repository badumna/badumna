﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.IO;

namespace MvcFakes
{
    public class FakeHttpResponse : HttpResponseBase
    {
        private TextWriter output;

        public FakeHttpResponse()
        {
            this.output = new StringWriter();
        }

        public override TextWriter Output
        {
            get
            {
                return this.output;
            }
        }

        public override void Clear()
        {
            this.output.Flush();
        }

        public override void Write(string s)
        {
            this.output.Write(s);
        }

        public override void End()
        {
        }

        public override void AddHeader(string name, string value)
        {
        }

    }
}
