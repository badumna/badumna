﻿//-----------------------------------------------------------------------
// <copyright file="ICertificateCreator.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2010 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace X509CertificateCreator
{
    using System;

    /// <summary>
    /// Interface for creation of self-signed X.509 certificates.
    /// </summary>
    public interface ICertificateCreator
    {
        /// <summary>
        /// Create a self-signed X.509 certificate.
        /// </summary>
        /// <param name="commonName">The common name to store in teh certificate.</param>
        /// <param name="expiryDate">The expiry date for teh certificate.</param>
        /// <returns>The newly created X.509 certificate.</returns>
        System.Security.Cryptography.X509Certificates.X509Certificate2 CreateCertificate(string commonName, DateTime expiryDate);

        /// <summary>
        /// Create a self-signed X.509 certificate, and save it in a password-protected PKCS#12 file.
        /// </summary>
        /// <param name="commonName">The common name to store in teh certificate.</param>
        /// <param name="expiryDate">The expiry date for teh certificate.</param>
        /// <param name="fileName">The name of the file to save the certificate in.</param>
        /// <param name="password">The password to use.</param>
        void CreateCertificate(string commonName, DateTime expiryDate, string fileName, string password);
    }
}
