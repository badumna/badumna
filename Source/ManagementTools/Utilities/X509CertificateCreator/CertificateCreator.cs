//-----------------------------------------------------------------------
// <copyright file="CertificateCreator.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace X509CertificateCreator
{
    using System;
    using System.Collections;
    using System.Globalization;
    using System.Security.Cryptography;
    using System.Security.Cryptography.X509Certificates;
    using Mono.Security.X509;
    using Mono.Security.X509.Extensions;

    /// <summary>
    /// A utility class for creating self-signed X.509 certificates.
    /// </summary>
    public class CertificateCreator : ICertificateCreator
    {
        #region Public methods

        /// <summary>
        /// Create a self-signed X.509 certificate and store it in a password-protected PKCS#12 file.
        /// </summary>
        /// <param name="commonName">The common name (i.e. the hostname of the machine the certificate is for).</param>
        /// <param name="expiryDate">The expiry date for the certificate.</param>
        /// <param name="fileName">The name of the file to save the certificate in.</param>
        /// <param name="password">The password to use.</param>
        public void CreateCertificate(string commonName, DateTime expiryDate, string fileName, string password)
        {
            PKCS12 pkcs12 = CreateCertificate(commonName, expiryDate, password);
            pkcs12.SaveToFile(fileName);
        }

        /// <summary>
        /// Create a self-signed X509 certificate.
        /// </summary>
        /// <param name="commonName">The common name (i.e. the hostname of the machine the certificate is for).</param>
        /// <param name="expiryDate">The expiry date for the certificate.</param>
        /// <returns>The new self-signed certificate.</returns>
        public X509Certificate2 CreateCertificate(string commonName, DateTime expiryDate)
        {
            PKCS12 pkcs12 = CreateCertificate(commonName, expiryDate, "password");
            return new X509Certificate2(pkcs12.GetBytes(), "password");
        }

        #endregion // Public methods

        #region Private methods

        /// <summary>
        /// Create a self-signed X509 certificate.
        /// </summary>
        /// <param name="commonName">The common name (i.e. the hostname of the machine the certificate is for).</param>
        /// <param name="expiryDate">The expiry date for the certificate.</param>
        /// <param name="password">The password that is used for the certificate.</param>
        /// <returns>A PKCS12 object containing a new self-signed certificate.</returns>
        private static PKCS12 CreateCertificate(string commonName, DateTime expiryDate, string password)
        {
            string name = string.Format(CultureInfo.InvariantCulture, "CN={0}", commonName);
            RSA key = RSA.Create();
            X509CertificateBuilder certificateBuilder = new X509CertificateBuilder();
            certificateBuilder.SerialNumber = Guid.NewGuid().ToByteArray();
            certificateBuilder.NotBefore = DateTime.Now;
            certificateBuilder.NotAfter = expiryDate;
            certificateBuilder.IssuerName = name;
            certificateBuilder.SubjectName = name;
            certificateBuilder.SubjectPublicKey = key;
            
            //// Add the extended key usage extension when creating a self signed certificate
            //// Add the key purpose with oid value "1.3.6.1.5.5.7.3.1" indicates that a certificate can be 
            ////    used as an SSL server certificate.
            //// Refer to http://www.alvestrand.no/objectid/1.3.6.1.5.5.7.3.1.html for more information.
            //// Also, mono 1.2.5 required this key purpose for validating the certificate usage.

            ExtendedKeyUsageExtension extendedKeyUsageExtension = new ExtendedKeyUsageExtension();
            extendedKeyUsageExtension.KeyPurpose.Add("1.3.6.1.5.5.7.3.1");
            certificateBuilder.Extensions.Add(extendedKeyUsageExtension);

            byte[] rawCertificate = certificateBuilder.Sign(key);
            Mono.Security.X509.X509Certificate certificate = new Mono.Security.X509.X509Certificate(rawCertificate);
            PKCS12 pkcs12 = new PKCS12();
            pkcs12.Password = password;
            ArrayList magic = new ArrayList() { new byte[4] { 1, 0, 0, 0 } };
            Hashtable attributes = new Hashtable() { { PKCS9.localKeyId, magic } };
            pkcs12.AddCertificate(certificate, attributes);
            pkcs12.AddPkcs8ShroudedKeyBag(key, attributes);
            return pkcs12;
        }

        #endregion // Private methods
    }
}
