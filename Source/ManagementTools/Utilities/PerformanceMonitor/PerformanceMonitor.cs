﻿//-----------------------------------------------------------------------
// <copyright file="PerformanceMonitor.cs" company="Scalify">
//     Copyright (c) 2012 Scalify. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Utilities
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Runtime.InteropServices;
    using System.Text;

    /// <summary>
    /// PerformanceMonitor class is used to monitor the cpu load and available memory on the 
    /// Windows machine (currently it's supported windows machine only)
    /// </summary>
    public class PerformanceMonitor
    {
        [DllImport("libc")]
        static extern int uname(IntPtr buf);

        private PerformanceCounter mCPUCounter;
        private PerformanceCounter mRAMCounter;
        private ProcessStartInfo mProcessStartInfo;
        private OperatingSystem os;

        public PerformanceMonitor(OperatingSystem operatingSystem)
        {
            this.os = operatingSystem;

            if (!this.os.Platform.Equals(PlatformID.Unix))
            {
                this.mCPUCounter = new PerformanceCounter();
                this.mCPUCounter.CategoryName = "Processor";
                this.mCPUCounter.CounterName = "% Processor Time";
                this.mCPUCounter.InstanceName = "_Total";

                this.mRAMCounter = new PerformanceCounter("Memory", "Available MBytes");
            }
            else
            {
                this.mProcessStartInfo = new ProcessStartInfo();
                this.mProcessStartInfo.RedirectStandardOutput = true;
                this.mProcessStartInfo.CreateNoWindow = true;
                this.mProcessStartInfo.UseShellExecute = false;
            }
        }

        public string[] GetMachineStatus()
        {
            string[] status = new string[2];
            if (!this.os.Platform.Equals(PlatformID.Unix))
            {
                status[0] = string.Format("{0:0.0} %", this.mCPUCounter.NextValue());
                status[1] = string.Format("{0:0.0} MB", this.mRAMCounter.NextValue());
            }
            else if (this.IsRunningOnMac())
            {
                status = this.GetMacOSXMachineStatus();
            }
            else
            {
                status = this.GetUnixMachineStatus();
            }

            return status;
        }

        private string[] GetUnixMachineStatus()
        {
            string[] status = new string[2];

            // extract the available memory
            string[] lines = File.ReadAllLines("/proc/meminfo");
            string line = string.Empty;

            for (int i = 0; i < lines.Length; i++)
            {
                if (lines[i].Contains("MemFree"))
                {
                    line = lines[i];
                    break;
                }
            }

            float memFloat = float.Parse(line.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries)[1]);
            status[1] = string.Format("{0:0.0} MB", Math.Floor(memFloat / 1024));

            lines = System.IO.File.ReadAllLines("/proc/loadavg");

            // extract the cpu load average in the last 1 mins
            // processOutput has the following format
            //
            // 0.20 0.18 0.12 1/80 11206
            // The first three columns measure CPU and IO utilization of 
            // the last one, five, and 15 minute periods. The fourth column 
            // shows the number of currently running processes and the total 
            // number of processes. The last column displays the last process ID used.
            string[] columns = lines[0].Split(' ');
            status[0] = string.Format("{0:0.0} %", float.Parse(columns[0]));

#if DEBUG
            Console.WriteLine("CPU Load: " + status[0]);
            Console.WriteLine("Memory available: " + status[1]);
#endif
            return status;
        }

        private string[] GetMacOSXMachineStatus()
        {
            string[] status = new string[2];
            int pageSize;
            StreamReader reader;

            Process process = new Process();
            process.StartInfo = this.mProcessStartInfo;
            process.StartInfo.FileName = "sysctl";
            process.StartInfo.Arguments = "vm.loadavg hw.pagesize";

            process.Start();
            process.WaitForExit();

            reader = process.StandardOutput;
            string processOutput = reader.ReadToEnd();

            string[] lines = processOutput.Split('\n');

            // extract the cpu load average in the last 1 mins
            // and the page size in bytes.
            // vm.loadavg: { 0.07 0.04 0.06 }
            // hw.pagesize: 4096
            status[0] = string.Format("{0:0.0} %", float.Parse(lines[0].Split(' ')[2]));
            pageSize = int.Parse(lines[1].Split(' ')[1]);

            // extract the availble memory
            process.StartInfo.FileName = "vm_stat";
            process.StartInfo.Arguments = "";

            process.Start();
            process.WaitForExit();

            reader = process.StandardOutput;
            processOutput = reader.ReadToEnd();
            process.Close();

            lines = processOutput.Split('\n');
            float memsize = 0.0f;

            // Mach Virtual Memory Statistics: (page size of 4096 bytes)
            // Pages free:                         420032.
            // Pages active:                       141726.
            // Pages inactive:                     133013.
            // Pages speculative:                  199608.
            // Pages wired down:                    88585.
            // "Translation faults":             28139128.
            // Pages copy-on-write:               1072150.
            // Pages zero filled:                14227538.
            // Pages reactivated:                       0.
            // Pageins:                             76185.
            // Pageouts:                                0.
            // Object cache: 14 hits of 47675 lookups (0% hit rate)
            foreach (string ln in lines)
            {
                if (ln.Contains("Pages free") || ln.Contains("Pages speculative"))
                {
                    memsize += float.Parse(ln.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries)[2]);
                }
            }

            float freemem = memsize * pageSize / 1024 / 1024;
            status[1] = string.Format("{0:0.0} MB", Math.Floor(freemem));

#if DEBUG
            Console.WriteLine("CPU Load: " + status[0]);
            Console.WriteLine("Memory available: " + status[1]);
#endif
            return status;
        }

        /// <summary>
        /// IsRunninOnMac method taken from http://github.com/jpobst/Pinta/blob/master/Pinta/Platform.cs
        /// </summary>
        /// <returns>Return true if it is running on mac.</returns>
        private bool IsRunningOnMac()
        {
            IntPtr buf = IntPtr.Zero;
            try
            {
                buf = Marshal.AllocHGlobal(8192);

                // This is a hacktastic way of getting sysname from uname ()
                if (uname(buf) == 0)
                {
                    string os = Marshal.PtrToStringAnsi(buf);
                    if (os == "Darwin")
                        return true;
                }
            }
            catch
            {
            }
            finally
            {
                if (buf != IntPtr.Zero)
                    Marshal.FreeHGlobal(buf);
            }
            return false;
        }
    }
}
