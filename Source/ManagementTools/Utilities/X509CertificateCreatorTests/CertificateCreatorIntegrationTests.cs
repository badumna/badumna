//-----------------------------------------------------------------------
// <copyright file="CertificateCreatorIntegrationTests.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace X509CertificateCreatorTests
{
    using System;
    using System.IO;
    using System.Security.Cryptography.X509Certificates;
    using Mono.Security.X509.Extensions;
    using NUnit.Framework;
    using X509CertificateCreator;

    [TestFixture]
    internal class CertificateCreatorTests
    {
        private string filename;
        private CertificateCreator certificateCreator;

        [SetUp]
        public void SetUp()
        {
            this.certificateCreator = new CertificateCreator();
            this.filename = Guid.NewGuid().ToString("N");
            Assert.IsFalse(File.Exists(this.filename));
        }

        [TearDown]
        public void TearDown()
        {
            this.certificateCreator = null;
            File.Delete(this.filename);
        }

        [Test]
        public void CreateCertificateCreatesCertificateFile()
        {
            this.certificateCreator.CreateCertificate("foo", DateTime.Now, this.filename, "pword");
            Assert.IsTrue(File.Exists(this.filename));
        }

        [Test]
        public void CreateCertificateCreatesValidCertificateFile()
        {
            this.certificateCreator.CreateCertificate("foo", DateTime.Now, this.filename, "pword");
            X509Certificate certificate = new X509Certificate(this.filename, "pword");
        }

        [Test]
        public void CreateCertificateReturnsX509Certificate()
        {
            X509Certificate cert = this.certificateCreator.CreateCertificate("foo", DateTime.Now);
        }

        [Test]
        public void CreateCertificateReturnsWithValidExtension()
        {
            X509Certificate cert = this.certificateCreator.CreateCertificate("foo", DateTime.Now);
            Mono.Security.X509.X509Certificate cert2 = new Mono.Security.X509.X509Certificate(cert.GetRawCertData());
            Mono.Security.X509.X509Extension extension = cert2.Extensions["2.5.29.37"];

            Assert.NotNull(extension);
            ExtendedKeyUsageExtension extendedKeyUsageExtension = new ExtendedKeyUsageExtension(extension);
            
            Assert.NotNull(extendedKeyUsageExtension);
            Assert.IsTrue(extendedKeyUsageExtension.KeyPurpose.Contains("1.3.6.1.5.5.7.3.1"));
        }
    }
}
