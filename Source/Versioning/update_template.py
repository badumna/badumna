#!/usr/bin/env python

import os
import re
import sys
import subprocess

GIT = 'git'

def populate_vars():
	vars = {}
	version_filename = os.path.join('..', '..', 'VERSION')
	with open(version_filename, 'r') as version_file:
		vars['VERSION'] = version_file.readline()
	return vars

def append_git_vars(vars):
	# -- any outstanding changes?
	wc_changes = subprocess.Popen([GIT, "diff-index", "--quiet", "HEAD"]).wait()
	assert wc_changes in (0,1)
	vars['WCMODS'] = bool(wc_changes)

	# -- revision
	vars ['COMMIT_ID'] = subprocess.check_output([GIT, "rev-parse", "HEAD"]).strip()

	# -- build number is the number of ancestor commits that exist for HEAD
	# (this will be an increasing number as time progresses, at least for a given branch)
	vars ['REVISION'] = int(subprocess.check_output([GIT, "rev-list", "--count", "HEAD"]).strip())

def replace(template_location, output_location, vars):
	import jinja2
	with open(template_location) as template:
		template = jinja2.Template(template.read().decode('UTF-8'))
	template.environment.undefined = jinja2.StrictUndefined

	with open(output_location, 'w') as output:
		output.write(template.render(vars).encode('UTF-8'))

def uses(name):
	lib = os.path.abspath(os.path.join(dirname, '..', '..', 'Third Party', name))
	if os.path.exists(lib):
		sys.path.insert(0, lib)
	else:
		print "Warning: %s not found at %s" % (name, lib,)

if __name__ == '__main__':
	dirname = os.path.dirname(os.path.abspath(__file__))
	os.chdir(dirname)
	uses('jinja2')
	uses('python-which')

	vars = populate_vars()

	# windows git is called "git.cmd", and subprocess.Popen doesn't find that
	from which import which
	GIT = which('git')
	# print "found git at: %s" % (GIT,)
	append_git_vars(vars)
	print repr(vars)
	replace('VersionInfo.tmpl', 'VersionInfo.cs', vars)
