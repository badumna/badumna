//
//  BadumnaDemoSprite.h
//  BadumnaDemo
//
//  Copyright (c) 2012 Scalify. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <GLKit/GLKit.h>
#include "RGBColour.h"

@interface BadumnaDemoSprite : NSObject

@property (assign) GLKVector2 position;
@property (assign) CGSize contentSize;

- (id)initWithFile:(NSString *)fileName effect:(GLKBaseEffect *)effect;
- (void)render:(RGBColour) colour;

@end
