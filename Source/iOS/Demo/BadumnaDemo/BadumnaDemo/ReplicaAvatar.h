//
//  ReplicaAvatar.h
//  BadumnaDemo
//
//  Copyright (c) 2012 Scalify. All rights reserved.
//

#ifndef REPLICA_AVATAR_H
#define REPLICA_AVATAR_H

#include "Avatar.h"
#include "Badumna/DataTypes/BadumnaId.h"
#include "Badumna/DataTypes/Vector3.h"
#include "Badumna/Replication/ISpatialReplica.h"
#include "Badumna/Replication/SpatialEntityStateSegment.h"

using namespace BadumnaDemo;

namespace BadumnaDemo
{
    class ReplicaAvatar : public Badumna::ISpatialReplica, public Avatar
    {
    public:
        ReplicaAvatar();
        
        virtual float GetX() const;
        virtual float GetY() const;
        virtual RGBColour GetColour() const;

        //
        // IEntity properties
        //
        Badumna::BadumnaId Guid() const;
        void SetGuid(Badumna::BadumnaId const &id);
        void HandleEvent(Badumna::InputStream *stream);
        
        //
        // ISpatialEntity properties
        //
        Badumna::Vector3 Position() const;
        void SetPosition(Badumna::Vector3 const &position);
        float Radius() const;
        void SetRadius(float val);
        float AreaOfInterestRadius() const;
        void SetAreaOfInterestRadius(float val);
        
        void Deserialize(
                         Badumna::BooleanArray const &included_part,
                         Badumna::InputStream *stream,
                         int estimated_milliseconds_since_departure);
                
    private:
        Badumna::Vector3 position_;
        Badumna::BadumnaId guid_;
        float radius_;
        float area_of_interest_radius_;
        
        
        // disallow copy and assign
        ReplicaAvatar(ReplicaAvatar const &other);
        ReplicaAvatar &operator= (ReplicaAvatar const &rhs);
    };
}

#endif //REPLICA_AVATAR_H
