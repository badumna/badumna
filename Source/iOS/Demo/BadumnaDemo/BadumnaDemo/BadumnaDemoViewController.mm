//
//  BadumnaDemoViewController.m
//  BadumnaDemo
//
//  Copyright (c) 2012 Scalify. All rights reserved.
//

#import "BadumnaDemoViewController.h"
#import "BadumnaDemoSprite.h"
#include "ReplicationManager.h"
#include "Badumna/Core/RuntimeInitializer.h"

@interface BadumnaDemoViewController ()
@property (strong, nonatomic) EAGLContext *context;
@property (strong) GLKBaseEffect * effect;
@property (strong) BadumnaDemoSprite * avatarSprite;
@property BadumnaDemo::ReplicationManager *replicationManager;
@end

@implementation BadumnaDemoViewController

@synthesize context = _context;
@synthesize avatarSprite = _avatarSprite;
@synthesize replicationManager = _replicationManager;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.context = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2];
    
    if (!self.context) {
        NSLog(@"Failed to create ES context");
    }
    
    // Set up view and graphics.
    GLKView *view = (GLKView *)self.view;
    view.context = self.context;
    [EAGLContext setCurrentContext:self.context];
    self.effect = [[GLKBaseEffect alloc] init];
    GLKMatrix4 projectionMatrix = GLKMatrix4MakeOrtho(0, 320, 0, 480, -1024, 1024);
    self.effect.transform.projectionMatrix = projectionMatrix;
    self.avatarSprite = [[BadumnaDemoSprite alloc] initWithFile:@"avatar.png" effect:self.effect];
    //    self.avatarSprite.position = GLKVector2Make(100, 100);
    
    // Set up gesture recognizers for handling input.
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapFrom:)];
    [self.view addGestureRecognizer:tapRecognizer];
    UITapGestureRecognizer *dualTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapFrom:)];
    dualTapRecognizer.numberOfTouchesRequired = 2;
    [self.view addGestureRecognizer:dualTapRecognizer];
    
    // Initialize Badumna runtime.
    Badumna::InitializeRuntime();

    // Create and initialize the replication manager.
    self.replicationManager = new BadumnaDemo::ReplicationManager();
    self.replicationManager->Initialize();
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

#pragma mark - GLKViewDelegate

- (void)glkView:(GLKView *)view drawInRect:(CGRect)rect {
    
    glClearColor(100.0/255, 149.0/255, 237.0/255, 1.0);
    glClear(GL_COLOR_BUFFER_BIT);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_BLEND);
    
    // Render the original avatar.
    self.avatarSprite.position = GLKVector2Make(self.replicationManager->GetOriginalAvatar()->GetX(), self.replicationManager->GetOriginalAvatar()->GetY());
    [self.avatarSprite render: self.replicationManager->GetOriginalAvatar()->GetColour()];
    
    // Render any replica avatars.
    std::vector<ReplicaAvatar*> replicas = self.replicationManager->GetReplicaAvatars();
    for(std::vector<ReplicaAvatar*>::reverse_iterator it = replicas.rbegin(); it != replicas.rend(); ++it) {
        self.avatarSprite.position = GLKVector2Make((*it)->GetX(), (*it)->GetY());
        [self.avatarSprite render: (*it)->GetColour()];
    }
}

- (void)update {
    self.replicationManager->Update((double)self.timeSinceLastUpdate);
}

- (void)handleTapFrom:(UITapGestureRecognizer *)recognizer {
    
    // A two-fingered tap will change the original avatar's colour.
    if ([recognizer numberOfTouches] > 1)
    {
        self.replicationManager->GetOriginalAvatar()->ChangeColour();
    }
    else
    {
        // A single-fingered tap will tell the original avatar to move to the tap location.
        CGPoint touchLocation = [recognizer locationInView:recognizer.view];
        touchLocation = CGPointMake(touchLocation.x, self.view.frame.size.height - touchLocation.y);
        
        self.replicationManager->GetOriginalAvatar()->SetTarget(touchLocation.x, touchLocation.y);
    }
}

- (void)dealloc {
    self.replicationManager->Shutdown();
    delete self.replicationManager;
    self.replicationManager = NULL;
}

@synthesize effect;
@end
