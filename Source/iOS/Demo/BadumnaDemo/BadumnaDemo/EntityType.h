//
//  EntityType.h
//  BadumnaDemo
//
//  Copyright (c) 2012 Scalify. All rights reserved.
//

#ifndef ENTITY_TYPE_H
#define ENTITY_TYPE_H

namespace BadumnaDemo
{
    enum EntityType
    {
        NormalEntity = 100,
        DeadRecknonableEntity = 101
    };
}

#endif // ENTITY_TYPE_H