//
//  Avatar.h
//  BadumnaDemo
//
//  Copyright (c) 2012 Scalify. All rights reserved.
//

#ifndef AVATAR_H
#define AVATAR_H

#include "RGBColour.h"

namespace BadumnaDemo
{
    class Avatar
    {
    public:
        virtual float GetX() const = 0;
        virtual float GetY() const = 0;
        virtual RGBColour GetColour() const = 0;
    protected:
        RGBColour colour_ = { 255, 128, 0};
    };
}

#endif //AVATAR_H
