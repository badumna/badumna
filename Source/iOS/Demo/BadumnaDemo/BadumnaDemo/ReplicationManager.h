//
//  ReplicationManager.h
//  BadumnaDemo
//
//  Copyright (c) 2012 Scalify. All rights reserved.
//

#ifndef REPLICATION_MANAGER_H
#define REPLICATION_MANAGER_H

#include "OriginalAvatar.h"
#include "ReplicaAvatar.h"

namespace BadumnaDemo
{
    class ReplicationManager
    {
    public:
        ReplicationManager();
        
        void Initialize();
        void Update(double elapsedSeconds);
        void Shutdown();
        OriginalAvatar *GetOriginalAvatar();
        std::vector<ReplicaAvatar *> GetReplicaAvatars();

        // replication delegates
        Badumna::ISpatialReplica *CreateSpatialReplica(
                                                       Badumna::NetworkScene const &scene,
                                                       Badumna::BadumnaId const &id,
                                                       uint32_t type);
        
        void RemoveSpatialReplica(Badumna::NetworkScene const &scene, Badumna::ISpatialReplica const &replica); 

    private:
        std::auto_ptr<Badumna::NetworkFacade> facade_;
        std::auto_ptr<OriginalAvatar> originalAvatar_;
        std::vector<ReplicaAvatar *> replicaAvatars_;
        std::auto_ptr<Badumna::NetworkScene> scene_;
        
        double secondsSinceLastUpdate_
        ;
        // disallow copy and assign
        ReplicationManager(ReplicationManager const &other);
        ReplicationManager &operator=(ReplicationManager const &rhs);
    };
}

#endif //REPLICATION_MANAGER_H
