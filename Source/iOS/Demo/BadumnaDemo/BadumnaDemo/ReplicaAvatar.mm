//
//  ReplicaAvatar.mm
//  BadumnaDemo
//
//  Copyright (c) 2012 Scalify. All rights reserved.
//

#include "ReplicaAvatar.h"
#include "Badumna/Replication/SpatialEntityStateSegment.h"
#include "StateSegment.h"

namespace BadumnaDemo
{
    using Badumna::BadumnaId;
    using Badumna::InputStream;
    using Badumna::Vector3;
    using Badumna::BooleanArray;
    
    ReplicaAvatar::ReplicaAvatar()
    : Avatar(),
    position_(0.0f, 0.0f, 0.0f),
    guid_(BadumnaId::None()),
    radius_(0.0f),
    area_of_interest_radius_(0.0f)
    {
    }

    float ReplicaAvatar::GetX() const
    {
        return position_.GetX();
    }

    float ReplicaAvatar::GetY() const
    {
        return position_.GetY();
    }

    RGBColour ReplicaAvatar::GetColour() const
    {
        return colour_;
    }

    BadumnaId ReplicaAvatar::Guid() const
    {
        return guid_;
    }
    
    void ReplicaAvatar::SetGuid(BadumnaId const &id)
    {
        guid_ = id;
    }
    
    void ReplicaAvatar::HandleEvent(InputStream *)
    {
    }
    
    Vector3 ReplicaAvatar::Position() const
    {
        return position_;
    }
    
    void ReplicaAvatar::SetPosition(Vector3 const &value)
    {
        position_ = value;
    }
    
    float ReplicaAvatar::Radius() const
    {
        return radius_;
    }
    
    void ReplicaAvatar::SetRadius(float value)
    {
        radius_ = value;
    }
    
    float ReplicaAvatar::AreaOfInterestRadius() const
    {
        return area_of_interest_radius_;
    }
    
    void ReplicaAvatar::SetAreaOfInterestRadius(float value)
    {
        area_of_interest_radius_ = value;
    }
    
    void ReplicaAvatar::Deserialize(
                                    BooleanArray const &included_parts,
                                    InputStream *stream,
                                    int /*estimated_milliseconds_since_departure*/)
    {
        // Deserialize data for the colour property if it is included.
        if(included_parts[Colour])
        {
            int r, g, b;
            (*stream) >> r;
            (*stream) >> g;
            (*stream) >> b;
            
            colour_.red = r;
            colour_.green = g;
            colour_.blue = b;
        }
    }}