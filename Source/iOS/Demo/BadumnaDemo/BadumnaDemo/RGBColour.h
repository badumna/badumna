//
//  RGBColour.h
//  BadumnaDemo
//
//  Copyright (c) 2012 Scalify. All rights reserved.
//

#ifndef RGBCOLOUR_H
#define RGBCOLOUR_H

struct RGBColour
{
    int red;
    int green;
    int blue;
};

#endif //RGBCOLOUR_H
