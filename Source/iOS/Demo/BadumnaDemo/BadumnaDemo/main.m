//
//  main.m
//  BadumnaDemo
//
//  Created by Geoff Battye on 21/09/12.
//  Copyright (c) 2012 Scalify. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BadumnaDemoAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([BadumnaDemoAppDelegate class]));
    }
}
