//
//  OriginalAvatar.h
//  BadumnaDemo
//
//  Copyright (c) 2012 Scalify. All rights reserved.
//

#ifndef ORIGINAL_AVATAR_H
#define ORIGINAL_AVATAR_H

#include "Avatar.h"
#include "Badumna/Core/NetworkFacade.h"
#include "Badumna/DataTypes/BadumnaId.h"
#include "Badumna/DataTypes/Vector3.h"
#include "Badumna/Replication/ISpatialOriginal.h"
#include "Badumna/Replication/SpatialEntityStateSegment.h"

namespace BadumnaDemo
{
    class OriginalAvatar : public Avatar, public Badumna::ISpatialOriginal
    {
    public:
        OriginalAvatar(
                       Badumna::NetworkFacade *facade,
                       Badumna::Vector3 initialPosition,
                       float radius,
                       float area_of_interest_radius);
        void Update(double elapsedSeconds);
        void SetTarget(float x, float y);
        void ChangeColour();
        virtual float GetX() const;
        virtual float GetY() const;
        virtual RGBColour GetColour() const;

        //
        // IEntity properties
        //
        Badumna::BadumnaId Guid() const;
        void SetGuid(Badumna::BadumnaId const &id);
        void HandleEvent(Badumna::InputStream *stream);
        
        //
        // ISpatialEntity properties
        //
        Badumna::Vector3 Position() const;
        void SetPosition(Badumna::Vector3 const &position);
        void SetPosition(float x, float y);
        float Radius() const;
        void SetRadius(float val);
        float AreaOfInterestRadius() const;
        void SetAreaOfInterestRadius(float val);
        
        //
        // ISpatialOriginal properties
        //
        void Serialize(Badumna::BooleanArray const &required_parts, Badumna::OutputStream *stream);

        static const int max_speed = 50; // pixels per second.

    private:
        Badumna::NetworkFacade *facade_;
        Badumna::BadumnaId guid_;
        Badumna::Vector3 position_;
        Badumna::Vector3 target_;
        float radius_;
        float area_of_interest_radius_;
                
        // disallow copy and assign
        OriginalAvatar(OriginalAvatar const &other);
        OriginalAvatar& operator=(OriginalAvatar const &rhs);
    };
}

#endif //ORIGINAL_AVATAR_H
