//
//  OriginalAvatar.mm
//  BadumnaDemo
//
//  Copyright (c) 2012 Scalify. All rights reserved.
//

#include "OriginalAvatar.h"
#include "StateSegment.h"
#include "Badumna/Core/NetworkFacade.h"
#include "Badumna/DataTypes/BadumnaId.h"
#include "Badumna/Replication/SpatialEntityStateSegment.h"

namespace BadumnaDemo
{
    using Badumna::BadumnaId;
    using Badumna::BooleanArray;
    using Badumna::InputStream;
    using Badumna::OutputStream;
    using Badumna::Vector3;

    OriginalAvatar::OriginalAvatar(
                                   Badumna::NetworkFacade *facade,
                                   Vector3 initialPosition,
                                   float radius,
                                   float area_of_interest_radius)
        : Avatar(),
        facade_(facade),
        guid_(BadumnaId::None()),
        position_(initialPosition),
        target_(initialPosition),
        radius_(radius),
        area_of_interest_radius_(area_of_interest_radius)
    {
    }

    float OriginalAvatar::GetX() const
    {
        return position_.GetX();
    }
    
    float OriginalAvatar::GetY() const
    {
        return position_.GetY();
    }
    
    RGBColour OriginalAvatar::GetColour() const
    {
        return colour_;
    }
    
    void OriginalAvatar::Update(double elapsedSeconds)
    {
        if (position_ == target_)
        {
            return;
        }
        
        double maxStep = max_speed * elapsedSeconds;
        
        Vector3 displacement = target_ - position_;
        double magnitude = displacement.Magnitude();
        if (magnitude <= maxStep)
        {
            SetPosition(target_);
        }
        else
        {
            SetPosition(position_ + (displacement * (maxStep / magnitude)));
        }
    }

    void OriginalAvatar::SetTarget(float x, float y)
    {
        target_.SetX(x);
        target_.SetY(y);
    }

    void OriginalAvatar::ChangeColour()
    {
        // Get indexes to RGB channels in random order.
        int channel1 = rand() % 3;
        int channel2 = (channel1 + (rand() & 1) + 1) % 3;
        int channel3 = ((channel1+1) ^ (channel2+1)) - 1;

        // Set one channel to max (255), one to min (0), and one to a random value, to pick a random bright colour.
        int *channels = &colour_.red;
        channels[channel1] = 255;
        channels[channel2] = 0;
        channels[channel3] = (rand() % 255);

        // Notify Badumna that the position property has been updated, it needs to be replicated to replicas.
        facade_->FlagForUpdate(*this, Colour);

    }
    
    BadumnaId OriginalAvatar::Guid() const
    {
        return guid_;
    }

    void OriginalAvatar::SetGuid(BadumnaId const &id)
    {
        guid_ = id;
    }

    void OriginalAvatar::HandleEvent(InputStream *)
    {
    }

    Vector3 OriginalAvatar::Position() const
    {
        return position_;
    }

    void OriginalAvatar::SetPosition(Vector3 const &value)
    {
        position_ = value;
        
        // Notify Badumna that the position property has been updated and needs to be replicated.
        facade_->FlagForUpdate(*this, Badumna::StateSegment_Position);
    }

    void OriginalAvatar::SetPosition(float x, float y)
    {
        Vector3 v(x, y, 0.0f);
        SetPosition(v);
    }

    float OriginalAvatar::Radius() const
    {
        return radius_;
    }

    void OriginalAvatar::SetRadius(float value)
    {
        radius_ = value;
        // Notify Badumna that the radius property has been updated and needs to be replicated.
        facade_->FlagForUpdate(*this, Badumna::StateSegment_Radius);
    }

    float OriginalAvatar::AreaOfInterestRadius() const
    {
        return area_of_interest_radius_;
    }

    void OriginalAvatar::SetAreaOfInterestRadius(float value)
    {
        area_of_interest_radius_ = value;
        // Notify Badumna that the area of interest radius property has been updated and needs to be replicated.
        facade_->FlagForUpdate(*this, Badumna::StateSegment_InterestRadius);
    }

    void OriginalAvatar::Serialize(BooleanArray const &required_parts, OutputStream *stream)
    {
        // Serialize the data for the colour property if required.
        if(required_parts[Colour])
        {
            (*stream) << colour_.red;
            (*stream) << colour_.green;
            (*stream) << colour_.blue;
        }
    }
}
