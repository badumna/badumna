//
//  ReplicationManager.mm
//  BadumnaDemo
//
//  Copyright (c) 2012 Scalify. All rights reserved.
//

#include "ReplicationManager.h"
#include "EntityType.h"

using namespace Badumna;

namespace BadumnaDemo
{
    struct GuidEqualityComparable : public std::binary_function<ISpatialReplica *, BadumnaId, bool>
    {
        bool operator()(ISpatialReplica *replica, BadumnaId const &id) const
        {
            return replica->Guid() == id;
        }
    };

    ReplicationManager::ReplicationManager() :
        secondsSinceLastUpdate_(0)
    {
    }

    void ReplicationManager::Initialize()
    {
        // Use the Options class to configure the badumna peer.
        Options options;
        options.GetConnectivityModule().SetStartPortRange(21300);
        options.GetConnectivityModule().SetEndPortRange(21399);
        options.GetConnectivityModule().SetMaxPortsToTry(3);
        options.GetConnectivityModule().EnableBroadcast();
        options.GetConnectivityModule().SetBroadcastPort(21250);
        
        // Use LAN mode for demo and testing purpose.
        // To use on the Internet, comment out the ConfigureForLan() line and add a known seed peer.  e.g.:
        // options.GetConnectivityModule().AddSeedPeer("seedpeer.example.com:21251");
        options.GetConnectivityModule().ConfigureForLan();
        
        // Set the application name
        options.GetConnectivityModule().SetApplicationName(L"api-example");
        
        // Create the facade
        facade_.reset(NetworkFacade::Create(options));
        
        float radius = 10.0f;
        float interest_radius = 50.0f;
        
        facade_->RegisterEntityDetails(interest_radius, OriginalAvatar::max_speed);
        
        // Log in to the network (production code should test login result for success).
        facade_->Login();

        // Create the local avatar.
        Vector3 initial_position(30.0f, 30.0f, 0.0f);
        originalAvatar_.reset(new OriginalAvatar(facade_.get(), initial_position, radius, interest_radius));

        // Join the badumna network scene.
        scene_.reset(facade_->JoinScene(L"api_example1_scene",
                                        Badumna::CreateSpatialReplicaDelegate(&ReplicationManager::CreateSpatialReplica, *this),
                                        Badumna::RemoveSpatialReplicaDelegate(&ReplicationManager::RemoveSpatialReplica, *this)));
        
        // Register the local avatar with the scene
        scene_->RegisterEntity(originalAvatar_.get(), NormalEntity);
    }

    void ReplicationManager::Update(double elapsedSeconds)
    {
        if (originalAvatar_.get() != NULL)
        {
            originalAvatar_->Update(elapsedSeconds);
        }
        
        if (facade_.get() != NULL && facade_->IsLoggedIn())
        {
            // ProcessNetworkState must be called regularly to allow Badumna to do its work.
            facade_->ProcessNetworkState();            
        }
    }

    void ReplicationManager::Shutdown()
    {
        if(scene_.get() != NULL)
        {
            // Unregister the local entity from the scene, and leave the scene.
            scene_->UnregisterEntity(*originalAvatar_.get());
            scene_->Leave();
        }

        if (facade_.get() != NULL)
        {
            facade_->Shutdown();
        }
    }
    
    OriginalAvatar *ReplicationManager::GetOriginalAvatar()
    {
        return originalAvatar_.get();
    }
    
    std::vector<ReplicaAvatar *> ReplicationManager::GetReplicaAvatars()
    {
        return replicaAvatars_;
    }
    
    // Callback for creating replicas; passed to Badumna when joining a scene.
    ISpatialReplica *ReplicationManager::CreateSpatialReplica(
                                                              NetworkScene const &,
                                                              BadumnaId const &id,
                                                              uint32_t)
    {
        std::vector<ReplicaAvatar*>::iterator iter;
        iter = std::find_if(
                            replicaAvatars_.begin(),
                            replicaAvatars_.end(),
                            std::bind2nd(GuidEqualityComparable(), id));
        if(iter != replicaAvatars_.end())
        {
            return *iter;
        }
        else
        {
            ReplicaAvatar* replica = new ReplicaAvatar();
            replica->SetGuid(id);
            replicaAvatars_.push_back(replica);
            
            return replica;
        }
    }
    
    // Callback for removing replicas; passed to Badumna when joining a scene.
    void ReplicationManager::RemoveSpatialReplica(NetworkScene const &, ISpatialReplica const &replica)
    {
        std::vector<ReplicaAvatar*>::iterator iter;
        iter = std::find_if(
                            replicaAvatars_.begin(),
                            replicaAvatars_.end(),
                            std::bind2nd(GuidEqualityComparable(), replica.Guid()));
        if(iter != replicaAvatars_.end())
        {
            ReplicaAvatar* to_remove = *iter;
            replicaAvatars_.erase(iter);
            delete to_remove;
        }
    }
}
