//
//  StateSegment.h
//  BadumnaDemo
//
//  Copyright (c) 2012 Scalify. All rights reserved.
//

#ifndef STATE_SEGMENT_H
#define STATE_SEGMENT_H

#include "Badumna/Replication/SpatialEntityStateSegment.h"

namespace BadumnaDemo
{
    enum StateSegment
    {
        Colour = Badumna::StateSegment_FirstAvailableSegment,
    };
}

#endif // STATE_SEGMENT_H