//
//  BadumnaDemoAppDelegate.h
//  BadumnaDemo
//
//  Copyright (c) 2012 Scalify. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BadumnaDemoAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
