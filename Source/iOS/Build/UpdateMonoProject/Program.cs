﻿//-----------------------------------------------------------------------
// <copyright file="Program.cs" author="Geoff Battye" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2012 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace UpdateMonoProject
{
    using System;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Xml.Linq;

    /// <summary>
    /// Program for updating MonoDevelop project files from .NET project files.
    /// </summary>
    /// <remarks>See the PrintHelp method for more information.</remarks>
    internal static class Program
    {
        /// <summary>
        /// XML Namespace for the project files.
        /// </summary>
        private static XNamespace xmlns = "http://schemas.microsoft.com/developer/msbuild/2003";
        
        /// <summary>
        /// The Microsoft .NET project file.
        /// </summary>
        private static XElement microsoftProject;

        /// <summary>
        /// The MonoDevelop project file.
        /// </summary>
        private static XElement monoProject;

        /// <summary>
        /// Relative path from mono project file to Microsoft project file.
        /// </summary>
        private static string pathRelation;

        /// <summary>
        /// The entry point for the program.
        /// </summary>
        /// <param name="args">Command line arguments.</param>
        /// <returns>Zero to indicate success, otherwise a non-zero value.</returns>
        public static int Main(string[] args)
        {
            if (args.Length > 0 && new string[] { "-h", "/h", "-?", "/?", "--help" }.Contains(args[0]))
            {
                Program.PrintHelp();
                return 0;
            }
            else if (args.Length < 2 || args.Length > 3)
            {
                return Program.ReportErrorAndReturnExitCode("Error: Invalid number of arguments.");
            }
            else
            {
                string extraDefines = "";
                if (args.Length == 3)
                {
                    extraDefines = args[2];
                }

                return Program.UpdateProject(args[0], args[1], extraDefines);
            }
        }

        /// <summary>
        /// Update a mono csproj file to include links to all files for compilation in a microsoft csproj file.
        /// </summary>
        /// <param name="monoTemplateFile">The mono csproj template file.</param>
        /// <param name="microsoftProjectFile">The microsoft csproj file.</param>
        /// <param name="extraDefines">Extra defines to be added to the mono project file.</param>
        /// <returns>Zero to indicate success, otherwise a non-zero value.</returns>
        public static int UpdateProject(string monoTemplateFile, string microsoftProjectFile, string extraDefines)
        {
            if (Path.GetExtension(monoTemplateFile) != ".tmpl")
            {
                return Program.ReportErrorAndReturnExitCode("Mono template filename must end in '.tmpl'");
            }

            monoTemplateFile = Path.GetFullPath(monoTemplateFile);
            microsoftProjectFile = Path.GetFullPath(microsoftProjectFile);

            Program.pathRelation = Program.FindRelativePath(monoTemplateFile, microsoftProjectFile);
            try
            {
                Program.microsoftProject = XElement.Load(microsoftProjectFile);
            }
            catch (Exception ex)
            {
                return Program.ReportErrorAndReturnExitCode(
                    string.Format(CultureInfo.CurrentCulture, "Error loading xml file {0}: {1}", Program.microsoftProject, ex.Message));
            }

            try
            {
                Program.monoProject = XElement.Load(monoTemplateFile);
            }
            catch (Exception ex)
            {
                return Program.ReportErrorAndReturnExitCode(
                    string.Format(CultureInfo.CurrentCulture, "Error loading xml file {0}: {1}", monoTemplateFile, ex.Message));
            }

            var microsoftCompileNodes = Program.microsoftProject.Descendants().Where(e => e.Name.LocalName == "Compile");
            XElement microsoftCompileNodeParent = microsoftCompileNodes.FirstOrDefault().Parent;
            if (microsoftCompileNodeParent == null)
            {
                return Program.ReportErrorAndReturnExitCode("Error: Could not find group of files to be compiled in MS project file: " + microsoftProjectFile);
            }

            var monoCompileNodes = Program.monoProject.Descendants().Where(e => e.Name.LocalName == "Compile");
            XElement monoCompileNodeParent = monoCompileNodes.FirstOrDefault().Parent;
            if (monoCompileNodeParent == null)
            {
                return Program.ReportErrorAndReturnExitCode("Error: Could not find group of files to be compiled in mono project file: " + monoTemplateFile);
            }

            // Add links to files in microsoft project, if not already present.
            int missingCount = 0;
            foreach (var microsoftNode in microsoftCompileNodes)
            {
                XElement match = Program.monoProject.Descendants().FirstOrDefault(
                    n => (n.Name.LocalName == "Compile" &&
                        Program.PathsEqual(
                            n.Attribute("Include").Value,
                            Program.ToMonoPath(microsoftNode.Attribute("Include").Value))));
                if (match == null)
                {
                    missingCount++;
                    monoCompileNodeParent.Add(Program.CreateLinkNode(microsoftNode));
                }
            }

            Console.WriteLine("Added {0} new links for files in Microsoft project.", missingCount);

            // Add extra defines
            foreach (var node in Program.monoProject.Descendants().Where(n => n.Name.LocalName == "DefineConstants"))
            {
                if (!string.IsNullOrEmpty(node.Value))
                {
                    node.Value += ";";
                }

                node.Value += extraDefines;
            }

            string monoProjectOutputFile = Path.Combine(Path.GetDirectoryName(monoTemplateFile), Path.GetFileNameWithoutExtension(monoTemplateFile));
            try
            {
                Program.monoProject.Save(monoProjectOutputFile);
                Console.WriteLine("Mono project file written to {0}", monoProjectOutputFile);
            }
            catch (Exception ex)
            {
                return Program.ReportErrorAndReturnExitCode(
                    string.Format(CultureInfo.CurrentCulture, "Error creating output file {0}: {1}", monoProjectOutputFile, ex.Message));
            }

            return 0;
        }

        /// <summary>
        /// Finds the relative path from the directory of one file to the directory of another.
        /// </summary>
        /// <param name="toFile">The file whose directory the relative path should be to.</param>
        /// <param name="fromFile">The file whose directoy the relative path should be from.</param>
        /// <returns>A relative path from the directly of the 'from' file to the directory of the 'to' file. </returns>
        private static string FindRelativePath(string toFile, string fromFile)
        {
            Uri toUri = new Uri(toFile);
            Uri fromUri = new Uri(fromFile);
            string pathToToFile = toUri.MakeRelativeUri(fromUri).ToString();
            return Path.GetDirectoryName(pathToToFile);
        }

        /// <summary>
        /// Report an error and exit.
        /// </summary>
        /// <param name="error">The error message.</param>
        /// <returns>A non-zero exit code.</returns>
        private static int ReportErrorAndReturnExitCode(string error)
        {
            Console.WriteLine(error);
            Console.WriteLine("No files were edited.");
            Console.WriteLine("Run \"UpdateProject -h\" for help and usage instructions.");
            return 1;
        }

        /// <summary>
        /// Show usage instructions.
        /// </summary>
        private static void PrintHelp()
        {
            StringBuilder help = new StringBuilder();
            help.AppendLine("This program will update a mono .csproj file to include links to all files specifed in compile elements of a Microsofot .NET .csproj file.");
            help.Append("It assumes that both files already have ItemGroup elements containing at least one Compile element, ");
            help.AppendLine("and that the mono file has an ItemGroup containing at least one Folder element.");
            help.AppendLine();
            help.AppendLine("Usage: UpdateMonoProject <Mono csproj file> <.NET csproj file> [<extra defines>]");
            Console.WriteLine(help);
        }

        /// <summary>
        /// Converts a path to a file relative to the microsoft project to be relative to the mono project.
        /// </summary>
        /// <param name="microsoftPath">The path to a file in the microsoft.</param>
        /// <returns>Converts a path to the file relative to the mono project.</returns>
        private static string ToMonoPath(string microsoftPath)
        {
            // Do not convert directory separators as MonoDevelop uses "\" in its project files.
            return Path.Combine(Program.pathRelation, microsoftPath);
        }

        /// <summary>
        /// Create an xml compile element for a linked file.
        /// </summary>
        /// <param name="microsoftCompileNode">The original compile element for the file to be linked.</param>
        /// <returns>An xml compile element linking to the file.</returns>
        private static XElement CreateLinkNode(XElement microsoftCompileNode)
        {
            string microsoftPath = microsoftCompileNode.Attribute("Include").Value;

            string linkPath = microsoftPath;
            var microsoftLinkNode = microsoftCompileNode.Descendants().Where(x => x.Name.LocalName == "Link").FirstOrDefault();
            if (microsoftLinkNode != null)
            {
                linkPath = microsoftLinkNode.Value;
            }

            return new XElement(
                xmlns + "Compile",
                new XAttribute("Include", Program.ToMonoPath(microsoftPath)),
                new XElement(xmlns + "Link", linkPath));
        }

        /// <summary>
        /// Compares two paths, treating directory separators / and \ as equal.
        /// </summary>
        /// <param name="path1">The first path.</param>
        /// <param name="path2">The second path.</param>
        /// <returns><c>true</c> if the directories are considered equal, otherwise <c>false</c>.</returns>
        private static bool PathsEqual(string path1, string path2)
        {
            path1 = path1
                .Replace('\\', Path.DirectorySeparatorChar)
                .Replace('/', Path.DirectorySeparatorChar);
            path2 = path2
                .Replace('\\', Path.DirectorySeparatorChar)
                .Replace('/', Path.DirectorySeparatorChar);
            return Path.Equals(path1, path2);
        }
    }
}
