using Badumna.Utilities;

namespace Badumna
{
	using System;
	using System.Collections.Generic;
	using Badumna.Transport;

	public static class ConstructGenerics
	{
		public static void Construct()
		{
			new Dictionary<int, PgpsScheduler<TransportEnvelope>.SourceGroup>();
			new SortedList<CyclicalID.UShortID, byte[]>();
		}
	}
}

