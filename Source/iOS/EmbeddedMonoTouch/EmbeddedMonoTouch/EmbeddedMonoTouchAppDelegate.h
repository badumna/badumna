//
//  EmbeddedMonoTouchAppDelegate.h
//  EmbeddedMonoTouch
//
//  Created by Geoff Battye on 2/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EmbeddedMonoTouchAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
