//
//  Embed.h
//  EmbeddedEmbeddedingTest
//
//  Created by Geoff Battye on 2/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
//#import <mono/metadata/image.h>

@interface Embed : NSObject

- (void) runMono;
- (void) runBadumna;
@end
