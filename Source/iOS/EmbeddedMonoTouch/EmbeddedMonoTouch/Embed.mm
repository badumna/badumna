//
//  Embed.m
//  EmbeddedingTest
//
//  Created by Geoff Battye on 2/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Embed.h"
#include <iostream>
//#include "mono/jit/jit.h"
//#include "mono/metadata/assembly.h"
//#include "mono/metadata/debug-helpers.h"
#include "Badumna/Configuration/Options.h"
#include "Badumna/Core/NetworkFacade.h"
#include "Badumna/Core/RuntimeInitializer.h"

@implementation Embed

- (void) runMono
{
////    NSString *filename = @"TestAssembly.dll";
//    NSString *filename = @"BadumnaIOS.dll";
//    NSString *resDir = @"Res";
//    NSString *resPath = [[[NSBundle mainBundle] resourcePath]stringByAppendingPathComponent:resDir];
//    NSString *filePath = [resPath stringByAppendingPathComponent:filename];
//    
//    NSString *mscorlibPath = [resPath stringByAppendingPathComponent:@"mono/2.0/mscorlib.dll"];
//    
//    NSLog(@"mono_set_dirs(%s,\".\");", [resPath cStringUsingEncoding:[NSString defaultCStringEncoding]]);
//    mono_set_dirs([resPath cStringUsingEncoding:[NSString defaultCStringEncoding]], ".");
//       
//    BOOL fileFound = [[NSFileManager defaultManager] fileExistsAtPath:mscorlibPath];
//    if (fileFound)
//    {
//        NSLog(@"mscorlib.dll found at %@", mscorlibPath);
//    }
//    else {
//        NSLog(@"mscorlib.dll NOT found at %@", mscorlibPath);
//    }
//    
//    const char *file = [filePath cStringUsingEncoding:[NSString defaultCStringEncoding]];
//    NSLog(@"mono_jit_init(%s);", file);
//    MonoDomain *domain = mono_jit_init(file);
//
//    MonoAssembly *assembly = mono_domain_assembly_open(domain, file);
//    if (!assembly)
//    {
//        NSLog(@"Could not open assembly %s", file);
//        exit(2);
//    }
//    
//    // mono_jit_exec(domain, assembly, 0, NULL);
//    
//    MonoImage *image = mono_assembly_get_image(assembly);
//    
////    MonoClass *my_class = mono_class_from_name (image, "TestAssembly", "DummyClass");
//    MonoClass *my_class = mono_class_from_name (image, "Badumna", "Options");
//    /* allocate memory for the object */
//    MonoObject *my_class_instance = mono_object_new (domain, my_class);
//    /* execute the default argument-less constructor */
////    NSLog(@"Image: %d, class: 
//    mono_runtime_object_init (my_class_instance);
//    
////    MonoProperty *property = mono_class_get_property_from_name(my_class, "X");
////    int *arguments[1];
////    int a = 7;
////    arguments[0] = &a;
////    
////    mono_property_set_value(property, my_class_instance, (void**)arguments, NULL);
////    MonoObject *propertyValue = mono_property_get_value(property, my_class_instance, NULL, NULL);
////    int int_result = *(int*)mono_object_unbox (propertyValue);
////
////    NSLog(@"Property set to %d", int_result);    
////    MonoMethodDesc *methodDesc = mono_method_desc_new("Badumna.DummyClass:HelloWorld", FALSE);
////    MonoMethod *method = mono_method_desc_search_in_image(methodDesc, image);    
////    void *args[0];
////    mono_runtime_invoke(method, NULL, args, NULL);
//    
////    int retval = mono_environment_exitcode_get();
//    
//    mono_jit_cleanup(domain);
//    
    return;
}

- (void) runBadumna
{
    printf("Initializing badumna\n");
    Badumna::InitializeRuntime();
    [self startBadumna];

    printf("Shutting down badumna\n");
    Badumna::ShutdownRuntime();

    return;
//    //return;
//    
//    // Badumna::BadumnaRuntimeInitializer2 initializer("mono_config_file");
//    NSString *resDir = @"Res";
//    NSString *resPath = [[[NSBundle mainBundle] resourcePath]stringByAppendingPathComponent:resDir];
//
//    NSString *filename = @"BadumnaIOS.dll";
//    NSString *filePath = [resPath stringByAppendingPathComponent:filename];
//    
////    NSString *mscorlibPath = [resPath stringByAppendingPathComponent:@"mono/2.1/mscorlib.dll"];
////    mono_set_dirs([resPath cStringUsingEncoding:[NSString defaultCStringEncoding]], ".");
//    NSLog(
//          @"Badumna::InitializeRuntime(%s, %s);",
//          [resPath cStringUsingEncoding:[NSString defaultCStringEncoding]],
//          [filePath cStringUsingEncoding:[NSString defaultCStringEncoding]]);
//    Badumna::InitializeRuntime(
//                               *new Badumna::String([resPath cStringUsingEncoding:[NSString defaultCStringEncoding]]),
//                               *new Badumna::String([filePath cStringUsingEncoding:[NSString defaultCStringEncoding]]));
//    
//    NSLog(@"Initialized runtime!!!");
//
//    NSString *filename2 = @"monotouch.dll";
//    NSString *filePath2 = [resPath stringByAppendingPathComponent:filename2];
//    mono_assembly_open([filePath2 cStringUsingEncoding:[NSString defaultCStringEncoding]], NULL);
//    //[Extra DoIt];
//    
//    NSLog(@"Starting Badumna...");
//    [self startBadumna];
//    NSLog(@"Errrrrr?");
//    
//    Badumna::ShutdownRuntime();
//    
}

- (int) startBadumna
{
    // configuration options used for this local badumna client. 
    Badumna::Options options;
    options.GetConnectivityModule().SetStartPortRange(21300);
    options.GetConnectivityModule().SetEndPortRange(21399);
    options.GetConnectivityModule().SetMaxPortsToTry(3);
    options.GetConnectivityModule().EnableBroadcast();
    options.GetConnectivityModule().SetBroadcastPort(21250);
    //options.GetConnectivityModule().ConfigureForLan();
    options.GetConnectivityModule().SetApplicationName(L"CppApiExample0");
    
    std::auto_ptr<Badumna::NetworkFacade> facade(Badumna::NetworkFacade::Create(options));
    if(!facade->Login())
    {
        // failed to login to the badumna network
        std::wcerr << L"failed to login to the badumna network." << std::endl;
        return 1;
    }
    
    // print out the network status
    // you should be able to see the network status (e.g. private and public address) of this client peer. 
    std::wcout << facade->GetNetworkStatus() << std::endl;
    
    // logout from the badaumna network, then shutdown the badumna engine.
    facade->Shutdown();
//    
    return 0;
}

@end
