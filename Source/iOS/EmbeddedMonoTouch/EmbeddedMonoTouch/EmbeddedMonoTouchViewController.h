//
//  EmbeddedMonoTouchViewController.h
//  EmbeddedMonoTouch
//
//  Created by Geoff Battye on 2/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EmbeddedMonoTouchViewController : UIViewController
@property (nonatomic, retain) IBOutlet UILabel *label;

@end
