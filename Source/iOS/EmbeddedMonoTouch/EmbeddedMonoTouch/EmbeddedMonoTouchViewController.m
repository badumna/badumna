//
//  EmbeddedMonoTouchViewController.m
//  EmbeddedMonoTouch
//
//  Created by Geoff Battye on 2/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "EmbeddedMonoTouchViewController.h"
#import "Embed.h"
//#import <mono/metadata/image.h>

@interface EmbeddedMonoTouchViewController ()

@end

@implementation EmbeddedMonoTouchViewController
@synthesize label;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    Embed *embed = [Embed new];
//    [embed runMono];
    [embed runBadumna];
     
    self.label.text = @"Ha ha!";
}

- (void)viewDidUnload
{
    [self setLabel:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
    } else {
        return YES;
    }
}

@end
