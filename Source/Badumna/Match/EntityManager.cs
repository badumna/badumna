﻿//-----------------------------------------------------------------------
// <copyright file="EntityManager.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2013 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.Match
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using System.IO;
    using Badumna.Autoreplication;
    using Badumna.Core;
    using Badumna.DataTypes;
    using Badumna.Replication;
    using Badumna.SpatialEntities;
    using Badumna.Utilities;

    using MatchAutoreplicationManager = Badumna.Autoreplication.MatchAutoreplicationManager;

    /// <summary>
    /// Responsible for liaising between match membership manager and replication system's entity
    /// manager to ensure match entities are replicated correctly.
    /// </summary>
    /// <remarks>
    /// TODO: Refactor into separate managers for entities, hosted entities, controllers etc.
    /// </remarks>
    internal class EntityManager : IReplicationService<IOriginal, IReplica>
    {
        /// <summary>
        /// Autoreplication manager.
        /// </summary>
        private readonly MatchAutoreplicationManager autoreplicationManager;

        /// <summary>
        /// For allocating unique IDs to entities.
        /// </summary>
        private readonly IBadumnaIdAllocator identifierAllocator;

        /// <summary>
        /// Replication system's entity manager for actually doing the replication.
        /// </summary>
        private readonly Replication.IEntityManager replicationEntityManager;

        /// <summary>
        /// For synchronizing access of matchEntitiesByMatchID dictionary.
        /// </summary>
        private readonly object matchEntitiesByMatchIDLock = new object();

        /// <summary>
        /// For synchronizing access of hostedEntitiesByMatchID dictionary.
        /// </summary>
        private readonly object hostedEntitiesByMatchIDLock = new object();

        /// <summary>
        /// Map of scene names to lists of network scenes.
        /// </summary>
        private Dictionary<BadumnaId, Match> matches = new Dictionary<BadumnaId, Match>();

        /// <summary>
        /// The queue to use for scheduling events.
        /// </summary>
        private INetworkEventScheduler eventQueue;

        /// <summary>
        /// Reports on network connectivity status.
        /// </summary>
        private INetworkConnectivityReporter connectivityReporter;

        /// <summary>
        /// One record per original entity
        /// </summary>
        private Dictionary<BadumnaId, MatchOriginalWrapper> matchOriginalWrappersByEntityID =
            new Dictionary<BadumnaId, MatchOriginalWrapper>();

        /// <summary>
        /// One record per original entity
        /// </summary>
        private Dictionary<object, MatchOriginalWrapper> matchOriginalWrappersByEntity =
            new Dictionary<object, MatchOriginalWrapper>(ReferenceEqualityComparer<object>.Instance);

        /// <summary>
        /// Hosted entities by Match ID.
        /// </summary>
        private Dictionary<BadumnaId, List<object>> matchEntitiesByMatchID =
            new Dictionary<BadumnaId, List<object>>();

        /// <summary>
        /// Reusable list of originals to remove (to avoid garbage generation).
        /// </summary>
        private List<object> matchOriginalsToRemove = new List<object>();

        /// <summary>
        /// One recored per replica
        /// </summary>
        private Dictionary<BadumnaId, MatchReplicaWrapper> matchReplicaWrappers = new Dictionary<BadumnaId, MatchReplicaWrapper>();

        /// <summary>
        /// Controller objects.
        /// </summary>
        private Dictionary<BadumnaId, object> controllers = new Dictionary<BadumnaId, object>();

        /// <summary>
        /// Dummy originals by member ID, for addressing updates to.
        /// </summary>
        private Dictionary<BadumnaId, IOriginal> dummyOriginals = new Dictionary<BadumnaId, IOriginal>();

        /// <summary>
        /// Controllers autoreplication replica wrappers by match ID (not member ID),
        /// for hooking up to controller replica wrappers.
        /// </summary>
        private Dictionary<BadumnaId, IReplica> controllerAutoReplicaWrappersByMatchID =
            new Dictionary<BadumnaId, IReplica>();

        /// <summary>
        /// Controllers autoreplication replica wrappers by member ID (not match ID),
        /// for delivering controller RPCs.
        /// </summary>
        private Dictionary<BadumnaId, IReplica> controllerAutoReplicaWrappersByMemberID =
            new Dictionary<BadumnaId, IReplica>();

        /// <summary>
        /// Controller match original wrappers by match ID.
        /// </summary>
        private Dictionary<BadumnaId, ControllerOriginalWrapper> controllerOriginalWrappersByMatchID =
            new Dictionary<BadumnaId, ControllerOriginalWrapper>();

        /// <summary>
        /// Controller match original wrappers by entity ID, for dispatching updates.
        /// </summary>
        private Dictionary<BadumnaId, ControllerOriginalWrapper> controllerOriginalWrappersByEntityID =
            new Dictionary<BadumnaId, ControllerOriginalWrapper>();

        /// <summary>
        /// Controller match replica wrapper by match ID (not entity ID), for takeover.
        /// </summary>
        private Dictionary<BadumnaId, ControllerReplicaWrapper> controllerReplicaWrappersByMatchID =
            new Dictionary<BadumnaId, ControllerReplicaWrapper>();

        /// <summary>
        /// Controller match replica wrapper by entity ID (not match ID), for passing replication updates to.
        /// </summary>
        private Dictionary<BadumnaId, ControllerReplicaWrapper> controllerReplicaWrappersByEntityID =
            new Dictionary<BadumnaId, ControllerReplicaWrapper>();

        /// <summary>
        /// Hosted originals by original entity ID.
        /// </summary>
        private Dictionary<BadumnaId, HostedOriginalWrapper> hostedOriginalWrappersByGuid =
            new Dictionary<BadumnaId, HostedOriginalWrapper>();

        /// <summary>
        /// Hosted originals by persistent ID.
        /// </summary>
        private Dictionary<BadumnaId, HostedOriginalWrapper> hostedOriginalWrappersByPersistentID =
            new Dictionary<BadumnaId, HostedOriginalWrapper>();

        /// <summary>
        /// Hosted replica wrappers by ID.
        /// </summary>
        private Dictionary<BadumnaId, HostedReplicaWrapper> hostedReplicaWrappersByGuid =
            new Dictionary<BadumnaId, HostedReplicaWrapper>();

        /// <summary>
        /// Hosted replica wrappers by PersistentID.
        /// </summary>
        private Dictionary<BadumnaId, HostedReplicaWrapper> hostedReplicaWrappersByPersistentId =
            new Dictionary<BadumnaId, HostedReplicaWrapper>();

        /// <summary>
        /// Hosted entities by persistent ID.
        /// </summary>
        private Dictionary<BadumnaId, object> hostedEntitiesByPersistentID =
            new Dictionary<BadumnaId, object>();

        /// <summary>
        /// Persistent IDs by hosted entity.
        /// </summary>
        private Dictionary<object, BadumnaId> persistentIDsByHostedEntity =
            new Dictionary<object, BadumnaId>(ReferenceEqualityComparer<object>.Instance);

        /// <summary>
        /// Hosted entity types by persistent ID.
        /// </summary>
        private Dictionary<BadumnaId, uint> hostedEntityTypesByPersistentID =
            new Dictionary<BadumnaId, uint>();

        /// <summary>
        /// Hosted entities by Match ID.
        /// </summary>
        private Dictionary<BadumnaId, List<object>> hostedEntitiesByMatchID =
            new Dictionary<BadumnaId, List<object>>();

        /// <summary>
        /// Temporary list of hosted entities to remove (for iteration while removing).
        /// TODO: Make inital capacity configurable.
        /// </summary>
        private List<object> hostedEntitiesToRemove = new List<object>();

        /// <summary>
        /// Certificate timestamp of current entity hosts.
        /// </summary>
        private Dictionary<BadumnaId, TimeSpan> hostedEntityCertificateTimestamps =
            new Dictionary<BadumnaId, TimeSpan>();

        /// <summary>
        /// Certificate timestamp of current controller controllers (ugh).
        /// </summary>
        private Dictionary<BadumnaId, TimeSpan> controllerCertificateTimestamps =
            new Dictionary<BadumnaId, TimeSpan>();

        /// <summary>
        /// The method to use to queue events that must run in the application's thread.
        /// </summary>
        private QueueInvokable queueApplicationEvent;

        /// <summary>
        /// Initializes a new instance of the EntityManager class.
        /// </summary>
        /// <param name="replicationEntityManager">The replication system's entity manager.</param>        
        /// <param name="autoreplicationManager">Match auto replication manager.</param>
        /// <param name="connectivityReporter">Reports on network connectivity status.</param>
        /// <param name="eventQueue">The event queue to schedule events on.</param>
        /// <param name="queueApplicationEvent">The method to use to queue events that must run in the application's thread.</param>
        /// <param name="identifierAllocator">For allocating IDs to entities.</param>
        public EntityManager(
            Replication.IEntityManager replicationEntityManager,
            MatchAutoreplicationManager autoreplicationManager,
            INetworkConnectivityReporter connectivityReporter,
            INetworkEventScheduler eventQueue,
            QueueInvokable queueApplicationEvent,
            IBadumnaIdAllocator identifierAllocator)
        {
            if (replicationEntityManager == null)
            {
                throw new ArgumentNullException("replicationEntityManager");
            }

            if (identifierAllocator == null)
            {
                throw new ArgumentNullException("identifierAllocator");
            }

            this.identifierAllocator = identifierAllocator;
            this.autoreplicationManager = autoreplicationManager;
            this.connectivityReporter = connectivityReporter;
            this.replicationEntityManager = replicationEntityManager;
            this.eventQueue = eventQueue;
            this.queueApplicationEvent = queueApplicationEvent;

            this.replicationEntityManager.Register((byte)EntityGroup.Match, this.CreateReplica, this.RemoveReplica);

            this.replicationEntityManager.Register(
                (byte)EntityGroup.MatchController,
                this.CreateControllerReplica,
                this.RemoveControllerReplica);

            ////this.garbageCollectionTask = new RegularTask(
            ////    "match_entity_manager_gc",
            ////    Parameters.EntityManagerGarbageCollectionPeriod,
            ////    eventQueue,
            ////    connectivityReporter,
            ////    this.GarbageCollection);
            ////this.garbageCollectionTask.Start();

            this.replicationEntityManager.Register(
                (byte)EntityGroup.MatchHosted,
                this.CreateHostedReplica,
                this.RemoveHostedReplica);
        }

        /// <summary>
        /// Join match.
        /// </summary>
        /// <param name="match">The match.</param>
        /// <param name="memberID">The ID of the local match member.</param>
        /// <param name="controller">Object to use as controller.</param>
        public void JoinMatch(Match match, BadumnaId memberID, object controller)
        {
            if (!this.matches.ContainsKey(match.MatchIdentifier))
            {
                this.matches[match.MatchIdentifier] = match;
            }

            // Register a dummy original with ID set to local membebr ID, so entity updates can be addressed here using that ID.
            var dummyOriginal = new DummyOriginal(memberID);
            this.replicationEntityManager.RegisterEntity(dummyOriginal, new EntityTypeId((byte)EntityGroup.MatchDummy, 0));
            this.dummyOriginals.Add(match.MatchIdentifier, dummyOriginal);

            // Create auto-replica wrapper for the controller, if there is one.
            if (controller != null)
            {
                this.controllers.Add(match.MatchIdentifier, controller);
                var autoReplicaWrapper = this.StartPosingAsIReplica(controller);
                this.controllerAutoReplicaWrappersByMatchID.Add(match.MembershipReporter.MatchIdentifier, autoReplicaWrapper);
                this.controllerAutoReplicaWrappersByMemberID.Add(memberID, autoReplicaWrapper);
            }
        }

        /// <summary>
        /// Remove all replicas from the match.
        /// </summary>
        /// <param name="match">The match.</param>
        /// <param name="memberID">The ID of the local match member.</param>
        public void LeaveMatch(Match match, BadumnaId memberID)
        {
            var removedList = new List<BadumnaId>();

            foreach (var keyPair in this.matchOriginalWrappersByEntity)
            {
                var original = keyPair.Value;
                if (original.MatchID.Equals(match.MatchIdentifier))
                {
                    this.matchOriginalsToRemove.Add(keyPair.Key);
                }
            }

            foreach (var entity in this.matchOriginalsToRemove)
            {
                this.DeregisterEntity(entity, match);
            }

            this.matchOriginalsToRemove.Clear();

            foreach (var keyPair in this.matchReplicaWrappers)
            {
                var replica = keyPair.Value;
                if (replica.Match.MatchIdentifier.Equals(match.MatchIdentifier))
                {
                    replica.RemoveFromMatch();
                    removedList.Add(keyPair.Key);
                }
            }

            foreach (var key in removedList)
            {
                this.matchReplicaWrappers.Remove(key);
            }

            removedList.Clear();

            this.matches.Remove(match.MatchIdentifier);

            this.UnregisterController(match);

            lock (this.matchEntitiesByMatchIDLock)
            {
                this.matchEntitiesByMatchID.Remove(match.MatchIdentifier);
            }

            List<object> hostedEntities;
            lock (this.hostedEntitiesByMatchIDLock)
            {
                this.hostedEntitiesByMatchID.TryGetValue(match.MatchIdentifier, out hostedEntities);
            }

            if (hostedEntities != null)
            {
                this.hostedEntitiesToRemove.AddRange(hostedEntities);
                for (int i = this.hostedEntitiesToRemove.Count - 1; i >= 0; --i)
                {
                    this.RemoveHostedEntity(this.hostedEntitiesToRemove[i], match.MatchIdentifier);
                }

                this.hostedEntitiesToRemove.Clear();
                lock (this.hostedEntitiesByMatchIDLock)
                {
                    this.hostedEntitiesByMatchID.Remove(match.MatchIdentifier);
                }
            }
        }

        /// <summary>
        /// Register an entity for replication to other members of the match.
        /// </summary>
        /// <param name="entity">The entity to replicate.</param>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="match">The match.</param>
        public void RegisterEntity(object entity, uint entityType, Match match)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("original");
            }

            var matchOriginalWrapper = new MatchOriginalWrapper(
                                            this.StartPosingAsIOriginal(entity),
                                            this.identifierAllocator.GetNextUniqueId(),
                                            entityType,
                                            match.MembershipReporter,
                                            this.replicationEntityManager);
            this.matchOriginalWrappersByEntityID[matchOriginalWrapper.Guid] = matchOriginalWrapper;
            this.matchOriginalWrappersByEntity[entity] = matchOriginalWrapper;

            lock (this.matchEntitiesByMatchIDLock)
            {
                List<object> matchEntities;
                if (!this.matchEntitiesByMatchID.TryGetValue(match.MatchIdentifier, out matchEntities))
                {
                    matchEntities = new List<object>();
                    this.matchEntitiesByMatchID[match.MatchIdentifier] = matchEntities;
                }

                matchEntities.Add(entity);
            }
        }

        /// <summary>
        /// Register a hosted entity for replication to other members of the match.
        /// </summary>
        /// <param name="entity">The entity to replicate.</param>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="membershipReporter">The match membership reporter.</param>
        public void RegisterHostedEntity(
            object entity,
            uint entityType,
            IMembershipReporter membershipReporter)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("original");
            }

            if (!membershipReporter.IsHost)
            {
                return;
            }

            if (this.persistentIDsByHostedEntity.ContainsKey(entity))
            {
                throw new InvalidOperationException("Only newly created hosted entities should be registered by the host.");
            }

            var persistentID = this.identifierAllocator.GetNextUniqueId();
            this.AddNewHostedEntity(entity, entityType, persistentID, membershipReporter.MatchIdentifier);
            this.TakeOverHostedEntityAsOriginal(membershipReporter, persistentID, entityType);
        }

        /// <summary>
        /// Mark an entity as inaccessible to prevent member access after application has unregistered it.
        /// </summary>
        /// <param name="entity">The entity to mark as inaccessible.</param>
        public void MarkEntityInaccessible(object entity)
        {
            if (!(entity is IOriginal))
            {
                this.autoreplicationManager.MarkOriginalInaccessible(entity);
            }
        }

        /// <summary>
        /// Mark all entities in a given match as inaccessible to prevent member access after application has unregistered it.
        /// </summary>
        /// <param name="match">The match whose entities are to be marked as inaccessible.</param>
        public void MarkOriginalsInaccessible(Match match)
        {
            List<object> hostedEntities = null;
            lock (this.hostedEntitiesByMatchIDLock)
            {
                this.hostedEntitiesByMatchID.TryGetValue(match.MatchIdentifier, out hostedEntities);
            }
            
            if (hostedEntities != null)
            {
                foreach (var hostedEntity in hostedEntities)
                {
                    this.autoreplicationManager.MarkOriginalInaccessible(hostedEntity);
                }
            }

            lock (this.matchEntitiesByMatchIDLock)
            {
                List<object> matchEntities;
                if (this.matchEntitiesByMatchID.TryGetValue(match.MatchIdentifier, out matchEntities))
                {
                    foreach (var matchEntity in matchEntities)
                    {
                        this.autoreplicationManager.MarkOriginalInaccessible(matchEntity);
                    }
                }
            }
        }

        /// <summary>
        /// Deregister an entity to cease replication to other members of the match.
        /// </summary>
        /// <param name="entity">The entity to stop replicating.</param>
        /// <param name="match">The match the entity is registered for.</param>
        public void DeregisterEntity(object entity, Match match)
        {
            MatchOriginalWrapper matchOriginalWrapper;
            if (this.matchOriginalWrappersByEntity.TryGetValue(entity, out matchOriginalWrapper))
            {
                this.StopPosingAsIOriginal(entity);

                // Replicas should be removed when unregistering a regular entity.
                matchOriginalWrapper.ReleaseEntity(true);
                this.matchOriginalWrappersByEntityID.Remove(matchOriginalWrapper.Guid);
                this.matchOriginalWrappersByEntity.Remove(entity);
            }

            lock (this.matchEntitiesByMatchIDLock)
            {
                List<object> matchEntities;
                if (this.matchEntitiesByMatchID.TryGetValue(match.MatchIdentifier, out matchEntities))
                {
                    int index = matchEntities.FindIndex(e => object.ReferenceEquals(e, entity));
                    if (index >= 0)
                    {
                        matchEntities.RemoveAt(index);
                    }
                }
            }

            // Only remove list from dictionary when match has been left to avoid recreation.
        }

        /// <summary>
        /// Deregister a hosted entity to remove it from the match.
        /// </summary>
        /// <param name="entity">The entity to remove.</param>
        /// <param name="removeReplicas">Remove the replicas.</param>
        /// <param name="membershipReporter">The membership reporter for the match that the entity is being removed from.</param>
        public void DeregisterHostedEntity(object entity, bool removeReplicas, IMembershipReporter membershipReporter)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }

            if (!membershipReporter.IsHost)
            {
                return;
            }

            BadumnaId persistentID;
            if (this.persistentIDsByHostedEntity.TryGetValue(entity, out persistentID))
            {
                this.ReleaseHostedEntityFromOriginal(persistentID, true);
                this.RemoveHostedEntity(entity, membershipReporter.MatchIdentifier);

                // TODO: Send match protocol method to all other members removing entity.
            }
        }

        /// <summary>
        /// Send a custom message to a spatial original's replica.
        /// </summary>
        /// <param name="original">Spatial original whose replicas should receive the message.</param>
        /// <param name="messageData">Custom message.</param>
        public void SendCustomMessageToReplicas(IOriginal original, MemoryStream messageData)
        {
            MatchOriginalWrapper originalWrapper;
            if (this.matchOriginalWrappersByEntityID.TryGetValue(original.Guid, out originalWrapper))
            {
                this.replicationEntityManager.SendCustomMessageToReplicas(originalWrapper, messageData);
                return;
            }

            HostedOriginalWrapper hostedOriginalWrapper;
            if (this.hostedOriginalWrappersByGuid.TryGetValue(original.Guid, out hostedOriginalWrapper))
            {
                this.replicationEntityManager.SendCustomMessageToReplicas(hostedOriginalWrapper, messageData);
                return;
            }

            throw new InvalidOperationException("Could not find original.");
        }

        /// <summary>
        /// Send a custom message to a spatial original.
        /// </summary>
        /// <param name="replica">Spatial replica whose original should receive the message.</param>
        /// <param name="messageData">Custom message.</param>
        public void SendCustomMessageToOriginal(IReplica replica, MemoryStream messageData)
        {
            MatchReplicaWrapper replicaWrapper;
            if (this.matchReplicaWrappers.TryGetValue(replica.Guid, out replicaWrapper))
            {
                this.replicationEntityManager.SendCustomMessageToOriginal(replicaWrapper, messageData);
                return;
            }

            HostedReplicaWrapper hostedReplicaWrapper;
            if (this.hostedReplicaWrappersByGuid.TryGetValue(replica.Guid, out hostedReplicaWrapper))
            {
                Logger.TraceInformation(LogTag.Match, "Sending message to original from hosted replica");
                this.replicationEntityManager.SendCustomMessageToOriginal(hostedReplicaWrapper, messageData);
                return;
            }

            throw new InvalidOperationException("Unknown replica");
        }

        /// <summary>
        /// Handle an incoming controller RPC.
        /// </summary>
        /// <param name="recipientID">The ID of the member the controller belongs to.</param>
        /// <param name="data">The serialized RPC data.</param>
        public void HandleControllerRPC(BadumnaId recipientID, byte[] data)
        {
            IReplica controllerAutoReplica;
            if (this.controllerAutoReplicaWrappersByMemberID.TryGetValue(recipientID, out controllerAutoReplica))
            {
                ////controllerAutoReplica.HandleEvent(new MemoryStream(data));
                this.queueApplicationEvent(Apply.Func(delegate { controllerAutoReplica.HandleEvent(new MemoryStream(data)); }));
            }
        }

        /// <summary>
        /// Schedule marks parts to update in network event queue.
        /// </summary>
        /// <param name="original">Original to update.</param>
        /// <param name="changedParts">List of parts to update.</param>
        public void MarkForUpdate(IOriginal original, BooleanArray changedParts)
        {
            if (null == original)
            {
                throw new ArgumentNullException("spatialOriginal cannot be null.");
            }

            if (null == original.Guid)
            {
                return;
            }

            // TODO: This needs refactoring!
            MatchOriginalWrapper matchOriginalWrapper;
            if (this.matchOriginalWrappersByEntityID.TryGetValue(original.Guid, out matchOriginalWrapper))
            {
                // Called in event queue to ensure there is no conflict with online events and other threading issues.
                this.eventQueue.Push(this.MarkForUpdateImplementation, matchOriginalWrapper, changedParts);
                return;
            }

            HostedOriginalWrapper hostedOriginalWrapper;
            if (this.hostedOriginalWrappersByGuid.TryGetValue(original.Guid, out hostedOriginalWrapper))
            {
                // Called in event queue to ensure there is no conflict with online events and other threading issues.
                this.eventQueue.Push(this.MarkForUpdateImplementation, hostedOriginalWrapper, changedParts);
                return;
            }

            ControllerOriginalWrapper controllerOriginalWrapper;
            if (this.controllerOriginalWrappersByEntityID.TryGetValue(original.Guid, out controllerOriginalWrapper))
            {
                // Called in event queue to ensure there is no conflict with online events and other threading issues.
                this.eventQueue.Push(this.MarkForUpdateImplementation, controllerOriginalWrapper, changedParts);
                return;
            }
        }

        /// <summary>
        /// Returns an IReplica for the entity, either by casting the entity
        /// to IReplica or by creating an autoreplication wrapper for it.
        /// </summary>
        /// <remarks>
        /// Must be matched by a call to <see cref="StopPosingAsIReplica"/>.
        /// </remarks>
        /// <param name="entity">The entity</param>
        /// <returns>The IReplica representation</returns>
        public IReplica StartPosingAsIReplica(object entity)
        {
            return entity as IReplica ?? this.autoreplicationManager.RegisterReplica(entity);
        }

        /// <summary>
        /// Undoes <see cref="StartPosingAsIReplica"/>.  After this call
        /// the IReplica can no longer be used.
        /// </summary>
        /// <param name="replica">The replica to unregister</param>
        /// <returns>The original entity</returns>
        public object StopPosingAsIReplica(IReplica replica)
        {
            var autoReplica = replica as IReplicaEntityWrapper;
            if (autoReplica != null)
            {
                return this.autoreplicationManager.UnregisterReplica(autoReplica);
            }

            return replica;
        }

        /// <summary>
        /// Find match based on the given match identifier.
        /// </summary>
        /// <param name="matchId">A given match identifier.</param>
        /// <returns>Return the match if exist.</returns>
        internal Match FindMatch(BadumnaId matchId)
        {
            Match match;
            if (!this.matches.TryGetValue(matchId, out match))
            {
                return null;
            }

            return match;
        }

        /// <summary>
        /// Gets an entity matching a given persistent ID, if found.
        /// </summary>
        /// <param name="persistentID">The persistent ID.</param>
        /// <param name="entity">The entity, if found.</param>
        /// <returns><c>true</c> if an entity was found, otherwise <c>false</c>.</returns>
        internal bool TryGetHostedEntity(BadumnaId persistentID, out object entity)
        {
            return this.hostedEntitiesByPersistentID.TryGetValue(persistentID, out entity);
        }

        /// <summary>
        /// Add a new hosted entity.
        /// </summary>
        /// <param name="entity">The entity to add.</param>
        /// <param name="entityType">An integer used by the application to identify the type of the entity.</param>
        /// <param name="persistentID">An ID for the entity that persists accross hosts.</param>
        /// <param name="matchID">The ID of the match the entity belongs to.</param>
        internal void AddNewHostedEntity(object entity, uint entityType, BadumnaId persistentID, BadumnaId matchID)
        {
            this.hostedEntitiesByPersistentID[persistentID] = entity;
            this.hostedEntityTypesByPersistentID[persistentID] = entityType;
            this.persistentIDsByHostedEntity[entity] = persistentID;
            List<object> hostedEntities;
            lock (this.hostedEntitiesByMatchIDLock)
            {
                if (!this.hostedEntitiesByMatchID.TryGetValue(matchID, out hostedEntities))
                {
                    hostedEntities = new List<object>();
                    this.hostedEntitiesByMatchID[matchID] = hostedEntities;
                }
            }

            hostedEntities.Add(entity);
        }

        /// <summary>
        /// Called to have a replica takeover control of a hosted entity.
        /// </summary>
        /// <param name="hostedReplicaWrapper">The replica taking over.</param>
        /// <returns><c>true</c> if takeover was successful, otherwise <c>false</c>.</returns>
        internal bool TakeOverHostedEntityAsReplica(HostedReplicaWrapper hostedReplicaWrapper)
        {
            Logger.TraceInformation(LogTag.Match, "Trying to take over hosted entity as replica.");

            if (!this.TryClaimControlOfHostedEntity(hostedReplicaWrapper.PersistentID, hostedReplicaWrapper.CertificateTimestamp))
            {
                return false;
            }

            // The hosted entity might still be being controlled by this peer.
            // Another original is already controlling the entity, so replicas of this original should be removed.
            this.ReleaseHostedEntityFromOriginal(hostedReplicaWrapper.PersistentID, true);

            // There might be another replica still controlling the hosted entity.
            this.ReleaseHostedEntityFromReplica(hostedReplicaWrapper.PersistentID);

            this.hostedReplicaWrappersByPersistentId[hostedReplicaWrapper.PersistentID] = hostedReplicaWrapper;
            return true;
        }

        /// <summary>
        /// Gets the autoreplication controller replica wrapper for a given match.
        /// </summary>
        /// <param name="matchID">The ID of the match.</param>
        /// <returns>The controller's auto replica wrapper.</returns>
        internal IReplica GetControllerAutoReplicaWrapper(BadumnaId matchID)
        {
            return this.controllerAutoReplicaWrappersByMatchID[matchID];
        }

        /// <summary>
        /// Called to have a controller replica wrapper takeover a controller.
        /// </summary>
        /// <param name="controllerReplicaWrapper">The wrapper taking over.</param>
        /// <returns><c>true</c> if takeover was successful, otherwise <c>false</c>.</returns>
        internal bool TakeOverControllerAsReplica(ControllerReplicaWrapper controllerReplicaWrapper)
        {
            Logger.TraceInformation(LogTag.Match, "Trying to take over controller as replica.");

            if (!this.TryClaimControlOfHostedEntity(controllerReplicaWrapper.MatchID, controllerReplicaWrapper.CertificateTimestamp))
            {
                return false;
            }

            // The controller might still be being controlled by this peer.
            // Another controller is already controlling the entity, so replicas of this original should be removed.
            this.ReleaseControllerFromOriginal(controllerReplicaWrapper.MatchID);

            // There might be another replica still controlling the hosted entity.
            this.ReleaseControllerFromReplica(controllerReplicaWrapper.MatchID);

            this.controllerReplicaWrappersByMatchID[controllerReplicaWrapper.MatchID] = controllerReplicaWrapper;
            return true;
        }

        /// <summary>
        /// Handle host change notifications.
        /// </summary>
        /// <param name="match">The match.</param>
        /// <param name="memberID">The local member whose match hosting status (host vs client) has changed.</param>
        /// <param name="isHost">A value indicating whether the member is now the host.</param>
        internal void OnHostChanged(Match match, BadumnaId memberID, bool isHost)
        {
            if (isHost)
            {
                Logger.TraceInformation(LogTag.Match, "Taking over as host.");
                this.TakeoverControllerAsOriginal(match);

                foreach (var persistentID in this.hostedEntitiesByPersistentID.Keys)
                {
                    this.ReleaseHostedEntityFromReplica(persistentID);
                    this.TakeOverHostedEntityAsOriginal(
                        match.MembershipReporter,
                        persistentID,
                        this.hostedEntityTypesByPersistentID[persistentID]);
                }
            }
            else
            {
                Logger.TraceInformation(LogTag.Match, "Stopped being host.");
                this.ReleaseControllerFromOriginal(match.MatchIdentifier);

                foreach (var persistentID in this.hostedEntitiesByPersistentID.Keys)
                {
                    this.ReleaseHostedEntityFromOriginal(persistentID, false);
                }
            }
        }

        /// <summary>
        /// Returns an IOriginal for the entity, either by casting the entity
        /// to IOriginal or by creating an autoreplication wrapper for it.
        /// </summary>
        /// <remarks>
        /// Must be matched by a call to <see cref="StopPosingAsIOriginal"/>.
        /// </remarks>
        /// <param name="entity">The entity</param>
        /// <returns>The IOriginal representation</returns>
        private IOriginal StartPosingAsIOriginal(object entity)
        {
            return entity as IOriginal ?? this.autoreplicationManager.RegisterOriginal(entity, new KeyValuePair<float, float>(0, 0));
        }

        /// <summary>
        /// Undoes <see cref="StartPosingAsIOriginal"/>.  After this call
        /// the IOriginal can no longer be used.
        /// </summary>
        /// <param name="entity">The entity to unregister</param>
        private void StopPosingAsIOriginal(object entity)
        {
            if (!(entity is IOriginal))
            {
                this.autoreplicationManager.UnregisterOriginal(entity);
            }
        }

        /// <summary>
        /// Marks parts to update.
        /// </summary>
        /// <param name="original">Match original to update.</param>
        /// <param name="changedParts">List of parts to update.</param>
        private void MarkForUpdateImplementation(IOriginal original, BooleanArray changedParts)
        {
            this.replicationEntityManager.MarkForUpdate(original, changedParts);
        }

        /// <summary>
        /// Creates the match replica.
        /// </summary>
        /// <param name="entityId">The entity id.</param>
        /// <param name="entityTypeId">Entity type id.</param>
        /// <returns>Returns IReplica.</returns>
        private IReplica CreateReplica(BadumnaId entityId, EntityTypeId entityTypeId)
        {
            Logger.TraceInformation(LogTag.Match, "Invoking CreateReplica for match entity {0}", entityId);
            MatchReplicaWrapper matchReplicaWrapper;
            if (!this.matchReplicaWrappers.TryGetValue(entityId, out matchReplicaWrapper))
            {
                Logger.TraceInformation(LogTag.Match, "Creating match replica wrapper for match entity {0}", entityId);
                matchReplicaWrapper = new MatchReplicaWrapper(entityId, entityTypeId.Id, this.eventQueue, this, this.queueApplicationEvent);
                this.matchReplicaWrappers.Add(entityId, matchReplicaWrapper);
            }

            return matchReplicaWrapper;
        }

        /// <summary>
        /// Remove replica.
        /// </summary>
        /// <param name="replica">Replica to remove.</param>
        private void RemoveReplica(IReplica replica)
        {
            MatchReplicaWrapper matchReplicaWrapper = replica as MatchReplicaWrapper;
            if (this.matchReplicaWrappers.ContainsKey(matchReplicaWrapper.Guid))
            {
                matchReplicaWrapper.RemoveFromMatch();
                this.matchReplicaWrappers.Remove(replica.Guid);
            }
        }

        /// <summary>
        /// Creates a replica wrapper for the controller (populated with controller on deserialization).
        /// </summary>
        /// <param name="entityId">The entity id.</param>
        /// <param name="entityTypeId">Entity type id.</param>
        /// <returns>Returns IReplica.</returns>
        private IReplica CreateControllerReplica(BadumnaId entityId, EntityTypeId entityTypeId)
        {
            Logger.TraceInformation(LogTag.Match, "Creating controller replica for controller {0}", entityId);
            ControllerReplicaWrapper controllerReplicaWrapper;
            if (!this.controllerReplicaWrappersByEntityID.TryGetValue(entityId, out controllerReplicaWrapper))
            {
                Logger.TraceInformation(LogTag.Match, "Really creating controller replica for controller {0}", entityId);
                controllerReplicaWrapper = new ControllerReplicaWrapper(entityId, this.eventQueue, this, this.queueApplicationEvent);
                this.controllerReplicaWrappersByEntityID.Add(entityId, controllerReplicaWrapper);
            }

            return controllerReplicaWrapper;
        }

        /// <summary>
        /// Remove controller replica.
        /// </summary>
        /// <param name="replica">Replica to remove.</param>
        private void RemoveControllerReplica(IReplica replica)
        {
            var controllerReplicaWrapper = replica as ControllerReplicaWrapper;
            Logger.TraceInformation(LogTag.Match, "Removing controller replica for controller {0} / {1}", controllerReplicaWrapper.Guid, controllerReplicaWrapper.MatchID);
            controllerReplicaWrapper.Release();
            this.controllerReplicaWrappersByEntityID.Remove(controllerReplicaWrapper.Guid);
        }

        /// <summary>
        /// Register a controller original with the replication system, so the local controller's state will be replicated.
        /// </summary>
        /// <param name="match">The match the controller is for.</param>
        private void TakeoverControllerAsOriginal(Match match)
        {
            object controller;
            if (!this.controllers.TryGetValue(match.MatchIdentifier, out controller))
            {
                Logger.TraceInformation(LogTag.Match, "No controller to take over.");
                return;
            }

            if (!this.TryClaimControlOfController(match.MatchIdentifier, match.MembershipReporter.Certificate.Timestamp))
            {
                return;
            }

            this.ReleaseControllerFromReplica(match.MatchIdentifier);

            var controllerID = this.identifierAllocator.GetNextUniqueId();
            var original = this.StartPosingAsIOriginal(controller);
            var controllerOriginalWrapper = new ControllerOriginalWrapper(
                                                            original,
                                                            controllerID,
                                                            match.MembershipReporter,
                                                            this.replicationEntityManager);

            this.controllerOriginalWrappersByMatchID.Add(match.MatchIdentifier, controllerOriginalWrapper);
            this.controllerOriginalWrappersByEntityID.Add(controllerID, controllerOriginalWrapper);
        }

        /// <summary>
        /// Unregister a match controller.
        /// </summary>
        /// <param name="match">The match.</param>
        private void UnregisterController(Match match)
        {
            IOriginal dummyOriginal;
            if (this.dummyOriginals.TryGetValue(match.MatchIdentifier, out dummyOriginal))
            {
                this.replicationEntityManager.UnregisterEntity(dummyOriginal, true);
                this.dummyOriginals.Remove(match.MatchIdentifier);
            }

            if (this.controllers.ContainsKey(match.MatchIdentifier))
            {
                var controllerAutoReplica = this.controllerAutoReplicaWrappersByMatchID[match.MatchIdentifier];
                this.StopPosingAsIReplica(controllerAutoReplica);
                this.controllerAutoReplicaWrappersByMatchID.Remove(match.MatchIdentifier);
                this.controllerAutoReplicaWrappersByMemberID.Remove(match.MemberIdentity.ID);

                this.ReleaseControllerFromReplica(match.MatchIdentifier);
                this.ReleaseControllerFromOriginal(match.MatchIdentifier);

                this.controllers.Remove(match.MatchIdentifier);
            }
        }

        /// <summary>
        /// Creates a replica wrapper for the controller (populated with controller on deserialization).
        /// </summary>
        /// <param name="entityId">The entity id.</param>
        /// <param name="entityTypeId">Entity type id.</param>
        /// <returns>Returns IReplica.</returns>
        private IReplica CreateHostedReplica(BadumnaId entityId, EntityTypeId entityTypeId)
        {
            HostedReplicaWrapper hostedReplicaWrapper;
            if (!this.hostedReplicaWrappersByGuid.TryGetValue(entityId, out hostedReplicaWrapper))
            {
                hostedReplicaWrapper = new HostedReplicaWrapper(entityId, entityTypeId.Id, this.eventQueue, this, this.queueApplicationEvent);
                this.hostedReplicaWrappersByGuid.Add(entityId, hostedReplicaWrapper);
            }

            return hostedReplicaWrapper;
        }

        /// <summary>
        /// Remove controller replica.
        /// </summary>
        /// <param name="replica">Replica to remove.</param>
        private void RemoveHostedReplica(IReplica replica)
        {
            var hostedReplicaWrapper = replica as HostedReplicaWrapper;
            Logger.TraceInformation(LogTag.Match, "Removing hosted replica {0} - {1}.", hostedReplicaWrapper.PersistentID, hostedReplicaWrapper.Guid);
            if (hostedReplicaWrapper.RemoveFromMatch())
            {
                Logger.TraceInformation(LogTag.Match, "Hosted replica WAS in control.");
                if (hostedReplicaWrapper.Match != null)
                {
                    this.RemoveHostedEntity(hostedReplicaWrapper.HostedEntity, hostedReplicaWrapper.Match.MatchIdentifier);
                }
                else
                {
                    Logger.TraceWarning(LogTag.Match, "Removing hosted replica that doesn't have associated match!");
                }
            }

            this.hostedReplicaWrappersByGuid.Remove(replica.Guid);
        }

        /// <summary>
        /// Remove a hosted entity and all associated wrappers.
        /// </summary>
        /// <param name="entity">The hosted entity.</param>
        /// <param name="matchID">The ID of the match it belongs to.</param>
        private void RemoveHostedEntity(object entity, BadumnaId matchID)
        {
            var pid = this.persistentIDsByHostedEntity[entity];
            this.ReleaseHostedEntityFromReplica(pid);
            this.ReleaseHostedEntityFromOriginal(pid, false);
            this.persistentIDsByHostedEntity.Remove(entity);
            this.hostedEntitiesByPersistentID.Remove(pid);
            this.hostedEntityTypesByPersistentID.Remove(pid);
            List<object> hostedEntities;
            lock (this.hostedEntitiesByMatchIDLock)
            {
                hostedEntities = this.hostedEntitiesByMatchID[matchID];
            }

            var indexToRemove = hostedEntities.FindIndex(e => object.ReferenceEquals(e, entity));
            hostedEntities.RemoveAt(indexToRemove);

            // Only remove list from dictionary when match has been left to avoid recreation.
        }

        /// <summary>
        /// Test if a claimant can takeover hosting of a given entity based on hosting certificate timestamp,
        /// recording claimant's timestamp for future tests if successful.
        /// </summary>
        /// <param name="persistentID">The persistent ID of the hosted entity.</param>
        /// <param name="timestamp">The timestamp of the new claimant's hosting certificate.</param>
        /// <returns><c>true</c> if the new claimant can takeover, otherwise <c>false</c>.</returns>
        private bool TryClaimControlOfHostedEntity(BadumnaId persistentID, TimeSpan timestamp)
        {
            TimeSpan currentTimestamp;
            if (this.hostedEntityCertificateTimestamps.TryGetValue(persistentID, out currentTimestamp))
            {
                if (currentTimestamp > timestamp)
                {
                    Logger.TraceWarning(LogTag.Match, "Rejecting attempt to take over entity hosted by more recent host.");
                    return false;
                }
            }

            Logger.TraceInformation(LogTag.Match, "Allowing attempt to take over hosted entity.");
            this.hostedEntityCertificateTimestamps[persistentID] = timestamp;
            return true;
        }

        /// <summary>
        /// Test if a claimant can takeover a match controller based on hosting certificate timestamp,
        /// recording claimant's timestamp for future tests if successful.
        /// </summary>
        /// <param name="matchID">The ID of the match the controller belongs to.</param>
        /// <param name="timestamp">The timestamp of the new claimant's hosting certificate.</param>
        /// <returns><c>true</c> if the new claimant can takeover, otherwise <c>false</c>.</returns>
        private bool TryClaimControlOfController(BadumnaId matchID, TimeSpan timestamp)
        {
            TimeSpan currentTimestamp;
            if (this.controllerCertificateTimestamps.TryGetValue(matchID, out currentTimestamp))
            {
                if (currentTimestamp > timestamp)
                {
                    Logger.TraceWarning(LogTag.Match, "Rejecting attempt to take over controller by more recent host.");
                    return false;
                }
            }

            Logger.TraceInformation(LogTag.Match, "Allowing attempt to take over controller.");
            this.controllerCertificateTimestamps[matchID] = timestamp;
            return true;
        }

        /// <summary>
        /// Begin hosting an entity on this peer.
        /// </summary>
        /// <param name="membershipReporter">The match's membership reporter.</param>
        /// <param name="persistentID">The persistent ID of the hosted entity.</param>
        /// <param name="entityType">Type of the entity.</param>
        private void TakeOverHostedEntityAsOriginal(IMembershipReporter membershipReporter, BadumnaId persistentID, uint entityType)
        {
            if (!this.TryClaimControlOfHostedEntity(persistentID, membershipReporter.Certificate.Timestamp))
            {
                return;
            }

            this.ReleaseHostedEntityFromReplica(persistentID);

            var entity = this.hostedEntitiesByPersistentID[persistentID];
            var original = this.StartPosingAsIOriginal(entity);
            var hostedOriginalWrapper = new HostedOriginalWrapper(
                                            original,
                                            this.identifierAllocator.GetNextUniqueId(),
                                            persistentID,
                                            entityType,
                                            membershipReporter,
                                            this.replicationEntityManager);
            this.hostedOriginalWrappersByPersistentID[persistentID] = hostedOriginalWrapper;
            this.hostedOriginalWrappersByGuid[hostedOriginalWrapper.Guid] = hostedOriginalWrapper;
        }

        /// <summary>
        /// Stop hosting an entity on this peer.
        /// </summary>
        /// <param name="persistentID">The persistent ID of the hosted entity.</param>
        /// <param name="removeReplicas">A value indicating whether to notify other peers to remove replicas.</param>
        private void ReleaseHostedEntityFromOriginal(BadumnaId persistentID, bool removeReplicas)
        {
            Logger.TraceInformation(LogTag.Match, "Trying to release hosted entity from original.");
            HostedOriginalWrapper hostedOriginalWrapper;
            if (this.hostedOriginalWrappersByPersistentID.TryGetValue(persistentID, out hostedOriginalWrapper))
            {
                Logger.TraceInformation(LogTag.Match, "Releasing hosted entity from original.");
                this.hostedOriginalWrappersByGuid.Remove(hostedOriginalWrapper.Guid);
                this.hostedOriginalWrappersByPersistentID.Remove(persistentID);

                this.StopPosingAsIOriginal(this.hostedEntitiesByPersistentID[persistentID]);
                hostedOriginalWrapper.ReleaseEntity(removeReplicas);
            }
        }

        /// <summary>
        /// Release a given hosted entity from any replica that may be controlling it.
        /// </summary>
        /// <param name="persistentID">The persistent ID of the hosted entity.</param>
        private void ReleaseHostedEntityFromReplica(BadumnaId persistentID)
        {
            Logger.TraceInformation(LogTag.Match, "Trying to release hosted entity from replica.");
            HostedReplicaWrapper oldReplicaWrapper;
            if (this.hostedReplicaWrappersByPersistentId.TryGetValue(persistentID, out oldReplicaWrapper))
            {
                Logger.TraceInformation(LogTag.Match, "Releasing hosted entity from replica.");
                oldReplicaWrapper.ReleaseEntity();
                this.hostedReplicaWrappersByPersistentId.Remove(persistentID);
            }
        }

        /// <summary>
        /// Stop controlling a match controller on this peer.
        /// </summary>
        /// <param name="matchID">The ID of the match the controller belongs to.</param>
        private void ReleaseControllerFromOriginal(BadumnaId matchID)
        {
            Logger.TraceInformation(LogTag.Match, "Trying to release controller from original.");
            ControllerOriginalWrapper controllerOriginalWrapper;
            if (this.controllerOriginalWrappersByMatchID.TryGetValue(matchID, out controllerOriginalWrapper))
            {
                Logger.TraceInformation(LogTag.Match, "Releasing controller from original.");

                this.StopPosingAsIOriginal(this.controllers[matchID]);
                controllerOriginalWrapper.Release();
                this.controllerOriginalWrappersByMatchID.Remove(matchID);
                this.controllerOriginalWrappersByEntityID.Remove(controllerOriginalWrapper.Guid);
            }
        }

        /// <summary>
        /// Release a given controller from any replica that may be controlling it.
        /// </summary>
        /// <param name="matchID">The ID of the match the controller belongs to.</param>
        private void ReleaseControllerFromReplica(BadumnaId matchID)
        {
            Logger.TraceInformation(LogTag.Match, "Trying to release controller from replica.");
            ControllerReplicaWrapper controllerReplicaWrapper;
            if (this.controllerReplicaWrappersByMatchID.TryGetValue(matchID, out controllerReplicaWrapper))
            {
                Logger.TraceInformation(LogTag.Match, "Releasing controller from replica.");
                controllerReplicaWrapper.Release();
                this.controllerReplicaWrappersByMatchID.Remove(matchID);
            }
        }

        /// <summary>
        /// Dummy original for other entities to address their updates to.
        /// </summary>
        private class DummyOriginal : IOriginal
        {
            /// <summary>
            /// Initializes a new instance of the EntityManager.DummyOriginal class.
            /// </summary>
            /// <param name="memberId">The ID of the match member used for
            /// interest notifications / addressing entity updates.</param>
            public DummyOriginal(BadumnaId memberId)
            {
                this.Guid = memberId;
            }

            /// <summary>
            /// Gets or sets the ID for the dummy original (must be the ID of the match
            /// member used for interest notifications / addressing entity updates.
            /// </summary>
            public BadumnaId Guid { get; set; }

            /// <summary>
            /// There's nothing to serialize, as it's a dummy original.
            /// </summary>
            /// <param name="requiredParts">Part list is ignored</param>
            /// <param name="stream">Stream is not written to.</param>
            /// <returns>An empty part list, to revent resending.</returns>
            public BooleanArray Serialize(BooleanArray requiredParts, Stream stream)
            {
                requiredParts.SetAll(false);
                return requiredParts;
            }

            /// <summary>
            /// Dummy originals do not support custom messages.
            /// </summary>
            /// <param name="stream">The stream.</param>
            public void HandleEvent(Stream stream)
            {
                throw new NotImplementedException();
            }
        }
    }
}
