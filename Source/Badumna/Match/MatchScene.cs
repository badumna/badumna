﻿// -----------------------------------------------------------------------
// <copyright file="MatchScene.cs" company="Scalify">
// Copyright 2013 Scalify Pty Ltd.
// </copyright>
// -----------------------------------------------------------------------

namespace Badumna.Match
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// A scene where all original entities are replicated to each other.
    /// </summary>
    internal class MatchScene
    {
        /////// <summary>
        /////// For managing entites.
        /////// </summary>
        ////private MatchEntityManager entityManager;

        ////public MatchScene()
        ////    : this(new MatchEntityManager())
        ////{
        ////}

        ////public MatchScene(MatchEntityManager entityManager)
        ////{
        ////    this.entityManager = entityManager;
        ////}
    }
}
