﻿//-----------------------------------------------------------------------
// <copyright file="Done.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2013 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.Match.MembershipState
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// State when the peer is not connected to a host, and has given up trying.
    /// </summary>
    internal class Done : Base
    {
        /// <summary>
        /// Initializes a new instance of the Done class.
        /// </summary>
        /// <param name="data">Data required for membership management.</param>
        public Done(MembershipData data)
            : base(data)
        {
        }

        /// <inheritdoc/>
        public override void OnEnter()
        {
            base.OnEnter();
        }
    }
}
