﻿//-----------------------------------------------------------------------
// <copyright file="Hosting.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2013 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Badumna.Match.MembershipState
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Badumna.DataTypes;
    using Badumna.Utilities;

    /// <summary>
    /// State for when this peer is acting as host for the match.
    /// </summary>
    internal class Hosting : Active
    {
        /// <summary>
        /// The period at which to ping the matchmaking server.
        /// </summary>
        private readonly TimeSpan pingInterval = TimeSpan.FromSeconds(3);

        /// <summary>
        /// Period to wait after last ping before timing out a client.
        /// </summary>
        private readonly TimeSpan clientTimeout = TimeSpan.FromSeconds(6);

        /// <summary>
        /// List to store snapshot of members (re-use to prevent allocation).
        /// </summary>
        private readonly List<MemberIdentity> memberSnapshot;

        /// <summary>
        /// Maps clients to the time of their most recent ping.
        /// </summary>
        private readonly Dictionary<BadumnaId, TimeSpan> clientPingTimes =
            new Dictionary<BadumnaId, TimeSpan>();

        /// <summary>
        /// The time the matchmaker was last pinged.
        /// </summary>
        private TimeSpan lastPing;

        /// <summary>
        /// Initializes a new instance of the Hosting class.
        /// </summary>
        /// <param name="data">Data required for membership management.</param>
        public Hosting(MembershipData data)
            : base(data)
        {
            this.memberSnapshot = new List<MemberIdentity>(this.Data.MaxPlayers);
        }

        /// <inheritdoc />
        public override void OnEnter()
        {
            base.OnEnter();            
            this.clientPingTimes.Clear();
            bool alreadyMember = false;
            foreach (var member in this.Data.Members)
            {
                if (member == this.Data.MemberIdentity)
                {
                    alreadyMember = true;
                    continue;
                }

                this.clientPingTimes[member.ID] = this.TimeEntered;
            }

            // It is be possible to become host when not a member.
            // In that scenario, no other member should be known, so
            // there will always be room for oneself.
            if (!alreadyMember)
            {
                this.Data.AddMember(this.Data.MemberIdentity);
            }

            this.PingMatchmaker(this.TimeEntered);
        }

        /// <inheritdoc />
        public override Base Update(TimeSpan currentTime)
        {
            if (currentTime - this.lastPing >= this.pingInterval)
            {
                this.PingMatchmaker(currentTime);
            }

            // TODO: drop frequency of timeout checks?
            this.memberSnapshot.Clear();
            this.memberSnapshot.InsertRange(0, this.Data.Members);
            foreach (var member in this.memberSnapshot)
            {
                if (member == this.Data.MemberIdentity)
                {
                    continue;
                }

                if (currentTime - this.clientPingTimes[member.ID] > this.clientTimeout)
                {
                    this.RemoveMember(member.ID);
                }
            }

            return this;
        }

        /// <inheritdoc />
        public override Base Leave()
        {
            foreach (var member in this.Data.Members)
            {
                this.Data.MatchProtocol.HostResignation(member.ID, this.Data.Certificate, this.Data.TakeoverCode);
            }

            return base.Leave();
        }

        /// <inheritdoc />
        public override Base OnJoinRequest(MemberIdentity clientIdentity, BadumnaId matchIdentifier)
        {
            // The requestor may already be a member if they lost connection and tried to rejoin before the host
            // realized they had left.
            if (this.Data.IsMember(clientIdentity))
            {
                this.ConfirmMembership(clientIdentity);
            }
            else
            {
                this.OnJoinAttempt(clientIdentity);
            }

            return this;
        }

        /// <inheritdoc />
        public override Base OnPingFromClientToHost(MemberIdentity clientIdentity, BadumnaId matchIdentifier)
        {
            // The pinger may not be a member if the host timed them out due to lack of pings, but they didn't realize.
            if (this.Data.IsMember(clientIdentity))
            {
                // If this is the first ping received since this peer took over hosting, then we need to resend
                // a join confirmation to confirm the current member set.
                if (this.clientPingTimes[clientIdentity.ID] == this.TimeEntered)
                {
                    this.ConfirmMembership(clientIdentity);
                }
                else
                {
                    this.clientPingTimes[clientIdentity.ID] = this.Data.TimeKeeper.Now;
                }
            }
            else
            {
                // Treat pings from non-members as join requests.
                this.OnJoinAttempt(clientIdentity);
            }

            return this;
        }

        /// <inheritdoc />
        public override Base OnLeaveNotification(BadumnaId client, BadumnaId matchIdentifier)
        {
            if (this.Data.IsMember(client))
            {
                this.RemoveMember(client);
            }

            return this;
        }

        /// <inheritdoc />
        public override Base OnPingFromMatchmaker()
        {
            this.PingMatchmaker(this.Data.TimeKeeper.Now);
            return this;
        }

        /// <summary>
        /// Ping the matchmaker to confirm hosting rights and report latest status.
        /// </summary>
        /// <param name="currentTime">The current time.</param>
        private void PingMatchmaker(TimeSpan currentTime)
        {
            var capacity = new MatchCapacity(this.Data.MaxPlayers, this.Data.FreeSlots);
            this.Data.Matchmaker.HostingRequest(
                this.Data.MemberIdentity.ID, this.Data.Certificate.MatchIdentifier, this.Data.Criteria, capacity, this.Data.TakeoverCode);
            this.lastPing = currentTime;
        }

        /// <summary>
        /// Confirm to a client that they are in the match. 
        /// </summary>
        /// <param name="clientIdentity">The ID of the client.</param>
        private void ConfirmMembership(MemberIdentity clientIdentity)
        {
            this.Data.MatchProtocol.JoinConfirmation(clientIdentity.ID, this.Data.Certificate, this.Data.Members);
            this.clientPingTimes[clientIdentity.ID] = this.Data.TimeKeeper.Now;
        }

        /// <summary>
        /// Let a member join the match if there is room.
        /// </summary>
        /// <param name="clientIdentity">The ID of the client seeking to join.</param>
        private void OnJoinAttempt(MemberIdentity clientIdentity)
        {
            if (this.Data.FreeSlots > 0)
            {
                this.Data.AddMember(clientIdentity);
                this.clientPingTimes[clientIdentity.ID] = this.Data.TimeKeeper.Now;
                this.Data.MatchProtocol.JoinConfirmation(clientIdentity.ID, this.Data.Certificate, this.Data.Members);
                foreach (var member in this.Data.Members)
                {
                    if (member != this.Data.MemberIdentity &&
                        member != clientIdentity)
                    {
                        this.Data.MatchProtocol.MemberJoinNotification(member.ID, this.Data.Certificate, clientIdentity);
                    }
                }
            }
            else
            {
                this.Data.MatchProtocol.JoinRejection(clientIdentity.ID, this.Data.Certificate);
            }
        }

        /// <summary>
        /// Remove a member from the match.
        /// </summary>
        /// <param name="memberToRemove">The member to remove.</param>
        private void RemoveMember(BadumnaId memberToRemove)
        {
            foreach (var member in this.Data.Members)
            {
                if (member != this.Data.MemberIdentity.ID &&
                    member != memberToRemove)
                {
                    this.Data.MatchProtocol.MemberLeaveNotification(
                        member.ID,
                        this.Data.Certificate,
                        memberToRemove);
                }
            }

            this.clientPingTimes.Remove(memberToRemove);
            this.Data.RemoveMember(memberToRemove);
        }
    }
}
