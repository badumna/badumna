﻿//-----------------------------------------------------------------------
// <copyright file="Base.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2013 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.Match.MembershipState
{
    using System;
    using System.Collections.Generic;
    using Badumna.Core;
    using Badumna.DataTypes;
    using Badumna.Utilities;

    /// <summary>
    /// Base class for membership states.
    /// </summary>
    internal class Base
    {
        /// <summary>
        /// Data required for membership management. 
        /// </summary>
        private readonly MembershipData data;

        /// <summary>
        /// The time the state was entered (for timeouts).
        /// </summary>
        private TimeSpan timeEntered;

        /// <summary>
        /// Initializes a new instance of the Base class.
        /// </summary>
        /// <param name="data">The data belonging to the owning MembershipManager.</param>
        public Base(MembershipData data)
        {
            if (data == null)
            {
                throw new ArgumentNullException("data");
            }

            this.data = data;
        }

        /// <summary>
        /// Gets the owning membership manager's data.
        /// </summary>
        protected MembershipData Data
        {
            get { return this.data; }
        }

        /// <summary>
        /// Gets the time the current state was entered.
        /// </summary>
        protected TimeSpan TimeEntered
        {
            get { return this.timeEntered; }
        }

        /// <summary>
        /// Called when the state is first entered.
        /// </summary>
        public virtual void OnEnter()
        {
            Logger.TraceInformation(LogTag.Match, "Entered state: {0}", this.GetType());
            this.timeEntered = this.data.TimeKeeper.Now;
        }

        /// <summary>
        /// Called to trigger regular processing (pings, timeouts etc.).
        /// </summary>
        /// <param name="currentTime">The current game time.</param>
        /// <returns>The membership state after update.</returns>
        public virtual Base Update(TimeSpan currentTime)
        {
            return this;
        }

        /// <summary>
        /// Called when the application leaves match.
        /// </summary>
        /// <returns>The resulting state after the message has been handled.</returns>
        public virtual Base Leave()
        {
            return this;
        }

        /// <summary>
        /// Called on receipt of a host authorization cerificate from the server.
        /// </summary>
        /// <param name="certificate">The latest certificate of authorization for the match.</param>
        /// <param name="takeoverCode">The authorization code to takeover hosting of a match
        /// before the current host has timedout.</param>
        /// <returns>The resulting state after the message has been handled.</returns>
        public virtual Base OnHostingReply(HostAuthorizationCertificate certificate, int takeoverCode)
        {
            return this;
        }

        /// <summary>
        /// Called when a leave notification is received from another peer.
        /// </summary>
        /// <param name="client">The sender ID.</param>
        /// <param name="matchIdentifier">The match ID.</param>
        /// <returns>The resulting state after the message has been handled.</returns>
        public virtual Base OnLeaveNotification(BadumnaId client, BadumnaId matchIdentifier)
        {
            return this;
        }
        
        /// <summary>
        /// Called when notification is received from a new host that this member has successfully joined.
        /// </summary>
        /// <remarks>Receipt of this message implies the host considers the receiving peer a member.</remarks>
        /// <param name="certificate">Certificate authorizing a host for the match.</param>
        /// <param name="members">The set of members currently recognized by the host.</param>
        /// <returns>The resulting state after the message has been handled.</returns>
        public virtual Base OnJoinConfirmation(HostAuthorizationCertificate certificate, IEnumerable<MemberIdentity> members)
        {
            return this;
        }

        /// <summary>
        /// Called when peer sends a message denying that it is the host.
        /// </summary>
        /// <param name="sender">The ID of the member whose client ping is being replied to.</param>
        /// <param name="certificate">Latest certified host known to the peer.</param>
        /// <returns>The resulting state after the message has been handled.</returns>
        public virtual Base OnHostDenial(BadumnaId sender, HostAuthorizationCertificate certificate)
        {
            return this;
        }

        /// <summary>
        /// Called when the local peer's membership of the match has been rejected by the current host.
        /// </summary>
        /// <param name="certificate">Certificate authorizing the host for the match.</param>
        /// <returns>The resulting state after the message has been handled.</returns>
        public virtual Base OnJoinRejection(HostAuthorizationCertificate certificate)
        {
            return this;
        }

        /// <summary>
        /// Called when a new member notification has been received from a host.
        /// </summary>
        /// <param name="certificate">Certificate authorizing the host for the match.</param>
        /// <param name="memberIdentity">The identity of the new member.</param>
        /// <returns>The resulting state after the message has been handled.</returns>
        public virtual Base OnMemberJoinNotification(
            HostAuthorizationCertificate certificate,
            MemberIdentity memberIdentity)
        {
            return this;
        }

        /// <summary>
        /// Called when a member leave notification has been received from a host.
        /// </summary>
        /// <param name="certificate">Certificate authorizing the host for the match.</param>
        /// <param name="member">The member that left.</param>
        /// <returns>The resulting state after the message has been handled.</returns>
        public virtual Base OnMemberLeaveNotification(
            HostAuthorizationCertificate certificate,
            BadumnaId member)
        {
            return this;
        }

        /// <summary>
        /// Handle a resignation message from the host
        /// </summary>
        /// <param name="certificate">The resigning host's certificate.</param>
        /// <param name="takeoverCode">Password that allows other peers to takeover hosting before current host times out.</param>
        /// <returns>The resulting state after the message has been handled.</returns>
        public virtual Base OnHostResignation(HostAuthorizationCertificate certificate, int takeoverCode)
        {
            return this;
        }

        /// <summary>
        /// Called when a join-request has been received from another peer.
        /// </summary>
        /// <param name="clientIdentity">The sender identity.</param>
        /// <param name="matchIdentifier">The match ID.</param>
        /// <returns>The resulting state after the message has been handled.</returns>
        public virtual Base OnJoinRequest(MemberIdentity clientIdentity, BadumnaId matchIdentifier)
        {
            return this;
        }

        /// <summary>
        /// Called when a keep-alive has been received from another peer.
        /// </summary>
        /// <param name="clientIdentity">The sender identity.</param>
        /// <param name="matchIdentifier">The match ID.</param>
        /// <returns>The resulting state after the message has been handled.</returns>
        public virtual Base OnPingFromClientToHost(MemberIdentity clientIdentity, BadumnaId matchIdentifier)
        {
            return this;
        }

        /// <summary>
        /// Called to notify on connection failures.
        /// </summary>
        /// <param name="address">The address of the peer to whom a connection has failed.</param>
        /// <returns>The resulting state after the message has been handled.</returns>
        public virtual Base OnConnectionLost(PeerAddress address)
        {
            return this;
        }

        /// <summary>
        /// Called when another peer has pinged to check the local peer is still in the match.
        /// </summary>
        /// <param name="sender">The sender ID.</param>
        /// <param name="matchIdentifier">The match ID.</param>
        /// <returns>The resulting state after the message has been handled.</returns>
        public virtual Base OnPingToPotentialHost(BadumnaId sender, BadumnaId matchIdentifier)
        {
            return this;
        }

        /// <summary>
        /// Called when a response to a ping has been received.
        /// </summary>
        /// <param name="peer">The peer the pong is from.</param>
        /// <param name="inMatch">A value indicating whether the peer still wants to be in the match.</param>
        /// <param name="recognizesThisPeer">A value indicating whether the peer recognizes this peer as a member of
        /// the match.</param>
        /// <param name="isConnected">A value indicating whether the peer is still connected to a host.</param>
        /// <param name="certificate">Latest certificate for the match known of by the peer.</param>
        /// <returns>The resulting state after the message has been handled.</returns>
        public virtual Base OnPongFromPotentialHost(
            BadumnaId peer,
            bool inMatch,
            bool recognizesThisPeer,
            bool isConnected,
            HostAuthorizationCertificate certificate)
        {
            return this;
        }

        /// <summary>
        /// Called on receipt of a ping from the matchmaker.
        /// </summary>
        /// <returns>The resulting state after the message has been handled.</returns>
        public virtual Base OnPingFromMatchmaker()
        {
            return this;
        }

        /// <summary>
        /// Check a certificate is applies to the current match, and is not outranked
        /// by a known certificate.
        /// </summary>
        /// <param name="certificate">The certificate to validate.</param>
        /// <returns><c>true</c> if the certificate is valid, otherwise <c>false</c>.</returns>
        protected bool CertificateIsValid(HostAuthorizationCertificate certificate)
        {
            return certificate.AppliesToSameMatch(this.data.Certificate) &&
                certificate.IsNotOutrankedBy(this.data.Certificate);
        }
    }
}
