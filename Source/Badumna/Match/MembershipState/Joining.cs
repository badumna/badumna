﻿//-----------------------------------------------------------------------
// <copyright file="Joining.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2013 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.Match.MembershipState
{
    using System;
    using Badumna.DataTypes;
    using Badumna.Utilities;

    /// <summary>
    /// State where peer has an address for a host and will try and connect to it.
    /// </summary>
    internal class Joining : Active
    {
        /// <summary>
        /// Period to wait in joining state before giving up.
        /// </summary>
        private readonly TimeSpan timeout = TimeSpan.FromSeconds(5);

        /// <summary>
        /// Initializes a new instance of the Joining class.
        /// </summary>
        /// <param name="data">Data required for membership management.</param>
        public Joining(MembershipData data)
            : base(data)
        {
        }

        /// <inheritdoc/>
        public override void OnEnter()
        {
            base.OnEnter();
            this.Data.MatchProtocol.PingFromClientToHost(
                this.Data.Certificate.Host, this.Data.MemberIdentity, this.Data.Certificate.MatchIdentifier);
        }

        /// <inheritdoc/>
        public override Base Update(TimeSpan currentTime)
        {
            if (currentTime - this.TimeEntered >= this.timeout)
            {
                Logger.TraceInformation(LogTag.Match, "Timing out attempt to join host {0}.", this.Data.Certificate.Host);
                return this.OnJoinFailure();
            }

            return this;
        }

        /// <inheritdoc/>
        public override Base OnHostDenial(BadumnaId sender, HostAuthorizationCertificate certificate)
        {
            if (sender == this.Data.Certificate.Host)
            {
                if (certificate.IsNotOutrankedBy(this.Data.Certificate))
                {
                    // Try again with new host.
                    this.Data.Certificate = certificate;
                    this.OnEnter();
                }
            }

            return this;
        }

        /// <inheritdoc/>
        public override Base OnConnectionLost(Core.PeerAddress address)
        {
            if (address.Equals(this.Data.Certificate.Host.Address))
            {
                Logger.TraceInformation(LogTag.Match, "Lost connection to host {0}.", this.Data.Certificate.Host);
                return this.OnJoinFailure();
            }

            return this;
        }

        /// <summary>
        /// Handle failure to join the host.
        /// </summary>
        /// <returns>The state to transition to.</returns>
        private Base OnJoinFailure()
        {
            // If this is the 2nd consecutive failed attempt to connect to this host,
            // or there are no other members in the match, then give up.
            if (this.Data.LastHostAttemptedToJoinIfLastJoinFailed == this.Data.Certificate.Host ||
                this.Data.Members.Count == 0)
            {
                this.Data.Error = MatchError.CannotConnectToHost;
                return this.Data.Done;
            }

            this.Data.LastHostAttemptedToJoinIfLastJoinFailed = this.Data.Certificate.Host;
            return this.Data.AwaitingHost;
        }
    }
}
