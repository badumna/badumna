﻿//-----------------------------------------------------------------------
// <copyright file="Connected.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2013 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.Match.MembershipState
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Badumna.DataTypes;
    using Badumna.Utilities;

    /// <summary>
    /// State where peer is accepted as a member by a host.
    /// </summary>
    internal class Connected : Active
    {
        /// <summary>
        /// Period at which to ping the host with keep-alive messages.
        /// </summary>
        private readonly TimeSpan pingInterval = TimeSpan.FromSeconds(2);

        /// <summary>
        /// The last time a ping was sent to the host.
        /// </summary>
        private TimeSpan lastPing;

        /// <summary>
        /// Initializes a new instance of the Connected class.
        /// </summary>
        /// <param name="data">Data required for membership management.</param>
        public Connected(MembershipData data)
            : base(data)
        {
        }

        /// <inheritdoc/>
        public override void OnEnter()
        {
            base.OnEnter();
            this.PingHost();
            this.Data.LastHostAttemptedToJoinIfLastJoinFailed = null;
        }

        /// <inheritdoc/>        
        public override Base Update(TimeSpan currentTime)
        {
            if (currentTime - this.lastPing >= this.pingInterval)
            {
                this.PingHost();
            }

            return this;
        }

        /// <inheritdoc />
        public override Base Leave()
        {
            this.Data.MatchProtocol.LeaveNotification(
                this.Data.Certificate.Host,
                this.Data.MemberIdentity.ID,
                this.Data.Certificate.MatchIdentifier);
            return base.Leave();
        }

        /// <inheritdoc/>        
        public override Base OnMemberJoinNotification(
            HostAuthorizationCertificate certificate,
            MemberIdentity member)
        {
            return this.OnMemberAdded(certificate, member);
        }

        /// <inheritdoc/>        
        public override Base OnMemberLeaveNotification(
            HostAuthorizationCertificate certificate,
            BadumnaId memberID)
        {
            return this.OnMemberRemoved(certificate, memberID);
        }

        /// <inheritdoc/>
        public override Base OnHostResignation(HostAuthorizationCertificate certificate, int takeoverCode)
        {
            if (certificate.Host == this.Data.Certificate.Host &&
                certificate.IsNotOutrankedBy(this.Data.Certificate))
            {
                this.Data.TakeoverCode = takeoverCode;
                this.Data.RemoveMember(this.Data.Certificate.Host);
                return this.Data.AwaitingHost;
            }

            return this;
        }

        /// <inheritdoc/>
        public override Base OnConnectionLost(Core.PeerAddress address)
        {
            if (address.Equals(this.Data.Certificate.Host.Address))
            {
                Logger.TraceInformation(LogTag.Match, "Lost connection to host.");
                return this.Data.AwaitingHost;
            }

            return this;
        }

        /// <inheritdoc />
        public override Base OnPingToPotentialHost(BadumnaId sender, BadumnaId matchIdentifier)
        {
            this.Data.MatchProtocol.PongFromPotentialHost(
                sender,
                this.Data.MemberIdentity.ID,
                true,
                this.Data.IsMember(sender),
                true,
                this.Data.Certificate);
            return this;
        }

        /// <summary>
        /// Send a keep-alive message to the host.
        /// </summary>
        private void PingHost()
        {
            this.lastPing = this.Data.TimeKeeper.Now;
            this.Data.MatchProtocol.PingFromClientToHost(
                this.Data.Certificate.Host, this.Data.MemberIdentity, this.Data.Certificate.MatchIdentifier);
        }

        /// <summary>
        /// Handle a member added message.
        /// </summary>
        /// <param name="certificate">The certificate from the sender.</param>
        /// <param name="memberIdentity">The identity of the member that has joined.</param>
        /// <returns>The resulting state.</returns> 
        private Base OnMemberAdded(
            HostAuthorizationCertificate certificate,
            MemberIdentity memberIdentity)
        {
            if (!this.CertificateIsValid(certificate))
            {
                return this;
            }

            // If the update is from a new host, then we need a full membership update,
            // so rejoin the host.
            if (certificate.Host != this.Data.Certificate.Host)
            {
                this.Data.Certificate = certificate;
                return this.Data.Joining;
            }

            this.Data.AddMember(memberIdentity);
            return this;
        }

        /// <summary>
        /// Handle a member removed message.
        /// </summary>
        /// <param name="certificate">The certificate from the sender.</param>
        /// <param name="memberID">The ID of the member that has left.</param>
        /// <returns>The resulting state.</returns> 
        private Base OnMemberRemoved(
            HostAuthorizationCertificate certificate,
            BadumnaId memberID)
        {
            if (!this.CertificateIsValid(certificate))
            {
                return this;
            }

            // If the update is from a new host, then we need a full membership update,
            // so rejoin the host.
            if (certificate.Host != this.Data.Certificate.Host)
            {
                this.Data.Certificate = certificate;
                return this.Data.Joining;
            }

            this.Data.RemoveMember(memberID);
            return this;
        }
    }
}
