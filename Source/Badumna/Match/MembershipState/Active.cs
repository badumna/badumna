﻿//-----------------------------------------------------------------------
// <copyright file="Active.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2013 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.Match.MembershipState
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Badumna.Core;
    using Badumna.DataTypes;

    /// <summary>
    /// Base class for membership states.
    /// </summary>
    internal class Active : Base
    {
        /// <summary>
        /// Initializes a new instance of the Active class.
        /// </summary>
        /// <param name="data">The data belonging to the owning MembershipManager.</param>
        public Active(MembershipData data)
            : base(data)
        {
        }

        /// <inheritdoc />
        public override Base Leave()
        {
            return this.Data.Done;
        }

        /// <inheritdoc />
        public override Base OnHostingReply(HostAuthorizationCertificate certificate, int takeoverCode)
        {
            if (this.CertificateIsValid(certificate))
            {
                this.Data.Certificate = certificate;
                if (certificate.Host == this.Data.MemberIdentity.ID)
                {
                    this.Data.TakeoverCode = takeoverCode;
                    return this.Data.Hosting;
                }
                else
                {
                    return this.Data.Joining;
                }
            }

            return this;
        }

        /// <inheritdoc />
        public override Base OnJoinConfirmation(HostAuthorizationCertificate certificate, IEnumerable<MemberIdentity> members)
        {
            if (this.CertificateIsValid(certificate))
            {
                this.Data.Certificate = certificate;
                this.Data.UpdateMembers(members);
                return this.Data.Connected;
            }

            return this;
        }

        /// <inheritdoc />
        public override Base OnHostDenial(BadumnaId sender, HostAuthorizationCertificate certificate)
        {
            if (sender == this.Data.Certificate.Host)
            {
                if (certificate.IsNotOutrankedBy(this.Data.Certificate))
                {
                    this.Data.Certificate = certificate;
                }

                return this.Data.Joining;
            }

            return this;
        }

        /// <inheritdoc />
        public override Base OnJoinRejection(HostAuthorizationCertificate certificate)
        {
            // Assume that we don't need to check certificate is current if it is from correct host?
            if (this.Data.Certificate.Host == certificate.Host)
            {
                this.Data.Error = MatchError.MatchFull;
                return this.Data.Done;
            }

            return this;
        }

        /// <inheritdoc />
        public override Base OnJoinRequest(MemberIdentity clientIdentity, BadumnaId matchIdentifier)
        {
            this.Data.MatchProtocol.HostDenial(clientIdentity.ID, this.Data.MemberIdentity.ID, this.Data.Certificate);
            return this;
        }

        /// <inheritdoc />
        public override Base OnPingFromClientToHost(MemberIdentity clientIdentity, BadumnaId matchIdentifier)
        {
            this.Data.MatchProtocol.HostDenial(clientIdentity.ID, this.Data.MemberIdentity.ID, this.Data.Certificate);
            return this;
        }

        /// <inheritdoc />
        public override Base OnConnectionLost(PeerAddress address)
        {
            return this;
        }

        /// <inheritdoc />
        public override Base OnPingToPotentialHost(BadumnaId sender, BadumnaId matchIdentifier)
        {
            this.Data.MatchProtocol.PongFromPotentialHost(
                sender,
                this.Data.MemberIdentity.ID,
                true,
                this.Data.IsMember(sender),
                false,
                this.Data.Certificate);
            return this;
        }
    }
}
