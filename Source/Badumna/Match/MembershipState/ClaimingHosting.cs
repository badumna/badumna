﻿//-----------------------------------------------------------------------
// <copyright file="ClaimingHosting.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2013 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.Match.MembershipState
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// State for when the peer is trying to take over hosting for the match
    /// (i.e. it has lost connection to the host, and doesn't know of a better
    /// candidate to take over).
    /// </summary>
    internal class ClaimingHosting : Active
    {
        /// <summary>
        /// The period to wait in this state without reply before giving up.
        /// </summary>
        private readonly TimeSpan timeout = TimeSpan.FromSeconds(10);

        /// <summary>
        /// Initializes a new instance of the ClaimingHosting class.
        /// </summary>
        /// <param name="data">Data required for membership management.</param>
        public ClaimingHosting(MembershipData data)
            : base(data)
        {
        }

        /// <inheritdoc />
        public override void OnEnter()
        {
            base.OnEnter();
            var status = new MatchCapacity(this.Data.MaxPlayers, this.Data.FreeSlots);
            this.Data.Matchmaker.HostingRequest(
                this.Data.MemberIdentity.ID, this.Data.Certificate.MatchIdentifier, this.Data.Criteria, status, this.Data.TakeoverCode);
        }

        /// <inheritdoc />
        public override Base OnConnectionLost(Core.PeerAddress address)
        {
            if (address.Equals(this.Data.ServerAddress))
            {
                this.Data.Error = MatchError.CannotConnectToMatchmaker;
                return this.Data.Done;
            }

            return this;
        }

        /// <inheritdoc />
        public override Base Update(TimeSpan currentTime)
        {
            if (currentTime - this.TimeEntered >= this.timeout)
            {
                this.Data.Error = MatchError.CannotConnectToMatchmaker;
                return this.Data.Done;
            }

            return this;
        }
    }
}
