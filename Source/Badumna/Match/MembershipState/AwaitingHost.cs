﻿//-----------------------------------------------------------------------
// <copyright file="AwaitingHost.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2013 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Badumna.Match.MembershipState
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Badumna.DataTypes;
    using Badumna.Utilities;

    /// <summary>
    /// State for when an existing connection to the host has been lost,
    /// and the peer is waiting for another member to take over hosting.
    /// </summary>
    internal class AwaitingHost : Active
    {
        /// <summary>
        /// Interval to ping potential hosts at.
        /// </summary>
        private TimeSpan pingInterval = TimeSpan.FromSeconds(1);

        /// <summary>
        /// How long to wait beofre giving waiting for a host.
        /// </summary>
        private TimeSpan waitingTimeout = TimeSpan.FromSeconds(10);

        /// <summary>
        /// Time potential hosts were last pinged.
        /// </summary>
        private TimeSpan lastPing;

        /// <summary>
        /// Peers that have a better claim to take over hosting than this peer,
        /// and an associated value indicating if they still maintain a connection
        /// to the current host.
        /// </summary>
        private Dictionary<BadumnaId, bool> preferredHosts = new Dictionary<BadumnaId, bool>();

        /// <summary>
        /// Initializes a new instance of the AwaitingHost class.
        /// </summary>
        /// <param name="data">Data required for membership management.</param>
        public AwaitingHost(MembershipData data)
            : base(data)
        {
        }

        /// <inheritdoc/>
        public override void OnEnter()
        {
            base.OnEnter();
            this.preferredHosts.Clear();
            foreach (var peer in this.Data.Members)
            {
                if (this.Data.MemberIdentity.ID.CompareTo(peer.ID) > 0 &&
                    this.Data.Certificate.Host != peer.ID)
                {
                    // Assume other peers are not still connected until told otherwise.
                    this.preferredHosts.Add(peer.ID, false);
                }
            }

            this.PingPreferredHosts();

            // TODO: Notify application.
        }

        /// <inheritdoc />
        public override Base Update(TimeSpan currentTime)
        {
            if (this.preferredHosts.Count == 0)
            {
                return this.Data.ClaimingHosting;
            }

            if (currentTime - this.TimeEntered > this.waitingTimeout)
            {
                // If a preferred peer is still connected to the host, try and rejoin.
                foreach (var val in this.preferredHosts.Values)
                {
                    if (val)
                    {
                        return this.Data.Joining;
                    }
                }

                this.Data.Error = MatchError.CannotConnectToHost;
                return this.Data.Done;
            }

            if (currentTime - this.lastPing > this.pingInterval)
            {
                this.PingPreferredHosts();
            }

            return this;
        }

        /// <inheritdoc />
        public override Base OnConnectionLost(Core.PeerAddress address)
        {
            foreach (var peer in this.preferredHosts.Keys)
            {
                if (address.Equals(peer.Address))
                {
                    return this.DropPreferredHost(peer);
                }
            }

            return this;
        }

        /// <inheritdoc />
        public override Base OnPongFromPotentialHost(
            BadumnaId peer,
            bool inMatch,
            bool recognizesThisPeer,
            bool isConnected,
            HostAuthorizationCertificate certificate)
        {
            if (!this.preferredHosts.ContainsKey(peer))
            {
                return this;
            }

            if (!inMatch)
            {
                return this.DropPreferredHost(peer);
            }

            if (certificate != null)
            {
                if (certificate.Host == this.Data.Certificate.Host)
                {
                    this.preferredHosts[peer] = isConnected;
                }
                else if (certificate.Timestamp > this.Data.Certificate.Timestamp)
                {
                    this.Data.Certificate = certificate;
                    return this.Data.Joining;
                }
            }

            if (!recognizesThisPeer)
            {
                // If the peer doesn't recognize us as a member, it's not much good for us,
                // so don't wait for it to take over.
                return this.DropPreferredHost(peer);
            }

            return this;
        }

        /// <summary>
        /// Send a ping to each preferred host to check they still want to be in the match.
        /// </summary>
        private void PingPreferredHosts()
        {
            this.lastPing = this.Data.TimeKeeper.Now;
            foreach (var peer in this.preferredHosts.Keys)
            {
                this.Data.MatchProtocol.PingToPotentialHost(peer, this.Data.MemberIdentity.ID, this.Data.Certificate.MatchIdentifier);
            }
        }

        /// <summary>
        /// Remove a peer from the list of preferred hosts (because they are no longer
        /// in the match or the connection to them has failed).
        /// </summary>
        /// <param name="peer">The peer to remove.</param>
        /// <returns>The state to transition to afterwards.</returns>
        private Base DropPreferredHost(BadumnaId peer)
        {
            Logger.TraceInformation(LogTag.Match, "Dropping potential host {0}.", peer);
            this.preferredHosts.Remove(peer);
            if (this.preferredHosts.Count == 0)
            {
                return this.Data.ClaimingHosting;
            }

            return this;
        }
    }
}
