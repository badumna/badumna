﻿//-----------------------------------------------------------------------
// <copyright file="IMembershipReporter.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2013 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.Match
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Badumna.DataTypes;

    /// <summary>
    /// Delegate type for host change notifications;
    /// </summary>
    /// <param name="isHost">A value indicating if the local member is now the host.</param>
    internal delegate void OnHostChanged(bool isHost);

    /// <summary>
    /// Interface for providing membership change notifications for a match.
    /// </summary>
    internal interface IMembershipReporter
    {
        /// <summary>
        /// Raised when the match host has changed.
        /// </summary>
        event OnHostChanged HostChanged;

        /// <summary>
        /// Raised when a new member joins the match.
        /// </summary>
        event GenericEventHandler<IMembershipReporter, MatchMembershipEventArgs> MemberAdded;

        /// <summary>
        /// Raised when an existing member leaves the match.
        /// </summary>
        event GenericEventHandler<IMembershipReporter, MatchMembershipEventArgs> MemberRemoved;

        /// <summary>
        /// Raised when a chat message has been recieved.
        /// </summary>
        event GenericEventHandler<IMembershipReporter, MatchChatEventArgs> ChatMessageReceived;

        /// <summary>
        /// Gets the current members of the match.
        /// </summary>
        IEnumerable<MemberIdentity> Members { get; }

        /// <summary>
        /// Gets the match identifier.
        /// </summary>
        BadumnaId MatchIdentifier { get; }

        /// <summary>
        /// Gets the current host authorization certificate.
        /// </summary>
        HostAuthorizationCertificate Certificate { get; }

        /// <summary>
        /// Gets a value indicating whether the local member is currently hosting the match.
        /// </summary>
        bool IsHost { get; }
    }
}
