﻿//-----------------------------------------------------------------------
// <copyright file="MatchError.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2013 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.Match
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// Enumeration of possible match errors.
    /// </summary>
    public enum MatchError
    {
        /// <summary>
        /// No error encountered.
        /// </summary>
        None = 0,

        /// <summary>
        /// This peer is unable to connect to the host.
        /// </summary>
        CannotConnectToHost,

        /// <summary>
        /// This peer is unable to join the match because it is full.
        /// </summary>
        MatchFull,

        /// <summary>
        /// This peer is unable to connect to the matchmaker.
        /// </summary>
        CannotConnectToMatchmaker
    }
}
