﻿//-----------------------------------------------------------------------
// <copyright file="IMatchProtocol.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2013 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.Match
{
    using System.Collections.Generic;
    using System.IO;
    using Badumna.DataTypes;

    /// <summary>
    /// Messages that can be sent between peers for matches.
    /// </summary>
    internal interface IMatchProtocol
    {
        /// <summary>
        /// Register a match messager handler with the messaging service.
        /// </summary>
        /// <param name="handlerID">The message handler's ID, used for addressing.</param>
        /// <param name="handler">The handler to register.</param>
        void RegisterMessageHandler(BadumnaId handlerID, IMatchMessageHandler handler);

        /// <summary>
        /// Deregister a match message handler from the messaging service.
        /// </summary>
        /// <param name="handlerID">The message handler to deregister.</param>
        void DeregisterMessageHandler(BadumnaId handlerID);

        /// <summary>
        /// Sends a join request to a match host.
        /// </summary>
        /// <param name="host">The host ID.</param>
        /// <param name="sender">The client ID.</param>
        /// <param name="matchIdentifier">The idenitifier identifying the match to join.</param>
        void JoinRequest(BadumnaId host, MemberIdentity sender, BadumnaId matchIdentifier);

        /// <summary>
        /// Sends a keep-alive request to a match host.
        /// </summary>
        /// <param name="host">The host ID.</param>
        /// <param name="sender">The client ID.</param>
        /// <param name="matchIdentifier">The idenitifier identifying the match to join.</param>
        void PingFromClientToHost(BadumnaId host, MemberIdentity sender, BadumnaId matchIdentifier);

        /// <summary>
        /// Sends a leave notification to a match host.
        /// </summary>
        /// <param name="host">The host ID.</param>
        /// <param name="client">The client ID.</param>
        /// <param name="matchIdentifier">The ID of the match to leave.</param>
        void LeaveNotification(BadumnaId host, BadumnaId client, BadumnaId matchIdentifier);

        /// <summary>
        /// Sent a notification to a client that they have successfully joined a new host.
        /// </summary>
        /// <param name="client">The client to notify.</param>
        /// <param name="certificate">The host's certificate.</param>
        /// <param name="members">Other members in the match.</param>
        void JoinConfirmation(BadumnaId client, HostAuthorizationCertificate certificate, IEnumerable<MemberIdentity> members);

        /// <summary>
        /// Sent a notification to a client that they have been rejected by a new host.
        /// </summary>
        /// <param name="client">The client to notify.</param>
        /// <param name="certificate">The host's certificate.</param>
        void JoinRejection(BadumnaId client, HostAuthorizationCertificate certificate);

        /// <summary>
        /// Send a reply to a client ping, denying that this is the host.
        /// </summary>
        /// <param name="client">The client to reply to.</param>
        /// <param name="sender">The peer sending the denial.</param>
        /// <param name="certificate">The latest known certificate for the match.</param>
        void HostDenial(BadumnaId client, BadumnaId sender, HostAuthorizationCertificate certificate);

        /// <summary>
        /// Notify a client when there's a new member.
        /// </summary>
        /// <param name="client">The client to notify.</param>
        /// <param name="certificate">The host's certificate.</param>
        /// <param name="identity">Identity of the member that has joined.</param>
        void MemberJoinNotification(
            BadumnaId client,
            HostAuthorizationCertificate certificate,
            MemberIdentity identity);

        /// <summary>
        /// Notify a client when a member has left.
        /// </summary>
        /// <param name="client">The client to notify.</param>
        /// <param name="certificate">The host's certificate.</param>
        /// <param name="member">Member that has left.</param>
        void MemberLeaveNotification(
            BadumnaId client,
            HostAuthorizationCertificate certificate,
            BadumnaId member);

        /// <summary>
        /// Notify a client that the current host is resigning.
        /// </summary>
        /// <param name="client">The client to notify.</param>
        /// <param name="certificate">The host's certificate.</param>
        /// <param name="takeoverCode">Password that allows other peers to takeover hosting from this one
        /// without waiting for the matchmaker to time this one out.</param>
        void HostResignation(BadumnaId client, HostAuthorizationCertificate certificate, int takeoverCode);

        /// <summary>
        /// Ping a peer to see if they are still available to take over as host for a given match.
        /// </summary>
        /// <param name="potentialHost">The peer to ping.</param>
        /// <param name="sender">The pinging peer.</param>
        /// <param name="matchIdentifier">The ID of the match.</param>
        void PingToPotentialHost(BadumnaId potentialHost, BadumnaId sender, BadumnaId matchIdentifier);

        /// <summary>
        /// Send a reply to a potential host ping.
        /// </summary>
        /// <param name="peer">The peer to reply to.</param>
        /// <param name="sender">The sender ID.</param>
        /// <param name="inMatch">A value indicating whether the pinged member still wants to be in the match.</param>
        /// <param name="recognizesPeer">A value indicating whether the pinged member regonizes the pinger as a member
        /// of the match.</param>
        /// <param name="isConnected">A value indicating whether the pinged member is connected to a host.</param>
        /// <param name="certificate">The latest known certificate for the match.</param>
        void PongFromPotentialHost(
            BadumnaId peer,
            BadumnaId sender,
            bool inMatch,
            bool recognizesPeer,
            bool isConnected,
            HostAuthorizationCertificate certificate);

        /// <summary>
        /// Send a chat message to another member of the match.
        /// </summary>
        /// <param name="recipient">The member to send the message to.</param>
        /// <param name="sender">The ID of the sender.</param>
        /// <param name="matchIdentifier">The ID if the match.</param>
        /// <param name="message">THe chat message.</param>
        /// <param name="type">The type of message being sent (public or private).</param>
        void Chat(BadumnaId recipient, BadumnaId sender, BadumnaId matchIdentifier, string message, ChatType type);

        /// <summary>
        /// Send a controller RPC to another member of the match.
        /// </summary>
        /// <param name="recipient">The member to send the RPC to.</param>
        /// <param name="stream">A stream containing the serialized RPC data.</param>
        void SendControllerRPC(BadumnaId recipient, MemoryStream stream);
    }
}
