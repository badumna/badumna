﻿//-----------------------------------------------------------------------
// <copyright file="EntitySource.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2013 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.Match
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// Enumeration of possible classes of match entity.
    /// </summary>
    public enum EntitySource
    {
        /// <summary>
        /// Unused default state.
        /// </summary>
        None = 0,

        /// <summary>
        /// Entity that runs on a single peer.
        /// </summary>
        Peer,

        /// <summary>
        /// Entity runs on the match host.
        /// </summary>
        Host
    }
}
