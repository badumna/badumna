﻿//-----------------------------------------------------------------------
// <copyright file="MemberIdentity.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2013 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.Match
{
    using System;
    using System.IO;
    using Badumna.Core;
    using Badumna.DataTypes;

    /////// <summary>
    /////// Internal interface for member identity exposing ID.
    /////// </summary>
    ////internal interface IMemberIdentity
    ////{
    ////    /// <summary>
    ////    /// Gets a name chosen by the member. Uniqueness not enforced.
    ////    /// </summary>
    ////    string Name { get; }

    ////    /// <summary>
    ////    /// Gets a unique ID for the member.
    ////    /// </summary>
    ////    BadumnaId ID { get; }
    ////}

    /// <summary>
    /// Represents match member identify data, including an ID and a name.
    /// </summary>
    /// <remarks>
    /// Only cares about ID when evaulating equality, not name.
    /// </remarks>
    public class MemberIdentity : IParseable, IEquatable<MemberIdentity>, IEquatable<BadumnaId>
    {
        /// <summary>
        /// A unique ID for the member.
        /// </summary>
        private BadumnaId memberID;

        /// <summary>
        /// A name chosen by the member. Uniqueness not enforced.
        /// </summary>
        private string name;

        /// <summary>
        /// Initializes a new instance of the MemberIdentity class.
        /// </summary>
        /// <param name="memberID">A unique ID for the member.</param>
        /// <param name="name">A name chosen by the user. Uniqueness not enforced.</param>
        internal MemberIdentity(BadumnaId memberID, string name)
        {
            if (memberID == null)
            {
                throw new ArgumentNullException("memberID");
            }

            if (name == null)
            {
                throw new ArgumentNullException("name");
            }

            this.memberID = memberID;
            this.name = name;
        }

        /// <summary>
        /// Initializes a new instance of the MemberIdentity class.
        /// </summary>
        /// <remarks>Should only be used when deserialized as IParseable.</remarks>
        internal MemberIdentity()
        {
        }

        /// <summary>
        /// Initializes a new instance of the MemberIdentity class from a BinaryReader.
        /// </summary>
        /// <param name="reader">The reader</param>
        internal MemberIdentity(BinaryReader reader)
        {
            var length = reader.ReadUInt16();
            var buffer = reader.ReadBytes(length);
            using (var message = new MessageBuffer(buffer))
            {
                ((IParseable)this).FromMessage(message);
            }
        }

        /// <summary>
        /// Gets a name chosen by the member. Uniqueness not enforced.
        /// </summary>
        public string Name
        {
            get { return this.name; }
        }

        /// <summary>
        /// Gets a unique ID for the member.
        /// </summary>
        internal BadumnaId ID
        {
            get { return this.memberID; }
        }

        /// <inheritdoc />
        public static bool operator ==(MemberIdentity leftOperand, MemberIdentity rightOperand)
        {
            if (object.ReferenceEquals(null, leftOperand))
            {
                return object.ReferenceEquals(null, rightOperand);
            }

            return leftOperand.Equals(rightOperand);
        }

        /// <inheritdoc />
        public static bool operator !=(MemberIdentity leftOperand, MemberIdentity rightOperand)
        {
            return !(leftOperand == rightOperand);
        }

        /// <inheritdoc />
        public static bool operator ==(BadumnaId leftOperand, MemberIdentity rightOperand)
        {
            if (object.ReferenceEquals(null, leftOperand))
            {
                return object.ReferenceEquals(null, rightOperand);
            }

            return rightOperand.Equals(leftOperand);
        }

        /// <inheritdoc />
        public static bool operator !=(BadumnaId leftOperand, MemberIdentity rightOperand)
        {
            return !(leftOperand == rightOperand);
        }

        /// <inheritdoc />
        public static bool operator ==(MemberIdentity leftOperand, BadumnaId rightOperand)
        {
            return rightOperand == leftOperand;
        }

        /// <inheritdoc />
        public static bool operator !=(MemberIdentity leftOperand, BadumnaId rightOperand)
        {
            return !(rightOperand == leftOperand);
        }

        /// <inheritdoc />
        public bool Equals(MemberIdentity other)
        {
            if ((object)other == null)
            {
                return false;
            }

            return this.memberID.Equals(other.memberID);
        }

        /// <inheritdoc />
        public override bool Equals(object other)
        {
            if (other is MemberIdentity)
            {
                return this.Equals((MemberIdentity)other);
            }

            return false;
        }

        /// <inheritdoc />
        public override int GetHashCode()
        {
            return this.memberID.GetHashCode();
        }

        /// <inheritdoc />        
        public override string ToString()
        {
            return this.name + " (" + this.GetHashCode() + ")";
        }

        /// <inheritdoc />        
        public bool Equals(BadumnaId otherID)
        {
            if (otherID == null)
            {
                return false;
            }

            return this.memberID.Equals(otherID);
        }

        /// <inheritdoc />
        void IParseable.ToMessage(MessageBuffer message, Type parameterType)
        {
            message.Write(this.memberID);
            message.Write(this.name);
        }

        /// <inheritdoc />        
        void IParseable.FromMessage(MessageBuffer message)
        {
            this.memberID = message.Read<BadumnaId>();
            this.name = message.ReadString();
        }

        /// <summary>
        /// Serialize to a BinaryWriter.
        /// </summary>
        /// <param name="writer">The writer</param>
        internal void Serialize(BinaryWriter writer)
        {
            // There's no great way to do this.  We could try to duplicate the IParseable
            // serialization code here, but there's no easy way to send a BadumnaId
            // to a BinaryWriter anyway.
            using (var message = new MessageBuffer())
            {
                ((IParseable)this).ToMessage(message, typeof(MemberIdentity));
                var buffer = message.GetBuffer();
                writer.Write((ushort)buffer.Length);
                writer.Write(buffer);
            }
        }
    }
}
