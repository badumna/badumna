﻿//-----------------------------------------------------------------------
// <copyright file="ChatType.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2013 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.Match
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// Enumeration of possible types of chat message.
    /// </summary>
    public enum ChatType : byte
    {
        /// <summary>
        /// Unused default state.
        /// </summary>
        None = 0,

        /// <summary>
        /// A public chat message for all members of a match.
        /// </summary>
        Public,

        /// <summary>
        /// A private chat message for a particular member of a match.
        /// </summary>
        Private
    }
}
