﻿//-----------------------------------------------------------------------
// <copyright file="ControllerOriginalWrapper.cs" company="Scalify">
//     Copyright (c) 2013 Scalify. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.Match
{
    using System;
    using System.IO;
    using Badumna.DataTypes;
    using Badumna.Replication;
    using Badumna.SpatialEntities;
    using Badumna.Utilities;

    /// <summary>
    /// Wrapper class used for managing controller originals with match membership
    /// and replication.
    /// </summary>
    internal class ControllerOriginalWrapper : IOriginal
    {
        /// <summary>
        /// The IOriginal for the controller.
        /// </summary>
        private readonly IOriginal original;

        /// <summary>
        /// Membership reporter.
        /// </summary>
        private IMembershipReporter membershipReporter;

        /// <summary>
        /// The entity manager.
        /// </summary>
        private IEntityManager entityManager;

        /// <summary>
        /// Initializes a new instance of the ControllerOriginalWrapper class.
        /// </summary>
        /// <param name="original">The match controller original.</param>
        /// <param name="guid">ID for the controller original wrapper.</param>
        /// <param name="membershipReporter">Membership reporter.</param>
        /// <param name="entityManager">Entity manager.</param>
        public ControllerOriginalWrapper(
            IOriginal original,
            BadumnaId guid,
            IMembershipReporter membershipReporter,
            IEntityManager entityManager)
        {
            this.original = original;
            this.membershipReporter = membershipReporter;
            this.entityManager = entityManager;
            this.CertificateTimestamp = membershipReporter.Certificate.Timestamp;
            this.Guid = guid;
            this.entityManager.RegisterEntity(
                    this,
                    new EntityTypeId((byte)EntityGroup.MatchController, 0));
            this.membershipReporter.MemberAdded += this.OnMemberJoined;
            this.membershipReporter.MemberRemoved += this.OnMemberLeft;

            foreach (var member in this.membershipReporter.Members)
            {
                this.AddInterested(member.ID);
            }
        }

        /// <summary>
        /// Gets or sets the unique identifier of the original.
        /// </summary>
        public BadumnaId Guid
        {
            get { return this.original.Guid; }
            set { this.original.Guid = value; }
        }

        /// <summary>
        /// Gets the timestamp of the hosting certificate authorizing hosting of the entity.
        /// </summary>
        public TimeSpan CertificateTimestamp
        {
            get;
            private set;
        }

        /// <inheritdoc />
        public BooleanArray Serialize(BooleanArray requiredParts, Stream stream)
        {
            using (var writer = new BinaryWriter(stream))
            {
                if (requiredParts[(int)SpatialEntityStateSegment.Match])
                {
                    var bytes = this.membershipReporter.MatchIdentifier.ToBytes();
                    writer.Write(bytes.Length);
                    writer.Write(bytes);

                    writer.Write(this.CertificateTimestamp.Ticks);
                }

                this.original.Serialize(requiredParts, stream);
                
                // We'll always send all required parts.
                return new BooleanArray();
            }
        }

        /// <inheritdoc />
        public void HandleEvent(Stream stream)
        {
            throw new System.NotImplementedException(
                "Controller RPCs are not sent via replication system, and are always sent to the replica wrapper.");
        }

        /// <summary>
        /// Stop this original wrapper from controlling the match controller.
        /// </summary>
        public void Release()
        {
            this.entityManager.UnregisterEntity(this, true);
            this.membershipReporter.MemberAdded -= this.OnMemberJoined;
            this.membershipReporter.MemberRemoved -= this.OnMemberLeft;
        }

        /// <summary>
        /// Handle a member join event.
        /// </summary>
        /// <param name="source">The event source.</param>
        /// <param name="e">The event data.</param>
        private void OnMemberJoined(IMembershipReporter source, MatchMembershipEventArgs e)
        {
            this.AddInterested(e.Member.ID);
        }

        /// <summary>
        /// Handle a member left event.
        /// </summary>
        /// <param name="source">The event source.</param>
        /// <param name="e">The event data.</param>
        private void OnMemberLeft(IMembershipReporter source, MatchMembershipEventArgs e)
        {
            this.RemoveInterested(e.Member.ID);
        }

        /// <summary>
        /// Add the interest member.
        /// </summary>
        /// <param name="member">The member interested.</param>
        private void AddInterested(BadumnaId member)
        {
            if (member.Address.Equals(this.Guid.OriginalAddress))
            {
                Logger.TraceWarning(
                    LogTag.Match,
                    "Ignoring interest notification for member {0} because it shares the same address as the match controller {1}",
                    member,
                    this.Guid);
                return;
            }

            Logger.TraceInformation(
                    LogTag.Match,
                    "Adding interest notification for match controller original {0}: interested member = {1}.",
                    this.Guid,
                    member);
            this.entityManager.AddInterested(this, member);
        }

        /// <summary>
        /// Remove interested member.
        /// </summary>
        /// <param name="member">The uninterested member.</param>
        private void RemoveInterested(BadumnaId member)
        {
            this.entityManager.RemoveInterested(this, member);
        }
    }
}
