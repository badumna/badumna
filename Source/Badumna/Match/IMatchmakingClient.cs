﻿//-----------------------------------------------------------------------
// <copyright file="IMatchmakingClient.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2013 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.Match
{
    /// <summary>
    /// Interface for clients of the matchmaking service.
    /// </summary>
    internal interface IMatchmakingClient
    {
        /// <summary>
        /// Handle a hosting reply from the matchmaker.
        /// </summary>
        /// <param name="certificate">The certificate of the current match host.</param>
        /// <param name="takeoverCode">A secret code for immediate takeover of the match
        /// (or zero, if the reply is not to the current host).</param>
        void HandleHostingReply(HostAuthorizationCertificate certificate, int takeoverCode);

        /// <summary>
        /// Handle a ping from the matchmaker prompting the client to ping the server to show it is still hosting.
        /// </summary>
        void HandlePingFromMatchmaker();
    }
}
