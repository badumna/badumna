﻿//-----------------------------------------------------------------------
// <copyright file="MatchRecord.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2013 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.Match
{
    using System;
    using Badumna.Core;
    using Badumna.DataTypes;
    using Badumna.Utilities;

    /// <summary>
    /// Match details recorded on the matchmaking server.
    /// </summary>
    internal class MatchRecord
    {
        /// <summary>
        /// For generating takeover codes.
        /// </summary>
        private readonly IRandomNumberGenerator rng;

        /// <summary>
        /// For recording update times.
        /// </summary>
        private readonly ITime timeKeeper;

        /// <summary>
        /// The cloud identifier (empty string if not on cloud).
        /// </summary>
        /// <remarks>
        /// This is stored here instead of as part of the MatchmakingCriteria because there's no easy
        /// way to ensure it's intialized properly if it's on MatchmakingCriteria.
        /// Because MatchmakingCriteria is public and a struct it's possible that developers could
        /// try to compare instances from matchmaking query results with their own instances and
        /// that comparison would fail because the cloud identifier wouldn't be set on their instance.
        /// </remarks>
        private readonly string cloudIdentifier;

        /// <summary>
        /// Initializes a new instance of the MatchRecord class.
        /// </summary>
        /// <param name="randomNumberGenerator">A random number generator for producing the takeover code.</param>
        /// <param name="timeKeeper">For recording update times.</param>
        /// <param name="cloudIdentifier">The cloud identifier (empty string if not on cloud).</param>
        /// <param name="matchID">The ID of the match.</param>
        /// <param name="hostID">The ID of the host.</param>
        /// <param name="criteria">The matchmaking criteria.</param>
        /// <param name="capacity">The match capacity.</param>
        public MatchRecord(
            IRandomNumberGenerator randomNumberGenerator,
            ITime timeKeeper,
            string cloudIdentifier,
            BadumnaId matchID,
            BadumnaId hostID,
            MatchmakingCriteria criteria,
            MatchCapacity capacity)
        {
            if (randomNumberGenerator == null)
            {
                throw new ArgumentNullException("randomNumberGenerator");
            }

            if (timeKeeper == null)
            {
                throw new ArgumentNullException("timeKeeper");
            }

            if (hostID == null)
            {
                throw new ArgumentNullException("hostID");
            }

            this.rng = randomNumberGenerator;
            this.timeKeeper = timeKeeper;
            this.cloudIdentifier = cloudIdentifier;

            this.SetHost(hostID, matchID, criteria, capacity);
        }

        /// <summary>
        /// Gets the time we received the last HostingRequest from the host.
        /// </summary>
        public TimeSpan LastUpdateTime { get; private set; }

        /// <summary>
        /// Gets the match certificate.
        /// </summary>
        public HostAuthorizationCertificate Certificate { get; private set; }

        /// <summary>
        /// Gets the matchmaking criteria.
        /// </summary>
        public MatchmakingCriteria Criteria { get; private set; }

        /// <summary>
        /// Gets the cloud identifier (empty string if not on cloud).
        /// </summary>
        public string CloudIdentifer
        {
            get { return this.cloudIdentifier; }
        }

        /// <summary>
        /// Gets or sets the match status.
        /// </summary>
        public MatchCapacity Status { get; set; }

        /// <summary>
        /// Gets the code required from peers wanting to take over before current host times out.
        /// </summary>
        public int TakeoverCode { get; private set; }

        /// <summary>
        /// Returns a value indicating whether the match is/will be
        /// timed out at the given time.
        /// </summary>
        /// <param name="time">The time to check</param>
        /// <returns>True iff the match is timed out</returns>
        public bool IsTimedOutAt(TimeSpan time)
        {
            return time - this.LastUpdateTime > Matchmaker.HostTimeout;
        }

        /// <summary>
        /// Update the match details.
        /// </summary>
        /// <param name="criteria">The new matchmaking criteria.</param>
        /// <param name="capacity">The new match capacity.</param>
        public void Update(MatchmakingCriteria criteria, MatchCapacity capacity)
        {
            this.LastUpdateTime = this.timeKeeper.Now;
            this.Criteria = criteria;
            this.Status = capacity;
        }

        /// <summary>
        /// Set a new host for the match.
        /// </summary>
        /// <param name="host">The ID of the new host.</param>
        /// <param name="criteria">The matchmaking criteria according to the new host.</param>
        /// <param name="capacity">The match capacity according to the new host.</param>
        public void TakeOver(BadumnaId host, MatchmakingCriteria criteria, MatchCapacity capacity)
        {
            this.SetHost(host, this.Certificate.MatchIdentifier, criteria, capacity);
        }

        /// <summary>
        /// Create a new certificate for a new host
        /// </summary>
        /// <param name="hostID">The ID of the host.</param>
        /// <param name="matchID">The ID of the match.</param>
        /// <param name="criteria">The matchmaking criteria according to the new host.</param>
        /// <param name="capacity">The match capacity according to the new host.</param>
        private void SetHost(BadumnaId hostID, BadumnaId matchID, MatchmakingCriteria criteria, MatchCapacity capacity)
        {
            // Update first to ensure certificate has correct timestamp.
            this.Update(criteria, capacity);
            this.TakeoverCode = this.rng.Next(int.MinValue, int.MaxValue);
            this.Certificate = new HostAuthorizationCertificate(matchID, hostID, this.LastUpdateTime);
        }
    }
}
