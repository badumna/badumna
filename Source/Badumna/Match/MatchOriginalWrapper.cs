﻿//-----------------------------------------------------------------------
// <copyright file="MatchOriginalWrapper.cs" company="Scalify">
//     Copyright (c) 2013 Scalify. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.Match
{
    using System.Collections.Generic;
    using System.IO;
    using Badumna.DataTypes;
    using Badumna.Replication;
    using Badumna.SpatialEntities;
    using Badumna.Utilities;

    /// <summary>
    /// Wrapper class used for managing spatial originals with match membership
    /// and replication.
    /// </summary>
    internal class MatchOriginalWrapper : IOriginal
    {
        /// <summary>
        /// The application's original.
        /// </summary>
        private readonly IOriginal original;

        /// <summary>
        /// Membership reporter.
        /// </summary>
        private readonly IMembershipReporter membershipReporter;

        /// <summary>
        /// The entity manager.
        /// </summary>
        private IEntityManager entityManager;
        
        /// <summary>
        /// Initializes a new instance of the MatchOriginalWrapper class.
        /// </summary>
        /// <param name="original">The local original.</param>
        /// <param name="entityID">An ID for this original.</param>
        /// <param name="entityType">An integer used by the application to identify the type of the entity.</param>
        /// <param name="membershipReporter">Membership reporter.</param>
        /// <param name="entityManager">Entity manager.</param>
        public MatchOriginalWrapper(
            IOriginal original,
            BadumnaId entityID,
            uint entityType,
            IMembershipReporter membershipReporter,
            IEntityManager entityManager)
        {
            this.original = original;
            this.membershipReporter = membershipReporter;
            this.entityManager = entityManager;
            this.Guid = entityID;
            this.entityManager.RegisterEntity(this, new EntityTypeId((byte)EntityGroup.Match, entityType));

            // TODO: Unsubscribe when match is closed.
            // TODO: Is that necessary or will the match and all dependencies
            // be garbage collected anyway?
            // If it is required, should use IDispoable pattern.
            this.membershipReporter.MemberAdded += this.OnMemberJoined;
            this.membershipReporter.MemberRemoved += this.OnMemberLeft;

            foreach (var member in this.membershipReporter.Members)
            {
                this.AddInterested(member.ID);
            }
        }

        /// <summary>
        /// Gets or sets the unique identifier of the original.
        /// </summary>
        public BadumnaId Guid
        {
            get { return this.Original.Guid; }
            set { this.Original.Guid = value; }
        }       

        /// <summary>
        /// Gets the auto original.
        /// </summary>
        public IOriginal Original
        {
            get
            {
                return this.original;
            }
        }

        /// <summary>
        /// Gets the ID of the match the original is registered to.
        /// </summary>
        public BadumnaId MatchID
        {
            get { return this.membershipReporter.MatchIdentifier; }
        }

        /// <inheritdoc />
        public BooleanArray Serialize(BooleanArray requiredParts, Stream stream)
        {
            using (var writer = new BinaryWriter(stream))
            {
                if (requiredParts[(int)SpatialEntityStateSegment.Match])
                {
                    var bytes = this.membershipReporter.MatchIdentifier.ToBytes();
                    writer.Write(bytes.Length);
                    writer.Write(bytes);
                }

                this.Original.Serialize(requiredParts, stream);
                
                // We'll always send all required parts.
                return new BooleanArray();
            }
        }

        /// <summary>
        /// Release the entity from being controlled by this original.
        /// </summary>
        /// <param name="removeReplicas">A value indicating whether to emove the associated replicas.</param>
        public void ReleaseEntity(bool removeReplicas)
        {
            this.entityManager.UnregisterEntity(this.original, removeReplicas);
            this.membershipReporter.MemberAdded -= this.OnMemberJoined;
            this.membershipReporter.MemberRemoved -= this.OnMemberLeft;
        }

        /// <inheritdoc />
        public void HandleEvent(Stream stream)
        {
            this.Original.HandleEvent(stream);
        }

        /// <summary>
        /// Handle a member join event.
        /// </summary>
        /// <param name="source">The event source.</param>
        /// <param name="e">The event data.</param>
        private void OnMemberJoined(IMembershipReporter source, MatchMembershipEventArgs e)
        {
            this.AddInterested(e.Member.ID);
        }

        /// <summary>
        /// Handle a member left event.
        /// </summary>
        /// <param name="source">The event source.</param>
        /// <param name="e">The event data.</param>
        private void OnMemberLeft(IMembershipReporter source, MatchMembershipEventArgs e)
        {
            this.RemoveInterested(e.Member.ID);
        }

        /// <summary>
        /// Add the interest member.
        /// </summary>
        /// <param name="member">The member interested.</param>
        private void AddInterested(BadumnaId member)
        {
            if (member.Address.Equals(this.Original.Guid.OriginalAddress))
            {
                Logger.TraceInformation(LogTag.Match, "Ignoring interest notification for {0} because it shares the same address as the entity {1}", member, this.Original.Guid);
                return;
            }

            this.entityManager.AddInterested(this.Original, member);
        }

        /// <summary>
        /// Remove interested member.
        /// </summary>
        /// <param name="member">The uninterested member.</param>
        private void RemoveInterested(BadumnaId member)
        {
            this.entityManager.RemoveInterested(this.Original, member);
        }
    }
}
