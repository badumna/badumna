﻿//-----------------------------------------------------------------------
// <copyright file="IMatchmaker.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2013 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.Match
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Badumna.DataTypes;
    using Badumna.Utilities;

    /// <summary>
    /// Registers matches and responds to match searches.
    /// </summary>
    internal interface IMatchmaker
    {
        /// <summary>
        /// Register a matchmaking client.
        /// </summary>
        /// <param name="handlerID">The client's ID, used for addressing.</param>
        /// <param name="client">The client to register.</param>
        void RegisterMatchmakingClient(BadumnaId handlerID, IMatchmakingClient client);

        /// <summary>
        /// Deregister a matchmaking client.
        /// </summary>
        /// <param name="client">The client to deregister.</param>
        void DeregisterMatchmakingClient(BadumnaId client);

        /// <summary>
        /// Request to become/remain the host for a given match.
        /// </summary>
        /// <param name="requestorID">The local id of the requestor.</param>
        /// <param name="matchIdentifier">Unique identifier for the match.</param>
        /// <param name="criteria">Matchmaking criteria.</param>
        /// <param name="status">Status of the match.</param>
        /// <param name="takeoverCode">Code authorizing takeover (for immediate takeover).</param>
        /// <remarks>
        /// Requests are rejected if another peer is already hosting the match.
        /// A rejection is notified by invocation of the reply handler
        /// with a certificate that does not match the local peer.
        /// Success is notified by invocation of the reply handler with a certificate
        /// matching the local peer.
        /// If the local peer is already the host and remains the host no notification is provided (to
        /// save bandwidth).
        /// </remarks>
        void HostingRequest(
            BadumnaId requestorID,
            BadumnaId matchIdentifier,
            MatchmakingCriteria criteria,
            MatchCapacity status,
            int takeoverCode);

        /// <summary>
        /// Request a list of matches meeting the given criteria.
        /// </summary>
        /// <param name="criteria">Matchmaking criteria.</param>
        /// <param name="resultNotification">Callback that provides notification of the query results.</param>
        /// <param name="failureNotification">Callback that provides notification of query failure.</param>
        void MatchQuery(
            MatchmakingCriteria criteria,
            GenericCallBack<IList<MatchmakingResult>> resultNotification,
            GenericCallBack<MatchError> failureNotification);
    }
}
