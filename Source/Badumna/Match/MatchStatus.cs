﻿//-----------------------------------------------------------------------
// <copyright file="MatchStatus.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2013 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.Match
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// Enumeration of possible states of the match.
    /// </summary>
    public enum MatchStatus
    {
        /// <summary>
        /// Default state on match creation.
        /// </summary>
        Initializing = 0,

        /// <summary>
        /// The match is currently being hosted on this peer.
        /// </summary>
        Hosting,

        /// <summary>
        /// This peer is currently connected to the match host.
        /// </summary>
        Connected,

        /// <summary>
        /// This peer is trying to connect to the match host.
        /// </summary>
        Connecting,

        /// <summary>
        /// The match has been closed.
        /// </summary>
        Closed
    }
}
