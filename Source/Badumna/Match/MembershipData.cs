﻿//-----------------------------------------------------------------------
// <copyright file="MembershipData.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2013 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.Match
{
    using System;
    using System.Collections.Generic;
    using Badumna.Core;
    using Badumna.DataTypes;
    using Badumna.Transport;

    /// <summary>
    /// Encapsulates match membership data for access by all membership manager states.
    /// </summary>
    /// <remarks>
    /// TODO: Make data access thread safe?
    /// It doesn't need to be at present, as network thread is blocked while
    /// updates are done on application thread, but we probably want to change that in future.
    /// </remarks>
    internal class MembershipData
    {
        /// <summary>
        /// Clock for getting the current time.
        /// </summary>
        private readonly ITime timeKeeper;

        /// <summary>
        /// The ID of the local member.
        /// </summary>
        private readonly MemberIdentity memberIdentity;

        /// <summary>
        /// The address of the matchmaking server.
        /// </summary>
        private readonly PeerAddress serverAddress;

        /// <summary>
        /// The matchmaker.
        /// </summary>
        private readonly IMatchmaker matchmaker;

        /// <summary>
        /// Protocol specifying messages that can be sent to other peers for matches.
        /// </summary>
        private readonly IMatchProtocol matchProtocol;

        /// <summary>
        /// The members currently known by this peer.
        /// </summary>
        private List<MemberIdentity> members;

        /// <summary>
        /// Explicit backing delegate for member added, required to prevent JITing on Unity iPhone.
        /// </summary>
        private GenericEventHandler<MembershipData, MatchMembershipEventArgs> memberAddedDelegate;

        /// <summary>
        /// Explicit backing delegate for member removed, required to prevent JITing on Unity iPhone.
        /// </summary>
        private GenericEventHandler<MembershipData, MatchMembershipEventArgs> memberRemovedDelegate;

        /// <summary>
        /// Initializes a new instance of the MembershipData class.
        /// </summary>
        /// <param name="certificate">A certificate authorizing a host for the match.</param>
        /// <param name="criteria">Matchmaking criteria.</param>
        /// <param name="maxPlayers">Maximum number of players permitted in the match.</param>
        /// <param name="memberIdentity">The identity of the local member.</param>
        /// <param name="serverAddress">The address of the matchmaking server.</param>
        /// <param name="timeKeeper">Clock for getting the current time.</param>
        /// <param name="matchmaker">The matchmaker.</param>
        /// <param name="matchProtocol">Protocol specifying messages that can be sent to other peers for matches.</param>
        public MembershipData(
            HostAuthorizationCertificate certificate,
            MatchmakingCriteria criteria,
            int maxPlayers,
            MemberIdentity memberIdentity,
            PeerAddress serverAddress,
            ITime timeKeeper,
            IMatchmaker matchmaker,
            IMatchProtocol matchProtocol)
        {
            this.members = new List<MemberIdentity>(maxPlayers);
            this.Certificate = certificate;
            this.Criteria = criteria;
            this.MaxPlayers = maxPlayers;
            this.memberIdentity = memberIdentity;
            this.serverAddress = serverAddress;
            this.timeKeeper = timeKeeper;
            this.matchmaker = matchmaker;
            this.matchProtocol = matchProtocol;

            this.Joining = new MembershipState.Joining(this);
            this.AwaitingHost = new MembershipState.AwaitingHost(this);
            this.ClaimingHosting = new MembershipState.ClaimingHosting(this);
            this.Hosting = new MembershipState.Hosting(this);
            this.Connected = new MembershipState.Connected(this);
            this.Done = new MembershipState.Done(this);
        }

        /// <summary>
        /// Raised when a new member has been added.
        /// </summary>
        public event GenericEventHandler<MembershipData, MatchMembershipEventArgs> MemberAdded
        {
            add { this.memberAddedDelegate += value; }
            remove { this.memberAddedDelegate -= value; }
        }

        /// <summary>
        /// Raised when a member has been removed.
        /// </summary>
        public event GenericEventHandler<MembershipData, MatchMembershipEventArgs> MemberRemoved
        {
            add { this.memberRemovedDelegate += value; }
            remove { this.memberRemovedDelegate -= value; }
        }

        /// <summary>
        /// Gets the clock.
        /// </summary>
        public ITime TimeKeeper
        {
            get { return this.timeKeeper; }
        }

        /// <summary>
        /// Gets the address of the matchmaking server.
        /// </summary>
        public PeerAddress ServerAddress
        {
            get { return this.serverAddress; }
        }

        /// <summary>
        /// Gets or sets the latest certificate authorizing a host for a match.
        /// </summary>
        public HostAuthorizationCertificate Certificate { get; set; }

        /// <summary>
        /// Gets or sets the matchmaking criteria for the match.
        /// </summary>
        public MatchmakingCriteria Criteria { get; set; }

        /// <summary>
        /// Gets or sets the player limit.
        /// </summary>
        public int MaxPlayers { get; set; }

        /// <summary>
        /// Gets the number of free places for players in the match.
        /// </summary>
        public int FreeSlots
        {
            get
            {
                return this.MaxPlayers - this.MemberCount;
            }
        }

        /// <summary>
        /// Gets the current members of the match.
        /// </summary>
        /// <remarks>
        /// This provides direct access to the members list and should only be used in the network thread.
        /// </remarks>
        public IList<MemberIdentity> Members
        {
            get { return this.members; }
        }

        /// <summary>
        /// Gets the count of members in the match.
        /// </summary>
        public int MemberCount
        {
            get { return this.members.Count; }
        }

        /// <summary>
        /// Gets the ID of the local member.
        /// </summary>
        public MemberIdentity MemberIdentity
        {
            get { return this.memberIdentity; }
        }

        /// <summary>
        /// Gets the matchmaker.
        /// </summary>
        public IMatchmaker Matchmaker
        {
            get { return this.matchmaker; }
        }

        /// <summary>
        /// Gets the protocol specifying messages that can be sent to other peers for matches.
        /// </summary>
        public IMatchProtocol MatchProtocol
        {
            get { return this.matchProtocol; }
        }

        /// <summary>
        /// Gets the state for trying to connect to a known host.
        /// </summary>
        public MembershipState.Joining Joining { get; private set; }

        /// <summary>
        /// Gets the state for trying to connect to a known host.
        /// </summary>
        public MembershipState.AwaitingHost AwaitingHost { get; private set; }

        /// <summary>
        /// Gets the state for trying be authorized as host.
        /// </summary>
        public MembershipState.ClaimingHosting ClaimingHosting { get; private set; }

        /// <summary>
        /// Gets the state for when acting as host.
        /// </summary>
        public MembershipState.Hosting Hosting { get; private set; }

        /// <summary>
        /// Gets the state for when connected to a known host.
        /// </summary>
        public MembershipState.Connected Connected { get; private set; }

        /// <summary>
        /// Gets the state when given up trying to join or left the match.
        /// </summary>
        public MembershipState.Done Done { get; private set; }

        /// <summary>
        /// Gets or sets an error message.
        /// </summary>
        public MatchError Error { get; set; }

        /// <summary>
        /// Gets or sets the secret code that authorizes other members to take over hosting.
        /// </summary>
        public int TakeoverCode { get; set; }

        /// <summary>
        /// Gets or sets the ID of the host last connection attempt was to if it failed, otherwise null.
        /// </summary>
        /// <remarks>
        /// Store the ID of the host when a connection attempt fails here, so that a second
        /// concurrent failure attempt will lead to the match terminating.
        /// </remarks>
        public BadumnaId LastHostAttemptedToJoinIfLastJoinFailed { get; set; }

        /// <summary>
        /// Add a member to the match, if not already joined.
        /// </summary>
        /// <param name="newMember">The member to add.</param>
        /// <returns><c>true</c> if the member was added, otherwise <c>false</c>.</returns>
        public bool AddMember(MemberIdentity newMember)
        {
            this.members.Add(newMember);
            this.OnMemberAdded(newMember);
            return true;
        }

        /// <summary>
        /// Remove a member from the match, if joined.
        /// </summary>
        /// <param name="memberIdentity">The member to remove.</param>
        /// <returns><c>true</c> if the member was removed, otherwise <c>false</c>.</returns>
        public bool RemoveMember(MemberIdentity memberIdentity)
        {
            this.members.Remove(memberIdentity);
            this.OnMemberRemoved(memberIdentity);
            return true;
        }

        /// <summary>
        /// Remove a member from the match, if joined.
        /// </summary>
        /// <param name="memberID">The ID of the member to remove.</param>
        /// <returns><c>true</c> if the member was removed, otherwise <c>false</c>.</returns>
        public bool RemoveMember(BadumnaId memberID)
        {
            foreach (var memberIdentity in this.members)
            {
                if (memberIdentity.ID == memberID)
                {
                    this.members.Remove(memberIdentity);
                    this.OnMemberRemoved(memberIdentity);
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Replace the membership of the match with a new set of members.
        /// </summary>
        /// <param name="newMembers">The new set of members for the match.</param>
        public void UpdateMembers(IEnumerable<MemberIdentity> newMembers)
        {
            // TODO: Change this method (and callers) to take a list, to avoid this allocation.
            var newMemberList = new List<MemberIdentity>(newMembers);
            foreach (var newMember in newMembers)
            {
                if (!this.members.Contains(newMember))
                {
                    this.OnMemberAdded(newMember);
                }
            }

            foreach (var existingMember in this.members)
            {
                if (!newMemberList.Contains(existingMember))
                {
                    this.OnMemberRemoved(existingMember);
                }
            }

            this.members.Clear();
            this.members.AddRange(newMembers);
        }

        /// <summary>
        /// Test if a given would-be member is recognized by this member as a member of the match.
        /// </summary>
        /// <param name="clientIdentity">The would-be member.</param>
        /// <returns><c>true</c> if the testee is a member, otherwise <c>false</c>.</returns>
        public bool IsMember(MemberIdentity clientIdentity)
        {
            return this.members.Contains(clientIdentity);
        }

        /// <summary>
        /// Test if a given would-be member is recognized by this member as a member of the match.
        /// </summary>
        /// <param name="clientID">The ID of the would-be member.</param>
        /// <returns><c>true</c> if the testee is a member, otherwise <c>false</c>.</returns>
        public bool IsMember(BadumnaId clientID)
        {
            foreach (var member in this.members)
            {
                if (member == clientID)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Send notification that a new member has joined.
        /// </summary>
        /// <param name="memberIdentity">The identity of the member.</param>
        private void OnMemberAdded(MemberIdentity memberIdentity)
        {
            var handler = this.memberAddedDelegate;
            if (handler != null)
            {
                handler(this, new MatchMembershipEventArgs(memberIdentity));
            }
        }

        /// <summary>
        /// Send notification that a new member has left.
        /// </summary>
        /// <param name="memberIdentity">The identity of the member.</param>
        private void OnMemberRemoved(MemberIdentity memberIdentity)
        {
            var handler = this.memberRemovedDelegate;
            if (handler != null)
            {
                handler(this, new MatchMembershipEventArgs(memberIdentity));
            }
        }
    }
}
