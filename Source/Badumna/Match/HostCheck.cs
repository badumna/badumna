﻿//-----------------------------------------------------------------------
// <copyright file="HostCheck.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2013 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.Match
{
    using System;
    using System.Collections.Generic;
    using Badumna.Core;
    using Badumna.DataTypes;

    /// <summary>
    /// Manages takeover attempts for matches whose host has not resigned or timed-out.
    /// </summary>
    internal class HostCheck
    {
        /// <summary>
        /// The matchmaker.
        /// </summary>
        private readonly Matchmaker matchmaker;

        /// <summary>
        /// The record for the match being checked.
        /// </summary>
        private readonly MatchRecord matchRecord;

        /// <summary>
        /// The network event queue for scheduling the timeout.
        /// </summary>
        private readonly INetworkEventScheduler eventQueue;

        /// <summary>
        /// The host timeout event.
        /// </summary>
        private readonly NetworkEvent hostTimeout;

        /// <summary>
        /// A list of all members trying to takeover hosting rights.
        /// </summary>
        private List<ClaimantDetails> claimants = new List<ClaimantDetails>();

        /// <summary>
        /// Initializes a new instance of the HostCheck class.
        /// </summary>
        /// <param name="matchRecord">The record for the match being checked.</param>
        /// <param name="claimant">The member seeking to claim hosting rights.</param>
        /// <param name="criteria">The matchmaking criteria according to the claimant.</param>
        /// <param name="capacity">The match capacity according to the claimant.</param>
        /// <param name="currentTime">The current time.</param>
        /// <param name="matchmaker">The matchmaker.</param>
        /// <param name="eventQueue">The network event queue for scheduling the timeout.</param>
        public HostCheck(
            MatchRecord matchRecord,
            BadumnaId claimant,
            MatchmakingCriteria criteria,
            MatchCapacity capacity,
            TimeSpan currentTime,
            Matchmaker matchmaker,
            INetworkEventScheduler eventQueue)
        {
            this.claimants.Add(new ClaimantDetails(claimant, criteria, capacity));
            this.matchmaker = matchmaker;
            this.matchRecord = matchRecord;
            this.eventQueue = eventQueue;

            this.matchmaker.PingFromMatchmakerToHost(matchRecord);
            var timeUntilNormalTimeout = (this.matchRecord.LastUpdateTime + Matchmaker.HostTimeout) - currentTime;
            var checkTimeout = timeUntilNormalTimeout < Matchmaker.HostCheckTimeout ?
                timeUntilNormalTimeout :
                Matchmaker.HostCheckTimeout;
            this.hostTimeout = this.eventQueue.Schedule(checkTimeout.TotalMilliseconds, this.OnTimeout);
        }

        /// <summary>
        /// Add another claimant to the list of members trying to takeover hosting.
        /// </summary>
        /// <param name="claimant">The claimant ID.</param>
        /// <param name="criteria">The matchmaking criteria accoring to the claimant.</param>
        /// <param name="capacity">The match capacity according to the claimant.</param>
        public void AddClaimant(BadumnaId claimant, MatchmakingCriteria criteria, MatchCapacity capacity)
        {
            this.claimants.Add(new ClaimantDetails(claimant, criteria, capacity));
        }

        /// <summary>
        /// Confirm the current host is still hosting to all claimants.
        /// </summary>
        public void ConfirmHostIsLive()
        {
            this.eventQueue.Remove(this.hostTimeout);
            this.Resolve();
        }

        /// <summary>
        /// Grant hosting rights to the first claimant as the current host as timed out.
        /// </summary>
        private void OnTimeout()
        {
            this.matchRecord.TakeOver(
                this.claimants[0].Claimant,
                this.claimants[0].Criteria,
                this.claimants[0].Capacity);
            this.Resolve();
        }

        /// <summary>
        /// Resolve by replying to all claimants with the host details, and removing the check from the matchmaker.
        /// </summary>
        private void Resolve()
        {
            foreach (var claimant in this.claimants)
            {
                this.matchmaker.SendHostingReply(claimant.Claimant, this.matchRecord);
            }

            this.matchmaker.RemoveCompletedHostCheck(this.matchRecord);
        }

        /// <summary>
        /// Store details of a claimant and their version of the match details.
        /// </summary>
        private class ClaimantDetails
        {
            /// <summary>
            /// Initializes a new instance of the HostCheck.ClaimantDetails class.
            /// </summary>
            /// <param name="claimant">The claimant ID</param>
            /// <param name="criteria">The matchmaking criteria according to the claimant.</param>
            /// <param name="capacity">The match capacity according to the claimant.</param>
            public ClaimantDetails(BadumnaId claimant, MatchmakingCriteria criteria, MatchCapacity capacity)
            {
                this.Claimant = claimant;
                this.Criteria = criteria;
                this.Capacity = capacity;
            }

            /// <summary>
            /// Gets the claimant ID.
            /// </summary>
            public BadumnaId Claimant { get; private set; }

            /// <summary>
            /// Gets the matchmaking criteria according to the claimant.
            /// </summary>
            public MatchmakingCriteria Criteria { get; private set; }

            /// <summary>
            /// Gets the match capacity according to the claimant.
            /// </summary>
            public MatchCapacity Capacity { get; private set; }
        }
    }
}
