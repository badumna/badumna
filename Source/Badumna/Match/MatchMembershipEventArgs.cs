﻿//-----------------------------------------------------------------------
// <copyright file="MatchMembershipEventArgs.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2013 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.Match
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Badumna.DataTypes;

    /// <summary>
    /// Provides data for a membership events.
    /// </summary>
    public struct MatchMembershipEventArgs
    {
        /// <summary>
        /// The member who changed.
        /// </summary>
        private readonly MemberIdentity member;

        /// <summary>
        /// Initializes a new instance of the MatchMembershipEventArgs struct.
        /// </summary>
        /// <param name="member">The member that changed.</param>
        public MatchMembershipEventArgs(MemberIdentity member)
        {
            this.member = member;
        }

        /// <summary>
        /// Gets the member that changed.
        /// </summary>
        public MemberIdentity Member
        {
            get { return this.member; }
        }
    }
}
