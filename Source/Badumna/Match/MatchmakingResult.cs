﻿//------------------------------------------------------------------------------
// <copyright file="MatchmakingResult.cs" company="National ICT Australia Limited">
//     Copyright (c) 2013 National ICT Australia Limited. All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

namespace Badumna.Match
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Badumna.Core;

    /// <summary>
    /// Details of a match reported by the matchmaking system.
    /// </summary>
    public class MatchmakingResult : IParseable
    {
        /// <summary>
        /// Initializes a new instance of the MatchmakingResult class.
        /// </summary>
        /// <param name="certificate">The certificate identifying the host</param>
        /// <param name="status">The current match status</param>
        /// <param name="criteria">The matchmaking criteria</param>
        internal MatchmakingResult(HostAuthorizationCertificate certificate, MatchCapacity status, MatchmakingCriteria criteria)
        {
            this.Certificate = certificate;
            this.Capacity = status;
            this.Criteria = criteria;
        }

        /// <summary>
        /// Initializes a new instance of the MatchmakingResult class for use as IParseable.
        /// </summary>
        internal MatchmakingResult()
        {
        }

        /// <summary>
        /// Gets the certificate identifying the host.
        /// </summary>
        public HostAuthorizationCertificate Certificate { get; private set; }

        /// <summary>
        /// Gets the current match status.
        /// </summary>
        public MatchCapacity Capacity { get; private set; }

        /// <summary>
        /// Gets the matchmaking criteria.
        /// </summary>
        public MatchmakingCriteria Criteria { get; private set; }

        /// <inheritdoc />
        void IParseable.ToMessage(MessageBuffer message, Type parameterType)
        {
            message.Write(this.Certificate);
            message.Write(this.Capacity);
            message.Write(this.Criteria);
        }

        /// <inheritdoc />
        void IParseable.FromMessage(MessageBuffer message)
        {
            this.Certificate = message.Read<HostAuthorizationCertificate>();
            this.Capacity = message.Read<MatchCapacity>();
            this.Criteria = message.Read<MatchmakingCriteria>();
        }
    }
}
