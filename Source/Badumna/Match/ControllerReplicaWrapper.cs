﻿//-----------------------------------------------------------------------
// <copyright file="ControllerReplicaWrapper.cs" company="Scalify">
//     Copyright (c) 2013 Scalify. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.Match
{
    using System;
    using System.IO;
    using Badumna.Core;
    using Badumna.DataTypes;
    using Badumna.Replication;
    using Badumna.Utilities;

    /// <summary>
    /// Replica wrapper used in match to represent an original entity's replica.
    /// </summary>
    internal class ControllerReplicaWrapper : IReplica
    {
        /// <summary>
        /// The queue to use for scheduling events.
        /// </summary>
        private readonly INetworkEventScheduler eventQueue;

        /// <summary>
        /// Match entity manager.
        /// </summary>
        private readonly EntityManager entityManager;

        /// <summary>
        /// The method to use to queue events that must run in the application's thread.
        /// </summary>
        private readonly QueueInvokable queueApplicationEvent;

        /// <summary>
        /// The IReplica for the controller.
        /// </summary>
        private IReplica replica;

        /// <summary>
        /// ID of the match the controller belongs to.
        /// </summary>
        private BadumnaId matchID = new BadumnaId();

        /// <summary>
        /// Initializes a new instance of the ControllerReplicaWrapper class.
        /// </summary>
        /// <param name="guid">The entityID for the controller replica.</param>
        /// <param name="eventQueue">The event queue to schedule events on.</param>
        /// <param name="entityManager">Match entity manager.</param>
        /// <param name="queueApplicationEvent">The method to use to queue events that must run in the application's thread.</param>
        public ControllerReplicaWrapper(
            BadumnaId guid,
            INetworkEventScheduler eventQueue,
            EntityManager entityManager,
            QueueInvokable queueApplicationEvent)
        {
            this.Guid = guid;
            this.eventQueue = eventQueue;
            this.entityManager = entityManager;
            this.queueApplicationEvent = queueApplicationEvent;
        }

        /// <summary>
        /// Gets or sets the unique identifier of the spatial replicas in the collection.
        /// </summary>
        public BadumnaId Guid { get; set; }

        /// <summary>
        /// Gets the ID of the match the controller belongs to.
        /// </summary>
        public BadumnaId MatchID
        {
            get { return this.matchID; }
        }

        /// <summary>
        /// Gets the timestamp of the hosting certificate authorizing hosting of the entity.
        /// </summary>
        public TimeSpan CertificateTimestamp
        {
            get;
            private set;
        }

        /// <inheritdoc/>
        public double Deserialize(BooleanArray includedParts, Stream stream, int estimatedMillisecondsSinceDeparture, BadumnaId id)
        {
            Logger.TraceInformation(LogTag.Match, "Deserialing controller replica wrapper.");
            using (var reader = new BinaryReader(stream))
            {
                if (includedParts[(int)Badumna.SpatialEntities.SpatialEntityStateSegment.Match])
                {
                    var length = reader.ReadInt32();
                    this.matchID.FromBytes(reader.ReadBytes(length));

                    this.CertificateTimestamp = TimeSpan.FromTicks(reader.ReadInt64());

                    if (this.replica == null)
                    {
                        if (this.entityManager.TakeOverControllerAsReplica(this))
                        {
                            this.replica = this.entityManager.GetControllerAutoReplicaWrapper(this.MatchID);
                            Logger.TraceInformation(LogTag.Match, "Controller replica wrapper taking over: {0}.", this.replica != null);
                        }
                    }
                }

                if (this.replica != null)
                {
                    return this.replica.Deserialize(includedParts, stream, estimatedMillisecondsSinceDeparture, id);
                }
                else
                {
                    return 0;
                }
            }
        }

        /// <summary>
        /// Stop passing updates to this replica wrapper on to the controller.
        /// </summary>
        public void Release()
        {
            this.replica = null;
        }

        /// <inheritdoc/>
        public void HandleEvent(Stream stream)
        {
            this.replica.HandleEvent(stream);
        }
    }
}
