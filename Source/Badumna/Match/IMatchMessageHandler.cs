﻿//-----------------------------------------------------------------------
// <copyright file="IMatchMessageHandler.cs" company="Scalify Pty Ltd">
//     Copyright (c); 2013 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.Match
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Badumna.Core;
    using Badumna.DataTypes;

    /// <summary>
    /// Interface for handling match messages.
    /// </summary>
    internal interface IMatchMessageHandler
    {
        /// <summary>
        /// Called when a join-request has been received from another peer.
        /// </summary>
        /// <param name="clientIdentity">The client's identity.</param>
        /// <param name="matchIdentifier">The match ID.</param>
        void OnJoinRequest(MemberIdentity clientIdentity, BadumnaId matchIdentifier);

        /// <summary>
        /// Called when a keep-alive has been received from another peer.
        /// </summary>
        /// <param name="clientIdentity">The client's identity.</param>
        /// <param name="matchIdentifier">The match ID.</param>
        void OnPingFromClientToHost(MemberIdentity clientIdentity, BadumnaId matchIdentifier);

        /// <summary>
        /// Called when a leave notification is received from another peer.
        /// </summary>
        /// <param name="client">The sender ID.</param>
        /// <param name="matchIdentifier">The match ID.</param>
        void OnLeaveNotification(BadumnaId client, BadumnaId matchIdentifier);
        
        /// <summary>
        /// Called when notification is received from a new host that this member has successfully joined.
        /// </summary>
        /// <remarks>Receipt of this message implies the host considers the receiving peer a member.</remarks>
        /// <param name="certificate">Certificate authorizing a host for the match.</param>
        /// <param name="members">The set of members currently recognized by the host.</param>
        void OnJoinConfirmation(HostAuthorizationCertificate certificate, IEnumerable<MemberIdentity> members);

        /// <summary>
        /// Called when the local peer's membership of the match has been rejected by the current host.
        /// </summary>
        /// <param name="certificate">Certificate authorizing the host for the match.</param>
        void OnJoinRejection(HostAuthorizationCertificate certificate);

        /// <summary>
        /// Called when peer sends a message denying that it is the host.
        /// </summary>
        /// <param name="sender">The ID of the member whose client ping is being replied to.</param>
        /// <param name="certificate">Latest certified host known to the peer.</param>
        void OnHostDenial(BadumnaId sender, HostAuthorizationCertificate certificate);

        /// <summary>
        /// Handle a new member notification from a host.
        /// </summary>
        /// <param name="certificate">Certificate authorizing the host for the match.</param>
        /// <param name="memberIdentity">The new member.</param>
        void OnMemberJoinNotification(
            HostAuthorizationCertificate certificate,
            MemberIdentity memberIdentity);

        /// <summary>
        /// Handle a member leave notification from a host.
        /// </summary>
        /// <param name="certificate">Certificate authorizing the host for the match.</param>
        /// <param name="member">The member that left.</param>
        void OnMemberLeaveNotification(
            HostAuthorizationCertificate certificate,
            BadumnaId member);

        /// <summary>
        /// Handle a resignation message from the host
        /// </summary>
        /// <param name="certificate">The resigning host's certificate.</param>
        /// <param name="takeoverCode">Password that allows other peers to takeover hosting before current host times out.</param>
        void OnHostResignation(HostAuthorizationCertificate certificate, int takeoverCode);

        /// <summary>
        /// Called when another peer has pinged to check the local peer is still in the match.
        /// </summary>
        /// <param name="sender">The sender ID.</param>
        /// <param name="matchIdentifier">The match ID.</param>
        void OnPingToPotentialHost(BadumnaId sender, BadumnaId matchIdentifier);

        /// <summary>
        /// Called when a response to a ping has been received.
        /// </summary>
        /// <param name="peer">The peer the pong is from.</param>
        /// <param name="inMatch">A value indicating whether the peer still wants to be in the match.</param>
        /// <param name="recognizesThisPeer">A value indicating whether the peer recognizes this peer as a member of
        /// the match.</param>
        /// <param name="isConnected">A value indicating whether the peer is still connected to a host.</param>
        /// <param name="certificate">Latest certificate for the match known of by the peer.</param>
        void OnPongFromPotentialHost(
            BadumnaId peer,
            bool inMatch,
            bool recognizesThisPeer,
            bool isConnected,
            HostAuthorizationCertificate certificate);

        /// <summary>
        /// Called when a chat message has been received.
        /// </summary>
        /// <param name="sender">The ID of the member who sent the message.</param>
        /// <param name="matchIdentifier">The ID of the match it belongs to.</param>
        /// <param name="message">The chat message.</param>
        /// <param name="type">The type of message received (public or private).</param>
        void OnChat(BadumnaId sender, BadumnaId matchIdentifier, string message, ChatType type);
    }
}
