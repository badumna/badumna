﻿//------------------------------------------------------------------------------
// <copyright file="MatchMessagingService.cs" company="National ICT Australia Limited">
//     Copyright (c) 2013 National ICT Australia Limited. All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

namespace Badumna.Match
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using Badumna.Core;
    using Badumna.DataTypes;
    using Badumna.Transport;
    using Badumna.Utilities;
    using Badumna.Utilities.Logging;

    /// <summary>
    /// Maintains a record of matches, including the host for the match.
    /// Responds to queries for active matches.
    /// </summary>
    internal class MatchMessagingService : IMatchProtocol
    {
        /// <summary>
        /// The time keeper.
        /// </summary>
        private readonly ITime timeKeeper;

        /// <summary>
        /// The transport.
        /// </summary>
        private readonly TransportProtocol transport;

        /// <summary>
        /// Entity manager for passing controller RPCs to.
        /// </summary>
        private readonly EntityManager entityManager;

        /// <summary>
        /// Registered match message handlers.
        /// </summary>
        private readonly Dictionary<BadumnaId, IMatchMessageHandler> messageHandlers
            = new Dictionary<BadumnaId, IMatchMessageHandler>();

        /// <summary>
        /// Initializes a new instance of the MatchMessagingService class.
        /// </summary>
        /// <param name="timeKeeper">The time keeper.</param>
        /// <param name="transport">The transport protocol.</param>
        /// <param name="entityManager">The match entity manager.</param>
        public MatchMessagingService(ITime timeKeeper, ITransportProtocol transport, EntityManager entityManager)
        {
            this.timeKeeper = timeKeeper;
            this.transport = (TransportProtocol)transport;
            this.entityManager = entityManager;

            this.transport.RegisterMethodsIn(this);
        }

        /// <inheritdoc/>
        public void RegisterMessageHandler(BadumnaId handlerID, IMatchMessageHandler handler)
        {
            this.messageHandlers.Add(handlerID, handler);
        }

        /// <inheritdoc/>
        public void DeregisterMessageHandler(BadumnaId handlerID)
        {
            this.messageHandlers.Remove(handlerID);
        }

        /// <inheritdoc/>
        public void JoinRequest(BadumnaId hostID, MemberIdentity clientIdentity, BadumnaId matchIdentifier)
        {
            Logger.TraceInformation(
                LogTag.Match | LogTag.SendProtocolMethod,
                "JoinRequest: from {0} to {1}",
                clientIdentity,
                hostID);
            this.transport.SendRemoteCall(
                hostID.Address,
                QualityOfService.Reliable,
                this.HandleJoinRequest,
                hostID.LocalId,
                clientIdentity.ID.LocalId,
                clientIdentity.Name,
                matchIdentifier);
        }

        /// <inheritdoc/>
        public void PingFromClientToHost(BadumnaId hostID, MemberIdentity clientIdentity, BadumnaId matchIdentifier)
        {
            Logger.TraceInformation(
                LogTag.Match | LogTag.SendProtocolMethod,
                "PingFromClientToHost: from {0} to {1}",
                clientIdentity,
                hostID);
            this.transport.SendRemoteCall(
                hostID.Address,
                QualityOfService.Reliable,
                this.HandlePingFromClientToHost,
                hostID.LocalId,
                clientIdentity.ID.LocalId,
                clientIdentity.Name,
                matchIdentifier);
        }

        /// <inheritdoc/>
        public void LeaveNotification(BadumnaId hostID, BadumnaId clientID, BadumnaId matchIdentifier)
        {
            Logger.TraceInformation(
                LogTag.Match | LogTag.SendProtocolMethod,
                "LeaveNotification: from {0} to {1}",
                clientID,
                hostID);
            this.transport.SendRemoteCall(
                hostID.Address,
                QualityOfService.Reliable,
                this.HandleLeaveNotification,
                hostID.LocalId,
                clientID.LocalId,
                matchIdentifier);
        }

        /// <inheritdoc/>
        public void JoinConfirmation(BadumnaId clientID, HostAuthorizationCertificate certificate, IEnumerable<MemberIdentity> members)
        {
            Logger.TraceInformation(
                LogTag.Match | LogTag.SendProtocolMethod,
                "JoinConfirmation: from {0} to {1}",
                certificate.Host,
                clientID);
            this.transport.SendRemoteCall(
                clientID.Address,
                QualityOfService.Reliable,
                this.HandleJoinConfirmation,
                clientID.LocalId,
                certificate,
                members);
        }

        /// <inheritdoc/>
        public void JoinRejection(BadumnaId clientID, HostAuthorizationCertificate certificate)
        {
            Logger.TraceInformation(
                LogTag.Match | LogTag.SendProtocolMethod,
                "JoinRejection: from {0} to {1}",
                certificate.Host,
                clientID);
            this.transport.SendRemoteCall(
                clientID.Address,
                QualityOfService.Reliable,
                this.HandleJoinRejection,
                clientID.LocalId,
                certificate);
        }

        /// <inheritdoc/>
        public void HostDenial(BadumnaId clientID, BadumnaId nonHostID, HostAuthorizationCertificate certificate)
        {
            Logger.TraceInformation(
                LogTag.Match | LogTag.SendProtocolMethod,
                "HostDenial: from {0} to {1}",
                nonHostID,
                clientID);
            this.transport.SendRemoteCall(
                clientID.Address,
                QualityOfService.Reliable,
                this.HandleHostDenial,
                clientID.LocalId,
                nonHostID.LocalId,
                certificate);
        }

        /// <inheritdoc/>
        public void MemberJoinNotification(BadumnaId clientID, HostAuthorizationCertificate certificate, MemberIdentity memberIdentity)
        {
            Logger.TraceInformation(
                LogTag.Match | LogTag.SendProtocolMethod,
                "MemberJoinNotification: from {0} to {1} about {2}",
                certificate.Host,
                clientID,
                memberIdentity);
            this.transport.SendRemoteCall(
                clientID.Address,
                QualityOfService.Reliable,
                this.HandleMemberJoinNotification,
                clientID.LocalId,
                certificate,
                memberIdentity);
        }

        /// <inheritdoc/>
        public void MemberLeaveNotification(BadumnaId clientID, HostAuthorizationCertificate certificate, BadumnaId member)
        {
            Logger.TraceInformation(
                LogTag.Match | LogTag.SendProtocolMethod,
                "MemberLeaveNotification: from {0} to {1} about {2}",
                certificate.Host,
                clientID,
                member);
            this.transport.SendRemoteCall(
                clientID.Address,
                QualityOfService.Reliable,
                this.HandleMemberLeaveNotification,
                clientID.LocalId,
                certificate,
                member);
        }

        /// <inheritdoc/>
        public void HostResignation(BadumnaId client, HostAuthorizationCertificate certificate, int takeoverCode)
        {
            Logger.TraceInformation(
                LogTag.Match | LogTag.SendProtocolMethod,
                "HostResignation: from {0} to {1}",
                certificate.Host,
                client);
            this.transport.SendRemoteCall(
                client.Address,
                QualityOfService.Reliable,
                this.HandleHostResignation,
                client.LocalId,
                certificate,
                takeoverCode);
        }

        /// <inheritdoc/>
        public void PingToPotentialHost(BadumnaId potentialHost, BadumnaId sender, BadumnaId matchIdentifier)
        {
            Logger.TraceInformation(
                LogTag.Match | LogTag.SendProtocolMethod,
                "PingToPotentialHost: from {0} to {1}",
                sender,
                potentialHost);
            this.transport.SendRemoteCall(
                potentialHost.Address,
                QualityOfService.Reliable,
                this.HandlePingToPotentialHost,
                potentialHost.LocalId,
                sender.LocalId,
                matchIdentifier);
        }

        /// <inheritdoc/>
        public void PongFromPotentialHost(BadumnaId pingerID, BadumnaId sender, bool inMatch, bool recognizesPeer, bool isConnected, HostAuthorizationCertificate certificate)
        {
            Logger.TraceInformation(
                LogTag.Match | LogTag.SendProtocolMethod,
                "PongFromPotentialHost: from {0} to {1}",
                sender,
                pingerID);
            byte bitField = 0;
            if (inMatch)
            {
                bitField |= 1;
            }

            if (recognizesPeer)
            {
                bitField |= 2;
            }

            if (isConnected)
            {
                bitField |= 4;
            }

            this.transport.SendRemoteCall(
                pingerID.Address,
                QualityOfService.Reliable,
                this.HandlePongFromPotentialHost,
                pingerID.LocalId,
                sender.LocalId,
                inMatch,
                recognizesPeer,
                isConnected,
                certificate);
        }

        /// <inheritdoc/>
        public void Chat(BadumnaId recipient, BadumnaId sender, BadumnaId matchIdentifier, string message, ChatType type)
        {
            Logger.TraceInformation(
                LogTag.Match | LogTag.SendProtocolMethod,
                "Chat: from {0} to {1}: {2}",
                sender,
                recipient,
                message);
            this.transport.SendRemoteCall(
                recipient.Address,
                QualityOfService.Reliable,
                this.HandleChat,
                recipient.LocalId,
                sender.LocalId,
                matchIdentifier,
                message,
                (byte)type);
        }

        /// <inheritdoc/>
        public void SendControllerRPC(BadumnaId recipient, MemoryStream stream)
        {
            Logger.TraceInformation(
                LogTag.Match | LogTag.SendProtocolMethod,
                "Sending controller RPC to {0}",
                recipient);
            this.transport.SendRemoteCall(
                recipient.Address,
                QualityOfService.Reliable,
                this.HandleControllerRPC,
                recipient.LocalId,
                stream.GetBuffer());
        }

        /// <inheritdoc/>
        [ConnectionfulProtocolMethod(ConnectionfulMethod.MatchJoinRequest)]
        private void HandleJoinRequest(ushort hostLocalID, ushort clientLocalID, string clientName, BadumnaId matchIdentifier)
        {
            var hostID = new BadumnaId(this.transport.CurrentEnvelope.Destination, hostLocalID);
            var clientID = new BadumnaId(this.transport.CurrentEnvelope.Source, clientLocalID);
            Logger.TraceInformation(
                LogTag.Match | LogTag.ProtocolMethod,
                "HandleJoinRequest: from {0} to {1}",
                clientID,
                hostID);
            var clientIdentity = new MemberIdentity(clientID, clientName);
            IMatchMessageHandler handler;
            if (this.messageHandlers.TryGetValue(hostID, out handler))
            {
                handler.OnJoinRequest(clientIdentity, matchIdentifier);
            }
        }

        /// <inheritdoc/>
        [ConnectionfulProtocolMethod(ConnectionfulMethod.MatchPingFromClientToHost)]
        private void HandlePingFromClientToHost(ushort hostLocalID, ushort clientLocalID, string clientName, BadumnaId matchIdentifier)
        {
            var hostID = new BadumnaId(this.transport.CurrentEnvelope.Destination, hostLocalID);
            var clientID = new BadumnaId(this.transport.CurrentEnvelope.Source, clientLocalID);
            Logger.TraceInformation(
                LogTag.Match | LogTag.ProtocolMethod,
                "HandlePingFromClientToHost: from {0} to {1}",
                clientID,
                hostID);
            var clientIdentity = new MemberIdentity(clientID, clientName);
            IMatchMessageHandler handler;
            if (this.messageHandlers.TryGetValue(hostID, out handler))
            {
                handler.OnPingFromClientToHost(clientIdentity, matchIdentifier);
            }
        }

        /// <inheritdoc/>
        [ConnectionfulProtocolMethod(ConnectionfulMethod.MatchLeaveNotification)]
        private void HandleLeaveNotification(ushort hostLocalID, ushort clientLocalID, BadumnaId matchIdentifier)
        {
            var hostID = new BadumnaId(this.transport.CurrentEnvelope.Destination, hostLocalID);
            var clientID = new BadumnaId(this.transport.CurrentEnvelope.Source, clientLocalID);
            Logger.TraceInformation(
                LogTag.Match | LogTag.ProtocolMethod,
                "HandleLeaveNotification: from {0} to {1}",
                clientID,
                hostID);
            IMatchMessageHandler handler;
            if (this.messageHandlers.TryGetValue(hostID, out handler))
            {
                handler.OnLeaveNotification(clientID, matchIdentifier);
            }
        }

        /// <summary>
        /// Pass join confirmation to addressee if registered.
        /// </summary>
        /// <param name="clientLocalID">The local ID for the adressee.</param>
        /// <param name="certificate">The host certificate.</param>
        /// <param name="members">The match members.</param>
        [ConnectionfulProtocolMethod(ConnectionfulMethod.MatchJoinConfirmation)]
        private void HandleJoinConfirmation(ushort clientLocalID, HostAuthorizationCertificate certificate, IEnumerable<MemberIdentity> members)
        {
            var clientID = new BadumnaId(this.transport.CurrentEnvelope.Destination, clientLocalID);
            Logger.TraceInformation(
                LogTag.Match | LogTag.ProtocolMethod,
                "HandleJoinConfirmation: from {0} to {1}",
                certificate.Host,
                clientID);
            IMatchMessageHandler handler;
            if (this.messageHandlers.TryGetValue(clientID, out handler))
            {
                handler.OnJoinConfirmation(certificate, members);
            }
        }

        /// <summary>
        /// Pass join rejection to addressee if registered.
        /// </summary>
        /// <param name="clientLocalID">The local ID of the addressee.</param>
        /// <param name="certificate">The host certificate.</param>
        [ConnectionfulProtocolMethod(ConnectionfulMethod.MatchJoinRejection)]
        private void HandleJoinRejection(ushort clientLocalID, HostAuthorizationCertificate certificate)
        {
            var clientID = new BadumnaId(this.transport.CurrentEnvelope.Destination, clientLocalID);
            Logger.TraceInformation(
                LogTag.Match | LogTag.ProtocolMethod,
                "HandleJoinRejection: from {0} to {1}",
                certificate.Host,
                clientID);
            IMatchMessageHandler handler;
            if (this.messageHandlers.TryGetValue(clientID, out handler))
            {
                handler.OnJoinRejection(certificate);
            }
        }

        /// <summary>
        /// Pass host denial to addressee if registered.
        /// </summary>
        /// <param name="clientLocalID">The local ID of the addressee.</param>
        /// <param name="nonHostLocalID">The local ID of the sender.</param>
        /// <param name="certificate">The host certificate.</param>
        [ConnectionfulProtocolMethod(ConnectionfulMethod.MatchHostDenial)]
        private void HandleHostDenial(ushort clientLocalID, ushort nonHostLocalID, HostAuthorizationCertificate certificate)
        {
            var clientID = new BadumnaId(this.transport.CurrentEnvelope.Destination, clientLocalID);
            var nonHostID = new BadumnaId(this.transport.CurrentEnvelope.Source, nonHostLocalID);
            Logger.TraceInformation(
                LogTag.Match | LogTag.ProtocolMethod,
                "HandleHostDenial: from {0} to {1}",
                nonHostID,
                clientID);
            IMatchMessageHandler handler;
            if (this.messageHandlers.TryGetValue(clientID, out handler))
            {
                handler.OnHostDenial(nonHostID, certificate);
            }
        }

        /// <summary>
        /// Pass new member notification from host to client if registered.
        /// </summary>
        /// <param name="clientLocalID">The local ID of the addressee.</param>
        /// <param name="certificate">The host certificate.</param>
        /// <param name="memberIdentity">The new member.</param>
        [ConnectionfulProtocolMethod(ConnectionfulMethod.MatchMemberJoinNotification)]
        private void HandleMemberJoinNotification(
            ushort clientLocalID,
            HostAuthorizationCertificate certificate,
            MemberIdentity memberIdentity)
        {
            var clientID = new BadumnaId(this.transport.CurrentEnvelope.Destination, clientLocalID);
            Logger.TraceInformation(
                LogTag.Match | LogTag.ProtocolMethod,
                "HandleMemberJoinNotification: from {0} to {1} for new member {2}.",
                certificate.Host,
                clientID,
                memberIdentity);
            IMatchMessageHandler handler;
            if (this.messageHandlers.TryGetValue(clientID, out handler))
            {
                handler.OnMemberJoinNotification(certificate, memberIdentity);
            }
        }

        /// <summary>
        /// Pass member departure notification from host to client if registered.
        /// </summary>
        /// <param name="clientLocalID">The local ID of the addressee.</param>
        /// <param name="certificate">The host certificate.</param>
        /// <param name="member">The new member.</param>
        [ConnectionfulProtocolMethod(ConnectionfulMethod.MatchMemberLeaveNotification)]
        private void HandleMemberLeaveNotification(
            ushort clientLocalID,
            HostAuthorizationCertificate certificate,
            BadumnaId member)
        {
            Logger.TraceInformation(LogTag.Match | LogTag.ProtocolMethod, "HandleMemberLeaveNotification");
            var clientID = new BadumnaId(this.transport.CurrentEnvelope.Destination, clientLocalID);
            Logger.TraceInformation(
                LogTag.Match | LogTag.ProtocolMethod,
                "HandleMemberLeaveNotification: from {0} to {1} for new member {2}.",
                certificate.Host,
                clientID,
                member);
            IMatchMessageHandler handler;
            if (this.messageHandlers.TryGetValue(clientID, out handler))
            {
                handler.OnMemberLeaveNotification(certificate, member);
            }
        }

        /// <summary>
        /// Pass resignation from host to client if registered.
        /// </summary>
        /// <param name="clientLocalID">The local ID of the client.</param>
        /// <param name="certificate">The host certificate.</param>
        /// <param name="takeoverCode">Password that allows other peers to takeover hosting before current host times out.</param>
        [ConnectionfulProtocolMethod(ConnectionfulMethod.MatchHostResignation)]
        private void HandleHostResignation(ushort clientLocalID, HostAuthorizationCertificate certificate, int takeoverCode)
        {
            var clientID = new BadumnaId(this.transport.CurrentEnvelope.Destination, clientLocalID);
            Logger.TraceInformation(
                LogTag.Match | LogTag.ProtocolMethod,
                "HandleHostResignation: from {0} to {1}.",
                certificate.Host,
                clientID);

            // Ignore resignations for a certified host that don't come from that host.
            if (!this.transport.CurrentEnvelope.Source.Equals(certificate.Host.Address))
            {
                Logger.TraceWarning(LogTag.Match, "Ignoring host resignation from wrong source.");
                return;
            }

            IMatchMessageHandler handler;
            if (this.messageHandlers.TryGetValue(clientID, out handler))
            {
                handler.OnHostResignation(certificate, takeoverCode);
            }
        }

        /// <summary>
        /// Pass a ping from a peer to a potential host, if registered.
        /// </summary>
        /// <param name="potentialHostLocalID">The local ID of the potential host.</param>
        /// <param name="senderLocalID">The local ID of the pinger.</param>
        /// <param name="matchIdentifier">The ID of the match.</param>
        [ConnectionfulProtocolMethod(ConnectionfulMethod.MatchPingToPotentialHost)]
        private void HandlePingToPotentialHost(ushort potentialHostLocalID, ushort senderLocalID, BadumnaId matchIdentifier)
        {
            var potentialHostID = new BadumnaId(this.transport.CurrentEnvelope.Destination, potentialHostLocalID);
            var senderID = new BadumnaId(this.transport.CurrentEnvelope.Source, senderLocalID);
            Logger.TraceInformation(
                LogTag.Match | LogTag.ProtocolMethod,
                "HandlePingToPotentialHost: from {0} to {1}.",
                senderID,
                potentialHostID);
            IMatchMessageHandler handler;
            if (this.messageHandlers.TryGetValue(potentialHostID, out handler))
            {
                handler.OnPingToPotentialHost(senderID, matchIdentifier);
            }
            else
            {
                // Send a negative pong if no handler is found as the member must have left the match,
                // but it's peer is still running, so no connection-lost will be sent to the sender.
                this.PongFromPotentialHost(
                    senderID,
                    potentialHostID,
                    false,
                    false,
                    false,
                    HostAuthorizationCertificate.Empty);
            }
        }

        /// <summary>
        /// Pass a pong from a potential host back to the pinger, if registered.
        /// </summary>
        /// <param name="pingerLocalID">The local ID of the pinger.</param>
        /// <param name="potentialHostLocalID">The local ID of the potential host.</param>
        /// <param name="inMatch">A value indicating whether the potential host still wants to be in the match.</param>
        /// <param name="recognizesPeer">A value indicating whether the potential host recognizes the pinger as a member of the match.</param>
        /// <param name="isConnected">A value indicating whether the potential host is connected to the current match host.</param>
        /// <param name="certificate">The latest certificate the potential host knows of.</param>
        [ConnectionfulProtocolMethod(ConnectionfulMethod.MatchPongFromPotentialHost)]
        private void HandlePongFromPotentialHost(
            ushort pingerLocalID,
            ushort potentialHostLocalID,
            bool inMatch,
            bool recognizesPeer,
            bool isConnected,
            ////byte bitField,
            HostAuthorizationCertificate certificate)
        {
            var potentialHostID = new BadumnaId(this.transport.CurrentEnvelope.Source, potentialHostLocalID);
            var pingerID = new BadumnaId(this.transport.CurrentEnvelope.Destination, pingerLocalID);
            Logger.TraceInformation(
                LogTag.Match | LogTag.ProtocolMethod,
                "HandlePongFromPotentialHost: from {0} to {1}.",
                potentialHostID,
                pingerID);
            IMatchMessageHandler handler;
            if (this.messageHandlers.TryGetValue(pingerID, out handler))
            {
                ////bool inMatch = bitField & 1 > 0;
                ////bool recognizesPeer = bitField & 2 > 0;
                ////bool isConnected = bitField & 4 > 0;
                handler.OnPongFromPotentialHost(potentialHostID, inMatch, recognizesPeer, isConnected, certificate);
            }
        }

        /// <inheritdoc/>
        [ConnectionfulProtocolMethod(ConnectionfulMethod.MatchChat)]
        private void HandleChat(ushort recipientLocalID, ushort senderLocalID, BadumnaId matchIdentifier, string message, byte type)
        {
            var recipientID = new BadumnaId(this.transport.CurrentEnvelope.Destination, recipientLocalID);
            var senderID = new BadumnaId(this.transport.CurrentEnvelope.Source, senderLocalID);
            Logger.TraceInformation(
                LogTag.Match | LogTag.ProtocolMethod,
                "HandleChat: from {0} to {1}: {2}",
                senderID,
                recipientID,
                message);
            IMatchMessageHandler handler;
            if (this.messageHandlers.TryGetValue(recipientID, out handler))
            {
                handler.OnChat(senderID, matchIdentifier, message, (ChatType)type);
            }
        }

        /// <inheritdoc/>
        [ConnectionfulProtocolMethod(ConnectionfulMethod.MatchControllerRPC)]
        private void HandleControllerRPC(ushort recipientLocalID, byte[] data)
        {
            var recipientID = new BadumnaId(this.transport.CurrentEnvelope.Destination, recipientLocalID);
            this.entityManager.HandleControllerRPC(recipientID, data);
        }
    }
}
