﻿//-----------------------------------------------------------------------
// <copyright file="Match.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2013 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.Match
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using Badumna.Autoreplication;
    using Badumna.Autoreplication.Serialization;
    using Badumna.Core;
    using Badumna.DataTypes;
    using Badumna.Replication;
    using Badumna.Utilities;
    using MatchAutoreplicationManager = Autoreplication.MatchAutoreplicationManager;

    /// <summary>
    /// A delegate called by the network layer when a new entity joins the match.
    /// The delegate should return the replica entity to which remote updates and custom messages will be applied.
    /// </summary>
    /// <param name="match">The match the entity belongs to.</param>
    /// <param name="source">The source of the entity (peer or host).</param>
    /// <param name="entityType">The application level type ID that was associated with this entity when the entity was registered on the
    /// owning peer.</param>
    /// <returns>An instance of the new entity.</returns>
    public delegate object CreateReplica(Match match, EntitySource source, uint entityType);

    /// <summary>
    /// A delegate called by the network layer when an entity leaves the match.
    /// This delegate gives the application layer the opportunity to cleanup any references to the given replica. It indicates
    /// that no more updates or custom messages will arrive for this replica.
    /// </summary>
    /// <param name="match">The match the entity belongs to.</param>
    /// <param name="source">The source of the entity (peer or host).</param>
    /// <param name="replica">The replica being removed.</param>
    public delegate void RemoveReplica(Match match, EntitySource source, object replica);

    /// <summary>
    /// Represents a match for a finite set of players.
    /// </summary>
    /// <remarks>
    /// Matches support:
    ///  - replication
    ///  - chat
    ///  - host logic
    /// </remarks>
    public class Match
    {
        /// <summary>
        /// The matchmaker grants hosting rights for the match.
        /// </summary>
        private readonly IMatchmaker matchmaker;

        /// <summary>
        /// Service for exchaning messages with other match members.
        /// </summary>
        private readonly IMatchProtocol messagingService;

        /// <summary>
        /// Match entity manager.
        /// </summary>
        private readonly EntityManager entityManager;

        /// <summary>
        /// Unique ID for the match.
        /// </summary>
        private readonly BadumnaId matchIdentifier;

        /// <summary>
        /// The ID of the local member;
        /// </summary>
        private readonly MemberIdentity memberIdentity;

        /// <summary>
        /// Object to act as match controller.
        /// </summary>
        private readonly object controllerEntity;

        /// <summary>
        /// For pushing work to be done on network thread.
        /// </summary>
        private readonly INetworkEventScheduler networkEventQueue;

        /// <summary>
        /// Method for executing code on application thread.
        /// </summary>
        private readonly QueueInvokable queueApplicationEvent;

        /// <summary>
        /// List of members for access in application thread, synchronized with
        /// member add/remove events.
        /// </summary>
        private readonly List<MemberIdentity> members;

        /// <summary>
        /// For adding autoreplication support to entities.
        /// </summary>
        private readonly MatchAutoreplicationManager autoreplicationManager;
        
        /// <summary>
        /// Manages membership of the match.
        /// </summary>
        private MembershipManager membership;

        /// <summary>
        /// Current state of the match.
        /// </summary>
        private MatchStatus state;

        /// <summary>
        /// Error resulting in failure, if failed.
        /// </summary>
        private MatchError error;

        /// <summary>
        /// Called when a new entity needs to be instantiated into the match.
        /// </summary>
        private CreateReplica createReplica;

        /// <summary>
        /// Called when an entity leave the match.
        /// </summary>
        private RemoveReplica removeReplica;

        /// <summary>
        /// Explicit backing delegate for state change, required to prevent JITing on Unity iPhone.
        /// </summary>
        private GenericEventHandler<Match, MatchStatusEventArgs> stateChangedDelegate;

        /// <summary>
        /// Explicit backing delegate for member added, required to prevent JITing on Unity iPhone.
        /// </summary>
        private GenericEventHandler<Match, MatchMembershipEventArgs> memberAddedDelegate;

        /// <summary>
        /// Explicit backing delegate for member removed, required to prevent JITing on Unity iPhone.
        /// </summary>
        private GenericEventHandler<Match, MatchMembershipEventArgs> memberRemovedDelegate;

        /// <summary>
        /// Explicit backing delegate for chat message received, required to prevent JITing on Unity iPhone.
        /// </summary>
        private GenericEventHandler<Match, MatchChatEventArgs> chatMessageReceivedDelegate;

        /// <summary>
        /// Initializes a new instance of the Match class.
        /// </summary>
        /// <param name="connectionNotifier">For subscribing to connectivity events.</param>
        /// <param name="certificate">Certificate of hosting authority.</param>
        /// <param name="criteria">Matchmaking criteria.</param>
        /// <param name="maxPlayers">Maximum number of players permitted in the match.</param>
        /// <param name="memberIdentitiy">ID for this member.</param>
        /// <param name="serverAddress">Address of the matchmaking server.</param>
        /// <param name="timeKeeper">Provides access to current time.</param>
        /// <param name="matchmaker">The matchmaker.</param>
        /// <param name="messagingService">Protocol for sending messages to the host and members of the match.</param>
        /// <param name="entityManager">Match entity manager.</param>
        /// <param name="createReplica">Called when a new entity needs to be instantiated into the match.</param>
        /// <param name="removeReplica">Called when an entity leave the match.</param>
        /// <param name="networkEventQueue">For pushing work to be done on the network thread.</param>
        /// <param name="queueApplicationCallback">Method for queuing code to be executed on applicaiton thread.</param>
        /// <param name="connectivityReporter">For notifications of network going online or offline.</param>
        /// <param name="autoreplicationManager">For supporting autoreplication of entities.</param>
        internal Match(
            IPeerConnectionNotifier connectionNotifier,
            HostAuthorizationCertificate certificate,
            MatchmakingCriteria criteria,
            int maxPlayers,
            MemberIdentity memberIdentitiy,
            PeerAddress serverAddress,
            ITime timeKeeper,
            IMatchmaker matchmaker,
            IMatchProtocol messagingService,
            EntityManager entityManager,
            CreateReplica createReplica,
            RemoveReplica removeReplica,
            INetworkEventScheduler networkEventQueue,
            QueueInvokable queueApplicationCallback,
            INetworkConnectivityReporter connectivityReporter,
            MatchAutoreplicationManager autoreplicationManager)
            : this(
                connectionNotifier,
                certificate,
                criteria,
                maxPlayers,
                memberIdentitiy,
                serverAddress,
                timeKeeper,
                matchmaker,
                messagingService,
                entityManager,
                createReplica,
                removeReplica,
                networkEventQueue,
                queueApplicationCallback,
                connectivityReporter,
                autoreplicationManager,
                null)
        {
        }

        /// <summary>
        /// Initializes a new instance of the Match class.
        /// </summary>
        /// <param name="connectionNotifier">For subscribing to connectivity events.</param>
        /// <param name="certificate">Certificate of hosting authority.</param>
        /// <param name="criteria">Matchmaking criteria.</param>
        /// <param name="maxPlayers">Maximum number of players permitted in the match.</param>
        /// <param name="memberIdentity">ID for this member.</param>
        /// <param name="serverAddress">Address of the matchmaking server.</param>
        /// <param name="timeKeeper">Provides access to current time.</param>
        /// <param name="matchmaker">The matchmaker.</param>
        /// <param name="messagingService">Protocol for sending messages to the host and members of the match.</param>
        /// <param name="entityManager">Match entity manager.</param>
        /// <param name="createReplica">Called when a new entity needs to be instantiated into the match.</param>
        /// <param name="removeReplica">Called when an entity leave the match.</param>
        /// <param name="networkEventQueue">For pushing work to be done on the network thread.</param>
        /// <param name="queueApplicationEvent">Method for queuing code to be executed on applicaiton thread.</param>
        /// <param name="connectivityReporter">For notifications of network going online or offline.</param>
        /// <param name="autoreplicationManager">For supporting autoreplication of entities.</param>
        /// <param name="controller">Object to be used as a match controller.</param>
        internal Match(
            IPeerConnectionNotifier connectionNotifier,
            HostAuthorizationCertificate certificate,
            MatchmakingCriteria criteria,
            int maxPlayers,
            MemberIdentity memberIdentity,
            PeerAddress serverAddress,
            ITime timeKeeper,
            IMatchmaker matchmaker,
            IMatchProtocol messagingService,
            EntityManager entityManager,
            CreateReplica createReplica,
            RemoveReplica removeReplica,
            INetworkEventScheduler networkEventQueue,
            QueueInvokable queueApplicationEvent,
            INetworkConnectivityReporter connectivityReporter,
            MatchAutoreplicationManager autoreplicationManager,
            object controller)
        {
            this.messagingService = messagingService;
            this.matchmaker = matchmaker;
            this.entityManager = entityManager;
            this.matchIdentifier = certificate.MatchIdentifier;
            this.memberIdentity = memberIdentity;
            this.createReplica = createReplica;
            this.removeReplica = removeReplica;
            this.networkEventQueue = networkEventQueue;
            this.autoreplicationManager = autoreplicationManager;
            this.controllerEntity = controller;
            this.queueApplicationEvent = queueApplicationEvent;
            this.members = new List<MemberIdentity>(maxPlayers);

            this.networkEventQueue.Push(() => this.Initialize(
                connectionNotifier,
                certificate,
                criteria,
                maxPlayers,
                memberIdentity,
                serverAddress,
                timeKeeper,
                matchmaker,
                this.messagingService,
                connectivityReporter,
                queueApplicationEvent));
        }

        /// <summary>
        /// Occurs when the match state has changed.
        /// </summary>
        /// <remarks>
        /// This event will only be raised during a call to
        /// <see cref="Badumna.INetworkFacade.ProcessNetworkState"/>.
        /// </remarks>
        public event GenericEventHandler<Match, MatchStatusEventArgs> StateChanged
        {
            add { this.stateChangedDelegate += value; }
            remove { this.stateChangedDelegate -= value; }
        }

        /// <summary>
        /// Occurs when a new member has joined the match.
        /// </summary>
        /// <remarks>
        /// This event will only be raised during a call to
        /// <see cref="Badumna.INetworkFacade.ProcessNetworkState"/>.
        /// </remarks>
        public event GenericEventHandler<Match, MatchMembershipEventArgs> MemberAdded
        {
            add { this.memberAddedDelegate += value; }
            remove { this.memberAddedDelegate -= value; }
        }

        /// <summary>
        /// Occurs when an existing member has left the match.
        /// </summary>
        /// <remarks>
        /// This event will only be raised during a call to
        /// <see cref="Badumna.INetworkFacade.ProcessNetworkState"/>.
        /// </remarks>
        public event GenericEventHandler<Match, MatchMembershipEventArgs> MemberRemoved
        {
            add { this.memberRemovedDelegate += value; }
            remove { this.memberRemovedDelegate -= value; }
        }

        /// <summary>
        /// Occurs when a chat message is received.
        /// </summary>
        /// <remarks>
        /// This event will only be raised during a call to
        /// <see cref="Badumna.INetworkFacade.ProcessNetworkState"/>.
        /// </remarks>
        public event GenericEventHandler<Match, MatchChatEventArgs> ChatMessageReceived
        {
            add { this.chatMessageReceivedDelegate += value; }
            remove { this.chatMessageReceivedDelegate -= value; }
        }

        /// <summary>
        /// Gets the identity of the local member.
        /// </summary>
        public MemberIdentity MemberIdentity
        {
            get { return this.memberIdentity; }
        }

        /// <summary>
        /// Gets the unique ID of the match.
        /// </summary>
        public BadumnaId MatchIdentifier
        {
            get { return this.matchIdentifier; }
        }

        /// <summary>
        /// Gets the current members of the match.
        /// </summary>
        public IEnumerable<MemberIdentity> Members
        {
            get { return this.members; }
        }

        /// <summary>
        /// Gets the count of members in the match.
        /// </summary>
        public int MemberCount
        {
            get { return this.members.Count; }
        }

        /// <summary>
        /// Gets the current state of the match.
        /// </summary>
        public MatchStatus State
        {
            get { return this.state; }
        }

        /// <summary>
        /// Gets the error, if the match has failed.
        /// </summary>
        public MatchError Error
        {
            get { return this.error; }
        }

        /// <summary>
        /// Gets a membership reporter for the match.
        /// </summary>
        internal IMembershipReporter MembershipReporter
        {
            get { return this.membership; }
        }

        /// <summary>
        /// Gets the autoreplication manager.
        /// </summary>
        internal MatchAutoreplicationManager AutoreplicationManager
        {
            get { return this.autoreplicationManager; }
        }

        /// <summary>
        /// Gets the messaging service.
        /// </summary>
        internal IMatchProtocol MessagingService
        {
            get { return this.messagingService; }
        }

        /// <summary>
        /// Gets the network event queue.
        /// </summary>
        internal INetworkEventScheduler NetworkEventQueue
        {
            get { return this.networkEventQueue; }
        }

        /// <summary>
        /// Gets the controller entity.
        /// </summary>
        protected internal object ControllerEntity
        {
            get { return this.controllerEntity; }
        }

        /// <summary>
        /// Close the match after use to free resources.
        /// TODO: Use IDisposable instead/as well?
        /// </summary>
        public void Leave()
        {
            this.entityManager.MarkOriginalsInaccessible(this);
            this.networkEventQueue.Push(this.LeaveImplementation);
        }

        /// <summary>
        /// Register an original entity into a match.
        /// </summary>
        /// <param name="entity">Entity to be registered.</param>
        /// <param name="entityType">The entity type.</param>
        public void RegisterEntity(object entity, uint entityType)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }

            this.networkEventQueue.Push(
                () => this.entityManager.RegisterEntity(entity, entityType, this));
        }

        /// <summary>
        /// Register a hosted entity into a match.
        /// </summary>
        /// <remarks>Entities will only be successfully registered if this peer is the current host.</remarks>
        /// <param name="entity">Entity to be registered.</param>
        /// <param name="entityType">The entity type.</param>
        public void RegisterHostedEntity(object entity, uint entityType)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }

            this.networkEventQueue.Push(
                () => this.entityManager.RegisterHostedEntity(entity, entityType, this.membership));
        }

        /// <summary>
        /// Unregister an original entity from a match.
        /// </summary>
        /// <param name="entity">Entity to be unregistered.</param>
        public void UnregisterEntity(object entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }

            this.entityManager.MarkEntityInaccessible(entity);
            this.networkEventQueue.Push(
                () => this.entityManager.DeregisterEntity(entity, this));
        }

        /// <summary>
        /// Unregister a hosted entity from a match.
        /// </summary>
        /// <remarks>Entities will only be successfully unregistered if this peer is the current host.</remarks>
        /// <param name="entity">Entity to be unregistered.</param>
        public void UnregisterHostedEntity(object entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }

            this.entityManager.MarkEntityInaccessible(entity);
            this.networkEventQueue.Push(
                () => this.entityManager.DeregisterHostedEntity(entity, true, this.membership));
        }

        /// <summary>
        /// Send a chat message to all other members of the match.
        /// </summary>
        /// <param name="message">The message to send.</param>
        public void Chat(string message)
        {
            if (message == null)
            {
                throw new ArgumentNullException("message");
            }

            this.networkEventQueue.Push(
                () => this.membership.Chat(message));
        }

        /// <summary>
        /// Send a chat message to other members of the match.
        /// </summary>
        /// <param name="member">The member to send the message to.</param>
        /// <param name="message">The message to send.</param>
        public void Chat(MemberIdentity member, string message)
        {
            // TODO: Check == operator implementation.
            if (object.ReferenceEquals(member, null))
            {
                throw new ArgumentNullException("member");
            }

            if (message == null)
            {
                throw new ArgumentNullException("message");
            }
            
            this.networkEventQueue.Push(
                () => this.membership.Chat(member, message));
        }

        /// <summary>
        /// Remotely call a method on all the replicas of a given original.
        /// </summary>
        /// <param name="method">The method on the original.</param>
        public void CallMethodOnReplicas(RpcSignature method)
        {
            this.networkEventQueue.Push(
                () => this.autoreplicationManager.CallMethodOnReplicas(method));
        }

        /// <summary>
        /// Remotely call a method on all the replicas of a given original.
        /// </summary>
        /// <typeparam name="T1">The type of the method's parameter.</typeparam>
        /// <param name="method">The method on the original.</param>
        /// <param name="arg1">The parameter.</param>
        public void CallMethodOnReplicas<T1>(RpcSignature<T1> method, T1 arg1)
        {
            this.networkEventQueue.Push(
                () => this.autoreplicationManager.CallMethodOnReplicas(method, arg1));
        }

        /// <summary>
        /// Remotely call a method on all the replicas of a given original.
        /// </summary>
        /// <typeparam name="T1">The type of the method's first parameter.</typeparam>
        /// <typeparam name="T2">The type of the method's second parameter.</typeparam>
        /// <param name="method">The method on the original.</param>
        /// <param name="arg1">The first parameter.</param>
        /// <param name="arg2">The second parameter.</param>
        public void CallMethodOnReplicas<T1, T2>(RpcSignature<T1, T2> method, T1 arg1, T2 arg2)
        {
            this.networkEventQueue.Push(
                () => this.autoreplicationManager.CallMethodOnReplicas(method, arg1, arg2));
        }

        /// <summary>
        /// Call a method on all the replicas of a given original.
        /// </summary>
        /// <typeparam name="T1">The type of the method's first parameter.</typeparam>
        /// <typeparam name="T2">The type of the method's second parameter.</typeparam>
        /// <typeparam name="T3">The type of the method's third parameter.</typeparam>
        /// <param name="method">The corresponding method on the original.</param>
        /// <param name="arg1">The first parameter.</param>
        /// <param name="arg2">The second parameter.</param>
        /// <param name="arg3">The third parameter.</param>
        public void CallMethodOnReplicas<T1, T2, T3>(
            RpcSignature<T1, T2, T3> method,
            T1 arg1,
            T2 arg2,
            T3 arg3)
        {
            this.networkEventQueue.Push(
                () => this.autoreplicationManager.CallMethodOnReplicas(method, arg1, arg2, arg3));
        }

        /// <summary>
        /// Call a method on all the replicas of a given original.
        /// </summary>
        /// <typeparam name="T1">The type of the method's first parameter.</typeparam>
        /// <typeparam name="T2">The type of the method's second parameter.</typeparam>
        /// <typeparam name="T3">The type of the method's third parameter.</typeparam>
        /// <typeparam name="T4">The type of the method's fourth parameter.</typeparam>
        /// <param name="method">The corresponding method on the original.</param>
        /// <param name="arg1">The first parameter.</param>
        /// <param name="arg2">The second parameter.</param>
        /// <param name="arg3">The third parameter.</param>
        /// <param name="arg4">The fourth parameter.</param>
        public void CallMethodOnReplicas<T1, T2, T3, T4>(
            RpcSignature<T1, T2, T3, T4> method,
            T1 arg1,
            T2 arg2,
            T3 arg3,
            T4 arg4)
        {
            this.networkEventQueue.Push(
                () => this.autoreplicationManager.CallMethodOnReplicas(method, arg1, arg2, arg3, arg4));
        }

        /// <summary>
        /// Call a method on all the replicas of a given original.
        /// </summary>
        /// <typeparam name="T1">The type of the method's first parameter.</typeparam>
        /// <typeparam name="T2">The type of the method's second parameter.</typeparam>
        /// <typeparam name="T3">The type of the method's third parameter.</typeparam>
        /// <typeparam name="T4">The type of the method's fourth parameter.</typeparam>
        /// <typeparam name="T5">The type of the method's fifth parameter.</typeparam>
        /// <param name="method">The corresponding method on the original.</param>
        /// <param name="arg1">The first parameter.</param>
        /// <param name="arg2">The second parameter.</param>
        /// <param name="arg3">The third parameter.</param>
        /// <param name="arg4">The fourth parameter.</param>
        /// <param name="arg5">The fifth parameter.</param>
        public void CallMethodOnReplicas<T1, T2, T3, T4, T5>(
            RpcSignature<T1, T2, T3, T4, T5> method,
            T1 arg1,
            T2 arg2,
            T3 arg3,
            T4 arg4,
            T5 arg5)
        {
            this.networkEventQueue.Push(
                () => this.autoreplicationManager.CallMethodOnReplicas(method, arg1, arg2, arg3, arg4, arg5));
        }

        /// <summary>
        /// Remotely call a method on all the replicas of a given original.
        /// </summary>
        /// <typeparam name="T1">The type of the method's first parameter.</typeparam>
        /// <typeparam name="T2">The type of the method's second parameter.</typeparam>
        /// <typeparam name="T3">The type of the method's third parameter.</typeparam>
        /// <typeparam name="T4">The type of the method's fourth parameter.</typeparam>
        /// <typeparam name="T5">The type of the method's fifth parameter.</typeparam>
        /// <typeparam name="T6">The type of the method's sixth parameter.</typeparam>
        /// <param name="method">The corresponding method on the original.</param>
        /// <param name="arg1">The first parameter.</param>
        /// <param name="arg2">The second parameter.</param>
        /// <param name="arg3">The third parameter.</param>
        /// <param name="arg4">The fourth parameter.</param>
        /// <param name="arg5">The fifth parameter.</param>
        /// <param name="arg6">The sixth parameter.</param>
        public void CallMethodOnReplicas<T1, T2, T3, T4, T5, T6>(
            RpcSignature<T1, T2, T3, T4, T5, T6> method,
            T1 arg1,
            T2 arg2,
            T3 arg3,
            T4 arg4,
            T5 arg5,
            T6 arg6)
        {
            this.networkEventQueue.Push(
                () => this.autoreplicationManager.CallMethodOnReplicas(method, arg1, arg2, arg3, arg4, arg5, arg6));
        }

        /// <summary>
        /// Remotely call a method on the original of a given replica.
        /// </summary>
        /// <param name="method">The replica's method.</param>
        public void CallMethodOnOriginal(RpcSignature method)
        {
            this.networkEventQueue.Push(
                () => this.autoreplicationManager.CallMethodOnOriginal(method));
        }

        /// <summary>
        /// Remotely call a method on the original of a given replica.
        /// </summary>
        /// <typeparam name="T1">The type of the method's first parameter.</typeparam>
        /// <param name="method">The replica's method.</param>
        /// <param name="arg1">The first parameter.</param>
        public void CallMethodOnOriginal<T1>(RpcSignature<T1> method, T1 arg1)
        {
            this.networkEventQueue.Push(
                () => this.autoreplicationManager.CallMethodOnOriginal(method, arg1));
        }

        /// <summary>
        /// Remotely call a method on the original of a given replica.
        /// </summary>
        /// <typeparam name="T1">The type of the method's first parameter.</typeparam>
        /// <typeparam name="T2">The type of the method's second parameter.</typeparam>
        /// <param name="method">The replica's method.</param>
        /// <param name="arg1">The first parameter.</param>
        /// <param name="arg2">The second parameter.</param>
        public void CallMethodOnOriginal<T1, T2>(RpcSignature<T1, T2> method, T1 arg1, T2 arg2)
        {
            this.networkEventQueue.Push(
                () => this.autoreplicationManager.CallMethodOnOriginal(method, arg1, arg2));
        }

        /// <summary>
        /// Call a method on all the original of a given replica.
        /// </summary>
        /// <typeparam name="T1">The type of the method's first parameter.</typeparam>
        /// <typeparam name="T2">The type of the method's second parameter.</typeparam>
        /// <typeparam name="T3">The type of the method's third parameter.</typeparam>
        /// <param name="method">The corresponding method on the replica.</param>
        /// <param name="arg1">The first parameter.</param>
        /// <param name="arg2">The second parameter.</param>
        /// <param name="arg3">The third parameter.</param>
        public void CallMethodOnOriginal<T1, T2, T3>(
            RpcSignature<T1, T2, T3> method,
            T1 arg1,
            T2 arg2,
            T3 arg3)
        {
            this.networkEventQueue.Push(
                () => this.autoreplicationManager.CallMethodOnOriginal(method, arg1, arg2, arg3));
        }

        /// <summary>
        /// Call a method on all the original of a given replica.
        /// </summary>
        /// <typeparam name="T1">The type of the method's first parameter.</typeparam>
        /// <typeparam name="T2">The type of the method's second parameter.</typeparam>
        /// <typeparam name="T3">The type of the method's third parameter.</typeparam>
        /// <typeparam name="T4">The type of the method's fourth parameter.</typeparam>
        /// <param name="method">The corresponding method on the replica.</param>
        /// <param name="arg1">The first parameter.</param>
        /// <param name="arg2">The second parameter.</param>
        /// <param name="arg3">The third parameter.</param>
        /// <param name="arg4">The fourth parameter.</param>
        public void CallMethodOnOriginal<T1, T2, T3, T4>(
            RpcSignature<T1, T2, T3, T4> method,
            T1 arg1,
            T2 arg2,
            T3 arg3,
            T4 arg4)
        {
            this.networkEventQueue.Push(
                () => this.autoreplicationManager.CallMethodOnOriginal(method, arg1, arg2, arg3, arg4));
        }

        /// <summary>
        /// Call a method on all the original of a given replica.
        /// </summary>
        /// <typeparam name="T1">The type of the method's first parameter.</typeparam>
        /// <typeparam name="T2">The type of the method's second parameter.</typeparam>
        /// <typeparam name="T3">The type of the method's third parameter.</typeparam>
        /// <typeparam name="T4">The type of the method's fourth parameter.</typeparam>
        /// <typeparam name="T5">The type of the method's fifth parameter.</typeparam>
        /// <param name="method">The corresponding method on the replica.</param>
        /// <param name="arg1">The first parameter.</param>
        /// <param name="arg2">The second parameter.</param>
        /// <param name="arg3">The third parameter.</param>
        /// <param name="arg4">The fourth parameter.</param>
        /// <param name="arg5">The fifth parameter.</param>
        public void CallMethodOnOriginal<T1, T2, T3, T4, T5>(
            RpcSignature<T1, T2, T3, T4, T5> method,
            T1 arg1,
            T2 arg2,
            T3 arg3,
            T4 arg4,
            T5 arg5)
        {
            this.networkEventQueue.Push(
                () => this.autoreplicationManager.CallMethodOnOriginal(method, arg1, arg2, arg3, arg4, arg5));
        }

        /// <summary>
        /// Call a method on all the original of a given replica.
        /// </summary>
        /// <typeparam name="T1">The type of the method's first parameter.</typeparam>
        /// <typeparam name="T2">The type of the method's second parameter.</typeparam>
        /// <typeparam name="T3">The type of the method's third parameter.</typeparam>
        /// <typeparam name="T4">The type of the method's fourth parameter.</typeparam>
        /// <typeparam name="T5">The type of the method's fifth parameter.</typeparam>
        /// <typeparam name="T6">The type of the method's sixth parameter.</typeparam>
        /// <param name="method">The corresponding method on the replica.</param>
        /// <param name="arg1">The first parameter.</param>
        /// <param name="arg2">The second parameter.</param>
        /// <param name="arg3">The third parameter.</param>
        /// <param name="arg4">The fourth parameter.</param>
        /// <param name="arg5">The fifth parameter.</param>
        /// <param name="arg6">The sixth parameter.</param>
        public void CallMethodOnOriginal<T1, T2, T3, T4, T5, T6>(
            RpcSignature<T1, T2, T3, T4, T5, T6> method,
            T1 arg1,
            T2 arg2,
            T3 arg3,
            T4 arg4,
            T5 arg5,
            T6 arg6)
        {
            this.networkEventQueue.Push(
                () => this.autoreplicationManager.CallMethodOnOriginal(method, arg1, arg2, arg3, arg4, arg5, arg6));
        }
        
        /// <summary>
        /// Perform regular processing.
        /// </summary>
        internal void Update()
        {
            this.membership.Update();
        }

        /// <summary>
        /// Instantiate a new replica into the match.
        /// </summary>
        /// <param name="source">The source of the entity (peer or host).</param>
        /// <param name="entityType">Entity type.</param>
        /// <returns>An instance of the new entity.</returns>
        internal object InstantiateReplica(EntitySource source, uint entityType)
        {
            if (this.createReplica == null)
            {
                throw new ReplicationException(
                    "Attempt to instantiate replica when match has no replica creation delegate." +
                    "To enable replication, pass the match delegates for replica creation and removal when creating or joining the match.");
            }

            var replica = this.createReplica(this, source, entityType);
            if (replica == null)
            {
                throw new ReplicationException("CreateReplica delegate returned null.");
            }

            return replica;
        }

        /// <summary>
        /// Remove an entity from the match.
        /// </summary>
        /// <param name="source">The source of the entity (peer or host).</param>
        /// <param name="replica">A replica being removed.</param>
        internal void RemoveReplica(EntitySource source, object replica)
        {
            if (this.removeReplica == null)
            {
                return;
            }

            this.removeReplica(this, source, replica);
        }

        /// <summary>
        /// Flag a state change, used by the C++ wrapper.
        /// </summary>
        /// <param name="original">The original to flag</param>
        /// <param name="changedParts">Segments to flag</param>
        internal void FlagForUpdate(IOriginal original, BooleanArray changedParts)
        {
            this.entityManager.MarkForUpdate(original, changedParts);
        }

        /// <summary>
        /// Send a custom message to replicas, used by the C++ wrapper.
        /// </summary>
        /// <param name="original">The original entity</param>
        /// <param name="messageData">The message data</param>
        internal void SendCustomMessageToReplicas(IOriginal original, MemoryStream messageData)
        {
            this.networkEventQueue.Push(
                () => this.entityManager.SendCustomMessageToReplicas(original, messageData));
        }

        /// <summary>
        /// Send a custom message to original, used by the C++ wrapper.
        /// </summary>
        /// <param name="replica">The replica entity</param>
        /// <param name="messageData">The message data</param>
        internal void SendCustomMessageToOriginal(IReplica replica, MemoryStream messageData)
        {
            this.networkEventQueue.Push(
                () => this.entityManager.SendCustomMessageToOriginal(replica, messageData));
        }

        /// <summary>
        /// Complete construction on network thread.
        /// </summary>        
        /// <param name="connectionNotifier">For subscribing to connectivity events.</param>
        /// <param name="certificate">Certificate of hosting authority.</param>
        /// <param name="criteria">Matchmaking criteria.</param>
        /// <param name="maxPlayers">Maximum number of players permitted in the match.</param>
        /// <param name="memberIdentity">Identity of the local member.</param>
        /// <param name="serverAddress">Address of the matchmaking server.</param>
        /// <param name="timeKeeper">Provides access to current time.</param>
        /// <param name="matchmaker">The matchmaker.</param>
        /// <param name="messagingService">Protocol for sending messages to the host and members of the match.</param>
        /// <param name="connectivityReporter">For notifications of network going online or offline.</param>
        /// <param name="queueApplicationCallback">Method for queuing code to be executed on applicaiton thread.</param>
        private void Initialize(
            IPeerConnectionNotifier connectionNotifier,
            HostAuthorizationCertificate certificate,
            MatchmakingCriteria criteria,
            int maxPlayers,
            MemberIdentity memberIdentity,
            PeerAddress serverAddress,
            ITime timeKeeper,
            IMatchmaker matchmaker,
            IMatchProtocol messagingService,
            INetworkConnectivityReporter connectivityReporter,
            QueueInvokable queueApplicationCallback)
        {
            this.membership = new MembershipManager(
                connectionNotifier,
                certificate,
                criteria,
                maxPlayers,
                memberIdentity,
                serverAddress,
                timeKeeper,
                matchmaker,
                this.messagingService,
                this.OnStateChange,
                this.networkEventQueue,
                connectivityReporter);

            this.membership.HostChanged += b => this.entityManager.OnHostChanged(this, this.memberIdentity.ID, b);
            this.membership.MemberAdded += this.OnMemberAdded;
            this.membership.MemberRemoved += this.OnMemberRemoved;
            this.membership.ChatMessageReceived += this.OnChatMessageReceived;

            this.membership.Initialize();

            this.entityManager.JoinMatch(this, this.memberIdentity.ID, this.controllerEntity);
            if (this.membership.IsHost)
            {
                this.entityManager.OnHostChanged(this, this.memberIdentity.ID, true);
            }
        }

        /// <summary>
        /// Handle state change notifications from the membership manager.
        /// </summary>
        /// <param name="state">The new state.</param>
        /// <param name="error">Any error.</param>
        private void OnStateChange(MatchStatus state, MatchError error)
        {
            if (state == MatchStatus.Closed)
            {
                this.entityManager.LeaveMatch(this, this.memberIdentity.ID);
            }

            this.queueApplicationEvent(Apply.Func(this.RaiseMatchStateChangeEvent, state, error));
        }

        /// <summary>
        /// Raise the state change event (should be called on application thread).
        /// </summary>
        /// <param name="state">The new state.</param>
        /// <param name="error">Any error.</param>
        private void RaiseMatchStateChangeEvent(MatchStatus state, MatchError error)
        {
            this.state = state;
            this.error = error;
            var handler = this.stateChangedDelegate;
            if (handler != null)
            {
                handler.Invoke(this, new MatchStatusEventArgs(state, error));
            }
        }

        /// <summary>
        /// Handle chat events.
        /// </summary>
        /// <param name="source">The event source.</param>
        /// <param name="e">The event data.</param>
        private void OnChatMessageReceived(IMembershipReporter source, MatchChatEventArgs e)
        {
            this.queueApplicationEvent(Apply.Func(this.DeliverChat, e));
        }

        /// <summary>
        /// Raise the chat event (should be called in application thread).
        /// </summary>
        /// <param name="e">The event data.</param>
        private void DeliverChat(MatchChatEventArgs e)
        {
            var handler = this.chatMessageReceivedDelegate;
            if (handler != null)
            {
                handler.Invoke(this, e);
            }
        }

        /// <summary>
        /// Handle member added events from the membership manager.
        /// </summary>
        /// <param name="membershipReporter">The event source.</param>
        /// <param name="e">The event data.</param>
        private void OnMemberAdded(IMembershipReporter membershipReporter, MatchMembershipEventArgs e)
        {
            this.queueApplicationEvent(Apply.Func(this.RaiseMemberAddedEvent, e));
        }

        /// <summary>
        /// Handle member removed events from the membership manager.
        /// </summary>
        /// <param name="membershipReporter">The event source.</param>
        /// <param name="e">The event data.</param>
        private void OnMemberRemoved(IMembershipReporter membershipReporter, MatchMembershipEventArgs e)
        {
            this.queueApplicationEvent(Apply.Func(this.RaiseMemberRemovedEvent, e));
        }

        /// <summary>
        /// Raise the member added event.
        /// </summary>
        /// <remarks>Must be called in application thread.</remarks>
        /// <param name="e">The event data.</param>
        private void RaiseMemberAddedEvent(MatchMembershipEventArgs e)
        {
            this.members.Add(e.Member);
            var handler = this.memberAddedDelegate;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        /// <summary>
        /// Raise the member removed event.
        /// </summary>
        /// <remarks>Must be called in application thread.</remarks>
        /// <param name="e">The event data.</param>
        private void RaiseMemberRemovedEvent(MatchMembershipEventArgs e)
        {
            this.members.Remove(e.Member);
            var handler = this.memberRemovedDelegate;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        /// <summary>
        /// Work required by Leave method that must be done on network thread.
        /// </summary>
        private void LeaveImplementation()
        {
            this.membership.Leave();

            // Only bother unsubscribing from events on objects whose lifespan could exceed this object's.
            this.membership.HostChanged -= b => this.entityManager.OnHostChanged(this, this.memberIdentity.ID, b);
        }
    }
}
