﻿//-----------------------------------------------------------------------
// <copyright file="MatchmakingCriteria.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2013 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.Match
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Badumna.Core;

    /// <summary>
    /// Criteria used for matchmaking.
    /// </summary>
    public struct MatchmakingCriteria : IParseable
    {
        /// <summary>
        /// Backing field for MatchName.
        /// </summary>
        private string matchName;

        /// <summary>
        /// Gets or sets the match name.
        /// </summary>
        /// <remarks>
        /// <para>
        /// A <c>null</c> match name is converted to the empty match name when setting this property.
        /// An empty or <c>null</c> match name indicates no match name is specified.
        /// </para>
        /// <para>
        /// If no match name is specified when searching for matches then public matches with any
        /// name will be returned.
        /// Otherwise, only matches that have exactly the name specified will be returned.
        /// If a match is created without specifying a name then it can only be matched
        /// by a query that doesn't specify a match name (such a query will also return
        /// matches that are named).
        /// </para>
        /// </remarks>
        public string MatchName
        {
            get { return this.matchName ?? ""; }
            set { this.matchName = value; }
        }

        /// <summary>
        /// Gets or sets the player group.
        /// </summary>
        /// <remarks>
        /// PlayerGroup property is used to divide the players into smaller groups, only players with the same player group can be matched together.
        /// This property will be ignored if it has a value of 0.
        /// This property is useful to group players based on the skill level, different type of matches, different type of game maps, etc. 
        /// </remarks>
        public ushort PlayerGroup { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the match is private.
        /// </summary>
        /// <remarks>
        /// Private matches are only returned in response to queries that specify the (non-empty) match name.
        /// The value of this flag is ignored when the criteria is used for a query, it is only used on
        /// match creation.
        /// Its value is stored in the criteria class to ensure it is set correctly whenever MatchName is
        /// used.
        /// </remarks>
        internal bool IsPrivate { get; set; }

        /// <summary>
        /// Returns a value indicating whether this set of criteria satisfy the desired criteria.
        /// </summary>
        /// <param name="desired">The desired criteria</param>
        /// <returns><c>true</c> iff the desired criteria are met</returns>
        public bool Satisfies(MatchmakingCriteria desired)
        {
            var querySpecifiesName = !string.IsNullOrEmpty(desired.MatchName);

            if (this.IsPrivate && !querySpecifiesName)
            {
                return false;
            }

            if (querySpecifiesName && this.MatchName != desired.MatchName)
            {
                return false;
            }

            // TODO: Handle scenario where player want to ignore player group, but there are no other matches
            // so they become host - what player group should they have? If it is kept zero, multiple players
            // with different player groups could be matched. If it is set to some arbitrary value, then that
            // will limit the number of players who are sent to the match.
            // I guess the scenario where there is no slot anywhere could be very low.
            if (desired.PlayerGroup == 0)
            {
                return true;
            }

            return desired.PlayerGroup == this.PlayerGroup;
        }

        /// <inheritdoc />
        void IParseable.ToMessage(MessageBuffer message, Type parameterType)
        {
            message.Write(this.MatchName);
            message.Write(this.IsPrivate);
            message.Write(this.PlayerGroup);
        }

        /// <inheritdoc />
        void IParseable.FromMessage(MessageBuffer message)
        {
            this.MatchName = message.ReadString();
            this.IsPrivate = message.ReadBool();
            this.PlayerGroup = message.ReadUShort();
        }
    }
}
