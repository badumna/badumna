﻿//------------------------------------------------------------------------------
// <copyright file="MatchmakingQueryResult.cs" company="National ICT Australia Limited">
//     Copyright (c) 2013 National ICT Australia Limited. All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

namespace Badumna.Match
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// The response to a matchmaking query.
    /// </summary>
    public class MatchmakingQueryResult
    {
        /// <summary>
        /// Initializes a new instance of the MatchmakingQueryResult class.
        /// </summary>
        /// <param name="error">The error, if any</param>
        /// <param name="results">The results</param>
        internal MatchmakingQueryResult(MatchError error, IList<MatchmakingResult> results)
        {
            this.Error = error;
            this.Results = results;
        }

        /// <summary>
        /// Gets the error returned by the query.
        /// Will be <see cref="MatchError.None" /> if the query was successful.
        /// </summary>
        public MatchError Error { get; private set; }

        /// <summary>
        /// Gets the matchmaking results.
        /// </summary>
        /// <remarks>
        /// If this collection is empty and the query was successful (<see cref="Error"/> is
        /// <see cref="MatchError.None"/>) then no matches fit the query.
        /// When making multiple sequential find match queries, the order of the results is not guaranteed to
        /// be consistent from one query to the next.  If a consistent ordering is required then the results should
        /// be sorted.
        /// </remarks>
        public IList<MatchmakingResult> Results { get; private set; }
    }
}
