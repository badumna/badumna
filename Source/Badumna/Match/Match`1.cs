﻿//-----------------------------------------------------------------------
// <copyright file="Match`1.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2013 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.Match
{
    using System;
    using System.IO;
    using Badumna.Autoreplication.Serialization;
    using Badumna.Core;
    using MatchAutoreplicationManager = Autoreplication.MatchAutoreplicationManager;

    /// <summary>
    /// Represents a match with a controller for a finite set of players.
    /// </summary>
    /// <typeparam name="T">The type of the object used as the match controller.</typeparam>
    public class Match<T> : Match
    {
        /// <summary>
        /// Initializes a new instance of the Match class.
        /// </summary>
        /// <param name="facade">The facade </param>
        /// <param name="connectionNotifier">For subscribing to connectivity events.</param>
        /// <param name="certificate">Certificate of hosting authority.</param>
        /// <param name="criteria">Matchmaking criteria.</param>
        /// <param name="maxPlayers">Maximum number of players permitted in the match.</param>
        /// <param name="memberIdentity">Identity of the local member.</param>
        /// <param name="serverAddress">Address of the matchmaking server.</param>
        /// <param name="timeKeeper">Provides access to current time.</param>
        /// <param name="matchmaker">The matchmaker.</param>
        /// <param name="messagingService">Protocol for sending messages to the host and members of the match.</param>
        /// <param name="entityManager">Match entity manager.</param>
        /// <param name="createReplica">Called when a new entity needs to be instantiated into the match.</param>
        /// <param name="removeReplica">Called when an entity leave the match.</param>
        /// <param name="networkEventQueue">For pushing work to be done on the network thread.</param>
        /// <param name="queueApplicationCallback">Method for queuing code to be executed on applicaiton thread.</param>
        /// <param name="connectivityReporter">For notifications of network going online or offline.</param>
        /// <param name="autoreplicationManager">For supporting autoreplication of entities.</param>
        /// <param name="controller">The object to use as the match controller.</param>
        internal Match(
            Facade facade,
            IPeerConnectionNotifier connectionNotifier,
            HostAuthorizationCertificate certificate,
            MatchmakingCriteria criteria,
            int maxPlayers,
            MemberIdentity memberIdentity,
            PeerAddress serverAddress,
            ITime timeKeeper,
            IMatchmaker matchmaker,
            IMatchProtocol messagingService,
            EntityManager entityManager,
            CreateReplica createReplica,
            RemoveReplica removeReplica,
            INetworkEventScheduler networkEventQueue,
            QueueInvokable queueApplicationCallback,
            INetworkConnectivityReporter connectivityReporter,
            MatchAutoreplicationManager autoreplicationManager,
            T controller)
            : base(
                connectionNotifier,
                certificate,
                criteria,
                maxPlayers,
                memberIdentity,
                serverAddress,
                timeKeeper,
                matchmaker,
                messagingService,
                entityManager,
                createReplica,
                removeReplica,
                networkEventQueue,
                queueApplicationCallback,
                connectivityReporter,
                autoreplicationManager,
                controller)
        {
        }

        /// <summary>
        /// Gets the match controller.
        /// </summary>
        public T Controller
        {
            get { return (T)this.ControllerEntity; }
        }

        /// <summary>
        /// Remotely call a controller method on a given member of the match.
        /// </summary>
        /// <param name="memberIdentity">The identity of the member to call the member on.</param>
        /// <param name="method">The controller method.</param>
        public void CallMethodOnMember(MemberIdentity memberIdentity, RpcSignature method)
        {
            this.CheckTarget(method);

            this.NetworkEventQueue.Push(
                () => this.AutoreplicationManager.CallMethodOnController(
                    s => this.MessagingService.SendControllerRPC(memberIdentity.ID, s),
                    method));
        }

        /// <summary>
        /// Remotely call a controller method on a given member of the match.
        /// </summary>
        /// <typeparam name="T1">The type of the method's first parameter.</typeparam>
        /// <param name="memberIdentity">The identity of the member to call the member on.</param>
        /// <param name="method">The controller method.</param>
        /// <param name="arg1">The first parameter.</param>
        public void CallMethodOnMember<T1>(
            MemberIdentity memberIdentity,
            RpcSignature<T1> method,
            T1 arg1)
        {
            this.CheckTarget(method);

            this.NetworkEventQueue.Push(
                () => this.AutoreplicationManager.CallMethodOnController(
                    s => this.MessagingService.SendControllerRPC(memberIdentity.ID, s),
                    method,
                    arg1));
        }

        /// <summary>
        /// Remotely call a controller method on a given member of the match.
        /// </summary>
        /// <typeparam name="T1">The type of the method's first parameter.</typeparam>
        /// <typeparam name="T2">The type of the method's second parameter.</typeparam>
        /// <param name="memberIdentity">The identity of the member to call the member on.</param>
        /// <param name="method">The controller method.</param>
        /// <param name="arg1">The first parameter.</param>
        /// <param name="arg2">The second parameter.</param>
        public void CallMethodOnMember<T1, T2>(
            MemberIdentity memberIdentity,
            RpcSignature<T1, T2> method,
            T1 arg1,
            T2 arg2)
        {
            this.CheckTarget(method);

            this.NetworkEventQueue.Push(
                () => this.AutoreplicationManager.CallMethodOnController(
                    s => this.MessagingService.SendControllerRPC(memberIdentity.ID, s),
                    method,
                    arg1,
                    arg2));
        }

        /// <summary>
        /// Remotely call a controller method on a given member of the match.
        /// </summary>
        /// <typeparam name="T1">The type of the method's first parameter.</typeparam>
        /// <typeparam name="T2">The type of the method's second parameter.</typeparam>
        /// <typeparam name="T3">The type of the method's third parameter.</typeparam>
        /// <param name="memberIdentity">The identity of the member to call the member on.</param>
        /// <param name="method">The controller method.</param>
        /// <param name="arg1">The first parameter.</param>
        /// <param name="arg2">The second parameter.</param>
        /// <param name="arg3">The third parameter.</param>
        public void CallMethodOnMember<T1, T2, T3>(
            MemberIdentity memberIdentity,
            RpcSignature<T1, T2, T3> method,
            T1 arg1,
            T2 arg2,
            T3 arg3)
        {
            this.CheckTarget(method);

            this.NetworkEventQueue.Push(
                () => this.AutoreplicationManager.CallMethodOnController(
                    s => this.MessagingService.SendControllerRPC(memberIdentity.ID, s),
                    method,
                    arg1,
                    arg2,
                    arg3));
        }

        /// <summary>
        /// Remotely call a controller method on a given member of the match.
        /// </summary>
        /// <typeparam name="T1">The type of the method's first parameter.</typeparam>
        /// <typeparam name="T2">The type of the method's second parameter.</typeparam>
        /// <typeparam name="T3">The type of the method's third parameter.</typeparam>
        /// <typeparam name="T4">The type of the method's fourth parameter.</typeparam>
        /// <param name="memberIdentity">The identity of the member to call the member on.</param>
        /// <param name="method">The controller method.</param>
        /// <param name="arg1">The first parameter.</param>
        /// <param name="arg2">The second parameter.</param>
        /// <param name="arg3">The third parameter.</param>
        /// <param name="arg4">The fourth parameter.</param>
        public void CallMethodOnMember<T1, T2, T3, T4>(
            MemberIdentity memberIdentity,
            RpcSignature<T1, T2, T3, T4> method,
            T1 arg1,
            T2 arg2,
            T3 arg3,
            T4 arg4)
        {
            this.CheckTarget(method);

            this.NetworkEventQueue.Push(
                () => this.AutoreplicationManager.CallMethodOnController(
                    s => this.MessagingService.SendControllerRPC(memberIdentity.ID, s),
                    method,
                    arg1,
                    arg2,
                    arg3,
                    arg4));
        }

        /// <summary>
        /// Remotely call a controller method on a given member of the match.
        /// </summary>
        /// <typeparam name="T1">The type of the method's first parameter.</typeparam>
        /// <typeparam name="T2">The type of the method's second parameter.</typeparam>
        /// <typeparam name="T3">The type of the method's third parameter.</typeparam>
        /// <typeparam name="T4">The type of the method's fourth parameter.</typeparam>
        /// <typeparam name="T5">The type of the method's fifth parameter.</typeparam>
        /// <param name="memberIdentity">The identity of the member to call the member on.</param>
        /// <param name="method">The controller method.</param>
        /// <param name="arg1">The first parameter.</param>
        /// <param name="arg2">The second parameter.</param>
        /// <param name="arg3">The third parameter.</param>
        /// <param name="arg4">The fourth parameter.</param>
        /// <param name="arg5">The fifth parameter.</param>
        public void CallMethodOnMember<T1, T2, T3, T4, T5>(
            MemberIdentity memberIdentity,
            RpcSignature<T1, T2, T3, T4, T5> method,
            T1 arg1,
            T2 arg2,
            T3 arg3,
            T4 arg4,
            T5 arg5)
        {
            this.CheckTarget(method);

            this.NetworkEventQueue.Push(
                () => this.AutoreplicationManager.CallMethodOnController(
                    s => this.MessagingService.SendControllerRPC(memberIdentity.ID, s),
                    method,
                    arg1,
                    arg2,
                    arg3,
                    arg4,
                    arg5));
        }

        /// <summary>
        /// Remotely call a controller method on a given member of the match.
        /// </summary>
        /// <typeparam name="T1">The type of the method's first parameter.</typeparam>
        /// <typeparam name="T2">The type of the method's second parameter.</typeparam>
        /// <typeparam name="T3">The type of the method's third parameter.</typeparam>
        /// <typeparam name="T4">The type of the method's fourth parameter.</typeparam>
        /// <typeparam name="T5">The type of the method's fifth parameter.</typeparam>
        /// <typeparam name="T6">The type of the method's sixth parameter.</typeparam>
        /// <param name="memberIdentity">The identity of the member to call the member on.</param>
        /// <param name="method">The controller method.</param>
        /// <param name="arg1">The first parameter.</param>
        /// <param name="arg2">The second parameter.</param>
        /// <param name="arg3">The third parameter.</param>
        /// <param name="arg4">The fourth parameter.</param>
        /// <param name="arg5">The fifth parameter.</param>
        /// <param name="arg6">The sixth parameter.</param>
        public void CallMethodOnMember<T1, T2, T3, T4, T5, T6>(
            MemberIdentity memberIdentity,
            RpcSignature<T1, T2, T3, T4, T5, T6> method,
            T1 arg1,
            T2 arg2,
            T3 arg3,
            T4 arg4,
            T5 arg5,
            T6 arg6)
        {
            this.CheckTarget(method);

            this.NetworkEventQueue.Push(
                () => this.AutoreplicationManager.CallMethodOnController(
                    s => this.MessagingService.SendControllerRPC(memberIdentity.ID, s),
                    method,
                    arg1,
                    arg2,
                    arg3,
                    arg4,
                    arg5,
                    arg6));
        }

        /// <summary>
        /// Remotely call a controller method on all the other members of the match.
        /// </summary>
        /// <param name="method">The method on the original.</param>
        public void CallMethodOnMembers(RpcSignature method)
        {
            this.CheckTarget(method);

            foreach (var memberIdentity in this.Members)
            {
                if (memberIdentity != this.MemberIdentity)
                {
                    this.NetworkEventQueue.Push(
                        () => this.AutoreplicationManager.CallMethodOnController(
                                s => this.MessagingService.SendControllerRPC(memberIdentity.ID, s),
                                method));
                }
            }
        }

        /// <summary>
        /// Remotely call a controller method on all the other members of the match.
        /// </summary>
        /// <typeparam name="T1">The type of the method's first parameter.</typeparam>
        /// <param name="method">The controller method.</param>
        /// <param name="arg1">The first parameter.</param>
        public void CallMethodOnMembers<T1>(RpcSignature<T1> method, T1 arg1)
        {
            this.CheckTarget(method);

            foreach (var memberIdentity in this.Members)
            {
                if (memberIdentity != this.MemberIdentity)
                {
                    this.NetworkEventQueue.Push(
                        () => this.AutoreplicationManager.CallMethodOnController(
                                s => this.MessagingService.SendControllerRPC(memberIdentity.ID, s),
                                method,
                                arg1));
                }
            }
        }

        /// <summary>
        /// Remotely call a controller method on all the other members of the match.
        /// </summary>
        /// <typeparam name="T1">The type of the method's first parameter.</typeparam>
        /// <typeparam name="T2">The type of the method's second parameter.</typeparam>
        /// <param name="method">The controller method.</param>
        /// <param name="arg1">The first parameter.</param>
        /// <param name="arg2">The second parameter.</param>
        public void CallMethodOnMembers<T1, T2>(
            RpcSignature<T1, T2> method,
            T1 arg1,
            T2 arg2)
        {
            this.CheckTarget(method);

            foreach (var identity in this.Members)
            {
                if (identity != this.MemberIdentity)
                {
                    this.NetworkEventQueue.Push(
                        () => this.AutoreplicationManager.CallMethodOnController(
                                s => this.MessagingService.SendControllerRPC(identity.ID, s),
                                method,
                                arg1,
                                arg2));
                }
            }
        }

        /// <summary>
        /// Remotely call a controller method on all the other members of the match.
        /// </summary>
        /// <typeparam name="T1">The type of the method's first parameter.</typeparam>
        /// <typeparam name="T2">The type of the method's second parameter.</typeparam>
        /// <typeparam name="T3">The type of the method's third parameter.</typeparam>
        /// <param name="method">The controller method.</param>
        /// <param name="arg1">The first parameter.</param>
        /// <param name="arg2">The second parameter.</param>
        /// <param name="arg3">The third parameter.</param>
        public void CallMethodOnMembers<T1, T2, T3>(
            RpcSignature<T1, T2, T3> method,
            T1 arg1,
            T2 arg2,
            T3 arg3)
        {
            this.CheckTarget(method);

            foreach (var identity in this.Members)
            {
                if (identity != this.MemberIdentity)
                {
                    this.NetworkEventQueue.Push(
                    () => this.AutoreplicationManager.CallMethodOnController(
                            s => this.MessagingService.SendControllerRPC(identity.ID, s),
                            method,
                            arg1,
                            arg2,
                            arg3));
                }
            }
        }

        /// <summary>
        /// Remotely call a controller method on all the other members of the match.
        /// </summary>
        /// <typeparam name="T1">The type of the method's first parameter.</typeparam>
        /// <typeparam name="T2">The type of the method's second parameter.</typeparam>
        /// <typeparam name="T3">The type of the method's third parameter.</typeparam>
        /// <typeparam name="T4">The type of the method's fourth parameter.</typeparam>
        /// <param name="method">The controller method.</param>
        /// <param name="arg1">The first parameter.</param>
        /// <param name="arg2">The second parameter.</param>
        /// <param name="arg3">The third parameter.</param>
        /// <param name="arg4">The fourth parameter.</param>
        public void CallMethodOnMembers<T1, T2, T3, T4>(
            RpcSignature<T1, T2, T3, T4> method,
            T1 arg1,
            T2 arg2,
            T3 arg3,
            T4 arg4)
        {
            this.CheckTarget(method);

            foreach (var identity in this.Members)
            {
                if (identity != this.MemberIdentity)
                {
                    this.NetworkEventQueue.Push(
                        () => this.AutoreplicationManager.CallMethodOnController(
                                s => this.MessagingService.SendControllerRPC(identity.ID, s),
                                method,
                                arg1,
                                arg2,
                                arg3,
                                arg4));
                }
            }
        }

        /// <summary>
        /// Remotely call a controller method on all the other members of the match.
        /// </summary>
        /// <typeparam name="T1">The type of the method's first parameter.</typeparam>
        /// <typeparam name="T2">The type of the method's second parameter.</typeparam>
        /// <typeparam name="T3">The type of the method's third parameter.</typeparam>
        /// <typeparam name="T4">The type of the method's fourth parameter.</typeparam>
        /// <typeparam name="T5">The type of the method's fifth parameter.</typeparam>
        /// <param name="method">The controller method.</param>
        /// <param name="arg1">The first parameter.</param>
        /// <param name="arg2">The second parameter.</param>
        /// <param name="arg3">The third parameter.</param>
        /// <param name="arg4">The fourth parameter.</param>
        /// <param name="arg5">The fifth parameter.</param>
        public void CallMethodOnMembers<T1, T2, T3, T4, T5>(
            RpcSignature<T1, T2, T3, T4, T5> method,
            T1 arg1,
            T2 arg2,
            T3 arg3,
            T4 arg4,
            T5 arg5)
        {
            this.CheckTarget(method);

            foreach (var identity in this.Members)
            {
                if (identity != this.MemberIdentity)
                {
                    this.NetworkEventQueue.Push(
                        () => this.AutoreplicationManager.CallMethodOnController(
                                s => this.MessagingService.SendControllerRPC(identity.ID, s),
                                method,
                                arg1,
                                arg2,
                                arg3,
                                arg4,
                                arg5));
                }
            }
        }

        /// <summary>
        /// Remotely call a controller method on all the other members of the match.
        /// </summary>
        /// <typeparam name="T1">The type of the method's first parameter.</typeparam>
        /// <typeparam name="T2">The type of the method's second parameter.</typeparam>
        /// <typeparam name="T3">The type of the method's third parameter.</typeparam>
        /// <typeparam name="T4">The type of the method's fourth parameter.</typeparam>
        /// <typeparam name="T5">The type of the method's fifth parameter.</typeparam>
        /// <typeparam name="T6">The type of the method's sixth parameter.</typeparam>
        /// <param name="method">The controller method.</param>
        /// <param name="arg1">The first parameter.</param>
        /// <param name="arg2">The second parameter.</param>
        /// <param name="arg3">The third parameter.</param>
        /// <param name="arg4">The fourth parameter.</param>
        /// <param name="arg5">The fifth parameter.</param>
        /// <param name="arg6">The sixth parameter.</param>
        public void CallMethodOnMembers<T1, T2, T3, T4, T5, T6>(
            RpcSignature<T1, T2, T3, T4, T5, T6> method,
            T1 arg1,
            T2 arg2,
            T3 arg3,
            T4 arg4,
            T5 arg5,
            T6 arg6)
        {
            this.CheckTarget(method);

            foreach (var identity in this.Members)
            {
                if (identity != this.MemberIdentity)
                {
                    this.NetworkEventQueue.Push(
                        () => this.AutoreplicationManager.CallMethodOnController(
                                s => this.MessagingService.SendControllerRPC(identity.ID, s),
                                method,
                                arg1,
                                arg2,
                                arg3,
                                arg4,
                                arg5,
                                arg6));
                }
            }
        }

        /// <summary>
        /// Remotely call a controller method on the current match host.
        /// </summary>
        /// <param name="method">The controller method.</param>
        public void CallMethodOnHost(RpcSignature method)
        {
            this.CheckTarget(method);

            this.NetworkEventQueue.Push(
                () => this.AutoreplicationManager.CallMethodOnController(
                    s => this.MessagingService.SendControllerRPC(this.MembershipReporter.Certificate.Host, s),
                    method));
        }

        /// <summary>
        /// Remotely call a controller method on the current match host.
        /// </summary>
        /// <typeparam name="T1">The type of the method's first parameter.</typeparam>
        /// <param name="method">The controller method.</param>
        /// <param name="arg1">The first parameter.</param>
        public void CallMethodOnHost<T1>(RpcSignature<T1> method, T1 arg1)
        {
            this.CheckTarget(method);

            this.NetworkEventQueue.Push(
                () => this.AutoreplicationManager.CallMethodOnController(
                    s => this.MessagingService.SendControllerRPC(this.MembershipReporter.Certificate.Host, s),
                    method,
                    arg1));
        }

        /// <summary>
        /// Remotely call a controller method on the current match host.
        /// </summary>
        /// <typeparam name="T1">The type of the method's first parameter.</typeparam>
        /// <typeparam name="T2">The type of the method's second parameter.</typeparam>
        /// <param name="method">The controller method.</param>
        /// <param name="arg1">The first parameter.</param>
        /// <param name="arg2">The second parameter.</param>
        public void CallMethodOnHost<T1, T2>(
            RpcSignature<T1, T2> method, T1 arg1, T2 arg2)
        {
            this.CheckTarget(method);

            this.NetworkEventQueue.Push(
                () => this.AutoreplicationManager.CallMethodOnController(
                    s => this.MessagingService.SendControllerRPC(this.MembershipReporter.Certificate.Host, s),
                    method,
                    arg1,
                    arg2));
        }

        /// <summary>
        /// Remotely call a controller method on the current match host.
        /// </summary>
        /// <typeparam name="T1">The type of the method's first parameter.</typeparam>
        /// <typeparam name="T2">The type of the method's second parameter.</typeparam>
        /// <typeparam name="T3">The type of the method's third parameter.</typeparam>
        /// <param name="method">The controller method.</param>
        /// <param name="arg1">The first parameter.</param>
        /// <param name="arg2">The second parameter.</param>
        /// <param name="arg3">The third parameter.</param>
        public void CallMethodOnHost<T1, T2, T3>(
            RpcSignature<T1, T2, T3> method, T1 arg1, T2 arg2, T3 arg3)
        {
            this.CheckTarget(method);

            this.NetworkEventQueue.Push(
                () => this.AutoreplicationManager.CallMethodOnController(
                    s => this.MessagingService.SendControllerRPC(this.MembershipReporter.Certificate.Host, s),
                    method,
                    arg1,
                    arg2,
                    arg3));
        }

        /// <summary>
        /// Remotely call a controller method on the current match host.
        /// </summary>
        /// <typeparam name="T1">The type of the method's first parameter.</typeparam>
        /// <typeparam name="T2">The type of the method's second parameter.</typeparam>
        /// <typeparam name="T3">The type of the method's third parameter.</typeparam>
        /// <typeparam name="T4">The type of the method's fourth parameter.</typeparam>
        /// <param name="method">The controller method.</param>
        /// <param name="arg1">The first parameter.</param>
        /// <param name="arg2">The second parameter.</param>
        /// <param name="arg3">The third parameter.</param>
        /// <param name="arg4">The fourth parameter.</param>
        public void CallMethodOnHost<T1, T2, T3, T4>(
            RpcSignature<T1, T2, T3, T4> method, T1 arg1, T2 arg2, T3 arg3, T4 arg4)
        {
            this.CheckTarget(method);

            this.NetworkEventQueue.Push(
                () => this.AutoreplicationManager.CallMethodOnController(
                    s => this.MessagingService.SendControllerRPC(this.MembershipReporter.Certificate.Host, s),
                    method,
                    arg1,
                    arg2,
                    arg3,
                    arg4));
        }

        /// <summary>
        /// Remotely call a controller method on the current match host.
        /// </summary>
        /// <typeparam name="T1">The type of the method's first parameter.</typeparam>
        /// <typeparam name="T2">The type of the method's second parameter.</typeparam>
        /// <typeparam name="T3">The type of the method's third parameter.</typeparam>
        /// <typeparam name="T4">The type of the method's fourth parameter.</typeparam>
        /// <typeparam name="T5">The type of the method's fifth parameter.</typeparam>
        /// <param name="method">The controller method.</param>
        /// <param name="arg1">The first parameter.</param>
        /// <param name="arg2">The second parameter.</param>
        /// <param name="arg3">The third parameter.</param>
        /// <param name="arg4">The fourth parameter.</param>
        /// <param name="arg5">The fifth parameter.</param>
        public void CallMethodOnHost<T1, T2, T3, T4, T5>(
            RpcSignature<T1, T2, T3, T4, T5> method, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5)
        {
            this.CheckTarget(method);

            this.NetworkEventQueue.Push(
                () => this.AutoreplicationManager.CallMethodOnController(
                    s => this.MessagingService.SendControllerRPC(this.MembershipReporter.Certificate.Host, s),
                    method,
                    arg1,
                    arg2,
                    arg3,
                    arg4,
                    arg5));
        }

        /// <summary>
        /// Remotely call a controller method on the current match host.
        /// </summary>
        /// <typeparam name="T1">The type of the method's first parameter.</typeparam>
        /// <typeparam name="T2">The type of the method's second parameter.</typeparam>
        /// <typeparam name="T3">The type of the method's third parameter.</typeparam>
        /// <typeparam name="T4">The type of the method's fourth parameter.</typeparam>
        /// <typeparam name="T5">The type of the method's fifth parameter.</typeparam>
        /// <typeparam name="T6">The type of the method's sixth parameter.</typeparam>
        /// <param name="method">The controller method.</param>
        /// <param name="arg1">The first parameter.</param>
        /// <param name="arg2">The second parameter.</param>
        /// <param name="arg3">The third parameter.</param>
        /// <param name="arg4">The fourth parameter.</param>
        /// <param name="arg5">The fifth parameter.</param>
        /// <param name="arg6">The sixth parameter.</param>
        public void CallMethodOnHost<T1, T2, T3, T4, T5, T6>(
            RpcSignature<T1, T2, T3, T4, T5, T6> method, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6)
        {
            this.CheckTarget(method);

            this.NetworkEventQueue.Push(
                () => this.AutoreplicationManager.CallMethodOnController(
                    s => this.MessagingService.SendControllerRPC(this.MembershipReporter.Certificate.Host, s),
                    method,
                    arg1,
                    arg2,
                    arg3,
                    arg4,
                    arg5,
                    arg6));
        }

        /// <summary>
        /// Send a custom message to a member, used by the C++ wrapper.
        /// </summary>
        /// <param name="memberId">The member id</param>
        /// <param name="messageData">The message data</param>
        internal void SendCustomMessageToMember(MemberIdentity memberId, MemoryStream messageData)
        {
            this.NetworkEventQueue.Push(
                () => this.MessagingService.SendControllerRPC(memberId.ID, messageData));
        }

        /// <summary>
        /// Send a custom message to all members, used by the C++ wrapper.
        /// </summary>
        /// <param name="messageData">The message data</param>
        internal void SendCustomMessageToMembers(MemoryStream messageData)
        {
            foreach (var identity in this.Members)
            {
                if (identity != this.MemberIdentity)
                {
                    this.NetworkEventQueue.Push(
                        () => this.MessagingService.SendControllerRPC(identity.ID, messageData));
                }
            }
        }

        /// <summary>
        /// Send a custom message to the host, used by the C++ wrapper.
        /// </summary>
        /// <param name="messageData">The message data</param>
        internal void SendCustomMessageToHost(MemoryStream messageData)
        {
            this.NetworkEventQueue.Push(
                () => this.MessagingService.SendControllerRPC(this.MembershipReporter.Certificate.Host, messageData));
        }

        /// <summary>
        /// Check that RPC target is the match controller.
        /// </summary>
        /// <param name="method">The RPC method.</param>
        private void CheckTarget(Delegate method)
        {
            if (!object.ReferenceEquals(method.Target, this.Controller))
            {
                throw new InvalidOperationException("method must be a method on the match controller.");
            }            
        }
    }
}
