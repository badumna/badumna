﻿//-----------------------------------------------------------------------
// <copyright file="HostedReplicaWrapper.cs" company="Scalify">
//     Copyright (c) 2013 Scalify. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.Match
{
    using System;
    using System.IO;
    using Badumna.Core;
    using Badumna.DataTypes;
    using Badumna.Replication;
    using Badumna.SpatialEntities;
    using Badumna.Utilities;

    /// <summary>
    /// Replica wrapper used in match to represent an original entity's replica.
    /// </summary>
    internal class HostedReplicaWrapper : IReplica
    {
        /// <summary>
        /// For debugging.
        /// </summary>
        private readonly int number;

        /// <summary>
        /// For debugging.
        /// </summary>
        private static int nextNumber;

        /// <summary>
        /// The IReplica for the hosted entity.
        /// </summary>
        private IReplica replica;

        /// <summary>
        /// The hosted entity instantiated by the application.
        /// </summary>
        private object hostedEntity;

        /// <summary>
        /// The match.
        /// </summary>
        private Match match;

        /// <summary>
        /// The queue to use for scheduling events.
        /// </summary>
        private INetworkEventScheduler eventQueue;

        /// <summary>
        /// Match entity manager.
        /// </summary>
        private EntityManager entityManager;

        /// <summary>
        /// The method to use to queue events that must run in the application's thread.
        /// </summary>
        private QueueInvokable queueApplicationEvent;

        /// <summary>
        /// A value indicating whether this replica wrapper should be controlling the entity.
        /// </summary>
        private bool isControllingEntity = true;

        /// <summary>
        /// Initializes a new instance of the HostedReplicaWrapper class.
        /// </summary>
        /// <param name="entityId">Unique ID of the replica.</param>
        /// <param name="entityType">Entity type of the replica.</param>
        /// <param name="eventQueue">The event queue to schedule events on.</param>
        /// <param name="entityManager">Match entity manager.</param>
        /// <param name="queueApplicationEvent">The method to use to queue events that must run in the application's thread.</param>
        public HostedReplicaWrapper(
            BadumnaId entityId,
            uint entityType,
            INetworkEventScheduler eventQueue,
            EntityManager entityManager,
            QueueInvokable queueApplicationEvent)
        {
            this.number = nextNumber++;
            Logger.TraceInformation(LogTag.Match, "Created HostedReplica Wrapper {0} for replica {1}", this.number, entityId);
            this.eventQueue = eventQueue;
            this.EntityType = entityType;
            this.Guid = entityId;
            this.entityManager = entityManager;
            this.queueApplicationEvent = queueApplicationEvent;
        }

        /// <summary>
        /// Gets the hosted entity being wrapped.
        /// </summary>
        public object HostedEntity
        {
            get { return this.hostedEntity; }
        }

        /// <summary>
        /// Gets or sets the unique identifier of the spatial replicas in the collection.
        /// </summary>
        public BadumnaId Guid { get; set; }

        /// <summary>
        /// Gets the type of the entities.
        /// </summary>
        public uint EntityType
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the match where the replica belong to.
        /// </summary>
        public Match Match
        {
            get { return this.match; }
        }

        /// <summary>
        /// Gets or sets a persistenet ID for the hosted entity.
        /// </summary>
        /// <remarks>
        /// Guid may change as new hosts create new originals for the entity, but
        /// this ID will persist.
        /// </remarks>
        public BadumnaId PersistentID { get; set; }

        /// <summary>
        /// Gets the timestamp of the hosting certificate authorizing hosting of the entity.
        /// </summary>
        public TimeSpan CertificateTimestamp
        {
            get;
            private set;
        }

        /// <inheritdoc/>
        public double Deserialize(BooleanArray includedParts, Stream stream, int estimatedMillisecondsSinceDeparture, BadumnaId id)
        {
            Logger.TraceInformation(LogTag.Match, "Trying to deserialize hosted replica.");

            if (!this.isControllingEntity)
            {
                return 0;
            }

            using (var reader = new BinaryReader(stream))
            {
                if (includedParts[(int)SpatialEntityStateSegment.Match])
                {
                    var length = reader.ReadInt32();
                    var matchId = new BadumnaId();
                    matchId.FromBytes(reader.ReadBytes(length));
                    this.match = this.entityManager.FindMatch(matchId);
                    
                    length = reader.ReadInt32();
                    this.PersistentID = new BadumnaId();
                    this.PersistentID.FromBytes(reader.ReadBytes(length));

                    this.CertificateTimestamp = TimeSpan.FromTicks(reader.ReadInt64());

                    // Only take over or instantiate the replica if it has not already been retrieved.
                    // The replica will not chage for the lifespan of the wrapper, and the match segment
                    // should only be included in the first update. Subsequent inclusion must be the result
                    // of a dropped ack.
                    if (this.hostedEntity == null)
                    {
                        if (!this.entityManager.TryGetHostedEntity(this.PersistentID, out this.hostedEntity))
                        {
                            this.hostedEntity = this.match.InstantiateReplica(EntitySource.Host, this.EntityType);
                            this.entityManager.AddNewHostedEntity(this.hostedEntity, this.EntityType, this.PersistentID, matchId);
                        }

                        Logger.TraceInformation(LogTag.Match, "HostedReplicaWrapper {2} trying to take over: {0} / {1}.", this.PersistentID, this.Guid, this.number);
                        this.isControllingEntity = this.entityManager.TakeOverHostedEntityAsReplica(this);
                        if (this.isControllingEntity)
                        {
                            Logger.TraceInformation(LogTag.Match, "HostedReplicaWrapper {0} successfully took over.", this.number);
                            this.replica = this.entityManager.StartPosingAsIReplica(this.hostedEntity);
                            this.replica.Guid = this.Guid;
                        }
                        else
                        {
                            Logger.TraceInformation(LogTag.Match, "HostedReplicaWrapper {0} failed to take over.", this.number);
                            return 0;
                        }
                    }
                }

                Logger.TraceInformation(LogTag.Match, "HostedReplicaWrapper {0} deserialing wrapped entity.", this.number);
                return this.replica.Deserialize(includedParts, stream, estimatedMillisecondsSinceDeparture, id);
            }
        }   

        /// <inheritdoc/>
        public void HandleEvent(Stream stream)
        {
            this.replica.HandleEvent(stream);
        }

        /// <summary>
        /// Remove this replica from match.
        /// </summary>
        /// <returns>A value indicating whether the hosted entity was removed from the application.</returns>
        public bool RemoveFromMatch()
        {
            // Only remove if currently controlling, otherwise it's most likely an old timed-out hosted replica.
            if (this.isControllingEntity)
            {
                this.entityManager.StopPosingAsIReplica(this.replica);
                if (this.match != null)
                {
                    Logger.TraceWarning(LogTag.Match, "HostedReplicaWrapper {0} removing entity.", this.number);
                    this.queueApplicationEvent(Apply.Func(delegate { this.match.RemoveReplica(EntitySource.Host, this.hostedEntity); }));
                }
                else
                {
                    Logger.TraceWarning(LogTag.Match, "Trying to remove HostedReplicaWrapper {0} from match, but do not know match!", this.number);
                }

                this.isControllingEntity = false;
                return true;
            }

            Logger.TraceWarning(LogTag.Match, "HostedReplicaWrapper {0} not controlling entity, so ignoring RemoveFromMatch.", this.number);
            return false;
        }

        /// <summary>
        ///  Stop feeding updates to the entity.
        /// </summary>
        public void ReleaseEntity()
        {
            if (this.isControllingEntity)
            {
                Logger.TraceWarning(LogTag.Match, "HostedReplicaWrapper {0} not releasing entity.", this.number);
                this.entityManager.StopPosingAsIReplica(this.replica);
                this.isControllingEntity = false;
                return;
            }

            Logger.TraceWarning(LogTag.Match, "HostedReplicaWrapper {0} not controlling entity, so ignoring ReleaseEntity.", this.number);
        }
    }
}
