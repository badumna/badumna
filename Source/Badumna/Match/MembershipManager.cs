﻿//-----------------------------------------------------------------------
// <copyright file="MembershipManager.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2013 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.Match
{
    using System;
    using System.Collections.Generic;
    using Badumna.Core;
    using Badumna.DataTypes;
    using Badumna.Transport;
    using Badumna.Utilities;

    /// <summary>
    /// Responsible for keeping track of who is in a match.
    /// </summary>
    internal class MembershipManager : IMatchMessageHandler, IMembershipReporter, IMatchmakingClient
    {
        /// <summary>
        /// Subscribing to connectivity events.
        /// </summary>
        private readonly IPeerConnectionNotifier connectionNotifier;

        /// <summary>
        /// Private fields are encapsulated in an object to allow access to states.
        /// </summary>
        private readonly MembershipData data;

        /// <summary>
        /// Ticker for running periodic updates (pinging, checking timeouts etc.).
        /// </summary>
        private readonly RegularTask ticker;

        /// <summary>
        /// The current state of the membership manager (using state pattern).
        /// </summary>
        private MembershipState.Base state;

        /// <summary>
        /// Delegate for updating application of state changes.
        /// </summary>
        private MatchStateHandler statusHandler;

        /// <summary>
        /// Explicit backing delegate for host changed, required to prevent JITing on Unity iPhone.
        /// </summary>
        private OnHostChanged hostChangedDelegate;

        /// <summary>
        /// Explicit backing delegate for member added, required to prevent JITing on Unity iPhone.
        /// </summary>
        private GenericEventHandler<IMembershipReporter, MatchMembershipEventArgs> memberAddedDelegate;

        /// <summary>
        /// Explicit backing delegate for member removed, required to prevent JITing on Unity iPhone.
        /// </summary>
        private GenericEventHandler<IMembershipReporter, MatchMembershipEventArgs> memberRemovedDelegate;

        /// <summary>
        /// Explicit backing delegate for chat message received, required to prevent JITing on Unity iPhone.
        /// </summary>
        private GenericEventHandler<IMembershipReporter, MatchChatEventArgs> chatMessageReceivedDelegate;

        /// <summary>
        /// Initializes a new instance of the MembershipManager class.
        /// </summary>
        /// <param name="connectionNotifier">For subscribing to connectivity events.</param>
        /// <param name="certificate">A certificate authorizing a host for the match.</param>
        /// <param name="criteria">Matchmaking criteria.</param>
        /// <param name="maxPlayers">Maximum number of players permitted in the match.</param>
        /// <param name="memberIdentity">The identity of the local member..</param>
        /// <param name="serverAddress">The address of the matchmaking server.</param>
        /// <param name="timeKeeper">Clock for getting the current time.</param>
        /// <param name="matchmaker">The matchmaker.</param>
        /// <param name="matchProtocol">Protocol specifying messages that can be sent to other peers for matches.</param>
        /// <param name="statusHandler">Status handler notifying application of state changes.</param>
        /// <param name="networkEventQueue">For pushing work to be done on the network thread.</param>
        /// <param name="connectivityReporter">For notifications of network going online or offline.</param>
        public MembershipManager(
            IPeerConnectionNotifier connectionNotifier,
            HostAuthorizationCertificate certificate,
            MatchmakingCriteria criteria,
            int maxPlayers,
            MemberIdentity memberIdentity,
            PeerAddress serverAddress,
            ITime timeKeeper,
            IMatchmaker matchmaker,
            IMatchProtocol matchProtocol,
            MatchStateHandler statusHandler,
            INetworkEventScheduler networkEventQueue,
            INetworkConnectivityReporter connectivityReporter)
        {
            if (connectionNotifier == null)
            {
                throw new ArgumentNullException("connectionNotifier");
            }

            this.connectionNotifier = connectionNotifier;
            this.connectionNotifier.ConnectionLostEvent += this.OnConnectionLost;
            this.statusHandler = statusHandler;

            this.data = new MembershipData(
                certificate,
                criteria,
                maxPlayers,
                memberIdentity,
                serverAddress,
                timeKeeper,
                matchmaker,
                matchProtocol);
            this.data.MemberAdded += this.OnMemberAdded;
            this.data.MemberRemoved += this.OnMemberRemoved;
            this.data.Matchmaker.RegisterMatchmakingClient(memberIdentity.ID, this);
            this.data.MatchProtocol.RegisterMessageHandler(memberIdentity.ID, this);

            this.ticker = new RegularTask(
                "Ticker",
                TimeSpan.FromSeconds(1.0 / 20),
                networkEventQueue,
                connectivityReporter,
                this.Update);
        }

        /// <inheritdoc/>
        public event OnHostChanged HostChanged
        {
            add { this.hostChangedDelegate += value; }
            remove { this.hostChangedDelegate -= value; }
        }

        /// <inheritdoc/>
        public event GenericEventHandler<IMembershipReporter, MatchMembershipEventArgs> MemberAdded
        {
            add { this.memberAddedDelegate += value; }
            remove { this.memberAddedDelegate -= value; }
        }

        /// <inheritdoc/>
        public event GenericEventHandler<IMembershipReporter, MatchMembershipEventArgs> MemberRemoved
        {
            add { this.memberRemovedDelegate += value; }
            remove { this.memberRemovedDelegate -= value; }
        }

        /// <inheritdoc/>
        public event GenericEventHandler<IMembershipReporter, MatchChatEventArgs> ChatMessageReceived
        {
            add { this.chatMessageReceivedDelegate += value; }
            remove { this.chatMessageReceivedDelegate -= value; }
        }

        /// <inheritdoc/>
        public BadumnaId MatchIdentifier
        {
            get
            {
                return this.data.Certificate.MatchIdentifier;
            }
        }

        /// <summary>
        /// Gets the current members of the match.
        /// </summary>
        public int MemberCount
        {
            get { return this.data.MemberCount; }
        }

        /// <summary>
        /// Gets the current members of the match.
        /// </summary>
        public IEnumerable<MemberIdentity> Members
        {
            get { return this.data.Members; }
        }

        /// <summary>
        /// Gets a string showing the current host.
        /// </summary>
        public string Host
        {
            get { return this.data.Certificate.Host.ToString(); }
        }

        /// <summary>
        /// Gets a string showing the current state.
        /// </summary>
        public string State
        {
            get { return this.state.GetType().Name; }
        }

        /// <inheritdoc/>
        public bool IsHost
        {
            get { return this.state == this.data.Hosting; }
        }

        /// <summary>
        /// Gets the certificate of the currently recognized host.
        /// </summary>
        public HostAuthorizationCertificate Certificate
        {
            get { return this.data.Certificate; }
        }

        /// <summary>
        /// Begin match membership management.
        /// </summary>
        /// <remarks>
        /// This is not done in the constructor in order to allow event subscription first.
        /// </remarks>
        public void Initialize()
        {
            if (this.data.Certificate.Host == this.data.MemberIdentity.ID)
            {
                this.UpdateState(this.data.Hosting);
            }
            else
            {
                this.UpdateState(this.data.Joining);
            }

            this.ticker.Start();
        }

        /// <summary>
        /// Deregister as message handler.
        /// TODO: Use IDisposable.
        /// </summary>
        public void Leave()
        {
            this.ticker.Stop();
            this.UpdateState(this.state.Leave());
            this.data.Matchmaker.DeregisterMatchmakingClient(this.data.MemberIdentity.ID);
            this.data.MatchProtocol.DeregisterMessageHandler(this.data.MemberIdentity.ID);
            this.connectionNotifier.ConnectionLostEvent += this.OnConnectionLost;
        }

        /// <summary>
        /// Update method should be called regularly (i.e. during process network state).
        /// </summary>
        public void Update()
        {
            this.UpdateState(this.state.Update(this.data.TimeKeeper.Now));
        }

        /// <summary>
        /// Send a chat message to all other members.
        /// </summary>
        /// <param name="message">The chat message.</param>
        public void Chat(string message)
        {
            foreach (var member in this.data.Members)
            {
                if (member == this.data.MemberIdentity)
                {
                    continue;
                }

                this.data.MatchProtocol.Chat(member.ID, this.data.MemberIdentity.ID, this.MatchIdentifier, message, ChatType.Public);
            }
        }

        /// <summary>
        /// Send a chat message to another member.
        /// </summary>
        /// <param name="member">The member to send the message to.</param>
        /// <param name="message">The chat message.</param>
        public void Chat(MemberIdentity member, string message)
        {
            this.data.MatchProtocol.Chat(member.ID, this.data.MemberIdentity.ID, this.MatchIdentifier, message, ChatType.Private);
        }

        /// <inheritdoc/>
        public void OnChat(BadumnaId senderID, BadumnaId matchID, string message, ChatType type)
        {
            var handler = this.chatMessageReceivedDelegate;
            if (handler != null)
            {
                foreach (var member in this.data.Members)
                {
                    if (member == senderID)
                    {
                        handler(this, new MatchChatEventArgs(message, member, type));
                        return;
                    }
                }
            }
        }

        /// <inheritdoc/>
        public void OnJoinRequest(MemberIdentity clientIdentity, BadumnaId matchIdentifier)
        {
            this.UpdateState(this.state.OnJoinRequest(clientIdentity, matchIdentifier));
        }

        /// <inheritdoc/>
        public void OnPingFromClientToHost(MemberIdentity clientIdentity, BadumnaId matchIdentifier)
        {
            this.UpdateState(this.state.OnPingFromClientToHost(clientIdentity, matchIdentifier));
        }

        /// <inheritdoc/>
        public void OnLeaveNotification(BadumnaId client, BadumnaId matchIdentifier)
        {
            this.UpdateState(this.state.OnLeaveNotification(client, matchIdentifier));
        }

        /// <inheritdoc/>
        public void OnJoinConfirmation(HostAuthorizationCertificate certificate, IEnumerable<MemberIdentity> members)
        {
            this.UpdateState(this.state.OnJoinConfirmation(certificate, members));
        }

        /// <inheritdoc/>
        public void OnJoinRejection(HostAuthorizationCertificate certificate)
        {
            this.UpdateState(this.state.OnJoinRejection(certificate));
        }

        /// <inheritdoc/>
        public void OnHostDenial(BadumnaId sender, HostAuthorizationCertificate certificate)
        {
            this.UpdateState(this.state.OnHostDenial(sender, certificate));
        }

        /// <inheritdoc/>
        public void OnMemberJoinNotification(
            HostAuthorizationCertificate certificate,
            MemberIdentity memberIdentity)
        {
            this.UpdateState(this.state.OnMemberJoinNotification(certificate, memberIdentity));
        }

        /// <inheritdoc/>
        public void OnMemberLeaveNotification(
            HostAuthorizationCertificate certificate,
            BadumnaId member)
        {
            this.UpdateState(this.state.OnMemberLeaveNotification(certificate, member));
        }

        /// <inheritdoc/>
        public void OnHostResignation(HostAuthorizationCertificate certificate, int takeoverCode)
        {
            this.UpdateState(this.state.OnHostResignation(certificate, takeoverCode));
        }

        /// <inheritdoc/>
        public void OnPingToPotentialHost(BadumnaId sender, BadumnaId matchIdentifier)
        {
            this.UpdateState(this.state.OnPingToPotentialHost(sender, matchIdentifier));
        }

        /// <inheritdoc/>
        public void OnPongFromPotentialHost(BadumnaId peer, bool inMatch, bool recognizesThisPeer, bool isConnected, HostAuthorizationCertificate certificate)
        {
            this.UpdateState(this.state.OnPongFromPotentialHost(peer, inMatch, recognizesThisPeer, isConnected, certificate));
        }

        /// <summary>
        /// Called to notify on connection failures.
        /// </summary>
        /// <param name="address">The address of the peer to whom a connection has failed.</param>
        public void OnConnectionLost(PeerAddress address)
        {
            this.UpdateState(this.state.OnConnectionLost(address));
        }

        /// <summary>
        /// Called on receipt of a host authorization cerificate from the server.
        /// </summary>
        /// <param name="certificate">The latest certificate of authorization for the match.</param>
        /// <param name="takeoverCode">Password that allows other peers to takeover hosting before current host times out.</param>
        public void HandleHostingReply(HostAuthorizationCertificate certificate, int takeoverCode)
        {
            this.UpdateState(this.state.OnHostingReply(certificate, takeoverCode));
        }

        /// <summary>
        /// Called on receipt of a ping from the matchmaking server.
        /// </summary>
        public void HandlePingFromMatchmaker()
        {
            this.UpdateState(this.state.OnPingFromMatchmaker());
        }

        /// <summary>
        /// Change state, if new state is different.
        /// </summary>
        /// <param name="newState">The new state.</param>
        private void UpdateState(MembershipState.Base newState)
        {
            bool wasHost = this.IsHost;
            if (newState != this.state)
            {
                // Internal states are mapped to just four visisble states for the application.
                if (newState == this.data.Connected)
                {
                    this.statusHandler(MatchStatus.Connected, this.data.Error);
                }
                else if (newState == this.data.Hosting)
                {
                    this.statusHandler(MatchStatus.Hosting, this.data.Error);
                }
                else if (newState == this.data.Done)
                {
                    this.statusHandler(MatchStatus.Closed, this.data.Error);
                }
                else if (this.state == this.data.Connected ||
                    this.state == this.data.Hosting ||
                    this.state == this.data.Done)
                {
                    this.statusHandler(MatchStatus.Connecting, this.data.Error);
                }

                this.state = newState;
                this.state.OnEnter();

                if (this.IsHost != wasHost)
                {
                    var handler = this.hostChangedDelegate;
                    if (handler != null)
                    {
                        handler(this.IsHost);
                    }
                }
            }
        }

        /// <summary>
        /// Handle a member join event.
        /// </summary>
        /// <param name="source">The event source.</param>
        /// <param name="e">The event data.</param>
        private void OnMemberAdded(MembershipData source, MatchMembershipEventArgs e)
        {
            var handler = this.memberAddedDelegate;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        /// <summary>
        /// Handle a member left event.
        /// </summary>
        /// <param name="source">The event source.</param>
        /// <param name="e">The event data.</param>
        private void OnMemberRemoved(MembershipData source, MatchMembershipEventArgs e)
        {
            var handler = this.memberRemovedDelegate;
            if (handler != null)
            {
                handler(this, e);
            }
        }
    }
}
