﻿//-----------------------------------------------------------------------
// <copyright file="MatchChatEventArgs.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2013 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.Match
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Badumna.DataTypes;

    /// <summary>
    /// Provides data for chat events.
    /// </summary>
    public struct MatchChatEventArgs
    {
        /// <summary>
        /// The message text.
        /// </summary>
        private readonly string message;
        
        /// <summary>
        /// The member who sent the messge.
        /// </summary>
        private readonly MemberIdentity sender;
        
        /// <summary>
        /// The type of message.
        /// </summary>
        private readonly ChatType type;
    
        /// <summary>
        /// Initializes a new instance of the MatchChatEventArgs struct.
        /// </summary>
        /// <param name="message">The message text.</param>
        /// <param name="sender">The member who sent the messge.</param>
        /// <param name="type">The type of message.</param>
        public MatchChatEventArgs(string message, MemberIdentity sender, ChatType type)
        {
            this.message = message;
            this.sender = sender;
            this.type = type;
        }

        /// <summary>
        /// Gets the text of the message.
        /// </summary>
        public string Message
        {
            get { return this.message; }
        }

        /// <summary>
        /// Gets the member that sent the message.
        /// </summary>
        public MemberIdentity Sender
        {
            get { return this.sender; }
        }

        /// <summary>
        /// Gets the type of message.
        /// </summary>
        public ChatType Type
        {
            get { return this.type; }
        }
    }
}
