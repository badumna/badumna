﻿//-----------------------------------------------------------------------
// <copyright file="MatchReplicaWrapper.cs" company="Scalify">
//     Copyright (c) 2013 Scalify. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.Match
{
    using System.IO;
    using Badumna.Core;
    using Badumna.DataTypes;
    using Badumna.Replication;
    using Badumna.SpatialEntities;
    using Badumna.Utilities;

    /// <summary>
    /// Replica wrapper used in match to represent an original entity's replica.
    /// </summary>
    internal class MatchReplicaWrapper : IReplica
    {
        /// <summary>
        /// The application's replica.
        /// </summary>
        private IReplica replica;

        /// <summary>
        /// The type of the entities.
        /// </summary>
        private uint entityType;

        /// <summary>
        /// The match.
        /// </summary>
        private Match match;

        /// <summary>
        /// The queue to use for scheduling events.
        /// </summary>
        private INetworkEventScheduler eventQueue;

        /// <summary>
        /// Match entity manager.
        /// </summary>
        private EntityManager entityManager;

        /// <summary>
        /// The method to use to queue events that must run in the application's thread.
        /// </summary>
        private QueueInvokable queueApplicationEvent;

        /// <summary>
        /// Initializes a new instance of the MatchReplicaWrapper class.
        /// </summary>
        /// <param name="entityId">Unique ID of the replica.</param>
        /// <param name="entityType">Entity type of the replica.</param>
        /// <param name="eventQueue">The event queue to schedule events on.</param>
        /// <param name="entityManager">Match entity manager.</param>
        /// <param name="queueApplicationEvent">The method to use to queue events that must run in the application's thread.</param>
        public MatchReplicaWrapper(
            BadumnaId entityId,
            uint entityType,
            INetworkEventScheduler eventQueue,
            EntityManager entityManager,
            QueueInvokable queueApplicationEvent)
        {
            this.eventQueue = eventQueue;
            this.entityType = entityType;
            this.Guid = entityId;
            this.entityManager = entityManager;
            this.queueApplicationEvent = queueApplicationEvent;
        }

        /// <summary>
        /// Gets or sets the unique identifier of the spatial replicas in the collection.
        /// </summary>
        public BadumnaId Guid { get; set; }

        /// <summary>
        /// Gets the match where the replica belong to.
        /// </summary>
        public Match Match
        {
            get { return this.match; }
        }

        /// <inheritdoc/>
        public double Deserialize(BooleanArray includedParts, Stream stream, int estimatedMillisecondsSinceDeparture, BadumnaId id)
        {
            Logger.TraceInformation(LogTag.Match, "Calling deserialize on replica wrapper for match entity {0}", this.Guid);
            using (var reader = new BinaryReader(stream))
            {
                if (includedParts[(int)SpatialEntityStateSegment.Match])
                {
                    var length = reader.ReadInt32();
                    var matchId = new BadumnaId();
                    matchId.FromBytes(reader.ReadBytes(length));

                    // The match should never change, so only set match and instantiate replica the first time it is received.
                    // The match ID will only be included in subsequent updates if there was a dropped ack.
                    if (this.match == null)
                    {
                        // get the match
                        this.match = this.entityManager.FindMatch(matchId);

                        // Instantiate replica
                        Logger.TraceInformation(LogTag.Match, "Instantiating replica for match entity {0}", this.Guid);
                        this.replica = this.entityManager.StartPosingAsIReplica(this.match.InstantiateReplica(EntitySource.Peer, this.entityType));
                        this.replica.Guid = this.Guid;
                    }
                }

                return this.replica.Deserialize(includedParts, stream, estimatedMillisecondsSinceDeparture, id);
            }
        }

        /// <inheritdoc/>
        public void HandleEvent(Stream stream)
        {
            this.replica.HandleEvent(stream);
        }

        /// <summary>
        /// Remove this replica from match.
        /// </summary>
        public void RemoveFromMatch()
        {
            this.queueApplicationEvent(Apply.Func(delegate
            {
                this.match.RemoveReplica(
                    EntitySource.Peer,
                    this.entityManager.StopPosingAsIReplica(this.replica));
            }));
        }
    }
}
