﻿//-----------------------------------------------------------------------
// <copyright file="MatchStatusEventArgs.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2013 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.Match
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Badumna.DataTypes;

    /// <summary>
    /// Provides data for a StatusChanged event.
    /// </summary>
    public struct MatchStatusEventArgs
    {
        /// <summary>
        /// The match status.
        /// </summary>
        private readonly MatchStatus status;

        /// <summary>
        /// The error.
        /// </summary>
        private readonly MatchError error;

        /// <summary>
        /// Initializes a new instance of the MatchStatusEventArgs struct.
        /// </summary>
        /// <param name="status">The new status.</param>
        public MatchStatusEventArgs(MatchStatus status)
            : this(status, MatchError.None)
        {
        }

        /// <summary>
        /// Initializes a new instance of the MatchStatusEventArgs struct.
        /// </summary>
        /// <param name="status">The new status.</param>
        /// <param name="error">The error.</param>
        public MatchStatusEventArgs(MatchStatus status, MatchError error)
        {
            this.status = status;
            this.error = error;
        }

        /// <summary>
        /// Gets the new status.
        /// </summary>
        public MatchStatus Status
        {
            get { return this.status; }
        }

        /// <summary>
        /// Gets any error encountered.
        /// </summary>
        public MatchError Error
        {
            get { return this.error; }
        }
    }
}
