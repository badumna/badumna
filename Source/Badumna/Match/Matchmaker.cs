﻿//------------------------------------------------------------------------------
// <copyright file="Matchmaker.cs" company="National ICT Australia Limited">
//     Copyright (c) 2013 National ICT Australia Limited. All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

namespace Badumna.Match
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using Badumna.Core;
    using Badumna.DataTypes;
    using Badumna.Transport;
    using Badumna.Utilities;
    using Badumna.Utilities.Logging;

    /// <summary>
    /// Maintains a record of matches, including the host for the match.
    /// Responds to queries for active matches.
    /// </summary>
    internal class Matchmaker : IMatchmaker
    {
        /// <summary>
        /// If a HostingRequest hasn't been received from the current host for this long then the match
        /// is up for grabs by a new host.  The matchmaker can throw the match record away at this
        /// point because the new host will recreate it if the game is continuing.
        /// </summary>
        public static readonly TimeSpan HostTimeout = TimeSpan.FromSeconds(10);  // TODO: Set this based on the host ping frequency.

        /// <summary>
        /// How many milliseconds to wait for a host to reply to a matchmaker ping (to retain hosting rights).
        /// </summary>
        public static readonly TimeSpan HostCheckTimeout = TimeSpan.FromSeconds(2);

        /// <summary>
        /// The maximum number of matches returned in response to a match query.
        /// </summary>
        private const int QueryResultLimit = 100;

        /// <summary>
        /// The maximum time to wait for a query result.
        /// </summary>
        private static readonly TimeSpan QueryTimeout = TimeSpan.FromSeconds(15);

        /// <summary>
        /// The time keeper.
        /// </summary>
        private readonly ITime timeKeeper;

        /// <summary>
        /// The network event queue.
        /// </summary>
        private readonly INetworkEventScheduler networkEventQueue;

        /// <summary>
        /// The transport.
        /// </summary>
        private readonly TransportProtocol transport;

        /// <summary>
        /// The system configuration.
        /// </summary>
        private readonly Options options;

        /// <summary>
        /// A random number generator for creating match takeover codes.
        /// </summary>
        private readonly IRandomNumberGenerator RNG;

        /// <summary>
        /// The active matches.
        /// </summary>
        private readonly Dictionary<BadumnaId, MatchRecord> matches = new Dictionary<BadumnaId, MatchRecord>();

        /// <summary>
        /// A map of hostID => hosting reply handler, used to farm out replies to hosting requests.
        /// </summary>
        private readonly Dictionary<BadumnaId, IMatchmakingClient> matchmakingClients =
            new Dictionary<BadumnaId, IMatchmakingClient>();

        /// <summary>
        /// In progress host checks made in response to takeover attempts.
        /// </summary>
        private readonly Dictionary<MatchRecord, HostCheck> hostChecksByMatch =
            new Dictionary<MatchRecord, HostCheck>();

        /// <summary>
        /// A map of query id => query record used for match query responses.
        /// </summary>
        private readonly Dictionary<ushort, QueryRecord> outstandingQueries = new Dictionary<ushort, QueryRecord>();

        /// <summary>
        /// Lock for the handler containers because they're accessed from the public
        /// API and from the network event queue.
        /// </summary>
        private readonly object clientsLock = new object();

        /// <summary>
        /// An id to match query messages with their replies.
        /// </summary>
        /// <remarks>
        /// Simply incremented after each query.
        /// Will wrap around, but if the client is making ushort.Max queries before the replies come
        /// back then they've got other problems.
        /// </remarks>
        private ushort nextQueryId;

        /// <summary>
        /// Initializes a new instance of the Matchmaker class.
        /// </summary>
        /// <param name="timeKeeper">The time keeper</param>
        /// <param name="networkEventQueue">For pushing work to be done on the network thread.</param>
        /// <param name="transport">The transport</param>
        /// <param name="options">The configuration</param>
        public Matchmaker(ITime timeKeeper, INetworkEventScheduler networkEventQueue, ITransportProtocol transport, Options options)
        {
            this.timeKeeper = timeKeeper;
            this.networkEventQueue = networkEventQueue;
            this.transport = (TransportProtocol)transport;
            this.options = options;

            // TODO: inject for testing.
            this.RNG = new RandomNumberGenerator();

            this.transport.RegisterMethodsIn(this);
        }

        /// <inheritdoc />
        public void RegisterMatchmakingClient(BadumnaId handlerID, IMatchmakingClient client)
        {
            if (handlerID == null)
            {
                throw new ArgumentNullException("handlerID");
            }

            if (client == null)
            {
                throw new ArgumentNullException("handler");
            }

            lock (this.clientsLock)
            {
                this.matchmakingClients.Add(handlerID, client);
            }
        }

        /// <inheritdoc />
        public void DeregisterMatchmakingClient(BadumnaId handlerID)
        {
            lock (this.clientsLock)
            {
                this.matchmakingClients.Remove(handlerID);
            }
        }

        /// <inheritdoc />
        public void HostingRequest(
            BadumnaId requestorID,
            BadumnaId matchIdentifier,
            MatchmakingCriteria criteria,
            MatchCapacity status,
            int takeoverCode)
        {
            Logger.TraceInformation(LogTag.Match, "Sending hosting request to server {0}", this.options.Matchmaking.ServerPeerAddress);

            lock (this.clientsLock)
            {
                if (!this.matchmakingClients.ContainsKey(requestorID))
                {
                    throw new InvalidOperationException("Must register reply handler first");
                }
            }

            this.transport.SendRemoteCall(
                this.options.Matchmaking.ServerPeerAddress,
                QualityOfService.Reliable,
                this.HandleHostingRequest,
                requestorID.LocalId,
                matchIdentifier,
                this.options.CloudIdentifier ?? "",
                criteria,
                status,
                takeoverCode);
        }

        /// <inheritdoc />
        public void MatchQuery(
            MatchmakingCriteria criteria,
            GenericCallBack<IList<MatchmakingResult>> resultNotification,
            GenericCallBack<MatchError> failureNotification)
        {
            if (resultNotification == null)
            {
                // Pointless making a query if we don't have somewhere to send the results.
                throw new ArgumentNullException("resultNotification");
            }

            if (failureNotification == null)
            {
                // And if we aren't handling failures then things ain't grand neither.
                throw new ArgumentNullException("failureNotification");
            }

            ushort queryId;
            lock (this.clientsLock)
            {
                queryId = this.nextQueryId;
                unchecked
                {
                    this.nextQueryId++;
                }
            }

            var timeoutEvent = this.networkEventQueue.Schedule(
                Matchmaker.QueryTimeout.TotalMilliseconds,
                delegate
                {
                    var queryRecord = this.FinalizeQuery(queryId);
                    if (queryRecord != null)
                    {
                        queryRecord.FailureHandler(MatchError.CannotConnectToMatchmaker);
                    }
                });

            lock (this.clientsLock)
            {
                this.outstandingQueries[queryId] = new QueryRecord(
                    resultNotification,
                    failureNotification,
                    timeoutEvent);
            }

            var qos = new FailNotificationQos(
                QualityOfService.Reliable,
                delegate(int attempt)
                {
                    if (attempt == -1)
                    {
                        // Connection to the matchmaking server failed, notify of failure.
                        QueryRecord queryRecord = this.FinalizeQuery(queryId);
                        if (queryRecord != null)
                        {
                            queryRecord.FailureHandler(MatchError.CannotConnectToMatchmaker);
                        }

                        return false;
                    }

                    return true; // Retry the message.
                });

            this.networkEventQueue.Push(
                delegate
                {
                    Logger.TraceInformation(LogTag.Match, "Sending match query {0} to server {1}", queryId, this.options.Matchmaking.ServerPeerAddress);

                    this.transport.SendRemoteCall(
                        this.options.Matchmaking.ServerPeerAddress,
                        qos,
                        this.HandleMatchQuery,
                        queryId,
                        this.options.CloudIdentifier ?? "",
                        criteria);
                });
        }

        /// <inheritdoc />
        public void PingFromMatchmakerToHost(MatchRecord matchRecord)
        {
            Logger.TraceInformation(
                LogTag.Match,
                "Pinging {0} to check it's still hosting match {1}",
                matchRecord.Certificate.Host,
                matchRecord.Certificate.MatchIdentifier);

            this.transport.SendRemoteCall(
                matchRecord.Certificate.Host.Address,
                QualityOfService.Reliable,
                this.HandlePingFromMatchmakerToHost,
                matchRecord.Certificate.Host.LocalId);
        }

        /// <summary>
        /// Send a hosting reply to a claimant.
        /// </summary>
        /// <param name="recipient">The claimant to reply to.</param>
        /// <param name="match">The match to reply about.</param>
        public void SendHostingReply(BadumnaId recipient, MatchRecord match)
        {
            Logger.TraceInformation(LogTag.Match, "Sending hosting reply.");

            // Only send the takeover code to the host.
            int publishedTakeoverCode = int.MaxValue;
            if (recipient == match.Certificate.Host)
            {
                publishedTakeoverCode = match.TakeoverCode;
            }

            this.transport.SendRemoteCall(
                recipient.Address,
                QualityOfService.Reliable,
                this.HandleHostingReply,
                recipient.LocalId,
                match.Certificate,
                publishedTakeoverCode);
        }

        /// <summary>
        /// Remove a completed host check.
        /// </summary>
        /// <param name="match">The match the host check is for.</param>
        public void RemoveCompletedHostCheck(MatchRecord match)
        {
            this.hostChecksByMatch.Remove(match);
        }

        /// <summary>
        /// Handle a ping from the matchmaker checking that a given host is still running on this peer.
        /// </summary>
        /// <param name="hostLocalID">The local ID of the host.</param>
        [ConnectionfulProtocolMethod(ConnectionfulMethod.MatchmakingHostPing)]
        private void HandlePingFromMatchmakerToHost(ushort hostLocalID)
        {
            Logger.TraceInformation(LogTag.Match, "Received ping from matchmaker.");
            var clientID = new BadumnaId(this.transport.CurrentEnvelope.Destination, hostLocalID);
            IMatchmakingClient client;
            lock (this.clientsLock)
            {
                this.matchmakingClients.TryGetValue(clientID, out client);
            }

            if (client != null)
            {
                client.HandlePingFromMatchmaker();
            }
        }

        /// <summary>
        /// Removes the query record from the outstanding set and cancels the associated timeout.
        /// </summary>
        /// <param name="queryId">The query id</param>
        /// <returns>The query record, or <c>null</c> if not found (because, e.g., it has already timed out).</returns>
        private QueryRecord FinalizeQuery(ushort queryId)
        {
            QueryRecord queryRecord;
            lock (this.clientsLock)
            {
                if (this.outstandingQueries.TryGetValue(queryId, out queryRecord))
                {
                    this.outstandingQueries.Remove(queryId);
                }
            }

            if (queryRecord != null)
            {
                this.networkEventQueue.Remove(queryRecord.TimeoutEvent);
            }

            return queryRecord;
        }

        /// <summary>
        /// Remote implementation of <see cref="IMatchmaker.HostingRequest"/>.
        /// </summary>
        /// <param name="requestorLocalID">The local id of the requestor.</param>
        /// <param name="matchIdentifier">Unique identifier for the match</param>
        /// <param name="cloudIdentifier">Unique identifier for the cloud application, or the empty string if not on the cloud.</param>
        /// <param name="criteria">Criteria used to filter match queries</param>
        /// <param name="status">The current status of the match</param>
        /// <param name="takeoverCode">Code authorizing takeover before time out.</param>
        [ConnectionfulProtocolMethod(ConnectionfulMethod.MatchmakingHostingRequest)]
        private void HandleHostingRequest(
            ushort requestorLocalID,
            BadumnaId matchIdentifier,
            string cloudIdentifier,
            MatchmakingCriteria criteria,
            MatchCapacity status,
            int takeoverCode)
        {
            Logger.TraceInformation(LogTag.Match, "Received hosting request.");
            
            var requestorID = new BadumnaId(this.transport.CurrentEnvelope.Source, requestorLocalID);
            
            MatchRecord matchRecord;
            if (!this.matches.TryGetValue(matchIdentifier, out matchRecord))
            {
                if (this.matches.Count >= this.options.Matchmaking.ActiveMatchLimit)
                {
                    this.CollectGarbage();  // Hopefully doesn't take too long...

                    if (this.matches.Count >= this.options.Matchmaking.ActiveMatchLimit)
                    {
                        // No free slots.
                        // TODO: Send failure notice (but only if ActiveMatchLimit > 0).
                        return;
                    }
                }

                // New match
                matchRecord = new MatchRecord(this.RNG, this.timeKeeper, cloudIdentifier, matchIdentifier, requestorID, criteria, status);
                this.matches[matchIdentifier] = matchRecord;
                this.SendHostingReply(requestorID, matchRecord);
                return;
            }

            if (cloudIdentifier != matchRecord.CloudIdentifer)
            {
                Logger.TraceWarning(
                    LogTag.Match,
                    "Ignoring Hosting Request from member with cloud ID {0} for match with cloud ID {1}",
                    cloudIdentifier,
                    matchRecord.CloudIdentifer);
                return;
            }

            if (requestorID.Equals(matchRecord.Certificate.Host))
            {
                // Ping from current host.
                matchRecord.Update(criteria, status);

                // Reply to reject any claimants.
                HostCheck outstandingHostCheck;
                if (this.hostChecksByMatch.TryGetValue(matchRecord, out outstandingHostCheck))
                {
                    outstandingHostCheck.ConfirmHostIsLive();
                }

                return;
            }

            // Attempted takeover by new host
            var now = this.timeKeeper.Now;
            if (takeoverCode == matchRecord.TakeoverCode ||
                matchRecord.IsTimedOutAt(now))
            {
                // Resigned and timed-out matches can be taken over immediately.
                matchRecord.TakeOver(requestorID, criteria, status);
                this.SendHostingReply(requestorID, matchRecord);
            }
            else
            {
                // Unresigned and not timed-out matches must ping host before allowing takeover.
                HostCheck hostCheck;
                if (this.hostChecksByMatch.TryGetValue(matchRecord, out hostCheck))
                {
                    hostCheck.AddClaimant(requestorID, criteria, status);
                }
                else
                {
                    this.hostChecksByMatch.Add(
                        matchRecord, new HostCheck(matchRecord, requestorID, criteria, status, now, this, this.networkEventQueue));
                }
            }
        }

        /// <summary>
        /// Handle the response to a hosting request.
        /// </summary>
        /// <param name="requestorLocalID">The local id of the requestor.</param>
        /// <param name="certificate">Certificate for currently authorized host.</param>
        /// <param name="takeoverCode">Password authorizing takeover (broadcast by host on resignation).</param>
        [ConnectionfulProtocolMethod(ConnectionfulMethod.MatchmakingHostingReply)]
        private void HandleHostingReply(ushort requestorLocalID, HostAuthorizationCertificate certificate, int takeoverCode)
        {
            Logger.TraceInformation(LogTag.Match, "Received hosting reply.");
            var requestorID = new BadumnaId(this.transport.CurrentEnvelope.Destination, requestorLocalID);
            IMatchmakingClient client;
            lock (this.clientsLock)
            {
                this.matchmakingClients.TryGetValue(requestorID, out client);
            }

            if (client != null)
            {
                client.HandleHostingReply(certificate, takeoverCode);
            }
        }

        /// <summary>
        /// Remote implementation of <see cref="IMatchmaker.MatchQuery"/>.
        /// </summary>
        /// <param name="queryId">Unique identifier for the query.</param>
        /// <param name="cloudIdentifier">Unique identifier for the cloud application, or the empty string if not on the cloud.</param>
        /// <param name="criteria">Criteria used to filter the query results.</param>
        [ConnectionfulProtocolMethod(ConnectionfulMethod.MatchmakingMatchQuery)]
        private void HandleMatchQuery(ushort queryId, string cloudIdentifier, MatchmakingCriteria criteria)
        {
            Logger.TraceInformation(LogTag.Match, "Received match query {0}.", queryId);
            var results = new List<MatchmakingResult>();
            var doGarbageCollection = false;
            foreach (var match in this.matches.Values)
            {
                if (match.IsTimedOutAt(this.timeKeeper.Now))
                {
                    doGarbageCollection = true;
                    continue;
                }

                // Cloud identifier can be simply checked for equality.
                // Either the game is on the cloud, in which case the ids must match;
                // or the game is off the cloud, in which case the ids must both be the empty string.
                if (match.Status.EmptySlots > 0 &&
                    match.CloudIdentifer == cloudIdentifier &&
                    match.Criteria.Satisfies(criteria) &&

                    // Ignore matches hosted on the peer who went the query, as they are probably stale.
                    !match.Certificate.Host.Address.Equals(this.transport.CurrentEnvelope.Source))
                {
                    results.Add(new MatchmakingResult(
                        match.Certificate,
                        match.Status,
                        match.Criteria));
                }

                if (results.Count >= Matchmaker.QueryResultLimit)
                {
                    break;
                }
            }

            this.transport.SendRemoteCall(
                this.transport.CurrentEnvelope.Source,
                QualityOfService.Reliable,
                this.HandleMatchQueryReply,
                queryId,
                results);

            if (doGarbageCollection)
            {
                this.CollectGarbage();
            }
        }

        /// <summary>
        /// Handle the response to a match query.
        /// </summary>
        /// <param name="queryId">Unique identifier for the query.</param>
        /// <param name="results">A list of matches that meet the criteria.</param>
        [ConnectionfulProtocolMethod(ConnectionfulMethod.MatchmakingMatchQueryReply)]
        private void HandleMatchQueryReply(ushort queryId, List<MatchmakingResult> results)
        {
            QueryRecord queryRecord = this.FinalizeQuery(queryId);
            if (queryRecord != null)
            {
                queryRecord.ResultHandler(results);
            }
            else
            {
                Logger.TraceWarning(LogTag.Match, "Got query reply with unknown id {0}", queryId);
            }
        }

        /// <summary>
        /// Discard matches that haven't sent a ping recently.
        /// </summary>
        private void CollectGarbage()
        {
            var toRemove = new List<BadumnaId>();
            foreach (var match in this.matches.Values)
            {
                if (match.IsTimedOutAt(this.timeKeeper.Now))
                {
                    toRemove.Add(match.Certificate.MatchIdentifier);
                }
            }

            foreach (var matchId in toRemove)
            {
                this.matches.Remove(matchId);
            }
        }

        /// <summary>
        /// Details of an outstanding query.
        /// </summary>
        private class QueryRecord
        {
            /// <summary>
            /// Initializes a new instance of the Matchmaker.QueryRecord class.
            /// </summary>
            /// <param name="resultHandler">The query result handler.</param>
            /// <param name="failureHandler">The query failure handler.</param>
            /// <param name="timeoutEvent">The timeout event.</param>
            public QueryRecord(
                GenericCallBack<IList<MatchmakingResult>> resultHandler,
                GenericCallBack<MatchError> failureHandler,
                NetworkEvent timeoutEvent)
            {
                this.ResultHandler = resultHandler;
                this.FailureHandler = failureHandler;
                this.TimeoutEvent = timeoutEvent;
            }

            /// <summary>
            /// Gets the query result handler.
            /// </summary>
            public GenericCallBack<IList<MatchmakingResult>> ResultHandler { get; private set; }

            /// <summary>
            /// Gets the query failure handler.
            /// </summary>
            public GenericCallBack<MatchError> FailureHandler { get; private set; }

            /// <summary>
            /// Gets the timeout event.
            /// </summary>
            public NetworkEvent TimeoutEvent { get; private set; }
        }
    }
}
