﻿//-----------------------------------------------------------------------
// <copyright file="HostAuthorizationCertificate.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2013 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.Match
{
    using System;
    using Badumna.Core;
    using Badumna.DataTypes;

    /// <summary>
    /// Issued by the matchmaking server that a known peer is authorized to act
    /// as host for a given match.
    /// </summary>
    /// <remarks>
    /// TODO: Use public key cryptography to digitally sign certificates.
    /// </remarks>
    public class HostAuthorizationCertificate : IParseable
    {
        /// <summary>
        /// An empty certificate.
        /// </summary>
        private static HostAuthorizationCertificate emptyCertificate = new HostAuthorizationCertificate();
        
        /// <summary>
        /// Initializes a new instance of the HostAuthorizationCertificate class.
        /// </summary>
        /// <param name="matchIdentifier">A unique identifier for the match.</param>
        /// <param name="host">An authorized host for the match.</param>
        /// <param name="timestamp">The time the certificate was issued.</param>
        public HostAuthorizationCertificate(BadumnaId matchIdentifier, BadumnaId host, TimeSpan timestamp)
        {
            this.MatchIdentifier = matchIdentifier;
            this.Host = host;
            this.Timestamp = timestamp;
        }

        /// <summary>
        /// Prevents a default instance of the HostAuthorizationCertificate class from being created.
        /// </summary>
        private HostAuthorizationCertificate()
        {
        }

        /// <summary>
        /// Gets an empty certificate.
        /// </summary>
        public static HostAuthorizationCertificate Empty
        {
            get { return emptyCertificate; }
        }
        
        /// <summary>
        /// Gets the unique identifier for the match.
        /// </summary>
        public BadumnaId MatchIdentifier { get; private set; }

        /// <summary>
        /// Gets the authorized host for the match.
        /// </summary>
        public BadumnaId Host { get; private set; }

        /// <summary>
        /// Gets the rank with which the host is assigned to the match.
        /// </summary>
        public TimeSpan Timestamp { get; private set; }

        /// <summary>
        /// Checks that another certificate is for the same match as this one.
        /// </summary>
        /// <param name="certificate">The other certificate to test.</param>
        /// <returns><c>true</c> if this certificate is for the given match,
        /// otherwise <c>false</c>.</returns>
        public bool AppliesToSameMatch(HostAuthorizationCertificate certificate)
        {
            return certificate.MatchIdentifier == this.MatchIdentifier;
        }

        /// <summary>
        /// Checks that this certificate is not outranked by another given certificate.
        /// </summary>
        /// <param name="certificate">The other certificate.</param>
        /// <returns><c>true</c> if this certificate is not outranked by the other one,
        /// otherwise <c>false</c>.</returns>
        public bool IsNotOutrankedBy(HostAuthorizationCertificate certificate)
        {
            if (certificate == null ||
                certificate.MatchIdentifier != this.MatchIdentifier ||
                certificate.Timestamp <= this.Timestamp)
            {
                return true;
            }

            return false;
        }

        /// <inheritdoc />
        void IParseable.ToMessage(MessageBuffer message, Type parameterType)
        {
            message.Write(this.MatchIdentifier);
            message.Write(this.Host);
            message.Write(this.Timestamp.Ticks);
        }

        /// <inheritdoc />
        void IParseable.FromMessage(MessageBuffer message)
        {
            this.MatchIdentifier = message.Read<BadumnaId>();
            this.Host = message.Read<BadumnaId>();
            this.Timestamp = TimeSpan.FromTicks(message.ReadLong());
        }
    }
}
