﻿//-----------------------------------------------------------------------
// <copyright file="MatchCapacity.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2013 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.Match
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Badumna.Core;

    /// <summary>
    /// Match status sent to server from host.
    /// </summary>
    public struct MatchCapacity : IParseable
    {
        /// <summary>
        /// Initializes a new instance of the MatchCapacity struct.
        /// </summary>
        /// <param name="numSlots">The number of players who can be in the match.</param>
        /// <param name="emptySlots">The number of empty places for players.</param>
        public MatchCapacity(int numSlots, int emptySlots)
            : this()
        {
            this.NumSlots = numSlots;
            this.EmptySlots = emptySlots;
        }

        /// <summary>
        /// Gets the number of players who can be in the match.
        /// </summary>
        public int NumSlots { get; private set; }

        /// <summary>
        /// Gets or sets the number of empty places for players.
        /// </summary>
        public int EmptySlots { get; set; }

        /// <inheritdoc />
        void IParseable.ToMessage(MessageBuffer message, Type parameterType)
        {
            message.Write(this.NumSlots);
            message.Write(this.EmptySlots);
        }

        /// <inheritdoc />
        void IParseable.FromMessage(MessageBuffer message)
        {
            this.NumSlots = message.ReadInt();
            this.EmptySlots = message.ReadInt();
        }
    }
}
