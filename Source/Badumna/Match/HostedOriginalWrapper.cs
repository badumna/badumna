﻿//-----------------------------------------------------------------------
// <copyright file="HostedOriginalWrapper.cs" company="Scalify">
//     Copyright (c) 2013 Scalify. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.Match
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using Badumna.DataTypes;
    using Badumna.Replication;
    using Badumna.SpatialEntities;
    using Badumna.Utilities;

    /// <summary>
    /// Wrapper class used for managing spatial originals with match membership
    /// and replication.
    /// </summary>
    internal class HostedOriginalWrapper : IOriginal
    {
        /// <summary>
        /// The IOriginal for the hosted entity.
        /// </summary>
        private readonly IOriginal original;

        /// <summary>
        /// Membership reporter.
        /// </summary>
        private readonly IMembershipReporter membershipReporter;

        /// <summary>
        /// The entity manager.
        /// </summary>
        private IEntityManager entityManager;

        /// <summary>
        /// Initializes a new instance of the HostedOriginalWrapper class.
        /// </summary>
        /// <param name="original">IOriginal for the hosted entity.</param>
        /// <param name="entityID">An ID for this original.</param>
        /// <param name="persistentID">An ID for the entity that persist over host transitions.</param>
        /// <param name="entityType">An integer used by the application to identify the type of the entity.</param>
        /// <param name="membershipReporter">Membership reporter.</param>
        /// <param name="entityManager">Entity manager.</param>
        public HostedOriginalWrapper(
            IOriginal original,
            BadumnaId entityID,
            BadumnaId persistentID,
            uint entityType,
            IMembershipReporter membershipReporter,
            IEntityManager entityManager)
        {
            Logger.TraceInformation(LogTag.Match, "Hosted original wrapper taking control {0} / {1}", persistentID, entityID);
            this.PersistentID = persistentID;
            this.CertificateTimestamp = membershipReporter.Certificate.Timestamp;
            this.membershipReporter = membershipReporter;
            this.entityManager = entityManager;

            this.original = original;
            this.original.Guid = entityID;
            this.entityManager.RegisterEntity(this, new EntityTypeId((byte)EntityGroup.MatchHosted, entityType));

            // TODO: Unsubscribe when match is closed.
            // TODO: Is that necessary or will the match and all dependencies
            // be garbage collected anyway?
            // If it is required, should use IDispoable pattern.
            this.membershipReporter.MemberAdded += this.OnMemberJoined;
            this.membershipReporter.MemberRemoved += this.OnMemberLeft;

            foreach (var member in this.membershipReporter.Members)
            {
                this.AddInterested(member.ID);
            }
        }

        /// <summary>
        /// Gets or sets the unique identifier of the original.
        /// </summary>
        public BadumnaId Guid
        {
            get { return this.original.Guid; }
            set { this.original.Guid = value; }
        }

        /// <summary>
        /// Gets the type of the entities.
        /// </summary>
        public uint EntityType
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets or sets a persistenet ID for the hosted entity.
        /// </summary>
        /// <remarks>
        /// Guid may change as new hosts create new originals for the entity, but
        /// this ID will persist.
        /// </remarks>
        public BadumnaId PersistentID
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the timestamp of the hosting certificate authorizing hosting of the entity.
        /// </summary>
        public TimeSpan CertificateTimestamp
        {
            get;
            private set;
        }

        /// <inheritdoc />
        public BooleanArray Serialize(BooleanArray requiredParts, Stream stream)
        {
            Logger.TraceInformation(LogTag.Match, "Serializing hosted original: {0}", requiredParts.HighestUsedIndex);
            for (int i = 0; i < requiredParts.HighestUsedIndex; ++i)
            {
                Logger.TraceInformation(LogTag.Match, " - {0}", requiredParts[i]);
            }

            using (var writer = new BinaryWriter(stream))
            {
                if (requiredParts[(int)SpatialEntityStateSegment.Match])
                {
                    var bytes = this.membershipReporter.MatchIdentifier.ToBytes();
                    Logger.TraceInformation(LogTag.Match, " - match ID: {0} bytes.", bytes.Length);
                    writer.Write(bytes.Length);
                    writer.Write(bytes);

                    // TODO: Should be able to serialize a BadumnaId without encoding length explicitly.
                    bytes = this.PersistentID.ToBytes();
                    Logger.TraceInformation(LogTag.Match, " - persistent ID: {0} bytes.", bytes.Length);
                    writer.Write(bytes.Length);
                    writer.Write(bytes);

                    writer.Write(this.CertificateTimestamp.Ticks);
                }

                this.original.Serialize(requiredParts, stream);
                
                // We'll always send all required parts.
                return new BooleanArray();
            }
        }

        /// <summary>
        /// Release the entity from being controlled by this original.
        /// </summary>
        /// <param name="removeReplicas">A value indicating whether to emove the associated replicas.</param>
        public void ReleaseEntity(bool removeReplicas)
        {
            Logger.TraceInformation(LogTag.Match, "Releasing hosted entity from original (removing replicas:{0}).", removeReplicas);
            this.entityManager.UnregisterEntity(this.original, removeReplicas);
            this.membershipReporter.MemberAdded -= this.OnMemberJoined;
            this.membershipReporter.MemberRemoved -= this.OnMemberLeft;
        }

        /// <inheritdoc />
        public void HandleEvent(Stream stream)
        {
            this.original.HandleEvent(stream);
        }

        /// <summary>
        /// Called when a new member has joined the match.
        /// </summary>
        /// <param name="sender">The event source.</param>
        /// <param name="e">The event data.</param>
        private void OnMemberJoined(IMembershipReporter sender, MatchMembershipEventArgs e)
        {
            this.AddInterested(e.Member.ID);
        }

        /// <summary>
        /// Called when an existing member has left the match.
        /// </summary>
        /// <param name="sender">The event source.</param>
        /// <param name="e">The event data.</param>
        private void OnMemberLeft(IMembershipReporter sender, MatchMembershipEventArgs e)
        {
            this.RemoveInterested(e.Member.ID);
        }

        /// <summary>
        /// Add the interest member.
        /// </summary>
        /// <param name="member">The member interested.</param>
        private void AddInterested(BadumnaId member)
        {
            if (member.Address.Equals(this.original.Guid.OriginalAddress))
            {
                Logger.TraceInformation(LogTag.Match, "Ignoring interest notification for {0} because it shares the same address as the entity {1}", member, this.original.Guid);
                return;
            }

            this.entityManager.AddInterested(this.original, member);
        }

        /// <summary>
        /// Remove interested member.
        /// </summary>
        /// <param name="member">The uninterested member.</param>
        private void RemoveInterested(BadumnaId member)
        {
            this.entityManager.RemoveInterested(this.original, member);
        }
    }
}
