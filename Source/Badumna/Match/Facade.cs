﻿//-----------------------------------------------------------------------
// <copyright file="Facade.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2013 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.Match
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;
    using Badumna.Core;
    using Badumna.Transport;
    using Badumna.Utilities;
    using MatchAutoreplicationManager = Autoreplication.MatchAutoreplicationManager;

    /// <summary>
    /// Delegate for match status updates.
    /// </summary>
    /// <param name="state">The status update.</param>
    /// <param name="error">Any error.</param>
    public delegate void MatchStateHandler(MatchStatus state, MatchError error);

    /// <summary>
    /// Delegate for handling matchmaking query results.
    /// </summary>
    /// <param name="result">The result of the query, including the error status and matches that met the query.</param>
    public delegate void MatchmakingResultHandler(MatchmakingQueryResult result);

    /// <summary>
    /// Exposes Match API to application code.
    /// </summary>
    public class Facade
    {
        /// <summary>
        /// For subscribing to connectivity events.
        /// </summary>
        private readonly INetworkConnectivityReporter networkConnectivityReporter;

        /// <summary>
        /// For subscribing to connectivity events.
        /// </summary>
        private readonly IPeerConnectionNotifier connectionNotifier;

        /// <summary>
        /// The matchmaker.
        /// </summary>
        private readonly IMatchmaker matchmaker;

        /// <summary>
        /// Service for sending messages between peers in a match.
        /// </summary>
        private readonly IMatchProtocol matchMessagingService;

        /// <summary>
        /// For allocating IDs for use in matches.
        /// </summary>
        private readonly IBadumnaIdAllocator identifierAllocator;

        /// <summary>
        /// For queueing functions to run on the application queue.
        /// </summary>
        private readonly QueueInvokable queueApplicationEvent;

        /// <summary>
        /// For pushing work to be done on network thread.
        /// </summary>
        private readonly INetworkEventScheduler networkEventQueue;

        /// <summary>
        /// Options specifying server address.
        /// </summary>
        private readonly MatchmakingModule options;

        /// <summary>
        /// Provides access to current time.
        /// </summary>
        private readonly ITime timeKeeper;

        /// <summary>
        /// Live membership managers that need regular ticking.
        /// </summary>
        private readonly List<MembershipManager> membershipManagers =
            new List<MembershipManager>();

        /// <summary>
        /// Match entity manager.
        /// </summary>
        private readonly EntityManager entityManager;

        /// <summary>
        /// For supporting autoreplication.
        /// </summary>
        private readonly MatchAutoreplicationManager autoreplicationManager;

        /// <summary>
        /// Initializes a new instance of the Facade class.
        /// </summary>
        /// <param name="networkConnectivityReporter">For network connectivity notifications.</param>
        /// <param name="peerConnectionNotifier">For subscribing to peer connectivity events.</param>
        /// <param name="transport">The transport protocol.</param>
        /// <param name="addressProvider">Dependency of ID allocator.</param>
        /// <param name="connectivityReporter">2nd dependency of ID allocator.</param>
        /// <param name="queueApplicationEvent">Delegate for queueing to the application queue.</param>
        /// <param name="networkEventQueue">For pushing work to be done on the network thread.</param>
        /// <param name="options">Configuration options.</param>
        /// <param name="timeKeeper">Provides access to the current time.</param>
        /// <param name="entityManager">Match entity manager.</param>
        /// <param name="autoreplicationManager">For supporting autoreplication.</param>
        internal Facade(
            INetworkConnectivityReporter networkConnectivityReporter,
            IPeerConnectionNotifier peerConnectionNotifier,
            ITransportProtocol transport,
            INetworkAddressProvider addressProvider,
            INetworkConnectivityReporter connectivityReporter,
            QueueInvokable queueApplicationEvent,
            INetworkEventScheduler networkEventQueue,
            Options options,
            ITime timeKeeper,
            EntityManager entityManager,
            MatchAutoreplicationManager autoreplicationManager)
            : this(
                networkConnectivityReporter,
                peerConnectionNotifier,
                new Matchmaker(timeKeeper, networkEventQueue, transport, options),
                new MatchMessagingService(timeKeeper, transport, entityManager),
                new BadumnaIdAllocator(addressProvider, connectivityReporter, new RandomNumberGenerator()),
                queueApplicationEvent,
                networkEventQueue,
                options.Matchmaking,
                timeKeeper,
                entityManager,
                autoreplicationManager)
        {
        }

        /// <summary>
        /// Initializes a new instance of the Facade class.
        /// </summary>
        /// <param name="networkConnectivityReporter">For network connectivity notifications.</param>
        /// <param name="peerConnectionNotifier">For subscribing to peer connectivity events.</param>
        /// <param name="matchmaker">The matchmaker.</param>
        /// <param name="matchMessagingService">The match messaging service.</param>
        /// <param name="identifierAllocator">For allocating IDs to members.</param>
        /// <param name="queueApplicationEvent">For queuing events on the application thread.</param>
        /// <param name="networkEventQueue">For pushing work to be done on the network thread.</param>
        /// <param name="optionsModule">Matchmaking options module.</param>
        /// <param name="timeKeeper">Time keeper providing access to current time.</param>
        /// <param name="entityManager">Match entity manager.</param>
        /// <param name="autoreplicationManager">For supporting autoreplication.</param>
        internal Facade(
            INetworkConnectivityReporter networkConnectivityReporter,
            IPeerConnectionNotifier peerConnectionNotifier,
            IMatchmaker matchmaker,
            IMatchProtocol matchMessagingService,
            IBadumnaIdAllocator identifierAllocator,
            QueueInvokable queueApplicationEvent,
            INetworkEventScheduler networkEventQueue,
            MatchmakingModule optionsModule,
            ITime timeKeeper,
            EntityManager entityManager,
            MatchAutoreplicationManager autoreplicationManager)
        {
            this.networkConnectivityReporter = networkConnectivityReporter;
            this.connectionNotifier = peerConnectionNotifier;
            this.matchmaker = matchmaker;
            this.matchMessagingService = matchMessagingService;
            this.identifierAllocator = identifierAllocator;
            this.queueApplicationEvent = queueApplicationEvent;
            this.networkEventQueue = networkEventQueue;
            this.options = optionsModule;
            this.timeKeeper = timeKeeper;
            this.entityManager = entityManager;
            this.autoreplicationManager = autoreplicationManager;
        }

        /// <summary>
        /// Create a match as host.
        /// </summary>
        /// <param name="criteria">Matchmaking criteria that will be published to matchmaking server.</param>
        /// <param name="maxPlayers">The maximum number of players permitted in the match.</param>
        /// <param name="playerName">A name for the local player in the match (uniqueness not enforced).</param>
        /// <param name="createReplicaDelegate">Called when a new entity needs to be instantiated into the match.</param>
        /// <param name="removeReplicaDelegate">Called when an entity leave the match.</param>
        /// <returns>A new match.</returns>
        public Match CreateMatch(
            MatchmakingCriteria criteria, 
            int maxPlayers,
            string playerName,
            CreateReplica createReplicaDelegate,
            RemoveReplica removeReplicaDelegate)
        {
            this.ThrowIfNoServerAddress();

            var memberIdentity = new MemberIdentity(
                this.identifierAllocator.GetNextUniqueId(),
                playerName);
            return new Match(
                this.connectionNotifier,
                new HostAuthorizationCertificate(memberIdentity.ID, memberIdentity.ID, TimeSpan.Zero),
                criteria,
                maxPlayers,
                memberIdentity,
                this.options.ServerPeerAddress,
                this.timeKeeper,
                this.matchmaker,
                this.matchMessagingService,
                this.entityManager,
                createReplicaDelegate,
                removeReplicaDelegate,
                this.networkEventQueue,
                this.queueApplicationEvent,
                this.networkConnectivityReporter,
                this.autoreplicationManager);
        }

        /// <summary>
        /// Create a private match as host.
        /// </summary>
        /// <remarks>
        /// Private matches are only included in <see cref="FindMatches"/> results
        /// if their name was specified in the search criteria.
        /// </remarks>
        /// <param name="criteria">Matchmaking criteria that will be published to matchmaking server.</param>
        /// <param name="maxPlayers">The maximum number of players permitted in the match.</param>
        /// <param name="playerName">A name for the local player in the match (uniqueness not enforced).</param>
        /// <param name="createReplicaDelegate">Called when a new entity needs to be instantiated into the match.</param>
        /// <param name="removeReplicaDelegate">Called when an entity leave the match.</param>
        /// <returns>A new match.</returns>
        public Match CreatePrivateMatch(
            MatchmakingCriteria criteria,
            int maxPlayers,
            string playerName,
            CreateReplica createReplicaDelegate,
            RemoveReplica removeReplicaDelegate)
        {
            this.ThrowIfNoServerAddress();

            if (string.IsNullOrEmpty(criteria.MatchName))
            {
                throw new ArgumentException("Private matches must specify MatchName", "criteria");
            }

            criteria.IsPrivate = true;
            return this.CreateMatch(
                criteria,
                maxPlayers,
                playerName,
                createReplicaDelegate,
                removeReplicaDelegate);
        }

        /// <summary>
        /// Join a known match.
        /// </summary>
        /// <param name="matchmakingResult">A match obtained by querying the matchmaking server.</param>
        /// <param name="playerName">A name for the local player in the match (uniqueness not enforced).</param>
        /// <param name="createReplicaDelegate">Called when a new entity needs to be instantiated into the match.</param>
        /// <param name="removeReplicaDelegate">Called when an entity leave the match.</param>
        /// <returns>A match object for managing the match.</returns>
        public Match JoinMatch(
            MatchmakingResult matchmakingResult,
            string playerName,
            CreateReplica createReplicaDelegate,
            RemoveReplica removeReplicaDelegate)
        {
            this.ThrowIfNoServerAddress();

            var memberIdentity = new MemberIdentity(
                this.identifierAllocator.GetNextUniqueId(),
                playerName);
            return new Match(
                this.connectionNotifier,
                matchmakingResult.Certificate,
                matchmakingResult.Criteria,
                matchmakingResult.Capacity.NumSlots,
                memberIdentity,
                this.options.ServerPeerAddress,
                this.timeKeeper,
                this.matchmaker,
                this.matchMessagingService,
                this.entityManager,
                createReplicaDelegate,
                removeReplicaDelegate,
                this.networkEventQueue,
                this.queueApplicationEvent,
                this.networkConnectivityReporter,
                this.autoreplicationManager);
        }

        /// <summary>
        /// Create a match as host.
        /// </summary>
        /// <typeparam name="T">The type of the match controller.</typeparam>
        /// <param name="controller">The object to use as match controller.</param>
        /// <param name="criteria">Matchmaking criteria that will be published to matchmaking server.</param>
        /// <param name="maxPlayers">The maximum number of players permitted in the match.</param>
        /// <param name="playerName">A name for the local player in the match (uniqueness not enforced).</param>
        /// <param name="createReplicaDelegate">Called when a new entity needs to be instantiated into the match.</param>
        /// <param name="removeReplicaDelegate">Called when an entity leave the match.</param>
        /// <returns>A new match.</returns>
        public Match<T> CreateMatch<T>(
            T controller,
            MatchmakingCriteria criteria,
            int maxPlayers,
            string playerName,
            CreateReplica createReplicaDelegate,
            RemoveReplica removeReplicaDelegate)
        {
            this.ThrowIfNoServerAddress();

            var memberIdentity = new MemberIdentity(
                this.identifierAllocator.GetNextUniqueId(),
                playerName);
            return new Match<T>(
                this,
                this.connectionNotifier,
                new HostAuthorizationCertificate(memberIdentity.ID, memberIdentity.ID, TimeSpan.Zero),
                criteria,
                maxPlayers,
                memberIdentity,
                this.options.ServerPeerAddress,
                this.timeKeeper,
                this.matchmaker,
                this.matchMessagingService,
                this.entityManager,
                createReplicaDelegate,
                removeReplicaDelegate,
                this.networkEventQueue,
                this.queueApplicationEvent,
                this.networkConnectivityReporter,
                this.autoreplicationManager,
                controller);
        }

        /// <summary>
        /// Create a private match as host.
        /// </summary>
        /// <remarks>
        /// Private matches are only included in <see cref="FindMatches"/> results
        /// if their name was specified in the search criteria.
        /// </remarks>
        /// <typeparam name="T">The type of the match controller.</typeparam>
        /// <param name="controller">The object to use as match controller.</param>
        /// <param name="criteria">Matchmaking criteria that will be published to matchmaking server.</param>
        /// <param name="maxPlayers">The maximum number of players permitted in the match.</param>
        /// <param name="playerName">A name for the local player in the match (uniqueness not enforced).</param>
        /// <param name="createReplicaDelegate">Called when a new entity needs to be instantiated into the match.</param>
        /// <param name="removeReplicaDelegate">Called when an entity leave the match.</param>
        /// <returns>A new match.</returns>
        public Match<T> CreatePrivateMatch<T>(
            T controller,
            MatchmakingCriteria criteria,
            int maxPlayers,
            string playerName,
            CreateReplica createReplicaDelegate,
            RemoveReplica removeReplicaDelegate)
        {
            this.ThrowIfNoServerAddress();

            if (string.IsNullOrEmpty(criteria.MatchName))
            {
                throw new ArgumentException("Private matches must specify MatchName", "criteria");
            }

            criteria.IsPrivate = true;
            return this.CreateMatch<T>(
                controller,
                criteria,
                maxPlayers,
                playerName,
                createReplicaDelegate,
                removeReplicaDelegate);
        }

        /// <summary>
        /// Join a known match.
        /// </summary>
        /// <typeparam name="T">The type of the match controller.</typeparam>
        /// <param name="controller">The object to use as match controller.</param>
        /// <param name="matchmakingResult">A match obtained by querying the matchmaking server.</param>
        /// <param name="playerName">A name for the local player in the match (uniqueness not enforced).</param>
        /// <param name="createReplicaDelegate">Called when a new entity needs to be instantiated into the match.</param>
        /// <param name="removeReplicaDelegate">Called when an entity leave the match.</param>
        /// <returns>A match object for managing the match.</returns>
        public Match<T> JoinMatch<T>(
            T controller,
            MatchmakingResult matchmakingResult,
            string playerName,
            CreateReplica createReplicaDelegate,
            RemoveReplica removeReplicaDelegate)
        {
            this.ThrowIfNoServerAddress();

            var memberIdentity = new MemberIdentity(
                this.identifierAllocator.GetNextUniqueId(),
                playerName);
            return new Match<T>(
                this,
                this.connectionNotifier,
                matchmakingResult.Certificate,
                matchmakingResult.Criteria,
                matchmakingResult.Capacity.NumSlots,
                memberIdentity,
                this.options.ServerPeerAddress,
                this.timeKeeper,
                this.matchmaker,
                this.matchMessagingService,
                this.entityManager,
                createReplicaDelegate,
                removeReplicaDelegate,
                this.networkEventQueue,
                this.queueApplicationEvent,
                this.networkConnectivityReporter,
                this.autoreplicationManager,
                controller);
        }

        /// <summary>
        /// Query the matchmaking server to find matches available to join.
        /// </summary>
        /// <remarks>
        /// Note that matches hosted on the local peer will not be included in the query results.
        /// Additionally, when making multiple sequential find match queries, the order of the results is not guaranteed to
        /// be consistent from one query to the next.  If a consistent ordering is required then the results should
        /// be sorted.
        /// </remarks>
        /// <param name="criteria">Matchmaking criteria.</param>
        /// <param name="resultHandler">A delegate for handling the matchmaking query result.</param>
        public void FindMatches(
            MatchmakingCriteria criteria,
            MatchmakingResultHandler resultHandler)
        {
            this.ThrowIfNoServerAddress();

            if (resultHandler == null)
            {
                throw new ArgumentNullException("resultHandler");
            }

            var qae = this.queueApplicationEvent;  // Local to make JSC happy.

            this.matchmaker.MatchQuery(
                criteria,
                results => qae(Apply.Func(() => resultHandler(new MatchmakingQueryResult(MatchError.None, results)))),
                error => qae(Apply.Func(() => resultHandler(new MatchmakingQueryResult(error, new List<MatchmakingResult>())))));
        }

        /// <summary>
        /// A helper to check that the server address is set.
        /// </summary>
        /// <remarks>
        /// This is excluded from obfuscation to avoid having an obfuscated method name
        /// on the call stack when the exception is thrown.
        /// </remarks>
        [Obfuscation(Exclude = true)]
        private void ThrowIfNoServerAddress()
        {
            if (!this.options.ServerPeerAddress.IsValid)
            {
                throw new ConfigurationException(
                    ConfigurationException.ErrorCode.InvalidMatchmakingServerAddress,
                    "No matchmaking server address was specified, no port was specified, or the specified address could not be resolved.");
            }
        }
    }
}
