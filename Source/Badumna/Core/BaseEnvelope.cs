//-----------------------------------------------------------------------
// <copyright file="BaseEnvelope.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2010 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.Core
{
    using System;
    using System.Diagnostics;
    using Badumna.Security;
    using Badumna.Utilities;

    /// <summary>
    /// An envelope for holding a message and associated data.
    /// <remarks>Responsibilities include encrypting and decrypting the message, and specifying
    /// the Quality of Service the message should be sent with.</remarks>
    /// </summary>
    ////TODO: (tim) implement IDisposable since we wrap an IDisposable MessageBuffer
    internal class BaseEnvelope : IEnvelope
    {
        /// <summary>
        /// Quality of serivce the message will be sent with.
        /// </summary>
        private QualityOfService qos = new QualityOfService();

        /// <summary>
        /// A label for the envelope (TO DO: for debug only?).
        /// </summary>
        private string label = string.Empty;
        
        /// <summary>
        /// The message held by the envelope.
        /// </summary>
        private MessageBuffer message = new MessageBuffer();
        
        /// <summary>
        /// A value indicating whether the envelope is currently valid.
        /// </summary>
        private bool isValid = false;

        /// <summary>
        /// A value indicating if the message (TO DO :should 
        /// </summary>
        private bool isEncrypted = false;

        /// <summary>
        /// Initializes a new instance of the BaseEnvelope class.
        /// </summary>
        public BaseEnvelope()
        {
        }

        /// <summary>
        /// Initializes a new instance of the BaseEnvelope class (copy constructor).
        /// </summary>
        /// <param name="copy">An existing envelope to copy.</param>
        public BaseEnvelope(BaseEnvelope copy)
            : this(copy.message, copy.isEncrypted)
        {
            this.isValid = copy.isValid;
            this.qos = copy.qos;
            
            // Following method conditional on TRACE.
            this.SetLabel(copy.label);
        }

        /// <summary>
        /// Initializes a new instance of the BaseEnvelope class.
        /// </summary>
        /// <param name="qos">The quality of service for the envelope's message to be sent with.</param>
        public BaseEnvelope(QualityOfService qos)
        {
            this.qos = qos;
        }

        /// <summary>
        /// Initializes a new instance of the BaseEnvelope class.
        /// </summary>
        /// <param name="message">The message to hold in the envelope.</param>
        /// <param name="isEncrypted">A value indicating if the message is encrypted.</param>
        internal BaseEnvelope(MessageBuffer message, bool isEncrypted)
        {
            this.message = new MessageBuffer(message);
            this.isEncrypted = isEncrypted;
        }

        /// <summary>
        /// Gets or sets the message held.
        /// </summary>
        public MessageBuffer Message
        {
            get { return this.message; }
            set { this.message = value; }
        }

        /// <summary>
        /// Gets the number of bytes available to be read from the message.
        /// </summary>
        public int Available
        {
            get
            {
                return null != this.message ? this.message.Available : 0;
            }
        }
        
        /// <summary>
        /// Gets the length of the message.
        /// </summary>
        public int Length
        {
            get
            {
                return null != this.message ? this.message.Length : 0;
            }
        }

        /// <summary>
        /// Gets or sets the quality of service the message should be sent with.
        /// </summary>
        public QualityOfService Qos
        {
            get { return this.qos; }
            set { this.qos = value; }
        }

        /// <summary>
        /// Gets a value indicating whether the envelope is valid.
        /// </summary>
        public bool IsValid
        {
            get
            {
                return this.isValid;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the message is encrypted.
        /// </summary>
        public bool IsEncrypted
        {
            get { return this.isEncrypted; }
            set { this.isEncrypted = true; }
        }

        /// <summary>
        /// Gets the envelope's label.
        /// </summary>
        public string Label
        {
            get { return this.label; }
        }

        /// <summary>
        /// Sets the envelope's label.
        /// </summary>
        /// <param name="label">The new label.</param>
        [Conditional("TRACE")]
        public void SetLabel(string label)
        {
            this.label = label;
        }

#if TRACE
        /// <summary>
        /// Appends text to the envelope's label.
        /// </summary>
        /// <param name="label">The text to append.</param>
        ////[Conditional("TRACE")]
        public void AppendLabel(string label)
        {
            this.label += label;
        }
#endif
        /// <summary>
        /// Reset the message to be read from the begining.
        /// </summary>
        public void ResetParseOffset()
        {
            this.Message.ReadOffset = 0;
        }

        /// <summary>
        /// Create an encrypted copy of the message in the envelope.
        /// </summary>
        /// <param name="key">The encryption key.</param>
        /// <param name="salt">The encrytption salt.</param>
        /// <returns>The encrypted message.</returns>
        public MessageBuffer Encrypt(SymmetricKey key, byte[] salt)
        {
            if (this.isEncrypted)
            {
                return null;
            }

            try
            {
                MessageBuffer encryptedBuffer = new MessageBuffer(this.message);
                encryptedBuffer.Checksum(salt, key.Key);
                if (Parameters.EncryptTraffic)
                {
                    encryptedBuffer.Encrypt(key);
                }

                return encryptedBuffer;
            }
            catch (Exception e)
            {
                Logger.TraceException(LogLevel.Warning, e, "Failed to encrypt message");
            }

            return null;
        }

        /// <summary>
        /// Decrypt the message held in the envelope.
        /// </summary>
        /// <param name="key">The encryption key.</param>
        /// <param name="salt">The encryption salt.</param>
        /// <returns><c>true</c> if the message was successfully decrypted, otherwise <c>false</c>.</returns>
        public bool Decrypt(SymmetricKey key, byte[] salt)
        {
            if (!this.isEncrypted)
            {
                Debug.Assert(false, "Message has already been decrypted.");
                Logger.TraceInformation(LogTag.Core, "Message has already been decrypted.");
                return true;
            }

            try
            {
                bool encryptTraffic = Parameters.EncryptTraffic;
                MessageBuffer decryptedMessage;
                if (encryptTraffic)
                {
                    decryptedMessage = new MessageBuffer(key.Decrypt(this.message.GetBuffer()));
                }
                else
                {
                    decryptedMessage = new MessageBuffer(this.message.GetBuffer());
                }

                int prechecksumLength = decryptedMessage.Length;
                if (prechecksumLength == 0)
                {
                    this.isEncrypted = false;
                    return true; // Hole punch messages are empty - no need to check checksum.
                }

                if (decryptedMessage.ValidateChecksum(salt, key.Key))
                {
                    Logger.RecordStatistic("Transport", "ChecksumOverheadBytesReceived", prechecksumLength - decryptedMessage.Length);
                    Logger.RecordStatistic("Transport", "TotalValidBytesReceived", decryptedMessage.Length);

                    this.message = decryptedMessage;
                    this.isEncrypted = false;
                    return true;
                }
            }
            catch (System.Security.Cryptography.CryptographicException e)
            {
                Logger.TraceInformation(LogTag.Core, "failed to decrypt the message: {0}", e.Message);
                return false;
            }

            return false;
        }

        #region IParseable Members

        /// <inheritdoc/>
        public virtual void ToMessage(MessageBuffer message, Type parameterType)
        {
            if (null != message)
            {
                message.Write(this.message);
                message.Write(this.qos);
            }
        }

        public virtual void FromMessage(MessageBuffer message)
        {
            if (null != message)
            {
                this.message.FromMessage(message);
                this.qos.FromMessage(message);
            }
        }

        #endregion

        /// <summary>
        /// Mark the envelope as valid or invalid.
        /// </summary>
        /// <param name="flag"><c>true</c> to mark the envelope as valid,
        /// or <c>false</c> to mark the envelope as invalid.</param>
        internal void Validate(bool flag)
        {
            this.isValid = flag;
        }
    }
}
