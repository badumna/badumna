﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

using Badumna.Transport;
using Badumna.Utilities;

namespace Badumna.Core
{
    /// <summary>
    /// A Stream implementation used to read data from message packets.
    /// </summary>
    public class MessageReadStream : MessageStream
    {
        /// <summary>
        /// Gets a value indicating whether the current stream supports reading. 
        /// </summary>
        public override bool CanRead { get { return true; } }

        /// <summary>
        /// Gets or sets the position within the current stream. 
        /// </summary>
        public override long Position
        {
            get { return this.Message.ReadOffset; }
            set { this.Message.ReadOffset = this.OriginalPosition + (int)value; }
        }

        internal MessageReadStream(BaseEnvelope envelope)
            : base(envelope.Message, envelope.Message.ReadOffset)
        {
        }

        internal MessageReadStream(byte[] data)
            : base(new MessageBuffer(data), 0)
        {
        }

        /// <summary>
        /// Reads a sequence of bytes from the current stream and advances the position within the stream by the number of bytes read. 
        /// </summary>
        /// <param name="buffer">An array of bytes. When this method returns, the buffer contains the specified byte array with the values between offset and (offset + count - 1) replaced by the bytes read from the current source. </param>
        /// <param name="offset">The zero-based byte offset in buffer at which to begin storing the data read from the current stream. </param>
        /// <param name="count">The maximum number of bytes to be read from the current stream. </param>
        /// <returns></returns>
        public override int Read(byte[] buffer, int offset, int count)
        {
            return this.Message.ReadBytes(buffer, count);
        }
    }
}
