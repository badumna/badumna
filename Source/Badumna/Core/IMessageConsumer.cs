﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Badumna.Core
{
    interface IMessageConsumer<EnvelopeType> where EnvelopeType : BaseEnvelope
    {
        void ProcessMessage(EnvelopeType envelope);
    }
}
