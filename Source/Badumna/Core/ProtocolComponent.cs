﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.Runtime.Serialization;

using Badumna.Utilities;

namespace Badumna.Core
{
    delegate void DispatchMessageDelegate<EnvelopeType>(EnvelopeType messageEnvelope) where EnvelopeType : IEnvelope;

    class ProtocolComponent<EnvelopeType> : IProtocolComponent<EnvelopeType> where EnvelopeType : IEnvelope
    {
        private MessageParser mParser;
                
        public MessageParser Parser { get { return this.mParser; } }

        private DispatchMessageDelegate<EnvelopeType> mDispatcherMethod;
        public DispatchMessageDelegate<EnvelopeType> DispatcherMethod 
        { 
            set { this.mDispatcherMethod = value; }
            get { return this.mDispatcherMethod; }
        }

        private IProtocolComponent<EnvelopeType> mParent;  // Kind of more like a controlling sibling

        protected EnvelopeType mCurrentEnvelope;

        public ProtocolComponent(string layerName, Type protocolAttributeType, GenericCallBackReturn<object, Type> factory)
        {
            this.mParser = new MessageParser(layerName, protocolAttributeType, factory);
            this.mParser.RegisterMethodsIn(this);
        }

        public ProtocolComponent(MessageParser messageParser)
        {
            this.mParser = messageParser;
            this.mParser.RegisterMethodsIn(this);
        }

        public ProtocolComponent(IProtocolComponent<EnvelopeType> parent)
        {
            if (parent == null)
            {
                Debug.Assert(false, "ProtocolComponent constructor got null parent");
                throw new ArgumentNullException("parent");
            }

            this.mParent = parent;
            this.mParser = parent.Parser;
            this.mDispatcherMethod = parent.DispatcherMethod;
            this.mParser.RegisterMethodsIn(this);
        }

        public void ForgeMethodList()
        {
            this.mParser.ForgeMethodList();
        }

        public void SendMessage(EnvelopeType envelope)
        {
            if (this.mParent != null)
            {
                if (this.mParent.DispatcherMethod != null)
                {
                    this.mParent.DispatcherMethod(envelope);
                }
                else
                {
                    Logger.TraceInformation(LogTag.Core, "No dispatcher specified. Please set DispatcherMethod.");
                }
            }
            else
            {
                Debug.Assert(this.mDispatcherMethod != null, "No dispatcher specified. Please set DispatcherMethod.");
                if (this.mDispatcherMethod != null)
                {
                    this.mDispatcherMethod(envelope);
                }
                else
                {
                    Logger.TraceInformation(LogTag.Core, "No dispatcher specified. Please set DispatcherMethod.");
                }
            }
        }

        public virtual void PrepareMessageForDeparture(ref EnvelopeType envelope)
        {
            if (this.mParent != null)
            {
                this.mParent.PrepareMessageForDeparture(ref envelope);
            }
        }

        public EnvelopeType CurrentEnvelope
        {
            get
            {
                if (this.mCurrentEnvelope != null)
                {
                    return this.mCurrentEnvelope;
                }
                if (this.mParent != null)
                {
                    return this.mParent.CurrentEnvelope;
                }
                return default(EnvelopeType);
            }
        }

        public void RemoteCall(IEnvelope envelope, GenericCallBack function)
        {
#if TRACE
            envelope.AppendLabel(function.Method.Name + " ");
#endif
            this.mParser.RemoteCall(envelope, function);
        }

        public void RemoteCall<A>(IEnvelope envelope, GenericCallBack<A> function, A a)
        {
#if TRACE
            envelope.AppendLabel(function.Method.Name + " ");
#endif
            this.mParser.RemoteCall(envelope, function, a);
        }

        public void RemoteCall<A, B>(IEnvelope envelope, GenericCallBack<A, B> function, A a, B b)
        {
#if TRACE
            envelope.AppendLabel(function.Method.Name + " ");
#endif
            this.mParser.RemoteCall(envelope, function, a, b);
        }

        public void RemoteCall<A, B, C>(IEnvelope envelope, GenericCallBack<A, B, C> function, A a, B b, C c)
        {
#if TRACE
            envelope.AppendLabel(function.Method.Name + " ");
#endif
            this.mParser.RemoteCall(envelope, function, a, b, c);
        }

        public void RemoteCall<A, B, C, D>(IEnvelope envelope, GenericCallBack<A, B, C, D> function, A a, B b, C c, D d)
        {
#if TRACE
            envelope.AppendLabel(function.Method.Name + " ");
#endif
            this.mParser.RemoteCall(envelope, function, a, b, c, d);
        }

        public void RemoteCall<A, B, C, D, E>(IEnvelope envelope, GenericCallBack<A, B, C, D, E> function, A a, B b, C c, D d, E e)
        {
#if TRACE
            envelope.AppendLabel(function.Method.Name + " ");
#endif
            this.mParser.RemoteCall(envelope, function, a, b, c, d, e);
        }

        public void RemoteCall<A, B, C, D, E, F>(IEnvelope envelope, GenericCallBack<A, B, C, D, E, F> function, A a, B b, C c, D d, E e, F f)
        {
#if TRACE
            envelope.AppendLabel(function.Method.Name + " ");
#endif
            this.mParser.RemoteCall(envelope, function, a, b, c, d, e, f);
        }

        public void ParseMessage(EnvelopeType envelope, int offset)
        {
            if (null == envelope.Message)
            {
                return;
            }

            this.mCurrentEnvelope = envelope;
            try
            {
                envelope.Message.ReadOffset = offset;
                this.mParser.Parse(envelope);
            }
            catch (ParseException e)
            {
                e.TraceMessage();

                this.DumpEnvelope(envelope);

                Debug.Assert(false, e.Message);
            }
            catch (BadumnaException e)
            {
                e.TraceMessage();
            }
            catch (Exception e)
            {
                Logger.TraceException(LogLevel.Error, e, "Message handling failed");
            }
            finally
            {
                this.mCurrentEnvelope = default(EnvelopeType);
            }
        }

        public void Traverse(ProtocolStackCrawler crawler)
        {
            crawler.VisitLayer(this);
            this.Parser.Traverse(crawler);
            crawler.LeaveLayer(this);
        }

        /// <inbheritdoc/>
        public void RegisterMethodsIn(object obj)
        {
            this.Parser.RegisterMethodsIn(obj);
        }

        [Conditional("DEBUG")]
        private void DumpEnvelope(EnvelopeType envelope)
        {
            try
            {
                using (System.IO.FileStream stream = new System.IO.FileStream("ParseError.dat", System.IO.FileMode.OpenOrCreate, System.IO.FileAccess.Write))
                {
                    stream.Write(envelope.Message.GetBuffer(), 0, envelope.Length);
                }
            }
            catch
            {
            }
        }
    }

    [Serializable]
    class ProtocolException : CoreException
    {
        public ProtocolException()
            : base(Resources.EndOfMessageException_Message) { }

        public ProtocolException(String message)
            : base(message) { }

        public ProtocolException(String message, Exception innerException)
            : base(message, innerException) { }

        protected ProtocolException(SerializationInfo serializationInfo, StreamingContext streamingContext)
            : base(serializationInfo, streamingContext) { }
    }
}
