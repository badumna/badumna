﻿//-----------------------------------------------------------------------
// <copyright file="INetworkAddressProvider.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2010 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.Core
{
    using System;
    using System.Collections.Generic;
    using Badumna.Utilities;

    /// <summary>
    /// Interface for providing network address information.
    /// </summary>
    internal interface INetworkAddressProvider
    {
        /// <summary>
        /// Provides notification when the public address changes.
        /// </summary>
        event GenericCallBack PublicAddressChanged;

        /// <summary>
        /// Gets a value indicating whether a valid public address is known.
        /// </summary>
        bool IsAddressKnownAndValid { get; }

        /// <summary>
        /// Gets the public address, the address advertised to remote peers for connecting to this peer.
        /// </summary>
        PeerAddress PublicAddress { get; }

        /// <summary>
        /// Gets the private port being used (or -1 if there is no private address).
        /// </summary>
        int PrivatePort { get; }

        /// <summary>
        /// Gets a list of the private addresses.
        /// </summary>
        IEnumerable<PeerAddress> PrivateAddresses { get; }

        /// <summary>
        /// Determines whether a specified private address is used.
        /// </summary>
        /// <param name="address">The private address to check for.</param>
        /// <returns><c>true</c> if the specified private address is used by the peer, otherwise <c>false</c>.</returns>
        bool HasPrivateAddress(PeerAddress address);
    }
}
