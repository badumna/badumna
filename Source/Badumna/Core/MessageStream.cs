﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

using Badumna.Transport;
using Badumna.Utilities;

namespace Badumna.Core
{
    /// <summary>
    /// A stream implementation used for reading and writing to message packets.
    /// </summary>
    public abstract class MessageStream : Stream
    {
        private int mOriginalPosition;
        internal int OriginalPosition { get { return this.mOriginalPosition; } }

        private MessageBuffer mMessage;
        internal MessageBuffer Message { get { return this.mMessage; } }

        private SeekOrigin mCurrentSeekOrigin = SeekOrigin.Begin; // Must never be SeekOrigin.Current;

        internal MessageStream(MessageBuffer message, int originalPosition)
        {
            this.mMessage = message;
            this.mOriginalPosition = originalPosition;
        }

        /// <summary>
        /// Gets a value indicating whether the current stream supports reading. 
        /// </summary>
        public override bool CanRead { get { return false; } }

        /// <summary>
        /// Gets a value indicating whether the current stream supports seeking. 
        /// </summary>
        public override bool CanSeek { get { return true; } }

        /// <summary>
        /// Gets a value indicating whether the current stream supports writing. 
        /// </summary>
        public override bool CanWrite { get { return false; } }
        
        /// <summary>
        /// Gets the length in bytes of the stream.
        /// </summary>
        public override long Length { get { return this.mMessage.Length - this.mOriginalPosition; } }

        /// <summary>
        /// Clears all buffers for this stream and causes any buffered data to be written to the underlying device.
        /// </summary>
        public override void Flush()
        {
        }

        /// <summary>
        /// Sets the position within the current stream. 
        /// </summary>
        /// <param name="offset">A byte offset relative to the origin parameter. </param>
        /// <param name="origin">A value of type SeekOrigin indicating the reference point used to obtain the new position. </param>
        /// <returns>The new position within the current stream. </returns>
        public override long Seek(long offset, SeekOrigin origin)
        {
            switch (origin)
            {
                case SeekOrigin.Begin:
                    this.Position = offset;
                    this.mCurrentSeekOrigin = SeekOrigin.Begin;
                    break;

                case SeekOrigin.End:
                    this.Position = Math.Max(this.mOriginalPosition, this.mMessage.Length - offset);
                    this.mCurrentSeekOrigin = SeekOrigin.End;
                    break;

                default:
                    return this.Seek(offset, this.mCurrentSeekOrigin);
            }

            return this.Position;
        }

        /// <summary>
        /// Length cannot be set. It is allocated as necessary.
        /// </summary>
        /// <param name="value"></param>
        public override void SetLength(long value)
        {
        }

        /// <summary>
        /// Reads a sequence of bytes from the current stream and advances the position within the stream by the number of bytes read. 
        /// </summary>
        /// <param name="buffer">An array of bytes. When this method returns, the buffer contains the specified byte array with the values between offset and (offset + count - 1) replaced by the bytes read from the current source. </param>
        /// <param name="offset">The zero-based byte offset in buffer at which to begin storing the data read from the current stream. </param>
        /// <param name="count">The maximum number of bytes to be read from the current stream. </param>
        /// <returns></returns>
        public override int Read(byte[] buffer, int offset, int count)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Writes a sequence of bytes to the current stream and advances the current position within this stream by the number of bytes written. 
        /// </summary>
        /// <param name="buffer">An array of bytes. This method copies count bytes from buffer to the current stream. </param>
        /// <param name="offset">The zero-based byte offset in buffer at which to begin copying bytes to the current stream. </param>
        /// <param name="count">The number of bytes to be written to the current stream. </param>
        public override void Write(byte[] buffer, int offset, int count)
        {
            throw new NotImplementedException();
        }
    }
}
