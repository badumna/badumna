//------------------------------------------------------------------------------
// <copyright file="ChecksumSalter.cs" company="National ICT Australia Limited">
//     Copyright (c) 2012 National ICT Australia Limited. All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System;
using System.Security.Cryptography;
using System.Text;

namespace Badumna.Core
{
    /// <summary>
    /// Produces checksum salts that should cause checksums to fail if messages are sent to the wrong peer or
    /// sent to a peer running a different application or badumna version.
    /// </summary>
    internal class ChecksumSalter
    {
        /// <summary>
        /// The protocol hash, a function of the default protocol hash, the application name, and whether we're obfuscated.
        /// TODO: Change default protocol hash to be a simple version number seeing as we now change it manually.
        /// TODO: Ensure the calculation of the protocol hash isn't likely to cause collisions between different applications / versions.
        /// </summary>
        private byte[] protocolHash;

        /// <summary>
        /// Initializes a new instance of the ChecksumSalter class.
        /// </summary>
        /// <param name="applicationName">The application name</param>
        public ChecksumSalter(string applicationName)
        {
            applicationName = applicationName ?? "";

            byte[] applicationHash;
            using (MD5CryptoServiceProvider hasher = new MD5CryptoServiceProvider())
            {
                applicationHash = hasher.ComputeHash(Encoding.UTF8.GetBytes(applicationName));
            }

            this.protocolHash = Parameters.GetDefaultProtocolHash();

            for (int i = 0; i < this.protocolHash.Length && i < applicationHash.Length; i++)
            {
                this.protocolHash[i] ^= applicationHash[i];
            }
        }

        /// <summary>
        /// Gets the salt for messages sent to the given address.
        /// </summary>
        /// <param name="address">The public address of the destination</param>
        /// <returns>The corresponding salt</returns>
        public byte[] GetSalt(PeerAddress address)
        {
            byte[] addressBytes = address.GetAddressBytes();
            byte[] salt = new byte[this.protocolHash.Length + addressBytes.Length];
            this.protocolHash.CopyTo(salt, 0);
            addressBytes.CopyTo(salt, this.protocolHash.Length);
            return salt;
        }
    }
}
