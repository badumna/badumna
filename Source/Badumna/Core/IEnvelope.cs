﻿//-----------------------------------------------------------------------
// <copyright file="IEnvelope.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2010 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
 
namespace Badumna.Core
{
    using System;

    using Badumna.Transport;
    using System.Diagnostics;

    /// <summary>
    /// Interface for envelopes (TO DO: improve documentation).
    /// </summary>
    internal interface IEnvelope : IParseable, ISized
    {
        /// <summary>
        /// Gets a value indicating whether the envelope is valid.
        /// </summary>
        bool IsValid { get; }

        /// <summary>
        /// Gets the number of unread bytes available in the message. 
        /// </summary>
        int Available { get; }

        /// <summary>
        /// Gets or sets the quality of service specified in the message.
        /// </summary>
        QualityOfService Qos { get; set; }

        /// <summary>
        /// Gets the message held in the envelope.
        /// </summary>
        MessageBuffer Message { get; }

        /// <summary>
        /// Gets the envelope's label.
        /// </summary>
        string Label
        {
            // TO DO: see if this is still required when the #if's are sorted out.
            get; // Getter is not ifdef'd out because it needs to be present for the Logger calls at compile time (even though they're eliminated if TRACE isn't defined).
        }

#if TRACE
        /// <summary>
        /// Append text to the envelope's label.
        /// </summary>
        /// <param name="label">The text to append.</param>
        void AppendLabel(string label);
#endif
        /// <summary>
        /// Reset the read position to the beginning of the message.
        /// </summary>
        void ResetParseOffset();
    }
}
