/***************************************************************************
 *  File Name: NetworkEvent.cs
 *
 *  Copyright (C) 2007 All Rights Reserved.
 * 
 *  Written by
 *      Scott Douglas <scdougl@csse.unimelb.edu.au>
 ****************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.Threading;
using System.Reflection;

using Badumna.Utilities;

namespace Badumna.Core
{
#if DEBUG
    public  // required for simulator
#endif
    class NetworkEvent : IComparable, IComparable<NetworkEvent>, IEquatable<NetworkEvent>
    {
        protected TimeSpan mTime;
        public TimeSpan Time
        {
            get { return this.mTime; }
            set { this.mTime = value; }
        }

        private IInvokable mInvokable;

        private string mName;
        public virtual String Name
        {
            get
            {
                if (this.mName != null)
                {
                    return this.mName;
                }
                if (this.mInvokable != null)
                {
                    return this.mInvokable.ToString();
                }
                return "";
            }
            set
            {
                this.mName = value;
            }
        }

        private static int sNextGuid = 0;

        private int mGuid;
        public int Guid { get { return this.mGuid; } }

        private bool mIsActive = true;
        internal virtual bool IsActive
        {
            get { return this.mIsActive; }
            set { this.mIsActive = value; }
        }

        private bool mIsDiscardable = true;
        internal bool IsDiscardable
        {
            get { return this.mIsDiscardable; }
            set { this.mIsDiscardable = value; }
        }

        public NetworkEvent(NetworkEvent ev)
        {
            this.mInvokable = ev.mInvokable;
            this.mTime = ev.mTime;
            this.mGuid = ev.mGuid;
        }

        /// <summary>
        /// This constructor should only be used when deriving a new instance of NetworkEvent which overrides the 
        /// PerformAction() method.
        /// </summary>
        /// <param name="time"></param>
        public NetworkEvent(TimeSpan time)
            : this(time, null)
        {
        }

        public NetworkEvent(TimeSpan time, IInvokable invokable)
        {
            this.mGuid = NetworkEvent.sNextGuid;
            NetworkEvent.sNextGuid++;
            this.mTime = time;
            this.mInvokable = invokable;
        }

        public virtual void PerformAction()
        {
            if (this.mInvokable == null || !this.mIsActive)
            {
                return;
            }

            Logger.RecordStatistic("NetworkEvent", this.Name, 1.0);
            //Logger.Indent(0);
            this.mInvokable.Invoke();
        }

        public override string ToString()
        {
            return this.Name;
        }

        #region IComparable Members

        public int CompareTo(object obj)
        {
            NetworkEvent objAsEvent = obj as NetworkEvent;
            if (objAsEvent != null)
            {
                return this.CompareTo(objAsEvent);
            }

            throw new ArgumentException("object is not a NetworkEvent.");
        }

        #endregion

        #region IComparable<NetworkEvent> Members

        public int CompareTo(NetworkEvent other)
        {
            int timeComparison = this.mTime.CompareTo(other.mTime);
            if (timeComparison == 0)
            {
                return this.mGuid.CompareTo(other.mGuid);
            }

            return timeComparison;
        }

        #endregion

        #region IEquatable<NetworkEvent> Members

        public bool Equals(NetworkEvent other)
        {
            return this.mGuid == other.mGuid;
        }

        #endregion

    }
}
