//------------------------------------------------------------------------------
// <copyright file="IParseable.cs" company="National ICT Australia Limited">
//     Copyright (c) 2007,2012 National ICT Australia Limited. All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System;

namespace Badumna.Core
{
    /// <summary>
    /// An object that knows how to serialize and deserialize itself to a <see cref="MessageBuffer"/>.
    /// </summary>
    /// <remarks>
    /// Implementors of this interface must also have a parameterless constructor.  This
    /// constructor will be used when deserializing the object.
    /// </remarks>
    internal interface IParseable 
    {
        /// <summary>
        /// Serializes the object to the message.
        /// </summary>
        /// <param name="message">The message to serialize into</param>
        /// <param name="parameterType">The type to serialize as (unused? probably should not be used at any rate)</param>
        /// <remarks>
        /// TODO: Remove the 'parameterType' parameter.
        /// </remarks>
        void ToMessage(MessageBuffer message, Type parameterType);

        /// <summary>
        /// Deserializes the object from the message.
        /// </summary>
        /// <param name="message">The message to deserialize from</param>
        void FromMessage(MessageBuffer message);
    }
}
