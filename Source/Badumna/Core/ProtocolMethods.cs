﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Badumna.Core
{
    /// <summary>
    /// This attribute is used to mark methods as writeable/parseable to MessageParsers.
    /// The MessageParser.RegisterMethodsIn() method can be used to find and register all the marked 
    /// methods in the given class.
    /// </summary>
    [AttributeUsage(AttributeTargets.Method, Inherited = true, AllowMultiple = false)]
    abstract class ProtocolMethodAttribute : Attribute
    {
        private int mId;
        public int Id 
        { 
            get { return this.mId; }
            set { this.mId = value; }
        }

        public ProtocolMethodAttribute(int id)
        {
            this.mId = id;
        }
    }

    class ConnectionlessProtocolMethodAttribute : ProtocolMethodAttribute
    {
        public ConnectionlessProtocolMethodAttribute(ConnectionlessMethod method)
            : base((int)method)
        {
        }
    }

    class ConnectionfulProtocolMethodAttribute : ProtocolMethodAttribute
    {
        public ConnectionfulProtocolMethodAttribute(ConnectionfulMethod method)
            : base((int)method)
        {
        }
    }

    class RouterProtocolMethodAttribute : ProtocolMethodAttribute
    {
        public RouterProtocolMethodAttribute(RouterMethod method)
            : base((int)method)
        {
        }
    }

    class DhtProtocolMethodAttribute : ProtocolMethodAttribute
    {
        public DhtProtocolMethodAttribute(DhtMethod method)
            : base((int)method)
        {
        }
    }

    class MulticastProtocolMethodAttribute : ProtocolMethodAttribute
    {
        public MulticastProtocolMethodAttribute(MulticastMethod method)
            : base((int)method)
        {
        }
    }

    class MulticastGroupProtocolMethodAttribute : ProtocolMethodAttribute
    {
        public MulticastGroupProtocolMethodAttribute(MulticastGroupMethod method)
            : base((int)method)
        {
        }
    }

    class EntityProtocolMethodAttribute : ProtocolMethodAttribute
    {
        public EntityProtocolMethodAttribute(EntityMethod method)
            : base((int)method)
        {
        }
    }

    class DiscoveryProtocolMethodAttribute : ProtocolMethodAttribute
    {
        public DiscoveryProtocolMethodAttribute(DiscoveryMethod method)
            : base((int)method)
        {
        }
    }

    class UnitTestProtocolMethodAttribute : ProtocolMethodAttribute
    {
        public UnitTestProtocolMethodAttribute(int method)
            : base(method)
        {
        }
    }


    // Enums are used to provide stable numbering of protocol methods between versions.
    // Generally, method names should not be removed from the list as that would cause later
    // methods to be renumbered.  
    // *Instead they should be renamed to indicate they are no longer required.*
    // e.g. ConnectionfulMethod.___Obsolete_ReceiveForwardedEnvelope
    internal enum ConnectionlessMethod : int
    {
        ProcessInitialMessage,
        __Obsolete_ProcessRejectionMessage,
        SynchronizeSession,
        UpdateCertificate,
        /*
        ConnectionChallenge,
        ConnectionChallengeAndResponse,
        ConnectionResponse,
         */
        RespondToInternalConnectivityTest,
        InternalConnectionAvailable,
        TransportConfirmation,
        HolePunchNotification,

        DiagnosticsInitializeAndConnect,
        DiagnosticsDisconnect,
        HolePunchConfirmation,
    }

    internal enum ConnectionfulMethod : int
    {
        // IdRouter (Bounded IM)
        IdAddressRouterHandleMessage,

        // Multicast Router 
        HandleMulticastMessage,

        Multiplex,
/*
        // Router
        RouterHandleJoinRequest,
        RouterHandleIterativeJoinRequest,
        RouterHandleIterativeJoinReply,
        RouterHandleLeaveRequest,
        RouterConsiderForRoutingTableEntry,
        RouterHandleSynchronizeRequest,
        RouterPing,
        RouterHandleDirectMessage,
        RouterHandleRouteRequest,
        RouterRecordKeepAlive,
        RouterHandleBroadcast,
        RouterHandleExtendedBroadcast,
        */
        // Gossip IMS
        GossipAboutRegion,

        // Entity router (Object Manager)
        ___Obsolete_EntityRouterHandleMessageWithSourceLocalId,
        ___Obsolete_EntityRouterHandleWithSource,
        EntityRouterHandleMessageWithSourceAndDestination,
        EntityRouterHandleMessageWithSourceAndHostedDestination,
        //___Obsolete_EntityRouterHandleMessageWithDestination,

        // Private Chat 
        ReceiveChatMessage,
        PresenceEvent,
        InvitationEvent,

        // Streaming
        RequestReceiveStream,
        AcceptTransfer,
        RequestSendStream,
        AcceptSendStream,
        RejectTransfer,
        RecieveSegment,
        ReceiveAcknowledgements,
        TerminateStream,
        EndOfStream,

        // Security
        ForwardComplaint,
        ChangeProxyAddress,

        // Persistence
        ___Obsolete_PersistenceVersionChanged,
        ___Obsolete_PersistenceSubscribe,

        // Overload
        CreateForwardingGroup,
        AddToForwardingGroup,
        RemoveFromForwardingGroup,
        RemoveForwardingGroup,
        ForwardToGroup,
        ___Obsolete_ReceiveForwardedEnvelope,
        ReceiveForwardingGroupPing,
        ReceiveForwardingGroupPong,
        SendRegisteredEntities,

        // Arbitration
        ArbitrationClientInitialSequence,
        ArbitrationServerInitialSequence,
        ReceiveClientArbitrationEvent,
        ReceiveServerArbitrationEvent,
        ReceiveServerArbitrationRejection,

        // Validation
        ValidationValidatorAssignmentRequest,
        ValidationValidatorAssignmentReply,
        ValidationReceiveEntityRegistrationRequest,
        ValidationReceiveEntityUnregistrationRequest,
        ValidationReceiveEntityTransitionRequest,
        ValidationReceiveEntityStateMilestone,
        ValidationReceiveEntityInput,
        ValidationReceiveEntityStateUpdate,
        ValidationReceiveSceneRegistrationRequest,
        ValidationReceiveSceneUnregistrationRequest,
        ValidationReceiveServerTerminationNotice,
        ValidationReceiveClientTerminationNotice,
        ValidationReceivePingRequest,
        ValidationReceivePingReply,
        ValidationReceiveServerError,

        // Matchmaking
        MatchmakingHostingRequest,
        MatchmakingHostingReply,
        MatchmakingMatchQuery,
        MatchmakingMatchQueryReply,
        MatchmakingHostPing,

        // Matches
        MatchPingFromClientToHost,
        MatchLeaveNotification,
        MatchJoinConfirmation,
        MatchJoinRejection,
        MatchHostDenial,
        MatchMemberJoinNotification,
        MatchMemberLeaveNotification,
        MatchHostResignation,
        MatchPingToPotentialHost,
        MatchPongFromPotentialHost,
        MatchChat,
        MatchControllerRPC,
        MatchJoinRequest
    }

    internal enum RouterMethod : int
    {
        // Router
        RouterHandleJoinRequest,
        RouterHandleIterativeJoinRequest,
        RouterHandleIterativeJoinReply,
        RouterHandleLeaveRequest,
        RouterConsiderForRoutingTableEntry,
        RouterHandleSynchronizeRequest,
        RouterPing,
        RouterHandleDirectMessage,
        RouterHandleRouteRequest,
        RouterRecordKeepAlive,
        RouterHandleBroadcast,
        RouterHandleExtendedBroadcast,
        RouterReconcileInconsistency,
        HandleStabilizationPingMessage
    }

    internal enum DhtMethod : int
    {
        DhtTransportProcessMessage,

        CheckReplicaStatus,
        StoreOriginal,
        StoreReplica,

        RegisterSceneRequest,
        RemoveSceneRequest,
        InsertRequest,
        RemoveRequest,
        RemoveWithoutNotificationRequest,
        ModifyRequest,
        StatusResponse,
        IntersectionNotification,
        ___Obsolete_SeparationNotification,

        ApplicationProtocolReceiveMessage,
        ApplicationProtocolReceiveBroadcastMessage,

        MoveAndWakeController,

        DataServiceGetRequest,
        DataServiceGetReply,
        DataServicePokeRequest,
        DataServicePokeReply,

        RendezvousRegisterAddress,
        RendezvousConnectionRequest,
        RendezvousConnectWith,
        RendezvousInitiateUnidirectionalHolePunch,
        RendezvousHolePunchNotification,
        RendezvousRelayMessage,
        RendezvousRelayMessageArrival,
        RendezvousRequestFailed,
      //  RendezvousRelayInternalConnectionAttemptNotification,
      //  RendezvousInternalConnectionAttemptNotification,
       // RendezvousRelayTransportMethodConfirmationMessage,
       // RendezvousConfirmationTransportMethod,

        MembershipUnsubscribeAndNotifyAny,
        MembershipSubscribeAndNotifyOldest,
        MembershipNewMember,
        MembershipNewMembers,
        MembershipMemberExits,

        ChangePresence,
        ForwardSubscriptionRequest,
        SubscriptionRequest,
        PresenceNotification,

        CheckRelayer,
        RelayManagerRoutedFindRelayerFor,
        RelayManagerPropogatedFindRelayerFor,
        RelayManagerRelayerAvailable,
        RelayManagerRelayMessage,
        RelayManagerRelayedMessageArrival,
        RelayManagerRouteUnavailable,
        RelayManagerDirectRouteAvailable,
        RelayManagerRelayOneOffMessage,

        SystemControlBlacklistUser,

        ServiceDiscoveryAnnounceServiceRequest,
        ServiceDiscoveryShutdownServiceRequest,
        ServiceDiscoveryGetServiceDetailsRequest,
        ServiceDiscoveryHandleServiceDetailsQueryReply,
        ServiceDiscoveryHandleSharedDhtQueryReplies,
        ServiceDiscoveryRequestServiceExchange,
        ServiceDiscoveryServiceExchangeReply,
        ServiceDiscoveryHandleGossipMessage,
        
        DataServiceTestRequest,

        ValidationAllocationRequest,
        ValidationAllocationReply
    }

    internal enum MulticastMethod : int
    {
        ReceiveChatMessage,
        PresenceEvent,
    }

    internal enum MulticastGroupMethod : int
    {
        Ping,
        Pong,
    }

    internal enum EntityMethod : int
    {
        RemoteInstantiationRequest,
        ProcessCustomMessageRequest,
        FlowControlFeedback,
        InterestSeperation,

        InstantiateEntity,
        RemoveEntity,
        RemoveEntityWithKnownAssociatedOriginal,
        UpdateEntity,
        ProcessCustomMessage,
        ReceiveProximityChatMessage,
        AcknowledgeSegments,
        UpdateMultipleEntities,
    }

    internal enum DiscoveryMethod : int
    {
        WhoIs,
        Polo,
    }
}
