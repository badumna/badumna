﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Badumna.Core
{
    interface IMessageProducer<EnvelopeType> where EnvelopeType : BaseEnvelope
    {
        /// <summary>
        /// Sets the consumer of received messages.
        /// </summary>
        /// <remarks>
        /// IMessageProducer classes are generally constructed before IMessageConsumer
        /// classes.  This setter allows the relationship to be formed once the
        /// consumer has been constructed.
        /// </remarks>
        IMessageConsumer<EnvelopeType> MessageConsumer { set; }
    }
}
