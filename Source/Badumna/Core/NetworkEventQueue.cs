/***************************************************************************
 *  File Name: NetworkEventQueue.cs
 *
 *  Copyright (C) 2007 All Rights Reserved.
 * 
 *  Written by
 *      Scott Douglas <scdougl@csse.unimelb.edu.au>
 ****************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Diagnostics;

using Badumna.Utilities;

namespace Badumna.Core
{
    /// <summary>
    /// A singleton class that provides an ordered and timed queue of network events.
    /// </summary>
    class NetworkEventQueue : INetworkEventScheduler, ITime
    {
        protected BinaryHeap<NetworkEvent> mEventHeap = new BinaryHeap<NetworkEvent>();
        protected FifoQueue<NetworkEvent> mDiscardableQueue = new FifoQueue<NetworkEvent>(3500);

        protected volatile bool mIsRunning = false;
        public bool IsRunning { get { return this.mIsRunning; } set { this.mIsRunning = value; } }

        public delegate void QueueEventHandler();
        private QueueEventHandler emptyQueueHandlerDelegate;
        public event QueueEventHandler EmptyQueueHandler
        {
            add { this.emptyQueueHandlerDelegate += value; }
            remove { this.emptyQueueHandlerDelegate -= value; }
        }

        private readonly object mEventLock = new object();
        private AutoResetEvent mPushSignal = new AutoResetEvent(false);

        /// <summary>
        /// Used to protect all structures typically accessed from the network
        /// event thread.
        /// </summary>
        private readonly TestableLock performLock = new TestableLock();

        /// <summary>
        /// Records the number of times application code has (re-)entered
        /// the perform lock.
        /// Protected by the perform lock.
        /// </summary>
        private int applicationPerformLockCount;

        protected TimeKeeper mTimeKeeper = new TimeKeeper();

        protected bool mAllowsDiscardable = true;

        private bool mIsPaused;
        public bool IsPaused
        {
            get
            {
                return this.mIsPaused;
            }

            set
            {
                if (this.mIsPaused == value)
                {
                    return;
                }

                this.mIsPaused = value;
                if (this.mIsPaused)
                {
                    this.mTimeKeeper.Pause();
                }
                else
                {
                    this.mTimeKeeper.Start();
                }
            }
        }

        /// <summary>
        /// The NetworkEventQueue's time counter.  This counter pauses when the NetworkEventQueue
        /// is paused.
        /// </summary>
        public TimeSpan Now
        {
            get { return this.GetTimeMillisecondsFromNow(0); }
        }

        /// <summary>
        /// Returns the total number of events (both active and *inactive*) in the queue.
        /// </summary>
        public int Count
        {
            get { return this.mEventHeap.Count + this.mDiscardableQueue.Count; }
        }

        /// <summary>
        /// Gets the exception object that caused the <see cref="Run"/> loop to
        /// crash, or <c>null</c> if the loop has not crashed.
        /// </summary>
        public Exception RunException { get; private set; }

        public void Clear()
        {
            this.mEventHeap.Clear();
        }

        public NetworkEventQueue()
        {
            this.Reset();
        }

        public virtual void Reset()
        {
            this.emptyQueueHandlerDelegate = null;
            this.Clear();
            this.mTimeKeeper.Restart();
        }

        protected virtual TimeSpan GetTimeMillisecondsFromNow(double millisecondsFromNow)
        {
            return this.mTimeKeeper.Elapsed + TimeSpan.FromMilliseconds(millisecondsFromNow);
        }


        protected virtual NetworkEvent PushInvokable(double millisecondsFromNow, bool discardable, IInvokable invokable)
        {
            NetworkEvent ev = new NetworkEvent(this.GetTimeMillisecondsFromNow(millisecondsFromNow), invokable);

            ev.IsDiscardable = discardable && this.mAllowsDiscardable;
            if (ev.IsDiscardable)
            {
                var discardableQueue = this.mDiscardableQueue; // Local to work around bug in JSC.
                Synchronization.Lock(this.mEventLock, () => discardableQueue.Push(ev));
                this.mPushSignal.Set();
            }
            else
            {
                this.PushHeap(ev);
            }

            return ev;
        }

        protected virtual void PushHeap(NetworkEvent ev)
        {
            if (null != ev)
            {
                var eventHeap = this.mEventHeap; // Local to work around bug in JSC.
                Synchronization.Lock(this.mEventLock, delegate
                {
                    System.Diagnostics.Debug.Assert(ev.IsActive, "Attempting to push an inactive event.");
                    eventHeap.Push(ev);
                });
                this.mPushSignal.Set();
            }
        }


        #region Push(...) overloads

        public void Schedule(long millisecondsFromNow, NetworkEvent ev)
        {
            ev.Time = this.GetTimeMillisecondsFromNow(millisecondsFromNow);
            ev.IsDiscardable = false;
            this.PushHeap(ev);
        }

        public NetworkEvent Schedule(double millisecondsFromNow, GenericCallBack function)
        {
            return this.PushInvokable(millisecondsFromNow, false, Apply.Func(function));
        }

        public NetworkEvent Schedule<A>(double millisecondsFromNow, GenericCallBack<A> function, A a)
        {
            return this.PushInvokable(millisecondsFromNow, false, Apply.Func(function, a));
        }

        public NetworkEvent Schedule<A, B>(double millisecondsFromNow, GenericCallBack<A, B> function, A a, B b)
        {
            return this.PushInvokable(millisecondsFromNow, false, Apply.Func(function, a, b));
        }

        public NetworkEvent Schedule<A, B, C>(double millisecondsFromNow, GenericCallBack<A, B, C> function, A a, B b, C c)
        {
            return this.PushInvokable(millisecondsFromNow, false, Apply.Func(function, a, b, c));
        }

        public NetworkEvent Schedule<A, B, C, D>(double millisecondsFromNow, GenericCallBack<A, B, C, D> function, A a, B b, C c, D d)
        {
            return this.PushInvokable(millisecondsFromNow, false, Apply.Func(function, a, b, c, d));
        }

        public NetworkEvent Schedule<A, B, C, D, E>(double millisecondsFromNow, GenericCallBack<A, B, C, D, E> function, A a, B b, C c, D d, E e)
        {
            return this.PushInvokable(millisecondsFromNow, false, Apply.Func(function, a, b, c, d, e));
        }


        public NetworkEvent Push(GenericCallBack function)
        {
            return this.Schedule(0, function);
        }

        public NetworkEvent Push<A>(GenericCallBack<A> function, A a)
        {
            return this.Schedule(0, function, a);
        }

        public NetworkEvent Push<A, B>(GenericCallBack<A, B> function, A a, B b)
        {
            return this.Schedule(0, function, a, b);
        }

        public NetworkEvent Push<A, B, C>(GenericCallBack<A, B, C> function, A a, B b, C c)
        {
            return this.Schedule(0, function, a, b, c);
        }

        public NetworkEvent Push<A, B, C, D>(GenericCallBack<A, B, C, D> function, A a, B b, C c, D d)
        {
            return this.Schedule(0, function, a, b, c, d);
        }

        public NetworkEvent Push<A, B, C, D, E>(GenericCallBack<A, B, C, D, E> function, A a, B b, C c, D d, E e)
        {
            return this.Schedule(0, function, a, b, c, d, e);
        }


        public NetworkEvent PushDiscardable(GenericCallBack function)
        {
            return this.PushInvokable(0, true, Apply.Func(function));
        }

        public NetworkEvent PushDiscardable<A>(GenericCallBack<A> function, A a)
        {
            return this.PushInvokable(0, true, Apply.Func(function, a));
        }

        public NetworkEvent PushDiscardable<A, B>(GenericCallBack<A, B> function, A a, B b)
        {
            return this.PushInvokable(0, true, Apply.Func(function, a, b));
        }

        public NetworkEvent PushDiscardable<A, B, C>(GenericCallBack<A, B, C> function, A a, B b, C c)
        {
            return this.PushInvokable(0, true, Apply.Func(function, a, b, c));
        }

        public NetworkEvent PushDiscardable<A, B, C, D>(GenericCallBack<A, B, C, D> function, A a, B b, C c, D d)
        {
            return this.PushInvokable(0, true, Apply.Func(function, a, b, c, d));
        }

        public NetworkEvent PushDiscardable<A, B, C, D, E>(GenericCallBack<A, B, C, D, E> function, A a, B b, C c, D d, E e)
        {
            return this.PushInvokable(0, true, Apply.Func(function, a, b, c, d, e));
        }

        #endregion

        protected NetworkEvent NextEvent(bool remove)
        {
            return Synchronization.Lock(this.mEventLock, () =>
            {
                NetworkEvent regularEvent = this.mEventHeap.Peek();
                while (regularEvent != null && !regularEvent.IsActive)
                {
                    this.mEventHeap.Pop();  // Throw away the inactive events
                    regularEvent = this.mEventHeap.Peek();
                }

                // No active regular event, or an active event at the top of the heap
                Debug.Assert(regularEvent == null || (regularEvent.IsActive && this.mEventHeap.Count > 0));

                NetworkEvent discardableEvent = this.mDiscardableQueue.Peek();
                while (discardableEvent != null && !discardableEvent.IsActive)
                {
                    this.mDiscardableQueue.Pop();
                    discardableEvent = this.mDiscardableQueue.Peek();
                }

                // No active discardable event, or a discardable event at the top of the queue
                Debug.Assert(discardableEvent == null || (discardableEvent.IsActive && this.mDiscardableQueue.Count > 0));

                if (discardableEvent != null)
                {
                    if (regularEvent != null)
                    {
                        if (discardableEvent.Time < regularEvent.Time)
                        {
                            if (remove)
                            {
                                this.mDiscardableQueue.Pop();
                            }

                            return discardableEvent;
                        }
                        else
                        {
                            if (remove)
                            {
                                this.mEventHeap.Pop();
                            }

                            return regularEvent;
                        }
                    }

                    if (remove)
                    {
                        this.mDiscardableQueue.Pop();
                    }

                    return discardableEvent;
                }

                if (regularEvent != null)
                {

                    if (remove)
                    {
                        this.mEventHeap.Pop();
                    }

                    return regularEvent;
                }

                return null;
            });
        }

        public NetworkEvent Peek()
        {
            return this.NextEvent(false);
        }

        public NetworkEvent Pop()
        {
            return this.NextEvent(true);
        }

        public void Remove(NetworkEvent ev)
        {
            Synchronization.Lock(this.mEventLock, delegate
            {
                if (null != ev && ev.IsActive)
                {
                    ev.IsActive = false;
                }
            });
        }

        /// <summary>
        /// Acquires the "perform" lock.
        /// </summary>
        public void AcquirePerformLock()
        {
            this.performLock.Lock();
        }

        /// <summary>
        /// Releases the "perform" lock.
        /// </summary>
        public void ReleasePerformLock()
        {
            this.performLock.Unlock();
        }

        /// <summary>
        /// Acquires the "perform" lock and records that it was acquired by an application thread.
        /// </summary>
        public void AcquirePerformLockForApplicationCode()
        {
            this.AcquirePerformLock();
            this.applicationPerformLockCount++;
        }

        /// <summary>
        /// Releases the "perform" lock.  To be used if the lock was acquired using AcquirePerformLockForApplicationCode().
        /// </summary>
        public void ReleasePerformLockForApplicationCode()
        {
            this.AssertIsApplicationThread();
            this.applicationPerformLockCount--;
            this.ReleasePerformLock();
        }

        /// <summary>
        /// Asserts that the code is running on an application thread.
        /// </summary>
        /// <remarks>
        /// This also checks that we're currently holding the perform lock
        /// as we only record the fact that we're on an application thread
        /// when we try to acquire the perform lock.
        /// </remarks>
        [Conditional("TRACE")]
        public void AssertIsApplicationThread()
        {
            Trace.Assert(
                this.performLock.IsHeldByMe() && this.applicationPerformLockCount > 0,
                "This code should only execute on application thread!");
        }

        /// <summary>
        /// Asserts that the perform lock is held by the current thread.
        /// </summary>
        [Conditional("TRACE")]
        public void AssertHasPerformLock()
        {
            Trace.Assert(
                this.performLock.IsHeldByMe(),
                "This code must be protected by the perform lock!");
        }

        public virtual void Run(Fiber fiber)
        {
            if (fiber != null)
            {
                if (this.mIsRunning)
                {
                    this.RunCore(fiber);
                }
                else
                {
                    fiber.IsComplete = true;
                }

                return;
            }

            this.mIsRunning = true;
            while (this.mIsRunning)
            {
                this.RunCore(fiber);
            }
        }

        /// <summary>
        /// Main body of the run loop.
        /// </summary>
        /// <param name="fiber">The current fiber, if any</param>
        private void RunCore(Fiber fiber)
        {
            if (0 == this.Count)
            {
                QueueEventHandler handler = this.emptyQueueHandlerDelegate;
                if (handler != null)
                {
                    handler();
                }

                // Sleep for 10 seconds, or until the next event.
                if (fiber != null)
                {
                    fiber.WaitFor(this.mPushSignal, 10000);
                }
                else
                {
                    this.mPushSignal.WaitOne(10000, false);
                }
            }
            else
            {
                TimeSpan nextEventTime = this.TimeOfNextEvent();

                if (this.Count != 0)
                {
                    TimeSpan eventDelay = (nextEventTime - this.Now);

                    if (eventDelay.TotalMilliseconds > 0)
                    {
                        // Sleep until either the next push signal or event.
                        if (fiber != null)
                        {
                            fiber.WaitFor(this.mPushSignal, (int)eventDelay.TotalMilliseconds);
                        }
                        else
                        {
                            this.mPushSignal.WaitOne(eventDelay, false);
                        }
                    }
                    else
                    {
                        NetworkEvent ev = this.Pop();

                        if (null != ev)
                        {
                            if (ev.IsDiscardable)
                            {
                                if ((ev.Time + Parameters.EventQueueAllowableLag) < this.Now)
                                {
                                    Logger.TraceInformation(LogTag.Core, "Discarding event {0} because it is too old.", ev.Name);
                                    return;
                                }

                                /* if (this.Count > Parameters.MaximumAllowableEventCount)
                                 {
                                     Logger.TraceInformation(LogTag.Core, "Discarding event {0} because the queue is too large.", ev.Name);
                                     return;
                                 }*/
                            }

                            this.AcquirePerformLock();
                            try
                            {
#if TRACE
                                var startTime = this.mTimeKeeper.Elapsed;
#endif

                                ev.PerformAction();

#if TRACE
                                var durationMs = (this.mTimeKeeper.Elapsed - startTime).TotalMilliseconds;
                                if (durationMs > 500)
                                {
                                    Logger.TraceWarning(LogTag.Event | LogTag.Core, "Slow network event ({0}ms): {1}", durationMs, ev.Name);
                                }
#endif
                            }
                            catch (Exception e)
                            {
                                this.RunException = e;
                                throw;
                            }
                            finally
                            {
                                this.ReleasePerformLock();
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Runs all events in the queue immediately (i.e. this function ignores the events' time properties)
        /// </summary>
        public void RunAllIgnoringTime()
        {
            while (this.Count > 0)
            {
                NetworkEvent ev = this.Pop();
                if (ev != null)
                {
                    Logger.TraceInformation(LogTag.Core, "{0} {1}", ev.Time, ev.Name);
                    ev.PerformAction();
                }
            }
        }

        public virtual void RunUntil(TimeSpan endTime)
        {
            NetworkEvent endEvent = new NetworkEvent(endTime, Apply.Func(this.Stop));

            endEvent.Name = "EndEvent";
            this.PushHeap(endEvent);
            this.Run(null);
        }

        public virtual void Stop()
        {
            this.mIsRunning = false;
            this.mPushSignal.Set();
        }

        /// <summary>
        /// Get the time of the next event in the queue
        /// </summary>
        /// <returns>A TimeSpan indicating the time of the next event relative to when Badumna was initialized
        /// (ignoring the duration of any event queue pauses).  Returns TimeSpan.Zero if there are no events
        /// on the queue; it is the caller's responsibility to distinguish this from the case when the next event
        /// occurs at time 0.</returns>
        public TimeSpan TimeOfNextEvent()
        {
            NetworkEvent ev = this.NextEvent(false);
            if (ev != null)
            {
                return ev.Time;
            }

            return TimeSpan.Zero;
        }

        // this can only be invoked in simulation mode
        public bool Contains(NetworkEvent ev)
        {
            if (!ev.IsActive)
            {
                return false;
            }

            // Locals to workaround bug in JSC
            var eventHeap = this.mEventHeap;
            var discardableQueue = this.mDiscardableQueue;

            return Synchronization.Lock(this.mEventLock, () => eventHeap.Contains(ev) || discardableQueue.Contains(ev));
        }

        // this can only be invoked in simulation mode
        public List<NetworkEvent> GetQueue()
        {
            return Synchronization.Lock<List<NetworkEvent>>(this.mEventLock, this.mEventHeap.GetQueue);
        }
    }
}