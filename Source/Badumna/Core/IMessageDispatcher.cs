﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Badumna.Core
{
    interface IMessageDispatcher<T> where T : BaseEnvelope
    {
        void SendMessage(T envelope);
    }
}
