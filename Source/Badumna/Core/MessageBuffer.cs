//------------------------------------------------------------------------------
// <copyright file="MessageBuffer.cs" company="National ICT Australia Limited">
//     Copyright (c) 2007-2012 National ICT Australia Limited. All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;

using Badumna.Utilities;

namespace Badumna.Core
{
    /// <summary>
    /// Serializes and deserializes objects to a byte array, and processes checksums.
    /// </summary>
    /// <remarks>
    /// TODO: Move the checksumming code to a separate class.
    /// TODO: Improve the serialization to pack data in bits, compress, etc.  Allow for specific precision on floating point numbers.
    /// </remarks>
    internal class MessageBuffer : IParseable, IDisposable
    {
        /// <summary>
        /// Length of a guid in bytes.
        /// </summary>
        private static readonly int GuidLength = Guid.Empty.ToByteArray().Length;

        /// <summary>
        /// The <see cref="BinaryFormatter"/> used when serializing / deserializing with the
        /// builtin .NET serialization.
        /// </summary>
        private static BinaryFormatter formatter;

#if TRACE
        /// <summary>
        /// Records the (potentially overlapping) boundaries of objects serialied within this message
        /// </summary>
        private List<ObjectBounds> objectBounds = new List<ObjectBounds>();
#endif

        /// <summary>
        /// The message buffer.
        /// </summary>
        private MemoryStream buffer;

        /// <summary>
        /// The backing field for the <see cref="ReadOffset"/> property.
        /// </summary>
        private int readOffset;

        /// <summary>
        /// The backing field for the <see cref="WriteOffset"/> property.
        /// </summary>
        private int writeOffset;

        /// <summary>
        /// Initializes static members of the MessageBuffer class.
        /// </summary>
        static MessageBuffer()
        {
            MessageBuffer.formatter = new BinaryFormatter();
            MessageBuffer.formatter.SurrogateSelector = new ParseableSurrogateSelector();
        }

        /// <summary>
        /// Initializes a new instance of the MessageBuffer class.
        /// </summary>
        public MessageBuffer()
        {
            this.buffer = new MemoryStream();
        }

        /// <summary>
        /// Initializes a new instance of the MessageBuffer class as a copy of the given message.
        /// </summary>
        /// <param name="message">The message to copy</param>
        public MessageBuffer(MessageBuffer message)
            : this()
        {
            message.Seek(0, SeekOrigin.Begin);
            message.buffer.WriteTo(this.buffer);
            this.Length = message.Length;
            this.HasBeenChecksummed = message.HasBeenChecksummed;
            this.readOffset = message.readOffset;
            this.writeOffset = message.writeOffset;
        }

        /// <summary>
        /// Initializes a new instance of the MessageBuffer class conataining the given data.
        /// </summary>
        /// <param name="buffer">The initial contents of the message</param>
        public MessageBuffer(byte[] buffer)
            : this()
        {
            if (buffer != null)
            {
                this.buffer.Write(buffer, 0, buffer.Length);
                this.Length = buffer.Length;
            }
        }

        /// <summary>
        /// Gets or sets the offset from the start of the buffer indicating the location of read operations.
        /// </summary>
        public int ReadOffset
        {
            get
            {
                return this.readOffset;
            }

            set
            {
                if (0 > value || value > this.Length)
                {
                    throw new ArgumentOutOfRangeException("value", value, Resources.MessageOutOfRange);
                }

                this.readOffset = value;
            }
        }

        /// <summary>
        /// Gets or sets the offset from the start of the buffer indicating the location of write operations.
        /// </summary>
        public int WriteOffset
        {
            get
            {
                return this.writeOffset;
            }

            set
            {
                if (0 > value || value > this.Length)
                {
                    throw new ArgumentOutOfRangeException("value", value, Resources.MessageOutOfRange);
                }

                this.writeOffset = value;
            }
        }

        /// <summary>
        /// Gets the length of the messsage.
        /// </summary>
        public int Length { get; private set; }

        /// <summary>
        /// Gets the number of bytes that have yet to be read before the end of the message.
        /// </summary>
        public int Available
        {
            get
            {
                return this.Length - this.readOffset;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the message includes checksum information.
        /// </summary>
        public bool HasBeenChecksummed { get; private set; }

        /// <summary>
        /// Removes the given number of bytes from the end of the message.
        /// </summary>
        /// <param name="n">The number of bytes to remove</param>
        public void DropLastNBytes(int n)
        {
            this.Length -= n;
            this.buffer.SetLength(this.Length);

            if (this.writeOffset > this.Length)
            {
                this.writeOffset = this.Length;
            }

            if (this.readOffset > this.Length)
            {
                this.readOffset = this.Length;
            }
        }

        /// <summary>
        /// Copies bytes into the given message.
        /// </summary>
        /// <param name="message">The message to copy bytes into</param>
        /// <param name="offset">The offset in *this* message to start copying from</param>
        /// <param name="length">The number of bytes to copy</param>
        public void CopyBytes(MessageBuffer message, int offset, int length)
        {
            int currentReadOffset = this.ReadOffset;
            this.ReadOffset = offset;
            byte[] buffer = this.ReadBytes(length);
            this.ReadOffset = currentReadOffset;
            message.Write(buffer, buffer.Length);
        }

        /// <summary>
        /// Gets a copy of the message buffer.
        /// </summary>
        /// <returns>The copy of the message buffer</returns>
        public byte[] GetBuffer()
        {
            byte[] buf = this.buffer.ToArray();
            return buf;
        }

        /// <summary>
        /// Deserializes a byte.
        /// </summary>
        /// <returns>The deserialized byte</returns>
        public byte ReadByte()
        {
            if (0 == this.Available)
            {
                throw new EndOfMessageException();
            }

            byte result;

            this.Seek(this.ReadOffset, SeekOrigin.Begin);
            result = (byte)this.buffer.ReadByte();

            this.ReadOffset++;
            return result;
        }

        /// <summary>
        /// Deserializes bytes.
        /// </summary>
        /// <param name="readLength">The number of bytes to deserialize</param>
        /// <returns>The deserialized bytes</returns>
        public byte[] ReadBytes(int readLength)
        {
            if (readLength > this.Available)
            {
                throw new ArgumentOutOfRangeException("readLength");
            }

            byte[] result = new byte[readLength];

            this.Seek(this.ReadOffset, SeekOrigin.Begin);
            int length = this.Read(result, 0, readLength);            

            if (length != readLength)
            {
                throw new ParseException("Failed to read enough bytes");
            }

            this.ReadOffset += readLength;

            return result;
        }

        /// <summary>
        /// Deserializes bytes.
        /// </summary>
        /// <param name="result">The buffer to deserialize the bytes into</param>
        /// <param name="len">The number of bytes to deserialize</param>
        /// <returns>The number of bytes deserialized</returns>
        public int ReadBytes(byte[] result, int len)
        {
            return this.ReadBytes(result, 0, len);
        }

        /// <summary>
        /// Deserializes bytes.
        /// </summary>
        /// <param name="result">The buffer to deserialize the bytes into</param>
        /// <param name="offset">The offset in the buffer to start deserializing bytes at</param>
        /// <param name="len">The number of bytes to deserialize</param>
        /// <returns>The number of bytes deserialized</returns>
        public int ReadBytes(byte[] result, int offset, int len)
        {
            int remainingBytes = this.Length - this.ReadOffset;
            int readLength = len > remainingBytes ? remainingBytes : len;

            if (0 == readLength)
            {
                return 0;
            }

            this.Seek(this.ReadOffset, SeekOrigin.Begin);
            this.Read(result, offset, readLength);
            this.ReadOffset += readLength;

            return readLength;
        }

        /// <summary>
        /// Deserializes a string.
        /// </summary>
        /// <returns>The deserialized string</returns>
        /// <remarks>
        /// TODO: This should throw if there aren't enough bytes available, rather than returning the empty string.
        /// </remarks>
        public string ReadString()
        {
            if (this.Available < 2)
            {
                return "";
            }

            int length = (int)this.ReadUShort();
            if (length > this.Available)
            {
                return "";
            }

            byte[] data = this.ReadBytes(length);

            if (data != null)
            {
                return Encoding.UTF8.GetString(data);
            }

            return "";
        }

        /// <summary>
        /// Deserializes a parseable of the given type, constructing the instance using the type's
        /// default constructor.
        /// </summary>
        /// <typeparam name="T">The type of the parseable to deserialize</typeparam>
        /// <returns>The deserialized parseable</returns>
        public T Read<T>() where T : IParseable
        {
            return (T)this.ReadIParseable(typeof(T), MessageBuffer.DefaultFactory);
        }

        /// <summary>
        /// Deserializes a parseable of the given type, constructing the instance using the type's
        /// default constructor.
        /// </summary>
        /// <remarks>This overload should only be used in situations
        /// where T is not available as a type param (e.g when it comes
        /// from a class typeparam in ActionScript)</remarks>
        /// <param name="t">The type of the parseable to deserialize</param>
        /// <returns>The deserialized parseable</returns>
        public object Read(Type t)
        {
            return this.ReadIParseable(t, MessageBuffer.DefaultFactory);
        }

        /// <summary>
        /// Deserializes a short.
        /// </summary>
        /// <returns>The deserialized short</returns>
        public short ReadShort()
        {
            return BitConverter.ToInt16(this.ReadBytes(2), 0);
        }

        /// <summary>
        /// Deserializes a ushort.
        /// </summary>
        /// <returns>The deserialized ushort</returns>
        public ushort ReadUShort()
        {
            return BitConverter.ToUInt16(this.ReadBytes(2), 0);
        }

        /// <summary>
        /// Deserializes a long.
        /// </summary>
        /// <returns>The deserialized long</returns>
        public long ReadLong()
        {
            return BitConverter.ToInt64(this.ReadBytes(8), 0);
        }

        /// <summary>
        /// Deserializes a ulong.
        /// </summary>
        /// <returns>The deserialized ulong</returns>
        public ulong ReadULong()
        {
            return BitConverter.ToUInt64(this.ReadBytes(8), 0);
        }

        /// <summary>
        /// Deserializes a float.
        /// </summary>
        /// <returns>The deserialized float</returns>
        public float ReadFloat()
        {
            return BitConverter.ToSingle(this.ReadBytes(4), 0);
        }

        /// <summary>
        /// Deserializes a double.
        /// </summary>
        /// <returns>The deserialized double</returns>
        public double ReadDouble()
        {
            return BitConverter.ToDouble(this.ReadBytes(8), 0);
        }

        /// <summary>
        /// Deserializes an int.
        /// </summary>
        /// <returns>The deserialized int</returns>
        public int ReadInt()
        {
            return BitConverter.ToInt32(this.ReadBytes(4), 0);
        }

        /// <summary>
        /// Deserializes a uint.
        /// </summary>
        /// <returns>The deserialized uint</returns>
        public uint ReadUInt()
        {
            return BitConverter.ToUInt32(this.ReadBytes(4), 0);
        }

        /// <summary>
        /// Deserializes a bool.
        /// </summary>
        /// <returns>The deserialized bool</returns>
        public bool ReadBool()
        {
            return this.ReadByte() != 0;
        }

        /// <summary>
        /// Deserializes a guid.
        /// </summary>
        /// <returns>The deserialized guid</returns>
        public Guid ReadGuid()
        {
            return new Guid(this.ReadBytes(MessageBuffer.GuidLength));
        }

        /// <summary>
        /// Serializes the given string.
        /// </summary>
        /// <param name="data">The string to serialize</param>
        public void Write(string data)
        {
            Debug.Assert(!this.HasBeenChecksummed, "Writing to message after checksum has been caculated.");
            if (null == data)
            {
                throw new ArgumentNullException("data");
            }

            int length = Encoding.UTF8.GetByteCount(data);

            this.Write((ushort)length);
            this.RecordObjectBounds(length, "<String: " + data + ">");
            this.Write(Encoding.UTF8.GetBytes(data), 0, length);
        }

        /// <summary>
        /// Serializes the given bool.
        /// </summary>
        /// <param name="data">The bool to serialize</param>
        public void Write(bool data)
        {
            Debug.Assert(!this.HasBeenChecksummed, "Writing to message after checksum has been caculated.");
            this.RecordObjectBounds(1, "(bool)");
            if (data)
            {
                this.Write((byte)1);
            }
            else
            {
                this.Write((byte)0);
            }
        }

        /// <summary>
        /// Serializes the given short.
        /// </summary>
        /// <param name="data">The short to serialize</param>
        public void Write(short data)
        {
            Debug.Assert(!this.HasBeenChecksummed, "Writing to message after checksum has been caculated.");
            this.RecordObjectBounds(2, data);
            this.Write(BitConverter.GetBytes(data), 0, 2);
        }

        /// <summary>
        /// Serializes the given ushort.
        /// </summary>
        /// <param name="data">The ushort to serialize</param>
        public void Write(ushort data)
        {
            Debug.Assert(!this.HasBeenChecksummed, "Writing to message after checksum has been caculated.");
            this.RecordObjectBounds(2, data);
            this.Write(BitConverter.GetBytes(data), 0, 2);
        }

        /// <summary>
        /// Serializes the given long.
        /// </summary>
        /// <param name="data">The long to serialize</param>
        public void Write(long data)
        {
            Debug.Assert(!this.HasBeenChecksummed, "Writing to message after checksum has been caculated.");
            this.RecordObjectBounds(8, data);
            this.Write(BitConverter.GetBytes(data), 0, 8);
        }
        
        /// <summary>
        /// Serializes the given ulong.
        /// </summary>
        /// <param name="data">The ulong to serialize</param>
        public void Write(ulong data)
        {
            Debug.Assert(!this.HasBeenChecksummed, "Writing to message after checksum has been caculated.");
            this.RecordObjectBounds(8, data);
            this.Write(BitConverter.GetBytes(data), 0, 8);
        }

        /// <summary>
        /// Serializes the given int.
        /// </summary>
        /// <param name="data">The int to serialize</param>
        public void Write(int data)
        {
            Debug.Assert(!this.HasBeenChecksummed, "Writing to message after checksum has been caculated.");
            this.RecordObjectBounds(4, data);
            this.Write(BitConverter.GetBytes(data), 0, 4);
        }

        /// <summary>
        /// Serializes the given uint.
        /// </summary>
        /// <param name="data">The uint to serialize</param>
        public void Write(uint data)
        {
            Debug.Assert(!this.HasBeenChecksummed, "Writing to message after checksum has been caculated.");
            this.RecordObjectBounds(4, data);
            this.Write(BitConverter.GetBytes(data), 0, 4);
        }

        /// <summary>
        /// Serializes the given float.
        /// </summary>
        /// <param name="data">The float to serialize</param>
        public void Write(float data)
        {
            Debug.Assert(!this.HasBeenChecksummed, "Writing to message after checksum has been caculated.");
            this.RecordObjectBounds(4, data);
            this.Write(BitConverter.GetBytes(data), 0, 4);
        }

        /// <summary>
        /// Serializes the given double.
        /// </summary>
        /// <param name="data">The double to serialize</param>
        public void Write(double data)
        {
            Debug.Assert(!this.HasBeenChecksummed, "Writing to message after checksum has been caculated.");
            this.RecordObjectBounds(8, data);
            this.Write(BitConverter.GetBytes(data), 0, 8);
        }

        /// <summary>
        /// Serializes the given byte.
        /// </summary>
        /// <param name="data">The byte to serialize</param>
        public void Write(byte data)
        {
            Debug.Assert(!this.HasBeenChecksummed, "Writing to message after checksum has been caculated.");
            this.Seek(this.WriteOffset, SeekOrigin.Begin);
            this.buffer.WriteByte(data);

            if (this.Length < this.WriteOffset + 1)
            {
                this.Length = this.WriteOffset + 1;
            }

            this.WriteOffset += 1;
        }

        /// <summary>
        /// Serializes the given bytes.
        /// </summary>
        /// <param name="data">The array containing the bytes to serialize</param>
        /// <param name="length">The number of bytes to serialize</param>
        /// <remarks>
        /// Note that the number of bytes serialized is not stored, and must
        /// be known when the matching Read call is made.
        /// </remarks>
        public void Write(byte[] data, int length)
        {
            Debug.Assert(!this.HasBeenChecksummed, "Writing to message after checksum has been calculated.");
            this.Write(data, 0, length);
        }

        /// <summary>
        /// Serializes the given bytes.
        /// </summary>
        /// <param name="data">The array containing the bytes to serialize</param>
        /// <param name="offset">The offset in the array to begin serializing bytes from</param>
        /// <param name="length">The number of bytes to serialize</param>
        /// <remarks>
        /// Note that the number of bytes serialized is not stored, and must
        /// be known when the matching Read call is made.
        /// </remarks>
        public void Write(byte[] data, int offset, int length)
        {
            Debug.Assert(!this.HasBeenChecksummed, "Writing to message after checksum has been calculated.");
            if (null == data || length == 0)
            {
                return;
            }

            this.Seek(this.WriteOffset, SeekOrigin.Begin);

            this.WriteHelper(data, offset, length);

            if (this.Length < this.WriteOffset + length)
            {
                this.Length = this.WriteOffset + length;
            }

            this.WriteOffset += length;
        }

        /// <summary>
        /// Serializes the given guid.
        /// </summary>
        /// <param name="guid">The guid to serialize</param>
        public void Write(Guid guid)
        {
            var guidBytes = guid.ToByteArray();
            this.Write(guidBytes, guidBytes.Length);
        }

        /// <summary>
        /// Serializes the given parseable.
        /// </summary>
        /// <param name="parseable">The parseable to serialize</param>
        public void Write(IParseable parseable)
        {
            Debug.Assert(!this.HasBeenChecksummed, "Writing to message after checksum has been calculated.");
            if (null == parseable)
            {
                throw new ArgumentNullException("parseable");
            }

            this.Write(parseable, parseable.GetType());
        }

        /// <summary>
        /// Serializes the given parseable.
        /// </summary>
        /// <param name="writable">The parseable to serialize</param>
        /// <param name="writableType">The type to serialize as (unused?)</param>
        /// <remarks>
        /// TODO: Remove the 'writeableType' parameter.
        /// </remarks>
        public void Write(IParseable writable, Type writableType)
        {
            Debug.Assert(!this.HasBeenChecksummed, "Writing to message after checksum has been calculated.");
            var start = this.writeOffset;
            writable.ToMessage(this, writableType);
            this.RecordObjectBounds(start, this.writeOffset, writable);
        }

        /// <summary>
        /// Deserializes an object of the given type.
        /// </summary>
        /// <param name="parameterType">The type to deserialize</param>
        /// <param name="factory">A factory that can construct objects of the given type, used for IParseables</param>
        /// <returns>The deserialized object</returns>
        public object GetObject(Type parameterType, GenericCallBackReturn<object, Type> factory)
        {
            if (null == parameterType)
            {
                throw new ArgumentNullException("parameterType");
            }

            if (parameterType == typeof(object))
            {
                // If the type is explicitly object, then the .NET serialization
                // was used.  See PutObject() for justification.
                return this.ReadObjectUsingDotNetSerialization();
            }

            if (typeof(IParseable).IsAssignableFrom(parameterType))
            {
                return this.ReadIParseable(parameterType, factory);
            }

            if (parameterType.IsGenericType)
            {
                Type[] genericArgs = parameterType.GetGenericArguments();

                switch (genericArgs.Length)
                {
                    case 1:
                        // Lists
                        IList list = null;

                        // parameterType is an IList (includes List, List<>, does not include IList<>)
                        if (typeof(IList).IsAssignableFrom(parameterType))
                        {
                            list = (IList)Activator.CreateInstance(parameterType);
                        }
                        else
                        {
                            // parameterType is a base class of List<T> (includes IList<T>)
                            Type listType = typeof(List<>).MakeGenericType(parameterType.GetGenericArguments());
                            if (parameterType.IsAssignableFrom(listType))
                            {
                                list = (IList)Activator.CreateInstance(listType);
                            }
                        }

                        if (list != null)
                        {
                            int length = this.ReadUShort();

                            for (int i = 0; i < length; i++)
                            {
                                list.Add(this.GetObject(genericArgs[0], factory));
                            }

                            return list;
                        }

                        break;

                    case 2:
                        // Dictionaries
                        if (typeof(IDictionary).IsAssignableFrom(parameterType))
                        {
                            IDictionary dictionary = (IDictionary)Activator.CreateInstance(parameterType);

                            if (dictionary != null)
                            {
                                int length = this.ReadUShort();

                                for (int i = 0; i < length; i++)
                                {
                                    dictionary.Add(this.GetObject(genericArgs[0], factory), this.GetObject(genericArgs[1], factory));
                                }

                                return dictionary;
                            }
                        }

                        break;
                }
            }

            if (typeof(bool) == parameterType)
            {
                return this.ReadBool();
            }

            if (typeof(int) == parameterType)
            {
                return this.ReadInt();
            }

            if (typeof(uint) == parameterType)
            {
                return this.ReadUInt();
            }

            if (typeof(short) == parameterType)
            {
                return this.ReadShort();
            }

            if (typeof(ushort) == parameterType)
            {
                return this.ReadUShort();
            }

            if (typeof(long) == parameterType)
            {
                return this.ReadLong();
            }

            if (typeof(string) == parameterType)
            {
                return this.ReadString();
            }

            if (typeof(double) == parameterType)
            {
                return this.ReadDouble();
            }

            if (typeof(float) == parameterType)
            {
                return this.ReadFloat();
            }

            if (typeof(byte) == parameterType)
            {
                return this.ReadByte();
            }

            if (typeof(byte[]) == parameterType)
            {
                int length = (int)this.ReadUShort();
                return this.ReadBytes(length);
            }

            Logger.TraceInformation(LogTag.Core, "Using built in de-serialization for type {0}", parameterType);

            return this.ReadObjectUsingDotNetSerialization();
        }

        /// <summary>
        /// Serializes the given object as the specified type.
        /// </summary>
        /// <param name="parameter">The object to serialize</param>
        /// <param name="parameterType">The type to serialize as</param>
        /// <remarks>
        /// The 'parameterType' parameter doesn't really make any sense. If
        /// it doesn't match parameter.GetType() then apparently we'll slice
        /// the object when we serialize it; except that we don't -- we
        /// still call virtual methods directly on the object.  Also
        /// we do direct inspection of the parameter type anyway, so it's
        /// all bunk.
        /// TODO: Eliminate the 'parameterType' parameter.
        /// </remarks>
        public void PutObject(object parameter, Type parameterType)
        {
            Debug.Assert(!this.HasBeenChecksummed, "Writing to message after checksum has been caculated.");
            if (null == parameterType)
            {
                throw new ArgumentNullException("parameterType");
            }

            if (parameterType == typeof(object))
            {
                // No choice here, the parameter is explicitly an object.  Unless
                // we write info to the stream about how we serialized it we just
                // have to use the .NET serialization for everything.  Shouldn't
                // be a problem because ProtocolMethods really shouldn't specify
                // 'object' parameters.
                this.WriteUsingDotNetSerialization(parameter);
                return;
            }

            IParseable asParseable = parameter as IParseable;
            if (null != asParseable)
            {
                asParseable.ToMessage(this, parameterType);
                return;
            }

            if (parameterType.IsGenericType)
            {
                IList list = parameter as IList;

                if (null != list)
                {
                    this.Write((ushort)list.Count);

                    Type itemType = parameterType.GetGenericArguments()[0];
                    foreach (object obj in list)
                    {
                        this.PutObject(obj, itemType);
                    }

                    return;
                }

                IDictionary dictionary = parameter as IDictionary;
                if (null != dictionary)
                {
                    this.Write((ushort)dictionary.Count);

                    Type[] types = parameterType.GetGenericArguments();
                    foreach (DictionaryEntry entry in dictionary)
                    {
                        this.PutObject(entry.Key, types[0]);
                        this.PutObject(entry.Value, types[1]);
                    }

                    return;
                }
            }

            if (parameter is int)
            {
                this.Write((int)parameter);
                return;
            }

            if (parameter is uint)
            {
                this.Write((uint)parameter);
                return;
            }

            if (parameter is short)
            {
                this.Write((short)parameter);
                return;
            }

            if (parameter is ushort)
            {
                this.Write((ushort)parameter);
                return;
            }

            if (parameter is long)
            {
                this.Write((long)parameter);
                return;
            }

            if (parameter is float)
            {
                this.Write((float)parameter);
                return;
            }

            if (parameter is double)
            {
                this.Write((double)parameter);
                return;
            }

            if (parameter is bool)
            {
                this.Write((bool)parameter);
                return;
            }

            string asString = parameter as string;
            if (null != asString)
            {
                this.Write(asString);
                return;
            }

            if (parameter is byte)
            {
                this.Write((byte)parameter);
                return;
            }

            byte[] asByteArray = parameter as byte[];
            if (null != asByteArray)
            {
                int length = asByteArray.Length;

                if (length > ushort.MaxValue)
                {
                    throw new OverflowException(Resources.Message_overflowException);
                }

                ushort shortLength = (ushort)length;
                this.Write(shortLength);
                this.Write(asByteArray, length);
                return;
            }

            Logger.TraceInformation(LogTag.Core, "Using built in serialization for type {0}", parameter.GetType());
            this.WriteUsingDotNetSerialization(parameter);
        }

        /// <inheritdoc />
        public override string ToString()
        {
            byte[] content = this.GetBuffer();
            int count = Math.Min(content.Length, 10);

            string result = "";

            for (int i = 0; i < count; i++)
            {
                result += (i == 0 ? "" : " ") + string.Format("{0:X2}", content[i]);
            }

            return result;
        }

        /// <summary>
        /// Deserializes an object of the specified type, recording statisics under the given
        /// keys.
        /// </summary>
        /// <param name="parameterType">The type to deserialize</param>
        /// <param name="factory">A factory that can construct objects of the given type, used for IParseables</param>
        /// <param name="parserName">The associated parser name</param>
        /// <param name="methodName">The associated method name</param>
        /// <returns>The deserialized object</returns>
        public object GetObjectWithStatistics(Type parameterType, GenericCallBackReturn<object, Type> factory, string parserName, string methodName)
        {
            int startOffset = this.ReadOffset;
            object result = this.GetObject(parameterType, factory);
            int bytes = this.ReadOffset - startOffset;

            Logger.RecordStatistic("LayerBytes", parserName, bytes);
            Logger.RecordStatistic("ArgumentSize", parameterType.FullName, bytes);
            Logger.RecordStatistic("ArgumentCount", parameterType.FullName, 1.0);

            if (result is BaseEnvelope)
            {
                bytes -= ((BaseEnvelope)result).Message.Length;  // Ignore the message contents because they will be counted later
            }

            Logger.RecordStatistic("ParseBytes" + "." + parserName, methodName, bytes);
            if (StatUtils.IsImRelated(parserName))
            {
                Logger.RecordStatistic("InterestManagement", "ParseBytes", bytes);
            }

            return result;
        }

        /// <inheritdoc />
        public void ToMessage(MessageBuffer message, Type parameterType)
        {
            if (null != message)
            {
                message.Write(this.Length);
                message.Write(this.buffer.ToArray(), this.Length);
            }
        }

        /// <inheritdoc />
        public void FromMessage(MessageBuffer message)
        {
            if (null != message)
            {
                int length = message.ReadInt();
                this.Write(message.ReadBytes(length), length);      
            }
        }

        /// <inheritdoc />
        public void Dispose()
        {
            this.buffer.Dispose();
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Encrypts the message using the given key.
        /// </summary>
        /// <param name="key">The symmetric key to use for encryption</param>
        internal void Encrypt(Badumna.Security.SymmetricKey key)
        {
            byte[] encryptedBuffer = key.Encrypt(this.GetBuffer());

            this.buffer.Seek(0, SeekOrigin.Begin);
            this.buffer.Write(encryptedBuffer, 0, encryptedBuffer.Length);
            this.Length = encryptedBuffer.Length;
        }

        /// <summary>
        /// Adds a checksum to the message using the given salts.
        /// </summary>
        /// <param name="checksumSalt">The primary salt</param>
        /// <param name="extraSalt">Extra salt to taste</param>
        internal void Checksum(byte[] checksumSalt, byte[] extraSalt)
        {
            // Firewall keep alive messages are completely empty, so this must allow for empty messages.
            if (!this.HasBeenChecksummed && this.Length > 0)
            {
                int initialReadOffset = this.ReadOffset;
                int initialWriteOffset = this.WriteOffset;
                this.WriteOffset = this.Length;
                this.Write(checksumSalt, checksumSalt.Length);
                this.Write(extraSalt, extraSalt.Length);
                ushort checksum = CRC.CRC16(this.GetBuffer(), 0, this.Length);

                this.DropLastNBytes(extraSalt.Length);
                this.DropLastNBytes(checksumSalt.Length);
                this.Write(checksum);
                this.ReadOffset = initialReadOffset;
                this.WriteOffset = initialWriteOffset;
                this.HasBeenChecksummed = true;
            }
        }

        /// <summary>
        /// Strips the checksum bytes from the message.
        /// </summary>
        internal void RemoveChecksum()
        {
            if (this.HasBeenChecksummed)
            {
                this.DropLastNBytes(2);
                this.HasBeenChecksummed = false;
            }
        }

        /// <summary>
        /// Validates the checksum on the message using the given salts.
        /// </summary>
        /// <param name="checksumSalt">The primary salt</param>
        /// <param name="extraSalt">Extra salt to taste</param>
        /// <returns>True iff the checksum is valid with respect to the given salts</returns>
        internal bool ValidateChecksum(byte[] checksumSalt, byte[] extraSalt)
        {
            try
            {
                if (this.Length <= 2)
                {
                    // No checksum or no data
                    return false;
                }

                int initialReadOffset = this.ReadOffset;
                int initialWriteOffset = this.WriteOffset;
                ushort checksumInMessage;
                ushort calculatedChecksum;

                this.ReadOffset = this.Length - 2;
                checksumInMessage = this.ReadUShort();
                this.DropLastNBytes(2);
                this.HasBeenChecksummed = false;

                this.WriteOffset = this.Length;
                this.Write(checksumSalt, checksumSalt.Length);
                this.Write(extraSalt, extraSalt.Length);

                calculatedChecksum = CRC.CRC16(this.GetBuffer(), 0, this.Length);

                this.DropLastNBytes(extraSalt.Length);
                this.DropLastNBytes(checksumSalt.Length);
                this.ReadOffset = Math.Min(initialReadOffset, this.Length);
                this.WriteOffset = Math.Min(initialWriteOffset, this.Length);

                if (checksumInMessage != calculatedChecksum)
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                Logger.TraceException(LogLevel.Information, e, "Failed to read checksum from message.");
                return false;
            }

            return true;
        }

        /// <summary>
        /// Records the bounds of an object about to be written.
        /// </summary>
        /// <param name="length">The length.</param>
        /// <param name="description">The description.</param>
        [Conditional("TRACE")]
        internal void RecordObjectBounds(int length, object description)
        {
            this.RecordObjectBounds(this.WriteOffset, this.WriteOffset + length, description);
        }

#if TRACE
        /// <summary>
        /// Describes the contents of this message's byte layout.
        /// </summary>
        /// <returns>The description</returns>
        internal string DescribeContents()
        {
            var sb = new StringBuilder();
            sb.AppendLine(String.Format("[[ Contents of MessageBuffer (length {0}) ]]", this.Length));
            foreach (var bounds in this.objectBounds)
            {
                sb.AppendLine(bounds.ToString());
            }

            sb.AppendLine(new ObjectBounds(0, this.Length, "[Entire message]").ToString());
            return sb.ToString();
        }
#endif

        /// <summary>
        /// Constructs an instance of the given type using its default
        /// constructor.
        /// </summary>
        /// <remarks>
        /// Works even if the default constructor is non-public.
        /// </remarks>
        /// <param name="type">The type to construct</param>
        /// <returns>The newly constructed object</returns>
        private static object DefaultFactory(Type type)
        {
            return Activator.CreateInstance(type, true);
        }

        /// <summary>
        /// Deserializes a parseable of the given type.
        /// </summary>
        /// <param name="type">The type of the parseable to deserialize</param>
        /// <param name="factory">A factory that can construct objects of the given type</param>
        /// <returns>The deserialized parseable</returns>
        private IParseable ReadIParseable(Type type, GenericCallBackReturn<object, Type> factory)
        {
            try
            {
                IParseable parseable = (IParseable)factory(type);
                parseable.FromMessage(this);
                return parseable;
            }
            catch (MissingMethodException e)
            {
                Logger.TraceException(LogLevel.Error, e, "Failed to create instance of {0}. It must have a parameterless constructor.", type.Name);
                throw;
            }
        }
        
        /// <summary>
        /// Deserializes an object using the builtin .NET deserialization.
        /// </summary>
        /// <returns>The deserialized object</returns>
        private object ReadObjectUsingDotNetSerialization()
        {
            this.buffer.Seek(this.ReadOffset, SeekOrigin.Begin);
            object result = MessageBuffer.formatter.Deserialize(this.buffer);
            this.readOffset = (int)this.buffer.Position;
            return result;
        }

        /// <summary>
        /// Serializes the given object using the builtin .NET serialization.
        /// </summary>
        /// <param name="obj">The object to serialize</param>
        private void WriteUsingDotNetSerialization(object obj)
        {
            this.buffer.Seek(this.WriteOffset, SeekOrigin.Begin);
            MessageBuffer.formatter.Serialize(this.buffer, obj);
            this.Length = (int)this.buffer.Length;
            this.WriteOffset = (int)this.buffer.Position;
        }

        /// <summary>
        /// Seek helper method, sets the position within the current stream to the specified value.
        /// </summary>
        /// <param name="offset">The new position within the stream.</param>
        /// <param name="loc">A value of type System.IO.SeekOrigin.</param>
        private void Seek(int offset, SeekOrigin loc)
        {
            this.buffer.Seek(offset, loc);
        }

        /// <summary>
        /// Reads helper method, reads a block of bytes from the current stream and writes the data to buffer.
        /// </summary>
        /// <param name="result">The result byte array.</param>
        /// <param name="offset">The byte offset in result at which to begin reading</param>
        /// <param name="count">The maximum number of bytes to read</param>
        /// <returns>The total number of bytes written into the result.</returns>
        private int Read(byte[] result, int offset, int count)
        {
            return this.buffer.Read(result, offset, count);
        }

        /// <summary>
        /// Write helper method, writes a block of bytes to the current stream using data read from buffer.
        /// </summary>
        /// <param name="buffer">The buffer to write data from.</param>
        /// <param name="offset">The byte offset in buffer at which to begin writing from.</param>
        /// <param name="count">The maximum number of bytes to write.</param>
        private void WriteHelper(byte[] buffer, int offset, int count)
        {
            this.buffer.Write(buffer, offset, count);
        }

        /// <summary>
        /// Records the object bounds.
        /// </summary>
        /// <param name="start">The start index.</param>
        /// <param name="end">The end index.</param>
        /// <param name="obj">The object written.</param>
        [Conditional("TRACE")]
        private void RecordObjectBounds(int start, int end, object obj)
        {
#if TRACE
            this.objectBounds.Add(new ObjectBounds(start, end, obj));
#endif
        }

#if TRACE
        /// <summary>
        /// A marker of the byte section in which a given object resides
        /// </summary>
        private class ObjectBounds : object
        {
            /// <summary>
            /// The object
            /// </summary>
            private object obj;

            /// <summary>
            /// the start index
            /// </summary>
            private int start;

            /// <summary>
            /// the end index
            /// </summary>
            private int end;

            /// <summary>
            /// Initializes a new instance of the MessageBuffer.ObjectBounds class.
            /// </summary>
            /// <param name="start">The start index.</param>
            /// <param name="end">The end index.</param>
            /// <param name="obj">The object written.</param>
            public ObjectBounds(int start, int end, object obj)
            {
                this.obj = obj;
                this.start = start;
                this.end = end;
            }

            /// <inheritdoc />
            public override string ToString()
            {
                var sb = new StringBuilder();
                var i = 0;
                for (; i < this.start; i++)
                {
                    sb.Append(' ');
                }

                for (; i < this.end; i++)
                {
                    sb.Append('-');
                }

                sb.AppendFormat(" [{0}b]", this.end - this.start);
                if (this.obj as string != null)
                {
                    sb.AppendFormat(" {0}", this.obj);
                }
                else
                {
                    sb.AppendFormat(" ({0}): {1}", this.obj.GetType(), this.obj);
                }

                return sb.ToString();
            }
        }

#endif
    }
}