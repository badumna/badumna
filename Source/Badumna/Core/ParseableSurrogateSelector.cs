﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.IO.Compression;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Reflection;
using System.Collections;

using Badumna.Core;


namespace Badumna.Core
{
    /// <summary>
    /// This class returns a serialization surrogate for any type which is IParseable.
    /// The surrogate will serialize the IParsable using IParseable.ToMesssage and
    /// deserialize using IParseable.FromMessage.
    /// </summary>
    class ParseableSurrogateSelector : SurrogateSelector
    {
        /// <summary>
        /// Serializes objects that are IParseable.
        /// </summary>
        private class ParseableSurrogate : ISerializationSurrogate
        {
            public void GetObjectData(object obj, SerializationInfo info, StreamingContext context)
            {
                IParseable parseable = (IParseable)obj;
                using (MessageBuffer tempMessage = new MessageBuffer())
                {
                    parseable.ToMessage(tempMessage, obj.GetType());
                    info.AddValue("message", tempMessage.GetBuffer(), typeof(byte[]));
                }
            }

            public object SetObjectData(object obj, SerializationInfo info, StreamingContext context, ISurrogateSelector selector)
            {
                // Invoke the constructor on the uninitialized object created by the deserializer (can't just
                // create a new instance because SetObjectData's return value is ignored by the caller).
                Type type = obj.GetType();
                if (!type.IsValueType)  // Can't get the struct default constructor through reflection, but the struct author can't override it so it doesn't matter
                {
                    ConstructorInfo cons = type.GetConstructor(
                        BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic, null, Type.EmptyTypes, null);
                    cons.Invoke(obj, null);
                }

                byte[] messageBuffer = (byte[])info.GetValue("message", typeof(byte[]));
                using (MessageBuffer tempMessage = new MessageBuffer())
                {
                    tempMessage.Write(messageBuffer, messageBuffer.Length);

                    IParseable parseable = (IParseable)obj;
                    parseable.FromMessage(tempMessage);
                }

                return obj;
            }
        }


        private ParseableSurrogate mParseableSurrogate;


        public ParseableSurrogateSelector()
        {
            this.mParseableSurrogate = new ParseableSurrogate();
        }

        public override void AddSurrogate(Type type, StreamingContext context, ISerializationSurrogate surrogate)
        {
            throw new NotSupportedException();
        }

        public override ISerializationSurrogate GetSurrogate(Type type, StreamingContext context, out ISurrogateSelector selector)
        {
            selector = this;

            if (typeof(IParseable).IsAssignableFrom(type))
            {
                return this.mParseableSurrogate;
            }

            // If the type is from a different Badumna version then it won't be castable to our
            // IParseable.  In this case we try to use the ParseableSurrogateSelector from the
            // other version.
            Type selectorType = type.Assembly.GetType("Badumna.Core.ParseableSurrogateSelector");
            if (selectorType != null && selectorType != this.GetType())
            {
                SurrogateSelector newSelector = (SurrogateSelector)Activator.CreateInstance(selectorType);
                return newSelector.GetSurrogate(type, context, out selector);
            }
            
            return base.GetSurrogate(type, context, out selector); // use base class's chaining functionality
        }
    }
}
