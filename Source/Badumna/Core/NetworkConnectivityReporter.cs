﻿//------------------------------------------------------------------------------
// <copyright file="NetworkConnectivityReporter.cs" company="National ICT Australia Limited">
//     Copyright (c) 2012 National ICT Australia Limited. All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;

using Badumna.Utilities;

namespace Badumna.Core
{
    /// <summary>
    /// The four possible states of connectivity.
    /// </summary>
    internal enum ConnectivityStatus
    {
        /// <summary>
        /// The peer is running and connected.
        /// </summary>
        Online,

        /// <summary>
        /// The peer is unable to connect with any other peers.
        /// </summary>
        Offline,

        /// <summary>
        /// The peer is initializing. The Initial state.
        /// </summary>
        Initializing,

        /// <summary>
        /// The peer is going to go offline shortly.
        /// </summary>
        ShuttingDown
    }

    /// <summary>
    /// Records the current connectivity status and triggers events when the status changes.
    /// </summary>
    /// <remarks>
    /// This class is used for system-wide notification of connectivity status changes.
    /// The current connectivity status is updated by different subsystems for a 
    /// number of different reasons.  To see how the status is changed, check the references
    /// for the SetStatus(ConnectivityStatus) method.
    /// TODO: It would be much clearer how the status changes if it were determined
    ///       directly by a relationship over the factors that affect the status, e.g. 
    ///       by a single method in this class that periodically checked all of the relevant
    ///       factors and updated the status accordingly.
    /// </remarks>
    internal class NetworkConnectivityReporter : INetworkConnectivityReporter
    {
        // Events are implemented with explicit delegate backing fields to avoid a bug encountered in Unity iOS builds.
        // See http://forum.unity3d.com/threads/113750-ExecutionEngineException-on-iOS-only for more info.

        /// <summary>
        /// Delegate for network online events.
        /// </summary>
        private GenericCallBack networkOnlineEventDelegate;

        /// <summary>
        /// Delegate for network offline events.
        /// </summary>
        private GenericCallBack networkOfflineEventDelegate;

        /// <summary>
        /// Delegate for network shutting down events.
        /// </summary>
        private GenericCallBack networkShuttingDownEventDelegate;

        /// <summary>
        /// Delegate for network initializing events.
        /// </summary>
        private GenericCallBack networkInitializingEventDelegate;

        /// <summary>
        /// Event queue, for the perform lock.
        /// </summary>
        private NetworkEventQueue eventQueue;

        /// <summary>
        /// Initializes a new instance of the NetworkConnectivityReporter class.
        /// </summary>
        /// <param name="eventQueue">The event queue</param>
        public NetworkConnectivityReporter(NetworkEventQueue eventQueue)
        {
            this.eventQueue = eventQueue;
            this.Status = ConnectivityStatus.Initializing;
        }

        /// <inheritdoc />
        public event GenericCallBack NetworkOnlineEvent
        {
            add { this.networkOnlineEventDelegate += value; }
            remove { this.networkOnlineEventDelegate -= value; }
        }

        /// <inheritdoc />
        public event GenericCallBack NetworkOfflineEvent
        {
            add { this.networkOfflineEventDelegate += value; }
            remove { this.networkOfflineEventDelegate -= value; }
        }

        /// <inheritdoc />
        public event GenericCallBack NetworkShuttingDownEvent
        {
            add { this.networkShuttingDownEventDelegate += value; }
            remove { this.networkShuttingDownEventDelegate -= value; }
        }

        /// <inheritdoc />
        public event GenericCallBack NetworkInitializingEvent
        {
            add { this.networkInitializingEventDelegate += value; }
            remove { this.networkInitializingEventDelegate -= value; }
        }

        /// <inheritdoc />
        public ConnectivityStatus Status { get; private set; }

        /// <inheritdoc />
        public void SetStatus(ConnectivityStatus status)
        {
            this.eventQueue.AcquirePerformLock();
            try
            {
                if (this.Status == status)
                {
                    Logger.TraceInformation(LogTag.Core, "Attempted to set status to {0} while already in this state", status);
                    return;
                }

                this.Status = status;

                Logger.TraceInformation(LogTag.Core, "Network status changed to {0}", this.Status.ToString());

                if (status == ConnectivityStatus.Online && null != this.networkOnlineEventDelegate)
                {
                    this.networkOnlineEventDelegate.Invoke();
                }

                if (status == ConnectivityStatus.Offline && null != this.networkOfflineEventDelegate)
                {
                    this.networkOfflineEventDelegate.Invoke();
                }

                if (status == ConnectivityStatus.ShuttingDown && null != this.networkShuttingDownEventDelegate)
                {
                    this.networkShuttingDownEventDelegate.Invoke();
                }

                if (status == ConnectivityStatus.Initializing && null != this.networkInitializingEventDelegate)
                {
                    this.networkInitializingEventDelegate.Invoke();
                }
            }
            catch (Exception e)
            {
                // TODO: Should probably remove this catch-all.
                Logger.TraceException(LogLevel.Error, e, "Exception raised by NetworkConnectivityReporter event failureHandler");
            }
            finally
            {
                this.eventQueue.ReleasePerformLock();
            }
        }
    }
}
