using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;
using System.Net;

using Badumna.Utilities;

namespace Badumna.Core
{
    /// <summary>
    /// Indicates that tagged element should not be taken into account when performing coverage analysis.
    /// </summary>
    /// <remarks>
    /// This attribute is intended for internal use only and may change or be removed in the future.
    /// </remarks>
    [AttributeUsage(AttributeTargets.All)]
    public sealed class DontPerformCoverage : Attribute { }

    /// <summary>
    /// An exception thrown by the Badumna library; the root of the Badumna exception hierarchy.
    /// </summary>
    [Serializable]
    public class BadumnaException : Exception
    {        
        internal BadumnaException() 
            : base() { }

        internal BadumnaException(String message) 
            : base(message) { }

        internal BadumnaException(String message, Exception innerException) : base(message, innerException) { }

        internal BadumnaException(SerializationInfo serializationInfo, StreamingContext streamingContext)
            : base(serializationInfo, streamingContext) {}
                
        internal virtual void TraceMessage()
        {
            Logger.TraceException(LogLevel.Error, this, "{0}", this.Message);
        }
    }

    [Serializable]
    class CoreException : BadumnaException
    {
        public CoreException() 
            : base() { }
        
        public CoreException(String message) 
            : base(message) { }

        public CoreException(String message, Exception innerException) : base(message, innerException) { }

        protected CoreException(SerializationInfo serializationInfo, StreamingContext streamingContext)
            : base(serializationInfo, streamingContext) {}
 
    }

    /// <summary>
    /// This exception is thrown when any tunnelled request fails.  When tunnelling is enabled, any
    /// request on the Badumna API may throw this exception if the request does not complete
    /// successfully.  The InnerException may provide additional information about the failure.
    /// </summary>
    [Serializable]
    public sealed class TunnelRequestException : BadumnaException
    {
        internal TunnelRequestException() 
            : this("Tunnel request failed.")
        {
        }

        internal TunnelRequestException(String message)
            : this(message, null)
        {
        }

        internal TunnelRequestException(String message, Exception innerException)
            : base(message, innerException)
        {
        }

        private TunnelRequestException(SerializationInfo serializationInfo, StreamingContext streamingContext)
            : base(serializationInfo, streamingContext)
        {
        }
    }

    [Serializable]
    class EndOfMessageException : CoreException
    {
        public EndOfMessageException()
            : base(Resources.EndOfMessageException_Message) { }
        
        public EndOfMessageException(String message) 
            : base(message) { }

        public EndOfMessageException(String message, Exception innerException) 
            : base(message, innerException) { }

        protected EndOfMessageException(SerializationInfo serializationInfo, StreamingContext streamingContext)
            : base(serializationInfo, streamingContext) {}
        
    }

    [Serializable]
    class ForgeMessageException : CoreException
    {
        public ForgeMessageException()
            : base(Resources.ForgeException_Message) { }

        public ForgeMessageException(String message) 
            : base(message) { }

        public ForgeMessageException(String message, Exception innerException) 
            : base(message, innerException) { }

        protected ForgeMessageException(SerializationInfo serializationInfo, StreamingContext streamingContext)
            : base(serializationInfo, streamingContext) {}
        
    }

    [Serializable]
    class UnknownMethodException : CoreException
    {
        public UnknownMethodException()
            : base(Resources.UnknownDelegate_Message) { }

        public UnknownMethodException(String message) 
            : base(message) { }

        public UnknownMethodException(String message, Exception innerException) 
            : base(message, innerException) { }

        protected UnknownMethodException(SerializationInfo serializationInfo, StreamingContext streamingContext)
            : base(serializationInfo, streamingContext) {}
        
    }

    [Serializable]
    class ParseableMethodException : CoreException
    {
        public ParseableMethodException()
            : base(Resources.ParseableMethod_Message){}

        public ParseableMethodException(String message) 
            : base(message) { }

        public ParseableMethodException(String message, Exception innerException)
            : base(message, innerException) { }

        protected ParseableMethodException(SerializationInfo serializationInfo, StreamingContext streamingContext)
            : base(serializationInfo, streamingContext) {}
        
    }

    [Serializable]
    class ParseException : CoreException
    {
        public ParseException() 
            : base() { }

        public ParseException(String message)
            : base(message) { }

        public ParseException(String message, Exception innerException) 
            : base(message, innerException) { }

        protected ParseException(SerializationInfo serializationInfo, StreamingContext streamingContext)
            : base(serializationInfo, streamingContext) {}

    }

    /// <summary>
    /// An exception thrown if there is an issue with the configuration.  
    /// </summary>
    [Serializable]
    public class ConfigurationException : BadumnaException
    {
        /// <summary>
        /// The possible types of configuration errors.
        /// </summary>
        public enum ErrorCode : byte
        {
            /// <summary>
            /// An unspecified error occured.  The Message property may contain further details.
            /// </summary>
            Unspecified,

            /// <summary>
            /// A tunnel server URI was supplied with a scheme that was not 'http' or 'https'.
            /// </summary>
            InvalidTunnelUriScheme,

            /// <summary>
            /// Tunnelling is enabled but the list of tunnel server URIs is empty.
            /// </summary>
            NoTunnelServersDefined,

            /// <summary>
            /// Two or more of the defined arbitration servers have the same name.
            /// </summary>
            DuplicateArbitrationServer,

            /// <summary>
            /// The XML given does not match the expected format.
            /// </summary>
            InvalidXml,

            /// <summary>
            /// Parts of the configuration are inconsistent with each other.
            /// </summary>
            Inconsistent,

            /// <summary>
            /// An error related to the logging configuration occurred.  The Message property may contain further details.
            /// </summary>
            LoggingConfigurationError,

            /// <summary>
            /// The URI string indicated by the Message property could not be parsed as a URI.
            /// </summary>
            InvalidUri,

            /// <summary>
            /// An error relating to file access occurred.
            /// </summary>
            FileError,

            /// <summary>
            /// The port is not an integer in the range 0-65535.
            /// </summary>
            InvalidPort,

            /// <summary>
            /// Invalid application name.
            /// </summary>
            InvalidApplicationName,

            /// <summary>
            /// No matchmaking server address was specified, no port was specified,
            /// or the specified address could not be resolved.
            /// </summary>
            InvalidMatchmakingServerAddress,
        }

        /// <summary>
        /// Gets a value that indicates the type of configuration error that occurred.
        /// </summary>
        public ErrorCode Error { get; private set; }

        /// <summary>
        /// Constructs a new instance of the ConfigurationException class.
        /// </summary>
        /// <param name="errorCode">The error code describing the cause of the exception.</param>
        /// <param name="message">Extra information specific to the current error code.</param>
        /// <param name="innerException">The inner exception.</param>
        public ConfigurationException(ErrorCode errorCode, string message, Exception innerException)
            : base(message, innerException)
        {
            this.Error = errorCode;
        }

        /// <summary>
        /// Constructs a new instance of the ConfigurationException class.
        /// </summary>
        /// <param name="errorCode">The error code describing the cause of the exception.</param>
        /// <param name="message">Extra information specific to the current error code.</param>
        public ConfigurationException(ErrorCode errorCode, string message)
            : base(message)
        {
            this.Error = errorCode;
        }

        /// <summary>
        /// Constructs a new instance of the ConfigurationException class.
        /// </summary>
        /// <param name="errorCode">The error code describing the cause of the exception.</param>
        public ConfigurationException(ErrorCode errorCode)
            : base()
        {
            this.Error = errorCode;
        }

        #region Traditional exception constructors

        /// <summary>
        /// Constructs a new instance of the ConfigurationException class.
        /// </summary>
        public ConfigurationException()
            : this(ErrorCode.Unspecified)
        {
        }

        /// <summary>
        /// Constructs a new instance of the ConfigurationException class with <see cref="Error"/>
        /// set to <see cref="ErrorCode.Unspecified"/>.
        /// </summary>
        /// <param name="message">A message describing the nature of the exception.</param>
        public ConfigurationException(string message)
            : this(ErrorCode.Unspecified, message)
        {
        }

        /// <summary>
        /// Constructs a new instance of the ConfigurationException class with <see cref="Error"/>
        /// set to <see cref="ErrorCode.Unspecified"/>.
        /// </summary>
        /// <param name="message">A message describing the nature of the exception.</param>
        /// <param name="innerException">The inner exception.</param>
        public ConfigurationException(string message, Exception innerException)
            : this(ErrorCode.Unspecified, message, innerException)
        {
        }

        /// <summary>
        /// Constructs a new instance of the ConfigurationException class by deserialization.
        /// </summary>
        /// <param name="serializationInfo">The serialized exception information.</param>
        /// <param name="streamingContext">The streaming context.</param>
        protected ConfigurationException(SerializationInfo serializationInfo, StreamingContext streamingContext)
            : base(serializationInfo, streamingContext)
        {
            this.Error = (ErrorCode)serializationInfo.GetByte("Error");
        }

        #endregion

        /// <summary>
        /// Serializes the exception.
        /// </summary>
        /// <param name="info">The instance used to store the serialized data.</param>
        /// <param name="context">The streaming context.</param>
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
            info.AddValue("Error", (byte)this.Error);
        }
    }

    /// <summary>
    /// An exception thrown if there is an issue with connectivity.
    /// </summary>
    [Serializable]
    public class ConnectivityException : BadumnaException
    {
        /// <summary>
        /// The possible types of connectivity errors.
        /// </summary>
        public enum ErrorCode : byte
        {
            /// <summary>
            /// An unspecified error occured.  The Message property may contain further details.
            /// </summary>
            Unspecified,

            /// <summary>
            /// The peer does not receive responses to its STUN requests, UDP traffic appears to be blocked.
            /// </summary>
            UdpBlocked,
        }

        /// <summary>
        /// Gets a value that indicates the type of connectivity error that occurred.
        /// </summary>
        public ErrorCode Error { get; private set; }

        /// <summary>
        /// Constructs a new instance of the ConnectivityException class.
        /// </summary>
        /// <param name="errorCode">The error code describing the cause of the exception.</param>
        /// <param name="message">Extra information specific to the current error code.</param>
        /// <param name="innerException">The inner exception.</param>
        public ConnectivityException(ErrorCode errorCode, string message, Exception innerException)
            : base(message, innerException)
        {
            this.Error = errorCode;
        }

        /// <summary>
        /// Constructs a new instance of the ConnectivityException class.
        /// </summary>
        /// <param name="errorCode">The error code describing the cause of the exception.</param>
        /// <param name="message">Extra information specific to the current error code.</param>
        public ConnectivityException(ErrorCode errorCode, string message)
            : base(message)
        {
            this.Error = errorCode;
        }

        /// <summary>
        /// Constructs a new instance of the ConnectivityException class.
        /// </summary>
        /// <param name="errorCode">The error code describing the cause of the exception.</param>
        public ConnectivityException(ErrorCode errorCode)
            : base()
        {
            this.Error = errorCode;
        }

        #region Traditional exception constructors

        /// <summary>
        /// Constructs a new instance of the ConnectivityException class.
        /// </summary>
        public ConnectivityException()
            : this(ErrorCode.Unspecified)
        {
        }

        /// <summary>
        /// Constructs a new instance of the ConnectivityException class with <see cref="Error"/>
        /// set to <see cref="ErrorCode.Unspecified"/>.
        /// </summary>
        /// <param name="message">A message describing the nature of the exception.</param>
        public ConnectivityException(string message)
            : this(ErrorCode.Unspecified, message)
        {
        }

        /// <summary>
        /// Constructs a new instance of the ConnectivityException class with <see cref="Error"/>
        /// set to <see cref="ErrorCode.Unspecified"/>.
        /// </summary>
        /// <param name="message">A message describing the nature of the exception.</param>
        /// <param name="innerException">The inner exception.</param>
        public ConnectivityException(string message, Exception innerException)
            : this(ErrorCode.Unspecified, message, innerException)
        {
        }

        /// <summary>
        /// Constructs a new instance of the ConnectivityException class by deserialization.
        /// </summary>
        /// <param name="serializationInfo">The serialized exception information.</param>
        /// <param name="streamingContext">The streaming context.</param>
        protected ConnectivityException(SerializationInfo serializationInfo, StreamingContext streamingContext)
            : base(serializationInfo, streamingContext)
        {
            this.Error = (ErrorCode)serializationInfo.GetByte("Error");
        }

        #endregion

        /// <summary>
        /// Serializes the exception.
        /// </summary>
        /// <param name="info">The instance used to store the serialized data.</param>
        /// <param name="context">The streaming context.</param>
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
            info.AddValue("Error", (byte)this.Error);
        }
    }
}
