//-----------------------------------------------------------------------
// <copyright file="INetworkEventScheduler.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2010 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace Badumna.Core
{
    using Badumna.Utilities;

    /// <summary>
    /// A interface for scheduling an ordered and timed queue of network events.
    /// </summary>
    internal interface INetworkEventScheduler
    {
        /// <summary>
        /// Schedule a network event to be actioned after a specified delay.
        /// </summary>
        /// <param name="millisecondsFromNow">The delay in milliseconds.</param>
        /// <param name="ev">The event.</param>
        void Schedule(long millisecondsFromNow, NetworkEvent ev);
        
        /// <summary>
        /// Schedule a call back to be invoked after a specified delay.
        /// </summary>
        /// <param name="millisecondsFromNow">The delay in milliseconds.</param>
        /// <param name="function">The callback.</param>
        /// <returns>A network event encapsulating the scheduled callback.</returns>
        NetworkEvent Schedule(double millisecondsFromNow, GenericCallBack function);

        /// <summary>
        /// Schedule a call back to be invoked after a specified delay.
        /// </summary>
        /// <typeparam name="A">The type of the argument for the callback.</typeparam>
        /// <param name="millisecondsFromNow">The delay in milliseconds.</param>
        /// <param name="function">The callback.</param>
        /// <param name="a">The argument for the call back.</param>
        /// <returns>A network event encapsulating the scheduled callback.</returns>
        NetworkEvent Schedule<A>(double millisecondsFromNow, GenericCallBack<A> function, A a);

        /// <summary>
        /// Schedule a call back to be invoked after a specified delay.
        /// </summary>
        /// <typeparam name="A">The type of the first argument for the callback.</typeparam>
        /// <typeparam name="B">The type of the second argument for the callback.</typeparam>
        /// <param name="millisecondsFromNow">The delay in milliseconds.</param>
        /// <param name="function">The callback.</param>
        /// <param name="a">The first argument for the call back.</param>
        /// <param name="b">The second argument for the call back.</param>
        /// <returns>A network event encapsulating the scheduled callback.</returns>
        NetworkEvent Schedule<A, B>(double millisecondsFromNow, GenericCallBack<A, B> function, A a, B b);

        /// <summary>
        /// Schedule a call back to be invoked after a specified delay.
        /// </summary>
        /// <typeparam name="A">The type of the first argument for the callback.</typeparam>
        /// <typeparam name="B">The type of the second argument for the callback.</typeparam>
        /// <typeparam name="C">The type of the third argument for the callback.</typeparam>
        /// <param name="millisecondsFromNow">The delay in milliseconds.</param>
        /// <param name="function">The callback.</param>
        /// <param name="a">The first argument for the call back.</param>
        /// <param name="b">The second argument for the call back.</param>
        /// <param name="c">The third argument for the call back.</param>
        /// <returns>A network event encapsulating the scheduled callback.</returns>
        NetworkEvent Schedule<A, B, C>(double millisecondsFromNow, GenericCallBack<A, B, C> function, A a, B b, C c);

        /// <summary>
        /// Schedule a call back to be invoked after a specified delay.
        /// </summary>
        /// <typeparam name="A">The type of the first argument for the callback.</typeparam>
        /// <typeparam name="B">The type of the second argument for the callback.</typeparam>
        /// <typeparam name="C">The type of the third argument for the callback.</typeparam>
        /// <typeparam name="D">The type of the fourth argument for the callback.</typeparam>
        /// <param name="millisecondsFromNow">The delay in milliseconds.</param>
        /// <param name="function">The callback.</param>
        /// <param name="a">The first argument for the call back.</param>
        /// <param name="b">The second argument for the call back.</param>
        /// <param name="c">The third argument for the call back.</param>
        /// <param name="d">The fourth argument for the call back.</param>
        /// <returns>A network event encapsulating the scheduled callback.</returns>
        NetworkEvent Schedule<A, B, C, D>(double millisecondsFromNow, GenericCallBack<A, B, C, D> function, A a, B b, C c, D d);

        /// <summary>
        /// Schedule a call back to be invoked after a specified delay.
        /// </summary>
        /// <typeparam name="A">The type of the first argument for the callback.</typeparam>
        /// <typeparam name="B">The type of the second argument for the callback.</typeparam>
        /// <typeparam name="C">The type of the third argument for the callback.</typeparam>
        /// <typeparam name="D">The type of the fourth argument for the callback.</typeparam>
        /// <typeparam name="E">The type of the fifth argument for the callback.</typeparam>
        /// <param name="millisecondsFromNow">The delay in milliseconds.</param>
        /// <param name="function">The callback.</param>
        /// <param name="a">The first argument for the call back.</param>
        /// <param name="b">The second argument for the call back.</param>
        /// <param name="c">The third argument for the call back.</param>
        /// <param name="d">The fourth argument for the call back.</param>
        /// <param name="e">The fifth argument for the call back.</param>
        /// <returns>A network event encapsulating the scheduled callback.</returns>
        NetworkEvent Schedule<A, B, C, D, E>(double millisecondsFromNow, GenericCallBack<A, B, C, D, E> function, A a, B b, C c, D d, E e);

        /// <summary>
        /// Schedule a call back to be invoked immediately.
        /// </summary>
        /// <param name="function">The callback.</param>
        /// <returns>A network event encapsulating the scheduled callback.</returns>
        NetworkEvent Push(GenericCallBack function);

        /// <summary>
        /// Schedule a call back to be invoked immediately.
        /// </summary>
        /// <typeparam name="A">The type of the argument for the callback.</typeparam>
        /// <param name="function">The callback.</param>
        /// <param name="a">The argument for the call back.</param>
        /// <returns>A network event encapsulating the scheduled callback.</returns>
        NetworkEvent Push<A>(GenericCallBack<A> function, A a);

        /// <summary>
        /// Schedule a call back to be invoked immediately.
        /// </summary>
        /// <typeparam name="A">The type of the first argument for the callback.</typeparam>
        /// <typeparam name="B">The type of the second argument for the callback.</typeparam>
        /// <param name="function">The callback.</param>
        /// <param name="a">The first argument for the call back.</param>
        /// <param name="b">The second argument for the call back.</param>
        /// <returns>A network event encapsulating the scheduled callback.</returns>
        NetworkEvent Push<A, B>(GenericCallBack<A, B> function, A a, B b);

        /// <summary>
        /// Schedule a call back to be invoked immediately.
        /// </summary>
        /// <typeparam name="A">The type of the first argument for the callback.</typeparam>
        /// <typeparam name="B">The type of the second argument for the callback.</typeparam>
        /// <typeparam name="C">The type of the third argument for the callback.</typeparam>
        /// <param name="function">The callback.</param>
        /// <param name="a">The first argument for the call back.</param>
        /// <param name="b">The second argument for the call back.</param>
        /// <param name="c">The third argument for the call back.</param>
        /// <returns>A network event encapsulating the scheduled callback.</returns>
        NetworkEvent Push<A, B, C>(GenericCallBack<A, B, C> function, A a, B b, C c);

        /// <summary>
        /// Schedule a call back to be invoked immediately.
        /// </summary>
        /// <typeparam name="A">The type of the first argument for the callback.</typeparam>
        /// <typeparam name="B">The type of the second argument for the callback.</typeparam>
        /// <typeparam name="C">The type of the third argument for the callback.</typeparam>
        /// <typeparam name="D">The type of the fourth argument for the callback.</typeparam>
        /// <param name="function">The callback.</param>
        /// <param name="a">The first argument for the call back.</param>
        /// <param name="b">The second argument for the call back.</param>
        /// <param name="c">The third argument for the call back.</param>
        /// <param name="d">The fourth argument for the call back.</param>
        /// <returns>A network event encapsulating the scheduled callback.</returns>
        NetworkEvent Push<A, B, C, D>(GenericCallBack<A, B, C, D> function, A a, B b, C c, D d);

        /// <summary>
        /// Schedule a call back to be invoked immediately.
        /// </summary>
        /// <typeparam name="A">The type of the first argument for the callback.</typeparam>
        /// <typeparam name="B">The type of the second argument for the callback.</typeparam>
        /// <typeparam name="C">The type of the third argument for the callback.</typeparam>
        /// <typeparam name="D">The type of the fourth argument for the callback.</typeparam>
        /// <typeparam name="E">The type of the fifth argument for the callback.</typeparam>
        /// <param name="function">The callback.</param>
        /// <param name="a">The first argument for the call back.</param>
        /// <param name="b">The second argument for the call back.</param>
        /// <param name="c">The third argument for the call back.</param>
        /// <param name="d">The fourth argument for the call back.</param>
        /// <param name="e">The fifth argument for the call back.</param>
        /// <returns>A network event encapsulating the scheduled callback.</returns>
        NetworkEvent Push<A, B, C, D, E>(GenericCallBack<A, B, C, D, E> function, A a, B b, C c, D d, E e);

        /// <summary>
        /// Schedule a discardable call back to be invoked immediately.
        /// </summary>
        /// <param name="function">The callback.</param>
        /// <returns>A network event encapsulating the scheduled callback.</returns>
        NetworkEvent PushDiscardable(GenericCallBack function);

        /// <summary>
        /// Schedule a discardable call back to be invoked immediately.
        /// </summary>
        /// <typeparam name="A">The type of the argument for the callback.</typeparam>
        /// <param name="function">The callback.</param>
        /// <param name="a">The argument for the call back.</param>
        /// <returns>A network event encapsulating the scheduled callback.</returns>
        NetworkEvent PushDiscardable<A>(GenericCallBack<A> function, A a);

        /// <summary>
        /// Schedule a discardable call back to be invoked immediately.
        /// </summary>
        /// <typeparam name="A">The type of the first argument for the callback.</typeparam>
        /// <typeparam name="B">The type of the second argument for the callback.</typeparam>
        /// <param name="function">The callback.</param>
        /// <param name="a">The first argument for the call back.</param>
        /// <param name="b">The second argument for the call back.</param>
        /// <returns>A network event encapsulating the scheduled callback.</returns>
        NetworkEvent PushDiscardable<A, B>(GenericCallBack<A, B> function, A a, B b);

        /// <summary>
        /// Schedule a discardable call back to be invoked immediately.
        /// </summary>
        /// <typeparam name="A">The type of the first argument for the callback.</typeparam>
        /// <typeparam name="B">The type of the second argument for the callback.</typeparam>
        /// <typeparam name="C">The type of the third argument for the callback.</typeparam>
        /// <param name="function">The callback.</param>
        /// <param name="a">The first argument for the call back.</param>
        /// <param name="b">The second argument for the call back.</param>
        /// <param name="c">The third argument for the call back.</param>
        /// <returns>A network event encapsulating the scheduled callback.</returns>
        NetworkEvent PushDiscardable<A, B, C>(GenericCallBack<A, B, C> function, A a, B b, C c);

        /// <summary>
        /// Schedule a discardable call back to be invoked immediately.
        /// </summary>
        /// <typeparam name="A">The type of the first argument for the callback.</typeparam>
        /// <typeparam name="B">The type of the second argument for the callback.</typeparam>
        /// <typeparam name="C">The type of the third argument for the callback.</typeparam>
        /// <typeparam name="D">The type of the fourth argument for the callback.</typeparam>
        /// <param name="function">The callback.</param>
        /// <param name="a">The first argument for the call back.</param>
        /// <param name="b">The second argument for the call back.</param>
        /// <param name="c">The third argument for the call back.</param>
        /// <param name="d">The fourth argument for the call back.</param>
        /// <returns>A network event encapsulating the scheduled callback.</returns>
        NetworkEvent PushDiscardable<A, B, C, D>(GenericCallBack<A, B, C, D> function, A a, B b, C c, D d);

        /// <summary>
        /// Schedule a discardable call back to be invoked immediately.
        /// </summary>
        /// <typeparam name="A">The type of the first argument for the callback.</typeparam>
        /// <typeparam name="B">The type of the second argument for the callback.</typeparam>
        /// <typeparam name="C">The type of the third argument for the callback.</typeparam>
        /// <typeparam name="D">The type of the fourth argument for the callback.</typeparam>
        /// <typeparam name="E">The type of the fifth argument for the callback.</typeparam>
        /// <param name="function">The callback.</param>
        /// <param name="a">The first argument for the call back.</param>
        /// <param name="b">The second argument for the call back.</param>
        /// <param name="c">The third argument for the call back.</param>
        /// <param name="d">The fourth argument for the call back.</param>
        /// <param name="e">The fifth argument for the call back.</param>
        /// <returns>A network event encapsulating the scheduled callback.</returns>
        NetworkEvent PushDiscardable<A, B, C, D, E>(GenericCallBack<A, B, C, D, E> function, A a, B b, C c, D d, E e);

        /// <summary>
        /// Remove a previously scheduled event.
        /// </summary>
        /// <param name="ev">The network event to remove.</param>
        void Remove(NetworkEvent ev);
    }
}