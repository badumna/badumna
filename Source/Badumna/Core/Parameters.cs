using System;
using System.Collections.Generic;
using System.Text;

using Badumna.DataTypes;

namespace Badumna.Core
{
    class Parameters
    {
        /// <summary>
        /// Returns a set of bytes that uniquely identifies the protocol version.
        /// </summary>
        /// <remarks>
        /// Returned from a function rather than stored as a static field so its value
        /// can't be accidentally mutated.
        /// The value should be changed to a new one whenever there are significant changes to the protocol. 
        /// Peers with different *final* protocol hash will never be able to communicate with each other.
        /// The final protocol hash is computed as function of the default protocol hash and the application name.
        /// TODO: Check how likely we are to have a collision in the final protocol hash space.
        /// Previous default hash values (4 bytes).
        ///  - 1.4 development, 0x0f, 0xee, 0xf0, 0xee
        ///  - 1.4 RC,          0x0f, 0xee, 0xab, 0xee
        ///  - 2.0 development, 0x0f, 0xee, 0x1a, 0x4f
        ///  - post 2.0 development, 0x0f, 0xee, 0x1b, 0x4f
        ///  - post 2.0 development, 0x0f, 0xee, 0x1c, 0x4f
        ///  - post 2.1.1 development - removed reserved method indexes from MessageParser, 0x0f, 0xee, 0x1d, 0x4f
        ///  - post 2.1.1 development - removed protocol version from ProcessInitialMessage, 0x0f, 0xee, 0x1e, 0x4f
        ///  - post 2.3 development - removed CongestionFeedback as separate message, 0x0f, 0xee, 0x1f, 0x4f
        ///  - eliminated mechanism preventing communication between obfuscated and unobfuscated version, 0xf0, 0x11, 0xe0, 0xb0 (this is just the complement of the old protocol hash, which is what the obfuscated version used; using this means that new builds will work with existing release builds).
        /// </remarks>
        /// <returns>The protocol hash</returns>
        public static byte[] GetDefaultProtocolHash()
        {
            return new byte[] { 0xf0, 0x11, 0xe0, 0xb0 };
        }

        #region Distributed controller

        /// <summary>
        /// The time to live value for distributed controller's initializer replica.
        /// </summary>
        public static readonly TimeSpan DistributedControllerInitializerReplicaTTL = TimeSpan.FromSeconds(60);

        /// <summary>
        /// Time interval between two distributed controller's initializer renewal operations.
        /// </summary>
        public static readonly TimeSpan DistributedControllerRenewalTaskInterval = TimeSpan.FromSeconds(15);
        
        /// <summary>
        /// The time to live value for distributed controller's checkpoint data. 
        /// </summary>
        public static readonly TimeSpan DistributedControllerCheckpointImageTTL = TimeSpan.FromSeconds(90);

        /// <summary>
        /// The max size of distributed controller's checkpoint image. 
        /// </summary>
        public static readonly ushort DistributedControllerMaxCheckpointSize = 4096;

        /// <summary>
        /// The max number of distributed controllers that can be started on the local peer. 
        /// </summary>
        public static readonly ushort MaximumNumberOfDistributedController = 64;

        #endregion 

        #region Interest management

        /// <summary>
        /// The default time to periodically remove empty stores from the IM server. 
        /// </summary>
        public static readonly TimeSpan DefaultStoreGarbageCollectionInterval = TimeSpan.FromMinutes(10);

        /// <summary>
        /// Each cell server is mapped to TotalNumberOfRedundantCellServers peers for redundancy purpose. 
        /// </summary>
        public static readonly int TotalNumberOfRedundantCellServers = 2;

        /// <summary>
        /// The default time to live for interest management service subscriptions.
        /// </summary>
        public static readonly TimeSpan DefaultSubscriptionTTL = TimeSpan.FromSeconds(60);

        /// <summary>
        /// The default time to synchronize the actual region and the subscribed region. 
        /// </summary>
        public static readonly TimeSpan MaxRegionSubscriptionSyncInterval = TimeSpan.FromSeconds(10);

        /// <summary>
        /// The maximum data rate (bytes/second) to use in gossip IM.
        /// </summary>
        public const double MaximumGossipBandwidth = 125.0f; 

        /// <summary>
        /// The number of known regions that will result in 1.0 probability for modify requests to the Dht ims.
        /// Also it is the number of calls made to a cell in the dht ims on average regardless of the number of peers. (Per gossip cluster)
        /// </summary>
        public const int GossipThreshold = 25; 

        #endregion

        #region Entity management

        /// <summary>
        /// Time in seconds between garbage collections on the entity manager. This garbage collection is
        /// responsible for disposing old entities.
        /// </summary>
        public static readonly TimeSpan EntityManagerGarbageCollectionPeriod = TimeSpan.FromSeconds(10.0);

        /// <summary>
        /// The period of time between flow control calculations and the delay for flow control messages.
        /// </summary>
        public static readonly TimeSpan FlowControlSendPeriod = TimeSpan.FromSeconds(15);

        /// <summary>
        /// The minimum feedback rate allowed. This value ensure the sender of the entity updates can at least
        /// send about 1 update per second. 
        /// </summary>
        public static readonly ushort MinFlowControlFeedbackBps = 128;

        /// <summary>
        /// After sending an instantiation request, any further instantiation requests will be suppressed
        /// until at least this much time has passed.
        /// </summary>
        public static readonly TimeSpan MinimumInstantiationRequestInterval = TimeSpan.FromSeconds(1);

        /// <summary>
        /// The limit of bandwidth usage for inbound updates.
        /// </summary>
        public const double MaximumUpdateDownloadRateBps = 100000;

        /// <summary>
        /// The minimum acceptable allocation of inbound bandwidth that can service an entity. 
        /// If other priotities cause the rate to fall below this amount the actual allocated amount will be 0.
        /// </summary>
        public const double MinimumUpdateRatePerEntityBps = 0;

        /// <summary>
        /// The period of time that a peer sending updates waits for flow control feedback messages before 
        /// declaring that the peer is lost (and consequantly stops sending updates to).
        /// Should be some multiple of FlowControlSendPeriod to allow for lost packets 
        /// (e.g. if 3 times larger it allows 3 lost packets)
        /// </summary>
        public static readonly TimeSpan FlowControlTimeout = TimeSpan.FromSeconds(45);

        /// <summary>
        /// The approximate number of bytes overhead in entity messages
        /// </summary>
        public const int EntityMessageOverhead = 14; // Just a guess

        /// <summary>
        /// The amount of time a network entity is considered alive since it last received an update.
        /// </summary>
        public static readonly TimeSpan ReplicaTimeToLive = TimeSpan.FromSeconds(30);

        /// <summary>
        /// The longest period of time that an entity can go without sending an update.
        /// </summary>
        public static readonly TimeSpan MinimumUpdateRate = TimeSpan.FromSeconds(6);

        public static readonly TimeSpan MaximumUpdateRate = TimeSpan.FromMilliseconds(100);

        public static readonly ushort MaximumDCNPCUpdateRate = 256;

        /// <summary>
        /// If we're dropping more than this portion of generated updates then we'll move that
        /// destination to the overload peer.
        /// </summary>
        public const double TriggerOnOverloadDropRate = 0.4;

        /// <summary>
        /// If we would drop less than this portion of generated updates then we'll move that
        /// destination back to direct sending.
        /// </summary>
        public const double TriggerOffOverloadDropRate = 0.25;

        /// <summary>
        /// The interval between checks on a custom message queue to ignore messages that haven't arrived and continue processing queue messages.
        /// </summary>
        public static readonly TimeSpan CustomMessageCleanupRate = TimeSpan.FromSeconds(10);

        public const int NumberOfDeadReckoningAdjustments = 10;

        public static readonly TimeSpan SegmentAckDelayTime = TimeSpan.FromSeconds(1);     // TODO: Tuning of these values!
        public static readonly TimeSpan SegmentResendDelayTime = TimeSpan.FromSeconds(3);  // TODO: Tuning of these values!

        /// <summary>
        /// The min runtime to stay in the overload mode. 
        /// </summary>
        public static readonly TimeSpan OverloadModeMinDuration = TimeSpan.FromMinutes(3);

        /// <summary>
        /// The max runtime to stay in the switching back mode. 
        /// </summary>
        public static readonly TimeSpan SwitchingBackModeMaxDuration = TimeSpan.FromMinutes(1);

        #endregion

        #region Priority

        /// <summary>
        /// The amount of smoothing that is applied to the bandwidth estimation. Higher values will result in greater smoothing,
        /// which means the estimate will be less susceptible to short burst but also slowwer to converge to the average.  Measured
        /// in seconds to 99% of input after step change.
        /// </summary>
        public static readonly TimeSpan BandwidthEstimateSettlingTime = TimeSpan.FromSeconds(4.5);

        /// <summary>
        /// The amount of time that is used to average the bandwidth over for the estimation. Low values will cause the
        /// estimation to have greater variability while high values will lessen how much the estimation reflects the instantaneous bandwidth.
        /// Must be greater than 0.
        /// </summary>
        public static readonly TimeSpan BandwidthSamplingInterval = TimeSpan.FromMilliseconds(750);

        #endregion

        #region Transport

        /// <summary>
        /// The default start of the port range.
        /// </summary>
        public const int DefaultStartPort = 21300;

        /// <summary>
        /// The default end of the port range.
        /// </summary>
        public const int DefaultEndPort = 21399;

        /// <summary>
        /// Default port for broadcast traffic.
        /// </summary>
        public const int DefaultBroadcastPort = 21250;

        /// <summary>
        /// The amount of time since the last packet was sent before we send an empty packet to keep stateful firewalls open.
        /// </summary>
        public static readonly TimeSpan MinimumSendPacketTimeout = TimeSpan.FromSeconds(20); // Windows Firewall times out after 60 seconds

        /// <summary>
        /// The minimum send rate that can be produced by the TCPFriendlyRateControl.
        /// </summary>
        public const int MinimumSendRate = 192;

        // Not strictly a parameter!
        /// <summary>
        /// The length of a UDP header.
        /// </summary>
        public const int UDPHeaderLength = 28;

        /// <summary>
        /// The maximum size of a packet.
        /// </summary>

        public const int MaximumPacketSize = 512;

        public const int ChecksumSize = 2;

        public const int EncryptionOverhead = 16;  // initialization vector + padding to block size

        /// <summary>
        /// The maximum size of the payload for an outbound packet
        /// </summary>
        public const int MaximumPayloadSize = MaximumPacketSize - ChecksumSize - EncryptionOverhead - Transport.Feedback.SerializedSize;

        /// <summary>
        /// The maximum size of a fragmented packet.
        /// </summary>
        public const int MaximumFragmentSize = MaximumPayloadSize;

        /// <summary>
        /// The number of additional bytes packed into relay messages.
        /// </summary>
        public const int RelayOverhead = 24;

        /// <summary>
        /// The amount of time we should wait before we send a FindRelayers message to prevent flooding if
        /// the list becomes empty.
        /// </summary>
        public static readonly TimeSpan MinimumFindRelayersQueryInterval = TimeSpan.FromSeconds(5);

        /// <summary>
        /// The amount of time elapsed before a STUN packet is considered lost.
        /// </summary>
        public const int STUNRequestTimeoutMilliseonds = 3000; // 3 seconds

        /// <summary>
        /// The number of messages sent in a STUN request. They may be sent at the same time.
        /// </summary>
        public const int STUNNumberOfAttempts = 3;

        /// <summary>
        /// The maximum delay allowed before sending an acknowledgment.
        /// </summary>
        public static readonly TimeSpan AckDelay = TimeSpan.FromMilliseconds(50);

        /// <summary>
        /// The maximum delay allowed before sending congestion feedback.
        /// </summary>
        public static readonly TimeSpan CongestionFeedbackDelay = TimeSpan.FromMilliseconds(100);

        /// <summary>
        /// Maximum amount of time between RTT samples.  If this time elapses without receiving
        /// a new sample we'll send an empty reliable message to force a new sample.
        /// </summary>
        public static readonly TimeSpan MaximumRttSamplePeriod = TimeSpan.FromMilliseconds(500);

        /// <summary>
        /// The rate at which old fragments (ones in a part never arrived) are cleaned from the de-fragmentation queue.
        /// </summary>
        public static readonly TimeSpan FragmentQueueTime = TimeSpan.FromMinutes(3);

        /// <summary>
        /// The additional time after a connection attempt notification before the attempt is considered failed.
        /// </summary>
        public const int ConnectionTimeoutMs = 3000;

        /// <summary>
        /// The number of times to try sending a message without receiving an acknowledgement before giving up.
        /// </summary>
        public const int MaximumNumberOfRetries = 5;

        /// <summary>
        /// Interval to wait before resending a reliable message after the first retry (which is RTT-based).
        /// </summary>
        public static readonly TimeSpan MultipleRetryInterval = TimeSpan.FromSeconds(1);

        /// <summary>
        /// The maximum size that a message can be before it is assumed too large and dropped.
        /// </summary>
        public const int MaximumMessageLength = 5000;

        /// <summary>
        /// The amount of time between broadcast discovery attempts
        /// </summary>
        public static readonly TimeSpan DiscoveryPeriod = TimeSpan.FromSeconds(5);

        /// <summary>
        /// The period of time between attempts to connect with other peers when isolated.
        /// </summary>
        public static readonly TimeSpan AttemptReconnectPeriod = TimeSpan.FromSeconds(60);

        /// <summary>
        /// The number of bytes to send as a challeng.
        /// </summary>
        public const int ConnectionChallengeLength = 20;

        /// <summary>
        /// The lease duration of the port mapping. 
        /// </summary>
        public static readonly TimeSpan PortMappingLeaseTime = TimeSpan.FromHours(8);

        public static readonly TimeSpan DesiredRateSamplingPeriod = TimeSpan.FromSeconds(5);
        public static readonly TimeSpan DesiredRateSettlingPeriod = TimeSpan.FromSeconds(15);
        public static readonly TimeSpan OverloadCheckPeriod = TimeSpan.FromSeconds(20);

        /// <summary>
        /// The interval between periodic punched holes keep alive messages. 
        /// </summary>
        public static readonly TimeSpan HolePunchKeepAlivePeriod = TimeSpan.FromSeconds(4);
        public static readonly TimeSpan NATMappingKeepAliveMinimumInterval = TimeSpan.FromSeconds(5);

        /// <summary>
        /// The number of open peer address that the seed peer will periodically save.
        /// </summary>
        public static readonly ushort NumberOfOpenPeerAddressesToSave = 32;

        /// <summary>
        /// Relayed connectionless messages are sent to RelayedConnectionlessMessageRedundancyFactor relayers in parallel. 
        /// </summary>
        public static readonly int RelayedConnectionlessMessageRedundancyFactor = 2;

        #endregion

        #region Weighted unicast

        /// <summary>
        /// The delay in milliseconds before the first update is dispatched for any given entity.
        /// </summary>
        public static readonly TimeSpan InitialUpdateDelay = TimeSpan.FromMilliseconds(10);

        /// <summary>
        /// Minimum rate to send at when packets are available, even if the control says to
        /// send slower.  This is a proportion of the quota assigned to the update scheduler.
        /// TODO:  Remove this value (it doesn't scale with the number of local sources).
        /// </summary>
        public const double MinimumUpdateSchedulerBandwidthFactor = 0.01;

            /// <summary>
        /// How quickly should the UpdateScheduler adjust its send rate when it's over / under
        /// utilising it's allocated bandwidth.  Should be between 0 and 1.
        /// </summary>
        public const double UpdateSendRateAdjustmentFactor = 0.1;

        /// <summary>
        /// The weight of time since last send on the priority of a update.
        /// The priority is calculated as the normalized sum of wighted parameters.
        /// The value must be between 0 and 1.
        /// </summary>
        public const double SecondsSinceLastSendWeight = 0.5;

        /// <summary>
        /// The value at which the seconds since last send results in the highest priority.
        /// The maximum value is the value for which the parameter (before weighting) will be 0.0 (the highest priority).
        /// </summary>
        public const double SecondsSinceLastSendMaximum = 2.0;

        /// <summary>
        /// The amount of extra bytes in an update packet excluding the update itself. Used to estimate bandwith.
        /// </summary>
        public const int UpdatePacketOverhead = Parameters.UDPHeaderLength + 6;

        #endregion

        #region Chat

        /// <summary>
        /// Each user's chat presence information will be mapped to ChatPresenceRedundancyFactor peers for redundancy purpose. 
        /// </summary>
        public const int ChatPresenceRedundancyFactor = 2;

        public const int GroupMembershipTimeout = 3600;

        public const int MaximumPendingPresenceEvents = 64; // Must be less that byte.MaxValue

        public const int MaximimumChatMessageLength = 500;

        /// <summary>
        /// The TTL value for the presence replica. This is used when the assoicated user is not offline. 
        /// A short ttl value allows the replica to be expired shortly after peer crash, so its friends can
        /// be notified. 
        /// </summary>
        public static readonly TimeSpan PresenceReplicaTTL = TimeSpan.FromMinutes(2);

        /// <summary>
        /// The TTL value for the presence replica used when the replica's associated user is in offline mode. 
        /// A much longer TTL value ensures the replica can survive longer to hold more pending subscription requests. 
        /// </summary>
        public static readonly TimeSpan PresenceReplicaOfflineModeTTL = TimeSpan.FromMinutes(6);

        /// <summary>
        /// The time interval to renew presence replica. 
        /// </summary>
        public static readonly TimeSpan PresenceRenewalTaskInterval = TimeSpan.FromSeconds(20);

        /// <summary>
        /// The time interval to renew the pending subscription request. 
        /// </summary>
        public static readonly TimeSpan PresenceSubscriptionRenewalInterval = TimeSpan.FromSeconds(90);

        #endregion

        #region Multicast

        public const int MaximumInMembershipList = 5;

        #endregion

        #region Event queue

        /// <summary>
        /// The amount of time the event queue can lag before we start to drop events.
        /// </summary>
        public static readonly TimeSpan EventQueueAllowableLag = TimeSpan.FromSeconds(15);

        /// <summary>
        /// The size of the event queue required before we start dropping events.
        /// </summary>
        public const int MaximumAllowableEventCount = 300;

        /// <summary>
        /// The amount of time to delay in milliseconds when shutting down the network event queue.
        /// This delay allows for final reliable messages to be sent out, etc.
        /// </summary>
        public const int ShutdownDelayMs = 2000;

        #endregion

        #region DHT

        /// <summary>
        /// The maximum allowed ttl value in seconds for replicated objects stored on the dht.
        /// </summary>
        public static readonly ushort MaximumIReplicaObjectTTLSeconds = 900;

        /// <summary>
        /// How often to send keep alive messages.
        /// </summary>
        public static readonly TimeSpan MinimumStatusCheckPeriod = TimeSpan.FromSeconds(1);

        /// <summary>
        /// How long since the last keep alive message was receieved until we consider the peer lost.
        /// </summary>
        public static readonly TimeSpan MaximumStatusCheckPeriod = TimeSpan.FromSeconds(10);

        public static readonly TimeSpan LeafsetQuarantineTime = TimeSpan.FromSeconds(20);

        public const DistributedHashTable.NamedTier ControllerTier = Badumna.DistributedHashTable.NamedTier.AllInclusive;

        public const DistributedHashTable.NamedTier ImsTier = Badumna.DistributedHashTable.NamedTier.OpenAddresses;

        public const DistributedHashTable.NamedTier RendezvousTier = Badumna.DistributedHashTable.NamedTier.OpenAddresses;

        public const DistributedHashTable.NamedTier MembershipTier = Badumna.DistributedHashTable.NamedTier.AllInclusive;

        public const DistributedHashTable.NamedTier PresenceTier = Badumna.DistributedHashTable.NamedTier.AllInclusive;

        public const DistributedHashTable.NamedTier ServiceDiscoveryTier = Badumna.DistributedHashTable.NamedTier.AllInclusive;

        public const int NumberOfRelayProxiesRequired = 3;

        public static readonly TimeSpan MaxTimeToKeepMessageInRelayProxyMessageQueue = TimeSpan.FromSeconds(20);

        public static readonly TimeSpan CheckRelayerTaskInterval = TimeSpan.FromSeconds(10);

        public static readonly TimeSpan RelayManagerGarbageCollectionTaskInterval = TimeSpan.FromSeconds(10);

        public static readonly TimeSpan NormalStabilizationTaskInterval = TimeSpan.FromMinutes(10);

        public static readonly TimeSpan ServicePeerStabilizationTaskInterval = TimeSpan.FromHours(1);

        public static readonly TimeSpan MinimalStabilizationPingMessageInterval = TimeSpan.FromMinutes(10);

        #endregion

        #region Security

        /// <summary>
        /// The minimum amount of time before a token expires that we should update it.
        /// </summary>
        public static readonly TimeSpan TokenExpirationBuffer = TimeSpan.FromMinutes(5);

#if !DEBUG
        public static readonly bool EncryptTraffic = true;  // DON'T CHANGE THIS LINE!  RELEASE BUILD SHOULD ALWAYS HAVE THIS SET TRUE!
#else
        public static readonly bool EncryptTraffic = true;  // Change this to false to enable extra packet trace information in the simulator
#endif

        #endregion

        #region Streaming

        public static readonly TimeSpan InitialResendAckTimeout = TimeSpan.FromSeconds(3);

        #endregion

        #region ServiceDiscovery

        /// <summary>
        /// The default string used in the service description. 
        /// </summary>
        public static readonly string ServiceDiscoveryDefaultString = "default";

        /// <summary>
        /// TTL for records hosted on the Dht.
        /// </summary>
        public static readonly TimeSpan ServiceDiscoveryDHTRecordTTL = TimeSpan.FromMinutes(6);

        /// <summary>
        /// Refresh interval for cached records.
        /// </summary>
        public static readonly TimeSpan ServiceDiscoveryCachedRecordRefreshInterval = TimeSpan.FromMinutes(3);

        /// <summary>
        /// Interval of the maintaince task excutions. 
        /// </summary>
        public static readonly TimeSpan ServiceDiscoveryMaintainceTaskInterval = TimeSpan.FromSeconds(10);

        /// <summary>
        /// TTL for the dht load estimator's records.  
        /// </summary>
        public static readonly TimeSpan ServiceDiscoveryDhtEstimatorRecordTTL = TimeSpan.FromMinutes(10);

        /// <summary>
        /// Garbage collection interval
        /// </summary>
        public static readonly TimeSpan ServiceDiscoveryGarbageCollectionInterval = TimeSpan.FromSeconds(30);

        /// <summary>
        /// The parallel factor when sending out replicas. 
        /// </summary>
        public const uint ServiceDiscoveryParalellFactor = 8;

        /// <summary>
        /// The number of replica per parallel path. The total number of replicas should be (mParalellFactor * mNumberOfReplicas) 
        /// </summary>
        public const uint ServiceDiscoveryNumberOfReplicas = 6;

        /// <summary>
        /// The parallel factor for queries.
        /// </summary>
        public const uint ServiceDiscoveryQueryParallelFactor = 3;

        /// <summary>
        /// The miniume populate interval.
        /// </summary>
        public static readonly TimeSpan ServiceDiscoveryMinPopulateInterval = TimeSpan.FromSeconds(10);

        /// <summary>
        /// Gossip interval.
        /// </summary>
        public static readonly TimeSpan ServiceDiscoveryGossipInterval = TimeSpan.FromSeconds(25);

        /// <summary>
        /// Expect time to complete the Dht query. 
        /// </summary>
        public static readonly TimeSpan ServiceDiscoveryExpectedDhtQueryLatency = TimeSpan.FromSeconds(30);

        /// <summary>
        /// Service announcement interval. 
        /// </summary>
        public static readonly TimeSpan ServiceDiscoveryAnnouncementInterval = TimeSpan.FromSeconds(30);

        /// <summary>
        /// The number of neighbours the local peer should contact to exchange record info. 
        /// </summary>
        public static readonly int ServiceDiscoveryMaxNumberOfNeighboursToExchangeInfo = 8;

        /// <summary>
        /// The number of neighbours the local peer should gossip with. 
        /// </summary>
        public static readonly int ServiceDiscoveryGossipNumberOfNeighbours = 10;

        #endregion

        #region Arbitration

        public static readonly TimeSpan ArbitrationConnectionTimeoutTime = TimeSpan.FromSeconds(15);

        public static readonly TimeSpan ServiceNotAvailableTimeoutTime = TimeSpan.FromMinutes(3);

        public static readonly TimeSpan ArbitrationServerStatsCollectionInterval = TimeSpan.FromSeconds(10);

        #endregion 

        #region Overload

        public static readonly TimeSpan OverloadGroupTTL = TimeSpan.FromMinutes(5);

        public static readonly TimeSpan OverloadReconnectInterval = TimeSpan.FromSeconds(20);

        public static readonly TimeSpan OverloadGarbageCollectionTaskInterval = TimeSpan.FromMinutes(3);

        public static readonly TimeSpan OverloadStatsCollectionTaskInterval = TimeSpan.FromSeconds(10);

        #endregion

        #region User Traker

        /// <summary>
        /// The hostname of the user tracker server.
        /// </summary>
        public static readonly string UserTrackerHostname = "stats.badumna.com";

        /// <summary>
        /// The port of the user tracker server. 
        /// </summary>
        public static readonly int UserTrackerPort = 21246;

        /// <summary>
        /// The initial phone home message interval.
        /// </summary>
        public static readonly TimeSpan InitialPhoneHomeInterval = TimeSpan.FromMinutes(1);

        /// <summary>
        /// The phone home message interval.
        /// </summary>
        public static readonly TimeSpan PhoneHomeInterval = TimeSpan.FromMinutes(15);

        #endregion

        public const double MaximumDevTrailVersionRuntimeMinutes = 65.0;

        public static readonly TimeSpan PingProxiesTaskPeriod = TimeSpan.FromSeconds(15);

        public const int DefaultHttpRequestTimeoutMs = 15000;
    }
}
