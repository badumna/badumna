﻿//------------------------------------------------------------------------------
// <copyright file="SimpleEventQueue.cs" company="National ICT Australia Limited">
//     Copyright (c) 2012 National ICT Australia Limited. All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;

using Badumna.Utilities;

namespace Badumna.Core
{
    /// <summary>
    /// Signature of the method to queue an invokable.
    /// </summary>
    /// <param name="invokable">The invokable to queue</param>
    internal delegate void QueueInvokable(IInvokable invokable);

    /// <summary>
    /// A simple queue of invokables which are executed all at once in the order they were added.
    /// </summary>
    internal class SimpleEventQueue
    {
        /// <summary>
        /// The queue of invokables.
        /// </summary>
        private Queue<NetworkEvent> queue = new Queue<NetworkEvent>();

        /// <summary>
        /// Adds an invokable to the queue.
        /// </summary>
        /// <param name="invokable">The invokable to add</param>
        public void Add(IInvokable invokable)
        {
            lock (this.queue)
            {
                this.queue.Enqueue(new NetworkEvent(TimeSpan.Zero, invokable));
            }
        }

        /// <summary>
        /// Executes all invokables on the queue.
        /// </summary>
        public void Run()
        {
            lock (this.queue)
            {
                // Perform delayed operations.
                while (this.queue.Count > 0)
                {
                    this.queue.Dequeue().PerformAction();  // Don't swallow exceptions here because they're probably bugs in application code
                }
            }
        }

        /// <summary>
        /// Clears the queue.
        /// </summary>
        public void Clear()
        {
            lock (this.queue)
            {
                this.queue.Clear();
            }
        }
    }
}
