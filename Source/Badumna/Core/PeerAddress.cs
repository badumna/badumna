//------------------------------------------------------------------------------
// <copyright file="PeerAddress.cs" company="National ICT Australia Limited">
//     Copyright (c) 2007-2012 National ICT Australia Limited. All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Net.NetworkInformation;

using Badumna.Utilities;

namespace Badumna.Core
{
    internal enum NatType // FromString assumes consecutive order 
    {
        Unknown = 0,
        Blocked = 1,
        Open = 2,
        SymmetricFirewall = 3,
        FullCone = 4,
        SymmetricNat = 5,
        RestrictedPort = 6,
        RestrictedCone = 7,
        MangledFullCone = 8,
        Internal = 9,
        HashAddressable = 10
    }

    /// <summary>
    /// A comparable and parseable representation of a peer IP end point.
    /// </summary>
    /// <remarks>
    /// The address includes not only the IP address and port, but the NAT type of the host.
    /// The class extends the IParseable interface to allow effiecient writing and reading from messages.
    /// </remarks>
    class PeerAddress : IEquatable<PeerAddress>, IParseable, IComparable<PeerAddress>
    {
        static readonly public PeerAddress Nowhere = new PeerAddress();
        internal const int DirectByteLength = 6;  // TODO: Support IPv6

        public bool IsValid
        {
            get
            {
                if (this.IsDhtAddress)
                {
                    return this.mHashAddress != null;
                }

                byte[] addressAsBytes = this.Address.GetAddressBytes();
                return addressAsBytes[0] != 0 || addressAsBytes[1] != 0 || addressAsBytes[2] != 0 || addressAsBytes[3] != 0;
            }
        }

        private NatType mNatType = NatType.Unknown;
        internal NatType NatType
        {
            get 
            {
                return this.mNatType;
            }
            set 
            {
                this.mNatType = value;
            }
        }

        private IPEndPoint endPoint = new IPEndPoint(IPAddress.Any, 0);

        public IPEndPoint EndPoint
        {
            get
            {
                return this.endPoint;
            }
        }

        public IPAddress Address
        {
            get 
            {
                return this.endPoint.Address;
            }
            set
            {
                this.endPoint.Address = value;
            }
        }

        public int Port
        {
            get { return this.endPoint.Port; }
            set { this.endPoint.Port = value; }
        }

        public bool IsDhtAddress 
        { 
            get
            {
                return this.mNatType == NatType.HashAddressable;
            } 
        }

        private DistributedHashTable.HashKey mHashAddress;
        public DistributedHashTable.HashKey HashAddress 
        { 
            get 
            { 
                return this.mHashAddress; 
            } 
        }

        public static bool operator >(PeerAddress firstOperand, PeerAddress secondOperand)
        {
            if (null == firstOperand)
            {
                throw new ArgumentNullException("firstOperand");
            }

            if (null == secondOperand)
            {
                throw new ArgumentNullException("secondOperand");
            }

            int firstAddress = BitConverter.ToInt32(firstOperand.Address.GetAddressBytes(), 0);
            int secondAddress = BitConverter.ToInt32(secondOperand.Address.GetAddressBytes(), 0);

            if (firstAddress == secondAddress)
            {
                return firstOperand.Port > secondOperand.Port;
            }

            return firstAddress > secondAddress;
        }

        public static bool operator <(PeerAddress firstOperand, PeerAddress secondOperand)
        {
            if (null == firstOperand)
            {
                throw new ArgumentNullException("firstOperand");
            }

            if (null == secondOperand)
            {
                throw new ArgumentNullException("secondOperand");
            }

            int firstAddress = BitConverter.ToInt32(firstOperand.Address.GetAddressBytes(), 0);
            int secondAddress = BitConverter.ToInt32(secondOperand.Address.GetAddressBytes(), 0);

            if (firstAddress == secondAddress)
            {
                return firstOperand.Port < secondOperand.Port;
            }

            return firstAddress < secondAddress;
        }

        public PeerAddress()
            : this(IPAddress.Any, 0, NatType.Unknown)
        {
        }

        public PeerAddress(IPEndPoint endPoint, NatType natType)
            : this(endPoint.Address, endPoint.Port, natType)
        {
        }

        public PeerAddress(IPAddress address, int port, NatType natType)
        {
            if (address.AddressFamily != AddressFamily.InterNetwork)
            {
                throw new ArgumentException("address must be an IPv4 address.");
            }

            this.mNatType = natType;

            if (this.mNatType == NatType.Unknown && this.IsAddressPrivate())
            {
                this.mNatType = NatType.Internal;
            }

            this.endPoint = new IPEndPoint(address, port);
        }

        public PeerAddress(DistributedHashTable.HashKey hashAddress)
        {
            this.mNatType = NatType.HashAddressable;
            this.mHashAddress = hashAddress;
        }

        public PeerAddress(PeerAddress address)
        {
            this.mNatType = address.NatType;
            this.mHashAddress = address.mHashAddress;
            this.endPoint = new IPEndPoint(address.Address, address.Port);
        }

        /// <summary>
        /// Constructs a new PeerAddress from a string of the form "hostname:port", with a NAT
        /// type of Unknown.
        /// </summary>
        /// <param name="endpoint">A string representing the address.</param>
        /// <returns>The new <see cref="PeerAddress"/>, or <c>PeerAddress.Nowhere</c> if the address was badly formed or unresolvable.</returns>
        static internal PeerAddress GetAddress(string endpoint)
        {
            return PeerAddress.GetAddress(endpoint, NatType.Unknown);
        }

        /// <summary>
        /// Constructs a new PeerAddress from a string of the form "hostname:port", with the
        /// given NAT type.
        /// </summary>
        /// <param name="endpoint">A string representing the address.</param>
        /// <param name="natType">The NAT type.</param>
        /// <returns>The new <see cref="PeerAddress"/>, or <c>PeerAddress.Nowhere</c> if the address was badly formed or unresolvable.</returns>
        static internal PeerAddress GetAddress(string endpoint, NatType natType)
        {
            string[] endpointParts = endpoint.Split(':');   // TODO: Fix for IPv6
            if (endpointParts.Length != 2)
            {
                return PeerAddress.Nowhere;
            }

            int port;
            if (!int.TryParse(endpointParts[1], out port))
            {
                return PeerAddress.Nowhere;
            }

            return PeerAddress.GetAddress(endpointParts[0], port, natType);
        }

        /// <summary>
        /// Constructs a new PeerAddress based on the given parameters with a NAT type of Unknown.
        /// </summary>
        /// <param name="hostname">The hostname</param>
        /// <param name="port">The port</param>
        /// <returns>The new PeerAddress</returns>
        static internal PeerAddress GetAddress(string hostname, int port)
        {
            return PeerAddress.GetAddress(hostname, port, NatType.Unknown);
        }

        /// <summary>
        /// Constructs a new PeerAddress based on the given parameters.
        /// </summary>
        /// <param name="hostname">The hostname</param>
        /// <param name="port">The port</param>
        /// <param name="natType">The NAT type</param>
        /// <returns>The new PeerAddress, or PeerAddress.Nowhere if the hostname could not be resolved.</returns>
        static internal PeerAddress GetAddress(string hostname, int port, NatType natType)
        {
            IPAddress address = null;
            try
            {
                foreach (IPAddress a in Dns.GetHostAddresses(hostname))
                {
                    if (a.AddressFamily == AddressFamily.InterNetwork)
                    {
                        address = a;
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.TraceInformation(
                    LogTag.Core,
                    "Exception thrown when attempting to resolve ip address {0}: {1}",
                    hostname,
                    ex.Message);
            }

            if (address == null)
            {
                Logger.TraceInformation(LogTag.Core, "Failed to resolve ip address {0}", hostname);
                return PeerAddress.Nowhere;
            }

            if (!(IPEndPoint.MinPort <= port && port <= IPEndPoint.MaxPort))
            {
                Logger.TraceInformation(LogTag.Core, "Bad port number {0}", port);
                return PeerAddress.Nowhere;
            }

            return new PeerAddress(address, port, natType);
        }

        /// <summary>
        /// Constructs a new PeerAddress that references the loopback address on the 
        /// specified port.
        /// </summary>
        /// <param name="port">The port</param>
        /// <returns>The new PeerAddress</returns>
        static public PeerAddress GetLoopback(int port)
        {
            return new PeerAddress(IPAddress.Loopback, port, NatType.Unknown);
        }

        /// <summary>
        /// Constructs a new PeerAddress that references the broadcast address on the 
        /// specified port.
        /// </summary>
        /// <param name="port">The port</param>
        /// <returns>The new PeerAddress</returns>
        static public PeerAddress GetBroadcast(int port)
        {
            return new PeerAddress(IPAddress.Broadcast, port, NatType.Internal);
        }

        static internal PeerAddress GetRandomAddress()
        {
            byte[] bytes = new byte[6];

            RandomSource.Generator.NextBytes(bytes);
            IPAddress address = new IPAddress(BitConverter.ToUInt32(bytes, 0));
            return new PeerAddress(address, (int)BitConverter.ToUInt16(bytes, 4), NatType.Open);
        }

        internal Byte[] GetAddressBytes()
        {
            // This is used for calculating the checksum salting.  Pretty sure
			// the salting was used to make sure different peers didn't accidentally
			// talk to each other.  This is no longer possible due to the
			// encryption now, so shouldn't matter that this is all zeros.
			// TODO: Eliminate this method, or make it work for any kind of address, not just IPv4 addresses.
            return this.Address.GetAddressBytes();
        }

        public bool IsAddressPrivate()
        {
            byte[] addressBytes = this.GetAddressBytes();

            return (addressBytes[0] == 10
                || (addressBytes[0] == 192 && addressBytes[1] == 168)
                || (addressBytes[0] == 172 && (addressBytes[1] >= 16 && addressBytes[1] <= 31)));
        }

        public int Tier
        {
            get
            {
                if (this.IsDhtAddress)
                {
                    return 2;
                }

                if (this.NatType == NatType.SymmetricNat || this.NatType == NatType.RestrictedPort ||
                    this.NatType == NatType.RestrictedCone || this.NatType == NatType.SymmetricFirewall ||
                    this.NatType == NatType.MangledFullCone)
                {
                    return 2;
                }

                if (this.NatType == NatType.Blocked)
                {
                    return -1;
                }

                return 1;                
            }
        }

        public void ToMessage(MessageBuffer message, Type parameterType)
        {
            message.Write((byte)this.mNatType);

            if (this.IsDhtAddress)
            {
                this.DhtToMessage(message, parameterType);
            }
            else
            {
                this.DirectToMessage(message, parameterType);
            }
        }

        private void DirectToMessage(MessageBuffer message, Type parameterType)
        {
            byte[] address = new byte[PeerAddress.DirectByteLength];

            for (int i = 0; i < 4; i++)
                address[i] = this.Address.GetAddressBytes()[i];

            byte[] portBytes = BitConverter.GetBytes(this.Port);

            if (!BitConverter.IsLittleEndian)
            {
                Array.Reverse(portBytes);
            }

            for (int i = 0; i < 2; i++)
                address[i + 4] = portBytes[i];

            message.Write(address, address.Length);
        }

        private void DhtToMessage(MessageBuffer message, Type parameterType)
        {
            this.mHashAddress.ToMessage(message, typeof(DistributedHashTable.HashKey));
        }

        public void FromMessage(MessageBuffer message)
        {
            this.mNatType = (NatType)message.ReadByte();

            if (this.IsDhtAddress)
            {
                this.DhtFromMessage(message);
            }
            else
            {
                this.DirectFromMessage(message);
            }
        }

        private void DirectFromMessage(MessageBuffer message)
        {
            byte[] address = message.ReadBytes(PeerAddress.DirectByteLength);
            if (address.Length < PeerAddress.DirectByteLength)
                throw new ParseException("Failed to read enough bytes for PeerAddress");

            byte[] ipAddress = new byte[4];

            for (int i = 0; i < 4; i++)
                ipAddress[i] = address[i];

            this.Address = new IPAddress(ipAddress);

            byte[] portBytes = new byte[2];
            portBytes[0] = address[4];
            portBytes[1] = address[5];

            if (!BitConverter.IsLittleEndian)
            {
                Array.Reverse(portBytes);
            }

            this.Port = (int)BitConverter.ToUInt16(portBytes, 0);
        }

        private void DhtFromMessage(MessageBuffer message)
        {
            this.mHashAddress = new Badumna.DistributedHashTable.HashKey();
            this.mHashAddress.FromMessage(message);
        }

        private static string[] NatTypeDescriptions = new string[]   // Must be in the same order as the values for NatType
        {            
        "Unknown",
        "Blocked",
        "Open",
        "Symmetric firewall",
        "Full cone",
        "Symmetric NAT",
        "Restricted port",
        "Restricted cone",
        "Mangled full cone",
        "Internal",
        "Dht"
        };

        public String HumanReadableNAT()
        {
            return PeerAddress.NatTypeDescriptions[(int)this.NatType];
        }

        // This must return the string in a format that can be parsed back into a PeerAddress by FromString
        public override String ToString()
        {
            if (this.mHashAddress != null)
            {
                return string.Format("{0}|#{1}", this.HumanReadableNAT(), this.mHashAddress);
            }

            return string.Format("{0}|{1}:{2}", this.HumanReadableNAT(), this.Address, this.Port);
        }

        public static PeerAddress FromString(string address)
        {
            string[] parts = address.Split('|');
            if (parts.Length != 2)
            {
                return null;
            }

            NatType? natType = null;
            for (int i = 0; i < PeerAddress.NatTypeDescriptions.Length; i++)
            {
                if (parts[0] == PeerAddress.NatTypeDescriptions[i])
                {
                    natType = (NatType)i;
                    break;
                }
            }
            if (!natType.HasValue)
            {
                return null;
            }

            if (parts[1].Length > 1 && parts[1][0] == '#')
            {
                // Hash address
                return null;  // TODO: Support hash addresses
            }

            string[] endPointParts = parts[1].Split(':');   // TODO: Fix for IPv6
            if (endPointParts.Length != 2)
            {
                return null;
            }

            IPEndPoint endPoint = null;
            try
            {
                endPoint = new IPEndPoint(IPAddress.Parse(endPointParts[0]), int.Parse(endPointParts[1]));
            }
            catch (FormatException) { }
            catch (OverflowException) { }
            catch (ArgumentOutOfRangeException) { }

            if (endPoint == null)
            {
                return null;
            }

            return new PeerAddress(endPoint, natType.Value);
        }

        public override bool Equals(Object comparand)
        {
            if (comparand is PeerAddress)
            {
                return this.Equals(comparand as PeerAddress);
            }

            return base.Equals(comparand);
        }

        public override int GetHashCode()
        {
            if (this.IsDhtAddress)
            {
                return this.mHashAddress.GetHashCode();
            }

            return this.Port ^ this.endPoint.GetHashCode();
        }

        public bool Equals(PeerAddress other)
        {
            if (other == null)
            {
                return false;
            }

            if (this.IsDhtAddress)
            {
                if (other.IsDhtAddress)
                {
                    return this.mHashAddress.Equals(other.mHashAddress);
                }

                return false;
            }
            else if (other.IsDhtAddress)
            {
                return false;
            }

            Byte[] thisAsBytes = this.Address.GetAddressBytes();
            Byte[] otherAsBytes = other.Address.GetAddressBytes();

            for (int i = 0; i < thisAsBytes.Length; i++)
            {
                if (thisAsBytes[i] != otherAsBytes[i])
                {
                    return false;
                }
            }

            return this.Port.Equals(other.Port);
        }

        #region IComparable<PeerAddress> Members

        public int CompareTo(PeerAddress other)
        {
            int natComparision = ((byte)this.NatType).CompareTo((byte)other.NatType);
            if (natComparision != 0)
            {
                return natComparision;
            }

            int thisAsInt = BitConverter.ToInt32(this.Address.GetAddressBytes(), 0);
            int otherAsInt = BitConverter.ToInt32(other.Address.GetAddressBytes(), 0);

            if (this.IsDhtAddress)
            {
                System.Diagnostics.Debug.Assert(this.mHashAddress != null, "Null hash key for Dht address");
                if (other.IsDhtAddress)
                {
                    System.Diagnostics.Debug.Assert(other.mHashAddress != null, "Null hash key for Dht address");
                    return this.mHashAddress.CompareTo(other.mHashAddress);
                }

                thisAsInt = this.mHashAddress.GetHashCode();
            }
            else if (other.IsDhtAddress)
            {
                System.Diagnostics.Debug.Assert(other.mHashAddress != null, "Null hash key for Dht address");
                otherAsInt = other.GetHashCode();
            }

            if (thisAsInt.CompareTo(otherAsInt) != 0)
            {
                return thisAsInt.CompareTo(otherAsInt);
            }

            return this.Port.CompareTo(other.Port);
        }

        #endregion
    }
}
