﻿//-----------------------------------------------------------------------
// <copyright file="IProtocolComponent.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using Badumna.Utilities;

namespace Badumna.Core
{
    /// <summary>
    /// The protocol component interface.
    /// </summary>
    /// <typeparam name="EnvelopeType">The type of the envelope type.</typeparam>
    internal interface IProtocolComponent<EnvelopeType> where EnvelopeType : IEnvelope
    {
        /// <summary>
        /// Gets the parser.
        /// </summary>
        /// <value>The parser.</value>
        MessageParser Parser
        {
            get;
        }

        /// <summary>
        /// Gets the current envelope.
        /// </summary>
        /// <value>The current envelope.</value>
        EnvelopeType CurrentEnvelope
        {
            get;
        }

        /// <summary>
        /// Gets or sets the dispatcher method.
        /// </summary>
        /// <value>The dispatcher method.</value>
        DispatchMessageDelegate<EnvelopeType> DispatcherMethod
        {
            get;
            set;
        }

        /// <summary>
        /// Forges the method list.
        /// </summary>
        void ForgeMethodList();

        /// <summary>
        /// Sends the message.
        /// </summary>
        /// <param name="envelope">The envelope.</param>
        void SendMessage(EnvelopeType envelope);

        /// <summary>
        /// Adds a remote call.
        /// </summary>
        /// <param name="envelope">The envelope.</param>
        /// <param name="function">The function.</param>
        void RemoteCall(IEnvelope envelope, GenericCallBack function);

        /// <summary>
        /// Adds a remote call.
        /// </summary>
        /// <typeparam name="A">The type of the first parameter.</typeparam>
        /// <param name="envelope">The envelope.</param>
        /// <param name="function">The function.</param>
        /// <param name="a">The first parameter.</param>
        void RemoteCall<A>(IEnvelope envelope, GenericCallBack<A> function, A a);

        /// <summary>
        /// Adds a remote call.
        /// </summary>
        /// <typeparam name="A">The type of the first parameter.</typeparam>
        /// <typeparam name="B">The type of the second parameter</typeparam>
        /// <param name="envelope">The envelope.</param>
        /// <param name="function">The function.</param>
        /// <param name="a">The first parameter.</param>
        /// <param name="b">The second parameter.</param>
        void RemoteCall<A, B>(IEnvelope envelope, GenericCallBack<A, B> function, A a, B b);

        /// <summary>
        /// Adds a remote call.
        /// </summary>
        /// <typeparam name="A">The type of the first parameter.</typeparam>
        /// <typeparam name="B">The type of the second parameter.</typeparam>
        /// <typeparam name="C">The type of the third parameter.</typeparam>
        /// <param name="envelope">The envelope.</param>
        /// <param name="function">The function.</param>
        /// <param name="a">The first parameter.</param>
        /// <param name="b">The second parameter.</param>
        /// <param name="c">The third parameter.</param>
        void RemoteCall<A, B, C>(IEnvelope envelope, GenericCallBack<A, B, C> function, A a, B b, C c);

        /// <summary>
        /// Adds a remote call.
        /// </summary>
        /// <typeparam name="A">The type of the first parameter.</typeparam>
        /// <typeparam name="B">The type of the second parameter.</typeparam>
        /// <typeparam name="C">The type of the third parameter.</typeparam>
        /// <typeparam name="D">The type of the forth parameter.</typeparam>
        /// <param name="envelope">The envelope.</param>
        /// <param name="function">The function.</param>
        /// <param name="a">The first parameter.</param>
        /// <param name="b">The second parameter.</param>
        /// <param name="c">The third parameter.</param>
        /// <param name="d">The forth parameter</param>
        void RemoteCall<A, B, C, D>(IEnvelope envelope, GenericCallBack<A, B, C, D> function, A a, B b, C c, D d);

        /// <summary>
        /// Adds a remote call.
        /// </summary>
        /// <typeparam name="A">The type of the first parameter.</typeparam>
        /// <typeparam name="B">The type of the second parameter.</typeparam>
        /// <typeparam name="C">The type of the third parameter.</typeparam>
        /// <typeparam name="D">The type of the forth parameter.</typeparam>
        /// <typeparam name="E">The type of the fifth parameter.</typeparam>
        /// <param name="envelope">The envelope.</param>
        /// <param name="function">The function.</param>
        /// <param name="a">The first parameter.</param>
        /// <param name="b">The second parameter.</param>
        /// <param name="c">The third parameter.</param>
        /// <param name="d">The forth parameter.</param>
        /// <param name="e">The fifth parameter.</param>
        void RemoteCall<A, B, C, D, E>(IEnvelope envelope, GenericCallBack<A, B, C, D, E> function, A a, B b, C c, D d, E e);

        /// <summary>
        /// Adds a remote call.
        /// </summary>
        /// <typeparam name="A">The type of the first parameter.</typeparam>
        /// <typeparam name="B">The type of the second parameter.</typeparam>
        /// <typeparam name="C">The type of the third parameter.</typeparam>
        /// <typeparam name="D">The type of the forth parameter.</typeparam>
        /// <typeparam name="E">The type of the fifth parameter.</typeparam>
        /// <typeparam name="F">The type of the sixth parameter.</typeparam>
        /// <param name="envelope">The envelope.</param>
        /// <param name="function">The function.</param>
        /// <param name="a">The first parameter.</param>
        /// <param name="b">The second parameter.</param>
        /// <param name="c">The third parameter.</param>
        /// <param name="d">The forth parameter.</param>
        /// <param name="e">The fifth parameter.</param>
        /// <param name="f">The sixth parameter.</param>
        void RemoteCall<A, B, C, D, E, F>(IEnvelope envelope, GenericCallBack<A, B, C, D, E, F> function, A a, B b, C c, D d, E e, F f);

        /// <summary>
        /// Traverses the protocol component.
        /// </summary>
        /// <param name="crawler">The crawler.</param>
        void Traverse(ProtocolStackCrawler crawler);

        /// <summary>
        /// Parses the message.
        /// </summary>
        /// <param name="envelope">The envelope.</param>
        /// <param name="offset">The offset.</param>
        void ParseMessage(EnvelopeType envelope, int offset);

        /// <summary>
        /// Prepares message for departure.
        /// </summary>
        /// <param name="envelope">The envelope.</param>
        void PrepareMessageForDeparture(ref EnvelopeType envelope);

        /// <summary>
        /// This method will search all the methods in the given object. Each method with the ProtocolMethod
        /// attribute will be registered with the MessageParser.
        /// </summary>
        /// <param name="obj">The object whose methods will be registered.</param>
        /// <inbheritdoc/>
        void RegisterMethodsIn(object obj);
    }
}
