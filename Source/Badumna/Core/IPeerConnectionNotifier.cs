﻿//-----------------------------------------------------------------------
// <copyright file="IPeerConnectionNotifier.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2010 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.Core
{
    using System;

    /// <summary>
    /// A delegate for notification of connection with a specific peer.
    /// </summary>
    /// <param name="address">The peer for which a connection has been established.</param>
    internal delegate void ConnectionHandler(PeerAddress address);

    /// <summary>
    /// Interface for network connectivity status querying and notifications.
    /// </summary>
    internal interface IPeerConnectionNotifier
    {
        /// <summary>
        /// Notification of a connection being established to a peer.
        /// </summary>
        event ConnectionHandler ConnectionEstablishedEvent;
        
        /// <summary>
        /// Notification of a connection to a peer being lost.
        /// </summary>
        event ConnectionHandler ConnectionLostEvent;
    }
}
