﻿//-----------------------------------------------------------------------
// <copyright file="INetworkConnectivityReporter.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2010 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.Core
{
    using System;
    using Badumna.Utilities;

    /// <summary>
    /// Interface for network connectivity status querying and notifications.
    /// </summary>
    internal interface INetworkConnectivityReporter
    {
        /// <summary>
        /// Notification of status change to online.
        /// </summary>
        event GenericCallBack NetworkOnlineEvent;

        /// <summary>
        /// Notification of status change to offline.
        /// </summary>
        event GenericCallBack NetworkOfflineEvent;

        /// <summary>
        /// Notification of status change to shutting down.
        /// </summary>
        event GenericCallBack NetworkShuttingDownEvent;

        /// <summary>
        /// Notification of status change to Initializing. Note: the status is by default.
        /// </summary>
        event GenericCallBack NetworkInitializingEvent;

        /// <summary>
        /// Gets the state of connectivity for this network context.
        /// The initial state is Initializing. From there it will change to either Online or Offline.
        /// From Offline the state can change only to ShuttingDown and from Offline it can change only
        /// to Initializing.
        /// </summary>
        ConnectivityStatus Status { get; }

        /// <summary>
        /// Sets the connectivity status.
        /// </summary>
        /// <remarks>
        /// This call will also trigger the appropriate event handlers.  It is a method rather than
        /// a setter as it may cause a fair amount of work to take place, and also to make it easier
        /// to find references that set the status.
        /// </remarks>
        /// <param name="status">The new status</param>
        void SetStatus(ConnectivityStatus status);
    }
}
