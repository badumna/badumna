//------------------------------------------------------------------------------
// <copyright file="MessageParser.cs" company="National ICT Australia Limited">
//     Copyright (c) 2007-2012 National ICT Australia Limited. All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Reflection;

using Badumna.Utilities;
using Badumna.Utilities.Logging;

namespace Badumna.Core
{
    /// <summary>
    /// Serializes and deserializes method calls.
    /// </summary>
    /// <remarks>
    /// Target/method pairs that are to be serialized must first be registered by calling
    /// <see cref="RegisterMethodsIn(object)"/>.  In addition, <see cref="ForgeMethodList()"/>
    /// must be called before attempting to serialize or deserialize any method calls.
    /// TODO: Methods on this class take IEnvelopes, but they should probably just take MessageBuffers.
    /// </remarks>
    internal class MessageParser
    {
        /// <summary>
        /// The BindingFlags used when searching a class for protocol methods to be registered.
        /// </summary>
        /// <remarks>
        /// TODO: Remove hint of support for static protocol methods - see ticket 551.
        /// </remarks>
        private const BindingFlags ProtocolMethodBindingFlags =
            BindingFlags.Instance |
            BindingFlags.Public |
            BindingFlags.NonPublic |
            BindingFlags.Static |
            BindingFlags.FlattenHierarchy;

        /// <summary>
        /// A logical timestamp to record the creation order of <see cref="MethodData"/> instances.
        /// This is used to make sorting of MethodData instances stable when multiples of the same
        /// method are being registered (with different targets).
        /// </summary>
        /// <remarks>
        /// TODO: Can this be made non-static?
        /// </remarks>
        private static int creationOrder;

        /// <summary>
        /// Indicates whether the method list has been forged.  Once forged, no more methods
        /// can be registered and method serializing/deserializing can commence.
        /// </summary>
        private bool hasBeenForged;

        /// <summary>
        /// The number of bytes required to store the delegate id.
        /// </summary>
        private int bytesInDelegateId;
        
        /// <summary>
        /// The registered methods.  Also used to map a method id to the method.
        /// </summary>
        private List<MethodData> methodList = new List<MethodData>();

        /// <summary>
        /// Used to map methods to method ids, by way of the method MetadataToken.  The values are lists
        /// of MethodData as MetadataToken identifies the method only, and doesn't vary with the target object.
        /// </summary>
        private Dictionary<int, List<MethodData>> methodToId = new Dictionary<int, List<MethodData>>();

        /// <summary>
        /// A name that identifies this <see cref="MessageParser"/> for debug purposes (such
        /// as recording statistics associated with the parser).
        /// </summary>
        private string parserName;

        /// <summary>
        /// The attribute used to identify methods that should be registered when calling <see cref="RegisterMethodsIn(object)"/>.
        /// </summary>
        private Type protocolAttributeType;

        /// <summary>
        /// A factory that constructs instances of the given type.
        /// </summary>
        private GenericCallBackReturn<object, Type> factory;

        /// <summary>
        /// Initializes a new instance of the MessageParser class.
        /// </summary>
        /// <param name="parserName">A name that identifies this instance for debug purposes</param>
        /// <param name="protocolAttributeType">The attribute used to mark methods that should be registered by this parser; must derive from <see cref="ProtocolMethodAttribute"/></param>
        /// <param name="factory">A factory that constructs instances of the given type</param>
        public MessageParser(string parserName, Type protocolAttributeType, GenericCallBackReturn<object, Type> factory)
        {
            if (parserName == null)
            {
                throw new ArgumentNullException("parserName");
            }

            if (protocolAttributeType == null)
            {
                throw new ArgumentNullException("protocolAttributeType");
            }

            if (factory == null)
            {
                throw new ArgumentNullException("factory");
            }

            if (!typeof(ProtocolMethodAttribute).IsAssignableFrom(protocolAttributeType) ||
                protocolAttributeType == typeof(ProtocolMethodAttribute))
            {
                throw new ArgumentOutOfRangeException("protocolAttributeType must be a subclass of ProtocolMethodAttribute");
            }

            this.parserName = parserName;
            this.protocolAttributeType = protocolAttributeType;
            this.factory = factory;
        }

        /// <summary>
        /// Gets the number of bits used to uniquely identify registered methods.  This value is
        /// only valid after <see cref="ForgeMethodList()"/> has been called.
        /// </summary>
        /// <remarks>
        /// TODO: This is currently only referenced by a unit test.  It should not be part of the public interface of the class.
        ///       It should be eliminated altogether and only bytesInDelegateId used instead.
        /// </remarks>
        public int BitsInDelegateId { get; private set; }

        /// <summary>
        /// Registers all methods on the given object that are tagged with the <see cref="ProtocolMethodAttribute"/>
        /// type that is associated with this <see cref="MessageParser"/>.
        /// </summary>
        /// <param name="obj">The object whose methods will be registered.</param>
        public
#if !IOS
        virtual // TODO: Not a generic method so probably could be virtual on all platforms?
#endif
        void RegisterMethodsIn(object obj)
        {
            if (null == obj)
            {
                throw new ArgumentException("Invalid object.");
            }

            MethodInfo[] methodList = obj.GetType().GetMethods(MessageParser.ProtocolMethodBindingFlags);

            foreach (MethodInfo method in methodList)
            {
                object[] attributes = method.GetCustomAttributes(typeof(ProtocolMethodAttribute), true);
                if (attributes.Length == 0)
                {
                    continue;
                }

                Debug.Assert(attributes.Length == 1, "Protocol method has multiple ProtocolMethod attributes");

                if (attributes[0].GetType() != this.protocolAttributeType)
                {
                    continue;
                }

                int sortId = ((ProtocolMethodAttribute)attributes[0]).Id;

                if (method.IsStatic)
                {
                    this.AddMethod(sortId, null, method);
                }
                else
                {
                    this.AddMethod(sortId, obj, method);
                }
            }
        }

        /// <summary>
        /// For unit tests only.  For normal use <see cref="RegisterMethodsIn(object)"/> should be used.
        /// Adds a delegate to the method list. This method cannot be called after <see cref="ForgeMethodList()"/> has been called.
        /// </summary>
        /// <param name="method">The delegate to add to the list.</param>
        /// <param name="id">A unique id for the method.</param>
        public void AddDelegate(Delegate method, int id)
        {
            if (null != method)
            {
                this.AddMethod(id, method.Target, method.Method);
            }
        }

        /// <summary>
        /// Prepares the <see cref="MessageParser"/> for method serialization and deserialization.  This
        /// must be called after all methods have been registered, but before any serialization or deserialization.
        /// </summary>
        public void ForgeMethodList()
        {
            if (this.methodList.Count == 0)
            {
                throw new InvalidOperationException("Must register some methods before forging");
            }

            this.methodList.Sort();

            // Set up mappings from methods to method ids
            for (int i = 0; i < this.methodList.Count; i++)
            {
                this.methodList[i].Id = i;

                List<MethodData> methodDatas;
                if (!this.methodToId.TryGetValue(this.methodList[i].Method.MetadataToken, out methodDatas))
                {
                    methodDatas = new List<MethodData>();
                    this.methodToId[this.methodList[i].Method.MetadataToken] = methodDatas;
                }

                methodDatas.Add(this.methodList[i]);
            }

            this.BitsInDelegateId = Math.Max(1, (int)Math.Ceiling(Math.Log((double)this.methodList.Count, 2.0)));
            this.bytesInDelegateId = Utils.BitsToBytes(this.BitsInDelegateId);
            this.hasBeenForged = true;
        }

        /// <summary>
        /// Traverses the methods registered on the <see cref="MessageParser"/> with the given crawler.
        /// </summary>
        /// <param name="crawler">The crawler that is fed the method information.</param>
        public void Traverse(ProtocolStackCrawler crawler)
        {
            foreach (MethodData data in this.methodList)
            {
                crawler.VisitMethod(data.Target, data.Method);
            }
        }

        /// <summary>
        /// Serializes the method call to the envelope.
        /// </summary>
        /// <param name="envelope">The envelope to write to</param>
        /// <param name="function">The method call to serialize</param>
        public
#if !IOS
        virtual // TODO: Not a generic method so probably could be virtual on all platforms?
#endif
        void RemoteCall(IEnvelope envelope, GenericCallBack function)
        {
            this.WriteMethodIDToMessage(envelope, function.Target, function.Method);
            this.TraceRemoteCall(envelope, function);
        }

        /// <summary>
        /// Serializes the method call to the envelope.
        /// </summary>
        /// <typeparam name="A">The type of the first parameter</typeparam>
        /// <param name="envelope">The envelope to write to</param>
        /// <param name="function">The method call to serialize</param>
        /// <param name="a">The first parameter of the call</param>
        public
#if !IOS
        virtual // To permit mocking, but virtual generic methods are not supported in MonoTouch.
#endif
        void RemoteCall<A>(IEnvelope envelope, GenericCallBack<A> function, A a)
        {
            this.WriteMethodIDToMessage(envelope, function.Target, function.Method);
            envelope.Message.PutObject(a, typeof(A));
            this.TraceRemoteCall(envelope, function, a);
        }

        /// <summary>
        /// Serializes the method call to the envelope.
        /// </summary>
        /// <typeparam name="A">The type of the first parameter</typeparam>
        /// <typeparam name="B">The type of the second parameter</typeparam>
        /// <param name="envelope">The envelope to write to</param>
        /// <param name="function">The method call to serialize</param>
        /// <param name="a">The first parameter of the call</param>
        /// <param name="b">The second parameter of the call</param>
        public
#if !IOS
        virtual // To permit mocking, but virtual generic methods are not supported in MonoTouch.
#endif
        void RemoteCall<A, B>(IEnvelope envelope, GenericCallBack<A, B> function, A a, B b)
        {
            this.WriteMethodIDToMessage(envelope, function.Target, function.Method);
            envelope.Message.PutObject(a, typeof(A));
            envelope.Message.PutObject(b, typeof(B));
            this.TraceRemoteCall(envelope, function, a, b);
        }

        /// <summary>
        /// Serializes the method call to the envelope.
        /// </summary>
        /// <typeparam name="A">The type of the first parameter</typeparam>
        /// <typeparam name="B">The type of the second parameter</typeparam>
        /// <typeparam name="C">The type of the third parameter</typeparam>
        /// <param name="envelope">The envelope to write to</param>
        /// <param name="function">The method call to serialize</param>
        /// <param name="a">The first parameter of the call</param>
        /// <param name="b">The second parameter of the call</param>
        /// <param name="c">The third parameter of the call</param>
        public
#if !IOS
        virtual // To permit mocking, but virtual generic methods are not supported in MonoTouch.
#endif
        void RemoteCall<A, B, C>(IEnvelope envelope, GenericCallBack<A, B, C> function, A a, B b, C c)
        {
            this.WriteMethodIDToMessage(envelope, function.Target, function.Method);
            envelope.Message.PutObject(a, typeof(A));
            envelope.Message.PutObject(b, typeof(B));
            envelope.Message.PutObject(c, typeof(C));
            this.TraceRemoteCall(envelope, function, a, b, c);
        }

        /// <summary>
        /// Serializes the method call to the envelope.
        /// </summary>
        /// <typeparam name="A">The type of the first parameter</typeparam>
        /// <typeparam name="B">The type of the second parameter</typeparam>
        /// <typeparam name="C">The type of the third parameter</typeparam>
        /// <typeparam name="D">The type of the fourth parameter</typeparam>
        /// <param name="envelope">The envelope to write to</param>
        /// <param name="function">The method call to serialize</param>
        /// <param name="a">The first parameter of the call</param>
        /// <param name="b">The second parameter of the call</param>
        /// <param name="c">The third parameter of the call</param>
        /// <param name="d">The fourth parameter of the call</param>
        public
#if !IOS
        virtual // To permit mocking, but virtual generic methods are not supported in MonoTouch.
#endif
        void RemoteCall<A, B, C, D>(IEnvelope envelope, GenericCallBack<A, B, C, D> function, A a, B b, C c, D d)
        {
            this.WriteMethodIDToMessage(envelope, function.Target, function.Method);
            envelope.Message.PutObject(a, typeof(A));
            envelope.Message.PutObject(b, typeof(B));
            envelope.Message.PutObject(c, typeof(C));
            envelope.Message.PutObject(d, typeof(D));
            this.TraceRemoteCall(envelope, function, a, b, c, d);
        }

        /// <summary>
        /// Serializes the method call to the envelope.
        /// </summary>
        /// <typeparam name="A">The type of the first parameter</typeparam>
        /// <typeparam name="B">The type of the second parameter</typeparam>
        /// <typeparam name="C">The type of the third parameter</typeparam>
        /// <typeparam name="D">The type of the fourth parameter</typeparam>
        /// <typeparam name="E">The type of the fifth parameter</typeparam>
        /// <param name="envelope">The envelope to write to</param>
        /// <param name="function">The method call to serialize</param>
        /// <param name="a">The first parameter of the call</param>
        /// <param name="b">The second parameter of the call</param>
        /// <param name="c">The third parameter of the call</param>
        /// <param name="d">The fourth parameter of the call</param>
        /// <param name="e">The fifth parameter of the call</param>
        public
#if !IOS
        virtual // To permit mocking, but virtual generic methods are not supported in MonoTouch.
#endif
        void RemoteCall<A, B, C, D, E>(IEnvelope envelope, GenericCallBack<A, B, C, D, E> function, A a, B b, C c, D d, E e)
        {
            this.WriteMethodIDToMessage(envelope, function.Target, function.Method);
            envelope.Message.PutObject(a, typeof(A));
            envelope.Message.PutObject(b, typeof(B));
            envelope.Message.PutObject(c, typeof(C));
            envelope.Message.PutObject(d, typeof(D));
            envelope.Message.PutObject(e, typeof(E));
            this.TraceRemoteCall(envelope, function, a, b, c, d, e);
        }

        /// <summary>
        /// Serializes the method call to the envelope.
        /// </summary>
        /// <typeparam name="A">The type of the first parameter</typeparam>
        /// <typeparam name="B">The type of the second parameter</typeparam>
        /// <typeparam name="C">The type of the third parameter</typeparam>
        /// <typeparam name="D">The type of the fourth parameter</typeparam>
        /// <typeparam name="E">The type of the fifth parameter</typeparam>
        /// <typeparam name="F">The type of the sixth parameter</typeparam>
        /// <param name="envelope">The envelope to write to</param>
        /// <param name="function">The method call to serialize</param>
        /// <param name="a">The first parameter of the call</param>
        /// <param name="b">The second parameter of the call</param>
        /// <param name="c">The third parameter of the call</param>
        /// <param name="d">The fourth parameter of the call</param>
        /// <param name="e">The fifth parameter of the call</param>
        /// <param name="f">The sixth parameter of the call</param>
        public
#if !IOS
        virtual // To permit mocking, but virtual generic methods are not supported in MonoTouch.
#endif
        void RemoteCall<A, B, C, D, E, F>(IEnvelope envelope, GenericCallBack<A, B, C, D, E, F> function, A a, B b, C c, D d, E e, F f)
        {
            this.WriteMethodIDToMessage(envelope, function.Target, function.Method);
            envelope.Message.PutObject(a, typeof(A));
            envelope.Message.PutObject(b, typeof(B));
            envelope.Message.PutObject(c, typeof(C));
            envelope.Message.PutObject(d, typeof(D));
            envelope.Message.PutObject(e, typeof(E));
            envelope.Message.PutObject(f, typeof(F));
            this.TraceRemoteCall(envelope, function, a, b, c, d, e, f);
        }

        /// <summary>
        /// Deserializes the method calls from the given message, invoking each in order.
        /// </summary>
        /// <param name="envelope">The message containing serialized method calls.</param>
        public void Parse(IEnvelope envelope)
        {
            if (envelope == null)
            {
                throw new ArgumentNullException("envelope");
            }

            if (envelope.Message == null)
            {
                throw new ArgumentNullException("envelope.Message");
            }

            if (!this.hasBeenForged)
            {
                throw new InvalidOperationException("Must forge first");
            }

            while (envelope.Message.Available > 0)
            {
                if (envelope.Message.Available < this.bytesInDelegateId)
                {
                    throw new EndOfMessageException();
                }

                byte[] indexBytes = new byte[sizeof(int)];
                int numBytesRead = envelope.Message.ReadBytes(indexBytes, this.bytesInDelegateId);

                if (numBytesRead < this.bytesInDelegateId)
                {
                    throw new ParseException("Failed to read method index.");
                }

                if (!BitConverter.IsLittleEndian)
                {
                    Array.Reverse(indexBytes);
                }

                int delegateIndex = BitConverter.ToInt32(indexBytes, 0);

                if (delegateIndex < 0 || delegateIndex >= this.methodList.Count)
                {
                    throw new ParseException(string.Format("Invalid method id {0}", delegateIndex));
                }

                try
                {
                    MethodData data = this.methodList[delegateIndex];

                    data.Function.CallFromMessageWithStatistics(envelope, this.parserName);

                    this.RecordBytes(numBytesRead, data.Method.Name);
                    Logger.RecordStatistic("ParseCount." + this.parserName, data.Method.Name, 1);
                }
                catch (TargetInvocationException e)
                {
                    if (null != e.InnerException)
                    {
                        // Ugly hack to preserve the stack trace of the inner exception which gets overwritten when it's thrown from here.
                        // Exploits the mechanism used by remoting to pass through the server's stack trace.
                        // http://dotnetjunkies.com/WebLog/chris.taylor/archive/2004/03/03/8353.aspx
                        FieldInfo remoteStackTraceString = typeof(Exception).GetField(
                            "_remoteStackTraceString",
                            BindingFlags.Instance | BindingFlags.NonPublic);

                        if (remoteStackTraceString != null)
                        {
                            remoteStackTraceString.SetValue(e.InnerException, e.InnerException.StackTrace + Environment.NewLine);
                        }

                        throw e.InnerException;
                    }
                    else
                    {
                        throw;
                    }
                }
            }
        }

        /// <summary>
        /// Deserializes the method calls from the given message, invoking the given action for each of the calls contained in the message.
        /// </summary>
        /// <param name="envelope">The message containing serialized method calls.</param>
        /// <param name="describerAction">The action to invoke for each method call.</param>
        /// <remarks>
        /// TODO: Factor out the common parts of this method and Parse.
        /// </remarks>
        public void Describe(IEnvelope envelope, GenericCallBack<string, MethodInfo, object[]> describerAction)
        {
            if (null == envelope)
            {
                throw new ArgumentNullException("envelope");
            }

            if (null == envelope.Message)
            {
                throw new ArgumentNullException("envelope.Message");
            }

            if (!this.hasBeenForged)
            {
                throw new InvalidOperationException("Must forge first");
            }

            while (envelope.Message.Available > 0)
            {
                if (envelope.Message.Available < this.bytesInDelegateId)
                {
                    describerAction("Premature end", null, null);
                    break;
                }

                byte[] indexBytes = new byte[sizeof(int)];
                int numBytesRead;
                numBytesRead = envelope.Message.ReadBytes(indexBytes, this.bytesInDelegateId);

                if (numBytesRead < this.bytesInDelegateId)
                {
                    describerAction("Failed read", null, null);
                    break;
                }

                if (!BitConverter.IsLittleEndian)
                {
                    Array.Reverse(indexBytes);
                }

                int delegateIndex = BitConverter.ToInt32(indexBytes, 0);

                if (delegateIndex < 0 || delegateIndex >= this.methodList.Count)
                {
                    describerAction(String.Format("Unknown method {0}", delegateIndex), null, null);
                    break;
                }

                MethodData data = this.methodList[delegateIndex];

                object[] args = data.Function.ReadParameters(envelope);

                describerAction("Method call", data.Method, args);
            }
        }

        /// <summary>
        /// Writes the id of the method to the envelope.
        /// </summary>
        /// <param name="envelope">The envelope to write to</param>
        /// <param name="target">The target of the method call</param>
        /// <param name="method">The method to call</param>
        public
#if !IOS
        virtual // TODO: Not a generic method so probably could be virtual on all platforms?
#endif
        void WriteMethodIDToMessage(IEnvelope envelope, object target, MethodInfo method)
        {
            if (null == envelope)
            {
                throw new ArgumentNullException("messageEnvelope");
            }

            if (!this.hasBeenForged)
            {
                throw new InvalidOperationException("Must forge first");
            }

            List<MethodData> methodDatas;
            if (!this.methodToId.TryGetValue(method.MetadataToken, out methodDatas))
            {
                throw new UnknownMethodException();
            }

            MethodData data = methodDatas.Find(delegate(MethodData m) { return m.Equals(target, method); });
            if (data == null)
            {
                throw new UnknownMethodException();
            }

            int delegateIndex = data.Id;
            byte[] indexInBytes = BitConverter.GetBytes(delegateIndex);

            // Convert to network byte order.
            // Usally network order is bigendian - but since most of our hosts will be little-endian I leave it as little.
            if (!BitConverter.IsLittleEndian)
            {
                Array.Reverse(indexInBytes);
            }

            envelope.Message.Write(indexInBytes, this.bytesInDelegateId);
        }
        
        /// <summary>
        /// Add a method to the method list. This method cannot be called after <see cref="ForgeMethodList()"/> has been called.
        /// </summary>
        /// <param name="sortId">The sort id.</param>
        /// <param name="target">The object defining the method.</param>
        /// <param name="method">The method to be registered.</param>
        private void AddMethod(int sortId, object target, MethodInfo method)
        {
            if (this.hasBeenForged)
            {
                throw new ForgeMessageException();
            }

            if (null == target && !method.IsStatic)
            {
                throw new ArgumentNullException("target");
            }

            if (null == method)
            {
                throw new ArgumentNullException("method");
            }

            // Sanity check to make sure that we haven't already added a method
            // with the same sort id and target.  It makes no sense to have
            // a duplicate registration for a single target+method [though a single
            // method may be registered multiple times for different instances; the
            // on-wire id specifies both the method and the instance].  Typically
            // a duplicate registration indicates that the same protocol method
            // id has been assigned to two different methods, which is a bug.
            if (this.methodList.Find(m => m.SortId == sortId && m.Target == target) != null)
            {
                string message = string.Format("Attempt to register duplicate method id for a given target (id = {0}, method = {1})", sortId, method.Name);
                Debug.Assert(false, message);
                throw new InvalidOperationException(message);
            }

            MethodData data = new MethodData(sortId, target, method, this.factory);

            if (!this.methodList.Contains(data))
            {
                this.methodList.Add(data);
            }
        }

        /// <summary>
        /// Logs the details of a deserialized method call.
        /// </summary>
        /// <param name="envelope">The envelope that contained the call</param>
        /// <param name="method">The method being called</param>
        /// <param name="args">The arguments of the call</param>
        [Conditional("TRACE")]
        private void TraceRemoteCall(IEnvelope envelope, Delegate method, params object[] args)
        {
            Logger.TraceInformation(
                LogTag.RemoteCall,
                "--> {0} {1}",
                envelope,
                Utils.DescribeCall(method, args));

            if (envelope.Length > 1024)
            {
                try
                {
                    // throw an exception here so we get a useful stack trace:
                    throw new Exception(String.Format("Suspiciously large envelope ({0}b)", envelope.Length));
                }
                catch (Exception e)
                {
                    Logger.TraceException(LogLevel.Warning, e, null);
                }
            }
        }

        /// <summary>
        /// Records statistics associated with this <see cref="MessageParser"/>.
        /// </summary>
        /// <param name="numBytes">The number of bytes processed</param>
        /// <param name="name">The subkey to record the bytes under</param>
        [Conditional("TRACE")]
        private void RecordBytes(int numBytes, string name)
        {
            Logger.RecordStatistic("LayerBytes", this.parserName, numBytes);
            Logger.RecordStatistic("ParseBytes." + this.parserName, name, numBytes);
            if (StatUtils.IsImRelated(this.parserName))
            {
                Logger.RecordStatistic("InterestManagement", "ParseBytes", numBytes);
            }
        }

        /// <summary>
        /// Records information about target / method pairs registered for serialization.
        /// </summary>
        private class MethodData : IEquatable<MethodData>, IComparable<MethodData>
        {
            /// <summary>
            /// To make the sort stable when multiple instances of the same class are being registered.
            /// In this case the on-wire ids will be assigned in the order that the instances had
            /// their methods registered.
            /// </summary>
            private int creationOrder;

            /// <summary>
            /// Initializes a new instance of the MessageParser.MethodData class.
            /// </summary>
            /// <param name="sortId">The sort id</param>
            /// <param name="target">The target to invoke the method on</param>
            /// <param name="method">The method to invoke</param>
            /// <param name="factory">A factory that constructs instances of the given type, used when the GeneralizedFunction is invoking the method
            /// using parameters from a <see cref="MessageBuffer"/></param>
            public MethodData(int sortId, object target, MethodInfo method, GenericCallBackReturn<object, Type> factory)
            {
                this.SortId = sortId;
                this.creationOrder = MessageParser.creationOrder++;
                this.Target = target;
                this.Method = method;
                this.Function = GeneralizedFunction.Make(target, method, factory);
            }

            /// <summary>
            /// Gets the target of the invocation.
            /// </summary>
            public object Target { get; private set; }

            /// <summary>
            /// Gets the method to be invoked on the target.
            /// </summary>
            public MethodInfo Method { get; private set; }

            /// <summary>
            /// Gets the function invoker, used to speed up invocation of the method.
            /// </summary>
            public GeneralizedFunction Function { get; private set; }

            /// <summary>
            /// Gets or sets the id on the wire.
            /// </summary>
            public int Id { get; set; }

            /// <summary>
            /// Gets or sets the id used to determine the on-wire id.
            /// </summary>
            public int SortId { get; set; }

            /// <inheritdoc /> 
            public override bool Equals(object obj)
            {
                return (obj is MethodData) && this.Equals((MethodData)obj);
            }

            /// <inheritdoc /> 
            public override int GetHashCode()
            {
                return this.Method.GetHashCode();
            }

            /// <inheritdoc /> 
            public bool Equals(MethodData data)
            {
                return data != null && this.Equals(data.Target, data.Method);
            }

            /// <inheritdoc /> 
            public bool Equals(object callingObject, MethodInfo method)
            {
                return callingObject == this.Target && method.MetadataToken == this.Method.MetadataToken;
            }

            /// <inheritdoc /> 
            public int CompareTo(MethodData other)
            {
                if (other == null)
                {
                    return +1;  // null is less than everything
                }

                if (this.SortId == other.SortId)
                {
                    return this.creationOrder.CompareTo(other.creationOrder);
                }

                return this.SortId.CompareTo(other.SortId);
            }

            /// <inheritdoc /> 
            public override string ToString()
            {
                return this.Method.Name;
            }
        }
    }
}
