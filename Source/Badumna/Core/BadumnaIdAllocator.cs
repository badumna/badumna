﻿//------------------------------------------------------------------------------
// <copyright file="BadumnaIdAllocator.cs" company="National ICT Australia Limited">
//     Copyright (c) 2012 National ICT Australia Limited. All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;

using Badumna.DataTypes;
using Badumna.DistributedHashTable;
using Badumna.Utilities;

namespace Badumna.Core
{
    /// <summary>
    /// Interface for obtaining unique IDs.
    /// </summary>
    internal interface IBadumnaIdAllocator
    {
        /// <summary>
        /// Get a unique ID.
        /// </summary>
        /// <returns>A new unique ID.</returns>
        BadumnaId GetNextUniqueId();
    }

    /// <summary>
    /// Allocates unique BadumnaId*s.
    /// </summary>
    internal class BadumnaIdAllocator : IBadumnaIdAllocator
    {
        /// <summary>
        /// Manages IDs created before public address available.
        /// </summary>
        private IncompleteIdProxy incompleteProxy;

        /// <summary>
        /// Counter for generating local IDs for IP address-based IDs.
        /// </summary>
        /// <remarks>
        /// Should be initialized to a random number to help prevent recent application interfering with each other.
        /// </remarks>
        private ushort nextLocalId;

        /// <summary>
        /// Counter for generating local IDs for hash key-based IDs.
        /// </summary>
        private ushort nextHashId = 1;

        /// <summary>
        /// Used for generating hash key-based IDs.
        /// </summary>
        private HashKey addressKey;

        /// <summary>
        /// Provides the current public address.
        /// </summary>
        private INetworkAddressProvider addressProvider;

        /// <summary>
        /// Initializes a new instance of the BadumnaIdAllocator class.
        /// </summary>
        /// <param name="addressProvider">Provides the current public address</param>
        /// <param name="connectivityReporter">Provides notification of network connectivity status</param>
        /// <param name="rng">A random number generator.</param>
        public BadumnaIdAllocator(
            INetworkAddressProvider addressProvider,
            INetworkConnectivityReporter connectivityReporter,
            IRandomNumberGenerator rng)
        {
            this.addressProvider = addressProvider;
            this.incompleteProxy = new IncompleteIdProxy(addressProvider, connectivityReporter);
            this.nextLocalId = (ushort)rng.Next(ushort.MaxValue);
        }

        /// <summary>
        /// Creates a new unique id.
        /// </summary>
        /// <returns>A new unique instance of BadumnaId.</returns>
        public BadumnaId GetNextUniqueId()
        {
            if (this.addressKey != null)
            {
                return this.GetNextUniqueId(new PeerAddress(this.addressKey));
            }

            BadumnaId incompleteId = this.GetNextUniqueId(this.addressProvider.PublicAddress);
            incompleteId.GeneratedLocally = true;
            this.incompleteProxy.Add(incompleteId);
            return incompleteId;
        }

        /// <summary>
        /// Create a unique BadumnaId based on a peer address (can be hash or IP based.)
        /// </summary>
        /// <param name="address">The peer address to use.</param>
        /// <returns>A new BadumnaId.</returns>
        public BadumnaId GetNextUniqueId(PeerAddress address)
        {
            if (address.HashAddress != null)
            {
                return new BadumnaId(new PeerAddress(address.HashAddress), this.nextHashId++);  // TODO: This probably doesn't need to construct a new address.
            }

            // make sure the constructed will never equals to BadumnaId.None() which carries a 0 local id. 
            if (this.nextLocalId == 0)
            {
                this.nextLocalId = 1;
            }

            return new BadumnaId(address, this.nextLocalId++);
        }

        /// <summary>
        /// Set the mode of <c>BadumnaId.GetNextUniqueId()</c> to use a specified hash key, or the local peer's public address.
        /// </summary>
        /// <param name="addressKey">A hash key to use when creating new IDs, or null to use the local peer's public address.</param>
        /// <remarks>
        /// TODO: This method is horrible and should be removed.  It exists so that ids generated in distributed controller code
        /// get the hash address rather than the ip address.  There must be a better way...
        /// </remarks>
        public void SetNewIdsAddress(HashKey addressKey)
        {
            this.addressKey = addressKey;
        }

        /// <summary>
        /// Helper class for managing BadumnaIDs created when the local peer's public IP address
        /// was not available.
        /// </summary>
        private class IncompleteIdProxy
        {
            /// <summary>
            /// A list of ids that have been created prior to the public address being set. 
            /// </summary>
            private List<WeakReference> incompleteIds = new List<WeakReference>();

            /// <summary>
            /// Provides the current public address.
            /// </summary>
            private INetworkAddressProvider addressProvider;

            /// <summary>
            /// Initializes a new instance of the BadumnaIdAllocator.IncompleteIdProxy class.
            /// </summary>
            /// <param name="addressProvider">Provides the current public address</param>
            /// <param name="connectivityReporter">Provides notification of network connectivity status</param>
            public IncompleteIdProxy(INetworkAddressProvider addressProvider, INetworkConnectivityReporter connectivityReporter)
            {
                this.addressProvider = addressProvider;
                connectivityReporter.NetworkOnlineEvent += this.WritePublicAddressToIncompleteIds;
            }

            /// <summary>
            /// Add a BadumnaId to the list 
            /// </summary>
            /// <param name="id">The BadumnaId to add.</param>
            public void Add(BadumnaId id)
            {
                this.incompleteIds.Add(new WeakReference(id));
            }

            /// <summary>
            /// Complete the incomplete instances of BadumnaId by updating them with local peer's public address.
            /// </summary>
            private void WritePublicAddressToIncompleteIds()
            {
                List<WeakReference> toRemove = new List<WeakReference>();

                foreach (WeakReference weakId in this.incompleteIds)
                {
                    BadumnaId id = weakId.Target as BadumnaId;

                    if (id != null)
                    {
                        id.OriginalAddress = new PeerAddress(this.addressProvider.PublicAddress);
                    }
                    else
                    {
                        toRemove.Add(weakId);  // target id got garbage collected so it must not be in use anymore
                    }
                }

                this.incompleteIds.RemoveAll(delegate(WeakReference w) { return toRemove.Contains(w); });
            }
        }
    }
}
