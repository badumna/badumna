using System;
using System.Collections.Generic;
using System.Text;

using Badumna.Utilities;

namespace Badumna.Core
{
    enum QosPriority
    {
        Highest = 0,  // 0.0
        High = 64,    // 0.25
        Medium = 128, // 0.5 
        Low = 192,    // 0.75
    }

    /// <summary>
    /// The QualistOfService class specifies the desired quality of service for a specific message. Generally the quality of
    /// service will be specified with a call to ProtocolLayer.GetMessageFor(), where it gets 
    /// attached to the returned envelope and is used by lower layers to determine the behaiviour during transit of the message.
    /// </summary>
    class QualityOfService : IParseable
    {
        // TODO: This class needs serious revision.  There are too many similarly named options that do similar things, and
        //       old options that are no longer obeyed.

        private const byte mReliableBit = 0x01;
        private const byte mInOrderBit = 0x02;
        private const byte mRoutePrecisionBit = 0x04;

        static public QualityOfService Reliable
        {
            get { return new QualityOfService(true); }
        }

        static public QualityOfService Unreliable
        {
            get { return new QualityOfService(false); }
        }

        private bool mIsReliable;

        /// <summary>
        /// Specifies whether the message will be resent until acknowledged.
        /// </summary>
        public bool IsReliable 
        { 
            get { return this.mIsReliable; }
            set { this.mIsReliable = value; }
        }

        private bool mMustBeInOrder;
        
        /// <summary>
        /// Specifies if the message should be discarded if it arrives out of order.
        /// Reliable message are 'out of order' by default.
        /// </summary>
        public bool MustBeInOrder
        {
            get { return this.mMustBeInOrder; }
            set { this.mMustBeInOrder = value; }
        }

        private bool mIsRealtimeMessage;

        public bool IsRealtimeMessage
        {
            get { return this.mIsRealtimeMessage; }
            set { this.mIsRealtimeMessage = value; }
        }

        private int mNumberOfAttempts = 1000;
        
        /// <summary>
        /// The number of attemps that should be made to resend this message if it is reliable.
        /// The actual number of retries will not be greater than Connection.MaxRetries.
        /// </summary>
        public int NumberOfAttempts
        {
            get { return this.mNumberOfAttempts; }
            set { this.mNumberOfAttempts = value; }
        }

        private byte mPriority = (byte)QosPriority.Medium;
        
        /// <summary>
        /// The priority of the message. Used to determine which messages can be discarded if the system is overloaded.
        /// Must be between 0 and 1. A low value indicates high priority.
        /// </summary>
        public float PriorityProbability
        {
            get { return (float)this.mPriority / (float)byte.MaxValue; }
            set
            {
                if (value < 0.0f || value > 1.0f)
                {
                    throw new ArgumentOutOfRangeException("priority", "Must be between 0 and 1");
                }
                this.mPriority = (byte)(value * byte.MaxValue);
            }
        }

        /// <summary>
        /// The priority of the message. Used to determine which messages can be discarded if the system is overloaded.
        /// </summary>
        public QosPriority Priority
        {
            set { this.mPriority = (byte)value; }
        }

        /// <summary>
        /// Applicable only to routed envelopes (such as for the DHT). Specifies whether the message should be 
        /// handled at each hop along the routed path.
        /// </summary>
        private bool mHandleAlongRoutePath;
        public bool HandleAlongRoutePath 
        { 
            get { return this.mHandleAlongRoutePath; }
            set { this.mHandleAlongRoutePath = value; }
        }

        /// <summary>
        /// Indicates if the message to which this quality of servise instance is attached has already been encrypted.
        /// </summary>
        private bool mHasBeenEncrypted;
        public bool HasBeenEncrypted
        {
            get { return this.mHasBeenEncrypted; }
            set { this.mHasBeenEncrypted = value; }
        }

        public QualityOfService()
            : this(QualityOfService.Unreliable)
        {
        }

        public QualityOfService(bool reliable)
        {
            this.IsReliable = reliable;

            // Default case : If its reliable then it doesn't need to be in order.
            this.mMustBeInOrder = !this.mIsReliable;
        }

        public QualityOfService(QualityOfService qos)
        {
            this.mIsRealtimeMessage = qos.mIsRealtimeMessage;
            this.mMustBeInOrder = qos.mMustBeInOrder;
            this.mIsReliable = qos.mIsReliable;
            this.mPriority = qos.mPriority;
            this.mHandleAlongRoutePath = qos.mHandleAlongRoutePath;
        }

        internal virtual void HandleConnectionFailure(PeerAddress destination)
        {
        }

        internal virtual bool HandleFailure(PeerAddress destination, int nthAttempt)
        {
            return true;
        }


        public void ToMessage(MessageBuffer message, Type parameterType)
        {
            byte qos = 0;

            if (this.mIsReliable)
            {
                qos |= QualityOfService.mReliableBit;
            }
            if (this.mMustBeInOrder)
            {
                qos |= QualityOfService.mInOrderBit;
            }
            if (this.mHandleAlongRoutePath)
            {
                qos |= QualityOfService.mRoutePrecisionBit;
            }

            message.Write(qos);
            message.Write(this.mPriority);
        }

        public void FromMessage(MessageBuffer message)
        {
            byte qos = message.ReadByte();

            this.mIsReliable = (qos & QualityOfService.mReliableBit) > 0;
            this.mMustBeInOrder = (qos & QualityOfService.mInOrderBit) > 0;
            this.mHandleAlongRoutePath = (qos & QualityOfService.mRoutePrecisionBit) > 0; 

            this.mPriority = message.ReadByte();
        }

        public override int GetHashCode()
        {
            return this.mIsReliable.GetHashCode();
        }

        public override bool Equals(Object comparand)
        {
            if (comparand is QualityOfService)
            {
                QualityOfService qos = (QualityOfService)comparand;
                return this.mIsReliable == qos.mIsReliable;
            }

            return false;
        }
    }


    /// <remarks>
    /// MultipleQOS allows a single QualityOfService instance to represent an aggregate of multiple compatible QualityOfSerivces.
    /// </remarks>
    class MultipleQOS : QualityOfService
    {
        private List<QualityOfService> mQOSes = new List<QualityOfService>();

        public MultipleQOS(QualityOfService initialQOS)
            : base()
        {
            this.mQOSes.Add(initialQOS);
            this.IsReliable = initialQOS.IsReliable;
            this.MustBeInOrder = initialQOS.MustBeInOrder;
            this.PriorityProbability = initialQOS.PriorityProbability;
        }

        public void Combine(QualityOfService qos)
        {
            mQOSes.Add(qos);
            this.IsReliable = (this.IsReliable || qos.IsReliable);
            this.MustBeInOrder = this.MustBeInOrder & qos.MustBeInOrder;
            this.PriorityProbability = Math.Min(this.PriorityProbability, qos.PriorityProbability);
        }

        internal override void HandleConnectionFailure(PeerAddress destination)
        {
            foreach (QualityOfService qos in this.mQOSes)
            {
                qos.HandleConnectionFailure(destination);
            }
        }

        internal override bool HandleFailure(PeerAddress destination, int nthAttempt)
        {
            bool shouldResend = false;

            foreach (QualityOfService qos in this.mQOSes)
            {
                if (qos.HandleFailure(destination, nthAttempt))
                {
                    shouldResend = true;
                }
            }

            return shouldResend;
        }

    }
}
