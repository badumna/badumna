﻿using System;
using System.Collections.Generic;
using System.Text;

using Badumna.Transport;
using Badumna.Utilities;

namespace Badumna.Core
{
    /// <summary>
    /// A Stream implementation used to write data to message packets.
    /// The Dispose() method will cause the stream message to be sent. It is recommended to use the 
    /// 'using' statement so that Dispose() is called automatically.
    /// </summary>
    /// <example>
    /// using (Stream messageStream = this.GetReliableMessageStream("destination"))
    /// {
    ///   StreamWriter writer = new StreamWriter(messageStream);
    ///   writer.WriteLine("...");
    ///   writer.Flush();
    /// }
    /// </example>
    public class MessageWriteStream : MessageStream
    {
        private LambdaFunction mSendAction;
        
        /// <summary>
        /// Gets a value indicating whether the current stream supports writing. 
        /// </summary>
        public override bool CanWrite { get { return true; } }

        /// <summary>
        /// Gets or sets the position within the current stream. 
        /// </summary>
        public override long Position
        {
            get { return this.Message.WriteOffset; }
            set { this.Message.WriteOffset = this.OriginalPosition + (int)value; }
        }

        internal MessageWriteStream(LambdaFunction sendFunction, BaseEnvelope envelope) 
            : base(envelope.Message, envelope.Length)
        {
            this.mSendAction = sendFunction;
        }
        
        /// <summary>
        /// Writes a sequence of bytes to the current stream and advances the current position within this stream by the number of bytes written. 
        /// </summary>
        /// <param name="buffer">An array of bytes. This method copies count bytes from buffer to the current stream. </param>
        /// <param name="offset">The zero-based byte offset in buffer at which to begin copying bytes to the current stream. </param>
        /// <param name="count">The number of bytes to be written to the current stream. </param>
        public override void Write(byte[] buffer, int offset, int count)
        {
            this.Message.Write(buffer, offset, count);
        }

        /// <summary>
        /// Sends the message
        /// </summary>
        /// <param name="disposing"></param>
        protected override void Dispose(bool disposing)
        {
            if (this.mSendAction != null)
            {
                this.mSendAction.Invoke();
            }
            base.Dispose(disposing);
        }
    }
}
