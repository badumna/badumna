﻿//-----------------------------------------------------------------------
// <copyright file="ITime.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2011 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;

namespace Badumna.Core
{
    /// <summary>
    /// A delegate that provides access to a clock that increases monotonically at the rate of real time
    /// without jumps.
    /// </summary>
    /// <returns>The current time</returns>
    internal delegate TimeSpan GetTime();

    /// <summary>
    /// An interface that provides access to a clock that increases monotonically at the rate of real time
    /// without jumps.
    /// </summary>
    internal interface ITime
    {
        /// <summary>
        /// Gets the current time (expressed as a TimeSpan because there's no intended correspondence between this
        /// time and any actual clock).
        /// </summary>
        TimeSpan Now { get; }
    }
}
