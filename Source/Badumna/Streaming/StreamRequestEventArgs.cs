﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

using Badumna.Core;
using Badumna.Utilities;
using Badumna.Streaming;

namespace Badumna.Streaming
{
    /// <summary>
    /// The state of a streaming operation.
    /// </summary>
    public enum StreamState
    {
        /// <summary>
        /// A streaming request has been created, but streaming of the data has not yet begun.
        /// </summary>
        Waiting,

        /// <summary>
        /// Data is currently being streamed.
        /// </summary>
        InProgress,

        /// <summary>
        /// Streaming has finished successfully.
        /// </summary>
        Complete,

        /// <summary>
        /// Streaming was canceled by the user.
        /// </summary>
        Canceled,

        /// <summary>
        /// Streaming failed due to an unknown error.
        /// </summary>
        Error,

        /// <summary>
        /// The streaming connection was lost.
        /// </summary>
        Disconnected
    }

    /// <summary>
    /// Controls a streaming operation and provides information on its current status.  Used on both sending and receiving end.
    /// </summary>
    public interface IStreamController : IAsyncResult
    {
        /// <summary>
        /// Total bytes to be transferred in the streaming operation.
        /// </summary>
        long BytesTotal { get; }

        /// <summary>
        /// Number of bytes transferred so far.
        /// </summary>
        long BytesTransfered { get; }

        /// <summary>
        /// Estimated transfer rate in kilobytes / second.
        /// </summary>
        double TransferRateKBps { get; }

        /// <summary>
        /// Estimated time required to complete the streaming operation.
        /// </summary>
        TimeSpan EstimatedTimeRemaining { get; }

        /// <summary>
        /// The current state of the streaming operation.
        /// </summary>
        StreamState CurrentState { get; }

        /// <summary>
        /// True if the streaming operation has completed successfully.
        /// </summary>
        bool WasSuccessful { get; }

        /// <summary>
        /// Triggered when any of the properties on this instance have changed.
        /// </summary>
        event EventHandler InformationChanged;

        /// <summary>
        /// Cancels the streaming operation.
        /// </summary>
        void Cancel();
    }

    /// <summary>
    /// Base class for streaming request events.  Common to both SendStreamEventArgs and ReceiveStreamEventArgs.
    /// Not intended be used as a base class by client applications.
    /// </summary>
    public abstract class StreamRequestEventArgs : EventArgs
    {
        private String mStreamTag;
        private String mStreamName;
        private double mSizeKB;
        private String mUserName;
        private byte[] mAdditionalData;
        private object mCallbackState;
        private AppliedFunctionReturn<IStreamController, StreamRequest, PeerAddress, int, StreamRequestEventArgs, AsyncCallback> mAcceptCallback;

        /// <summary>
        /// Used internally.
        /// </summary>
        protected bool mIsAccepted;

        /// <summary>
        /// An application specified tag used to select the handler for the stream.
        /// </summary>
        public String StreamTag { get { return this.mStreamTag; } }

        /// <summary>
        /// A human readable name for the transfer.
        /// </summary>
        public String StreamName { get { return this.mStreamName; } }

        /// <summary>
        /// Size of the transfer if available.
        /// </summary>
        public double SizeKiloBytes { get { return this.mSizeKB; } }

        /// <summary>
        /// The username of the stream sender.
        /// </summary>
        public String UserName { get { return this.mUserName; } }

        /// <summary>
        /// Additional application data attached to the transfer request.
        /// </summary>
        public byte[] AdditionalData { get { return this.mAdditionalData; } }

        /// <summary>
        /// Indicates whether this transfer request has been accepted.
        /// </summary>
        public bool IsAccepted { get { return this.mIsAccepted; } }

        private PeerAddress mPeersAddress;
        internal PeerAddress PeersAddress { get { return this.mPeersAddress; } }

        internal object CallbackState { get { return this.mCallbackState; } }

        internal StreamRequestEventArgs(StreamRequest request, PeerAddress source, int dataRateBps,
            GenericCallBackReturn<IStreamController, StreamRequest, PeerAddress, int, StreamRequestEventArgs, AsyncCallback> callback)
        {
            StreamingManager.ParseTaggedName(request.FileName, out this.mStreamTag, out this.mStreamName);
            this.mSizeKB = request.FileSize;
            this.mUserName = request.UserName;
            this.mAdditionalData = request.AdditionalData;
            this.mPeersAddress = source;
            this.mAcceptCallback = new AppliedFunctionReturn<IStreamController, StreamRequest, PeerAddress, int, StreamRequestEventArgs, AsyncCallback>
             (callback, request, source, dataRateBps, this, null);
        }


        /// <summary>
        /// Reject request to transfer the file to this machine.
        /// </summary>
        public void Reject()
        {
            this.mIsAccepted = false;
            if (this.mAcceptCallback != null)
            {
                this.mAcceptCallback.Invoke();
                this.mAcceptCallback = null;   // So it can be called only once
            }
        }

        internal IStreamController Accept(AsyncCallback callback, object callbackState)
        {
            this.mCallbackState = callbackState;
            this.mIsAccepted = true;
            if (this.mAcceptCallback != null)
            {
                IStreamController controller = this.mAcceptCallback.Invoke(callback);
                this.mAcceptCallback = null;   // So it can be called only once
                return controller;
            }

            return null;
        }

        /// <summary>
        /// Accept the request, using a file as the source/destination.
        /// </summary>
        abstract public IStreamController AcceptFile(String pathName, AsyncCallback completionCallback, object callbackState);

        /// <summary>
        /// Accept the request, using a stream as the source/destionation.
        /// </summary>
        abstract public IStreamController AcceptStream(Stream stream, AsyncCallback completionCallback, object callbackState);
    }

    /// <summary>
    /// Passed to the receive stream event handler when a peer attempts to send a stream to this peer.
    /// </summary>
    public sealed class ReceiveStreamEventArgs : StreamRequestEventArgs
    {
        private Stream mDestination;

        /// <summary>
        /// The stream to which data is written if the transfer is accepted.
        /// </summary>
        internal Stream DestinationStream { get { return this.mDestination; } }

        internal ReceiveStreamEventArgs(StreamRequest request, PeerAddress source, int dataRateBps,
            GenericCallBackReturn<IStreamController, StreamRequest, PeerAddress, int, StreamRequestEventArgs, AsyncCallback> callback)
            : base(request, source, dataRateBps, callback)
        {
        }

        /// <summary>
        /// Accept the request. Copying the file to the given path name.
        /// </summary>
        /// <param name="pathName">The full name of the destination file.</param>
        /// <param name="callback">The completion callback. Called when the transfer has stopped by error or completion</param>
        /// <param name="callbackState">User defined state object which is passed to the completionCallback.</param>
        /// <returns>The stream controller, used for controling the transfer operation</returns>
        public override IStreamController AcceptFile(string pathName, AsyncCallback callback, object callbackState)
        {
            if (this.mIsAccepted)
            {
                throw new InvalidOperationException("Cannot accept a transfer more than once.");
            }

            if (!Path.IsPathRooted(pathName))
            {
                throw new ArgumentException("File name must contain full path name.", "pathName");
            }

            CopyOnCompletionTask completionTask = new CopyOnCompletionTask(pathName, true, callback);

            return this.AcceptStream(completionTask.GetStream(), completionTask.RequestFileOperationComplete, callbackState);
        }

        /// <summary>
        /// Accept the request. Streaming to the given stream.
        /// </summary>
        /// <param name="stream">The writeable stream to which the output is piped</param>
        /// <param name="completionCallback">The completion callback. Called when the transfer has stopped by error or completion</param>
        /// <param name="callbackState">User defined state object which is passed to the completionCallback.</param>
        /// <returns>The stream controller, used for controling the transfer operation</returns>
        public override IStreamController AcceptStream(Stream stream, AsyncCallback completionCallback, object callbackState)
        {
            if (this.mIsAccepted)
            {
                throw new InvalidOperationException("Cannot accept a transfer more than once.");
            }

            this.mDestination = stream;

            IStreamController controller = base.Accept(completionCallback, callbackState);

            if (null == controller && completionCallback != null)
            {
                controller = new ErrorProgress();
                completionCallback(controller);
            }

            return controller;
        }

    }

    /// <summary>
    /// Passed to the stend stream event handler when a peer requests that this peer send a stream.
    /// </summary>
    public sealed class SendStreamEventArgs : StreamRequestEventArgs
    {
        private Stream mSource;

        /// <summary>
        /// The stream to which data is written if the transfer is accepted.
        /// </summary>
        internal Stream SourceStream { get { return this.mSource; } }

        internal SendStreamEventArgs(StreamRequest request, PeerAddress source, int dataRateBps,
            GenericCallBackReturn<IStreamController, StreamRequest, PeerAddress, int, StreamRequestEventArgs, AsyncCallback> callback)
            : base(request, source, dataRateBps, callback)
        {
        }

        /// <summary>
        /// Accept the request to send the file
        /// </summary>
        /// <param name="pathName">The full name of the file to send.</param>
        /// <param name="callback">The completion callback. Called when the transfer has stopped by error or completion</param>
        /// <param name="callbackState">User defined state object which is passed to the completionCallback.</param>
        /// <returns>The stream controller, used for controling the transfer operation</returns>
        public override IStreamController AcceptFile(String pathName, AsyncCallback callback, object callbackState)
        {
            if (this.mIsAccepted)
            {
                throw new InvalidOperationException("Cannot accept a transfer request more than once.");
            }

            if (!Path.IsPathRooted(pathName))
            {
                throw new ArgumentException("File name must contain full path name.", "pathName");
            }

            try
            {
                return this.AcceptStream(new FileStream(pathName, FileMode.Open, FileAccess.Read, FileShare.Read), callback, callbackState);
            }
            catch (IOException e)
            {
                Logger.TraceException(LogLevel.Warning, e, "Unable to access file for transfer.");
                base.Reject();
            }

            return null;
        }

        /// <summary>
        /// Accept the request to send the file
        /// </summary>
        /// <param name="stream">The readable stream which is sent to the remote peer.</param>
        /// <param name="completionCallback">The completion callback. Called when the transfer has stopped by error or completion</param>
        /// <param name="callbackState">User defined state object which is passed to the completionCallback.</param>
        /// <returns>The stream controller, used for controling the transfer operation</returns>
        public override IStreamController AcceptStream(Stream stream, AsyncCallback completionCallback, object callbackState)
        {
            if (this.mIsAccepted)
            {
                throw new InvalidOperationException("Cannot accept a transfer request more than once.");
            }

            this.mIsAccepted = true;
            this.mSource = stream;

            IStreamController controller = base.Accept(completionCallback, callbackState);

            if (null == controller && completionCallback != null)
            {
                controller = new ErrorProgress();
                completionCallback(controller);
            }

            return controller;
        }

    }


    class ErrorProgress : IStreamController
    {
        #region IStreamController Members

        public long BytesTotal { get { return 1; } }
        public long BytesTransfered { get { return 0; } }
        public double TransferRateKBps { get { return 0.0; } }
        public TimeSpan EstimatedTimeRemaining { get { return TimeSpan.Zero; } }
        public StreamState CurrentState { get { return StreamState.Error; } }
        public bool WasSuccessful { get { return false; } }

        private EventHandler informationChangedDelegate;
        public event EventHandler InformationChanged
        {
            add { this.informationChangedDelegate += value; }
            remove { this.informationChangedDelegate -= value; }
        }

        public void Cancel()
        {
        }

        #endregion

        public void TriggerChanged()
        {
            EventHandler handler = this.informationChangedDelegate;
            if (handler != null)
            {
                handler.Invoke(this, null);
            }
        }

        #region IAsyncResult Members

        private object mState;
        public object AsyncState
        {
            get { return this.mState; }
            set { this.mState = value; }
        }

        private System.Threading.ManualResetEvent mWaitHandle = new System.Threading.ManualResetEvent(true);
        public System.Threading.WaitHandle AsyncWaitHandle
        {
            get { return this.mWaitHandle; }
        }

        public bool CompletedSynchronously { get { return true; } }
        public bool IsCompleted { get { return true; } }

        #endregion
    }
}