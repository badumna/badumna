﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Threading;

using Badumna.Core;
using Badumna.Utilities;
using Badumna.Transport;

#if DEBUG
using NUnit.Framework;
#endif


namespace Badumna.Streaming
{
    class TransferSource : StreamingAgent
    {
        private InsertionSortedList<Segment> mSentButUnacknowledgedSegments;

        private Thread mReadThread;
        private bool mIsReading = false;
        private AutoResetEvent mReadAvailable;

        public TransferSource(CyclicalID.UShortID localStreamId, Stream stream, PeerAddress destination, AsyncCallback completionCallback)
            : base(localStreamId, stream, destination, completionCallback)
        {
            this.mSentButUnacknowledgedSegments = new InsertionSortedList<Segment>();
        }

        public override void BeginTransfer(CyclicalID.UShortID streamId, int dataRateBps, long length)
        {
            base.BeginTransfer(streamId, dataRateBps, length);
            this.SetEmptiedCallback(this.ChannelEmpty);

            this.mReadAvailable = new AutoResetEvent(false);
            this.mReadThread = new Thread(this.ReadLoop);
            this.mReadThread.Priority = ThreadPriority.BelowNormal;
            this.mReadThread.IsBackground = true;
            this.mReadThread.Start();           
        }

        private void ReadLoop()
        {
            this.mIsReading = true;
            CyclicalID.UShortID nextSegmentNumber = new CyclicalID.UShortID(0);

            this.mReadAvailable.Set();

            do
            {
                this.mReadAvailable.WaitOne();
                Segment segment = null;

                do
                {
                    segment = new Segment(nextSegmentNumber, this);
                    nextSegmentNumber.Increment();

                    try
                    {
                        if (this.Stream.CanRead)
                        {
                            segment.Read(this.Stream);
                        }
                    }
                    catch (IOException e)
                    {
                        Logger.TraceInformationLogTag.Streaming, "IOException in file transfer : {0}", e.Message);
                        this.Shutdown(TerminationCode.ReadFailure);
                        return;
                    }
                }
                while (this.QueueSegment(segment));
            }
            while (this.mIsReading);
        }

        private bool QueueSegment(Segment segment)
        {
            if (segment.Length > 0)
            {
                this.AddTransferedAmount(segment.Length);
                NetworkEventQueue.Instance.Push(this.Put, segment);

                lock (this.mSentButUnacknowledgedSegments)
                {
                    this.mSentButUnacknowledgedSegments.Add(segment);
                    return this.mSentButUnacknowledgedSegments.Count < StreamingAgent.WindowSize;
                }
            }

            if (segment.Length == 0)
            {
                this.mIsReading = false;
                this.TriggerEndOfStreamEvent(segment.Number);
                this.Stream.Close();
            }

            return false;
        }

        private void CheckIfComplete(bool channelIsEmpty)
        {
            lock (this.mSentButUnacknowledgedSegments)
            {
                if (channelIsEmpty && !this.mIsReading && this.mSentButUnacknowledgedSegments.Count == 0 && !this.IsCompleted)
                {
                    Logger.TraceInformationLogTag.Streaming, "Read complete and all segments acknowledged for stream {0}.", this.LocalStreamId);
                    this.TriggerTerminatedEvent(TerminationCode.EndOfStream);
                }
            }
        }

        private void ChannelEmpty(Channel<Segment> agent)
        {
            this.CheckIfComplete(true);
        }

        public void ProcessAcknowledgements(AcknowledgementSet acknowledgements)
        {
            lock (this.mSentButUnacknowledgedSegments)
            {
                List<Segment> sentSegments = new List<Segment>(this.mSentButUnacknowledgedSegments);

                // Copyied to a new list because removal is O(n) so its faster to add all items again than remove half of them.
                this.mSentButUnacknowledgedSegments.Clear();
                foreach (Segment segment in sentSegments)
                {
                    if (!acknowledgements.Contains(segment.Number))
                    {
                        if (segment.Number < acknowledgements.Maximum)
                        {
                            this.Put(segment);
                        }
                        this.mSentButUnacknowledgedSegments.Add(segment);
                    }
                }
            }
         
            if (this.mIsReading)
            {
                this.mReadAvailable.Set();
            }
            else
            {
                this.CheckIfComplete(this.Peek() == null);
            }
        }

        // Must be thread safe
        private void Shutdown(TerminationCode code)
        {
            try
            {
                this.Stream.Close();
            }
            catch { }

            if (!this.IsCompleted)
            {
                this.TriggerTerminatedEvent(code);
            }
        }
    }


    /*
    #region Unit tests
#if DEBUG


    [TestFixture]
    [DontPerformCoverage]
    public class TransferSourceTester
    {

        [SetUp]
        public void Initialize()
        {
            Simulator.CreateAndUseSimulatorQueue();
        }

        [TearDown]
        public void Terminate()
        {
            NetworkEventQueue.CreateAndUseDefaultQueue();
        }

        private Stream GenerateStream(int length, out ushort checksum)
        {
            Random generator = new Random((int)DateTime.Now.Ticks);
            byte[] buffer = new byte[length];

            generator.NextBytes(buffer);
            checksum = CRC.CRC16(buffer);
            MemoryStream stream = new MemoryStream(buffer);

            stream.Position = 0;
            return stream;
        }


        public void SendTest(int length, int dataRate)
        {

        }


    }


#endif
    #endregion
    */
}

