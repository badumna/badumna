﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

using Badumna;
using Badumna.Core;
using Badumna.Utilities;
using Badumna.Transport;

namespace Badumna.Streaming
{
    class StreamingProtocol : TransportProtocol
    {
        private const int DataRateBps = 10000000;
        private CyclicalID.UShortID mNextStreamId = new CyclicalID.UShortID();
        //  private PgpsScheduler<Segment> mScheduler;
        //  private RateLimiter<Segment> mLimiter;
        private ResourceAllocator<CyclicalID.UShortID> mInboundAllocator = new ResourceAllocator<CyclicalID.UShortID>(StreamingProtocol.DataRateBps, 0.0);

        private Dictionary<PeerAddress, Dictionary<CyclicalID.UShortID, StreamingAgent>> mTransfers;
        private readonly object mTransfersLock = new object();

        private EventHandler<ReceiveStreamEventArgs> receiveRequestEventDelegate;
        public event EventHandler<ReceiveStreamEventArgs> ReceiveRequestEvent
        {
            add { this.receiveRequestEventDelegate += value; }
            remove { this.receiveRequestEventDelegate -= value; }
        }

        private EventHandler<SendStreamEventArgs> sendRequestEventDelegate;
        public event EventHandler<SendStreamEventArgs> SendRequestEvent
        {
            add { this.sendRequestEventDelegate += value; }
            remove { this.sendRequestEventDelegate -= value; }
        }

        private IChannelFunnel<TransportEnvelope> mChannelSink;

        /// <summary>
        /// The queue to use for scheduling events.
        /// </summary>
        private NetworkEventQueue eventQueue;

        /// <summary>
        /// Provides access to the current time.
        /// </summary>
        private ITime timeKeeper;

        /// <summary>
        /// Reports on network connectivity status.
        /// </summary>
        private INetworkConnectivityReporter connectivityReporter;

        public StreamingProtocol(
            TransportProtocol parent,
            IChannelFunnel<TransportEnvelope> connectionTable,
            NetworkEventQueue eventQueue,
            ITime timeKeeper,
            INetworkConnectivityReporter connectivityReporter,
            IPeerConnectionNotifier connectionNotifier)
            : base(parent)
        {
            //     this.mScheduler = new PgpsScheduler<Segment>();
           /* this.mLimiter = new RateLimiter<Segment>(
                delegate { return StreamingProtocol.DataRateBps; },
                this.mScheduler,
                this.WriteSegmentToEnvelope);
            */
            this.mChannelSink = connectionTable;
            this.eventQueue = eventQueue;
            this.timeKeeper = timeKeeper;
            this.connectivityReporter = connectivityReporter;

            this.mTransfers = new Dictionary<PeerAddress, Dictionary<CyclicalID.UShortID, StreamingAgent>>();
            connectionNotifier.ConnectionLostEvent += this.ConnectionLostEvent;
        }

        // TODO : Save the state of the stream so that we can commence if the connection is restarted
        private void ConnectionLostEvent(PeerAddress address)
        {
            Dictionary<CyclicalID.UShortID, StreamingAgent> agentsForPeer = null;

            lock (this.mTransfersLock)
            {
                if (this.mTransfers.TryGetValue(address, out agentsForPeer))
                {
                    List<StreamingAgent> departingAgents = new List<StreamingAgent>(agentsForPeer.Values);

                    foreach (StreamingAgent agent in departingAgents)
                    {
                        if (agent is Source)
                        {
                            this.DetachChannel(address, agent as Source);
                        }

                        agent.ConnectionLost();
                    }
                }
            }
        }

        public IStreamController BeginSendReliableStream(Stream sourceStream, String streamName,
            PeerAddress address, String userName, AsyncCallback completionCallback, object callbackState)
        {
            int dataRateBps = StreamingProtocol.DataRateBps;
            long length = 0;

            try
            {
                length = sourceStream.Length;
            }
            catch (NotSupportedException)
            {
            }

            ReliableTransferSource source = this.CreateFileSource(sourceStream, address, dataRateBps, completionCallback, callbackState);
            StreamRequest request = new StreamRequest(streamName, null, length, userName, this.mNextStreamId.Previous);

            this.SendReceiveRequest(request, address, dataRateBps);

            return source;
        }

        public IStreamController BeginRequestReliableStream(String streamName, Stream destinationStream, PeerAddress address, String userName,
            AsyncCallback completionCallback, object callbackState)
        {
            int dataRateBps = StreamingProtocol.DataRateBps;

            try
            {
                if (destinationStream != null)
                {
                    ReliableTransferSink sink = new ReliableTransferSink(
                        this.mNextStreamId,
                        destinationStream,
                        address,
                        completionCallback,
                        callbackState,
                        this.eventQueue,
                        this.timeKeeper,
                        this.connectivityReporter);
                    StreamRequest request = new StreamRequest(streamName, null, 0, userName, this.mNextStreamId);

                    this.mNextStreamId.Increment();
                    this.AddAgent(sink.LocalStreamId, sink);
                    this.mInboundAllocator.AllocateSlice(sink.PeersStreamId, dataRateBps, 0.5f);
                    sink.Acknowledge += this.SendAcknowledgements;
                    sink.TranfserTerminated += this.TerminateStream;

                    this.SendSendRequest(request, address, dataRateBps);

                    return sink;
                }
            }
            catch (IOException e)
            {
                Logger.TraceException(LogLevel.Warning, e, "Unable to create reliable sink for transfer.");
            }

            return null;
        }
        /*
        private IStreamController CreateSource(Stream stream, PeerAddress address, int dataRateBps, AsyncCallback completionCallback)
        {
            try
            {
                ReliableTransferSource source = new ReliableTransferSource(this.mNextStreamId, stream, address, completionCallback);

                this.AddAgent(source.LocalStreamId, source);
                this.mNextStreamId.Increment();
                this.mScheduler.AddInputChannel(source, 0.5);
                source.TranfserTerminated += this.TerminateStream;
                source.EndOfStream += this.SendEndNotification;

                return source;
            }
            catch (IOException e)
            {
                Logger.TraceException(LogLevel.Warning, e, "Unable to access file for transfer.");
            }

            return null;
        }
        */

        private ReliableTransferSource CreateFileSource(Stream stream, PeerAddress address, int dataRateBps,
            AsyncCallback completionCallback, object callbackState)
        {
            ReliableTransferSource source = new ReliableTransferSource(
                this.mNextStreamId,
                stream,
                address, 
                this.WriteSegmentToEnvelope,
                completionCallback,
                callbackState,
                this.eventQueue,
                this.timeKeeper,
                this.connectivityReporter);

            this.AddAgent(source.LocalStreamId, source);
            this.mNextStreamId.Increment();
            this.AttachChannel(address, source);
            source.TranfserTerminated += this.TerminateStream;
            source.EndOfStream += this.SendEndNotification;

            return source;
        }

        private IStreamController CreateReliableSink(
            StreamRequest request,
            Stream stream,
            PeerAddress source,
            int dataRateBps,
            AsyncCallback completionCallback,
            object callbackState)
        {
            try
            {
                if (stream != null)
                {
                    ReliableTransferSink sink = new ReliableTransferSink(
                        this.mNextStreamId,
                        stream,
                        source,
                        completionCallback,
                        callbackState,
                        this.eventQueue,
                        this.timeKeeper,
                        this.connectivityReporter);

                    this.mNextStreamId.Increment();
                    this.AddAgent(sink.LocalStreamId, sink);
                    this.mInboundAllocator.AllocateSlice(sink.PeersStreamId, dataRateBps, 0.5f);
                    sink.BeginTransfer(request.StreamId, dataRateBps, request.FileSize);
                    sink.Acknowledge += this.SendAcknowledgements;
                    sink.TranfserTerminated += this.TerminateStream;

                    return sink;
                }
            }
            catch (IOException e)
            {
                Logger.TraceException(LogLevel.Warning, e, "Unable to create reliable sink for transfer.");
            }

            return null;
        }

        private void AttachChannel(PeerAddress address, Source streamSource)
        {
            this.mChannelSink.AddChannel(address, streamSource.EnvelopeSource, ChannelGroup.Streaming);
        }

        private void DetachChannel(PeerAddress address, Source streamSource)
        {
            this.mChannelSink.RemoveChannel(address, streamSource.EnvelopeSource);
        }

        private void AddAgent(CyclicalID.UShortID streamId, StreamingAgent agent)
        {
            Dictionary<CyclicalID.UShortID, StreamingAgent> agentsForPeer = null;

            lock (this.mTransfersLock)
            {
                if (!this.mTransfers.TryGetValue(agent.PeersAddress, out agentsForPeer))
                {
                    agentsForPeer = new Dictionary<CyclicalID.UShortID, StreamingAgent>();
                    this.mTransfers.Add(agent.PeersAddress, agentsForPeer);
                }
            }

            agentsForPeer.Add(streamId, agent);
        }

        private void RemoveAgent(PeerAddress peerAddress, CyclicalID.UShortID streamId)
        {
            Dictionary<CyclicalID.UShortID, StreamingAgent> agentsForPeer = null;

            lock (this.mTransfersLock)
            {
                if (this.mTransfers.TryGetValue(peerAddress, out agentsForPeer))
                {
                    StreamingAgent agent = null;

                    if (agentsForPeer.TryGetValue(streamId, out agent))
                    {
                        if (agent is Source)
                        {
                            this.DetachChannel(peerAddress, agent as Source);
                        }

                        agentsForPeer.Remove(streamId);
                    }

                    if (agentsForPeer.Count == 0)
                    {
                        this.mTransfers.Remove(peerAddress);
                    }
                }
            }
        }

        public TransportEnvelope WriteSegmentToEnvelope(Segment segment)
        {
            if (segment == null)
            {
                System.Diagnostics.Debug.Assert(false, "Null segement given to WriteSegmentToEnvelope");
                return null;
            }

            if (segment.LocalOnlySourceAgent == null)
            {
                System.Diagnostics.Debug.Assert(false, "Segement without localOnlySourceAgent given to WriteSegmentToEnvelope");
                return null;
            }

            if (segment.LocalOnlySourceAgent.PeersAddress == null)
            {
                System.Diagnostics.Debug.Assert(false, "Segement withour valid peer address given to WriteSegmentToEnvelope");
                return null;
            }

            TransportEnvelope enveleope = this.GetMessageFor(segment.LocalOnlySourceAgent.PeersAddress, QualityOfService.Unreliable);

            this.RemoteCall(enveleope, this.RecieveSegment, segment.LocalOnlySourceAgent.PeersStreamId, segment);
            return enveleope;
        }

        private void SendAcknowledgements(object sender, AcknowledgeEventArgs args)
        {
            Sink sink = sender as Sink;

            if (sink != null)
            {
                TransportEnvelope envelope = this.GetMessageFor(sink.PeersAddress, QualityOfService.Reliable);

                this.RemoteCall(envelope, this.ReceiveAcknowledgements, sink.PeersStreamId, args.Acknowledgements, args.WindowSize);
                this.SendMessage(envelope);
            }
            else
            {
                System.Diagnostics.Debug.Assert(false, "Sender is not a TransferSink.");
            }
        }

        public void SendEndNotification(object sender, EndOfStreamEventArgs args)
        {
            StreamingAgent agent = sender as StreamingAgent;

            if (agent != null)
            {
                TransportEnvelope envelope = this.GetMessageFor(agent.PeersAddress, QualityOfService.Reliable);

                this.RemoteCall(envelope, this.EndOfStream, agent.PeersStreamId, args.LastSegmentNumber);
                this.SendMessage(envelope);
            }
            else
            {
                System.Diagnostics.Debug.Assert(false, "Sender is not a TransferAgent.");
            }
        }

        public void TerminateStream(object sender, StreamTerminatedEventArgs args)
        {
            StreamingAgent agent = sender as StreamingAgent;

            if (agent != null)
            {
                if (args.Code != TerminationCode.EndOfStream)
                {
                    TransportEnvelope envelope = this.GetMessageFor(agent.PeersAddress, QualityOfService.Reliable);

                    this.RemoteCall(envelope, this.TerminateStream, agent.PeersStreamId, (byte)args.Code);
                    this.SendMessage(envelope);
                }

                this.RemoveAgent(agent.PeersAddress, agent.LocalStreamId);
            }
            else
            {
                System.Diagnostics.Debug.Assert(false, "Sender is not a TransferAgent.");
            }
        }

        private T GetAgent<T>(PeerAddress address, CyclicalID.UShortID requestId) where T : StreamingAgent
        {
            Dictionary<CyclicalID.UShortID, StreamingAgent> agentsForPeer = null;

            lock (this.mTransfersLock)
            {
                if (this.mTransfers.TryGetValue(this.CurrentEnvelope.Source, out agentsForPeer))
                {
                    StreamingAgent agent = null;

                    if (agentsForPeer.TryGetValue(requestId, out agent))
                    {
                        return agent as T;
                    }
                }
            }

            return null;
        }

        private IStreamController ReceiveRequestCallback(StreamRequest request, PeerAddress peer, int dataRateBps, StreamRequestEventArgs args,
            AsyncCallback callback)
        {
            ReceiveStreamEventArgs receiveArgs = args as ReceiveStreamEventArgs;

            if (receiveArgs != null && receiveArgs.IsAccepted)
            {
                IStreamController transfer = this.CreateReliableSink(request, receiveArgs.DestinationStream, peer, dataRateBps, callback, args.CallbackState);

                if (transfer != null)
                {
                    TransportEnvelope envelope = this.GetMessageFor(peer, QualityOfService.Reliable);

                    this.RemoteCall(envelope, this.AcceptTransfer,
                        request.StreamId, this.mNextStreamId.Previous, (int)(dataRateBps * 1000.0 / 8.0), request.FileSize);
                    this.SendMessage(envelope);

                    return transfer;
                }
                else
                {
                    Logger.TraceInformation(LogTag.Streaming, "Forced to reject request becuase the streaming agent was not found.");
                }
            }

            TransportEnvelope rejectionEnvelope = this.GetMessageFor(peer, QualityOfService.Reliable);

            this.RemoteCall(rejectionEnvelope, this.RejectTransfer, request.StreamId);
            this.SendMessage(rejectionEnvelope);

            return null;
        }

        private IStreamController SendRequestCallback(StreamRequest request, PeerAddress peer, int dataRateBps, StreamRequestEventArgs args,
           AsyncCallback callback)
        {
            SendStreamEventArgs sendArgs = args as SendStreamEventArgs;

            if (sendArgs != null && sendArgs.IsAccepted)
            {
                ReliableTransferSource transferAgent = this.CreateFileSource(sendArgs.SourceStream, peer, dataRateBps, callback, args.CallbackState);

                if (transferAgent != null)
                {
                    long length = transferAgent.Stream.CanSeek ? transferAgent.Stream.Length : 0;

                    transferAgent.BeginTransfer(request.StreamId, dataRateBps, length);

                    TransportEnvelope envelope = this.GetMessageFor(peer, QualityOfService.Reliable);

                    this.RemoteCall(envelope, this.AcceptTransfer,
                        request.StreamId, this.mNextStreamId.Previous, (int)(dataRateBps * 1000.0 / 8.0), length);
                    this.SendMessage(envelope);

                    return transferAgent;
                }
                else
                {
                    Logger.TraceInformation(LogTag.Streaming, "Forced to reject request becuase the streaming agent was not found.");
                }
            }

            TransportEnvelope rejectionEnvelope = this.GetMessageFor(peer, QualityOfService.Reliable);

            this.RemoteCall(rejectionEnvelope, this.RejectTransfer, request.StreamId);
            this.SendMessage(rejectionEnvelope);

            return null;
        }

        #region Protocol Methods

        private void SendReceiveRequest(StreamRequest request, PeerAddress address, int dataRateBps)
        {
            TransportEnvelope envelope = this.GetMessageFor(address, QualityOfService.Reliable);

            this.RemoteCall(envelope, this.RequestReceiveStream, request, dataRateBps);
            this.SendMessage(envelope);
        }

        [ConnectionfulProtocolMethodAttribute(ConnectionfulMethod.RequestReceiveStream)]
        private void RequestReceiveStream(StreamRequest request, int dataRateBps)
        {
            EventHandler<ReceiveStreamEventArgs> handler = this.receiveRequestEventDelegate;
            if (handler != null)
            {
                handler.Invoke(this, new ReceiveStreamEventArgs(request, this.CurrentEnvelope.Source, dataRateBps, this.ReceiveRequestCallback));
            }
        }

        private void SendSendRequest(StreamRequest request, PeerAddress address, int dataRateBps)
        {
            TransportEnvelope envelope = this.GetMessageFor(address, QualityOfService.Reliable);

            this.RemoteCall(envelope, this.RequestSendStream, request, dataRateBps);
            this.SendMessage(envelope);
        }

        [ConnectionfulProtocolMethodAttribute(ConnectionfulMethod.RequestSendStream)]
        private void RequestSendStream(StreamRequest request, int dataRateBps)
        {
            EventHandler<SendStreamEventArgs> handler = this.sendRequestEventDelegate;
            if (handler != null)
            {
                handler.Invoke(this, new SendStreamEventArgs(request, this.CurrentEnvelope.Source, dataRateBps, this.SendRequestCallback));
            }
        }


        [ConnectionfulProtocolMethod(ConnectionfulMethod.AcceptTransfer)]
        private void AcceptTransfer(CyclicalID.UShortID requestId, CyclicalID.UShortID peersStreamId, int dataRateBps, long length)
        {
            StreamingAgent agent = this.GetAgent<StreamingAgent>(this.CurrentEnvelope.Source, requestId);

            if (agent != null)
            {
                agent.BeginTransfer(peersStreamId, dataRateBps, length);
            }
            else
            {
                Logger.TraceInformation(LogTag.Streaming, "Accept transfer for unknown peer {0} and stream Id {1}", this.CurrentEnvelope.Source, requestId);
            }
        }

        [ConnectionfulProtocolMethod(ConnectionfulMethod.RejectTransfer)]
        private void RejectTransfer(CyclicalID.UShortID requestId)
        {
            StreamingAgent agent = this.GetAgent<StreamingAgent>(this.CurrentEnvelope.Source, requestId);
            if (agent != null)
            {
                agent.ForceClose(TerminationCode.UserCanceled);
                this.RemoveAgent(agent.PeersAddress, agent.LocalStreamId);
                Logger.TraceInformation(LogTag.Streaming, "Transfer request rejected");
            }
            else
            {
                Logger.TraceInformation(LogTag.Streaming, "Reject transfer for unknown peer {0} and stream Id {1}", this.CurrentEnvelope.Source, requestId);
            }
        }

        [ConnectionfulProtocolMethod(ConnectionfulMethod.RecieveSegment)]
        private void RecieveSegment(CyclicalID.UShortID requestId, Segment segment)
        {
            StreamingAgent agent = this.GetAgent<StreamingAgent>(this.CurrentEnvelope.Source, requestId);

            if (agent != null)
            {
                Logger.TraceInformation(LogTag.Streaming, "Queueing segment {0} from {1}, stream {2}", segment.Num, this.CurrentEnvelope.Source, requestId);
                segment.EstimatedRoundTripTime = this.CurrentEnvelope.EstimatedRoundTripTime;
                agent.Put(segment);
            }
            else
            {
                Logger.TraceInformation(LogTag.Streaming, "Received segment for unknown peer {0} and stream Id {1}", this.CurrentEnvelope.Source, requestId);
            }
        }

        [ConnectionfulProtocolMethod(ConnectionfulMethod.ReceiveAcknowledgements)]
        private void ReceiveAcknowledgements(CyclicalID.UShortID requestId, AcknowledgementSet acknowledgements, int windowSize)
        {
            Source source = this.GetAgent<Source>(this.CurrentEnvelope.Source, requestId);

            if (source != null)
            {
                source.ProcessAcknowledgements(acknowledgements, windowSize);
            }
            else
            {
                Logger.TraceInformation(LogTag.Streaming, "Received acknowledgement for unknown peer {0} and stream Id {1}", this.CurrentEnvelope.Source, requestId);
            }
        }

        [ConnectionfulProtocolMethod(ConnectionfulMethod.TerminateStream)]
        private void TerminateStream(CyclicalID.UShortID requestId, byte code)
        {
            StreamingAgent agent = this.GetAgent<StreamingAgent>(this.CurrentEnvelope.Source, requestId);

            if (agent != null)
            {
                Logger.TraceInformation(LogTag.Streaming, "Transfer {0} terminated unexpectadely : termination code = {1}", requestId, code);
                agent.ForceClose((TerminationCode)code);
                this.RemoveAgent(agent.PeersAddress, agent.LocalStreamId);
            }
            else
            {
                Logger.TraceInformation(LogTag.Streaming, "Received terminate stream message for unknown peer {0} and stream Id {1}", this.CurrentEnvelope.Source, requestId);
            }
        }

        [ConnectionfulProtocolMethod(ConnectionfulMethod.EndOfStream)]
        private void EndOfStream(CyclicalID.UShortID requestId, CyclicalID.UShortID lastSegmentNumber)
        {
            Sink sink = this.GetAgent<Sink>(this.CurrentEnvelope.Source, requestId);

            if (sink != null)
            {
                sink.Finish(lastSegmentNumber);
            }
            else
            {
                Logger.TraceInformation(LogTag.Streaming, "Received end of stream message for unknown peer {0} and stream Id {1}", this.CurrentEnvelope.Source, requestId);
            }
        }

        #endregion
    }


}
