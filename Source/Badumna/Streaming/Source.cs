﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Threading;
using System.Diagnostics;

using Badumna.Core;
using Badumna.Utilities;
using Badumna.Transport;

namespace Badumna.Streaming
{
    abstract class Source : StreamingAgent
    {
        internal ISource<TransportEnvelope> EnvelopeSource { get; private set; }

        private Thread mReadThread;
        private bool mIsReading = false;
        private AutoResetEvent mReadAvailable;
        private object mAcknowledgementLock = new object();
        private volatile bool mIsReadThreadWaiting;

        public const ushort SyncSequenceNumberValue = 0;
        public const ushort FirstSegmentSeqNumberValue = 1;

        public Source(
            CyclicalID.UShortID localStreamId,
            Stream stream,
            PeerAddress destination,
            ChannelAdapter<Segment, TransportEnvelope>.Converter segmentConverter,
            AsyncCallback completionCallback,
            object callbackState,
            NetworkEventQueue eventQueue,
            ITime timeKeeper,
            INetworkConnectivityReporter connectivityReporter)
            : base(localStreamId, stream, destination, completionCallback, callbackState, eventQueue, timeKeeper, connectivityReporter)
        {
            this.EnvelopeSource = new ChannelAdapter<Segment, TransportEnvelope>(this, segmentConverter, timeKeeper);
        }

        public override void BeginTransfer(CyclicalID.UShortID streamId, int dataRateBps, long length)
        {
            base.BeginTransfer(streamId, dataRateBps, length);
            this.SetEmptiedCallback(this.ChannelEmpty);

            this.mReadAvailable = new AutoResetEvent(true);
            this.mReadThread = new Thread(this.ReadLoop);
            //this.mReadThread.Priority = ThreadPriority.BelowNormal;  DGC: Commented this out because the main app at Normal priority could prevent this thread from running at all

            this.mIsReadThreadWaiting = false;
            this.mReadThread.IsBackground = true;
            this.mReadThread.Name = "Streaming source read thread";
            this.mReadThread.Start();

            this.WaitForReadLoopToFillUpWhenSimulating();
        }

        protected virtual bool HasFinished()
        {
            return true;
        }

        protected abstract bool ContinueReading(Segment lastSegmentRead);

        protected abstract bool Acknowledge(AcknowledgementSet acknowledgements, int windowSize);
        
        [Conditional("DEBUG")]
        private void WaitForReadLoopToFillUpWhenSimulating()
        {
            // If simulating - wait for the readloop to read as many segments as possible before returning.
            // this ensures the simulated time the segments are inserted is accurate.
            bool isSimulated = this.eventQueue is Simulator;
            if (isSimulated)
            {
                while (!this.mIsReadThreadWaiting)
                {
                    System.Threading.Thread.Sleep(20);
                }
            }
        }

        private void ReadLoop()
        {
            this.mIsReading = true;
            CyclicalID.UShortID nextSegmentNumber = new CyclicalID.UShortID(FirstSegmentSeqNumberValue);

            do
            {
                this.mReadAvailable.WaitOne();
                this.mIsReadThreadWaiting = false;

                Segment segment = null;

                do
                {
                    segment = new Segment(nextSegmentNumber, this);
                    nextSegmentNumber.Increment();

                    try
                    {
                        segment.Read(this.Stream);
                    }
                    catch (IOException e)
                    {
                        Logger.TraceException(LogLevel.Warning, e, "IOException in file transfer");
                        this.Shutdown(TerminationCode.ReadFailure);
                        return;
                    }
                }
                while (this.QueueSegment(segment));

                this.mIsReadThreadWaiting = true;
            }
            while (this.mIsReading);
        }

        private bool QueueSegment(Segment segment)
        {
            if (segment.Length > 0)
            {
                this.mTransferStatus.AddTransferedAmount(segment.Length);

                this.eventQueue.Push(this.Put, segment);

                lock (this.mAcknowledgementLock)
                {
                    return this.ContinueReading(segment);
                }
            }

            if (segment.Length == 0)
            {
                this.mIsReading = false;
                this.eventQueue.Push(this.TriggerEndOfStreamEvent, segment.Num);
            }

            return false;
        }

        private void CheckIfComplete(bool channelIsEmpty)
        {
            lock (this.mAcknowledgementLock)
            {
                if (channelIsEmpty && !this.mIsReading && !this.IsCompleted && this.HasFinished())
                {
                    Logger.TraceInformation(LogTag.Streaming, "Read complete and all segments acknowledged for stream {0}.", this.LocalStreamId);
                    this.TriggerTerminatedEvent(TerminationCode.EndOfStream);
                }
            }
        }

        private void ChannelEmpty(Channel<Segment> agent)
        {
            this.CheckIfComplete(true);
        }

        public void ProcessAcknowledgements(AcknowledgementSet acknowledgements, int windowSize)
        {
            lock (this.mAcknowledgementLock)
            {
                if (!this.Acknowledge(acknowledgements, windowSize))
                {
                    this.CheckIfComplete(this.Peek() == null);
                    return;
                }
            }

            if (this.mIsReading)
            {
                this.mReadAvailable.Set();
                this.WaitForReadLoopToFillUpWhenSimulating();
            }
            else
            {
                this.CheckIfComplete(this.Peek() == null);
            }
        }

        // Must be thread safe
        private void Shutdown(TerminationCode code)
        {
            try
            {
                this.Stream.Close();
            }
            catch { }

            if (!this.IsCompleted)
            {
                this.TriggerTerminatedEvent(code);
            }
        }
    }


}
