﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

using Badumna.Core;
using Badumna.Utilities;
using Badumna.Transport;

namespace Badumna.Streaming
{
    enum TerminationCode
    {
        EndOfStream,
        ReadFailure,
        WriteFailure,
        DoesNotExist,
        UserCanceled,
        ConnectionLost
    }

    class StreamTerminatedEventArgs : EventArgs
    {
        public TerminationCode Code;

        public StreamTerminatedEventArgs(TerminationCode code)
        {
            this.Code = code;
        }
    }

    class EndOfStreamEventArgs : EventArgs
    {
        public CyclicalID.UShortID LastSegmentNumber;

        public EndOfStreamEventArgs(CyclicalID.UShortID lastSegmentNumber)
        {
            this.LastSegmentNumber = lastSegmentNumber;
        }
    }

    class AcknowledgeEventArgs : EventArgs
    {
        public AcknowledgementSet Acknowledgements;
        public int WindowSize;

        public AcknowledgeEventArgs(AcknowledgementSet acknowledgements, int windowSize)
        {
            this.Acknowledgements = acknowledgements;
            this.WindowSize = windowSize;
        }
    }

    abstract class StreamingAgent : Channel<Segment>, IStreamController
    {
        private SortedList<CyclicalID.UShortID, Segment> mSegmentWindow;

        protected int mDataRate = 1;
        public const int SegmentSize = Parameters.MaximumPayloadSize - 10; // TODO : Should adjust if being relayed.
        public const int InitialWindowSize = 5; // Must be < ushort.MaxValue / 2 

        private CyclicalID.UShortID mPeersStreamId;
        public CyclicalID.UShortID PeersStreamId { get { return this.mPeersStreamId; } }

        private CyclicalID.UShortID mLocalStreamId;
        public CyclicalID.UShortID LocalStreamId { get { return this.mLocalStreamId; } }

        private Stream mStream;
        public Stream Stream { get { return this.mStream; } }

        private PeerAddress mPeersAddress;
        public PeerAddress PeersAddress { get { return this.mPeersAddress; } }

        private EventHandler<StreamTerminatedEventArgs> tranfserTerminatedDelegate;
        public event EventHandler<StreamTerminatedEventArgs> TranfserTerminated
        {
            add { this.tranfserTerminatedDelegate += value; }
            remove { this.tranfserTerminatedDelegate -= value; }
        }

        private EventHandler<EndOfStreamEventArgs> endOfStreamDelegate;
        public event EventHandler<EndOfStreamEventArgs> EndOfStream
        {
            add { this.endOfStreamDelegate += value; }
            remove { this.endOfStreamDelegate -= value; }
        }

        private AsyncCallback mCompletionCallback;

        private EventHandler informationChangedDelegate;
        public event EventHandler InformationChanged
        {
            add { this.informationChangedDelegate += value; }
            remove { this.informationChangedDelegate -= value; }
        }

        protected TransferStatus mTransferStatus;

        private bool mCompletedSuccessfully = false;
        public bool WasSuccessful { get { return this.mIsCompleted && this.mCompletedSuccessfully; } }

        protected bool mIsReliable;
        public bool IsReliable
        {
            get { return this.mIsReliable; }
        }

        private RegularTask mHeatBeatRegularTask;
        protected TimeSpan mHeartbeat;

        /// <summary>
        /// The queue to use for scheduling events.
        /// </summary>
        protected NetworkEventQueue eventQueue;
        
        /// <summary>
        /// Provides access to the current time.
        /// </summary>
        protected ITime timeKeeper;

        public StreamingAgent(CyclicalID.UShortID localStreamId,
            Stream stream,
            PeerAddress destination, 
            AsyncCallback completionCallback,
            object callbackState,
            NetworkEventQueue eventQueue,
            ITime timeKeeper,
            INetworkConnectivityReporter connectivityReporter)
            : base(timeKeeper)
        {
            this.eventQueue = eventQueue;
            this.timeKeeper = timeKeeper;

            this.mLocalStreamId = localStreamId;
            this.mStream = stream;
            this.mPeersAddress = destination;
            this.mCompletionCallback = completionCallback;
            this.mState = callbackState;
            this.mCurrentState = StreamState.Waiting;
            this.mSegmentWindow = new SortedList<CyclicalID.UShortID,Segment>();
            this.mTransferStatus = new TransferStatus(this.timeKeeper);
            this.mTransferStatus.InformationChanged += delegate { this.TriggerInformationChanged(); };
            this.mHeartbeat = this.timeKeeper.Now;
            this.mHeatBeatRegularTask = new RegularTask("streaming_heart_beat", TimeSpan.FromSeconds(3), eventQueue, connectivityReporter, this.RegularHeartBeatCheck);
            this.mHeatBeatRegularTask.Start();
        }

        private void TriggerInformationChanged()
        {
            EventHandler handler = this.informationChangedDelegate;
            if (handler != null)
            {
                handler(this, null);
            }
        }

        public virtual void ForceClose(TerminationCode code)
        {
            this.TranslateTerminationCode(code);
            this.Stream.Close();
            this.CompleteAsyncOperation();
            this.mHeatBeatRegularTask.Stop();
        }

        private void TranslateTerminationCode(TerminationCode code)
        {
            if (code == TerminationCode.EndOfStream)
            {
                this.mCurrentState = StreamState.Complete;
            }
            else if (code == TerminationCode.UserCanceled)
            {
                this.mCurrentState = StreamState.Canceled;
            }
            else if (code == TerminationCode.ConnectionLost)
            {
                this.mCurrentState = StreamState.Disconnected;
            }
            else
            {
                this.mCurrentState = StreamState.Error;
            }
        }

        protected void TriggerEndOfStreamEvent(CyclicalID.UShortID lastSegmentNumber)
        {
            EventHandler<EndOfStreamEventArgs> handler = this.endOfStreamDelegate;
            if (handler != null)
            {
                handler.Invoke(this, new EndOfStreamEventArgs(lastSegmentNumber));
            }
        }

        protected void TriggerTerminatedEvent(TerminationCode code)
        {
            this.TranslateTerminationCode(code);
            Logger.TraceInformation(LogTag.Streaming, "Stream {0} has finished. State = {1}", this.mLocalStreamId, (int)code);

            EventHandler<StreamTerminatedEventArgs> handler = this.tranfserTerminatedDelegate;
            if (handler != null)
            {
                handler.Invoke(this, new StreamTerminatedEventArgs(code));
            }

            try
            {
                this.Stream.Close();
            }
            catch { }

            if (code == TerminationCode.EndOfStream)
            {
                this.mCompletedSuccessfully = true;
            }

            this.CompleteAsyncOperation();
            this.mHeatBeatRegularTask.Stop();
        }

        protected void CompleteAsyncOperation()
        {
            this.mIsCompleted = true;
            this.mWaitHandle.Set();
            if (this.mCompletionCallback != null)
            {
                this.mCompletionCallback.Invoke(this);
            }
            this.mWaitHandle.Close();
        }

        public void AdjustRate(int dataRateBps)
        {
            this.mDataRate = dataRateBps;
        }

        public override bool IsEmpty { get { return this.mSegmentWindow.Count == 0; } }

        protected override void DoPut(Segment item)
        {
            lock (this.mSegmentWindow)
            {
                this.mSegmentWindow[item.Num] = item;
            }
        }

        protected override Segment DoGet()
        {
            lock (this.mSegmentWindow)
            {
                if (this.mSegmentWindow.Count == 0)
                {
                    return null;
                }

                Segment segment = this.mSegmentWindow.Values[0];
                this.mSegmentWindow.RemoveAt(0);
                return segment;
            }
        }

        public override Segment Peek()
        {
            lock (this.mSegmentWindow)
            {
                if (this.mSegmentWindow.Count == 0)
                {
                    return null;
                }

                return this.mSegmentWindow.Values[0];
            }
        }

        public override void Clear()
        {
            lock (this.mSegmentWindow)
            {
                this.mSegmentWindow.Clear();
            }
        }

        public virtual void BeginTransfer(CyclicalID.UShortID streamId, int dataRateBps, long length)
        {
            this.mCurrentState = StreamState.InProgress;
            this.mPeersStreamId = streamId;
            this.mDataRate = dataRateBps;
            this.mTransferStatus.BytesTotal = length;

            this.TriggerInformationChanged();
        }

        #region IAsyncResult Members

        private object mState;
        public object AsyncState
        {
            get { return this.mState; }
            set { this.mState = value; }
        }

        private System.Threading.ManualResetEvent mWaitHandle = new System.Threading.ManualResetEvent(false);
        public System.Threading.WaitHandle AsyncWaitHandle
        {
            get { return this.mWaitHandle; }
        }

        public bool CompletedSynchronously { get { return false; } }

        protected bool mIsCompleted;
        public bool IsCompleted { get { return this.mIsCompleted; } }

        #endregion


        private StreamState mCurrentState;
        public StreamState CurrentState { get { return this.mCurrentState; } }

        public long BytesTotal
        {
            get { return this.mTransferStatus.BytesTotal; }
        }

        public long BytesTransfered
        {
            get { return this.mTransferStatus.BytesTransfered; }
        }

        public double TransferRateKBps
        {
            get { return this.mTransferStatus.TransferRateKBps; }
        }        

        public TimeSpan EstimatedTimeRemaining
        {
            get { return this.mTransferStatus.EstimatedTimeRemaining; }
        }


        public void Cancel()
        {
            this.ForceClose(TerminationCode.UserCanceled);
            this.mCurrentState = StreamState.Canceled;
            this.TriggerTerminatedEvent(TerminationCode.UserCanceled);
        }

        public void ConnectionLost()
        {
            this.ForceClose(TerminationCode.ConnectionLost);
            this.mCurrentState = StreamState.Canceled;
            this.TriggerTerminatedEvent(TerminationCode.ConnectionLost);
        }

        private void RegularHeartBeatCheck()
        {
            if (this.ShouldForceStop())
            {
                // no incoming activity, assume the connection is lost.
                this.ForceClose(TerminationCode.ConnectionLost);
                this.mCurrentState = StreamState.Canceled;
                this.TriggerTerminatedEvent(TerminationCode.ConnectionLost);
            }
        }

        protected void RecordHeartBeat()
        {
            this.mHeartbeat = this.timeKeeper.Now;
        }

        protected bool ShouldForceStop(int seconds)
        {
            if (this.timeKeeper.Now - this.mHeartbeat >= TimeSpan.FromSeconds(seconds))
            {
                return true;
            }

            return false;
        }

        protected abstract bool ShouldForceStop();
    }
}
