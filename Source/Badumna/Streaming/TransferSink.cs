﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Threading;

using Badumna.Core;
using Badumna.Utilities;
using Badumna.Transport;

#if DEBUG
using NUnit.Framework;
#endif

namespace Badumna.Streaming
{
    
    class TransferSink : StreamingAgent, IAsyncResult
    {
        private CyclicalID.UShortID mLastSegmentNumber;
        private AcknowledgementSet mAcknowledgementSet = new AcknowledgementSet();
        private BinaryHeap<Segment> mWriteQueue = new BinaryHeap<Segment>();
        private bool mLastSegmentNumberIsKnown = false;

        public event EventHandler<AcknowledgeEventArgs> Acknowledge;

        private Thread mWriterThread;
        private AutoResetEvent mWriteAvailable;
        private bool mIsWriting;

        private object mLastSegmentLock = new object();
        private RegularTask mSendAcknowledgementsTask;
        private DateTime mLastAcknowledgementTime = DateTime.Now;

        public TransferSink(CyclicalID.UShortID localStreamId, Stream stream, PeerAddress destination, AsyncCallback completionCallback)
            : base(localStreamId, stream, destination, completionCallback)
        {
            this.mWriteAvailable = new AutoResetEvent(false);

            this.mSendAcknowledgementsTask = new RegularTask("Send transfer acks",
                TimeSpan.FromSeconds(3), this.CheckAcknowledgements);               // TODO : Set an appropriate dynamic time 

            this.mSendAcknowledgementsTask.Start();
            this.mWriterThread = new Thread(this.WriteLoop);
            this.mWriterThread.Priority = ThreadPriority.BelowNormal;
            this.mWriterThread.IsBackground = true;
            this.mWriterThread.Start();
        }

        private void CheckAcknowledgements()
        {
            if ((DateTime.Now - this.mLastAcknowledgementTime).TotalSeconds > 3)
            {
                this.SendAcknowledgements();
            }
        }

        private void AcknowledgeSegment(CyclicalID.UShortID segmentNumber)
        {
            if (this.mAcknowledgementSet.Minimum > segmentNumber)
            {
                return;
            }

            this.mAcknowledgementSet.Add(segmentNumber);

            // TODO : Should also send periodically so that if no segments arrive the stream can continue.
            if (this.mAcknowledgementSet.Count >= (StreamingAgent.WindowSize / 2)) // Send when half the window has arrived
            {
                this.SendAcknowledgements();
            }
        }

        private void SendAcknowledgements()
        {
            if (this.Acknowledge != null)
            {
                this.Acknowledge(this, new AcknowledgeEventArgs(this.mAcknowledgementSet));
            }

            this.mLastAcknowledgementTime = DateTime.Now;
            this.mAcknowledgementSet.ClipToFirstMissing(); // Must be called after the Acknowledge is invoked. 
        }

        public override void ForceClose(TerminationCode code)
        {
            this.mIsWriting = false;
            base.ForceClose(code);
            this.mWriteAvailable.Set();
        }

        public void Finish(CyclicalID.UShortID lastSegmentNumber)
        {
            lock (this.mLastSegmentLock)
            {
                this.mLastSegmentNumberIsKnown = true;
                this.mLastSegmentNumber = lastSegmentNumber;
            }
        }

        private bool IsLastSegment(CyclicalID.UShortID segmentNumber)
        {
            bool isLast = false;
            lock (this.mLastSegmentLock)
            {
                isLast = this.mLastSegmentNumberIsKnown && this.mLastSegmentNumber == segmentNumber;
            }

            return isLast;
        }

        protected override void DoPut(Segment item)
        {
            this.AcknowledgeSegment(item.Number);
            this.AddTransferedAmount(item.Length);

            lock (this.mWriteQueue)
            {
                this.mWriteQueue.Push(item);
                this.mWriteAvailable.Set();
            }
        }

        private void WriteLoop()
        {
            this.mIsWriting = true;
            CyclicalID.UShortID nextSegmentNumber = new CyclicalID.UShortID(0);

            do
            {
                this.mWriteAvailable.WaitOne();
                Segment segment = null;
                do
                {
                    lock (this.mWriteQueue)
                    {
                        if (this.mWriteQueue.Count > 0 && this.mWriteQueue.Peek().Number == nextSegmentNumber)
                        {
                            segment = this.mWriteQueue.Pop();
                            nextSegmentNumber.Increment();
                        }
                        else
                        {
                            segment = null; // In case we read in this loop already
                        }
                    }                    
                }
                while (this.WriteSegment(segment));
            }
            while (this.mIsWriting);

            this.mSendAcknowledgementsTask.Stop();
        }

        // Called from the write thread
        private bool WriteSegment(Segment segment)
        {
            if (segment != null)
            {
                CyclicalID.UShortID nextSegmentNumber = segment.Number;
                nextSegmentNumber.Increment();

                try
                {
                    if (this.Stream.CanWrite)
                    {
                        this.Stream.Write(segment.Buffer, 0, segment.Length);

                        if (this.IsLastSegment(nextSegmentNumber))
                        {
                            this.Stream.Flush();
                            this.Stream.Close();

                            NetworkEventQueue.Instance.Push(this.SendAcknowledgements);
                            NetworkEventQueue.Instance.Push(this.TriggerTerminatedEvent, TerminationCode.EndOfStream);
                            return false;
                        }

                        return true;
                    }
                    else
                    {
                        this.mIsWriting = false;
                    }
                }
                catch
                {
                    this.mIsWriting = false;
                }
            }

            return false;
        }

    }
    /*
    #region Unit tests
#if DEBUG


    [TestFixture]
    [DontPerformCoverage]
    public class TransferSinkTester
    {

        private Stream GenerateStream(int length, out ushort checksum)
        {
            Random generator = new Random((int)DateTime.Now.Ticks);
            byte[] buffer = new byte[length];

            generator.NextBytes(buffer);
            checksum = CRC.CRC16(buffer);
            MemoryStream stream = new MemoryStream(buffer);

            stream.Position = 0;
            return stream;
        }

        public void TransferSegmentTest(int length)
        {
            ushort originalChecksum;
            Stream originalStream = this.GenerateStream(length, out originalChecksum);
            MemoryStream outputStream = new MemoryStream();
            TransferSink sink = new TransferSink(new CyclicalID.UShortID(0), outputStream, PeerAddress.Nowhere, (double)length / 1000.0, null);
            CyclicalID.UShortID segmentNumber = new CyclicalID.UShortID();
            bool isComplete = false;

            sink.EndOfStream += delegate(object sender, EndOfStreamEventArgs args)
            {
                 isComplete = true;
            };

            sink.Acknowledge += delegate(object sender, AcknowledgeEventArgs args)
            {
                Assert.IsTrue(args.Acknowledgements.Count == StreamingAgent.WindowSize / 2);
                Assert.AreEqual(args.Acknowledgements.Maximum, segmentNumber);
            };

            Segment segment;

            do
            {
                segment = new Segment(segmentNumber);
                segment.Read(originalStream);
                sink.Put(segment);
                segmentNumber.Increment();
            }
            while (segment.Length >= StreamingAgent.SegmentSize);
            originalStream.Close();
            sink.Finish(segmentNumber);

            while (!isComplete)
            {
                System.Threading.Thread.Sleep(50);
            }

            Assert.IsTrue(sink.IsCompleted, "Sink should be completed");
            Assert.AreEqual(originalChecksum, CRC.CRC16(outputStream.ToArray()), "Output stream is incorrect");

            outputStream.Close();
        }

        [Test]
        public void ZeroTransferTest()
        {
            this.TransferSegmentTest(0);
        }

        [Test]
        public void SingleTransferTest()
        {
            this.TransferSegmentTest(StreamingAgent.SegmentSize - 1);
        }

        [Test]
        public void ExactSingleTransferTest()
        {
            this.TransferSegmentTest(StreamingAgent.SegmentSize);
        }

        [Test]
        public void MultipleTransferTest()
        {
            this.TransferSegmentTest(StreamingAgent.SegmentSize + 1);
        }

        [Test]
        public void ManyTransferTest()
        {
            this.TransferSegmentTest(StreamingAgent.SegmentSize * StreamingAgent.WindowSize);
        }

        [Test]
        public void FinishOutOfOrderTest()
        {
            ushort originalChecksum;
            Stream originalStream = this.GenerateStream(StreamingAgent.SegmentSize + 1, out originalChecksum);
            MemoryStream outputStream = new MemoryStream();
            TransferSink sink = new TransferSink(new CyclicalID.UShortID(0), outputStream, PeerAddress.Nowhere, (double)StreamingAgent.SegmentSize + 1.0 / 1000.0, null);
            CyclicalID.UShortID segmentNumber = new CyclicalID.UShortID();
            bool isComplete = false;

            sink.EndOfStream += delegate(object sender, EndOfStreamEventArgs args) { isComplete = true; };

            Segment segment;

            sink.Finish(new CyclicalID.UShortID(2));

            do
            {
                segment = new Segment(segmentNumber);
                segment.Read(originalStream);
                sink.Put(segment);
                segmentNumber.Increment();
            }
            while (segment.Length >= StreamingAgent.SegmentSize);
            originalStream.Close();

            while (!isComplete)
            {
                System.Threading.Thread.Sleep(50);
            }

            Assert.IsTrue(sink.IsCompleted, "Sink should be completed");
            Assert.AreEqual(originalChecksum, CRC.CRC16(outputStream.ToArray()), "Output stream is incorrect");

            outputStream.Close();
        }


        public void ArriveOutOfOrderTest(int length)
        {
            ushort originalChecksum;
            Random generator = new Random((int)DateTime.Now.Ticks);
            Stream originalStream = this.GenerateStream(length, out originalChecksum);
            MemoryStream outputStream = new MemoryStream();
            TransferSink sink = new TransferSink(new CyclicalID.UShortID(0), outputStream, PeerAddress.Nowhere, (double)length / 1000.0, null);
            CyclicalID.UShortID segmentNumber = new CyclicalID.UShortID();
            bool isComplete = false;

            sink.EndOfStream += delegate(object sender, EndOfStreamEventArgs args) { isComplete = true; };

            Segment segment;
            List<Segment> segmentList = new List<Segment>();

            do
            {
                segment = new Segment(segmentNumber);
                segment.Read(originalStream);
                segmentList.Add(segment);
                segmentNumber.Increment();
            }
            while (segment.Length >= StreamingAgent.SegmentSize);
            originalStream.Close();

            sink.Finish(segmentNumber);

            // Randomly put the segments
            while (segmentList.Count > 0)
            {
                int index = generator.Next(segmentList.Count);

                sink.Put(segmentList[index]);
                segmentList.RemoveAt(index);
            }

            while (!isComplete)
            {
                System.Threading.Thread.Sleep(50);
            }

            Assert.IsTrue(sink.IsCompleted, "Sink should be completed");
            Assert.AreEqual(originalChecksum, CRC.CRC16(outputStream.ToArray()), "Output stream is incorrect");

            outputStream.Close();
        }


        [Test]
        public void MultipleArriveOutOfOrderTest()
        {
            this.ArriveOutOfOrderTest(StreamingAgent.SegmentSize + 1);
        }

        [Test]
        public void ManyArriveOutOfOrderTest()
        {
            this.ArriveOutOfOrderTest(StreamingAgent.SegmentSize * 10);
        }

    }


#endif
    #endregion
     */
}
