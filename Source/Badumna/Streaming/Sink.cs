﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Threading;

using Badumna.Core;
using Badumna.Utilities;
using Badumna.Transport;

namespace Badumna.Streaming
{
    abstract class Sink : StreamingAgent, IAsyncResult
    {
        private EventHandler<AcknowledgeEventArgs> acknowledgeDelegate;
        public event EventHandler<AcknowledgeEventArgs> Acknowledge
        {
            add { this.acknowledgeDelegate += value; }
            remove { this.acknowledgeDelegate -= value; }
        }

        private Thread mWriterThread;
        private AutoResetEvent mWriteAvailable;
        private bool mIsWriting;
        private object mWriteQueueLock = new object();
        private object mSequenceLock = new object();
        private CyclicalID.UShortID mLastSegmentNumber; // The number of the last segment in the stream
        private CyclicalID.UShortID mNextSegmentNumber; // The number of the next segment to be written.
        private bool mLastSegmentNumberIsKnown = false;

        private AcknowledgementSet mAcknowledgementSet = new AcknowledgementSet();
        private RegularTask mSendAcknowledgementsTask;

        private TimeSpan mLastAcknowledgementTime;
        private TimeSpan mLastNaturalAcknowledgementTime;
        private int mWindowSize = StreamingAgent.InitialWindowSize;

        private Smoother mAcknowledgementRateSmoother;
        private TimeSpan mMinumumAckDelay = Parameters.InitialResendAckTimeout;

        public Sink(CyclicalID.UShortID localStreamId,
            Stream stream,
            PeerAddress destination,
            AsyncCallback completionCallback,
            object callbackState,
            NetworkEventQueue eventQueue,
            ITime timeKeeper,
            INetworkConnectivityReporter connectivityReporter)
            : base(localStreamId, stream, destination, completionCallback, callbackState, eventQueue, timeKeeper, connectivityReporter)
        {
            this.mLastAcknowledgementTime = this.timeKeeper.Now;
            this.mLastNaturalAcknowledgementTime = this.timeKeeper.Now;

            this.mSendAcknowledgementsTask = new RegularTask(
                "Send transfer acks",
                Parameters.InitialResendAckTimeout,
                eventQueue,
                connectivityReporter,
                this.CheckAcknowledgements);

            // synchronize the acknowledge set. 
            // we need to tell the acknowledge set the sequence number carried by the first segment. 
            // consider the situation in which the first few segements are lost during transmittion, the
            // acknowledgement set would incorrectly set its min member to be the first segment received
            // by the sink. this would block all previous segments resent by the source. 
            this.mNextSegmentNumber = new CyclicalID.UShortID(Source.FirstSegmentSeqNumberValue);
            this.mAcknowledgementSet.Add(new CyclicalID.UShortID(Source.SyncSequenceNumberValue));

            this.mSendAcknowledgementsTask.Start();

            this.mWriteAvailable = new AutoResetEvent(false);
            this.mWriterThread = new Thread(this.WriteLoop);
            this.mWriterThread.IsBackground = true;
            this.mWriterThread.Start();
        }

        public override void ForceClose(TerminationCode code)
        {
            this.mIsWriting = false;
            base.ForceClose(code);
            this.mWriteAvailable.Set();
            this.mSendAcknowledgementsTask.Stop();
        }

        public void Finish(CyclicalID.UShortID lastSegmentNumber)
        {
            lock (this.mSequenceLock)
            {
                this.mLastSegmentNumberIsKnown = true;
                this.mLastSegmentNumber = lastSegmentNumber;
            }

            if (this.IsLastSegment())
            {
                this.mWriteAvailable.Set(); // So the write loop can finish if all segments have arrived already
            }
        }

        protected abstract void PushSegment(Segment segment);

        protected abstract Segment PopSegment();

        protected override sealed void DoPut(Segment item)
        {
            if (this.AcceptSegment(item.Num, item.EstimatedRoundTripTime))
            {
                this.mTransferStatus.AddTransferedAmount(item.Length);

                lock (this.mWriteQueueLock)
                {
                    this.PushSegment(item);
                    this.mWriteAvailable.Set();
                }
            }
        }

        private void WriteLoop()
        {
            this.mIsWriting = true;

            do
            {
                this.mWriteAvailable.WaitOne();
                Segment segment = null;
                do
                {
                    lock (this.mWriteQueueLock)
                    {
                        segment = this.PopSegment();
                    }
                }
                while (this.WriteSegment(segment));
            }
            while (this.mIsWriting);

            this.mSendAcknowledgementsTask.Stop();
        }

        // Called from the write thread
        private bool WriteSegment(Segment segment)
        {
            if (segment != null)
            {
                try
                {
                    if (this.Stream.CanWrite)
                    {
                        this.Stream.Write(segment.Buffer, 0, segment.Length);

                        lock (this.mSequenceLock)
                        {
                            this.mNextSegmentNumber.Increment();
                        }

                        return !this.IsLastSegment();
                    }
                    else
                    {
                        this.mIsWriting = false;
                    }
                }
                catch
                {
                    this.mIsWriting = false;
                }

                this.eventQueue.Push(this.TriggerTerminatedEvent, TerminationCode.WriteFailure);
            }

            return false;
        }

        private bool IsLastSegment()
        {
            bool isLast = false;
            lock (this.mSequenceLock)
            {
                isLast = this.mLastSegmentNumberIsKnown && this.mLastSegmentNumber == this.mNextSegmentNumber;
            }

            if (isLast)
            {
                this.Stream.Flush();

                this.eventQueue.Push(this.SendAcknowledgements);
                this.eventQueue.Push(this.TriggerTerminatedEvent, TerminationCode.EndOfStream);

                this.mIsWriting = false;
                return true;
            }

            return false;
        }

        private void CheckAcknowledgements()
        {
            if (this.mLastAcknowledgementTime + this.mMinumumAckDelay < this.timeKeeper.Now)
            {
                this.SendAcknowledgements();
            }
        }

        private bool AcceptSegment(CyclicalID.UShortID segmentNumber, TimeSpan estimatedRoundTripTime)
        {
            if (segmentNumber < this.mAcknowledgementSet.Minimum || this.mAcknowledgementSet.Contains(segmentNumber))
            {
                return false;
            }

            this.mAcknowledgementSet.Add(segmentNumber);

            // Send when the latest segment marks half the window
            if ((this.mAcknowledgementSet.Count + this.mAcknowledgementSet.NumberMissing) % (this.mWindowSize / 2) == 0)
            {
                this.SendNaturalAcknowledgement(estimatedRoundTripTime);
            }

            return true;
        }

        // Acknowledgements that result from a segment arriving as opposed to timeout.
        private void SendNaturalAcknowledgement(TimeSpan estimatedRoundTripTime)
        {
            TimeSpan timeSinceLastNaturalAcknowledgment = this.timeKeeper.Now - this.mLastNaturalAcknowledgementTime;

            if (this.mAcknowledgementRateSmoother != null)
            {
                this.mAcknowledgementRateSmoother.ValueEvent(timeSinceLastNaturalAcknowledgment.TotalMilliseconds);
            }
            else
            {
                this.mAcknowledgementRateSmoother = new Smoother(100, estimatedRoundTripTime.TotalMilliseconds);
            }

            // The resend time is set to twice the estimated round trip time. This could be smaller, but generally
            // the estimate is not so accurate (especially when the link is busy) so we set it conservatively.
            this.mMinumumAckDelay = TimeSpan.FromMilliseconds(this.mAcknowledgementRateSmoother.EstimatedValue * 2);

            // We try to make the window size large enough so that acks are sent roughly in the same order
            // as round trip time. Generally it is better to over estimate this becuase the scheduler will 
            // restric flow if packets are lost and we don't want to artificially stem the flow.
            if (timeSinceLastNaturalAcknowledgment < estimatedRoundTripTime)
            {
                this.mWindowSize += 100;
            }

            this.mLastNaturalAcknowledgementTime = this.timeKeeper.Now;
            this.SendAcknowledgements();
        }

        private void SendAcknowledgements()
        {
            // Need to trigger send even if ack set is empty:  The initial set of
            // packets sent by the sender may all be dropped.  The sender won't
            // do anything more until it receives an ack.  Hence, the timeout
            // triggered ack must send an ack message even if we haven't received
            // anything.

            EventHandler<AcknowledgeEventArgs> handler = this.acknowledgeDelegate;
            if (handler != null)
            {
                handler.Invoke(this, new AcknowledgeEventArgs(this.mAcknowledgementSet, this.mWindowSize));
            }

            // Remove the acknowledged segments from the set ready for new arrivals.
            // The acks are sent reliably so we don't have to record them.
            this.mAcknowledgementSet.ClipToFirstMissing(); // Must be called after the Acknowledge is invoked. 

            this.mLastAcknowledgementTime = this.timeKeeper.Now;
            this.mSendAcknowledgementsTask.Delay(this.mMinumumAckDelay);
        }
    }
}

