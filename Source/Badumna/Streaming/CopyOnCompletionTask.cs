﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

using Badumna;
using Badumna.Core;
using Badumna.Utilities;

namespace Badumna.Streaming
{
    class CopyOnCompletionTask
    {
        private String mDestinationPath;
        private bool mOverwriteIfRequired;
        private AsyncCallback mCompletionCallback;
        private String mTemporaryPath;

        public CopyOnCompletionTask(string destinationPath, bool overwrite, AsyncCallback callback)
        {
            this.mDestinationPath = destinationPath;
            this.mOverwriteIfRequired = overwrite;
            this.mCompletionCallback = callback;
        }

        public Stream GetStream()
        {
            this.mTemporaryPath = Path.GetTempFileName();
            return new FileStream(this.mTemporaryPath, FileMode.Create, FileAccess.Write, FileShare.None);
        }

        public void RequestFileOperationComplete(IAsyncResult ar)
        {
            IStreamController controller = ar as IStreamController;

            if (controller.WasSuccessful)
            {
                if (File.Exists(this.mDestinationPath))
                {
                    if (this.mOverwriteIfRequired)
                    {
                        try
                        {
                            File.Delete(this.mDestinationPath);
                        }
                        catch (Exception e)
                        {
                            Logger.TraceException(LogLevel.Warning, e, "Cannot delete file to overwrite.");
                            this.FindNextAvailableName();
                        }
                    }
                    else
                    {
                        this.FindNextAvailableName();
                    }
                }

                try
                {
                    File.Move(this.mTemporaryPath, this.mDestinationPath);
                }
                catch (Exception e)
                {
                    Logger.TraceException(LogLevel.Error, e, "Cannot move transfered file to destination.");
                }
            }

            if (this.mCompletionCallback != null)
            {
                this.mCompletionCallback.Invoke(ar);
            }
        }

        private void FindNextAvailableName()
        {
            int i = 0;
            while (File.Exists(this.mDestinationPath))
            {
                this.mDestinationPath =
                    Path.Combine(Path.GetFullPath(this.mDestinationPath),
                    String.Format("{0}(copy {1}).{2}", Path.GetFileNameWithoutExtension(this.mDestinationPath), i++, Path.GetExtension(this.mDestinationPath)));
            }
        }
    }
}
