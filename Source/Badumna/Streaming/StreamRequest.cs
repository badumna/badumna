﻿using System;
using System.Collections.Generic;
using System.Text;

using Badumna.Core;
using Badumna.Utilities;

namespace Badumna.Streaming
{
    class StreamRequest : IParseable
    {
        public String FileName;
        public long FileSize;
        public String UserName;
        public CyclicalID.UShortID StreamId;
        public byte[] AdditionalData;

        public StreamRequest() { } // For IParseable

        public StreamRequest(String fileName, byte[] addditionalData, long fileSize, String userName, CyclicalID.UShortID streamId)
        {
            this.FileName = fileName;
            this.FileSize = fileSize;
            this.UserName = userName;
            this.StreamId = streamId;
            this.AdditionalData = addditionalData;
        }

        #region IParseable Members

        public void ToMessage(MessageBuffer message, Type parameterType)
        {
            if (message != null)
            {
                message.Write(this.FileName);
                message.Write(this.FileSize);
                message.Write(this.UserName);
                this.StreamId.ToMessage(message, typeof(CyclicalID.UShortID));
                if (this.AdditionalData != null)
                {
                    message.Write((ushort)this.AdditionalData.Length);
                    message.Write(this.AdditionalData, this.AdditionalData.Length);
                }
                else
                {
                    message.Write((ushort)0);
                }
            }
        }

        public void FromMessage(MessageBuffer message)
        {
            if (message != null)
            {
                this.FileName = message.ReadString();
                this.FileSize = message.ReadLong();
                this.UserName = message.ReadString();
                this.StreamId = message.Read<CyclicalID.UShortID>();
                int length = message.ReadUShort();
                if (length > 0)
                {
                    this.AdditionalData = message.ReadBytes(length);
                }
            }
        }

        #endregion
    }
}
