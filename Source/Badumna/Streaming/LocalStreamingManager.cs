﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;

using Badumna.Core;
using Badumna.Utilities;
using Badumna.Transport;
using Badumna.DataTypes;
using Badumna.SpatialEntities;


namespace Badumna.Streaming
{
    class LocalStreamingManager : StreamingManager
    {
        private StreamingProtocol mProtocol;
        
        /// <summary>
        /// The method to use to queue events that must run in the application's thread.
        /// </summary>
        private QueueInvokable queueApplicationEvent;

        public LocalStreamingManager(
            TransportProtocol parentProtocol,
            IChannelFunnel<TransportEnvelope> connectionTable,
            NetworkEventQueue eventQueue,
            QueueInvokable queueApplicationEvent,
            ITime timeKeeper,
            INetworkConnectivityReporter connectivityReporter,
            IPeerConnectionNotifier connectionNotifier,
            EntityIDProvider<IReplicableEntity> entityIDProvider)
            : base(entityIDProvider)
        {
            this.queueApplicationEvent = queueApplicationEvent;

            this.mProtocol = new StreamingProtocol(parentProtocol, connectionTable, eventQueue, timeKeeper, connectivityReporter, connectionNotifier);
            this.mProtocol.ReceiveRequestEvent += this.ReceiveRequestEventHandler;
            this.mProtocol.SendRequestEvent += this.SendRequestEventHandler;
        }

        private void ReceiveRequestEventHandler(object sender, ReceiveStreamEventArgs e)
        {
            this.queueApplicationEvent(
                new AppliedFunction<ReceiveStreamEventArgs>(this.TriggerReceiveRequest, e));
        }

        private void SendRequestEventHandler(object sender, SendStreamEventArgs e)
        {
            this.queueApplicationEvent(
                new AppliedFunction<SendStreamEventArgs>(this.TriggerSendRequest, e));
        }

        protected override IStreamController OnBeginSendReliableStream(String taggedName, Stream stream,
            BadumnaId destination, string userName, AsyncCallback callback, object callbackState)
        {
            return this.mProtocol.BeginSendReliableStream(stream, taggedName, destination.Address, userName, callback, callbackState);
        }

        protected override IStreamController OnBeginRequestReliableStream(string taggedName, Stream destinationStream, BadumnaId source,
            string userName, AsyncCallback callback, object callbackState)
        {
            if (destinationStream == null)
            {
                throw new ArgumentNullException("destinationStream");
            }

            if (!destinationStream.CanWrite)
            {
                throw new InvalidOperationException("Destination stream must be writeable");
            }

            return this.mProtocol.BeginRequestReliableStream(taggedName, destinationStream, source.Address, userName, callback, callbackState);
        }
    }
}
