﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

using Badumna;
using Badumna.Core;
using Badumna.Utilities;
using Badumna.Transport;

using Badumna.DataTypes;
using Badumna.SpatialEntities;

namespace Badumna.Streaming
{
    /// <summary>
    /// Provides the API for streaming operations.
    /// </summary>
    public abstract class StreamingManager
    {
        /// <summary>
        /// For providing the ID assigned to an entity.
        /// </summary>
        private readonly EntityIDProvider<IReplicableEntity> entityIDProvider;

        private Dictionary<String, EventHandler<ReceiveStreamEventArgs>> mReceiveRequestHandlers
            = new Dictionary<string, EventHandler<ReceiveStreamEventArgs>>();

        private Dictionary<String, EventHandler<SendStreamEventArgs>> mSendRequestHandlers
            = new Dictionary<string, EventHandler<SendStreamEventArgs>>();

        private EventHandler<ReceiveStreamEventArgs> mDefaultReceiveHandler;
        private EventHandler<SendStreamEventArgs> mDefaultSendHandler;

        /// <summary>
        /// Initializes a new instance of the StreamingManager class.
        /// </summary>
        /// <param name="entityIDProvider"></param>
        internal StreamingManager(EntityIDProvider<IReplicableEntity> entityIDProvider)
        {
            if (entityIDProvider == null)
            {
                throw new ArgumentNullException("entityIDProvider");
            }

            this.entityIDProvider = entityIDProvider;
        }

        #region Send stream methods

        /// <summary>
        /// Begin an asynchronous transfer of a file.
        /// </summary>
        /// <param name="streamTag">A unique string to differentiate streaming classes.  The handler with the corresponding tag will be invoked on the remote end.</param>
        /// <param name="fullPath">The path to the file to be sent.  The filename part of this path will be sent to the remote end as an identifier.</param>
        /// <param name="replica">A replica from the destination peer</param>
        /// <param name="userName">The username associated with the send request</param>
        /// <param name="callback">The callback to be invoked when the streaming operation ends</param>
        /// <param name="callbackState">User state passed to the callback</param>
        /// <returns>An IStreamController for monitoring/cancelling the streaming operation.</returns>
        public IStreamController BeginSendReliableFile(String streamTag, String fullPath, IReplicableEntity replica,
            string userName, AsyncCallback callback, object callbackState)
        {
            return this.BeginSendReliableFile(streamTag, fullPath, this.GetEntityID(replica), userName, callback, callbackState);
        }

        /// <summary>
        /// Begin an asynchronous transfer of a file.
        /// </summary>
        /// <param name="streamTag">A unique string to differentiate streaming classes.  The handler with the corresponding tag will be invoked on the remote end.</param>
        /// <param name="fullPath">The path to the file to be sent.  The filename part of this path will be sent to the remote end as an identifier.</param>
        /// <param name="destination">A BadumnaId specifying the destination peer</param>
        /// <param name="userName">The username associated with the send request</param>
        /// <param name="callback">The callback to be invoked when the streaming operation ends</param>
        /// <param name="callbackState">User state passed to the callback</param>
        /// <returns>An IStreamController for monitoring/cancelling the streaming operation.</returns>
        public IStreamController BeginSendReliableFile(String streamTag, String fullPath, BadumnaId destination,
            string userName, AsyncCallback callback, object callbackState)
        {
            this.CheckTagValidity(streamTag);

            if (!Path.IsPathRooted(fullPath))
            {
                throw new InvalidOperationException("Path name must be rooted.");
            }

            if (!File.Exists(fullPath))
            {
                throw new InvalidOperationException("File must exist in order to send it.");
            }

            FileStream stream = new FileStream(fullPath, FileMode.Open, FileAccess.Read, FileShare.Read);
            return this.BeginSendReliableStream(streamTag, Path.GetFileName(fullPath), stream, destination, userName, callback, callbackState);
        }

        /// <summary>
        /// Begin an asynchronous transfer of a stream.
        /// </summary>
        /// <param name="streamTag">A unique string to differentiate streaming classes.  The handler with the corresponding tag will be invoked on the remote end.</param>
        /// <param name="streamName">An identifying name for the stream</param>
        /// <param name="stream">The stream to be sent</param>
        /// <param name="replica">A replica from the destination peer</param>
        /// <param name="userName">The username associated with the send request</param>
        /// <param name="callback">The callback to be invoked when the streaming operation ends</param>
        /// <param name="callbackState">User state passed to the callback</param>
        /// <returns>An IStreamController for monitoring/cancelling the streaming operation.</returns>
        public IStreamController BeginSendReliableStream(String streamTag, String streamName, Stream stream, IReplicableEntity replica,
            string userName, AsyncCallback callback, object callbackState)
        {
            return this.BeginSendReliableStream(
                streamTag, 
                streamName,
                stream,
                this.entityIDProvider(replica),
                userName,
                callback,
                callbackState);
        }

        /// <summary>
        /// Begin an asynchronous transfer of a stream.
        /// </summary>
        /// <param name="streamTag">A unique string to differentiate streaming classes.  The handler with the corresponding tag will be invoked on the remote end.</param>
        /// <param name="streamName">An identifying name for the stream</param>
        /// <param name="stream">The stream to be sent</param>
        /// <param name="destination">A BadumnaId specifying the destination peer</param>
        /// <param name="userName">The username associated with the send request</param>
        /// <param name="callback">The callback to be invoked when the streaming operation ends</param>
        /// <param name="callbackState">User state passed to the callback</param>
        /// <returns>An IStreamController for monitoring/cancelling the streaming operation.</returns>
        public IStreamController BeginSendReliableStream(String streamTag, String streamName, Stream stream, BadumnaId destination,
            string userName, AsyncCallback callback, object callbackState)
        {
            this.CheckTagValidity(streamTag);

            return this.OnBeginSendReliableStream(StreamingManager.TaggedName(streamTag, streamName), stream, destination, userName, callback, callbackState);
        }

        /// <summary>
        /// Used internally.  Not intended to be overridden by clients.
        /// </summary>
        protected abstract IStreamController OnBeginSendReliableStream(String taggedName, Stream stream, BadumnaId destination,
            string userName, AsyncCallback callback, object callbackState);

        #endregion

        #region Request stream methods

        /// <summary>
        /// Begin an asynchronous request for a remote file.
        /// </summary>
        /// <param name="streamTag">A unique string to differentiate streaming classes.  The handler with the corresponding tag will be invoked on the remote end.</param>
        /// <param name="fileName">The filename to request from the remote peer</param>
        /// <param name="destinationPath">The local path to save the requested file</param>
        /// <param name="replica">A replica from the remote peer to request the file from</param>
        /// <param name="userName">The username associated with the request</param>
        /// <param name="callback">The callback to be invoked when the streaming operation ends</param>
        /// <param name="callbackState">User state passed to the callback</param>
        /// <returns>An IStreamController for monitoring/cancelling the streaming operation.</returns>
        public IStreamController BeginRequestReliableFile(String streamTag, string fileName, string destinationPath, IReplicableEntity replica,
            string userName, AsyncCallback callback, object callbackState)
        {
            return this.BeginRequestReliableFile(streamTag, fileName, destinationPath, this.entityIDProvider(replica), userName, callback, callbackState);
        }

        /// <summary>
        /// Begin an asynchronous request for a remote file.
        /// </summary>
        /// <param name="streamTag">A unique string to differentiate streaming classes.  The handler with the corresponding tag will be invoked on the remote end.</param>
        /// <param name="fileName">The filename to request from the remote peer</param>
        /// <param name="destinationPath">The local path to save the requested file</param>
        /// <param name="source">A BadumnaId specifying the remote peer to request the file from</param>
        /// <param name="userName">The username associated with the request</param>
        /// <param name="callback">The callback to be invoked when the streaming operation ends</param>
        /// <param name="callbackState">User state passed to the callback</param>
        /// <returns>An IStreamController for monitoring/cancelling the streaming operation.</returns>
        public IStreamController BeginRequestReliableFile(String streamTag, string fileName, string destinationPath, BadumnaId source,
            string userName, AsyncCallback callback, object callbackState)
        {
            this.CheckTagValidity(streamTag);

            CopyOnCompletionTask completionTask = new CopyOnCompletionTask(destinationPath, true, callback);

            return this.BeginRequestReliableStream(streamTag, fileName, completionTask.GetStream(), source,
                userName, completionTask.RequestFileOperationComplete, callbackState);
        }

        /// <summary>
        /// Begin an asynchronous request for a remote stream.
        /// </summary>
        /// <param name="streamTag">A unique string to differentiate streaming classes.  The handler with the corresponding tag will be invoked on the remote end.</param>
        /// <param name="streamName">The name of the stream to request from the remote peer</param>
        /// <param name="destinationStream">The stream that the content of the remote stream should be written to</param>
        /// <param name="replica">A replica from the remote peer to request the file from</param>
        /// <param name="userName">The username associated with the request</param>
        /// <param name="callback">The callback to be invoked when the streaming operation ends</param>
        /// <param name="callbackState">User state passed to the callback</param>
        /// <returns>An IStreamController for monitoring/cancelling the streaming operation.</returns>
        public IStreamController BeginRequestReliableStream(String streamTag, String streamName, Stream destinationStream, IReplicableEntity replica,
            string userName, AsyncCallback callback, object callbackState)
        {
            return this.BeginRequestReliableStream(
                streamTag,
                streamName,
                destinationStream,
                this.entityIDProvider(replica),
                userName,
                callback,
                callbackState);
        }

        /// <summary>
        /// Begin an asynchronous request for a remote stream.
        /// </summary>
        /// <param name="streamTag">A unique string to differentiate streaming classes.  The handler with the corresponding tag will be invoked on the remote end.</param>
        /// <param name="streamName">The name of the stream to request from the remote peer</param>
        /// <param name="destinationStream">The stream that the content of the remote stream should be written to</param>
        /// <param name="source">A BadumnaId specifying the remote peer to request the stream from</param>
        /// <param name="userName">The username associated with the request</param>
        /// <param name="callback">The callback to be invoked when the streaming operation ends</param>
        /// <param name="callbackState">User state passed to the callback</param>
        /// <returns>An IStreamController for monitoring/cancelling the streaming operation.</returns>
        public IStreamController BeginRequestReliableStream(String streamTag, String streamName, Stream destinationStream, BadumnaId source,
            string userName, AsyncCallback callback, object callbackState)
        {
            this.CheckTagValidity(streamTag);

            return this.OnBeginRequestReliableStream(StreamingManager.TaggedName(streamTag, streamName), destinationStream, source, userName, callback, callbackState);
        }

        /// <summary>
        /// Used internally.  Not intended to be overridden by clients.
        /// </summary>
        protected abstract IStreamController OnBeginRequestReliableStream(String taggedName, Stream destinationStream, BadumnaId source,
            string userName, AsyncCallback callback, object callbackState);

        #endregion

        /// <summary>
        /// Subscribe a handler that will be invoked when a remote peer attempts to send a stream to this peer.
        /// </summary>
        /// <param name="streamTag">A unique string to differentiate streaming classes.  This must match the streamTag passed to BeginSendReliableStream / BeginSendReliableFile.</param>
        /// <param name="handler">The handler to be invoked</param>
        public void SubscribeToReceiveStreamRequests(String streamTag, EventHandler<ReceiveStreamEventArgs> handler)
        {
            this.CheckTagValidity(streamTag);
            this.mReceiveRequestHandlers[streamTag] = handler;
        }

        internal void AddDefaultHandlers(EventHandler<ReceiveStreamEventArgs> receiveHandler, EventHandler<SendStreamEventArgs> sendHandler)
        {
            this.mDefaultReceiveHandler = receiveHandler;
            this.mDefaultSendHandler = sendHandler;
        }

        /// <summary>
        /// Subscribe a handler that will be invoked when a remote peer attempts to request a stream from this peer.
        /// </summary>
        /// <param name="streamTag">A unique string to differentiate streaming classes.  This must match the streamTag passed to BeginRequestReliableStream / BeginRequestReliableFile.</param>
        /// <param name="handler">The handler to be invoked</param>
        public void SubscribeToSendStreamRequests(String streamTag, EventHandler<SendStreamEventArgs> handler)
        {
            this.CheckTagValidity(streamTag);
            this.mSendRequestHandlers[streamTag] = handler;
        }

        private void CheckTagValidity(String streamTag)
        {
            if (string.IsNullOrEmpty(streamTag))
            {
                throw new ArgumentNullException("streamTag");
            }

            if (streamTag.Contains(":"))
            {
                throw new InvalidOperationException("Stream tags must not contain ':' characters.");
            }
        }

        internal static string TaggedName(string streamTag, string streamName)
        {
            return streamTag + ":" + streamName;
        }

        internal static void ParseTaggedName(string taggedName, out string streamTag, out string streamName)
        {
            streamTag = null;
            streamName = null;

            if (string.IsNullOrEmpty(taggedName))
            {
                return;
            }

            string[] streamNameParts = taggedName.Split(new char[] { ':' }, 2);

            switch (streamNameParts.Length)
            {
                case 1:
                    streamName = streamNameParts[0];
                    break;

                case 2:
                    streamTag = streamNameParts[0];
                    streamName = streamNameParts[1];
                    break;
            }
        }

        /// <summary>
        /// Used internally.  Not intended to be overridden by clients.
        /// </summary>
        protected void TriggerReceiveRequest(ReceiveStreamEventArgs e)
        {
            EventHandler<ReceiveStreamEventArgs> handler = null;

            if (string.IsNullOrEmpty(e.StreamTag))
            {
                Logger.TraceInformation(LogTag.Streaming, "Rejecting stream request from {0} becuase the stream tag could not be parsed.", e.PeersAddress);
                e.Reject();
                return;
            }

            if (this.mReceiveRequestHandlers.TryGetValue(e.StreamTag, out handler) ||
                (handler = this.mDefaultReceiveHandler) != null)
            {
                handler(this, e);
            }
            else
            {
                Logger.TraceInformation(LogTag.Streaming, "Rejecting stream request because tag {0} is unsubscribed to.", e.StreamTag);
                e.Reject();
            }
        }

        /// <summary>
        /// Used internally.  Not intended to be overridden by clients.
        /// </summary>
        protected void TriggerSendRequest(SendStreamEventArgs e)
        {
            EventHandler<SendStreamEventArgs> handler = null;

            if (string.IsNullOrEmpty(e.StreamTag))
            {
                Logger.TraceInformation(LogTag.Streaming, "Rejecting stream request from {0} becuase the stream tag could not be parsed.", e.PeersAddress);
                e.Reject();
                return;
            }

            if (this.mSendRequestHandlers.TryGetValue(e.StreamTag, out handler) ||
                (handler = this.mDefaultSendHandler) != null)
            {
                handler(this, e);
            }
            else
            {
                Logger.TraceInformation(LogTag.Streaming, "Rejecting stream request because tag {0} is unsubscribed to.", e.StreamTag);
                e.Reject();
            }
        }

        private BadumnaId GetEntityID(IReplicableEntity entity)
        {
            BadumnaId entityID = this.entityIDProvider(entity);
            if (entityID != null)
            {
                return entityID;
            }

            ISpatialReplica replica = entity as ISpatialReplica;
            if (replica != null)
            {
                entityID = replica.Guid;
            }

            if (entityID == null)
            {
                throw new InvalidOperationException("Entities passed to streaming manager must be replicas.");
            }

            return entityID;
        }
    }
}
