﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

using Badumna.Core;
using Badumna.Utilities;
using Badumna.Transport;

namespace Badumna.Streaming
{
    class RealTimeSource : Source
    {
        private CyclicalID.UShortID mMaximumSegmentNumber;

        public RealTimeSource(
            CyclicalID.UShortID localStreamId,
            Stream stream,
            PeerAddress destination,
            ChannelAdapter<Segment, TransportEnvelope>.Converter segmentConverter,
            AsyncCallback completionCallback,
            object callbackState,
            NetworkEventQueue eventQueue,
            ITime timeKeeper,
            INetworkConnectivityReporter connectivityReporter)
            : base(localStreamId, stream, destination, segmentConverter, completionCallback, callbackState, eventQueue, timeKeeper, connectivityReporter)
        {
            this.mIsReliable = false;
            this.mMaximumSegmentNumber = new CyclicalID.UShortID(StreamingAgent.InitialWindowSize);
        }

        protected override bool ContinueReading(Segment lastSegmentRead)
        {
            return lastSegmentRead.Num < this.mMaximumSegmentNumber;
        }

        protected override bool Acknowledge(AcknowledgementSet acknowledgements, int windowSize)
        {
            this.mMaximumSegmentNumber = new CyclicalID.UShortID((ushort)((acknowledgements.Maximum.Value + windowSize) % ushort.MaxValue));
            
            return true;
        }

        protected override bool ShouldForceStop()
        {
            return false;
        }
    }
}
