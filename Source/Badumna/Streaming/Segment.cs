﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

using Badumna.Core;
using Badumna.Utilities;
using Badumna.Transport;

namespace Badumna.Streaming
{
    class Segment : IComparable, IComparable<Segment>, ISized, IParseable
    {
        public CyclicalID.UShortID Num;
        public byte[] Buffer;

        private int mLength;
        public int Length
        {
            get { return this.mLength; }
            set { this.mLength = value; }
        }

        private StreamingAgent mAgent;
        public StreamingAgent LocalOnlySourceAgent 
        { 
            get { return this.mAgent; }
        }

        public TimeSpan EstimatedRoundTripTime { get; set; }

        public Segment() // for IParseable
        {
        }

        public Segment(CyclicalID.UShortID number, StreamingAgent agent)
        {
            this.Num = number;
            this.Buffer = new byte[StreamingAgent.SegmentSize];
            this.mAgent = agent;
        }

        public void Read(Stream stream)
        {
            this.mLength = stream.Read(this.Buffer, 0, this.Buffer.Length);
        }

        public IAsyncResult BeginRead(Stream stream, AsyncCallback callback)
        {
            return stream.BeginRead(this.Buffer, 0, this.Buffer.Length, callback, this);
        }

        public void EndRead(Stream stream, IAsyncResult ar)
        {
            this.mLength = stream.EndRead(ar);
        }

        public IAsyncResult BeginWrite(Stream stream, AsyncCallback callback)
        {
            return stream.BeginWrite(this.Buffer, 0, this.mLength, callback, this);
        }

        #region IComparable Members

        public int CompareTo(object obj)
        {
            if (obj is Segment)
            {
                return this.CompareTo(obj as Segment);
            }

            return base.GetHashCode().CompareTo(obj.GetHashCode());
        }

        #endregion

        #region IComparable<QueuedSegment> Members

        public int CompareTo(Segment other)
        {
            return this.Num.CompareTo(other.Num);
        }

        #endregion


        #region IParseable Members

        public void ToMessage(MessageBuffer message, Type parameterType)
        {
            if (message != null)
            {
                this.Num.ToMessage(message, typeof(CyclicalID.UShortID));
                message.Write((ushort)this.mLength);
                message.Write(this.Buffer, this.mLength);
            }
        }

        public void FromMessage(MessageBuffer message)
        {
            if (message != null)
            {
                this.Num = message.Read<CyclicalID.UShortID>();
                this.mLength = (int)message.ReadUShort();
                this.Buffer = message.ReadBytes(this.mLength);
            }
        }

        #endregion

        public override string ToString()
        {
            return String.Format("Seg #{0}", this.Num);
        }
    }
}
