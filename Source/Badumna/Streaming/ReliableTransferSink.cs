﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;

using Badumna.Core;
using Badumna.Utilities;

namespace Badumna.Streaming
{
    class ReliableTransferSink : Sink
    {
        private BinaryHeap<Segment> mWriteQueue = new BinaryHeap<Segment>();
        private CyclicalID.UShortID mNextSequenceNumber = new CyclicalID.UShortID(Source.FirstSegmentSeqNumberValue);

        public ReliableTransferSink(
            CyclicalID.UShortID localStreamId,
            Stream stream,
            PeerAddress destination, 
            AsyncCallback completionCallback,
            object callbackState,
            NetworkEventQueue eventQueue,
            ITime timeKeeper,
            INetworkConnectivityReporter connectivityReporter)
            : base(localStreamId, stream, destination, completionCallback, callbackState, eventQueue, timeKeeper, connectivityReporter)
        {
            this.mIsReliable = true;
        }

        protected override void PushSegment(Segment segment)
        {
            this.RecordHeartBeat();
            this.mWriteQueue.Push(segment);
        }

        protected override Segment PopSegment()
        {
            Segment segment = null;

            if (this.mWriteQueue.Count > 0 && this.mWriteQueue.Peek().Num == this.mNextSequenceNumber)
            {
                segment = this.mWriteQueue.Pop();
                this.mNextSequenceNumber.Increment();
            }

            return segment;
        }

        protected override bool ShouldForceStop()
        {
            return this.ShouldForceStop(40);
        }
    }
}
