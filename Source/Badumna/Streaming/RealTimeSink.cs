﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Threading;

using Badumna.Core;
using Badumna.Utilities;
using Badumna.Transport;

namespace Badumna.Streaming
{
    class RealTimeSink : Sink
    {
        private FifoQueue<Segment> mWriteQueue = new FifoQueue<Segment>(StreamingAgent.InitialWindowSize);

        public RealTimeSink(
            CyclicalID.UShortID localStreamId,
            Stream stream,
            PeerAddress destination, 
            AsyncCallback completionCallback,
            object callbackState,
            NetworkEventQueue eventQueue,
            ITime timeKeeper,
            INetworkConnectivityReporter connectivityReporter)
            : base(localStreamId, stream, destination, completionCallback, callbackState, eventQueue, timeKeeper, connectivityReporter)
        {
            this.mIsReliable = false;
        }

        protected override void PushSegment(Segment segment)
        {
            this.mWriteQueue.Push(segment);
        }

        protected override Segment PopSegment()
        {
            Segment segment = null;

            if (this.mWriteQueue.Count > 0)
            {
                segment = this.mWriteQueue.Pop();
            }

            return segment;
        }

        protected override bool ShouldForceStop()
        {
            return false;
        }
    }
}
