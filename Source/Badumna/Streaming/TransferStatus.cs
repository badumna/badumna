﻿using System;
using System.Collections.Generic;
using System.Text;

using Badumna.Utilities;
using Badumna.Core;

namespace Badumna.Streaming
{
    class TransferStatus
    {
        private const int UpdateFrequencySeconds = 1;
        private const double RateSampleIntervalSeconds = 1.0;
        private const double RateConvergenceSeconds = 10.0;


        private EventHandler informationChangedDelegate;
        public event EventHandler InformationChanged
        {
            add { this.informationChangedDelegate += value; }
            remove { this.informationChangedDelegate -= value; }
        }

        private RateSmoother mRateEstimator;
        DateTime mLastInfoUpdateTime = DateTime.Now;

        public TimeSpan EstimatedTimeRemaining
        {
            get
            {
                if (this.mRateEstimator.EstimatedRate <= 0)
                {
                    return TimeSpan.Zero;
                }

                double bytesLeft = Math.Max(0.0, this.mBytesTotal - this.mBytesTransfered);
                return TimeSpan.FromSeconds(bytesLeft / this.mRateEstimator.EstimatedRate);
            }
        }

        private long mBytesTotal = 1;
        public long BytesTotal
        {
            get { return this.mBytesTotal; }
            set { this.mBytesTotal = value; }
        }

        private long mBytesTransfered;
        public long BytesTransfered { get { return this.mBytesTransfered; } }

        public double TransferRateKBps { get { return this.mRateEstimator.EstimatedRate / 1024.0; } }

        public TransferStatus(ITime timeKeeper)
        {
            this.mRateEstimator = new RateSmoother(
                TimeSpan.FromSeconds(RateSampleIntervalSeconds), 
                TimeSpan.FromSeconds(RateConvergenceSeconds),
                timeKeeper);
        }

        private void TriggerInformationChanged()
        {
            EventHandler handler = this.informationChangedDelegate;
            if (handler != null)
            {
                handler.Invoke(this, null);
            }
        }

        public void AddTransferedAmount(int bytes)
        {
            this.mRateEstimator.ValueEvent(bytes);
            this.mBytesTransfered += bytes;

            if ((DateTime.Now - this.mLastInfoUpdateTime).TotalSeconds > TransferStatus.UpdateFrequencySeconds)
            {
                this.mLastInfoUpdateTime = DateTime.Now;
                this.TriggerInformationChanged();
            }
        }
    }
}
