﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

using Badumna.Core;
using Badumna.Utilities;
using Badumna.Transport;

namespace Badumna.Streaming
{
    class ReliableTransferSource : Source
    {
        private InsertionSortedList<Segment> mSentButUnacknowledgedSegments;
        private int mWindowSize = StreamingAgent.InitialWindowSize;

        public ReliableTransferSource(
            CyclicalID.UShortID localStreamId,
            Stream stream,
            PeerAddress destination,
            ChannelAdapter<Segment, TransportEnvelope>.Converter segmentConverter,
            AsyncCallback completionCallback,
            object callbackState,
            NetworkEventQueue eventQueue,
            ITime timeKeeper,
            INetworkConnectivityReporter connectivityReporter)
            : base(localStreamId, stream, destination, segmentConverter, completionCallback, callbackState, eventQueue, timeKeeper, connectivityReporter)
        {
            this.mIsReliable = true;
            this.mSentButUnacknowledgedSegments = new InsertionSortedList<Segment>();
        }

        protected override bool HasFinished()
        {
            return this.mSentButUnacknowledgedSegments.Count == 0;
        }

        protected override bool ContinueReading(Segment lastSegmentRead)
        {
          /*  System.Diagnostics.Debug.Assert(this.mSentButUnacknowledgedSegments.Count <= this.mWindowSize,
                "Streaming window size is too large.");
            */
            if (lastSegmentRead.LocalOnlySourceAgent == null)
            {
                return false;
            }

            this.mSentButUnacknowledgedSegments.Add(lastSegmentRead);
            return this.mSentButUnacknowledgedSegments.Count < this.mWindowSize;
        }

        protected override bool Acknowledge(AcknowledgementSet acknowledgements, int windowSize)
        {
            int numberOfSentSegments = this.mSentButUnacknowledgedSegments.Count;
            int numberOfAcknowledgedSegments = 0;
            int numberOfUnacknowledgedSegments = 0;
            List<Segment> sentSegments = new List<Segment>(this.mSentButUnacknowledgedSegments);
            IList<Segment> toResend = new List<Segment>();

            // ack is used as the heart beat as the ReliableTransferSink is expected to send ack every
            // 3 seconds.
            this.RecordHeartBeat();

            // Copyied to a new list because removal is O(n) so its faster to add all items again than remove half of them.
            this.mSentButUnacknowledgedSegments.Clear();
            foreach (Segment segment in sentSegments)
            {
                if (!acknowledgements.Contains(segment.Num))
                {
                    numberOfUnacknowledgedSegments++;
                    int distance = unchecked((short)segment.Num.DistanceTo(acknowledgements.Maximum));
                    if (distance >= 3) // Got some later acks, assume this was lost and resend immediately
                    {
                        toResend.Add(segment);
                    }
                    this.mSentButUnacknowledgedSegments.Add(segment);
                }
                else
                {
                    numberOfAcknowledgedSegments++;
                }
            }

            if (numberOfAcknowledgedSegments == 0)
            {
                // The receiver should send acks at regular intervals, and if it hasn't
                // received anything new then the ack will just be a repeat of the last one.
                // So if we didn't get any new info from the ack, resend all unacked segments.
                // TODO: This may be overly conservative.
                toResend = this.mSentButUnacknowledgedSegments;
            }
            foreach (Segment segment in toResend)
            {
                this.Put(segment);
            }

            System.Diagnostics.Debug.Assert(acknowledgements.Count >= numberOfAcknowledgedSegments, "Inconsist acknowledgements set size.");
            System.Diagnostics.Debug.Assert(numberOfUnacknowledgedSegments + numberOfAcknowledgedSegments == numberOfSentSegments,
                "Inconsistancy after processing of acknowledgements.");

            this.mWindowSize = windowSize;
            return numberOfUnacknowledgedSegments < windowSize;
        }

        protected override bool ShouldForceStop()
        {
            return this.ShouldForceStop(40);
        }
    }
}
