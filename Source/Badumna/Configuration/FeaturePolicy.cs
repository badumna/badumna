﻿//-----------------------------------------------------------------------
// <copyright file="FeaturePolicy.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2013 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;

namespace Badumna.Configuration
{
    /// <summary>
    /// Default Badumna Feature Policy
    /// </summary>
    internal class FeaturePolicy : IFeaturePolicy
    {
        /// <summary>
        /// The application's Options instance.
        /// </summary>
        private Options options;

        /// <summary>
        /// Initializes a new instance of the <see cref="FeaturePolicy"/> class.
        /// </summary>
        /// <param name="options">The options.</param>
        public FeaturePolicy(Options options)
        {
            this.options = options;
        }

        /// <inheritdoc/>
        public void AccessPrivateChat()
        {
            this.DisableOnCloud("Private Chat");
        }

        /// <inheritdoc/>
        public void AccessStreaming()
        {
            this.DisableOnCloud("Streaming");
        }

        /// <inheritdoc/>
        public void AccessServiceDiscovery()
        {
            this.DisableOnCloud("Service Discovery");
        }

        /// <summary>
        /// Disables the given feature in cloud mode.
        /// </summary>
        /// <param name="feature">The feature name.</param>
        private void DisableOnCloud(string feature)
        {
            if (this.options.OnCloud)
            {
                throw new InvalidOperationException(String.Format("The {0} API is not available in BadumnaCloud", feature));
            }
        }
    }
}
