﻿//-----------------------------------------------------------------------
// <copyright file="Options.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2011 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Xml;
using System.Xml.XPath;

using Badumna.Core;
using Badumna.Utilities;

namespace Badumna
{
    /// <summary>
    /// Specifies the configuration options for Badumna.
    /// </summary>
    public class Options
    {
        /// <summary>
        /// List of all option modules.
        /// </summary>
        private List<IOptionModule> modules;

        /// <summary>
        /// Whether running in cpp mode.
        /// </summary>
        private bool isInCppMode;

        /// <summary>
        /// Initializes a new instance of the <see cref="Options"/> class with the default options.
        /// </summary>
        public Options()
            : this((IXPathNavigable)null)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Options"/> class from the contents of the
        /// specified file.
        /// </summary>
        /// <remarks>
        /// The file should contain XML describing the configuration options.  To generate
        /// such a file: construct a default <see cref="Options"/> instance, configure it programmatically,
        /// then call <see cref="Save(string)"/>.
        /// </remarks>
        /// <param name="filename">The path of the file to load the options from.</param>
        /// <exception cref="ConfigurationException">Thrown if the XML file could not be loaded.</exception>
        public Options(string filename)
            : this(Options.LoadXmlDocument(filename))
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Options"/> class from an XML document.
        /// </summary>
        /// <remarks>
        /// The document should contain XML describing the configuration options.  To generate
        /// such a document: construct a default <see cref="Options"/> instance, configure it programmatically,
        /// then call <see cref="Save(XmlWriter)"/>.
        /// </remarks>
        /// <param name="xml">The XML document containing the desired options; or <c>null</c> to use the default options.</param>
        public Options(IXPathNavigable xml)
        {
            this.Load(xml);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Options"/> class, copying the options from the given instance.
        /// </summary>
        /// <param name="options">The options to copy.</param>
        public Options(Options options)
        {
            using (MemoryStream stream = new MemoryStream())
            using (XmlWriter writer = XmlWriter.Create(stream))
            {
                options.Save(writer);
                writer.Flush();
                stream.Position = 0;

                XmlDocument optionsXml = new XmlDocument();
                optionsXml.Load(stream);
                this.Load(optionsXml);
            }

            this.CloudIdentifier = options.CloudIdentifier;
            this.isInCppMode = options.isInCppMode;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this peer is in CPP mode.
        /// </summary>
        /// <value>
        /// <c>true</c> if this peer is in CPP mode; otherwise, <c>false</c>.
        /// </value>
        public bool IsInCppMode
        {
            get { return this.isInCppMode; }
            set { this.isInCppMode = value; }
        }

        /// <summary>
        /// Gets a reference to the options for the arbitration module.
        /// </summary>
        public ArbitrationModule Arbitration { get; private set; }

        /// <summary>
        /// Gets a reference to the options for the connectivity module.
        /// </summary>
        public ConnectivityModule Connectivity { get; private set; }

        /// <summary>
        /// Gets a reference to the options for the logger module.
        /// </summary>
        public LoggerModule Logger { get; private set; }

        /// <summary>
        /// Gets a reference to the options for the overload module.
        /// </summary>
        public OverloadModule Overload { get; private set; }

        /// <summary>
        /// Gets a reference to the options for the Validation module.
        /// </summary>
        public ValidationModule Validation { get; private set; }

        /// <summary>
        /// Gets a reference to the options for the Matchmaking module.
        /// </summary>
        public MatchmakingModule Matchmaking { get; private set; }

        /// <summary>
        /// Gets a value indicating whether service discovery is enabled.
        /// </summary>
        /// <remarks>
        /// Previously the value of this property was dependent on the configuration, but now it's always enabled regardless.
        /// </remarks>
        internal bool IsServiceDiscoveryEnabled
        {
            get
            {
                ////return this.Overload.IsDistributedLookupUsed ||
                ////    ((IArbitrationModule)this.Arbitration).RequiresDistributedLookup ||
                ////    ((IValidationModule)this.Validation).RequiresDistributedLookup;   
                return true;
            }
        }

        /// <summary>
        /// Gets or sets the string identifying which cloud subnetwork to connect to.
        /// </summary>
        /// <remarks>
        /// This value isn't serialized or deserialized because it's set depending on which NetworkFacade factory method was called.
        /// </remarks>
        internal string CloudIdentifier { get; set; }

        /// <summary>
        /// Gets a value indicating whether the network is configured to use the cloud network.
        /// </summary>
        /// <remarks>
        /// This value isn't serialized or deserialized because it's set depending on which NetworkFacade factory method was called.
        /// </remarks>
        internal bool OnCloud
        {
            get
            {
                return !string.IsNullOrEmpty(this.CloudIdentifier);
            }
        }

        /// <summary>
        /// Validates all options to ensure they are consistent.
        /// </summary>
        /// <remarks>
        /// This method is called automatically when <see cref="Badumna.NetworkFacade.Create(Options)"/> is called.  It can be
        /// called manually to determine if a set of options is OK to be passed to <see cref="Badumna.NetworkFacade.Create(Options)"/>.
        /// </remarks>
        /// <exception cref="ConfigurationException">Thrown if the configuration is not valid.  Check the
        /// <see cref="ConfigurationException"/> instance for further details.</exception>
        public void Validate()
        {
            foreach (IOptionModule module in this.modules)
            {
                module.Validate();
            }
        }

        /// <summary>
        /// Stores the current options in XML format.
        /// </summary>
        /// <param name="xmlWriter">The writer to use to write the options.</param>
        public void Save(XmlWriter xmlWriter)
        {
            xmlWriter.WriteStartElement("Modules");

            foreach (IOptionModule module in this.modules)
            {
                xmlWriter.WriteStartElement("Module");
                xmlWriter.WriteAttributeString("Name", module.ModuleType);
                xmlWriter.WriteAttributeString("Enabled", "true");

                module.Save(xmlWriter);

                xmlWriter.WriteEndElement();
            }

            xmlWriter.WriteEndElement();
            xmlWriter.Flush();
        }

        /// <summary>
        /// Stores the current options into the specified file in XML format.
        /// </summary>
        /// <param name="filename">The name of the file to store the options into.</param>
        public void Save(string filename)
        {
            using (XmlWriter writer = XmlWriter.Create(filename, new XmlWriterSettings { Indent = true, IndentChars = "    ", NewLineChars = "\r\n", NewLineHandling = NewLineHandling.Replace }))
            {
                this.Save(writer);
            }
        }

        /// <summary>
        /// Checks whether the element identified by <paramref name="path"/> is enabled.  If the element cannot
        /// be found then this method returns false.  If the element exists and does not have
        /// an <c>"Enabled"</c> attribute, or the <c>"Enabled"</c> attribute is empty this method returns <c>true</c>.
        /// Otherwise, this method returns <c>true</c> if the <c>"Enabled"</c> attribute contains <c>"true"</c>.
        /// </summary>
        /// <param name="navigator">The <see cref="XPathNavigator"/> that will be searched for path.</param>
        /// <param name="path">The XPath of the element to check.</param>
        /// <returns>A boolean indicating whether the XML element is enabled.</returns>
        internal static bool IsEnabled(XPathNavigator navigator, string path)
        {
            XPathNavigator element = navigator.SelectSingleNode(path);
            if (element == null)
            {
                return false;
            }

            string enabled = element.GetAttribute("Enabled", string.Empty).Trim();
            if (enabled.Length == 0)
            {
                return true;
            }

            return enabled.Equals("true", StringComparison.InvariantCultureIgnoreCase);
        }

        /// <summary>
        /// Attempts to parse an <see cref="int"/> from the inner text of the node specified by <paramref name="pathName"/>.
        /// </summary>
        /// <param name="navigator">The <see cref="XPathNavigator"/> that will be searched for <paramref name="pathName"/>.</param>
        /// <param name="pathName">An XPath identifying the node containing the <see cref="int"/>.</param>
        /// <returns>The resulting <see cref="int"/>, or <c>null</c> if no node corresponding to <paramref name="pathName"/> could be found.</returns>
        /// <exception cref="ConfigurationException">Thrown if <paramref name="pathName"/> was found but the text it contained
        /// could not be parsed into an <see cref="int"/>.</exception>
        internal static int? TryReadInt32(XPathNavigator navigator, string pathName)
        {
            XPathNavigator node = navigator.SelectSingleNode(pathName);
            if (node == null)
            {
                return null;
            }

            int result;
            if (Int32.TryParse(node.InnerXml, NumberStyles.Integer, CultureInfo.InvariantCulture, out result))
            {
                return result;
            }

            throw new ConfigurationException(ConfigurationException.ErrorCode.InvalidXml, "Could not parse Int32 from " + pathName);
        }

        /// <summary>
        /// Convenience function to allow chaining of the constructors.
        /// </summary>
        /// <param name="filename">The name of the file to load.</param>
        /// <returns>An <see cref="XmlDocument"/> representing the file.</returns>
        /// <exception cref="ConfigurationException">Thrown if the XML file could not be loaded.</exception>
        private static XmlDocument LoadXmlDocument(string filename)
        {
            GenericCallBack<ConfigurationException.ErrorCode, string, Exception> onFileError =
                (error, message, exception) =>
                {
                    throw new ConfigurationException(error, message, exception);
                };
            XmlDocument document = new XmlDocument();
            try
            {
                document.Load(filename);
            }
            catch (ArgumentException ex)
            {
                onFileError(ConfigurationException.ErrorCode.FileError, "Bad file name.", ex);
            }
            catch (System.Xml.XmlException ex)
            {
                onFileError(ConfigurationException.ErrorCode.InvalidXml, "Error parsing xml.", ex);
            }
            catch (IOException ex)
            {
                onFileError(ConfigurationException.ErrorCode.FileError, "Error opening file.", ex);
            }
            catch (UnauthorizedAccessException ex)
            {
                onFileError(ConfigurationException.ErrorCode.FileError, "Error opening file.", ex);
            }
            catch (NotSupportedException ex)
            {
                onFileError(ConfigurationException.ErrorCode.FileError, "Invalid filename.", ex);
            }
            catch (System.Security.SecurityException ex)
            {
                onFileError(ConfigurationException.ErrorCode.FileError, "Permission denied.", ex);
            }

            return document;
        }

        /// <summary>
        /// Initializes the options according to the given XML document.
        /// </summary>
        /// <param name="xml">The XML document containing the desired options; or <c>null</c> to use the default options.</param>
        private void Load(IXPathNavigable xml)
        {
            Dictionary<string, GenericCallBackReturn<IOptionModule, IXPathNavigable>> factories =
                new Dictionary<string, GenericCallBackReturn<IOptionModule, IXPathNavigable>>
            {
                { ArbitrationModule.Name, x => { this.Arbitration = new ArbitrationModule(x); return this.Arbitration; } },
                { ConnectivityModule.Name, x => { this.Connectivity = new ConnectivityModule(x); return this.Connectivity; } },
                { LoggerModule.Name, x => { this.Logger = new LoggerModule(x); return this.Logger; } },
                { OverloadModule.Name, x => { this.Overload = new OverloadModule(x); return this.Overload; } },
                { ValidationModule.Name, x => { this.Validation = new ValidationModule(x); return this.Validation; } },
                { MatchmakingModule.Name, x => { this.Matchmaking = new MatchmakingModule(x); return this.Matchmaking; } },
            };

            this.modules = new List<IOptionModule>();

            if (xml == null)
            {
                xml = new XmlDocument();
            }

            XPathNavigator navigator = xml.CreateNavigator();

            foreach (KeyValuePair<string, GenericCallBackReturn<IOptionModule, IXPathNavigable>> factory in factories)
            {
                string xpathQuery = String.Format(System.Globalization.CultureInfo.CurrentCulture, "Modules/Module[@Name='{0}']", factory.Key);
                XPathNavigator moduleNode = navigator.SelectSingleNode(xpathQuery);

                IOptionModule newModule;
                if (moduleNode != null && !moduleNode.GetAttribute("Enabled", string.Empty).Equals("false", StringComparison.InvariantCultureIgnoreCase))
                {
                    newModule = factory.Value(moduleNode);
                }
                else
                {
                    Badumna.Utilities.Logger.TraceInformation(LogTag.Configuration, "Using defaults for configuration of module '{0}'", factory.Key);
                    newModule = factory.Value(null);  // Configure using defaults
                }

                this.modules.Add(newModule);
            }
        }
    }
}
