﻿//-----------------------------------------------------------------------
// <copyright file="IOptionModule.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2011 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System.Xml;

namespace Badumna
{
    /// <summary>
    /// The internal interface implemented by all option modules.
    /// </summary>
    /// <remarks>
    /// Implementations of this interface are expected to implement a constructor with the following
    /// signature for the purpose of deserializing from XML:
    ///     OptionModule(IXPathNavigable)
    /// If the parameter to this constructor is null then the constructor should initialize the instance
    /// with the default options.
    /// </remarks>
    internal interface IOptionModule
    {
        /// <summary>
        /// Gets a string that identifies the module type.
        /// </summary>
        string ModuleType { get; }

        /// <summary>
        /// Ensures that all the options stored in the module are valid and consistent.
        /// </summary>
        /// <exception cref="Badumna.Core.ConfigurationException">If the options are not valid.</exception>
        void Validate();

        /// <summary>
        /// Serializes the configuration into XML.
        /// </summary>
        /// <param name="xmlWriter">The XmlWriter to send the configuration to.</param>
        void Save(XmlWriter xmlWriter);
    }
}
