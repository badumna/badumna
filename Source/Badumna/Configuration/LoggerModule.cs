//-----------------------------------------------------------------------
// <copyright file="LoggerModule.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2011 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Xml;
using System.Xml.XPath;

using Badumna.Core;
using Badumna.Utilities.Logging;

namespace Badumna
{
    /// <summary>
    /// Identifies the different types of logging systems.
    /// </summary>
    public enum LoggerType
    {
        /// <summary>
        /// No logging system.
        /// </summary>
        None,

        /// <summary>
        /// Logging using the .NET Framework's Trace system.
        /// </summary>
        DotNetTrace,

        /// <summary>
        /// Logging using Unity's Trace system.
        /// </summary>
        UnityTrace,

        /// <summary>
        /// Logging using Log4Net.
        /// </summary>
        Log4Net,

        /// <summary>
        /// Logging using Android's internal log system, Util.Log
        /// </summary>
        Android,

        /// <summary>
        /// Logging using the console.
        /// </summary>
        Console,

        /// <summary>
        /// Logging to a file.
        /// </summary>
        File,
    }

    /// <summary>
    /// Identifies the class of log messages.
    /// </summary>
    public enum LogLevel
    {
        /// <summary>
        /// Informational messages, describing normal progress of the system.
        /// </summary>
        Information,

        /// <summary>
        /// Warning messages, indicating an potential problem.
        /// </summary>
        Warning,

        /// <summary>
        /// Error messages, indicating a serious problem.
        /// </summary>
        Error
    }

    /// <summary>
    /// A set of tags that can be applied to a log message to allow for easy filtering.
    /// </summary>
    [Flags]
    public enum LogTag : ulong
    {
        /// <summary>
        /// No tags (for use as a filter).
        /// </summary>
        None = 0,

        /// <summary>
        /// Indicates old logging calls that have not yet been updated with specific tags.
        /// </summary>
        OldStyle = 1UL << 0,

        /// <summary>
        /// Messages that indicate an event has occurred (as distinct from Periodic).
        /// </summary>
        Event = 1UL << 1,

        /// <summary>
        /// Messages that are logged periodically (as distinct from Event).
        /// </summary>
        Periodic = 1UL << 2,

        /// <summary>
        /// Messages related to Badumna.Transport.Connections.
        /// </summary>
        Connection = 1UL << 3,

        /// <summary>
        /// Messages related to replicated entities.
        /// </summary>
        Entity = 1UL << 4,

        /// <summary>
        /// Messages related to the transport system.
        /// </summary>
        Transport = 1UL << 5,

        /// <summary>
        /// Messages related to the overload system.
        /// </summary>
        Overload = 1UL << 6,

        /// <summary>
        /// Messages related to the remote method call system itself (not instances of remote calls, but the system for sending them over the network).
        /// </summary>
        RemoteCall = 1UL << 7,

        /// <summary>
        /// Messages related to the chat system.
        /// </summary>
        Chat = 1UL << 8,

        /// <summary>
        /// Messages related to the chat / user presence system.
        /// </summary>
        Presence = 1UL << 9,

        /// <summary>
        /// Messages related to Interest Management.
        /// </summary>
        InterestManagement = 1UL << 10,

        /// <summary>
        /// Messages describing internal state.
        /// </summary>
        State = 1UL << 11,

        /// <summary>
        /// Messages related to DHT message routing.
        /// </summary>
        Routing = 1UL << 12,

        /// <summary>
        /// Messages related to cryptography.
        /// </summary>
        Crypto = 1UL << 13,

        /// <summary>
        /// Messages routed from other sources (e.g the host platform's default logging system).
        /// </summary>
        Wrapped = 1UL << 14,

        /// <summary>
        /// Messages relating to entity replication.
        /// </summary>
        Replication = 1UL << 15,

        /// <summary>
        /// Messages relating to Arbitration.
        /// </summary>
        Arbitration = 1UL << 16,

        /// <summary>
        /// Messages relating to autoreplication.
        /// </summary>
        Autoreplication = 1UL << 17,

        /// <summary>
        /// Messages relating to the match system.
        /// </summary>
        Match = 1UL << 18,

        /// <summary>
        /// Messages relating to the DHT.
        /// </summary>
        DHT = 1UL << 19,

        /// <summary>
        /// Messages relating to distributed validation.
        /// </summary>
        Validation = 1UL << 20,

        /// <summary>
        /// Messages relating to classe in the Badumna.Core namespace.
        /// </summary>
        Core = 1UL << 21,

        /// <summary>
        /// Messages relating to security.
        /// </summary>
        Security = 1UL << 22,

        /// <summary>
        /// Messages relating to the network facade.
        /// </summary>
        Facade = 1UL << 23,

        /// <summary>
        /// Messages relating to spatial entities.
        /// </summary>
        SpatialEntities = 1UL << 24,

        /// <summary>
        /// Messages relating to configuration.
        /// </summary>
        Configuration = 1UL << 25,

        /// <summary>
        /// Messages relating to distributed validation.
        /// </summary>
        DistributedController = 1UL << 26,

        /// <summary>
        /// Messages relating to multicast.
        /// </summary>
        Multicast = 1UL << 27,

        /// <summary>
        /// Messages relating to service discovery.
        /// </summary>
        ServiceDiscovery = 1UL << 28,

        /// <summary>
        /// Messages relating to streaming.
        /// </summary>
        Streaming = 1UL << 29,

        /// <summary>
        /// Messages relating to utilities.
        /// </summary>
        Utilities = 1UL << 30,

        /// <summary>
        /// Messages logging the invocation (receipt) of a protocol method.
        /// </summary>
        ProtocolMethod = 1UL << 31,

        /// <summary>
        /// Messages logging the remote call (sending) of a protocol method.
        /// </summary>
        SendProtocolMethod = 1UL << 32,

        /// <summary>
        /// Messages relating to Dei.
        /// </summary>
        Dei = 1UL << 33,

        /// <summary>
        /// Messages relating to tests.
        /// </summary>
        Tests = 1UL << 34,

        /// <summary>
        /// Messages relating to the simulator.
        /// </summary>
        Simulator = 1UL << 35,

        /// <summary>
        /// Messages temporarily inserted for debugging.
        /// </summary>
        Special = 1UL << 36,

        /// <summary>
        /// All tags (for use as a filter).
        /// </summary>
        All = ulong.MaxValue
    }
    
    /// <summary>
    /// Configuration options for the logging system.
    /// </summary>
    /// <remarks>
    /// Logging is only available in trace builds of the Badumna DLL.  In normal builds
    /// these options will have no effect.
    /// </remarks>
    public class LoggerModule : IOptionModule
    {
        /// <summary>
        /// The identifying name for this group of configuration options.
        /// </summary>
        internal const string Name = "Logger";

        /// <summary>
        /// Initializes a new instance of the <see cref="LoggerModule"/> class.
        /// </summary>
        /// <param name="xml">The xml containing the configuration options, or <c>null</c> to use the default options.</param>
        internal LoggerModule(IXPathNavigable xml)
        {
            this.LoggerType = LoggerType.DotNetTrace;
            this.LoggerConfig = null;

            this.LogTimestamp = true;
            this.LogLevel = Badumna.LogLevel.Warning;
            this.IncludeTags = LogTag.All;
            this.ExcludeTags = LogTag.None;

            if (xml != null)
            {
                this.Configure(xml);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the messages sent to the .NET Trace system
        /// should be forwarded to Badumna's logging system.  Defaults to <c>false</c>.
        /// </summary>
        /// <remarks>
        /// <para>
        /// This value is ignored if the logger type is set to <see cref="Badumna.LoggerType.DotNetTrace"/> because it
        /// would result in an infinite loop.
        /// </para>
        /// <para>
        /// This value should be set to <c>false</c> when using Unity's webplayer as it does not permit
        /// access to .NET's Trace system.
        /// </para>
        /// </remarks>
        public bool IsDotNetTraceForwardingEnabled { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether .NET's assert dialogs should be suppressed.
        /// Defaults to <c>false</c>.
        /// </summary>
        /// <remarks>
        /// This property only has an effect for <c>DEBUG</c> builds because non-<c>DEBUG</c> builds have
        /// <see cref="System.Diagnostics.Debug.Assert(bool, string)"/> calls automatically stripped out.
        /// </remarks>
        public bool IsAssertUiSuppressed { get; set; }

        /// <summary>
        /// Gets or sets a value indicating the type of logging system to use.  Defaults to
        /// <see cref="Badumna.LoggerType.DotNetTrace"/>.
        /// </summary>
        public LoggerType LoggerType { get; set; }

        /// <summary>
        /// Gets or sets a value containing configuration options specific to the logger type
        /// in use.  Defaults to <c>null</c>.
        /// </summary>
        public string LoggerConfig { get; set; }

        /// <summary>
        /// Gets or sets a value indicating the minimum severity for messages to be logged.
        /// Defaults to <see cref="Badumna.LogLevel.Warning"/>.
        /// </summary>
        public LogLevel LogLevel { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to add a timestamp to the logged messages.
        /// Defaults to <c>true</c>.
        /// </summary>
        public bool LogTimestamp { get; set; }

        /// <summary>
        /// Gets or sets a value indicating which logging tags should be included in the log.
        /// Defaults to <see cref="Badumna.LogTag.All"/>.
        /// </summary>
        /// <remarks>
        /// Only messages which have an included tag and no excluded tags will appear in the log.
        /// </remarks>
        public LogTag IncludeTags { get; set; }

        /// <summary>
        /// Gets or sets a value indicating which logging tags should be excluded from the log.
        /// Defaults to <see cref="Badumna.LogTag.None"/>.
        /// </summary>
        /// <remarks>
        /// Only messages which have an included tag and no excluded tags will appear in the log.
        /// </remarks>
        public LogTag ExcludeTags { get; set; }

        /// <inheritdoc />
        string IOptionModule.ModuleType
        {
            get
            {
                return LoggerModule.Name;
            }
        }

        /// <inheritdoc />
        void IOptionModule.Validate()
        {
        }

        /// <inheritdoc />
        void IOptionModule.Save(XmlWriter xmlWriter)
        {
            xmlWriter.WriteAttributeString("EnableDotNetTraceForwarding", this.IsDotNetTraceForwardingEnabled ? "true" : "false");
            xmlWriter.WriteAttributeString("SuppressAssertUI", this.IsAssertUiSuppressed ? "true" : "false");
            xmlWriter.WriteAttributeString("IncludeTags", this.IncludeTags.ToString());
            xmlWriter.WriteAttributeString("ExcludeTags", this.ExcludeTags.ToString());
            xmlWriter.WriteAttributeString("LogLevel", this.LogLevel.ToString());
            xmlWriter.WriteAttributeString("LogTimestamp", this.LogTimestamp ? "true" : "false");
            xmlWriter.WriteAttributeString("LoggerType", this.LoggerType.ToString());

            if (this.LoggerConfig != null)
            {
                xmlWriter.WriteAttributeString("LoggerConfig", this.LoggerConfig);
            }
        }

        /// <summary>
        /// Deserializes the logger options from the given XML.
        /// </summary>
        /// <param name="navigable">The XML containing the logger options.</param>
        private void Configure(IXPathNavigable navigable)
        {
            XPathNavigator navigator = navigable.CreateNavigator();

            if (navigator.SelectSingleNode("attribute::EnableDotNetTraceForwarding") != null)
            {
                this.IsDotNetTraceForwardingEnabled = this.IsEnabled(navigator, "EnableDotNetTraceForwarding");
            }

            this.IsAssertUiSuppressed = this.IsEnabled(navigator, "SuppressAssertUI");

            if (navigator.SelectSingleNode("attribute::LogTimestamp") != null)
            {
                this.LogTimestamp = this.IsEnabled(navigator, "LogTimestamp");
            }

            if (navigator.SelectSingleNode("attribute::IncludeTags") != null)
            {
                this.IncludeTags = this.Parse<LogTag>(navigator, "IncludeTags");
            }

            if (navigator.SelectSingleNode("attribute::ExcludeTags") != null)
            {
                this.ExcludeTags = this.Parse<LogTag>(navigator, "ExcludeTags");
            }

            if (navigator.SelectSingleNode("attribute::LogLevel") != null)
            {
                this.LogLevel = this.Parse<LogLevel>(navigator, "LogLevel");
            }

            if (navigator.SelectSingleNode("attribute::LoggerType") != null)
            {
                this.LoggerType = this.Parse<LoggerType>(navigator, "LoggerType");
            }

            if (navigator.SelectSingleNode("attribute::LoggerConfig") != null)
            {
                this.LoggerConfig = navigator.GetAttribute("LoggerConfig", string.Empty);
            }
        }

        /// <summary>
        /// Determines if a given attribute exists and has a value of <c>true</c>.
        /// </summary>
        /// <param name="navigator">The node to search for the attribute.</param>
        /// <param name="attributeName">The name of the attribute to search for.</param>
        /// <returns><c>true</c> iff the attribute exists and is set to <c>"true"</c></returns>
        private bool IsEnabled(XPathNavigator navigator, string attributeName)
        {
            try
            {
                return navigator.GetAttribute(attributeName, string.Empty).Equals("true", StringComparison.InvariantCultureIgnoreCase);
            }
            catch
            {
            }

            return false;
        }

        /// <summary>
        /// Parses an enumeration from a string.
        /// </summary>
        /// <param name="navigator">The node to search for the attribute.</param>
        /// <param name="attributeName">The name of the attribute to search for.</param>
        /// <typeparam name="T">The enumeration type to parse</typeparam>
        /// <returns>The log tags found.</returns>
        /// <exception cref="ConfigurationException">Thrown if the tags were unknown or empty.</exception>
        private T Parse<T>(XPathNavigator navigator, string attributeName)
        {
            string value = navigator.GetAttribute(attributeName, string.Empty);

            try
            {
                return (T)Enum.Parse(typeof(T), value, true);
            }
            catch (ArgumentException)
            {
                throw new ConfigurationException(ConfigurationException.ErrorCode.InvalidXml, "Unknown value in '" + attributeName + "'");
            }
        }
    }
}
