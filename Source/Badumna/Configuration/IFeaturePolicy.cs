﻿//-----------------------------------------------------------------------
// <copyright file="IFeaturePolicy.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2013 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;

namespace Badumna.Configuration
{
    /// <summary>
    /// FeaturePolicy restricts access to certain features when
    /// they are accessed.
    /// Currently, availability of features is determined by whether tha
    /// app is running in Cloud mode.
    /// </summary>
    internal interface IFeaturePolicy
    {
        /// <summary>
        /// Access private chat.
        /// </summary>
        void AccessPrivateChat();

        /// <summary>
        /// Access streaming.
        /// </summary>
        void AccessStreaming();

        /// <summary>
        /// Access service discovery.
        /// </summary>
        void AccessServiceDiscovery();
    }
}
