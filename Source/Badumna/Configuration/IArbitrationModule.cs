﻿//-----------------------------------------------------------------------
// <copyright file="IArbitrationModule.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna
{
    using System.Collections.Generic;

    /// <summary>
    /// Interface for an Arbitration Configuration Module, specifying arbitration services.
    /// </summary>
    internal interface IArbitrationModule
    {
        /// <summary>
        /// Gets a value indicating whether the the arbitration module is enabled.
        /// </summary>
        bool IsEnabled { get; }

        /// <summary>
        /// Gets a list of the servers (details) specified.
        /// </summary>
        IList<ArbitrationServerDetails> Servers { get; }

        /// <summary>
        /// Gets a value indicating whether the distributed lookup service is required.
        /// </summary>
        bool RequiresDistributedLookup { get; }

        /// <summary>
        /// Get the arbitration server details for a named service.
        /// </summary>
        /// <param name="name">The name of the arbitration service.</param>
        /// <returns>The arbitration server details.</returns>
        ArbitrationServerDetails GetServerDetails(string name);

        /// <summary>
        /// Does the module specify a server with the given name.
        /// </summary>
        /// <param name="name">The name of the arbitration server to look for.</param>
        /// <returns>True if the modules specifies a server with the given name.</returns>
        bool HasServer(string name);
    }
}
