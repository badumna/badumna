﻿//-----------------------------------------------------------------------
// <copyright file="ArbitrationModule.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna
{
    using System;
    using System.Collections.Generic;
    using System.Xml;
    using System.Xml.XPath;
    using Badumna.Core;
    using Badumna.Utilities;

    /// <summary>
    /// Configuration options for the arbitration system.
    /// </summary>
    public class ArbitrationModule : IArbitrationModule, IOptionModule
    {
        /// <summary>
        /// The identifying name for this group of configuration options.
        /// </summary>
        internal const string Name = "Arbitration";

        /// <summary>
        /// A list of all the server details.
        /// </summary>
        private List<ArbitrationServerDetails> servers = new PrintableList<ArbitrationServerDetails>();

        /// <summary>
        /// Initializes a new instance of the <see cref="ArbitrationModule"/> class.
        /// </summary>
        /// <param name="xml">The xml containing the configuration options, or <c>null</c> to use the default options.</param>
        internal ArbitrationModule(IXPathNavigable xml)
        {
            if (xml != null)
            {
                this.Configure(xml);
            }
        }

        /// <summary>
        /// Gets the list of arbitration servers.  Defaults to an empty list.
        /// </summary>
        /// <remarks>
        /// To add a server to the list call the <see cref="System.Collections.Generic.ICollection{T}.Add(T)"/> method on the list returned
        /// by this property.
        /// </remarks>
        /// <seealso cref="ArbitrationServerDetails"/>
        public IList<ArbitrationServerDetails> Servers
        {
            get { return this.servers; }
        }

        /// <inheritdoc />
        string IOptionModule.ModuleType
        {
            get
            {
                return ArbitrationModule.Name;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the module is enabled.
        /// </summary>
        /// <remarks>
        /// The arbitration module is considered to be enabled iff there is at least
        /// one arbitration server defined.
        /// </remarks>
        bool IArbitrationModule.IsEnabled
        {
            get
            {
                return this.servers.Count > 0;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the distributed lookup service is required.
        /// </summary>
        /// <remarks>
        /// The distributed lookup service is required if any server definition specifies distributed lookup.
        /// </remarks>
        bool IArbitrationModule.RequiresDistributedLookup
        {
            get
            {
                foreach (ArbitrationServerDetails serverDetails in this.servers)
                {
                    if (serverDetails.UseDistributedLookup)
                    {
                        return true;
                    }
                }

                return false;
            }
        }

        /// <summary>
        /// Test if there is a server with a given name.
        /// </summary>
        /// <param name="name">The name to look for.</param>
        /// <returns><c>true</c> if there is a server with the given name.</returns>
        bool IArbitrationModule.HasServer(string name)
        {
            return ((IArbitrationModule)this).GetServerDetails(name) != null;
        }

        /// <summary>
        /// Get the details of the server with a given name.
        /// </summary>
        /// <param name="name">The name of the server to query.</param>
        /// <returns>The details of the server with the given name, or <c>null</c> if no server has that name.</returns>
        ArbitrationServerDetails IArbitrationModule.GetServerDetails(string name)
        {
            foreach (ArbitrationServerDetails rec in this.servers)
            {
                if (rec.Name.Equals(name))
                {
                    return rec;
                }
            }

            return null;
        }

        /// <inheritdoc />
        void IOptionModule.Validate()
        {
            Dictionary<string, ArbitrationServerDetails> dict = new Dictionary<string, ArbitrationServerDetails>();

            foreach (ArbitrationServerDetails rec in this.servers)
            {
                if (dict.ContainsKey(rec.Name))
                {
                    throw new ConfigurationException(ConfigurationException.ErrorCode.DuplicateArbitrationServer);
                }

                rec.Validate();

                dict[rec.Name] = rec;
            }
        }

        /// <inheritdoc />
        void IOptionModule.Save(XmlWriter xmlWriter)
        {
            foreach (ArbitrationServerDetails serverDetails in this.servers)
            {
                xmlWriter.WriteStartElement("Server");
                xmlWriter.WriteElementString("Name", serverDetails.Name);

                if (!serverDetails.UseDistributedLookup)
                {
                    xmlWriter.WriteElementString("ServerAddress", serverDetails.Address);
                }

                xmlWriter.WriteEndElement();
            }
        }

        /// <summary>
        /// Configure the module from the xml data.
        /// </summary>
        /// <param name="navigable">An object from which to create an XPathNavigator to navigate the XML data.</param>
        private void Configure(IXPathNavigable navigable)
        {
            XPathNavigator navigator = navigable.CreateNavigator();

            foreach (XPathNavigator serverNode in navigator.SelectChildren("Server", string.Empty))
            {
                // name
                string serverName = null;
                XPathNavigator nameNode = serverNode.SelectSingleNode("child::Name");
                if (null != nameNode)
                {
                    serverName = nameNode.InnerXml.ToLower().Trim();
                }

                if (string.IsNullOrEmpty(serverName))
                {
                    throw new ConfigurationException(ConfigurationException.ErrorCode.InvalidXml, "Must specify the server name in arbitration module's configuration.");
                }

                // static server address
                string serverAddress = null;
                XPathNavigator serverAddressNode = serverNode.SelectSingleNode("child::ServerAddress");
                if (serverAddressNode != null)
                {
                    serverAddress = serverAddressNode.InnerXml.Trim();
                }

                // register a new server details record
                this.servers.Add(new ArbitrationServerDetails(serverName, serverAddress));
            }
        }
    }
}
