﻿//-----------------------------------------------------------------------
// <copyright file="ValidationModule.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna
{
    using System;
    using System.Collections.Generic;
    using System.Xml;
    using System.Xml.XPath;
    using Badumna.Core;
    using Badumna.Utilities;

    /// <summary>
    /// Interface for validation module.
    /// </summary>
    public interface IValidationModule
    {
        /// <summary>
        /// Gets a value indicating whether the the validation module is enabled.
        /// </summary>
        bool IsEnabled { get; }

        /// <summary>
        /// Gets a list of the servers (details) specified.
        /// </summary>
        IList<ValidationServerDetails> Servers { get; }

        /// <summary>
        /// Gets a value indicating whether the distributed lookup service is required.
        /// </summary>
        bool RequiresDistributedLookup { get; }

        /// <summary>
        /// Get the server details for a named service.
        /// </summary>
        /// <param name="name">The name of the validation service.</param>
        /// <returns>The validation server details.</returns>
        ValidationServerDetails GetServerDetails(string name);

        /// <summary>
        /// Does the module specify a server with the given name.
        /// </summary>
        /// <param name="name">The name of the validation server to look for.</param>
        /// <returns>True if the modules specifies a server with the given name.</returns>
        bool HasServer(string name);
    }

    /// <summary>
    /// Configuration options for the validation system.
    /// </summary>
    public class ValidationModule : IValidationModule, IOptionModule
    {
        /// <summary>
        /// The identifying name for this group of configuration options.
        /// </summary>
        internal const string Name = "Validation";

        /// <summary>
        /// A list of all the server details.
        /// </summary>
        private List<ValidationServerDetails> servers = new PrintableList<ValidationServerDetails>();

        /// <summary>
        /// Initializes a new instance of the ValidationModule class.
        /// </summary>
        /// <param name="xml">The xml containing the configuration options, or null to use the default options.</param>
        internal ValidationModule(IXPathNavigable xml)
        {
            if (xml != null)
            {
                this.Configure(xml);
            }
        }

        /// <summary>
        /// Gets a list of all the server details.
        /// </summary>
        public IList<ValidationServerDetails> Servers
        {
            get { return this.servers; }
        }

        /// <inheritdoc />
        string IOptionModule.ModuleType
        {
            get
            {
                return ValidationModule.Name;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the module is enabled.
        /// </summary>
        /// <remarks>
        /// The validation module is considered to be enabled iff there is at least
        /// one validation server defined.
        /// </remarks>
        bool IValidationModule.IsEnabled
        {
            get
            {
                return this.servers.Count > 0;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the distributed lookup service is required.
        /// </summary>
        /// <remarks>
        /// The distributed lookup service is required if any server definition specifies distributed lookup.
        /// </remarks>
        bool IValidationModule.RequiresDistributedLookup
        {
            get
            {
                foreach (ValidationServerDetails serverDetails in this.servers)
                {
                    if (serverDetails.UseDistributedLookup)
                    {
                        return true;
                    }
                }

                return false;
            }
        }

        /// <summary>
        /// Test if there is a server with a given name.
        /// </summary>
        /// <param name="name">The name to look for.</param>
        /// <returns>True if there is a server with the given name.</returns>
        bool IValidationModule.HasServer(string name)
        {
            return ((IValidationModule)this).GetServerDetails(name) != null;
        }

        /// <summary>
        /// Get the details of the server with a given name.
        /// </summary>
        /// <param name="name">The name of the server to query.</param>
        /// <returns>The details of the server with the given name, or null if no server has that name.</returns>
        ValidationServerDetails IValidationModule.GetServerDetails(string name)
        {
            foreach (ValidationServerDetails rec in this.servers)
            {
                if (rec.Name.Equals(name))
                {
                    return rec;
                }
            }

            return null;
        }

        /// <inheritdoc />
        void IOptionModule.Validate()
        {
            Dictionary<string, ValidationServerDetails> dict = new Dictionary<string, ValidationServerDetails>();

            foreach (ValidationServerDetails rec in this.servers)
            {
                if (dict.ContainsKey(rec.Name))
                {
                    throw new ConfigurationException(ConfigurationException.ErrorCode.DuplicateArbitrationServer);
                }

                rec.Validate();

                dict[rec.Name] = rec;
            }
        }

        /// <inheritdoc />
        void IOptionModule.Save(XmlWriter xmlWriter)
        {
            foreach (ValidationServerDetails serverDetails in this.servers)
            {
                xmlWriter.WriteStartElement("Server");
                xmlWriter.WriteElementString("Name", serverDetails.Name);

                if (!serverDetails.UseDistributedLookup)
                {
                    xmlWriter.WriteElementString("ServerAddress", serverDetails.Address);
                }

                xmlWriter.WriteEndElement();
            }
        }

        /// <summary>
        /// Configure the module from the xml data.
        /// </summary>
        /// <param name="navigable">An object from which to create an XPathNavigator to navigate the XML data.</param>
        private void Configure(IXPathNavigable navigable)
        {
            XPathNavigator navigator = navigable.CreateNavigator();

            foreach (XPathNavigator serverNode in navigator.SelectChildren("Server", string.Empty))
            {
                // name
                string serverName = null;
                XPathNavigator nameNode = serverNode.SelectSingleNode("child::Name");
                if (null != nameNode)
                {
                    serverName = nameNode.InnerXml.ToLower().Trim();
                }

                if (string.IsNullOrEmpty(serverName))
                {
                    throw new ConfigurationException(ConfigurationException.ErrorCode.InvalidXml, "Must specify the server name in validation module's configuration.");
                }

                // static server address
                string serverAddress = null;
                XPathNavigator serverAddressNode = serverNode.SelectSingleNode("child::ServerAddress");
                if (serverAddressNode != null)
                {
                    serverAddress = serverAddressNode.InnerXml.Trim();
                }

                // register a new server details record
                this.servers.Add(new ValidationServerDetails(serverName, serverAddress));
            }
        }
    }
}
