//-----------------------------------------------------------------------
// <copyright file="ConnectivityModule.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Xml;
using System.Xml.XPath;

using Badumna.Core;
using Badumna.Transport;
using Badumna.Utilities;

namespace Badumna
{
    /// <summary>
    /// Specifies when a tunnelling connection should be used.
    /// </summary>
    public enum TunnelMode
    {
        /// <summary>
        /// Never use tunnelling.
        /// </summary>
        Off,

        /// <summary>
        /// Use tunnelling if a direct connection cannot be established.
        /// </summary>
        Auto,

        /// <summary>
        /// Always use tunnelling.
        /// </summary>
        On
    }

    /// <summary>
    /// Configuration options for the connectivity system.
    /// </summary>
    public class ConnectivityModule : IOptionModule
    {
        /// <summary>
        /// The identifying name for this group of configuration options.
        /// </summary>
        internal const string Name = "Connectivity";

        /// <summary>
        /// A string that must be supplied whenever the transport limiter is enabled.
        /// </summary>
        private const string TransportLimitDisclaimer = "i_understand_this_is_for_testing_only";

        /// <summary>
        /// A list of the URIs from <see cref="TunnelUris"/> converted from <see cref="string"/> to <see cref="System.Uri"/>.
        /// Only valid after <see cref="IOptionModule.Validate()"/> has been called.
        /// </summary>
        private List<Uri> tunnelUrisAsUris;
      
        /// <summary>
        /// Initializes a new instance of the <see cref="ConnectivityModule"/> class.
        /// </summary>
        /// <param name="xml">The xml containing the configuration options, or <c>null</c> to use the default options.</param>
        internal ConnectivityModule(IXPathNavigable xml)
        {
            this.ApplicationName = string.Empty;

            this.StunServers = new PrintableList<string>();
            string[] defaultStunServers = new string[]
            {
                "stun1.noc.ams-ix.net",
                "stun.voipbuster.com",
                "stun.voxgratia.org",
            };

            foreach (string stunServer in defaultStunServers)
            {
                this.StunServers.Add(stunServer);
            }

            this.SeedPeers = new PrintableList<string>();
            
            this.StartPortRange = Parameters.DefaultStartPort;
            this.EndPortRange = Parameters.DefaultEndPort;
            this.MaxPortsToTry = 5;

            this.IsPortForwardingEnabled = true;

            this.BroadcastPort = Parameters.DefaultBroadcastPort;
            this.IsBroadcastEnabled = true;

            this.TunnelMode = TunnelMode.Off;
            this.TunnelUris = new PrintableList<string>();

            if (xml != null)
            {
                this.Configure(xml);
            }
        }

        // -----------------------------------------------------------------------------------------------------------------------------

        /// <inheritdoc />
        string IOptionModule.ModuleType
        {
            get
            {
                return ConnectivityModule.Name;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the transport limiter is enabled.  Defaults to <c>false</c>.
        /// </summary>
        /// <remarks>
        /// The transport limiter limits outband bandwidth to the value specified by <see cref="BandwidthLimit"/>.
        /// It is only intended for testing purposes.
        /// </remarks>
        /// <value>
        /// <c>true</c> if the transport limiter is enabled; otherwise <c>false</c>.
        /// </value>
        /// <seealso cref="EnableTransportLimiter(string)"/>
        /// <seealso cref="DisableTransportLimiter()"/>
        /// <seealso cref="BandwidthLimit"/>
        public bool IsTransportLimiterEnabled { get; private set; }

        /// <summary>
        /// Gets or sets the name uniquely identifying the application.  Defaults to the empty string.
        /// </summary>
        /// <remarks>
        /// Only peers initialized with the same application name will be able to connect to each other.
        /// The recommended format is a reverse domain name style string to ensure uniqueness.
        /// e.g. <c>"com.example.epic-elevator-conflict"</c>.
        /// </remarks>
        public string ApplicationName { get; set; }

        /// <summary>
        /// Gets or sets the bandwidth limit in bytes per second.  Defaults to <c>0</c>.
        /// </summary>
        /// <remarks>
        /// This value defines the bandwidth limit when the transport limiter is enabled.
        /// </remarks>
        /// <seealso cref="EnableTransportLimiter(string)"/>
        /// <seealso cref="DisableTransportLimiter()"/>
        /// <seealso cref="IsTransportLimiterEnabled"/>
        public int BandwidthLimit { get; set; }

        /// <summary>
        /// Gets the list of STUN servers.  Defaults to a list containing at least
        /// one STUN server.
        /// </summary>
        /// <remarks>
        /// STUN will be used to determine the peer's public IP address if any STUN servers
        /// are defined in this list.  To add a server to the list call the 
        /// <see cref="System.Collections.Generic.ICollection{T}.Add(T)"/> method on the list returned
        /// by this property.  Clear this list to disable STUN.  
        /// </remarks>
        public IList<string> StunServers { get; private set; }

        /// <summary>
        /// Gets or sets the start of the UDP port range to use for Badumna traffic.  Defaults to <c>21300</c>.
        /// </summary>
        /// <remarks>
        /// A single port within the range specified by <see cref="StartPortRange"/> and <see cref="EndPortRange"/> (inclusive) will be used.
        /// </remarks>
        /// <seealso cref="EndPortRange"/>
        /// <seealso cref="MaxPortsToTry"/>
        public int StartPortRange { get; set; }

        /// <summary>
        /// Gets or sets the end of the UDP port range to use for Badumna traffic.  Defaults to <c>21399</c>.
        /// </summary>
        /// <remarks>
        /// A single port within the range specified by <see cref="StartPortRange"/> and <see cref="EndPortRange"/> (inclusive) will be used.
        /// </remarks>
        /// <seealso cref="StartPortRange"/>
        /// <seealso cref="MaxPortsToTry"/>
        public int EndPortRange { get; set; }

        /// <summary>
        /// Gets or sets the maximum number of ports to try within the port range.  Defaults to <c>5</c>.
        /// </summary>
        /// <remarks>
        /// Badumna will try up to this many ports when initializing before giving up.  If <see cref="TunnelMode"/> is set to
        /// <see cref="Badumna.TunnelMode.Auto"/> then tunnelling will be used once <see cref="MaxPortsToTry"/> ports have been tried.
        /// Set this to <c>0</c> to try all ports in the port range.
        /// Setting this too low may make Badumna think it cannot form a connection when there are actually ports available.  Setting
        /// this too high will result in a long time before Badumna detects that it cannot form a connection in the case UDP is blocked
        /// (and thus a long time before it auto switches to tunnelling if that behaviour is configured).
        /// </remarks>
        /// <seealso cref="StartPortRange"/>
        /// <seealso cref="EndPortRange"/>
        public int MaxPortsToTry { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether an attempt should be made to configure a port
        /// forwarding rule on the local router using a mechanism such as UPnP.  Defaults to <c>true</c>.
        /// </summary>
        public bool IsPortForwardingEnabled { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether local network broadcast should be used to discover peers.
        /// Defaults to <c>true</c>.
        /// </summary>
        public bool IsBroadcastEnabled { get; set; }

        /// <summary>
        /// Gets or sets the UDP port to be used for broadcast discovery of peers on the local network.
        /// Defaults to <c>39817</c>.
        /// </summary>
        public int BroadcastPort { get; set; }

        /// <summary>
        /// Gets or sets an option indicating when a tunnelled connection should be used.
        /// Defaults to <see cref="Badumna.TunnelMode.Off"/>.
        /// </summary>
        public TunnelMode TunnelMode { get; set; }

        /// <summary>
        /// Gets the list of tunnel server URIs.  Defaults to an empty list.
        /// </summary>
        /// <remarks>
        /// To add a URI to the list call the <see cref="System.Collections.Generic.ICollection{T}.Add(T)"/>
        /// method on the list returned by this property.
        /// </remarks>
        public IList<string> TunnelUris { get; private set; }

        /// <summary>
        /// Gets or sets a value indicating whether the current process is a hosted service, such as
        /// a seedpeer, overload server, or arbitration server.  Defaults to <c>false</c>.
        /// </summary>
        /// <value><c>true</c> if the process is a hosted service; otherwise <c>false</c>.</value>
        public bool IsHostedService { get; set; }

        /// <summary>
        /// Gets the list of seed peer addresses.  Defaults to an empty list.
        /// </summary>
        /// <remarks>
        /// Seed peers are peers expected to always be present on the network and are
        /// used to connect to the network when no other peers are known.  To add an
        /// address to the list call the <see cref="System.Collections.Generic.ICollection{T}.Add(T)"/>
        /// method on the list returned by this property.
        /// </remarks>
        public IList<string> SeedPeers { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the STUN protocol should be used to determine the peer's public address.
        /// </summary>
        internal bool PerformStun
        {
            get
            {
                return this.StunServers.Count > 0;
            }
        }

        /// <summary>
        /// Gets a list of tunnel URIs as <see cref="System.Uri"/> objects.
        /// This getter can only be called after <see cref="IOptionModule.Validate()"/> has been called.
        /// </summary>
        internal List<Uri> TunnelUrisAsUris
        {
            get
            {
                if (this.tunnelUrisAsUris == null)
                {
                    throw new InvalidOperationException("Internal error: Validate must be called first");
                }

                return this.tunnelUrisAsUris;
            }
        }

        // -----------------------------------------------------------------------------------------------------------------------------

        /// <summary>
        /// Enables the transport limiter.
        /// </summary>
        /// <param name="testingOnlyAcknowledgement">Must contain the string <c>"i_understand_this_is_for_testing_only"</c>
        /// to indicate acknowledgement that this feature is not intended for use in production.</param>
        public void EnableTransportLimiter(string testingOnlyAcknowledgement)
        {
            if (testingOnlyAcknowledgement != ConnectivityModule.TransportLimitDisclaimer)
            {
                throw new ArgumentException("testingOnlyAcknowledgement must contain the string \"" + ConnectivityModule.TransportLimitDisclaimer + "\"");
            }

            this.IsTransportLimiterEnabled = true;
        }

        /// <summary>
        /// Disables the transport limiter.
        /// </summary>
        public void DisableTransportLimiter()
        {
            this.IsTransportLimiterEnabled = false;
        }

        /// <summary>
        /// Sets configuration options appropriate for use on a local area network.
        /// </summary>
        /// <remarks>
        /// Specifically, this method:
        /// <list type="bullet">
        ///    <item><description>Clears the <see cref="StunServers"/> list.</description></item>
        ///    <item><description>Sets <see cref="IsPortForwardingEnabled"/> to <c>false</c>.</description></item>
        ///    <item><description>Sets <see cref="IsBroadcastEnabled"/> to <c>true</c>.</description></item>
        /// </list>
        /// </remarks>
        public void ConfigureForLan()
        {
            this.StunServers.Clear();
            this.IsPortForwardingEnabled = false;
            this.IsBroadcastEnabled = true;
        }

        /// <summary>
        /// Sets the port range properties so that a specific port is used.
        /// </summary>
        /// <remarks>
        /// Specifically, this method:
        /// <list type="bullet">
        ///    <item><description>Sets <see cref="StartPortRange"/> and <see cref="EndPortRange"/> to <paramref name="port"/>.</description></item>
        ///    <item><description>Sets <see cref="MaxPortsToTry"/> to <c>1</c>.</description></item>
        /// </list>
        /// </remarks>
        /// <param name="port">The port to use for Badumna traffic.</param>
        public void ConfigureForSpecificPort(int port)
        {
            this.StartPortRange = port;
            this.EndPortRange = port;
            this.MaxPortsToTry = 1;
        }

        /// <summary>
        /// Validates the configuration.
        /// </summary>
        void IOptionModule.Validate()
        {
            if (string.IsNullOrEmpty(this.ApplicationName))
            {
                throw new ConfigurationException(ConfigurationException.ErrorCode.InvalidApplicationName, "Application Name can not be empty or null");
            }

            this.ValidatePort(this.StartPortRange, "StartPortRange");
            this.ValidatePort(this.EndPortRange, "EndPortRange");

            if (this.StartPortRange > this.EndPortRange)
            {
                throw new ConfigurationException(ConfigurationException.ErrorCode.Inconsistent, "StartPortRange must be less than or equal to EndPortRange");
            }

            if (this.IsBroadcastEnabled)
            {
                this.ValidatePort(this.BroadcastPort, "BroadcastPort");

                if (this.BroadcastPort >= this.StartPortRange && this.BroadcastPort <= this.EndPortRange)
                {
                    throw new ConfigurationException(ConfigurationException.ErrorCode.Inconsistent, "The broadcast port cannot be within the port range.");
                }
            }

            if (!this.IsBroadcastEnabled && this.SeedPeers.Count == 0)
            {
                throw new ConfigurationException(ConfigurationException.ErrorCode.Inconsistent, "No discovery mechanism configured.  Enable broadcast or add a seed peer.");
            }

            this.tunnelUrisAsUris = new List<Uri>();
            foreach (string uriString in this.TunnelUris)
            {
                try
                {
                    this.tunnelUrisAsUris.Add(new Uri(uriString));
                }
                catch (UriFormatException)
                {
                    throw new ConfigurationException(ConfigurationException.ErrorCode.InvalidUri, uriString);
                }
            }

            /* TODO: Perform some validation on seed peer addresses.  e.g. check that port is specified and parseable and in range.
             * TODO: Perform validation on stun server address.  At the moment, it's not possible to specify an alternate port -
             *      it always uses the default stun port (hardcoded magic number in UDPTransportLayer).
             */

            if (this.IsTransportLimiterEnabled)
            {
                Logger.TraceInformation(LogTag.Configuration, "Transport limiter: Enabled -- this should *ONLY* be used for testing purpose.");
            }
        }

        /// <inheritdoc />
        void IOptionModule.Save(XmlWriter xmlWriter)
        {
            // Unusual elements that we only want to write out if they have been enabled.
            if (this.IsTransportLimiterEnabled)
            {
                xmlWriter.WriteElementString("TransportLimiter", ConnectivityModule.TransportLimitDisclaimer);
            }

            if (this.BandwidthLimit != 0)
            {
                xmlWriter.WriteElementString("BandwidthLimit", this.BandwidthLimit.ToString(CultureInfo.InvariantCulture));
            }

            if (this.IsHostedService)
            {
                xmlWriter.WriteStartElement("HostedService");
                xmlWriter.WriteAttributeString("Enabled", "true");
                xmlWriter.WriteEndElement();
            }

            // All other elements that should always be written out regardless.  This avoids the problem of checking
            // whether the current setting matches the default value.

            // Application name
            xmlWriter.WriteElementString("ApplicationName", this.ApplicationName);

            // Ports
            xmlWriter.WriteStartElement("PortRange");
            xmlWriter.WriteAttributeString("MaxPortsToTry", this.MaxPortsToTry.ToString(CultureInfo.InvariantCulture));
            xmlWriter.WriteString(string.Format(CultureInfo.InvariantCulture, "{0},{1}", this.StartPortRange, this.EndPortRange));
            xmlWriter.WriteEndElement();

            // Stun
            xmlWriter.WriteStartElement("Stun");
            foreach (string stunServer in this.StunServers)
            {
                xmlWriter.WriteElementString("Server", stunServer);
            }

            xmlWriter.WriteEndElement();

            // Port forwarding
            xmlWriter.WriteStartElement("PortForwarding");
            xmlWriter.WriteAttributeString("Enabled", this.IsPortForwardingEnabled ? "true" : "false");
            xmlWriter.WriteEndElement();

            // Broadcast
            xmlWriter.WriteStartElement("Broadcast");
            xmlWriter.WriteAttributeString("Enabled", this.IsBroadcastEnabled ? "true" : "false");
            xmlWriter.WriteString(this.BroadcastPort.ToString(CultureInfo.InvariantCulture));
            xmlWriter.WriteEndElement();

            // Seed peers
            xmlWriter.WriteElementString("Initializer", this.SeedPeers.ToString());

            // Tunnel
            xmlWriter.WriteStartElement("Tunnel");
            string tunnelMode = "off";
            switch (this.TunnelMode)
            {
                case TunnelMode.On:
                    tunnelMode = "on";
                    break;

                case TunnelMode.Auto:
                    tunnelMode = "auto";
                    break;
            }

            xmlWriter.WriteAttributeString("Mode", tunnelMode);

            foreach (string tunnelUri in this.TunnelUris)
            {
                xmlWriter.WriteElementString("Uri", tunnelUri);
            }

            xmlWriter.WriteEndElement();
        }

        /// <summary>
        /// Ensures that a port is in the range of an unsigned short.
        /// </summary>
        /// <param name="port">The port to check.</param>
        /// <param name="use">An English description of the port's use.</param>
        private void ValidatePort(int port, string use)
        {
            if (port < ushort.MinValue || port > ushort.MaxValue)
            {
                throw new ConfigurationException(
                    ConfigurationException.ErrorCode.InvalidPort,
                    string.Format("{0} must be in the range {1}-{2}", use, ushort.MinValue, ushort.MaxValue));
            }
        }

        /// <summary>
        /// Configure the connectivity options from the given XML object.
        /// </summary>
        /// <param name="navigable">IXPathNavigable object containing the connectivity options</param>
        private void Configure(IXPathNavigable navigable)
        {
            XPathNavigator navigator = navigable.CreateNavigator();

            XPathNavigator transportLimiterNode = navigator.SelectSingleNode("child::TransportLimiter");
            if (null != transportLimiterNode)
            {
                this.IsTransportLimiterEnabled = transportLimiterNode.InnerXml.ToLower() == ConnectivityModule.TransportLimitDisclaimer;
            }

            int? bandwidthLimit = Options.TryReadInt32(navigator, "child::BandwidthLimit");
            if (bandwidthLimit != null)
            {
                this.BandwidthLimit = bandwidthLimit.Value;
            }

            int? port = Options.TryReadInt32(navigator, "child::Port");
            if (port != null)
            {
                this.ConfigureForSpecificPort(port.Value);
            }

            XPathNavigator applicationNameNode = navigator.SelectSingleNode("child::ApplicationName");
            if (applicationNameNode != null)
            {
                this.ApplicationName = applicationNameNode.InnerXml.Trim();
            }

            this.ConfigurePortRange(navigator.SelectSingleNode("child::PortRange"));

            this.IsHostedService = Options.IsEnabled(navigator, "child::HostedService");

            XPathNavigator stunNode = navigator.SelectSingleNode("child::Stun");
            if (stunNode != null)
            {
                // Remove any default servers
                this.StunServers.Clear();

                foreach (XPathNavigator addressNode in stunNode.SelectChildren("Server", string.Empty))
                {
                    string address = addressNode.InnerXml.Trim();
                    if (address.Length > 0)
                    {
                        this.StunServers.Add(address);
                    }
                }

                if (stunNode.SelectSingleNode("child::DefaultNAT") != null)
                {
                    Logger.TraceInformation(LogTag.Configuration, "The DefaultNAT option is no longer supported.");
                }
            }

            // Port forwarding defaults to true if the element isn't present.
            if (navigator.SelectSingleNode("child::PortForwarding") != null)
            {
                this.IsPortForwardingEnabled = Options.IsEnabled(navigator, "child::PortForwarding");
            }

            // Broadcast defaults to true if the element isn't present.
            // TODO: For all options - should always only overwrite the default value if the element is present
            //       (rather than just directly setting the value from IsEnabled in the case where the default
            //        value is false).
            if (navigator.SelectSingleNode("child::Broadcast") != null)
            {
                this.IsBroadcastEnabled = Options.IsEnabled(navigator, "child::Broadcast");
            }

            int? broadcastPort = Options.TryReadInt32(navigator, "child::Broadcast");
            if (broadcastPort != null)
            {
                this.BroadcastPort = broadcastPort.Value;
            }

            XPathNavigator initializerNode = navigator.SelectSingleNode("child::Initializer");
            if (initializerNode != null)
            {
                string[] seedPeers = initializerNode.InnerXml.Split(',');
                foreach (string seedPeer in seedPeers)
                {
                    string trimmedPeer = seedPeer.Trim();
                    if (trimmedPeer.Length == 0)
                    {
                        continue;
                    }

                    this.SeedPeers.Add(trimmedPeer);
                }
            }

            this.ConfigureTunnelling(navigator.SelectSingleNode("child::Tunnel"));

            // Legacy option: LanTestMode.  This won't be serialized, but if it's present then we'll accept it.
            if (Options.IsEnabled(navigator, "child::LanTestMode"))
            {
                this.ConfigureForLan();  // Do this last because it's intended to override any other options.
                Logger.TraceInformation(LogTag.Configuration, "LanTestMode element found, invoking ConfigureForLan()");
            }
        }

        /// <summary>
        /// Configure tunnelling from the given XML object.
        /// </summary>
        /// <param name="tunnel">XPathNavigator object containing the tunnelling configuration</param>
        private void ConfigureTunnelling(XPathNavigator tunnel)
        {
            if (tunnel == null)
            {
                return;
            }

            string modeString = tunnel.GetAttribute("Mode", string.Empty).ToLower(CultureInfo.InvariantCulture).Trim();

            bool modeSpecified = modeString.Length > 0;

            if (modeSpecified)
            {
                switch (modeString)
                {
                    case "off":
                        this.TunnelMode = TunnelMode.Off;
                        break;

                    case "auto":
                        this.TunnelMode = TunnelMode.Auto;
                        break;

                    case "on":
                        this.TunnelMode = TunnelMode.On;
                        break;

                    default:
                        throw new ConfigurationException(ConfigurationException.ErrorCode.InvalidXml, "Unrecognised Tunnel Mode");
                }
            }

            foreach (XPathNavigator uri in tunnel.SelectChildren("Uri", string.Empty))
            {
                this.TunnelUris.Add(uri.Value);
            }

            if (this.TunnelUris.Count > 0 && !modeSpecified)
            {
                this.TunnelMode = TunnelMode.Auto;
            }
        }

        /// <summary>
        /// Configure the port range from the given XML object.
        /// </summary>
        /// <param name="portRangeNode">XPathNavigator object containing the prot range configuration</param>
        private void ConfigurePortRange(XPathNavigator portRangeNode)
        {
            if (portRangeNode == null)
            {
                return;
            }

            string[] portRangeStrings = portRangeNode.InnerXml.Split(',');

            if (portRangeStrings.Length != 2)
            {
                throw new ConfigurationException(ConfigurationException.ErrorCode.InvalidXml, "PortRange must contain two integers separated by a comma (e.g. \"20000,21000\")");
            }

            int start;
            int end;
            if (int.TryParse(portRangeStrings[0], NumberStyles.Integer, CultureInfo.InvariantCulture, out start) &&
                int.TryParse(portRangeStrings[1], NumberStyles.Integer, CultureInfo.InvariantCulture, out end))
            {
                this.StartPortRange = Math.Min(start, end);
                this.EndPortRange = Math.Max(start, end);
            }
            else
            {
                throw new ConfigurationException(ConfigurationException.ErrorCode.InvalidXml, "Ports specified in PortRange could not be parsed as integers");
            }

            int? maxPortsToTry = Options.TryReadInt32(portRangeNode, "attribute::MaxPortsToTry");
            if (maxPortsToTry != null)
            {
                this.MaxPortsToTry = maxPortsToTry.Value;
            }
        }
    }
}
