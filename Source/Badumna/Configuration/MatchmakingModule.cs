﻿//------------------------------------------------------------------------------
// <copyright file="MatchmakingModule.cs" company="National ICT Australia Limited">
//     Copyright (c) 2013 National ICT Australia Limited. All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

namespace Badumna
{
    using System;
    using System.Globalization;
    using System.Xml;
    using System.Xml.XPath;
    using Badumna.Core;

    /// <summary>
    /// Configuration options for the matchmaking system.
    /// </summary>
    public class MatchmakingModule : IOptionModule
    {
        /// <summary>
        /// The identifying name for this group of configuration options.
        /// </summary>
        internal const string Name = "Matchmaking";

        /// <summary>
        /// The resolved <see cref="PeerAddress"/> of the matchmaking server.
        /// </summary>
        /// <remarks>
        /// Iff <see cref="IOptionModule.Validate()"/> has not been called and succeeded then this will be set to <c>null</c>.
        /// </remarks>
        private PeerAddress serverAddress;

        /// <summary>
        /// Initializes a new instance of the <see cref="MatchmakingModule"/> class.
        /// </summary>
        /// <param name="xml">The xml containing the configuration options, or <c>null</c> to use the default options.</param>
        internal MatchmakingModule(IXPathNavigable xml)
        {
            this.ServerAddress = "";

            if (xml != null)
            {
                this.Configure(xml);
            }
        }

        /// <summary>
        /// Initializes a new instance of the MatchmakingModule class.
        /// </summary>
        /// <remarks>TODO: Remove this constructor hacked in here for ease of development.</remarks>
        /// <param name="serverAddress">The address of the matchmaking server.</param>
        /// <param name="activeMatchLimit">The maximum number of matches permitted to be active at once.</param>
        internal MatchmakingModule(string serverAddress, int activeMatchLimit)
        {
            this.serverAddress = PeerAddress.GetAddress(serverAddress);
            this.ActiveMatchLimit = activeMatchLimit;
        }

        /// <summary>
        /// Gets or sets the address of the matchmaking server.
        /// </summary>
        /// <remarks>
        /// Defaults to the empty string.
        /// </remarks>
        public string ServerAddress { get; set; }

        /// <summary>
        /// Gets or sets the maximum number of active matches allowed on the matchmaking server.
        /// </summary>
        /// <remarks>
        /// This defaults to 0, meaning the peer will not act as a matchmaking server.
        /// </remarks>
        public int ActiveMatchLimit { get; set; }

        /// <inheritdoc />
        string IOptionModule.ModuleType
        {
            get { return MatchmakingModule.Name; }
        }

        /// <summary>
        /// Gets the <see cref="PeerAddress"/> corresponding to <see cref="ServerAddress"/>.
        /// </summary>
        internal PeerAddress ServerPeerAddress
        {
            get
            {
                if (this.serverAddress == null)
                {
                    this.InitializeServerPeerAddress();
                }

                return this.serverAddress;
            }
        }

        /// <inheritdoc />
        void IOptionModule.Validate()
        {
            if (this.ActiveMatchLimit < 0)
            {
                throw new ConfigurationException(
                    Badumna.Core.ConfigurationException.ErrorCode.Unspecified,
                    "Matchmaking.ActiveMatchLimit must not be negative.");
            }
        }

        /// <inheritdoc />
        void IOptionModule.Save(XmlWriter xmlWriter)
        {
            xmlWriter.WriteElementString("ServerAddress", this.ServerAddress);
            xmlWriter.WriteElementString("ActiveMatchLimit", this.ActiveMatchLimit.ToString(CultureInfo.InvariantCulture));
        }

        /// <summary>
        /// Configure the module from the xml data.
        /// </summary>
        /// <param name="navigable">An object from which to create an XPathNavigator to navigate the XML data.</param>
        private void Configure(IXPathNavigable navigable)
        {
            var navigator = navigable.CreateNavigator();

            var serverAddressNode = navigator.SelectSingleNode("child::ServerAddress");
            if (serverAddressNode != null)
            {
                this.ServerAddress = serverAddressNode.InnerXml.Trim();
            }

            var activeMatchLimitNode = Options.TryReadInt32(navigator, "child::ActiveMatchLimit");
            if (activeMatchLimitNode.HasValue)
            {
                this.ActiveMatchLimit = activeMatchLimitNode.Value;
            }
        }

        /// <summary>
        /// Create the server peer address by parsing the specified address, if set.
        /// </summary>
        private void InitializeServerPeerAddress()
        {
            if (!string.IsNullOrEmpty(this.ServerAddress))
            {
                this.serverAddress = PeerAddress.GetAddress(this.ServerAddress);
            }

            if (this.serverAddress == null)
            {
                this.serverAddress = PeerAddress.Nowhere;
            }
        }
    }
}
