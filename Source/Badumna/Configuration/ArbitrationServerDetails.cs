﻿//-----------------------------------------------------------------------
// <copyright file="ArbitrationServerDetails.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace Badumna
{
    using System;
    using System.Net;
    using Badumna.Core;

    /// <summary>
    /// Identifies an arbitration server.
    /// </summary>
    public class ArbitrationServerDetails
    {
        /// <summary>
        /// The resolved <see cref="PeerAddress"/>.
        /// </summary>
        /// <remarks>
        /// Iff <see cref="Validate()"/> has not been called and succeeded then this will be set to <c>null</c>.
        /// If <see cref="UseDistributedLookup"/> is true then this will have the value PeerAddress.Nowhere after calling Validate().
        /// </remarks>
        private PeerAddress peerAddress;

        /// <summary>
        /// Initializes a new instance of the <see cref="ArbitrationServerDetails"/> class that
        /// identifies a server that will be located using distributed lookup.
        /// </summary>
        /// <param name="name">The name identifying the arbitration server.</param>
        /// <exception cref="ArgumentException">Thrown if <paramref name="name"/> is empty or <c>null</c>.</exception>
        public ArbitrationServerDetails(string name)
            : this(name, null)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ArbitrationServerDetails"/> class with
        /// a specific server address.
        /// </summary>
        /// <param name="name">The name identifying the arbitration server.</param>
        /// <param name="address">The server address, or <c>null</c> to indicate that distributed lookup should be used.</param>
        /// <exception cref="ArgumentException">Thrown if <paramref name="name"/> is empty or <c>null</c>.</exception>
        public ArbitrationServerDetails(string name, string address)
        {
            if (string.IsNullOrEmpty(name))
            {
                throw new ArgumentException("Server name must not be empty or null.", "name");
            }

            if (name.Contains(";") || name.Contains(","))
            {
                throw new ArgumentException("Server name must not contain ',' or ';' characters", "name");
            }

            this.Name = name.ToLower();
            this.Address = address;
        }

        /// <summary>
        /// Gets the name of the server.
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// Gets a value indicating whether distributed lookup will be used.
        /// </summary>
        public bool UseDistributedLookup
        {
            get
            {
                return this.Address == null;
            }
        }

        /// <summary>
        /// Gets the address of the server.
        /// </summary>
        /// <remarks>
        /// Not valid if <see cref="UseDistributedLookup"/> is <c>true</c>.
        /// </remarks>
        public string Address { get; private set; }

        /// <summary>
        /// Gets the <see cref="PeerAddress"/> corresponding to <see cref="Address"/>.
        /// </summary>
        internal PeerAddress PeerAddress
        {
            get
            {
                if (this.peerAddress == null)
                {
                    throw new InvalidOperationException("Internal error: Validate must be called first");
                }

                return this.peerAddress;
            }
        }

        /// <summary>
        /// Returns a string representing the server details in the form <c>"name[;address]"</c>.  The
        /// address component is only present if an address is specified (i.e. it is not present
        /// if distributed lookup is used).
        /// </summary>
        /// <returns>A string representing the instance.</returns>
        public override string ToString()
        {
            if (this.UseDistributedLookup)
            {
                return this.Name;
            }
            else
            {
                return this.Name + ";" + this.Address;
            }
        }

        /// <summary>
        /// Validates the configuration and attempts to resolve the address.
        /// </summary>
        internal void Validate()
        {
            if (this.UseDistributedLookup)
            {
                this.peerAddress = PeerAddress.Nowhere;
            }
            else
            {
                this.peerAddress = PeerAddress.GetAddress(this.Address);
                if (this.peerAddress == null)
                {
                    // TODO: Record the fact the name resolution failed somewhere that the application can check.
                    this.peerAddress = PeerAddress.Nowhere;
                }
            }
        }
    }
}
