﻿//---------------------------------------------------------------------------------
// <copyright file="OverloadModule.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

using System;
using System.Net;
using System.Xml;
using System.Xml.XPath;

using Badumna.Core;
using Badumna.Utilities;

namespace Badumna
{
    /// <summary>
    /// Configuration options for the overload system.
    /// </summary>
    /// <remarks>
    /// For overload servers set <see cref="IsServer"/> = <c>true</c>.
    /// For overload clients set <see cref="IsClientEnabled"/> = <c>true</c> and (optionally) set <see cref="ServerAddress"/> to the overload server's address.
    /// </remarks>
    public class OverloadModule : IOptionModule
    {
        /// <summary>
        /// The identifying name for this group of configuration options.
        /// </summary>
        internal const string Name = "Overload";

        /// <summary>
        /// A string that must be supplied whenever overload is forced.
        /// </summary>
        private const string ForcedOverloadDisclaimer = "i_understand_this_is_for_testing_only";

        /// <summary>
        /// The resolved <see cref="PeerAddress"/> for the overload server.
        /// </summary>
        /// <remarks>
        /// Iff <see cref="IOptionModule.Validate()"/> has not been called and succeeded then this will be set to <c>null</c>.
        /// If <see cref="IsDistributedLookupUsed"/> is <c>true</c> then this will have the value 
        /// <see cref="PeerAddress.Nowhere"/> after calling <see cref="IOptionModule.Validate()"/>.
        /// </remarks>
        private PeerAddress overloadPeerAddress;

        /// <summary>
        /// The maximum outbound traffic limit in bytes/second before it should switch  
        /// to overload server.
        /// </summary>
        private int outboundTrafficLimitBytesPerSecond;

        /// <summary>
        /// Initializes a new instance of the <see cref="OverloadModule"/> class.
        /// </summary>
        /// <param name="xml">The xml containing the configuration options, or <c>null</c> to use the default options.</param>
        internal OverloadModule(IXPathNavigable xml)
        {
            if (xml != null)
            {
                this.Configure(xml);
            }
        }

        /// <summary>
        /// Gets or sets the overload server address.  Defaults to <c>null</c>.
        /// </summary>
        /// <remarks>
        /// If the value is set to <c>null</c> or the empty string then distributed lookup will
        /// be used to locate overload servers.
        /// </remarks>
        public string ServerAddress { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this peer should forward traffic for
        /// other peers.  Defaults to <c>false</c>.
        /// </summary>
        /// <value><c>true</c> if this peer should forward traffic for overloaded peers; otherwise, <c>false</c>.</value>
        public bool IsServer { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this peer will send its excess
        /// outbound traffic via an overload server.  Defaults to <c>false</c>.
        /// </summary>
        /// <value><c>true</c> if this peer should send excess outbound traffic to an overload server; otherwise, <c>false</c>.</value>
        /// <seealso cref="ServerAddress"/>
        public bool IsClientEnabled { get; set; }

        /// <summary>
        /// Gets a value indicating whether overload is forced on.  Defaults to <c>false</c>.
        /// </summary>
        /// <remarks>
        /// If this property is set to <c>true</c> then all outbound traffic will be sent via
        /// an overload server, regardless of whether this peer is currently overloaded or not.
        /// Forced overload should only be used for testing purposes.
        /// </remarks>
        /// <value><c>true</c> if overload is forced; otherwise, <c>false</c>.</value>
        /// <seealso cref="EnableForcedOverload(string)"/>
        /// <seealso cref="DisableForcedOverload()"/>
        public bool IsForced { get; private set; }

        /// <summary>
        /// Gets or sets the outbound traffic limit in bytes/second before it should switch  
        /// to overload server.
        /// <remarks>
        /// Set this with a resonable value, as this value will be used as a reference whether this peer
        /// considered overloaded and should send its excess outbound traffic via an overload server. This 
        /// is not a hard limit, the actual traffic may still exceed this limit.
        /// </remarks>
        /// </summary>
        public int OutboundTrafficLimitBytesPerSecond 
        {
            get
            {
                if (this.outboundTrafficLimitBytesPerSecond == 0)
                {
                    return int.MaxValue;
                }

                return this.outboundTrafficLimitBytesPerSecond;
            }
            
            set
            {
                this.outboundTrafficLimitBytesPerSecond = value;
            }
        }

        /// <inheritdoc />
        string IOptionModule.ModuleType
        {
            get
            {
                return OverloadModule.Name;
            }
        }

        /// <summary>
        /// Gets a value indicating whether to use distributed lookup to locate overload servers.
        /// </summary>
        /// <remarks>
        /// To enable distributed lookup, set <see cref="ServerAddress"/> to null or the empty string.
        /// </remarks>
        /// <value>
        /// <c>true</c> if distributed lookup should be used; otherwise, <c>false</c>.
        /// </value>
        internal bool IsDistributedLookupUsed
        {
            get
            {
                return string.IsNullOrEmpty(this.ServerAddress);
            }
        }

        /// <summary>
        /// Gets the <see cref="PeerAddress"/> corresponding to <see cref="ServerAddress"/>.
        /// </summary>
        internal PeerAddress OverloadPeerAddress
        {
            get
            {
                if (this.overloadPeerAddress == null)
                {
                    throw new InvalidOperationException("Internal error: Validate must be called first");
                }

                return this.overloadPeerAddress;
            }
        }

        /// <summary>
        /// Enables forced overloading.
        /// </summary>
        /// <param name="testingOnlyAcknowledgement">Must contain the string <c>"i_understand_this_is_for_testing_only"</c>
        /// to indicate acknowledgement that this feature is not intended for use in production.</param>
        public void EnableForcedOverload(string testingOnlyAcknowledgement)
        {
            if (testingOnlyAcknowledgement != OverloadModule.ForcedOverloadDisclaimer)
            {
                throw new ArgumentException("testingOnlyAcknowledgement must contain the string \"" + OverloadModule.ForcedOverloadDisclaimer + "\"");
            }

            this.IsForced = true;
        }

        /// <summary>
        /// Disables forced overloading.
        /// </summary>
        public void DisableForcedOverload()
        {
            this.IsForced = false;
        }

        /// <inheritdoc />
        void IOptionModule.Validate()
        {
            if (this.IsDistributedLookupUsed)
            {
                this.overloadPeerAddress = PeerAddress.Nowhere;
            }
            else
            {
                this.overloadPeerAddress = PeerAddress.GetAddress(this.ServerAddress);
                if (this.overloadPeerAddress == null)
                {
                    // TODO: Record the fact the name resolution failed somewhere that the application can check.
                    this.overloadPeerAddress = PeerAddress.Nowhere;
                }
            }

            if (this.IsServer && this.IsClientEnabled)
            {
                throw new ConfigurationException(
                    ConfigurationException.ErrorCode.Inconsistent,
                    "IsServer and IsClientEnabled can not both be set to true.");
            }

            if (this.OutboundTrafficLimitBytesPerSecond < 0)
            {
                throw new ConfigurationException("MaximumOutboundTrafficBytesPerSecond can not be negative.");
            }

            if (this.IsForced)
            {
                Logger.TraceInformation(LogTag.Configuration, "Forcing Overload: Enabled -- this should *ONLY* be used for testing purpose!");
            }
        }

        /// <inheritdoc />
        void IOptionModule.Save(XmlWriter xmlWriter)
        {
            xmlWriter.WriteElementString("EnableOverload", this.IsClientEnabled ? "enabled" : "disabled");

            if (this.IsForced)
            {
                xmlWriter.WriteElementString("ForceOverload", OverloadModule.ForcedOverloadDisclaimer);
            }

            if (!this.IsDistributedLookupUsed)
            {
                xmlWriter.WriteElementString("OverloadPeer", this.ServerAddress);
            }

            xmlWriter.WriteElementString("AcceptOverload", this.IsServer ? "enabled" : "disabled");
            xmlWriter.WriteElementString("MaximumOutboundTrafficBytesPerSecond", this.OutboundTrafficLimitBytesPerSecond.ToString());
        }

        /// <summary>
        /// Parse any module specific information from the given xml node.
        /// </summary>
        /// <param name="navigable">The XPath node for the derived module as read from the configuration file.</param>
        private void Configure(IXPathNavigable navigable)
        {
            XPathNavigator navigator = navigable.CreateNavigator();

            XPathNavigator enableOverload = navigator.SelectSingleNode("child::EnableOverload");
            if (enableOverload != null)
            {
                this.IsClientEnabled = enableOverload.InnerXml.ToLower() == "enabled";
            }

            XPathNavigator forceOverload = navigator.SelectSingleNode("child::ForceOverload");
            if (forceOverload != null)
            {
                this.IsForced = forceOverload.InnerXml.ToLower() == OverloadModule.ForcedOverloadDisclaimer;
            }

            XPathNavigator overloadPeer = navigator.SelectSingleNode("child::OverloadPeer");
            if (overloadPeer != null)
            {
                this.ServerAddress = overloadPeer.InnerXml;
            }

            XPathNavigator acceptOverload = navigator.SelectSingleNode("child::AcceptOverload");
            if (acceptOverload != null)
            {
                this.IsServer = acceptOverload.InnerXml.ToLower() == "enabled";
            }

            XPathNavigator maximumOutboundTraffic = navigator.SelectSingleNode("child::MaximumOutboundTrafficBytesPerSecond");
            if (maximumOutboundTraffic != null)
            {
                this.OutboundTrafficLimitBytesPerSecond = int.Parse(maximumOutboundTraffic.InnerXml);
            }
        }
    }
}
