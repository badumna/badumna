﻿//---------------------------------------------------------------------------------
// <copyright file="ServiceManager.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

using Badumna.Arbitration;
using Badumna.Core;
using Badumna.Overload;
using Badumna.Utilities;

namespace Badumna.ServiceDiscovery
{
    /// <summary>
    /// The service manager class is used to connect the badumna stack components with the service discovery module. 
    /// </summary>
    internal class ServiceManager
    {
        /// <summary>
        /// Whether there is any announced service.
        /// </summary>
        private bool hasAnnouncedService;

        /// <summary>
        /// The service discovery service.
        /// </summary>
        private ServiceDiscoveryService discoveryService;

        /// <summary>
        /// A list of announced services.
        /// </summary>
        private List<ServiceDescription> announcedServices;

        /// <summary>
        /// A list of interested services.
        /// </summary>
        private List<ServiceDescription> interestedServices;

        /// <summary>
        /// Service consumers for each service description.
        /// </summary>
        private Dictionary<ServiceDescription, IServiceConsumer> serviceConsumer;

        /// <summary>
        /// Last switch host time for each server type.
        /// </summary>
        private Dictionary<ServiceDescription, TimeSpan> lastSwitchHostTime;

        /// <summary>
        /// The regular task used to detect and handle failed service consumers. 
        /// </summary>
        private RegularTask regularTask;

        /// <summary>
        /// Provides access to the current time.
        /// </summary>
        private ITime timeKeeper;

        /// <summary>
        /// The configuration options for the overload system.
        /// </summary>
        private OverloadModule overloadOptions;

        /// <summary>
        /// The configuration options for the arbitration system.
        /// </summary>
        private IArbitrationModule arbitrationOptions;

        /// <summary>
        /// Initializes a new instance of the <see cref="ServiceManager"/> class.
        /// </summary>
        public ServiceManager(
            IArbitrationModule arbitrationOptions, 
            OverloadModule overloadOptions, 
            NetworkEventQueue eventQueue, 
            ITime timeKeeper,
            INetworkConnectivityReporter connectivityReporter)
        {
            this.timeKeeper = timeKeeper;
            this.arbitrationOptions = arbitrationOptions;
            this.overloadOptions = overloadOptions;

            this.announcedServices = new List<ServiceDescription>();
            this.interestedServices = new List<ServiceDescription>();
            this.serviceConsumer = new Dictionary<ServiceDescription, IServiceConsumer>();
            this.lastSwitchHostTime = new Dictionary<ServiceDescription, TimeSpan>();
            this.regularTask = new RegularTask(
                "service_manager_regular_task", 
                TimeSpan.FromSeconds(20), 
                eventQueue, 
                connectivityReporter,
                this.RegularCheckTask);
            this.regularTask.Start();
        }

        /// <summary>
        /// Shutdowns this instance.
        /// </summary>
        public void Shutdown()
        {
            if (null != this.regularTask && this.regularTask.IsRunning)
            {
                this.regularTask.Stop();
            }
        }

        /// <summary>
        /// Announces the specified service.
        /// </summary>
        /// <param name="description">The service description.</param>
        public void AnnounceService(ServiceDescription description)
        {
            this.hasAnnouncedService = true;
            if (null != this.discoveryService)
            {
                this.discoveryService.AnnounceService(description);
            }
            else if (!this.announcedServices.Contains(description))
            {
                this.announcedServices.Add(description);
            }
        }

        /// <summary>
        /// Shutdowns the specified service.
        /// </summary>
        /// <param name="description">The service description.</param>
        public void ShutdownService(ServiceDescription description)
        {
            if (null != this.discoveryService)
            {
                this.discoveryService.ShutdownService(description);
            }
        }

        /// <summary>
        /// Shutdowns all announced services.
        /// </summary>
        public void ShutdownService()
        {
            if (null != this.discoveryService)
            {
                this.discoveryService.ShutdownService();
            }
        }

        /// <summary>
        /// Registers the specified interested service.
        /// </summary>
        /// <param name="description">The service description.</param>
        public void RegisterInterestedService(ServiceDescription description)
        {
            if (null != this.discoveryService)
            {
                this.discoveryService.RegisterInterestedService(description);
            }
            else if (!this.interestedServices.Contains(description))
            {
                this.interestedServices.Add(description);
            }
        }

        /// <summary>
        /// Registers the service consumer.
        /// </summary>
        /// <param name="serviceType">Type of the service.</param>
        /// <param name="consumer">The service consumer.</param>
        public void RegisterServiceConsumer(ServiceDescription serviceType, IServiceConsumer consumer)
        {
            if (this.serviceConsumer.ContainsKey(serviceType))
            {
                throw new InvalidOperationException("Service Consumer already exist for the specified service descritpion.");
            }

            this.serviceConsumer[serviceType] = consumer;
        }

        /// <summary>
        /// Called when service become offline.
        /// </summary>
        /// <param name="serviceType">Type of the service.</param>
        public void OnServiceBecomeOffline(ServiceDescription serviceType)
        {
            this.OnServiceStatusChanged(serviceType);
        }

        /// <summary>
        /// Called when service become available.
        /// </summary>
        /// <param name="serviceType">Type of the service.</param>
        public void OnServiceBecomeAvailable(ServiceDescription serviceType)
        {
            this.OnServiceStatusChanged(serviceType);
        }

        /// <summary>
        /// Starts the service manager.
        /// </summary>
        /// <param name="service">The service.</param>
        public void Start(ServiceDiscoveryService service)
        {
            if (null == service)
            {
                throw new ArgumentException("ServiceDiscoveryService can't be null");
            }

            this.discoveryService = service;

            if (this.interestedServices.Count == 0)
            {
                Logger.TraceInformation(LogTag.ServiceDiscovery, "The local peer doesn't has any interested service registered.");
            }

            foreach (ServiceDescription description in this.interestedServices)
            {
                this.discoveryService.RegisterInterestedService(description);
            }

            if (this.announcedServices.Count == 0)
            {
                Logger.TraceInformation(LogTag.ServiceDiscovery, "The local peer doesn't host any service.");
            }

            foreach (ServiceDescription description in this.announcedServices)
            {
                this.discoveryService.AnnounceService(description);
            }
        }

        /// <summary>
        /// Announces the arbitration service.
        /// </summary>
        public void AnnounceArbitrationService()
        {
            ////if (this.arbitrationOptions.Servers.Count != 1)
            ////{
            ////    throw new ArgumentException("Only one arbitration service can be defined in the configuration file.");
            ////}

            foreach (ArbitrationServerDetails rec in this.arbitrationOptions.Servers)
            {
                if (rec.UseDistributedLookup)
                {
                    ServiceDescription description = new ServiceDescription(ServerType.Arbitration, rec.Name);
                    this.AnnounceService(description);
                    return;
                }
            }
        }

        /// <summary>
        /// Announces the overload service.
        /// </summary>
        public void AnnounceOverloadService()
        {
            if (!this.overloadOptions.IsServer)
            {
                throw new InvalidOperationException("AcceptOverload is not enabled in the badumna configuration.");
            }

            if (this.overloadOptions.IsDistributedLookupUsed)
            {
                ServiceDescription description = new ServiceDescription(ServerType.Overload);
                this.AnnounceService(description);
            }
        }

        /// <summary>
        /// Called when service status changed.
        /// </summary>
        /// <param name="serviceType">Type of the service.</param>
        private void OnServiceStatusChanged(ServiceDescription serviceType)
        { 
            if (!this.serviceConsumer.ContainsKey(serviceType))
            {
                Logger.TraceInformation(LogTag.ServiceDiscovery, "No service consumer is associated with the specified server type.");
                return;
            }

            Logger.TraceInformation(LogTag.ServiceDiscovery, "OnServiceStatusChanged is called.");

            IServiceConsumer consumer = this.serviceConsumer[serviceType];
            ServiceDescription offlineService = consumer.GetOfflineService(serviceType);

            if (null != offlineService)
            {
                Logger.TraceInformation(LogTag.ServiceDiscovery, "An offline service has been identified, going to switch it to a new host.");
                this.SwitchToNewHost(serviceType, consumer);
            }
            else
            {
                Logger.TraceInformation(LogTag.ServiceDiscovery, "No offline service is found. On service status changed notification will be ignored.");
            }
        }

        /// <summary>
        /// Regularly check the availability of services and switches to new host when necessary. 
        /// </summary>
        private void RegularCheckTask()
        {
            Logger.TraceInformation(LogTag.ServiceDiscovery, "Service Manager RegularCheckTask is called.");
            if (this.hasAnnouncedService)
            {
                // this is a server, do nothing
                Logger.TraceInformation(LogTag.ServiceDiscovery, "This peer has announced service, will ignore the regular check.");
                return;
            }

            foreach (KeyValuePair<ServiceDescription, IServiceConsumer> kvp in this.serviceConsumer)
            {
                if (this.lastSwitchHostTime.ContainsKey(kvp.Key))
                {
                    // if we know when was the last time when I switched to a new host, then enforce this
                    // no more than one switch than Parameters.ArbitrationConnectionTimeoutTime rule.
                    TimeSpan lastSwitchTime = this.lastSwitchHostTime[kvp.Key];
                    if (this.timeKeeper.Now - lastSwitchTime > 
                        TimeSpan.FromSeconds(Parameters.ArbitrationConnectionTimeoutTime.TotalSeconds))
                    {
                        if (null != kvp.Value.GetOfflineService(kvp.Key))
                        {
                            this.SwitchToNewHost(kvp.Key, kvp.Value);
                        }
                    }
                }
                else
                {
                    // no idea when was the last time when the local peer switched to a new host, then just switch it to
                    // the new host and record the current time.
                    if (null != kvp.Value.GetOfflineService(kvp.Key))
                    {
                        this.SwitchToNewHost(kvp.Key, kvp.Value);
                    }
                }
            }
        }

        /// <summary>
        /// Queries the specified service.
        /// </summary>
        /// <param name="description">The description.</param>
        /// <returns>The address of the returned service.</returns>
        private PeerAddress QueryService(ServiceDescription description)
        {
            if (this.discoveryService != null)
            {
                return this.discoveryService.QueryService(description);
            }

            return null;
        }

        /// <summary>
        /// Switches to new host.
        /// </summary>
        /// <param name="serviceType">Type of the service.</param>
        /// <param name="consumer">The consumer.</param>
        private void SwitchToNewHost(ServiceDescription serviceType, IServiceConsumer consumer)
        {
            if (this.discoveryService == null)
            {
                Logger.TraceInformation(LogTag.ServiceDiscovery, "Service Discovery Service has been initialized.");
            }

            Logger.TraceInformation(LogTag.ServiceDiscovery, "ServiceManager.SwitchToNewHost is called.");

            PeerAddress address = this.QueryService(serviceType);
            if (null != address)
            {
                Logger.TraceInformation(LogTag.ServiceDiscovery, "Going to call consumer.SwitchToNewHost.");
                consumer.SwitchToNewServiceHost(serviceType, address);
                this.lastSwitchHostTime[serviceType] = this.timeKeeper.Now;
                return;
            }
            else
            {
                Logger.TraceInformation(LogTag.ServiceDiscovery, "Couldn't get any PeerAddress for the specified service type.");
            }
        }
    }
}
