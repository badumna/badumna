﻿//---------------------------------------------------------------------------------
// <copyright file="ServiceRecordStore.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;

using Badumna;
using Badumna.Core;
using Badumna.Security;
using Badumna.Utilities;

namespace Badumna.ServiceDiscovery
{
    /// <summary>
    /// The container class used to store and manage service records.
    /// </summary>
    internal class ServiceRecordStore
    {
        /// <summary>
        /// The dictionary that contains all known service detail records for each service type.
        /// </summary>
        private Dictionary<ServiceType, List<ServiceDetailsRecord>> knownServices;

        /// <summary>
        /// The call back method that is called when the service becomes available. 
        /// </summary>
        private OnServiceBecomeAvailable onServiceBecomeAvailable;

        /// <summary>
        /// Provides access to the current time.
        /// </summary>
        private ITime timeKeeper;

        /// <summary>
        /// Initializes a new instance of the <see cref="ServiceRecordStore"/> class.
        /// </summary>
        public ServiceRecordStore(ITime timeKeeper)
        {
            this.timeKeeper = timeKeeper;
            this.knownServices = new Dictionary<ServiceType, List<ServiceDetailsRecord>>();
            this.Clear();
        }

        /// <summary>
        /// Sets the on service become available delgate.
        /// </summary>
        /// <value>The on service become available delgate.</value>
        public OnServiceBecomeAvailable OnServiceBecomeAvailable
        {
            set
            {
                this.onServiceBecomeAvailable = value;
            }
        }

        /// <summary>
        /// Gets the number of total different service types.
        /// </summary>
        /// <value>The number of service types.</value>
        public int Size
        {
            get
            {
                return this.knownServices.Count;
            }
        }

        /// <summary>
        /// Gets the number of total known records.
        /// </summary>
        /// <value>The number of known records.</value>
        public int Count
        {
            get
            {
                int total = 0;

                foreach (KeyValuePair<ServiceType, List<ServiceDetailsRecord>> kvp in this.knownServices)
                {
                    if (null != kvp.Value)
                    {
                        total = total + kvp.Value.Count;
                    }
                }

                return total;
            }
        }

        /// <summary>
        /// Adds the records to the store.
        /// </summary>
        /// <param name="records">The records.</param>
        public void AddRecords(List<ServiceDetailsRecord> records)
        {
            foreach (ServiceDetailsRecord record in records)
            {
                this.AddRecord(record);
            }
        }

        /// <summary>
        /// Adds the specified record.
        /// </summary>
        /// <param name="record">The record.</param>
        public void AddRecord(ServiceDetailsRecord record)
        {
            bool serviceBecomeAvailable = false;

            List<ServiceDetailsRecord> recordList = null;
            if (!this.knownServices.TryGetValue(record.Type, out recordList))
            {
                recordList = new List<ServiceDetailsRecord>();
                this.knownServices[record.Type] = recordList;
            }

            if (!recordList.Contains(record))
            {
                Logger.TraceInformation(LogTag.ServiceDiscovery, "Adding {0} to the distributed store", record.ServiceAddress);
                recordList.Add(record);
                serviceBecomeAvailable = true;
            }
            else
            {
                // update the experiation time if the existing one in the list is older
                foreach (ServiceDetailsRecord existingRecord in recordList)
                {
                    if (existingRecord.Equals(record) && existingRecord.ExpirationTime < record.ExpirationTime)
                    {
                        existingRecord.ExpirationTime = record.ExpirationTime;
                    }
                }
            }

            // notify the IServiceConsumer
            if (serviceBecomeAvailable)
            {
                Logger.TraceInformation(LogTag.ServiceDiscovery, "A service has become available, notifying the service consumer.");
                this.ServiceBecomeAvailable(record.Type.ServiceDescription);
            }
        }

        /// <summary>
        /// Clears this instance.
        /// </summary>
        public void Clear()
        {
            foreach (ServiceType type in this.knownServices.Keys)
            {
                this.knownServices[type].Clear();
            }

            this.knownServices.Clear();
        }

        /// <summary>
        /// Removes the records.
        /// </summary>
        /// <param name="records">The records.</param>
        public void RemoveRecords(ServiceDetailsRecord[] records)
        {
            foreach (ServiceDetailsRecord record in records)
            {
                this.RemoveRecord(record);
            }
        }

        /// <summary>
        /// Removes the record.
        /// </summary>
        /// <param name="record">The record.</param>
        public void RemoveRecord(ServiceDetailsRecord record)
        {
            List<ServiceDetailsRecord> list = null;
            if (this.knownServices.TryGetValue(record.Type, out list))
            {
                list.Remove(record);

                if (list.Count == 0)
                {
                    this.knownServices.Remove(record.Type);
                }
            }
        }

        /// <summary>
        /// Gets the service records.
        /// </summary>
        /// <param name="serverType">Type of the server.</param>
        /// <returns>The service records for the specified server type.</returns>
        public List<ServiceDetailsRecord> GetServiceRecords(ServerType serverType)
        {
            return this.GetServiceRecords(serverType, "default");
        }

        /// <summary>
        /// Gets the service records.
        /// </summary>
        /// <param name="serverType">Type of the server.</param>
        /// <param name="serviceTypeName">Name of the service.</param>
        /// <returns>A list of known service details records.</returns>
        public List<ServiceDetailsRecord> GetServiceRecords(ServerType serverType, string serviceTypeName)
        {
            // service attribute is always ignored when searching for records in the store.
            List<ServiceDetailsRecord> list = null;
            foreach (KeyValuePair<ServiceType, List<ServiceDetailsRecord>> kvp in this.knownServices)
            {
                if (kvp.Key.ServerType == serverType && kvp.Key.ServiceTypeName.Equals(serviceTypeName))
                {
                    if (null == list)
                    {
                        list = new List<ServiceDetailsRecord>();
                    }

                    list.AddRange(kvp.Value);
                }
            }

            return list;
        }

        /// <summary>
        /// Gets the number of service records of the specified service type.
        /// </summary>
        /// <param name="type">The service type.</param>
        /// <returns>The number of such records.</returns>
        public int GetNumberOfServiceRecords(ServiceType type)
        {
            if (this.knownServices.ContainsKey(type))
            {
                return this.knownServices[type].Count;
            }

            return 0;
        }

        /// <summary>
        /// Garbage collection.
        /// </summary>
        public void GarbageCollection()
        {
            List<ServiceDetailsRecord> toRemove = new List<ServiceDetailsRecord>();
            foreach (KeyValuePair<ServiceType, List<ServiceDetailsRecord>> kvp in this.knownServices)
            {
                List<ServiceDetailsRecord> records = kvp.Value;
                if (null != records && records.Count > 0)
                {
                    toRemove.Clear();
                    TimeSpan now = this.timeKeeper.Now;
                    foreach (ServiceDetailsRecord record in records)
                    {
                        if (now >= record.ExpirationTime)
                        {
                            toRemove.Add(record);
                        }
                    }

                    foreach (ServiceDetailsRecord record in toRemove)
                    {
                        records.Remove(record);
                    }
                }
            }
        }

        /// <summary>
        /// Gets two records for each type.
        /// </summary>
        /// <returns>A list records.</returns>
        public List<ServiceDetailsRecord> GetTwoRecordsEachType()
        {
            List<ServiceDetailsRecord> records = new List<ServiceDetailsRecord>();
            foreach (KeyValuePair<ServiceType, List<ServiceDetailsRecord>> kvp in this.knownServices)
            {
                int count = kvp.Value.Count;
                if (count > 0)
                {
                    if (count <= 2)
                    {
                        records.AddRange(kvp.Value);
                    }
                    else
                    {
                        int randomIndex = RandomSource.Generator.Next(count);
                        records.Add(kvp.Value[randomIndex]);

                        int secondRandomIndex;
                        while ((secondRandomIndex = RandomSource.Generator.Next(count)) != randomIndex)
                        {
                            records.Add(kvp.Value[secondRandomIndex]);
                        }
                    }
                }
            }

            return records;
        }

        /// <summary>
        /// Gets all known records.
        /// </summary>
        /// <returns>All known records.</returns>
        public List<ServiceDetailsRecord> GetAllRecords()
        {
            List<ServiceDetailsRecord> allRecords = new List<ServiceDetailsRecord>();
            foreach (KeyValuePair<ServiceType, List<ServiceDetailsRecord>> kvp in this.knownServices)
            {
                allRecords.AddRange(kvp.Value);
            }

            return allRecords;
        }

        /// <summary>
        /// Call back method that is called when service becomes available.
        /// </summary>
        /// <param name="type">The server type.</param>
        private void ServiceBecomeAvailable(ServiceDescription type)
        {
            if (null != this.onServiceBecomeAvailable)
            {
                this.onServiceBecomeAvailable(type);
            }
        }
    }
}