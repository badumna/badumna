﻿//---------------------------------------------------------------------------------
// <copyright file="ServiceDiscoveryService.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Xml;

using Badumna;
using Badumna.Core;
using Badumna.DataTypes;
using Badumna.DistributedHashTable;
using Badumna.Security;
using Badumna.Utilities;

namespace Badumna.ServiceDiscovery
{
    /// <summary>
    /// This is a wrapper class for the ServiceDiscoveryManager. It employs different selection policies for different
    /// service types. It also gives clients (service consumer) the chance to get different services when possible. 
    /// </summary>
    internal class ServiceDiscoveryService : IServiceDiscovery, IServiceRegistry
    {
        /// <summary>
        /// The service discovery manager.
        /// </summary>
        protected ServiceDiscoveryManager serviceDiscoveryManager;

        /// <summary>
        /// The service selector.
        /// </summary>
        protected ServiceSelector selector;

        /// <summary>
        /// A list of peer addresses that have been previously returned as the results of queries. 
        /// </summary>
        protected Dictionary<ServiceType, List<PeerAddress>> previouslyReturnedRecords;

        /// <summary>
        /// When this module is enabled. Used for testing purpose only. 
        /// </summary>
        private TimeSpan enableTime;

        /// <summary>
        /// Provides access to the current time.
        /// </summary>
        private ITime timeKeeper;

        /// <summary>
        /// Initializes a new instance of the <see cref="ServiceDiscoveryService"/> class.
        /// </summary>
        /// <param name="manager">The service discovery manager.</param>
        /// <param name="timeKeeper">The time source.</param>
        public ServiceDiscoveryService(ServiceDiscoveryManager manager, ITime timeKeeper)
        {
            this.timeKeeper = timeKeeper;
            this.serviceDiscoveryManager = manager;
            this.selector = new ServiceSelector(this.serviceDiscoveryManager);
            this.previouslyReturnedRecords = new Dictionary<ServiceType, List<PeerAddress>>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ServiceDiscoveryService"/> class.
        /// </summary>
        /// <param name="facade">The Dht facade.</param>
        /// <param name="isServiceDiscoveryEnabled">Indicates where service discovery is enabled.</param>
        /// <param name="eventQueue">The event queue to schedule events on.</param>
        /// <param name="timeKeeper">The time source.</param>
        /// <param name="addressProvider">Provides the public peer address.</param>
        /// <param name="connectivityReporter">Reports on network connectivity status.</param>
        /// <param name="tokens">Provides security tokens.</param>
        public ServiceDiscoveryService(DhtFacade facade, bool isServiceDiscoveryEnabled, NetworkEventQueue eventQueue, ITime timeKeeper, INetworkAddressProvider addressProvider, INetworkConnectivityReporter connectivityReporter, TokenCache tokens)
            : this(new ServiceDiscoveryManager(facade, isServiceDiscoveryEnabled, eventQueue, timeKeeper, addressProvider, connectivityReporter, tokens), timeKeeper)
        {
        }

        /// <summary>
        /// Gets or sets the enable time.
        /// </summary>
        /// <value>The enable time.</value>
        public TimeSpan EnableTime
        {
            get { return this.enableTime; }
            set { this.enableTime = value; }
        }

        /// <summary>
        /// Starts this instance.
        /// </summary>
        public void Start()
        {
            this.serviceDiscoveryManager.Start();
            this.enableTime = this.timeKeeper.Now;
        }

        /// <summary>
        /// Sets the on service become available callback.
        /// </summary>
        /// <param name="callback">The callback.</param>
        public void SetOnServiceBecomeAvailableCallback(OnServiceBecomeAvailable callback)
        {
            this.serviceDiscoveryManager.SetOnServiceBecomeAvailableCallback(callback);
        }

        /// <summary>
        /// Shutdowns this instance.
        /// </summary>
        public void Shutdown()
        {
            this.serviceDiscoveryManager.Shutdown();
            this.previouslyReturnedRecords.Clear();
        }

        /// <summary>
        /// Queries the specified service.
        /// </summary>
        /// <param name="description">The service description.</param>
        /// <returns>The returned service.</returns>
        public PeerAddress QueryService(ServiceDescription description)
        {
            ServiceType type = new ServiceType(description);
            ServiceSelectionPolicyType policy = ServiceSelectionPolicy.GetServiceSelectionPolicy(type);
            return this.QueryService(description, policy);
        }

        /// <summary>
        /// Queries the specified service and return one service address by applying the specified selection policy.
        /// </summary>
        /// <param name="description">The service description.</param>
        /// <param name="policy">The service selection policy.</param>
        /// <returns>The returned service.</returns>
        public PeerAddress QueryService(ServiceDescription description, ServiceSelectionPolicyType policy)
        {
            ServiceType type = new ServiceType(description);
            if (!this.serviceDiscoveryManager.ServiceRegistered(type))
            {
                throw new InvalidOperationException("Trying to query a service that is not interested in.");
            }
            
            // get the excluded addresses
            List<PeerAddress> excludes = null;
            excludes = this.GetReturnedRecords(type);
            
            bool hasExclude = false;
            if (null != excludes && excludes.Count > 0)
            {
                hasExclude = true;
            }

            // always try to get different service if possible. 
            ServiceDetailsRecord record = this.selector.QueryService(policy, type, excludes);
            if (null != record)
            {
                this.AddToReturnedRecords(type, record.ServiceAddress);
                return record.ServiceAddress;
            }

            if (hasExclude)
            {
                // couldn't get a different service host, try the one which has already been previously returned to the client.
                // override the policy to be last refreshed.  
                record = this.selector.QueryService(ServiceSelectionPolicyType.LastRefreshed, type, null);
                if (null != record)
                {
                    this.AddToReturnedRecords(type, record.ServiceAddress);
                    return record.ServiceAddress;
                }
            }

            return null;
        }

        /// <summary>
        /// Announce the availability of the specified service.
        /// </summary>
        /// <param name="description">The service description.</param>
        public void AnnounceService(ServiceDescription description)
        {
            ServiceType type = new ServiceType(description);
            this.serviceDiscoveryManager.AnnounceService(type);
        }

        /// <summary>
        /// Register the interested service.
        /// </summary>
        /// <param name="description">The service description.</param>
        public void RegisterInterestedService(ServiceDescription description)
        {
            ServiceType type = new ServiceType(description);
            this.serviceDiscoveryManager.RegisterInterestedService(type);
        }

        /// <summary>
        /// Shutdown the specified service.
        /// </summary>
        /// <param name="description">The service description.</param>
        public void ShutdownService(ServiceDescription description)
        {
            ServiceType type = new ServiceType(description);
            this.serviceDiscoveryManager.ShutdownService(type);
        }

        /// <summary>
        /// Shutdown all registered services.
        /// </summary>
        public void ShutdownService()
        {
            this.serviceDiscoveryManager.ShutdownService();
        }

        /// <summary>
        /// Adds the specified address to the returned records list.
        /// </summary>
        /// <param name="type">The service type.</param>
        /// <param name="address">The address.</param>
        private void AddToReturnedRecords(ServiceType type, PeerAddress address)
        {
            // ignore the attribute
            ServiceType key = new ServiceType(type.ServerType, type.ServiceTypeName);

            List<PeerAddress> records = null;
            if (!this.previouslyReturnedRecords.TryGetValue(key, out records))
            {
                records = new List<PeerAddress>();
                this.previouslyReturnedRecords[key] = records;
            }

            if (!records.Contains(address))
            {
                records.Add(address);
            }
        }

        /// <summary>
        /// Gets the records that have been previously returned.
        /// </summary>
        /// <param name="type">The service type.</param>
        /// <returns>A list of records that have been previously returned.</returns>
        private List<PeerAddress> GetReturnedRecords(ServiceType type)
        {
            // ignore the attribute
            ServiceType key = new ServiceType(type.ServerType, type.ServiceTypeName);
            List<PeerAddress> list = null;
            this.previouslyReturnedRecords.TryGetValue(key, out list);

            return list;
        }
    }

    // this helper class is only used for debugging and simulation purpose. 
    class MockServiceDiscoveryService : ServiceDiscoveryService
    {
        //protected MockServiceDiscoveryManager mServiceDiscoveryManager;
        public MockServiceDiscoveryService(DhtFacade facade, bool isServiceDiscoveryEnabled, NetworkEventQueue eventQueue, ITime timeKeeper, INetworkAddressProvider addressProvider, INetworkConnectivityReporter connectivityReporter, TokenCache tokens)
            : base(new MockServiceDiscoveryManager(facade, isServiceDiscoveryEnabled, eventQueue, timeKeeper, addressProvider, connectivityReporter, tokens), timeKeeper)
        {
        }

        public TimeSpan GetFirstDiscoveryTime
        {
            get { return this.serviceDiscoveryManager.FirstDiscoveryTime; }
        }

        public void AnnounceFakeService(PeerAddress address, ServiceDescription description)
        {
            ServiceType type = new ServiceType(description);
            MockServiceDiscoveryManager mockManager = (MockServiceDiscoveryManager)this.serviceDiscoveryManager;
            mockManager.AnnounceFakeService(address, type);
        }

        public void ForceCheckDht()
        {
            MockServiceDiscoveryManager mockManager = (MockServiceDiscoveryManager)this.serviceDiscoveryManager;
            mockManager.ForceCheckDht();
        }

        public void ForcePopulate()
        {
            MockServiceDiscoveryManager mockManager = (MockServiceDiscoveryManager)this.serviceDiscoveryManager;
            mockManager.ForcePopulate();
        }

        public void ClearMemory()
        {
            this.previouslyReturnedRecords.Clear();
        }
    }
}