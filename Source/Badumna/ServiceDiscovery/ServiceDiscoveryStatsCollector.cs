﻿using System;
using System.Collections.Generic;
using System.Text;

using Badumna.Utilities;
using Badumna.Core;
using Badumna;
using Badumna.DataTypes;
using System.Diagnostics;

namespace Badumna.ServiceDiscovery
{
    /// <summary>
    /// This class is only used when running in simulation mode. 
    /// </summary>
    [DontPerformCoverage]
    class ServiceQueryDetails
    {
        private PeerAddress mAddress;
        public PeerAddress Address { get { return this.mAddress; } }

        private ServiceType mType;
        public ServiceType Type { get { return this.mType; } }

        public ServiceQueryDetails(PeerAddress address, ServiceType type)
        {
            this.mAddress = address;
            this.mType = type;
        }

        public override bool Equals(object obj)
        {
            ServiceQueryDetails queryDetails = obj as ServiceQueryDetails;
            if (null == queryDetails)
            {
                return base.Equals(obj);
            }
            else
            {
                return this.mAddress.Equals(queryDetails.mAddress) && this.mType.Equals(queryDetails.mType);
            }
        }

        public override int GetHashCode()
        {
            return this.mAddress.GetHashCode() | this.mType.GetHashCode();
        }

        public override string ToString()
        {
            return this.mType.ToString() + "_" + this.mAddress.ToString();
        }
    }

    /// <summary>
    /// A null implementation of IServiceDiscoveryStatsCollector that is used for normal operation.
    /// </summary>
    /// <remarks>
    /// The real implementation, ServiceDiscoveryStatsCollector, can be enabled in
    /// ServiceDiscoveryStatsCollector.GetInstance(...) below.
    /// </remarks>
    class NullServiceDiscoveryStatsCollector : IServiceDiscoveryStatsCollector
    {
        public void RecordDhtAccess()
        {
        }

        public void RecordIncomingTraffic(int length, PeerAddress publicAddress)
        {
        }

        public void RecordOutgoingTraffic(int length, PeerAddress publicAddress)
        {
        }

        public void RecordPopulateDelay(TimeSpan enable, TimeSpan unemptiedTime)
        {
        }

        public long GetNumberOfQuery()
        {
            return 0;
        }

        public long GetNumberOfSuccessfulQuery()
        {
            return 0;
        }

        public double GetDhtAccessRate()
        {
            return 0;
        }
    }


    /// <summary>
    /// This stats collector is only used when running in simulation mode. 
    /// </summary>
    [DontPerformCoverage]
    class ServiceDiscoveryStatsCollector : IServiceDiscoveryStatsCollector
    {
        private static IServiceDiscoveryStatsCollector instance;

        /// <summary>
        /// Gets the ServiceDiscoveryStatsCollector instance.  NOT THREAD SAFE.
        /// </summary>
        /// <param name="timeKeeper">The tiem source</param>
        /// <returns>The ServiceDiscoveryStatsCollector instance</returns>
        public static IServiceDiscoveryStatsCollector GetInstance(ITime timeKeeper)
        {
            if (ServiceDiscoveryStatsCollector.instance == null)
            {
#if false
                ServiceDiscoveryStatsCollector.instance = new ServiceDiscoveryStatsCollector(timeKeeper);
#else
                ServiceDiscoveryStatsCollector.instance = new NullServiceDiscoveryStatsCollector();
#endif
            }

            return ServiceDiscoveryStatsCollector.instance;
        }

        private TimeSpan mStartTime;

        protected Dictionary<ServiceDetailsRecord, Stack<TimeSpan>> mAnnouncement;
        protected Dictionary<ServiceQueryDetails, Stack<TimeSpan>> mQueryDetails;
        private Dictionary<ServiceQueryDetails, Stack<TimeSpan>> mSuccessfulQueries;
        private List<TimeSpan> mPopulateDelays;

        private bool trafficStartTimeRecorded;
        private bool trafficEndTimeRecorded;
        private TimeSpan trafficMeasureStartTime;
        private TimeSpan trafficMeasureEndTime;
        private long incomingTrafficBytes;
        private long outgoingTrafficBytes;

        /// <summary>
        /// Used to determine the number of distinct peers in the system.
        /// </summary>
        private Dictionary<PeerAddress, string> peers;

        private List<TimeSpan> mSessionTime;
        private List<TimeSpan> mDownTime;

        private TimeSpan mLastSuccessfulServiceQuery;

        private long mNumberOfDhtAccess;
        private long mNumberOfTotalMessages;

        private int mNetworkSize;

        /// <summary>
        /// Provides access to the current time.
        /// </summary>
        private ITime timeKeeper;

        public ServiceDiscoveryStatsCollector(ITime timeKeeper)
        {
            this.timeKeeper = timeKeeper;
            this.Clear();
        }

        public void Clear()
        {
            this.mNumberOfDhtAccess = 0;
            this.mNumberOfTotalMessages = 0;
            this.mNetworkSize = 0;
            this.mLastSuccessfulServiceQuery = this.timeKeeper.Now;

            this.incomingTrafficBytes = 0;
            this.outgoingTrafficBytes = 0;
            this.trafficEndTimeRecorded = false;
            this.trafficStartTimeRecorded = false;

            this.mStartTime = this.timeKeeper.Now;
            this.mAnnouncement = new Dictionary<ServiceDetailsRecord, Stack<TimeSpan>>();
            this.mQueryDetails = new Dictionary<ServiceQueryDetails, Stack<TimeSpan>>();
            this.mSuccessfulQueries = new Dictionary<ServiceQueryDetails, Stack<TimeSpan>>();
            this.mPopulateDelays = new List<TimeSpan>();
            this.mSessionTime = new List<TimeSpan>();
            this.mDownTime = new List<TimeSpan>();
            this.peers = new Dictionary<PeerAddress, string>();
        }

        public void RecordDhtAccess()
        {
            this.mNumberOfDhtAccess++;
        }

        public void RecordTotalPeerMessage()
        {
            this.mNumberOfTotalMessages++;
        }

        public void RecordSuccessfulServiceQueryTime()
        {
            this.mLastSuccessfulServiceQuery = this.timeKeeper.Now;
        }

        public void RecordNetworkSizeIncrease()
        {
            this.mNetworkSize++;
        }

        public void RecordNetworkSizeDecrease()
        {
            this.mNetworkSize--;
        }

        public int NetworkSize { get { return this.mNetworkSize; } }

        public void RecordSessionTime(TimeSpan start, TimeSpan end)
        {
            if (end < start)
            {
                throw new InvalidOperationException("end < start time for session time.");
            }

            this.mSessionTime.Add(end - start);
        }

        public void RecordDownTime(TimeSpan start, TimeSpan end)
        {
            if (end > start)
            {
                throw new InvalidOperationException("end < start time for down time.");
            }

            this.mDownTime.Add(start - end);
        }

        public void RecordAnnouncement(PeerAddress address, ServiceType type)
        {
            this.RecordAnnouncement(new ServiceDetailsRecord(address, type, this.timeKeeper));
        }

        public void RecordAnnouncement(ServiceDetailsRecord service)
        {
            Stack<TimeSpan> list = null;
            if (!this.mAnnouncement.TryGetValue(service, out list))
            {
                list = new Stack<TimeSpan>();
                this.mAnnouncement[service] = list;
            }

            list.Push(this.timeKeeper.Now);
        }

        public void RecordQuery(PeerAddress address, ServiceType type)
        {
            ServiceQueryDetails query = new ServiceQueryDetails(address, type);
            Stack<TimeSpan> list = null;
            if (!this.mQueryDetails.TryGetValue(query, out list))
            {
                list = new Stack<TimeSpan>();
                this.mQueryDetails[query] = list;
            }

            if (list.Count == 0)
            {
                list.Push(this.timeKeeper.Now);
            }
            else
            {
                TimeSpan mostRecentTime = list.Peek();
                if (this.timeKeeper.Now - mostRecentTime > TimeSpan.FromSeconds(5))
                {
                    list.Push(this.timeKeeper.Now);
                }
                else
                {
                    Logger.TraceInformation(LogTag.ServiceDiscovery, "Issuing two identicial queries within 5 seconds.");
                }
            }
        }

        public void RecordSuccessfulQuery(PeerAddress address, ServiceType type)
        {
            ServiceQueryDetails query = new ServiceQueryDetails(address, type);
            Stack<TimeSpan> list = null;
            if (!this.mSuccessfulQueries.TryGetValue(query, out list))
            {
                list = new Stack<TimeSpan>();
                this.mSuccessfulQueries[query] = list;
            }

            if (list.Count == 0)
            {
                list.Push(this.timeKeeper.Now);
            }
            else
            {
                TimeSpan mostRecentTime = list.Peek();
                if (this.timeKeeper.Now - mostRecentTime > TimeSpan.FromSeconds(5))
                {
                    list.Push(this.timeKeeper.Now);
                }
            }
        }

        public void RecordPopulateDelay(TimeSpan enable, TimeSpan unemptiedTime)
        {
            if (this.timeKeeper.Now - this.mStartTime < TimeSpan.FromSeconds(120))
            {
                // ignore all populate delay in the first two minutes.
                return;
            }

            if (enable < unemptiedTime)
            {
                this.mPopulateDelays.Add(unemptiedTime - enable);
            }
        }

        /// <summary>
        /// Records an incoming packet.
        /// </summary>
        /// <param name="length">The size of the packet in bytes.</param>
        /// <param name="publicAddress">The public address of the receiver, used to distinguish different peers in the simulator.</param>
        public void RecordIncomingTraffic(int length, PeerAddress publicAddress)
        {
            this.UpdateTrafficStatsTimeStamps();
            this.incomingTrafficBytes += length;
            this.peers[publicAddress] = null;
        }

        /// <summary>
        /// Records an outgoing packet.
        /// </summary>
        /// <param name="length">The size of the packet in bytes.</param>
        /// <param name="publicAddress">The public address of the sender, used to distinguish different peers in the simulator.</param>
        public void RecordOutgoingTraffic(int length, PeerAddress publicAddress)
        {
            this.UpdateTrafficStatsTimeStamps();
            this.outgoingTrafficBytes += length;
            this.peers[publicAddress] = null;
        }

        private void UpdateTrafficStatsTimeStamps()
        {
            TimeSpan now = this.timeKeeper.Now;
            if (!this.trafficStartTimeRecorded)
            {
                this.trafficStartTimeRecorded = true;
                this.trafficMeasureStartTime = now;
            }

            if (!this.trafficEndTimeRecorded)
            {
                this.trafficEndTimeRecorded = true;
                this.trafficMeasureEndTime = now;
                return;
            }

            if (this.trafficMeasureEndTime < now)
            {
                this.trafficMeasureEndTime = now;
            }
        }

        private TimeSpan GetAvg(List<TimeSpan> records)
        {
            if (records.Count == 0)
            {
                return TimeSpan.FromSeconds(0);    
            }

            TimeSpan total = TimeSpan.FromSeconds(0);
            foreach (TimeSpan rec in records)
            {
                total += rec;
            }

            long totalTick = total.Ticks;
            return TimeSpan.FromTicks(totalTick / records.Count);
        }

        public TimeSpan GetAvgPopulateDelay()
        {
            return GetAvg(this.mPopulateDelays);
        }

        public TimeSpan GetAvgPeerSessionTime()
        {
            return GetAvg(this.mSessionTime);
        }

        public TimeSpan GetAvgDownTime()
        {
            return GetAvg(this.mDownTime);
        }

        public int GetTotalNumberOfServiceHost()
        {
            List<ServiceDetailsRecord> list;
            list = RemoveDuplicates<ServiceDetailsRecord>(new List<ServiceDetailsRecord>(this.mAnnouncement.Keys));

            return list.Count;
        }

        public long GetNumberOfQuery()
        {
            long total = 0;
            foreach (KeyValuePair<ServiceQueryDetails, Stack<TimeSpan>> kvp in this.mQueryDetails)
            {
                total = total + kvp.Value.Count;
            }

            return total;
        }

        public long GetNumberOfSuccessfulQuery()
        {
            long total = 0;
            foreach (KeyValuePair<ServiceQueryDetails, Stack<TimeSpan>> kvp in this.mSuccessfulQueries)
            {
                total = total + kvp.Value.Count;
            }

            return total;
        }

        public double GetDhtAccessRate()
        {
            double duration = (this.timeKeeper.Now - this.mStartTime).TotalSeconds;
            double dhtAccessRate = (double)this.mNumberOfDhtAccess / (duration * (double)ServiceDiscoveryManager.DHTReplicas);

            return dhtAccessRate;
        }

        public string GetStatsReport()
        {
            // for now, just a very naive one
            long totalQueries = this.GetNumberOfQuery();
            long totalSuccessfulQueries = this.GetNumberOfSuccessfulQuery();
            TimeSpan avgPopulateDelay = this.GetAvgPopulateDelay();

            double duration = (this.timeKeeper.Now - this.mStartTime).TotalSeconds;
            double dhtAccessRate = (double)this.mNumberOfDhtAccess / (duration * (double)ServiceDiscoveryManager.DHTReplicas);

            double per = (double)totalSuccessfulQueries / (double)totalQueries * 100.0;

            int trafficStatsTotalSecs = (int)(this.trafficMeasureEndTime - this.trafficMeasureStartTime).TotalSeconds;
            long totalIncomingTraffic = this.incomingTrafficBytes / trafficStatsTotalSecs;
            long totalOutgoingTraffic = this.outgoingTrafficBytes / trafficStatsTotalSecs;

            if (this.peers.Count > 0)
            {
                totalIncomingTraffic = totalIncomingTraffic / this.peers.Count;
                totalOutgoingTraffic = totalOutgoingTraffic / this.peers.Count;
            }

            string percentage = string.Format("Service Discovery Stats: {0}/{1} ({2}%) queries are successful.", totalSuccessfulQueries, totalQueries, per);
            string populateDelay = string.Format("Avg Populate Delay: {0} seconds", avgPopulateDelay.TotalSeconds);
            string sessionTime = string.Format("Avg Session Time: {0} seconds", this.GetAvgPeerSessionTime(), this.GetAvgDownTime());
            string dhtAccess = string.Format("Avg DHT access per hosting peer per second: {0}", dhtAccessRate);
            string lastSuccessfulQuery = string.Format("Last successful query: {0}", this.mLastSuccessfulServiceQuery.ToString());
            string trafficReport = string.Format("Average Incoming Bytes Per sec : {0}, average outoing bytes per sec : {1}", totalIncomingTraffic, totalOutgoingTraffic);

            string report = percentage + "\n" + populateDelay + "\n" + sessionTime + "\n" + dhtAccess + "\n" + lastSuccessfulQuery + "\n" + trafficReport;

            return report;
        }

        private static List<T> RemoveDuplicates<T>(List<T> original)
        {
            Dictionary<string, T> uniqueStore = new Dictionary<string, T>();
            foreach (T element in original)
            {
                if (!uniqueStore.ContainsKey(element.ToString()))
                {
                    uniqueStore.Add(element.ToString(), element);
                }
            }

            return new List<T>(uniqueStore.Values);
        }
    }
}