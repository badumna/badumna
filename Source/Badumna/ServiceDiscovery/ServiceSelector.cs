﻿//---------------------------------------------------------------------------------
// <copyright file="ServiceSelector.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Diagnostics;

using Badumna.Core;
using Badumna.Utilities;

namespace Badumna.ServiceDiscovery
{
    /// <summary>
    /// The ServiceSelector selects a service host from a list of known service hosts based on the specified policy. 
    /// </summary>
    internal class ServiceSelector
    {
        /// <summary>
        /// The service discovery manager.
        /// </summary>
        private ServiceDiscoveryManager serviceDiscoveryManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="ServiceSelector"/> class.
        /// </summary>
        /// <param name="manager">The service discovery manager.</param>
        public ServiceSelector(ServiceDiscoveryManager manager)
        {
            this.serviceDiscoveryManager = manager;
        }

        /// <summary>
        /// Queries the service.
        /// </summary>
        /// <param name="policy">The selection policy.</param>
        /// <param name="type">The service type.</param>
        /// <param name="excludes">The host addresses that should be excluded.</param>
        /// <returns>A service details record that contains the details or null if no matching record can be 
        /// found.</returns>
        public ServiceDetailsRecord QueryService(
            ServiceSelectionPolicyType policy, 
            ServiceType type, 
            IList<PeerAddress> excludes)
        {
            ServiceDetailsRecord record = null;
            switch (policy)
            {
                case ServiceSelectionPolicyType.Random:
                    record = this.GetRandomService(type, excludes);
                    break;
                case ServiceSelectionPolicyType.FirstAvailable:
                    record = this.GetFirstService(type, excludes);
                    break;
                case ServiceSelectionPolicyType.LastRefreshed:
                    record = this.GetLastRefreshedService(type, excludes);
                    break;
                default:
                    throw new InvalidOperationException("Unknown service selection policy.");
            }

            return record;
        }

        /// <summary>
        /// Gets the random service.
        /// </summary>
        /// <param name="type">The service type.</param>
        /// <param name="excludes">The host addresses that should be excluded.</param>
        /// <returns>A service details record that contains the details or null if no matching record can be 
        /// found.</returns>
        private ServiceDetailsRecord GetRandomService(ServiceType type, IList<PeerAddress> excludes)
        {
            KnownService services = this.serviceDiscoveryManager.QueryService(type);
            services.Exclude(excludes);
            if (services.HasServiceFromCachedQueryReply())
            {
                List<ServiceDetailsRecord> records;
                if (type.HasPreferred)
                {
                    records = this.GetPreferredServices(type, services.ServiceFromCachedQueryReply);
                }
                else
                {
                    records = services.ServiceFromCachedQueryReply;
                }

                return this.GetRandomService(records);
            }

            return null;
        }

        /// <summary>
        /// Gets the random service from the input list of records.
        /// </summary>
        /// <param name="records">The records.</param>
        /// <returns>A random service.</returns>
        private ServiceDetailsRecord GetRandomService(List<ServiceDetailsRecord> records)
        {
            if (records == null || records.Count == 0)
            {
                return null;
            }

            int randomIndex = RandomSource.Generator.Next(records.Count);
            return records[randomIndex];
        }

        /// <summary>
        /// Gets the first known service.
        /// </summary>
        /// <param name="type">The service type.</param>
        /// <param name="excludes">The host addresses that should be excluded.</param>
        /// <returns>A service details record that contains the details or null if no matching record can be 
        /// found.</returns>
        private ServiceDetailsRecord GetFirstService(ServiceType type, IList<PeerAddress> excludes)
        {
            KnownService services = this.serviceDiscoveryManager.QueryService(type);
            services.Exclude(excludes);
            if (services.HasServiceFromCachedQueryReply())
            {
                List<ServiceDetailsRecord> records;
                if (type.HasPreferred)
                {
                    records = this.GetPreferredServices(type, services.ServiceFromCachedQueryReply);
                }
                else
                {
                    records = services.ServiceFromCachedQueryReply;
                }

                return this.GetFirstService(records);
            }

            return null;
        }

        /// <summary>
        /// Gets the first service.
        /// </summary>
        /// <param name="records">The input records.</param>
        /// <returns>The first record in the input records.</returns>
        private ServiceDetailsRecord GetFirstService(List<ServiceDetailsRecord> records)
        {
            if (records == null || records.Count == 0)
            {
                return null;
            }

            return records[0];
        }

        /// <summary>
        /// Gets the last refreshed service.
        /// </summary>
        /// <param name="type">The service type.</param>
        /// <param name="excludes">The host addresses that should be excluded.</param>
        /// <returns>A service details record that contains the details or null if no matching record can be 
        /// found.</returns>
        private ServiceDetailsRecord GetLastRefreshedService(ServiceType type, IList<PeerAddress> excludes)
        {
            KnownService services = this.serviceDiscoveryManager.QueryService(type);
            services.Exclude(excludes);
            if (services.HasServiceFromCachedQueryReply())
            {
                List<ServiceDetailsRecord> records;
                if (type.HasPreferred)
                {
                    records = this.GetPreferredServices(type, services.ServiceFromCachedQueryReply);
                }
                else
                {
                    records = services.ServiceFromCachedQueryReply;
                }

                return this.GetLastRefreshedService(records);
            }

            return null;
        }

        /// <summary>
        /// Gets the last refreshed service.
        /// </summary>
        /// <param name="records">A list of records.</param>
        /// <returns>The record that is most recently refreshed.</returns>
        private ServiceDetailsRecord GetLastRefreshedService(List<ServiceDetailsRecord> records)
        {
            if (records == null || records.Count == 0)
            {
                return null;
            }

            TimeSpan mostRecentRefreshTime = records[0].ExpirationTime;
            int resultIndex = 0;

            for (int i = 0; i < records.Count; i++)
            {
                if (mostRecentRefreshTime < records[i].ExpirationTime)
                {
                    mostRecentRefreshTime = records[i].ExpirationTime;
                    resultIndex = i;
                }
            }

            return records[resultIndex];
        }

        /// <summary>
        /// Gets the preferred services.
        /// </summary>
        /// <param name="type">The service type.</param>
        /// <param name="knownServices">The known services.</param>
        /// <returns>A list of preferred services.</returns>
        private List<ServiceDetailsRecord> GetPreferredServices(
            ServiceType type, 
            List<ServiceDetailsRecord> knownServices)
        {
            if (!type.HasPreferred)
            {
                return null;
            }

            List<ServiceDetailsRecord> preferred = new List<ServiceDetailsRecord>();
            foreach (ServiceDetailsRecord rec in knownServices)
            {
                // type need to be exactly the same, including the server type, service type and the attribute.
                if (rec.Type.Equals(type))
                {
                    preferred.Add(rec);
                }
            }

            if (preferred.Count > 0)
            {
                return preferred;
            }
            else
            {
                return null;
            }
        }
    }
}