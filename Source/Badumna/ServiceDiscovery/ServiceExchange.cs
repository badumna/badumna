﻿//---------------------------------------------------------------------------------
// <copyright file="ServiceExchange.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;

using Badumna;
using Badumna.Core;
using Badumna.DataTypes;
using Badumna.DistributedHashTable;
using Badumna.Transport;
using Badumna.Utilities;

namespace Badumna.ServiceDiscovery
{
    /// <summary>
    /// The ServiceExchangeManager is used to request neighbouring peers to send their known service hosts. 
    /// It is different from the gossip feature used in the ServiceDiscoveryManager in which local peer sends
    /// their known service host information to neighbours. 
    /// </summary>
    internal class ServiceExchangeManager : DhtProtocol
    {
        /// <summary>
        /// The Dht facade.
        /// </summary>
        private DhtFacade facade;

        /// <summary>
        /// The service discovery manager.
        /// </summary>
        private ServiceDiscoveryManager serviceDiscoveryManager;

        /// <summary>
        /// Indicates whether service discovery is enabled.
        /// </summary>
        private bool isServiceDiscoveryEnabled;

        /// <summary>
        /// Provides the public peer address.
        /// </summary>
        private INetworkAddressProvider addressProvider;

        /// <summary>
        /// Hacky way to collect service discovery stats in the simulator.  Multiple peers share the
        /// same instance of the stats collector.
        /// </summary>
        protected IServiceDiscoveryStatsCollector statsCollector;
        
        /// <summary>
        /// Initializes a new instance of the <see cref="ServiceExchangeManager"/> class.
        /// </summary>
        /// <param name="facade">The Dht facade.</param>
        /// <param name="serviceDiscoveryManager">The service discovery manager.</param>
        /// <param name="addressProvider">Provides the current public address.</param>
        /// <param name="isServiceDiscoveryEnabled">Indicates whether service discovery is enabled.</param>
        /// <param name="statsCollector">Hacky way to collect service discovery stats in the simulator.  Multiple peers share the
        /// same instance of the stats collector.</param>
        public ServiceExchangeManager(
            DhtFacade facade, 
            ServiceDiscoveryManager serviceDiscoveryManager,
            INetworkAddressProvider addressProvider,
            bool isServiceDiscoveryEnabled,
            IServiceDiscoveryStatsCollector statsCollector)
            : base(facade)
        {
            this.facade = facade;
            this.serviceDiscoveryManager = serviceDiscoveryManager;
            this.addressProvider = addressProvider;
            this.isServiceDiscoveryEnabled = isServiceDiscoveryEnabled;
            this.statsCollector = statsCollector;
        }

        /// <summary>
        /// Performs the service exchange.
        /// </summary>
        public void ServiceExchange()
        {
            if (!this.isServiceDiscoveryEnabled)
            {
                return;
            }

            NodeInfo[] nodes = this.facade.GetRoutingTableNodes();
            if (null == nodes || nodes.Length == 0)
            {
                return;
            }

            int expected = nodes.Length > Parameters.ServiceDiscoveryMaxNumberOfNeighboursToExchangeInfo ? 
                Parameters.ServiceDiscoveryMaxNumberOfNeighboursToExchangeInfo : nodes.Length;
            int[] set = Utils.GetRandomSet(nodes.Length, expected);

            NodeInfo[] newNodeSet = new NodeInfo[expected];
            for (int i = 0; i < expected; i++)
            {
                newNodeSet[i] = nodes[set[i]];
            }

            foreach (NodeInfo node in newNodeSet)
            {
                if (null == node)
                {
                    continue;
                }

                DhtEnvelope envelope = this.GetMessageFor(node.Address, QualityOfService.Unreliable);
                this.RemoteCall(envelope, this.RequestServiceExchange);
                this.statsCollector.RecordOutgoingTraffic(envelope.Length, this.addressProvider.PublicAddress);
                this.SendMessage(envelope);
            }
        }

        /// <summary>
        /// Gossips the service details with GossipNumberOfNeighbours random neighbours. 
        /// </summary>
        /// <param name="source">if set to <c>true</c>, meaning the local peer just got refreshed by the service host, 
        /// it will exchange all records contained in its distributed store. when set to <c>false</c>, two records for 
        /// each type randomly selected from the cached query replies will be exchanged with neighbours.</param>
        public void GossipServiceDetails(bool source)
        {
            List<ServiceDetailsRecord> records;
            if (source)
            {
                records = this.serviceDiscoveryManager.DistributedStore.GetAllRecords();
            }
            else
            {
                records = this.serviceDiscoveryManager.CachedRecords.GetTwoRecordsEachType();
            }

            if (null == records || records.Count == 0)
            {
                return;
            }

            // get no more than GossipNumberOfNeighbours random neighbours
            NodeInfo[] nodes = this.facade.GetRoutingTableNodes();
            if (null == nodes || nodes.Length == 0)
            {
                return;
            }

            int number = nodes.Length > Parameters.ServiceDiscoveryGossipNumberOfNeighbours ? 
                Parameters.ServiceDiscoveryGossipNumberOfNeighbours : nodes.Length;
            int[] set = Utils.GetRandomSet(nodes.Length, number);

            NodeInfo[] newNodeSet = new NodeInfo[number];
            for (int i = 0; i < number; i++)
            {
                newNodeSet[i] = nodes[set[i]];
            }

            foreach (NodeInfo node in newNodeSet)
            {
                if (node != null)
                {
                    // source of gossip will be sent via reliable communication
                    this.GossipServiceDetails(node.Address, records, source, false);
                }
            }

            // make sure the immediate neighbours will always get the source records.
            NodeInfo[] immediateNeighbours = this.facade.GetImmediateNeighbours();
            if (source && immediateNeighbours != null)
            {
                foreach (NodeInfo node in immediateNeighbours)
                {
                    if (node != null)
                    {
                        this.GossipServiceDetails(node.Address, records, source, false);
                    }
                }
            }
        }

        /// <summary>
        /// Gossips the service details.
        /// </summary>
        /// <param name="address">The address of peer to gossip with.</param>
        /// <param name="records">The records.</param>
        /// <param name="reliable">Use reliable communication if set to <c>true</c>.</param>
        /// <param name="welcome">Send the welcome information when set to <c>true</c>.</param>
        public void GossipServiceDetails(
            PeerAddress address, 
            List<ServiceDetailsRecord> records, 
            bool reliable, 
            bool welcome)
        {
            DhtEnvelope envelope;
            if (!reliable)
            {
                envelope = this.GetMessageFor(address, QualityOfService.Unreliable);
            }
            else
            {
                envelope = this.GetMessageFor(address, QualityOfService.Reliable);
            }

            this.RemoteCall(envelope, this.HandleGossipMessage, records, welcome);
            this.statsCollector.RecordOutgoingTraffic(envelope.Length, this.addressProvider.PublicAddress);
            this.SendMessage(envelope);
        }

        /// <summary>
        /// Handles the gossip message.
        /// </summary>
        /// <param name="records">The service details records.</param>
        /// <param name="welcome">send welcome information if set to <c>true</c>.</param>
        [DhtProtocolMethod(DhtMethod.ServiceDiscoveryHandleGossipMessage)]
        public void HandleGossipMessage(List<ServiceDetailsRecord> records, bool welcome)
        {
            Logger.TraceInformation(LogTag.ServiceDiscovery, "ServiceDiscoveryManager HandleGossipMessage is called, records.Count = {0}", records.Count);
            this.statsCollector.RecordIncomingTraffic(this.CurrentEnvelope.Length, this.addressProvider.PublicAddress);
            bool wasEmpty = this.serviceDiscoveryManager.CachedRecords.Count == 0;
            this.serviceDiscoveryManager.CachedRecords.AddRecords(records);
            bool nowEmpty = this.serviceDiscoveryManager.CachedRecords.Count == 0;

            if (wasEmpty && !nowEmpty)
            {
                this.serviceDiscoveryManager.OnCachedRecordsUnemptied();
            }
        }

        /// <summary>
        /// Requests the service exchange.
        /// </summary>
        [DhtProtocolMethod(DhtMethod.ServiceDiscoveryRequestServiceExchange)]
        private void RequestServiceExchange()
        {
            this.statsCollector.RecordIncomingTraffic(this.CurrentEnvelope.Length, this.addressProvider.PublicAddress);
            List<ServiceDetailsRecord> list = new List<ServiceDetailsRecord>();
            foreach (ServiceType type in this.serviceDiscoveryManager.InterestedServices)
            {
                ServiceDetailsRecord[] records = this.serviceDiscoveryManager.GetKnownServiceHosts(type);
                if (null == records || records.Length == 0)
                {
                    continue;
                }

                int randomIndex = RandomSource.Generator.Next(records.Length);
                list.Add(records[randomIndex]);
            }

            if (list.Count > 0)
            {
                DhtEnvelope envelope = this.GetMessageFor(this.CurrentEnvelope.Source, QualityOfService.Unreliable);
                this.RemoteCall(envelope, this.ServiceExchangeReply, list);
                this.statsCollector.RecordOutgoingTraffic(envelope.Length, this.addressProvider.PublicAddress);
                this.SendMessage(envelope);
            }
        }

        /// <summary>
        /// Handles the service exchange reply.
        /// </summary>
        /// <param name="list">The returned list of service details record.</param>
        [DhtProtocolMethod(DhtMethod.ServiceDiscoveryServiceExchangeReply)]
        private void ServiceExchangeReply(List<ServiceDetailsRecord> list)
        {
            this.statsCollector.RecordIncomingTraffic(this.CurrentEnvelope.Length, this.addressProvider.PublicAddress);
            this.serviceDiscoveryManager.CachedRecords.AddRecords(list);
        }
    }
}