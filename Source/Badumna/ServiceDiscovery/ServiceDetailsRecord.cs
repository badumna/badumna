﻿//---------------------------------------------------------------------------------
// <copyright file="ServiceDetailsRecord.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

using Badumna;
using Badumna.Core;
using Badumna.DataTypes;
using Badumna.DistributedHashTable;
using Badumna.Security;
using Badumna.Utilities;

namespace Badumna.ServiceDiscovery
{
    /// <summary>
    /// The server types.
    /// </summary>
    public enum ServerType : byte
    {
        /// <summary>
        /// The default value used internally.
        /// </summary>
        None = 0,

        /// <summary>
        /// Arbitration server.
        /// </summary>
        Arbitration,

        /// <summary>
        /// Overload server.
        /// </summary>
        Overload    
    }

    /// <summary>
    /// This class is used to describe the service. 
    /// </summary>
    public class ServiceDescription : IEquatable<ServiceDescription>
    {
        /// <summary>
        /// The server type, e.g. Arbitration or HttpTunnel servers. 
        /// </summary>
        private ServerType serverType;

        /// <summary>
        /// The service type, e.g. Arbitration server for movement, Arbitration server for guestbook, the Overload server for avatar updates.
        /// </summary>
        private string serviceType;

        /// <summary>
        /// Attribute of the service, e.g. the server for peers located in US. 
        /// </summary>
        private string attribute;

        /// <summary>
        /// Initializes a new instance of the <see cref="ServiceDescription"/> class.
        /// </summary>
        public ServiceDescription()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ServiceDescription"/> class.
        /// </summary>
        /// <param name="serverType">Type of the server.</param>
        public ServiceDescription(ServerType serverType)
            : this(serverType, Parameters.ServiceDiscoveryDefaultString, Parameters.ServiceDiscoveryDefaultString)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ServiceDescription"/> class.
        /// </summary>
        /// <param name="serverType">Type of the server.</param>
        /// <param name="serviceType">Type of the service.</param>
        public ServiceDescription(ServerType serverType, string serviceType)
            : this(serverType, serviceType, Parameters.ServiceDiscoveryDefaultString)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ServiceDescription"/> class.
        /// </summary>
        /// <param name="serverType">Type of the server.</param>
        /// <param name="serviceType">Type of the service.</param>
        /// <param name="attribute">The attribute.</param>
        public ServiceDescription(ServerType serverType, string serviceType, string attribute)
        {
            Debug.Assert(null != serviceType && null != attribute, "service type and attribute can't be null.");
            this.serverType = serverType;
            this.serviceType = serviceType;
            this.attribute = attribute;
        }

        /// <summary>
        /// Gets a value indicating whether this instance is prefered.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is prefered; otherwise, <c>false</c>.
        /// </value>
        public bool HasPreferred
        {
            get
            {
                return !this.attribute.Equals(Parameters.ServiceDiscoveryDefaultString);
            }
        }
        
        /// <summary>
        /// Gets or sets the type of the server.
        /// </summary>
        /// <value>The type of the server.</value>
        public ServerType ServerType 
        {
            get { return this.serverType; } 
            set { this.serverType = value; }
        }
        
        /// <summary>
        /// Gets or sets the type of the service.
        /// </summary>
        /// <value>The type of the service.</value>
        public string ServiceType
        {
            get { return this.serviceType; }
            set { this.serviceType = value; }
        }
        
        /// <summary>
        /// Gets or sets the attribute.
        /// </summary>
        /// <value>The attribute.</value>
        public string Attribute
        {
            get { return this.attribute; }
            set { this.attribute = value; }
        }

        #region IEquatable<ServiceDescription> Members

        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// <param name="other">An object to compare with this object.</param>
        /// <returns>
        /// true if the current object is equal to the <paramref name="other"/> parameter; otherwise, false.
        /// </returns>
        public bool Equals(ServiceDescription other)
        {
            if (this.ServerType == other.ServerType && this.ServiceType.Equals(other.ServiceType) && this.Attribute.Equals(other.Attribute))
            {
                return true;
            }

            return false;
        }

        #endregion

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
        /// </returns>
        public override int GetHashCode()
        {
            int hash = (byte)this.ServerType;
            hash += this.ServiceType.GetHashCode();
            hash += this.Attribute.GetHashCode();

            return hash;
        }
    }

    /// <summary>
    /// Wrapper of the service description. 
    /// </summary>
    internal class ServiceType : IParseable, IEquatable<ServiceType>
    {
        /// <summary>
        /// The service description.
        /// </summary>
        private ServiceDescription description;

        /// <summary>
        /// Initializes a new instance of the <see cref="ServiceType"/> class.
        /// </summary>
        public ServiceType()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ServiceType"/> class.
        /// </summary>
        /// <param name="description">The service description.</param>
        public ServiceType(ServiceDescription description)
        {
            this.description = description;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ServiceType"/> class.
        /// </summary>
        /// <param name="serverType">Type of the server.</param>
        public ServiceType(ServerType serverType)
            : this(serverType, Parameters.ServiceDiscoveryDefaultString, Parameters.ServiceDiscoveryDefaultString)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ServiceType"/> class.
        /// </summary>
        /// <param name="serverType">Type of the server.</param>
        /// <param name="serviceTypeName">Name of the service type.</param>
        public ServiceType(ServerType serverType, string serviceTypeName)
            : this(serverType, serviceTypeName, Parameters.ServiceDiscoveryDefaultString)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ServiceType"/> class.
        /// </summary>
        /// <param name="serverType">Type of the server.</param>
        /// <param name="serviceTypeName">Name of the service type.</param>
        /// <param name="attribute">The attribute.</param>
        public ServiceType(ServerType serverType, string serviceTypeName, string attribute)
        {
            this.description = new ServiceDescription();
            this.description.ServerType = serverType;
            this.description.ServiceType = serviceTypeName;
            this.description.Attribute = attribute;
        }

        /// <summary>
        /// Gets the service description.
        /// </summary>
        /// <value>The service description.</value>
        public ServiceDescription ServiceDescription
        {
            get { return this.description; }
        }

        /// <summary>
        /// Gets the type of the server.
        /// </summary>
        /// <value>The type of the server.</value>
        public ServerType ServerType 
        { 
            get { return this.description.ServerType; } 
        }
        
        /// <summary>
        /// Gets the name of the service type.
        /// </summary>
        /// <value>The name of the service type.</value>
        public string ServiceTypeName 
        { 
            get { return this.description.ServiceType; } 
        }
        
        /// <summary>
        /// Gets the attribute.
        /// </summary>
        /// <value>The attribute.</value>
        public string Attribute 
        { 
            get { return this.description.Attribute; } 
        }

        /// <summary>
        /// Gets a value indicating whether this instance has preferred attribute set.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has preferred attribute set; otherwise, <c>false</c>.
        /// </value>
        public bool HasPreferred
        {
            get { return this.description.HasPreferred; }
        }

        #region IParseable Members

        /// <summary>
        /// Serialize the object to the message buffer.
        /// </summary>
        /// <param name="message">The message buffer.</param>
        /// <param name="parameterType">Type of the parameter.</param>
        public void ToMessage(MessageBuffer message, Type parameterType)
        {
            message.Write((byte)this.description.ServerType);
            message.Write(this.description.ServiceType);
            message.Write(this.description.Attribute);
        }

        /// <summary>
        /// Deserialize the object from the message buffer.
        /// </summary>
        /// <param name="message">The message buffer.</param>
        public void FromMessage(MessageBuffer message)
        {
            this.description = new ServiceDescription();
            this.description.ServerType = (ServerType)message.ReadByte();
            this.description.ServiceType = message.ReadString();
            this.description.Attribute = message.ReadString();
        }

        #endregion

        #region IEquatable<ServiceType> Members

        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// <param name="other">An object to compare with this object.</param>
        /// <returns>
        /// true if the current object is equal to the <paramref name="other"/> parameter; otherwise, false.
        /// </returns>
        public bool Equals(ServiceType other)
        {
            if (null == other)
            {
                return false;
            }

            return this.description.Equals(other.description);
        }

        #endregion

        /// <summary>
        /// Returns a <see cref="System.String"/> that will be used to generate hash key.
        /// </summary>
        /// <returns>A hash key string.</returns>
        public string ToHashKeyString()
        {
            string serverType = Enum.GetName(typeof(ServerType), this.description.ServerType);
            string str = serverType + "_" + this.description.ServiceType;

            return str;
        }

        /// <summary>
        /// Returns a <see cref="System.String"/> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String"/> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            string serverType = Enum.GetName(typeof(ServerType), this.description.ServerType);
            string str = serverType + "_" + this.description.ServiceType + "_" + this.description.Attribute;

            return str;
        }

        /// <summary>
        /// Determines whether the specified <see cref="System.Object"/> is equal to this instance.
        /// </summary>
        /// <param name="obj">The <see cref="System.Object"/> to compare with this instance.</param>
        /// <returns>
        /// <c>true</c> if the specified <see cref="System.Object"/> is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        /// <exception cref="T:System.NullReferenceException">The <paramref name="obj"/> parameter is null.</exception>
        public override bool Equals(object obj)
        {
            ServiceType serviceType = obj as ServiceType;
            if (null == serviceType)
            {
                return false;
            }

            return this.Equals(serviceType);
        }

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
        /// </returns>
        public override int GetHashCode()
        {
            int hash = (byte)this.description.ServerType;
            hash += this.description.ServiceType.GetHashCode();
            hash += this.description.Attribute.GetHashCode();

            return hash;
        }
    }

    /// <summary>
    /// The service details.
    /// </summary>
    internal class ServiceDetailsRecord : IParseable
    {
        /// <summary>
        /// The address of the service.
        /// </summary>
        private PeerAddress serviceAddress;

        /// <summary>
        /// The expiration time of this record. 
        /// </summary>
        private TimeSpan expirationTime;

        /// <summary>
        /// The service type.
        /// </summary>
        private ServiceType type;

        /// <summary>
        /// Provides access to the current time.
        /// </summary>
        private ITime timeKeeper;

        /// <summary>
        /// Initializes a new instance of the <see cref="ServiceDetailsRecord"/> class. This constructor is only used when FromMessage is
        /// to be immediately used.
        /// </summary>
        /// <param name="timeKeeper">The time source.</param>
        public ServiceDetailsRecord(ITime timeKeeper)
            : this(PeerAddress.Nowhere, new ServiceType(ServerType.None), timeKeeper)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ServiceDetailsRecord"/> class.
        /// </summary>
        /// <param name="address">The address.</param>
        /// <param name="type">The service type.</param>
        /// <param name="timeKeeper">The time source.</param>
        public ServiceDetailsRecord(PeerAddress address, ServiceType type, ITime timeKeeper)
        {
            this.timeKeeper = timeKeeper;
            this.serviceAddress = address;
            this.type = type;
            this.expirationTime = this.timeKeeper.Now + Parameters.ServiceDiscoveryDHTRecordTTL;
        }

        /// <summary>
        /// Gets or sets the service address.
        /// </summary>
        /// <value>The service address.</value>
        public PeerAddress ServiceAddress
        {
            get { return this.serviceAddress; }
            set { this.serviceAddress = value; }
        }

        /// <summary>
        /// Gets or sets the expiration time. 
        /// </summary>
        /// <value>The expiration time.</value>
        public TimeSpan ExpirationTime
        {
            get { return this.expirationTime; }
            set { this.expirationTime = value; }
        }

        /// <summary>
        /// Gets a value indicating whether the record should be refreshed. For cached record, we refresh them every ServiceDiscoveryManager.CachedRecordRefreshInterval 
        /// minutes.
        /// </summary>
        /// <value><c>true</c> if [should refresh]; otherwise, <c>false</c>.</value>
        public bool ShouldRefresh
        {
            get { return this.expirationTime - this.timeKeeper.Now < Parameters.ServiceDiscoveryCachedRecordRefreshInterval; }
        }

        /// <summary>
        /// Gets a value indicating whether the service is [considered as offline].
        /// </summary>
        /// <value><c>true</c> if [considered as offline]; otherwise, <c>false</c>.</value>
        public bool ConsideredAsOffline
        {
            get { return this.expirationTime <= this.timeKeeper.Now; }
        }
        
        /// <summary>
        /// Gets or sets the service type.
        /// </summary>
        /// <value>The service type.</value>
        public ServiceType Type
        {
            get { return this.type; }
            set { this.type = value; }
        }

        /// <summary>
        /// Serialize the object to the message buffer. 
        /// </summary>
        /// <param name="message">The message buffer.</param>
        /// <param name="parameterType">Type of the parameter.</param>
        public void ToMessage(MessageBuffer message, Type parameterType)
        {
            if (null != message)
            {
                message.Write(this.serviceAddress);
                this.type.ToMessage(message, parameterType);
                int ttl = (int)Math.Max(0, (int)(this.expirationTime - this.timeKeeper.Now).TotalSeconds);
                ttl = Math.Min(ttl, (int)Parameters.ServiceDiscoveryDHTRecordTTL.TotalSeconds);
                message.Write(ttl);
            }
        }

        /// <summary>
        /// Deserialize the object from the message buffer.
        /// </summary>
        /// <param name="message">The message buffer.</param>
        public void FromMessage(MessageBuffer message)
        {
            if (null != message)
            {
                this.serviceAddress = message.Read<PeerAddress>();
                this.type = new ServiceType();
                this.type.FromMessage(message);
                Debug.Assert(this.type.ServerType != ServerType.None, "Server type can't be None in service details record.");
                int ttl = message.ReadInt();
                ttl = Math.Max(ttl, 0);
                ttl = Math.Min(ttl, (int)Parameters.ServiceDiscoveryDHTRecordTTL.TotalSeconds);
                this.expirationTime = this.timeKeeper.Now + TimeSpan.FromSeconds(ttl);
            }
        }

        /// <summary>
        /// Returns a <see cref="System.String"/> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String"/> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return this.type.ToString() + "_" + this.serviceAddress.ToString();
        }

        /// <summary>
        /// Determines whether the specified <see cref="System.Object"/> is equal to this instance.
        /// </summary>
        /// <param name="obj">The <see cref="System.Object"/> to compare with this instance.</param>
        /// <returns><c>true</c> if the specified <see cref="System.Object"/> is equal to this instance; otherwise, <c>false</c></returns>
        public override bool Equals(object obj)
        {
            ServiceDetailsRecord other = obj as ServiceDetailsRecord;
            if (null == other)
            {
                return false;
            }

            return this.serviceAddress.Equals(other.serviceAddress) && this.type.Equals(other.type);
        }

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
        /// </returns>
        public override int GetHashCode()
        {
            return this.serviceAddress.GetHashCode() ^ this.type.GetHashCode();
        }
    }
}