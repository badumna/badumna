﻿//---------------------------------------------------------------------------------
// <copyright file="IServiceDiscovery.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;

using Badumna.Core;

namespace Badumna.ServiceDiscovery
{
    /// <summary>
    /// The call back method that will be called when a service (e.g. Overload server) becomes offline. 
    /// </summary>
    /// <param name="type">The server type</param>
    internal delegate void OnServiceBecomeOffline(ServiceDescription type);

    /// <summary>
    /// The call back method that will be called when the service discovery module locally caches a new service record. 
    /// </summary>
    /// <param name="type">The server type</param>
    internal delegate void OnServiceBecomeAvailable(ServiceDescription type);

    /// <summary>
    /// Trusted peers or dedicated servers used for ruuning the overload/arbitration services are regarded as service providers.
    /// They use the IServiceRegistry interface to register or remove their services. 
    /// </summary>
    internal interface IServiceRegistry
    {
        /// <summary>
        /// Announce the availability of the specified service. 
        /// </summary>
        /// <param name="description">The description.</param>
        void AnnounceService(ServiceDescription description);
        
        /// <summary>
        /// Shutdown the specified service.
        /// </summary>
        /// <param name="description">The description.</param>
        void ShutdownService(ServiceDescription description);
       
        /// <summary>
        /// Shutdown all registered services.
        /// </summary>
        void ShutdownService();
    }

    /// <summary>
    /// Untrusted user peers consume services provided by the above service provider. 
    /// The service discovery interface allows peers to query required services. 
    /// </summary>
    internal interface IServiceDiscovery
    {
        /// <summary>
        /// Register the interested service.
        /// </summary>
        /// <param name="description">The description.</param>
        void RegisterInterestedService(ServiceDescription description);
        
        /// <summary>
        /// Queries the specified service
        /// </summary>
        /// <param name="description">The description.</param>
        /// <returns>The returned service.</returns>
        PeerAddress QueryService(ServiceDescription description);

        /// <summary>
        /// Queries the specified service and return one service address by applying the specified selection policy.
        /// </summary>
        /// <param name="description">The service description.</param>
        /// <param name="policy">The service selection policy.</param>
        /// <returns>The returned service.</returns>
        PeerAddress QueryService(ServiceDescription description, ServiceSelectionPolicyType policy);
    }

    /// <summary>
    /// The IServiceConsumer interface is suppose to be implemented by the module that actually controls the service. 
    /// ArbitrationManager and EntityManager are two current examples. 
    /// </summary>
    internal interface IServiceConsumer
    {
        /// <summary>
        /// Gets the offline service for the specified service type.
        /// </summary>
        /// <param name="description">The service description.</param>
        /// <returns>The service description of the offline service, or null if all services are available.</returns>
        ServiceDescription GetOfflineService(ServiceDescription description);

        /// <summary>
        /// Switches to a new service host.
        /// </summary>
        /// <param name="description">The service description.</param>
        /// <param name="address">The address of the new service host.</param>
        void SwitchToNewServiceHost(ServiceDescription description, PeerAddress address);

        /// <summary>
        /// Register the service manager.
        /// </summary>
        /// <param name="manager">The service manager.</param>
        void RegisterWithServiceManager(ServiceManager manager);
    }
}