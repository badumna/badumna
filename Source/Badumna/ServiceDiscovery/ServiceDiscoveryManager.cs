﻿//---------------------------------------------------------------------------------
// <copyright file="ServiceDiscoveryManager.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

using System;
using System.Collections.Generic;

using Badumna.Core;
using Badumna.DistributedHashTable;
using Badumna.Utilities;
using Badumna.Security;

namespace Badumna.ServiceDiscovery
{
    /// <summary>
    /// The service discovery manager is used to implement the service discovery feature for the badumna library. 
    /// </summary>
    internal class ServiceDiscoveryManager : DhtProtocol
    {
        // the following variables are for debugging purpose. not for stats collection. 
        public int mTotalPopulate;
        public int mTotalReturnedRecords;
        public int mTotalSuccessfulQuery;

        /// <summary>
        /// When this module is enabled.
        /// </summary>
        protected TimeSpan enableTime;

        /// <summary>
        /// The distributed store for storing service records on the Dht.
        /// </summary>
        protected ServiceRecordStore distributedStore;

        /// <summary>
        /// Force dht query or not. 
        /// </summary>
        protected bool forceCheckDht;

        /// <summary>
        /// The container used to store cached query results. 
        /// </summary>
        protected ServiceRecordStore cachedRecords;

        /// <summary>
        /// The number of maintaince operations
        /// </summary>
        private int maintainceTaskCounter;

        /// <summary>
        /// The Dht facade. It should be an All Inclusive Dht. 
        /// </summary>
        private DhtFacade dhtFacade;

        /// <summary>
        /// The service exchange manager which implements the service exchange and gossip feature. 
        /// </summary>
        private ServiceExchangeManager serviceExchange;

        /// <summary>
        /// All services announced by the local peer. 
        /// </summary>
        private Dictionary<ServiceType, ServiceDetailsRecord> announcedServices;
        
        /// <summary>
        /// Services interested by the local peer.
        /// </summary>
        private List<ServiceType> interestedServices;

        /// <summary>
        /// The maintaince task that is periodically called. It will try to populate the cached records, do the garbage collection and etc.
        /// </summary>
        private RegularTask maintainceTask;

        /// <summary>
        /// When the last garbage collection is performed.
        /// </summary>
        private TimeSpan lastGarbageCollectionTime;

        /// <summary>
        /// When the last service announcement is performed. 
        /// </summary>
        private TimeSpan lastAnnouncementTime;

        /// <summary>
        /// When last gossip is performed. 
        /// </summary>
        private TimeSpan lastGossipTime;

        // when the server announces itself, it send out mParalellFactor messages in parallel. 
        // peers who received such message should each in turn sends out mNumberOfReplicas messages
        // to create mNumberOfReplicas replicas. 
        
        /// <summary>
        /// A list of recently returned dht query replies.
        /// </summary>
        private List<ServiceDetailsRecord> recentDhtQueryReplies;

        /// <summary>
        /// Expected Dht query replies exchange time. 
        /// </summary>
        private TimeSpan expectedDhtQueryRepliesExchangeTime;

        /// <summary>
        /// The estimator used for estimating the load on dht nodes used for hosting service records. 
        /// </summary>
        private DhtLoadEstimator loadEstimator;

        /// <summary>
        /// When the cached records is last populated. 
        /// </summary>
        private TimeSpan lastPopulateTime;

        /// <summary>
        /// Has at least queried dht once.
        /// </summary>
        private bool queriedDht;
        
        /// <summary>
        /// When the manager discover the first service peer
        /// </summary>
        private TimeSpan firstDiscoveryTime;

        /// <summary>
        /// The queue to use for scheduling events.
        /// </summary>
        private NetworkEventQueue eventQueue;

        /// <summary>
        /// Provides access to the current time.
        /// </summary>
        protected ITime timeKeeper;

        /// <summary>
        /// Indicates whether service discovery is enabled.
        /// </summary>
        private bool isServiceDiscoveryEnabled;
        
        /// <summary>
        /// Provides the public peer address.
        /// </summary>
        private INetworkAddressProvider addressProvider;

        /// <summary>
        /// Reports on network connectivity status.
        /// </summary>
        private INetworkConnectivityReporter connectivityReporter;

        /// <summary>
        /// Provides security tokens.
        /// </summary>
        private TokenCache tokens;

        /// <summary>
        /// Hacky way to collect service discovery stats in the simulator.  Multiple peers share the
        /// same instance of the stats collector.
        /// </summary>
        protected IServiceDiscoveryStatsCollector statsCollector;

        /// <summary>
        /// Initializes a new instance of the <see cref="ServiceDiscoveryManager"/> class.
        /// </summary>
        /// <param name="facade">The Dht facade.</param>
        /// <param name="isServiceDiscoveryEnabled">Indicates whether service discovery is enabled.</param>
        /// <param name="eventQueue">The event queue to schedule events on.</param>
        /// <param name="timeKeeper">The time source.</param>
        /// <param name="addressProvider">Provides the public peer address.</param>
        /// <param name="connectivityReporter">Reports on network connectivity status.</param>
        /// <param name="tokens">Provides security tokens.</param>
        public ServiceDiscoveryManager(
            DhtFacade facade,
            bool isServiceDiscoveryEnabled,
            NetworkEventQueue eventQueue,
            ITime timeKeeper,
            INetworkAddressProvider addressProvider,
            INetworkConnectivityReporter connectivityReporter,
            TokenCache tokens)
            : base(facade)
        {
            this.isServiceDiscoveryEnabled = isServiceDiscoveryEnabled;
            this.eventQueue = eventQueue;
            this.timeKeeper = timeKeeper;
            this.addressProvider = addressProvider;
            this.connectivityReporter = connectivityReporter;
            this.tokens = tokens;

            this.statsCollector = ServiceDiscoveryStatsCollector.GetInstance(this.timeKeeper);

            this.dhtFacade = facade;
            this.distributedStore = new ServiceRecordStore(this.timeKeeper);
            this.cachedRecords = new ServiceRecordStore(this.timeKeeper);
            this.serviceExchange = new ServiceExchangeManager(facade, this, this.addressProvider, isServiceDiscoveryEnabled, this.statsCollector);
            this.announcedServices = new Dictionary<ServiceType, ServiceDetailsRecord>();
            this.interestedServices = new List<ServiceType>();
            this.recentDhtQueryReplies = new List<ServiceDetailsRecord>();

            this.expectedDhtQueryRepliesExchangeTime = this.timeKeeper.Now;
            this.lastPopulateTime = this.timeKeeper.Now - Parameters.ServiceDiscoveryMinPopulateInterval;
            this.maintainceTask = new RegularTask(
                "service_discovery_regular_task",
                Parameters.ServiceDiscoveryMaintainceTaskInterval,
                eventQueue,
                connectivityReporter,
                this.ServiceDiscoveryReglarTask);

            this.forceCheckDht = false;
            this.queriedDht = false;

            this.lastGarbageCollectionTime = this.timeKeeper.Now;
            this.lastAnnouncementTime = this.timeKeeper.Now;
            this.lastGossipTime = this.timeKeeper.Now;

            this.mTotalPopulate = 0;
            this.mTotalReturnedRecords = 0;
            this.mTotalSuccessfulQuery = 0;
            this.enableTime = this.timeKeeper.Now;

            this.loadEstimator = new DhtLoadEstimator(facade, this.timeKeeper);
        }

        /// <summary>
        /// Gets the number of DHT replicas.
        /// </summary>
        /// <value>The number of DHT replicas.</value>
        public static uint DHTReplicas
        {
            get { return Parameters.ServiceDiscoveryParalellFactor * Parameters.ServiceDiscoveryNumberOfReplicas; }
        }

        /// <summary>
        /// Gets or sets the maintaince task counter.
        /// </summary>
        /// <value>The maintaince task counter.</value>
        public int MaintainceTaskCounter
        {
            get { return this.maintainceTaskCounter; }
            set { this.maintainceTaskCounter = value; }
        }

        /// <summary>
        /// Gets or sets the first discovery time.
        /// </summary>
        /// <value>The first discovery time.</value>
        public TimeSpan FirstDiscoveryTime
        {
            get { return this.firstDiscoveryTime; }
            set { this.firstDiscoveryTime = value; }
        }

        /// <summary>
        /// Gets the distributed store.
        /// </summary>
        /// <value>The distributed store.</value>
        public ServiceRecordStore DistributedStore
        {
            get { return this.distributedStore; }
        }

        /// <summary>
        /// Gets the cached records.
        /// </summary>
        /// <value>The cached records.</value>
        public ServiceRecordStore CachedRecords
        {
            get { return this.cachedRecords; }
        }

        /// <summary>
        /// Gets the interested services.
        /// </summary>
        /// <value>The interested services.</value>
        public List<ServiceType> InterestedServices
        {
            get { return this.interestedServices; }
        }

        /// <summary>
        /// Starts this instance.
        /// </summary>
        public void Start()
        {
            ServiceDiscoveryHelper.ThrowExceptionWhenNotEnabled(this.isServiceDiscoveryEnabled);

            Logger.TraceInformation(LogTag.ServiceDiscovery, "ServiceDiscoveryManager Start is called.");
            this.maintainceTask.Start();

            // populate the cached services as soon as the local peer is ready. 
            this.dhtFacade.InitializationCompleteNotification += this.PeerInitializationCompleteNotificationHandler;
            
            // send new arrived neighbor the welcome message that contains my known hosts.
            this.dhtFacade.NeighbourArrivalNotification += this.NeighbourArrivalNotificationHandler;

            this.connectivityReporter.NetworkShuttingDownEvent += this.OnNetworkShuttingdown;
        }

        /// <summary>
        /// Sets the service become available callback.
        /// </summary>
        /// <param name="callback">The callback delegate.</param>
        public void SetOnServiceBecomeAvailableCallback(OnServiceBecomeAvailable callback)
        {
            this.cachedRecords.OnServiceBecomeAvailable = callback;
        }

        /// <summary>
        /// Shutdowns this instance.
        /// </summary>
        public void Shutdown()
        {
            Logger.TraceInformation(LogTag.ServiceDiscovery, "ServiceDiscoveryManager.Shutdown is called.");
            this.maintainceTask.Stop();

            this.dhtFacade.InitializationCompleteNotification -= this.PeerInitializationCompleteNotificationHandler;
            this.dhtFacade.NeighbourArrivalNotification -= this.NeighbourArrivalNotificationHandler;
            this.connectivityReporter.NetworkShuttingDownEvent -= this.OnNetworkShuttingdown;

            this.Clear();
        }

        /// <summary>
        /// Announces the specified service.
        /// </summary>
        /// <param name="type">The service type.</param>
        public void AnnounceService(ServiceType type)
        {
            ServiceDiscoveryHelper.ThrowExceptionWhenNotEnabled(this.isServiceDiscoveryEnabled);

            Logger.TraceInformation(LogTag.ServiceDiscovery, "ServiceDiscoveryManager AnnounceService is called.");
            ServiceDetailsRecord record = null;
            if (!this.announcedServices.TryGetValue(type, out record))
            {
                record = new ServiceDetailsRecord(this.addressProvider.PublicAddress, type, this.timeKeeper);
                this.announcedServices[type] = record;
            }
            else
            {
                // TODO : shall we allow two instances of the same service on a single peer?  
                Logger.TraceInformation(LogTag.ServiceDiscovery, "Service type already registered on this peer - {0}", Enum.GetName(typeof(ServiceType), type));
                throw new InvalidOperationException("Trying to register the same service twice.");
            }

            this.AnnounceRegisteredServices();
        }

        /// <summary>
        /// Registers the interested service.
        /// </summary>
        /// <param name="type">The interested service type.</param>
        public void RegisterInterestedService(ServiceType type)
        {
            ServiceDiscoveryHelper.ThrowExceptionWhenNotEnabled(this.isServiceDiscoveryEnabled);
           
            Logger.TraceInformation(LogTag.ServiceDiscovery, "ServiceDiscoveryManager RegisterInterestedService is called.");
            if (this.interestedServices.Contains(type))
            {
                throw new InvalidOperationException("The interested service {0} has already been registered.");
            }

            this.interestedServices.Add(type);
        }

        /// <summary>
        /// Whether the specified service has been registered.
        /// </summary>
        /// <param name="type">The service type.</param>
        /// <returns>Whether registered.</returns>
        public bool ServiceRegistered(ServiceType type)
        {
            return this.interestedServices.Contains(type);
        }

        /// <summary>
        /// Shutdowns the specified service.
        /// </summary>
        /// <param name="type">The service type.</param>
        public void ShutdownService(ServiceType type)
        {
            ServiceDiscoveryHelper.ThrowExceptionWhenNotEnabled(this.isServiceDiscoveryEnabled);

            ServiceDetailsRecord record = null;
            if (!this.announcedServices.TryGetValue(type, out record))
            {
                Logger.TraceInformation(LogTag.ServiceDiscovery, "Trying to shutdown a service that has never been registered.");
                return;
            }
            else
            {
                this.announcedServices.Remove(type);
            }

            this.NotifyShutdown(record);
        }

        /// <summary>
        /// Shutdowns all registered services.
        /// </summary>
        public void ShutdownService()
        {
            ServiceDiscoveryHelper.ThrowExceptionWhenNotEnabled(this.isServiceDiscoveryEnabled);

            Logger.TraceInformation(LogTag.ServiceDiscovery, "Going to shut down all registered services.");

            Dictionary<ServiceType, ServiceDetailsRecord> services = new Dictionary<ServiceType, ServiceDetailsRecord>(this.announcedServices);
            foreach (ServiceType type in services.Keys)
            {
                this.ShutdownService(type);
            }
        }

        /// <summary>
        /// Queries the specified service.
        /// </summary>
        /// <param name="type">The service type.</param>
        /// <returns>A KnownService object that contains possible suitable services.</returns>
        public KnownService QueryService(ServiceType type)
        {
            ServiceDiscoveryHelper.ThrowExceptionWhenNotEnabled(this.isServiceDiscoveryEnabled);

            Logger.TraceInformation(LogTag.ServiceDiscovery, "ServiceDiscoveryManager QueryService is called, type = {0}", type.ToString());
            KnownService knownService = new KnownService();

            List<ServiceDetailsRecord> queryReplyList = this.cachedRecords.GetServiceRecords(type.ServerType, type.ServiceTypeName);
            if (null == queryReplyList)
            {
                knownService.ServiceFromCachedQueryReply = new List<ServiceDetailsRecord>();
            }
            else
            {
                knownService.ServiceFromCachedQueryReply = new List<ServiceDetailsRecord>(queryReplyList);
            }

            if (null == queryReplyList || queryReplyList.Count == 0)
            {
                this.ForcePopulateCachedServices();
            }

            return knownService;
        }

        /// <summary>
        /// Gets the known service hosts for the specified service type.
        /// </summary>
        /// <param name="type">The service type.</param>
        /// <returns>An array of service details records.</returns>
        public ServiceDetailsRecord[] GetKnownServiceHosts(ServiceType type)
        {
            Logger.TraceInformation(LogTag.ServiceDiscovery, "ServiceDiscoveryManager GetKnownServiceHosts is called, type = {0}", type.ToString());
            List<ServiceDetailsRecord> list = this.distributedStore.GetServiceRecords(type.ServerType, type.ServiceTypeName);
            if (null != list)
            {
                return list.ToArray();
            }

            list = this.cachedRecords.GetServiceRecords(type.ServerType, type.ServiceTypeName);
            if (null != list)
            {
                return list.ToArray();
            }

            return null;
        }

        /// <summary>
        /// Called when cached records become unemptied.
        /// </summary>
        public virtual void OnCachedRecordsUnemptied()
        {
        }

        /// <summary>
        /// Announces the specified service.
        /// </summary>
        /// <param name="record">The service details record.</param>
        protected void Announce(ServiceDetailsRecord record)
        {
            Logger.TraceInformation(LogTag.ServiceDiscovery, "ServiceDiscoveryManager Announce is called.");

            Permission permission = this.tokens.GetPermission(PermissionType.Announce);
            PrivateKey keyPair = this.tokens.GetPermissionKeyPair();
            if (permission == null || keyPair == null)
            {
                Logger.TraceInformation(LogTag.ServiceDiscovery, "The local peer doesn't have permission to announce service to other peers.");
                return;
            }
            string recordString = record.ToString();
            Signature signature = new Signature(keyPair, recordString);

            for (uint i = 0; i < Parameters.ServiceDiscoveryParalellFactor; i++)
            {
                uint replicaNumber = i * Parameters.ServiceDiscoveryNumberOfReplicas;
                uint index = 0;

                HashKey key = ServiceRecordKeyCreator.GetKey(record, replicaNumber);
                DhtEnvelope envelope = this.GetMessageFor(key, QualityOfService.Reliable);
                this.RemoteCall(envelope, this.AnnounceServiceRequest, record, i, index, permission, signature);
                this.statsCollector.RecordOutgoingTraffic(envelope.Length, this.addressProvider.PublicAddress);
                this.SendMessage(envelope);
            }
        }

        /// <summary>
        /// Handles the service announcement request. 
        /// </summary>
        /// <param name="record">The service details record.</param>
        /// <param name="rank">The replica rank.</param>
        /// <param name="index">The replica index.</param>
        /// <param name="permission">TODO: Document</param>
        /// <param name="signature">TODO: Document</param>
        [DhtProtocolMethod(DhtMethod.ServiceDiscoveryAnnounceServiceRequest)]
        protected void AnnounceServiceRequest(ServiceDetailsRecord record, uint rank, uint index, Permission permission, Signature signature)
        {
            // ignore the request if the permission and signature can't be verified
            if (!this.VerifyPermission(record, permission, signature))
            {
                return;
            }

            uint curIndex = index + 1;
            this.distributedStore.AddRecord(record);
            this.cachedRecords.AddRecord(record);

            this.statsCollector.RecordIncomingTraffic(this.CurrentEnvelope.Length, this.addressProvider.PublicAddress);

            if (curIndex < (Parameters.ServiceDiscoveryNumberOfReplicas - (uint)1))
            {
                uint replicaNumber = (rank * Parameters.ServiceDiscoveryNumberOfReplicas) + curIndex;

                HashKey key = ServiceRecordKeyCreator.GetKey(record, replicaNumber);
                DhtEnvelope envelope = this.GetMessageFor(key, QualityOfService.Reliable);
                this.RemoteCall(envelope, this.AnnounceServiceRequest, record, rank, curIndex, permission, signature);
                this.statsCollector.RecordOutgoingTraffic(envelope.Length, this.addressProvider.PublicAddress);
                this.SendMessage(envelope);
            }

            this.serviceExchange.GossipServiceDetails(true);
        }

        /// <summary>
        /// Forces to populate cached services.
        /// </summary>
        protected void ForcePopulateCachedServices()
        {
            Logger.TraceInformation(LogTag.ServiceDiscovery, "ServiceDiscoveryManager ForcePopulateCachedService is called.");
            this.lastPopulateTime = this.timeKeeper.Now - Parameters.ServiceDiscoveryMinPopulateInterval;
            this.PopulateCachedServices();
        }

        /// <summary>
        /// Handles shutdown service request. 
        /// </summary>
        /// <param name="record">The service details record.</param>
        [DhtProtocolMethod(DhtMethod.ServiceDiscoveryShutdownServiceRequest)]
        protected void ShutdownServiceRequest(ServiceDetailsRecord record)
        {
            Logger.TraceInformation(LogTag.ServiceDiscovery, "ServiceDiscoveryManager ShutdownServiceRequest is called. record = {0}", record);
            this.statsCollector.RecordIncomingTraffic(this.CurrentEnvelope.Length, this.addressProvider.PublicAddress);
            this.distributedStore.RemoveRecord(record);
        }

        /// <summary>
        /// Sends the query service request.
        /// </summary>
        /// <param name="type">The service type.</param>
        protected void SendQueryServiceRequest(ServiceType type)
        {
            uint random;
            uint randomReplicaNumber;
            Logger.TraceInformation(LogTag.ServiceDiscovery, "ServiceDiscoveryManager SendQueryServiceRequest is called. type = {0}", type.ToString());

            // tell the load estimator that the local peer itself just issued a dht query
            this.loadEstimator.RecordObservedDhtQuery(type);

            for (uint i = 0; i < Parameters.ServiceDiscoveryQueryParallelFactor; i++)
            {
                random = (uint)RandomSource.Generator.Next((int)Parameters.ServiceDiscoveryNumberOfReplicas);
                randomReplicaNumber = random + (Parameters.ServiceDiscoveryNumberOfReplicas * i);
                HashKey key = ServiceRecordKeyCreator.GetKey(type, randomReplicaNumber);
                DhtEnvelope envelope = this.GetMessageFor(key, QualityOfService.Reliable);
                this.RemoteCall(envelope, this.GetServiceDetailsRequest, type);
                this.statsCollector.RecordOutgoingTraffic(envelope.Length, this.addressProvider.PublicAddress);
                this.SendMessage(envelope);
            }

            this.expectedDhtQueryRepliesExchangeTime = this.timeKeeper.Now + Parameters.ServiceDiscoveryExpectedDhtQueryLatency;
        }

        /// <summary>
        /// Handles the get service details request. 
        /// </summary>
        /// <param name="type">The service type.</param>
        [DhtProtocolMethod(DhtMethod.ServiceDiscoveryGetServiceDetailsRequest)]
        protected void GetServiceDetailsRequest(ServiceType type)
        {
#if DEBUG
            this.statsCollector.RecordDhtAccess();
#endif
            List<ServiceDetailsRecord> records = null;
            this.statsCollector.RecordIncomingTraffic(this.CurrentEnvelope.Length, this.addressProvider.PublicAddress);
            records = this.distributedStore.GetServiceRecords(type.ServerType, type.ServiceTypeName);
            if (records == null || records.Count == 0)
            {
                return;
            }

            DhtEnvelope envelope = this.GetMessageFor(this.CurrentEnvelope.Source, QualityOfService.Reliable);
            this.RemoteCall(envelope, this.HandleServiceDetailsQueryReply, records);
            this.statsCollector.RecordOutgoingTraffic(envelope.Length, this.addressProvider.PublicAddress);
            this.SendMessage(envelope);
        }

        /// <summary>
        /// Handles the service details query reply.
        /// </summary>
        /// <param name="records">The service details records.</param>
        [DhtProtocolMethod(DhtMethod.ServiceDiscoveryHandleServiceDetailsQueryReply)]
        protected void HandleServiceDetailsQueryReply(List<ServiceDetailsRecord> records)
        {
            Logger.TraceInformation(LogTag.ServiceDiscovery, "ServiceDiscoveryManager HandleServiceDetailsQueryReply is called. count = {0}", records.Count);
            this.statsCollector.RecordIncomingTraffic(this.CurrentEnvelope.Length, this.addressProvider.PublicAddress);
            bool wasEmpty = this.cachedRecords.Count == 0;
            this.mTotalReturnedRecords += records.Count;
            this.cachedRecords.AddRecords(records);
            this.recentDhtQueryReplies.AddRange(records);
            bool nowEmpty = this.cachedRecords.Count == 0;

            if (wasEmpty && !nowEmpty)
            {
                this.OnCachedRecordsUnemptied();    
            }
        }

        /// <summary>
        /// Handles the request for sharing DHT query replies.
        /// </summary>
        /// <param name="records">The service details records.</param>
        [DhtProtocolMethod(DhtMethod.ServiceDiscoveryHandleSharedDhtQueryReplies)]
        protected void HandleSharedDhtQueryReplies(List<ServiceDetailsRecord> records)
        {
            this.statsCollector.RecordIncomingTraffic(this.CurrentEnvelope.Length, this.addressProvider.PublicAddress);
            // record all incoming records 
            this.serviceExchange.HandleGossipMessage(records, false);

            // record all observed dht queries
            List<ServiceType> types = new List<ServiceType>();
            foreach (ServiceDetailsRecord record in records)
            {
                if (!types.Contains(record.Type))
                {
                    types.Add(record.Type);
                }
            }

            foreach (ServiceType type in types)
            {
                this.loadEstimator.RecordObservedDhtQuery(type);
            }
        }

        private bool VerifyPermission(ServiceDetailsRecord record, Permission permission, Signature signature)
        {
            if (permission == null || signature == null)
            {
                return false;
            }

            // check whether the permission is indeed granted by the system
            if (!permission.Verify(PermissionType.Announce, this.tokens))
            {
                return false;   
            }

            if (!signature.Verify(permission, record.ToString()))
            {
                return false;
            }
            
            return true;
        }

        /// <summary>
        /// Called when the network is being shut down.
        /// </summary>
        private void OnNetworkShuttingdown()
        {
            Logger.TraceInformation(LogTag.ServiceDiscovery, "Service discovery on network shutting down is called.");
            this.ShutdownService();
        }

        /// <summary>
        /// The neighbour arrival notification event handler.
        /// </summary>
        /// <param name="node">The arriving node.</param>
        private void NeighbourArrivalNotificationHandler(NodeInfo node)
        {
            if (!this.isServiceDiscoveryEnabled)
            {
                // to minimize traffic, the welcome message will not be sent. 
                return;
            }

            if (null != node && null != node.Address)
            {
                Logger.TraceInformation(LogTag.ServiceDiscovery, "NeighbourArrivalNotificationHandler is called, neighbour address = {0}", node.Address);
                this.SendWelcomeMessage(node.Address);
            }
        }

        /// <summary>
        /// The event handler for the peer initialization complete notification.
        /// </summary>
        /// <param name="node">The affected node.</param>
        private void PeerInitializationCompleteNotificationHandler(NodeInfo node)
        {
            Logger.TraceInformation(LogTag.ServiceDiscovery, "InitializationCompleteNotificationHandler is called.");

            // now the local peer is actually enabled.
            this.enableTime = this.timeKeeper.Now + TimeSpan.FromSeconds(10);

            // announce all registered services (if the local peer is a service host)
            if (this.isServiceDiscoveryEnabled)
            {
                this.AnnounceRegisteredServices();

                // when the initialization is done, each peer got a chance to populate their cached services. 
                this.InitialForcePopulateCachedServices();
            }

            return;
        }

        /// <summary>
        /// Populate the cached services on initilization completion. 
        /// </summary>
        private void InitialForcePopulateCachedServices()
        {
            // FIXME
            // the following 10 seconds delay is an ugly hack. need to figure out what is the notification
            // indicating that the local peer has fully connected to the dht.
            if (this.connectivityReporter.Status != ConnectivityStatus.Online)
            {
                this.eventQueue.Schedule(2000, this.InitialForcePopulateCachedServices);
            }
            else
            {
                this.eventQueue.Schedule(10000, this.ForcePopulateCachedServices);
            }
        }

        /// <summary>
        /// Populates the cached services.
        /// </summary>
        private void PopulateCachedServices()
        {
            Logger.TraceInformation(LogTag.ServiceDiscovery, "ServiceDiscoveryManager PopulateCachedService is called.");
            if (!this.dhtFacade.IsRouting)
            {
                // not ready yet, wait another 5 seconds
                this.eventQueue.Schedule(5000, this.ForcePopulateCachedServices);
                return;
            }

            if (this.timeKeeper.Now - this.lastPopulateTime < Parameters.ServiceDiscoveryMinPopulateInterval)
            {
                return;
            }

            this.lastPopulateTime = this.timeKeeper.Now;

            bool populate = false;
            bool containsOldRecord = false;
            bool noRecord = false;

            foreach (ServiceType type in this.interestedServices)
            {
                bool alreadyQueried = false;
                List<ServiceDetailsRecord> records = this.cachedRecords.GetServiceRecords(type.ServerType, type.ServiceTypeName);
                if (null != records)
                {
                    foreach (ServiceDetailsRecord record in records)
                    {
                        if (record.ShouldRefresh)
                        {
                            containsOldRecord = true;
                        }
                    }
                }

                if (null == records || records.Count == 0)
                {
                    if (this.loadEstimator.DhtQueryAllowed(type) && this.queriedDht)
                    {
                        alreadyQueried = true;
                        Logger.TraceInformation(LogTag.ServiceDiscovery, "No record for type {0}, DhtQueryAllowed by the estimator, ServiceDiscoveryManager will try dht query.", type.ToString());
                        this.SendQueryServiceRequest(type);
                    }

                    noRecord = true;
                }

                if ((this.forceCheckDht || !this.queriedDht) && !alreadyQueried)
                {
                    this.mTotalPopulate++;

                    // send dht based service query. this is considered as expensive operation, not because
                    // of the multi-hops routing nature, but due to the fact that the number of peers indexing
                    // records (i.e. hosting records that can be addressed via well known keys) are limited. 
                    Logger.TraceInformation(LogTag.ServiceDiscovery, "ServiceDiscoveryManager will try dht query.");
                    this.SendQueryServiceRequest(type);
                    populate = true;
                }
            }

            if (this.forceCheckDht)
            {
                this.forceCheckDht = false;
            }

            if (!this.queriedDht)
            {
                this.queriedDht = true;
            }

            if (populate || containsOldRecord || noRecord)
            {
                Logger.TraceInformation(LogTag.ServiceDiscovery, "No record or contains old record, ServiceDiscoveryManager will try service exchange.");
                this.serviceExchange.ServiceExchange();
            }
        }

        /// <summary>
        /// Notifies the involved nodes to remove hosted service records.
        /// </summary>
        /// <param name="record">The service details record.</param>
        private void NotifyShutdown(ServiceDetailsRecord record)
        {
            Logger.TraceInformation(LogTag.ServiceDiscovery, "ServiceDiscoveryManager NotifyShutdown is called. record = {0}", record.ToString());
            for (uint i = 0; i < Parameters.ServiceDiscoveryNumberOfReplicas * Parameters.ServiceDiscoveryParalellFactor; i++)
            {
                HashKey key = ServiceRecordKeyCreator.GetKey(record, i);
                DhtEnvelope envelope = this.GetMessageFor(key, QualityOfService.Reliable);
                this.RemoteCall(envelope, this.ShutdownServiceRequest, record);
                this.statsCollector.RecordOutgoingTraffic(envelope.Length, this.addressProvider.PublicAddress);
                this.SendMessage(envelope);
            }
        }

        /// <summary>
        /// Announces the registered services.
        /// </summary>
        private void AnnounceRegisteredServices()
        {
            Logger.TraceInformation(LogTag.ServiceDiscovery, "ServiceDiscoveryManager AnnounceRegisteredServices is called.");

            foreach (ServiceType type in this.announcedServices.Keys)
            {
                ServiceDetailsRecord record = this.announcedServices[type];
                record.ExpirationTime = this.timeKeeper.Now + Parameters.ServiceDiscoveryDHTRecordTTL;
                this.Announce(record);
            }
        }

        /// <summary>
        /// Exchanges the DHT query replies with neighbours. 
        /// </summary>
        private void ExchangeDhtQueryReplies()
        {
            if (this.timeKeeper.Now > this.expectedDhtQueryRepliesExchangeTime)
            {
                if (this.recentDhtQueryReplies.Count > 0)
                {
                    Logger.TraceInformation(LogTag.ServiceDiscovery, "ServiceDiscoveryManager ExchangeDhtQueryReplies is going to exchange relies with all neighbours, Count = {0}", this.recentDhtQueryReplies.Count);
                    NodeInfo[] nodes = this.dhtFacade.GetRoutingTableNodes();
                    if (null == nodes || nodes.Length == 0)
                    {
                        // this is highly unlikely
                        this.recentDhtQueryReplies.Clear();
                        return;
                    }

                    int expected = nodes.Length > 16 ? 16 : nodes.Length;
                    int[] set = Utils.GetRandomSet(nodes.Length, expected);

                    NodeInfo[] newNodeSet = new NodeInfo[expected];
                    for (int i = 0; i < expected; i++)
                    {
                        newNodeSet[i] = nodes[set[i]];
                    }

                    foreach (NodeInfo ni in newNodeSet)
                    {
                        if (ni != null && ni.Address != null)
                        {
                            DhtEnvelope envelope = this.GetMessageFor(ni.Address, QualityOfService.Unreliable);
                            this.RemoteCall(envelope, this.HandleSharedDhtQueryReplies, this.recentDhtQueryReplies);
                            this.statsCollector.RecordOutgoingTraffic(envelope.Length, this.addressProvider.PublicAddress);
                            this.SendMessage(envelope);
                        }
                    }

                    this.recentDhtQueryReplies.Clear();
                    return;
                }
            }
        }

        /// <summary>
        /// Sends the welcome message to the specified peer address.
        /// </summary>
        /// <param name="address">The peer address.</param>
        private void SendWelcomeMessage(PeerAddress address)
        {
            List<ServiceDetailsRecord> records = this.cachedRecords.GetTwoRecordsEachType();
            if (null == records || records.Count == 0)
            {
                return;
            }

            Logger.TraceInformation(LogTag.ServiceDiscovery, "Sending welcome information.");
            this.serviceExchange.GossipServiceDetails(address, records, false, true);
        }

        /// <summary>
        /// Garbage collection.
        /// </summary>
        private void GarbageCollection()
        {
            Logger.TraceInformation(LogTag.ServiceDiscovery, "ServiceDiscoveryManager GarbageCollection is called.");
            this.loadEstimator.GarbageCollection(Parameters.ServiceDiscoveryDhtEstimatorRecordTTL);
            this.distributedStore.GarbageCollection();
            this.cachedRecords.GarbageCollection();
        }

        /// <summary>
        /// Determines whether the system is ready.
        /// </summary>
        /// <returns>
        /// <c>true</c> if system is ready; otherwise, <c>false</c>.
        /// </returns>
        private bool IsSystemReady()
        {
            if (!this.dhtFacade.IsRouting)
            {
                Logger.TraceInformation(LogTag.ServiceDiscovery, "The dht is not routing.");
                return false;
            }

            if (this.connectivityReporter.Status != ConnectivityStatus.Online)
            {
                Logger.TraceInformation(LogTag.ServiceDiscovery, "ConnectivityStatus is not online.");
                return false;
            }

            return true;
        }

        /// <summary>
        /// The regular task.
        /// </summary>
        private void ServiceDiscoveryReglarTask()
        {
            if (!this.IsSystemReady())
            {
                Logger.TraceInformation(LogTag.ServiceDiscovery, "The peer is not ready, will skip the maintaince task. ");
            }

            this.maintainceTaskCounter++;
            TimeSpan now = this.timeKeeper.Now;

            // remove out of date records.
            if (now - this.lastGarbageCollectionTime >= Parameters.ServiceDiscoveryGarbageCollectionInterval)
            {
                this.GarbageCollection();
                this.lastGarbageCollectionTime = now;
            }

            if (this.isServiceDiscoveryEnabled)
            {
                // if the local peer hosts any service, reannounce all registered services. this will update the time to live 
                // value of the stored objects on the dht, it will also fix/repair lost objects during dht churn.  
                if (now - this.lastAnnouncementTime >= Parameters.ServiceDiscoveryAnnouncementInterval)
                {
                    this.AnnounceRegisteredServices();
                    this.lastAnnouncementTime = now;
                }

                // do gossip between neighbors to share service details.
                if (now - this.lastGossipTime >= Parameters.ServiceDiscoveryGossipInterval)
                {
                    this.serviceExchange.GossipServiceDetails(false);
                    this.lastGossipTime = now;
                }

                // tries to get query service addresses using dht query and/or neighbour exchange
                this.PopulateCachedServices();

                // exchange the Dht query replies so neighbors can have a rough idea about what kind of load the Dht hosting nodes are having
                this.ExchangeDhtQueryReplies();
            }
        }

        /// <summary>
        /// Clears this instance.
        /// </summary>
        private void Clear()
        {
            Logger.TraceInformation(LogTag.ServiceDiscovery, "ServiceDiscoveryManager.Clear() is called.");

            this.distributedStore.Clear();
            this.cachedRecords.Clear();
            this.announcedServices.Clear();
            this.recentDhtQueryReplies.Clear();
            this.loadEstimator.Clear();

            this.maintainceTaskCounter = 0;

            this.forceCheckDht = false;
            this.mTotalPopulate = 0;
            this.mTotalReturnedRecords = 0;
            this.mTotalSuccessfulQuery = 0;
        }
    }

    // this helper class is only used for debugging and simulation purpose. 
    [DontPerformCoverage]
    internal class MockServiceDiscoveryManager : ServiceDiscoveryManager
    {
        public MockServiceDiscoveryManager(DhtFacade facade, bool isServiceDiscoveryEnabled, NetworkEventQueue eventQueue, ITime timeKeeper, INetworkAddressProvider addressProvider, INetworkConnectivityReporter connectivityReporter, TokenCache tokens)
            : base(facade, isServiceDiscoveryEnabled, eventQueue, timeKeeper, addressProvider, connectivityReporter, tokens)
        {
        }

        // helper methods for unit tests and simulation
        public int GetDistributedStoreSize()
        {
            return this.distributedStore.Size;
        }

        public void ForceCheckDht()
        {
            this.forceCheckDht = true;
        }

        public void ForcePopulate()
        {
            this.ForcePopulateCachedServices();
        }

        public void ClearCachedQueryResult()
        {
            this.cachedRecords.Clear();
        }

        public int GetQueryResultSetSize()
        {
            return this.cachedRecords.Size;
        }

        public bool QueryReplyContains(PeerAddress address, ServiceType type)
        {
            List<ServiceDetailsRecord> list = this.cachedRecords.GetServiceRecords(type.ServerType, type.ServiceTypeName);

            if (null == list)
            {
                return false;
            }

            foreach (ServiceDetailsRecord rec in list)
            {
                if (rec.ServiceAddress.Equals(address))
                {
                    return true;
                }
            }

            return false;
        }

        public void QueryAnnouncedService(ServiceType type)
        {
            this.SendQueryServiceRequest(type);
        }

        public void AnnounceFakeService(PeerAddress address, ServiceType type)
        {
            ServiceDetailsRecord record = new ServiceDetailsRecord(address, type, this.timeKeeper);
            this.Announce(record);
        }

        public override void OnCachedRecordsUnemptied()
        {
            Logger.TraceInformation(LogTag.ServiceDiscovery, "now = {0}, enable time = {1}",
                this.timeKeeper.Now, this.enableTime);
            this.statsCollector.RecordPopulateDelay(this.enableTime, this.timeKeeper.Now);
        }
    }
}