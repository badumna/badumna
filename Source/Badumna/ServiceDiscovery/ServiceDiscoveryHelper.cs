﻿//---------------------------------------------------------------------------------
// <copyright file="ServiceDiscoveryHelper.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;

using Badumna;
using Badumna.Core;
using Badumna.DataTypes;
using Badumna.DistributedHashTable;
using Badumna.Security;
using Badumna.Utilities;

namespace Badumna.ServiceDiscovery
{
    /// <summary>
    /// ServiceRecordKeyCreator creates keys that will be used when inserting/querying records into/from the Dht. 
    /// </summary>
    internal class ServiceRecordKeyCreator
    {
        /// <summary>
        /// Gets the hash key of the service detail record.
        /// </summary>
        /// <param name="record">The service details record.</param>
        /// <param name="replica">The replica id.</param>
        /// <returns>The hash key of the record.</returns>
        public static HashKey GetKey(ServiceDetailsRecord record, uint replica)
        {
            return GetKey(record.Type, replica);
        }

        /// <summary>
        /// Gets the hash key of the service type.
        /// </summary>
        /// <param name="type">The service type.</param>
        /// <param name="replica">The replica id.</param>
        /// <returns>The hash key of the service type.</returns>
        public static HashKey GetKey(ServiceType type, uint replica)
        {
            string keyString = "Badumna_Service_Discovery_" + type.ToHashKeyString() + "_" + replica;
            return HashKey.Hash(keyString);
        }
    }

    /// <summary>
    /// Container object used when the client queries the discovery service. 
    /// </summary>
    internal class KnownService
    {
        /// <summary>
        /// The service details from the cached query replies. 
        /// </summary>
        private List<ServiceDetailsRecord> serviceFromCachedQueryReply;

        /// <summary>
        /// Gets or sets the service from cached query reply.
        /// </summary>
        /// <value>The service from cached query reply.</value>
        public List<ServiceDetailsRecord> ServiceFromCachedQueryReply
        {
            get { return this.serviceFromCachedQueryReply; }
            set { this.serviceFromCachedQueryReply = value; }
        }

        /// <summary>
        /// Determines whether there are service details records from the cached query reply store. 
        /// </summary>
        /// <returns>
        /// <c>true</c> if [has service from cached query reply]; otherwise, <c>false</c>.
        /// </returns>
        public bool HasServiceFromCachedQueryReply()
        {
            return (null != this.serviceFromCachedQueryReply) && (this.serviceFromCachedQueryReply.Count > 0);
        }

        /// <summary>
        /// Excludes the specified addresses.
        /// </summary>
        /// <param name="addresses">The addresses.</param>
        public void Exclude(IList<PeerAddress> addresses)
        {
            if (null == addresses || addresses.Count == 0)
            {
                return;
            }

            if (this.HasServiceFromCachedQueryReply())
            {
                KnownService.RemoveExcludes(ref this.serviceFromCachedQueryReply, addresses);
            }
        }

        /// <summary>
        /// Removes the excluded addresses from the records.
        /// </summary>
        /// <param name="records">The service details records.</param>
        /// <param name="addresses">The addresses that should be excluded.</param>
        private static void RemoveExcludes(ref List<ServiceDetailsRecord> records, IList<PeerAddress> addresses)
        {
            List<ServiceDetailsRecord> toRemove = new List<ServiceDetailsRecord>();
            foreach (PeerAddress address in addresses)
            {
                toRemove.Clear();

                foreach (ServiceDetailsRecord record in records)
                {
                    if (record.ServiceAddress.Equals(address))
                    {
                        toRemove.Add(record);
                    }
                }

                foreach (ServiceDetailsRecord record in toRemove)
                {
                    records.Remove(record);
                }
            }
        }
    }

    /// <summary>
    /// The helper class used in the service discovery module.
    /// </summary>
    internal class ServiceDiscoveryHelper
    {
        /// <summary>
        /// Throws the exception when the module is not enabled.
        /// </summary>
        public static void ThrowExceptionWhenNotEnabled(bool isServiceDiscoveryEnabled)
        {
            if (!isServiceDiscoveryEnabled)
            {
                throw new InvalidOperationException("The Service Discovery module is not enabled.");
            }
        }
    }
}