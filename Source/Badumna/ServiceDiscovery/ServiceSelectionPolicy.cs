﻿//---------------------------------------------------------------------------------
// <copyright file="ServiceSelectionPolicy.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

using System;

namespace Badumna.ServiceDiscovery
{
    /// <summary>
    /// Types of the selection policies.
    /// </summary>
    internal enum ServiceSelectionPolicyType : byte
    {
        /// <summary>
        /// Random policy.
        /// </summary>
        Random,
        
        /// <summary>
        /// First Available.
        /// </summary>
        FirstAvailable,
        
        /// <summary>
        /// Most recently refreshed.
        /// </summary>
        LastRefreshed,
    }

    /// <summary>
    /// Map service types to policy types.
    /// </summary>
    internal class ServiceSelectionPolicy
    {
        /// <summary>
        /// Gets the default service selection policy for the specified service type.
        /// </summary>
        /// <param name="type">The service type.</param>
        /// <returns>The service selection policy type.</returns>
        public static ServiceSelectionPolicyType GetServiceSelectionPolicy(ServiceType type)
        {
            switch (type.ServerType)
            {
                case ServerType.Arbitration:
                    return ServiceSelectionPolicyType.FirstAvailable;
                case ServerType.Overload:
                    return ServiceSelectionPolicyType.Random;
                default:
                    throw new InvalidOperationException("Unknown server type, couldn't determine the service selection policy.");
            }
        }
    }
}