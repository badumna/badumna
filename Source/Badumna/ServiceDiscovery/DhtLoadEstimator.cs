﻿//---------------------------------------------------------------------------------
// <copyright file="DhtLoadEstimator.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

using System;
using System.Collections.Generic;

using Badumna.Core;
using Badumna.DistributedHashTable;
using Badumna.Utilities;

namespace Badumna.ServiceDiscovery
{
    /// <summary>
    /// The DhtLoadEstimator class is used to estimate the load of those peers being used for hosting Dht records. The estimated load
    /// is used to determine whether the local peer can issue a Dht query to search for such Dht records. 
    /// </summary>
    internal class DhtLoadEstimator
    {
        /// <summary>
        /// For each service type, the time stamps of when Dht queries are observed. 
        /// </summary>
        private Dictionary<ServiceType, List<TimeSpan>> observedDhtQueries;

        /// <summary>
        /// The start up time.
        /// </summary>
        private TimeSpan startupTime;

        /// <summary>
        /// Provides access to the current time.
        /// </summary>
        private ITime timeKeeper;

        /// <summary>
        /// Initializes a new instance of the <see cref="DhtLoadEstimator"/> class.
        /// </summary>
        /// <param name="facade">The Dht facade.</param>
        /// <param name="timeKeeper">The time source.</param>
        public DhtLoadEstimator(DhtFacade facade, ITime timeKeeper)
        {
            this.timeKeeper = timeKeeper;
            this.observedDhtQueries = new Dictionary<ServiceType, List<TimeSpan>>();
            this.startupTime = this.timeKeeper.Now;
        }

        /// <summary>
        /// Gets or sets the observed DHT queries.
        /// </summary>
        /// <value>The observed DHT queries.</value>
        protected Dictionary<ServiceType, List<TimeSpan>> ObservedDhtQueries
        {
            get { return this.observedDhtQueries; }
            set { this.observedDhtQueries = value; }
        }

        /// <summary>
        /// Clears this instance.
        /// </summary>
        public void Clear()
        {
            this.observedDhtQueries.Clear();
            this.startupTime = this.timeKeeper.Now;
        }

        /// <summary>
        /// Garbage collection.
        /// </summary>
        /// <param name="recordTTL">The TTL of records.</param>
        public void GarbageCollection(TimeSpan recordTTL)
        {
            TimeSpan now = this.timeKeeper.Now;

            List<TimeSpan> toRemove;
            foreach (KeyValuePair<ServiceType, List<TimeSpan>> kvp in this.observedDhtQueries)
            {
                toRemove = new List<TimeSpan>();
                foreach (TimeSpan ts in kvp.Value)
                {
                    if (now - ts > recordTTL)
                    {
                        toRemove.Add(ts);
                    }
                }

                foreach (TimeSpan ts in toRemove)
                {
                    kvp.Value.Remove(ts);
                }
            }
        }

        /// <summary>
        /// Records the observed DHT query.
        /// </summary>
        /// <param name="type">The service type.</param>
        public void RecordObservedDhtQuery(ServiceType type)
        {
            if (null == type)
            {
                throw new ArgumentNullException("type");
            }

            List<TimeSpan> list = null;
            if (this.observedDhtQueries.TryGetValue(type, out list))
            {
                list.Add(this.timeKeeper.Now);

                if (list.Count >= 3)
                {
                    Logger.TraceInformation(LogTag.ServiceDiscovery, "Peer issued no less than 3 dht queries in 10 minutes.");
                }
            }
            else
            {
                list = new List<TimeSpan>();
                list.Add(this.timeKeeper.Now);
                this.observedDhtQueries[type] = list;
            }
        }

        /// <summary>
        /// Is it allowed to locally issue a Dht query.
        /// </summary>
        /// <param name="type">The service type.</param>
        /// <returns>Whether Dht quiery is allowed for the specified service type on local peer.</returns>
        public bool DhtQueryAllowed(ServiceType type)
        {
            if (null == type)
            {
                throw new ArgumentNullException("type");
            }

            if (this.timeKeeper.Now - this.startupTime < TimeSpan.FromMinutes(3))
            {
                // just started less than 3 minutes ago, the peer has already completed its initial dht query. 
                return false;
            }

            // FIXME
            // the goal of this class is to estimate the query load of dht nodes that are hosting the
            // service detail records. due the lack of the estimated network size at the current version
            // the following implementation just employ some very ulgy and rough guess. 
            List<TimeSpan> list = null;
            if (this.observedDhtQueries.TryGetValue(type, out list))
            {
                if (list.Count >= 5)
                {
                    return false;
                }
            }

            return true;
        }
    }
}