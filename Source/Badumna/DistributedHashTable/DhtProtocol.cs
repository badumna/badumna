using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;

using Badumna.Core;
using Badumna.Utilities;
using Badumna.Transport;


namespace Badumna.DistributedHashTable
{
    class DhtProtocol : ProtocolComponent<DhtEnvelope>, IDhtProtocol
    {
        public DhtProtocol(String layerName, GenericCallBackReturn<object, Type> factory)
            : base(layerName, typeof(DhtProtocolMethodAttribute), factory)
        {
        }

        public DhtProtocol(IDhtProtocol parent)
            : base(parent)
        { }

        public DhtEnvelope GetMessageFor(PeerAddress address, QualityOfService qos)
        {
            if (address.HashAddress != null)
            {
                return this.GetMessageFor(address.HashAddress, qos);
            }

            DhtEnvelope envelope = new DhtEnvelope(new TransportEnvelope(address, qos));

            this.PrepareMessageForDeparture(ref envelope);
            return envelope;
        }

        public DhtEnvelope GetMessageFor(HashKey key, QualityOfService qos)
        {
            return this.GetMessageFor(key, qos, 1);
        }

        public DhtEnvelope GetMessageFor(HashKey key, QualityOfService qos, int redundancyFactor)
        {           
            DhtEnvelope envelope = new DhtEnvelope(key, qos);
            this.PrepareMessageForDeparture(ref envelope);

            if (redundancyFactor > 1)
            {
                // failure handler is set in Router.RouteMessage
                RouteableQos<DhtEnvelope> newQos = new RouteableQos<DhtEnvelope>(envelope, null, null);
                newQos.RedundencyFactor = redundancyFactor;
                envelope.Qos = newQos;
            }

            return envelope;
        }
    }
}
