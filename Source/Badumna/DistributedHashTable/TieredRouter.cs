//---------------------------------------------------------------------------------
// <copyright file="TieredRouter.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using Badumna.Core;
using Badumna.Transport;
using Badumna.Utilities;
using System.Reflection;

namespace Badumna.DistributedHashTable
{
    /// <summary>
    /// The tiered dht router. 
    /// </summary>
    class TieredRouter : Router
    {
        /// <summary>
        /// The tier id.
        /// </summary>
        private int mTier;

        /// <summary>
        /// The stabilization task. 
        /// </summary>
        private RegularTask stabilizationTask;
        
        /// <summary>
        /// The interval of stabilization task. 
        /// </summary>
        private TimeSpan stabilizationTaskInterval;

        /// <summary>
        /// Provides access to the current time.
        /// </summary>
        private ITime timeKeeper;

        /// <summary>
        /// Initializes a new instance of the <see cref="TieredRouter"/> class.
        /// </summary>
        /// <param name="protocolParent">The protocol parent.</param>
        /// <param name="connectionTable">The connection table.</param>
        /// <param name="childRouter">The child router.</param>
        /// <param name="tier">The tier number.</param>
        /// <param name="eventQueue">The event queue to schedule events on.</param>
        /// <param name="timeKeeper">The time source.</param>
        /// <param name="addressProvider">Provides the public address.</param>
        /// <param name="connectivityReporter">Reports on network connectivity status.</param>
        /// <param name="connectionNotifier">Provides notifications of peer connections and disconnections.</param>
        public TieredRouter(
            RouterProtocol protocolParent,
            IConnectionTable connectionTable,
            IMessageConsumer<DhtEnvelope> childRouter,
            int tier,
            NetworkEventQueue eventQueue,
            ITime timeKeeper,
            INetworkAddressProvider addressProvider,
            INetworkConnectivityReporter connectivityReporter,
            IPeerConnectionNotifier connectionNotifier)
            : base(protocolParent, connectionTable, childRouter, eventQueue, addressProvider, connectivityReporter, connectionNotifier)
        {
            this.timeKeeper = timeKeeper;

            this.mTier = tier;

            if (this.mTier == (int)NamedTier.AllInclusive)
            {
                this.stabilizationTaskInterval = this.GetStabilizationTaskInterval();
                this.stabilizationTask = new RegularTask("dht_stabilization_task", this.stabilizationTaskInterval, eventQueue, connectivityReporter, this.StablizationTask);
                this.stabilizationTask.Start();
            }
        }

        /// <summary>
        /// Determines whether the peer specified by the address is in this tier.
        /// </summary>
        /// <param name="address">The address.</param>
        /// <returns>
        /// <c>true</c> if is in this tier; otherwise, <c>false</c>.
        /// </returns>
        protected override bool IsInThisTier(PeerAddress address)
        {
            return address.Tier <= this.mTier;
        }

        /// <summary>
        /// Descriptions this instance.
        /// </summary>
        /// <returns>The description.</returns>
        protected override string Description()
        {
            return String.Format("Tier {0}", this.mTier);
        }

        /// <summary>
        /// The stabilization task. 
        /// This task periodically send a stabilization ping message to those known peers which has not received a message from for some long time. 
        /// This is to ensure the connections associated with peers that are no longer online will be removed, so that:
        /// 1. The size of the connection table won't keep increasing. 
        /// 2. The routing table is reasonablly update to date in terms of the availability of peers.
        /// The task here actually stabilize the connection set in the connection table, rather than the dht routing table because in the all inclusive
        /// ring the should contain the same set of peers. 
        /// </summary>
        private void StablizationTask()
        {
            ConnectionTable ct = this.mConnectionTable as ConnectionTable;
            if (ct == null)
            {
                return;
            }
            
            // assuming a failed connection can be detected within 3 minutes after sending this reliable message. 
            double timeWindowUntilNextStabilization = this.stabilizationTaskInterval.TotalMilliseconds - TimeSpan.FromMinutes(3).TotalMilliseconds;
            Debug.Assert(timeWindowUntilNextStabilization > 0);

            TimeSpan now = this.timeKeeper.Now;
            foreach (Connection connection in ct.Connections)
            {
                // didn't receive any message from that peer for long time, time to say hello.
                if (connection.IsConnected && now - connection.LastReceiveTime > Parameters.MinimalStabilizationPingMessageInterval)
                {
                    int delayInMilliseconds = RandomSource.Generator.Next((int)timeWindowUntilNextStabilization);
                    // the address object associated with the connection can be changed. 
                    PeerAddress address = new PeerAddress(connection.PeersPublicAddress);
                    this.eventQueue.Schedule(delayInMilliseconds, this.SendStabilizationPingMessage, address);
                }
            }
        }

        /// <summary>
        /// Sends the stabilization ping message.
        /// </summary>
        /// <param name="address">The destination address.</param>
        private void SendStabilizationPingMessage(PeerAddress address)
        {
            TransportEnvelope envelope = this.GetMessageFor(address, QualityOfService.Reliable);
            this.RemoteCall(envelope, this.HandleStabilizationPingMessage);
            this.SendMessage(envelope);
        }

        /// <summary>
        /// Gets the stabilization task interval based on the type of peers. 
        /// </summary>
        /// <returns>The interval of stabilization task.</returns>
        private TimeSpan GetStabilizationTaskInterval()
        {
            try
            {
                foreach (Assembly asm in AppDomain.CurrentDomain.GetAssemblies())
                {
                    // running with peer harness. service peer. 
                    if (asm.GetType("GermHarness.PeerHarness") != null)
                    {
                        return Parameters.ServicePeerStabilizationTaskInterval;
                    }
                }

                // normal peer. 
                return Parameters.NormalStabilizationTaskInterval;
            }
            catch
            {
                return Parameters.NormalStabilizationTaskInterval;
            }
        }
    }
}
