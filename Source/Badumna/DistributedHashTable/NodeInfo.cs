using System;
using System.Collections.Generic;
using System.Text;

using Badumna.Core;
using Badumna.Utilities;

namespace Badumna.DistributedHashTable
{
    class NodeInfo
    {
        private HashKey mKey;
        public HashKey Key { get { return this.mKey; } }

        private HashKey mForwardDistance;
        public HashKey ForwardDistance { get { return this.mForwardDistance; } }

        private HashKey mBackwardDistance;
        public HashKey BackwardDistance { get { return this.mBackwardDistance; } }

        private PeerAddress mAddress;
        public PeerAddress Address { get { return this.mAddress; } }

        public NodeInfo(PeerAddress address, HashKey localKey)
        {
            if (null == address)
            {
                throw new ArgumentNullException("address");
            }

            if (null == localKey)
            {
                throw new ArgumentNullException("localKey");
            }

            this.mAddress = address;
            this.mKey = HashKey.Hash(address);

            this.mForwardDistance = this.mKey.DistanceFrom(localKey);
            this.mBackwardDistance = localKey.DistanceFrom(this.mKey);

            /*
            if (this.Key.Succeeds(localKey))
            {
                this.mForwardDistance = this.mKey.AbsoluteDistanceFrom(localKey);
                this.mBackwardDistance = HashKey.Max.Subtract(this.ForwardDistance);
                this.mPreceedsLocalNode = false;
            }
            else
            {
                this.mBackwardDistance = this.Key.AbsoluteDistanceFrom(localKey);
                this.mForwardDistance = HashKey.Max.Subtract(this.BackwardDistance);
                this.mPreceedsLocalNode = true;
            }*/
        }

        public HashKey Distance(bool inForwardDirection)
        {
            if (inForwardDirection)
            {
                return this.ForwardDistance;
            }

            return this.BackwardDistance;
        }

        public override int GetHashCode()
        {
            return this.Address.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            NodeInfo asNodeInfo = obj as NodeInfo;
            if (null != asNodeInfo)
            {
                return asNodeInfo.Address.Equals(this.Address);
            }

            PeerAddress asPeerAddress = obj as PeerAddress;
            if (null != asPeerAddress)
            {
                return asPeerAddress.Equals(this.Address);
            }

            return false;
        }
    }
}
