using System;
using System.Collections.Generic;
using System.Text;

using Badumna.Core;
using Badumna.Utilities;
using Badumna;
using Badumna.DataTypes;

namespace Badumna.DistributedHashTable
{
    interface IReplicatedObject : IParseable, IExpireable
    {
        BadumnaId Guid 
        { 
            get; set; 
        }

        CyclicalID.UShortID ModificationNumber 
        { 
            get; set; 
        }
        
        bool IgnoreModificationNumber 
        { 
            get;
        }
        
        HashKey Key 
        { 
            get; set; 
        }
        
        uint TimeToLiveSeconds 
        { 
            get; set; 
        }
        
        int NumberOfReplicas 
        { 
            get; 
        }
    }
}
