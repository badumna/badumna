using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;

using Badumna.Transport;
using Badumna.Core;

namespace Badumna.DistributedHashTable
{
    class DhtEnvelope : TransportEnvelope
    {
        private HashKey mDestinationKey;
        public HashKey DestinationKey { get { return this.mDestinationKey; } }

        public DhtEnvelope() { }

        public DhtEnvelope(TransportEnvelope transportEnvelope)
            : base(transportEnvelope) { }

        public DhtEnvelope(HashKey destinationKey, QualityOfService qos)
        {
            this.Qos = qos;
            this.mDestinationKey = destinationKey;
        }

        internal void SetDestinationKey(HashKey key)
        {
            this.mDestinationKey = key;
        }

        #region IParseable Members

        public override void ToMessage(MessageBuffer message, Type parameterType)
        {
            if (null != message)
            {
                base.ToMessage(message, typeof(TransportEnvelope));

                if (parameterType == typeof(DhtEnvelope))
                {
                    Debug.Assert(null != this.mDestinationKey, "Attempt to write a dht envelope to a message without a valid destination key.");
                    message.Write(this.mDestinationKey);
                    if (null == this.Source)
                    {
                        message.Write(PeerAddress.Nowhere);
                    }
                    else
                    {
                        message.Write(this.Source);
                    }
                }
            }
        }

        public override void FromMessage(MessageBuffer message)
        {
            if (null != message)
            {
                base.FromMessage(message);
                this.mDestinationKey = message.Read<HashKey>();
                this.Source = message.Read<PeerAddress>();
            }
        }

        #endregion
    }
}
