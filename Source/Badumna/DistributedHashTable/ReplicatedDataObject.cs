using System;
using System.Collections.Generic;
using System.Text;

using Badumna.Core;
using Badumna.Utilities;
using Badumna;
using Badumna.DataTypes;

namespace Badumna.DistributedHashTable
{
    class ReplicatedDataObject : IReplicatedObject
    {
        private bool ignoreModificationNumber = false;
        public bool IgnoreModificationNumber
        {
            get { return this.ignoreModificationNumber; }
        }

        private CyclicalID.UShortID mModificationNumber;
        public CyclicalID.UShortID ModificationNumber
        {
            get { return this.mModificationNumber; }
            set { this.mModificationNumber = value; }
        }

        private BadumnaId mGuid;
        public BadumnaId Guid
        {
            get { return this.mGuid; }
            set { this.mGuid = value; }
        }

        private HashKey mObjectKey;
        public HashKey Key
        {
            get { return this.mObjectKey; }
            set { this.mObjectKey = value; }
        }

        private uint mTimeToLiveSeconds;
        public uint TimeToLiveSeconds
        {
            get { return this.mTimeToLiveSeconds; }
            set { this.mTimeToLiveSeconds = value; }
        }

        private TimeSpan mExpirationTime = new TimeSpan();
        public TimeSpan ExpirationTime
        {
            get { return this.mExpirationTime; }
            set { this.mExpirationTime = value; }
        }

        private String mData = String.Empty;
        public String Data
        {
            get { return this.mData; }
            set { this.mData = value; }
        }

        public int NumberOfReplicas { get { return 5; } }

        public ReplicatedDataObject()
        {           
        }

        public ReplicatedDataObject(String key, BadumnaId id)
        {
            this.mGuid = id;
            this.mObjectKey = HashKey.Hash(key);          
        }

        public void FromMessage(MessageBuffer message)
        {
            if (null != message)
            {
                this.mData = message.ReadString();
                this.mGuid = message.Read<BadumnaId>();
            }
        }

        public void ToMessage(MessageBuffer message, Type parameterType)
        {
            if (null != message)
            {
                message.Write(this.mData);
                message.Write(this.mGuid);
            }
        }
    }
}
