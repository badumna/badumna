using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

using Badumna.Core;

namespace Badumna.DistributedHashTable
{
    [Serializable]
    class DhtException : BadumnaException
    {
        public DhtException() 
            : base() { }
        
        public DhtException(String message)
            : base(message) {}
        
        public DhtException(String message, Exception innerException) 
            : base(message, innerException) { }

        protected DhtException(SerializationInfo serializationInfo, StreamingContext streamingContext)
            : base(serializationInfo, streamingContext) {}      
    }

    [Serializable]
    class InvalidReplicaTypeException : DhtException
    {
        public InvalidReplicaTypeException() 
            : base() { }
        
        public InvalidReplicaTypeException(String message)
            : base(message) {}

        public InvalidReplicaTypeException(String message, Exception innerException) 
            : base(message, innerException) { }

        protected InvalidReplicaTypeException(SerializationInfo serializationInfo, StreamingContext streamingContext)
            : base(serializationInfo, streamingContext) {}

        public InvalidReplicaTypeException(Type type)
            : base(String.Format("Replica type {0} is invalid. Must be {1}", type, typeof(IReplicatedObject))) { }           
    }
}
