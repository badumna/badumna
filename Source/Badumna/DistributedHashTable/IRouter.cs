using System;
using System.Collections.Generic;
using System.Text;

using Badumna.Core;
using Badumna.Utilities;

namespace Badumna.DistributedHashTable
{
    interface IRouter
    {
        bool IsInitialized { get; }
        bool IsRouting { get; }
        HashKey LocalHashKey { get; }
        int UniqueNeighbourCount { get; }

        NodeInfo[] GetImmediateNeighbours();
        NodeInfo[] GetRoutingTableNodes();
        bool MapsToLocalPeer(HashKey key, ICollection<PeerAddress> excludedNodes);

        void RouteMessage(DhtEnvelope messageEnvelope);
        void RouteMessage(DhtEnvelope payloadMessage, IList<PeerAddress> excludedAddresses);
        void DirectSend(Transport.TransportEnvelope messageEnvelope);
        void Broadcast(Transport.TransportEnvelope messageEnvelope);
    }
}
