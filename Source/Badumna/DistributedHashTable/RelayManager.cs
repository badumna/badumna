﻿using System;
using System.Collections.Generic;
using System.Text;

using Badumna.Core;
using Badumna.Utilities;
using Badumna.DistributedHashTable;

namespace Badumna.Transport
{
    interface IRelayManager
    {
        event EventHandler<RelayMessageArrivalEventArgs> MessageArrivalEvent;
        void BuildRoutePath(PeerAddress destination);
        void TearDownRoutePath(PeerAddress destination);
        void RelayMessage(TransportEnvelope payloadEnvelope, bool useReliableTransport);
        void RelayMessage(TransportEnvelope payloadEnvelope, bool useReliableTransport, int redundancyFactor);
    }

    class RelayMessageArrivalEventArgs : EventArgs
    {
        private TransportEnvelope mMessageEnvelope;
        public TransportEnvelope MessageEnvelope { get { return this.mMessageEnvelope; } }
        public PeerAddress Source { get { return this.MessageEnvelope.Source; } }

        public RelayMessageArrivalEventArgs(TransportEnvelope messageEnvelope)
        {
            this.mMessageEnvelope = messageEnvelope;
        }
    }

    class RelayManager : DhtProtocol, IRelayManager
    {
        private EventHandler<RelayMessageArrivalEventArgs> messageArrivalEventDelegate;

        public event EventHandler<RelayMessageArrivalEventArgs> MessageArrivalEvent
        {
            add { this.messageArrivalEventDelegate += value; }
            remove { this.messageArrivalEventDelegate -= value; }
        }

        private DhtFacade mFacade;
        private IConnectionTable mConnectionTable;
        
        /// <summary>
        /// The key is the destination addrss, the value is a ReplayerProxies object which contains a list of proxies. 
        /// </summary>
        private Dictionary<PeerAddress, RelayerProxies> mProxies;
        private RegularTask mPingProxiesTask;

        private RegularTask mCheckRelayerTask;

        private RegularTask mGarbageCollectionTask;

        /// <summary>
        /// The queue to use for scheduling events.
        /// </summary>
        private NetworkEventQueue eventQueue;

        /// <summary>
        /// Provides access to the current time.
        /// </summary>
        private ITime timeKeeper;

        /// <summary>
        /// Provides the public peer address.
        /// </summary>
        private INetworkAddressProvider addressProvider;

        // ******************************************************************************************************************

        public RelayManager(
            DhtFacade facade,
            IConnectionTable connectionTable,
            NetworkEventQueue eventQueue,
            ITime timeKeeper,
            INetworkAddressProvider addressProvider,
            INetworkConnectivityReporter connectivityReporter,
            IPeerConnectionNotifier connectionNotifier)
            : base(facade)
        {
            this.eventQueue = eventQueue;
            this.timeKeeper = timeKeeper;
            this.addressProvider = addressProvider;

            this.mFacade = facade;
            this.mConnectionTable = connectionTable;
            this.mProxies = new Dictionary<PeerAddress, RelayerProxies>();
            connectionNotifier.ConnectionLostEvent += this.CurrentContext_ConnectionLostEvent;
            connectionNotifier.ConnectionEstablishedEvent += CurrentContext_ConnectionEstablishedEvent;

            // TODO:
            // to properly shut down these tasks, flush the queued message if possible. 
            this.mPingProxiesTask = new RegularTask("Ping relay proxies", Parameters.PingProxiesTaskPeriod, eventQueue, connectivityReporter, this.PingProxies);
            this.mPingProxiesTask.Start();

            this.mCheckRelayerTask = new RegularTask("Check relayer task", Parameters.CheckRelayerTaskInterval, eventQueue, connectivityReporter, this.CheckRelayerTask);
            this.mCheckRelayerTask.Start();

            this.mGarbageCollectionTask = new RegularTask("RelayProxyGC", Parameters.RelayManagerGarbageCollectionTaskInterval, eventQueue, connectivityReporter, this.GarbageCollection);
            this.mGarbageCollectionTask.Start();
        }

        // ******************************************************************************************************************

        public void BuildRoutePath(PeerAddress destination)
        {
            RelayerProxies proxyList = null;

            if (!this.mProxies.TryGetValue(destination, out proxyList))
            {
                proxyList = new RelayerProxies(destination, this.mConnectionTable, this.timeKeeper);
                this.mProxies.Add(destination, proxyList);
            }

            this.SendFindRelayers(proxyList, Parameters.NumberOfRelayProxiesRequired);
        }

        public void TearDownRoutePath(PeerAddress destination)
        {
            RelayerProxies proxyList = null;
            if (this.mProxies.TryGetValue(destination, out proxyList))
            {
                proxyList.ShutDown();
                this.mProxies.Remove(destination);
            }
        }

        public void RelayMessage(TransportEnvelope payloadEnvelope, bool useReliableTransport, int redundancyFactor)
        {
            bool isRealtimeMessage = false;
            if (payloadEnvelope.Qos != null)
            {
                isRealtimeMessage = payloadEnvelope.Qos.IsRealtimeMessage;
            }

            System.Diagnostics.Debug.Assert(payloadEnvelope.IsEncrypted, "Attempting to relay an unencrypted message.");
            payloadEnvelope.Qos = useReliableTransport ? QualityOfService.Reliable : QualityOfService.Unreliable;
            payloadEnvelope.Qos.IsRealtimeMessage = isRealtimeMessage;

            this.RelayMessage(payloadEnvelope, this.addressProvider.PublicAddress, redundancyFactor);
        }

        public void RelayMessage(TransportEnvelope payloadEnvelope, bool useReliableTransport)
        {
            this.RelayMessage(payloadEnvelope, useReliableTransport, 1);
        }

        // ******************************************************************************************************************

        private bool HasDirectConnection(PeerAddress address)
        {
            return this.mConnectionTable.IsConnected(address) && !this.mConnectionTable.IsRelayed(address);
        }

        private void RelayMessage(TransportEnvelope payloadEnvelope, PeerAddress source)
        {
            this.RelayMessage(payloadEnvelope, source, 1);
        }

        private void RelayMessage(TransportEnvelope payloadEnvelope, PeerAddress source, int redundancyFactor)
        {
            if(redundancyFactor < 1)
            {
                throw new ArgumentException("RelayMessage");
            }

            RelayerProxies proxyList = null;
            bool useReliableTransport = payloadEnvelope.Qos.IsReliable;

            if (this.mProxies.TryGetValue(payloadEnvelope.Destination, out proxyList))
            {
                // A route path exists so use it
                PeerAddress relayerAddress = proxyList.FindAProxy();
                if (relayerAddress != null)
                {
                    DhtEnvelope envelope = this.GetRelayEnvelope(relayerAddress, payloadEnvelope, useReliableTransport);

                    this.RemoteCall(envelope, this.RelayMessage, payloadEnvelope, source, payloadEnvelope.Destination, useReliableTransport);
                    this.SendMessage(envelope);
                }
                else
                {
                    payloadEnvelope.Source = source;
                    proxyList.QueueMessage(payloadEnvelope);

                    this.SendFindRelayers(proxyList, Parameters.NumberOfRelayProxiesRequired);
                }
            }
            else
            {
               // No path exists so use a one off relay message request
                QualityOfService qos = useReliableTransport ? QualityOfService.Reliable : QualityOfService.Unreliable;
                qos.HandleAlongRoutePath = true; // Ask intermediary peers to handle the message if they can.
                qos.MustBeInOrder = false;
                DhtEnvelope relayEnvelope;
                if (redundancyFactor == 1)
                {
                    relayEnvelope = this.GetMessageFor(HashKey.Hash(payloadEnvelope.Destination), qos);
                }
                else
                {
                    relayEnvelope = this.GetMessageFor(HashKey.Hash(payloadEnvelope.Destination), qos, redundancyFactor);
                    relayEnvelope.Qos.HandleAlongRoutePath = true;
                    relayEnvelope.Qos.MustBeInOrder = false;
                }
#if TRACE
                relayEnvelope.AppendLabel(String.Format(" relay<{0}> ", payloadEnvelope.Label));
#endif
                this.RemoteCall(relayEnvelope, this.RelayOneOffMessage, payloadEnvelope, source, payloadEnvelope.Destination, useReliableTransport);
                this.RouteMessage(relayEnvelope, source, this.addressProvider.PublicAddress);
            }
        }

        private void DeliverMessage(TransportEnvelope payloadEnvelope, PeerAddress source, PeerAddress destination, bool useReliableTransport)
        {
            DhtEnvelope relayEnvelope = this.GetRelayEnvelope(destination, payloadEnvelope, useReliableTransport);

            this.RemoteCall(relayEnvelope, this.RelayedMessageArrival, payloadEnvelope, source);
            this.SendMessage(relayEnvelope);
        }

        private void RouteMessage(DhtEnvelope envelope, params PeerAddress[] excludedAddressParams)
        {
            List<PeerAddress> excludedAddresses = new List<PeerAddress>(excludedAddressParams);

            this.mFacade.RouteMessage(envelope, excludedAddresses);
        }

        private void CurrentContext_ConnectionEstablishedEvent(PeerAddress address)
        {
            RelayerProxies proxyList = null;

            // a direct connection (non-relayed) has been established, proxies are thus no longer required. 
            if (this.mProxies.TryGetValue(address, out proxyList) && !this.mConnectionTable.IsRelayed(address))
            {
                TransportEnvelope payloadEnvelope = proxyList.PopQueuedMessage();
                while (null != payloadEnvelope)
                {
                    this.DeliverMessage(payloadEnvelope, payloadEnvelope.Source, address, payloadEnvelope.Qos.IsReliable);
                    payloadEnvelope = proxyList.PopQueuedMessage();
                }

                this.mProxies.Remove(address);
            }
        }

        private void CurrentContext_ConnectionLostEvent(PeerAddress address)
        {
            foreach (RelayerProxies proxyList in this.mProxies.Values)
            {
                proxyList.RemoveProxy(address);

                if (proxyList.Count == 0)
                {
                    this.SendFindRelayers(proxyList, Parameters.NumberOfRelayProxiesRequired);
                }
            }
        }

        private void GarbageCollection()
        {
            foreach (RelayerProxies proxy in this.mProxies.Values)
            {
                proxy.GarbageCollection();
            }
        }

        private void CheckRelayerTask()
        {
            DhtEnvelope envelope = this.GetMessageFor(HashKey.Hash(this.addressProvider.PublicAddress), QualityOfService.Reliable, 2);
            this.RemoteCall(envelope, this.CheckRelayer, this.addressProvider.PublicAddress, true);
            this.SendMessage(envelope);
        }

        private void PingProxies()
        {
            foreach (RelayerProxies proxyList in this.mProxies.Values)
            {
                PeerAddress anyProxy = proxyList.FindAProxy();
                if (anyProxy == null)
                {
                    this.SendFindRelayers(proxyList, Parameters.NumberOfRelayProxiesRequired);
                }
                else
                {
                    this.mConnectionTable.ConfirmConnection(anyProxy);
                }
            }
        }

        private DhtEnvelope GetRelayEnvelope(PeerAddress relayerAddress, TransportEnvelope payloadEnvelope, bool reliableTransport)
        {
            QualityOfService qos = reliableTransport ? QualityOfService.Reliable : QualityOfService.Unreliable;
            qos.MustBeInOrder = false;
            DhtEnvelope relayEnvelope = this.GetMessageFor(relayerAddress, qos);

#if TRACE
            relayEnvelope.AppendLabel(String.Format(" relay<{0}> ", payloadEnvelope.Label));
#endif

            return relayEnvelope;
        }

        private void SendUnavailableReply(PeerAddress source, PeerAddress destination, TransportEnvelope payloadEnvelope)
        {
            DhtEnvelope envelope = this.GetMessageFor(source, QualityOfService.Reliable);

            this.RemoteCall(envelope, this.RouteUnavailable, destination, payloadEnvelope);
            this.SendMessage(envelope);
        }

        private void SendDirectRouteAvailable(PeerAddress source)
        {

        }

        private void SendFindRelayers(RelayerProxies proxyList, int number)
        {
            if (this.timeKeeper.Now - proxyList.TimeOfLastQuery > Parameters.MinimumFindRelayersQueryInterval)
            {
                HashKey key = HashKey.Hash(proxyList.Destination);
                DhtEnvelope envelope = this.GetMessageFor(key, QualityOfService.Reliable);

                this.RemoteCall(envelope, this.RoutedFindRelayerFor, proxyList.Destination, (byte)number);
                this.RouteMessage(envelope, proxyList.Destination, this.addressProvider.PublicAddress);
                proxyList.TimeOfLastQuery = this.timeKeeper.Now;
            }
        }

        [DhtProtocolMethod(DhtMethod.RelayManagerRoutedFindRelayerFor)]
        private void RoutedFindRelayerFor(PeerAddress address, byte numberRequired)
        {
            this.PropogateQuery(address, (byte)(numberRequired / 2), this.CurrentEnvelope.Source);

            if (!address.Equals(this.addressProvider.PublicAddress) &&
                !this.CurrentEnvelope.Source.Equals(this.addressProvider.PublicAddress)
                && this.HasDirectConnection(address))
            {
                DhtEnvelope envelope = this.GetMessageFor(this.CurrentEnvelope.Source, QualityOfService.Reliable);

                this.RemoteCall(envelope, this.RelayerAvailable, address);
                this.SendMessage(envelope);
            }
        }

        private void PropogateQuery(PeerAddress address, byte numberRequired, PeerAddress source)
        {
            PeerAddress immediateSource = this.CurrentEnvelope.Source;
            NodeInfo[] relayerCandidates = this.mFacade.GetImmediateNeighbours();

            if (relayerCandidates[0] == null && relayerCandidates[1] == null)
            {
                Logger.TraceInformation(LogTag.DHT, "Unable to find any neighbours to propogate find relayers query to.");
                return;
            }

            if (relayerCandidates[0] != null && relayerCandidates[1] != null
                && relayerCandidates[0].Address.Equals(relayerCandidates[1].Address))
            {
                relayerCandidates[1] = null;
            }

            foreach (NodeInfo node in relayerCandidates)
            {
                if (node != null && (immediateSource == null || !node.Address.Equals(immediateSource)))
                {
                    DhtEnvelope envelope = this.GetMessageFor(node.Address, QualityOfService.Reliable);

                    this.RemoteCall(envelope, this.PropogatedFindRelayerFor, address, source, (byte)(numberRequired - 1));
                    this.SendMessage(envelope);
                }
            }
        }

        [DhtProtocolMethod(DhtMethod.CheckRelayer)]
        private void CheckRelayer(PeerAddress source, bool checkCandidates)
        {
            if (!source.Equals(this.addressProvider.PublicAddress))
            {
                if (!this.HasDirectConnection(source))
                {
                    this.mConnectionTable.ConfirmConnection(source);
                }
            }

            if (checkCandidates)
            {
                NodeInfo[] relayerCandidates = this.mFacade.GetImmediateNeighbours();
                
                if (relayerCandidates != null)
                {
                    if (relayerCandidates[0] != null && relayerCandidates[1] != null
                        && relayerCandidates[0].Address.Equals(relayerCandidates[1].Address))
                    {
                        relayerCandidates[1] = null;
                    }

                    foreach (NodeInfo node in relayerCandidates)
                    {
                        if (node != null && (source == null || !node.Address.Equals(source)))
                        {
                            DhtEnvelope envelope = this.GetMessageFor(node.Address, QualityOfService.Reliable);

                            this.RemoteCall(envelope, this.CheckRelayer, source, false);
                            this.SendMessage(envelope);
                        }
                    }
                }
            }
        }

        [DhtProtocolMethod(DhtMethod.RelayManagerPropogatedFindRelayerFor)]
        private void PropogatedFindRelayerFor(PeerAddress address, PeerAddress source, byte numberRequired)
        {
            if (!address.Equals(this.addressProvider.PublicAddress) &&
                !source.Equals(this.addressProvider.PublicAddress)
                && this.HasDirectConnection(address))
            {
                DhtEnvelope envelope = this.GetMessageFor(source, QualityOfService.Reliable);

                this.RemoteCall(envelope, this.RelayerAvailable, address);
                this.SendMessage(envelope);
            }

            if (numberRequired > 0
                && !address.Equals(this.addressProvider.PublicAddress)) // The propogation has gone all the way round the dht.
            {
                this.PropogateQuery(address, (byte)(numberRequired - 1), source);
            }
        }

        [DhtProtocolMethod(DhtMethod.RelayManagerRelayerAvailable)]
        private void RelayerAvailable(PeerAddress address)
        {
            RelayerProxies proxyList = null;

            if (this.mProxies.TryGetValue(address, out proxyList))
            {
                proxyList.AddProxy(this.CurrentEnvelope.Source);

                // now the relayer is available, flush all queued messages. 
                if (proxyList.QueuedMessages.Count > 0)
                {
                    Queue<QueuedMessage> queue = proxyList.QueuedMessages;
                    proxyList.CreateNewQueue();

                    while (queue.Count > 0)
                    {
                        TransportEnvelope queuedMessageEnvelope = queue.Dequeue().Envelope;
                        this.RelayMessage(queuedMessageEnvelope, queuedMessageEnvelope.Qos.IsReliable);
                    }
                }
            }
            else
            {
                Logger.TraceWarning(LogTag.DHT, "Received relayer available message for unknown destination from {0}.", this.CurrentEnvelope.Source);
            }
        }

        // Relay a message to a destination peer, maintaining a persistent path (repair it if necessary)
        [DhtProtocolMethod(DhtMethod.RelayManagerRelayMessage)]
        private void RelayMessage(TransportEnvelope payloadEnvelope, PeerAddress source, PeerAddress destination, bool useReliableTransport)
        {
            if (destination.Equals(this.addressProvider.PublicAddress))
            {
                this.eventQueue.Push(this.RelayedMessageArrival, payloadEnvelope, source);
                return;
            }

            if (!this.HasDirectConnection(destination))
            {
                // For some reason the immediate source of this message incorrectly thinks this peer has a connection,
                // so return the message.
                this.SendUnavailableReply(this.CurrentEnvelope.Source, destination, payloadEnvelope);
                return;
            }

            if (source.Equals(this.addressProvider.PublicAddress))
            {
                Logger.TraceError(LogTag.DHT, "Request to relay message to peer {0} has been delivered to the source.", destination);
                return;
            }

            Logger.TraceInformation(LogTag.DHT, "Relay request from {0} to {1}", source, destination);

            this.DeliverMessage(payloadEnvelope, source, destination, useReliableTransport);
        }

        // Relay a message to a destination peer without trying to form a persistent path.
        [DhtProtocolMethod(DhtMethod.RelayManagerRelayOneOffMessage)]
        private void RelayOneOffMessage(TransportEnvelope payloadEnvelope, PeerAddress source, PeerAddress destination, bool useReliableTransport)
        {
            if (source.Equals(this.addressProvider.PublicAddress))
            {
                if (this.HasDirectConnection(destination))
                {
                    this.DeliverMessage(payloadEnvelope, source, destination, useReliableTransport);
                    return;
                }

                Logger.TraceWarning(LogTag.DHT, "Relay one off has arrived at source. Dropping message");
                return;
            }

            if (destination.Equals(this.addressProvider.PublicAddress))
            {
                this.eventQueue.Push(this.RelayedMessageArrival, payloadEnvelope, source);
                return;
            }

            if (!this.HasDirectConnection(destination))
            {
                bool isClosestNode = this.mFacade.MapsToLocalPeer(this.CurrentEnvelope.DestinationKey, new PeerAddress[] { destination });
                if (isClosestNode)
                {
                    // We are the closest node and not connected and since it is likely that we will get requests for this address in the 
                    // future we should ensure that we form a connection to it. 
                    this.mConnectionTable.ConfirmConnection(destination);
                }
                else
                {
                    this.CurrentEnvelope.Qos.HandleAlongRoutePath = true; // Indicate to continue routing the message
                    return;
                }
            }

            Logger.TraceInformation(LogTag.DHT, "Relay request from {0} to {1}", this.CurrentEnvelope.Source, destination);

            this.DeliverMessage(payloadEnvelope, source, destination, useReliableTransport);
            this.CurrentEnvelope.Qos.HandleAlongRoutePath = false; // Indicate that we don't require routing of the message to continue
        }

        [DhtProtocolMethod(DhtMethod.RelayManagerRelayedMessageArrival)]
        private void RelayedMessageArrival(TransportEnvelope messageEnvelope, PeerAddress source)
        {
            messageEnvelope.ArrivalTime = this.timeKeeper.Now;
            messageEnvelope.IsEncrypted = true;
            messageEnvelope.Source = source;
            messageEnvelope.SetDestination(this.addressProvider.PublicAddress);
            EventHandler<RelayMessageArrivalEventArgs> handler = this.messageArrivalEventDelegate;
            if (handler != null)
            {
                handler.Invoke(this, new RelayMessageArrivalEventArgs(messageEnvelope));
            }
        }

        [DhtProtocolMethod(DhtMethod.RelayManagerRouteUnavailable)]
        private void RouteUnavailable(PeerAddress destination, TransportEnvelope payloadEnvelope)
        {
            RelayerProxies proxyList = null;

            if (this.mProxies.TryGetValue(destination, out proxyList))
            {
                proxyList.RemoveProxy(this.CurrentEnvelope.Source);

                Logger.TraceInformation(LogTag.DHT, "Resending relay message to {0}", destination);
                payloadEnvelope.SetDestination(destination);
                payloadEnvelope.IsEncrypted = true;
                this.RelayMessage(payloadEnvelope, this.addressProvider.PublicAddress);
            }
            else
            {
                Logger.TraceWarning(LogTag.DHT, "Received route unavailable message for unknown destination from {0}.", this.CurrentEnvelope.Source);
            }
        }
    }
}
