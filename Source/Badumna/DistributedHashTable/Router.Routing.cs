using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;

using Badumna.Core;
using Badumna.Utilities;
using Badumna.Transport;

namespace Badumna.DistributedHashTable
{
    partial class Router
    {

        public void RouteMessage(DhtEnvelope messageEnvelope)
        {
            this.RouteMessage(messageEnvelope, null);
        }

        public void RouteMessage(DhtEnvelope messageEnvelope, IList<PeerAddress> excludedAddresses)
        {
            Debug.Assert(null != messageEnvelope, "A null mesage envelope has been passed to RouteMessage");
            bool useRedundency = false;
            int redundancyFactor = 0;

            // check whether redundency is requested for this message.
            if(messageEnvelope.Qos != null && messageEnvelope.Qos is RouteableQos<DhtEnvelope>)
            {
                redundancyFactor = ((RouteableQos<DhtEnvelope>)messageEnvelope.Qos).RedundencyFactor;
                useRedundency = (redundancyFactor > 1);
            }

            // Set the new quality of service that includes the list of excluded addresses and the error handling delegate.
            messageEnvelope.Qos = new RouteableQos<DhtEnvelope>(messageEnvelope, excludedAddresses, this.FailedRouteAttempt);
            messageEnvelope.Source = this.addressProvider.PublicAddress;

            if (!useRedundency)
            {
                this.StartProcessMessage(messageEnvelope, excludedAddresses);
            }
            else
            {
                for (int i = 1; i < redundancyFactor; i++)
                {
                    DhtEnvelope redundentEnvelope = new DhtEnvelope(messageEnvelope);
                    HashKey destination = new HashKey(messageEnvelope.DestinationKey.ToBytes());
                    // get the key used by the redundancy replica. 
                    destination.AdjustToReplicaKey(redundancyFactor, i);
                    redundentEnvelope.SetDestinationKey(destination);
                    redundentEnvelope.Qos = new RouteableQos<DhtEnvelope>(redundentEnvelope, excludedAddresses, this.FailedRouteAttempt);

                    this.StartProcessMessage(redundentEnvelope, excludedAddresses);
                }

                this.StartProcessMessage(messageEnvelope, excludedAddresses);
            }
        }

        private void StartProcessMessage(DhtEnvelope messageEnvelope, IList<PeerAddress> excludedAddresses)
        {
            // Store the envelope (if required) until routing is enabled.
            if (!this.mRouteRequestHandlingEnabled)
            {
                this.mDelayedRouteRequests.Add(messageEnvelope);
                Logger.TraceInformation(LogTag.DHT, "Delaying dht message to {0} until routing enabled", messageEnvelope.DestinationKey);
            }
            else
            {
                this.ProcessMessage(messageEnvelope, excludedAddresses, false);
            }
        }

        public void DirectSend(TransportEnvelope messageEnvelope)
        {
            if (null == messageEnvelope.Destination)
            {
                throw new ArgumentNullException("destination");
            }

            if (this.addressProvider.PublicAddress.Equals(messageEnvelope.Destination))
            {
                Logger.RecordStatistic("Transport", "DhtDirectLoopbackBytes", messageEnvelope.Length);
                Logger.RecordStatistic("Transport", "DhtDirectLoopbackPackets", 1.0);
                messageEnvelope.Source = this.addressProvider.PublicAddress;
                this.eventQueue.Push(this.mChildRouter.ProcessMessage, new DhtEnvelope(messageEnvelope));
                return;
            }

            Logger.TraceInformation(LogTag.DHT, "Sending direct message to {0}", messageEnvelope.Destination);

            TransportEnvelope directEnvelope = this.GetMessageFor(messageEnvelope.Destination, messageEnvelope.Qos);

#if TRACE
            directEnvelope.SetLabel(directEnvelope.Label + messageEnvelope.Label);
#endif
            this.RemoteCall(directEnvelope, this.HandleDirectMessage, messageEnvelope);
            this.SendMessage(directEnvelope);
        }

        // ******************************************************************************************************************

        // Protocol methods need to be protected so they are visible to the MessageParser in derived classes
        #region Protocol methods

        [RouterProtocolMethod(RouterMethod.RouterHandleRouteRequest)]
        protected void HandleRouteRequest(DhtEnvelope messageEnvelope)
        {
            Debug.Assert(null != messageEnvelope, "A null mesage envelope has been received");
            Debug.Assert(null != messageEnvelope.Source && messageEnvelope.Source.IsValid, "Invalid source address attached to routed message.");

            // Add a routeable qos to the envelope to ensure that it has an exclusion list and error handler.
            messageEnvelope.Qos = new RouteableQos<DhtEnvelope>(messageEnvelope, null, this.FailedRouteAttempt);

            // Queue the message until ready if necessary.
            if (!this.mRouteRequestHandlingEnabled)
            {
                this.mDelayedRouteRequests.Add(messageEnvelope);
                return;
            }

            List<PeerAddress> excludedAddress = new List<PeerAddress>();

            excludedAddress.Add(messageEnvelope.Source);
            excludedAddress.Add(this.CurrentEnvelope.Source);
            this.ConsiderAddressForRoutingTableInsertion(messageEnvelope.Source);
            this.ProcessMessage(messageEnvelope, excludedAddress, false);
        }

        [RouterProtocolMethod(RouterMethod.RouterHandleDirectMessage)]
        protected void HandleDirectMessage(TransportEnvelope messageEnvelope)
        {
            messageEnvelope.Source = this.CurrentEnvelope.Source;
            messageEnvelope.SetDestination(this.CurrentEnvelope.Destination);

            DhtEnvelope dhtEnvelope = new DhtEnvelope(messageEnvelope);

            if (messageEnvelope.Destination.HashAddress != null)
            {
                dhtEnvelope.SetDestinationKey(messageEnvelope.Destination.HashAddress);
            }

            this.ConsiderAddressForRoutingTableInsertion(messageEnvelope.Source);
            this.mChildRouter.ProcessMessage(dhtEnvelope);
        }

        #endregion

        // ******************************************************************************************************************

        private void ProcessMessage(DhtEnvelope messageEnvelope, IList<PeerAddress> excludedAddresses, bool useOnlyLeafset)
        {
            Debug.Assert(messageEnvelope.Qos is RouteableQos<DhtEnvelope>, "ProcessMessage called with an inappropriate quality of service type");
            Debug.Assert(null != messageEnvelope.Source && messageEnvelope.Source.IsValid, "Invalid source address.");
            Debug.Assert(null != messageEnvelope.DestinationKey, "Invalid destination key.");

            Logger.TraceInformation(LogTag.DHT, "Processing route request from {0} to {1}", messageEnvelope.Source, messageEnvelope.DestinationKey);

            if (!this.IsCurrentlyParticipating(this.addressProvider.PublicAddress))
            {
                Logger.TraceInformation(LogTag.DHT, "Local node is not currently participating in this tier. Possibly quarantined.");

                // Exclude the local node because it is not part of the network.
                if (null == excludedAddresses)
                {
                    excludedAddresses = new List<PeerAddress>();
                }

                if (!excludedAddresses.Contains(this.addressProvider.PublicAddress))
                {
                    excludedAddresses.Add(this.addressProvider.PublicAddress);
                }
            }

            NodeInfo closestNode = null;

            if (useOnlyLeafset)
            {
                closestNode = this.mLeafSet.GetNodeClosestTo(messageEnvelope.DestinationKey, excludedAddresses);
            }
            else
            {
                closestNode = this.GetClosestNode(messageEnvelope.DestinationKey, excludedAddresses);
            }

            if (null == closestNode)
            {
                Logger.TraceInformation(LogTag.DHT, "Failed to find any candidate neighbours to route to. Dropping message.");
                return;
            }

#if TRACE
            for (int i = -this.mLeafSet.PredecessorCount; i <= this.mLeafSet.SuccessorCount; i++)
            {
                Logger.TraceInformation(LogTag.DHT, "r {0} \t {1}", this.mLeafSet[i].Address, this.mLeafSet[i].Key);
            }
#endif 

            if (this.IsLocal(closestNode))
            {
                // Handle the message or queue it until routing is enabled.
                messageEnvelope.ResetParseOffset();
                if (!this.mRouteRequestHandlingEnabled)
                {
                    this.mDelayedRouteRequests.Add(messageEnvelope);
                }
                else
                {
                    Debug.Assert(!this.mQuarantineList.Contains(this.addressProvider.PublicAddress),
                        "Processing message when quarantined.");
                    Logger.TraceInformation(LogTag.DHT, "Handling routed message from {0} locally", messageEnvelope.Source);

                    if (messageEnvelope.Source.Equals(this.addressProvider.PublicAddress))
                    {
                        Logger.RecordStatistic("Transport", "DhtRoutedLoopbackBytes", messageEnvelope.Length);
                        Logger.RecordStatistic("Transport", "DhtRoutedLoopbackPackets", 1.0);
                    }

#if TRACE
                    if (null != this.CurrentEnvelope)
                    {
                        messageEnvelope.SetLabel(messageEnvelope.Label + this.CurrentEnvelope.Label);
                    }
#endif
                    this.eventQueue.Push(this.HandleMessage, messageEnvelope);
                }
            }
            else
            {
                // Does the message need to be handled at each hop?
                if (!messageEnvelope.Source.Equals(this.addressProvider.PublicAddress) && messageEnvelope.Qos.HandleAlongRoutePath)
                {
                    this.HandleMessage(messageEnvelope);

                    // Check if the handle along path property has been changed.
                    if (!messageEnvelope.Qos.HandleAlongRoutePath)
                    {
                        return;
                    }

                    Logger.TraceInformation(LogTag.DHT, "Passing on routed message to {0}", closestNode.Address);
                }

                messageEnvelope.Qos.IsReliable = true;

                // Send a route request to the closest node.
                TransportEnvelope transportMessage = this.GetMessageFor(closestNode.Address, messageEnvelope.Qos);

#if TRACE
                transportMessage.SetLabel(transportMessage.Label + messageEnvelope.Label);
#endif
                this.RemoteCall(transportMessage, this.HandleRouteRequest, messageEnvelope);
                this.SendMessage(transportMessage);
            }
        }

        private void FailedRouteAttempt(PeerAddress neighbour, DhtEnvelope messageEnvelope, IList<PeerAddress> excludedAddresses)
        {
            Logger.TraceInformation(LogTag.DHT, "Failed route attempt to {0} trying alternate peer", neighbour);
            this.ProcessMessage(messageEnvelope, excludedAddresses, true);
            this.mConnectionTable.ConfirmConnection(neighbour);
        }

        private void HandleDelayedRouteRequest(DhtEnvelope messageEnvelope)
        {
            Debug.Assert(messageEnvelope.Qos is RouteableQos<DhtEnvelope>, "HandleDelayedRouteRequest called with an inappropriate quality of service type");

            RouteableQos<DhtEnvelope> qos = messageEnvelope.Qos as RouteableQos<DhtEnvelope>;

            if (null != qos)
            {
                // If the source is invalid then it must have been set before the public address was know on the local peer.
                if (null == messageEnvelope.Source || !messageEnvelope.Source.IsValid)
                {
                    messageEnvelope.Source = this.addressProvider.PublicAddress;
                }

                this.ProcessMessage(messageEnvelope, qos.ExcludedAddresses, false);
            }
            else
            {
                Logger.TraceError(LogTag.DHT, "Quality of service for delayed route message is the wrong type.");
            }
        }
    }
}
