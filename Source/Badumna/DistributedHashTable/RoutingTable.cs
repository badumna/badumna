using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using Badumna.Core;
using Badumna.Utilities;

namespace Badumna.DistributedHashTable
{
    class RoutingTable 
    {
        private List<NodeInfo>[,] mTable;

        private int mBitsPerDigit;
        private int mNumberOfRows;
        private int mNumberOfColumns;
        private NodeInfo mLocalNodeInfo;

        public RoutingTable(int bitsPerDigit, NodeInfo localNodeInfo)
        {
            this.Initialize(bitsPerDigit, localNodeInfo);
        }

        private void Initialize(int bitsPerDigit, NodeInfo localNodeInfo)
        {
            if (null == localNodeInfo)
            {
                throw new ArgumentNullException("localNodeInfo");
            }

            Debug.Assert(bitsPerDigit <= 8, "Bits per digit > 8");
            Debug.Assert(8 % bitsPerDigit == 0, "8 is not divisible by bitsPerDigit");

            int maxBits = HashKey.Length * 8;

            this.mBitsPerDigit = bitsPerDigit;
            this.mNumberOfRows = maxBits / bitsPerDigit;
            this.mNumberOfColumns = (int)Math.Pow(2, bitsPerDigit);
            this.mLocalNodeInfo = localNodeInfo;
            this.mTable = new List<NodeInfo>[this.mNumberOfRows, this.mNumberOfColumns];
        }        

        private IList<NodeInfo> TableEntryFor(HashKey key)
        {
            byte remainder;
            int row = key.FirstDifferentDigit(this.mLocalNodeInfo.Key, this.mBitsPerDigit, out remainder);
            int column = (int)remainder;

            if (row < 0)
            {
                Debug.Assert(key.Equals(this.mLocalNodeInfo.Key),
                    "The first different digit of the inserted node cannot be found and the given node is not the same as the local node");

                // The node must be the local node. Ignore.
                return null;
            }

            if (null == this.mTable[row, column])
            {
                this.mTable[row, column] = new List<NodeInfo>();
            }

            return this.mTable[row, column];
        }

        public List<PeerAddress> ConstructListOfPossibleEntriesFor(PeerAddress destination, int maximumNumberOfCandidates)
        {
            List<PeerAddress> candidateList = new List<PeerAddress>();
            int numberOfRows = Math.Max(this.mNumberOfRows, maximumNumberOfCandidates);

            for (int i = 0; i < numberOfRows; i++)
            {
                for (int j = 0; j < this.mNumberOfColumns; j++)
                {
                    if (null != this.mTable[i, j] && this.mTable[i, j].Count > 0)
                    {
                        candidateList.Add(this.mTable[i, j][0].Address);
                        break;
                    }
                }
            }

            return candidateList;
        }


        public bool IsCandidateForInsertion(PeerAddress address)
        {
            HashKey key = HashKey.Hash(address);
            IList<NodeInfo> entry = this.TableEntryFor(key);

            return null != entry && 0 == entry.Count;
        }

        public void InsertNodeInfo(NodeInfo nodeInfo)
        {
            IList<NodeInfo> entry = this.TableEntryFor(nodeInfo.Key);

            if (null != entry && !entry.Contains(nodeInfo))
            {
                entry.Add(nodeInfo);
            }
        }

        public void RemoveNodeInfo(NodeInfo nodeInfo)
        {
            IList<NodeInfo> entry = this.TableEntryFor(nodeInfo.Key);

            if (null != entry && entry.Contains(nodeInfo))
            {
                entry.Remove(nodeInfo);
            }
        }

        public NodeInfo BestCandidateForRouteTo(HashKey key)
        {
            // TODO : Needs to be optimised so that the connection with the smallest latency is used.
            IList<NodeInfo> entry = this.TableEntryFor(key);

            if (null == entry || 0 == entry.Count)
            {
                return null;
            }

            return entry[0];
        }

        public NodeInfo BestCandidateForRouteTo(HashKey key, ICollection<PeerAddress> excludedAddressList)
        {
            IList<NodeInfo> entry = this.TableEntryFor(key);

            if (null == entry || 0 == entry.Count)
            {
                return null;
            }

            foreach (NodeInfo nodeInfo in entry)
            {
                if (null == excludedAddressList || !excludedAddressList.Contains(nodeInfo.Address))
                {
                    Logger.TraceInformation(LogTag.DHT, "Candidate for route found in RoutingTable for key {0} : {1} ({2})", key, nodeInfo.Address, nodeInfo.Key);
                    return nodeInfo;
                }
            }

            return null;
        }

        public NodeInfo ClosestProceedingNode(HashKey key, ICollection<PeerAddress> excludedAddressList)
        {
            byte remainder;
            int row = key.FirstDifferentDigit(this.mLocalNodeInfo.Key, this.mBitsPerDigit, out remainder);
            int column = (int)remainder;

            HashKey distance = key.DistanceFrom(this.mLocalNodeInfo.Key);

            for (; row < this.mNumberOfRows; row++)
            {
                for (; column > 0; column--)
                {
                    if (null != this.mTable[row, column] && this.mTable[row, column].Count > 0 &&
                        (null == excludedAddressList || !excludedAddressList.Contains(this.mTable[row, column][0].Address)))
                    {
                        NodeInfo node = this.mTable[row, column][0];

                        if (node.ForwardDistance < distance)
                        {
                            Logger.TraceInformation(LogTag.DHT, "node forward vs distance : {0} -- {1}", node.ForwardDistance, distance);
                            return node;
                        }
                    }
                }
                column = this.mNumberOfColumns - 1;
            }

            return null;
        }

        public NodeInfo[] GetNodes()
        {
            Dictionary<string, NodeInfo> nodes = new Dictionary<string, NodeInfo>();

            for (int i = 0; i < this.mNumberOfRows; i++)
            {
                for (int j = 0; j < this.mNumberOfColumns; j++)
                {
                    List<NodeInfo> list = this.mTable[i, j];
                    if (list == null || list.Count == 0)
                    {
                        continue;
                    }

                    foreach (NodeInfo node in list)
                    {
                        if (null != node && !nodes.ContainsKey(node.Address.ToString()))
                        {
                            nodes[node.Address.ToString()] = node;
                        }
                    }
                }
            }

            return (new List<NodeInfo>(nodes.Values).ToArray());
        }
    }
}
