//-----------------------------------------------------------------------
// <copyright file="IDhtFacade.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2011 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace Badumna.DistributedHashTable
{
    using System;
    using System.Collections.Generic;
    using Badumna.Core;
    using Badumna.DataTypes;
    using Badumna.Utilities;

    /// <summary>
    /// Interace for DHT facade (TO DO: improve documentation).
    /// </summary>
    internal interface IDhtFacade : IDhtProtocol, IRouter, IMessageConsumer<DhtEnvelope>
    {
        /// <summary>
        /// TO DO: documentation.
        /// </summary>
        event Action<NodeInfo> InitializationCompleteNotification;

        /// <summary>
        /// TO DO: documentation.
        /// </summary>
        event Action<NodeInfo> NeighbourArrivalNotification;

        /// <summary>
        /// TO DO: documentation.
        /// </summary>
        event Action<NodeInfo> NeighbourDepartureNotification;

        /// <summary>
        /// TO DO: documentation.
        /// </summary>
        /// <typeparam name="T">TO DO: T documentation</typeparam>
        /// <param name="expireHandler">TO DO: documentation</param>
        void RegisterTypeForReplication<T>(EventHandler<ExpireEventArgs> expireHandler) where T : class, IReplicatedObject;

        /// <summary>
        /// TO DO: documentation.
        /// </summary>
        /// <typeparam name="T">TO DO: T documentation</typeparam>
        /// <param name="expireHandler">TO DO: documentation</param>
        /// <param name="arrivalNotifier">TO DO: documentation 2</param>
        void RegisterTypeForReplication<T>(
            EventHandler<ExpireEventArgs> expireHandler,
            ReplicaManager.ReplicaArrivalNotification arrivalNotifier) where T : class, IReplicatedObject;

        /// <summary>
        /// TO DO: documentation.
        /// </summary>
        /// <param name="replica">TO DO: documentation</param>
        void RemoveReplica(IReplicatedObject replica);

        /// <summary>
        /// TO DO: documentation.
        /// </summary>
        /// <param name="replica">TO DO: documentation</param>
        /// <param name="key">TO DO: documentation 2</param>
        /// <param name="timeToLiveSeconds">TO DO: documentation 3</param>
        void Replicate(IReplicatedObject replica, HashKey key, uint timeToLiveSeconds);

        /// <summary>
        /// TO DO: documentation.
        /// </summary>
        /// <param name="replica">TO DO: documentation</param>
        /// <param name="timeToLiveSeconds">TO DO: documentation 2</param>
        void UpdateReplica(IReplicatedObject replica, uint timeToLiveSeconds);

        /// <summary>
        /// TO DO: documentation.
        /// </summary>
        /// <param name="replica">TO DO: documentation</param>
        /// <param name="timeToLiveSeconds">TO DO: documentation 2</param>
        void KeepReplicaAlive(IReplicatedObject replica, uint timeToLiveSeconds);

        /// <summary>
        /// TO DO: documentation.
        /// </summary>
        /// <typeparam name="T">TO DO: T documentation</typeparam>
        /// <param name="key">TO DO: documentation</param>
        /// <returns>TO DO: return documentation</returns>
        ICollection<T> AccessReplicas<T>(HashKey key) where T : class, IReplicatedObject;

        /// <summary>
        /// TO DO: documentation.
        /// </summary>
        /// <typeparam name="T">TO DO: T documentation</typeparam>
        /// <param name="key">TO DO: documentation</param>
        /// <param name="id">TO DO: documentation 2</param>
        /// <returns>TO DO: return documentation</returns>
        T AccessReplica<T>(HashKey key, BadumnaId id) where T : class, IReplicatedObject;

        /// <summary>
        /// TO DO: documentation.
        /// </summary>
        /// <param name="envelope">TO DO: documentation</param>
        void DispatchMessage(DhtEnvelope envelope);
    }
}
