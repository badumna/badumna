using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

using Badumna.Core;
using Badumna.Utilities;

namespace Badumna.DistributedHashTable
{
    [Serializable]
    class InitializationException : CoreException
    {
        private HashKey mHashKey;
        internal HashKey HashKey { get { return this.mHashKey; } }

        private BaseEnvelope mMessageEnvelope;
        internal BaseEnvelope MessageEnvelope { get { return this.mMessageEnvelope; } }

        public InitializationException()
            : base() { }

        public InitializationException(HashKey hashKey, BaseEnvelope messageEnvelope)
            : base(Resources.DhtFacade_InitializationExceptionMessage)
        {
            this.mHashKey = hashKey;
            this.mMessageEnvelope = messageEnvelope;
        }

        public InitializationException(String message)
            : base(message) {}

        public InitializationException(String message, Exception innerException) 
            : base(message, innerException) { }

        protected InitializationException(SerializationInfo serializationInfo, StreamingContext streamingContext)
            : base(serializationInfo, streamingContext) {}
                
    }
}
