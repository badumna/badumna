using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;

using Badumna;
using Badumna.Core;
using Badumna.Utilities;
using Badumna.Transport;
using Badumna.DataTypes;
using Badumna.Controllers;
using Badumna.Replication;

namespace Badumna.DistributedHashTable
{
    class DhtFacade : DhtProtocol, IRouter, IMessageConsumer<DhtEnvelope>, IDhtFacade
    {
        private IRouter mRouter;

        public int UniqueNeighbourCount { get { return this.mRouter.UniqueNeighbourCount; } }

        public HashKey LocalHashKey { get { return this.mRouter.LocalHashKey; } }

        private int mTier;

        [NonSerialized]
        private ReplicaManager mReplicaManager;

        public bool IsInitialized
        {
            get { return null != this.mRouter && this.mRouter.IsInitialized; }
        }

        public bool IsRouting
        {
            get { return null != this.mRouter && this.mRouter.IsRouting; }
        }

        private Action<NodeInfo> initializationCompleteNotificationDelegate;
        public event Action<NodeInfo> InitializationCompleteNotification
        {
            add { this.initializationCompleteNotificationDelegate += value; }
            remove { this.initializationCompleteNotificationDelegate -= value; }
        }

        private Action<NodeInfo> neighbourArrivalNotificationDelegate;
        public event Action<NodeInfo> NeighbourArrivalNotification
        {
            add { this.neighbourArrivalNotificationDelegate += value; }
            remove { this.neighbourArrivalNotificationDelegate -= value; }
        }

        private Action<NodeInfo> neighbourDepartureNotificationDelegate;
        public event Action<NodeInfo> NeighbourDepartureNotification
        {
            add { this.neighbourDepartureNotificationDelegate += value; }
            remove { this.neighbourDepartureNotificationDelegate -= value; }
        }

        /// <summary>
        /// Provides the public peer address.
        /// </summary>
        private INetworkAddressProvider addressProvider;

        // This should only be used for unit testing/simulation.
        public DhtFacade(IRouter router, Simulator eventQueue, ITime timeKeeper, INetworkAddressProvider addressProvider, INetworkConnectivityReporter connectivityReporter, GenericCallBackReturn<object, Type> factory)
            : base("Test DHT", factory)
        {
            if (null == router)
            {
                throw new ArgumentNullException("router");
            }

            if (eventQueue == null)
            {
                throw new InvalidProgramException("public DhtFacade(IRouter router) should only be used in simulation mode.");
            }

            this.mRouter = router;
            this.DispatcherMethod = this.DispatchMessage;

            this.mReplicaManager = new ReplicaManager(this, eventQueue, timeKeeper, addressProvider, connectivityReporter);

            this.addressProvider = addressProvider;
        }

        public DhtFacade(
            RouterProtocol protocolParent,
            IConnectionTable connectionTable,
            NamedTier tier,
            NetworkEventQueue eventQueue,
            ITime timeKeeper,
            INetworkAddressProvider addressProvider,
            INetworkConnectivityReporter connectivityReporter,
            IPeerConnectionNotifier connectionNotifier,
            GenericCallBackReturn<object, Type> factory)
            : base(String.Format("DHT Facade {0}", (int)tier), factory)
        {
            Router router = new TieredRouter(protocolParent, connectionTable, this, (int)tier, eventQueue, timeKeeper, addressProvider, connectivityReporter, connectionNotifier);

            router.LocalPeerReleasedFromQuarantineEvent += this.LocalPeerReleasedFromQuarantineEvent;
            router.ReplicaCandidateArrivalEvent += this.ReplicaCandidateNodeArrivalHandler;
         //   router.ReplicaCandidateDepartureEvent += this.ReplicaCandidateNodeDepartureHandler;

            router.NeighbourAdditionEvent += this.NeighbourAdditionEvent;
            router.NeighbourRemovalEvent += this.NeighbourRemovalEvent;

            this.mRouter = router;
            this.mTier = (int)tier;

            this.mReplicaManager = new ReplicaManager(this, eventQueue, timeKeeper, addressProvider, connectivityReporter);

            this.DispatcherMethod = this.DispatchMessage;

            this.addressProvider = addressProvider;
        }

        public bool MapsToLocalPeer(HashKey key, ICollection<PeerAddress> excludedNodes)
        {
            return this.mRouter.MapsToLocalPeer(key, excludedNodes);
        }
        
        public NodeInfo[] GetImmediateNeighbours()
        {
            return this.mRouter.GetImmediateNeighbours();
        }

        public NodeInfo[] GetRoutingTableNodes()
        {
            return this.mRouter.GetRoutingTableNodes();
        }

        private void LocalPeerReleasedFromQuarantineEvent(object sender, LeafSetChangeEventArgs e)
        {
            if (this.addressProvider.PublicAddress.Tier <= this.mTier)
            {
                Action<NodeInfo> handler = this.initializationCompleteNotificationDelegate;
                if (handler != null)
                {
                    handler(e.NodeInfo);
                }
            }
        }

        private void NeighbourAdditionEvent(object sender, LeafSetChangeEventArgs e)
        {
            if (this.addressProvider.PublicAddress.Tier <= this.mTier)
            {
                Action<NodeInfo> handler = this.neighbourArrivalNotificationDelegate;
                if (handler != null)
                {
                    handler(e.NodeInfo);
                }
            }
        }

        private void NeighbourRemovalEvent(object sender, LeafSetChangeEventArgs e)
        {
            if (this.addressProvider.PublicAddress.Tier <= this.mTier)
            {
                Action<NodeInfo> handler = this.neighbourDepartureNotificationDelegate;
                if (handler != null)
                {
                    handler(e.NodeInfo);
                }
            }
        }

        private void ReplicaCandidateNodeArrivalHandler(object sender, LeafSetChangeEventArgs e)
        {
            Logger.TraceInformation(LogTag.DHT, "Node {0} is a candidate for replication", e.NodeInfo.Address);
            ICollection<PeerAddress> excludedNodes = new List<PeerAddress>();

            excludedNodes.Add(e.NodeInfo.Address);
            this.mReplicaManager.NeighbourArrivalNotification(e.NodeInfo, excludedNodes);
        }
        
     /*   private void ReplicaCandidateNodeDepartureHandler(object sender, LeafSetChangeEventArgs e)
        {
            Logger.TraceInformation(LogTag.DHT, "Node {0} is no longer a candidate for replication", e.NodeInfo.Address);
            ICollection<PeerAddress> excludedNodes = new List<PeerAddress>();

            excludedNodes.Add(e.NodeInfo.Address);
            this.mReplicaManager.NeighbourDepartureNotification(e.NodeInfo, excludedNodes);
        }
        */

        public void RegisterTypeForReplication<T>(EventHandler<ExpireEventArgs> expireHandler) where T : class, IReplicatedObject
        {
            this.RegisterTypeForReplication<T>(expireHandler, null);
        }

        public void RegisterTypeForReplication<T>(EventHandler<ExpireEventArgs> expireHandler,
            ReplicaManager.ReplicaArrivalNotification arrivalNotifier) where T : class, IReplicatedObject
        {
            ReplicatedTypeMap.Current.Register(typeof(T));
            this.mReplicaManager.AddHandlers(typeof(T), expireHandler, arrivalNotifier);
        }

        public void RemoveReplica(IReplicatedObject replica)
        {
            if (null == replica.Guid)
            {
                Logger.TraceError(LogTag.DHT, "Cannot remove replica because the guid is invalid.");
                return;
            }
            if (null == replica.Key)
            {
                Logger.TraceError(LogTag.DHT, "Cannot remove replica because the key is invalid.");
                return;
            }

            this.mReplicaManager.RemoveReplica(replica);
        }

        public void Replicate(IReplicatedObject replica, HashKey key, uint timeToLiveSeconds)
        {
            if (null == replica.Guid)
            {
                Logger.TraceError(LogTag.DHT, "Cannot replicate replica because the guid is invalid.");
                return;
            }

            replica.TimeToLiveSeconds = timeToLiveSeconds;
            replica.Key = key;
            this.mReplicaManager.Replicate(replica);
        }

        // NOTE : Can be called on any peer, but conflicts may arise if it is called on multiple peers,
        //        simultaneousely - so best if it is only called on the peer that maps to the replicas key.
        public void UpdateReplica(IReplicatedObject replica, uint timeToLiveSeconds)
        {
            if (null == replica.Guid)
            {
                Logger.TraceError(LogTag.DHT, "Cannot replicate replica because the guid is invalid.");
                return;
            }

            if (null == replica.Key)
            {
                Logger.TraceError(LogTag.DHT, "Cannot replicate replica because the key is invalid.");
                return;
            }

            replica.TimeToLiveSeconds = timeToLiveSeconds;
            this.mReplicaManager.Replicate(replica);
        }

        public void KeepReplicaAlive(IReplicatedObject replica, uint timeToLiveSeconds)
        {
            this.UpdateReplica(replica, timeToLiveSeconds);
        }

        public ICollection<T> AccessReplicas<T>(HashKey key) where T : class, IReplicatedObject
        {
            return this.mReplicaManager.AccessReplicas<T>(key);
        }

        public T AccessReplica<T>(HashKey key, BadumnaId id) where T : class, IReplicatedObject
        {
            return this.mReplicaManager.AccessReplica<T>(key, id);
        }

        public void DispatchMessage(DhtEnvelope envelope)
        {
            if (null != envelope.Destination && envelope.Destination.IsValid)
            {
                this.DirectSend(envelope);
            }
            else if (null != envelope.DestinationKey)
            {
                this.RouteMessage(envelope);
            }
            else
            {
                this.Broadcast(envelope);
            }
        }

        public void RouteMessage(DhtEnvelope payloadMessage, IList<PeerAddress> excludedAddresses)
        {
            Debug.Assert(payloadMessage.DestinationKey != null, "No destination key given for routed message");
            this.mRouter.RouteMessage(payloadMessage, excludedAddresses);
        }

        public void RouteMessage(DhtEnvelope messageEnvelope)
        {
            Debug.Assert(messageEnvelope.DestinationKey != null, "No destination key given for routed message");
            this.mRouter.RouteMessage(messageEnvelope);
        }

        public void DirectSend(TransportEnvelope messageEnvelope)
        {
            Debug.Assert(messageEnvelope.Destination != null, "No destination address given for direct send message");
            this.mRouter.DirectSend(messageEnvelope);
        }

        public void Broadcast(TransportEnvelope messageEnvelope)
        {
            this.mRouter.Broadcast(messageEnvelope);
        }


        #region IMessageConsumer<DhtEnvelope> Members

        public void ProcessMessage(DhtEnvelope envelope)
        {
            this.ParseMessage(envelope, 0);
        }

        #endregion
    }
}
