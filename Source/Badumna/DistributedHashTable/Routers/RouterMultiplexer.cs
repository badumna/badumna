﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

using Badumna.Core;
using Badumna.Transport;
using Badumna.Utilities;

namespace Badumna.DistributedHashTable
{
    class RouterMultiplexer : TransportProtocol, IEnumerable<TransportProtocol>
    {
        private Dictionary<int, RouterProtocol> mRouters;

        /// <summary>
        /// The queue to use for scheduling events.
        /// </summary>
        private NetworkEventQueue eventQueue;

        /// <summary>
        /// Provides access to the current time.
        /// </summary>
        private ITime timeKeeper;

        /// <summary>
        /// Provides the public peer address.
        /// </summary>
        private INetworkAddressProvider addressProvider;

        /// <summary>
        /// Reports on network connectivity status.
        /// </summary>
        private INetworkConnectivityReporter connectivityReporter;

        /// <summary>
        /// Provides notification of peer connections and disconnections.
        /// </summary>
        private IPeerConnectionNotifier connectionNotifier;

        /// <summary>
        /// A factory that constructs instances of the given type.
        /// </summary>
        private GenericCallBackReturn<object, Type> factory;

        public RouterMultiplexer(
            TransportProtocol parent,
            NetworkEventQueue eventQueue,
            ITime timeKeeper,
            INetworkAddressProvider addressProvider,
            INetworkConnectivityReporter connectivityReporter,
            IPeerConnectionNotifier connectionNotifier,
            GenericCallBackReturn<object, Type> factory)
            : base(parent)
        {
            this.mRouters = new Dictionary<int, RouterProtocol>();
            this.eventQueue = eventQueue;
            this.timeKeeper = timeKeeper;
            this.addressProvider = addressProvider;
            this.connectivityReporter = connectivityReporter;
            this.connectionNotifier = connectionNotifier;
            this.factory = factory;
        }

        private RouterProtocol AddTier(int tier)
        {
            RouterProtocol protocol = null;

            if (!this.mRouters.TryGetValue(tier, out protocol))
            {
                protocol = new RouterProtocol(tier, this, this.factory);
                this.mRouters.Add(tier, protocol);
            }

            return protocol;
        }

        private void RemoveTier(int tier)
        {
            if (this.mRouters.ContainsKey(tier))
            {
                this.mRouters.Remove(tier);
            }
        }

        public TransportEnvelope GetMessageFor(int tier, PeerAddress address, QualityOfService qos)
        {
            TransportEnvelope envelope = this.GetMessageFor(address, qos);

            this.PrepareMessageForDeparture(ref envelope);
            this.RemoteCall(envelope, this.Multiplex, (byte)tier);

            return envelope;
        }

        [ConnectionfulProtocolMethod(ConnectionfulMethod.Multiplex)]
        private void Multiplex(byte tier)
        {
            RouterProtocol protocol = null;

            if (this.mRouters.TryGetValue((int)tier, out protocol))
            {
                Logger.TraceInformation(LogTag.DHT, "Received message for tier {0}", tier);
                protocol.ParseMessage(this.CurrentEnvelope, this.CurrentEnvelope.Message.ReadOffset);
            }
            else
            {
                Logger.TraceError(LogTag.DHT, "Received message for unknown tier {0}", tier);
            }
        }

        public DhtFacade CreateDhtFacade(NamedTier tier, IConnectionTable connectionTable)
        {
            RouterProtocol protocol = this.AddTier((int)tier);

            if (protocol != null)
            {
                DhtFacade facade = new DhtFacade(protocol, connectionTable, tier, this.eventQueue, this.timeKeeper, this.addressProvider, this.connectivityReporter, this.connectionNotifier, this.factory);

                protocol.ForgeMethodList();
                return facade;
            }

            return null;
        }

        #region IEnumerable<TransportProtocol> Members

        IEnumerator<TransportProtocol> IEnumerable<TransportProtocol>.GetEnumerator()
        {
            return new Enumerator(this.mRouters.Values.GetEnumerator());
        }

        class Enumerator : IEnumerator<TransportProtocol>
        {
            private Dictionary<int, RouterProtocol>.ValueCollection.Enumerator routers;

            public Enumerator(Dictionary<int, RouterProtocol>.ValueCollection.Enumerator routers)
            {
                this.routers = routers;
            }

            #region IEnumerator<TransportProtocol> Members

            public TransportProtocol Current
            {
                get
                {
                    return this.routers.Current;
                }
            }

            #endregion

            #region IDisposable Members

            public void Dispose()
            {
                this.routers.Dispose();
            }

            #endregion

            #region IEnumerator Members

            object IEnumerator.Current
            {
                get
                {
                    return this.routers.Current;
                }
            }

            public bool MoveNext()
            {
                return this.routers.MoveNext();
            }

            public void Reset()
            {
                throw new NotSupportedException();
            }

            #endregion
        }
        #endregion

        #region IEnumerable Members

        IEnumerator IEnumerable.GetEnumerator()
        {
            return (this as IEnumerable<TransportProtocol>).GetEnumerator();
        }

        #endregion
    }
}
