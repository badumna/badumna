﻿using System;
using System.Collections.Generic;
using System.Text;

using Badumna.Core;
using Badumna.Transport;
using Badumna.Utilities;

namespace Badumna.DistributedHashTable
{
    class RouterProtocol : TransportProtocol
    {
        private int mTier;
        private RouterMultiplexer mMultiplexer;

        public RouterProtocol(int tier, RouterMultiplexer multiplexer, GenericCallBackReturn<object, Type> factory)
            : base(String.Format("Router {0}", tier), typeof(RouterProtocolMethodAttribute), factory)
        {
            this.mTier = tier;
            this.mMultiplexer = multiplexer;
            base.DispatcherMethod = multiplexer.SendMessage;
        }

        public RouterProtocol(RouterProtocol parent)
            : base(parent)
        {
            this.mTier = parent.mTier;
            this.mMultiplexer = parent.mMultiplexer;

            if (this.mMultiplexer != null)
            {
                base.DispatcherMethod = this.mMultiplexer.SendMessage;
            }
            else
            {
                base.DispatcherMethod = this.SendMessage;
            }
        }

        public RouterProtocol(String layerName, Type protocolAttributeType, GenericCallBackReturn<object, Type> factory)
            : base(layerName, protocolAttributeType, factory)
        {
        }

        public override void PrepareMessageForDeparture(ref TransportEnvelope envelope)
        {
            if (this.mMultiplexer != null)
            {
                envelope = this.mMultiplexer.GetMessageFor(this.mTier, envelope.Destination, envelope.Qos);
            }
        }
    }
}
