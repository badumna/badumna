﻿using System;
using System.Collections.Generic;
using System.Text;

using Badumna.Core;
using Badumna.Transport;
using Badumna.Utilities;

namespace Badumna.DistributedHashTable
{
    // A Wrapper of a TransportEnvelope without copying it
    class RouterEnvelope : IEnvelope
    {
        private int mTier;
        public int Tier { get { return this.mTier; } }

        private IEnvelope mEnvelope;

        public RouterEnvelope(IEnvelope envelope, int tier)
        {
            this.mTier = tier;
            this.mEnvelope = envelope;
        }

        #region IEnvelope Members

        public bool IsValid
        {
            get { return this.mEnvelope != null && this.mEnvelope.IsValid; }
        }

        public int Available
        {
            get { return this.mEnvelope.Available; }
        }

        public QualityOfService Qos
        {
            get { return this.mEnvelope.Qos; }
            set { this.mEnvelope.Qos = value; }
        }

        public MessageBuffer Message
        {
            get { return this.mEnvelope.Message; }
        }

        public string Label
        {
            get { return this.mEnvelope.Label; }
        }

        public void ResetParseOffset()
        {
            this.mEnvelope.ResetParseOffset();
        }

#if TRACE
        public void AppendLabel(String label)
        {
            this.mEnvelope.AppendLabel(label);
        }
#endif

        #endregion



        #region IParseable Members

        public void ToMessage(MessageBuffer message, Type parameterType)
        {
            throw new NotImplementedException();
        }

        public void FromMessage(MessageBuffer message)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region ISized Members

        public int Length
        {
            get { return this.mEnvelope.Length; }
        }

        #endregion
    }
}
