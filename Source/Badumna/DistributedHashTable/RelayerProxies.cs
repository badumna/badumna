﻿using System;
using System.Collections.Generic;
using System.Text;

using Badumna.Core;
using Badumna.Utilities;
using Badumna.Transport;

namespace Badumna.DistributedHashTable
{
    internal class QueuedMessage
    {
        private TimeSpan addedTime;

        private TransportEnvelope envelope;

        public QueuedMessage(TransportEnvelope envelope, TimeSpan addedTime)
        {
            this.envelope = envelope;
            this.addedTime = addedTime;
        }

        public TimeSpan AddedTime
        {
            get { return this.addedTime; }
        }

        public TransportEnvelope Envelope
        {
            get { return this.envelope; }
        }
    }

    internal class RelayerProxies
    {
        public int Count { get { return this.mProxies.Count; } }
        public PeerAddress Destination { get { return this.mDestinationAddress; } }

        public int MessageCount { get { return this.mQueuedMessages.Count; } }

        private PeerAddress mDestinationAddress;
        private List<ProxyInformation> mProxies;
        private IConnectionTable mConnectionTable;
        private Queue<QueuedMessage> mQueuedMessages;

        public TimeSpan TimeOfLastQuery { get; set; }

        public Queue<QueuedMessage> QueuedMessages
        {
            get { return this.mQueuedMessages; }
        }

        /// <summary>
        /// Provides access to the current time.
        /// </summary>
        private ITime timeKeeper;

        class ProxyInformation
        {
            public TimeSpan TimeOfLastPing;
            public PeerAddress Address;

            public ProxyInformation(PeerAddress address)
            {
                this.Address = address;
                this.TimeOfLastPing = TimeSpan.MinValue;
            }

            public override bool Equals(object obj)
            {
                if (obj is ProxyInformation)
                {
                    return ((ProxyInformation)obj).Address.Equals(this.Address);
                }

                return false;
            }

            public override int GetHashCode()
            {
                return this.Address.GetHashCode();
            }
        }

        public RelayerProxies(PeerAddress address, IConnectionTable connectionTable, ITime timeKeeper)
        {
            this.timeKeeper = timeKeeper;
            this.mDestinationAddress = address;
            this.mProxies = new List<ProxyInformation>();
            this.mQueuedMessages = new Queue<QueuedMessage>();
            this.mConnectionTable = connectionTable;
        }

        public void ShutDown()
        {
            // TODO : Do something with theses queued messages
            Logger.TraceWarning(LogTag.DHT, "Closing relay path with {0} outstanding messages", this.mQueuedMessages.Count);
        }

        public void AddProxy(PeerAddress address)
        {
            ProxyInformation proxy = new ProxyInformation(address);

            if (!this.mProxies.Contains(proxy))
            {
                this.mProxies.Add(proxy);
            }
        }

        public void RemoveProxy(PeerAddress address)
        {
            this.mProxies.RemoveAll(delegate(ProxyInformation proxyInformation) { return proxyInformation.Address.Equals(address); });
        }

        public void MoveProxyToEnd(PeerAddress address)
        {
            ProxyInformation proxyToMove = null;
            for (int i = 0; i < this.mProxies.Count; i++)
            {
                while (this.mProxies[i].Address.Equals(address) && i < this.mProxies.Count)
                {
                    if (proxyToMove != null)
                    {
                        proxyToMove = this.mProxies[i];
                    }
                    this.mProxies.RemoveAt(i);
                }
            }

            if (proxyToMove != null)
            {
                this.mProxies.Add(proxyToMove);
            }
        }

        public void CreateNewQueue()
        {
            this.mQueuedMessages = new Queue<QueuedMessage>();
        }

        public void QueueMessage(TransportEnvelope payloadMessage)
        {
            if (payloadMessage.Qos.IsRealtimeMessage)
            {
                // do not queue real time messages
                Logger.TraceInformation(LogTag.DHT, "A realtime message (e.g. entity update message) is dropped.");
                return;
            }

            this.mQueuedMessages.Enqueue(new QueuedMessage(payloadMessage, this.timeKeeper.Now));
        }

        public TransportEnvelope PopQueuedMessage()
        {
            if (this.mQueuedMessages.Count > 0)
            {
                QueuedMessage message = this.mQueuedMessages.Dequeue();
                return message.Envelope;
            }

            return null;
        }

        public PeerAddress FindAProxy()
        {
            while (this.mProxies.Count > 0)
            {
                ProxyInformation proxy = this.mProxies[0];

                if (this.mConnectionTable.IsConnected(proxy.Address) && !this.mConnectionTable.IsRelayed(proxy.Address))
                {
                    return proxy.Address;
                }
                else
                {
                    this.mProxies.RemoveAt(0);
                }
            }

            return null;
        }

        public void GarbageCollection()
        {
            TimeSpan now = this.timeKeeper.Now;

            if (this.mQueuedMessages.Count == 0)
            {
                return;
            }

            QueuedMessage message = this.mQueuedMessages.Peek();
            while (now - message.AddedTime > Parameters.MaxTimeToKeepMessageInRelayProxyMessageQueue)
            {
                this.mQueuedMessages.Dequeue();
                if (this.mQueuedMessages.Count > 0)
                {
                    message = this.mQueuedMessages.Peek();
                }
                else
                {
                    break;
                }
            }
        }
    }
}
