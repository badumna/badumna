﻿using System;
using System.Collections.Generic;
using System.Text;

using Badumna.Core;
using Badumna.Utilities;
using Badumna.DataTypes;
using System.Diagnostics;

namespace Badumna.DistributedHashTable
{
    /// <summary>
    /// Concrete class to allow replicated objects to be passed through protocol methods.
    /// </summary>
    class ReplicatedObjectPackage : IParseable
    {
        private IReplicatedObject mReplicatedObject;
        public IReplicatedObject ReplicatedObject { get { return this.mReplicatedObject; } }

        /// <summary>
        /// Provides access to the current time.
        /// </summary>
        /// <remarks>
        /// This is only valid when the ReplicatedObjectPackage was constructed using
        /// ReplicatedObjectPackage(IReplicatedObject, ITime).  If this instance was
        /// constructed by deserialization then timeKeeper will be null.  This
        /// means that a deserializaed ReplicatedObjectPackage cannot be reserialized --
        /// a new package must be created instead.
        /// </remarks>
        private ITime timeKeeper;

        // For IParseable
        private ReplicatedObjectPackage()
        {
        }

        public ReplicatedObjectPackage(IReplicatedObject replicatedObject, ITime timeKeeper)
        {
            this.mReplicatedObject = replicatedObject;
            this.timeKeeper = timeKeeper;
        }

        public void ToMessage(MessageBuffer message, Type parameterType)
        {
            Type type = this.mReplicatedObject.GetType();
            message.Write(ReplicatedTypeMap.Current.FromType(type));
            this.mReplicatedObject.ToMessage(message, type);
            message.Write(this.mReplicatedObject.Guid);
            message.Write(this.mReplicatedObject.ModificationNumber);

            Debug.Assert(this.timeKeeper != null);
            uint timeRemaining = (uint)Math.Max(0, (this.mReplicatedObject.ExpirationTime - this.timeKeeper.Now).TotalSeconds);
            message.Write(timeRemaining);
        }

        public void FromMessage(MessageBuffer message)
        {
            byte typeId = message.ReadByte();
            Type type = ReplicatedTypeMap.Current.FromId(typeId);
            this.mReplicatedObject = (IReplicatedObject)Activator.CreateInstance(type, true);
            this.mReplicatedObject.FromMessage(message);
            this.mReplicatedObject.Guid = message.Read<BadumnaId>();
            this.mReplicatedObject.ModificationNumber = message.Read<CyclicalID.UShortID>();
            this.mReplicatedObject.TimeToLiveSeconds = message.ReadUInt();
        }
    }
}
