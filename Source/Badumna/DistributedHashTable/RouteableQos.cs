using System;
using System.Collections.Generic;
using System.Text;

using Badumna.Core;
using Badumna.Utilities;

namespace Badumna.DistributedHashTable
{
    class RouteableQos<T> : QualityOfService where T : BaseEnvelope
    {
        private IList<PeerAddress> mExcludedAddresses;
        public IList<PeerAddress> ExcludedAddresses { get { return this.mExcludedAddresses; } }

        private T mMessageEnvelope;
        public T MessageEnvelope { get { return this.mMessageEnvelope; } }

        private int mRedundencyFactor;
        public int RedundencyFactor
        {
            get { return this.mRedundencyFactor; }
            set { this.mRedundencyFactor = value; }
        }

        private GenericCallBack<PeerAddress, T, IList<PeerAddress>> mFailureHandler;

        public RouteableQos(T envelope, IList<PeerAddress> excludedAddresses,
            GenericCallBack<PeerAddress, T, IList<PeerAddress>> failureHandler)
            : base(envelope.Qos)
        {
            this.mRedundencyFactor = 0;
            this.mMessageEnvelope = envelope;
            this.mExcludedAddresses = excludedAddresses;
            this.mFailureHandler = failureHandler;
        }

        internal override void HandleConnectionFailure(PeerAddress destination)
        {
        }

        internal override bool HandleFailure(PeerAddress destination, int nthAttempt)
        {
            if ((nthAttempt == 2 || nthAttempt == -1) && null != this.mFailureHandler)
            {
                if (null == this.mExcludedAddresses)
                {
                    this.mExcludedAddresses = new List<PeerAddress>();
                }

                // Add the failed peer to the exclusion list.
                if (!this.mExcludedAddresses.Contains(destination))
                {
                    this.mExcludedAddresses.Add(destination);
                }

                this.mFailureHandler(destination, this.mMessageEnvelope, this.mExcludedAddresses);
                return false;
            }

            return true;
        }
    }

    class ExcludedResendEventArgs : SendFailureEventArgs
    {
        private PeerAddress mMessageOrigin;
        public PeerAddress MessageOrigin { get { return this.mMessageOrigin; } }

        private List<PeerAddress> mExcludedNeihbours = new List<PeerAddress>();
        public IList<PeerAddress> ExcludedNeighbours { get { return this.mExcludedNeihbours; } }

        public ExcludedResendEventArgs(PeerAddress source)
            : base()
        {
            this.mMessageOrigin = source;
        }

        public void Exclude(PeerAddress address)
        {
            this.mExcludedNeihbours.Add(address);
        }
    }

    class ExcludedRouteEventArgs : ExcludedResendEventArgs
    {
        private DhtEnvelope mMessageEnvelope;
        public DhtEnvelope MessageEnvelope { get { return this.mMessageEnvelope; } }

        public ExcludedRouteEventArgs(DhtEnvelope envelope)
            : base(envelope.Source)
        {
            this.mMessageEnvelope = envelope;
        }
    }
}

