using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;

using Badumna.Core;
using Badumna.Utilities;
using Badumna.Transport;

namespace Badumna.DistributedHashTable
{

    partial class Router
    {
        private class DelayedBroadcastRequest
        {
            public TransportEnvelope Envevelope;
            public PeerAddress Source;
            public HashKey StartRange;
            public HashKey EndRange;

            public DelayedBroadcastRequest(TransportEnvelope envelope, PeerAddress source, HashKey start, HashKey end)
            {
                this.Envevelope = envelope;
                this.Source = source;
                this.StartRange = start;
                this.EndRange = end;
            }
        }

        public void Broadcast(TransportEnvelope messageEnvelope)
        {
            if (messageEnvelope.Source == null)
            {
                messageEnvelope.Source = this.addressProvider.PublicAddress;
            }

            if (!this.mRouteRequestHandlingEnabled)
            {
                this.mDelayedBroadcastRequests.Add(new DelayedBroadcastRequest(messageEnvelope, this.addressProvider.PublicAddress, null, this.LocalHashKey));
                return;
            }

            this.MakeBroadcastRequest(this.mLeafSet[0], messageEnvelope, this.mLeafSet[0].Key, messageEnvelope.Source);
        }

        private bool HandleFailedBroadcastMessage(PeerAddress destination, TransportEnvelope payloadMessage,
           HashKey startOfRegion, HashKey endOfRegion, PeerAddress source, int nthAttempt)
        {
            if (nthAttempt == 2)
            {
                List<PeerAddress> excludedList = new List<PeerAddress>();

                excludedList.Add(this.mLeafSet[0].Address);
                excludedList.Add(destination);

                Logger.TraceInformation(LogTag.DHT, "Retry broadcast from {0}", startOfRegion);
                Logger.TraceInformation(LogTag.DHT, "Retry broadcast to {0}", endOfRegion);

                // Find the next suitable peer.
                NodeInfo nextClosestNode = this.mRoutingTable.ClosestProceedingNode(startOfRegion, excludedList);

                if (null == nextClosestNode && this.mLeafSet.SuccessorCount > 0)
                {
                    for (int i = this.mLeafSet.SuccessorCount; i > 0; i--)
                    {
                        Logger.TraceInformation(LogTag.DHT, "  b** {0} {1}", this.mLeafSet[i].Address, this.mLeafSet[i].Key);

                        HashKey distanceToEndOfSubRegion = endOfRegion.DistanceFrom(this.LocalHashKey);
                        HashKey distanceToStartOfRegion = startOfRegion.DistanceFrom(this.LocalHashKey);

                        if (this.mLeafSet[i].ForwardDistance < distanceToEndOfSubRegion && this.mLeafSet[i].ForwardDistance > distanceToStartOfRegion)
                        {
                            Logger.TraceInformation(LogTag.DHT, "Handoff broadcast to successor {0}", this.mLeafSet[i].Address);
                            nextClosestNode = this.mLeafSet[i];
                            break;
                        }
                    }
                }

                if (null != nextClosestNode)
                {
                    this.MakeExtendedBroadcastRequest(nextClosestNode, payloadMessage, startOfRegion, endOfRegion, source);
                    return false;
                }
            }

            return true;
        }

        private void MakeBroadcastRequest(NodeInfo node, TransportEnvelope payloadMessage, HashKey endOfRegion, PeerAddress source)
        {
            Logger.TraceInformation(LogTag.DHT, "Asking {0} to perform broadcast for {1} - {2}", node.Address, node.Key, endOfRegion);

            FailNotificationQos errorHandledQos = new FailNotificationQos(payloadMessage.Qos,
                new LambdaFunction(new GenericCallBackReturn<bool, PeerAddress, TransportEnvelope, HashKey, HashKey, PeerAddress, int>(this.HandleFailedBroadcastMessage),
                node.Address, payloadMessage, node.Key, endOfRegion, source));

            TransportEnvelope transportEnvelope = this.GetMessageFor(node.Address, errorHandledQos);

            this.RemoteCall(transportEnvelope, this.HandleBroadcast, payloadMessage, endOfRegion, source);
            this.SendMessage(transportEnvelope);
        }

        private void MakeExtendedBroadcastRequest(NodeInfo node, TransportEnvelope payloadMessage, HashKey startOfRegion, HashKey endOfRegion, PeerAddress source)
        {
            Logger.TraceInformation(LogTag.DHT, "Asking {0} to perform extended broadcast for {1} - {2}", node.Address, startOfRegion, endOfRegion);

            FailNotificationQos errorHandledQos = new FailNotificationQos(payloadMessage.Qos,
                new LambdaFunction(new GenericCallBackReturn<bool, PeerAddress, TransportEnvelope, HashKey, HashKey, PeerAddress, int>(this.HandleFailedBroadcastMessage),
                node.Address, payloadMessage, startOfRegion, endOfRegion, source));

            TransportEnvelope transportEnvelope = this.GetMessageFor(node.Address, errorHandledQos);

            this.RemoteCall(transportEnvelope, this.HandleExtendedBroadcast, payloadMessage, startOfRegion, endOfRegion, source);
            this.SendMessage(transportEnvelope);
        }

        [RouterProtocolMethod(RouterMethod.RouterHandleBroadcast)]
        protected void HandleBroadcast(TransportEnvelope payloadMessage, HashKey endOfThisRegion, PeerAddress source)
        {
            if (!this.mRouteRequestHandlingEnabled)
            {
                this.mDelayedBroadcastRequests.Add(new DelayedBroadcastRequest(payloadMessage, source, null, endOfThisRegion));
                return;
            }

            payloadMessage.Source = source;
            this.mChildRouter.ProcessMessage(new DhtEnvelope(payloadMessage));

            this.HandleExtendedBroadcast(payloadMessage, this.LocalHashKey, endOfThisRegion, source);
        }

        [RouterProtocolMethod(RouterMethod.RouterHandleExtendedBroadcast)]
        protected void HandleExtendedBroadcast(TransportEnvelope payloadMessage, HashKey startOfRegion, HashKey endOfThisRegion, PeerAddress source)
        {

            if (!this.mRouteRequestHandlingEnabled)
            {
                this.mDelayedBroadcastRequests.Add(new DelayedBroadcastRequest(payloadMessage, source, startOfRegion, endOfThisRegion));
                return;
            }

            List<PeerAddress> excludedList = new List<PeerAddress>();

            excludedList.Add(this.mLeafSet[0].Address);

            HashKey distanceToStartOfRegion = startOfRegion.DistanceFrom(this.LocalHashKey);
            if (distanceToStartOfRegion > endOfThisRegion.DistanceFrom(this.LocalHashKey))
            {
                // This peers location is within the range.
                this.mChildRouter.ProcessMessage(new DhtEnvelope(payloadMessage));
                startOfRegion = this.LocalHashKey;
                distanceToStartOfRegion = HashKey.Min;

                // TODO : Should I send in the reverse direction as well?
            }

            HashKey range = HashKey.Max;
            if (!endOfThisRegion.Equals(this.LocalHashKey))
            {
                range = endOfThisRegion.Subtract(this.LocalHashKey);
            }

            Logger.TraceInformation(LogTag.DHT, "Broadcast from {0}", startOfRegion);
            Logger.TraceInformation(LogTag.DHT, "Broadcast to {0}", endOfThisRegion);
            Logger.TraceInformation(LogTag.DHT, "Broadcast range = {0}", range);

            HashKey branchingRange = range.DivideByPowerOfTwo(3); // branching factor = 8
            HashKey nextBranchEndpoint = endOfThisRegion.Subtract(branchingRange);
            NodeInfo nextNode = this.mRoutingTable.ClosestProceedingNode(nextBranchEndpoint, excludedList);

            HashKey endOfSubRegion = endOfThisRegion;
            while (null != nextNode)
            {
                if (!endOfThisRegion.Equals(nextNode.Key))
                {
                    if (nextNode.ForwardDistance > distanceToStartOfRegion)
                    {
                        Debug.Assert(!nextNode.Key.Equals(endOfSubRegion), "Broadcast loop in routing table section");
                        this.MakeBroadcastRequest(nextNode, payloadMessage, endOfSubRegion, source);
                    }
                    else
                    {
                        this.MakeExtendedBroadcastRequest(nextNode, payloadMessage, startOfRegion, endOfSubRegion, source);

                        // Reached the start of the region. No need to continue.
                        return;
                    }
                }

                endOfSubRegion = nextNode.Key;
                nextBranchEndpoint = nextBranchEndpoint.Subtract(branchingRange);
                excludedList.Add(nextNode.Address);
                nextNode = this.mRoutingTable.ClosestProceedingNode(nextBranchEndpoint, excludedList);
            }

            Logger.TraceInformation(LogTag.DHT, "Routing table remainder = {0}", endOfSubRegion);

            // Continue with the leafset
            for (int i = this.mLeafSet.SuccessorCount; i > 0; i--)
            {
                Logger.TraceInformation(LogTag.DHT, "  b** {0} {1}", this.mLeafSet[i].Address, this.mLeafSet[i].Key);

                // If the end of the sub region is equal to the local key then the distance is max,
                // This is not caught by DistanceFrom which returns 0.
                HashKey distanceToEndOfSubRegion = HashKey.Max;
                if (!endOfSubRegion.Equals(this.LocalHashKey))
                {
                    distanceToEndOfSubRegion = endOfSubRegion.DistanceFrom(this.LocalHashKey);
                }

                Logger.TraceInformation(LogTag.DHT, "distance to end of region = {0}", distanceToEndOfSubRegion);

                if (this.mLeafSet[i].ForwardDistance < distanceToEndOfSubRegion && this.mLeafSet[i].ForwardDistance > distanceToStartOfRegion)
                {
                    Debug.Assert(!this.mLeafSet[i].Key.Equals(endOfSubRegion), "Broadcast loop in leafset handoff");
                    Logger.TraceInformation(LogTag.DHT, "Handoff broadcast to successor {0}", this.mLeafSet[i].Address);
                    this.MakeBroadcastRequest(this.mLeafSet[i], payloadMessage, endOfSubRegion, source);
                    endOfSubRegion = this.mLeafSet[i].Key;
                }
            }

            Logger.TraceInformation(LogTag.DHT, "Leafset remainder = {0}", endOfSubRegion);
        }
    }
}
