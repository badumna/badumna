using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

using Badumna.Transport;


namespace Badumna.DistributedHashTable
{
    enum NamedTier
    {
        Trusted = 0,
        OpenAddresses = 1,
        AllInclusive = 2
    }

    class DhtFacadeCollection : IEnumerable<DhtFacade>
    {
        private const int TierCount = 3;

        private DhtFacade[] mTiers = new DhtFacade[DhtFacadeCollection.TierCount];

        internal DhtFacade this[NamedTier namedTier]
        {
            get { return this[(int)namedTier]; }
        }

        internal DhtFacade this[int index]
        {
            get
            {
                if (index >= DhtFacadeCollection.TierCount)
                {
                    throw new ArgumentOutOfRangeException("index", "Index exceeds the number of network tiers.");
                }
                if (index < 0)
                {
                    throw new ArgumentOutOfRangeException("index", "Network tier index must be greater than zero.");
                }
                return this.mTiers[index];
            }
             /*
#if DEBUG
            set
            {
                this.mTiers[index] = value;
            }
#endif
              */
        }

        /*
        public DhtFacadeCollection(IConnectionTable parent)
        {
            for (int i = 0; i < DhtFacadeCollection.TierCount; i++)
            {
                this.mTiers[i] = new DhtFacade(parent, i);
            }
        }
        */

        public DhtFacadeCollection(DhtFacade tier0, DhtFacade tier1, DhtFacade tier2)
        {
            this.mTiers[0] = tier0;
            this.mTiers[1] = tier1;
            this.mTiers[2] = tier2;
        }

        public IEnumerator<DhtFacade> GetEnumerator()
        {
            return new Enumerator(this.mTiers.GetEnumerator());
        }

        class Enumerator : IEnumerator<DhtFacade>
        {
            private IEnumerator tiersEnumerator;

            public Enumerator(IEnumerator enumerator)
            {
                this.tiersEnumerator = enumerator;
            }

            #region IEnumerator<DhtFacade> Members

            public DhtFacade Current
            {
                get
                {
                    return (DhtFacade)this.tiersEnumerator.Current;
                }
            }

            #endregion

            #region IDisposable Members

            public void Dispose()
            {
            }

            #endregion

            #region IEnumerator Members

            object IEnumerator.Current
            {
                get
                {
                    return this.tiersEnumerator.Current;
                }
            }

            public bool MoveNext()
            {
                return this.tiersEnumerator.MoveNext();
            }

            public void Reset()
            {
                throw new NotSupportedException();
            }

            #endregion
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }
    }
}
