using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;

using Badumna.Core;
using Badumna.Utilities;
using Badumna.Transport;
using Badumna.Utilities.Logging;

namespace Badumna.DistributedHashTable
{
    // TODO : Make sure that the dht can handle differing views of the leafset, if for example,
    // two neighbouring peers cannot send messages between each other.

    /// <summary>
    /// The primary class used for the DHT. 
    /// TODO : overview documentation for :
    ///   1. Leaf set :
    ///   2. Quarantine :
    ///   3. Join procedure :
    ///   4. Routing Table :
    ///   5. Routing (Iterative, handle along path, etc) :
    ///   6. Broadcast :
    /// </summary>
    partial class Router : RouterProtocol, IRouter
    {

        public HashKey LocalHashKey
        {
            get
            {
                if (!this.IsInitialized)
                {
                    throw new InitializationException((HashKey)null, (BaseEnvelope)null);
                }

                return this.mLeafSet[0].Key;
            }
        }

        public LeafSet LeafSet { get { return this.mLeafSet; } }

        public bool IsInitialized { get { return null != this.mLeafSet && this.mLeafSet.IsInitialized; } }
        public bool IsRouting { get { return this.mRouteRequestHandlingEnabled; } }
        public int UniqueNeighbourCount 
        { 
            get 
            {
                if (null == this.mLeafSet)
                {
                    return 0;
                }

                return this.mLeafSet.UniqueNodeCount; 
            } 
        }

        private EventHandler<LeafSetChangeEventArgs> replicaCandidateArrivalEventDelegate;
        public event EventHandler<LeafSetChangeEventArgs> ReplicaCandidateArrivalEvent
        {
            add { this.replicaCandidateArrivalEventDelegate += value; }
            remove { this.replicaCandidateArrivalEventDelegate -= value; }
        }

        private EventHandler<LeafSetChangeEventArgs> replicaCandidateDepartureEventDelegate;
        public event EventHandler<LeafSetChangeEventArgs> ReplicaCandidateDepartureEvent
        {
            add { this.replicaCandidateDepartureEventDelegate += value; }
            remove { this.replicaCandidateDepartureEventDelegate -= value; }
        }

        private EventHandler<LeafSetChangeEventArgs> localPeerReleasedFromQuarantineEventDelegate;
        public event EventHandler<LeafSetChangeEventArgs> LocalPeerReleasedFromQuarantineEvent
        {
            add { this.localPeerReleasedFromQuarantineEventDelegate += value; }
            remove { this.localPeerReleasedFromQuarantineEventDelegate -= value; }
        }


        // These events are required because the leafet can change in the course of a routers lifetime.
        private EventHandler<LeafSetChangeEventArgs> neighbourAdditionEventDelegate;
        public event EventHandler<LeafSetChangeEventArgs> NeighbourAdditionEvent
        {
            add { this.neighbourAdditionEventDelegate += value; }
            remove { this.neighbourAdditionEventDelegate -= value; }
        }

        private EventHandler<LeafSetChangeEventArgs> neighbourRemovalEventDelegate;
        public event EventHandler<LeafSetChangeEventArgs> NeighbourRemovalEvent
        {
            add { this.neighbourRemovalEventDelegate += value; }
            remove { this.neighbourRemovalEventDelegate -= value; }
        }

        private const int MinimumSyncronizationDelayMilliseconds = 20000;
        private const int MaximumSyncronisationDelayMilliseconds = 40000;

        private bool mNeedToSyncronize;
        private bool mHasOutstandingJoinRequest;
        private bool mRouteRequestHandlingEnabled;

        private LeafSet mLeafSet;

        /// <summary>
        /// Used to ensure replicas are sent to a peer while it's in quarantine, so that it's ready to handle queries once it comes out of quarantine?
        /// </summary>
        private LeafSet mReplicaLeafset;

        private RoutingTable mRoutingTable;
        private IMessageConsumer<DhtEnvelope> mChildRouter;

        private List<DhtEnvelope> mDelayedRouteRequests = new List<DhtEnvelope>();
        private List<DelayedBroadcastRequest> mDelayedBroadcastRequests = new List<DelayedBroadcastRequest>();
        private List<PeerAddress> mQuarantineList = new List<PeerAddress>();
        private RegularTask mSynchronizeLeafSetTask;

        /* private RegularTask mPingClosestNeighboursTask;

         private struct KeepAlive
         {
             public TimeSpan LastAckTime;
             public PeerAddress NeighboursAddress;
         }

         private KeepAlive[] mNeighboursKeepAlive = new KeepAlive[2];
         */

        //  protected bool mDoActiveAcknowledgement = false;


        protected IConnectionTable mConnectionTable;

        /// <summary>
        /// The queue to use for scheduling events.
        /// </summary>
        protected NetworkEventQueue eventQueue;

        /// <summary>
        /// Provides the public peer address.
        /// </summary>
        private INetworkAddressProvider addressProvider;

        /// <summary>
        /// Reports on network connectivity status.
        /// </summary>
        private INetworkConnectivityReporter connectivityReporter;

        // ******************************************************************************************************************


        public Router(RouterProtocol parent,
            IConnectionTable connectionTable,
            IMessageConsumer<DhtEnvelope> childRouter,
            NetworkEventQueue eventQueue,
            INetworkAddressProvider addressProvider,
            INetworkConnectivityReporter connectivityReporter,
            IPeerConnectionNotifier connectionNotifier)
            : base(parent)
        {
            this.eventQueue = eventQueue;
            this.addressProvider = addressProvider;
            this.connectivityReporter = connectivityReporter;
            this.mChildRouter = childRouter;
            this.mConnectionTable = connectionTable;
            this.mLeafSet = new LeafSet();
            this.mReplicaLeafset = new LeafSet();

            /*        if (this.mDoActiveAcknowledgement)
                    {
                        this.mPingClosestNeighboursTask = new RegularTask("Ping neighbours", Parameters.MinimumStatusCheckPeriod, this.CheckNeighboursExist);
                        this.mPingClosestNeighboursTask.Start();
                    }
                    */

            this.connectivityReporter.NetworkOnlineEvent += this.BeginInitialization;
            this.connectivityReporter.NetworkOfflineEvent += this.Shutdown;
            this.connectivityReporter.NetworkShuttingDownEvent += this.MakeLeaveRequest;
            connectionNotifier.ConnectionLostEvent += this.ConnectionLostHandler;
            connectionNotifier.ConnectionEstablishedEvent += this.ConnectionEstablishedHandler;

            // Create the periodic synchronisation task without starting it (done in BeginInitialization). 
            // A randomized delay is used to prevent all the peers in the network syncronising at the same time (mostly for simulations).
            TimeSpan randomizedDelay = TimeSpan.FromMilliseconds(
                RandomSource.Generator.Next(Router.MinimumSyncronizationDelayMilliseconds, Router.MaximumSyncronisationDelayMilliseconds));
            this.mSynchronizeLeafSetTask = new RegularTask("Sync leafset", randomizedDelay, this.eventQueue, this.connectivityReporter, this.PeriodicLeasfSetSyncronization);

            // Begin intialization when online
            if (this.connectivityReporter.Status == ConnectivityStatus.Online)
            {
                this.BeginInitialization();
            }
        }

        // ******************************************************************************************************************

        /// <summary>
        /// Returns whether the nodeInfo refers to the local peer.
        /// </summary>
        private bool IsLocal(NodeInfo nodeInfo)
        {
            return nodeInfo.Address.Equals(this.addressProvider.PublicAddress);
        }

        /// <summary>
        /// Indicates whether a given key is known to map to this peer. The excludedPeers list
        /// allows the mapping to ignore certain neighbours.
        /// </summary>
        /// <param name="key">The hash key that</param>
        /// <param name="excludedPeers">A collection of address that we wish to exclude in the lookup. 
        /// If the local node is included in this list the result will always be false.</param>
        /// <returns>True if the local peer is responsible for the hash key.</returns>
        public bool MapsToLocalPeer(HashKey key, ICollection<PeerAddress> excludedPeers)
        {
            NodeInfo closestNode = this.GetClosestNode(key, excludedPeers);
            return closestNode != null && this.IsLocal(closestNode);
        }

        /// <summary>
        /// Get the two closest known neighbours information. The returned array will always have 
        /// two items, which may be null. If the router is unintialized then both items will be null;
        /// </summary>
        /// <remarks>Note that it is possible that both NodeInfos refer to the same peer if there is only one other 
        /// known peer in the network</remarks>
        /// <returns>An array of NodeInfo instance with length == 2. 
        /// The first neighbour is the immediate predecessor, the second is the immediate successor</returns>
        public NodeInfo[] GetImmediateNeighbours()
        {
            NodeInfo[] result = new NodeInfo[2];

            if (this.IsInitialized)
            {
                result[0] = this.mReplicaLeafset.ImmediatePredecessor;
                result[1] = this.mReplicaLeafset.ImmediateSuccessor;
            }

            return result;
        }

        public NodeInfo[] GetRoutingTableNodes()
        {
            //return this.LeafSet.GetLeafSetNodes();
            return this.mRoutingTable.GetNodes();
        }

        public void QuarantinePeer(PeerAddress address)
        {
            if (address.Equals(this.addressProvider.PublicAddress))
            {
                this.QuarantineSelf();
                return;
            }

            if (this.IsInThisTier(address) && !this.mQuarantineList.Contains(address))
            {
                if (this.mReplicaLeafset.IsCandidateForAddition(address))
                {
                    this.mReplicaLeafset.AddNeighbour(address);
                }

                Logger.TraceInformation(LogTag.DHT, "{1} : Peer {0} has been quarantined", address, this.Description());
                this.mQuarantineList.Add(address);
                this.eventQueue.Schedule((int)Parameters.LeafsetQuarantineTime.TotalMilliseconds, this.ReleasePeerFromQuarantine, address);

                if (this.mLeafSet.UniqueNodeCount == 0 && !this.mHasOutstandingJoinRequest)
                {
                    this.MakeIterativeJoinRequest(address);
                }
            }
        }

        public void Shutdown()
        {
            this.mRouteRequestHandlingEnabled = false;
            if (null != this.mLeafSet)
            {
                this.mLeafSet.NeighbourAdditionEvent -= this.LeafsetAdditionHandler;
                this.mLeafSet.NeighbourRemovalEvent -= this.LeafsetRemovalHandler;
            }
            if (this.mReplicaLeafset != null)
            {
                this.mReplicaLeafset.NeighbourAdditionEvent -= this.ReplicaLeafsetAdditionHandler;
                this.mReplicaLeafset.NeighbourRemovalEvent -= this.replicaCandidateDepartureEventDelegate;
            }

            this.mQuarantineList.Clear();
            this.mDelayedRouteRequests.Clear();
            this.mDelayedBroadcastRequests.Clear();

            this.mLeafSet = null; // new LeafSet(); 
            this.mReplicaLeafset = null;
            this.mRoutingTable = null;
        }

        public bool RemoveNeighbour(PeerAddress address)
        {
            NodeInfo nodeInfo = new NodeInfo(address, this.LocalHashKey);
            bool nodeWasInLeafset = this.mLeafSet.Contains(nodeInfo);

            this.mRoutingTable.RemoveNodeInfo(nodeInfo);
            this.mReplicaLeafset.RemoveNeighbour(address);
            this.mLeafSet.RemoveNeighbour(address);

            if (nodeWasInLeafset)
            {
                NodeInfo possibleReplacement = this.mRoutingTable.BestCandidateForRouteTo(nodeInfo.Key);

                if (null != possibleReplacement && !address.Equals(possibleReplacement.Address))
                {
                    this.mLeafSet.AddNeighbour(possibleReplacement.Address);
                    this.mReplicaLeafset.AddNeighbour(possibleReplacement.Address);
                    this.mConnectionTable.ConfirmConnection(possibleReplacement.Address);
                }
            }

            if (this.mQuarantineList.Contains(address))
            {
                this.mQuarantineList.Remove(address);
            }

            return nodeWasInLeafset;
        }

        // ******************************************************************************************************************

        protected virtual String Description()
        {
            return "";
        }

        protected virtual void ConnectionEstablishedHandler(PeerAddress address)
        {
            this.QuarantinePeer(address);
        }

        protected virtual void ConnectionLostHandler(PeerAddress address)
        {
            this.RemoveNeighbour(address);
        }

        protected virtual bool IsInThisTier(PeerAddress address)
        {
            return true;
        }

        protected void MakeLeaveRequest()
        {
            if (!this.IsInitialized || !this.IsInThisTier(this.addressProvider.PublicAddress))
            {
                return;
            }

            for (int i = -this.mLeafSet.PredecessorCount; i <= this.mLeafSet.SuccessorCount; i++)
            {
                Logger.TraceInformation(LogTag.DHT, "**d {0} \t {1}", this.mLeafSet[i].Address, this.mLeafSet[i].Key);
            }

            int replacementOffset = (this.mLeafSet.SuccessorCount + this.mLeafSet.PredecessorCount) / 2 + 1;
            for (int i = -this.mLeafSet.PredecessorCount; i <= this.mLeafSet.SuccessorCount; i++)
            {
                if (i != 0)
                {
                    int replacementIndex = Math.Sign(i) * (Math.Abs(i) - replacementOffset);

                    Debug.Assert(replacementIndex >= -this.mLeafSet.PredecessorCount &&
                        replacementIndex <= this.mLeafSet.SuccessorCount && replacementIndex != 0,
                        String.Format("Invalid replacement index {0}", replacementIndex));

                    this.SendLeaveMessage(this.mLeafSet[i].Address, this.mLeafSet[replacementIndex].Address);
                }
            }

            // Do the same for the replica leafset
            for (int i = -this.mReplicaLeafset.PredecessorCount; i <= this.mReplicaLeafset.SuccessorCount; i++)
            {
                // But only if we haven't already sent a request there.
                if (i != 0 && !this.mLeafSet.Contains(this.mReplicaLeafset[i]))
                {
                    NodeInfo closerNode = this.GetNextHopForExcludingSelf(this.mReplicaLeafset[i].Key);

                    if (closerNode != null && !closerNode.Address.Equals(this.mLeafSet[0].Address))
                    {
                        this.SendLeaveMessage(this.mReplicaLeafset[i].Address, closerNode.Address);
                    }
                }
            }

            // Turn off local routing - so all routed messages get sent to another peer.
            this.QuarantineSelf();
        }

        // Protocol methods need to be protected so they are visible to the MessageParser in derived classes
        #region Protocol methods

        [RouterProtocolMethod(RouterMethod.RouterHandleIterativeJoinRequest)]
        protected void HandleIterativeJoinRequest(PeerAddress sourcesAddress)
        {
            // Find the closest known peer to the source, excluding the source.
            HashKey sourceKey = HashKey.Hash(sourcesAddress);
            NodeInfo closestNode = this.GetClosestNode(sourceKey, sourcesAddress);

            if (closestNode == null)
            {
                Logger.TraceWarning(LogTag.DHT, "No candidate for iterative join request!");
                closestNode = this.mLeafSet.LocalNodeInfo;
            }

            // If the source of the request is a candidate then we issue a synchronization request with it 
            // in order to allow it to fill its leafset even if it is not in this tier. This may 
            // result in two synchronisation requests (from different peers) being sent because this peer may not 
            // be the closest node, however it adds some more robustness because the closest node may not be 
            // available (quarantined or require a relayed connection, etc.) yet. If this peer is not quarantined 
            // it also means that the peer can begin routing faster as it doesn't have to wait for the reply for 
            // last IterativeJoinReply message.
            if (this.IsLocal(closestNode)
                || this.mLeafSet.IsCandidateForAddition(sourcesAddress))
            {
                Logger.TraceInformation(LogTag.DHT, "Synchronizing with {0}", sourcesAddress);
                this.MakeSynchronizationRequest(sourcesAddress);
                this.MakeRoutingTableEntryConsiderationRequest(sourcesAddress);
            }

            Logger.TraceInformation(LogTag.DHT, "Notifying joining peer {0} of a closer candidate ({1})", sourcesAddress, closestNode.Address);
            TransportEnvelope envelope = this.GetMessageFor(sourcesAddress, QualityOfService.Reliable);
            envelope.Qos.Priority = QosPriority.High;

            this.RemoteCall(envelope, this.HandleIterativeJoinReply, closestNode.Address);
            this.SendMessage(envelope);

            this.mConnectionTable.ConfirmConnection(sourcesAddress);
        }

        [RouterProtocolMethod(RouterMethod.RouterHandleIterativeJoinReply)]
        protected void HandleIterativeJoinReply(PeerAddress closestKnownPeer)
        {
            // Try a connection with the peer - the more the merrier for the routing table when joining.
            this.mConnectionTable.ConfirmConnection(closestKnownPeer);

            if (closestKnownPeer.Equals(this.CurrentEnvelope.Source))
            {
                this.mHasOutstandingJoinRequest = false;
            }
            else
            {
                this.MakeIterativeJoinRequest(closestKnownPeer);
            }
        }

        /*
        [RouterProtocolMethod(RouterMethod.RouterHandleJoinRequest)]
        protected void HandleJoinRequest(PeerAddress messageOrigin)
        {
            // We exclude the message origin from the search for closest node because it may have already been 
            // inserted into the leafset by a connection established event. If this is so the join message 
            // will be incorrectly routed back to the origin.
            List<PeerAddress> excludeList = new List<PeerAddress>();
            excludeList.Add(messageOrigin);
            if (null != this.CurrentEnvelope)
            {
                excludeList.Add(this.CurrentEnvelope.Source);
            }
            this.HandleExcludedJoinRequest(messageOrigin, excludeList);
        }
        */

        [RouterProtocolMethod(RouterMethod.RouterHandleLeaveRequest)]
        protected void HandleLeaveRequest(PeerAddress possibleReplacement)
        {            
            this.RemoveNeighbour(this.CurrentEnvelope.Source);

            if (!possibleReplacement.Equals(this.mLeafSet[0].Address)
                && !possibleReplacement.Equals(this.CurrentEnvelope.Source))
            {
                if (this.mConnectionTable.IsConnected(possibleReplacement)
                    && !this.mLeafSet.Contains(possibleReplacement))
                {
                    if (this.mQuarantineList.Contains(possibleReplacement))
                    {
                        this.ReleasePeerFromQuarantine(possibleReplacement);
                    }
                    else
                    {
                        this.AddNeighbour(possibleReplacement);
                    }
                }

                this.mConnectionTable.ConfirmConnection(possibleReplacement);
            }
        }

        [RouterProtocolMethod(RouterMethod.RouterConsiderForRoutingTableEntry)]
        protected void ConsiderForRoutingTableEntry(List<PeerAddress> addressList)
        {
            foreach (PeerAddress adddress in addressList)
            {
                this.ConsiderAddressForRoutingTableInsertion(adddress);
            }
        }

        [RouterProtocolMethod(RouterMethod.RouterHandleSynchronizeRequest)]
        protected void HandleSynchronizeRequest(LeafSet leafSet, byte isQuarantined)
        {
            if (leafSet[0].Address.Equals(this.addressProvider.PublicAddress))
            {
                Logger.TraceWarning(LogTag.DHT, "Ignoring request to synchronize with self.");
                return;
            }

            if (leafSet[0] == null)
            {
                Logger.TraceError(LogTag.DHT, "Leafset given for synchronization from {0} does not contain local node", this.CurrentEnvelope.Source);
            }

            Logger.TraceInformation(LogTag.DHT, "Synchronizing leafset with {0}. Has {1} unique neighbours", leafSet.LocalNodeInfo.Address, leafSet.UniqueNodeCount);


            // Check for inconsistencies.
            if (leafSet.Contains(this.mLeafSet[0]) &&
                !this.mLeafSet.Contains(leafSet[0].Address) &&
                !this.mLeafSet.IsCandidateForAddition(leafSet[0].Address))
            {
                // The peer who made this request thinks the local peer is its neighbour, but
                // the local peer doesn't agree. Synchronize so it can reconcile the problem.
                Logger.TraceWarning(LogTag.DHT, "Inconsistent leaf sets with {0}", leafSet[0].Address);

                ICollection<PeerAddress> excludedAddresses = new List<PeerAddress>(leafSet);
                excludedAddresses.Add(this.addressProvider.PublicAddress);
                NodeInfo closestNode = this.GetClosestNode(leafSet[0].Key, excludedAddresses);
                if (closestNode != null)
                {
                    Logger.TraceInformation(LogTag.DHT, "Using neighbour {1} to reconcile inconsistency for {0}.", leafSet[0].Address, closestNode.Address);
                    this.MakeReconcileInconsistencyRequest(leafSet[0].Address, closestNode.Address);
                }
                else
                {
                    Logger.TraceInformation(LogTag.DHT, "Cannot find an appropriate neighbour to reconcile inconsistency for {0}.", leafSet[0].Address);
                    this.MakeSynchronizationRequest(leafSet[0].Address);
                }
            }
            else if (leafSet.UniqueNodeCount == 0 && this.mLeafSet.UniqueNodeCount > 1)
            {
                Logger.TraceWarning(LogTag.DHT, "Neighbour {0} has empty leafset.", leafSet[0].Address);
                this.MakeSynchronizationRequest(leafSet[0].Address);
            }

#if TRACE
            for (int i = -leafSet.PredecessorCount; i <= leafSet.SuccessorCount; i++)
            {
                Logger.TraceInformation(LogTag.DHT, "**z {0} \t {1}", leafSet[i].Address, leafSet[i].Key);
            }
#endif

            // Check for additions.
            for (int i = -leafSet.PredecessorCount; i <= leafSet.SuccessorCount; i++)
            {
                if (this.mLeafSet.IsCandidateForAddition(leafSet[i].Address))
                {
                    this.mConnectionTable.ConfirmConnection(leafSet[i].Address);
                }
            }

            // Check for removals.
            for (int i = -this.mLeafSet.PredecessorCount; i < this.mLeafSet.SuccessorCount; i++)
            {
                if (i != 0 && leafSet.IsCandidateForAddition(this.mLeafSet[i].Address))
                {
                    Logger.TraceInformation(LogTag.DHT, "Ping {0} to check for removal", this.mLeafSet[i].Address);
                    this.MakePingRequest(this.mLeafSet[i].Address, leafSet[0].Address);
                    //this.mConnectionTable.ConfirmConnection(this.mLeafSet[i].Address);

                    // This could also happen if the node is up and was added locally
                    // prior to addition with the neighbour. This will only be the case until 
                    // we sync with this neighbour again, so on average this will only incurr
                    // 1 unneccessary ping message.
                }
            }

            if ((isQuarantined & 0x01) == 0x00) // Does neighbour think it is not quarantined
            {
                Logger.TraceInformation(LogTag.DHT, "{1} : Releasing neighbour {0} from quarantine because it says its active.", leafSet[0].Address, this.Description());
                this.ReleasePeerFromQuarantine(leafSet[0].Address);

                /*
                if (!this.mRouteRequestHandlingEnabled)
                {
                    this.StartRouting();
                }*/
            }
        }

        [RouterProtocolMethod(RouterMethod.RouterPing)]
        protected void Ping(PeerAddress queryAddress)
        {
            if (!queryAddress.Equals(PeerAddress.Nowhere) && !queryAddress.Equals(this.addressProvider.PublicAddress))
            {
                Logger.TraceInformation(LogTag.DHT, "Ping from {0} asks me to confirm connection with {1}", this.CurrentEnvelope.Source, queryAddress);
                this.mConnectionTable.ConfirmConnection(queryAddress);
            }
        }

        [RouterProtocolMethod(RouterMethod.RouterReconcileInconsistency)]
        protected void ReconcileInconsistency(PeerAddress closestPeer)
        {
            // The sending peer has sent the closest peer it knows because it 
            // believes this peers leaf set is out of sync. If the peer is a candidate 
            // then it is not known and closer so we should perform a join operation 
            // to find the any closer peers
            if (this.mLeafSet.IsCandidateForAddition(closestPeer))
            {
                Logger.TraceInformation(LogTag.DHT, "Peer {0} has indicated possible inconcistency. Contacting {1} to reconcile",
                    this.CurrentEnvelope.Source, closestPeer);
                this.MakeIterativeJoinRequest(closestPeer);
            }
        }

        [RouterProtocolMethod(RouterMethod.HandleStabilizationPingMessage)]
        protected void HandleStabilizationPingMessage()
        {
            // do nothing here. 
        }

        #endregion

        // ******************************************************************************************************************

        // Called when the peer comes online.
        private void BeginInitialization()
        {
            PeerAddress localAddress = this.addressProvider.PublicAddress;

            // Remove any event subscriptions on the old leafsets, to allow its future garbage collection.
            if (this.mLeafSet != null)
            {
                this.mLeafSet.NeighbourAdditionEvent -= this.LeafsetAdditionHandler;
                this.mLeafSet.NeighbourRemovalEvent -= this.LeafsetRemovalHandler;
            }
            if (this.mReplicaLeafset != null)
            {
                this.mReplicaLeafset.NeighbourAdditionEvent -= this.ReplicaLeafsetAdditionHandler;
                this.mReplicaLeafset.NeighbourRemovalEvent -= this.replicaCandidateDepartureEventDelegate;
            }

            Logger.TraceInformation(LogTag.DHT, "{0} starting DHT ...", localAddress);

            this.mLeafSet = new LeafSet();
            this.mLeafSet.Initialize(localAddress);
            this.mLeafSet.NeighbourAdditionEvent += this.LeafsetAdditionHandler;
            this.mLeafSet.NeighbourRemovalEvent += this.LeafsetRemovalHandler;

            this.mReplicaLeafset = new LeafSet();
            this.mReplicaLeafset.Initialize(localAddress);
            this.mReplicaLeafset.NeighbourAdditionEvent += this.ReplicaLeafsetAdditionHandler;
            this.mReplicaLeafset.NeighbourRemovalEvent += this.replicaCandidateDepartureEventDelegate;

            this.mRoutingTable = new RoutingTable(4, this.mLeafSet.LocalNodeInfo); // 16 rows x 40 columns = 640 entries

            this.mHasOutstandingJoinRequest = false;
            this.QuarantineSelf();

            this.mNeedToSyncronize = true;
            this.mSynchronizeLeafSetTask.Start(); // Will only start if its not already running, so we don't need to check
        }

        private void QuarantineSelf()
        {
            PeerAddress address = this.addressProvider.PublicAddress;

            if (this.IsInThisTier(address) && !this.mQuarantineList.Contains(address))
            {
                Logger.TraceInformation(LogTag.DHT, "{0} : Quarantining self", this.Description());
                this.mQuarantineList.Add(address);

                this.eventQueue.Schedule((int)Parameters.LeafsetQuarantineTime.TotalMilliseconds, this.ReleaseSelfFromQuarantine);
            }
        }

        private void ReleaseSelfFromQuarantine()
        {
            PeerAddress address = this.addressProvider.PublicAddress;

            if (this.mQuarantineList.Contains(address))
            {
                Logger.TraceInformation(LogTag.DHT, "{0} : Releasing self from quarantine", this.Description());
                this.mQuarantineList.Remove(address); // Must be called before StartRouting

                // Perform a synchronisation to notifiy neighbours that we are no longer quarantined.
                this.PerformSynchronization();
                this.StartRouting();

                EventHandler<LeafSetChangeEventArgs> handler = this.localPeerReleasedFromQuarantineEventDelegate;
                if (handler != null)
                {
                    handler(this, new LeafSetChangeEventArgs(this.mLeafSet[0]));
                }
            }
        }

        private void ReleasePeerFromQuarantine(PeerAddress address)
        {
            if (this.mQuarantineList.Contains(address))
            {
                Logger.TraceInformation(LogTag.DHT, "{1} : Peer {0} has been released from quarantine", address, this.Description());
                this.mQuarantineList.Remove(address); // Must be called before StartRouting

                if (this.mConnectionTable.IsConnected(address))
                {
                    this.AddNeighbour(address);

                    if (!this.IsRouting)
                    {
                        this.StartRouting();
                    }
                }
            }
        }

        private void StartRouting()
        {
            if (this.mRouteRequestHandlingEnabled)
            {
                return;
            }

            if (null == this.mLeafSet || !this.mLeafSet.IsInitialized)
            {
                Logger.TraceWarning(LogTag.DHT, "Cannot start routing until leafset is initialized.");
                return;
            }

            this.mRouteRequestHandlingEnabled = true;

            Logger.TraceInformation(LogTag.DHT, "{0} Handling delayed route requests", this.Description());
            foreach (DhtEnvelope requestEnvelope in this.mDelayedRouteRequests)
            {
                Debug.Assert(null != requestEnvelope.DestinationKey, "Attempt to handle a delayed routed message with an invalid destination key");
                this.HandleDelayedRouteRequest(requestEnvelope);
            }

            foreach (DelayedBroadcastRequest broadcastRequest in this.mDelayedBroadcastRequests)
            {
                if (null == broadcastRequest.StartRange)
                {
                    this.HandleBroadcast(broadcastRequest.Envevelope, broadcastRequest.EndRange, broadcastRequest.Source);
                }
                else
                {
                    this.HandleExtendedBroadcast(broadcastRequest.Envevelope, broadcastRequest.StartRange, broadcastRequest.EndRange, broadcastRequest.Source);
                }
            }

            this.mDelayedRouteRequests.Clear();
            this.mDelayedBroadcastRequests.Clear();

            List<PeerAddress> sentToAlready = new List<PeerAddress>(LeafSet.MaximumNumberOfNeighbours);
            // Synchronise with all replica leafset nodes that are not in the routing leafset to advertise that this peer is active
            for (int i = -this.mReplicaLeafset.PredecessorCount; i <= this.mReplicaLeafset.SuccessorCount; i++)
            {
                PeerAddress address = this.mReplicaLeafset[i].Address;
                if (i != 0 && !this.mLeafSet.Contains(address) && !sentToAlready.Contains(address))
                {
                    // Synchronize with the neihghbours to let them know this node is no longer quarantined.
                    this.MakeSynchronizationRequest(address);
                    sentToAlready.Add(address);
                }
            }
        }

        // True if the given peer is part of this tier and not quarantined.
        private bool IsCurrentlyParticipating(PeerAddress address)
        {
            return this.IsInThisTier(address) && !this.mQuarantineList.Contains(address);
        }

        private NodeInfo GetNextHopForExcludingSelf(HashKey destinationKey)
        {
            return this.GetClosestNode(destinationKey, new PeerAddress[] { this.addressProvider.PublicAddress });
        }

        private NodeInfo GetClosestNode(HashKey destinationKey, PeerAddress excludedAddress)
        {
            return this.GetClosestNode(destinationKey, new PeerAddress[] { excludedAddress });
        }

        private NodeInfo GetClosestNode(HashKey destinationKey, ICollection<PeerAddress> excludedAddressList)
        {
            Debug.Assert(this.IsInitialized, "Attempt to get next hop for unitialized router");

            NodeInfo closestInLeafset = this.mLeafSet.GetNodeClosestTo(destinationKey, excludedAddressList);
            NodeInfo closestInRoutingTable = this.mRoutingTable.BestCandidateForRouteTo(destinationKey, excludedAddressList);

            // HACK: If we don't have a connection to the route in the routing table, then ignore it.
            //       This is a work around for ticket #930.  For some reason the routing table has stale
            //       routes in it, and these are sent to new peers as their closest node.
            //       The new peer then wastes time trying to connect to a peer that is probably offline.
            if (closestInRoutingTable != null && !this.mConnectionTable.IsConnected(closestInRoutingTable.Address))
            {
                closestInRoutingTable = null;
            }

            if (null == closestInRoutingTable)
            {
                return closestInLeafset;
            }

            if (null == closestInLeafset)
            {
                return null;
            }

            if (closestInRoutingTable.Key.AbsoluteDistanceFrom(destinationKey) < closestInLeafset.Key.AbsoluteDistanceFrom(destinationKey))
            {
                return closestInRoutingTable;
            }

            return closestInLeafset;
        }

        private void AddNeighbour(PeerAddress neighbourAddress)
        {
            if (!this.IsInThisTier(neighbourAddress))
            {
                return;
            }

            Debug.Assert(this.IsInitialized, "Cannot add neighbour if not initialized");
            Debug.Assert(!this.mQuarantineList.Contains(neighbourAddress), "Attempting to put peer in leafset that is still in quarantine");            

            if (this.mLeafSet.Contains(neighbourAddress))
            {
                // The neighbour has possibly been up and down without detection.
                this.mLeafSet.RemoveNode(new NodeInfo(neighbourAddress, this.LocalHashKey));
                this.mNeedToSyncronize = true;
            }

            this.mRoutingTable.InsertNodeInfo(new NodeInfo(neighbourAddress, this.LocalHashKey));
            this.mReplicaLeafset.AddNeighbour(neighbourAddress);
            this.mLeafSet.AddNeighbour(neighbourAddress);
        }

        private void HandleMessage(DhtEnvelope messageEnvelope)
        {
            this.mChildRouter.ProcessMessage(messageEnvelope);
        }

        private void ConsiderAddressForRoutingTableInsertion(PeerAddress address)
        {
            if (!this.IsInThisTier(address))
            {
                return;
            }

            if (this.mRoutingTable.IsCandidateForInsertion(address))
            {
                this.mConnectionTable.ConfirmConnection(address);
            }
        }

        private void ReplicaLeafsetAdditionHandler(object sender, LeafSetChangeEventArgs e)
        {
            Logger.TraceInformation(LogTag.DHT, "Added neighbour {0} to replica leaf set.", e.NodeInfo.Address);

            EventHandler<LeafSetChangeEventArgs> handler = this.replicaCandidateArrivalEventDelegate;
            if (handler != null)
            {
                handler.Invoke(this, e);
            }
        }

        private void ReplicaLeafsetRemovalHandler(object sender, LeafSetChangeEventArgs e)
        {
            Logger.TraceInformation(LogTag.DHT, "Removed neighbour {0} from replica leaf set.", e.NodeInfo.Address);

            EventHandler<LeafSetChangeEventArgs> handler = this.replicaCandidateDepartureEventDelegate;
            if (handler != null)
            {
                handler.Invoke(this, e);
            }
        }

        private void LeafsetAdditionHandler(object sender, LeafSetChangeEventArgs e)
        {
            Logger.TraceInformation(LogTag.DHT, "Added neighbour {0} to routing leaf set.", e.NodeInfo.Address);
            this.mNeedToSyncronize = true;

            EventHandler<LeafSetChangeEventArgs> handler = this.neighbourAdditionEventDelegate;
            if (handler != null && !this.mQuarantineList.Contains(this.mLeafSet[0].Address))
            {
                handler.Invoke(sender, e);
            }
        }

        private void LeafsetRemovalHandler(object sender, LeafSetChangeEventArgs e)
        {
            this.mNeedToSyncronize = true;

            EventHandler<LeafSetChangeEventArgs> handler = this.neighbourRemovalEventDelegate;
            if (handler != null && !this.mQuarantineList.Contains(this.mLeafSet[0].Address))
            {
                handler.Invoke(sender, e);
            }
        }

        private void PeriodicLeasfSetSyncronization()
        {
            if (this.connectivityReporter.Status == ConnectivityStatus.Offline)
            {
                return; // I don't think this is called when offline because it is in a regular task now. TODO : Check this before removing.
            }

            if (!this.IsInitialized)
            {
                return;
            }

            if (this.mNeedToSyncronize)
            {
                this.PerformSynchronization();
            }
        }

        private void PerformSynchronization()
        {
            if (!this.IsInThisTier(this.addressProvider.PublicAddress))
            {
                // This peer is not in a member of this tier its neighbours don't know about it, so :

                // 1. Confirm connection with each address in the leafset becasue this is the only way to detect if they go down
                for (int i = -this.mLeafSet.PredecessorCount; i <= this.mLeafSet.SuccessorCount; i++)
                {
                    // Duplicate calls to ConfirmConnection don't incur much extra cost so we can ignore duplicates in the leafest.
                    if (i == 0) continue; // skip self.
                    this.mConnectionTable.ConfirmConnection(this.mLeafSet[i].Address);
                }

                // 2. Make a join request which will result in a synchronise response from the closet peer.
                NodeInfo closestNode = this.GetNextHopForExcludingSelf(this.mLeafSet.LocalNodeInfo.Key);
                if (closestNode != null)
                {
                    this.MakeIterativeJoinRequest(closestNode.Address);
                }

                this.mNeedToSyncronize = false;
                return;
            }

            List<PeerAddress> duplicateList = new List<PeerAddress>();

            for (int i = -this.mLeafSet.PredecessorCount; i <= this.mLeafSet.SuccessorCount; i++)
            {
                if (i == 0) continue; // skip self.

                if (!duplicateList.Contains(this.mLeafSet[i].Address))
                {
                    this.MakeSynchronizationRequest(this.mLeafSet[i].Address);
                    duplicateList.Add(this.mLeafSet[i].Address);
                }
            }

            this.mNeedToSyncronize = false;
        }

        /// <summary>
        /// The iterative join request process tries to find the closest peer in the dht to this peer
        /// through an iterative search. The first request can be sent to any peer, which will return 
        /// any peers it knows of that are closer than itself. A requaest is then sent to that closest peer,
        /// and so on until a peer is found that is closer to all others. The closest peer will initiate 
        /// a leafset synchronisation which enables this peer to fill its leafset appropriatly.
        /// This allows a joining process that can occur accross tiers and also provides an oppurtunity to
        /// fill the routing table with some peers prior to starting. The process can start when the first 
        /// neighbour has been connected with, but there should only be a single process active at any one time.
        /// </summary>
        /// <param name="closestKnownPeer"></param>
        private void MakeIterativeJoinRequest(PeerAddress closestKnownPeer)
        {
            Logger.TraceInformation(LogTag.DHT, "{1} Making iterative lookup request with {0}", closestKnownPeer, this.Description());

            QualityOfService qos = new QualityOfService(true);
            qos.Priority = QosPriority.Highest;
            TransportEnvelope envelope = this.GetMessageFor(closestKnownPeer, qos);

            this.RemoteCall(envelope, this.HandleIterativeJoinRequest, this.addressProvider.PublicAddress);
            this.SendMessage(envelope);
            this.mHasOutstandingJoinRequest = true;
        }

        /*
        private void MakeJoinRequest(PeerAddress address, PeerAddress source)
        {
            this.MakeJoinRequest(address, source, null);
        }

        private void MakeJoinRequest(PeerAddress address, PeerAddress source, ICollection<PeerAddress> excludedAddresses)
        {
            ExcludedResendEventArgs eventArgs = new ExcludedResendEventArgs(source);
            FailHandlerQos errorHandledQos = new FailHandlerQos(QualityOfService.Reliable, eventArgs);
            errorHandledQos.AllowableDelayMilliseconds = 0;
            errorHandledQos.Priority = QosPriority.Highest;
            TransportEnvelope envelope = this.GetMessageFor(address, errorHandledQos);

            eventArgs.Exclude(source);
            eventArgs.Exclude(address);
            if (null != excludedAddresses)
            {
                foreach (PeerAddress excludedAddress in excludedAddresses)
                {
                    eventArgs.Exclude(excludedAddress);
                }
            }

            errorHandledQos.FailureEvent += this.ExcludedJoinEventHandler;

            this.RemoteCall(envelope, this.HandleJoinRequest, source);
            this.SendMessage(envelope);
        }       

        private void ExcludedJoinEventHandler(object sender, SendFailureEventArgs e)
        {
            ExcludedResendEventArgs args = e as ExcludedResendEventArgs;

            if (args.NthAttempt == 2 || args.IsFinalAttempt)
            {
                this.HandleExcludedJoinRequest(args.MessageOrigin, args.ExcludedNeighbours);
            }
        }

        private void HandleExcludedJoinRequest(PeerAddress messageOrigin, ICollection<PeerAddress> excludeList)
        {
            HashKey destinationKey = HashKey.Hash(messageOrigin);
            NodeInfo closestNode = this.GetNextHopFor(destinationKey, excludeList);

            if (null == closestNode)
            {
                Logger.TraceError(LogTag.DHT, "Failed to find an appropriate neighbour for join request. Local node ({0}) is excluded.",
                    NetworkContext.CurrentContext.PublicAddress);
                return;
            }

            if (closestNode.IsLocalNode)
            {
                this.MakeSynchronizationRequest(messageOrigin, 100);
                this.MakeRoutingTableEntryConsiderationRequest(messageOrigin, 10);
            }
            else
            {
                Logger.TraceInformation(LogTag.DHT, "Performing join request with {0} on behalf of {1}", closestNode.Address, messageOrigin);

                Debug.Assert(null == this.CurrentEnvelope || !this.CurrentEnvelope.Source.Equals(closestNode.Address),
                        String.Format("Route destination ({0}) is the same as the current message source at {1}",
                        closestNode.Address, NetworkContext.CurrentContext.PublicAddress));

                // Make a join request with the closest node on behalf of the message origin.
                this.MakeJoinRequest(closestNode.Address, messageOrigin, excludeList);

                // If the message origin is a candidate for this leafset we should synchronise.
                if (this.mLeafSet.IsCandidateForAddition(messageOrigin))
                {
                    this.MakeSynchronizationRequest(messageOrigin, 1000);
                }
            }
        }
        */

        private void SendLeaveMessage(PeerAddress address, PeerAddress possibleReplacement)
        {
            QualityOfService immediateAndUnorderedQos = new QualityOfService(false);

            immediateAndUnorderedQos.MustBeInOrder = false;
            immediateAndUnorderedQos.Priority = QosPriority.High;
            TransportEnvelope envelope = this.GetMessageFor(address, immediateAndUnorderedQos);

            this.RemoteCall(envelope, this.HandleLeaveRequest, possibleReplacement);
            this.SendMessage(envelope);
        }

        private void MakeRoutingTableEntryConsiderationRequest(PeerAddress address)
        {
            if (!this.IsInThisTier(address))
            {
                return;
            }

            List<PeerAddress> possibleEntries = this.mRoutingTable.ConstructListOfPossibleEntriesFor(address, 10);
            if (possibleEntries != null && possibleEntries.Count > 0)
            {
                QualityOfService reliableAndDelayableQos = new QualityOfService(true);
                TransportEnvelope envelope = this.GetMessageFor(address, reliableAndDelayableQos);
                envelope.Qos.Priority = QosPriority.Low;

                this.RemoteCall(envelope, this.ConsiderForRoutingTableEntry, possibleEntries);
                this.SendMessage(envelope);
            }
        }

        private void MakeSynchronizationRequest(PeerAddress address)
        {
            QualityOfService reliableAndDelayableQos = new QualityOfService(true);
            TransportEnvelope envelope = this.GetMessageFor(address, reliableAndDelayableQos);
            envelope.Qos.Priority = QosPriority.Highest;

            byte isQuarantined = 0x00;
            if (this.mQuarantineList.Contains(this.mLeafSet[0].Address))
            {
                isQuarantined = 0x01;
            }

            Logger.TraceInformation(LogTag.DHT, "{1} Sending syncronize request to {0} (quarantined = {2})", address, this.Description(), isQuarantined);

            this.RemoteCall(envelope, this.HandleSynchronizeRequest, this.mLeafSet, isQuarantined);
            this.SendMessage(envelope);
        }


        private void MakePingRequest(PeerAddress destination, PeerAddress queryAddress)
        {
            TransportEnvelope envelope = this.GetMessageFor(destination, QualityOfService.Reliable);
            envelope.Qos.Priority = QosPriority.Medium;

            this.RemoteCall(envelope, this.Ping, queryAddress);
            this.SendMessage(envelope);
        }

        private void MakeReconcileInconsistencyRequest(PeerAddress destination, PeerAddress closestPeerAddress)
        {
            TransportEnvelope envelope = this.GetMessageFor(destination, QualityOfService.Reliable);
            envelope.Qos.Priority = QosPriority.Medium;

            this.RemoteCall(envelope, this.ReconcileInconsistency, closestPeerAddress);
            this.SendMessage(envelope);
        }

        // This is not currently used so I'm commenting it - but it may be useful in the future.
        /* 
        // Check the two closest neighbours last keep alive times. If we have (and ONLY have) receieved a keep alive message 
        // from a particular neighbour but not since a particular time then we can consider the neighbour lost, in which case 
        // we can safely remove it from the leaf set.
        private void CheckNeighboursExist()
        {
            if (this.mLeafSet != null && this.mLeafSet.IsInitialized && this.mLeafSet.UniqueNodeCount > 0
                && this.IsPartOfNetwork(this.mLeafSet[0].Address))
            {
                PeerAddress predecessorAddress = this.mLeafSet.ImmediatePredecessor.Address;
                PeerAddress successorAddress = this.mLeafSet.ImmediateSuccessor.Address;

                this.SendKeepAlive(predecessorAddress);
                if (predecessorAddress.Equals(this.mNeighboursKeepAlive[0].NeighboursAddress))
                {
                    if ((NetworkEventQueue.Instance.Now - this.mNeighboursKeepAlive[0].LastAckTime) > Parameters.MaximumStatusCheckPeriod)
                    {
                        Logger.TraceWarning(LogTag.DHT, "{1} : Immediate predecessor {0} has failed to send acks.", predecessorAddress, this.Description());
                        this.RemoveNeighbour(predecessorAddress);
                        this.mConnectionTable.ConfirmConnection(predecessorAddress);
                    }
                }
                else
                {
                    this.mNeighboursKeepAlive[0].NeighboursAddress = predecessorAddress;
                    this.mNeighboursKeepAlive[0].LastAckTime = NetworkEventQueue.Instance.Now;
                }

                if (this.mLeafSet.UniqueNodeCount > 1)
                {
                    this.SendKeepAlive(successorAddress);
                    if (successorAddress.Equals(this.mNeighboursKeepAlive[1].NeighboursAddress))
                    {
                        if ((NetworkEventQueue.Instance.Now - this.mNeighboursKeepAlive[1].LastAckTime) > Parameters.MaximumStatusCheckPeriod)
                        {
                            Logger.TraceWarning(LogTag.DHT, "{1} : Immediate successor {0} has failed to send acks.", successorAddress, this.Description());
                            this.RemoveNeighbour(successorAddress);
                            this.mConnectionTable.ConfirmConnection(successorAddress);
                        }
                    }
                    else
                    {
                        this.mNeighboursKeepAlive[1].NeighboursAddress = successorAddress;
                        this.mNeighboursKeepAlive[1].LastAckTime = NetworkEventQueue.Instance.Now;
                    }
                }
            }
        }

        private void SendKeepAlive(PeerAddress address)
        {
            TransportEnvelope envelope = this.GetMessageFor(address, 
                new QualityOfService(false, (uint)Parameters.MinimumStatusCheckPeriod.TotalMilliseconds));

            envelope.Qos.Priority = QosPriority.Highest;

            this.RemoteCall(envelope, this.RecordKeepAlive);
            this.SendMessage(envelope);
        }

        [RouterProtocolMethod(RouterMethod.RouterRecordKeepAlive)]
        protected void RecordKeepAlive()
        {
            if (this.mLeafSet == null || !this.mLeafSet.IsInitialized || !this.IsPartOfNetwork(this.mLeafSet[0].Address))
            {
                return;
            }
                        
            bool isNeighbour = false;

            if (this.mLeafSet.ImmediatePredecessor != null && this.mLeafSet.ImmediatePredecessor.Address.Equals(this.CurrentEnvelope.Source))
            {
                this.mNeighboursKeepAlive[0].NeighboursAddress = this.CurrentEnvelope.Source;
                this.mNeighboursKeepAlive[0].LastAckTime = NetworkEventQueue.Instance.Now;
                isNeighbour = true;
            }
           
            if (this.mLeafSet.ImmediateSuccessor != null && this.mLeafSet.ImmediateSuccessor.Address.Equals(this.CurrentEnvelope.Source))
            {
                this.mNeighboursKeepAlive[1].NeighboursAddress = this.CurrentEnvelope.Source;
                this.mNeighboursKeepAlive[1].LastAckTime = NetworkEventQueue.Instance.Now;
                isNeighbour = true;
            }

            if (!isNeighbour)
            {
                // If the peer should be a neighbour then we should insert it into the leafset.
                if (this.IsPartOfNetwork(this.CurrentEnvelope.Source))
                {
                    NodeInfo nodeInfo = new NodeInfo(this.CurrentEnvelope.Source, this.LocalHashKey);

                    if (this.mLeafSet.UniqueNodeCount == 0 ||
                        nodeInfo.ForwardDistance < this.mLeafSet.ImmediateSuccessor.ForwardDistance ||
                        nodeInfo.BackwardDistance < this.mLeafSet.ImmediatePredecessor.BackwardDistance)
                    {
                        this.AddNeighbour(this.CurrentEnvelope.Source);
                        return;
                    }

                    Logger.TraceInformation(LogTag.DHT, "Peer {0} thinks that it is an immediate neighbour but its not.", this.CurrentEnvelope.Source);
                    this.MakeSynchronizationRequest(this.CurrentEnvelope.Source, 0);
                }
            }
            else
            {
                // If a peer is in quarantine the IsPartOfNetwork() method returns false,
                // so check if the neighbour is in quarantine
                if (this.mQuarantineList.Contains(this.CurrentEnvelope.Source))
                {
                    // Only peers that are out of quarantine send acks so we can release it from quarantine.
                    this.ReleasePeerFromQuarantine(this.CurrentEnvelope.Source);
                }
            }
        }
        */
    }
}
