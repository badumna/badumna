using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.Security.Cryptography;
using System.IO;

using Badumna.Core;
using Badumna.Utilities;

namespace Badumna.DistributedHashTable
{
    class HashKey : IParseable, IComparable, IComparable<HashKey>//, IEquatable<HashKey>, IEquatable<byte[]>
    {
        private const int sLength = 20; // Must be at least 4.
        public static int Length { get { return HashKey.sLength; } }

        private static HashKey sMin = new HashKey();
        public static HashKey Min { get { return HashKey.sMin; } }

        private static HashKey sMid;
        public static HashKey Mid
        {
            get
            {
                if (null != HashKey.sMid)
                {
                    return HashKey.sMid;
                }

                HashKey.sMid = new HashKey();
                HashKey.sMid[0] = 0x80;

                return HashKey.sMid;
            }
        }


        private static HashKey sMax;
        public static HashKey Max
        {
            get
            {
                if (null != HashKey.sMax)
                {
                    return HashKey.sMax;
                }

                HashKey.sMax = new HashKey();
                for (int i = 0; i < HashKey.sLength; i++)
                {
                    HashKey.sMax[i] = 0xff;
                }

                return HashKey.sMax;
            }
        }

        public static bool operator >(HashKey firstOperand, HashKey secondOperand)
        {
            if (null == firstOperand)
            {
                throw new ArgumentNullException("firstOperand");
            }

            return firstOperand.CompareTo(secondOperand) > 0;
        }

        public static bool operator <(HashKey firstOperand, HashKey secondOperand)
        {
            if (null == firstOperand)
            {
                throw new ArgumentNullException("firstOperand");
            }

            return firstOperand.CompareTo(secondOperand) < 0;
        }

        public static bool operator <=(HashKey firstOperand, HashKey secondOperand)
        {
            if (null == firstOperand)
            {
                throw new ArgumentNullException("firstOperand");
            }

            return firstOperand.CompareTo(secondOperand) <= 0;
        }

        public static bool operator >=(HashKey firstOperand, HashKey secondOperand)
        {
            if (null == firstOperand)
            {
                throw new ArgumentNullException("firstOperand");
            }

            return firstOperand.CompareTo(secondOperand) >= 0;
        }

        public static bool operator !=(HashKey firstOperand, HashKey secondOperand)
        {
            if (null == (object)firstOperand)
            {
                if (null == (object)secondOperand)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }

            return !firstOperand.Equals(secondOperand);
        }


        public static bool operator ==(HashKey firstOperand, HashKey secondOperand)
        {
            return !(firstOperand != secondOperand);
        }

        internal HashKey Subtract(HashKey secondOperand)
        {
            return this.DistanceFrom(secondOperand);
        }

        /// <summary>
        /// Divide by 2^power
        /// </summary>
        internal HashKey DivideByPowerOfTwo(int power)
        {
            HashKey key = new HashKey();
            this.mBytes.CopyTo(key.mBytes, 0);

            for (; power > 0; power--)
            {
                key.ShiftRight();
            }

            return key;
        }

        private void ShiftRight()
        {
            this.mBytes[HashKey.Length - 1] = (byte)(this.mBytes[HashKey.Length - 1] >> 1);
            for (int i = HashKey.Length - 2; i >= 0; i--)
            {
                byte carryByte = (byte)((this.mBytes[i] & 0x01) == 1 ? 0x80 : 0x00);

                this.mBytes[i] = (byte)(this.mBytes[i] >> 1);
                this.mBytes[i + 1] |= carryByte;
            }
        }

        private byte[] mBytes;

        internal byte this[int index]
        {
            get
            {
                if (index < 0 || index >= this.mBytes.Length)
                {
                    throw new ArgumentOutOfRangeException("index", index, Resources.HashKey_outOfBoundsError);
                }
                return this.mBytes[index];
            }

            set
            {
                if (index < 0 || index >= this.mBytes.Length)
                {
                    throw new ArgumentOutOfRangeException("index", index, Resources.HashKey_outOfBoundsError);
                }
                this.mBytes[index] = value;
            }
        }

        internal uint Abreviated
        {
            get
            {
                byte[] valueAsBytes = new byte[HashKey.Length];

                this.mBytes.CopyTo(valueAsBytes, 0);

                if (BitConverter.IsLittleEndian)
                {
                    Array.Reverse(valueAsBytes);
                }

                return BitConverter.ToUInt32(valueAsBytes, 16);
            }
        }

        public HashKey()
        {
            this.mBytes = new byte[HashKey.sLength];
            Debug.Assert(this.mBytes.Length == HashKey.sLength, "New HashKey has incorrect length.");
        }

        internal HashKey(byte[] bytes)
        {
            if (null == bytes)
            {
                throw new ArgumentNullException("bytes");
            }

            if (bytes.Length != HashKey.sLength)
            {
                throw new ArgumentException(Resources.HashKey_lengthError, "bytes");
            }

            this.mBytes = new byte[HashKey.Length];
            bytes.CopyTo(this.mBytes, 0);
            this.CheckOverflow();
        }

        internal HashKey(ulong value)
        {
            byte[] valueAsBytes = BitConverter.GetBytes(value);

            if (BitConverter.IsLittleEndian)
            {
                Array.Reverse(valueAsBytes);
            }
            this.mBytes = new byte[HashKey.Length];
            valueAsBytes.CopyTo(this.mBytes, HashKey.Length - valueAsBytes.Length);
            this.CheckOverflow();
        }

        public HashKey Copy()
        {
            HashKey result = new HashKey();

            this.mBytes.CopyTo(result.mBytes, 0);

            return result;
        }

        internal static HashKey Hash(PeerAddress address)
        {
            if (address.HashAddress != null)
            {
                return new HashKey(address.HashAddress.mBytes);
            }

            return HashKey.Hash(address.ToString());
        }

        public static HashKey Hash(String key)
        {
            return HashKey.Hash(Encoding.UTF8.GetBytes(key));
        }

        internal static HashKey Hash(IParseable key)
        {
            using (MessageBuffer buffer = new MessageBuffer())
            {
                buffer.Write(key);
                return HashKey.Hash(buffer.GetBuffer());
            }
        }

        public static HashKey Hash(byte[] key)
        {
            using (SHA1 sha = new SHA1Managed())
            {
                HashKey result = new HashKey();

                sha.Initialize();
                sha.ComputeHash(key).CopyTo(result.mBytes, 0);
                result.CheckOverflow();
                Debug.Assert(result.mBytes.Length == HashKey.sLength, "New HashKey has incorrect length.");

                return result;
            }
        }

        internal static HashKey Random()
        {
            HashKey key = new HashKey();

            RandomSource.Generator.NextBytes(key.mBytes);
            key.CheckOverflow();

            return key;
        }

        // Ensure that the value HashKey.Max is not used as it is logically the same value as HashKey.Min.
        private void CheckOverflow()
        {
            if (this.Equals(HashKey.Max))
            {
                for (int i = 0; i < HashKey.Length; i++)
                {
                    this.mBytes[i] = 0x00;
                }
            }

            Debug.Assert(!this.Equals(HashKey.Max), "Max HashKey after overflow adjustment.");
        }

        internal HashKey Opposite()
        {
            HashKey result = new HashKey(this.mBytes);

            // Flip the first bit.
            result.mBytes[0] = (byte)(result[0] ^ 0x80);

            return result;
        }


        internal bool Precedes(HashKey other)
        {
            if (this <= HashKey.Mid)
            {
                return other > this && other <= this.Opposite();
            }

            return other > this || other <= this.Opposite();
        }

        internal bool Succeeds(HashKey other)
        {
            if (this <= HashKey.Mid)
            {
                return other < this || this.Opposite() <= other;
            }

            return other < this && other >= this.Opposite();
        }

        internal HashKey DistanceFrom(HashKey other)
        {
            if (null == other)
            {
                return null;
            }

            HashKey result = new HashKey();
            int carryIn = 0;

            for (int i = HashKey.Length - 1; i >= 0; i--)
            {
                int diff, totalSub;
                totalSub = (int)other[i] + carryIn;

                if (this.mBytes[i] < totalSub)
                {
                    diff = (this.mBytes[i] + 0x100) - totalSub;
                    carryIn = 1;
                }
                else
                {
                    diff = this.mBytes[i] - totalSub;
                    carryIn = 0;
                }

                result[i] = (byte)diff;
            }

            return result;
        }

        // Is key A closer to this than key B?
        internal bool IsCloserTo(HashKey a, HashKey b)
        {
            return this.AbsoluteDistanceFrom(a) < this.AbsoluteDistanceFrom(b);
        }

        public HashKey AbsoluteDistanceFrom(HashKey other)
        {
            if (null == (object)other)
            {
                return null;
            }

            if (other > this)
            {
                return other.AbsoluteDistanceFrom(this);
            }

            HashKey result = this.DistanceFrom(other);

            if (result > HashKey.Mid)
            {
                return other.DistanceFrom(this);
            }

            return result;
        }

        internal int FirstDifferentDigit(HashKey other, int bitsPerDigit, out byte remainder)
        {
            if (null == (object)other)
            {
                throw new ArgumentNullException("other");
            }

            Debug.Assert(bitsPerDigit <= 8, "Bits per digit > 8");
            Debug.Assert(8 % bitsPerDigit == 0, "Bits per digit not modulo 8");

            for (int i = 0; i < HashKey.Length; i++)
            {
                if (this.mBytes[i] != other[i])
                {
                    int shifts = 1;
                    while ((byte)(this.mBytes[i] >> (shifts * bitsPerDigit)) != (byte)(other[i] >> (shifts * bitsPerDigit)))
                    {
                        shifts++;
                    }

                    byte a = (byte)(this.mBytes[i] >> ((shifts - 1) * bitsPerDigit));
                    byte b = (byte)(other[i] >> ((shifts - 1) * bitsPerDigit));

                    //  remainder = (byte)(this.mBytes[i] >> ((shifts - 1) * bitsPerDigit) ^ other[i] >> ((shifts - 1) * bitsPerDigit));

                    if (a - b < 0)
                    {
                        remainder = (byte)(Math.Pow(2, bitsPerDigit) + (a - b));
                    }
                    else
                    {
                        remainder = (byte)(a - b);
                    }

                    Debug.Assert(remainder < Math.Pow(2, bitsPerDigit), "The remainder is larger than the maximum");

                    return ((i + 1) * (8 / bitsPerDigit)) - shifts;
                }
            }


            remainder = 0;
            return -1;
        }

        public void ToMessage(MessageBuffer message, Type parameterType)
        {
            if (null != message)
            {
                message.Write(this.mBytes, HashKey.sLength);
            }
        }

        public void FromMessage(MessageBuffer message)
        {
            if (null != message)
            {
                message.ReadBytes(this.mBytes, HashKey.sLength);
            }
        }

        public override string ToString()
        {
            int length = this.mBytes.Length * 2;
            StringBuilder builder = new StringBuilder(length);

            foreach (byte b in this.mBytes)
            {
                builder.Append(b.ToString("X2"));
            }

            return builder.ToString();
        }

        internal byte[] ToBytes()
        {
            byte[] bytes = new byte[this.mBytes.Length];
            Array.Copy(this.mBytes, bytes, this.mBytes.Length);
            return bytes;
        }

        public int CompareTo(object obj)
        {
            if (null == obj)
            {
                throw new ArgumentNullException("obj");
            }

            HashKey asKey = obj as HashKey;
            if (null != asKey)
            {
                return this.CompareTo(asKey);
            }

            return this.GetHashCode().CompareTo(obj.GetHashCode());
        }

        public int CompareTo(HashKey other)
        {
            if (null == (object)other)
            {
                throw new ArgumentNullException("other");
            }

            int i = 0;
            while (i < HashKey.Length)
            {
                if (this.mBytes[i] != other.mBytes[i])
                {
                    return this.mBytes[i].CompareTo(other.mBytes[i]);
                }
                i++;
            }

            return 0;//  this.mBytes.Length.CompareTo(other.mBytes.Length);
        }

        public override bool Equals(Object obj)
        {
            HashKey asKey = obj as HashKey;
            if (null != (object)asKey)
            {
                for (int i = 0; i < HashKey.Length; i++)
                {
                    if (asKey.mBytes[i] != this.mBytes[i])
                    {
                        return false;
                    }
                }

                return true;
            }

            return false;
        }

        public override int GetHashCode()
        {
            int hashCode = 0;

            hashCode = BitConverter.ToInt32(this.mBytes, 0);
            for (int i = 4; i < HashKey.Length; i += 4)
            {
                hashCode = hashCode ^ BitConverter.ToInt32(this.mBytes, i);
            }

            return hashCode;
        }

        /// <summary>
        /// Adjusts to key for a given Dht redundancy replica.
        /// </summary>
        /// <param name="totalReplicas">The total number of replicas.</param>
        /// <param name="replicaId">The replica id. The original object's replica id is zero and 1 for the first replica.</param>
        public void AdjustToReplicaKey(int totalReplicas, int replicaId)
        {
            if (totalReplicas <= 0 || replicaId >= totalReplicas)
            {
                throw new ArgumentException("AdjustToReplicaKey");
            }

            int replicaKeyDistance = 256 / totalReplicas;
            byte diff = (byte)((replicaKeyDistance * replicaId) & 0xFF);

            this.mBytes[0] += diff;
        }
    }
}
