using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;

using Badumna.Core;
using Badumna.Utilities;
using Badumna;
using Badumna.DataTypes;
using Badumna.Utilities.Logging;

namespace Badumna.DistributedHashTable
{
    class ReplicaManager : DhtProtocol
    {
        // TODO:  After changing ReplicaManager<T> to ReplicaManager so that there is only
        //        one set of ReplicaManager protocol methods reigstered, GetType() is used
        //        in a few places to check the type of the replica.  This works fine
        //        as the replicated object must be of the type registered (i.e. not
        //        a sub-class) because that's the type that will be instantiated on a remote peer.
        //        However, it might be nicer to use the mapped replica type ids directly in the replica
        //        manager rather than looking up the types all the time.

        public delegate void ReplicaArrivalNotification(IReplicatedObject replica, bool isOriginal);

        private Dictionary<Type, ReplicaTypeInfo> mReplicaTypes = new Dictionary<Type, ReplicaTypeInfo>();

        private DhtFacade mDhtFacade;

        private RegularTask mMaintainceTask;

        private const int mNumberOfReplicas = 4;

        // TODO : This should be a function of the reliablity of the object and the average lifespan of peers. 
        // private const int ReplicationIntervalMilliseconds = 15000;
        private const int GarbageCollectionIntervalMilliseconds = 30000;
        internal static int GarbageCollectionIntervalSeconds
        {
            get { return (int)TimeSpan.FromMilliseconds(ReplicaManager.GarbageCollectionIntervalMilliseconds).TotalSeconds; }
        }

        /// <summary>
        /// The queue to use for scheduling events.
        /// </summary>
        private NetworkEventQueue eventQueue;

        /// <summary>
        /// Provides access to the current time.
        /// </summary>
        private ITime timeKeeper;

        /// <summary>
        /// Provides the public peer address.
        /// </summary>
        private INetworkAddressProvider addressProvider;

        /// <summary>
        /// Reports on network connectivity status.
        /// </summary>
        private INetworkConnectivityReporter connectivityReporter;

        public ReplicaManager(
            DhtFacade parent,
            NetworkEventQueue eventQueue,
            ITime timeKeeper,
            INetworkAddressProvider addressProvider,
            INetworkConnectivityReporter connectivityReporter)
            : base(parent)
        {
            this.mDhtFacade = parent;
            this.eventQueue = eventQueue;
            this.timeKeeper = timeKeeper;
            this.addressProvider = addressProvider;
            this.connectivityReporter = connectivityReporter;

            this.connectivityReporter.NetworkOfflineEvent += this.Shutdown;

            this.mMaintainceTask = new RegularTask(
                "Replica_Manager_maintaince_task",
                TimeSpan.FromSeconds(ReplicaManager.GarbageCollectionIntervalSeconds),
                eventQueue,
                connectivityReporter,
                this.PeriodicGarbageCollection);
            this.mMaintainceTask.Start();
        }

        public void AddHandlers(Type type, EventHandler<ExpireEventArgs> expireHandler, ReplicaArrivalNotification arrivalHandler)
        {
            ReplicaTypeInfo replicaTypeInfo = this.GetOrCreateReplicaTypeInfo(type);
            replicaTypeInfo.ExpirationHandler += expireHandler;
            replicaTypeInfo.ArrivalHandler += arrivalHandler;
        }

        public void Shutdown()
        {
            Logger.TraceInformation(LogTag.DHT, "Shutting down replica manager");

            if (this.mMaintainceTask.IsRunning)
            {
                this.mMaintainceTask.Stop();
            }

            foreach (KeyValuePair<Type, ReplicaTypeInfo> pair in this.mReplicaTypes)
            {
                pair.Value.Clear();
            }
        }

        public void Replicate(IReplicatedObject replica)
        {
            Debug.Assert(replica.Key != null, "Replica should always have a hash key.");

            CyclicalID.UShortID nextModificationNumber = replica.ModificationNumber;
            nextModificationNumber.Increment();
            replica.ModificationNumber = nextModificationNumber;

            replica.ExpirationTime = this.timeKeeper.Now + TimeSpan.FromSeconds(replica.TimeToLiveSeconds);

            if (this.mDhtFacade.MapsToLocalPeer(replica.Key, null))
            {
                byte propagationDecay = (byte)((replica.NumberOfReplicas - 1) / 2);
                Logger.TraceInformation(LogTag.Event | LogTag.DHT, "Replicate: StoreAndPropagate({0}[{1}], {2})", replica, replica.ModificationNumber, propagationDecay);
                this.StoreAndPropogateReplica(replica, propagationDecay, null);
            }
            else
            {
                DhtEnvelope envelope = this.GetMessageFor(replica.Key, QualityOfService.Reliable);
                envelope.Qos.Priority = QosPriority.High;

                Logger.TraceInformation(LogTag.Event | LogTag.DHT, "Replicate: StoreOriginal({0}[{1}], {2})", replica, replica.ModificationNumber, replica.NumberOfReplicas);
                this.RemoteCall(envelope, this.StoreOriginal, new ReplicatedObjectPackage(replica, this.timeKeeper), (byte)replica.NumberOfReplicas);
                this.SendMessage(envelope);
            }
        }

        public void RemoveReplica(IReplicatedObject replica)
        {
            replica.TimeToLiveSeconds = 0;
            this.Replicate(replica);

            ReplicaTypeInfo replicaTypeInfo = this.GetOrCreateReplicaTypeInfo(replica.GetType());
            replicaTypeInfo.RemoveReplica(replica.Key, replica.Guid);
        }

        private void PeriodicGarbageCollection()
        {
            if (this.connectivityReporter.Status != ConnectivityStatus.Online)
            {
                return;
            }

            foreach (KeyValuePair<Type, ReplicaTypeInfo> pair in this.mReplicaTypes)
            {
                pair.Value.CollectGarbage();
            }
        }

        public void NeighbourArrivalNotification(NodeInfo node, ICollection<PeerAddress> excludedAddresses)
        {
            foreach (KeyValuePair<Type, ReplicaTypeInfo> replicaType in this.mReplicaTypes)
            {
                replicaType.Value.NeighbourArrivalNotification(node, excludedAddresses);
            }
        }

        public void SendCheckReplicaStatus(IReplicatedObject replica)
        {
            DhtEnvelope envelope = this.GetMessageFor(replica.Key, QualityOfService.Reliable);
            envelope.Qos.Priority = QosPriority.Low;

            this.RemoteCall(envelope, this.CheckReplicaStatus, replica.Guid, ReplicatedTypeMap.Current.FromType(replica.GetType()));
            this.SendMessage(envelope);
        }

        [DhtProtocolMethod(DhtMethod.CheckReplicaStatus)]
        private void CheckReplicaStatus(BadumnaId replicaId, byte replicaType)
        {
            Debug.Assert(this.CurrentEnvelope.DestinationKey != null, "No replica key specified");

            HashKey replicaKey = this.CurrentEnvelope.DestinationKey;
            Logger.TraceInformation(LogTag.DHT, "Request to check status of replica {0} at {1}", replicaId, replicaKey);

            ReplicaTypeInfo replicaTypeInfo = this.GetOrCreateReplicaTypeInfo(ReplicatedTypeMap.Current.FromId(replicaType));
            IReplicatedObject replica = replicaTypeInfo.GetReplica(replicaKey, replicaId);

            if (replica != null)
            {
                if (replica.ExpirationTime > this.timeKeeper.Now)
                {
                    // The replica has not expired. Do nothing.
                    return;
                }

                Logger.TraceInformation(LogTag.DHT, "Retiring replicated object {0}", replica.Guid);
                replicaTypeInfo.InvokeExpirationHandler(replica);
                replicaTypeInfo.RemoveReplica(replicaKey, replicaId);
                return;
            }

            if (this.CurrentEnvelope.Source.Equals(this.addressProvider.PublicAddress))
            {
                // The request was routed locally. This means this peer is now (or always was) responsible for
                // the original replica, in which case it should retire the object even if it has been removed.
                Logger.TraceInformation(LogTag.DHT, "Retiring previously removed replicated object {0}", replicaId);
                replicaTypeInfo.InvokeExpirationHandlerForRemovedReplica(replicaId);
            }
        }
        
        [DhtProtocolMethod(DhtMethod.StoreOriginal)]
        private void StoreOriginal(ReplicatedObjectPackage replicatedObjectPackage, byte numberOfReplicas)
        {
            Debug.Assert(this.CurrentEnvelope.DestinationKey != null, "No replica key specified");
            IReplicatedObject replicatedObject = replicatedObjectPackage.ReplicatedObject;

            Logger.TraceInformation(LogTag.DHT, "Store original {0}, {1}", replicatedObject.GetType().Name, this.CurrentEnvelope.DestinationKey);

            replicatedObject.Key = this.CurrentEnvelope.DestinationKey;
            byte propogationDecay = (byte)((numberOfReplicas - 1) / 2);

            this.StoreAndPropogateReplica(replicatedObject, propogationDecay, null);
        }
        
        private void StoreAndPropogateReplica(IReplicatedObject replica, byte propogationDecay, PeerAddress immediateSource)
        {
            ReplicaTypeInfo replicaTypeInfo = this.GetOrCreateReplicaTypeInfo(replica.GetType());

            if (replica.TimeToLiveSeconds <= 0)
            {
                // Remove request do not call expire event
                replicaTypeInfo.RemoveReplica(replica.Key, replica.Guid);
            }
            else
            {
                replicaTypeInfo.Store(replica, immediateSource == null || replica.Key.IsCloserTo(this.mDhtFacade.LocalHashKey, HashKey.Hash(immediateSource))
                    );  // TODO: If we have a newer replica we should propagate that instead of the one we just got
            }

            if (propogationDecay > 0)
            {
                NodeInfo[] replicationCandidates = this.mDhtFacade.GetImmediateNeighbours();

                if (replicationCandidates[0] == null && replicationCandidates[1] == null)
                {
                    Logger.TraceInformation(LogTag.DHT, "Unable to find any neighbours to replicate with.");
                    return;
                }

                // Only send once if both candidates are the same peer.
                if (replicationCandidates[0] != null && replicationCandidates[1] != null
                    && replicationCandidates[0].Address.Equals(replicationCandidates[1].Address))
                {
                    replicationCandidates[1] = null;
                }

                foreach (NodeInfo node in replicationCandidates)
                {
                    if (node != null && (immediateSource == null || !node.Address.Equals(immediateSource)))
                    {
                        this.SendStoreReplica(node.Address, replica, propogationDecay);
                    }
                }
            }
        }

        public void SendStoreReplica(PeerAddress nodeAddress, IReplicatedObject replicatedObject)
        {
            this.SendStoreReplica(nodeAddress, replicatedObject, (byte)1);
        }

        public void SendStoreReplica(PeerAddress nodeAddress, IReplicatedObject replicatedObject, byte propogationDelay)
        {
            DhtEnvelope envelope = this.GetMessageFor(nodeAddress, new QualityOfService(true));
            envelope.Qos.Priority = QosPriority.Medium;

            this.RemoteCall(envelope, this.StoreReplica, new ReplicatedObjectPackage(replicatedObject, this.timeKeeper), replicatedObject.Key, propogationDelay);
            this.SendMessage(envelope);
        }

        [DhtProtocolMethod(DhtMethod.StoreReplica)]
        private void StoreReplica(ReplicatedObjectPackage replicatedObjectPackage, HashKey key, byte propogationDecay)
        {
            IReplicatedObject replicatedObject = replicatedObjectPackage.ReplicatedObject;

            replicatedObject.Key = key;
            propogationDecay--;
            this.StoreAndPropogateReplica(replicatedObject, propogationDecay, this.CurrentEnvelope.Source);
        }

        public ICollection<T> AccessReplicas<T>(HashKey key) where T : IReplicatedObject
        {
            ReplicaTypeInfo replicaTypeInfo = this.GetOrCreateReplicaTypeInfo(typeof(T));

            return replicaTypeInfo.AccessReplicas<T>(key);
        }

        public T AccessReplica<T>(HashKey key, BadumnaId id) where T : IReplicatedObject
        {
            ReplicaTypeInfo replicaTypeInfo = this.GetOrCreateReplicaTypeInfo(typeof(T));
            return (T)replicaTypeInfo.AccessReplica(key, id);
        }

        private ReplicaTypeInfo GetOrCreateReplicaTypeInfo(Type type)
        {
            ReplicaTypeInfo result;
            if (!this.mReplicaTypes.TryGetValue(type, out result))
            {
                result = new ReplicaTypeInfo(this, this.mDhtFacade, this.eventQueue, this.timeKeeper);
                this.mReplicaTypes[type] = result;
            }
            return result;
        }
    }
}
