﻿//------------------------------------------------------------------------------
// <copyright file="ReplicatedTypeMap.cs" company="National ICT Australia Limited">
//     Copyright (c) 2012 National ICT Australia Limited. All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;

namespace Badumna.DistributedHashTable
{
    /// <summary>
    /// Maps types replicated on the DHT to ids, and vice-versa.
    /// </summary>
    /// <remarks>
    /// This class can be a singleton because all peers in the system must have exactly the same
    /// type map, and once created it remains constant.
    /// </remarks>
    internal class ReplicatedTypeMap
    {
        /// <summary>
        /// Maps ids to types.
        /// </summary>
        private List<Type> idToType = new List<Type>();

        /// <summary>
        /// Maps types to ids.
        /// </summary>
        private Dictionary<Type, byte> typeToId = new Dictionary<Type, byte>();

        /// <summary>
        /// Initializes static members of the ReplicatedTypeMap class, including constructing
        /// the singleton instance.
        /// </summary>
        static ReplicatedTypeMap()
        {
            ReplicatedTypeMap.Current = new ReplicatedTypeMap();
        }

        /// <summary>
        /// Prevents a default instance of the ReplicatedTypeMap class from being created,
        /// because ReplicatedTypeMap is a singleton.
        /// </summary>
        private ReplicatedTypeMap()
        {
        }

        /// <summary>
        /// Gets the singleton instance of the type map.
        /// </summary>
        public static ReplicatedTypeMap Current { get; private set; }

        /// <summary>
        /// Registers a type for replication.
        /// </summary>
        /// <remarks>
        /// <para>
        /// WARNING! Care must be taken to ensure that the same sequence of calls are made to this method on
        /// all peers in the system.  This is because type ids are assigned incrementally as this method
        /// is called, and because type ids on different peers must match.
        /// </para>
        /// <para>
        /// If the type has already been registered this method will have no effect.
        /// </para>
        /// </remarks>
        /// <param name="type">The type to register</param>
        /// <returns>The id allocated for the type</returns>
        public byte Register(Type type)
        {
            if (this.idToType.Count == byte.MaxValue)
            {
                throw new InvalidOperationException("Too many replicated types");
            }

            if (this.typeToId.ContainsKey(type))
            {
                return this.typeToId[type];
            }

            byte id = (byte)this.idToType.Count;
            this.idToType.Add(type);
            this.typeToId[type] = id;
            return id;
        }

        /// <summary>
        /// Returns the type corresponding to the given id.
        /// </summary>
        /// <param name="id">The id of the type</param>
        /// <returns>The corresponding type</returns>
        public Type FromId(byte id)
        {
            return this.idToType[id];
        }

        /// <summary>
        /// Returns the id corresponding to the given type.
        /// </summary>
        /// <param name="type">The type to find the id of</param>
        /// <returns>The corresponding id</returns>
        public byte FromType(Type type)
        {
            return this.typeToId[type];
        }
    }
}
