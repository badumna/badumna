//-----------------------------------------------------------------------
// <copyright file="IDhtProtocol.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2011 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.DistributedHashTable
{
    using Badumna.Core;

    /// <summary>
    /// Interface for DHT protocol.
    /// </summary>
    internal interface IDhtProtocol : IProtocolComponent<DhtEnvelope>
    {
        /// <summary>
        /// Gets a DHT envelope.
        /// </summary>
        /// <param name="address">The address for the envelope</param>
        /// <param name="qos">The quality of service for the envelope.</param>
        /// <returns>The DHT envelope.</returns>
        DhtEnvelope GetMessageFor(PeerAddress address, QualityOfService qos);

        /// <summary>
        /// Gets a DHT envelope.
        /// </summary>
        /// <param name="key">The hash key for the envelope.</param>
        /// <param name="qos">The quality of service for the envelope.</param>
        /// <returns>The DHT envelope.</returns>
        DhtEnvelope GetMessageFor(HashKey key, QualityOfService qos);

        /// <summary>
        /// Gets a DHT envelope.
        /// </summary>
        /// <param name="key">The hash key for the envelope.</param>
        /// <param name="qos">The quality of service for the envelope.</param>
        /// <param name="redundancyFactor">The redundancy factor for the quality of service.</param>
        /// <returns>The DHT envelope.</returns>
        DhtEnvelope GetMessageFor(HashKey key, QualityOfService qos, int redundancyFactor);
    }
}
