using System;
using System.Collections.Generic;
using System.Text;

using Badumna.Core;
using Badumna.Utilities;

namespace Badumna.DistributedHashTable
{
    class LeafSetChangeEventArgs : EventArgs
    {
        private NodeInfo mNodeInfo;
        public NodeInfo NodeInfo { get { return this.mNodeInfo; } }

        public LeafSetChangeEventArgs(NodeInfo nodeInfo)
        {
            this.mNodeInfo = nodeInfo;
        }
    }

    class LeafSet : IParseable, IEnumerable<PeerAddress>
    {
        public const int MaximumNumberOfNeighbours = 4;                        // Must be even and < 256
        private const int MaximumNumberOfNodes = LeafSet.MaximumNumberOfNeighbours / 2;

        private EventHandler<LeafSetChangeEventArgs> neighbourAdditionEventDelegate;
        public event EventHandler<LeafSetChangeEventArgs> NeighbourAdditionEvent
        {
            add { this.neighbourAdditionEventDelegate += value; }
            remove { this.neighbourAdditionEventDelegate -= value; }
        }

        private EventHandler<LeafSetChangeEventArgs> neighbourRemovalEventDelegate;
        public event EventHandler<LeafSetChangeEventArgs> NeighbourRemovalEvent
        {
            add { this.neighbourRemovalEventDelegate += value; }
            remove { this.neighbourRemovalEventDelegate -= value; }
        }

        private List<NodeInfo> mPredecessors = new List<NodeInfo>();
        private List<NodeInfo> mSuccessors = new List<NodeInfo>();

        public int PredecessorCount { get { return this.mPredecessors.Count; } }
        public int SuccessorCount { get { return this.mSuccessors.Count; } }

        private int mUniqueNodeCount;
        public int UniqueNodeCount { get { return this.mUniqueNodeCount; } }

        private NodeInfo mLocalNodeInfo;
        public NodeInfo LocalNodeInfo { get { return this.mLocalNodeInfo; } }

        public bool IsInitialized { get { return null != this.mLocalNodeInfo; } }

        public NodeInfo this[int index]
        {
            get
            {
                if (index < 0)
                {
                    if (Math.Abs(index) > this.mPredecessors.Count)
                    {
                        throw new ArgumentOutOfRangeException("index", index, "Index out of bound. Expected >= " + (-this.mPredecessors.Count));
                    }
                    return this.mPredecessors[Math.Abs(index) - 1];
                }
                if (index > 0)
                {
                    if (index > this.mSuccessors.Count)
                    {
                        throw new ArgumentOutOfRangeException("index", index, "Index out of bound. Expected <= " + this.mSuccessors.Count);
                    }
                    return this.mSuccessors[index - 1];
                }

                return this.mLocalNodeInfo;
            }
        }


        public LeafSet()
        {
        }

        public void Initialize(PeerAddress localAddress)
        {
            if (null == localAddress)
            {
                throw new ArgumentNullException("localAddress");
            }

            this.mLocalNodeInfo = new NodeInfo(localAddress, HashKey.Hash(localAddress));
        }

        public bool Contains(PeerAddress nodeAddress)
        {
            if (null == this.mLocalNodeInfo)
            {
                throw new InvalidOperationException("Must initialize LeafSet prior to usage.");
            }

            return this.Contains(new NodeInfo(nodeAddress, this.mLocalNodeInfo.Key));
        }

        public bool Contains(NodeInfo nodeInfo)
        {
            return this.LocalNodeInfo.Address.Equals(nodeInfo.Address) ||
                this.mPredecessors.Contains(nodeInfo) || this.mSuccessors.Contains(nodeInfo);
        }

        private bool IsBetween(HashKey lesserKey, HashKey greaterKey, HashKey key)
        {
            if (lesserKey.Succeeds(greaterKey))
            {
                return lesserKey.Precedes(key) || greaterKey.Succeeds(key);
            }

            return lesserKey.Precedes(key) && greaterKey.Succeeds(key);
        }

        public bool Encompases(HashKey key)
        {
            if (this.UniqueNodeCount < LeafSet.MaximumNumberOfNeighbours ||
                this[-this.PredecessorCount].Key.Equals(key) ||
                this[this.PredecessorCount].Key.Equals(key))
            {
                return true;
            }

            return this.IsBetween(this[-this.PredecessorCount].Key, this[this.SuccessorCount].Key, key);
        }

        public NodeInfo ImmediateSuccessor
        {
            get
            {
                if (0 == this.mSuccessors.Count)
                {
                    return null;
                }

                return this.mSuccessors[0];
            }
        }

        public NodeInfo ImmediatePredecessor
        {
            get
            {
                if (0 == this.mPredecessors.Count)
                {
                    return null;
                }

                return this.mPredecessors[0];
            }
        }

        public NodeInfo[] GetLeafSetNodes()
        {
            Dictionary<string, NodeInfo> nodes = new Dictionary<string, NodeInfo>();

            foreach (NodeInfo node in this.mSuccessors)
            {
                if (null != node && !nodes.ContainsKey(node.Address.ToString()))
                {
                    nodes[node.Address.ToString()] = node;
                }
            }

            foreach (NodeInfo node in this.mPredecessors)
            {
                if (null != node && !nodes.ContainsKey(node.Address.ToString()))
                {
                    nodes[node.Address.ToString()] = node;
                }
            }

            return (new List<NodeInfo>(nodes.Values).ToArray());
        }

        // TODO : Need to ensure that a key equidistant from two neighbouring nodes will always return a consistent ordering,
        // that is, the smallest (or highest) node key is prefered.
        // Does checking the local node in order as opposed to first fix this ?? NO - there are other places that use the same logic - needs to be done in comparison method
        public NodeInfo GetNodeClosestTo(HashKey key, ICollection<PeerAddress> excludedAddresses)
        {
            NodeInfo closest = this.mLocalNodeInfo;
            HashKey distance = key.AbsoluteDistanceFrom(this.mLocalNodeInfo.Key);

            if (null != excludedAddresses && excludedAddresses.Contains(closest.Address))
            {
                closest = null;
                distance = HashKey.Mid;
            }

            foreach (NodeInfo nodeInfo in this.mPredecessors)
            {
                HashKey nodeKeyDistance = nodeInfo.Key.AbsoluteDistanceFrom(key);
                if (nodeKeyDistance < distance &&
                    (null == excludedAddresses || !excludedAddresses.Contains(nodeInfo.Address)))
                {
                    closest = nodeInfo;
                    distance = nodeKeyDistance;
                }
            }
            foreach (NodeInfo nodeInfo in this.mSuccessors)
            {
                HashKey nodeKeyDistance = nodeInfo.Key.AbsoluteDistanceFrom(key);
                if (nodeKeyDistance < distance &&
                    (null == excludedAddresses || !excludedAddresses.Contains(nodeInfo.Address)))
                {
                    closest = nodeInfo;
                    distance = nodeKeyDistance;
                }
            }

            System.Diagnostics.Debug.Assert(null == closest || null == excludedAddresses || !excludedAddresses.Contains(closest.Address),
                "Closest node is in the excluded list.");
            return closest;
        }

        /*
        public IList<NodeInfo> GetReplicatingNodesList(HashKey key, int maximumHopDistance)
        {
            List<NodeInfo> returnList = new List<NodeInfo>();
            NodeInfo closestNode = this.GetNodeClosestTo(key, null);

            if (null == closestNode)
            {
                return returnList;
            }

            int min = 0;
            int max = 0;
            for (int i = -this.PredecessorCount; i <= this.SuccessorCount; i++)
            {
                if (closestNode.Equals(this[i]))
                {
                    min = i - maximumHopDistance;
                    max = i + maximumHopDistance;
                    break;
                }
            }

            min = Math.Max(min, -this.PredecessorCount);
            max = Math.Min(max, this.SuccessorCount);

            for (int i = min; i <= max; i++)
            {
                if (i != 0 && !returnList.Contains(this[i]))
                {
                    returnList.Add(this[i]);
                }
            }

            return returnList;
        }
        */

        // Removes the given address and invokes the NeighbourRemovalEvent if it was present
        public void RemoveNeighbour(PeerAddress address)
        {
            NodeInfo nodeInfo = new NodeInfo(address, this.mLocalNodeInfo.Key);

            // Call the remove event if it was removed.
            if (this.RemoveNode(nodeInfo))
            {
                Logger.TraceInformation(LogTag.DHT, "Removed neighbour {0} from leaf set.", nodeInfo.Address);

                EventHandler<LeafSetChangeEventArgs> handler = this.neighbourRemovalEventDelegate;
                if (handler != null)
                {
                    handler.Invoke(this, new LeafSetChangeEventArgs(nodeInfo));
                }
            }
        }

        // Removes a node without calling the NeighbourRemovalEvent
        public bool RemoveNode(NodeInfo nodeInfo)
        {
            bool removed = false;

            if (this.mPredecessors.Contains(nodeInfo))
            {
                removed = true;
                this.mPredecessors.Remove(nodeInfo);
            }

            if (this.mSuccessors.Contains(nodeInfo))
            {
                removed = true;
                this.mSuccessors.Remove(nodeInfo);
            }

            // Attempt to rebalance the set.
            if (this.PredecessorCount < LeafSet.MaximumNumberOfNodes)
            {
                for (int i = this.mSuccessors.Count - 1; i >= 0; i--)
                {
                    if (!this.mPredecessors.Contains(this.mSuccessors[i]))
                    {
                        this.AddNodeInfo(this.mSuccessors[i], false, ref this.mPredecessors, ref this.mSuccessors);
                    }
                }
            }

            if (this.SuccessorCount < LeafSet.MaximumNumberOfNodes)
            {
                for (int i = this.mPredecessors.Count - 1; i >= 0; i--)
                {
                    if (!this.mSuccessors.Contains(this.mPredecessors[i]))
                    {
                        this.AddNodeInfo(this.mPredecessors[i], true, ref this.mSuccessors, ref this.mPredecessors);
                    }
                }
            }

            if (removed)
            {
                this.mUniqueNodeCount--;
            }

            return removed;
        }

        public bool IsCandidateForAddition(PeerAddress address)
        {
            if (address.Equals(this.mLocalNodeInfo.Address))
            {
                return false;
            }

            if (this.PredecessorCount + this.SuccessorCount < LeafSet.MaximumNumberOfNeighbours)
            {
                return !this.Contains(address);
            }

            NodeInfo candidateInfo = new NodeInfo(address, this.mLocalNodeInfo.Key);

            foreach (NodeInfo info in this.mPredecessors)
            {
                if (candidateInfo.Address.Equals(info.Address))
                {
                    return false;
                }

                if (candidateInfo.Distance(false) < info.Distance(false))
                {
                    return true;
                }
            }

            foreach (NodeInfo info in this.mSuccessors)
            {
                if (candidateInfo.Address.Equals(info.Address))
                {
                    return false;
                }

                if (candidateInfo.Distance(true) < info.Distance(true))
                {
                    return true;
                }
            }

            return false;
        }

        public void AddNeighbour(PeerAddress address)
        {
            if (null == this.mLocalNodeInfo)
            {
                throw new InvalidOperationException("Must initialize LeafSet prior to usage.");
            }

            if (this.mLocalNodeInfo.Equals(address))
            {
                return;
            }

            if (this.Contains(address))
            {
                return;
            }

            NodeInfo nodeInfo = new NodeInfo(address, this.mLocalNodeInfo.Key);

            // Attempt to add the node to both the predecessor and successor lists.
            bool addedAsSuccessor = this.AddNodeInfo(nodeInfo, true, ref this.mSuccessors, ref this.mPredecessors);
            bool addedAsPredecessor = this.AddNodeInfo(nodeInfo, false, ref this.mPredecessors, ref this.mSuccessors);

            if (addedAsPredecessor || addedAsSuccessor)
            {
                // Call the add node event.
                EventHandler<LeafSetChangeEventArgs> handler = this.neighbourAdditionEventDelegate;
                if (handler != null)
                {
                    handler.Invoke(this, new LeafSetChangeEventArgs(nodeInfo));
                }

                this.mUniqueNodeCount++;
            }
        }

        private bool AddNodeInfo(NodeInfo nodeInfo, bool direction, ref List<NodeInfo> primaryList, ref List<NodeInfo> secondaryList)
        {

            if (primaryList.Count >= LeafSet.MaximumNumberOfNodes)
            {
                // If the new node is closer than the furthest node, then remove it.
                if (nodeInfo.Distance(direction) < primaryList[primaryList.Count - 1].Distance(direction))
                {
                    // Try to add the removed node to the secondary list.
                    // This is done in the case that there are fewer than max nodes in the secondary list or
                    // the displaced node is closer in than others in the secondaryList
                    if (!secondaryList.Contains(primaryList[primaryList.Count - 1]) &&
                        !this.AddNodeInfo(primaryList[primaryList.Count - 1], !direction, ref secondaryList, ref primaryList))
                    {
                        // Call the remove event if it was not added.
                        EventHandler<LeafSetChangeEventArgs> handler = this.neighbourRemovalEventDelegate;
                        if (handler != null)
                        {
                            handler.Invoke(this, new LeafSetChangeEventArgs(primaryList[primaryList.Count - 1]));
                        }

                        this.mUniqueNodeCount--;
                    }

                    // Remove the furthest node.
                    primaryList.RemoveAt(primaryList.Count - 1);
                }
                else
                {
                    return false;
                }
            }

            // Find the position for the new node info.
            int i = 0;
            while (i < primaryList.Count && primaryList[i].Distance(direction) < nodeInfo.Distance(direction))
            {
                i++;
            }

            primaryList.Insert(i, nodeInfo);
            return true;
        }


        public void ToMessage(MessageBuffer message, Type parameterType)
        {
            if (this.mLocalNodeInfo == null)
            {
                this.Initialize(PeerAddress.Nowhere);
            }

            message.Write(this.mLocalNodeInfo.Address);
            message.Write((byte)(this.PredecessorCount + this.SuccessorCount));

            foreach (NodeInfo node in this.mPredecessors)
            {
                message.Write(node.Address);
            }
            foreach (NodeInfo node in this.mSuccessors)
            {
                message.Write(node.Address);
            }
        }

        public void FromMessage(MessageBuffer message)
        {
            PeerAddress localAddress = new PeerAddress();

            localAddress.FromMessage(message);
            this.Initialize(localAddress);

            int length = (int)message.ReadByte();
            for (int i = 0; i < length; i++)
            {
                NodeInfo nodeInfo = new NodeInfo(message.Read<PeerAddress>(), this.mLocalNodeInfo.Key);

                if (!this.Contains(nodeInfo))
                {
                    this.mUniqueNodeCount++;

                    // Attempt to add the node to both the predecessor and successor lists.
                    this.AddNodeInfo(nodeInfo, true, ref this.mSuccessors, ref this.mPredecessors);
                    this.AddNodeInfo(nodeInfo, false, ref this.mPredecessors, ref this.mSuccessors);
                }
            }
        }


        #region IEnumerable<PeerAddress> Members

        public IEnumerator<PeerAddress> GetEnumerator()
        {
            return new Enumerator(this);
        }

        class Enumerator : IEnumerator<PeerAddress>
        {
            private LeafSet leafSet;
            private int iterator;
            private int current;
            private bool isValid;

            public Enumerator(LeafSet leafSet)
            {
                this.leafSet = leafSet;
                this.iterator = -this.leafSet.PredecessorCount;
                this.isValid = false;
            }

            #region IEnumerator<PeerAddress> Members

            public PeerAddress Current
            {
                get
                {
                    if (this.isValid)
                    {
                        return this.leafSet[this.current].Address;
                    }
                    else
                    {
                        throw new InvalidOperationException();
                    }
                }
            }

            #endregion

            #region IDisposable Members

            public void Dispose()
            {
            }

            #endregion

            #region IEnumerator Members

            object System.Collections.IEnumerator.Current
            {
                get 
                {
                    if (this.isValid)
                    {
                        return this.leafSet[this.current].Address;
                    }
                    else
                    {
                        throw new InvalidOperationException();
                    }
                }
            }

            public bool MoveNext()
            {
                while (this.iterator < this.leafSet.SuccessorCount)
                {
                    this.current = this.iterator;
                    this.iterator++;
                    this.isValid = true;
                    return true;
                }

                this.isValid = false;
                return false;
            }

            public void Reset()
            {
                throw new NotSupportedException();
            }

            #endregion
        }

        #endregion

        #region IEnumerable Members

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        #endregion
    }
}
