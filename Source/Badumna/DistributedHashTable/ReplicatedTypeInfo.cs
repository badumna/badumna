﻿using System;
using System.Collections.Generic;
using System.Text;

using Badumna.Core;
using Badumna.Utilities;
using Badumna;
using Badumna.DataTypes;

namespace Badumna.DistributedHashTable
{
    class ReplicaTypeInfo
    {
        private EventHandler<ExpireEventArgs> expirationHandlerDelegate;
        public event EventHandler<ExpireEventArgs> ExpirationHandler
        {
            add { this.expirationHandlerDelegate += value; }
            remove { this.expirationHandlerDelegate -= value; }
        }

        private ReplicaManager.ReplicaArrivalNotification arrivalHandlerDelegate;
        public event ReplicaManager.ReplicaArrivalNotification ArrivalHandler
        {
            add { this.arrivalHandlerDelegate += value; }
            remove { this.arrivalHandlerDelegate -= value; }
        }

        private Dictionary<HashKey, Dictionary<BadumnaId, IReplicatedObject>> mReplicatedObjectStore
            = new Dictionary<HashKey, Dictionary<BadumnaId, IReplicatedObject>>();

        private Dictionary<BadumnaId, IReplicatedObject> mGarbageReplicas = new Dictionary<BadumnaId, IReplicatedObject>();

        private ReplicaManager mReplicaManager;
        private DhtFacade mDhtFacade;

        /// <summary>
        /// The queue to use for scheduling events.
        /// </summary>
        private NetworkEventQueue eventQueue;

        /// <summary>
        /// Provides access to the current time.
        /// </summary>
        private ITime timeKeeper;

        public ReplicaTypeInfo(ReplicaManager replicaManager, DhtFacade dhtFacade, NetworkEventQueue eventQueue, ITime timeKeeper)
        {
            this.eventQueue = eventQueue;
            this.timeKeeper = timeKeeper;
            this.mReplicaManager = replicaManager;
            this.mDhtFacade = dhtFacade;
        }

        public void InvokeExpirationHandlerForRemovedReplica(BadumnaId removedReplicaId)
        {
            IReplicatedObject replica = null;

            if (this.mGarbageReplicas.TryGetValue(removedReplicaId, out replica))
            {
                EventHandler<ExpireEventArgs> handler = this.expirationHandlerDelegate;
                if (handler != null)
                {
                    handler(null, new ExpireEventArgs(replica));
                }
            }
        }

        public void InvokeExpirationHandler(IReplicatedObject replica)
        {
            EventHandler<ExpireEventArgs> handler = this.expirationHandlerDelegate;
            if (handler != null)
            {
                handler(null, new ExpireEventArgs(replica));
            }
        }

        public void InvokeArrivalHandler(IReplicatedObject replica, bool isOriginal)
        {
            ReplicaManager.ReplicaArrivalNotification handler = this.arrivalHandlerDelegate;
            if (handler != null)
            {
                handler(replica, isOriginal);
            }
        }

        public void Clear()
        {
            // References to these dictionaries may be held by clients (although we don't
            // guarantee that we support such behaviour).
            foreach (KeyValuePair<HashKey, Dictionary<BadumnaId, IReplicatedObject>> pair in this.mReplicatedObjectStore)
            {
                pair.Value.Clear();
            }

            this.mReplicatedObjectStore.Clear();
        }

        public void CollectGarbage()
        {
            this.mGarbageReplicas.Clear(); // Remove any replicas that were deemed garbage by the last call to CollectGarbage

            List<HashKey> unusedKeys = new List<HashKey>();

            foreach (KeyValuePair<HashKey, Dictionary<BadumnaId, IReplicatedObject>> pair in this.mReplicatedObjectStore)
            {
                foreach (KeyValuePair<BadumnaId, IReplicatedObject> replicaPair in pair.Value)
                {
                    if (replicaPair.Value.ExpirationTime <= this.timeKeeper.Now)
                    {
                        Logger.TraceInformation(LogTag.DHT, "Replicated object {0} at {1} has expired", replicaPair.Key, pair.Key);

                        if (!this.mGarbageReplicas.ContainsKey(replicaPair.Key))
                        {
                            this.mGarbageReplicas.Add(replicaPair.Key, replicaPair.Value);
                        }

                        // If a peer has departed it could take time for a keep alive to propogate.
                        // To allow for this we route a message to the key source to check if the object has truly expired.
                        // This also prevents calls to ExpireEvent on multiple peers.
                        this.mReplicaManager.SendCheckReplicaStatus(replicaPair.Value);
                    }
                }

                foreach (BadumnaId replicaGuid in this.mGarbageReplicas.Keys)
                {
                    this.mReplicatedObjectStore[pair.Key].Remove(replicaGuid);
                }

                if (0 == this.mReplicatedObjectStore[pair.Key].Count)
                {
                    unusedKeys.Add(pair.Key);
                }
            }

            foreach (HashKey key in unusedKeys)
            {
                this.mReplicatedObjectStore.Remove(key);
            }
        }

        public void NeighbourArrivalNotification(NodeInfo node, ICollection<PeerAddress> excludedAddresses)
        {
            foreach (KeyValuePair<HashKey, Dictionary<BadumnaId, IReplicatedObject>> pair in this.mReplicatedObjectStore)
            {
                foreach (KeyValuePair<BadumnaId, IReplicatedObject> replicaPair in pair.Value)
                {
                    IReplicatedObject replicatedObject = replicaPair.Value;

                    // Only the controlling peer sends to new peers. This ensures consistencey
                    if (this.mDhtFacade.MapsToLocalPeer(replicatedObject.Key, excludedAddresses))
                    {
                        Logger.TraceInformation(LogTag.DHT, "New neighbour {0} should replicate {1}({2}) @ {3}",
                            node.Address, replicatedObject.Guid, replicatedObject.ModificationNumber.Value, replicatedObject.Key);

                        this.mReplicaManager.SendStoreReplica(node.Address, replicatedObject);
                    }
                }
            }
        }

        public void RemoveReplica(HashKey replicaKey, BadumnaId replicaGuid)
        {
            if (this.mReplicatedObjectStore.ContainsKey(replicaKey))
            {
                if (this.mReplicatedObjectStore[replicaKey].ContainsKey(replicaGuid))
                {
                    this.mReplicatedObjectStore[replicaKey].Remove(replicaGuid);

                    Logger.TraceInformation(LogTag.DHT, "Replicated object {0} has been removed", replicaGuid);
                }

                if (0 == this.mReplicatedObjectStore[replicaKey].Count)
                {
                    this.mReplicatedObjectStore.Remove(replicaKey);
                }
            }
        }

        public IReplicatedObject GetReplica(HashKey replicaKey, BadumnaId replicaGuid)
        {
            Dictionary<BadumnaId, IReplicatedObject> replicaList;
            if (this.mReplicatedObjectStore.TryGetValue(replicaKey, out replicaList))
            {
                IReplicatedObject replica;
                replicaList.TryGetValue(replicaGuid, out replica);
                return replica;
            }
            return null;
        }

        public void Store(IReplicatedObject replica, bool isOriginal)
        {
            IReplicatedObject currentReplica = this.GetReplica(replica.Key, replica.Guid);
            if (currentReplica != null && (!currentReplica.IgnoreModificationNumber && !replica.IgnoreModificationNumber) && 
                currentReplica.ModificationNumber >= replica.ModificationNumber)
            {
                Logger.TraceInformation(LogTag.DHT, "Ignoring request to overwrite {0} replica {1} since it doesn't have a later modification number. ({2} >= {3})",
                    replica.GetType().Name, replica.Guid, currentReplica.ModificationNumber, replica.ModificationNumber);
                return;
            }

            // make sure the ttl value won't be surprisingly large. 
            if (replica.TimeToLiveSeconds < Parameters.MaximumIReplicaObjectTTLSeconds)
            {
                replica.ExpirationTime = this.timeKeeper.Now + TimeSpan.FromSeconds(replica.TimeToLiveSeconds);
            }
            else
            {
                Logger.TraceInformation(LogTag.DHT, "Replica {0} has a very large ttl value {1}, capped.", replica.GetType().Name, replica.TimeToLiveSeconds);
                replica.TimeToLiveSeconds = Parameters.MaximumIReplicaObjectTTLSeconds;
                replica.ExpirationTime = this.timeKeeper.Now + TimeSpan.FromSeconds(Parameters.MaximumIReplicaObjectTTLSeconds);
            }

            if (replica.TimeToLiveSeconds > 0)
            {
                Logger.TraceInformation(LogTag.DHT, "Storing {0} replica with key {1}({3}) until {2}",
                    replica.GetType().Name, replica.Guid, replica.ExpirationTime, replica.ModificationNumber.Value);

                Dictionary<BadumnaId, IReplicatedObject> replicaList;
                if (!this.mReplicatedObjectStore.TryGetValue(replica.Key, out replicaList))
                {
                    replicaList = new Dictionary<BadumnaId, IReplicatedObject>();
                    this.mReplicatedObjectStore[replica.Key] = replicaList;
                }

                this.mReplicatedObjectStore[replica.Key][replica.Guid] = replica;

                this.eventQueue.Schedule(5, this.InvokeArrivalHandler, replica, isOriginal);  // 5 milliseconds possibly for a good reason
            }
        }

        public ICollection<T> AccessReplicas<T>(HashKey hashKey) where T : IReplicatedObject
        {
            Dictionary<BadumnaId, IReplicatedObject> replicaList;
            if (this.mReplicatedObjectStore.TryGetValue(hashKey, out replicaList))
            {
                List<T> replicas = new List<T>();
                foreach (IReplicatedObject replica in replicaList.Values)
                {
                    replicas.Add((T)replica);
                }
                return replicas;
            }
            return null;
        }

        public IReplicatedObject AccessReplica(HashKey key, BadumnaId id)
        {
            Dictionary<BadumnaId, IReplicatedObject> replicaList;
            if (this.mReplicatedObjectStore.TryGetValue(key, out replicaList))
            {
                IReplicatedObject replica;
                replicaList.TryGetValue(id, out replica);
                return replica;
            }
            return null;
        }
    }


}
