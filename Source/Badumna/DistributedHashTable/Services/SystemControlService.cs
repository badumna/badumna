﻿using System;
using System.Collections.Generic;
using System.Text;

using Badumna.Core;
using Badumna.Utilities;
using Badumna.Transport;
using Badumna.Security;

namespace Badumna.DistributedHashTable
{
    class SystemControlService : DhtProtocol
    {
        private ConnectionTable mConnectionTable;

        protected TokenCache tokens;

        public SystemControlService(DhtProtocol parent, ConnectionTable connectionTable, TokenCache tokens)
            : base(parent)
        {
            this.mConnectionTable = connectionTable;
            this.tokens = tokens;
        }

        #region Protocol methods

        [DhtProtocolMethod(DhtMethod.SystemControlBlacklistUser)]
        protected void BlacklistUser(long blacklistedUserId, TimeSpan blacklistEntryLifeSpan, long timestamp, Permission permission, Signature signature)
        {
            if (permission.Verify(PermissionType.Blacklist, this.tokens))
            {
                if (signature.Verify(permission, blacklistedUserId, blacklistEntryLifeSpan, timestamp))
                {
                    this.mConnectionTable.BlacklistUser(blacklistedUserId, blacklistEntryLifeSpan);
                }
                else
                {
                    Logger.TraceError(LogTag.DHT, "Could not verify signature for blacklist request");
                }
            }
            else
            {
                Logger.TraceError(LogTag.DHT, "Could not verify that user has blacklist permission.");
            }
        }

        #endregion

    }
}
