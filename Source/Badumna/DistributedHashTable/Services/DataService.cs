using System;
using System.Collections.Generic;
using System.Text;

using Badumna.Core;
using Badumna.Utilities;
using Badumna;
using Badumna.DataTypes;

namespace Badumna.DistributedHashTable
{
    delegate void GetReplyHandler(String key, ICollection<String> values);

    class DataService : DhtProtocol
    {
        private ReplicaManager.ReplicaArrivalNotification arrivalHandlerDelegate;
        private EventHandler<ExpireEventArgs> expirationHandlerDelegate;

        public event ReplicaManager.ReplicaArrivalNotification ArrivalHandler
        {
            add { this.arrivalHandlerDelegate += value; }
            remove { this.arrivalHandlerDelegate -= value; }
        }

        public event EventHandler<ExpireEventArgs> ExpirationHandler
        {
            add { this.expirationHandlerDelegate += value; }
            remove { this.expirationHandlerDelegate -= value; }
        }

        public int NumberPendingRequests { get { return this.mRequestHandlers.Count; } }

        private DhtFacade mDhtFacade;
        private Dictionary<String, GetReplyHandler> mRequestHandlers = new Dictionary<string, GetReplyHandler>();

        /// <summary>
        /// Provides the public peer address.
        /// </summary>
        private INetworkAddressProvider addressProvider;

        /// <summary>
        /// A delegate that will generate a unique BadumnaId associated with this peer.
        /// </summary>
        private GenericCallBackReturn<BadumnaId> generateBadumnaId;

        public DataService(DhtFacade facade, INetworkAddressProvider addressProvider, GenericCallBackReturn<BadumnaId> generateBadumnaId)
            : base(facade)
        {
            this.mDhtFacade = facade;
            this.addressProvider = addressProvider;
            this.generateBadumnaId = generateBadumnaId;
            facade.RegisterTypeForReplication<ReplicatedDataObject>(this.ReplicaExpirationNotification, this.ReplicaArrivalNotification);
        }

        private void ReplicaArrivalNotification(IReplicatedObject replica, bool isOriginal)
        {
            ReplicaManager.ReplicaArrivalNotification handler = this.arrivalHandlerDelegate;
            if (handler != null)
            {
                handler.Invoke(replica, isOriginal);
            }
        }

        private void ReplicaExpirationNotification(object sender, ExpireEventArgs e)
        {
            EventHandler<ExpireEventArgs> handler = this.expirationHandlerDelegate;
            if (handler != null)
            {
                handler.Invoke(sender, e);
            }
        }

        public void Put(String key, String value, uint timeToLiveSeconds)
        {
            Logger.TraceInformation(LogTag.DHT, "Putting ---- {0} - {1}", key, value);

            ReplicatedDataObject data = new ReplicatedDataObject(key, this.generateBadumnaId());

            data.Data = value;
            this.mDhtFacade.Replicate(data, HashKey.Hash(key), timeToLiveSeconds);
        }

        public void Get(String key, GetReplyHandler replyCallback)
        {
            DhtEnvelope envelope = this.GetMessageFor(HashKey.Hash(key), QualityOfService.Reliable);

            // Should be set prior to RouteMessage() call in case of immediate callback.
            if (!this.mRequestHandlers.ContainsKey(key))
            {
                this.mRequestHandlers.Add(key, replyCallback);
            }

            this.RemoteCall(envelope, this.GetRequest, key);
            this.SendMessage(envelope);
        }

        [DhtProtocolMethod(DhtMethod.DataServiceGetRequest)]
        protected void GetRequest(String key)
        {
            DhtEnvelope envelope = this.GetMessageFor(this.CurrentEnvelope.Source, QualityOfService.Reliable);

            IEnumerable<ReplicatedDataObject> replicas = this.mDhtFacade.AccessReplicas<ReplicatedDataObject>(HashKey.Hash(key));
            IList<String> items = new List<String>();

            if (null != replicas)
            {
                foreach (ReplicatedDataObject replica in replicas)
                {
                    items.Add(replica.Data);
                }
            }

            Logger.TraceInformation(LogTag.DHT, "Getting ---- {0} ({1})", key, items.Count);

            this.RemoteCall(envelope, this.GetReply, key, items);
            this.SendMessage(envelope);
        }

        [DhtProtocolMethod(DhtMethod.DataServiceGetReply)]
        protected void GetReply(String key, IList<String> values)
        {
            if (this.mRequestHandlers.ContainsKey(key))
            {
                this.mRequestHandlers[key].Invoke(key, values);
                this.mRequestHandlers.Remove(key);
            }
        }


        public delegate void PokeReplyHandler(HashKey key, PeerAddress address);
        private Dictionary<HashKey, PokeReplyHandler> mPokeHandlers = new Dictionary<HashKey, PokeReplyHandler>();

        public void Poke(HashKey key, PokeReplyHandler handler)
        {
            DhtEnvelope envelope = this.GetMessageFor(key, QualityOfService.Reliable);

            Logger.TraceInformation(LogTag.DHT, "making poke request to {0}", key.ToString());

            this.mPokeHandlers.Add(key, handler);
            this.RemoteCall(envelope, this.PokeRequest, this.addressProvider.PublicAddress);
            this.SendMessage(envelope);
        }

        [DhtProtocolMethod(DhtMethod.DataServicePokeRequest)]
        public void PokeRequest(PeerAddress requestSource)
        {
            DhtEnvelope envelope = this.GetMessageFor(requestSource, QualityOfService.Reliable);

            Logger.TraceInformation(LogTag.DHT, "poke request from {0} ({1})", requestSource, this.CurrentEnvelope.DestinationKey);

            this.RemoteCall(envelope, this.PokeReply, this.CurrentEnvelope.DestinationKey, this.addressProvider.PublicAddress);
            this.SendMessage(envelope);
        }

        [DhtProtocolMethod(DhtMethod.DataServicePokeReply)]
        public void PokeReply(HashKey key, PeerAddress address)
        {
            Logger.TraceInformation(LogTag.DHT, "poke reply from {0}", address);

            if (this.mPokeHandlers.ContainsKey(key))
            {
                this.mPokeHandlers[key].Invoke(key, address);
                this.mPokeHandlers.Remove(key);
            }
        }

        public delegate void TestRouteHandler(PeerAddress sourceAddress, int messageId, string message, bool hasArrived);
        public TestRouteHandler TestRouteArrivalEvent { get; set; }

        public void TestRouteMessage(HashKey destinationKey, int messageId, string message)
        {
            DhtEnvelope envelope = this.GetMessageFor(destinationKey, QualityOfService.Reliable);

            envelope.Qos.HandleAlongRoutePath = true;

            this.RemoteCall(envelope, this.TestRequest, messageId, message);
            this.SendMessage(envelope);
        }

        [DhtProtocolMethod(DhtMethod.DataServiceTestRequest)]
        public void TestRequest(int messageId, string message)
        {
            if (this.TestRouteArrivalEvent != null)
            {
                this.TestRouteArrivalEvent(this.CurrentEnvelope.Source, messageId, message, 
                    this.mDhtFacade.MapsToLocalPeer(this.CurrentEnvelope.DestinationKey, null));
            }
        }
    }
}
