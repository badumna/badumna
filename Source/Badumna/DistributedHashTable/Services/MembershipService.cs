using System;
using System.Collections.Generic;
using System.Text;

using Badumna.Core;
using Badumna.Utilities;
using Badumna;
using Badumna.DataTypes;

namespace Badumna.DistributedHashTable
{
    class MembershipReplica : IReplicatedObject
    {
        private List<BadumnaId> mMembers;

        public BadumnaId GroupId { get { return this.mGuid; } }
        public IList<BadumnaId> MemberIds { get { return this.mMembers; } }

        private TimeSpan mStorageTime;
        public TimeSpan StorageTime { get { return this.mStorageTime; } }

        public MembershipReplica() { }

        public MembershipReplica(BadumnaId groupId, uint timeToLive)
        {
            this.mGuid = groupId;
            this.mMembers = new List<BadumnaId>();
            this.mTimeToLiveSeconds = timeToLive;
        }

        #region IReplicatedObject Members

        private bool ignoreModificationNumber = false;
        public bool IgnoreModificationNumber
        {
            get { return this.ignoreModificationNumber; }
        }

        private BadumnaId mGuid;
        public BadumnaId Guid
        {
            get { return this.mGuid; }
            set { this.mGuid = value; }
        }

        private CyclicalID.UShortID mModificationNumber;
        public CyclicalID.UShortID ModificationNumber
        {
            get { return this.mModificationNumber; }
            set { this.mModificationNumber = value; }
        }

        private HashKey mObjectKey;
        public HashKey Key
        {
            get { return this.mObjectKey; }
            set { this.mObjectKey = value; }
        }

        private uint mTimeToLiveSeconds;
        public uint TimeToLiveSeconds
        {
            get { return this.mTimeToLiveSeconds; }
            set { this.mTimeToLiveSeconds = value; }
        }

        public int NumberOfReplicas { get { return 5; } }

        #endregion

        #region IParseable Members

        public void ToMessage(MessageBuffer message, Type parameterType)
        {
            if (null != message)
            {
                message.Write(this.mGuid);
                message.Write((ushort)this.mMembers.Count);

                foreach (BadumnaId memberId in this.mMembers)
                {
                    ((IParseable)memberId).ToMessage(message, typeof(BadumnaId));
                }
            }
        }

        public void FromMessage(MessageBuffer message)
        {
            if (null != message)
            {
                this.mGuid = message.Read<BadumnaId>();
                int membersLength = (int)message.ReadUShort();

                this.mMembers = new List<BadumnaId>();
                for (int i = 0; i < membersLength; i++)
                {
                    this.mMembers.Add(message.Read<BadumnaId>());
                }
            }
        }

        #endregion

        #region IExpireable Members

        private TimeSpan mExpirationTime;
        public TimeSpan ExpirationTime
        {
            get { return this.mExpirationTime; }
            set
            {
                if (this.mExpirationTime == new TimeSpan())
                {
                    this.mStorageTime = value;
                }
                this.mExpirationTime = value;
            }
        }

        #endregion
    }

    class MembershipEvent : EventArgs
    {
        private BadumnaId mGroupId;
        public BadumnaId GroupId { get { return this.mGroupId; } }

        private BadumnaId mMemberId;
        public BadumnaId MemberId { get { return this.mMemberId; } }

        private bool mIsEntering;
        public bool IsEntering { get { return this.mIsEntering; } }

        public MembershipEvent(BadumnaId memberId, BadumnaId groupId, bool isEntering)
        {
            this.mMemberId = memberId;
            this.mGroupId = groupId;
            this.mIsEntering = isEntering;
        }
    }

    class MembershipListEvent : EventArgs
    {
        private BadumnaId mGroupId;
        public BadumnaId GroupId { get { return this.mGroupId; } }

        private IList<BadumnaId> mMembers;
        public IList<BadumnaId> Members { get { return this.mMembers; } }

        public MembershipListEvent(IList<BadumnaId> members, BadumnaId groupId)
        {
            this.mMembers = members;
            this.mGroupId = groupId;
        }
    }

    class MembershipService : DhtProtocol
    {
        private class Group
        {
            private BadumnaId mGroupId;
            private EventHandler<MembershipEvent> mMembershipEventHandler;

            private List<BadumnaId> mKnownMembers = new List<BadumnaId>();
            public List<BadumnaId> KnownMembers { get { return this.mKnownMembers; } }

            private List<BadumnaId> mOldestMembers = new List<BadumnaId>();
            public List<BadumnaId> OldestMembers { get { return this.mOldestMembers; } }

            internal Group(EventHandler<MembershipEvent> membershipEventHandler, BadumnaId groupId, BadumnaId memberId)
            {
                this.mGroupId = groupId;
                this.mMembershipEventHandler = membershipEventHandler;
                this.mKnownMembers.Add(memberId);
                this.mOldestMembers.Add(memberId);
            }

            public void AddMember(BadumnaId memberId)
            {
                if (!this.mKnownMembers.Contains(memberId))
                {
                    this.mKnownMembers.Add(memberId);
                    if (null != this.mMembershipEventHandler)
                    {
                        this.mMembershipEventHandler(this, new MembershipEvent(memberId, this.mGroupId, true));
                    }
                }
                if (this.mOldestMembers.Count < Parameters.MaximumInMembershipList)
                {
                    this.mOldestMembers.Add(memberId);
                }
            }

            public void RemoveMemeber(BadumnaId memberId)
            {
                if (this.mKnownMembers.Contains(memberId))
                {
                    this.mKnownMembers.Remove(memberId);
                    if (null != this.mMembershipEventHandler)
                    {
                        this.mMembershipEventHandler(this, new MembershipEvent(memberId, this.mGroupId, false));
                    }
                }
                if (this.mOldestMembers.Contains(memberId))
                {
                    this.mOldestMembers.Remove(memberId);
                    if (this.mKnownMembers.Count > Parameters.MaximumInMembershipList)
                    {
                        this.mOldestMembers.Add(this.mKnownMembers[Parameters.MaximumInMembershipList]);
                    }
                }
            }
        }

        private DhtFacade mDhtFacade;

        private Dictionary<BadumnaId, Group> mGroups = new Dictionary<BadumnaId, Group>();

        public MembershipService(DhtFacade facade)
            : base(facade)
        {
            this.mDhtFacade = facade;

            this.mDhtFacade.RegisterTypeForReplication<MembershipReplica>(this.ExpirationHanlder);
        }

        public void Join(BadumnaId groupId, BadumnaId memberId, uint timeToLiveSeconds, EventHandler<MembershipEvent> membershipEventHandler)
        {
            HashKey key = HashKey.Hash(groupId.ToString());
            DhtEnvelope envelope = this.GetMessageFor(key, QualityOfService.Reliable);

            envelope.Qos.HandleAlongRoutePath = true; // Handle at the next hop
            this.RemoteCall(envelope, this.SubscribeAndNotifyOldest, groupId, memberId.LocalId, timeToLiveSeconds);
            this.SendMessage(envelope);

            if (this.mGroups.ContainsKey(groupId))
            {
                this.mGroups.Remove(groupId);
            }

            this.mGroups.Add(groupId, new Group(membershipEventHandler, groupId, memberId));
        }

        public void Leave(BadumnaId groupId, BadumnaId memberId)
        {
            HashKey key = HashKey.Hash(groupId.ToString());
            DhtEnvelope envelope = this.GetMessageFor(key, QualityOfService.Reliable);

            envelope.Qos.HandleAlongRoutePath = true; // Handle at the next hop
            this.RemoteCall(envelope, this.UnsubscribeAndNotifyAny, groupId, memberId.LocalId);
            this.SendMessage(envelope);

            if (this.mGroups.ContainsKey(groupId))
            {
                this.mGroups.Remove(groupId);
            }
        }

        private void ExpirationHanlder(object sender, ExpireEventArgs e)
        {
        }

        private MembershipReplica GetGroupReplica(HashKey key, BadumnaId groupId)
        {
            return this.mDhtFacade.AccessReplica<MembershipReplica>(key, groupId);
        }

        private DhtEnvelope GetEnvelopeToFirstMember(HashKey key, BadumnaId groupId)
        {
            MembershipReplica replica = this.GetGroupReplica(key, groupId);

            if (null != replica && replica.MemberIds.Count > 0)
            {
                return this.GetMessageFor(replica.MemberIds[0].Address, QualityOfService.Reliable);
            }

            return null;
        }

        private void NotifyOldest(HashKey key, BadumnaId groupId, BadumnaId memberId, IList<PeerAddress> excludedPeers)
        {
            MembershipReplica group = this.GetGroupReplica(key, groupId);

            if (null != group)
            {
                FailHandlerQos errorHandledQos = new FailHandlerQos(QualityOfService.Reliable, new SendFailureEventArgs());
                errorHandledQos.NumberOfAttempts = 2;

                errorHandledQos.FailureEvent += delegate(object sender, SendFailureEventArgs e)
                {
                    if (e.NthAttempt == 2 || e.IsFinalAttempt)
                    {
                        excludedPeers.Add(e.Destination);
                        this.NotifyOldest(key, groupId, memberId, excludedPeers);
                    }
                };

                if (group.MemberIds.Count > 0)
                {
                    BadumnaId firstMemberId = group.MemberIds[0];
                    DhtEnvelope envelope = this.GetMessageFor(firstMemberId.Address, errorHandledQos);

                    this.RemoteCall(envelope, this.NewMember, groupId, memberId);
                    this.SendMessage(envelope);

                    envelope = this.GetMessageFor(memberId.Address, QualityOfService.Reliable);

                    this.RemoteCall(envelope, this.NewMembers, groupId, group.MemberIds);
                    this.SendMessage(envelope);
                }
            }
        }

        private void AddMemberToReplica(MembershipReplica replica, BadumnaId groupId, BadumnaId memberId, uint timeToLiveSeconds)
        {
            if (null != replica && !replica.MemberIds.Contains(memberId))
            {
                replica.MemberIds.Add(memberId);
                replica.TimeToLiveSeconds = timeToLiveSeconds;

                if (replica.MemberIds.Count > Parameters.MaximumInMembershipList)
                {
                    replica.MemberIds.RemoveAt(0);
                }

                this.mDhtFacade.Replicate(replica, replica.Key, timeToLiveSeconds);
            }

            if (null == replica)
            {
                MembershipReplica groupReplica = new MembershipReplica(groupId, timeToLiveSeconds);

                groupReplica.MemberIds.Add(memberId); 
                groupReplica.TimeToLiveSeconds = timeToLiveSeconds;
                this.mDhtFacade.Replicate(groupReplica, HashKey.Hash(groupId.ToString()), timeToLiveSeconds);
            }
        }

        private void RemoveMemberFromReplica(MembershipReplica replica, BadumnaId memberId, uint timeToLiveSeconds)
        {
            if (null != replica && replica.MemberIds.Contains(memberId))
            {
                replica.MemberIds.Remove(memberId);
                this.mDhtFacade.UpdateReplica(replica, timeToLiveSeconds);
            }
        }

        #region Protocol methods

        [DhtProtocolMethod(DhtMethod.MembershipUnsubscribeAndNotifyAny)]
        private void UnsubscribeAndNotifyAny(BadumnaId groupId, ushort localId)
        {
            BadumnaId memberId = new BadumnaId(this.CurrentEnvelope.Source, localId);

            if (!this.mDhtFacade.MapsToLocalPeer(this.CurrentEnvelope.DestinationKey, null))
            {
                Group group = null;

                this.mGroups.TryGetValue(groupId, out group);
                if (null != group)
                {
                    Logger.TraceInformation(
                        LogTag.DHT,
                        "Membership leave request intercepted for group {0} and member {1}",
                        groupId,
                        memberId);
                    group.RemoveMemeber(memberId);
                }

                return;
            }

            MembershipReplica replica = this.GetGroupReplica(this.CurrentEnvelope.DestinationKey, groupId);
            this.RemoveMemberFromReplica(replica, memberId, Parameters.GroupMembershipTimeout);

            DhtEnvelope envelope = this.GetEnvelopeToFirstMember(this.CurrentEnvelope.DestinationKey, groupId);
            if (null != envelope)
            {
                this.RemoteCall(envelope, this.MemberExits, groupId, memberId);
                this.SendMessage(envelope);
            }
        }

        [DhtProtocolMethod(DhtMethod.MembershipSubscribeAndNotifyOldest)]
        private void SubscribeAndNotifyOldest(BadumnaId groupId, ushort localId, uint timeToLive)
        {
            BadumnaId memberId = new BadumnaId(this.CurrentEnvelope.Source, localId);

            if (!this.mDhtFacade.MapsToLocalPeer(this.CurrentEnvelope.DestinationKey, null))
            {
                Group group = null;

                this.mGroups.TryGetValue(groupId, out group);
                if (null != group)
                {
                    if (group.KnownMembers.Count > Parameters.MaximumInMembershipList / 2)
                    {
                        Logger.TraceInformation(
                            LogTag.DHT,
                            "Membership join request intercepted for group {0} and member {1}",
                            groupId, 
                            memberId);
                        group.AddMember(memberId);

                        // Send a notification to the source for this address.
                        DhtEnvelope envelope = this.GetMessageFor(this.CurrentEnvelope.Source, QualityOfService.Reliable);

                        this.RemoteCall(envelope, this.NewMembers, groupId, group.OldestMembers);
                        this.SendMessage(envelope);

                        // Only end the route path here if there are enough members to be able to assume that 
                        // this peer is well established in the group. That is it knows enough people to be 
                        // confident that the rest will be found through other means.

                        this.CurrentEnvelope.Qos.HandleAlongRoutePath = false;
                    }
                }

                return;
            }

            MembershipReplica replica = this.GetGroupReplica(this.CurrentEnvelope.DestinationKey, groupId);

            this.NotifyOldest(this.CurrentEnvelope.DestinationKey, groupId, memberId, new List<PeerAddress>());
            this.AddMemberToReplica(replica, groupId, memberId, timeToLive);
        }

        [DhtProtocolMethod(DhtMethod.MembershipNewMember)]
        private void NewMember(BadumnaId groupId, BadumnaId memberId)
        {
            Group group = null;

            this.mGroups.TryGetValue(groupId, out group);
            if (null != group)
            {
                Logger.TraceInformation(LogTag.DHT, "Notification of 1 new member");

                group.AddMember(memberId);
            }
        }

        [DhtProtocolMethod(DhtMethod.MembershipNewMembers)]
        private void NewMembers(BadumnaId groupId, IList<BadumnaId> memberIds)
        {
            Group group = null;

            this.mGroups.TryGetValue(groupId, out group);
            if (null != group)
            {
                Logger.TraceInformation(LogTag.DHT, "Notification of {0} new members", memberIds.Count);

                foreach (BadumnaId memberId in memberIds)
                {
                    group.AddMember(memberId);
                }
            }
        }

        [DhtProtocolMethod(DhtMethod.MembershipMemberExits)]
        private void MemberExits(BadumnaId groupId, BadumnaId memberId)
        {
            Group group = null;

            this.mGroups.TryGetValue(groupId, out group);
            if (null != group)
            {
                group.RemoveMemeber(memberId);
            }
        }

        #endregion

    }
}
