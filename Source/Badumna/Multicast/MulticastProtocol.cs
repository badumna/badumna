using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;

using Badumna.Core;
using Badumna.Utilities;
using Badumna;
using Badumna.Transport;
using Badumna.DataTypes;

namespace Badumna.Multicast
{
    class MulticastProtocol : ProtocolComponent<MulticastEnvelope>
    {
        public MulticastProtocol(GenericCallBackReturn<object, Type> factory)
            : base("Multicast", typeof(MulticastProtocolMethodAttribute), factory)
        {
        }

        public MulticastProtocol(MulticastProtocol parent)
            : base(parent)
        { }

        public MulticastEnvelope GetMessageFor(BadumnaId groupKey, QualityOfService qos)
        {
            MulticastEnvelope envelope = new MulticastEnvelope(groupKey, qos);

            this.PrepareMessageForDeparture(ref envelope);
            return envelope;
        }

        public MulticastEnvelope GetDirectEnvelope(PeerAddress destination, BadumnaId groupKey, QualityOfService qos)
        {
            MulticastEnvelope envelope = new MulticastEnvelope(destination, groupKey, qos);

            this.PrepareMessageForDeparture(ref envelope);
            return envelope;
        }
    }
}
