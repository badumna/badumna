using System;
using System.Collections.Generic;
using System.Text;

using Badumna.Core;
using Badumna;
using Badumna.Utilities;
using Badumna.Transport;
using Badumna.DistributedHashTable;
using Badumna.DataTypes;

namespace Badumna.Multicast
{
    class MulticastFacade : MulticastProtocol, IMessageConsumer<MulticastEnvelope>
    {
        private IConnectionTable mConnectionTable;
        private MulticastTransportLayer mTransportLayer;
        private MembershipService mMembershipService;

        private Dictionary<BadumnaId, MulticastGroup> mGroups = new Dictionary<BadumnaId, MulticastGroup>();

        /// <summary>
        /// The queue to use for scheduling events.
        /// </summary>
        private NetworkEventQueue eventQueue;

        /// <summary>
        /// Provides the public peer address.
        /// </summary>
        private INetworkAddressProvider addressProvider;

        /// <summary>
        /// Reports on network connectivity status.
        /// </summary>
        private INetworkConnectivityReporter connectivityReporter;

        /// <summary>
        /// Provides notification of peer connections and disconnections.
        /// </summary>
        private IPeerConnectionNotifier connectionNotifier;

        /// <summary>
        /// A delegate that will generate a unique BadumnaId associated with this peer.
        /// </summary>
        private GenericCallBackReturn<BadumnaId> generateBadumnaId;

        /// <summary>
        /// A factory that constructs an instance of the given type.
        /// </summary>
        private GenericCallBackReturn<object, Type> factory;

        public MulticastFacade(
            ProtocolRoot connectionTable,
            MembershipService membershipService,
            NetworkEventQueue eventQueue,
            INetworkAddressProvider addressProvider,
            INetworkConnectivityReporter connectivityReporter,
            IPeerConnectionNotifier connectionNotifier,
            GenericCallBackReturn<BadumnaId> generateBadumnaId,
            GenericCallBackReturn<object, Type> factory)
            : base(factory)
        {
            this.eventQueue = eventQueue;
            this.addressProvider = addressProvider;
            this.connectivityReporter = connectivityReporter;
            this.connectionNotifier = connectionNotifier;
            this.generateBadumnaId = generateBadumnaId;
            this.factory = factory;

            this.mConnectionTable = connectionTable;
            this.mTransportLayer = new MulticastTransportLayer(connectionTable, this);
            this.mMembershipService = membershipService;
            this.DispatcherMethod = this.DispatchMessage;

            this.connectivityReporter.NetworkOnlineEvent += this.OnlineHandler;
            this.connectivityReporter.NetworkShuttingDownEvent += this.ShuttingDown;
        }

        private void OnlineHandler()
        {
            foreach (KeyValuePair<BadumnaId, MulticastGroup> item in this.mGroups)
            {
                if (!item.Value.IsInitialized)
                {
                    item.Value.Initialize();
                }
            }
        }        
        
        private void ShuttingDown()
        {
            foreach (KeyValuePair<BadumnaId, MulticastGroup> item in this.mGroups)
            {
                if (item.Value.IsInitialized)
                {
                    item.Value.Shutdown();
                }
            }
        }

        public void DispatchMessage(MulticastEnvelope envelope)
        {
            MulticastGroup group = null;

            this.mGroups.TryGetValue(envelope.GroupKey, out group);
            if (null != group)
            {
                if (null != envelope.Destination)
                {
                    group.SendDirect(envelope);
                }
                else
                {
                    group.Multicast(envelope);
                }
            }
        }

        public BadumnaId AddGroup(BadumnaId groupKey)
        {
            MulticastGroup group = null;

            this.mGroups.TryGetValue(groupKey, out group);
            if (null == group)
            {
                Logger.TraceInformation(LogTag.Multicast, "Subscribing to multicast group {0}", groupKey);
                group = new MulticastGroup(groupKey, this.mConnectionTable, this.mTransportLayer, this, this.mMembershipService, this.eventQueue, this.addressProvider, this.connectivityReporter, this.connectionNotifier, this.generateBadumnaId(), this.factory);
                this.mGroups.Add(groupKey, group);
                group.Initialize();
            }

            return group.MemberId;
        }

        public void LeaveGroup(BadumnaId groupKey)
        {
            MulticastGroup group = null;

            this.mGroups.TryGetValue(groupKey, out group);
            if (null != group)
            {
                group.Shutdown();
                this.mGroups.Remove(groupKey);
            }
        }

        public void HandleMessage(BadumnaId groupKey, TransportEnvelope envelope)
        {
            MulticastGroup group = null;

            this.mGroups.TryGetValue(groupKey, out group);
            if (null != group)
            {
                Logger.TraceInformation(LogTag.Multicast, "Message from multicast group {0}", groupKey);
                group.ProcessMessage(envelope);
            }
        }

        #region IMessageConsumer<MulticastEnvelope> Members

        public void ProcessMessage(MulticastEnvelope envelope)
        {
            this.ParseMessage(envelope, 0);
        }

        #endregion
    }
}
