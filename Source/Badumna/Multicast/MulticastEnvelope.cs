using System;
using System.Collections.Generic;
using System.Text;

using Badumna.Core;
using Badumna;
using Badumna.Utilities;
using Badumna.DistributedHashTable;
using Badumna.Transport;
using Badumna.DataTypes;

namespace Badumna.Multicast
{
    class MulticastEnvelope : TransportEnvelope
    {
        private BadumnaId mGroupKey;
        internal BadumnaId GroupKey
        {
            get { return this.mGroupKey; }
            set { this.mGroupKey = value; }
        }

        private PeerAddress mImmediateSource;
        public PeerAddress ImmediateSource
        {
            get { return this.mImmediateSource; }
            set { this.mImmediateSource = value; }
        }

        public MulticastEnvelope()
        { }

        public MulticastEnvelope(BadumnaId groupKey, QualityOfService qos)
            : base(null, qos)
        {
            this.mGroupKey = groupKey;
        }

        public MulticastEnvelope(PeerAddress destination, BadumnaId groupKey, QualityOfService qos)
            : base(destination, qos) 
        {
            this.mGroupKey = groupKey;
        }

        public MulticastEnvelope(TransportEnvelope transportEnvelope, BadumnaId groupKey)
            : base(transportEnvelope)
        {
            this.mGroupKey = groupKey;
        }

        #region IParseable Members

        public override void ToMessage(MessageBuffer message, Type parameterType)
        {
            if (null != message)
            {
                base.ToMessage(message, typeof(TransportEnvelope));

                if (parameterType == typeof(MulticastEnvelope))
                {
                    ((IParseable)this.mGroupKey).ToMessage(message, typeof(BadumnaId));
                }
            }
        }

        public override void FromMessage(MessageBuffer message)
        {
            if (null != message)
            {
                base.FromMessage(message);

                this.mGroupKey = new BadumnaId();
                ((IParseable)this.mGroupKey).FromMessage(message);           
            }
        }

        #endregion
    }
}
