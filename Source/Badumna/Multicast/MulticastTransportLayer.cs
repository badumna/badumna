using System;
using System.Collections.Generic;
using System.Text;

using Badumna.Core;
using Badumna.Transport;
using Badumna.Utilities;
using Badumna.DataTypes;

namespace Badumna.Multicast
{
    class MulticastTransportLayer : TransportProtocol
    {
        private MulticastFacade mFacade;

        public MulticastTransportLayer(TransportProtocol parent, MulticastFacade facade)
            : base(parent)
        {
            this.mFacade = facade;
        }

        public void PrepareTransportEnvelope(BadumnaId groupKey, ref TransportEnvelope envelope)
        {
            envelope = (TransportEnvelope)this.GetMessageFor(envelope.Destination, envelope.Qos);

            if (null != envelope)
            {
                this.RemoteCall(envelope, this.HandleMulticastMessage, groupKey);
            }
        }

        #region Protocol methods

        [ConnectionfulProtocolMethod(ConnectionfulMethod.HandleMulticastMessage)]
        private void HandleMulticastMessage(BadumnaId groupKey)
        {
            this.mFacade.HandleMessage(groupKey, this.CurrentEnvelope);
        }

        #endregion

    }
}
