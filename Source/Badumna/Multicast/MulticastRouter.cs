using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;

using Badumna.Core;
using Badumna;
using Badumna.Utilities;
using Badumna.Transport;
using Badumna.DistributedHashTable;

namespace Badumna.Multicast
{
    class MulticastRouter : Router
    {
        private List<PeerAddress> mPendingConfirmations = new List<PeerAddress>();

        public MulticastRouter(
            RouterProtocol parent,
            IConnectionTable connectionTable,
            IMessageConsumer<DhtEnvelope> child,
            NetworkEventQueue eventQueue,
            INetworkAddressProvider addressProvider,
            INetworkConnectivityReporter connectivityReporter,
            IPeerConnectionNotifier connectionNotifier)
            : base(parent, connectionTable, child, eventQueue, addressProvider, connectivityReporter, connectionNotifier)
        { }

        public void LeaveGroup()
        {
            this.MakeLeaveRequest();
            this.eventQueue.Schedule(500, this.Shutdown);
        }

        public void AddPendingConfirmation(PeerAddress address)
        {
            this.mPendingConfirmations.Add(address);
        }

        protected override void ConnectionEstablishedHandler(PeerAddress address)
        {
            if (this.mPendingConfirmations.Contains(address))
            {
                this.QuarantinePeer(address);
                this.mPendingConfirmations.Remove(address);
            }
        }

        protected override void ConnectionLostHandler(PeerAddress address)
        {
            if (this.mPendingConfirmations.Contains(address))
            {
                this.mPendingConfirmations.Remove(address);
            }
            this.RemoveNeighbour(address);
        }
    }
}