using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;

using Badumna.Core;
using Badumna;
using Badumna.Utilities;
using Badumna.Transport;
using Badumna.DistributedHashTable;
using Badumna.DataTypes;

namespace Badumna.Multicast
{
    class MulticastGroup : RouterProtocol, IConnectionTable, IMessageConsumer<DhtEnvelope>
    {
        private BadumnaId mGroupKey;

        private BadumnaId mMemberId;
        public BadumnaId MemberId { get { return this.mMemberId; } }

        private MulticastRouter mRouter;
        private MembershipService mMembershipService;

        [NonSerialized]
        private IConnectionTable mConnectionTable;
        private MulticastTransportLayer mTransportLayer;
        private IMessageConsumer<MulticastEnvelope> mMulticastConsumer;
        private PeerAddress mImmediateSource;

        private bool mIsInitialized;
        public bool IsInitialized { get { return this.mIsInitialized; } }

        public MulticastGroup(BadumnaId groupKey,
            IConnectionTable connectionTable,
            MulticastTransportLayer transportLater,
            IMessageConsumer<MulticastEnvelope> multicastConsumer, 
            MembershipService membershipService,
            NetworkEventQueue eventQueue,
            INetworkAddressProvider addressProvider,
            INetworkConnectivityReporter connectivityReporter,
            IPeerConnectionNotifier connectionNotifier,
            BadumnaId memberId,
            GenericCallBackReturn<object, Type> factory)
            : base("MulticastGroup", typeof(MulticastGroupProtocolMethodAttribute), factory)
        {
            this.mGroupKey = groupKey;
            this.mConnectionTable = connectionTable;
            this.mTransportLayer = transportLater;
            this.mRouter = new MulticastRouter(this, this, this, eventQueue, addressProvider, connectivityReporter, connectionNotifier);
            this.mMulticastConsumer = multicastConsumer;
            this.mMembershipService = membershipService;
            this.mMemberId = memberId;
            this.DispatcherMethod = this.DispatchMessage;
        }

        public void Initialize()
        {
            Logger.TraceInformation(LogTag.Multicast, "Initializing multicast group {0}", this.mGroupKey);

            this.mIsInitialized = true;
            //this.router.Initialize();
            this.mMembershipService.Join(this.mGroupKey, this.mMemberId, Parameters.GroupMembershipTimeout, this.MembershipEventHandler);
        }

        public void Shutdown()
        {
            Logger.TraceInformation(LogTag.Multicast, "Shuting down multicast group {0}", this.mGroupKey);

            this.mIsInitialized = false;
            this.mRouter.LeaveGroup();
            this.mMembershipService.Leave(this.mGroupKey, this.mMemberId);
        }

        public void Multicast(TransportEnvelope payloadMessage)
        {
            this.mImmediateSource = this.mMemberId.Address;
            payloadMessage.Source = this.mMemberId.Address;
            this.mRouter.Broadcast(payloadMessage);
        }

        public void SendDirect(TransportEnvelope payloadMessage)
        {
            this.mImmediateSource = this.mMemberId.Address;
            payloadMessage.Source = this.mMemberId.Address;
            this.mRouter.DirectSend(payloadMessage);
        }

        public void DispatchMessage(TransportEnvelope envelope)
        {
            this.mTransportLayer.SendMessage(envelope);
        }

        public override void PrepareMessageForDeparture(ref TransportEnvelope envelope)
        {
            this.mTransportLayer.PrepareTransportEnvelope(this.mGroupKey, ref envelope);
        }

        private void MembershipEventHandler(object sender, MembershipEvent e)
        {
            if (e.IsEntering)
            {
                this.ConfirmConnection(e.MemberId.Address);
            }
            else
            {
                this.mRouter.RemoveNeighbour(e.MemberId.Address);
            }
        }

        #region IConnectionTable Members

        public bool IsKnown(PeerAddress address)
        {
            return this.mConnectionTable.IsKnown(address);
        }

        public bool IsConnected(PeerAddress address)
        {
            return this.mConnectionTable.IsConnected(address);
        }

        public bool IsRelayed(PeerAddress address)
        {
            return this.mConnectionTable.IsRelayed(address);
        }

        public void RemoveConnection(PeerAddress address)
        {
            this.mConnectionTable.RemoveConnection(address);
        }

        public IEnumerable<IConnection> AllConnections
        {
            get { throw new NotImplementedException(); }
        }

        public void ConfirmConnection(PeerAddress address)
        {
            if (this.mConnectionTable.IsConnected(address))
            {
                if (!this.mRouter.LeafSet.Contains(address))
                {
                    this.mRouter.QuarantinePeer(address);
                }
            }
            else
            {
                this.mRouter.AddPendingConfirmation(address);
            }

            TransportEnvelope pingEnvelope = this.GetMessageFor(address, QualityOfService.Reliable);

            this.RemoteCall(pingEnvelope, this.Ping);
            this.SendMessage(pingEnvelope);        
        }

        #endregion


        #region IMessageConsumer<TransportEnvelope> Members

        public void ProcessMessage(TransportEnvelope envelope)
        {
            this.mImmediateSource = envelope.Source;
            this.ParseMessage(envelope, envelope.Message.ReadOffset);
        }

        #endregion

        #region IMessageConsumer<DhtEnvelope> Members

        public void ProcessMessage(DhtEnvelope envelope)
        {
            MulticastEnvelope multicastEnvelope = new MulticastEnvelope(envelope, this.mGroupKey);

            multicastEnvelope.ImmediateSource = this.mImmediateSource;

            this.mMulticastConsumer.ProcessMessage(multicastEnvelope);
        }

        #endregion


        #region Protocol methods

        [MulticastGroupProtocolMethod(MulticastGroupMethod.Ping)]
        private void Ping()
        {
            // Only add the neighbor if it is unknown, otherwise it will cause unnecessary synchronisation.
            if (!this.mRouter.LeafSet.Contains(this.CurrentEnvelope.Source))
            {
                this.mRouter.QuarantinePeer(this.CurrentEnvelope.Source);
            }

            TransportEnvelope pongEnvelope = this.GetMessageFor(this.CurrentEnvelope.Source, QualityOfService.Reliable);

            this.RemoteCall(pongEnvelope, this.Pong);
            this.SendMessage(pongEnvelope);
        }

        [MulticastGroupProtocolMethod(MulticastGroupMethod.Pong)]
        private void Pong()
        {
            // Only add the neighbor if it is unknown, otherwise it will cause unnecessary synchronisation.
            if (!this.mRouter.LeafSet.Contains(this.CurrentEnvelope.Source))
            {
                this.mRouter.QuarantinePeer(this.CurrentEnvelope.Source);
            }
        }

        #endregion

    }

}