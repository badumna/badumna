﻿//-----------------------------------------------------------------------
// <copyright file="DistributedControllerStub.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2011 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.CompilerServices;
using Badumna.Controllers;
using Badumna.Core;
using Badumna.DataTypes;
using Badumna.SpatialEntities;

namespace Badumna.CppWrapperStub
{
    /// <summary>
    /// The stub class for distributed controller.
    /// </summary>
    /// <exclude/>
    public class DistributedControllerStub : DistributedSceneController
    {
        /// <summary>
        /// The original object controlled by the controller.
        /// </summary>
        private ISpatialOriginal original;

        /// <summary>
        /// Known replicas;
        /// </summary>
        private Dictionary<BadumnaId, ISpatialReplica> replicas;

        /// <summary>
        /// Initializes a new instance of the <see cref="DistributedControllerStub"/> class.
        /// </summary>
        /// <param name="uniqueName">A unique string identifying the controller.</param>
        public DistributedControllerStub(string uniqueName)
            : base(uniqueName)
        {
            this.replicas = new Dictionary<BadumnaId, ISpatialReplica>();
            CallCreateController(this, uniqueName, ControllerInitializerHelper.GetTypeName(uniqueName));
        }

        /// <summary>
        /// C++, Calls the create controller.
        /// </summary>
        /// <param name="managed_object">The managed object.</param>
        /// <param name="uniqueName">The unique name of controller.</param>
        /// <param name="typeName">Name of the type.</param>
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        public static extern void CallCreateController(
            DistributedControllerStub managed_object, 
            string uniqueName, 
            string typeName);

        /// <summary>
        /// C++, Calls the take control of entity.
        /// </summary>
        /// <param name="uniqueName">Name of the unique.</param>
        /// <param name="entityId">The entity id.</param>
        /// <param name="entityType">Type of the entity.</param>
        /// <returns>
        /// The original entity.
        /// </returns>
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        public static extern ISpatialOriginal CallTakeControlOfDCEntity(
            string uniqueName,
            BadumnaId entityId,
            uint entityType);

        /// <summary>
        /// C++ CreateDCSpatialReplica method.
        /// </summary>
        /// <param name="uniqueName">The unique id.</param>
        /// <param name="entityId">Entity id.</param>
        /// <param name="entityType">Entity type.</param>
        /// <returns>Return ISpatialReplica stub.</returns>
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        public static extern ISpatialReplica CallCreateDCSpatialReplica(
            string uniqueName,
            BadumnaId entityId,
            uint entityType);

        /// <summary>
        /// C++ RemoveDCSpatialReplica method.
        /// </summary>
        /// <param name="uniqueName">The unique name.</param>
        /// <param name="repliaId">The replia id.</param>
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        public static extern void CallRemoveDCSpatialReplica(
            string uniqueName,
            string repliaId);

        /// <summary>
        /// C++, Calls the DC checkpoint.
        /// </summary>
        /// <param name="uniqueName">The unique name of the controller.</param>
        /// <returns>The checkpointed state.</returns>
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        public static extern byte[] CallDCCheckpoint(string uniqueName);

        /// <summary>
        /// C++, Calls the DC recover.
        /// </summary>
        /// <param name="uniqueName">The unique name of the controller.</param>
        /// <param name="data">The data to recover from.</param>
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        public static extern void CallDCRecover(string uniqueName, byte[] data);

        /// <summary>
        /// C++, Calls the DC recover2.
        /// </summary>
        /// <param name="uniqueName">The unique name of the controller.</param>
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        public static extern void CallDCRecover2(string uniqueName);

        /// <summary>
        /// C++, Calls the DC process.
        /// </summary>
        /// <param name="uniqueName">The unique name of controller.</param>
        /// <param name="duration_in_ms">The duration in millisecond.</param>
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        public static extern void CallDCProcess(string uniqueName, int duration_in_ms);

        /// <summary>
        /// C++, Calls the DC wake.
        /// </summary>
        /// <param name="uniqueName">The unique name of the controller.</param>
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        public static extern void CallDCWake(string uniqueName);

        /// <summary>
        /// C++, Calls the DC sleep.
        /// </summary>
        /// <param name="uniqueName">The unique name of the controller.</param>
        /// <param name="originalId">The original id.</param>
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        public static extern void CallDCSleep(string uniqueName, string originalId);

        /// <summary>
        /// Calls the DC shutdown.
        /// </summary>
        /// <param name="uniqueName">The unique name of the controller.</param>
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        public static extern void CallDCShutdown(string uniqueName);

        /// <inheritdoc/>
        internal override void Destroy()
        {
            List<ISpatialReplica> toRemove = new List<ISpatialReplica>();
            foreach (ISpatialReplica replica in this.replicas.Values)
            {
                toRemove.Add(replica);
            }

            foreach (ISpatialReplica replica in toRemove)
            {
                this.RemoveEntity(replica);
            }

            CallDCShutdown(this.UniqueName);
        }

        /// <inheritdoc/>
        protected override ISpatialEntity TakeControlOfEntity(
            BadumnaId entityId,
            ISpatialReplica remoteEntity,
            uint entityType)
        {
            if (this.original == null)
            {
                this.original = CallTakeControlOfDCEntity(this.UniqueName, entityId, entityType);
            }

            return this.original;
        }

        /// <inheritdoc/>
        protected override ISpatialEntity InstantiateRemoteEntity(BadumnaId entityId, uint entityType)
        {
            if (this.replicas.ContainsKey(entityId))
            {
                return this.replicas[entityId];
            }
            else
            {
                ISpatialReplica replica = CallCreateDCSpatialReplica(this.UniqueName, entityId, entityType);
                this.replicas[entityId] = replica;
                return replica;
            }
        }

        /// <inheritdoc/>
        protected override void RemoveEntity(IReplicableEntity replica)
        {
            ISpatialReplica spatialReplica = (ISpatialReplica)replica;
            if (this.replicas.ContainsKey(spatialReplica.Guid))
            {
                string replica_id = string.Empty;
                if (spatialReplica is SpatialReplicaStub)
                {
                    replica_id = ((SpatialReplicaStub)spatialReplica).UniqueId;
                }
              
                CallRemoveDCSpatialReplica(this.UniqueName, replica_id);
                this.replicas.Remove(spatialReplica.Guid);
            }
        }

        /// <inheritdoc/>
        protected override void Checkpoint(BinaryWriter writer)
        {
            if (this.original != null)
            {
                byte[] data = CallDCCheckpoint(this.UniqueName);
                if (data != null)
                {
                    writer.Write(data);
                }
            }
        }

        /// <inheritdoc/>
        protected override void Recover(BinaryReader reader)
        {
            if (this.original != null)
            {
                byte[] data = reader.ReadBytes((int)reader.BaseStream.Length);
                CallDCRecover(this.UniqueName, data);
            }
        }

        /// <inheritdoc/>
        protected override void Recover()
        {
            if (this.original != null)
            {
                CallDCRecover2(this.UniqueName);
            }
        }

        /// <inheritdoc/>
        protected override void Process(TimeSpan duration)
        {
            if (this.original != null)
            {
                CallDCProcess(this.UniqueName, (int)duration.TotalMilliseconds);
            }
        }

        /// <inheritdoc/>
        protected override void ReceiveMessage(MessageStream message, BadumnaId source)
        {
            throw new NotImplementedException();
        }

        /// <inheritdoc/>
        protected override void Wake()
        {
            CallDCWake(this.UniqueName);
        }

        /// <inheritdoc/>
        protected override void Sleep()
        {
            if (this.original != null)
            {
                string original_id = string.Empty;
                if (this.original is SpatialOriginalStub)
                {
                    original_id = ((SpatialOriginalStub)this.original).UniqueId;
                }

                CallDCSleep(this.UniqueName, original_id);
                this.original = null;
            }
        }
    }
}
