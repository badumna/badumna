﻿//-----------------------------------------------------------------------
// <copyright file="ArbitrationServerStub.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Runtime.CompilerServices;

namespace Badumna.CppWrapperStub
{
    /// <summary>
    /// The arbitration server stub. 
    /// </summary>
    /// <exclude/>
    public class ArbitrationServerStub
    {
        /// <summary>
        /// The facade object.
        /// </summary>
        private INetworkFacade facade;

        /// <summary>
        /// The stub id.
        /// </summary>
        private string uniqueId;

        /// <summary>
        /// Initializes a new instance of the <see cref="ArbitrationServerStub"/> class.
        /// </summary>
        /// <param name="id">The server stub id.</param>
        /// <param name="facade">The network facade object.</param>
        public ArbitrationServerStub(string id, INetworkFacade facade)
        {
            this.facade = facade;
            this.uniqueId = id;
        }

        /// <summary>
        /// Calls the client message handler.
        /// </summary>
        /// <param name="id">The stub id.</param>
        /// <param name="sessionId">The session id.</param>
        /// <param name="message">The message.</param>
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        public static extern void CallClientMessageHandler(string id, int sessionId, byte[] message);

        /// <summary>
        /// Calls the client disconnect handler.
        /// </summary>
        /// <param name="id">The stub id.</param>
        /// <param name="sessionId">The session id.</param>
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        public static extern void CallClientDisconnectHandler(string id, int sessionId);

        /// <summary>
        /// Registers the handlers.
        /// </summary>
        /// <param name="timeOutSeconds">The time out seconds.</param>
        public void Register(int timeOutSeconds)
        {
            this.facade.RegisterArbitrationHandler(
                this.ClientMessageHandler, 
                TimeSpan.FromSeconds(timeOutSeconds), 
                this.ClientDisconnectHandler);
        }

        /// <summary>
        /// The Client message handler.
        /// </summary>
        /// <param name="sessionId">The session id.</param>
        /// <param name="message">The message.</param>
        private void ClientMessageHandler(int sessionId, byte[] message)
        {
            CallClientMessageHandler(this.uniqueId, sessionId, message);
        }

        /// <summary>
        /// The Client disconnection handler.
        /// </summary>
        /// <param name="sessionId">The session id.</param>
        private void ClientDisconnectHandler(int sessionId)
        {
            CallClientDisconnectHandler(this.uniqueId, sessionId);
        }
    }
}
