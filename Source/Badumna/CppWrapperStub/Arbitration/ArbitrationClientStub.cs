﻿//-----------------------------------------------------------------------
// <copyright file="ArbitrationClientStub.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Runtime.CompilerServices;
using Badumna.Arbitration;

namespace Badumna.CppWrapperStub
{
    /// <summary>
    /// The stub class for arbitration client.
    /// </summary>
    /// <exclude/>
    public class ArbitrationClientStub
    {
        /// <summary>
        /// A unique id for the stub.
        /// </summary>
        private string uniqueId;

        /// <summary>
        /// The arbitrator.
        /// </summary>
        private IArbitrator arbitrator;

        /// <summary>
        /// Initializes a new instance of the <see cref="ArbitrationClientStub"/> class.
        /// </summary>
        /// <param name="id">The arbitrator id.</param>
        /// <param name="facade">The network facade object.</param>
        public ArbitrationClientStub(string id, INetworkFacade facade)
        {
            // the arbitrator name is the id
            this.arbitrator = facade.GetArbitrator(id);
            this.uniqueId = id;
        }

        /// <summary>
        /// Gets a value indicating whether this instance is server connected.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is server connected; otherwise, <c>false</c>.
        /// </value>
        public bool IsServerConnected
        {
            get { return this.arbitrator.IsServerConnected; }
        }

        /// <summary>
        /// Calls the connection result handler.
        /// </summary>
        /// <param name="id">The arbitrator id.</param>
        /// <param name="type">The connection result type.</param>
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        public static extern void CallConnectionResultHandler(string id, int type);

        /// <summary>
        /// Calls the connection failure handler.
        /// </summary>
        /// <param name="id">The arbitrator id.</param>
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        public static extern void CallConnectionFailureHandler(string id);

        /// <summary>
        /// Calls the server message handler.
        /// </summary>
        /// <param name="id">The arbitrator id.</param>
        /// <param name="message">The message.</param>
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        public static extern void CallServerMessageHandler(string id, byte[] message);

        /// <summary>
        /// Connects the arbitrator.
        /// </summary>
        public void Connect()
        {
            this.arbitrator.Connect(
                this.ConnectionResultHandler,
                this.ConnectionFailureHandler,
                this.ServerMessageHandler);
        }

        /// <summary>
        /// Sends the event to the server.
        /// </summary>
        /// <param name="message">The message.</param>
        public void SendEvent(byte[] message)
        {
            this.arbitrator.SendEvent(message);
        }

        /// <summary>
        /// The connection result handler. 
        /// </summary>
        /// <param name="type">The connection result type.</param>
        private void ConnectionResultHandler(ServiceConnectionResultType type)
        {
            CallConnectionResultHandler(this.uniqueId, (int)type);
        }

        /// <summary>
        /// The connection failure handler.
        /// </summary>
        private void ConnectionFailureHandler()
        {
            CallConnectionFailureHandler(this.uniqueId);
        }

        /// <summary>
        /// The message server handler.
        /// </summary>
        /// <param name="message">The message.</param>
        private void ServerMessageHandler(byte[] message)
        {
            CallServerMessageHandler(this.uniqueId, message);
        }
    }
}
