﻿//------------------------------------------------------------------------------
// <copyright file="MatchReplicaStub.cs" company="National ICT Australia Limited">
//     Copyright (c) 2013 National ICT Australia Limited. All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

namespace Badumna.CppWrapperStub
{
    using System;
    using System.IO;
    using System.Runtime.CompilerServices;
    using Badumna.DataTypes;
    using Badumna.Replication;
    using Badumna.Utilities;

    /// <summary>
    /// Maps between the managed and C++ match replicas.
    /// </summary>
    public class MatchReplicaStub : IReplica
    {
        /// <summary>
        /// Initializes a new instance of the MatchReplicaStub class.
        /// </summary>
        /// <param name="cppData">Data for the C++ wrapper.</param>
        public MatchReplicaStub(IntPtr cppData)
        {
            this.CppData = cppData;
        }
        
        /// <summary>
        /// Gets the replica data for the C++ wrapper.
        /// </summary>
        public IntPtr CppData { get; private set; }

        /// <inheritdoc />
        public BadumnaId Guid { get; set; }

        /// <summary>
        /// Shuts down the stub, preventing any more calls back into the C++ code.
        /// </summary>
        public void Shutdown()
        {
            this.CppData = IntPtr.Zero;
        }

        /// <inheritdoc />
        public double Deserialize(BooleanArray includedParts, Stream stream, int estimatedMillisecondsSinceDeparture, BadumnaId id)
        {
            if (this.CppData == IntPtr.Zero)
            {
                return 0;  // Set to zero in line with what ReplicaEntityWrapper returns.
            }

            if (includedParts.Any())
            {
                BinaryReader reader = new BinaryReader(stream);
                MatchReplicaStub.CallDeserialize(
                    this.CppData,
                    includedParts,
                    reader.ReadBytes((int)stream.Length),
                    estimatedMillisecondsSinceDeparture);
            }

            return 0;  // Set to zero in line with what ReplicaEntityWrapper returns.
        }

        /// <inheritdoc />
        public void HandleEvent(Stream stream)
        {
            if (this.CppData == IntPtr.Zero)
            {
                return;
            }

            BinaryReader reader = new BinaryReader(stream);
            MatchReplicaStub.CallHandleEvent(this.CppData, reader.ReadBytes((int)stream.Length));
        }

        /// <summary>
        /// Deserialize method in C++ code.
        /// </summary>
        /// <param name="cppData">Data for the C++ wrapper</param>
        /// <param name="includedParts">Included parts.</param>
        /// <param name="bytes">bytes data.</param>
        /// <param name="estimatedMillisecondsSinceDeparture">Estimated in milliseconds since departure.</param>
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        private static extern void CallDeserialize(
            IntPtr cppData,
            BooleanArray includedParts,
            byte[] bytes,
            int estimatedMillisecondsSinceDeparture);

        /// <summary>
        /// Handle event method in c++ code.
        /// </summary>
        /// <param name="cppData">Data for the C++ wrapper</param>
        /// <param name="data">The data byte array.</param>
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        private static extern void CallHandleEvent(IntPtr cppData, byte[] data);
    }
}
