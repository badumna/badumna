﻿//------------------------------------------------------------------------------
// <copyright file="MatchStub.cs" company="National ICT Australia Limited">
//     Copyright (c) 2013 National ICT Australia Limited. All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

namespace Badumna.CppWrapperStub
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Runtime.CompilerServices;
    using Badumna.Autoreplication;
    using Badumna.Match;
    using Badumna.Replication;
    using Badumna.Utilities;

    /// <summary>
    /// Maps between the managed and unmanaged Match.
    /// </summary>
    public class MatchStub
    {
        /// <summary>
        /// Data for the C++ wrapper create/remove replica delegates.
        /// </summary>
        private readonly IntPtr cppData;

        /// <summary>
        /// Original stubs, keyed by the C++ IMatchOriginal pointer.
        /// </summary>
        private Dictionary<IntPtr, MatchOriginalStub> originalStubs = new Dictionary<IntPtr, MatchOriginalStub>();

        /// <summary>
        /// Replica stubs, keyed by the C++ IMatchReplica pointer.
        /// </summary>
        private Dictionary<IntPtr, MatchReplicaStub> replicaStubs = new Dictionary<IntPtr, MatchReplicaStub>();

        /// <summary>
        /// Hosted stubs, keyed by the C++ IMatchHosted pointer.
        /// </summary>
        private Dictionary<IntPtr, MatchHostedStub> hostedStubs = new Dictionary<IntPtr, MatchHostedStub>();

        /// <summary>
        /// Initializes a new instance of the MatchStub class, joining the specified match.
        /// </summary>
        /// <param name="matchFacade">The match facade</param>
        /// <param name="cppData">Data for the C++ wrapper create/remove replica delegates.</param>
        /// <param name="matchmakingResult">A match obtained by querying the matchmaking server.</param>
        /// <param name="playerName">A name for the local player in the match (uniqueness not enforced).</param>
        /// <param name="controller">The match controller, or <c>null</c> if none.</param>
        public MatchStub(
            Badumna.Match.Facade matchFacade,
            IntPtr cppData,
            MatchmakingResult matchmakingResult,
            string playerName,
            object controller = null)
        {
            this.cppData = cppData;

            if (controller == null)
            {
                this.Match = matchFacade.JoinMatch(
                    matchmakingResult,
                    playerName,
                    this.CreateReplica,
                    this.RemoveReplica);
            }
            else
            {
                var controllerStub = controller as MatchHostedStub;
                this.hostedStubs[controllerStub.CppData] = controllerStub;

                this.Match = matchFacade.JoinMatch<MatchHostedStub>(
                    controllerStub,
                    matchmakingResult,
                    playerName,
                    this.CreateReplica,
                    this.RemoveReplica);
            }

            this.InstallHandlers();
        }

        /// <summary>
        /// Initializes a new instance of the MatchStub class, creating the specified match.
        /// </summary>
        /// <param name="matchFacade">The match facade</param>
        /// <param name="cppData">Data for the C++ wrapper create/remove replica delegates.</param>
        /// <param name="criteria">Matchmaking criteria that will be published to matchmaking server.</param>
        /// <param name="maxPlayers">The maximum number of players permitted in the match.</param>
        /// <param name="playerName">A name for the local player in the match (uniqueness not enforced).</param>
        /// <param name="isPrivate">Indicates whether the match is private.</param>
        /// <param name="controller">The match controller, or <c>null</c> if none.</param>
        public MatchStub(
            Badumna.Match.Facade matchFacade,
            IntPtr cppData,
            MatchmakingCriteria criteria,
            int maxPlayers,
            string playerName,
            bool isPrivate,
            object controller = null)
        {
            this.cppData = cppData;

            if (controller == null)
            {
                if (isPrivate)
                {
                    this.Match = matchFacade.CreatePrivateMatch(
                        criteria,
                        maxPlayers,
                        playerName,
                        this.CreateReplica,
                        this.RemoveReplica);
                }
                else
                {
                    this.Match = matchFacade.CreateMatch(
                        criteria,
                        maxPlayers,
                        playerName,
                        this.CreateReplica,
                        this.RemoveReplica);
                }
            }
            else
            {
                var controllerStub = controller as MatchHostedStub;
                this.hostedStubs[controllerStub.CppData] = controllerStub;

                if (isPrivate)
                {
                    this.Match = matchFacade.CreatePrivateMatch<MatchHostedStub>(
                        controllerStub,
                        criteria,
                        maxPlayers,
                        playerName,
                        this.CreateReplica,
                        this.RemoveReplica);
                }
                else
                {
                    this.Match = matchFacade.CreateMatch<MatchHostedStub>(
                        controllerStub,
                        criteria,
                        maxPlayers,
                        playerName,
                        this.CreateReplica,
                        this.RemoveReplica);
                }
            }

            this.InstallHandlers();
        }

        /// <summary>
        /// Gets the managed match.
        /// </summary>
        public Badumna.Match.Match Match { get; private set; }

        /// <summary>
        /// Gets the members of the match.
        /// </summary>
        public List<MemberIdentity> Members
        {
            get { return new List<MemberIdentity>(this.Match.Members); }
        }

        /// <summary>
        /// Registers an entity with the match.
        /// </summary>
        /// <param name="cppOriginal">C++ data for the original</param>
        /// <param name="entityType">The entity type</param>
        public void RegisterEntity(IntPtr cppOriginal, uint entityType)
        {
            var original = new MatchOriginalStub(cppOriginal);
            this.originalStubs[cppOriginal] = original;
            this.Match.RegisterEntity(original, entityType);
        }

        /// <summary>
        /// Unregisters an entity from the match.
        /// </summary>
        /// <param name="cppOriginal">C++ data for the original</param>
        public void UnregisterEntity(IntPtr cppOriginal)
        {
            MatchOriginalStub original;
            if (!this.originalStubs.TryGetValue(cppOriginal, out original))
            {
                return;
            }

            this.Match.UnregisterEntity(original);
            this.originalStubs.Remove(cppOriginal);
        }

        /// <summary>
        /// Register a hosted entity into a match.
        /// </summary>
        /// <param name="cppHosted">C++ data for the entity</param>
        /// <param name="entityType">The entity type</param>
        public void RegisterHostedEntity(IntPtr cppHosted, uint entityType)
        {
            var hosted = new MatchHostedStub(cppHosted);
            this.hostedStubs[cppHosted] = hosted;
            this.Match.RegisterHostedEntity(hosted, entityType);
        }

        /// <summary>
        /// Unregister a hosted entity from a match.
        /// </summary>
        /// <param name="cppHosted">C++ data for the entity</param>
        public void UnregisterHostedEntity(IntPtr cppHosted)
        {
            MatchHostedStub hosted;
            if (!this.hostedStubs.TryGetValue(cppHosted, out hosted))
            {
                return;
            }

            this.Match.UnregisterHostedEntity(hosted);
            this.hostedStubs.Remove(cppHosted);
        }

        /// <summary>
        /// Flags state changes for replication.
        /// </summary>
        /// <param name="cppOriginal">C++ data for the original</param>
        /// <param name="changedParts">Segments to flag</param>
        public void FlagForUpdate(IntPtr cppOriginal, BooleanArray changedParts)
        {
            MatchOriginalStub original;
            if (this.originalStubs.TryGetValue(cppOriginal, out original))
            {
                this.Match.FlagForUpdate(original, changedParts);
                return;
            }

            MatchHostedStub hosted;
            if (this.hostedStubs.TryGetValue(cppOriginal, out hosted))
            {
                this.Match.FlagForUpdate(hosted, changedParts);
            }
        }

        /// <summary>
        /// Flags state change for replication.
        /// </summary>
        /// <param name="cppOriginal">C++ data for the original</param>
        /// <param name="changedPartIndex">Segment index to flag</param>
        public void FlagIndexForUpdate(IntPtr cppOriginal, int changedPartIndex)
        {
            this.FlagForUpdate(cppOriginal, new BooleanArray(changedPartIndex));
        }

        /// <summary>
        /// Sends a custom message to replicas.
        /// </summary>
        /// <param name="cppOriginal">C++ data for the original</param>
        /// <param name="messageData">The message data</param>
        public void SendCustomMessageToReplicas(IntPtr cppOriginal, byte[] messageData)
        {
            MemoryStream stream = new MemoryStream();
            using (BinaryWriter writer = new BinaryWriter(stream))
            {
                writer.Write(messageData);
            }

            MatchOriginalStub original;
            if (this.originalStubs.TryGetValue(cppOriginal, out original))
            {
                this.Match.SendCustomMessageToReplicas(original, stream);
                return;
            }

            MatchHostedStub hosted;
            if (this.hostedStubs.TryGetValue(cppOriginal, out hosted))
            {
                this.Match.SendCustomMessageToReplicas(hosted, stream);
            }
        }

        /// <summary>
        /// Sends a custom message to the original.
        /// </summary>
        /// <param name="cppReplica">C++ data for the replica</param>
        /// <param name="messageData">The message data</param>
        public void SendCustomMessageToOriginal(IntPtr cppReplica, byte[] messageData)
        {
            MemoryStream stream = new MemoryStream();
            using (BinaryWriter writer = new BinaryWriter(stream))
            {
                writer.Write(messageData);
            }

            MatchReplicaStub replica;
            if (this.replicaStubs.TryGetValue(cppReplica, out replica))
            {
                this.Match.SendCustomMessageToOriginal(replica, stream);
                return;
            }

            MatchHostedStub hosted;
            if (this.hostedStubs.TryGetValue(cppReplica, out hosted))
            {
                this.Match.SendCustomMessageToOriginal(hosted, stream);
            }
        }

        /// <summary>
        /// Sends a custom message to the member.
        /// </summary>
        /// <param name="memberId">The member to send to</param>
        /// <param name="messageData">The message data</param>
        public void SendCustomMessageToMember(MemberIdentity memberId, byte[] messageData)
        {
            var controlledMatch = this.Match as Match<MatchHostedStub>;
            if (controlledMatch == null)
            {
                throw new InvalidOperationException("Must have a match controller to send message to members.");
            }

            MemoryStream stream = new MemoryStream();
            using (BinaryWriter writer = new BinaryWriter(stream))
            {
                writer.Write(messageData);
            }

            controlledMatch.SendCustomMessageToMember(memberId, stream);
        }

        /// <summary>
        /// Sends a custom message to all members.
        /// </summary>
        /// <param name="messageData">The message data</param>
        public void SendCustomMessageToMembers(byte[] messageData)
        {
            var controlledMatch = this.Match as Match<MatchHostedStub>;
            if (controlledMatch == null)
            {
                throw new InvalidOperationException("Must have a match controller to send message to members.");
            }

            MemoryStream stream = new MemoryStream();
            using (BinaryWriter writer = new BinaryWriter(stream))
            {
                writer.Write(messageData);
            }

            controlledMatch.SendCustomMessageToMembers(stream);
        }

        /// <summary>
        /// Sends a custom message to the host.
        /// </summary>
        /// <param name="messageData">The message data</param>
        public void SendCustomMessageToHost(byte[] messageData)
        {
            var controlledMatch = this.Match as Match<MatchHostedStub>;
            if (controlledMatch == null)
            {
                throw new InvalidOperationException("Must have a match controller to send message to host.");
            }

            MemoryStream stream = new MemoryStream();
            using (BinaryWriter writer = new BinaryWriter(stream))
            {
                writer.Write(messageData);
            }

            controlledMatch.SendCustomMessageToHost(stream);
        }

        /// <summary>
        /// Sends a chat message to all members.
        /// </summary>
        /// <param name="message">The message</param>
        public void Chat(string message)
        {
            this.Match.Chat(message);
        }

        /// <summary>
        /// Sends a chat message to the specified member.
        /// </summary>
        /// <param name="member">The member</param>
        /// <param name="message">The message</param>
        public void Chat(MemberIdentity member, string message)
        {
            this.Match.Chat(member, message);
        }

        /// <summary>
        /// Leave the match.
        /// </summary>
        public void Leave()
        {
            foreach (var stub in this.originalStubs.Values)
            {
                stub.Shutdown();
            }

            foreach (var stub in this.replicaStubs.Values)
            {
                stub.Shutdown();
            }

            foreach (var stub in this.hostedStubs.Values)
            {
                stub.Shutdown();
            }

            this.originalStubs.Clear();
            this.replicaStubs.Clear();
            this.hostedStubs.Clear();

            this.Match.Leave();
            this.Match = null;
        }

        /// <summary>
        /// The create replica delegate.
        /// </summary>
        /// <param name="match">The match the entity belongs to.</param>
        /// <param name="source">The source of the entity (peer or host).</param>
        /// <param name="entityType">The application level type ID that was associated with this entity when the entity was registered on the
        /// owning peer.</param>
        /// <returns>An instance of the new entity.</returns>
        public IReplica CreateReplica(Badumna.Match.Match match, EntitySource source, uint entityType)
        {
            if (this.Match == null)
            {
                Debug.Assert(false, "Got create replica after left match");
                return null;
            }

            switch (source)
            {
                case EntitySource.Peer:
                    var cppReplica = InvokeMatchCreateReplicaDelegate(this.cppData, entityType);
                    if (cppReplica == IntPtr.Zero)
                    {
                        throw new ReplicationException("Create replica delegate returned null.");
                    }
                    
                    var replica = new MatchReplicaStub(cppReplica);
                    this.replicaStubs[cppReplica] = replica;
                    return replica;

                case EntitySource.Host:
                    var cppHostedReplica = InvokeMatchCreateHostedReplicaDelegate(this.cppData, entityType);
                    if (cppHostedReplica == IntPtr.Zero)
                    {
                        throw new ReplicationException("Create hosted replica delegate returned null.");
                    }

                    var hosted = new MatchHostedStub(cppHostedReplica);
                    this.hostedStubs[cppHostedReplica] = hosted;
                    return hosted;

                default:
                    throw new ArgumentException("source");
            }
        }

        /// <summary>
        /// The remove replica delegate.
        /// </summary>
        /// <param name="match">The match the entity belongs to.</param>
        /// <param name="source">The source of the entity (peer or host).</param>
        /// <param name="replica">The replica being removed.</param>
        public void RemoveReplica(Badumna.Match.Match match, EntitySource source, object replica)
        {
            // Ignore removes after leaving the match (the delegates provided by the C++ app can't
            // be expected to be valid after leaving).
            if (this.Match == null)
            {
                return;
            }

            switch (source)
            {
                case EntitySource.Peer:
                    var replicaStub = (MatchReplicaStub)replica;
                    InvokeMatchRemoveReplicaDelegate(this.cppData, replicaStub.CppData);
                    this.replicaStubs.Remove(replicaStub.CppData);
                    break;

                case EntitySource.Host:
                    var hostedStub = (MatchHostedStub)replica;
                    InvokeMatchRemoveHostedReplicaDelegate(this.cppData, hostedStub.CppData);
                    this.hostedStubs.Remove(hostedStub.CppData);
                    break;
            }
        }

        /// <summary>
        /// Invokes the C++ create replica delegate.
        /// </summary>
        /// <param name="cppData">Data for the C++ wrapper</param>
        /// <param name="entityType">The application level type ID that was associated with this entity when the entity was registered on the
        /// owning peer.</param>
        /// <returns>C++ data for the replica</returns>
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        private static extern IntPtr InvokeMatchCreateReplicaDelegate(IntPtr cppData, uint entityType);

        /// <summary>
        /// Invokes the C++ create replica delegate.
        /// </summary>
        /// <param name="cppData">Data for the C++ wrapper</param>
        /// <param name="cppReplica">The C++ data for the replica</param>
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        private static extern void InvokeMatchRemoveReplicaDelegate(IntPtr cppData, IntPtr cppReplica);

        /// <summary>
        /// Invokes the C++ create replica delegate.
        /// </summary>
        /// <param name="cppData">Data for the C++ wrapper</param>
        /// <param name="entityType">The application level type ID that was associated with this entity when the entity was registered on the
        /// owning peer.</param>
        /// <returns>C++ data for the hosted replica</returns>
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        private static extern IntPtr InvokeMatchCreateHostedReplicaDelegate(IntPtr cppData, uint entityType);

        /// <summary>
        /// Invokes the C++ create replica delegate.
        /// </summary>
        /// <param name="cppData">Data for the C++ wrapper</param>
        /// <param name="cppHostedReplica">The C++ data for the replica</param>
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        private static extern void InvokeMatchRemoveHostedReplicaDelegate(IntPtr cppData, IntPtr cppHostedReplica);

        /// <summary>
        /// Invokes the C++ state changed delegate.
        /// </summary>
        /// <remarks>
        /// This doesn't pass a MatchStatusEventArgs directly because that requires that
        /// C++ knows the layout of the struct.
        /// Instead we pass the fields of the struct directly.
        /// This still assumes that the underlying type of the enums stay as ints.
        /// </remarks>
        /// <param name="cppData">Data for the C++ wrapper</param>
        /// <param name="status">The match status</param>
        /// <param name="error">The match error</param>
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        private static extern void InvokeMatchStateChangedDelegate(IntPtr cppData, MatchStatus status, MatchError error);

        /// <summary>
        /// Invokes the C++ member added delegate.
        /// </summary>
        /// <param name="matchData">Match data for the C++ wrapper</param>
        /// <param name="memberId">The member id</param>
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        private static extern void InvokeMemberAddedDelegate(IntPtr matchData, MemberIdentity memberId);

        /// <summary>
        /// Invokes the C++ member removed delegate.
        /// </summary>
        /// <param name="matchData">Match data for the C++ wrapper</param>
        /// <param name="memberId">The member id</param>
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        private static extern void InvokeMemberRemovedDelegate(IntPtr matchData, MemberIdentity memberId);

        /// <summary>
        /// Invokes the C++ chat message delegate.
        /// </summary>
        /// <remarks>
        /// This doesn't pass a MatchChatEventArgs directly because that requires that
        /// C++ knows the layout of the struct.
        /// Instead we pass the fields of the struct directly.
        /// </remarks>
        /// <param name="matchData">Match data for the C++ wrapper</param>
        /// <param name="message">The message</param>
        /// <param name="senderId">The sender id</param>
        /// <param name="chatType">The chat type</param>
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        private static extern void InvokeChatMessageDelegate(IntPtr matchData, string message, MemberIdentity senderId, ChatType chatType);

        /// <summary>
        /// Installs event handlers that forward to their C++ equivalents.
        /// </summary>
        private void InstallHandlers()
        {
            this.Match.StateChanged +=
                delegate(Match sender, MatchStatusEventArgs args)
                {
                    // Ignore any invocations after leaving the match.
                    if (this.Match == null)
                    {
                        return;
                    }

                    MatchStub.InvokeMatchStateChangedDelegate(this.cppData, args.Status, args.Error);
                };

            this.Match.MemberAdded +=
                delegate(Match sender, MatchMembershipEventArgs args)
                {
                    // Ignore any invocations after leaving the match.
                    if (this.Match == null)
                    {
                        return;
                    }

                    MatchStub.InvokeMemberAddedDelegate(this.cppData, args.Member);
                };

            this.Match.MemberRemoved +=
                delegate(Match sender, MatchMembershipEventArgs args)
                {
                    // Ignore any invocations after leaving the match.
                    if (this.Match == null)
                    {
                        return;
                    }

                    MatchStub.InvokeMemberRemovedDelegate(this.cppData, args.Member);
                };

            this.Match.ChatMessageReceived +=
                delegate(Match sender, MatchChatEventArgs args)
                {
                    // Ignore any invocations after leaving the match.
                    if (this.Match == null)
                    {
                        return;
                    }

                    MatchStub.InvokeChatMessageDelegate(this.cppData, args.Message, args.Sender, args.Type);
                };
        }
    }
}
