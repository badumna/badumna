﻿//------------------------------------------------------------------------------
// <copyright file="MatchHostedStub.cs" company="National ICT Australia Limited">
//     Copyright (c) 2013 National ICT Australia Limited. All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

namespace Badumna.CppWrapperStub
{
    using System;
    using System.IO;
    using System.Runtime.CompilerServices;
    using Badumna.DataTypes;
    using Badumna.Replication;
    using Badumna.Utilities;

    /// <summary>
    /// Maps between the managed world and the unmanaged IMatchHosted interface.
    /// </summary>
    public class MatchHostedStub : IOriginal, IReplica
    {
        /// <summary>
        /// Initializes a new instance of the MatchHostedStub class.
        /// </summary>
        /// <param name="cppData">Data for the C++ wrapper</param>
        public MatchHostedStub(IntPtr cppData)
        {
            this.CppData = cppData;
        }

        /// <summary>
        /// Gets the data for the C++ wrapper.
        /// </summary>
        public IntPtr CppData { get; private set; }

        /// <inheritdoc />
        public BadumnaId Guid { get; set; }

        /// <summary>
        /// Shuts down the stub, preventing any more calls back into the C++ code.
        /// </summary>
        public void Shutdown()
        {
            this.CppData = IntPtr.Zero;
        }

        /// <inheritdoc />
        public BooleanArray Serialize(BooleanArray requiredParts, Stream stream)
        {
            if (this.CppData == IntPtr.Zero)
            {
                return new BooleanArray();
            }

            if (requiredParts.Any())
            {
                using (var writer = new BinaryWriter(stream))
                {
                    var data = MatchHostedStub.CallSerialize(this.CppData, requiredParts);
                    if (data != null)
                    {
                        writer.Write(data);
                    }
                }
            }

            // We'll always send all required parts.
            return new BooleanArray();
        }

        /// <inheritdoc />
        public double Deserialize(BooleanArray includedParts, Stream stream, int estimatedMillisecondsSinceDeparture, BadumnaId id)
        {
            if (this.CppData == IntPtr.Zero)
            {
                return 0;  // Set to zero in line with what ReplicaEntityWrapper returns.
            }

            if (includedParts.Any())
            {
                BinaryReader reader = new BinaryReader(stream);
                MatchHostedStub.CallDeserialize(
                    this.CppData,
                    includedParts,
                    reader.ReadBytes((int)stream.Length),
                    estimatedMillisecondsSinceDeparture);
            }

            return 0;  // Set to zero in line with what ReplicaEntityWrapper returns.
        }

        /// <inheritdoc />
        public void HandleEvent(Stream stream)
        {
            if (this.CppData == IntPtr.Zero)
            {
                return;
            }

            var reader = new BinaryReader(stream);
            MatchHostedStub.CallHandleEvent(this.CppData, reader.ReadBytes((int)stream.Length));
        }

        /// <summary>
        /// Serialize method in C++ code.
        /// </summary>
        /// <param name="cppData">Data for the C++ wrapper</param>
        /// <param name="requiredParts">Required parts.</param>
        /// <returns>The serialized data.</returns>
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        private static extern byte[] CallSerialize(IntPtr cppData, BooleanArray requiredParts);
        
        /// <summary>
        /// Deserialize method in C++ code.
        /// </summary>
        /// <param name="cppData">Data for the C++ wrapper</param>
        /// <param name="includedParts">Included parts.</param>
        /// <param name="bytes">bytes data.</param>
        /// <param name="estimatedMillisecondsSinceDeparture">Estimated in milliseconds since departure.</param>
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        private static extern void CallDeserialize(
            IntPtr cppData,
            BooleanArray includedParts,
            byte[] bytes,
            int estimatedMillisecondsSinceDeparture);

        /// <summary>
        /// Handle event method in C++ code.
        /// </summary>
        /// <param name="cppData">Data for the C++ wrapper</param>
        /// <param name="data">The data byte array.</param>
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        private static extern void CallHandleEvent(IntPtr cppData, byte[] data);
    }
}
