﻿//------------------------------------------------------------------------------
// <copyright file="MatchOriginalStub.cs" company="National ICT Australia Limited">
//     Copyright (c) 2013 National ICT Australia Limited. All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

namespace Badumna.CppWrapperStub
{
    using System;
    using System.IO;
    using System.Runtime.CompilerServices;
    using Badumna.DataTypes;
    using Badumna.Replication;
    using Badumna.Utilities;

    /// <summary>
    /// Maps between the managed and unmanaged IOriginal interfaces.
    /// </summary>
    public class MatchOriginalStub : IOriginal
    {
        /// <summary>
        /// Data for the C++ wrapper.
        /// </summary>
        private IntPtr cppData;

        /// <summary>
        /// Initializes a new instance of the MatchOriginalStub class.
        /// </summary>
        /// <param name="cppData">Data for the C++ wrapper</param>
        public MatchOriginalStub(IntPtr cppData)
        {
            this.cppData = cppData;
        }

        /// <inheritdoc />
        public BadumnaId Guid { get; set; }

        /// <summary>
        /// Shuts down the stub, preventing any more calls back into the C++ code.
        /// </summary>
        public void Shutdown()
        {
            this.cppData = IntPtr.Zero;
        }

        /// <inheritdoc />
        public BooleanArray Serialize(BooleanArray requiredParts, Stream stream)
        {
            if (this.cppData == IntPtr.Zero)
            {
                return new BooleanArray();
            }

            if (requiredParts.Any())
            {
                using (var writer = new BinaryWriter(stream))
                {
                    var data = MatchOriginalStub.CallSerialize(this.cppData, requiredParts);
                    if (data != null)
                    {
                        writer.Write(data);
                    }
                }
            }

            // We'll always send all required parts.
            return new BooleanArray();
        }

        /// <inheritdoc />
        public void HandleEvent(Stream stream)
        {
            if (this.cppData == IntPtr.Zero)
            {
                return;
            }

            var reader = new BinaryReader(stream);
            MatchOriginalStub.CallHandleEvent(this.cppData, reader.ReadBytes((int)stream.Length));
        }

        /// <summary>
        /// Serialize method in C++ code.
        /// </summary>
        /// <param name="cppData">Data for the C++ wrapper</param>
        /// <param name="requiredParts">Required parts.</param>
        /// <returns>The serialized data.</returns>
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        private static extern byte[] CallSerialize(IntPtr cppData, BooleanArray requiredParts);

        /// <summary>
        /// Handle event method in C++ code.
        /// </summary>
        /// <param name="cppData">Data for the C++ wrapper</param>
        /// <param name="data">The data byte array.</param>
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        private static extern void CallHandleEvent(IntPtr cppData, byte[] data);
    }
}
