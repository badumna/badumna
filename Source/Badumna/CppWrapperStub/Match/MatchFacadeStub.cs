﻿//------------------------------------------------------------------------------
// <copyright file="MatchFacadeStub.cs" company="National ICT Australia Limited">
//     Copyright (c) 2013 National ICT Australia Limited. All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

namespace Badumna.CppWrapperStub
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Text;
    using Badumna.Match;

    /// <summary>
    /// Maps between the managed and C++ match facades.
    /// </summary>
    public class MatchFacadeStub
    {
        /// <summary>
        /// The match facade.
        /// </summary>
        private Badumna.Match.Facade matchFacade;

        /// <summary>
        /// Initializes a new instance of the MatchFacadeStub class.
        /// </summary>
        /// <param name="matchFacade">The match facade</param>
        public MatchFacadeStub(Badumna.Match.Facade matchFacade)
        {
            this.matchFacade = matchFacade;
        }

        /// <summary>
        /// Create a match as host.
        /// </summary>
        /// <param name="criteria">Matchmaking criteria that will be published to matchmaking server.</param>
        /// <param name="maxPlayers">The maximum number of players permitted in the match.</param>
        /// <param name="playerName">A name for the local player in the match (uniqueness not enforced).</param>
        /// <param name="cppData">C++ data for the match delegates</param>
        /// <returns>A new match.</returns>
        public MatchStub CreateMatch(
            MatchmakingCriteria criteria,
            int maxPlayers,
            string playerName,
            IntPtr cppData)
        {
            return new MatchStub(this.matchFacade, cppData, criteria, maxPlayers, playerName, isPrivate: false);
        }

        /// <summary>
        /// Create a match as host with a controller.
        /// </summary>
        /// <param name="controllerData">C++ data for the match controller</param>
        /// <param name="criteria">Matchmaking criteria that will be published to matchmaking server.</param>
        /// <param name="maxPlayers">The maximum number of players permitted in the match.</param>
        /// <param name="playerName">A name for the local player in the match (uniqueness not enforced).</param>
        /// <param name="matchData">C++ data for the match delegates</param>
        /// <returns>A new match.</returns>
        public MatchStub CreateMatch(
            IntPtr controllerData,
            MatchmakingCriteria criteria,
            int maxPlayers,
            string playerName,
            IntPtr matchData)
        {
            return new MatchStub(this.matchFacade, matchData, criteria, maxPlayers, playerName, isPrivate: false, controller: new MatchHostedStub(controllerData));
        }

        /// <summary>
        /// Create a private match as host.
        /// </summary>
        /// <param name="criteria">Matchmaking criteria that will be published to matchmaking server.</param>
        /// <param name="maxPlayers">The maximum number of players permitted in the match.</param>
        /// <param name="playerName">A name for the local player in the match (uniqueness not enforced).</param>
        /// <param name="cppData">C++ data for the match delegates</param>
        /// <returns>A new match.</returns>
        public MatchStub CreatePrivateMatch(
            MatchmakingCriteria criteria,
            int maxPlayers,
            string playerName,
            IntPtr cppData)
        {
            return new MatchStub(this.matchFacade, cppData, criteria, maxPlayers, playerName, isPrivate: true);
        }

        /// <summary>
        /// Create a private match as host with a controller.
        /// </summary>
        /// <param name="controllerData">C++ data for the match controller</param>
        /// <param name="criteria">Matchmaking criteria that will be published to matchmaking server.</param>
        /// <param name="maxPlayers">The maximum number of players permitted in the match.</param>
        /// <param name="playerName">A name for the local player in the match (uniqueness not enforced).</param>
        /// <param name="matchData">C++ data for the match delegates</param>
        /// <returns>A new match.</returns>
        public MatchStub CreatePrivateMatch(
            IntPtr controllerData,
            MatchmakingCriteria criteria,
            int maxPlayers,
            string playerName,
            IntPtr matchData)
        {
            return new MatchStub(this.matchFacade, matchData, criteria, maxPlayers, playerName, isPrivate: true, controller: new MatchHostedStub(controllerData));
        }

        /// <summary>
        /// Join a known match.
        /// </summary>
        /// <param name="matchmakingResult">A match obtained by querying the matchmaking server.</param>
        /// <param name="playerName">A name for the local player in the match (uniqueness not enforced).</param>
        /// <param name="cppData">C++ data for the match delegates</param>
        /// <returns>A match object for managing the match.</returns>
        public MatchStub JoinMatch(
            MatchmakingResult matchmakingResult,
            string playerName,
            IntPtr cppData)
        {
            return new MatchStub(this.matchFacade, cppData, matchmakingResult, playerName);
        }

        /// <summary>
        /// Join a known match with a controller.
        /// </summary>
        /// <param name="controllerData">C++ data for the match controller</param>
        /// <param name="matchmakingResult">A match obtained by querying the matchmaking server.</param>
        /// <param name="playerName">A name for the local player in the match (uniqueness not enforced).</param>
        /// <param name="matchData">C++ data for the match delegates</param>
        /// <returns>A match object for managing the match.</returns>
        public MatchStub JoinMatch(
            IntPtr controllerData,
            MatchmakingResult matchmakingResult,
            string playerName,
            IntPtr matchData)
        {
            return new MatchStub(this.matchFacade, matchData, matchmakingResult, playerName, controller: new MatchHostedStub(controllerData));
        }

        /// <summary>
        /// Query the matchmaking server to find matches available to join.
        /// </summary>
        /// <param name="criteria">Matchmaking criteria.</param>
        /// <param name="cppData">C++ data for the results delegate</param>
        public void FindMatches(
            MatchmakingCriteria criteria,
            IntPtr cppData)
        {
            this.matchFacade.FindMatches(
                criteria,
                (MatchmakingQueryResult result) =>
                {
                    InvokeFindMatchesResultsDelegate(cppData, result);
                });
        }

        /// <summary>
        /// Invokes the C++ find matches results delegate.
        /// </summary>
        /// <param name="cppData">C++ data for the results delegate</param>
        /// <param name="result">The query results</param>
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        private static extern void InvokeFindMatchesResultsDelegate(IntPtr cppData, MatchmakingQueryResult result);
    }
}
