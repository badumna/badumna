﻿//-----------------------------------------------------------------------
// <copyright file="ChatSessionStub.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System.Runtime.CompilerServices;
using Badumna.Chat;
using Badumna.DataTypes;

namespace Badumna.CppWrapperStub
{
    /// <summary>
    /// The Stub class for chat session.
    /// </summary>
    /// <exclude/>
    public class ChatSessionStub
    {
        /// <summary>
        /// The id of this instance.
        /// </summary>
        private string uniqueId;

        /// <summary>
        /// The chat service.
        /// </summary>
        private IChatSession chatSession;

        /// <summary>
        /// Initializes a new instance of the <see cref="ChatSessionStub"/> class.
        /// </summary>
        /// <param name="id">The id for this instance.</param>
        /// <param name="facade">The network facade object.</param>
        public ChatSessionStub(string id, INetworkFacade facade)
        {
            this.uniqueId = id;
            this.chatSession = facade.ChatSession;
        }

        /// <summary>
        /// Calls the message handler.
        /// </summary>
        /// <param name="uniqueId">The unique session id.</param>
        /// <param name="channel">The chat channel.</param>
        /// <param name="userId">The sender's user id.</param>
        /// <param name="message">The chat message.</param>
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        public static extern void CallMessageHandler(
            string uniqueId,
            ChatChannelId channel,
            BadumnaId userId,
            string message);

        /// <summary>
        /// Calls the invitation handler.
        /// </summary>
        /// <param name="uniqueId">The unique session id.</param>
        /// <param name="channel">The channel.</param>
        /// <param name="username">The username.</param>
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        public static extern void CallInvitationHandler(
            string uniqueId,
            ChatChannelId channel,
            string username);

        /// <summary>
        /// Calls the presence handler.
        /// </summary>
        /// <param name="uniqueId">The unique session id.</param>
        /// <param name="channel">The channel.</param>
        /// <param name="userId">The user id.</param>
        /// <param name="displayName">The display name.</param>
        /// <param name="status">The status.</param>
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        public static extern void CallPresenceHandler(
            string uniqueId,
            ChatChannelId channel,
            BadumnaId userId,
            string displayName,
            int status);

        /// <summary>
        /// Subscribes to proximity channel.
        /// </summary>
        /// <param name="entityId">The entity id.</param>
        /// <returns>The proximity channel</returns>
        public IChatChannel SubscribeToProximityChannel(BadumnaId entityId)
        {
            IChatChannel channel = this.chatSession.SubscribeToProximityChannel(entityId, this.MessageHandler);
            return channel;
        }

        /// <summary>
        /// Opens the private channels.
        /// </summary>
        public void OpenPrivateChannels()
        {
            this.chatSession.OpenPrivateChannels(this.InvitationHandler);
        }

        /// <summary>
        /// Invites the user to private channel.
        /// </summary>
        /// <param name="username">The username.</param>
        public void InviteUserToPrivateChannel(string username)
        {
            this.chatSession.InviteUserToPrivateChannel(username);
        }

        /// <summary>
        /// Accepts the invitation.
        /// </summary>
        /// <param name="channelId">The channel ID.</param>
        /// <returns>The private channel</returns>
        public IChatChannel AcceptInvitation(ChatChannelId channelId)
        {
            return this.chatSession.AcceptInvitation(channelId, this.MessageHandler, this.PresenceHandler);
        }

        /// <summary>
        /// Changes the presence.
        /// </summary>
        /// <param name="status">The status.</param>
        public void ChangePresence(int status)
        {
            this.chatSession.ChangePresence((ChatStatus)status);
        }

        /// <summary>
        /// Proximity chat message handler.
        /// </summary>
        /// <param name="channel">The channel.</param>
        /// <param name="userId">The user id.</param>
        /// <param name="message">The incoming chat message.</param>
        private void MessageHandler(IChatChannel channel, BadumnaId userId, string message)
        {
            CallMessageHandler(this.uniqueId, channel.Id, userId, message);
        }

        /// <summary>
        /// Chat invitation handler.
        /// </summary>
        /// <param name="channel">The channel.</param>
        /// <param name="username">The username.</param>
        private void InvitationHandler(ChatChannelId channel, string username)
        {
            CallInvitationHandler(this.uniqueId, channel, username);
        }

        /// <summary>
        /// Presence handler.
        /// </summary>
        /// <param name="channel">The channel.</param>
        /// <param name="userId">The user id.</param>
        /// <param name="displayName">The display name.</param>
        /// <param name="status">The status.</param>
        private void PresenceHandler(IChatChannel channel, BadumnaId userId, string displayName, ChatStatus status)
        {
            CallPresenceHandler(this.uniqueId, channel.Id, userId, displayName, (int)status);
        }
    }
}
