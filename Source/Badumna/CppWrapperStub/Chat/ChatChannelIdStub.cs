﻿//-----------------------------------------------------------------------
// <copyright file="ChatChannelIdStub.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Runtime.CompilerServices;
using Badumna.Chat;
using Badumna.DataTypes;

namespace Badumna.CppWrapperStub
{
    /// <summary>
    /// The Stub class for chat channel ID.
    /// </summary>
    /// <exclude/>
    public class ChatChannelIdStub
    {
        /// <summary>
        /// Creates a ChatChannelId from a type and BadumnaId.
        /// This should only be used internally by the C++ wrapper.
        /// </summary>
        /// <param name="type">The ChatChannelType</param>
        /// <param name="id">The BadumnaID</param>
        /// <returns>A new chat channel ID</returns>
        public static ChatChannelId CreateChatChannelId(ChatChannelType type, BadumnaId id)
        {
            return new ChatChannelId(type, id);
        }
    }
}
