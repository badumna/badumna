﻿//-----------------------------------------------------------------------
// <copyright file="Vector3Stub.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2011 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;

namespace Badumna.CppWrapperStub
{
    /// <summary>
    /// Vector3 stub.
    /// </summary>
    /// <exclude/>
    public class Vector3Stub
    {
        /// <summary>
        /// x value of the vector3.
        /// </summary>
        private float x;

        /// <summary>
        /// y value of the vector3.
        /// </summary>
        private float y;

        /// <summary>
        /// z value of the vector3.
        /// </summary>
        private float z;

        /// <summary>
        /// Initializes a new instance of the <see cref="Vector3Stub"/> class.
        /// </summary>
        public Vector3Stub()
        {
            this.x = 0.0f;
            this.y = 0.0f;
            this.z = 0.0f;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Vector3Stub"/> class.
        /// </summary>
        /// <param name="x">The x value.</param>
        /// <param name="y">The y value.</param>
        /// <param name="z">The z value.</param>
        public Vector3Stub(float x, float y, float z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        /// <summary>
        /// Gets the X value.
        /// </summary>
        public float X
        {
            get { return this.x; }
        }

        /// <summary>
        /// Gets the Y value.
        /// </summary>
        public float Y
        {
            get { return this.y; }
        }

        /// <summary>
        /// Gets the Z value.
        /// </summary>
        public float Z
        {
            get { return this.z; }
        }
    }
}
