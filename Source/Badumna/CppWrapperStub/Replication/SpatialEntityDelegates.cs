﻿//-----------------------------------------------------------------------
// <copyright file="SpatialEntityDelegates.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.CppWrapperStub
{
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using Badumna.DataTypes;
    using Badumna.SpatialEntities;

    /// TODO(lni): this class should be renamed to something like NetworkSceneStub.
    /// <summary>
    /// SpatialEntitiesDelegates class has responsibility to call the appropriate delegate in C++ world.
    /// </summary>
    /// <exclude/>
    public class SpatialEntityDelegates
    {
        /// <summary>
        /// The facade object.
        /// </summary>
        private INetworkFacade facade;

        /// <summary>
        /// A unique id.
        /// </summary>
        private string uniqueId;

        /// <summary>
        /// The network scene.
        /// </summary>
        private NetworkScene networkScene;

        /// <summary>
        /// Known replicas;
        /// </summary>
        private Dictionary<BadumnaId, ISpatialReplica> replicas;

        /// <summary>
        /// Initializes a new instance of the <see cref="SpatialEntityDelegates"/> class.
        /// </summary>
        /// <param name="id">A unique id.</param>
        /// <param name="facade">The network facade object.</param>
        public SpatialEntityDelegates(string id, INetworkFacade facade)
        {
            this.facade = facade;
            this.uniqueId = id;
            this.replicas = new Dictionary<BadumnaId, ISpatialReplica>();
        }

        /// <summary>
        /// Gets the name of the scene.
        /// </summary>
        /// <value>The scene name.</value>
        public string Name
        {
            get { return this.networkScene.Name; }
        }
        
        /// <summary>
        /// Gets a value indicating whether the scene is a mini scene.
        /// </summary>
        public bool MiniScene
        {
            get { return this.networkScene.MiniScene; }
        }

        /// <summary>
        /// C++ CreateSpatialReplica method.
        /// </summary>
        /// <param name="uniqueId">The unique id.</param>
        /// <param name="scene">Network scene.</param>
        /// <param name="sceneId">The scene id.</param>
        /// <param name="entityId">Entity id.</param>
        /// <param name="entityType">Entity type.</param>
        /// <returns>
        /// Return ISpatialReplica stub.
        /// </returns>
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        public static extern ISpatialReplica CallCreateSpatialReplica(
            string uniqueId,
            SpatialEntityDelegates scene,
            string sceneId,
            BadumnaId entityId,
            uint entityType);

        /// <summary>
        /// C++ RemoveSpatialReplica method.
        /// </summary>
        /// <param name="uniqueId">The unique id.</param>
        /// <param name="scene">Network scene.</param>
        /// <param name="repliaId">The replia id.</param>
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        public static extern void CallRemoveSpatialReplica(
            string uniqueId,
            SpatialEntityDelegates scene,
            string repliaId);

        /// <summary>
        /// Join to badumna scene, this method will be called from the C++ side.
        /// </summary>
        /// <param name="sceneName">Scene name.</param>
        public void JoinScene(string sceneName)
        {
            this.networkScene = this.facade.JoinScene(
                sceneName, 
                this.CreateSpatialReplica, 
                this.RemoveSpatialReplica) as NetworkScene;
        }

        /// <summary>
        /// Join to badumna mini scene, this method will be called from the C++ side.
        /// </summary>
        /// <param name="sceneName">Scene name.</param>
        public void JoinMiniScene(string sceneName)
        {
            this.networkScene = this.facade.JoinMiniScene(
                sceneName,
                this.CreateSpatialReplica,
                this.RemoveSpatialReplica) as NetworkScene;
        }

        /// <summary>
        /// Leaves the scene.
        /// </summary>
        public void Leave()
        {
            this.networkScene.Leave();
        }

        /// <summary>
        /// Registers the entity.
        /// </summary>
        /// <param name="originalStub">The original stub.</param>
        /// <param name="entityType">Type of the entity.</param>
        /// <returns>The GUID of the registered entity.</returns>
        public BadumnaId RegisterEntity(SpatialOriginalStub originalStub, uint entityType)
        {
            this.networkScene.RegisterEntity(originalStub, entityType);
            return originalStub.Guid;
        }

        /// <summary>
        /// Unregisters the entity.
        /// </summary>
        /// <param name="originalStub">The original stub.</param>
        public void UnregisterEntity(SpatialOriginalStub originalStub)
        {
            this.networkScene.UnregisterEntity(originalStub);
        }

        /// <summary>
        /// CreateSpatialReplica callback function responsible for calling the CreateSpatialReplica in C++ world.
        /// </summary>
        /// <param name="scene">Network scene.</param>
        /// <param name="entityId">Entity id.</param>
        /// <param name="entityType">Entity type.</param>
        /// <returns>Return ISpatialReplica stub.</returns>
        private ISpatialReplica CreateSpatialReplica(NetworkScene scene, BadumnaId entityId, uint entityType)
        {
            if (this.replicas.ContainsKey(entityId))
            {
                System.Console.Error.WriteLine("the replica is a known one. entity id = {0}", entityId);
                return this.replicas[entityId];
            }
            else
            {
                ISpatialReplica replica = CallCreateSpatialReplica(this.uniqueId, this, this.uniqueId, entityId, entityType);
                this.replicas[entityId] = replica;
                return replica;
            }
        }

        /// <summary>
        /// RemoveSpatialReplica callback function responsible for calling the RemoveSpatialReplica in C++ world.
        /// </summary>
        /// <param name="scene">Network scene.</param>
        /// <param name="replica">Replica need to be removed.</param>
        private void RemoveSpatialReplica(NetworkScene scene, IReplicableEntity replica)
        {
            ISpatialReplica spatialReplica = (ISpatialReplica)replica;
            if (this.replicas.ContainsKey(spatialReplica.Guid))
            {
                string replica_id = string.Empty;
                if (spatialReplica is SpatialReplicaStub)
                {
                    replica_id = ((SpatialReplicaStub)spatialReplica).UniqueId;
                }

                CallRemoveSpatialReplica(this.uniqueId, this, replica_id);
                this.replicas.Remove(spatialReplica.Guid);
            }
        }
    }
}
