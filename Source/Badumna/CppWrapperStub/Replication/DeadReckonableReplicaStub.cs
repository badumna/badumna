﻿//-----------------------------------------------------------------------
// <copyright file="DeadReckonableReplicaStub.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System.Runtime.CompilerServices;
using Badumna.DataTypes;
using Badumna.SpatialEntities;

namespace Badumna.CppWrapperStub
{
    /// <summary>
    /// The stub for deadreckonable replica. 
    /// </summary>
    /// <exclude/>
    public class DeadReckonableReplicaStub : SpatialReplicaStub, IDeadReckonable
    {
        /// <summary>
        /// The replica velocity. 
        /// </summary>
        private Vector3 velocity;

        /// <summary>
        /// Initializes a new instance of the <see cref="DeadReckonableReplicaStub"/> class.
        /// </summary>
        /// <param name="id">The unique id.</param>
        /// <param name="facade">The network facade object.</param>
        public DeadReckonableReplicaStub(string id, INetworkFacade facade)
            : base(id, facade)
        {
        }

        /// <summary>
        /// Gets or sets the known velocity of the network entity.
        /// </summary>
        public Vector3 Velocity
        {
            get
            {
                return this.velocity;
            }

            set
            {
                SetVelocityProperty(this.UniqueId, value.X, value.Y, value.Z);
                this.velocity = value;
            }
        }

        /// <summary>
        /// Calls the attempt movement.
        /// </summary>
        /// <param name="uniqueId">Replica's unique id.</param>
        /// <param name="x">The x value of the position.</param>
        /// <param name="y">The y value of the position.</param>
        /// <param name="z">The z value of the position.</param>
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        public static extern void CallAttemptMovement(string uniqueId, float x, float y, float z);

        /// <summary>
        /// Sets the velocity property.
        /// </summary>
        /// <param name="uniqueId">The unique id.</param>
        /// <param name="x">The x value of the velocity.</param>
        /// <param name="y">The y value of the velocity.</param>
        /// <param name="z">The z value of the velocity.</param>
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        public static extern void SetVelocityProperty(string uniqueId, float x, float y, float z);

        /// <summary>
        /// This method if called on remote copies inside the NetworkFacade.ProcessNetworkState() method.
        /// The reckonedPosition is the current estimated position of the controlling entity.
        /// The implementation of this method should check that the new position is valid (i.e. check for collisions etc) 
        /// and apply it if so.
        /// </summary>
        /// <param name="reckonedPosition">The estimated position of the entity</param>
        public void AttemptMovement(Vector3 reckonedPosition)
        {
            CallAttemptMovement(this.UniqueId, reckonedPosition.X, reckonedPosition.Y, reckonedPosition.Z);
        }

        /// <summary>
        /// Sets the velocity.
        /// </summary>
        /// <param name="x">The x value of the velocity.</param>
        /// <param name="y">The y value of the velocity.</param>
        /// <param name="z">The z value of the velocity.</param>
        public void SetVelocity(float x, float y, float z)
        {
            Vector3 new_velocity = new Vector3(x, y, z);
            this.velocity = new_velocity;
        }
    }
}
