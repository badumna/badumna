﻿//-----------------------------------------------------------------------
// <copyright file="DeadReckonableOriginalStub.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System.Runtime.CompilerServices;

using Badumna.DataTypes;
using Badumna.SpatialEntities;

namespace Badumna.CppWrapperStub
{
    /// <summary>
    /// The stub for DeadReckonableOriginal. 
    /// </summary>
    public class DeadReckonableOriginalStub : SpatialOriginalStub, IDeadReckonable
    {
        /// <summary>
        /// The velocity.
        /// </summary>
        private Vector3 velocity;

        /// <summary>
        /// Initializes a new instance of the <see cref="DeadReckonableOriginalStub"/> class.
        /// </summary>
        /// <param name="id">The unique id.</param>
        /// <param name="facade">The network facade object.</param>
        public DeadReckonableOriginalStub(string id, INetworkFacade facade)
            : base(id, facade)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DeadReckonableOriginalStub"/> class.
        /// </summary>
        /// <param name="facade">The network facade object.</param>
        public DeadReckonableOriginalStub(INetworkFacade facade)
            : base(facade)
        {
        }

        /// <summary>
        /// Gets or sets the known velocity of the network entity.
        /// </summary>
        public Vector3 Velocity
        {
            get
            {
                return this.velocity;
            }

            set
            {
                this.velocity = value;
            }
        }

        /// <summary>
        /// Gets the velocity X value. This is for testing purpose. 
        /// </summary>
        public float VelocityX
        {
            get { return this.velocity.X; }
        }

        /// <summary>
        /// Gets the velocity Y value. This is for testing purpose. 
        /// </summary>
        public float VelocityY
        {
            get { return this.velocity.Y; }
        }

        /// <summary>
        /// Gets the velocity Z value. This is for testing purpose. 
        /// </summary>
        public float VelocityZ
        {
            get { return this.velocity.Z; }
        }

        /// <summary>
        /// This method if called on remote copies inside the NetworkFacade.ProcessNetworkState() method.
        /// The reckonedPosition is the current estimated position of the controlling entity.
        /// The implementation of this method should check that the new position is valid (i.e. check for collisions etc) 
        /// and apply it if so.
        /// </summary>
        /// <param name="reckonedPosition">The estimated position of the entity</param>
        public void AttemptMovement(Vector3 reckonedPosition)
        {
            // always ignored. 
        }

        /// <summary>
        /// Sets the velocity.
        /// </summary>
        /// <param name="x">The x value of the velocity.</param>
        /// <param name="y">The y value of the velocity.</param>
        /// <param name="z">The z value of the velocity.</param>
        public void SetVelocity(float x, float y, float z)
        {
            Vector3 v = new Vector3(x, y, z);
            this.velocity = v;
        }
    }
}
