﻿//-----------------------------------------------------------------------
// <copyright file="SpatialOriginalStub.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.CppWrapperStub
{
    using System;
    using System.IO;
    using System.Runtime.CompilerServices;
    using Badumna.DataTypes;
    using Badumna.SpatialEntities;
    using Badumna.Utilities;

    /// <summary>
    /// SpatialOriginalStub is used in C++ wrapper project, so the C# can call the native C++ code.
    /// </summary>
    /// <exclude/>
    public class SpatialOriginalStub : ISpatialOriginal
    {
        /// <summary>
        /// The facade object.
        /// </summary>
        private INetworkFacade facade;

        /// <summary>
        /// A unique id.
        /// </summary>
        private string uniqueId;

        /// <summary>
        /// Local entity position.
        /// </summary>
        private Vector3 position;

        /// <summary>
        /// Local entity radius.
        /// </summary>
        private float radius;

        /// <summary>
        /// Local entity of area of interest radius.
        /// </summary>
        private float areaOfInterestRadius;

        /// <summary>
        /// Local entity id.
        /// </summary>
        private BadumnaId guid;

        /// <summary>
        /// Initializes a new instance of the <see cref="SpatialOriginalStub"/> class.
        /// </summary>
        /// <param name="uniqueId">The unique id</param>
        /// <param name="facade">The network facade object.</param>
        public SpatialOriginalStub(string uniqueId, INetworkFacade facade)
        {
            this.facade = facade;
            this.uniqueId = uniqueId;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SpatialOriginalStub"/> class.
        /// </summary>
        /// <param name="facade">The network facade object.</param>
        public SpatialOriginalStub(INetworkFacade facade)
        {
            this.facade = facade;
            this.uniqueId = string.Empty;
        }

        #region ISpatialEntity Members

        /// <summary>
        /// Gets or sets the local entity position.
        /// </summary>
        public Vector3 Position
        {
            get
            {
                return this.position;
            }

            set
            {
                this.position = value;
            }
        }

        /// <summary>
        /// Gets or sets local entity radius.
        /// </summary>
        public float Radius
        {
            get
            {
                return this.radius;
            }

            set
            {
                this.radius = value;
            }
        }

        /// <summary>
        /// Gets or sets local entity area of interest radius.
        /// </summary>
        public float AreaOfInterestRadius
        {
            get
            {
                return this.areaOfInterestRadius;
            }

            set
            {
                this.areaOfInterestRadius = value;
            }
        }

        #endregion

        /// <summary>
        /// Gets the X axis position.
        /// </summary>
        /// <value>The position X.</value>
        public float PositionX
        {
            get { return this.position.X; }
        }

        /// <summary>
        /// Gets the Y axis position.
        /// </summary>
        /// <value>The position Y.</value>
        public float PositionY
        {
            get { return this.position.Y; }
        }

        /// <summary>
        /// Gets the Z axis position.
        /// </summary>
        /// <value>The position Z.</value>
        public float PositionZ
        {
            get { return this.position.Z; }
        }

        #region IEntity Members

        /// <summary>
        /// Gets or sets local entity id.
        /// </summary>
        public BadumnaId Guid
        {
            get { return this.guid; }
            set { this.guid = value; }
        }

        /// <summary>
        /// Gets the unique id.
        /// </summary>
        /// <value>
        /// The unique id.
        /// </value>
        public string UniqueId
        {
            get { return this.uniqueId; }
        }

        /// <summary>
        /// Serialize method in C++ code.
        /// </summary>
        /// <param name="uniqueId">Unique id.</param>
        /// <param name="requiredParts">Required parts.</param>
        /// <returns>The serialized data.</returns>
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        public static extern byte[] CallSerialize(string uniqueId, BooleanArray requiredParts);

        /// <summary>
        /// Handle event method in C++ code.
        /// </summary>
        /// <param name="uniqueId">Unique id.</param>
        /// <param name="data">The data byte array.</param>
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        public static extern void CallOriginalHandleEvent(string uniqueId, byte[] data);

        /// <summary>
        /// Handle incoming messages.
        /// </summary>
        /// <param name="stream">Badumna stream.</param>
        public void HandleEvent(Stream stream)
        {
            BinaryReader reader = new BinaryReader(stream);
            CallOriginalHandleEvent(this.uniqueId, reader.ReadBytes((int)stream.Length));
        }

        /// <summary>
        /// Sets the unique id.
        /// </summary>
        /// <param name="id">The unique id.</param>
        public void SetUniqueId(string id)
        {
            this.uniqueId = id;
        }

        /// <summary>
        /// Sends the custom message to remote copies.
        /// </summary>
        /// <param name="data">The event data.</param>
        public void SendCustomMessageToRemoteCopies(byte[] data)
        {
            MemoryStream stream = new MemoryStream();
            using (BinaryWriter writer = new BinaryWriter(stream))
            {
                writer.Write(data);
            }

            this.facade.SendCustomMessageToRemoteCopies(this, stream);
        }

        #endregion

        #region ISpatialOriginal Members

        /// <summary>
        /// Serialize callback function will be called if update(s) required to be sent via Badumna.
        /// </summary>
        /// <param name="requiredParts">Required parts.</param>
        /// <param name="stream">Badumna stream.</param>
        public void Serialize(BooleanArray requiredParts, Stream stream)
        {
            byte[] data = null;
            using (BinaryWriter writer = new BinaryWriter(stream))
            {
                data = CallSerialize(this.uniqueId, requiredParts);

                if (data != null)
                {
                    writer.Write(data);
                }
            }
        }

        #endregion

        /// <summary>
        /// Sets the position.
        /// </summary>
        /// <param name="x">The x value.</param>
        /// <param name="y">The y value.</param>
        /// <param name="z">The z value.</param>
        public void SetPosition(float x, float y, float z)
        {
            Vector3 new_position = new Vector3(x, y, z);
            this.position = new_position;
        }
    }
}
