﻿//-----------------------------------------------------------------------
// <copyright file="SpatialReplicaStub.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.CppWrapperStub
{
    using System;
    using System.IO;
    using System.Runtime.CompilerServices;
    using Badumna.DataTypes;
    using Badumna.SpatialEntities;
    using Badumna.Utilities;

    /// <summary>
    /// SpatialReplicaStub is used in C++ wrapper project, so the C# can call the native C++ code.
    /// </summary>
    /// <exclude/>
    public class SpatialReplicaStub : ISpatialReplica
    {
        /// <summary>
        /// The facade object.
        /// </summary>
        private INetworkFacade facade;

        /// <summary>
        /// A unique id.
        /// </summary>
        private string uniqueId;

        /// <summary>
        /// Local entity position.
        /// </summary>
        private Vector3 position;

        /// <summary>
        /// Local entity radius.
        /// </summary>
        private float radius;

        /// <summary>
        /// Local entity of area of interest radius.
        /// </summary>
        private float areaOfInterestRadius;

        /// <summary>
        /// Local entity id.
        /// </summary>
        private BadumnaId guid;

        /// <summary>
        /// Initializes a new instance of the <see cref="SpatialReplicaStub"/> class.
        /// </summary>
        /// <param name="uniqueId">Unique id.</param>
        /// <param name="facade">The network facade object.</param>
        public SpatialReplicaStub(string uniqueId, INetworkFacade facade)
        {
            this.facade = facade;
            this.uniqueId = uniqueId;
        }

        /// <summary>
        /// Gets the unique id.
        /// </summary>
        public string UniqueId
        {
            get { return this.uniqueId; }
        }

        #region ISpatialEntity Members

        /// <summary>
        /// Gets or sets the local entity position.
        /// </summary>
        public Vector3 Position
        {
            get
            {
                return this.position;
            }

            set
            {
                SetPositionProperty(this.uniqueId, (int)SpatialEntityStateSegment.Position, value.X, value.Y, value.Z);
                this.position = value;
            }
        }

        /// <summary>
        /// Gets or sets local entity radius.
        /// </summary>
        public float Radius
        {
            get
            {
                return this.radius;
            }

            set
            {
                SetProperty(this.uniqueId, (int)SpatialEntityStateSegment.Radius, value);
                this.radius = value;
            }
        }

        /// <summary>
        /// Gets or sets local entity area of interest radius.
        /// </summary>
        public float AreaOfInterestRadius
        {
            get
            {
                return this.areaOfInterestRadius;
            }

            set
            {
                SetProperty(this.uniqueId, (int)SpatialEntityStateSegment.InterestRadius, value);
                this.areaOfInterestRadius = value;
            }
        }

        #endregion

        #region IEntity Members

        /// <summary>
        /// Gets or sets local entity id.
        /// </summary>
        public BadumnaId Guid
        {
            get { return this.guid; }
            set { this.guid = value; }
        }

        #endregion

        /// <summary>
        /// Deserialize method in C++ code.
        /// </summary>
        /// <param name="uniqueId">Unique id.</param>
        /// <param name="includedParts">Included parts.</param>
        /// <param name="bytes">bytes data.</param>
        /// <param name="estimatedMillisecondsSinceDeparture">Estimated in milliseconds since departure.</param>
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        public static extern void CallDeserialize(
            string uniqueId,
            BooleanArray includedParts,
            byte[] bytes,
            int estimatedMillisecondsSinceDeparture);

        /// <summary>
        /// Handle event method in c++ code.
        /// </summary>
        /// <param name="uniqueId">Unique id.</param>
        /// <param name="data">The data byte array.</param>
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        public static extern void CallReplicaHandleEvent(string uniqueId, byte[] data);

        /// <summary>
        /// Set the property value on the C++ code.
        /// </summary>
        /// <param name="uniqueId">Unique id.</param>
        /// <param name="propertyIndex">The property index that is required to be set</param>
        /// <param name="value">The value of the property.</param>
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        public static extern void SetProperty(string uniqueId, int propertyIndex, object value);

        /// <summary>
        /// Sets the position property.
        /// </summary>
        /// <param name="uniqueId">The unique id.</param>
        /// <param name="propertyIndex">Index of the property.</param>
        /// <param name="x">The x value.</param>
        /// <param name="y">The y value.</param>
        /// <param name="z">The z value.</param>
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        public static extern void SetPositionProperty(string uniqueId, int propertyIndex, float x, float y, float z);

        /// <summary>
        /// Handle incoming messages.
        /// </summary>
        /// <param name="stream">Badumna stream.</param>
        public void HandleEvent(Stream stream)
        {
            BinaryReader reader = new BinaryReader(stream);
            CallReplicaHandleEvent(this.uniqueId, reader.ReadBytes((int)stream.Length));
        }

        #region ISpatialReplica Members

        /// <summary>
        /// Deserialize callback function will be called if update(s) recevied from its original.
        /// </summary>
        /// <param name="includedParts">Included parts.</param>
        /// <param name="stream">Badumna stream.</param>
        /// <param name="estimatedMillisecondsSinceDeparture">Estimated in milliseconds since departure.</param>
        public void Deserialize(BooleanArray includedParts, Stream stream, int estimatedMillisecondsSinceDeparture)
        {
            if (!includedParts.Any())
            {
                return;
            }

            BinaryReader reader = new BinaryReader(stream);
            CallDeserialize(
                this.uniqueId, 
                includedParts, 
                reader.ReadBytes((int)stream.Length), 
                estimatedMillisecondsSinceDeparture);
        }

        #endregion

        /// <summary>
        /// Sends the custom message to Original.
        /// </summary>
        /// <param name="data">The event data.</param>
        public void SendCustomMessageToOriginal(byte[] data)
        {
            MemoryStream stream = new MemoryStream();
            using (BinaryWriter writer = new BinaryWriter(stream))
            {
                writer.Write(data);
            }

            this.facade.SendCustomMessageToOriginal(this, stream);
        }

        /// <summary>
        /// Synchronizes the position.
        /// </summary>
        /// <param name="x">The x value of the position.</param>
        /// <param name="y">The y value of the position.</param>
        /// <param name="z">The z value of the position.</param>
        public void SynchronizePosition(float x, float y, float z)
        {
            Vector3 new_position = new Vector3(x, y, z);
            this.position = new_position;
        }
    }
}
