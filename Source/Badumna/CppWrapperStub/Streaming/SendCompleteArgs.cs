﻿//-----------------------------------------------------------------------
// <copyright file="SendCompleteArgs.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using Badumna.DataTypes;

namespace Badumna.CppWrapperStub
{
    /// <summary>
    /// The arguments for the send completion notification. 
    /// </summary>
    /// <exclude/>
    internal class SendCompleteArgs
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SendCompleteArgs"/> class.
        /// </summary>
        public SendCompleteArgs()
        {
        }

        /// <summary>
        /// Gets or sets the name of the tag.
        /// </summary>
        /// <value>
        /// The name of the tag.
        /// </value>
        public string TagName
        { 
            get; set; 
        }

        /// <summary>
        /// Gets or sets the name of the stream.
        /// </summary>
        /// <value>
        /// The name of the stream.
        /// </value>
        public string StreamName
        { 
            get; set; 
        }

        /// <summary>
        /// Gets or sets the destination.
        /// </summary>
        /// <value>
        /// The destination.
        /// </value>
        public BadumnaId Destination
        { 
            get; set; 
        }
    }
}
