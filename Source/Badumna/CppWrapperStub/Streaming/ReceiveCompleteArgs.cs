﻿//-----------------------------------------------------------------------
// <copyright file="ReceiveCompleteArgs.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using Badumna.DataTypes;

namespace Badumna.CppWrapperStub
{
    /// <summary>
    /// The arguments for the receive completion notification. 
    /// </summary>
    /// <exclude/>
    internal class ReceiveCompleteArgs
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ReceiveCompleteArgs"/> class.
        /// </summary>
        public ReceiveCompleteArgs()
        {
        }

        /// <summary>
        /// Gets or sets the name of the tag.
        /// </summary>
        /// <value>
        /// The name of the tag.
        /// </value>
        public string TagName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the name of the stream.
        /// </summary>
        /// <value>
        /// The name of the stream.
        /// </value>
        public string StreamName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the name of the user.
        /// </summary>
        /// <value>
        /// The name of the user.
        /// </value>
        public string UserName
        {
            get;
            set;
        }
    }
}
