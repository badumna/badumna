﻿//-----------------------------------------------------------------------
// <copyright file="StreamControllerStub.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using Badumna.Streaming;

namespace Badumna.CppWrapperStub
{
    /// <summary>
    /// The stub class for stream controller.
    /// </summary>
    /// <exclude/>
    public class StreamControllerStub
    {
        /// <summary>
        /// The actual stream controller
        /// </summary>
        private IStreamController controller;

        /// <summary>
        /// Initializes a new instance of the <see cref="StreamControllerStub"/> class.
        /// </summary>
        /// <param name="controller">The actual controller.</param>
        public StreamControllerStub(IStreamController controller)
        {
            this.controller = controller;
        }

        /// <summary>
        /// Gets the total bytes to be transferred in the streaming operation.
        /// </summary>
        public long BytesTotal 
        {
            get { return this.controller.BytesTotal; }
        }

        /// <summary>
        /// Gets the number of bytes transferred so far.
        /// </summary>
        public long BytesTransfered 
        {
            get { return this.controller.BytesTransfered; }
        }

        /// <summary>
        /// Gets the estimated transfer rate in kilobytes / second.
        /// </summary>
        public double TransferRateKBps 
        {
            get { return this.controller.TransferRateKBps; } 
        }

        /// <summary>
        /// Gets the estimated time required to complete the streaming operation.
        /// </summary>
        public int EstimatedTimeRemaining 
        {
            get { return (int)this.controller.EstimatedTimeRemaining.TotalSeconds; }
        }

        /// <summary>
        /// Gets the current state of the streaming operation.
        /// </summary>
        public StreamState CurrentState 
        {
            get { return this.controller.CurrentState; } 
        }

        /// <summary>
        /// Gets a value indicating whether the streaming operation has completed successfully.
        /// </summary>
        public bool WasSuccessful 
        {
            get { return this.controller.WasSuccessful; } 
        }

        /// <summary>
        /// Cancels the operation.
        /// </summary>
        public void Cancel()
        {
            this.controller.Cancel();
        }
    }
}
