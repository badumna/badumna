﻿//-----------------------------------------------------------------------
// <copyright file="StreamingManagerStub.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Runtime.CompilerServices;
using Badumna.DataTypes;
using Badumna.Streaming;

namespace Badumna.CppWrapperStub
{
    /// <summary>
    /// A stub class for streaming manager. 
    /// </summary>
    /// <exclude/>
    public class StreamingManagerStub
    {
        /// <summary>
        /// The actual manager.
        /// </summary>
        private StreamingManager manager;

        /// <summary>
        /// Initializes a new instance of the <see cref="StreamingManagerStub"/> class.
        /// </summary>
        /// <param name="manager">The streaming manager.</param>
        public StreamingManagerStub(StreamingManager manager)
        {
            this.manager = manager;
        }

        /// <summary>
        /// Calls the stream send complete.
        /// </summary>
        /// <param name="tag">The tag name.</param>
        /// <param name="name">The stream name.</param>
        /// <param name="destination">The destination id.</param>
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        public static extern void CallStreamSendComplete(string tag, string name, BadumnaId destination);

        /// <summary>
        /// Calls the stream request handler.
        /// </summary>
        /// <param name="args">The StreamRequestStub instance containing the event data.</param>
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        public static extern void CallStreamRequestHandler(StreamRequestStub args);

        /// <summary>
        /// Begins the send reliable file.
        /// </summary>
        /// <param name="stream_tag">The stream tag.</param>
        /// <param name="full_path">The full path.</param>
        /// <param name="destination">The destination id.</param>
        /// <param name="username">The user name.</param>
        /// <returns>The controller stub.</returns>
        public StreamControllerStub BeginSendReliableFile(
            string stream_tag,
            string full_path,
            BadumnaId destination,
            string username)
        {
            SendCompleteArgs args = new SendCompleteArgs();
            args.TagName = stream_tag;
            args.StreamName = full_path;
            args.Destination = destination;

            IStreamController controller = this.manager.BeginSendReliableFile(
                stream_tag, 
                full_path, 
                destination, 
                username, 
                this.FileSendComplete, 
                args);

            return new StreamControllerStub(controller);
        }

        /// <summary>
        /// Subscribes to receive stream requests.
        /// </summary>
        /// <param name="streamTag">The stream tag.</param>
        public void SubscribeToReceiveStreamRequests(string streamTag)
        {
            this.manager.SubscribeToReceiveStreamRequests(streamTag, this.StreamingRequestHandler);
        }

        /// <summary>
        /// Files the send complete.
        /// </summary>
        /// <param name="ar">The argument.</param>
        private void FileSendComplete(IAsyncResult ar)
        {
            SendCompleteArgs args = (SendCompleteArgs)ar.AsyncState;
            CallStreamSendComplete(args.TagName, args.StreamName, args.Destination);
        }

        /// <summary>
        /// The streaming request handler.
        /// </summary>
        /// <param name="sender">The sender of the request.</param>
        /// <param name="args">The <see cref="Badumna.Streaming.ReceiveStreamEventArgs"/> instance containing the event
        /// data.</param>
        private void StreamingRequestHandler(object sender, ReceiveStreamEventArgs args)
        {
            CallStreamRequestHandler(new StreamRequestStub(args));
        }
    }
}
