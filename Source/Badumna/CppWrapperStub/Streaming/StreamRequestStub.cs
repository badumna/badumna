﻿//-----------------------------------------------------------------------
// <copyright file="StreamRequestStub.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Runtime.CompilerServices;
using Badumna.Streaming;

namespace Badumna.CppWrapperStub
{
    /// <summary>
    /// The stub class for ReceiveStreamEventArgs.
    /// </summary>
    /// <exclude/>
    public class StreamRequestStub
    {
        /// <summary>
        /// The actual event args.
        /// </summary>
        private ReceiveStreamEventArgs args;

        /// <summary>
        /// Initializes a new instance of the <see cref="StreamRequestStub"/> class.
        /// </summary>
        /// <param name="args">The <see cref="Badumna.Streaming.ReceiveStreamEventArgs"/> instance containing the event
        /// data.</param>
        public StreamRequestStub(ReceiveStreamEventArgs args)
        {
            this.args = args;
        }

        /// <summary>
        /// Gets the stream tag.
        /// </summary>
        public string StreamTag 
        {
            get { return this.args.StreamTag; } 
        }

        /// <summary>
        /// Gets the name of the stream.
        /// </summary>
        /// <value>
        /// The name of the stream.
        /// </value>
        public string StreamName 
        {
            get { return this.args.StreamName; }
        }

        /// <summary>
        /// Gets the size in kilo bytes.
        /// </summary>
        public double SizeKiloBytes 
        { 
            get { return this.args.SizeKiloBytes; } 
        }

        /// <summary>
        /// Gets the name of the user.
        /// </summary>
        /// <value>
        /// The name of the user.
        /// </value>
        public string UserName 
        { 
            get { return this.args.UserName; } 
        }

        /// <summary>
        /// Gets a value indicating whether this instance is accepted.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is accepted; otherwise, <c>false</c>.
        /// </value>
        public bool IsAccepted 
        { 
            get { return this.args.IsAccepted; } 
        }

        /// <summary>
        /// Calls the stream send complete.
        /// </summary>
        /// <param name="tag">The tag name.</param>
        /// <param name="name">The stream name.</param>
        /// <param name="userName">Name of the user.</param>
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        public static extern void CallStreamReceiveComplete(string tag, string name, string userName);

        /// <summary>
        /// Rejects the request.
        /// </summary>
        public void Reject()
        {
            this.args.Reject();
        }

        /// <summary>
        /// Accepts the file.
        /// </summary>
        /// <param name="pathName">Name of the path.</param>
        /// <returns>The controller stub.</returns>
        public StreamControllerStub AcceptFile(string pathName)
        {
            ReceiveCompleteArgs complete_args = new ReceiveCompleteArgs();
            complete_args.StreamName = this.StreamName;
            complete_args.TagName = this.StreamTag;
            complete_args.UserName = this.UserName;

            IStreamController controller = this.args.AcceptFile(pathName, this.FileReceiveComplete, complete_args);
            return new StreamControllerStub(controller);
        }

        /// <summary>
        /// Files the send complete.
        /// </summary>
        /// <param name="ar">The argument.</param>
        private void FileReceiveComplete(IAsyncResult ar)
        {
            ReceiveCompleteArgs receive_args = (ReceiveCompleteArgs)ar.AsyncState;
            CallStreamReceiveComplete(receive_args.TagName, receive_args.StreamName, receive_args.UserName);
        }
    }
}
