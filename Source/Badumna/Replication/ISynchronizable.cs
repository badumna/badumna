﻿//------------------------------------------------------------------------------
// <copyright file="ISynchronizable.cs" company="National ICT Australia Limited">
//     Copyright (c) 2013 National ICT Australia Limited. All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

namespace Badumna.Replication
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// Identifies classes that implement the Synchronize method.
    /// </summary>
    /// <remarks>
    /// TODO: Figure out exactly what this is for and document it properly.
    /// </remarks>
    internal interface ISynchronizable
    {
        /// <summary>
        /// FIXME:
        /// Consider the situation where there are two original entities (an NPC and a player
        /// controlled avatar) on a peer called A. Let's say both of them see a replica R and R is
        /// keep getting updated. At some point, the NPC is no longer interested in R because R is
        /// no longer within its AOI. Clearly the IReplica (which is a SpatialReplicaCollection) R
        /// should still be kept on A as the player controlled avatar is still interested in R, what
        /// we need to do is somehow pass some sort of information to the IReplica and let it remove
        /// the ISpatialReplica(s) that are no longer required. In the above described case, we want 
        /// the ISpatialReplica created by the NPC to be removed.
        /// Synchronize(Object obj) is used to pass such extra information to the IReplica. I want
        /// to make it general enough to be able to handle different situations so an Object object
        /// is passed to the IReplica.
        /// To some extent, this is like a hack. It would be great if someone can fix it and make
        /// this interface better. 
        /// </summary>
        /// <param name="obj">To do: documentation.</param>
        /// <exclude/>
        void Synchronize(object obj);
    }
}
