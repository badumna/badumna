﻿//-----------------------------------------------------------------------
// <copyright file="OriginalWrapper.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2010 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.Replication
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using Badumna.Core;
    using Badumna.DataTypes;
    using Badumna.Transport;
    using Badumna.Utilities;
    using Badumna.Utilities.Logging;

    /// <summary>
    /// To do: documentation.
    /// </summary>
    internal class OriginalWrapper : IOriginalWrapper
    {
        /// <summary>
        /// The entity manager.
        /// </summary>
        private IEntityManager entityManager;
        
        /// <summary>
        /// To do: documentation.
        /// </summary>
        private InterestedPeerList interestedPeerList;
        
        /// <summary>
        /// To do: documentation.
        /// </summary>
        private CyclicalID.UShortID stateSequenceNumber;
        
        /// <summary>
        /// To do: documentation.
        /// </summary>
        private BooleanArray changedStateSegments;
        
        /// <summary>
        /// To do: documentation.
        /// </summary>
        private TimeSpan nextSendTime;


        // TODO:
        // The localInterestedEntities list should be managed by the InterestedPeerList class. 

        /// <summary>
        /// To do: documentation.
        /// </summary>
        private List<BadumnaId> localInterestedEntities = new List<BadumnaId>();
        
        /// <summary>
        /// To do: documentation.
        /// </summary>
        private CyclicalID.UShortID customMessageModificationNumber = new CyclicalID.UShortID();

        /// <summary>
        /// To do: documentation.
        /// </summary>
        private Queue<CustomRequest> customMessageRequestQueue = new Queue<CustomRequest>();

        /// <summary>
        /// To do: documentation.
        /// </summary>
        private UpdateResendManager resendManager;

        /// <summary>
        /// To do: documentation.
        /// </summary>
        private bool forceUpdate;

        /// <summary>
        /// The queue to use for scheduling events.
        /// </summary>
        private NetworkEventQueue eventQueue;

        /// <summary>
        /// Provides access to the current time.
        /// </summary>
        private ITime timeKeeper;

        /// <summary>
        /// Provides the public peer address.
        /// </summary>
        private INetworkAddressProvider addressProvider;

        /// <summary>
        /// Reports on network connectivity status.
        /// </summary>
        private INetworkConnectivityReporter connectivityReporter;

        /// <summary>
        /// Wraps a memory stream as a custom request.
        /// </summary>
        private struct CustomRequest
        {
            /// <summary>
            /// The request data.
            /// </summary>
            public MemoryStream Data;

            /// <summary>
            /// Initializes a new instance of the OriginalWrapper.CustomRequest struct.
            /// </summary>
            /// <param name="data">The request data.</param>
            public CustomRequest(MemoryStream data)
            {
                this.Data = data;
            }
        }

        /// <summary>
        /// Initializes a new instance of the OriginalWrapper class.
        /// </summary>
        /// <param name="entityManager">To do: entity manager documentaion.</param>
        /// <param name="localEntity">The original being wrapped.</param>
        /// <param name="entityType">The ID of the type of entity beign wrapped.</param>
        /// <param name="overloadOptions">The configuration options for the overload system.</param>
        /// <param name="eventQueue">The queue to use for scheduling events.</param>
        /// <param name="timeKeeper">Provides access to the current time.</param>
        /// <param name="connectionTable">The connection table.</param>
        /// <param name="addressProvider">Provides the public peer address.</param>
        /// <param name="connectivityReporter">Reports on network connectivity status.</param>
        public OriginalWrapper(
            IEntityManager entityManager,
            IOriginal localEntity,
            EntityTypeId entityType,
            OverloadModule overloadOptions,
            NetworkEventQueue eventQueue,
            ITime timeKeeper,
            IChannelFunnel<TransportEnvelope> connectionTable,
            INetworkAddressProvider addressProvider,
            INetworkConnectivityReporter connectivityReporter)
        {
            this.eventQueue = eventQueue;
            this.timeKeeper = timeKeeper;
            this.addressProvider = addressProvider;
            this.connectivityReporter = connectivityReporter;

            this.entityManager = entityManager;
            this.LocalEntity = localEntity;
            this.nextSendTime = this.timeKeeper.Now + Parameters.MinimumUpdateRate;

            this.EntityType = entityType;
            this.changedStateSegments = new BooleanArray();
            this.resendManager = new UpdateResendManager(entityManager, this, this.timeKeeper);
            this.interestedPeerList = new InterestedPeerList(entityManager, localEntity.Guid, overloadOptions, this.eventQueue, this.timeKeeper, connectivityReporter, connectionTable);
        }

        /// <summary>
        /// Gets the original entity.
        /// </summary>
        public IOriginal LocalEntity
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the entity type (represented by a type ID).
        /// </summary>
        public EntityTypeId EntityType
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets a value indicating whether an update is required to be sent.
        /// </summary>
        public bool IsUpdateRequired
        {
            get
            {
                return this.changedStateSegments.Any() ||
                       this.MinimumUpdateTimeHasElapsed ||
                       this.forceUpdate;
            }
        }

        /// <summary>
        /// Gets the sequence number of update generated by most recent call to GetUpdate.
        /// i.e. Should be referenced *after* call to GetUpdate.
        /// </summary>
        public CyclicalID.UShortID StateSequenceNumber
        {
            get { return this.stateSequenceNumber; }
        }

        /// <summary>
        /// Gets a value indicating whether ... to do: documentation.
        /// </summary>
        public bool ForwardingPeerAvailable
        {
            get { return this.interestedPeerList.ForwardingPeerAvailable; }
        }

        /// <summary>
        /// Gets ... to do: documentation.
        /// </summary>
        public List<BadumnaId> LocalInterestedEntities
        {
            get { return this.localInterestedEntities; }
        }

        /// <summary>
        /// Gets a value indicating whether the ... to do.
        /// </summary>
        private bool MinimumUpdateTimeHasElapsed
        {
            get { return this.timeKeeper.Now > this.nextSendTime; } 
        }

        /// <summary>
        /// To do: documentation.
        /// </summary>
        /// <param name="removeReplicas">A value indicating whether replicas should be removed.</param>
        public void ShutDown(bool removeReplicas)
        {
            if (removeReplicas)
            {
                EntityEnvelope envelope = new EntityEnvelope(this.LocalEntity.Guid, QualityOfService.Reliable);
                envelope.Qos.Priority = QosPriority.High;
                this.entityManager.RemoteCallRemoveEntity(envelope, this.LocalEntity.Guid);
                this.interestedPeerList.DispatchToInterested(envelope);

                foreach (BadumnaId id in this.localInterestedEntities)
                {
                    this.eventQueue.Push(this.entityManager.RemoveEntity, id);
                }
            }

            this.localInterestedEntities.Clear();
            this.interestedPeerList.Shutdown();
            this.interestedPeerList = null;
        }

        /// <summary>
        /// To do: documentation.
        /// </summary>
        /// <param name="address">The address of the new forwarding peer.</param>
        public void SwitchToNewForwardingPeer(PeerAddress address)
        {
            this.interestedPeerList.SwitchToNewForwardingPeer(address);
        }

        /// <summary>
        /// To do: documentation.
        /// </summary>
        /// <param name="entity">To do: entity documentation.</param>
        public void AddInterested(BadumnaId entity)
        {
            if (entity.Address.Equals(this.addressProvider.PublicAddress))
            {
                // the interested entity is on my local peer
                if (!this.localInterestedEntities.Contains(entity))
                {
                    this.localInterestedEntities.Add(entity);
                }
            }
            else
            {
                this.interestedPeerList.AddInterestedEntity(entity);
            }

            Logger.TraceInformation(
                LogTag.Replication,
                "Forcing update of local entity {0} because of new interested remote entity: {1}",
                this.LocalEntity.Guid,
                entity);
            this.forceUpdate = true;
        }

        /// <summary>
        /// To do: documentation.
        /// </summary>
        /// <param name="entity">To do: entity documentation.</param>
        public void RemoveInterested(BadumnaId entity)
        {
            Logger.TraceInformation(LogTag.Replication, "RemoveInterested is called.");
            
            if (entity.Address.Equals(this.addressProvider.PublicAddress))
            {
                // the interested entity is on my local peer
                if (this.localInterestedEntities.Contains(entity))
                {
                    this.localInterestedEntities.Remove(entity);
                }
            }
            else
            {
                this.interestedPeerList.RemoveInterestedEntity(entity);
            }

            this.resendManager.RemoveUnacknowledgedPeer(entity);

            EntityEnvelope envelope = new EntityEnvelope(entity, this.LocalEntity.Guid, QualityOfService.Reliable);
            envelope.Qos.Priority = QosPriority.High;
            this.entityManager.RemoteCallRemoveEntityWithKnownAssociatedOriginal(envelope, this.LocalEntity.Guid, entity);
        }

        /// <summary>
        /// Returns a count of the number of interested peers.
        /// </summary>
        /// <returns>A count of the number of interested peers.</returns>
        public int InterestedCount()
        {
            return this.interestedPeerList.InterestedCount;
        }

        /// <summary>
        /// To do: documentation.
        /// </summary>
        /// <returns>To do: return value documentation.</returns>
        public List<Pair<BadumnaId, BadumnaId>> GetTimeOutPeers()
        {
            return this.interestedPeerList.GetTimeOutPeers();
        }

        /// <summary>
        /// Indiciate which (additional) parts of the entity require updates to be sent.
        /// </summary>
        /// <param name="changedParts">The parts of the entity that have changed.</param>
        public void MarkForUpdate(BooleanArray changedParts)
        {
            this.changedStateSegments.Or(changedParts);
        }

        /// <summary>
        /// Indicate that a part of the entity required updates to be sent.
        /// </summary>
        /// <param name="changedPartIndex">The index of the part that has changed.</param>
        public void MarkForUpdate(int changedPartIndex)
        {
            this.changedStateSegments[changedPartIndex] = true;
        }

        /// <summary>
        /// Get a sequence number for the next custom message.
        /// </summary>
        /// <returns>The sequence number.</returns>
        public CyclicalID.UShortID GetNextCustomMessageNumber()
        {
            this.customMessageModificationNumber.Increment();
            return this.customMessageModificationNumber;
        }

        /// <summary>
        /// Queue a custom message for sending.
        /// </summary>
        /// <param name="data">The custom message data.</param>
        public void QueueCustomMessageRequest(MemoryStream data)
        {
            Logger.TraceInformation(LogTag.Replication, "queue custom message request");
            this.customMessageRequestQueue.Enqueue(new CustomRequest(data));
        }

        /// <summary>
        /// To do: documentation.
        /// </summary>
        public void ProcessCustomMessageRequests()
        {
            while (this.customMessageRequestQueue.Count > 0)
            {
                CustomRequest request = this.customMessageRequestQueue.Dequeue();
                Logger.TraceInformation(LogTag.Replication, "Processing custom message request to {0}", this.LocalEntity.Guid);
                try
                {
                    this.LocalEntity.HandleEvent(request.Data);
                }
                catch (Exception e)
                {
                    Logger.TraceException(LogLevel.Warning, e, "Failed to process custom message request on {0}", this.LocalEntity.Guid);
                }
            }
        }

        /// <summary>
        /// To do: documentation.
        /// </summary>
        /// <param name="source">To do: source documentation.</param>
        /// <param name="maximumUpdateRateBps">To do: maximumUpdateRateBps documentation.</param>
        /// <param name="attention">To do: attention documentation.</param>
        public void ApplyFlowControlFeedback(PeerAddress source, ushort maximumUpdateRateBps, float attention)
        {
            Logger.TraceInformation(
                LogTag.Replication,
                "Peer {0} has allocated {1}B/s bandwidth for updates from {2}. Attention = {3}", 
                source,
                maximumUpdateRateBps,
                this.LocalEntity.Guid,
                attention);
            this.interestedPeerList.ApplyFlowControlFeedback(source, maximumUpdateRateBps, attention);
        }

        /// <summary>
        /// Send a proximity event to interested peers.
        /// </summary>
        /// <param name="eventEnvelope">The envelope containing the event.</param>
        public void SendProximityEvent(EntityEnvelope eventEnvelope)
        {
            this.SendEvent(eventEnvelope);
        }

        /// <summary>
        /// Dispatch updates to interested peers.
        /// </summary>
        /// <param name="sequenceNumber">The sequence number for the update.</param>
        /// <param name="includedParts">An array of values indicating which parts are included in the update.</param>
        /// <param name="update">The update data.</param>
        /// <param name="sourceId">To do: source documentation.</param>
        public void DispatchUpdate(CyclicalID.UShortID sequenceNumber, BooleanArray includedParts, byte[] update, BadumnaId sourceId)
        {
            if (this.connectivityReporter.Status == ConnectivityStatus.Online)
            {
                this.interestedPeerList.ScheduleUpdate(sequenceNumber, includedParts, update, sourceId);
            }
        }

        /// <summary>
        /// Clear the changed state flags for all segments.
        /// </summary>
        public void ClearChangedState()
        {
            if (this.connectivityReporter.Status == ConnectivityStatus.Online)
            {
                this.changedStateSegments.SetAll(false);
            }
        }

        /// <summary>
        /// Send an event to all interested peers.
        /// </summary>
        /// <param name="envelope">The envelope containing the event.</param>
        public void SendEvent(EntityEnvelope envelope)
        {
            foreach (BadumnaId id in this.localInterestedEntities)
            {
                envelope.DestinationEntity = id;
                this.entityManager.Router.DirectSend(envelope);
            }

            this.interestedPeerList.DispatchToInterested(envelope);
        }

        /// <summary>
        /// To do: documentation.
        /// </summary>
        /// <param name="address">To do: address documentation.</param>
        /// <returns>To do: return value documentation.</returns>
        public InterestedPeer GetInterestedPeer(PeerAddress address)
        {
            return this.interestedPeerList.GetInterestedPeer(address);
        }

        /// <summary>
        /// Requests that the LocalEntity serializes the specified parts.
        /// </summary>
        /// <param name="includedParts">Parts that the entity should include.  This parameter can be mutated to
        /// indicate which parts were included.</param>
        /// <param name="remainingParts">Parts which weren't serialized by the current call.  SerializeParts
        /// should be called again to serialized the remaining parts.</param>
        /// <returns>The serialized data.</returns>
        public byte[] SerializeParts(BooleanArray includedParts, out BooleanArray remainingParts)
        {
            this.eventQueue.AssertIsApplicationThread();  // For Serialize(...)

            // !! User's Serialize function must be allowed to mutate the BooleanArray in case they decide they
            //    need to include extra stuff (i.e. in Badumna Position implies Velocity should be included if dead-reckonable).
            Debug.Assert(!(includedParts is ImmutableBooleanArray), "includedParts cannot be immutable.");

            using (MemoryStream inputStream = new MemoryStream())
            {
                remainingParts = this.LocalEntity.Serialize(includedParts, inputStream);
                return inputStream.ToArray();
            }
        }

        /// <summary>
        /// Gets the next update for the entity.
        /// </summary>
        /// <param name="includedParts">null to indicate should send segments which are flagged, or
        /// a BooleanArray instance indicating parts to send.  On exit this parameter indicates
        /// which parts were actually written to the update.</param>
        /// <param name="remainingParts">Output parameter indicating which parts are yet to be written.
        /// Another call should be made to generate an update with these parts.</param>
        /// <returns>The update data</returns>
        public byte[] GetUpdate(ref BooleanArray includedParts, out BooleanArray remainingParts)
        {
            this.stateSequenceNumber.Increment();
            this.nextSendTime = this.timeKeeper.Now + Parameters.MinimumUpdateRate;
            this.forceUpdate = false;
            if (includedParts == null)
            {
                includedParts = new BooleanArray(this.changedStateSegments);
            }

            byte[] update = this.SerializeParts(includedParts, out remainingParts);

            if (includedParts.Any())
            {
                // Update sequence number records.  Done after call to GetUpdate(BooleanArray) because
                // that call may modify which bits are set in the array.
                this.resendManager.UpdateResendEvents(includedParts, this.stateSequenceNumber, this.interestedPeerList.InterestedRegions);
            }

            // Will resend only if required time has elapsed.  Called from here because we
            // must call Serialize from the application thread.
            // TODO: Ensure this doesn't cause too much delay in the resends (they'll be
            //       delayed by Parameters.SegmentResendDelayTime plus however long it takes to
            //       call this function again.
            this.resendManager.ResendUpdates();

            return update;
        }

        /// <summary>
        /// Gets a complete update including all parts. An applications' serialize method may choose
        /// to omit some parts.
        /// </summary>
        /// <param name="includedParts">The parts that were included in the update.</param>
        /// <param name="remainingParts">The parts that were not included in the update.</param>
        /// <returns>The update data.</returns>
        public byte[] GetCompleteUpdate(out BooleanArray includedParts, out BooleanArray remainingParts)
        {
            // This function doesn't do any other book keeping (such us updating mLastChangeSequenceNumber)
            // because it's only called for instantiation.  GetUpdate(out BooleanArray) is called
            // regularly and the updates are distributed to all interested peers, so book keeping only
            // makes sense there.
            includedParts = new BooleanArray(true);
            return this.SerializeParts(includedParts, out remainingParts);
        }

        /// <summary>
        /// To do: documentation.
        /// </summary>
        /// <param name="id">To do: id documentation.</param>
        /// <param name="includedSegments">To do: includedSegments documentation.</param>
        /// <param name="sequenceNumbers">To do: sequeneceNumbers documentation.</param>
        public void ProcessAcknowledgements(BadumnaId id, BooleanArray includedSegments, List<CyclicalID.UShortID> sequenceNumbers)
        {
            this.resendManager.ProcessAcknowledgements(id, includedSegments, sequenceNumbers);
        }

        /// <summary>
        /// Determines whether the original entity is interested in the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns>
        ///     <c>true</c> if the original entity is interested in the specified entity; otherwise, <c>false</c>.
        /// </returns>
        private bool IsInterestedIn(BadumnaId entity)
        {
            if (this.LocalInterestedEntities.Contains(entity))
            {
                return true;
            }

            if (this.interestedPeerList.IsInterestedIn(entity))
            {
                return true;
            }

            return false;
        }
    }
}
