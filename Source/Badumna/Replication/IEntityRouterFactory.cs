﻿//-----------------------------------------------------------------------
// <copyright file="IEntityRouterFactory.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using Badumna.Core;
using Badumna.Transport;

namespace Badumna.Replication
{
    /// <summary>
    /// document this interface.
    /// </summary>
    internal interface IEntityRouterFactory
    {
        /// <summary>
        /// Creates the specified parent.
        /// </summary>
        /// <param name="parent">The parent.</param>
        /// <param name="messageConsumer">The message consumer.</param>
        /// <returns>The entity router object.</returns>
        IEntityRouter Create(TransportProtocol parent, IMessageConsumer<EntityEnvelope> messageConsumer);
    }
}