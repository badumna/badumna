﻿//---------------------------------------------------------------------------------
// <copyright file="OverloadSwitchingManager.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Diagnostics;
using Badumna.Core;
using Badumna.Overload;
using Badumna.Transport;
using Badumna.Utilities;
using Badumna.Utilities.Logging;

namespace Badumna.Replication
{   
    using InterestedPeers = Dictionary<PeerAddress, InterestedPeer>;
   
    /// <summary>
    /// This class is responsible for determine the switching between direct update delivery and delivery through 
    /// overload server. 
    /// </summary>
    internal class OverloadSwitchingManager
    {
        /// <summary>
        /// Update channels.
        /// </summary>
        private UpdateChannelManager updateChannels;

        /// <summary>
        /// Interested peers. 
        /// </summary>
        private InterestedPeers interestedPeers;

        /// <summary>
        /// Overloaded entities.
        /// </summary>
        private List<InterestedEntity> overloadEntities;

        /// <summary>
        /// Provides access to the current time.
        /// </summary>
        private ITime timeKeeper;

        /// <summary>
        /// The configuration options for the overload system.
        /// </summary>
        private OverloadModule overloadOptions;

        /// <summary>
        /// The connection table, used to check drop rate on a channel.
        /// </summary>
        private IChannelFunnel<TransportEnvelope> connectionTable;

        /// <summary>
        /// Initializes a new instance of the <see cref="OverloadSwitchingManager"/> class.
        /// </summary>
        /// <param name="peers">The peers.</param>
        /// <param name="updateChannels">The update channels.</param>
        /// <param name="overloadEntities">The overload entities.</param>
        /// <param name="overloadOptions">The configuration options for the overload system.</param>
        /// <param name="timeKeeper">Provides access to the current time.</param>
        /// <param name="connectionTable">The connection table.</param>
        public OverloadSwitchingManager(
            InterestedPeers peers,
            UpdateChannelManager updateChannels,
            List<InterestedEntity> overloadEntities,
            OverloadModule overloadOptions,
            ITime timeKeeper,
            IChannelFunnel<TransportEnvelope> connectionTable)
        {
            this.interestedPeers = peers;
            this.updateChannels = updateChannels;
            this.overloadEntities = overloadEntities;
            this.timeKeeper = timeKeeper;
            this.overloadOptions = overloadOptions;
            this.connectionTable = connectionTable;
        }

        /// <summary>
        /// Gets the overloaded channels.
        /// </summary>
        /// <returns>A list of overloaded channels.</returns>
        public List<UpdateChannel> GetOverloadedChannel()
        {
            if (this.overloadOptions.IsForced)
            {
                Logger.TraceWarning(LogTag.Replication, "Force Overload is currently set to be true - this should only be used for testing purpose.");
            }

            List<UpdateChannel> overloaded = new List<UpdateChannel>();
            foreach (UpdateChannel channel in this.updateChannels)
            {
                if (channel.Destination.IsDhtAddress)
                {
                    // we can't tell the estimated bandwidth between the local peer and a dht addressed remote peer, 
                    // the this.mEntityManager.GetAvailableBandwidth seems always return Infinity, which means there
                    // is currently no way to tell when underloaded. 
                    continue;
                }

                double dropRate = channel.DropRate;
                double connectionDropRate = this.connectionTable.GetConnectionLossRate(channel.Destination);

                double thresholdMultiplier = this.GetTriggerOverloadDropRateMultiplier(channel.Destination);
                ////Console.WriteLine("drop rate is {0}", dropRate);
                double triggerOnOverloadDropRate = Parameters.TriggerOnOverloadDropRate * thresholdMultiplier;
                double triggerOnOverloadConnectionDropRate = 0.2 * thresholdMultiplier;

                /*
                 *  Checking the connection loss rate here is a HACK!  It should be eliminated,
                 *  along with GetConnectionLossRate method on the IChannelFunnel<T> interface
                 *  and the passing of the connection table instance into this class
                 *  (and owning classes which don't otherwise need it).
                 * 
                 *  The reason this is a hack: if the TCP-friend Rate Control is working properly
                 *  then in the steady state the connection's loss rate should be close to zero.
                 *  If the loss rate is non-zero the assumption is the connection is congested
                 *  and we should lower our sending rate, which should decrease the loss rate.
                 *  
                 *  This hack exists because at the time of the 2.0 release it improved the
                 *  overload switching in test conditions and was a simple change.  
                 *  
                 *  TODO: The TFRC code needs to be investigated to see why it stabilises with a
                 *        significant loss rate.
                 */

                if (dropRate > triggerOnOverloadDropRate ||
                    connectionDropRate > triggerOnOverloadConnectionDropRate ||
                    this.overloadOptions.IsForced)
                {
                    overloaded.Add(channel);
                }
            }

            // Extra check when the MaximumOutboundTrafficBytesPerSecond is set in overload module.
            double excessTraffic = this.connectionTable.GetTotalOutboundTraffic() - (double)this.overloadOptions.OutboundTrafficLimitBytesPerSecond;

            if (excessTraffic > 0)
            {
                foreach (UpdateChannel channel in overloaded)
                {
                    excessTraffic -= channel.GeneratingBytesPerSecond;
                }

                if (excessTraffic > 0)
                {
                    // We need extra channel to be switched to overload.
                    foreach (UpdateChannel channel in this.updateChannels)
                    {
                        if (!overloaded.Contains(channel))
                        {
                            overloaded.Add(channel);
                            excessTraffic -= channel.GeneratingBytesPerSecond;

                            if (excessTraffic < 0)
                            {
                                break;
                            }
                        }
                    }
                }
            }

            return overloaded;
        }

        /// <summary>
        /// Checks for underloaded destinations.
        /// </summary>
        /// <param name="changeToSwitchingBackMethod">Entities need to change to switching back method.</param>
        /// <param name="changeToDirectMethod">Entities need to change to direct method.</param>
        public void GetUnderloaded(out List<InterestedEntity> changeToSwitchingBackMethod, out List<InterestedEntity> changeToDirectMethod)
        {
            TimeSpan now = this.timeKeeper.Now;
            changeToSwitchingBackMethod = new List<InterestedEntity>();
            changeToDirectMethod = new List<InterestedEntity>();

            foreach (InterestedEntity entity in this.overloadEntities)
            {
                if (entity.UpdateMethod == UpdateDeliveryMethod.Direct)
                {
                    Debug.Assert(false, "Overloaded entity is sending direct updates.");
                }
                else if (entity.UpdateMethod == UpdateDeliveryMethod.Overload)
                {
                    if (now - entity.SwitchTime > Parameters.OverloadModeMinDuration)
                    {
                        changeToSwitchingBackMethod.Add(entity);
                    }
                }
                else if (entity.UpdateMethod == UpdateDeliveryMethod.SwitchingBack)
                {
                    if (now - entity.SwitchTime > Parameters.SwitchingBackModeMaxDuration)
                    {
                        changeToDirectMethod.Add(entity);
                    }
                }
            }
        }

        /// <summary>
        /// Gets the trigger overload drop rate.
        /// </summary>
        /// <param name="address">The address.</param>
        /// <returns>The threshold drop rate.</returns>
        private double GetTriggerOverloadDropRateMultiplier(PeerAddress address)
        {
            InterestedPeer peer;
            if (this.interestedPeers.TryGetValue(address, out peer))
            {
                foreach (InterestedEntity entity in peer)
                {
                    if (entity.UpdateMethod == UpdateDeliveryMethod.Direct)
                    {
                        return 1.0;
                    }
                    else if (entity.UpdateMethod == UpdateDeliveryMethod.SwitchingBack)
                    {
                        // when in switching back mode, it is much easier to be considered as overloaded. 
                        return 0.66;
                    }
                    else
                    {
                        Debug.Assert(false, string.Format("Inconsistent update method. delivery method = {0}", entity.UpdateMethod));
                        return 1.0;
                    }
                }
            }
            else
            {
                Debug.Assert(false, "Unknown peer.");
                return 1.0;
            }

            Debug.Assert(false, "Inconsistency in GetTriggerOverloadDropRate");
            return double.PositiveInfinity;
        }
    }
}
