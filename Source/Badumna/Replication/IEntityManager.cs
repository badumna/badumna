﻿//-----------------------------------------------------------------------
// <copyright file="IEntityManager.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.Replication
{
    using System.Collections.Generic;
    using System.IO;
    using Badumna.Core;
    using Badumna.DataTypes;
    using Badumna.Overload;
    using Badumna.ServiceDiscovery;
    using Badumna.Utilities;

    /// <summary>
    /// The IEntityManager interface.
    /// To do: better documentation.
    /// </summary>
    internal interface IEntityManager : IEntityProtocol
    {
        /// <summary>
        /// Gets the forwarding manager.
        /// </summary>
        /// <value>The forwarding manager.</value>
        ForwardingManager ForwardingManager
        {
            get;
        }

        /// <summary>
        /// Gets the entity router.
        /// </summary>
        /// <value>The entity router.</value>
        IEntityRouter Router
        {
            get;
        }

        /// <summary>
        /// Gets or sets the on forwarding peer become offline event handler.
        /// </summary>
        /// <value>The on forwrading peer become offline event handler.</value>
        OnServiceBecomeOffline OnForwradingPeerBecomeOffline
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the associated replica object.
        /// </summary>
        /// <value>The associated replica object.</value>
        AssociatedReplica Association
        {
            get;
        }

        /// <summary>
        /// Acknowledges the segments.
        /// </summary>
        /// <param name="envelope">The envelope.</param>
        /// <param name="segmentsToAck">The segments to ack.</param>
        /// <param name="seqNumbers">The seq numbers.</param>
        void AcknowledgeSegments(EntityEnvelope envelope, BooleanArray segmentsToAck, List<CyclicalID.UShortID> seqNumbers);

        /// <summary>
        /// Adds a entity channel.
        /// </summary>
        /// <param name="peerAddress">The peer address.</param>
        /// <param name="channel">The update channel to be added.</param>
        void AddEntityChannel(PeerAddress peerAddress, UpdateChannel channel);

        /// <summary>
        /// Removes the update channel.
        /// </summary>
        /// <param name="peerAddress">The peer address.</param>
        /// <param name="channel">The channel.</param>
        void RemoveEntityChannel(PeerAddress peerAddress, UpdateChannel channel);

        /// <summary>
        /// Performs a remote call to update entity.
        /// </summary>
        /// <param name="envelope">The envelope.</param>
        /// <param name="sequenceNumber">The sequence number.</param>
        /// <param name="includedParts">The included parts.</param>
        /// <param name="update">The update.</param>
        void RemoteCallUpdateEntity(EntityEnvelope envelope, CyclicalID.UShortID sequenceNumber, BooleanArray includedParts, byte[] update);

        /// <summary>
        /// Performs a remote call to update multiple entities.
        /// </summary>
        /// <param name="envelope">The envelope.</param>
        /// <param name="sequenceNumber">The sequence number.</param>
        /// <param name="includedParts">The included parts.</param>
        /// <param name="update">The update.</param>
        /// <param name="targets">The targets.</param>
        void RemoteCallUpdateMultipleEntities(EntityEnvelope envelope, CyclicalID.UShortID sequenceNumber, BooleanArray includedParts, byte[] update, List<BadumnaId> targets);

        /// <summary>
        /// Performs a remote call to remove entity.
        /// </summary>
        /// <param name="envelope">The envelope.</param>
        /// <param name="id">The entity id.</param>
        void RemoteCallRemoveEntity(EntityEnvelope envelope, BadumnaId id);

        /// <summary>
        /// Performs a remote call to remove entity with known associated original id.
        /// </summary>
        /// <param name="envelope">The envelope.</param>
        /// <param name="originalId">The original id.</param>
        /// <param name="id">The entity id.</param>
        void RemoteCallRemoveEntityWithKnownAssociatedOriginal(EntityEnvelope envelope, BadumnaId originalId, BadumnaId id);

        /// <summary>
        /// Updates the entity.
        /// </summary>
        /// <param name="envelope">The envelope.</param>
        /// <param name="sequenceNumber">The sequence number.</param>
        /// <param name="includedParts">The included parts.</param>
        /// <param name="update">The update.</param>
        void UpdateEntity(EntityEnvelope envelope, CyclicalID.UShortID sequenceNumber, BooleanArray includedParts, byte[] update);

        /// <summary>
        /// Removes the entity.
        /// </summary>
        /// <param name="id">The entity id.</param>
        void RemoveEntity(BadumnaId id);

        /// <summary>
        /// Sends the proximity message.
        /// </summary>
        /// <param name="entityId">The entity id.</param>
        /// <param name="envelope">The envelope.</param>
        void SendProximityMessage(BadumnaId entityId, EntityEnvelope envelope);

        /// <summary>
        /// Register create and remove handlers for an entity group.
        /// </summary>
        /// <param name="groupId">The unique id identifying the group</param>
        /// <param name="createReplica">The delegate to be called when a new replica arrives</param>
        /// <param name="removeReplica">The delegate to be called when a replica is to be removed</param>
        void Register(byte groupId, CreateReplica createReplica, RemoveReplica removeReplica);

        /// <summary>
        /// Register an IOriginal to be replicated
        /// </summary>
        /// <remarks>
        /// Must be called only from event thread
        /// </remarks>
        /// <param name="localEntity">The entity to be replicated</param>
        /// <param name="entityType">The type of the entity.  This will be passed to the CreateReplica delegate so that it can instantiate the appropriate class.</param>
        void RegisterEntity(IOriginal localEntity, EntityTypeId entityType);

        /// <summary>
        /// Unregisters the IOriginal.
        /// </summary>
        /// <remarks>
        /// Unlike RegisterEntity there is no guarantee that ProcessNetworkState() will be called again after the application
        /// unregisters its entities (it could be shutting down).  Thread safety is maintained by suspending the network event queue
        /// within this function.
        /// </remarks>
        /// <param name="localEntity">The entity to be unregistered.</param>
        /// <param name="removeReplicas">Remove the associated replicas.</param>
        void UnregisterEntity(IOriginal localEntity, bool removeReplicas);

        /// <summary>
        /// Gets the time out peers.
        /// </summary>
        /// <returns>A list of time out peers.</returns>
        List<Pair<BadumnaId, BadumnaId>> GetTimeOutPeers();

        /// <summary>
        /// Gets the interested peer.
        /// </summary>
        /// <param name="originalId">The unique ID of the original entity.</param>
        /// <param name="address">The address.</param>
        /// <returns>The interested peer object.</returns>
        InterestedPeer GetInterestedPeer(BadumnaId originalId, PeerAddress address);

        /// <summary>
        /// Add interested entity of the IOriginal.
        /// </summary>
        /// <param name="original">The IOriginal</param>
        /// <param name="region">Unique ID of the interested entity.</param>
        void AddInterested(IOriginal original, BadumnaId region);

        /// <summary>
        /// Remove interested entity of the IOriginal.
        /// </summary>
        /// <param name="original">The IOriginal.</param>
        /// <param name="region">Unique ID of the interested entity.</param>
        void RemoveInterested(IOriginal original, BadumnaId region);

        /// <summary>
        /// Gets the interested count of the IOriginal.
        /// </summary>
        /// <param name="original">The IOriginal.</param>
        /// <returns>The interested count.</returns>
        int InterestedCount(IOriginal original);

        /// <summary>
        /// Marks for update.
        /// </summary>
        /// <param name="localEntity">Entity to update.</param>
        /// <param name="changedParts">The changed parts.</param>
        void MarkForUpdate(IOriginal localEntity, BooleanArray changedParts);

        /// <summary>
        /// Marks for update.
        /// </summary>
        /// <param name="localEntity">Entity to update.</param>
        /// <param name="changedPartIndex">Index of the changed part.</param>
        void MarkForUpdate(IOriginal localEntity, int changedPartIndex);

        /// <summary>
        /// Send a custom message to a replica entity.
        /// </summary>
        /// <param name="localEntity">Original entity whose replica should receive the message.</param>
        /// <param name="messageData">Custom message.</param>
        void SendCustomMessageToReplicas(IOriginal localEntity, MemoryStream messageData);

        /// <summary>
        /// Send a custom message to an original entity.
        /// </summary>
        /// <param name="remoteEntity">Remote entity whose original should receive the message.</param>
        /// <param name="messageData">Custom message.</param>
        void SendCustomMessageToOriginal(IReplica remoteEntity, MemoryStream messageData);

        /// <summary>
        /// Do periodic tidying.
        /// </summary>
        void CollectGarbage();
    }
}
