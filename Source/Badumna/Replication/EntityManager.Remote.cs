﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

using Badumna.Core;
using Badumna.DataTypes;
using Badumna.Utilities;
using Badumna.Utilities.Logging;

namespace Badumna.Replication
{
    partial class EntityManager
    {
        private class UpdateInformation
        {
            public BadumnaId EntityId;
            public BadumnaId OriginalEntityId;
            public CyclicalID.UShortID SequenceNumber;
            public BooleanArray IncludedParts;
            public byte[] Update;
            public ushort EstimatedDelayMilliseconds;

            public UpdateInformation(BadumnaId entityId, BadumnaId originalEntityId, ushort estimatedDelayMilliseconds, CyclicalID.UShortID sequenceNumber,
                BooleanArray includedParts, byte[] update)
            {
                this.EntityId = entityId;
                this.OriginalEntityId = originalEntityId;
                this.EstimatedDelayMilliseconds = estimatedDelayMilliseconds;
                this.SequenceNumber = sequenceNumber;
                this.IncludedParts = new BooleanArray(includedParts);
                this.Update = update;
            }
        }

        private List<UpdateInformation> mUpdateQueue;
        private Queue<LambdaFunction> mEventQueue;

        private Set<Pair<BadumnaId, BadumnaId>> mUpdateLog;

        /// <summary>
        /// Cache for custom messages that arrive before a replica is instantiated.
        /// </summary>
        private CustomMessageCache customMessageCache = new CustomMessageCache();

        // Executed in application thread!
        public void ProcessInboundUpdates()
        {
            // Process arrival, departure and chat events.
            while (0 < this.mEventQueue.Count)
            {
                this.mEventQueue.Dequeue().Invoke();
            }

            // Process custom messages
            foreach (ReplicaWrapper wrapper in this.replicaWrappers.Values)
            {
                wrapper.ProcessCustomMessages();
            }

            // Process custom message requests
            foreach (OriginalWrapper wrapper in this.originalWrappers.Values)
            {
                wrapper.ProcessCustomMessageRequests();
            }

            // Process update messages.
            lock (this.mUpdateQueue)
            {
                foreach (UpdateInformation updateInfo in this.mUpdateQueue)
                {
                    this.RecordUpdateTime(updateInfo.EntityId, updateInfo.OriginalEntityId);

                    // Default attention value non-zero because if we have no info on the attention
                    // we should try to instantiate it to get more info rather than ignoring it altogether.
                    // TODO: This used to be able to do some calculation directly from the update if no
                    //       wrapper was found.  Can't do that any longer because if we don't have the
                    //       wrapper we don't know the format of the update.  Probably doesn't make much
                    //       difference because position info was not included in every update anyway;
                    //       may want to revisit this though.
                    double attentionWeight = 0.001;

                    IReplicaWrapper wrapper;
                    this.replicaWrappers.TryGetValue(updateInfo.EntityId, out wrapper);

                    // This block must be entered even if updateInfo.Update is empty, because wrapper.ApplyUpdate needs to be notified that an update arrived so it doesn't time out
                    if (wrapper != null)
                    {
                        if (this.association.IsAssociated(updateInfo.OriginalEntityId, updateInfo.EntityId))
                        {
                            // We have the wrapper, and it is associated with one of the local originals, so we're not going to make any more instantiation requests
                            this.lastInstantiationRequestTime.Remove(new Pair<BadumnaId, BadumnaId>(updateInfo.EntityId, updateInfo.OriginalEntityId));
                            // Apply the update first so it can be used in the attention calculation
                            Logger.TraceInformation(LogTag.Replication, "Applying update {0} to {1}", updateInfo.SequenceNumber, updateInfo.EntityId);
                            attentionWeight = wrapper.ApplyUpdate(updateInfo.SequenceNumber, updateInfo.IncludedParts, updateInfo.Update,
                                updateInfo.EstimatedDelayMilliseconds, updateInfo.OriginalEntityId);
                        }
                        else
                        {
                            // we have the replica collection, but the original and replica are not associated
                            this.RequestInstantiation(updateInfo.EntityId, updateInfo.OriginalEntityId);
                        }
                    }
                    else
                    {
                        if (attentionWeight > 0.0)
                        {
                            this.RequestInstantiation(updateInfo.EntityId, updateInfo.OriginalEntityId);
                        }
                        else
                        {
                            Logger.TraceInformation(LogTag.Replication, "Ignoring update from {0} becuase no local entities are interested.", updateInfo.EntityId);
                        }
                    }

                    this.inboundBandwidthAllocator.AllocateWeight(updateInfo.EntityId, attentionWeight);
                }
                this.mUpdateQueue.Clear();
            }

            // Try and deliver cached custom messages
            foreach (var entity in this.customMessageCache.GetEntitiesWithCachedMessages())
            {
                IReplicaWrapper wrapper = null;
                if (this.replicaWrappers.TryGetValue(entity, out wrapper))
                {
                    foreach (var cachedMessage in this.customMessageCache.PopCachedMessages(entity))
                    {
                        wrapper.QueueCustomMessage(cachedMessage.UpdateNumber, new MemoryStream(cachedMessage.EventData));
                    }

                    wrapper.ProcessCustomMessages();
                }
            }
        }

        public void CollectGarbage()
        {
            foreach (KeyValuePair<BadumnaId, IReplicaWrapper> wrapperKVP in this.replicaWrappers)
            {
                if (!wrapperKVP.Value.IsExpired)
                {
                    continue;
                }

                Logger.TraceInformation(LogTag.Replication, "Replica {0} has expired", wrapperKVP.Key);
                this.mEventQueue.Enqueue(new LambdaFunction(new GenericCallBack<BadumnaId>(this.SafeReplicaRemoval), wrapperKVP.Value.Guid));
            }

            this.customMessageCache.CollectGarbage();
        }

        public void SendCustomMessageToOriginal(IReplica remoteEntity, MemoryStream messageData)
        {
            IReplicaWrapper remoteWrapper = null;

            if (remoteEntity == null)
            {
                throw new ArgumentNullException("remoteEntity");
            }

            if (remoteEntity.Guid == null)
            {
                throw new ArgumentNullException("remoteEntity.Guid");
            }

            if (this.replicaWrappers.TryGetValue(remoteEntity.Guid, out remoteWrapper))
            {
                // TODO: Support for MemoryStream in Message
                Logger.TraceInformation(LogTag.Replication, "Sending custom message request to {0}", remoteEntity.Guid);
                EntityEnvelope envelope = this.GetMessageFor(remoteEntity.Guid, QualityOfService.Reliable);
                envelope.Qos.Priority = QosPriority.High;
                this.RemoteCall(envelope, this.ProcessCustomMessageRequest, messageData.ToArray());

                this.SendMessage(envelope);
            }
            else
            {
                Logger.TraceInformation(LogTag.Replication, "Failed to send custom message request to unknown remote entity {0}", remoteEntity.Guid);
            }
        }

        /// <summary>
        /// Call update entity on remote peer. 
        /// </summary>
        /// <param name="envelope">The entity envelope.</param>
        /// <param name="sequenceNumber">The sequence number.</param>
        /// <param name="includedParts">The included parts.</param>
        /// <param name="update">The update.</param>
        public void RemoteCallUpdateEntity(EntityEnvelope envelope, CyclicalID.UShortID sequenceNumber, BooleanArray includedParts, byte[] update)
        {
            this.RemoteCall(envelope, this.UpdateEntity, sequenceNumber, includedParts, update);
        }

        /// <summary>
        /// Call update multiple entities on remote peer.
        /// </summary>
        /// <param name="envelope">The entity envelope.</param>
        /// <param name="sequenceNumber">The sequence number.</param>
        /// <param name="includedParts">The included parts.</param>
        /// <param name="update">The update.</param>
        /// <param name="targets">The remote peers.</param>
        public void RemoteCallUpdateMultipleEntities(EntityEnvelope envelope, CyclicalID.UShortID sequenceNumber, BooleanArray includedParts,
            byte[] update, List<BadumnaId> targets)
        {
            this.RemoteCall(envelope, this.UpdateMultipleEntities, sequenceNumber, includedParts, update, targets);
        }

        /// <summary>
        /// Performans a remote call to remove entity.
        /// </summary>
        /// <param name="envelope">The envelope.</param>
        /// <param name="id">The entity id.</param>
        public void RemoteCallRemoveEntity(EntityEnvelope envelope, BadumnaId id)
        {
            this.RemoteCall(envelope, this.RemoveEntity, id);
        }

        /// <summary>
        /// Performans a remote call to remove entity with known associated original id.
        /// </summary>
        /// <param name="envelope">The envelope.</param>
        /// <param name="originalId">The original id.</param>
        /// <param name="id">The id.</param>
        public void RemoteCallRemoveEntityWithKnownAssociatedOriginal(EntityEnvelope envelope, BadumnaId originalId, BadumnaId id)
        {
            this.RemoteCall(envelope, this.RemoveEntityWithKnownAssociatedOriginal, originalId, id);
            this.SendMessage(envelope);
        }


        #region Protocol

        [EntityProtocolMethod(EntityMethod.InstantiateEntity)]
        private void InstantiateEntity(EntityTypeId entityType, CyclicalID.UShortID sequenceNumber, BooleanArray includedParts, byte[] update, ushort desiredBandwidth)
        {
            BadumnaId entityId = this.CurrentEnvelope.SourceEntity;
            BadumnaId associatedOriginalEntityId = this.CurrentEnvelope.DestinationEntity;
            Logger.TraceInformation(
                LogTag.Replication | LogTag.ProtocolMethod,
                "InstantiateEntity: entity = {0}, assoc. orig = {1}, seq = {2}",
                entityId,
                associatedOriginalEntityId,
                sequenceNumber);

            // queue the event for create the replica wrapper.
            this.mEventQueue.Enqueue(new LambdaFunction(new GenericCallBack<BadumnaId, EntityTypeId, double, BadumnaId>(this.SafeEntityArrivalHandler),
                entityId, entityType, (double)desiredBandwidth, associatedOriginalEntityId));

            // remove all existing update in the queue. if those update doesn't not carry the scene name, then the just
            // created replica will reach DetectSeperation without a valid scene name. it will be regarded as no longer interested and the original 
            // will be incorrectly notified to stop sending udpates. this in turn will lead to a once off flash of the replica.
            // in short, we need to ensure the full update that we are going to queue at the end of this method will be the first one to be applied 
            // on the above created replica. 
            this.RemoveExistingUpdates(this.CurrentEnvelope.SourceEntity, this.CurrentEnvelope.DestinationEntity);

            // queue the initial full update for the above created replica
            this.UpdateEntity(sequenceNumber, includedParts, update);
        }

        /// <summary>
        /// Removes the existing updates from the update queue.
        /// </summary>
        /// <param name="entityId">The replica's entity id.</param>
        /// <param name="associatedOriginalEntityId">The associated original entity id.</param>
        private void RemoveExistingUpdates(BadumnaId entityId, BadumnaId associatedOriginalEntityId)
        {
            List<UpdateInformation> newUpdateQueue = new List<UpdateInformation>();

            foreach (UpdateInformation update in this.mUpdateQueue)
            {
                if (!update.EntityId.Equals(entityId) || !update.OriginalEntityId.Equals(associatedOriginalEntityId))
                {
                    newUpdateQueue.Add(update);
                }
            }

            this.mUpdateQueue = newUpdateQueue;
        }

        private void SafeEntityArrivalHandler(BadumnaId entityId, EntityTypeId entityType, double desiredBandwidth, BadumnaId associatedOriginalId)
        {
            // Execution of this function must always result in an IReplica for this entityId being created.
            // If the application can't create one then we create a stub.  This is to ensure we don't continuously
            // re-request instantiation.
            // TODO: If we can't instantiate then send a message to the source telling it we don't want these updates.
            Logger.TraceInformation(LogTag.Replication, "Asking application layer to instantiate entity {0}, associated original {1}", entityId, associatedOriginalId);

            EntityTypeInfo typeInfo;
            if (!this.entityGroups.TryGetValue(entityType.Group, out typeInfo))
            {
                Logger.TraceWarning(LogTag.Replication, "Got instanation message for entity of unknown type {0}", entityType);
                typeInfo = this.stubTypeInfo;
            }

            // record the association relationship
            // an original entity identified by associatedOriginalId interests in the replica identified by entityId
            this.association.AddAssociationRelationship(associatedOriginalId, entityId);

            IReplicaWrapper remoteWrapper;
            if (!this.replicaWrappers.TryGetValue(entityId, out remoteWrapper))
            {
                Logger.TraceInformation(LogTag.Entity | LogTag.Event, "Replica add {0} type {1}", entityId, entityType);

                // for the current system, only the SpatialEntity module uses the replication system. 
                // so for now, this will return a SpatialReplicaCollection which is a IReplica. 
                // its status can be updated by the update info sent by its associated original entity. 
                IReplica replica = typeInfo.CreateReplica(entityId, entityType);

                if (replica == null)
                {
                    replica = this.stubTypeInfo.CreateReplica(entityId, entityType);
                }

                remoteWrapper = new ReplicaWrapper(this, replica, entityType, typeInfo.RemoveReplica, this.eventQueue, this.timeKeeper, this.connectivityReporter);
                this.replicaWrappers[entityId] = remoteWrapper;
                this.inboundBandwidthAllocator.AllocateSlice(entityId, desiredBandwidth, 0.5f);
            }
        }

        [EntityProtocolMethod(EntityMethod.RemoveEntityWithKnownAssociatedOriginal)]
        public void RemoveEntityWithKnownAssociatedOriginal(BadumnaId entityId, BadumnaId associatedOriginal)
        {
            Logger.TraceInformation(
                LogTag.Replication | LogTag.ProtocolMethod,
                "RemoveEntityWithKnownAssociatedOriginal: entity = {0}, assoc. original = {1}.",
                entityId,
                associatedOriginal);
            if (!this.replicaWrappers.ContainsKey(entityId))
            {
                return;
            }

            Logger.TraceInformation(LogTag.Replication, "Received remove entity request for {0}", entityId);
            this.RemoveUpdates(entityId);
            this.mEventQueue.Enqueue(new LambdaFunction(new GenericCallBack<BadumnaId, BadumnaId>(this.SafeReplicaRemoval), entityId, associatedOriginal));
        }

        [EntityProtocolMethod(EntityMethod.RemoveEntity)]
        public void RemoveEntity(BadumnaId entityId)
        {
            Logger.TraceInformation(LogTag.Replication | LogTag.ProtocolMethod, "RemoveEntity: entity = {0}.", entityId);
            if (!this.replicaWrappers.ContainsKey(entityId))
            {
                return;
            }

            Logger.TraceInformation(LogTag.Replication, "Received remove entity request for {0}", entityId);
            this.RemoveUpdates(entityId);
            this.mEventQueue.Enqueue(new LambdaFunction(new GenericCallBack<BadumnaId>(this.SafeReplicaRemoval), entityId));
        }

        private void RemoveUpdates(BadumnaId entityId)
        {
            List<UpdateInformation> newUpdateQueue = new List<UpdateInformation>();
            foreach (UpdateInformation ui in this.mUpdateQueue)
            {
                if (!ui.EntityId.Equals(entityId))
                {
                    newUpdateQueue.Add(ui);
                }
            }

            this.mUpdateQueue = newUpdateQueue;
        }

        private void SafeReplicaRemoval(BadumnaId entityId)
        {
            this.SafeReplicaRemoval(entityId, null);
        }

        private void SafeReplicaRemoval(BadumnaId entityId, BadumnaId associatedOriginalId)
        {
            IReplicaWrapper remoteWrapper;
            if (!this.replicaWrappers.TryGetValue(entityId, out remoteWrapper))
            {
                return;
            }

            if (null != associatedOriginalId)
            {
                this.association.RemoveAssociatedReplicas(associatedOriginalId, entityId);
                Pair<BadumnaId, BadumnaId> key = new Pair<BadumnaId, BadumnaId>(entityId, associatedOriginalId);
                this.lastRemovalRequestTime[key] = this.timeKeeper.Now;
            }
            else
            {
                List<BadumnaId> originals = this.association.GetAssociatedOriginals(entityId);
                if (null != originals)
                {
                    foreach (BadumnaId id in originals)
                    {
                        Pair<BadumnaId, BadumnaId> key = new Pair<BadumnaId, BadumnaId>(entityId, id);
                        this.lastRemovalRequestTime[key] = this.timeKeeper.Now;
                    }
                }

                this.association.RemoveFromAllAssociatedOriginals(entityId);
            }

            // no longer has any associated original
            if (!this.association.HasAssociatedOriginal(entityId))
            {
                Logger.TraceInformation(LogTag.Entity | LogTag.Event, "Replica remove {0}", entityId);
                remoteWrapper.Shutdown();
                this.replicaWrappers.Remove(entityId);
                this.inboundBandwidthAllocator.RemoveSlice(entityId);
            }
            else
            {
                if (null == associatedOriginalId)
                {
                    Logger.TraceError(LogTag.Replication, "SafeReplicaRemoval believes there is still associated original, which shouldn't.");
                }

                // the IReplica is still required, but not all ISpatialReplicas are required. 
                remoteWrapper.Synchronize(this.association.GetAssociatedOriginals(entityId));
            }
        }

        private void RecordUpdateTime(BadumnaId sourceId, BadumnaId originalId)
        {
            lock (this.mUpdateLog)
            {
                this.mUpdateLog.Add(new Pair<BadumnaId, BadumnaId>(sourceId, originalId));
            }
        }

        public void UpdateEntity(EntityEnvelope envelope, CyclicalID.UShortID sequenceNumber, BooleanArray includedParts, byte[] update)
        {
            this.RemoteCall(envelope, this.UpdateEntity, sequenceNumber, includedParts, update);
        }

        [EntityProtocolMethod(EntityMethod.UpdateEntity)]
        public void UpdateEntity(CyclicalID.UShortID sequenceNumber, BooleanArray includedParts, byte[] update)
        {
            Logger.TraceInformation(
                LogTag.Replication | LogTag.ProtocolMethod,
                "UpdateEntity: from {0} to {1}, seq = {2}.",
                this.CurrentEnvelope.SourceEntity,
                this.CurrentEnvelope.DestinationEntity,
                sequenceNumber);
            if (!this.originalWrappers.ContainsKey(this.CurrentEnvelope.DestinationEntity))
            {
                // the remote peer claims the update is for the replica with the id this.CurrentEnvelope.SourceEntity that
                // is associated with the local original entity with id this.CurrentEnvelope.DestinationEntity, however
                // the mentioned local entity is not on this peer. 
                return;
            }

            if (this.CurrentEnvelope.SourceEntity.OriginalAddress != this.CurrentEnvelope.SourceEntity.Address &&
                this.CurrentEnvelope.SourceEntity.OriginalAddress.Equals(this.CurrentEnvelope.DestinationEntity.Address))
            {
                // Ignore replication of hosted entities originating on this peer.
                return;
            }

            Logger.TraceInformation(LogTag.Replication, "Update for {0}", this.CurrentEnvelope.SourceEntity);
            this.QueueUpdate(
                new UpdateInformation(
                    this.CurrentEnvelope.SourceEntity,
                    this.CurrentEnvelope.DestinationEntity,
                    this.EstimatedDelayInMillisecondsSinceEnvelopeConstruction(),
                    sequenceNumber,
                    includedParts,
                    update));
        }

        /// <summary>
        /// Estimates the delay in milliseconds since the current envelope was constructed on the source.
        /// This method can only be called during the execution of a remote call!
        /// </summary>
        /// <returns>The estimated delay in milliseconds</returns>
        private ushort EstimatedDelayInMillisecondsSinceEnvelopeConstruction()
        {
            TimeSpan timeSinceDeparture = this.timeKeeper.Now - this.router.CurrentEnvelope.EstimatedTimeOfDeparture;
            return (ushort)(this.CurrentEnvelope.SourceQueuingDelay + timeSinceDeparture).TotalMilliseconds;
        }

        [EntityProtocolMethod(EntityMethod.UpdateMultipleEntities)]
        public void UpdateMultipleEntities(CyclicalID.UShortID sequenceNumber, BooleanArray includedParts, byte[] update, List<BadumnaId> targets)
        {
            var targetIds = targets[0].ToString();
            for (int i = 1; i < targets.Count; ++i)
            {
                targetIds += ", " + targets[i];
            }

            Logger.TraceInformation(
                LogTag.Replication | LogTag.ProtocolMethod,
                "UpdateMultipleEntities: from {0} to {1}, seq = {2}.",
                this.CurrentEnvelope.SourceEntity,
                targetIds,
                sequenceNumber);
            Debug.Assert(targets != null, "targets is null in UpdateMultipleEntities");

            foreach (BadumnaId targetId in targets)
            {
                if (!this.originalWrappers.ContainsKey(targetId))
                {
                    continue;
                }

                this.QueueUpdate(
                    new UpdateInformation(
                        this.CurrentEnvelope.SourceEntity,
                        targetId,
                        this.EstimatedDelayInMillisecondsSinceEnvelopeConstruction(),
                        sequenceNumber,
                        includedParts,
                        update));
            }
        }

        private void QueueUpdate(UpdateInformation info)
        {
            lock (this.mUpdateQueue)
            {
                this.mUpdateQueue.Add(info);
            }
        }

        [EntityProtocolMethod(EntityMethod.ProcessCustomMessage)]
        public void ProcessCustomMessage(CyclicalID.UShortID updateNumber, byte[] eventData)
        {
            Logger.TraceInformation(
                LogTag.Replication | LogTag.ProtocolMethod,
                "ProcessCustomMessage: from {0}, seq = {2}.",
                this.CurrentEnvelope.SourceEntity,
                updateNumber);
            BadumnaId entityId = this.CurrentEnvelope.SourceEntity;
            IReplicaWrapper wrapper = null;

            if (entityId == null)
            {
                Logger.TraceError(LogTag.Replication, "Invalid source entity id for process event");
                return;
            }

            if (this.replicaWrappers.TryGetValue(entityId, out wrapper))
            {
                // Events are processed in ProcessInboundUpdates()
                wrapper.QueueCustomMessage(updateNumber, new MemoryStream(eventData));
            }
            else
            {
                // The custom message has been received before the entity was instantiated,
                // so cache the message for delivery on instantiation.
                this.customMessageCache.Cache(entityId, updateNumber, eventData);
            }
        }

        #endregion
    }
}
