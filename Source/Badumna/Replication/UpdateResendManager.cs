﻿//---------------------------------------------------------------------------------
// <copyright file="UpdateResendManager.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Diagnostics;
using Badumna.Core;
using Badumna.DataTypes;
using Badumna.Utilities;
using Badumna.Utilities.Logging;

namespace Badumna.Replication
{
    /// <summary>
    /// The resent manager implements the update resend policies. 
    /// </summary>
    internal class UpdateResendManager
    {
        /// <summary>
        /// Key is the index of the state segment bit.  Records the sequence number that was current
        /// when a given state segment last changed.
        /// </summary>
        private Dictionary<int, SegmentState> segmentState;

        /// <summary>
        /// Resend events.
        /// </summary>
        private BinaryHeap<ResendEvent> resendEvents;

        /// <summary>
        /// The entity manager.
        /// </summary>
        private IEntityManager entityManager;

        /// <summary>
        /// The original wrapper. 
        /// </summary>
        private IOriginalWrapper originalWrapper;

        /// <summary>
        /// Provides access to the current time.
        /// </summary>
        private ITime timeKeeper;

        /// <summary>
        /// Initializes a new instance of the <see cref="UpdateResendManager"/> class.
        /// </summary>
        /// <param name="entityManager">The entity manager.</param>
        /// <param name="originalWrapper">The original wrapper.</param>
        /// <param name="timeKeeper">Provides access to the current time.</param>
        public UpdateResendManager(IEntityManager entityManager, IOriginalWrapper originalWrapper, ITime timeKeeper)
        {
            this.timeKeeper = timeKeeper;
            this.originalWrapper = originalWrapper;
            this.entityManager = entityManager;
            this.segmentState = new Dictionary<int, SegmentState>();
            this.resendEvents = new BinaryHeap<ResendEvent>();
        }

        /// <summary>
        /// Updates the resend events.
        /// </summary>
        /// <param name="sentParts">The sent parts.</param>
        /// <param name="sequenceNumber">The sequence number.</param>
        /// <param name="targets">The targets.</param>
        public void UpdateResendEvents(BooleanArray sentParts, CyclicalID.UShortID sequenceNumber, IEnumerable<BadumnaId> targets)
        {
            TimeSpan now = this.timeKeeper.Now;
            TimeSpan resendTime = now + Parameters.SegmentResendDelayTime;

            ResendEvent resendEvent = new ResendEvent(resendTime, sentParts);
            this.resendEvents.Push(resendEvent);

            for (int i = 0; i <= sentParts.HighestUsedIndex; i++)
            {
                if (!sentParts[i])
                {
                    continue;
                }

                SegmentState state;
                if (!this.segmentState.TryGetValue(i, out state))
                {
                    state = new SegmentState();
                    this.segmentState[i] = state;
                }
                else
                {
                    // Segment i is now part of the newly created resend event
                    state.ResendEvent.Segments[i] = false;
                }

                state.LastChangeSequenceNumber = sequenceNumber;
                state.UnacknowledgedPeers.Clear();
                state.UnacknowledgedPeers.AddRange(targets);
                state.ResendEvent = resendEvent;
            }
        }

        /// <summary>
        /// Resends the updates.
        /// </summary>
        public void ResendUpdates()
        {
            //// TODO: Maybe do something clever to find peers which require the same updates?
            BooleanArray partsToResend = new BooleanArray();
            TimeSpan now = this.timeKeeper.Now;

            while (this.resendEvents.Count > 0 && this.resendEvents.Peek().Time <= now)
            {
                ResendEvent resendEvent = this.resendEvents.Pop();
                partsToResend.Or(resendEvent.Segments);
            }

            if (partsToResend.Any())
            {
                ResendEvent newResendEvent = new ResendEvent(now + Parameters.SegmentResendDelayTime, partsToResend);
                this.resendEvents.Push(newResendEvent);

                BooleanArray singlePart = new BooleanArray();
                Dictionary<PeerAddress, EntityEnvelope> envelopes = new Dictionary<PeerAddress, EntityEnvelope>();

                for (int i = 0; i <= newResendEvent.Segments.HighestUsedIndex; i++)
                {
                    if (!newResendEvent.Segments[i])
                    {
                        continue;
                    }

                    SegmentState state;
                    if (!this.segmentState.TryGetValue(i, out state))
                    {
                        Debug.Assert(false, "Missing segment state for segment " + i.ToString());
                        newResendEvent.Segments[i] = false;
                        continue;
                    }

                    if (state.UnacknowledgedPeers.Count == 0)
                    {
                        // Everyone has acknowledged this segment, get it out of the resend queue
                        newResendEvent.Segments[i] = false;
                        continue;
                    }

                    singlePart.SetAll(false);
                    singlePart[i] = true;

                    // TODO: Should probably cache these for a given sequence number
                    BooleanArray remainingParts;
                    byte[] segmentUpdate = this.originalWrapper.SerializeParts(singlePart, out remainingParts);
                    //// Debug.Assert(!remainingParts.Any(), "Couldn't serialize even a single part");

                    foreach (BadumnaId id in state.UnacknowledgedPeers)
                    {
                        EntityEnvelope envelope;
                        if (!envelopes.TryGetValue(id.Address, out envelope))
                        {
                            envelope = this.entityManager.GetMessageFor(this.originalWrapper.LocalEntity.Guid, id, QualityOfService.Unreliable);
                            envelopes[id.Address] = envelope;
                        }

                        Logger.TraceInformation(LogTag.Replication, "Resending update to {0}", id);
                        this.entityManager.UpdateEntity(envelope, state.LastChangeSequenceNumber, singlePart, segmentUpdate);
                    }
                }

                foreach (EntityEnvelope envelope in envelopes.Values)
                {
                    this.entityManager.Router.DirectSend(envelope);
                }
            }
        }

        /// <summary>
        /// Removes the unacknowledged peer when the peer is no longer interested by the original entity.  
        /// </summary>
        /// <param name="entity">The entity.</param>
        public void RemoveUnacknowledgedPeer(BadumnaId entity)
        {
            foreach (SegmentState state in this.segmentState.Values)
            {
                if (state.UnacknowledgedPeers.Contains(entity))
                {
                    Logger.TraceInformation(LogTag.Replication, "Going to remove unacknowledged entity {0}, it is no longer interested.", entity);
                    state.UnacknowledgedPeers.Remove(entity);
                }
            }
        }

        /// <summary>
        /// Processes the acknowledgements.
        /// </summary>
        /// <param name="id">The source id.</param>
        /// <param name="includedSegments">The included segments.</param>
        /// <param name="sequenceNumbers">The sequence numbers.</param>
        public void ProcessAcknowledgements(BadumnaId id, BooleanArray includedSegments, List<CyclicalID.UShortID> sequenceNumbers)
        {
            int sequenceNumberIndex = 0;

            for (int i = 0; i <= includedSegments.HighestUsedIndex; i++)
            {
                if (!includedSegments[i])
                {
                    continue;
                }

                CyclicalID.UShortID sequenceNumber = sequenceNumbers[sequenceNumberIndex];
                sequenceNumberIndex++;

                SegmentState state;
                if (!this.segmentState.TryGetValue(i, out state))
                {
                    Logger.TraceWarning(LogTag.Replication, "Got acknowledgement for segment {0} but no SegmentState found for this segment", i);
                    continue;
                }

                if (state.LastChangeSequenceNumber != sequenceNumber)
                {
                    // Ignore the ack because there's a later change sent
                    continue;
                }

                state.UnacknowledgedPeers.Remove(id);
            }
        }
    }
}
