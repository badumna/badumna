﻿//---------------------------------------------------------------------------------
// <copyright file="InterestedEntity.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

using System;
using Badumna.Core;
using Badumna.DataTypes;

namespace Badumna.Replication
{
    /// <summary>
    /// The update method of the interested entity. 
    /// </summary>
    internal enum UpdateDeliveryMethod
    {
        /// <summary>
        /// All updates are sent directly to the replica.
        /// </summary>
        Direct,

        /// <summary>
        /// Updates are sent through the overload server. 
        /// </summary>
        Overload,

        /// <summary>
        /// Updates are sent to the replica via both direct delivery and overload server. 
        /// </summary>
        SwitchingBack,

        /// <summary>
        /// The error status.
        /// </summary>
        Unknown,
    }

    /// <summary>
    /// The entity that is interested in the local entity. 
    /// </summary>
    internal class InterestedEntity
    {
        /// <summary>
        /// The update method.
        /// </summary>
        private UpdateDeliveryMethod updateMethod;

        /// <summary>
        /// When switched to the current update method.
        /// </summary>
        private TimeSpan switchTime;

        /// <summary>
        /// The badumna id of the interested entity.
        /// </summary>
        private BadumnaId entityId;

        /// <summary>
        /// Provides access to the current time.
        /// </summary>
        private ITime timeKeeper;

        /// <summary>
        /// Initializes a new instance of the <see cref="InterestedEntity"/> class.
        /// </summary>
        /// <param name="id">The entity id.</param>
        /// <param name="timeKeeper">Provides access to the current time.</param>
        public InterestedEntity(BadumnaId id, ITime timeKeeper)
        {
            this.timeKeeper = timeKeeper;
            this.updateMethod = UpdateDeliveryMethod.Direct;
            this.switchTime = this.timeKeeper.Now;
            this.entityId = id;
        }

        /// <summary>
        /// Gets the update method.
        /// </summary>
        /// <value>The update method.</value>
        public UpdateDeliveryMethod UpdateMethod
        {
            get { return this.updateMethod; }
        }

        /// <summary>
        /// Gets the switch time.
        /// </summary>
        /// <value>The switch time.</value>
        public TimeSpan SwitchTime
        {
            get { return this.switchTime; }
        }

        /// <summary>
        /// Gets the entity id.
        /// </summary>
        /// <value>The entity id.</value>
        public BadumnaId EntityId
        {
            get { return this.entityId; }
        }

        /// <summary>
        /// Equalses the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns>Whether the specified entity is equal to this instance.</returns>
        public bool Equals(InterestedEntity entity)
        {
            return this.entityId.Equals(entity.entityId);
        }

        /// <summary>
        /// Determines whether the specified <see cref="System.Object"/> is equal to this instance.
        /// </summary>
        /// <param name="obj">The <see cref="System.Object"/> to compare with this instance.</param>
        /// <returns>
        /// <c>true</c> if the specified <see cref="System.Object"/> is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        /// <exception cref="T:System.NullReferenceException">The <paramref name="obj"/> parameter is null.</exception>
        public override bool Equals(object obj)
        {
            if (obj is InterestedEntity)
            {
                InterestedEntity entity = (InterestedEntity)obj;
                
                return this.entityId.Equals(entity.entityId);
            }

            return false;
        }

        /// <summary>
        /// Switches the update method.
        /// </summary>
        /// <param name="method">The method.</param>
        public void SwitchUpdateMethod(UpdateDeliveryMethod method)
        {
            if (this.updateMethod == UpdateDeliveryMethod.Direct && method == UpdateDeliveryMethod.SwitchingBack)
            {
                throw new InvalidOperationException("SwitchUpdateMethod");
            }

            this.updateMethod = method;
            this.switchTime = this.timeKeeper.Now;
        }

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
        /// </returns>
        public override int GetHashCode()
        {
            return this.entityId.GetHashCode();
        }

        /// <summary>
        /// Returns a string representation of this object.
        /// </summary>
        /// <returns>A string representation of this object.</returns>
        public override string ToString()
        {
            return this.entityId.ToString();
        }
    }
}
