﻿//-----------------------------------------------------------------------
// <copyright file="IOriginal.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2010 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.Replication
{
    using System.IO;
    using Badumna.Utilities;

    /// <summary>
    /// To be implemented by classes representing the original instance of an entity.
    /// </summary>
    /// <remarks>
    /// Remote and local entities are paired. Local entities reside on the controlling peer and are
    /// responsible for propogating state changes to their remote replicas. When this entity
    /// requires its state to be propogated the NetworkFacade.FlagForUpdate() method should be
    /// called. This schedules the update dispatch to be processed during some future call to
    /// NetworkFacade.ProcessNetworkState() (there is no guarantee that the dispatch will be made in
    /// the next call to ProcessNetworkState(), only that it will be made in a very near future
    /// call). When ProcessNetworkState() goes to dispatch the update it will make a call to
    /// Serialize() which should generate the content of the update.  This update data is then sent
    /// unreliably to all peers currently interested in the entity.
    /// </remarks>
    public interface IOriginal : IEntity
    {
        /// <summary>
        /// Serializes the specified required parts
        /// </summary>
        /// <param name="requiredParts">The required parts.  This parameter can be mutated and on
        /// exit should indicate the parts actually included.</param>
        /// <param name="stream">The stream to which the required parts are serialized.</param>
        /// <returns>A BooleanArray indicating any parts the entity has left to serialize.
        /// Generally the implementation should serialize all requiredParts onto the stream in one
        /// go and return a BooleanArray will all bits false.</returns>
        BooleanArray Serialize(BooleanArray requiredParts, Stream stream);
    }
}
