﻿//---------------------------------------------------------------------------------
// <copyright file="ReplicaWrapper.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

using System;
using System.IO;

using Badumna.Core;
using Badumna.DataTypes;
using Badumna.Utilities;
using Badumna.Utilities.Logging;

namespace Badumna.Replication
{
    /// <summary>
    /// A wrapper class for the IReplica object with the ability to track ack events. 
    /// </summary>
    internal class ReplicaWrapper : IReplicaWrapper
    {
        /// <summary>
        /// The replica object. 
        /// </summary>
        private IReplica replica;

        /// <summary>
        /// When the replica object will become expired. 
        /// </summary>
        private TimeSpan expireTime;

        /// <summary>
        /// A queue of custom message. 
        /// </summary>
        private CustomMessageQueue customMessageQueue;

        /// <summary>
        /// The remove replica delegate. 
        /// </summary>
        private RemoveReplica removeReplica;

        /// <summary>
        /// The update ack event handler.
        /// </summary>
        private IUpdateAckEventHandler ackHandler;

        /// <summary>
        /// Provides access to the current time.
        /// </summary>
        private ITime timeKeeper;

        /// <summary>
        /// Initializes a new instance of the <see cref="ReplicaWrapper"/> class.
        /// </summary>
        /// <param name="entityManager">The entity manager.</param>
        /// <param name="replica">The replica.</param>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="removeReplica">The remove replica.</param>
        /// <param name="eventQueue">The queue to use for scheduling events.</param>
        /// <param name="timeKeeper">Provides access to the current time.</param>
        /// <param name="connectivityReporter">Reports on network connectivity status.</param>
        public ReplicaWrapper(
            IEntityManager entityManager,
            IReplica replica,
            EntityTypeId entityType,
            RemoveReplica removeReplica,
            NetworkEventQueue eventQueue,
            ITime timeKeeper,
            INetworkConnectivityReporter connectivityReporter)
        {
            // TODO: Parameter entityType is not used. Remove it?
            this.timeKeeper = timeKeeper;
            this.customMessageQueue = new CustomMessageQueue(eventQueue, this.timeKeeper, connectivityReporter);

            this.replica = replica;
            this.removeReplica = removeReplica;
            this.ackHandler = new UpdateAckEventHandler(entityManager, this.replica.Guid, eventQueue, this.timeKeeper);

            this.expireTime = this.timeKeeper.Now + Parameters.ReplicaTimeToLive;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ReplicaWrapper"/> class. For unit testing only. 
        /// </summary>
        /// <param name="entityManager">The entity manager.</param>
        /// <param name="replica">The replica.</param>
        /// <param name="ackHandler">The ack handler.</param>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="removeReplica">The remove replica.</param>
        /// <param name="eventQueue">The queue to use for scheduling events.</param>
        /// <param name="timeKeeper">Provides access to the current time.</param>
        /// <param name="connectivityReporter">Reports on network connectivity status.</param>
        public ReplicaWrapper(
            IEntityManager entityManager,
            IReplica replica,
            IUpdateAckEventHandler ackHandler,
            EntityTypeId entityType,
            RemoveReplica removeReplica,
            NetworkEventQueue eventQueue,
            ITime timeKeeper,
            INetworkConnectivityReporter connectivityReporter)
            : this(entityManager, replica, entityType, removeReplica, eventQueue, timeKeeper, connectivityReporter)
        {
            this.ackHandler = ackHandler;
        }

        /// <summary>
        /// Gets the expire time.
        /// </summary>
        /// <value>The expire time.</value>
        public TimeSpan ExpireTime
        {
            get { return this.expireTime; }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is expired.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is expired; otherwise, <c>false</c>.
        /// </value>
        public bool IsExpired 
        { 
            get { return this.timeKeeper.Now > this.expireTime; } 
        }

        /// <summary>
        /// Gets the GUID.
        /// </summary>
        /// <value>The replica's GUID.</value>
        public BadumnaId Guid 
        { 
            get { return this.replica.Guid; } 
        }

        /// <summary>
        /// Synchronizes the specified obj.
        /// </summary>
        /// <param name="obj">The obj used for synchronization.</param>
        public void Synchronize(object obj)
        {
            var synchronizable = this.replica as ISynchronizable;
            if (synchronizable != null)
            {
                synchronizable.Synchronize(obj);
            }
        }

        /// <summary>
        /// Shutdowns this instance.
        /// </summary>
        public void Shutdown()
        {
            this.removeReplica(this.replica);
        }

        /// <summary>
        /// Queues the custom message.
        /// </summary>
        /// <param name="updateNumber">The update seq number.</param>
        /// <param name="messageData">The message data.</param>
        public void QueueCustomMessage(CyclicalID.UShortID updateNumber, MemoryStream messageData)
        {
            Logger.TraceInformation(LogTag.Replication, "queue custom message {0}", updateNumber.Value);
            this.customMessageQueue.Add(updateNumber, messageData);
        }

        /// <summary>
        /// Processes the custom messages.
        /// </summary>
        public void ProcessCustomMessages()
        {
            CustomMessageQueue.CustomMessage customMessage = this.customMessageQueue.InOrderPop();

            while (null != customMessage)
            {
                try
                {
                    Logger.TraceInformation(LogTag.Replication, "Processing custom message {0} from {1}", customMessage.MessageNumber.Value, this.replica.Guid);
                    this.replica.HandleEvent(customMessage.Data);
                }
                catch (Exception e)
                {
                    Logger.TraceException(LogLevel.Warning, e, "Failed to process event");
                }

                customMessage = this.customMessageQueue.InOrderPop();
            }
        }

        /// <summary>
        /// Applies the update.
        /// </summary>
        /// <param name="sequenceNumber">The sequence number.</param>
        /// <param name="includedParts">The included parts.</param>
        /// <param name="update">The update.</param>
        /// <param name="estimatedDelay">The estimated delay.</param>
        /// <param name="id">The source id.</param>
        /// <returns>The updated attention value.</returns>
        public double ApplyUpdate(CyclicalID.UShortID sequenceNumber, BooleanArray includedParts, byte[] update, int estimatedDelay, BadumnaId id)
        {
            Logger.TraceInformation(LogTag.Match, "Applying update {0} from entity {1}", sequenceNumber.Value, id);
            using (Stream dataStream = new MemoryStream(update))
            {
                double attention = this.replica.Deserialize(includedParts, dataStream, estimatedDelay, id);
                this.ackHandler.UpdateDelayedAckTimes(sequenceNumber, includedParts);
                this.expireTime = this.timeKeeper.Now + Parameters.ReplicaTimeToLive;

                return attention;
            }
        }
    }
}
