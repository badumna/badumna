using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;

using Badumna.Core;
using Badumna.Utilities;
using Badumna;
using Badumna.DataTypes;

namespace Badumna.Replication
{
    class EntityProtocol : ProtocolComponent<EntityEnvelope>, IEntityProtocol
    {
        /// <summary>
        /// Provides the current public address.
        /// </summary>
        private INetworkAddressProvider addressProvider;

        public EntityProtocol(INetworkAddressProvider addressProvider, GenericCallBackReturn<object, Type> factory)
            : base("Entity interaction", typeof(EntityProtocolMethodAttribute), factory)
        {
            this.addressProvider = addressProvider;
        }

        public EntityProtocol(EntityProtocol parent, INetworkAddressProvider addressProvider)
            : base(parent)
        {
            this.addressProvider = addressProvider;
        }

        /// <summary>
        /// Sends from 'Any' address to given destination entity.
        /// </summary>
        /// <param name="destinationId"></param>
        /// <param name="qos"></param>
        /// <returns></returns>
        public EntityEnvelope GetMessageFor(BadumnaId destinationId, QualityOfService qos)
        {
            EntityEnvelope envelope = new EntityEnvelope(destinationId, new BadumnaId(this.addressProvider.PublicAddress, 0), qos);

            this.PrepareMessageForDeparture(ref envelope);

            return envelope;
        }

        /// <summary>
        /// Gets a message envelope from the given entity. The message destination is not set because the message is intended to be sent
        /// only to interested peers, which does not require a destination.
        /// </summary>
        /// <param name="entityId"></param>
        /// <param name="qos"></param>
        /// <returns></returns>
        public EntityEnvelope GetMessageFrom(BadumnaId entityId, QualityOfService qos)
        {
            EntityEnvelope envelope = new EntityEnvelope(null, entityId, qos);

            this.PrepareMessageForDeparture(ref envelope);

            return envelope;
        }

        /// <summary>
        /// Sends from a message between specific source and destination entities.
        /// </summary>
        /// <param name="sourceEntityId"></param>
        /// <param name="destinationEntityId"></param>
        /// <param name="qos"></param>
        /// <returns></returns>
        public EntityEnvelope GetMessageFor(BadumnaId sourceEntityId, BadumnaId destinationEntityId, QualityOfService qos)
        {
            EntityEnvelope envelope = new EntityEnvelope(destinationEntityId, sourceEntityId, qos);

            this.PrepareMessageForDeparture(ref envelope);

            return envelope;
        }
    }
}
