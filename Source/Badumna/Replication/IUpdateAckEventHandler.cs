﻿//-----------------------------------------------------------------------
// <copyright file="IUpdateAckEventHandler.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using Badumna.Utilities;

namespace Badumna.Replication
{
    /// <summary>
    /// UpdateAckEventHandler interface.
    /// </summary>
    internal interface IUpdateAckEventHandler
    {
        /// <summary>
        /// Updates the delayed ack times.
        /// </summary>
        /// <param name="sequenceNumber">The sequence number.</param>
        /// <param name="receivedParts">The received parts.</param>
        void UpdateDelayedAckTimes(CyclicalID.UShortID sequenceNumber, BooleanArray receivedParts);
    }
}
