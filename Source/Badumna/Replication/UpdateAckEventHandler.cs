﻿//---------------------------------------------------------------------------------
// <copyright file="UpdateAckEventHandler.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Diagnostics;

using Badumna.Core;
using Badumna.DataTypes;
using Badumna.Utilities;

namespace Badumna.Replication
{
    /// <summary>
    /// The handler class for update ack event.
    /// </summary>
    internal class UpdateAckEventHandler : IUpdateAckEventHandler
    {
        /// <summary>
        /// The entity manager.
        /// </summary>
        private IEntityManager entityManager;

        /// <summary>
        /// Last seq number for all segments. Key is the index of the state segment bit.
        /// </summary>
        private Dictionary<int, LastSequenceInfo> lastSequenceNumber = new Dictionary<int, LastSequenceInfo>();

        /// <summary>
        /// The Update ack events. 
        /// </summary>
        private BinaryHeap<UpdateAckEvent> updateAckEvents = new BinaryHeap<UpdateAckEvent>();

        /// <summary>
        /// The task for sending ack. 
        /// </summary>
        private OneShotTask ackTask;

        /// <summary>
        /// The id of the replica.
        /// </summary>
        private BadumnaId replicaId;

        /// <summary>
        /// Provides access to the current time.
        /// </summary>
        private ITime timeKeeper;

        /// <summary>
        /// Initializes a new instance of the <see cref="UpdateAckEventHandler"/> class.
        /// </summary>
        /// <param name="entityManager">The entity manager.</param>
        /// <param name="replicaId">The replica id.</param>
        /// <param name="eventQueue">The queue to use for scheduling events.</param>
        /// <param name="timeKeeper">Provides access to the current time.</param>
        public UpdateAckEventHandler(
            IEntityManager entityManager,
            BadumnaId replicaId,
            NetworkEventQueue eventQueue,
            ITime timeKeeper)
        {
            this.entityManager = entityManager;
            this.replicaId = replicaId;
            this.timeKeeper = timeKeeper;
            this.ackTask = new OneShotTask(eventQueue);
        }

        /// <summary>
        /// Updates the delayed ack times.
        /// This maintains the record of ack send time for each segment.  Each segment should only
        /// be flagged in a single AckEvent at any time.  If a new update for a segment is received,
        /// the ack event for that segment will be pushed back in time.
        /// </summary>
        /// <param name="sequenceNumber">The sequence number.</param>
        /// <param name="receivedParts">The received parts.</param>
        public void UpdateDelayedAckTimes(CyclicalID.UShortID sequenceNumber, BooleanArray receivedParts)
        {
            UpdateAckEvent ackEvent = new UpdateAckEvent(this.timeKeeper.Now + Parameters.SegmentAckDelayTime, receivedParts);
            this.updateAckEvents.Push(ackEvent);

            for (int i = 0; i <= receivedParts.HighestUsedIndex; i++)
            {
                if (receivedParts[i])
                {
                    LastSequenceInfo lastSequenceInfo;
                    if (!this.lastSequenceNumber.TryGetValue(i, out lastSequenceInfo))
                    {
                        lastSequenceInfo = new LastSequenceInfo();
                        this.lastSequenceNumber[i] = lastSequenceInfo;
                    }
                    else
                    {
                        lastSequenceInfo.AckEvent.Segments[i] = false;  // Segment i is now part of the newly created ack event
                    }

                    lastSequenceInfo.SequenceNumber = sequenceNumber;
                    lastSequenceInfo.AckEvent = ackEvent;
                }
            }

            if (!this.ackTask.IsPending)
            {
                this.ackTask.Trigger(ackEvent.Time - this.timeKeeper.Now, Apply.Func(this.SendAcks));
            }

            this.DumpAckEvents();
        }

        /// <summary>
        /// Sends the acks.
        /// </summary>
        private void SendAcks()
        {
            TimeSpan now = this.timeKeeper.Now;
            BooleanArray segmentsToAck = new BooleanArray();

            while (this.updateAckEvents.Count > 0 && this.updateAckEvents.Peek().Time <= now)
            {
                UpdateAckEvent ackEvent = this.updateAckEvents.Pop();
                segmentsToAck.Or(ackEvent.Segments);
            }

            if (segmentsToAck.Any())
            {
                List<CyclicalID.UShortID> sequenceNumbers = new List<CyclicalID.UShortID>();
                for (int i = 0; i <= segmentsToAck.HighestUsedIndex; i++)
                {
                    if (!segmentsToAck[i])
                    {
                        continue;
                    }

                    LastSequenceInfo lastSequenceInfo;
                    if (!this.lastSequenceNumber.TryGetValue(i, out lastSequenceInfo))
                    {
                        Debug.Assert(false, "Missing last sequence number info for segment " + i.ToString());
                        segmentsToAck[i] = false;
                        continue;
                    }

                    sequenceNumbers.Add(lastSequenceInfo.SequenceNumber);
                    this.lastSequenceNumber.Remove(i);
                }

                EntityEnvelope envelope = this.entityManager.GetMessageFor(this.replicaId, QualityOfService.Unreliable);
                this.entityManager.AcknowledgeSegments(envelope, segmentsToAck, sequenceNumbers);

                List<BadumnaId> originals = this.entityManager.Association.GetAssociatedOriginals(this.replicaId);
                if (null != originals)
                {
                    foreach (BadumnaId curOriginal in originals)
                    {
                        envelope.SourceEntity = curOriginal;
                        this.entityManager.Router.DirectSend(envelope);
                    }
                }
            }

            if (this.updateAckEvents.Count > 0)
            {
                TimeSpan nextTime = this.updateAckEvents.Peek().Time;
                this.ackTask.Trigger(nextTime - this.timeKeeper.Now, Apply.Func(this.SendAcks));
            }

            this.DumpAckEvents();
        }

        /// <summary>
        /// Dumps the ack events for debugging purpose. 
        /// </summary>
        private void DumpAckEvents()
        {
// This nasty conditional directive replaces a [Conditional("false")] attribute on the method which is not premitted in MonoDevelop.
// Previously it was just "#if DEBUG".
#if DEBUG && !DEBUG
            foreach (UpdateAckEvent ackEvent in this.updateAckEvents.GetQueue())
            {
                Debug.WriteLine(ackEvent.Time.TotalSeconds.ToString() + ":  " + ackEvent.Segments.ToString());
                for (int i = 0; i <= ackEvent.Segments.HighestUsedIndex; i++)
                {
                    if (!ackEvent.Segments[i])
                    {
                        continue;
                    }

                    LastSequenceInfo lastSequenceInfo;
                    if (this.lastSequenceNumber.TryGetValue(i, out lastSequenceInfo))
                    {
                        Debug.WriteLine(String.Format("{0}: {1}, ack event does {2} match", i, lastSequenceInfo.SequenceNumber, lastSequenceInfo.AckEvent == ackEvent));
                    }
                    else
                    {
                        Debug.WriteLine(String.Format("{0}: null", i));
                    }
                }
            }
#endif
        }

        /// <summary>
        /// The ack event for the entity update.
        /// </summary>
        private class UpdateAckEvent : IComparable<UpdateAckEvent>
        {
            /// <summary>
            /// Time of the ack event.
            /// </summary>
            private TimeSpan time;

            /// <summary>
            /// Segments involved with the ack event.
            /// </summary>
            private BooleanArray segments;

            /// <summary>
            /// Initializes a new instance of the <see cref="UpdateAckEventHandler.UpdateAckEvent"/> class.
            /// </summary>
            /// <param name="time">The ack event time.</param>
            /// <param name="segments">The segments.</param>
            public UpdateAckEvent(TimeSpan time, BooleanArray segments)
            {
                this.time = time;
                this.segments = new BooleanArray(segments);
            }

            /// <summary>
            /// Gets the segments.
            /// </summary>
            /// <value>The segments.</value>
            public BooleanArray Segments
            {
                get { return this.segments; }
                private set { this.segments = value; }
            }

            /// <summary>
            /// Gets the time.
            /// </summary>
            /// <value>The ack event time.</value>
            public TimeSpan Time
            {
                get { return this.time; }
                private set { this.time = value; }
            }

            /// <summary>
            /// Compares to.
            /// </summary>
            /// <param name="other">The obj to be compared.</param>
            /// <returns>
            /// A 32-bit signed integer that indicates the relative order of the objects being compared. The return value has the following meanings:
            /// Value
            /// Meaning
            /// Less than zero
            /// This object is less than the <paramref name="other"/> parameter.
            /// Zero
            /// This object is equal to <paramref name="other"/>.
            /// Greater than zero
            /// This object is greater than <paramref name="other"/>.
            /// </returns>
            public int CompareTo(object other)
            {
                if (other == null)
                {
                    // All objects > null
                    return 1;
                }

                if (!(other is UpdateAckEvent))
                {
                    throw new ArgumentException("other");
                }

                return this.Time.CompareTo(((UpdateAckEvent)other).Time);
            }

            /// <summary>
            /// Compares the current object with another object of the same type.
            /// </summary>
            /// <param name="other">An object to compare with this object.</param>
            /// <returns>
            /// A 32-bit signed integer that indicates the relative order of the objects being compared. The return value has the following meanings:
            /// Value
            /// Meaning
            /// Less than zero
            /// This object is less than the <paramref name="other"/> parameter.
            /// Zero
            /// This object is equal to <paramref name="other"/>.
            /// Greater than zero
            /// This object is greater than <paramref name="other"/>.
            /// </returns>
            public int CompareTo(UpdateAckEvent other)
            {
                return this.Time.CompareTo(other.Time);
            }
        }

        /// <summary>
        /// The class used to associcate a seq number with an ack event.
        /// </summary>
        private class LastSequenceInfo
        {
            /// <summary>
            /// The ack event.
            /// </summary>
            private UpdateAckEvent ackEvent;

            /// <summary>
            /// The sequence number.
            /// </summary>
            private CyclicalID.UShortID sequenceNumber;

            /// <summary>
            /// Gets or sets the ack event.
            /// </summary>
            /// <value>The ack event.</value>
            public UpdateAckEvent AckEvent
            {
                get { return this.ackEvent; }
                set { this.ackEvent = value; }
            }

            /// <summary>
            /// Gets or sets the sequence number.
            /// </summary>
            /// <value>The sequence number.</value>
            public CyclicalID.UShortID SequenceNumber
            {
                get { return this.sequenceNumber; }
                set { this.sequenceNumber = value; }
            }
        }
    }
}
