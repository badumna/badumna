﻿//-----------------------------------------------------------------------
// <copyright file="IOriginalWrapper.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System.Collections.Generic;
using System.IO;
using Badumna.Core;
using Badumna.DataTypes;
using Badumna.Utilities;

namespace Badumna.Replication
{
    /// <summary>
    /// The original wrapper interface.
    /// </summary>
    internal interface IOriginalWrapper
    {
        /// <summary>
        /// Gets the local entity.
        /// </summary>
        /// <value>The local entity.</value>
        IOriginal LocalEntity
        {
            get;
        }

        /// <summary>
        /// Gets the type of the entity.
        /// </summary>
        /// <value>The type of the entity.</value>
        EntityTypeId EntityType
        {
            get;
        }

        /// <summary>
        /// Gets a value indicating whether this instance is update required.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is update required; otherwise, <c>false</c>.
        /// </value>
        bool IsUpdateRequired
        {
            get;
        }

        /// <summary>
        /// Gets the state sequence number.
        /// </summary>
        /// <value>The state sequence number.</value>
        CyclicalID.UShortID StateSequenceNumber
        {
            get;
        }

        /// <summary>
        /// Gets a value indicating whether the forwarding peer is available.
        /// </summary>
        /// <value>
        /// <c>true</c> if the forwarding peer available; otherwise, <c>false</c>.
        /// </value>
        bool ForwardingPeerAvailable
        {
            get;
        }

        /// <summary>
        /// Gets the local interested entities.
        /// </summary>
        /// <value>The local interested entities.</value>
        List<BadumnaId> LocalInterestedEntities
        {
            get;
        }

        /// <summary>
        /// Shutdown this instance.
        /// </summary>
        /// <param name="removeReplicas">if set to <c>true</c> remove replicas.</param>
        void ShutDown(bool removeReplicas);

        /// <summary>
        /// Switches to new forwarding peer.
        /// </summary>
        /// <param name="address">The address of the new forwarding peer.</param>
        void SwitchToNewForwardingPeer(PeerAddress address);

        /// <summary>
        /// Adds the interested entity.
        /// </summary>
        /// <param name="entity">The entity id.</param>
        void AddInterested(BadumnaId entity);

        /// <summary>
        /// Removes the interested entity.
        /// </summary>
        /// <param name="entity">The entity id.</param>
        void RemoveInterested(BadumnaId entity);

        /// <summary>
        /// The count of interested entities.
        /// </summary>
        /// <returns>The number of interested entities. </returns>
        int InterestedCount();

        /// <summary>
        /// Gets the time out peers.
        /// </summary>
        /// <returns>A list of time out peers.</returns>
        List<Pair<BadumnaId, BadumnaId>> GetTimeOutPeers();

        /// <summary>
        /// Marks for update.
        /// </summary>
        /// <param name="changedParts">The changed parts.</param>
        void MarkForUpdate(BooleanArray changedParts);

        /// <summary>
        /// Marks for update.
        /// </summary>
        /// <param name="changedPartIndex">Index of the changed part.</param>
        void MarkForUpdate(int changedPartIndex);

        /// <summary>
        /// Gets the next custom message sequence number.
        /// </summary>
        /// <returns>The seq number.</returns>
        CyclicalID.UShortID GetNextCustomMessageNumber();

        /// <summary>
        /// Queues the custom message request.
        /// </summary>
        /// <param name="data">The request data.</param>
        void QueueCustomMessageRequest(MemoryStream data);

        /// <summary>
        /// Processes the custom message requests.
        /// </summary>
        void ProcessCustomMessageRequests();

        /// <summary>
        /// Applies the flow control feedback.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="maximumUpdateRateBps">The maximum update rate BPS.</param>
        /// <param name="attention">The attention value.</param>
        void ApplyFlowControlFeedback(PeerAddress source, ushort maximumUpdateRateBps, float attention);

        /// <summary>
        /// Sends the proximity event.
        /// </summary>
        /// <param name="eventEnvelope">The event envelope.</param>
        void SendProximityEvent(EntityEnvelope eventEnvelope);

        /// <summary>
        /// Dispatches the update.
        /// </summary>
        /// <param name="sequenceNumber">The sequence number.</param>
        /// <param name="includedParts">The included parts.</param>
        /// <param name="update">The update data.</param>
        /// <param name="sourceId">The source id.</param>
        void DispatchUpdate(CyclicalID.UShortID sequenceNumber, BooleanArray includedParts, byte[] update, BadumnaId sourceId);

        /// <summary>
        /// Clears the state of the changed.
        /// </summary>
        void ClearChangedState();

        /// <summary>
        /// Sends the event.
        /// </summary>
        /// <param name="envelope">The envelope.</param>
        void SendEvent(EntityEnvelope envelope);

        /// <summary>
        /// Gets the interested peer.
        /// </summary>
        /// <param name="address">The address.</param>
        /// <returns>The interested peer obejct.</returns>
        InterestedPeer GetInterestedPeer(PeerAddress address);

        /// <summary>
        /// Gets the update.
        /// </summary>
        /// <param name="includedParts">The included parts.</param>
        /// <param name="remainingParts">The remaining parts.</param>
        /// <returns>The update data.</returns>
        byte[] GetUpdate(ref BooleanArray includedParts, out BooleanArray remainingParts);

        /// <summary>
        /// Gets the complete update.
        /// </summary>
        /// <param name="includedParts">The included parts.</param>
        /// <param name="remainingParts">The remaining parts.</param>
        /// <returns>The update data.</returns>
        byte[] GetCompleteUpdate(out BooleanArray includedParts, out BooleanArray remainingParts);

        /// <summary>
        /// Processes the acknowledgements.
        /// </summary>
        /// <param name="id">The source id.</param>
        /// <param name="includedSegments">The included segments.</param>
        /// <param name="sequenceNumbers">The sequence numbers.</param>
        void ProcessAcknowledgements(BadumnaId id, BooleanArray includedSegments, List<CyclicalID.UShortID> sequenceNumbers);

        /// <summary>
        /// Serializes the parts.
        /// </summary>
        /// <param name="includedParts">The included parts.</param>
        /// <param name="remainingParts">The remaining parts.</param>
        /// <returns>The serialized part.</returns>
        byte[] SerializeParts(BooleanArray includedParts, out BooleanArray remainingParts);
    }
}
