﻿//---------------------------------------------------------------------------------
// <copyright file="UpdateResendEvent.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using Badumna.Utilities;
using Badumna.DataTypes;

namespace Badumna.Replication
{
    /// <summary>
    /// Segment state. 
    /// </summary>
    internal class SegmentState
    {
        public CyclicalID.UShortID LastChangeSequenceNumber;
        public List<BadumnaId> UnacknowledgedPeers = new List<BadumnaId>();
        public ResendEvent ResendEvent;
    }

    /// <summary>
    /// Resend event.
    /// </summary>
    internal class ResendEvent : IComparable<ResendEvent>
    {
        public TimeSpan Time { get; private set; }
        public BooleanArray Segments { get; private set; }

        public ResendEvent(TimeSpan time, BooleanArray segments)
        {
            this.Time = time;
            this.Segments = new BooleanArray(segments);
        }

        public int CompareTo(object obj)
        {
            if (obj == null)
            {
                return 1;  // Any object is greater than null
            }

            if (!(obj is ResendEvent))
            {
                throw new ArgumentException("obj");
            }

            return this.Time.CompareTo(((ResendEvent)obj).Time);
        }

        public int CompareTo(ResendEvent other)
        {
            return this.Time.CompareTo(other.Time);
        }
    }
}
