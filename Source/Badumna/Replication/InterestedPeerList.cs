using System;
using System.Collections.Generic;
using System.Diagnostics;
using Badumna.Core;
using Badumna.DataTypes;
using Badumna.Overload;
using Badumna.SpatialEntities;
using Badumna.Utilities;
using Badumna.Transport;
using Badumna.Utilities.Logging;

namespace Badumna.Replication
{
    // TODO: Need to adjust entity timeout due to lack of updates according to current flow control and load
    //       on owning peer.  e.g. If flow control says send at rate lower than rate needed to not time out,
    //       then entity will time out.  Or if owning peer has to send to so many interested peers that it
    //       doesn't have enough bandwidth to send at the minimum rate, then entity will time out on all
    //       (and cause a large number of instatiation requests / responses).

    // TODO: Only engage overload if at least two peers are on the overload.  Maybe implement this in ForwardingGroup.ForwardEnvelope().

    // TODO: interested entities are addressed by both badumna id and InterestedEntity in this class. should be consistent. 

    /// <summary>
    /// Maintains a list of peers and entities on those peers that are interested in a given peer. 
    /// </summary>
    internal class InterestedPeerList
    {
        /// <summary>
        /// The entity manager.
        /// </summary>
        private IEntityManager mEntityManager;

        /// <summary>
        /// The entity id of the original entity.
        /// </summary>
        private BadumnaId mSourceEntityId;

        /// <summary>
        /// All interested peers.
        /// </summary>
        private Dictionary<PeerAddress, InterestedPeer> mInterestedPeers;

        /// <summary>
        /// All update channels.
        /// </summary>
        private UpdateChannelManager updateChannels;

        /// <summary>
        /// The update method manager.
        /// </summary>
        private UpdateMethodManager updateMethodManager;

        /// <summary>
        /// Overloaded entities.
        /// </summary>
        private List<InterestedEntity> mOverloadEntities;

        /// <summary>
        /// The forwarding group used for delivering updates. 
        /// </summary>
        private IForwardingGroup mForwardingGroup;

        /// <summary>
        /// The regular task for checking overload/underload. 
        /// </summary>
        private RegularTask mCheckOverloadTask;

        /// <summary>
        /// Rate that updates are generated in bytes/sec.  We need this much bandwidth on each connection that's interested.
        /// </summary>
        private RateSmoother mUpdateRate;

        /// <summary>
        /// The update frequency manager.
        /// </summary>
        private UpdateFrequencyManager updateFrequencyManager;

        /// <summary>
        /// The switching manager.
        /// </summary>
        private OverloadSwitchingManager overloadSwitchingManager;

        /// <summary>
        /// Provides access to the current time.
        /// </summary>
        private ITime timeKeeper;

        /// <summary>
        /// The configuration options for the overload system.
        /// </summary>
        private OverloadModule overloadOptions;

        /// <summary>
        /// Initializes a new instance of the <see cref="InterestedPeerList"/> class.
        /// </summary>
        /// <param name="entityManager">The entity manager.</param>
        /// <param name="sourceEntityId">The source entity id.</param>
        /// <param name="overloadOptions">The configuration options for the overload system.</param>
        /// <param name="eventQueue">The event queue to schedule events on.</param>
        /// <param name="timeKeeper">The time source.</param>
        /// <param name="connectivityReporter">Reports on network connectivity status.</param>
        /// <param name="connectionTable">The connection table.</param>
        public InterestedPeerList(
            IEntityManager entityManager,
            BadumnaId sourceEntityId,
            OverloadModule overloadOptions,
            NetworkEventQueue eventQueue,
            ITime timeKeeper,
            INetworkConnectivityReporter connectivityReporter,
            IChannelFunnel<TransportEnvelope> connectionTable)
        {
            this.timeKeeper = timeKeeper;

            this.overloadOptions = overloadOptions;

            this.mInterestedPeers = new Dictionary<PeerAddress, InterestedPeer>();
            this.updateChannels = new UpdateChannelManager(entityManager, eventQueue, timeKeeper);
            this.mOverloadEntities = new List<InterestedEntity>();
            this.overloadSwitchingManager = new OverloadSwitchingManager(this.mInterestedPeers, this.updateChannels, this.mOverloadEntities, this.overloadOptions, this.timeKeeper, connectionTable);
            this.updateFrequencyManager = new UpdateFrequencyManager(this.timeKeeper);
            this.mUpdateRate = new RateSmoother(Parameters.BandwidthSamplingInterval, Parameters.BandwidthEstimateSettlingTime, timeKeeper);
            
            this.mEntityManager = entityManager;
            this.mSourceEntityId = sourceEntityId;

            // if overload is enable then create the forwarding group and the regular task that will assign remote peers to the
            // forwarding group. 
            if (this.mEntityManager.ForwardingManager != null && this.mEntityManager.ForwardingManager.IsForwardingEnabled)
            {
                this.mForwardingGroup = this.mEntityManager.ForwardingManager.CreateForwardingGroup(this.mSourceEntityId);
                this.mForwardingGroup.ForwardingGroupLost += this.ForwardingGroupLostEventHandler;
                this.mForwardingGroup.OnForwradingPeerBecomeOffline = this.mEntityManager.OnForwradingPeerBecomeOffline;
                this.mCheckOverloadTask = new RegularTask("Check entity overload", Parameters.OverloadCheckPeriod, eventQueue, connectivityReporter, this.OverloadGroupMaintainceTask);
                // TODO: Test this task
                this.mCheckOverloadTask.Start();
            }

            this.updateMethodManager = new UpdateMethodManager(this.mInterestedPeers, this.updateChannels, this.mOverloadEntities, this.mForwardingGroup, this.overloadOptions);

            connectivityReporter.NetworkOfflineEvent += this.Shutdown;
        }

        /// <summary>
        /// Gets the interested count.
        /// </summary>
        /// <value>The interested count.</value>
        public int InterestedCount
        {
            get
            {
                int count = 0;

                foreach (InterestedPeer peer in this.mInterestedPeers.Values)
                {
                    count += peer.Count;
                }

                return count;
            }
        }

        /// <summary>
        /// Gets the interested regions.
        /// </summary>
        /// <value>The interested regions.</value>
        public IEnumerable<BadumnaId> InterestedRegions
        {
            get
            {
                List<BadumnaId> list = new List<BadumnaId>();
                foreach (InterestedPeer peer in this.mInterestedPeers.Values)
                {
                    foreach (InterestedEntity entity in peer)
                    {
                        list.Add(entity.EntityId);
                    }
                }

                return list;
            }
        }

        /// <summary>
        /// Gets a value indicating whether forwarding peer is available.
        /// </summary>
        /// <value>
        ///  <c>true</c> if [forwarding peer available]; otherwise, <c>false</c>.
        /// </value>
        public bool ForwardingPeerAvailable
        {
            get { return this.mForwardingGroup.IsForwardingPeerAvailable; }
        }

        /// <summary>
        /// Switches to new forwarding peer.
        /// </summary>
        /// <param name="address">The address.</param>
        public void SwitchToNewForwardingPeer(PeerAddress address)
        {
            if (null != this.mForwardingGroup)
            {
                this.mForwardingGroup.SwitchToNewForwardingPeer(address);
            }
            else
            {
                Logger.TraceInformation(LogTag.InterestManagement | LogTag.Event, "forwarding group not available.");
            }
        }

        /// <summary>
        /// Shutdowns this instance.
        /// </summary>
        public void Shutdown()
        {
            if (null != this.mCheckOverloadTask && this.mCheckOverloadTask.IsRunning)
            {
                Logger.TraceInformation(LogTag.InterestManagement | LogTag.Event, "Going to stop the check overload task.");
                this.mCheckOverloadTask.Stop();
                this.updateMethodManager.ClearOverloadDestinations();
            }

            this.updateChannels.ClearUpdateChannels();
            this.updateMethodManager.Shutdown();
            this.updateFrequencyManager.Reset();
        }

        /// <summary>
        /// Asserts the forwarding available.
        /// </summary>
        private void AssertForwardingAvailable()
        {
            if (this.mForwardingGroup == null)
            {
                Debug.Assert(false, "No forwarding group");
                throw new InvalidOperationException("No forwarding group");
            }
        }

        /// <summary>
        /// Adds the interested entity.
        /// </summary>
        /// <param name="entityId">The id of the entity.</param>
        public void AddInterestedEntity(BadumnaId entityId)
        {
            InterestedPeer interestedPeer;
            if (!this.mInterestedPeers.TryGetValue(entityId.Address, out interestedPeer))
            {
                Logger.TraceInformation(LogTag.Event | LogTag.InterestManagement, "Creating new interested peer " + entityId.Address);
                interestedPeer = new InterestedPeer(this.timeKeeper);
                this.mInterestedPeers[entityId.Address] = interestedPeer;

                // default update method is direct send. 
                interestedPeer.Add(entityId);
                this.updateChannels.AddUpdateChannel(entityId.Address);
            }
            else
            {
                Logger.TraceInformation(LogTag.Event | LogTag.InterestManagement, "Adding to already-interested peer " + entityId.Address);
                interestedPeer.Add(entityId);
            }

            this.TraceInterestedPeers();
        }

        /// <summary>
        /// Determines whether is interested in the specified entity.
        /// </summary>
        /// <param name="entityId">The id of the specified entity.</param>
        /// <returns>
        /// <c>true</c> if is interested in the specified entity; otherwise, <c>false</c>.
        /// </returns>
        public bool IsInterestedIn(BadumnaId entityId)
        {
            InterestedPeer interestedPeer;
            if (this.mInterestedPeers.TryGetValue(entityId.Address, out interestedPeer))
            {
                foreach (InterestedEntity entity in interestedPeer)
                {
                    if (entity.EntityId.Equals(entityId))
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// Removes the interested entity.
        /// </summary>
        /// <param name="entityId">The id of the specified entity.</param>
        public void RemoveInterestedEntity(BadumnaId entityId)
        {
            InterestedPeer interestedPeer;
            if (!this.mInterestedPeers.TryGetValue(entityId.Address, out interestedPeer))
            {
                // not interested at all. 
                return;
            }

            if (this.updateMethodManager.HasOverloaded(entityId))
            {
                this.updateMethodManager.RemoveOverloadDestination(entityId);
            }

            if (interestedPeer.Count == 1)
            {
                if (interestedPeer.Remove(entityId))
                {
                    this.mInterestedPeers.Remove(entityId.Address);

                    // the channel is no longer required.
                    UpdateChannel updateChannel;
                    if (this.updateChannels.TryGetValue(entityId.Address, out updateChannel))
                    {
                        this.updateChannels.RemoveUpdateChannel(updateChannel);
                    }
                }
            }
            else
            {
                // there is still other entities on that remote peer are interested in my updates
                // so keep the update channel. 
                interestedPeer.Remove(entityId);
                return;
            }

            this.TraceInterestedPeers();
        }

        [Conditional("DEBUG")]
        private void TraceInterestedPeers()
        {
            var peerDescriptions = CollectionUtils.Map(this.mInterestedPeers, (pair) => "<#Peer " + pair.Key + " with entities: [" + CollectionUtils.Join(pair.Value, ", ") + "]>");
            Logger.TraceInformation(LogTag.State | LogTag.InterestManagement, "Network entity {0} interested = [{1}]", mSourceEntityId, CollectionUtils.Join(peerDescriptions, ", "));
        }

        /// <summary>
        /// The maintaince task for overload groups.
        /// </summary>
        private void OverloadGroupMaintainceTask()
        {
            this.CheckForOverload();

            if (!this.overloadOptions.IsForced)
            {
                this.CheckForUnderload();
            }
        }

        /// <summary>
        /// Checks for overload.
        /// </summary>
        private void CheckForOverload()
        {
            List<UpdateChannel> overloaded = this.overloadSwitchingManager.GetOverloadedChannel();
            
            // after switching to overload, how many overloaded destinations will be? 
            int totalOverloaded = this.mOverloadEntities.Count + overloaded.Count;
            bool shouldSwitch = this.updateMethodManager.ShouldSwitchToOverload(totalOverloaded);

            // only switch to overload server if the server is known to be online.
            // if there is a total of 1 overloaded destination, then it does not make sense to switch to overload 
            if (this.mForwardingGroup.ConfirmConnected && shouldSwitch)
            {
                this.updateMethodManager.SwitchToOverloadMode(overloaded);
            }
        }

        /// <summary>
        /// Checks for underloaded destinations.
        /// </summary>
        private void CheckForUnderload()
        {
            List<InterestedEntity> changeToSwitchingBackMethod = null;
            List<InterestedEntity> changeToDirectMethod = null;

            this.overloadSwitchingManager.GetUnderloaded(out changeToSwitchingBackMethod, out changeToDirectMethod);
            this.updateMethodManager.SwitchToDirectMode(changeToDirectMethod);
            this.updateMethodManager.SwitchToSwitchingBackMethod(changeToSwitchingBackMethod);
        }
        
        /// <summary>
        /// Forwards the entity envelope to the forwarding group.
        /// </summary>
        /// <param name="envelope">The entity envelope.</param>
        private void ForwardEnvelopeToForwardingGroup(EntityEnvelope envelope)
        {
            if (!this.mForwardingGroup.IsForwardingPeerAvailable)
            {
                // TODO :
                // the fowarding peer associated with the current mForwardingGroup
                // is no longer available, need to handle this. the group settings
                // in this current mForwardingGroup need to be kept after changing
                // to another forwarding peer (ForwardGroup.Import).
            }

            this.mForwardingGroup.ForwardEnvelope(envelope);
        }

        /// <summary>
        /// Dispatches the entity envelope to all interested entities.
        /// </summary>
        /// <param name="envelope">The entity envelope.</param>
        public void DispatchToInterested(EntityEnvelope envelope)
        {
            foreach (KeyValuePair<PeerAddress, InterestedPeer> item in this.mInterestedPeers)
            {
                foreach (InterestedEntity entity in item.Value)
                {
                    envelope.DestinationEntity = entity.EntityId;
                    this.mEntityManager.Router.DirectSend(envelope);
                }
            }
        }

        /// <summary>
        /// Schedules the update.
        /// </summary>
        /// <param name="sequenceNumber">The sequence number of the update.</param>
        /// <param name="includedParts">The included parts in the update.</param>
        /// <param name="update">The update buffer.</param>
        /// <param name="sourceId">The id of the sending entity.</param>
        public void ScheduleUpdate(CyclicalID.UShortID sequenceNumber, BooleanArray includedParts, byte[] update, BadumnaId sourceId)
        {
            // this envelope will be used if there is only one entity per channel
            EntityEnvelope singleEntityUpdateEnvelope = this.mEntityManager.GetMessageFrom(sourceId, QualityOfService.Unreliable);
            // this is an entity update message, it is only valid within a short period of time, set the realtime flag to be true
            // to ensure it won't be delivered after long delay. 
            singleEntityUpdateEnvelope.Qos.IsRealtimeMessage = true;
            this.mEntityManager.RemoteCallUpdateEntity(singleEntityUpdateEnvelope, sequenceNumber, includedParts, update);

            // schedule the update
            foreach (KeyValuePair<PeerAddress, InterestedPeer> kvp in this.mInterestedPeers)
            {
                if (!this.updateFrequencyManager.ShouldUpdateReplica(kvp.Key, includedParts))
                {
                    continue;
                }

                List<BadumnaId> targets = new List<BadumnaId>();
                foreach (InterestedEntity entity in kvp.Value)
                {
                    targets.Add(entity.EntityId);
                }

                if (targets.Count > 1)
                {
                    // need a new envelope as the content (i.e. the targets parameter) will be different. 
                    EntityEnvelope updateEnvelope = this.mEntityManager.GetMessageFor(sourceId, targets[0], QualityOfService.Unreliable);
                    updateEnvelope.Qos.IsRealtimeMessage = true;
                    this.mEntityManager.RemoteCallUpdateMultipleEntities(updateEnvelope, sequenceNumber, includedParts, update, targets);
                    if (this.updateChannels.ContainsKey(kvp.Key))
                    {
                        UpdateChannel channel = this.updateChannels[kvp.Key];
                        channel.NewUpdate(updateEnvelope);
                    }
                }
                else if (targets.Count == 1)
                {
                    if (this.updateChannels.ContainsKey(kvp.Key))
                    {
                        singleEntityUpdateEnvelope.DestinationEntity = targets[0];
                        UpdateChannel channel = this.updateChannels[kvp.Key];

                        channel.NewUpdate(singleEntityUpdateEnvelope);
                    }
                }
                else
                {
                    // no entity
                    Logger.TraceWarning(LogTag.InterestManagement | LogTag.Event, "No entity is assocated with this channel.");
                }
            }

            if (this.mForwardingGroup != null && this.mOverloadEntities.Count > 0 && singleEntityUpdateEnvelope != null)
            {
                // Ensure there's an address for constructing the transport envelope (it's not used, but throws a null ref exception if none is set)
                singleEntityUpdateEnvelope.DestinationEntity = new BadumnaId(this.overloadOptions.OverloadPeerAddress, 0);
                this.ForwardEnvelopeToForwardingGroup(singleEntityUpdateEnvelope);
            }

            if (null != singleEntityUpdateEnvelope)
            {
                // TODO: Might need to add some allowance for TransportEnvelope overhead, but it's probably not critical.
                // TODO: should we call value event for each envelope? 
                this.mUpdateRate.ValueEvent(singleEntityUpdateEnvelope.Length);
            }
        }

        /// <summary>
        /// Checks for timeout peers.
        /// </summary>
        public List<Pair<BadumnaId, BadumnaId>> GetTimeOutPeers()
        {
            List<InterestedPeer> toRemove = new List<InterestedPeer>();
            foreach (KeyValuePair<PeerAddress, InterestedPeer> interestedPeer in this.mInterestedPeers)
            {
                // Check that we're receiving flow control feedback
                if (interestedPeer.Value.TimedOut)
                {
                    toRemove.Add(interestedPeer.Value);
                }
            }

            List<BadumnaId> toRemoveId = new List<BadumnaId>();
            foreach (InterestedPeer peer in toRemove)
            {
                foreach (InterestedEntity entity in peer)
                {
                    toRemoveId.Add(entity.EntityId);
                }
            }

            List<Pair<BadumnaId, BadumnaId>> result = new List<Pair<BadumnaId, BadumnaId>>();
            foreach (BadumnaId id in toRemoveId)
            {
                Pair<BadumnaId, BadumnaId> curPair = new Pair<BadumnaId, BadumnaId>(this.mSourceEntityId, id);
                result.Add(curPair);
            }

            return result;
        }

        public InterestedPeer GetInterestedPeer(PeerAddress address)
        {
            InterestedPeer peer;
            if (this.mInterestedPeers.TryGetValue(address, out peer))
            {
                return peer;
            }

            return null;
        }

        /// <summary>
        /// The event handler for the forwarding group lost event.
        /// </summary>
        /// <param name="addedEntities">The affected entities.</param>
        private void ForwardingGroupLostEventHandler(List<BadumnaId> addedEntities)
        {
            foreach (BadumnaId entity in addedEntities)
            {
                if (this.updateMethodManager.HasOverloaded(entity))
                {
                    foreach (InterestedEntity curEntity in this.mOverloadEntities)
                    {
                        if (curEntity.EntityId.Equals(entity))
                        {
                            curEntity.SwitchUpdateMethod(UpdateDeliveryMethod.Direct);
                        }
                    }

                    this.updateMethodManager.RemoveOverloadDestination(entity);
                }

                if (this.mInterestedPeers.ContainsKey(entity.Address))
                {
                    InterestedPeer peer = this.mInterestedPeers[entity.Address];
                    if (!peer.Contains(entity))
                    {
                        peer.Add(entity);
                    }
                }
                else
                {
                    InterestedPeer peer = new InterestedPeer(this.timeKeeper);
                    peer.Add(entity);
                    this.mInterestedPeers[entity.Address] = peer;
                }

                if (!this.updateChannels.ContainsKey(entity.Address))
                {
                    this.updateChannels.AddUpdateChannel(entity.Address);
                }
            }
        }

        // TODO : Use the given attention metric in prioritization of updates.
        /// <summary>
        /// Applies the flow control feedback.
        /// </summary>
        /// <param name="interestRegionSource">The interest region source.</param>
        /// <param name="maximumUpdateRate">The maximum update rate.</param>
        /// <param name="attention">The attention.</param>
        public void ApplyFlowControlFeedback(PeerAddress interestRegionSource, ushort maximumUpdateRate, float attention)
        {
            InterestedPeer interestedPeer;
            if (this.mInterestedPeers.TryGetValue(interestRegionSource, out interestedPeer))
            {
                interestedPeer.ApplyFlowControlFeedback(maximumUpdateRate, attention);
            }

            UpdateChannel info;
            if (this.updateChannels.TryGetValue(interestRegionSource, out info))
            {
                info.ApplyFlowControlFeedback(maximumUpdateRate, attention);
            }
        }
    }
}
