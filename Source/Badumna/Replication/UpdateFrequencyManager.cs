﻿//---------------------------------------------------------------------------------
// <copyright file="UpdateFrequencyManager.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using Badumna.Core;
using Badumna.SpatialEntities;
using Badumna.Utilities;

namespace Badumna.Replication
{
    /// <summary>
    /// The update frequency manager determines whether the remote replica needs to be updated. 
    /// </summary>
    internal class UpdateFrequencyManager
    {
        /// <summary>
        /// The last update time when sending updates to NPCs.
        /// </summary>
        private Dictionary<PeerAddress, TimeSpan> npcLastUpdateTime;

        /// <summary>
        /// Provides access to the current time.
        /// </summary>
        private ITime timeKeeper;

        /// <summary>
        /// Initializes a new instance of the <see cref="UpdateFrequencyManager"/> class.
        /// </summary>
        /// <param name="timeKeeper">Provides access to the current time.</param>
        public UpdateFrequencyManager(ITime timeKeeper)
        {
            this.timeKeeper = timeKeeper;
            this.Reset();
        }

        /// <summary>
        /// Resets this instance.
        /// </summary>
        public void Reset()
        {
            this.npcLastUpdateTime = new Dictionary<PeerAddress, TimeSpan>();
        }

        /// <summary>
        /// Whether the update should be sent to NPCs. We assume most updates are position and velocity, by dropping those redundent
        /// position and velocity updates, all other updates get the chance to be delivered faster and also save bandwidth. This is
        /// also based on the assumption that NPCs do not need to have replicas associated with them to be updated dozen times per seconds.
        /// </summary>
        /// <param name="address">The address.</param>
        /// <param name="includedParts">The included parts.</param>
        /// <returns>Whether the update should be sent.</returns>
        public bool ShouldUpdateReplica(PeerAddress address, BooleanArray includedParts)
        {
            // update sent to regular peers will also be allowed. 
            if (!address.IsDhtAddress)
            {
                return true;
            }

            // updates that contains none position/velocity info to NPCs will always be allowed. 
            for (int i = 0; i < includedParts.HighestUsedIndex; i++)
            {
                if (includedParts[i] && i != (int)SpatialEntityStateSegment.Position && i != (int)SpatialEntityStateSegment.Velocity)
                {
                    return true;
                }
            }

            TimeSpan lastUpdateTime;
            TimeSpan now = this.timeKeeper.Now;
            if (!this.npcLastUpdateTime.TryGetValue(address, out lastUpdateTime))
            {
                this.npcLastUpdateTime[address] = now;
                return true;
            }
            else
            {
                if (now - lastUpdateTime > TimeSpan.FromSeconds(2))
                {
                    this.npcLastUpdateTime[address] = now;
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
    }
}
