//------------------------------------------------------------------------------
// <copyright file="EntityEnvelope.cs" company="National ICT Australia Limited">
//     Copyright (c) 2012 National ICT Australia Limited. All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System;

using Badumna.Core;
using Badumna.DataTypes;

namespace Badumna.Replication
{
    /// <summary>
    /// An envelope for messages from/to entities.
    /// </summary>
    internal class EntityEnvelope : BaseEnvelope
    {
        /// <summary>
        /// The local time that this instance was constructed.
        /// </summary>
        private DateTime constructionTime;

        /// <summary>
        /// Initializes a new instance of the EntityEnvelope class.
        /// </summary>
        /// <remarks>
        /// This constructor is intended for use when deserializing only.
        /// </remarks>
        public EntityEnvelope()
        {
            this.constructionTime = DateTime.Now;
        }

        /// <summary>
        /// Initializes a new instance of the EntityEnvelope class with a specific source entity.
        /// </summary>
        /// <param name="sourceEntityId">The source entity id</param>
        /// <param name="qos">The quality of service</param>
        public EntityEnvelope(BadumnaId sourceEntityId, QualityOfService qos)
            : this()
        {
            this.SourceEntity = sourceEntityId;
            this.Qos = qos;
        }

        /// <summary>
        /// Initializes a new instance of the EntityEnvelope class with specific source and destination entities.
        /// </summary>
        /// <param name="destination">The destination entity id</param>
        /// <param name="source">The source entity id</param>
        /// <param name="qos">The quality of service</param>
        public EntityEnvelope(BadumnaId destination, BadumnaId source, QualityOfService qos)
            : this()
        {
            this.DestinationEntity = destination;
            this.SourceEntity = source;
            this.Qos = qos;
        }

        /// <summary>
        /// Initializes a new instance of the EntityEnvelope class by copying another instance.
        /// </summary>
        /// <param name="other">The instance to copy</param>
        public EntityEnvelope(EntityEnvelope other)
            : base(other)
        {
            this.DestinationEntity = other.DestinationEntity;
            this.SourceEntity = other.SourceEntity;
            this.constructionTime = other.constructionTime;
            this.SourceQueuingDelay = other.SourceQueuingDelay;
        }

        /// <summary>
        /// Gets or sets the id of the source entity.
        /// </summary>
        public BadumnaId SourceEntity { get; set; }

        /// <summary>
        /// Gets or sets the id of the destination entity.
        /// </summary>
        public BadumnaId DestinationEntity { get; set; }
        
        /// <summary>
        /// Gets the amount of time between construction and serialization on the source peer.
        /// </summary>
        /// <remarks>
        /// TODO: Check if calculation of this delay is really necessary (probably typically very small, and costs bandwidth to send it)
        /// </remarks>
        public TimeSpan SourceQueuingDelay { get; private set; }

        /// <inheritdoc />
        public override void ToMessage(MessageBuffer message, Type parameterType)
        {
            if (null != message)
            {
                base.ToMessage(message, typeof(BaseEnvelope));
                this.SourceQueuingDelay = DateTime.Now - this.constructionTime;
                message.Write((ushort)this.SourceQueuingDelay.TotalMilliseconds);
            }
        }

        /// <inheritdoc />
        public override void FromMessage(MessageBuffer message)
        {
            if (null != message)
            {
                base.FromMessage(message);

                this.SourceQueuingDelay = TimeSpan.FromMilliseconds(message.ReadUShort());
            }
        }
    }
}
