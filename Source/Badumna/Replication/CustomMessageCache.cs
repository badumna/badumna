﻿// -----------------------------------------------------------------------
// <copyright file="CustomMessageCache.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2013 Scalify Pty Ltd. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace Badumna.Replication
{
    using System.Collections.Generic;
    using Badumna.DataTypes;
    using Badumna.Utilities;

    /// <summary>
    /// For caching custom messages for entities that have not been instantiated yet.
    /// </summary>
    internal class CustomMessageCache
    {
        /// <summary>
        /// List of entities with cached messages miantained independently of dictionary
        /// keys to avoid re-populating each tick.
        /// </summary>
        private List<BadumnaId> entitiesWithCachedMessages = new List<BadumnaId>(5);

        /// <summary>
        /// Separate list for returning to clients to allow modifications during iteration.
        /// </summary>
        private List<BadumnaId> returnedEntitiesWithCachedMessages = new List<BadumnaId>(5);

        /// <summary>
        /// Cached messages by Entity ID.
        /// </summary>
        private Dictionary<BadumnaId, List<CachedCustomMessage>> customMessagesByEntity =
            new Dictionary<BadumnaId, List<CachedCustomMessage>>();

        /// <summary>
        /// Stored as field to prevent short lived object thrashing triggering garbage collection.
        /// </summary>
        private List<BadumnaId> customMessageCachesToGarbageCollect = new List<BadumnaId>(5);

        /// <summary>
        /// Gets IDs of entities with cached custom messages.
        /// </summary>
        /// <returns>A collection of IDs of entities who currently have cached messages.</returns>
        public IEnumerable<BadumnaId> GetEntitiesWithCachedMessages()
        {
            // Locked to prevent concurrent iterations which are not supported.
            lock (this.returnedEntitiesWithCachedMessages)
            {
                this.returnedEntitiesWithCachedMessages.Clear();
                this.returnedEntitiesWithCachedMessages.AddRange(this.entitiesWithCachedMessages);
                return this.returnedEntitiesWithCachedMessages;
            }
        }

        /// <summary>
        /// Cache a message for an entity.
        /// </summary>
        /// <param name="entityId">The ID of the entity.</param>
        /// <param name="sequenceNumber">The sequence number of the message.</param>
        /// <param name="messageData">The message data.</param>
        public void Cache(BadumnaId entityId, CyclicalID.UShortID sequenceNumber, byte[] messageData)
        {
            // Called on network thread. Locked to prevent concurrent popping on application thread.
            lock (this.customMessagesByEntity)
            {
                List<CachedCustomMessage> customMessages = null;
                if (!this.customMessagesByEntity.TryGetValue(entityId, out customMessages))
                {
                    customMessages = new List<CachedCustomMessage>(5);
                    this.customMessagesByEntity.Add(entityId, customMessages);
                    this.entitiesWithCachedMessages.Add(entityId);
                }

                customMessages.Add(new CachedCustomMessage(sequenceNumber, messageData));
            }
        }

        /// <summary>
        /// Get the cached messages for a given entity.
        /// </summary>
        /// <param name="entityId">The iD of the entity.</param>
        /// <returns>A collection of cached messages.</returns>
        public IEnumerable<CachedCustomMessage> PopCachedMessages(BadumnaId entityId)
        {
            // Called on application thread. Locked to prevent concurrent caching or garbage
            // collection on network thread.
            lock (this.customMessagesByEntity)
            {
                var messages = this.customMessagesByEntity[entityId];
                this.customMessagesByEntity.Remove(entityId);
                this.entitiesWithCachedMessages.Remove(entityId);
                return messages;
            }
        }

        /// <summary>
        /// Clear out any cached messages that have been here since last GC.
        /// </summary>
        public void CollectGarbage()
        {
            // Called on network thread. Locked to prevent concurrent popping on application thread.
            lock (this.customMessagesByEntity)
            {
                // Since garbage collection is generational, call frequency dictates cache lifespan!
                foreach (var entity in this.customMessageCachesToGarbageCollect)
                {
                    this.customMessagesByEntity.Remove(entity);
                    this.entitiesWithCachedMessages.Remove(entity);
                }

                this.customMessageCachesToGarbageCollect.Clear();
                this.customMessageCachesToGarbageCollect.AddRange(
                    this.customMessagesByEntity.Keys);
            }
        }

        /// <summary>
        /// A cached custom message.
        /// </summary>
        internal class CachedCustomMessage
        {
            /// <summary>
            /// Initializes a new instance of the CustomMessageCache.CachedCustomMessage class.
            /// </summary>
            /// <param name="updateNumber">The message's sequence number.</param>
            /// <param name="eventData">The message data.</param>
            public CachedCustomMessage(CyclicalID.UShortID updateNumber, byte[] eventData)
            {
                this.UpdateNumber = updateNumber;
                this.EventData = eventData;
            }

            /// <summary>
            /// Gets the message's sequence number.
            /// </summary>
            public CyclicalID.UShortID UpdateNumber { get; private set; }

            /// <summary>
            /// Gets the message's data.
            /// </summary>
            public byte[] EventData { get; private set; }
        }
    }
}
