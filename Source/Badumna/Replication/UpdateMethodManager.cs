﻿//---------------------------------------------------------------------------------
// <copyright file="UpdateMethodManager.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

using Badumna.Core;
using Badumna.DataTypes;
using Badumna.Overload;
using Badumna.Utilities;
using Badumna.Utilities.Logging;

namespace Badumna.Replication
{
    using InterestedPeers = Dictionary<PeerAddress, InterestedPeer>;
    
    /// <summary>
    /// The update method manager manages the update delivery methods for all interested peers.
    /// </summary>
    internal class UpdateMethodManager
    {
        /// <summary>
        /// How many overloaded entities can be put into switching back status. 
        /// </summary>
        private const ushort NumberOfSwitchingBackSlots = 3;

        /// <summary>
        /// All overloaded entities.
        /// </summary>
        private List<InterestedEntity> overloadEntities;

        /// <summary>
        /// All interested peers.
        /// </summary>
        private InterestedPeers interestedPeers;

        /// <summary>
        /// All update channels.
        /// </summary>
        private UpdateChannelManager updateChannels;

        /// <summary>
        /// The forwarding group.
        /// </summary>
        private IForwardingGroup forwardingGroup;

        /// <summary>
        /// The number of entities currently in switching back mode. 
        /// </summary>
        private ushort currentEntitiesInSwitchingBackMode;

        /// <summary>
        /// The configuration options for the overload system.
        /// </summary>
        private OverloadModule overloadOptions;

        /// <summary>
        /// Initializes a new instance of the <see cref="UpdateMethodManager"/> class.
        /// </summary>
        /// <param name="peers">The peers.</param>
        /// <param name="updateChannels">The update channels.</param>
        /// <param name="overloadEntities">The overload entities.</param>
        /// <param name="forwardingGroup">The forwarding group.</param>
        /// <param name="overloadOptions">The configuration options for the overload system.</param>
        public UpdateMethodManager(
            InterestedPeers peers,
            UpdateChannelManager updateChannels,
            List<InterestedEntity> overloadEntities,
            IForwardingGroup forwardingGroup,
            OverloadModule overloadOptions)
        {
            this.overloadEntities = overloadEntities;
            this.interestedPeers = peers;
            this.updateChannels = updateChannels;
            this.forwardingGroup = forwardingGroup;
            this.overloadOptions = overloadOptions;

            this.currentEntitiesInSwitchingBackMode = 0;
        }

        /// <summary>
        /// Gets the current entities in switching back mode.
        /// </summary>
        /// <value>The current entities in switching back mode.</value>
        public int CurrentEntitiesInSwitchingBackMode
        {
            get { return this.currentEntitiesInSwitchingBackMode; }
        }

        /// <summary>
        /// Determines whether the specified entity id has overloaded.
        /// </summary>
        /// <param name="entityId">The entity id.</param>
        /// <returns>
        /// <c>true</c> if the specified entity id has overloaded; otherwise, <c>false</c>.
        /// </returns>
        public bool HasOverloaded(BadumnaId entityId)
        {
            foreach (InterestedEntity entity in this.overloadEntities)
            {
                if (entity.EntityId.Equals(entityId))
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Adds the overload destination.
        /// </summary>
        /// <param name="entity">The overloaded entity.</param>
        public void AddOverloadDestination(InterestedEntity entity)
        {
            if (this.HasOverloaded(entity.EntityId))
            {
                return;
            }

            Logger.TraceInformation(LogTag.Overload | LogTag.Event, "Overload add {0}", entity.EntityId);

            this.overloadEntities.Add(entity);
            this.forwardingGroup.Add(entity.EntityId);
        }

        /// <summary>
        /// Removes the overload destination.
        /// </summary>
        /// <param name="entity">The entity.</param>
        public void RemoveOverloadDestination(InterestedEntity entity)
        {
            this.RemoveOverloadDestination(entity.EntityId);
        }

        /// <summary>
        /// Removes the overload destination.
        /// </summary>
        /// <param name="entityId">The entity id.</param>
        public void RemoveOverloadDestination(BadumnaId entityId)
        {
            if (!this.HasOverloaded(entityId))
            {
                return;
            }

            Logger.TraceInformation(LogTag.Overload | LogTag.Event, "Overload remove {0}", entityId);

            InterestedEntity entityToRemove = null;
            foreach (InterestedEntity entity in this.overloadEntities)
            {
                if (entity.EntityId.Equals(entityId))
                {
                    entityToRemove = entity;
                    break;
                }
            }

            if (entityToRemove != null)
            {
                this.overloadEntities.Remove(entityToRemove);
                this.forwardingGroup.Remove(entityId);
            }
        }

        /// <summary>
        /// Clears the overload destinations.
        /// </summary>
        public void ClearOverloadDestinations()
        {
            this.forwardingGroup.RemoveAll();
            this.overloadEntities.Clear();
        }

        /// <summary>
        /// Shoulds the switch to overload.
        /// </summary>
        /// <param name="overloaded">The number of overloaded destinations.</param>
        /// <returns>Whether should switch to overload.</returns>
        public bool ShouldSwitchToOverload(int overloaded)
        {
            if (!this.forwardingGroup.ConfirmConnected)
            {
                return false;
            }

            if (overloaded == 1)
            {
                if (this.overloadOptions.IsForced)
                {
                    return true;
                }
                else
                {
                    // does not make sense to use overload server. 
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Switches to overload mode.
        /// </summary>
        /// <param name="overloaded">The overloaded channels.</param>
        public void SwitchToOverloadMode(List<UpdateChannel> overloaded)
        {
            foreach (UpdateChannel channel in overloaded)
            {
                InterestedPeer interestedPeer = null;
                if (this.interestedPeers.TryGetValue(channel.Destination, out interestedPeer))
                {
                    foreach (InterestedEntity entity in interestedPeer)
                    {
                        // the switching back peer is still overloaded. change to overload mode. 
                        if (entity.UpdateMethod == UpdateDeliveryMethod.SwitchingBack)
                        {
                            this.currentEntitiesInSwitchingBackMode--;
                            Debug.Assert(this.currentEntitiesInSwitchingBackMode >= 0, "illegal currentEntitiesInSwitchingBackMode");
                        }

                        // switch to the overload method.
                        entity.SwitchUpdateMethod(UpdateDeliveryMethod.Overload);
                        this.AddOverloadDestination(entity);
                    }

                    this.updateChannels.RemoveUpdateChannel(channel);
                }
            }
        }

        /// <summary>
        /// Switches to direct mode.
        /// </summary>
        /// <param name="changeToDirectMethod">Entities need to change to direct method.</param>
        public void SwitchToDirectMode(List<InterestedEntity> changeToDirectMethod)
        {
            foreach (InterestedEntity entity in changeToDirectMethod)
            {
                entity.SwitchUpdateMethod(UpdateDeliveryMethod.Direct);
                this.updateChannels.AddUpdateChannel(entity.EntityId.Address);
                this.RemoveOverloadDestination(entity);
                this.currentEntitiesInSwitchingBackMode--;
                Debug.Assert(this.currentEntitiesInSwitchingBackMode >= 0, "illegal currentEntitiesInSwitchingBackMode");
            }
        }

        /// <summary>
        /// Switches to switching back method.
        /// </summary>
        /// <param name="changeToSwitchingBackMethod">Entities need to change to switching back method.</param>
        public void SwitchToSwitchingBackMethod(List<InterestedEntity> changeToSwitchingBackMethod)
        {
            foreach (InterestedEntity entity in changeToSwitchingBackMethod)
            {
                if (this.currentEntitiesInSwitchingBackMode < NumberOfSwitchingBackSlots)
                {
                    // do not remove from overloaded list.
                    entity.SwitchUpdateMethod(UpdateDeliveryMethod.SwitchingBack);
                    this.updateChannels.AddUpdateChannel(entity.EntityId.Address);
                    this.currentEntitiesInSwitchingBackMode++;
                }
            }
        }

        /// <summary>
        /// Shutdowns this instance.
        /// </summary>
        public void Shutdown()
        {
            this.currentEntitiesInSwitchingBackMode = 0;
        }
    }
}
