﻿//-----------------------------------------------------------------------
// <copyright file="IReplicaWrapper.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.IO;
using Badumna.DataTypes;
using Badumna.Utilities;

namespace Badumna.Replication
{
    /// <summary>
    /// The replica wrapper interface.
    /// </summary>
    internal interface IReplicaWrapper
    {
        /// <summary>
        /// Gets the expire time.
        /// </summary>
        /// <value>The expire time.</value>
        TimeSpan ExpireTime
        {
            get;
        }

        /// <summary>
        /// Gets a value indicating whether this instance is expired.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is expired; otherwise, <c>false</c>.
        /// </value>
        bool IsExpired
        {
            get;
        }

        /// <summary>
        /// Gets the replica id.
        /// </summary>
        /// <value>The replica id.</value>
        BadumnaId Guid
        {
            get;
        }

        /// <summary>
        /// Synchronizes the specified obj.
        /// </summary>
        /// <param name="obj">The obj to use for synchronization.</param>
        void Synchronize(object obj);

        /// <summary>
        /// Shutdowns this instance.
        /// </summary>
        void Shutdown();

        /// <summary>
        /// Queues the custom message.
        /// </summary>
        /// <param name="updateNumber">The update number.</param>
        /// <param name="messageData">The message data.</param>
        void QueueCustomMessage(CyclicalID.UShortID updateNumber, MemoryStream messageData);

        /// <summary>
        /// Processes the custom messages.
        /// </summary>
        void ProcessCustomMessages();

        /// <summary>
        /// Applies the update.
        /// </summary>
        /// <param name="sequenceNumber">The sequence number.</param>
        /// <param name="includedParts">The included parts.</param>
        /// <param name="update">The update.</param>
        /// <param name="estimatedDelay">The estimated delay.</param>
        /// <param name="id">The source id.</param>
        /// <returns>The attention value.</returns>
        double ApplyUpdate(CyclicalID.UShortID sequenceNumber, BooleanArray includedParts, byte[] update, int estimatedDelay, BadumnaId id);
    }
}
