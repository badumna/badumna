﻿//---------------------------------------------------------------------------------
// <copyright file="UpdateChannelManager.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using Badumna.Core;
using Badumna.Utilities;
using Badumna.Utilities.Logging;

namespace Badumna.Replication
{
    /// <summary>
    /// The update channel manager manages all update channels.
    /// </summary>
    internal class UpdateChannelManager : IEnumerable<UpdateChannel>
    {
        /// <summary>
        /// All update channels for sending direct updates. 
        /// </summary>
        private Dictionary<PeerAddress, UpdateChannel> updateChannels;

        /// <summary>
        /// The entity manager.
        /// </summary>
        private IEntityManager entityManager;

        /// <summary>
        /// The queue to use for scheduling events.
        /// </summary>
        private NetworkEventQueue eventQueue;

        /// <summary>
        /// Provides access to the current time.
        /// </summary>
        private ITime timeKeeper;

        /// <summary>
        /// Initializes a new instance of the <see cref="UpdateChannelManager"/> class.
        /// </summary>
        /// <param name="entityManager">The entity manager.</param>
        /// <param name="eventQueue">The queue to use for scheduling events.</param>
        /// <param name="timeKeeper">Provides access to the current time.</param>
        public UpdateChannelManager(IEntityManager entityManager, NetworkEventQueue eventQueue, ITime timeKeeper)
        {
            this.entityManager = entityManager;
            this.eventQueue = eventQueue;
            this.timeKeeper = timeKeeper;
            this.updateChannels = new Dictionary<PeerAddress, UpdateChannel>();
        }

        /// <summary>
        /// Gets or sets the <see cref="Badumna.Replication.UpdateChannel"/> with the specified address.
        /// </summary>
        /// <param name="address">The address.</param>
        /// <value>update channel</value>
        public UpdateChannel this[PeerAddress address]
        {
            get
            {
                return this.updateChannels[address];
            }

            set
            {
                this.updateChannels[address] = value;
            }
        }

        /// <summary>
        /// Adds the update channel.
        /// </summary>
        /// <param name="destination">The destination of the update channel.</param>
        public void AddUpdateChannel(PeerAddress destination)
        {
            if (this.updateChannels.ContainsKey(destination))
            {
                Logger.TraceInformation(LogTag.Replication, "update channel for {0} already added. ignoring", destination);
                return;
            }

            UpdateChannel updateChannel = new UpdateChannel(destination, this.entityManager.Router, this.eventQueue, this.timeKeeper);
            this.updateChannels[destination] = updateChannel;
            this.entityManager.AddEntityChannel(destination, updateChannel);
        }

        /// <summary>
        /// Removes the update channel.
        /// </summary>
        /// <param name="updateChannel">The update channel.</param>
        public void RemoveUpdateChannel(UpdateChannel updateChannel)
        {
            if (this.updateChannels.ContainsKey(updateChannel.Destination))
            {
                this.updateChannels.Remove(updateChannel.Destination);
                this.entityManager.RemoveEntityChannel(updateChannel.Destination, updateChannel);
            }
        }

        /// <summary>
        /// Clears the update channels.
        /// </summary>
        public void ClearUpdateChannels()
        {
            foreach (KeyValuePair<PeerAddress, UpdateChannel> channel in this.updateChannels)
            {
                this.entityManager.RemoveEntityChannel(channel.Key, channel.Value);
            }

            this.updateChannels.Clear();
        }

        /// <summary>
        /// Returns an enumerator that iterates through the collection.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.Collections.Generic.IEnumerator`1"/> that can be used to iterate through the collection.
        /// </returns>
        public IEnumerator<UpdateChannel> GetEnumerator()
        {
            return new Enumerator(this.updateChannels.Values.GetEnumerator());
        }

        /// <summary>
        /// Gets the enumerator.
        /// </summary>
        /// <returns>The enumerator.</returns>
        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        /// <summary>
        /// Tries the get value.
        /// </summary>
        /// <param name="address">The address.</param>
        /// <param name="channel">The channel.</param>
        /// <returns>Whether can get the value.</returns>
        public bool TryGetValue(PeerAddress address, out UpdateChannel channel)
        {
            if (this.updateChannels.TryGetValue(address, out channel))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Determines whether the specified address contains key.
        /// </summary>
        /// <param name="address">The address.</param>
        /// <returns>
        /// <c>true</c> if the specified address contains key; otherwise, <c>false</c>.
        /// </returns>
        public bool ContainsKey(PeerAddress address)
        {
            return this.updateChannels.ContainsKey(address);
        }

        /// <summary>
        /// Inner class enumerator, enumerate UpdateChannel.
        /// </summary>
        private class Enumerator : IEnumerator<UpdateChannel>
        {
            /// <summary>
            /// Update channels enumerator.
            /// </summary>
            private Dictionary<PeerAddress, UpdateChannel>.ValueCollection.Enumerator updateChannels;

            /// <summary>
            /// Initializes a new instance of the UpdateChannelManager.Enumerator class.
            /// </summary>
            /// <param name="updateChannels">Update channels enumerator.</param>
            public Enumerator(Dictionary<PeerAddress, UpdateChannel>.ValueCollection.Enumerator updateChannels)
            {
                this.updateChannels = updateChannels;
            }

            #region IEnumerator<UpdateChannel> Members

            /// <summary>
            /// Gets the current value of the update channels enumerator.
            /// </summary>
            public UpdateChannel Current
            {
                get
                {
                    return this.updateChannels.Current;
                }
            }

            #endregion

            #region IEnumerator Members

            /// <summary>
            /// Gets the current value of the update channels enumerator.
            /// </summary>
            object System.Collections.IEnumerator.Current
            {
                get
                {
                    return this.updateChannels.Current;
                }
            }

            /// <summary>
            /// Move the current pointer to the next value of the update 
            /// channels enumerator.
            /// </summary>
            /// <returns>Returns true if the enumerator is still has value.</returns>
            public bool MoveNext()
            {
                return this.updateChannels.MoveNext();
            }

            /// <summary>
            /// Reset the update channels enumerator.
            /// </summary>
            public void Reset()
            {
                throw new NotSupportedException();
            }

            #endregion

            #region IDisposable Members

            /// <summary>
            /// Dispose the update channels enumerator.
            /// </summary>
            public void Dispose()
            {
                this.updateChannels.Dispose();
            }

            #endregion
        }
    }
}
