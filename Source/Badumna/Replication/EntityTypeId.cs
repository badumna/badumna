﻿using System;
using System.Collections.Generic;
using System.Text;
using Badumna.Core;

namespace Badumna.Replication
{
    enum EntityGroup : byte
    {
        Spatial,
        Match,
        MatchDummy,
        MatchController,
        MatchHosted
    }

    /// <summary>
    /// A class that represents an application specific grouping of entity types, allowing the easy
    /// categorization of remote entities.
    /// </summary>
    /// The type id consists of a group and and id. The group can be used to classify entity
    /// types in a custom manner, e.g., spatial entities vs other types of entities. While the id can indicate specific 
    /// entities, e.g., within spatial entities id could indicate Monsters, Avatars or NPC
    public struct EntityTypeId : IParseable
    {
        /// <summary>
        /// Gets the group.
        /// </summary>
        /// <value>The group.</value>
        public byte Group { get { return (byte)(this.mId >> 24); } }

        /// <summary>
        /// Gets the id.
        /// </summary>
        /// <value>The id.</value>
        public uint Id { get { return this.mId & 0x00FFFFFF; } }

        private uint mId;

        /// <summary>
        /// Initializes a new instance of the <see cref="EntityTypeId"/> struct from the given group and id.
        /// </summary>
        /// <param name="group">The group</param>
        /// <param name="id">The id</param>
        public EntityTypeId(byte group, uint id)
        {
            if ((id & 0xFF000000) > 0)
            {
                throw new ArgumentOutOfRangeException("id");
            }

            this.mId = ((uint)group << 24) | id;
        }

        void IParseable.ToMessage(MessageBuffer message, Type parameterType)
        {
            message.Write(this.mId);
        }

        void IParseable.FromMessage(MessageBuffer message)
        {
            this.mId = message.ReadUInt();
        }

        /// <inheritdoc />
        public override string ToString()
        {
            return String.Format("{0}/{1}", this.Group, this.Id);
        }
    }
}
