﻿//-----------------------------------------------------------------------
// <copyright file="EntityRouterFactory.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using Badumna.Core;
using Badumna.Transport;

namespace Badumna.Replication
{
    /// <summary>
    /// Document this class.
    /// </summary>
    internal class EntityRouterFactory : IEntityRouterFactory
    {
        /// <summary>
        /// Creates the specified parent.
        /// </summary>
        /// <param name="parent">The parent.</param>
        /// <param name="messageConsumer">The message consumer.</param>
        /// <returns>The entity router.</returns>
        public IEntityRouter Create(TransportProtocol parent, IMessageConsumer<EntityEnvelope> messageConsumer)
        {
            return new EntityRouter(parent, messageConsumer);
        }
    }
}