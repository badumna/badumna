using System;
using System.Collections.Generic;
using System.Diagnostics;
using Badumna.Core;
using Badumna.DataTypes;
using Badumna.Transport;

namespace Badumna.Replication
{
    internal class EntityRouter : TransportProtocol, IEntityRouter
    {
        private IMessageConsumer<EntityEnvelope> mMessageConsumer;

        public EntityRouter(TransportProtocol parent, IMessageConsumer<EntityEnvelope> messageConsumer)
            : base(parent)
        {
            this.mMessageConsumer = messageConsumer;
        }

        public TransportEnvelope ToTransportEnvelope(EntityEnvelope envelope)
        {
            if (envelope.SourceEntity == null || envelope.DestinationEntity == null)
            {
                throw new ArgumentException("ToTransportEnvelope");
            }

            TransportEnvelope transportEnvelope = this.GetMessageFor(envelope.DestinationEntity.Address, envelope.Qos);
            transportEnvelope.Qos.IsRealtimeMessage = envelope.Qos.IsRealtimeMessage;

            if (envelope.DestinationEntity.Address != envelope.DestinationEntity.OriginalAddress)
            {
                // Hosted destination entities need their full BadumnaId to be sent
                this.RemoteCall(
                    transportEnvelope,
                    this.HandleMessageWithSourceAndHostedDestination,
                    envelope,
                    envelope.SourceEntity,
                    envelope.DestinationEntity);
            }
            else
            {
                // Non-Hosted destination entities can have their BadumnaId reconstructed from the
                // local ID on the destination peer.
                this.RemoteCall(
                    transportEnvelope,
                    this.HandleMessageWithSourceAndDestination,
                    envelope,
                    envelope.SourceEntity,
                    envelope.DestinationEntity.LocalId);
            }
#if TRACE
            transportEnvelope.AppendLabel(envelope.Label + " ");
#endif

            return transportEnvelope;
        }

        public void DirectSend(EntityEnvelope envelope)
        {
            this.SendMessage(this.ToTransportEnvelope(envelope));
        }

        [ConnectionfulProtocolMethod(ConnectionfulMethod.EntityRouterHandleMessageWithSourceAndHostedDestination)]
        private void HandleMessageWithSourceAndHostedDestination(EntityEnvelope envelope, BadumnaId sourceId, BadumnaId destinationId)
        {
            envelope.SourceEntity = sourceId;
            envelope.DestinationEntity = destinationId;
            this.mMessageConsumer.ProcessMessage(envelope);
        }

        [ConnectionfulProtocolMethod(ConnectionfulMethod.EntityRouterHandleMessageWithSourceAndDestination)]
        private void HandleMessageWithSourceAndDestination(EntityEnvelope envelope, BadumnaId sourceId, ushort destinationLocalId)
        {
            envelope.SourceEntity = sourceId;
            envelope.DestinationEntity = new BadumnaId(this.CurrentEnvelope.Destination, destinationLocalId);
            this.mMessageConsumer.ProcessMessage(envelope);
        }
    }
}
