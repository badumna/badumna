﻿using System;
using System.Diagnostics;
using Badumna.Core;
using Badumna.DataTypes;
using Badumna.Transport;
using Badumna.Utilities;

namespace Badumna.Replication
{
    /// <summary>
    /// Channel for updates from a single Original to a single interested peer.
    /// </summary>
    /// <remarks>
    /// This channel will only store one update at a time.  If a new update is
    /// add to it then it will superceed any previous update.
    /// </remarks>
    class UpdateChannel : ISource<TransportEnvelope>
    {
        public PeerAddress Destination { get; private set; }

        public double DropRate
        {
            get
            {
                double genRate = this.mUpdateGenRate.EstimatedRate;
                double sendRate = this.mUpdateSendRate.EstimatedRate;
                double dropRate;

                if (genRate <= sendRate ||  // Probably can't happen, but maybe the smoother might give this temporarily?
                    genRate == 0)
                {
                    dropRate = 0.0;
                }
                else
                {
                    dropRate = 1.0 - (sendRate / genRate);
                }

                Logger.TraceInformation(
                    LogTag.Entity | LogTag.Periodic,
                    "UpdateChannel {0} DropRate = {1:F3} (genRate = {2:F3}, sendRate = {3:F3})",
                    this.Destination,
                    dropRate,
                    genRate,
                    sendRate);

                return dropRate;
            }
        }

        // Varies between 0 and 1, higher value means more attention
        public float LastAttention { get; private set; }

        public double GeneratingBytesPerSecond { get { return this.mGeneratingRate.EstimatedRate; } }
        
        public bool IsEmpty
        {
            get { return this.mMessageEnvelope == null || this.timeKeeper.Now < this.mTimeOfNextSend; }
        }

        private TimeSpan mTimeOfLastSend;
        private TimeSpan mTimeOfNextSend;
        
        private ushort mMaximumUpdateRateBps = ushort.MaxValue;

        private TransportEnvelope mMessageEnvelope;

        private GenericCallBack<ISource<TransportEnvelope>> mUnemptiedDelegate;

        private OneShotTask mUnemptiedTask;

        private IEntityRouter mRouter;

        private RateSmoother mGeneratingRate;
        private RateSmoother mUpdateGenRate;
        private RateSmoother mUpdateSendRate;

        /// <summary>
        /// Provides access to the current time.
        /// </summary>
        private ITime timeKeeper;

        public UpdateChannel(PeerAddress destination, IEntityRouter router, NetworkEventQueue eventQueue, ITime timeKeeper)
        {
            this.timeKeeper = timeKeeper;

            this.mGeneratingRate = new RateSmoother(Parameters.DesiredRateSamplingPeriod, Parameters.DesiredRateSettlingPeriod, timeKeeper);
            this.mUpdateGenRate = new RateSmoother(Parameters.DesiredRateSamplingPeriod, Parameters.DesiredRateSettlingPeriod, timeKeeper);
            this.mUpdateSendRate = new RateSmoother(Parameters.DesiredRateSamplingPeriod, Parameters.DesiredRateSettlingPeriod, timeKeeper);

            this.mUnemptiedTask = new OneShotTask(eventQueue);

            this.Destination = destination;
            this.mRouter = router;

            this.LastAttention = 0.5f;  // Assume we've got some attention until we get a flow control update

            this.mTimeOfLastSend = TimeSpan.Zero;
            this.mTimeOfNextSend = new TimeSpan();
        }

        public TransportEnvelope Get()
        {
            if (this.IsEmpty)
            {
                return null;
            }

            TransportEnvelope envelope = this.mMessageEnvelope;
            this.mMessageEnvelope = null;

            this.mUpdateSendRate.ValueEvent(1);
            this.mTimeOfLastSend = this.timeKeeper.Now;

            return envelope;
        }

        public TransportEnvelope Peek()
        {
            if (this.IsEmpty)
            {
                return null;
            }

            return this.mMessageEnvelope;
        }

        public void SetUnemptiedCallback(GenericCallBack<ISource<TransportEnvelope>> unemptiedDelegate)
        {
            this.mUnemptiedDelegate = unemptiedDelegate;
            
            // Since subscription happens on network thread, but events are actioned on the application thread, the initial triggering
            // may occur before subscription, so we trigger it again here to prevent any delay in replication.
            // See tickets #842 and #1075.
            this.TriggerUnemptied();
        }

        private void TriggerUnemptied()
        {
            if (!this.IsEmpty)
            {
                // Don't worry about updates not being triggered if no handler has subscribed,
                // as this will be called again immediately upon subscription.
                GenericCallBack<ISource<TransportEnvelope>> handler = this.mUnemptiedDelegate;
                if (handler != null)
                {
                    handler(this);
                }
            }
            else if (this.mMessageEnvelope != null)
            {
                // Schedule for a time when we should be unempty
                this.mUnemptiedTask.Trigger(this.mTimeOfNextSend - this.timeKeeper.Now, Apply.Func(this.TriggerUnemptied));
            }
        }

        public void ApplyFlowControlFeedback(ushort maximumUpdateRate, float attention)
        {
            this.mMaximumUpdateRateBps = maximumUpdateRate;
            this.LastAttention = attention;
        }

        public void NewUpdate(EntityEnvelope originalEnvelope)
        {
            // This should be safe because there shouldn't be anything executing in parallel
            // that will change the EntityEnvelope.  Once it's written into the TransportEnvelope
            // then other UpdateChannels are free to change the destination.
            if (originalEnvelope.DestinationEntity == null || originalEnvelope.DestinationEntity.Address == null)
            {
                Logger.TraceWarning(LogTag.Replication, "Destination is unknown in the update.");
                originalEnvelope.DestinationEntity = new BadumnaId(this.Destination, 0);
            }

            ////if (this.mMessageEnvelope != null)
            ////{
            ////    Logger.TraceInformation(LogTag.Replication, "Overwriting envelope in update channel to {0}.", this.Destination);
            ////}

            this.mMessageEnvelope = this.mRouter.ToTransportEnvelope(originalEnvelope);
            this.mGeneratingRate.ValueEvent(this.mMessageEnvelope.Length);
            this.mUpdateGenRate.ValueEvent(1);

            this.CalculateNextSendTime();
            this.TriggerUnemptied();
        }

        // TODO: How does this interact with the reliable update segment mechanism?
        //       i.e. Not sending a segment is not necessarily a good idea to reduce bandwidth
        //       because it will just result in resends?
        private void CalculateNextSendTime()
        {
            double delaySeconds = Parameters.MinimumUpdateRate.TotalSeconds;
            if (this.mMaximumUpdateRateBps > 0)
            {
                delaySeconds = (double)this.mMessageEnvelope.Length / this.mMaximumUpdateRateBps;
            }

            delaySeconds = Math.Min(delaySeconds, Parameters.MinimumUpdateRate.TotalSeconds);
            if (this.mTimeOfLastSend == TimeSpan.Zero)
            {
                this.mTimeOfNextSend = this.timeKeeper.Now;
            }
            else
            {
                this.mTimeOfNextSend = this.mTimeOfLastSend + TimeSpan.FromSeconds(delaySeconds);
            }
        }
    }
}
