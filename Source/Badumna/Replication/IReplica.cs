﻿//-----------------------------------------------------------------------
// <copyright file="IReplica.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2010 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.Replication
{
    using System;
    using System.IO;
    using Badumna.DataTypes;
    using Badumna.Utilities;

    /// <summary>
    /// To be implemented by classes representing remote instances of an entity.
    /// </summary>
    /// <remarks>
    /// Remote and local entities are paired. Local entities reside on the controlling peer and are
    /// responsible for propogating state changes to their remote replicas.  When updates arrive
    /// from an entity controlled elsewhere they will be passed to an instance of
    /// <see cref="IReplica"/> that represents that entity on this peer (see
    /// <see cref="Badumna.SpatialEntities.NetworkScene"/> for details on how
    /// <see cref="Badumna.SpatialEntities.ISpatialReplica"/> instances is created).
    /// </remarks>
    public interface IReplica : IEntity
    {
        /// <summary>
        /// Deserialize the entity state from the given stream.
        /// </summary>
        /// <remarks>
        /// The data in inboundStream is the same as was written by the original entity in
        /// IOriginal.Serialize(...).  includedParts specifies which sections are included in
        /// the stream.  State changes are guaranteed to be replicated eventually, however
        /// intermediate states may be skipped (i.e. if a particular piece of state changes twice
        /// quickly, the first state change may not make it to all (any) replicas).
        /// </remarks>
        /// <param name="includedParts">An array of values indicating which parts are included in
        /// the stream.</param>
        /// <param name="stream">The data to be deserialized.</param>
        /// <param name="estimatedMillisecondsSinceDeparture">To do: documentation.</param>
        /// <param name="id">An ID used to uniquely identify the entity.</param>
        /// <returns>An attention value between 0.0 (no attention) and 1.0 (maximum attention). This
        /// is used to prioritize future updates to this replica.</returns>
        double Deserialize(
            BooleanArray includedParts,
            Stream stream,
            int estimatedMillisecondsSinceDeparture,
            BadumnaId id);
    }
}
