//-----------------------------------------------------------------------
// <copyright file="IReplicationService.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2013 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System.IO;
using Badumna.Utilities;

namespace Badumna.Replication
{
    /// <summary>
    /// Interface for service supporting sending of custom messages between originals and replicas.
    /// </summary>
    /// <typeparam name="O">Type of originals.</typeparam>
    /// <typeparam name="R">Type of replicas</typeparam>
    internal interface IReplicationService<O, R>
    {
        /// <summary>
        /// Sends a message to a replica's original.
        /// </summary>
        /// <param name="replica">The replica whose original should receive the message.</param>
        /// <param name="messageData">The serialized message.</param>
        void SendCustomMessageToOriginal(R replica, MemoryStream messageData);

        /// <summary>
        /// Sends a message to all replicas of a specified original.
        /// </summary>
        /// <param name="original">The original whose replicas should receive the message.</param>
        /// <param name="messageData">The serialized message.</param>
        void SendCustomMessageToReplicas(O original, MemoryStream messageData);

        /// <summary>
        /// Mark parts of an original entity as requiring replication.
        /// </summary>
        /// <param name="original">The original entity requiring replication.</param>
        /// <param name="changedParts">List of parts to update.</param>
        void MarkForUpdate(O original, BooleanArray changedParts);
    }
}
