//-----------------------------------------------------------------------
// <copyright file="EntityManager.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.Replication
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;

    using Badumna.Core;
    using Badumna.DataTypes;
    using Badumna.Overload;
    using Badumna.ServiceDiscovery;
    using Badumna.Transport;
    using Badumna.Utilities;

    /// <summary>
    /// To do: documentation.
    /// </summary>
    internal partial class EntityManager : EntityProtocol, IEntityManager, IMessageConsumer<EntityEnvelope>, IServiceConsumer, IReplicationService<IOriginal, IReplica>
    {
        /// <summary>
        /// Helper class for holding replica creation and removal delegates for an entity type.
        /// </summary>
        private class EntityTypeInfo
        {
            /// <summary>
            /// The replica creation delegate.
            /// </summary>
            public CreateReplica CreateReplica;

            /// <summary>
            /// The replica removal delegate.
            /// </summary>
            public RemoveReplica RemoveReplica;

            /// <summary>
            /// Initializes a new instance of the EntityManager.EntityTypeInfo class.
            /// </summary>
            /// <param name="createReplica">The replica creation delegate.</param>
            /// <param name="removeReplica">The replica removal delegate.</param>
            public EntityTypeInfo(CreateReplica createReplica, RemoveReplica removeReplica)
            {
                this.CreateReplica = createReplica;
                this.RemoveReplica = removeReplica;
            }
        }

        /// <summary>
        /// The regular task for garbage collection.
        /// </summary>
        private readonly RegularTask garbageCollectionTask;

        /// <summary>
        /// A callback for when the forwarding peer goes offline.
        /// </summary>
        private OnServiceBecomeOffline onForwradingPeerBecomeOffline;
        
        /// <summary>
        /// Original wrappers by ID.
        /// </summary>
        private Dictionary<BadumnaId, IOriginalWrapper> originalWrappers;

        /// <summary>
        /// Replica wrappers by ID.
        /// </summary>
        private Dictionary<BadumnaId, IReplicaWrapper> replicaWrappers;

        /// <summary>
        /// To do: documentation.
        /// </summary>
        private ServiceDescription mServiceDescription;
        
        /// <summary>
        /// This is an ugly hack. 
        /// in its original design, the replication system shouldn't require any spatial information,
        /// because all spatial details are suppose to be kept in the Spatial Entity module, however
        /// it is actually essential to keep such information to implement the many-to-many
        /// relationship.
        /// </summary>
        private AssociatedReplica association = new AssociatedReplica();

        /// <summary>
        /// To do: documentation.
        /// </summary>
        private Dictionary<byte, EntityTypeInfo> entityGroups = new Dictionary<byte, EntityTypeInfo>();

        /// <summary>
        /// To do: documentation.
        /// </summary>
        private EntityRouter router;

        /// <summary>
        /// To do: documentation.
        /// </summary>
        private ForwardingManager forwardingManager;

        /// <summary>
        /// To do: documentation.
        /// </summary>
        private RegularTask flowControlFeedbackTask;

        /// <summary>
        /// To do: documentation.
        /// </summary>
        private ResourceAllocator<BadumnaId> inboundBandwidthAllocator;

        /// <summary>
        /// To do: documentation.
        /// </summary>
        private EntityTypeInfo stubTypeInfo;

        /// <summary>
        /// To do: documentation.
        /// </summary>
        private IChannelFunnel<TransportEnvelope> connectionTable;

        /// <summary>
        /// To do: documentation.
        /// </summary>
        private Dictionary<Pair<BadumnaId, BadumnaId>, TimeSpan> lastInstantiationRequestTime = new Dictionary<Pair<BadumnaId, BadumnaId>, TimeSpan>();

        /// <summary>
        /// To do: documentation.
        /// </summary>
        private Dictionary<Pair<BadumnaId, BadumnaId>, TimeSpan> lastRemovalRequestTime = new Dictionary<Pair<BadumnaId, BadumnaId>, TimeSpan>();

        /// <summary>
        /// The queue to use for scheduling events.
        /// </summary>
        private NetworkEventQueue eventQueue;

        /// <summary>
        /// The method to use to queue events that must run in the application's thread.
        /// </summary>
        private QueueInvokable queueApplicationEvent;

        /// <summary>
        /// Provides access to the current time.
        /// </summary>
        private ITime timeKeeper;

        /// <summary>
        /// The configuration options for the overload system.
        /// </summary>
        private OverloadModule overloadOptions;

        /// <summary>
        /// Provides the public peer address.
        /// </summary>
        private INetworkAddressProvider addressProvider;

        /// <summary>
        /// Reports on network connectivity status.
        /// </summary>
        private INetworkConnectivityReporter connectivityReporter;

        /// <summary>
        /// Initializes a new instance of the EntityManager class.
        /// </summary>
        /// <param name="parent">The parent transport protocol.</param>
        /// <param name="connectionTable">The connection table.</param>
        /// <param name="forwardingManager">The forwarding manager.</param>
        /// <param name="overloadOptions">The configuration options for the overload system.</param>
        /// <param name="eventQueue">The queue to use for scheduling events.</param>
        /// <param name="queueApplicationEvent">The method to use to queue events that must run in the application's thread.</param>
        /// <param name="timeKeeper">Provides access to the current time.</param>
        /// <param name="addressProvider">Provides the public peer address.</param>
        /// <param name="connectivityReporter">Reports on network connectivity status.</param>
        /// <param name="factory">A factory that constructs instances of the given type.</param>
        public EntityManager(
            TransportProtocol parent,
            IChannelFunnel<TransportEnvelope> connectionTable,
            ForwardingManager forwardingManager,
            OverloadModule overloadOptions,
            NetworkEventQueue eventQueue,
            QueueInvokable queueApplicationEvent,
            ITime timeKeeper,
            INetworkAddressProvider addressProvider,
            INetworkConnectivityReporter connectivityReporter,
            GenericCallBackReturn<object, Type> factory)
            : base(addressProvider, factory)
        {
            this.overloadOptions = overloadOptions;
            this.eventQueue = eventQueue;
            this.queueApplicationEvent = queueApplicationEvent;
            this.timeKeeper = timeKeeper;
            this.addressProvider = addressProvider;
            this.connectivityReporter = connectivityReporter;

            this.router = new EntityRouter(parent, this);
            this.forwardingManager = forwardingManager;
            this.DispatcherMethod = this.router.DirectSend;
            this.connectionTable = connectionTable;

            this.stubTypeInfo = new EntityTypeInfo(
                delegate(BadumnaId entityId, EntityTypeId entityType)
                {
                    return new ReplicaStub(entityId);
                },
                delegate { });

            this.Reset();
            this.inboundBandwidthAllocator = new ResourceAllocator<BadumnaId>(Parameters.MaximumUpdateDownloadRateBps, Parameters.MinimumUpdateRatePerEntityBps);
#if IOS
            // The following line is used to force the AOT to compile this type during compilation.
            new Dictionary<Pair<BadumnaId,BadumnaId>, bool>();
#endif
            this.mUpdateLog = new Set<Pair<BadumnaId, BadumnaId>>();
            this.flowControlFeedbackTask = new RegularTask("Flow control", Parameters.FlowControlSendPeriod, eventQueue, connectivityReporter, this.PerformFlowControl);
            this.flowControlFeedbackTask.Start();

            if (null != forwardingManager)
            {
                forwardingManager.EntityManager = this;
            }

            this.garbageCollectionTask = new RegularTask(
                "replication_entity_manager_gc",
                Parameters.EntityManagerGarbageCollectionPeriod,
                eventQueue,
                connectivityReporter,
                this.CollectGarbage);
            this.garbageCollectionTask.Start();
        }

        /// <summary>
        /// Gets the ... to do.
        /// </summary>
        public AssociatedReplica Association
        {
            get { return this.association; }
        }

        /// <summary>
        /// Gets or sets the delegate for handling when the forwarding peer goes offline.
        /// </summary>
        public OnServiceBecomeOffline OnForwradingPeerBecomeOffline
        {
            get { return this.onForwradingPeerBecomeOffline; }
            set { this.onForwradingPeerBecomeOffline = value; }
        }

        /// <summary>
        /// Gets a count of the original entities being managed.
        /// </summary>
        public int OriginalEntityCount
        {
            get { return this.originalWrappers.Count; }
        }

        /// <summary>
        /// Gets a count of the number of replicas being managed.
        /// </summary>
        public int ReplicaEntityCount
        {
            get { return this.replicaWrappers.Count; }
        }

        /// <summary>
        /// Gets the router.
        /// </summary>
        public IEntityRouter Router
        {
            get { return this.router; }
        }

        /// <summary>
        /// Gets the forwarding manager.
        /// </summary>
        public ForwardingManager ForwardingManager
        {
            get { return this.forwardingManager; }
        }

        /// <summary>
        /// Register create and remove handlers for an entity group.
        /// </summary>
        /// <param name="groupId">The unique id identifying the group</param>
        /// <param name="createReplica">The delegate to be called when a new replica arrives</param>
        /// <param name="removeReplica">The delegate to be called when a replica is to be removed</param>
        public void Register(byte groupId, CreateReplica createReplica, RemoveReplica removeReplica)
        {
            if (this.entityGroups.ContainsKey(groupId))
            {
                throw new InvalidOperationException("Group id already registered");
            }

            this.entityGroups[groupId] = new EntityTypeInfo(createReplica, removeReplica);
        }

        /// <summary>
        /// Clear out replicas and originals and empty update and event queues.
        /// </summary>
        public void Reset()
        {
            this.replicaWrappers = new Dictionary<BadumnaId, IReplicaWrapper>();
            this.originalWrappers = new Dictionary<BadumnaId, IOriginalWrapper>();

            this.mUpdateQueue = new List<UpdateInformation>();
            this.mEventQueue = new Queue<LambdaFunction>();
        }

        /// <summary>
        /// To do: documentation.
        /// </summary>
        /// <returns></returns>
        public List<Pair<BadumnaId, BadumnaId>> GetTimeOutPeers()
        {
            List<Pair<BadumnaId, BadumnaId>> list = new List<Pair<BadumnaId, BadumnaId>>();

            foreach (KeyValuePair<BadumnaId, IOriginalWrapper> kvp in this.originalWrappers)
            {
                List<Pair<BadumnaId, BadumnaId>> curList = kvp.Value.GetTimeOutPeers();
                if (curList != null && curList.Count > 0)
                {
                    list.AddRange(curList);
                }
            }

            return list;
        }

        /// <summary>
        /// To do: documentation.
        /// </summary>
        /// <param name="originalId"></param>
        /// <param name="address"></param>
        /// <returns></returns>
        public InterestedPeer GetInterestedPeer(BadumnaId originalId, PeerAddress address)
        {
            IOriginalWrapper original = null;
            if (this.originalWrappers.TryGetValue(originalId, out original))
            {
                return original.GetInterestedPeer(address);
            }

            return null;
        }


        public void ProcessMessage(EntityEnvelope envelope)
        {
            this.ParseMessage(envelope, envelope.Message.ReadOffset);
        }

        public double GetAvailableBandwidth(PeerAddress peerAddress, double newChannelWeight)
        {
            return this.connectionTable.GetAvailableBandwidth(peerAddress, newChannelWeight);
        }

        public void AddEntityChannel(PeerAddress peerAddress, UpdateChannel channel)
        {
            // add/remove channels are done on the app thread as during most of the time the channels are accessed on the app thread
            this.queueApplicationEvent(Apply.Func(this.connectionTable.AddChannel, peerAddress, channel, ChannelGroup.EntityUpdates));
        }

        public void RemoveEntityChannel(PeerAddress peerAddress, UpdateChannel channel)
        {
            this.queueApplicationEvent(Apply.Func(this.connectionTable.RemoveChannel, peerAddress, channel));
        }

        #region IServiceConsumer Members

        /// <summary>
        /// Gets the offline service.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <returns></returns>
        public ServiceDescription GetOfflineService(ServiceDescription service)
        {
            if (!service.Equals(this.mServiceDescription))
            {
                Debug.Assert(false, "GetOfflineService failed in EntityManager.");
            }

            // currently, the forwarding peer only has one valid service and attribute type. 
            foreach (OriginalWrapper original in this.originalWrappers.Values)
            {
                if (!original.ForwardingPeerAvailable)
                {
                    return this.mServiceDescription;
                }
            }

            return null;
        }

        /// <summary>
        /// Switches to a new service host.
        /// </summary>
        /// <param name="description">The service description.</param>
        /// <param name="address">The address of the new service host.</param>
        public void SwitchToNewServiceHost(ServiceDescription description, PeerAddress address)
        {
            foreach (OriginalWrapper original in this.originalWrappers.Values)
            {
                original.SwitchToNewForwardingPeer(address);
            }
        }

        /// <summary>
        /// Register with the service manager.
        /// </summary>
        /// <param name="manager">The service manager.</param>
        public void RegisterWithServiceManager(ServiceManager manager)
        {
            if (this.overloadOptions.IsClientEnabled)
            {
                ServiceDescription description = new ServiceDescription(ServerType.Overload);
                this.mServiceDescription = description;
                manager.RegisterServiceConsumer(description, this);
                manager.RegisterInterestedService(description);
                this.onForwradingPeerBecomeOffline = manager.OnServiceBecomeOffline;
            }
        }

        #endregion
    }
}
