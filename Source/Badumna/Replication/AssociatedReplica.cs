﻿//-----------------------------------------------------------------------
// <copyright file="AssociatedReplica.cs" company="Scalify">
//     Copyright (c) 2013 Scalify. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.Replication
{
    using System;
    using System.IO;
    using System.Collections.Generic;
    using Badumna.DataTypes;
    using Badumna.Utilities;

    /// <summary>
    /// The AssociatedReplica class is used to record the many-to-many interest
    /// relationships between replicas and originals.
    /// </summary>
    internal class AssociatedReplica
    {
        // key is the original entity id, value is a list of replica ids associated with the key
        protected Dictionary<BadumnaId, Set<BadumnaId>> mAssociation = new Dictionary<BadumnaId, Set<BadumnaId>>();

        public void AddAssociationRelationship(BadumnaId original, BadumnaId replica)
        {
            if (null == original || null == replica)
            {
                throw new ArgumentNullException();
            }

            Set<BadumnaId> set;
            if (!this.mAssociation.TryGetValue(original, out set))
            {
                set = new Set<BadumnaId>();
                this.mAssociation[original] = set;
            }
         
            set.Add(replica);
        }

        public List<BadumnaId> GetAssociatedReplicas(BadumnaId original)
        {
            if (null == original)
            {
                throw new ArgumentNullException();
            }

            Set<BadumnaId> set;
            if(this.mAssociation.TryGetValue(original, out set))
            {
                return new List<BadumnaId>(set);
            }

            return null;
        }

        public List<BadumnaId> GetAssociatedOriginals(BadumnaId replica)
        {
            if (null == replica)
            {
                throw new ArgumentNullException();
            }

            List<BadumnaId> list = null;
            foreach (KeyValuePair<BadumnaId, Set<BadumnaId>> kvp in this.mAssociation)
            {
                if (kvp.Value.Contains(replica))
                {
                    if (null == list)
                    {
                        list = new List<BadumnaId>();
                    }

                    list.Add(kvp.Key);
                }
            }

            return list;
        }

        public void RemoveFromAllAssociatedOriginals(BadumnaId replica)
        {
            if (null == replica)
            {
                throw new ArgumentNullException();
            }

            Dictionary<BadumnaId, Set<BadumnaId>> newAssociation = new Dictionary<BadumnaId, Set<BadumnaId>>();
            Set<BadumnaId> set;
            foreach (KeyValuePair<BadumnaId, Set<BadumnaId>> kvp in this.mAssociation)
            {
                set = new Set<BadumnaId>();
                foreach (BadumnaId id in kvp.Value)
                {
                    if (!id.Equals(replica))
                    {
                        set.Add(id);
                    }
                }

                if (set.Count > 0)
                {
                    newAssociation[kvp.Key] = set;
                }
            }

            this.mAssociation = newAssociation;
        }

        public void RemoveAssociatedReplicas(BadumnaId original)
        {
            if (null == original)
            {
                throw new ArgumentNullException();
            }

            this.mAssociation.Remove(original);
        }

        public void RemoveAssociatedReplicas(BadumnaId original, BadumnaId replica)
        {
            if (null == original || null == replica)
            {
                throw new ArgumentNullException();
            }

            Set<BadumnaId> set;
            if (this.mAssociation.TryGetValue(original, out set))
            {
                set.Remove(replica);
                if (set.Count == 0)
                {
                    this.mAssociation.Remove(original);
                }
            }
        }

        public bool HasAssociatedOriginal(BadumnaId replica)
        {
            if (null == replica)
            {
                throw new ArgumentNullException("");
            }

            foreach (KeyValuePair<BadumnaId, Set<BadumnaId>> kvp in this.mAssociation)
            {
                if (kvp.Value.Contains(replica))
                {
                    return true;
                }                
            }

            return false;
        }

        public bool IsAssociated(BadumnaId original, BadumnaId replica)
        {
            if (null == original || null == replica)
            {
                throw new ArgumentNullException();
            }

            Set<BadumnaId> set;
            if (!this.mAssociation.TryGetValue(original, out set))
            {
                return false;
            }

            return set.Contains(replica);
        }

        public void RemoveOriginal(BadumnaId originalId)
        {
            if (originalId == null)
            {
                throw new ArgumentNullException();
            }

            this.mAssociation.Remove(originalId);
        }

        public void Clear()
        {
            this.mAssociation.Clear();
        }
    }
}