﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using Badumna.Core;
using Badumna.DataTypes;
using Badumna.Utilities;
using Badumna.Utilities.Logging;

namespace Badumna.Replication
{
    partial class EntityManager
    {
        public void MarkForUpdate(IOriginal localEntity, BooleanArray changedParts)
        {
            if (null == localEntity)
            {
                throw new ArgumentNullException("localEntity cannot be null.");
            }

            if (null == localEntity.Guid)
            {
                return;
            }

            IOriginalWrapper wrapper = null;
            if (this.originalWrappers.TryGetValue(localEntity.Guid, out wrapper))
            {
                wrapper.MarkForUpdate(changedParts);
            }
        }

        public void MarkForUpdate(IOriginal localEntity, int changedPartIndex)
        {
            if (null == localEntity)
            {
                throw new ArgumentNullException("localEntity cannot be null.");
            }

            if (null == localEntity.Guid)
            {
                return;
            }

            IOriginalWrapper wrapper = null;
            if (this.originalWrappers.TryGetValue(localEntity.Guid, out wrapper))
            {
                wrapper.MarkForUpdate(changedPartIndex);
            }
        }

        /// <summary>
        /// Register an IOriginal to be replicated
        /// </summary>
        /// <remarks>
        /// Must be called only from event thread
        /// </remarks>
        /// <param name="localEntity">The entity to be replicated</param>
        /// <param name="entityType">The type of the entity.  This will be passed to the CreateReplica delegate so that it can instantiate the appropriate class.</param>
        public void RegisterEntity(IOriginal localEntity, EntityTypeId entityType)
        {
            if (localEntity == null)
            {
                throw new ArgumentNullException("localEntity");
            }

            if (null == localEntity.Guid || !localEntity.Guid.IsValid)
            {
                throw new ArgumentException("Local entity must have a valid guid.");
            }

            Logger.TraceInformation(LogTag.Replication, "Registering entity {0}", localEntity.Guid);

            IOriginalWrapper localWrapper;
            if (!this.originalWrappers.TryGetValue(localEntity.Guid, out localWrapper))
            {
                localWrapper = new OriginalWrapper(
                    this, 
                    localEntity, 
                    entityType, 
                    this.overloadOptions, 
                    this.eventQueue, 
                    this.timeKeeper, 
                    this.connectionTable,
                    this.addressProvider,
                    this.connectivityReporter);
                this.originalWrappers.Add(localEntity.Guid, localWrapper);
            }
        }

        // Unlike RegisterEntity there is no guarantee that ProcessNetworkState() will be called again after the application
        // unregisters its entities (it could be shutting down).  Thread safety is maintained by suspending the network event queue
        // within this function.
        public void UnregisterEntity(IOriginal localEntity, bool removeReplicas)
        {
            if (null == localEntity.Guid || !localEntity.Guid.IsValid)
            {
                throw new ArgumentException("Can only remove entities which have a valid guid");
            }

            this.eventQueue.AcquirePerformLockForApplicationCode();
            try
            {
                IOriginalWrapper localWrapper = null;
                Logger.TraceInformation(LogTag.Replication, "Removing original entity {0} at request of application", localEntity.Guid);

                if (!this.originalWrappers.TryGetValue(localEntity.Guid, out localWrapper))
                {
                    Logger.TraceWarning(LogTag.Replication, "RemoveEntity called on unknown entity {0}", localEntity.Guid);
                    return;
                }

                localWrapper.ShutDown(removeReplicas);
                this.originalWrappers.Remove(localEntity.Guid);
                this.association.RemoveOriginal(localEntity.Guid);
            }
            finally
            {
                this.eventQueue.ReleasePerformLockForApplicationCode();
            }
        }

        public void AddInterested(IOriginal original, BadumnaId region)
        {
            IOriginalWrapper originalWrapper;
            if (!this.originalWrappers.TryGetValue(original.Guid, out originalWrapper))
            {
                throw new InvalidOperationException("Original must be registered first by calling RegisterEntity");
            }

            originalWrapper.AddInterested(region);
        }

        public void RemoveInterested(IOriginal original, BadumnaId region)
        {
            IOriginalWrapper originalWrapper;
            if (!this.originalWrappers.TryGetValue(original.Guid, out originalWrapper))
            {
                throw new InvalidOperationException("Original must be registered first by calling RegisterEntity");
            }

            originalWrapper.RemoveInterested(region);
        }

        public int InterestedCount(IOriginal original)
        {
            IOriginalWrapper originalWrapper;
            if (!this.originalWrappers.TryGetValue(original.Guid, out originalWrapper))
            {
                throw new InvalidOperationException("Original must be registered first by calling RegisterEntity");
            }

            return originalWrapper.InterestedCount();
        }

        public void SendCustomMessageToReplicas(IOriginal localEntity, MemoryStream messageData)
        {
            IOriginalWrapper wrapper = null;

            if (localEntity == null)
            {
                throw new ArgumentNullException("localEntity");
            }

            if (localEntity.Guid == null)
            {
                throw new ArgumentNullException("localEntity.Guid");
            }

            if (this.originalWrappers.TryGetValue(localEntity.Guid, out wrapper))
            {
                EntityEnvelope envelope = this.GetMessageFrom(localEntity.Guid, QualityOfService.Reliable);
                CyclicalID.UShortID messageNumber = wrapper.GetNextCustomMessageNumber();

                envelope.Qos.Priority = QosPriority.High;
                this.RemoteCall(envelope, this.ProcessCustomMessage, messageNumber, messageData.ToArray());

                Logger.TraceInformation(LogTag.Replication, "Sending custom message {0} to remote replicas of {1}", messageNumber.Value, localEntity.Guid);

                wrapper.SendEvent(envelope);
            }
            else
            {
                Logger.TraceWarning(LogTag.Replication, "Cannot send custom message to unknown local entity {0}", localEntity.Guid);
            }
        }

        public void SendProximityMessage(BadumnaId entityId, EntityEnvelope envelope)
        {
            IOriginalWrapper wrapper = null;

            if (null == entityId)
            {
                throw new ArgumentNullException("entityId");
            }

            this.originalWrappers.TryGetValue(entityId, out wrapper);
            if (null != wrapper)
            {
                wrapper.SendProximityEvent(envelope);
            }
        }

        public void DispatchUpdates()
        {
            if (this.connectivityReporter.Status == ConnectivityStatus.Online)
            {
                foreach (KeyValuePair<BadumnaId, IOriginalWrapper> localWrapperKVP in this.originalWrappers)
                {
                    if (localWrapperKVP.Value.IsUpdateRequired)
                    {
                        BooleanArray includedParts = null;
                        BooleanArray remainingParts;

                        do
                        {
                            byte[] update = localWrapperKVP.Value.GetUpdate(ref includedParts, out remainingParts);

                            if (!includedParts.Any() && update.Length > 0)
                            {
                                Debug.Assert(false, "There is no part included, but the update buffer length is non-zero.");
                            }

                            CyclicalID.UShortID sequenceNumber = localWrapperKVP.Value.StateSequenceNumber;
                            localWrapperKVP.Value.DispatchUpdate(sequenceNumber, includedParts, update, localWrapperKVP.Value.LocalEntity.Guid);
                            Logger.TraceInformation(LogTag.Match, "Dispatching update {0} (is hosted: {1})", sequenceNumber.Value, localWrapperKVP.Value.EntityType.Group == (byte)EntityGroup.MatchHosted);

                            // Loopback the update so that other NetworkScenes get informed of changes to the original (to support, e.g., DistributedSceneControllers)
                            foreach (BadumnaId entity in localWrapperKVP.Value.LocalInterestedEntities)
                            {
                                this.QueueUpdate(new UpdateInformation(localWrapperKVP.Key, entity, 0, sequenceNumber, includedParts, update));
                            }

                            includedParts = remainingParts;  // Parts to send in the next iteration
                        }
                        while (remainingParts.Any());

                        // mark all flaged changes to false
                        localWrapperKVP.Value.ClearChangedState();
                    }
                }
            }
        }

        #region Protocol

        private void RequestInstantiation(BadumnaId entityId, BadumnaId sourceEntityId)
        {
            // Instantiation requests are ignored if they happen shortly after an instantiation or removal request.
            // I'm not sure why that is necessary, and it has the unfortunate effect of causing a delay in instantiation
            // if an entity changes scene and then back again quickly.

            TimeSpan lastRequestTime;
            Pair<BadumnaId, BadumnaId> key = new Pair<BadumnaId, BadumnaId>(entityId, sourceEntityId);
            this.lastInstantiationRequestTime.TryGetValue(key, out lastRequestTime);
            if (this.timeKeeper.Now < lastRequestTime + Parameters.MinimumInstantiationRequestInterval)
            {
                Logger.TraceInformation(LogTag.Replication, "Instantiation request ignored, last request time is {0}", lastRequestTime);
                return;
            }

            TimeSpan lastRemovalTime;
            if (this.lastRemovalRequestTime.TryGetValue(key, out lastRemovalTime))
            {
                if (this.timeKeeper.Now - lastRemovalTime < TimeSpan.FromSeconds(1))
                {
                    Logger.TraceInformation(LogTag.Replication, "Instantiation request ignored, last removal request time is {0}", lastRemovalTime);
                    return;
                }
            }

            Logger.TraceInformation(LogTag.Replication, "Requesting instantiation of entity {0}", entityId);
            EntityEnvelope envelope = this.GetMessageFor(sourceEntityId, entityId, QualityOfService.Reliable);
            envelope.Qos.Priority = QosPriority.High;
            this.RemoteCall(envelope, this.RemoteInstantiationRequest);
            this.router.DirectSend(envelope);
            this.lastInstantiationRequestTime[key] = this.timeKeeper.Now;
        }

        [EntityProtocolMethod(EntityMethod.RemoteInstantiationRequest)]
        private void RemoteInstantiationRequest()
        {
            BadumnaId entityId = this.CurrentEnvelope.DestinationEntity;
            BadumnaId sourceEntityId = this.CurrentEnvelope.SourceEntity;
            Logger.TraceInformation(
                LogTag.Replication | LogTag.ProtocolMethod,
                "RemoteInstantiationRequest: from {0} to {1}",
                sourceEntityId,
                entityId); 

            // need to get a full update of the original entity, must be one on the application queue. 
            this.queueApplicationEvent(Apply.Func(this.PerformRemoteInstantiationRequest, entityId, sourceEntityId));
        }

        private void PerformRemoteInstantiationRequest(BadumnaId entityId, BadumnaId sourceEntityId)
        {
            IOriginalWrapper localWrapper;
            if (!this.originalWrappers.TryGetValue(entityId, out localWrapper))
            {
                Logger.TraceWarning(LogTag.Replication, "Received RequestInstantiation message for unknown entity {0}", entityId);
                return;
            }

            BooleanArray includedParts;
            BooleanArray remainingParts;
            byte[] fullUpdate = localWrapper.GetCompleteUpdate(out includedParts, out remainingParts);

            // TODO: GetCompleteUpdate should really get a complete update, but it's a bit hard to do
            //       in the tunnel.
            if (remainingParts.Any())
            {
                // Send the remining parts out in an update
                localWrapper.MarkForUpdate(remainingParts);
            }

            // TODO : !! This is a wild guess. Need to get a better measure.
            double desiredRate = fullUpdate.Length * 0.5 + Parameters.UpdatePacketOverhead;
            desiredRate /= Parameters.MaximumUpdateRate.TotalSeconds;

            EntityEnvelope envelope = this.GetMessageFor(entityId, sourceEntityId, QualityOfService.Reliable);
            envelope.Qos.Priority = QosPriority.High;

            this.RemoteCall(envelope, this.InstantiateEntity, localWrapper.EntityType, localWrapper.StateSequenceNumber, 
                includedParts, fullUpdate, (ushort)desiredRate);

            this.router.DirectSend(envelope);
        }

        [EntityProtocolMethod(EntityMethod.ProcessCustomMessageRequest)]
        private void ProcessCustomMessageRequest(byte[] eventData)
        {
            BadumnaId entityId = this.CurrentEnvelope.DestinationEntity;
            Logger.TraceInformation(
                LogTag.Replication | LogTag.ProtocolMethod,
                "ProcessCustomMessageRequest: from {0} to {1}",
                this.CurrentEnvelope.SourceEntity,
                entityId);

            this.ProcessCustomMessageRequestFrom(entityId, new MemoryStream(eventData));  // TODO: Add support for MemoryStream in Message
        }

        private void ProcessCustomMessageRequestFrom(BadumnaId entityId, MemoryStream eventData)
        {
            IOriginalWrapper wrapper = null;

            if (null == entityId)
            {
                Logger.TraceError(LogTag.Replication, "Invalid destination entity id for process event request");
                return;
            }

            if (this.originalWrappers.TryGetValue(entityId, out wrapper))
            {
                wrapper.QueueCustomMessageRequest(eventData);
            }
        }


        // TODO : Throttle this for scalability
        private void PerformFlowControl()
        {
            if (this.connectivityReporter.Status != ConnectivityStatus.Online)
            {
                return;
            }

            this.inboundBandwidthAllocator.CalculateAllocation();

            lock (this.mUpdateLog)
            {
                foreach (Pair<BadumnaId, BadumnaId> updateSource in this.mUpdateLog)
                {
                    ushort flowControlFeedbackBps;
                    byte attentionOnEntity;

                    // the update rate for replicas associated with an NPC is set to be low by assuming NPCs do not need very accuate
                    // views of other players. 
                    if (updateSource.Second.Address.IsDhtAddress)
                    {
                        flowControlFeedbackBps = (ushort)this.inboundBandwidthAllocator.GetAllocatedRate(updateSource.First);
                        flowControlFeedbackBps = Math.Min(Parameters.MaximumDCNPCUpdateRate, flowControlFeedbackBps);
                        attentionOnEntity = (byte)(this.inboundBandwidthAllocator.GetWeight(updateSource.First) * byte.MaxValue);
                    }
                    else
                    {
                        flowControlFeedbackBps = (ushort)this.inboundBandwidthAllocator.GetAllocatedRate(updateSource.First);
                        attentionOnEntity = (byte)(this.inboundBandwidthAllocator.GetWeight(updateSource.First) * byte.MaxValue);
                    }

                    // enfore the minimum rate
                    flowControlFeedbackBps = Math.Max(flowControlFeedbackBps, Parameters.MinFlowControlFeedbackBps);

                    QualityOfService qos = new QualityOfService(true);
                    qos.MustBeInOrder = false;
                    EntityEnvelope envelope = this.GetMessageFor(updateSource.First, qos);
                    this.RemoteCall(envelope, this.FlowControlFeedback, updateSource.Second.Address, flowControlFeedbackBps, attentionOnEntity);
                    this.Router.DirectSend(envelope);
                }

                this.mUpdateLog.Clear();
            }
        }

        [EntityProtocolMethod(EntityMethod.FlowControlFeedback)]
        private void FlowControlFeedback(PeerAddress source, ushort maximumUpdateRateBps, byte attention)
        {
            BadumnaId destination = this.CurrentEnvelope.DestinationEntity;
            Logger.TraceInformation(
                LogTag.Replication | LogTag.ProtocolMethod,
                "FlowControlFeedback: from {0} to {1}",
                source,
                destination);
            IOriginalWrapper localWrapper = null;

            if (this.originalWrappers.TryGetValue(destination, out localWrapper))
            {
                localWrapper.ApplyFlowControlFeedback(source, maximumUpdateRateBps, (float)attention / (float)byte.MaxValue);
            }
            else
            {
                Logger.TraceWarning(LogTag.Replication, "When trying to apply flow control feedback in entity manager, couldn't get the local wrapper, destination is {0}", destination);
            }
        }

        public void AcknowledgeSegments(EntityEnvelope envelope, BooleanArray includedSegments, List<CyclicalID.UShortID> sequenceNumbers)
        {
            this.RemoteCall(envelope, this.AcknowledgeSegments, includedSegments, sequenceNumbers);
        }

        [EntityProtocolMethod(EntityMethod.AcknowledgeSegments)]
        private void AcknowledgeSegments(BooleanArray includedSegments, List<CyclicalID.UShortID> sequenceNumbers)
        {
            BadumnaId entityId = this.CurrentEnvelope.DestinationEntity;
            Logger.TraceInformation(
                LogTag.Replication | LogTag.ProtocolMethod,
                "AcknowledgeSegments: from {0} to {1}",
                this.CurrentEnvelope.SourceEntity,
                entityId);

            IOriginalWrapper originalWrapper;
            if (!this.originalWrappers.TryGetValue(entityId, out originalWrapper))
            {
                return;
            }

            originalWrapper.ProcessAcknowledgements(this.CurrentEnvelope.SourceEntity, includedSegments, sequenceNumbers);
        }

        #endregion
    }
}