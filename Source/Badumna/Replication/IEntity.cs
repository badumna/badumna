﻿//-----------------------------------------------------------------------
// <copyright file="IEntity.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2010 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.Replication
{
    using System.IO;
    using Badumna.DataTypes;

    /// <summary>
    /// A delegate called by the network layer when a new replica arrives.  The delegate should
    /// return an instance of IReplica to which remote updates and custom messages will be applied.
    /// </summary>
    /// <param name="entityId">The id of the new entity</param>
    /// <param name="entityType">The application specific type ID that was associated with this
    /// entity when the entity was registered on the owning peer.</param>
    /// <returns>An instance of the new entity</returns>
    public delegate IReplica CreateReplica(BadumnaId entityId, EntityTypeId entityType);

    /// <summary>
    /// A delegate called by the network layer when a replica is to be removed. This delegate gives
    /// the application layer the opportunity to cleanup any references to the given replica. It
    /// indicates that no more updates or custom messages will arrive for this replica.
    /// </summary>
    /// <param name="replica">The replica being removed.</param>
    public delegate void RemoveReplica(IReplica replica);

    /// <summary>
    /// A collection of properties common to entities.
    /// </summary>
    public interface IEntity
    {
        /// <summary>
        /// Gets or sets a network wide identifier for the entity.
        /// </summary>
        /// <remarks>
        /// This property is used internally in the Badumna library but can also be used by the
        /// application as a unique reference to an entity. It will be assigned automatically in
        /// NetworkScene.RegisterEntity() if not already assigned from an explicit call to
        /// BadumnaId.GetNextUniqueID();
        /// </remarks>
        BadumnaId Guid { get; set; }

        /// <summary>
        /// Process an event sent to this entity by a call to, e.g.,
        /// NetworkFacade.SendCustomMessageToRemoteCopies().
        /// </summary>
        /// <param name="stream">A stream containing the serialized event data</param>
        void HandleEvent(Stream stream);
    }
}
