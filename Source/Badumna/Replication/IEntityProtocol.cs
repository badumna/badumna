﻿//-----------------------------------------------------------------------
// <copyright file="IEntityProtocol.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using Badumna.Core;
using Badumna.DataTypes;

namespace Badumna.Replication
{
    /// <summary>
    /// The entity protocol interface
    /// </summary>
    internal interface IEntityProtocol : IProtocolComponent<EntityEnvelope>
    {
        /// <summary>
        /// Sends from 'Any' address to given destination entity.
        /// </summary>
        /// <param name="destinationId">The destination id.</param>
        /// <param name="qos">The required qos.</param>
        /// <returns>The entity envelope</returns>
        EntityEnvelope GetMessageFor(BadumnaId destinationId, QualityOfService qos);

        /// <summary>
        /// Gets a message envelope from the given entity. The message destination is not set because the message is 
        /// intended to be sent only to interested peers, which does not require a destination.
        /// </summary>
        /// <param name="entityId">The source entity id.</param>
        /// <param name="qos">The required qos.</param>
        /// <returns>The entity envelope.</returns>
        EntityEnvelope GetMessageFrom(BadumnaId entityId, QualityOfService qos);

        /// <summary>
        /// Sends from a message between specific source and destination entities.
        /// </summary>
        /// <param name="sourceEntityId">Source entity id.</param>
        /// <param name="destinationEntityId">Destination entity id.</param>
        /// <param name="qos">The required qos.</param>
        /// <returns>The entity envelope.</returns>
        EntityEnvelope GetMessageFor(BadumnaId sourceEntityId, BadumnaId destinationEntityId, QualityOfService qos);
    }
}
