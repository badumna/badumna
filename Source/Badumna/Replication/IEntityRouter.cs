﻿//-----------------------------------------------------------------------
// <copyright file="IEntityRouter.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System.Collections.Generic;

using Badumna.DataTypes;
using Badumna.Transport;

namespace Badumna.Replication
{
    /// <summary>
    /// Document this class.
    /// </summary>
    internal interface IEntityRouter
    {
        /// <summary>
        /// Convert the entity entity envelope into a transport envelope.
        /// </summary>
        /// <param name="envelope">The envelope.</param>
        /// <returns>The transport envelope.</returns>
        TransportEnvelope ToTransportEnvelope(EntityEnvelope envelope);

        /// <summary>
        /// Directly send the entity envelope to its specified destination.
        /// </summary>
        /// <param name="envelope">The entity envelope.</param>
        void DirectSend(EntityEnvelope envelope);
    }
}