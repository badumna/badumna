using System;
using System.IO;

using Badumna.Core;
using Badumna.Utilities;
using Badumna.Utilities.Logging;

namespace Badumna.Replication
{
    /// <summary>
    /// Queue for storing arrived sequential messages.
    /// Responsible for removing duplicates and skipping overdue ones
    /// to prevent the queue being blocked.
    /// </summary>
    class CustomMessageQueue : BinaryHeap<CustomMessageQueue.CustomMessage>
    {
        public class CustomMessage : IComparable<CustomMessage>
        {
            public CyclicalID.UShortID MessageNumber;
            public MemoryStream Data;
            public TimeSpan ExpireTime;

            public int CompareTo(object obj)
            {
                CustomMessage customMessage = obj as CustomMessage;
                if (null != customMessage)
                {
                    return this.MessageNumber.CompareTo(customMessage.MessageNumber);
                }
                return this.GetHashCode().CompareTo(obj.GetHashCode());
            }

            public int CompareTo(CustomMessage other)
            {
                return this.MessageNumber.CompareTo(other.MessageNumber);
            }

        }

        private RegularTask mCleanupTask;
        private CyclicalID.UShortID mNextMessageNumber = new CyclicalID.UShortID(0);
        private bool mIsFirstMessage = true;

        /// <summary>
        /// Provides access to the current time.
        /// </summary>
        private ITime timeKeeper;

        public CustomMessageQueue(NetworkEventQueue eventQueue, ITime timeKeeper, INetworkConnectivityReporter connectivityReporter)
        {
            this.timeKeeper = timeKeeper;
            this.mCleanupTask = new RegularTask("Custom message queue cleanup", Parameters.CustomMessageCleanupRate, eventQueue, connectivityReporter, this.CheckBlockages);
            this.mCleanupTask.Start();
        }

        public void Add(CyclicalID.UShortID number, MemoryStream messageData)
        {
            if (this.mIsFirstMessage || this.mNextMessageNumber <= number)
            {
                CustomMessage customMessage = new CustomMessage();

                customMessage.ExpireTime = this.timeKeeper.Now + TimeSpan.FromSeconds(Parameters.CustomMessageCleanupRate.TotalSeconds / 2);
                customMessage.Data = messageData;
                customMessage.MessageNumber = number;

                this.Push(customMessage);
            }

            if (this.mIsFirstMessage)
            {
                this.mNextMessageNumber = number;
                this.mIsFirstMessage = false;
            }
        }

        public CustomMessage InOrderPop()
        {
            if (this.Count == 0)
            {
                return null;
            }
            
            if (this.mNextMessageNumber.Value == this.Peek().MessageNumber.Value)
            {
                CustomMessage message = this.Pop();

                // Remove any duplicate messages
                while (this.Count > 0 && this.Peek().MessageNumber.Value == message.MessageNumber.Value) this.Pop();

                // The next available message is the next in order
                this.mNextMessageNumber.Increment();
                return message;
            }

            return null;
        }

        public void CheckBlockages()
        {
            CustomMessage customMessage = this.Peek();

            if (null != customMessage && this.timeKeeper.Now > customMessage.ExpireTime)
            {
                Logger.TraceWarning(LogTag.Replication, "Skipping custom message(s) {0} through {1} because they have not arrived",
                    this.mNextMessageNumber.Value, customMessage.MessageNumber.Previous.Value);
                this.mNextMessageNumber = customMessage.MessageNumber;
            }
        }
    }
}
