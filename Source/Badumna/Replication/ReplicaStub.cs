using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

using Badumna.Utilities;
using Badumna.DataTypes;

namespace Badumna.Replication
{
    /// <summary>
    /// An empty network entity that is used when an entity is not created by the application
    /// </summary>
    class ReplicaStub : IReplica
    {
        public BadumnaId Guid { get; set; }

        
        public ReplicaStub(BadumnaId id)
        {
            this.Guid = id;
        }

        public void HandleEvent(Stream stream)
        {
        }

        public double Deserialize(BooleanArray includedParts, Stream stream, int estimatedMillisecondsSinceDeparture, BadumnaId id)
        {
            return 0.0001;  // Minimal attention so we don't use much of the source's bandwidth and don't keep requesting instantiation.
        }
    }
}
