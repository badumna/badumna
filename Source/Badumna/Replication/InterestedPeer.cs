﻿using System;
using System.Collections.Generic;
using System.Text;

using Badumna.DataTypes;
using Badumna.Core;
using Badumna.Utilities;
using Badumna.Utilities.Logging;

namespace Badumna.Replication
{
    internal class InterestedPeer : IEnumerable<InterestedEntity>
    {
        /// <summary>
        /// True if we haven't received a flow control message recently.
        /// </summary>
        public bool TimedOut
        {
            get
            {
                double secondsSinceLastFlowControl = (this.timeKeeper.Now - this.timeOfLastFlowControl).TotalSeconds;
                return secondsSinceLastFlowControl > Parameters.FlowControlTimeout.TotalSeconds;
            }
        }

        /// <summary>
        /// Gets the count of interested entities.
        /// </summary>
        /// <value>The count.</value>
        public int Count 
        { 
            get { return this.interestEntities.Count; } 
        }

        /// <summary>
        /// All interested entities on this peer. 
        /// </summary>
        private List<InterestedEntity> interestEntities;

        /// <summary>
        /// When the last flow control message was received.
        /// </summary>
        private TimeSpan timeOfLastFlowControl;

        /// <summary>
        /// Provides access to the current time.
        /// </summary>
        private ITime timeKeeper;

        /// <summary>
        /// Initializes a new instance of the <see cref="InterestedPeer"/> class.
        /// </summary>
        public InterestedPeer(ITime timeKeeper)
        {
            this.timeKeeper = timeKeeper;
            this.interestEntities = new List<InterestedEntity>();
            this.timeOfLastFlowControl = this.timeKeeper.Now;
        }

        /// <summary>
        /// Adds the specified entity.
        /// </summary>
        /// <param name="entityId">The entity id.</param>
        /// <returns>Whether added.</returns>
        public bool Add(BadumnaId entityId)
        {
            if (this.Contains(entityId))
            {
                Logger.TraceInformation(LogTag.Entity | LogTag.Event, "Ignoring already-interested entity {0}", entityId);
                return false;
            }

            Logger.TraceInformation(LogTag.Entity | LogTag.Event, "Interested add {0}", entityId);

            InterestedEntity entity = new InterestedEntity(entityId, this.timeKeeper);
            this.interestEntities.Add(entity);
            return true;
        }

        /// <summary>
        /// Removes the specified entity id.
        /// </summary>
        /// <param name="entityId">The entity id.</param>
        /// <returns></returns>
        public bool Remove(BadumnaId entityId)
        {
            if (!this.Contains(entityId))
            {
                return false;
            }

            Logger.TraceInformation(LogTag.Entity | LogTag.Event, "Interested remove {0}", entityId);

            foreach (InterestedEntity entity in this.interestEntities)
            {
                if (entity.EntityId.Equals(entityId))
                {
                    this.interestEntities.Remove(entity);
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Determines whether contains the specified entity id.
        /// </summary>
        /// <param name="entityId">The entity id.</param>
        /// <returns>
        /// <c>true</c> if contains the specified entity id; otherwise, <c>false</c>.
        /// </returns>
        public bool Contains(BadumnaId entityId)
        {
            foreach (InterestedEntity entity in this.interestEntities)
            {
                if (entity.EntityId.Equals(entityId))
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Applies the flow control feedback.
        /// </summary>
        /// <param name="maximumUpdateRate">The maximum update rate.</param>
        /// <param name="attention">The attention.</param>
        public void ApplyFlowControlFeedback(ushort maximumUpdateRate, float attention)
        {
            this.timeOfLastFlowControl = this.timeKeeper.Now;
        }

        /// <summary>
        /// Returns an enumerator that iterates through the collection.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.Collections.Generic.IEnumerator`1"/> that can be used to iterate through the collection.
        /// </returns>
        public IEnumerator<InterestedEntity> GetEnumerator()
        {
            return this.interestEntities.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }
    }
}


