﻿//-----------------------------------------------------------------------
// <copyright file="DataSeries.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.Utilities
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Delegate for creating instances implementing the IDataSeries interface.
    /// </summary>
    /// <param name="size">The size of the data series.</param>
    /// <returns>A new instance implementing the IDataSeries interface.</returns>
    internal delegate IDataSeries DataSeriesFactory(int size);
    
    /// <summary>
    /// Interface for the DataSeries class.
    /// </summary>
    internal interface IDataSeries
    {
        /// <summary>
        /// Gets the mean of the data series.
        /// </summary>
        double Mean { get; }

        /// <summary>
        /// Gets the standard deviation of the data series.
        /// </summary>
        double StdDev { get; }

        /// <summary>
        /// Add a data point to the series.
        /// </summary>
        /// <param name="datum">The data point.</param>
        void Add(double datum);
    }

    /// <summary>
    /// Holds a series of data points for calculating a mean and standard deviation.
    /// The series has a fixed maximum size, which when exceeded, will cause oldest points to be replaced.
    /// </summary>
    internal class DataSeries : IDataSeries
    {
        /// <summary>
        /// Size of the data series. Once the size is reached, new data points replcaed the oldest ones.
        /// </summary>
        private int size;

        /// <summary>
        /// Index tracking the where to insert new data points when the series is full.
        /// </summary>
        private int head;

        /// <summary>
        /// The number of data points recorded.
        /// </summary>
        private int n;

        /// <summary>
        /// The sum of the data points.
        /// </summary>
        private double sum;

        /// <summary>
        /// The data points.
        /// </summary>
        private List<double> data;

        /// <summary>
        /// Initializes a new instance of the DataSeries class.
        /// </summary>
        /// <param name="size">The maximum number of data points to hold.</param>
        public DataSeries(int size)
        {
            this.size = size;
            this.data = new List<double>(this.size);
        }

        /// <inheritdoc/>
        public double Mean
        {
            get
            {
                if (this.n == 0)
                {
                    return double.MaxValue;
                }
                else
                {
                    return this.sum / this.n;
                }
            }
        }

        /// <inheritdoc/>
        public double StdDev
        {
            get
            {
                int n = 0;
                double mean = 0;
                double meanSquared = 0;
                foreach (double x in this.data)
                {
                    n += 1;
                    double delta = x - mean;
                    mean += delta / n;
                    meanSquared += delta * (x - mean); // Using new mean;
                }

                return Math.Sqrt(meanSquared / n);
            }
        }

        /// <inheritdoc/>
        public void Add(double datum)
        {
            if (this.data.Count < this.size)
            {
                this.data.Add(datum);
                this.n++;
                this.sum += datum;
            }
            else
            {
                this.sum -= this.data[this.head];
                this.sum += datum;
                this.data[this.head] = datum;
                this.head++;
                this.head = this.head % this.size;
            }
        }
    }
}
