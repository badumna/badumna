using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;

namespace Badumna.Utilities
{
    /// <summary>
    /// Used for collection of network statistics
    /// </summary>
    public interface IStatistics
    {
        /// <summary>
        /// Recore a specific statistic
        /// </summary>
        /// <param name="group"></param>
        /// <param name="name"></param>
        /// <param name="value"></param>
        void Record(String group, String name, double value);
    }

    static class StatUtils
    {
        static public bool IsImRelated(string parserName)
        {
#if TRACE
            return new List<string>
            {
                "DhtProtocol",
                "DhtFacade",
                "TieredRouter",
                "ReplicaManager`1",
                "IdAddressedRouter",
                "GossipIMService"
            }.Contains(parserName);
#else
            return false;
#endif
        }
    }
}
