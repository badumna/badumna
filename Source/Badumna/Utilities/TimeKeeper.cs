﻿using System;
using System.Diagnostics;
using System.Reflection;
using System.Collections.Generic;
using System.Threading;
using Badumna.Platform;

namespace Badumna.Utilities
{
    /// <summary>
    /// Keeps track of elapsed time.  Unaffected by changes made to the system clock.
    /// </summary>
    internal class TimeKeeper : IDisposable
    {
        private enum TickCountMode
        {
            EnvironmentTickCount,
            Mono125Workaround,
            WindowsTimeGetTime,
        }

        private const uint TimeGetTimeResolution = 1;  // ms

        private long mElapsedMilliseconds;

        private int mLastCount;

        /// <summary>
        /// Returns a value indicating whether the TimeKeeper is currently running or paused.
        /// </summary>
        public bool IsRunning { get; private set; }

        private readonly object mUpdateLock = new object();

        /// <summary>
        /// Used for Mono125Workaround mode.  Stores the last good tick count.  When the
        /// tick count goes backwards, this is used as a temporary base to which deltas
        /// from mDroppedTickCount are added.
        /// </summary>
        private int mLastValidEnvTickCount;

        /// <summary>
        /// Used for Mono125Workaround mode.  Stores the tick count at the time the a
        /// drop occurs.  Until the clock is restored to the correct time, deltas are
        /// taken from this point to estimate the time.
        /// </summary>
        private int mDroppedTickCount;

        /// <summary>
        /// Identifies the method used to get a tick count from the OS.
        /// </summary>
        private TickCountMode tickCountMode;

        /// <summary>
        /// Indicates whether the object has already been disposed.
        /// </summary>
        private bool isDisposed;


        public TimeKeeper()
        {
            this.tickCountMode = TickCountMode.EnvironmentTickCount;

            if (ExecutionPlatform.IsRunningOnOldMacMono())
            {
                this.tickCountMode = TickCountMode.Mono125Workaround;
                this.mLastValidEnvTickCount = Environment.TickCount - 1;
            }
            else
            {
#if !IOS
                try
                {
                    WinMM.TimeBeginPeriod(TimeKeeper.TimeGetTimeResolution);
                    this.tickCountMode = TickCountMode.WindowsTimeGetTime;
                }
                catch (Exception)
                {
                }
#endif
            }
        }

        public override string ToString()
        {
            return this.tickCountMode.ToString();
        }

        ~TimeKeeper()
        {
            this.Dispose(false);
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (this.isDisposed)
            {
                return;
            }
            this.isDisposed = true;

            if (this.tickCountMode == TickCountMode.WindowsTimeGetTime)
            {
                WinMM.TimeEndPeriod(TimeKeeper.TimeGetTimeResolution);
            }
        }

        public TimeSpan Elapsed
        {
            get
            {
                return new TimeSpan(this.UpdateElapsedTicks() * TimeSpan.TicksPerMillisecond);
            }
        }

        /// <summary>
        /// Returns the estimated timer resolution in milliseconds.  If the resolution cannot
        /// be determined this method will return double.PositiveInfinity.
        /// </summary>
        /// <remarks>
        /// This method may take some time to run depending on the actual resolution of the clock.
        /// It should complete in less than 200ms, however because it runs at highest priority
        /// everything else will block for the duration.
        /// </remarks>
        /// <returns>The estimated timer resolution in milliseconds or double.PositiveInfinity if estimation
        /// fails.</returns>
        public double EstimateResolution()
        {
            ThreadPriority originalPriority = Thread.CurrentThread.Priority;
            try
            {
                Thread.CurrentThread.Priority = ThreadPriority.Highest;

                int iterations = 1;
                int periods = 0;  // A period is the time between two different samples.

                // Don't want to make the number of periods too large because if the resolution is coarse it'll take a long time to run.
                int maxPeriods = 10;

                int sample = this.GetTickCount();
                int first = sample;
                while (periods < maxPeriods)
                {
                    int current = this.GetTickCount();

                    if (current != sample)
                    {
                        periods++;
                        sample = current;
                    }

                    iterations++;
                }

                if (iterations < periods * 2)
                {
                    // Sampling loop is too slow to detect timer resolution -- should have
                    // at least two iterations for each periods.
                    return double.PositiveInfinity;
                }

                double averagePeriodLength = (double)(sample - first) / periods;
                return averagePeriodLength;
            }
            finally
            {
                Thread.CurrentThread.Priority = originalPriority;
            }
        }

        public void Reset()
        {
            lock (this.mUpdateLock)
            {
                this.mElapsedMilliseconds = 0;
                this.mLastCount = this.GetTickCount();  // In case we're currently unpaused
            }
        }

        public void Restart()
        {
            this.Reset();
            this.Start();
        }

        public void Start()
        {
            lock (this.mUpdateLock)
            {
                if (!this.IsRunning)
                {
                    this.IsRunning = true;
                    this.mLastCount = this.GetTickCount();
                }
            }
        }

        public void Pause()
        {
            lock (this.mUpdateLock)
            {
                if (this.IsRunning)
                {
                    this.UpdateElapsedTicks();
                    this.IsRunning = false;
                }
            }
        }

        // Currently implemented using System.Environment.TickCount which has a resolution of
        // at most 1ms.  Could change to using System.Diagnostics.Stopwatch, but it seems like
        // that might have issues on some hardware [http://www.virtualdub.org/blog/pivot/entry.php?id=106].
        //
        // Stopwatch doesn't work in unity webplayer mode.
        /// <summary>
        /// Returns the 'tick count', which must be a monotonically increasing value that tracks real time in milliseconds.
        /// </summary>
        /// <returns>The current 'tick count' in milliseconds</returns>
        protected virtual int GetTickCount()
        {
            switch (this.tickCountMode)
            {
                case TickCountMode.EnvironmentTickCount:
                    return Environment.TickCount;

                case TickCountMode.WindowsTimeGetTime:
                    return unchecked((int)WinMM.TimeGetTime());  // So we can do signed subtraction later.  The bits stay the same.

                case TickCountMode.Mono125Workaround:
                    int tick = Environment.TickCount;
                    if (tick < this.mLastValidEnvTickCount)
                    {
                        // had a sudden value drop
                        if (this.mDroppedTickCount == 0)
                        {
                            this.mDroppedTickCount = tick;
                            return this.mLastValidEnvTickCount;
                        }
                        else
                        {
                            return this.mLastValidEnvTickCount + (tick - this.mDroppedTickCount);
                        }
                    }
                    else
                    {
                        // DGC: Confirm with Lei.  Clock could still go backwards?  If it doesn't catch up to quite the correct time when
                        //      it resyncs.  e.g.  Env.TC (our TC):  1 (1), 2 (2), 3 (3), 4 (4), 5 (5), 1 (5), 2 (6), 3 (7), 6 (6)
                        //      Maybe the actual code in Mono 1.2.5 means this can not occur though.
                        this.mDroppedTickCount = 0;
                        this.mLastValidEnvTickCount = tick;
                        return tick;
                    }

                default:
                    throw new InvalidOperationException();
            }
        }

        /// <summary>
        /// Updates and returns the elapsed time in milliseconds.
        /// </summary>
        /// <returns>Elapsed time in milliseconds</returns>
        private long UpdateElapsedTicks()
        {
            return Synchronization.Lock(this.mUpdateLock, delegate
            {
                if (!this.IsRunning)
                {
                    return this.mElapsedMilliseconds;
                }

                int now = this.GetTickCount();
                int elapsed;
                unchecked
                {
                    elapsed = now - this.mLastCount;
                }
                if (elapsed < 0)
                {
                    int oldLastCount = this.mLastCount;
                    this.mLastCount = now;
                    Debug.Assert(false, String.Format("Time is running backwards! (change = {0}, now = {1}, last count = {2})", elapsed, now, oldLastCount));

                    return this.mElapsedMilliseconds;
                }

                this.mElapsedMilliseconds += elapsed;
                this.mLastCount = now;

                return this.mElapsedMilliseconds;
            });
        }
    }
}
