using System;
using System.Collections.Generic;
using System.Text;

namespace Badumna.Utilities
{
    internal delegate void GenericCallBack();
    internal delegate void GenericCallBack<A>(A arg1);
    internal delegate void GenericCallBack<A, B>(A arg1, B arg2);
    internal delegate void GenericCallBack<A, B, C>(A arg1, B arg2, C arg3);
    internal delegate void GenericCallBack<A, B, C, D>(A arg1, B arg2, C arg3, D arg4);
    internal delegate void GenericCallBack<A, B, C, D, E>(A arg1, B arg2, C arg3, D arg4, E arg5);
    internal delegate void GenericCallBack<A, B, C, D, E, F>(A arg1, B arg2, C arg3, D arg4, E arg5, F arg6);

    internal delegate R GenericCallBackReturn<R>();
    internal delegate R GenericCallBackReturn<R, A>(A arg1);
    internal delegate R GenericCallBackReturn<R, A, B>(A arg1, B arg2);
    internal delegate R GenericCallBackReturn<R, A, B, C>(A arg1, B arg2, C arg3);
    internal delegate R GenericCallBackReturn<R, A, B, C, D>(A arg1, B arg2, C arg3, D arg4);
    internal delegate R GenericCallBackReturn<R, A, B, C, D, E>(A arg1, B arg2, C arg3, D arg4, E arg5);
    internal delegate R GenericCallBackReturn<R, A, B, C, D, E, F>(A arg1, B arg2, C arg3, D arg4, E arg5, F arg6);
}
