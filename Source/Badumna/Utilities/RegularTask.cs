using System;
using System.Collections.Generic;
using System.Text;

using Badumna.Core;

namespace Badumna.Utilities
{
    /// <summary>
    /// Delegate for a method that creates a regular task.
    /// </summary>
    /// <param name="description">A descripton of the regular task.</param>
    /// <param name="period">The period to repeat the task at.</param>
    /// <param name="queue">The event scheduler.</param>
    /// <param name="connectivityReporter">Reports on network connectivity status.</param>
    /// <param name="task">The task to repeat.</param>
    /// <returns>A regular task.</returns>
    internal delegate IRegularTask RegularTaskFactory(
        string description,
        TimeSpan period,
        INetworkEventScheduler queue,
        INetworkConnectivityReporter connectivityReporter,
        GenericCallBack task);

    internal interface IRegularTask
    {
        double PeriodMilliseconds { get; set; }
        
        bool IsRunning { get; }
        
        void Start();
        
        void Stop();
        
        void Delay();

        /// <summary>
        /// Changes the next execution time of the task so that is doesn't run until a full period has elasped from now.
        /// If the RegularTask is currently stopped, this call has no effect.
        /// </summary>
        /// <param name="newPeriod">The new period.</param>
        void Delay(TimeSpan newPeriod);

        string ToString();
}

    class RegularTask : IRegularTask
    {
        private NetworkEvent mEvent;

        protected Delegate mTask;
        private Object[] mArguments;

        private string mDescription;
        private double mPeriodMilliseconds;
        public double PeriodMilliseconds
        {
            get { return this.mPeriodMilliseconds; }
            set { this.mPeriodMilliseconds = value; }
        }

        private bool mRunning;
        public bool IsRunning { get { return this.mRunning; } }

        /// <summary>
        /// The queue to use for scheduling events.
        /// </summary>
        private INetworkEventScheduler eventQueue;

        /// <summary>
        /// Reports on network connectivity status.
        /// </summary>
        private INetworkConnectivityReporter connectivityReporter;

        protected RegularTask(string description, TimeSpan period, INetworkEventScheduler eventQueue, INetworkConnectivityReporter connectivityReporter)
        {
            this.eventQueue = eventQueue;
            this.connectivityReporter = connectivityReporter;
            this.mDescription = description;
            this.mPeriodMilliseconds = period.TotalMilliseconds;
        }

        public RegularTask(
            string description,
            TimeSpan period,
            INetworkEventScheduler eventQueue,
            INetworkConnectivityReporter connectivityReporter,
            GenericCallBack task) :
            this(description, period, eventQueue, connectivityReporter)
        {
            this.mTask = task;
            this.mArguments = null;
        }

        public RegularTask(
            string description,
            TimeSpan period,
            INetworkEventScheduler eventQueue,
            INetworkConnectivityReporter connectivityReporter,
            Delegate task,
            params Object[] args) :
            this(description, period, eventQueue, connectivityReporter)
        {
            this.mTask = task;
            this.mArguments = args;
        }

        public void Start()
        {
            if (this.mRunning)
            {
                return;
            }

            this.mRunning = true;
            this.connectivityReporter.NetworkOnlineEvent += this.OnNetworkOnline;
            this.connectivityReporter.NetworkOfflineEvent += this.OnNetworkOffline;
            if (this.connectivityReporter.Status == ConnectivityStatus.Online)
            {
                this.ScheduleEvent();
            }
        }

        public void Stop()
        {
            if (!this.mRunning)
            {
                return;
            }

            this.mRunning = false;
            this.connectivityReporter.NetworkOnlineEvent -= this.OnNetworkOnline;
            this.connectivityReporter.NetworkOfflineEvent -= this.OnNetworkOffline;
            this.RemoveEvent();
        }

        /// <summary>
        /// Changes the next execution time of the task so that is doesn't run until a full period has elasped from now.
        /// If the RegularTask is currently stopped, this call has no effect.
        /// </summary>
        public void Delay()
        {
            if (!this.mRunning)
            {
                return;
            }

            this.RemoveEvent();
            this.ScheduleEvent();
        }

        /// <summary>
        /// Changes the next execution time of the task so that is doesn't run until a full period has elasped from now.
        /// If the RegularTask is currently stopped, this call has no effect.
        /// </summary>
        /// <param name="newPeriod">The new period.</param>
        public void Delay(TimeSpan newPeriod)
        {
            this.mPeriodMilliseconds = newPeriod.TotalMilliseconds;
            if (!this.mRunning)
            {
                return;
            }

            this.RemoveEvent();
            this.ScheduleEvent();
        }

        private void OnNetworkOnline()
        {
            this.ScheduleEvent();
        }

        private void OnNetworkOffline()
        {
            this.RemoveEvent();
        }

        protected virtual void ScheduleEvent()
        {
            // Only schedule if there isn't already one, and the network is up (TODO: Possible race?)
            if (!this.mRunning || this.connectivityReporter.Status != ConnectivityStatus.Online ||
                this.mEvent != null)
            {
                return;
            }

            this.mEvent = this.eventQueue.Schedule(this.mPeriodMilliseconds, this.Execute);
        }

        private void RemoveEvent()
        {
            if (this.mEvent != null)
            {
                this.eventQueue.Remove(this.mEvent);
                this.mEvent = null;
            }
        }

        private void Execute()
        {
            this.mEvent = null;  // Event is executing, so it's no longer on the event queue
            this.ScheduleEvent();
            this.mTask.Method.Invoke(this.mTask.Target, this.mArguments);
        }

        public override string ToString()
        {
            return this.mDescription;
        }
    }
}
