/***************************************************************************
 *  File Name: CyclicalID.cs
 *
 *  Copyright (C) 2007 All Rights Reserved.
 * 
 *  Written by
 *      David Churchill <dgc@csse.unimelb.edu.au>
 ****************************************************************************/


using System;
using System.Collections.Generic;
using System.Text;

using Badumna.Core;

namespace Badumna.Utilities
{
    /// <remarks>
    /// The CyclicalID class provides ID numbers from a space that is expected to be exhausted and reused.  Comparison functions
    /// are provided to determine whether a CyclicalID falls before or after another CyclicalID chronologically.  Ordering of IDs
    /// relies on the assumption that any two IDs compared were generated close together.  Depending on the size of the address
    /// space and the rate of use of IDs it may not always be possible to reliably determine the ordering.
    /// 
    /// Actual cyclical ID classes are implemented as nested classes of CyclicalID relying on the CyclicalID.Compare(...) static 
    /// function.  This is done because cyclical ID classes should really be value types given their intended use, and structs
    /// cannot be part of an inheritance hierarchy.  It should be possible to implement further cyclical IDs by converting the
    /// current CyclicalID.UShortID into a generic type, but this gets hairy because there's no constraint to indicate that
    /// a generic type should be an integer.
    /// </remarks>
    static class CyclicalID
    {
        public static int Compare(uint max, uint left, uint right)
        {
            if (left == right)
                return 0;

            bool swapped = false;
            if (left > right)
            {
                uint tmp = left;
                left = right;
                right = tmp;
                swapped = true;
            }

            uint diff1 = right - left;
            uint diff2 = (left - 0) + (max - right + 1);  // + 1 accounts for interval between max and 0

            // The case where diff1 == diff2 is indeterminate.  So that the function gives equivalent results
            // regardless of the order of the 'left' and 'right' parameters, it returns the result of an ordinary
            // comparison in this case.
            int result;
            if (diff1 <= diff2)
                result = -1;  // left preceeds right
            else
                result = 1;

            return swapped ? -result : result;
        }

        public static uint Distance(uint max, uint low, uint high)
        {
            return (high + max - low) % max;
        }

        public struct UShortID : IParseable, IComparable<UShortID>
        {
            private ushort mValue;

            /// <value>
            /// The current ID
            /// </value>
            public ushort Value
            {
                get { return mValue; }
                set { mValue = value; }
            }

            public ushort MaxValue
            {
                get { return ushort.MaxValue; }
            }

            public UShortID Previous
            {
                get
                {
                    if (0 == this.Value)
                    {
                        return new UShortID(this.MaxValue);
                    }

                    return new UShortID((ushort)(this.Value - 1));
                }
            }

            public static UShortID operator -(UShortID a, UShortID b)
            {
                if (a <= b)
                {
                    if (a.Value <= b.Value)
                    {
                        return new UShortID((ushort)(b.Value - a.Value));
                    }
                    else
                    {
                        return new UShortID((ushort)((a.MaxValue - a.Value) + b.Value));
                    }
                }
                else
                {
                    return b - a;
                }
            }

            /// <summary>
            /// Constructs a new UShortID.
            /// </summary>
            /// <param name="initialValue">Initial value for the ID</param>
            public UShortID(ushort initialValue)
            {
                this.mValue = initialValue;
            }

            /// <summary>
            /// Increments the ID.
            /// </summary>
            public void Increment()
            {
                unchecked
                {
                    this.mValue++;
                }
            }

            /// <summary>
            /// Returns the distance from this id to the other.  This id is assumed to be less than other.
            /// </summary>
            /// <param name="other"></param>
            /// <returns></returns>
            public uint DistanceTo(UShortID other)
            {
                return CyclicalID.Distance(this.MaxValue, this.mValue, other.mValue);
            }
            
            public static bool operator !=(UShortID left, UShortID right)
            {
                return left.CompareTo(right) != 0;
            }

            public static bool operator ==(UShortID left, UShortID right)
            {
                return left.CompareTo(right) == 0;
            }

            public static bool operator <(UShortID left, UShortID right)
            {
                return left.CompareTo(right) < 0;
            }

            public static bool operator >(UShortID left, UShortID right)
            {
                return left.CompareTo(right) > 0;
            }

            public static bool operator <=(UShortID left, UShortID right)
            {
                return left.CompareTo(right) <= 0;
            }

            public static bool operator >=(UShortID left, UShortID right)
            {
                return left.CompareTo(right) >= 0;
            }

            public override bool Equals(Object other)
            {
                if (other is UShortID)
                {
                    return this.CompareTo((UShortID)other) == 0;
                }

                return false;
            }

            public override int GetHashCode()
            {
                return (int)this.mValue;
            }

            public override String ToString()
            {
                return this.mValue.ToString();
            }


            #region IParseable Members

            public void ToMessage(MessageBuffer message, Type parameterType)
            {
                message.Write(this.mValue);
            }

            public void FromMessage(MessageBuffer message)
            {
                this.mValue = message.ReadUShort();
            }

            #endregion

            #region IComparable<UShortID> Members

            public int CompareTo(UShortID other)
            {
                return CyclicalID.Compare(this.MaxValue, this.mValue, other.mValue);
            }

            #endregion
        }
    }
}
