﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Badumna.Utilities
{
    class PositiveSmoother : Smoother
    {
        /// <param name="smoothingFactor">The amount of smoothing that is applied to the estimation. 
        /// Higher values will result in greater smoothing, which means the estimate will be less susceptible to 
        /// short burst but also slower to converge to the average.</param>
        /// <param name="initialValue">The starting value</param>
        public PositiveSmoother(double smoothingFactor, double initialValue)
            : base(smoothingFactor, initialValue)
        {
        }

        public override void ValueEvent(double value)
        {
            base.ValueEvent(value);
            this.mSmoothedMeanEstimator = Math.Max(0, this.mSmoothedMeanEstimator);
        }
    }
}
