/***************************************************************************
 *  File Name: MessageDumper.cs
 *
 *  Copyright (C) 2007 All Rights Reserved.
 * 
 *  Written by
 *      David Churchill <dgc@csse.unimelb.edu.au>
 * 
 *  TODO: This should all really be part of MessageParser somehow (modify Parse(...) to take a delegate?)
 ****************************************************************************/
/*
#if DEBUG


using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Reflection;

using Badumna.Core;


namespace Badumna.Utilities
{
    public class MessageDumper<EnvelopeType> where EnvelopeType : BaseEnvelope
    {
        internal class ProtocolTreeNode
        {
            private ProtocolTreeNode parent;
            public ProtocolTreeNode Parent { get { return parent; } }

            private String mName;
            public String Name { get { return this.mName; } }

            private List<ProtocolTreeNode> mChildren = new List<ProtocolTreeNode>();
            public List<ProtocolTreeNode> Children { get { return this.mChildren; } }

            private List<MethodInfo> mMethods = new List<MethodInfo>();
            public List<MethodInfo> Methods { get { return this.mMethods; } }

            public ProtocolTreeNode(String name)
                : this(name, null)
            {
            }

            private ProtocolTreeNode(String name, ProtocolTreeNode parent)
            {
                this.parent = parent;
                this.mName = name;
            }

            public ProtocolTreeNode NewChild(String name)
            {
                ProtocolTreeNode newNode = new ProtocolTreeNode(name, this);
                this.mChildren.Add(newNode);
                return newNode;
            }

            public void AddMethod(MethodInfo info)
            {
                this.mMethods.Add(info);
            }
        }

        internal class TreeBuildingCrawler : ProtocolStackCrawler
        {
            private ProtocolTreeNode mTree;
            public ProtocolTreeNode Tree { get { return this.mTree; } }

            private ProtocolTreeNode mCurrentNode;

            public TreeBuildingCrawler()
            {
            }

            public override void VisitLayer(MessageParser layer)
            {
                if (this.mCurrentNode == null)
                {
                    this.mTree = new ProtocolTreeNode(layer.GetType().Name);
                    this.mCurrentNode = this.mTree;
                }
                else
                {
                    this.mCurrentNode = this.mCurrentNode.NewChild(layer.GetType().Name);
                }
            }

            public override void VisitMethod(object target, MethodInfo method)
            {
                this.mCurrentNode.AddMethod(method);
            }

            public override void LeaveLayer(MessageParser layer)
            {
                this.mCurrentNode = mCurrentNode.Parent;
            }
        }


        private ProtocolTreeNode mTree;
        private const int mReservedMethodIndexes = 16;  // this is not good

        public MessageDumper(ProtocolLayer<EnvelopeType> protocolStack)
        {
            TreeBuildingCrawler crawler = new TreeBuildingCrawler();
            protocolStack.Traverse(crawler);
            this.mTree = crawler.Tree;
        }

        public String Dump(Message message)
        {
            ProtocolTreeNode currentNode = this.mTree;
            StringBuilder result = new StringBuilder();

            while (message.Available > 0)
            {
                byte id = message.ReadByte();  // assuming only one byte used...

                if (id == MessageParser.EndOfMessageMarker)
                {
                    return result + " EOM " + Dump(message);
                }

                if (id < MessageDumper<EnvelopeType>.mReservedMethodIndexes)
                {
                    currentNode = currentNode.Children[id];
                    result.Append(currentNode.Name).Append(" ");
                    continue;
                }
                else
                {
                    MethodInfo info = currentNode.Methods[id - MessageDumper<EnvelopeType>.mReservedMethodIndexes];

                    result.Append(info.Name).Append("(");

                    ParameterInfo[] paramTypeList = info.GetParameters();
                    for (int i = 0; i < paramTypeList.Length; i++)
                    {
                        if (i != 0)
                        {
                            result.Append(", ");
                       }

                        Object parameter = message.GetObject(paramTypeList[i].ParameterType);
                        MessageParser.ObjectToString(parameter, result);
                    }

                    result.Append(") ");
                }
            }


            return result.ToString();
        }
    }
}

#endif // DEBUG
*/