﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Badumna.Utilities
{
    class ResourceAllocator<T>
    {
        class ResourceSlice
        {
            public PositiveSmoother WeightSmoother;
            public double DesiredAmount;
            public double AllocatedAmount;
        }

        private double mMaximumAmount;
        private double mMinimumAmount;
        private Dictionary<T, ResourceSlice> mSlices;

        private double mTotalWeight;
        private double mTotalDesiredAmount;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="maximumAmount">The maximum total resource to allocate</param>
        /// <param name="minimumAmount">The minimum amount of resource that a slice will accept. 
        /// I.e. If there is less resource than this amount available for a particular slice it will get non of the resource</param>
        public ResourceAllocator(double maximumAmount, double minimumAmount)
        {
            this.mMaximumAmount = maximumAmount;
            this.mMinimumAmount = minimumAmount;
            this.mSlices = new Dictionary<T, ResourceSlice>();
        }

        /// <summary>
        /// Assign or adjust a slice of the resource with the given index.
        /// </summary>
        /// <param name="sliceIndex">An identifier for the slice</param>
        /// <param name="desiredRate">The amount consumed by the slice if the resource wasn't over constrained. 
        /// Values over the maximum rate of the resource are limited to the maximum.</param>
        /// <param name="weight">A value indicating the weight of this slice. The value must be between 0.0 and 1.0 (inclusive)
        /// and can be independent of other weights (i.e. the sum of weights can be != 1.0). 
        /// Smaller weights indicate lower priority.</param>
        public void AllocateSlice(T sliceIndex, double desiredRate, float weight)
        {
            if (weight < 0.0 || weight > 1.0)
            {
                throw new InvalidOperationException("Weight must be between 0 and 1");
            }

            ResourceSlice slice = null;
            double inverseWeight = 1.0 - weight;

            if (!this.mSlices.TryGetValue(sliceIndex, out slice))
            {
                slice = new ResourceAllocator<T>.ResourceSlice();
                slice.WeightSmoother = new PositiveSmoother(5.0, inverseWeight); // TODO : Find an appropriate value for the smoothing factor
                slice.AllocatedAmount = this.mMaximumAmount / (double)this.mSlices.Count;
                this.mSlices.Add(sliceIndex, slice);
            }
            else
            {
                this.mTotalDesiredAmount -= slice.DesiredAmount;
                this.mTotalWeight -= slice.WeightSmoother.EstimatedValue;
                slice.WeightSmoother.ValueEvent(inverseWeight);
            }

            slice.DesiredAmount = Math.Min(this.mMaximumAmount, desiredRate);
            this.mTotalWeight += slice.WeightSmoother.EstimatedValue;
            this.mTotalDesiredAmount += slice.DesiredAmount;
        }

        public void AllocateWeight(T sliceIndex, double weight)
        {
            ResourceSlice slice = null;
            double inverseWeight = 1.0 - weight;

            if (this.mSlices.TryGetValue(sliceIndex, out slice))
            {
                this.mTotalWeight -= slice.WeightSmoother.EstimatedValue;
                slice.WeightSmoother.ValueEvent(inverseWeight);
                this.mTotalWeight += slice.WeightSmoother.EstimatedValue;
            }
        }

        public void RemoveSlice(T sliceIndex)
        {
            ResourceSlice slice = null;

            if (this.mSlices.TryGetValue(sliceIndex, out slice))
            {
                this.mTotalWeight -= slice.WeightSmoother.EstimatedValue;
                this.mTotalDesiredAmount -= slice.DesiredAmount;
                this.mSlices.Remove(sliceIndex);
            }
        }

        public void CalculateAllocation()
        {
            double deficit = this.mTotalDesiredAmount - this.mMaximumAmount;
            double leftOver = 0;
            int numberOfActiveSlices = 0;

            foreach (ResourceSlice slice in this.mSlices.Values)
            {
                // When there is not enough resource to service all slices, deny each slice an amount
                // propotional to that slices weight. Alternatevily, if there is enough resource then
                // allocate extra according to weights.
                double scaledWeight = this.mTotalWeight == 0.0 ? 1.0 / (double)this.mSlices.Count : (slice.WeightSmoother.EstimatedValue / this.mTotalWeight);
                double allocation = slice.DesiredAmount - (deficit * scaledWeight);

                if (allocation < this.mMinimumAmount)
                {
                    slice.AllocatedAmount = 0;
                    leftOver += allocation;
                }
                else
                {
                    slice.AllocatedAmount = allocation;
                    numberOfActiveSlices++;
                }
            }

            // Redistribute left over resource to the remaining slices equally.
            double redistributedLeftOver = leftOver / (double)numberOfActiveSlices;

            foreach (ResourceSlice slice in this.mSlices.Values)
            {
                if (slice.AllocatedAmount != 0)
                {
                    slice.AllocatedAmount += redistributedLeftOver;
                }
            }
        }

        public double GetAllocatedRate(T sliceIndex)
        {
            ResourceSlice slice = null;

            if (this.mSlices.TryGetValue(sliceIndex, out slice))
            {
                return slice.AllocatedAmount;
            }

            return 0;
        }

        public double GetWeight(T sliceIndex)
        {
            ResourceSlice slice = null;

            if (this.mSlices.TryGetValue(sliceIndex, out slice))
            {
                return 1.0 - slice.WeightSmoother.EstimatedValue;
            }

            return 0;
        }
    }
}
