﻿//---------------------------------------------------------------------------------
// <copyright file="DataScrambleScheme.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

using System;

namespace Badumna.Utilities
{
    /// <summary>
    /// A simple scrambling scheme for UDP over UDP. 
    /// In this scheme, for each non-empty packet, 4 bytes junk data is inserted at the beginning of the payload. The 
    /// payload is divided into blocks each with DataScrambleScheme.BlockSize bytes (the last block may have less than
    /// DataScrambleScheme.BlockSize bytes), each of these blocks is then expended with 2 bytes CRC16 checksum. The 
    /// layout of the scrambled data is shown below:
    ///   -----------------------------------------------------------------------------------------------
    ///   | 4bytes junk | block 0 | 2 bytes crc | block 1 | 2 bytes crc | ..... | block N | 2 bytes crc | 
    ///   -----------------------------------------------------------------------------------------------
    /// For empty packet which has a 0 byte payload, the packet is unchanged.
    /// </summary>
    public class DataScrambleScheme
    {
        /// <summary>
        /// Size of the junk data at the beginning. 
        /// </summary>
        private static readonly int JunkSize = 4;

        /// <summary>
        /// Size of each checksum.
        /// </summary>
        private static readonly int ChecksumSize = 2;

        /// <summary>
        /// The min size of a scrambled payload.
        /// The minimal length of scrambed data is 4 bytes junk + 1 byte payload + 2 bytes checksum.
        /// </summary>
        private static readonly int MinScrambledPayloadSize = 7;

        /// <summary>
        /// Size of each block in bytes that will be checksumed. 
        /// </summary>
        private static readonly int BlockSize = 64;

        /// <summary>
        /// The random number generator.
        /// </summary>
        private static Random random = new Random(Environment.TickCount);

        /// <summary>
        /// Scrambles the UDP data.
        /// </summary>
        /// <exception cref="System.ArgumentNullException">Thrown when original is null.</exception>
        /// <param name="original">The original data in byte array.</param>
        /// <returns>Scrambled data.</returns>
        public static byte[] ScrambleUDPData(byte[] original)
        {
            if (original == null)
            {
                throw new ArgumentNullException("input data can't be null.");
            }

            if (original.Length == 0)
            {
                // do nothing if the payload length is zero. this is probably a keep alive message
                return original;
            }

            int randomNumber = DataScrambleScheme.random.Next();
            int numberOfBlocks = (original.Length + DataScrambleScheme.BlockSize - 1) / DataScrambleScheme.BlockSize;
            int outputArrayLength = original.Length + JunkSize + (numberOfBlocks * ChecksumSize);
            byte[] outputArray = new byte[outputArrayLength];

            //// copy the random number to the output array, they will be eventually dropped anyway so we
            //// don't care about whether the platform is little or big endian. 
            outputArray[0] = (byte)randomNumber;
            outputArray[1] = (byte)(randomNumber >> 8);
            outputArray[2] = (byte)(randomNumber >> 0x10);
            outputArray[3] = (byte)(randomNumber >> 0x18);

            int outputOffset = 4;
            int actualBlockSize;
            for (int i = 0; i < numberOfBlocks; ++i)
            {
                if (i == numberOfBlocks - 1)
                {
                    actualBlockSize = original.Length - (DataScrambleScheme.BlockSize * (numberOfBlocks - 1));
                }
                else
                {
                    actualBlockSize = DataScrambleScheme.BlockSize;
                }

                Buffer.BlockCopy(original, i * DataScrambleScheme.BlockSize, outputArray, outputOffset, actualBlockSize);
                ushort crc16 = CRC.CRC16(original, i * DataScrambleScheme.BlockSize, actualBlockSize);

                outputOffset += actualBlockSize;
                outputArray[outputOffset++] = (byte)crc16;
                outputArray[outputOffset++] = (byte)(crc16 >> 8);
            }

            return outputArray;
        }

        /// <summary>
        /// Unscrambles the UDP data.
        /// </summary>
        /// <exception cref="System.ArgumentNullException">Thrown when scrambledData is null.</exception>
        /// <param name="scrambledData">The scrambled data in byte array.</param>
        /// <returns>Unscrambled data or null on error.</returns>
        public static byte[] UnscrambleUDPData(byte[] scrambledData)
        {
            if (scrambledData == null)
            {
                throw new ArgumentNullException("input data can't be null");
            }

            if (scrambledData.Length == 0)
            {
                // do nothing if the payload length is zero. this is probably a keep alive message
                return scrambledData;
            }

            if (scrambledData.Length < MinScrambledPayloadSize)
            {
                return null;
            }

            int numOfBlocks =
                (scrambledData.Length - JunkSize + DataScrambleScheme.BlockSize + ChecksumSize - 1) / (DataScrambleScheme.BlockSize + ChecksumSize);

            // assume the packet is well formed. 
            int totalOverhead = (numOfBlocks * ChecksumSize) + JunkSize;

            // if the length is smaller than the min possible length, then it is not well formed.  
            int minPossibleLength = totalOverhead + ((numOfBlocks - 1) * DataScrambleScheme.BlockSize) + 1;
            if (scrambledData.Length < minPossibleLength)
            {
                return null;
            }

            byte[] actualPayload = null;
            int actualBlockSize;
            int inputOffset;
            int checksumOffset;
            for (int i = 0; i < numOfBlocks; ++i)
            {
                if (i == numOfBlocks - 1)
                {
                    actualBlockSize =
                        (scrambledData.Length - JunkSize) - ((DataScrambleScheme.BlockSize + ChecksumSize) * (numOfBlocks - 1)) - ChecksumSize;
                }
                else
                {
                    actualBlockSize = DataScrambleScheme.BlockSize;
                }

                inputOffset = JunkSize + ((DataScrambleScheme.BlockSize + ChecksumSize) * i);
                checksumOffset = inputOffset + actualBlockSize;
                ushort crc16 = CRC.CRC16(scrambledData, inputOffset, actualBlockSize);

                ushort receivedChecksum = scrambledData[checksumOffset + 1];
                receivedChecksum = (ushort)(receivedChecksum << 8);
                receivedChecksum |= (byte)scrambledData[checksumOffset];

                if (receivedChecksum != crc16)
                {
                    // checksum doesn't match
                    return null;
                }
                else
                {
                    if (actualPayload == null)
                    {
                        int actualPayloadSize = scrambledData.Length - totalOverhead;
                        actualPayload = new byte[actualPayloadSize];
                    }

                    // copy the payload of the current block to the output byte array  
                    Buffer.BlockCopy(
                        scrambledData,
                        inputOffset,
                        actualPayload,
                        i * DataScrambleScheme.BlockSize,
                        actualBlockSize);
                }
            }

            return actualPayload;
        }
    }
}
