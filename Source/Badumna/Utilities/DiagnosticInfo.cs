﻿//-----------------------------------------------------------------------
// <copyright file="DiagnosticInfo.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2011 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;

namespace Badumna.Utilities
{
    /// <summary>
    /// A dictionary of diagnostic information.
    /// </summary>
    /// <remarks>
    /// This class is a collection of <c>string</c> -&gt; value pairs.
    /// As a precaution only known immutable types and other <see cref="DiagnosticInfo"/> isntances can be inserted.
    /// This prevents accidental mutation of the objects actually in use inside Badumna.
    /// </remarks>
    internal class DiagnosticInfo
    {
        /// <summary>
        /// Storage for the diagnostic information.
        /// </summary>
        private Dictionary<string, object> contents = new Dictionary<string, object>();

        /// <summary>
        /// Gets an enumeration of the keys stored in this dictionary.
        /// </summary>
        public IEnumerable<string> Keys
        {
            get
            {
                return this.contents.Keys;
            }
        }

        /// <summary>
        /// Retrieves the value associated with <paramref name="key"/>.
        /// If <paramref name="key"/> is not present or is not of type <typeparamref name="T"/> this method returns <c>default(T)</c>.
        /// If <typeparamref name="T"/> is <see cref="string"/> then a string representation of the value is returned.
        /// </summary>
        /// <typeparam name="T">The type to use for the result.</typeparam>
        /// <param name="key">The key to retrieve.</param>
        /// <returns>The value corresponding to <paramref name="key"/>, or <c>default(T)</c> if the key is not present.</returns>
        public T Get<T>(string key)
        {
            T result = default(T);
            object value;
            if (this.contents.TryGetValue(key, out value))
            {
                if (value is T)
                {
                    result = (T)value;
                }
            }

            return result;
        }

        /// <summary>
        /// Determines whether <paramref name="key"/> is present in the dictionary.
        /// </summary>
        /// <param name="key">The key to test.</param>
        /// <returns>True iff the key is present.</returns>
        public bool ContainsKey(string key)
        {
            return this.contents.ContainsKey(key);
        }

        /// <summary>
        /// Adds <paramref name="value"/> to the dictionary under <paramref name="key"/>.
        /// </summary>
        /// <remarks>
        /// Only known immutable or value types can be added to the dictionary to ensure
        /// that the original state isn't accidentally mutated by users of this class.
        /// </remarks>
        /// <typeparam name="T">The type of the value to add.</typeparam>
        /// <param name="key">The key for the value.</param>
        /// <param name="value">The value.</param>
        public void Add<T>(string key, T value) where T : struct
        {
            // TODO: The "where T : struct" constraint is not quite right -- a struct may contain a field that is not immutable / value-type.
            this.contents[key] = value;
        }

        /// <summary>
        /// Adds <paramref name="value"/> to the dictionary under <paramref name="key"/>.
        /// </summary>
        /// <remarks>
        /// Only known immutable or value types can be added to the dictionary to ensure
        /// that the original state isn't accidentally mutated by users of this class.
        /// </remarks>
        /// <param name="key">The key for the value.</param>
        /// <param name="value">The value.</param>
        public void Add(string key, string value)
        {
            this.contents[key] = value;
        }

        /// <summary>
        /// Adds <paramref name="value"/> to the dictionary under <paramref name="key"/>.
        /// </summary>
        /// <remarks>
        /// Only known immutable or value types can be added to the dictionary to ensure
        /// that the original state isn't accidentally mutated by users of this class.
        /// </remarks>
        /// <param name="key">The key for the value.</param>
        /// <param name="value">The value.</param>
        public void Add(string key, DiagnosticInfo value)
        {
            this.contents[key] = value;
        }
    }
}
