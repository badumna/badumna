﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Badumna.Utilities
{
    interface ISmoother
    {
        double EstimatedValue { get; }
        void ValueEvent(double value);
    }

    class Smoother : ISmoother
    {
        public double EstimatedValue { get { return this.mSmoothedMeanEstimator; } }
        public int EventCount { get; private set; }
        
        protected double mSmoothedMeanEstimator = 0.0;
        protected double mSmoothingFactor;
        private double mInitialValue;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="smoothingFactor">The amount of smoothing that is applied to the estimation. 
        /// Higher values will result in greater smoothing, which means the estimate will be less susceptible to 
        /// short burst but also slower to converge to the average.</param>
        /// <param name="initialValue">The starting value</param>
        public Smoother(double smoothingFactor, double initialValue)
        {
            this.mSmoothingFactor = smoothingFactor;
            this.mInitialValue = initialValue;
            this.Reset();
        }

        public virtual void ValueEvent(double value)
        {
            double delta = value - this.mSmoothedMeanEstimator;
            this.mSmoothedMeanEstimator += delta / this.mSmoothingFactor;
            this.EventCount++;
        }

        /// <summary>
        /// Returns the number of value events required to settle to within 
        /// allowedError of a steady signal, assuming the initial error is 100%.
        /// </summary>
        /// <param name="allowedError">Error allowed, as a fraction (e.g. 0.05 = 5% error)</param>
        /// <returns></returns>
        public int SettlingTime(double allowedError)
        {
            return (int)Math.Ceiling(Math.Log(allowedError) / Math.Log(1 - (1 / this.mSmoothingFactor)));
        }

        public void Reset()
        {
            this.mSmoothedMeanEstimator = this.mInitialValue;
            this.EventCount = 0;
        }
    }
}
