//-----------------------------------------------------------------------
// <copyright file="IJsonWrapper.cs" company="Scalify Pty Ltd">
//     Some portions Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

#region Header
/*
 * IJsonWrapper.cs
 *   Interface that represents a type capable of handling all kinds of JSON
 *   data. This is mainly used when mapping objects through JsonMapper, and
 *   it's implemented by JsonData.
 *
 * The authors disclaim copyright to this source code. For more details, see
 * the COPYING file included with this distribution.
 */
#endregion

using System.Collections;
using System.Collections.Specialized;

namespace Badumna.Utilities.LitJson
{
    internal enum JsonType
    {
        None,

        Object,
        Array,
        String,
        Integer,
        Double,
        Boolean
    }

    internal interface IJsonWrapper : IList, IOrderedDictionary
    {
        bool IsArray { get; }
        bool IsBoolean { get; }
        bool IsDouble { get; }
        bool IsInteger { get; }
        bool IsObject { get; }
        bool IsString { get; }

        bool GetBoolean();
        double GetDouble();
        long GetInteger();
        JsonType GetJsonType();
        string GetString();

        void SetBoolean(bool val);
        void SetDouble(double val);
        void SetInteger(long val);
        void SetJsonType(JsonType type);
        void SetString(string val);

        string ToJson();
        void ToJson(JsonWriter writer);
    }
}
