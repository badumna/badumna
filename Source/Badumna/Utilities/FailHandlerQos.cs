using System;
using System.Collections.Generic;
using System.Text;

using Badumna.Core;

namespace Badumna.Utilities
{
    class SendFailureEventArgs : EventArgs
    {
        private int mNthAttemp;
        public int NthAttempt
        {
            get { return this.mNthAttemp; }
            set { this.mNthAttemp = value; }
        }

        private PeerAddress mDestination;
        public PeerAddress Destination
        {
            get { return this.mDestination; }
            set { this.mDestination = value; }
        }

        public bool IsFinalAttempt { get { return this.mNthAttemp == -1; } }

        public SendFailureEventArgs()
        {
        }
    }


    class FailHandlerQos : QualityOfService
    {
        private EventHandler<SendFailureEventArgs> failureEventDelegate;
        public event EventHandler<SendFailureEventArgs> FailureEvent
        {
            add { this.failureEventDelegate += value; }
            remove { this.failureEventDelegate -= value; }
        }

        private SendFailureEventArgs mEventArgs;

        public FailHandlerQos(QualityOfService qos, SendFailureEventArgs eventArgs)
            : base(qos)
        {
            this.mEventArgs = eventArgs;
        }

        internal override void HandleConnectionFailure(PeerAddress destination)
        {
            this.HandleFailure(destination, -1);
        }

        internal override bool HandleFailure(PeerAddress destination, int nthAttempt)
        {
            EventHandler<SendFailureEventArgs> handler = this.failureEventDelegate;
            if (handler != null)
            {
                this.mEventArgs.Destination = destination;
                this.mEventArgs.NthAttempt = nthAttempt;
                handler.Invoke(this, this.mEventArgs);
            }

            return nthAttempt < this.NumberOfAttempts;
        }
    }

    class FailNotificationQos : QualityOfService
    {
        private LambdaFunction mCallback;

        public delegate bool FailureCallback(int nthAttempt);

        public FailNotificationQos(QualityOfService qos, LambdaFunction callback)
            : base(qos)
        {
            this.mCallback = callback;
        }

        public FailNotificationQos(QualityOfService qos, FailureCallback callback)
            : this(qos, new LambdaFunction(callback))
        {
        }

        internal override void HandleConnectionFailure(PeerAddress destination)
        {
            this.HandleFailure(destination, -1);
        }

        internal override bool HandleFailure(PeerAddress destination, int nthAttempt)
        {
            return (bool)this.mCallback.Invoke(nthAttempt);
        }
    }
}
