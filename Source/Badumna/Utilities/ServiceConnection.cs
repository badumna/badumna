﻿//---------------------------------------------------------------------------------
// <copyright file="ServiceConnection.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------
using System;
using Badumna.Core;
using Badumna.Utilities;

namespace Badumna
{
    /// <summary>
    /// The types of connection timeout.
    /// </summary>
    public enum ServiceConnectionResultType
    {
        /// <summary>
        /// Successfully connected without error. 
        /// </summary>
        Success,

        /// <summary>
        /// Failed to connect.
        /// </summary>
        ConnectionTimeout,

        /// <summary>
        /// Failed to locate any service host.
        /// </summary>
        ServiceNotAvailable,
    }

    /// <summary>
    /// The helper class used to monitor the connection timeout event when trying to connect
    /// to badumna servers such as Arbitration and Overload servers. 
    /// </summary>
    internal class ServiceConnection
    {
        /// <summary>
        /// How long to wait before timing out a connection attempt.
        /// </summary>
        private readonly TimeSpan connectionTimeoutPeriod;

        /// <summary>
        /// How long to wait before triggering the serviceNotAvailableTimeoutEvent.
        /// </summary>
        private readonly TimeSpan serviceNotAvailableTimeoutPeriod;

        /// <summary>
        /// Whether is trying to connect.
        /// </summary>
        private bool isConnecting;

        /// <summary>
        /// Whether the connection has been established. 
        /// </summary>
        private bool isConnected;

        /// <summary>
        /// The network event used to handle connection timeout. 
        /// </summary>
        private NetworkEvent connectionTimeoutEvent;

        /// <summary>
        /// The network event used to handle the situation where the module can't locate any valid service host. 
        /// </summary>
        private NetworkEvent serviceNotAvailableTimeoutEvent;

        /// <summary>
        /// Whether connection has at least been attempted.
        /// </summary>
        private bool connectionAttempted;

        /// <summary>
        /// The connection error type.
        /// </summary>
        private ServiceConnectionResultType latestConnectionErrorType;

        /// <summary>
        /// The queue to use for scheduling events.
        /// </summary>
        private NetworkEventQueue eventQueue;

        /// <summary>
        /// Initializes a new instance of the <see cref="ServiceConnection"/> class.
        /// </summary>
        /// <param name="connectionTimeoutPeriod">How long to wait before timing out a connection attempt.</param>
        /// <param name="serviceNotAvailableTimeoutPeriod">The service not available timeout period.</param>
        /// <param name="eventQueue">The queue to use for scheduling events.</param>
        public ServiceConnection(
            TimeSpan connectionTimeoutPeriod,
            TimeSpan serviceNotAvailableTimeoutPeriod,
            NetworkEventQueue eventQueue)
        {
            if (eventQueue == null)
            {
                throw new ArgumentNullException("eventQueue");
            }

            this.eventQueue = eventQueue;

            this.connectionTimeoutPeriod = connectionTimeoutPeriod;
            this.serviceNotAvailableTimeoutPeriod = serviceNotAvailableTimeoutPeriod;
            this.isConnecting = false;
            this.isConnected = false;
            this.connectionAttempted = false;
            this.latestConnectionErrorType = ServiceConnectionResultType.Success;

            // schedule the service not available timeout event
            this.serviceNotAvailableTimeoutEvent = this.eventQueue.Schedule(this.serviceNotAvailableTimeoutPeriod.TotalMilliseconds, this.OnConnectionTimeout);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ServiceConnection"/> class.
        /// </summary>
        /// <param name="eventQueue">The queue to use for scheduling events.</param>
        public ServiceConnection(NetworkEventQueue eventQueue) :
            this(Parameters.ArbitrationConnectionTimeoutTime, Parameters.ServiceNotAvailableTimeoutTime, eventQueue)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ServiceConnection"/> class.
        /// </summary>
        /// <param name="connectionTimeoutPeriod">The connection timeout period.</param>
        /// <param name="eventQueue">The queue to use for scheduling events.</param>
        public ServiceConnection(TimeSpan connectionTimeoutPeriod, NetworkEventQueue eventQueue) :
            this(connectionTimeoutPeriod, Parameters.ServiceNotAvailableTimeoutTime, eventQueue)
        {
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is connected.
        /// </summary>
        /// <value>
        ///     <c>true</c> if this instance is connected; otherwise, <c>false</c>.
        /// </value>
        protected bool IsConnected
        {
            get { return this.isConnected; }
            set { this.isConnected = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is connecting.
        /// </summary>
        /// <value>
        ///     <c>true</c> if this instance is connecting; otherwise, <c>false</c>.
        /// </value>
        protected bool IsConnecting
        {
            get { return this.isConnecting; }
            set { this.isConnecting = value; }
        }

        /// <summary>
        /// Gets the type of the connection result.
        /// </summary>
        /// <value>The type of the connection result.</value>
        protected ServiceConnectionResultType ConnectionResultType
        {
            get { return this.latestConnectionErrorType; }
        }

        /// <summary>
        /// Schedules the connection timeout event.
        /// </summary>
        protected void ScheduleConnectionTimeoutEvent()
        {
            if (this.connectionTimeoutEvent != null)
            {
                this.eventQueue.Remove(this.connectionTimeoutEvent);
            }

            // when ScheduleConnectionTimeoutEvent is called, it means at least the current service host is known to the client,
            // so remove this not available timeout event. 
            if (this.serviceNotAvailableTimeoutEvent != null)
            {
                this.eventQueue.Remove(this.serviceNotAvailableTimeoutEvent);
                this.serviceNotAvailableTimeoutEvent = null;
            }

            this.connectionAttempted = true;
            this.connectionTimeoutEvent = this.eventQueue.Schedule(this.connectionTimeoutPeriod.TotalMilliseconds, this.OnConnectionTimeout);
        }

        /// <summary>
        /// Cancels the timeout event.
        /// </summary>
        protected void CancelConnectionTimeoutEvent()
        {
            if (this.connectionTimeoutEvent != null)
            {
                this.eventQueue.Remove(this.connectionTimeoutEvent);
                this.connectionTimeoutEvent = null;
            }
        }

        /// <summary>
        /// Determines whether this instance is available.
        /// </summary>
        /// <returns>
        ///     <c>true</c> if this instance is available; otherwise, <c>false</c>.
        /// </returns>
        protected bool IsAvailable()
        {
            if (this.isConnecting)
            {
                Logger.TraceInformation(LogTag.Utilities, "Trying to connect, considered as available.");
                return true;
            }
            else
            {
                Logger.TraceInformation(LogTag.Utilities, "IsConnected? {0}", this.isConnected);
                return this.isConnected;
            }
        }

        /// <summary>
        /// The handler method called when fail to connect. 
        /// </summary>
        protected virtual void ConnectionTimeoutHandler()
        {
        }

        /// <summary>
        /// Called when connection timeout.
        /// </summary>
        private void OnConnectionTimeout()
        {
            this.isConnecting = false;
            this.isConnected = false;

            if (this.connectionAttempted)
            {
                this.latestConnectionErrorType = ServiceConnectionResultType.ConnectionTimeout;
            }
            else
            {
                this.latestConnectionErrorType = ServiceConnectionResultType.ServiceNotAvailable;
            }

            this.ConnectionTimeoutHandler();
        }
    }
}