﻿using System;
using System.Collections.Generic;
using System.Text;

using Badumna.Core;

namespace Badumna.Utilities
{
    class OneShotTask
    {
        public bool IsPending { get { return this.mEvent != null; } }

        private NetworkEvent mEvent;
        private IInvokable mInvokable;

        /// <summary>
        /// The queue to use for scheduling events.
        /// </summary>
        private NetworkEventQueue eventQueue;

        public OneShotTask(NetworkEventQueue eventQueue)
        {
            this.eventQueue = eventQueue;
        }

        public void Trigger(TimeSpan timeSpan, IInvokable invokable)
        {
            this.Cancel();  // Cancel any pending event
            this.mInvokable = invokable;
            this.mEvent = this.eventQueue.Schedule((int)timeSpan.TotalMilliseconds, this.Execute);
        }

        public void TriggerNow(IInvokable invokable)
        {
            this.Trigger(TimeSpan.FromMilliseconds(0.0), invokable);
        }

        private void Execute()
        {
            this.mEvent = null;
            this.mInvokable.Invoke();
            // Don't clear mInvokable here because the task just invoked may have Triggered a new task
        }

        public void Cancel()
        {
            if (this.mEvent != null)
            {
                this.eventQueue.Remove(this.mEvent);
                this.mEvent = null;
            }
        }

        public override string ToString()
        {
            return this.mInvokable != null ? this.mInvokable.ToString() : "(none)";
        }
    }
}
