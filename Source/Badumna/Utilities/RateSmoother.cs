//-----------------------------------------------------------------------
// <copyright file="RateSmoother.cs" company="National ICT Australia Ltd">
//     Copyright (c) 2010 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.Utilities
{
    using System;
    using Badumna.Core;

    /// <summary>
    /// Maintains an estimate of average units / second for a single quantity.
    /// </summary>
    internal class RateSmoother
    {
        /// <summary>
        /// The duration of the sample interval.
        /// </summary>
        private TimeSpan sampleInterval;
        
        /// <summary>
        /// The factor controlling the rate at which a new estimate is converged on.
        /// </summary>
        private double mixingFactor;

        /// <summary>
        /// The current estimated rate.
        /// </summary>
        private double estimate;

        /// <summary>
        /// The cummulative measure of the quanity during the current sample period.
        /// </summary>
        private double valueCarried;

        /// <summary>
        /// The time the current sample interval started.
        /// </summary>
        private TimeSpan intervalStart;

        /// <summary>
        /// Provides access to the current time.
        /// </summary>
        private ITime timeKeeper;

        /// <summary>
        /// Initializes a new instance of the RateSmoother class.
        /// </summary>
        /// <param name="sampleInterval">Length of sample interval.  This should be long enough to avoid a
        /// high rate of samples with a value of 0 (which would result in the estimate decaying back to 0).</param>
        /// <param name="timeToConverge">After a step change in input rate, the estimation will converge to 99% of
        /// the correct value within this timespan.  Too low and disturbances will overwhelm the estimate, too high
        /// and the estimate will get stale.  This must be at least twice sampleInterval.</param>
        /// <param name="timeKeeper">The time source.</param>
        public RateSmoother(TimeSpan sampleInterval, TimeSpan timeToConverge, ITime timeKeeper)
        {
            this.timeKeeper = timeKeeper;

            this.sampleInterval = sampleInterval;

            double samplesToConverge = Math.Ceiling(timeToConverge.TotalSeconds / this.sampleInterval.TotalSeconds);
            if (samplesToConverge < 1.5)
            {
                // If we're converging in one sample then we're not doing any filtering...
                throw new ArgumentOutOfRangeException("timeToConverge must be at least twice sampleInterval");
            }
            
            // Our filter is:
            //    x_1 = m * x_0 + (1 - m) * s_1
            // where:
            //   x_i - ith estimate
            //   m   - mixing rate (0 < m < 1)
            //   s_i - ith sample
            //
            // If the input goes through a step change of s at t = 0 and remains constant then
            //   x_i = s - m^i * s
            //
            // Setting x_n = 0.99 * s gives
            //   0.99 * s = s - m^n * s
            //   m^n = 0.01  (s != 0)
            this.mixingFactor = Math.Pow(0.01, 1.0 / samplesToConverge);

            this.Reset();
        }

        /// <summary>
        /// Gets the current estimated rate.
        /// </summary>
        public double EstimatedRate
        {
            get
            {
                this.UpdateEstimate();
                return this.estimate / this.sampleInterval.TotalSeconds;
            }
        }

        /// <summary>
        /// Resets the rate smoother.
        /// </summary>
        public void Reset()
        {
            this.estimate = 0.0;
            this.valueCarried = 0.0;
            this.intervalStart = this.timeKeeper.Now;
        }
        
        /// <summary>
        /// Record a new amount of the quantity being measured.
        /// </summary>
        /// <param name="value">New data of the quantity.</param>
        public void ValueEvent(double value)
        {
            this.UpdateEstimate();
            this.valueCarried += value;
        }

        /// <summary>
        /// Updates the estimate with data from the latest sample period which has just ended.
        /// </summary>
        private void ApplyNextSample()
        {
            this.estimate = this.valueCarried + (this.mixingFactor * (this.estimate - this.valueCarried));
            this.valueCarried = 0.0;
        }

        /// <summary>
        /// Check to see if the sample period has ended, and if so, update the estimate.
        /// </summary>
        private void UpdateEstimate()
        {
            TimeSpan elapsedTime = this.timeKeeper.Now - this.intervalStart;

            // TODO: Calculate directly rather than by iterating.  Probably doesn't matter because the sample rate should be set
            //       so that ValueEvent() gets called roughly at least once per sample period anyway.
            while (elapsedTime >= this.sampleInterval)
            {
                this.ApplyNextSample();
                elapsedTime -= this.sampleInterval;
                this.intervalStart += this.sampleInterval;
            }
        }
    }
}
