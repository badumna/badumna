﻿//------------------------------------------------------------------------------
// <copyright file="QualifiedName.cs" company="National ICT Australia Limited">
//     Copyright (c) 2012 National ICT Australia Limited. All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

namespace Badumna.Utilities
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Badumna.Core;

    /// <summary>
    /// A two-part name; immutable.
    /// </summary>
    internal struct QualifiedName : IEquatable<QualifiedName>, IParseable
    {
        /// <summary>
        /// The string that separates the components of the name when representing the qualified name as a single string.
        /// </summary>
        public const string Separator = "|";

        /// <summary>
        /// The qualifier (the more general part of the qualified name).
        /// </summary>
        /// <remarks>
        /// Code should use the Qualifier getter to ensure that null and empty qualifiers are treated as equivalents.
        /// </remarks>
        private string qualifier;

        /// <summary>
        /// The name (the more specific part of the qualified name).
        /// </summary>
        /// <remarks>
        /// Code should use the Name getter to ensure that null and empty names are treated as equivalents.
        /// </remarks>
        private string name;

        /// <summary>
        /// Initializes a new instance of the QualifiedName struct.
        /// </summary>
        /// <param name="qualifier">The qualifier (more general part of the full name)</param>
        /// <param name="name">The name (more specific part of the full name)</param>
        public QualifiedName(string qualifier, string name)
        {
            // Make sure qualifier doesn't contain the separator, because if it does we won't be able to parse it back out
            // after we write the name as a single string.
            if (qualifier != null && qualifier.Contains(QualifiedName.Separator))
            {
                throw new ArgumentException("Qualifier can not contain the string '" + QualifiedName.Separator + "'", "qualifier");
            }

            this.qualifier = qualifier;
            this.name = name;
        }

        /// <summary>
        /// Initializes a new instance of the QualifiedName struct from a string in the format "{qualifier}{separator}{name}".
        /// </summary>
        /// <param name="qualifiedName">The qualified name</param>
        public QualifiedName(string qualifiedName)
        {
            var nameParts = qualifiedName.Split(new string[] { QualifiedName.Separator }, 2, StringSplitOptions.None);
            if (nameParts.Length != 2)
            {
                throw new ArgumentException("Qualified name does not contain a separator", "qualifiedName");
            }

            this.qualifier = nameParts[0];
            this.name = nameParts[1];
        }

        /// <summary>
        /// Gets the qualifier (the more general part of the qualified name).
        /// </summary>
        public string Qualifier
        {
            get { return this.qualifier ?? ""; }
        }

        /// <summary>
        /// Gets the name (the more specific part of the qualified name).
        /// </summary>
        public string Name
        {
            get { return this.name ?? ""; }
        }

        /// <inheritdoc />
        public static bool operator ==(QualifiedName left, QualifiedName right)
        {
            return left.Equals(right);
        }

        /// <inheritdoc />
        public static bool operator !=(QualifiedName left, QualifiedName right)
        {
            return !(left == right);
        }

        /// <inheritdoc />
        public override bool Equals(object obj)
        {
            return obj is QualifiedName && this.Equals((QualifiedName)obj);
        }

        /// <inheritdoc />
        public bool Equals(QualifiedName other)
        {
            return this.Qualifier == other.Qualifier && this.Name == other.Name;
        }

        /// <inheritdoc />
        public override int GetHashCode()
        {
            return this.Name.GetHashCode();
        }

        /// <inheritdoc />
        public override string ToString()
        {
            return this.Qualifier + QualifiedName.Separator + this.Name;
        }

        /// <inheritdoc />
        public void ToMessage(MessageBuffer message, Type parameterType)
        {
            message.Write(this.ToString());
        }

        /// <inheritdoc />
        public void FromMessage(MessageBuffer message)
        {
            // Ideally we'd do:
            //    this = new QualifiedName(message.ReadString());
            // but jsc don't like it.
            var temp = new QualifiedName(message.ReadString());
            this.qualifier = temp.Qualifier;
            this.name = temp.Name;
        }
    }
}
