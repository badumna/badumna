using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Badumna.Utilities
{
    static class Utils
    {
        /// <summary>
        /// Returns the minimum number of bytes required to store a given number of bits
        /// </summary>
        /// <param name="bits"></param>
        /// <returns></returns>
        public static int BitsToBytes(int bits)
        {
            return (bits + 7) / 8;
        }
        
        /// <summary>
        /// Returns a string representation of a method call.
        /// </summary>
        /// <param name="function">The method</param>
        /// <param name="args">The parameters</param>
        /// <returns>The string representation</returns>
        public static string DescribeCall(Delegate function, params object[] args)
        {
            var call = new StringBuilder();

            if (function.Target != null)
            {
                call.Append(function.Target.GetType().Name).Append(".");
            }

            call.Append(function.Method.Name).Append("(");

            for (int i = 0; i < args.Length; i++)
            {
                if (i != 0)
                {
                    call.Append(", ");
                }

                Utils.ObjectToString(args[i], call);
            }

            call.Append(")");

            return call.ToString();
        }

        public static string ObjectToString(Object o)
        {
            var builder = new StringBuilder();
            ObjectToString(o, builder);
            return builder.ToString();
        }

        public static void ObjectToString(Object o, StringBuilder result)
        {
            if (o == null)
            {
                result.Append("<null>");
            }
            else if (o is String)
            {
                result.Append("\"").Append(o).Append("\"");
            }
            else if (o is IEnumerable)
            {
                result.Append("[");
                bool first = true;
                foreach (Object element in (IEnumerable)o)
                {
                    if (first)
                    {
                        first = false;
                    }
                    else
                    {
                        result.Append(", ");
                    }

                    Utils.ObjectToString(element, result);
                }
                result.Append("]");
            }
            else
            {
                result.Append(o.ToString());
            }
        }

        public static int[] GetRandomSet(int length, int expected)
        {
            if (length < expected)
            {
                throw new InvalidOperationException("length must be greater than expected.");
            }

            if (expected == 0)
            {
                throw new InvalidOperationException("expected value can't be zero.");
            }

            int[] set = new int[expected];
            int interval = length / expected;

            for (int i = 0; i < expected; i++)
            {
                int random;
                if (i != expected - 1)
                {
                    random = RandomSource.Generator.Next(interval);
                }
                else
                {
                    int remaining = length - interval * (expected - 1);
                    random = RandomSource.Generator.Next(remaining);
                }
                set[i] = random + i * interval;
            }

            return set;
        }
    }
}
