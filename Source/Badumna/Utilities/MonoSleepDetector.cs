﻿//---------------------------------------------------------------------------------
// <copyright file="MonoSleepDetector.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

using System;
using Badumna.Core;

namespace Badumna
{
    /// <summary>
    /// The sleep wakeup handler.
    /// </summary>
    internal delegate void WakeUpFromSleepHandler();

    /// <summary>
    /// A detector to monitor wake up from sleep event.
    /// </summary>
    internal class MonoSleepDetector
    {
        /// <summary>
        /// The network event for checking sleep.
        /// </summary>
        private NetworkEvent networkEvent;

        /// <summary>
        /// The wake up from sleep event handler.
        /// </summary>
        private WakeUpFromSleepHandler sleepEventHandler;

        /// <summary>
        /// When last time checked.
        /// </summary>
        private DateTime lastObservedDateTime;

        /// <summary>
        /// The queue to use for scheduling events.
        /// </summary>
        private NetworkEventQueue eventQueue;

        /// <summary>
        /// Initializes a new instance of the <see cref="MonoSleepDetector"/> class.
        /// </summary>
        /// <param name="handler">The event handler.</param>
        /// <param name="eventQueue">The queue to use for scheduling events.</param>
        public MonoSleepDetector(WakeUpFromSleepHandler handler, NetworkEventQueue eventQueue)
        {
            this.eventQueue = eventQueue;
            this.sleepEventHandler = handler;
            this.lastObservedDateTime = DateTime.UtcNow;
        }

        /// <summary>
        /// Starts this instance.
        /// </summary>
        public void Start()
        {
            this.networkEvent = this.eventQueue.Schedule(1000, this.SleepDetectionTask);
        }

        /// <summary>
        /// Stops this instance.
        /// </summary>
        public void Stop()
        {
            if (this.networkEvent != null)
            {
                this.eventQueue.Remove(this.networkEvent);
            }
        }

        /// <summary>
        /// Detect the resume from sleep event by look at the clock. On mono, we will not be notified for resuming from
        /// sleep, this is a workaround for that. 
        /// </summary>
        private void SleepDetectionTask()
        {
            DateTime now = DateTime.UtcNow;

            if (now - this.lastObservedDateTime > TimeSpan.FromSeconds(5))
            {
                this.HandleSystemResume();
            }

            this.lastObservedDateTime = now;
            this.networkEvent = this.eventQueue.Schedule(1000, this.SleepDetectionTask);
        }

        /// <summary>
        /// Handles the system resume event.
        /// </summary>
        private void HandleSystemResume()
        {
            if (this.sleepEventHandler != null)
            {
                this.sleepEventHandler();
            }
        }
    }
}