﻿//-----------------------------------------------------------------------
// <copyright file="PrintableList.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2011 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;

namespace Badumna.Utilities
{
    /// <summary>
    /// A List[T] that overrides ToString() to produce a comma separated list.
    /// Primarily intended to simplify the implementation of the C++ wrapper.
    /// </summary>
    /// <typeparam name="T">The type of the contained objects</typeparam>
    internal class PrintableList<T> : List<T>
    {
        /// <inheritdoc />
        public override string ToString()
        {
            return string.Join(",", this.ConvertAll(x => x.ToString()).ToArray());
        }
    }
}
