using System;
using System.Collections.Generic;
using System.Text;

using Badumna.Core;

namespace Badumna.Utilities
{
#if DEBUG
    public  // required for ExperimentLibrary
#endif
    interface IExpireable
    {
        TimeSpan ExpirationTime { get; set; }           
    }

#if DEBUG
    public // Required for ExperimentLibrary
#endif
    class ExpireEventArgs : EventArgs
    {
        private object mExpiredObject;
        public object ExpiredObject { get { return this.mExpiredObject; } }

        public ExpireEventArgs(object expiredObject)
            : base()
        {
            this.mExpiredObject = expiredObject;
        }
    }

    class Expireable<T> : IExpireable
    {
        private TimeSpan mExpirationTime;
        public TimeSpan ExpirationTime
        {
            get { return this.mExpirationTime; }
            set { this.mExpirationTime = value; }
        }

        private T mObject;
        public T Object { get { return this.mObject; } }

        public Expireable(T obj)
        {
            this.mObject = obj;
        }
    }

    class GarbageCollector
    {
        private EventHandler<ExpireEventArgs> expirationEventDelegate;
        public event EventHandler<ExpireEventArgs> ExpirationEvent
        {
            add { this.expirationEventDelegate += value; }
            remove { this.expirationEventDelegate -= value; }
        }

        private List<IExpireable> mExpirables = new List<IExpireable>();

        private RegularTask mTask;

        /// <summary>
        /// Provides access to the current time.
        /// </summary>
        private ITime timeKeeper;

        public GarbageCollector(string name, double checkDelaySeconds, NetworkEventQueue eventQueue, ITime timeKeeper, INetworkConnectivityReporter connectivityReporter)
        {
            this.timeKeeper = timeKeeper;
            this.mTask = new RegularTask(
                String.Format("Garbage collection ({0})", name), 
                TimeSpan.FromSeconds(checkDelaySeconds),
                eventQueue,
                connectivityReporter,
                this.Collect);
            this.mTask.Start();
        }

        public void Shutdown()
        {
            this.mTask.Stop();
        }

        public void ExpireObjectSecondsFromNow(IExpireable expirable, int secondsTillExpiration)
        {
            if (!this.mTask.IsRunning)
            {
                throw new InvalidOperationException("Garbage collector has been shutdown");
            }

            expirable.ExpirationTime = this.timeKeeper.Now + TimeSpan.FromSeconds(secondsTillExpiration);

            if (!this.mExpirables.Contains(expirable))
            {
                this.mExpirables.Add(expirable);
            }

            this.mExpirables.Sort(GarbageCollector.CompareExpireables);
        }

        public void Remove(IExpireable expireable)
        {
            this.mExpirables.Remove(expireable);
        }

        private void Collect()
        {
            TimeSpan now = this.timeKeeper.Now;

            while (this.mExpirables.Count > 0 && this.mExpirables[0].ExpirationTime < now)
            {
                IExpireable expired = this.mExpirables[0];

                EventHandler<ExpireEventArgs> handler = this.expirationEventDelegate;
                if (handler != null)
                {
                    handler.Invoke(this, new ExpireEventArgs(expired));                    
                }

                this.mExpirables.Remove(expired);
            }
        }

        private static int CompareExpireables(IExpireable a, IExpireable b)
        {
            return a.ExpirationTime.CompareTo(b.ExpirationTime);
        }
    }
}
