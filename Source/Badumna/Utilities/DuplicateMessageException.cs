using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;

using Badumna.Core;

using SequenceNumber = Badumna.Utilities.CyclicalID.UShortID;

namespace Badumna.Utilities
{
    /*
    internal class DuplicateMessageException : CoreException
    {
        private PeerAddress mMessageSource;
        public PeerAddress MessageSource
        {
            get { return this.mMessageSource; }
        }

        private BaseEnvelope mMessageEnvelope;
        public BaseEnvelope MessageEnvelope
        {
            get { return this.mMessageEnvelope; }
        }

        private SequenceNumber mSequenceNumber;
        public SequenceNumber SequenceNumber { get { return this.mSequenceNumber; } }

        public DuplicateMessageException(PeerAddress messageSource, BaseEnvelope messageEnvelope, SequenceNumber sequenceNumber)
            : base(String.Format("Message {0} from {1} is a duplicate.", sequenceNumber, messageSource))
        {            
            this.mMessageSource = messageSource;
            this.mMessageEnvelope = messageEnvelope;
            this.mSequenceNumber = sequenceNumber;
        }

        internal override void TraceMessage()
        {
            Logger.TraceException(System.Diagnostics.TraceEventType.Verbose, this, "{0}", this.Message);
        }
    }
     */

}
