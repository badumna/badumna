using System;
using System.Collections.Generic;
using System.Text;

namespace Badumna.Utilities
{
    internal class RandomSource
    {
        private static Random mGenerator = new Random(GetSeed());
        public static Random Generator
        {
            get { return RandomSource.mGenerator; }
        }

        public static void Initialize()
        {
            RandomSource.mGenerator = new Random();
        }

        public static void Initialize(int seed)
        {
            RandomSource.mGenerator = new Random(seed);
        }

        public static uint NextUInt()
        {
            byte[] buffer = new byte[4];
            RandomSource.Generator.NextBytes(buffer);
            return BitConverter.ToUInt32(buffer, 0);
        }

        public static int GetSeed()
        {
#if IOS || ANDROID
            return (int)DateTime.UtcNow.TimeOfDay.TotalMilliseconds;
#else
            int seed = 0;

            try
            {
                DateTime currentDateTime = DateTime.UtcNow;
                seed = (int)currentDateTime.TimeOfDay.TotalMilliseconds;
                int pid = System.Diagnostics.Process.GetCurrentProcess().Id;

                return seed ^ (pid + (pid << 15));
            }
            catch
            {
                // pid will be inaccessible in unity 2.6's webplayer mode
                return (int)DateTime.UtcNow.TimeOfDay.TotalMilliseconds;
            }
#endif
        }
    }
}
