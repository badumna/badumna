using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;

using Badumna.Core;
using Badumna.InterestManagement;
using Badumna.Utilities;

namespace Badumna.Utilities
{
    class Simulator : NetworkEventQueue
    {
        public delegate void TraceMessage(TraceEventType level, string message);
        public delegate void TraceException(TraceEventType level, Exception exception, string message);
        public delegate void TraceNetworkEvent(string peerAddress, TimeSpan currentTime, string label);

        public Simulator()
        {
            this.mAllowsDiscardable = false; // Don't discard any messages in simulator.
        }


        private TimeSpan mTime = new TimeSpan();
        public TimeSpan Time { get { return this.mTime; } }

        public event EventHandler HasBeenReset;

        private bool mSuspendFlag;
        public bool Suspend 
        { 
            set { this.mSuspendFlag = value; }
            get { return this.mSuspendFlag; }
        }

        public event TraceNetworkEvent NewNetworkEvent;

        // Amount after the scheduled time to actually execute the events (to simulate delays
        // in the NetworkEventQueue loop)
        private TimeSpan mExecutionDelay = TimeSpan.Zero;
        public TimeSpan ExecutionDelay
        {
            get { return this.mExecutionDelay; }
            set { this.mExecutionDelay = value; }
        }

        public override void Reset()
        {
            base.Reset();

            this.Suspend = false;
            this.mTime = new TimeSpan();

            EventHandler resetHandler = this.HasBeenReset;
            if (resetHandler != null)
            {
                resetHandler(this, null);
            }
        }

        public void MaybeRunOne()
        {
            if (this.Count > 0)
            {
                this.RunOne();
            }
        }

        public void RunOne()
        {
            if (this.Count == 0)
            {
                throw new InvalidOperationException("No events to run");
            }
            this.mTime = this.TimeOfNextEvent();
            NetworkEvent nextEvent = this.Pop();

            this.AcquirePerformLock();
            try
            {
                nextEvent.PerformAction();

                // Trace after execution of the event because we need to know the NetworkContext it used
                TraceNetworkEvent handler = this.NewNetworkEvent;
                if (handler != null && nextEvent.IsActive)
                {
                    // peer address was currently determined from NetworkContext.CurrentContext, but that has now been removed...
                    handler("<unknown>", this.mTime, nextEvent.Name);
                }
            }
            finally
            {
                this.ReleasePerformLock();
            }
        }

        public void RunFor(TimeSpan duration)
        {
            TimeSpan endTime = this.Time + duration;

            // Weird flow control below so that JSC can translate it...
            var done = false;
            do
            {
                NetworkEvent ev = this.NextEvent(false);
                done = ev == null || ev.Time > endTime;

                if (!done)
                {
                    if (this.mSuspendFlag)
                    {
                        System.Threading.Thread.Sleep(250);
                    }
                    else
                    {
                        this.RunOne();
                    }
                }
            }
            while (!done);

            this.mTime = endTime;
        }

        /// <summary>
        /// Invokes <see cref="RunUntil(GenericCallBackReturn{bool}, TimeSpan, GenericCallBack)"/> with no
        /// <c>failureAction</c>.
        /// </summary>
        /// <param name="condition">Termination condition</param>
        /// <param name="allowedTime">Allowed time</param>
        /// <returns>Simulated time elapsed during this method call</returns>
        public TimeSpan RunUntil(GenericCallBackReturn<bool> condition, TimeSpan allowedTime)
        {
            return this.RunUntil(condition, allowedTime, null);
        }

        /// <summary>
        /// Runs the simulator until either condition is true or allowedTime has elapsed.
        /// The return value indicates the amount of time elapsed.
        /// </summary>
        /// <remarks>
        /// If condition doesn't become true then the simulated time on exit may be much greater
        /// than the time at start + allowedTime, depending on how soon the first event occurs
        /// after the allowedTime.
        /// </remarks>
        /// <param name="condition">Termination condition</param>
        /// <param name="allowedTime">Allowed time</param>
        /// <param name="failureAction">Action to execute if condition doesn't become true within allowedTime</param>
        /// <returns>Simulated time elapsed during this method call</returns>
        public TimeSpan RunUntil(GenericCallBackReturn<bool> condition, TimeSpan allowedTime, GenericCallBack failureAction)
        {
            TimeSpan start = this.Now;
            while (true)
            {
                TimeSpan elapsed = this.Now - start;

                if (elapsed > allowedTime)
                {
                    if (failureAction != null)
                    {
                        failureAction();
                    }

                    return elapsed;
                }

                if (condition())
                {
                    return elapsed;
                }

                this.RunOne();
            }
        }

        public override void Run(Fiber fiber)
        {
            throw new NotSupportedException("Simulator does not support calls to Run() because the base implementation relies on real time elapsing");
        }

        protected override void PushHeap(NetworkEvent ev)
        {
            ev.Time += this.mExecutionDelay;
            base.PushHeap(ev);
        }

        protected override TimeSpan GetTimeMillisecondsFromNow(double millisecondsFromNow)
        {
            return this.Time + TimeSpan.FromMilliseconds(millisecondsFromNow);
        }

        // Make Push accessible to clients of the Simulator class
        public void Schedule<T>(TimeSpan timeFromNow, Action<T> action, T arg)
        {
            this.PushInvokable((int)timeFromNow.TotalMilliseconds, false, Apply.Func(delegate { action(arg); }));
        }

        public String DescribeQueuedEvents()
        {
            return "[" + CollectionUtils.Join(this.GetQueue(), ", ") + "]";
        }
    }
}

