﻿//-----------------------------------------------------------------------
// <copyright file="Fiber.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2011 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace Badumna.Utilities
{
    using System;
    using System.Threading;

    /// <summary>
    /// Fiber class is used as thread helper, this mainly used for as thread replacement for 
    /// NetworkEventQueue thread. Flash translation will required this helper class, as Flash
    /// is not multithreading.
    /// </summary>
    internal class Fiber
    {
        /// <summary>
        /// The fiber name.
        /// </summary>
        private string name;

        /// <summary>
        /// A value indicating whether this should run in the background.
        /// </summary>
        private bool isBackground;

        /// <summary>
        /// A value indicating whether the fiber is complete.
        /// </summary>
        private bool isComplete;

        /// <summary>
        /// The fiber wait handle.
        /// </summary>
        private WaitHandle isWatingOn;

        /// <summary>
        /// The previous fiber wait handle.
        /// </summary>
        private WaitHandle prevWaitHandle;

        /// <summary>
        /// When fiber is waiting on something the timeout field need to be set as well.
        /// </summary>
        private int waitTimeout;

        /// <summary>
        /// The fiber body (delegate method).
        /// </summary>
        private Action<Fiber> body;

        /// <summary>
        /// A thread where the fiber body will run on.
        /// </summary>
        private Thread fiberThread;

        /// <summary>
        /// Start wait time.
        /// </summary>
        private int startWaitTime;

        /// <summary>
        /// Initializes a new instance of the <see cref="Fiber"/> class.
        /// </summary>
        /// <param name="body">Delegate method.</param>
        public Fiber(Action<Fiber> body)
        {
            this.body = body;
        }

        /// <summary>
        /// Gets or sets the fiber name.
        /// </summary>
        public string Name
        {
            get { return this.name; }
            set { this.name = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this should run in the background.
        /// </summary>
        public bool IsBackground
        {
            get { return this.isBackground; }
            set { this.isBackground = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the fiber is complete.
        /// </summary>
        public bool IsComplete
        {
            get { return this.isComplete; }
            set { this.isComplete = value; }
        }

        /// <summary>
        /// Gets or sets fiber wait handle.
        /// </summary>
        public WaitHandle IsWaitingOn
        {
            get { return this.isWatingOn; }
            set { this.isWatingOn = value; }
        }

        /// <summary>
        /// Gets or sets the previous fiber wait handle.
        /// </summary>
        public WaitHandle PrevWaitHandle
        {
            get { return this.prevWaitHandle; }
            set { this.prevWaitHandle = value; }
        }

        /// <summary>
        /// Gets the fiber wait timeout value.
        /// </summary>
        public int WaitTimeout
        {
            get { return this.waitTimeout; }
        }

        /// <summary>
        /// Gets the fiber delegate method.
        /// </summary>
        public Action<Fiber> Body
        {
            get { return this.body; }
        }

        /// <summary>
        /// Gets or sets the fiber thread.
        /// </summary>
        public Thread FiberThread
        {
            get { return this.fiberThread; }
            set { this.fiberThread = value; }
        }

        /// <summary>
        /// Gets or sets the start wait time.
        /// </summary>
        public int StartWaitTime
        {
            get { return this.startWaitTime; }
            set { this.startWaitTime = value; }
        }

        /// <summary>
        /// Set the fiber wait handle and timeout.
        /// </summary>
        /// <param name="waitHandle">Wait handle.</param>
        /// <param name="timeout">Wait timeout.</param>
        public void WaitFor(WaitHandle waitHandle, int timeout)
        {
            this.isWatingOn = waitHandle;
            this.waitTimeout = timeout;
        }

        /// <summary>
        /// Blocks the calling thread until a thread terminates or the specified time
        /// elapses (wrapping the thread join method).
        /// </summary>
        /// <param name="millisecondsTimeout">The number of milliseconds to wait for the thread to terminate.</param>
        /// <returns>True if the thread has terminated; False if the thread has not terminated
        /// after the amount of time specified by the millisecondsTimeout parameter has
        /// elapsed.</returns>
        public bool Join(int millisecondsTimeout)
        {
            return this.fiberThread.Join(millisecondsTimeout);
        }

        /// <summary>
        /// Raises a System.Threading.ThreadAbortException in the thread on which it
        /// is invoked, to begin the process of terminating the thread. Calling this
        /// method usually terminates the thread (wrapping the thread join method).
        /// </summary>
        public void Join()
        {
            this.fiberThread.Join();
        }

        /// <summary>
        /// Abort the fiber thread.
        /// </summary>
        public void Abort()
        {
            this.fiberThread.Abort();
        }
    }
}
