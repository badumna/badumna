using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Badumna.Utilities
{
    class FilteredEnumerable<T> : IEnumerable<T>
    {
        private IEnumerable<T> mFullEnumeration;
        private Predicate<T> mFilter;


        public FilteredEnumerable(IEnumerable<T> enumeration, Predicate<T> filter)
        {
            this.mFullEnumeration = enumeration;
            this.mFilter = filter;
        }

        public IEnumerator<T> GetEnumerator()
        {
           foreach (T item in this.mFullEnumeration)
           {
               if (this.mFilter(item))
               {
                   yield return item;
               }
           }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }
    }
}
