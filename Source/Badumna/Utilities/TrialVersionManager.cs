﻿//---------------------------------------------------------------------------------
// <copyright file="TrialVersionManager.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;

using Badumna.Core;

namespace Badumna.Utilities
{
    /// <summary>
    /// Utility class for managing trial version. 
    /// </summary>
    internal class TrialVersionManager
    {
        /// <summary>
        /// Gets a value indicating whether the running peer instance is built on a dev trial version of badumna.
        /// </summary>
        /// <returns>
        /// <c>true</c> if built on dev trial version; otherwise, <c>false</c>.
        /// </returns>
        private static bool IsDevTrialVersion
        {
            get
            {
#if DEV_TRIAL
                return true;
#else
                return false;
#endif
            }
        }

        /// <summary>
        /// Gets a value indicating whether the allowed trial run time has expired.
        /// </summary>
        /// <param name="now">The current execution time</param>
        /// <param name="onCloud">Indicates whether whether running on cloud or not</param>
        /// <returns>
        /// <c>true</c> if trial run time expired; otherwise, <c>false</c>.
        /// </returns>
        public static bool HasTrialVersionRunTimeExpired(TimeSpan now, bool onCloud)
        {
            return !onCloud && TrialVersionManager.IsDevTrialVersion && now.TotalMinutes > Parameters.MaximumDevTrailVersionRuntimeMinutes;
        }
    }
}
