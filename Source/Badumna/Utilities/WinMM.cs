﻿//-----------------------------------------------------------------------
// <copyright file="WinMM.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2010 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.InteropServices;

namespace Badumna.Utilities
{
    /// <summary>
    /// Wrapper for native winmm.dll functions.
    /// </summary>
    internal class WinMM
    {
        /// <summary>
        /// Requests that the timer resolution be set to the specified value.  This call
        /// must be matched by a call to TimeEndPeriod(uint).
        /// </summary>
        /// <param name="milliseconds">The desired timer resolution in milliseconds</param>
        public static void TimeBeginPeriod(uint milliseconds)
        {
            uint result;
            try
            {
                result = timeBeginPeriod(milliseconds);
            }
            catch (DllNotFoundException)
            {
                throw new NotSupportedException();
            }

            if (result != 0)
            {
                throw new ArgumentOutOfRangeException();
            }
        }

        /// <summary>
        /// Signifies that the previously requested timer resolution is no longer required.
        /// The parameter must match the parameter passed to the TimeBeginPeriod(uint) call.
        /// </summary>
        /// <param name="milliseconds">The previously specified timer resolution</param>
        public static void TimeEndPeriod(uint milliseconds)
        {
            uint result;
            try
            {
                result = timeEndPeriod(milliseconds);
            }
            catch (DllNotFoundException)
            {
                throw new NotSupportedException();
            }

            if (result != 0)
            {
                throw new ArgumentOutOfRangeException();
            }
        }

        /// <summary>
        /// Returns the current time in milliseconds.  If a call to TimeBeginPeriod(uint) has been
        /// made then the resolution of this value should be the resolution specified in that call.
        /// </summary>
        /// <returns>The current time in milliseconds</returns>
        public static uint TimeGetTime()
        {
            try
            {
                return timeGetTime();
            }
            catch (DllNotFoundException)
            {
                throw new NotSupportedException();
            }
        }

        [SuppressMessage("Microsoft.StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "This is an external function name which can't be changed")]
        [DllImport("winmm")]
        private static extern uint timeBeginPeriod(uint milliseconds);

        [SuppressMessage("Microsoft.StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "This is an external function name which can't be changed")]
        [DllImport("winmm")]
        private static extern uint timeEndPeriod(uint milliseconds);

        [SuppressMessage("Microsoft.StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "This is an external function name which can't be changed")]
        [DllImport("winmm")]
        private static extern uint timeGetTime();
    }
}
