﻿//---------------------------------------------------------------------------------
// <copyright file="StatisticsDetails.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

namespace Badumna.Utilities.UserTracker
{
    using System.IO;
    using Badumna.Core;
    using Badumna.Security;

    /// <summary>
    /// User details to be sent in statistics message.
    /// </summary>
    internal class StatisticsDetails : TrackingPacket
    {
        /// <summary>
        /// The magic number.
        /// </summary>
        private static readonly ushort MagicNumber = 0xab13;

        /// <summary>
        /// Backing field for <see cref="Payload"/>.  Marked as
        /// <c>volatile</c> so that updates from a different
        /// thread are seen.
        /// </summary>
        private volatile string payload = string.Empty;

        /// <summary>
        /// Initializes a new instance of the <see cref="StatisticsDetails"/> class.
        /// </summary>
        public StatisticsDetails()
            : base(StatisticsDetails.MagicNumber)
        {
            this.UserID = -1;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="StatisticsDetails"/> class.
        /// </summary>
        /// <param name="payload">The initial payload.</param>
        /// <param name="uniqueishId">The uniqueish identifier (an ID that with high probability distinguishes 
        /// this session within the local machine).</param>
        public StatisticsDetails(string payload, uint uniqueishId)
            : this()
        {
            this.Payload = payload;
            this.UniqueishID = uniqueishId;
        }

        /// <summary>
        /// Gets the minimum size.
        /// </summary>
        public static int MinimumSize
        {
            get { return 17; }
        }

        /// <summary>
        /// Gets or sets the user ID.  A value of -1 indicates an unknown user ID
        /// (most likely because no user is logged in).
        /// </summary>
        public long UserID { get; set; }

        /// <summary>
        /// Gets or sets the payload.
        /// </summary>
        public string Payload
        {
            get { return this.payload; }
            set { this.payload = value; }
        }

        /// <summary>
        /// Gets a value that will (with reasonable probability) distinguish this session from other sessions on the same machine.
        /// </summary>
        /// <remarks>
        /// This value shouldn't be modified within a session, so it can only be set through the constructor.
        /// </remarks>
        public uint UniqueishID { get; private set; }

        /// <inheritdoc />
        protected override void WritePayload(BinaryWriter writer)
        {
            writer.Write(this.UserID);
            writer.Write(this.Payload);
            writer.Write(this.UniqueishID);
        }

        /// <inheritdoc />
        protected override bool ReadPayload(BinaryReader reader)
        {
            this.UserID = reader.ReadInt64();
            this.Payload = reader.ReadString();
            this.UniqueishID = reader.ReadUInt32();
            return true;
        }
    }
}