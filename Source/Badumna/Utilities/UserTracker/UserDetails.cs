﻿//---------------------------------------------------------------------------------
// <copyright file="UserDetails.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

using System;
using System.IO;
using Badumna.Core;

namespace Badumna.Utilities.UserTracker
{
    /// <summary>
    /// The user details class contains all information that will be sent back to the stats collector server.
    /// </summary>
    internal class UserDetails : TrackingPacket
    {
        /// <summary>
        /// The magic number.
        /// </summary>
        private static readonly ushort MagicNumber = 0xab12;

        /// <summary>
        /// The network facade to query for addresses in use.
        /// Only used when serializing (not required for deserialization).
        /// </summary>
        private INetworkFacade networkFacade;

        /// <summary>
        /// Initializes a new instance of the <see cref="UserDetails"/> class
        /// ready for deserialization.
        /// </summary>
        public UserDetails()
            : base(UserDetails.MagicNumber)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UserDetails"/> class
        /// ready for serialization.
        /// </summary>
        /// <param name="networkFacade">The network facade to query for addresses in use.</param>
        /// <param name="applicationName">The application name</param>
        public UserDetails(INetworkFacade networkFacade, string applicationName)
            : base(UserDetails.MagicNumber)
        {
            this.networkFacade = networkFacade;
            this.ApplicationName = applicationName ?? "";
        }

        /// <summary>
        /// Gets the minimum size.
        /// </summary>
        /// <value>The minimum size.</value>
        public static int MinimumSize
        {
            get { return 11; }
        }

        /// <summary>
        /// Gets or sets the revision.
        /// </summary>
        /// <value>The revision.</value>
        public int Revision { get; set; }

        /// <summary>
        /// Gets or sets the name of the application.
        /// </summary>
        /// <value>The name of the application.</value>
        public string ApplicationName { get; set; }

        /// <summary>
        /// Gets or sets the public address.
        /// </summary>
        /// <value>The public address.</value>
        public string PublicAddress { get; set; }

        /// <summary>
        /// Gets or sets the private address.
        /// </summary>
        /// <value>The private address.</value>
        public string PrivateAddress { get; set; }

        /// <inheritdoc />
        protected override void WritePayload(BinaryWriter writer)
        {
            this.GetDetails();
            writer.Write(this.Revision);
            writer.Write(this.ApplicationName);
            writer.Write(this.PublicAddress);
            writer.Write(this.PrivateAddress);
        }

        /// <inheritdoc />
        protected override bool ReadPayload(BinaryReader reader)
        {
            this.Revision = reader.ReadInt32();
            this.ApplicationName = reader.ReadString();
            this.PublicAddress = reader.ReadString();
            this.PrivateAddress = reader.ReadString();
            return true;
        }

        /// <summary>
        /// Gets the details.
        /// </summary>
        private void GetDetails()
        {
            this.Revision = this.GetRevision();
            if (this.networkFacade != null)
            {
                NetworkStatus status = this.networkFacade.GetNetworkStatus();
                this.PrivateAddress = status.PrivateAddress;
                this.PublicAddress = status.PublicAddress;
            }
            else
            {
                this.PrivateAddress = string.Empty;
                this.PublicAddress = string.Empty;
            }
        }

        /// <summary>
        /// Gets the revision.
        /// </summary>
        /// <returns>The revision number or -1 on error.</returns>
        private int GetRevision()
        {
            int revision = -1;

            try
            {
                Version version = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version;
                revision = version.Revision;
            }
            catch 
            { 
            }

            return revision;
        }
    }
}
