﻿//-----------------------------------------------------------------------
// <copyright file="UserTracker.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2011 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using Badumna.Core;

namespace Badumna.Utilities.UserTracker
{
    /// <summary>
    /// A <see cref="Tracker{T}"/> for <see cref="UserDetails"/>.
    /// </summary>
    internal class UserTracker : Tracker<UserDetails>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UserTracker"/> class.
        /// </summary>
        /// <param name="networkFacade">The network facade to collect status information from.</param>
        /// <param name="eventQueue">The queue to schedule events on.</param>
        /// <param name="connectivityReporter">Reports on network connectivity status.</param>
        /// <param name="applicationName">The current application name</param>
        public UserTracker(INetworkFacade networkFacade, NetworkEventQueue eventQueue, INetworkConnectivityReporter connectivityReporter, string applicationName)
            : base(eventQueue, connectivityReporter, Parameters.PhoneHomeInterval, Parameters.UserTrackerHostname, Parameters.UserTrackerPort, new UserDetails(networkFacade, applicationName))
        {
        }
    }
}
