﻿//-----------------------------------------------------------------------
// <copyright file="StatisticsTracker.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2011 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using Badumna.Core;
using Badumna.Security;
using Badumna.Utilities.UserTracker;

namespace Badumna.Utilities
{
    /// <summary>
    /// Regularly sends packets to a server to estimate usage statistics.
    /// </summary>
    public class StatisticsTracker
    {
        /// <summary>
        /// The actual tracker.
        /// </summary>
        private Tracker<StatisticsDetails> tracker;

        /// <summary>
        /// Initializes a new instance of the <see cref="StatisticsTracker"/> class.
        /// </summary>
        /// <param name="eventQueue">The queue to schedule events on.</param>
        /// <param name="connectivityReporter">Reports on network connectivity status.</param>
        /// <param name="interval">The interval between sending packets.</param>
        /// <param name="serverAddress">The server's address.</param>
        /// <param name="serverPort">The server's port.</param>
        /// <param name="initialPayload">The intial payload string for the packet.</param>
        internal StatisticsTracker(NetworkEventQueue eventQueue, INetworkConnectivityReporter connectivityReporter, TimeSpan interval, string serverAddress, int serverPort, string initialPayload)
        {
            uint uniqueishId = RandomSource.NextUInt();
            this.tracker = new Tracker<StatisticsDetails>(eventQueue, connectivityReporter, interval, serverAddress, serverPort, new StatisticsDetails(initialPayload, uniqueishId));
        }

        /// <summary>
        /// Sets the user ID in the data packet.
        /// </summary>
        /// <param name="userId">The new user ID.</param>
        public void SetUserID(long userId)
        {
            this.tracker.Packet.UserID = userId;
        }

        /// <summary>
        /// Sets the payload string in the data packet.
        /// </summary>
        /// <remarks>
        /// Tracking packets with matching payload strings are grouped together in the statistics
        /// on the tracking server.
        /// A typical use would be to set the payload to the name of the current scene.
        /// </remarks>
        /// <param name="payload">The new payload.</param>
        public void SetPayload(string payload)
        {
            this.tracker.Packet.Payload = payload;
        }

        /// <summary>
        /// Starts the tracker.
        /// </summary>
        public void Start()
        {
            this.tracker.Start();
        }

        /// <summary>
        /// Stops the tracker.
        /// </summary>
        public void Stop()
        {
            this.tracker.Stop();
        }
    }
}
