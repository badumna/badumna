﻿//---------------------------------------------------------------------------------
// <copyright file="Tracker.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Net;

using Badumna.Core;
using Badumna.Platform;

namespace Badumna.Utilities.UserTracker
{
    /// <summary>
    /// Sends messages to a tracking server to estimate usage statistics.
    /// </summary>
    /// <typeparam name="T">The packet type to send.</typeparam>
    internal class Tracker<T> where T : TrackingPacket
    {
        /// <summary>
        /// The task used to send messages.
        /// </summary>
        private OneShotTask task;

        /// <summary>
        /// The queue to use for scheduling events.
        /// </summary>
        private NetworkEventQueue eventQueue;

        /// <summary>
        /// The interval between sending messages.
        /// </summary>
        private TimeSpan interval;

        /// <summary>
        /// The server's address.
        /// </summary>
        private string serverAddress;

        /// <summary>
        /// The server's port.
        /// </summary>
        private int serverPort;

        /// <summary>
        /// Reports on network connectivity status.
        /// </summary>
        private INetworkConnectivityReporter connectivityReporter;

        /// <summary>
        /// Initializes a new instance of the <see cref="Tracker{T}"/> class.
        /// </summary>
        /// <param name="eventQueue">The queue to use for scheduling events.</param>
        /// <param name="connectivityReporter">Reports on network connectivity status.</param>
        /// <param name="interval">The interval between sending messages.</param>
        /// <param name="serverAddress">The address of the server.</param>
        /// <param name="serverPort">The port on the server.</param>
        /// <param name="packet">The data packet to send.</param>
        public Tracker(NetworkEventQueue eventQueue, INetworkConnectivityReporter connectivityReporter, TimeSpan interval, string serverAddress, int serverPort, T packet)
        {
            this.eventQueue = eventQueue;
            this.connectivityReporter = connectivityReporter;
            this.interval = interval;
            this.serverAddress = serverAddress;
            this.serverPort = serverPort;
            this.Packet = packet;
        }

        /// <summary>
        /// Gets the data packet.
        /// </summary>
        public T Packet { get; private set; }

        /// <summary>
        /// Starts this instance.
        /// </summary>
        public void Start()
        {
            TimeSpan interval = this.interval;

            if (this.task == null)
            {
                // initial setup
                this.task = new OneShotTask(this.eventQueue);
                interval = Parameters.InitialPhoneHomeInterval;
            }

            this.task.Trigger(interval, Apply.Func(this.SendPhoneHomeMessage));
        }

        /// <summary>
        /// Shutdowns this instance.
        /// </summary>
        public void Stop()
        {
            if (this.task != null)
            {
                this.task.Cancel();
                this.task = null;
            }
        }

        /// <summary>
        /// Sends the phone home message and schedule the next task..
        /// </summary>
        private void SendPhoneHomeMessage()
        {
            if (this.connectivityReporter.Status != ConnectivityStatus.Online)
            {
                this.task.Trigger(Parameters.InitialPhoneHomeInterval, Apply.Func(this.SendPhoneHomeMessage));
                return;
            }

            try
            {
                using (Socket socket = this.GetSocket())
                {
                    byte[] message = this.Packet.ToArray();
                    IPEndPoint endpoint = this.GetServerEndPoint();
                    if (endpoint != null && message != null && message.Length > 0)
                    {
                        int sent = socket.SendTo(message, endpoint);
                        if (sent != message.Length)
                        {
                            Logger.TraceInformation(LogTag.Utilities, "Failed to send the whole message.");
                        }
                    }
                }
            }
            catch (Exception)
            {
            }

            this.task.Trigger(this.interval, Apply.Func(this.SendPhoneHomeMessage));
        }

        /// <summary>
        /// Gets the socket used for sending the phone home message.
        /// </summary>
        /// <returns>The udp socket.</returns>
        private Socket GetSocket()
        {
            Socket socket = null;

            try
            {
                socket = Socket.Create(System.Net.Sockets.AddressFamily.InterNetwork, System.Net.Sockets.SocketType.Dgram, System.Net.Sockets.ProtocolType.Udp);
            }
            catch (Exception e)
            {
                Logger.TraceException(LogLevel.Warning, e, "Failed to create socket for phone home message.");
                throw;
            }

            return socket;
        }

        /// <summary>
        /// Gets the server end point.
        /// </summary>
        /// <returns>The server end point.</returns>
        private IPEndPoint GetServerEndPoint()
        {
            PeerAddress serverEndPoint = PeerAddress.GetAddress(this.serverAddress, this.serverPort);
            if (!serverEndPoint.IsValid)
            {
                Logger.TraceInformation(LogTag.Utilities, "Invalid user tracker address {0}", serverEndPoint);
                return null;
            }

            return serverEndPoint.EndPoint;
        }
    }
}
