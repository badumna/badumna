﻿//-----------------------------------------------------------------------
// <copyright file="TrackingPacket.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2011 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System.IO;

namespace Badumna.Utilities.UserTracker
{
    /// <summary>
    /// A container for data that will be sent to a tracking server.
    /// </summary>
    internal abstract class TrackingPacket
    {
        /// <summary>
        /// Identifies the packet.
        /// </summary>
        private readonly ushort magicNumber;

        /// <summary>
        /// Initializes a new instance of the <see cref="TrackingPacket"/> class.
        /// </summary>
        /// <param name="magicNumber">A number that distinguishes tracking packets from other packets.</param>
        protected TrackingPacket(ushort magicNumber)
        {
            this.magicNumber = magicNumber;
        }

        /// <summary>
        /// Get a byte array that contains serialized packet data.
        /// </summary>
        /// <returns>The serialized packet data.</returns>
        public byte[] ToArray()
        {
            using (MemoryStream stream = new MemoryStream())
            {
                using (BinaryWriter writer = new BinaryWriter(stream))
                {
                    writer.Write(this.magicNumber);

                    this.WritePayload(writer);

                    writer.Flush();

                    ushort checksum = CRC.CRC16(stream.ToArray());

                    writer.Write(checksum);
                    writer.Flush();

                    return stream.ToArray();
                }
            }
        }

        /// <summary>
        /// Deserializes the specified packet data.
        /// </summary>
        /// <param name="buffer">The packet data.</param>
        /// <returns><c>true</c> if the packet contained valid data, <c>false</c> otherwise.</returns>
        public bool Deserialize(byte[] buffer)
        {
            try
            {
                using (MemoryStream stream = new MemoryStream(buffer))
                {
                    using (BinaryReader reader = new BinaryReader(stream))
                    {
                        ushort magic = reader.ReadUInt16();

                        if (magic != this.magicNumber)
                        {
                            return false;
                        }

                        if (!this.ReadPayload(reader))
                        {
                            return false;
                        }

                        ushort checksum = CRC.CRC16(buffer, 0, (int)stream.Position);
                        if (checksum != reader.ReadUInt16())
                        {
                            return false;
                        }
                    }
                }
            }
            catch
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Writes the payload data to the packet.
        /// </summary>
        /// <param name="writer">The binary writer to write the payload data to.</param>
        protected abstract void WritePayload(BinaryWriter writer);

        /// <summary>
        /// Reads the payload data from the packet.
        /// </summary>
        /// <param name="reader">The binary reader to read the payload data from.</param>
        /// <returns><c>true</c> if the packet contained a valid payload, <c>false</c> otherwise.</returns>
        protected abstract bool ReadPayload(BinaryReader reader);
    }
}
