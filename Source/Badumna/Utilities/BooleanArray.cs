﻿using System;
using System.Diagnostics;
using System.Text;

using Badumna.Core;

namespace Badumna.Utilities
{
    // TODO: Better support for sparse arrays?

    /// <summary>
    /// An automatically sized array of booleans.  Supports a default value for
    /// elements which are not explicitly set.
    /// </summary>
    public class BooleanArray : IEquatable<BooleanArray>, IParseable
    {
        private const int BitsPerElement = 32;
        
        /// <summary>
        /// The maximum usable index (inclusive).  e.g. Accessing this[MaxIndex + 1] will
        /// throw an ArgumentOfOfRangeException, accessing this[MaxIndex] will succeed.
        /// </summary>
        // !!! DO NOT CHANGE THIS VALUE without reviewing its use in UpdateEntityRequest.UpdateToJson and related places !!!
        public const int MaxIndex = 126;  // Currently restricted to this because of the serialization method

        /// <summary>
        /// Get or set the element at the specified index.
        /// </summary>
        public virtual bool this[int index]
        {
            get
            {
                if (index < 0 || index > MaxIndex)
                {
                    throw new ArgumentOutOfRangeException("index");
                }

                this.mHighestUsedIndex = Math.Max(this.mHighestUsedIndex, index);

                int uintIndex = index / BitsPerElement;
                if (uintIndex >= this.mBooleans.Length)
                {
                    return this.mDefaultValue;
                }

                uint bit = (this.mBooleans[uintIndex] >> (index % BitsPerElement)) & 1;
                return bit != 0;
            }

            set
            {
                if (index < 0 || index > MaxIndex)
                {
                    throw new ArgumentOutOfRangeException("index");
                }

                this.mHighestUsedIndex = Math.Max(this.mHighestUsedIndex, index);

                int uintIndex = index / BitsPerElement;
                while (uintIndex >= this.mBooleans.Length)
                {
                    if (value == this.mDefaultValue)
                    {
                        return;
                    }
                    this.Resize(this.mBooleans.Length * 2);
                }

                uint bit = (uint)1 << (index % BitsPerElement);
                if (value)
                {
                    this.mBooleans[uintIndex] |= bit;
                }
                else
                {
                    this.mBooleans[uintIndex] &= ~bit;
                }
            }
        }

        /// <summary>
        /// The highest index actually referenced (directly or indirectly, e.g. via an Or).  A
        /// value less than 0 indicates that no index has been referenced yet.
        /// </summary>
        public int HighestUsedIndex { get { return this.mHighestUsedIndex; } }


        /// <summary>
        /// The behaviour of this field shouldn't be modified without serious consideration of effects on existing code.
        /// </summary>
        private int mHighestUsedIndex = -1;

        private bool mDefaultValue;
        private uint[] mBooleans = new uint[1];  // Any bit present in here is valid.  0 = false, 1 = true always.  If bit is outside range (i.e index / 64 >= mBooleans.Length) then mDefaultValue is used.


        /// <summary>
        /// Construct a new BooleanArray with all elements initialized to false.
        /// </summary>
        public BooleanArray()
            : this(false)
        {
        }

        /// <summary>
        /// Construct a new BooleanArray with all elements initialized to the given initialValue.
        /// </summary>
        /// <param name="initialValue">The value to initialize elements with.</param>
        public BooleanArray(bool initialValue)
        {
            // We don't call SetAll for the false case because ImmutableBooleanArray calls this 
            // constructor and overrides SetAll to throw NotSupportedException.  Don't need
            // to do anything for the false case anyway because the defaults are correct.
            if (initialValue)
            {
                this.SetAll(true);
            }
        }

        /// <summary>
        /// Construct a new BooleanArray with the specified indexes set to true, with the remaining
        /// elements set to false.
        /// </summary>
        /// <param name="indexesSetToTrue"></param>
        public BooleanArray(params int[] indexesSetToTrue)
        {
            foreach (int index in indexesSetToTrue)
            {
                this[index] = true;
            }
        }

        /// <summary>
        /// Construct a copy of a BooleanArray.
        /// </summary>
        /// <param name="other">The BooleanArray to copy</param>
        public BooleanArray(BooleanArray other)
        {
            this.mHighestUsedIndex = other.mHighestUsedIndex;
            this.mDefaultValue = other.mDefaultValue;
            this.mBooleans = new uint[other.mBooleans.Length];
            Array.Copy(other.mBooleans, this.mBooleans, other.mBooleans.Length);
        }

        private void Resize(int newUintArraySize)
        {
            if (newUintArraySize <= this.mBooleans.Length)
            {
                return;
            }

            uint[] newBooleans = new uint[newUintArraySize];
            Array.Copy(this.mBooleans, newBooleans, this.mBooleans.Length);
            if (this.mDefaultValue)
            {
                for (int i = this.mBooleans.Length; i < newUintArraySize; i++)
                {
                    newBooleans[i] = uint.MaxValue;
                }
            }
            this.mBooleans = newBooleans;
        }

        private uint GetUInt(int index)
        {
            if (index < this.mBooleans.Length)
            {
                return this.mBooleans[index];
            }

            return this.mDefaultValue ? uint.MaxValue : 0;
        }

        /// <summary>
        /// Set all elements of the BooleanArray to the specified value.
        /// </summary>
        /// <param name="value">The value to set the elements of the BooleanArray to.</param>
        public virtual void SetAll(bool value)
        {
            for (int i = 0; i < this.mBooleans.Length; i++)
            {
                this.mBooleans[i] = value ? uint.MaxValue : 0;
            }
            this.mDefaultValue = value;
        }

        /// <summary>
        /// Returns true if any element of the array is true, false otherwise.
        /// </summary>
        /// <returns></returns>
        public bool Any()
        {
            if (this.mDefaultValue)
            {
                return true;
            }

            for (int i = 0; i < this.mBooleans.Length; i++)
            {
                if (this.mBooleans[i] > 0)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Performs an elementwise logical or with a BooleanArray, storing the result in this
        /// BooleanArray.
        /// </summary>
        /// <param name="other">The BooleanArray to 'or' with.</param>
        public virtual void Or(BooleanArray other)
        {
            if (this.mBooleans.Length < other.mBooleans.Length && !this.mDefaultValue)
            {
                this.Resize(other.mBooleans.Length);
            }

            for (int i = 0; i < this.mBooleans.Length; i++)
            {
                this.mBooleans[i] |= other.GetUInt(i);
            }

            this.mDefaultValue |= other.mDefaultValue;

            this.mHighestUsedIndex = Math.Max(this.mHighestUsedIndex, other.mHighestUsedIndex);
        }

        /// <summary>
        /// Gets the hash code for the instance.
        /// </summary>
        public override int GetHashCode()
        {
            return this.mBooleans[0].GetHashCode();
        }

        /// <summary>
        /// Returns a string representing the contents of the instance.
        /// </summary>
        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();

            if (this.mDefaultValue)
            {
                builder.Append("...111");
            }
            else
            {
                builder.Append("...000");
            }

            for (int i = this.mHighestUsedIndex; i >= 0; i--)
            {
                builder.Append(this[i] ? "1" : "0");
            }

            return builder.ToString();
        }

        /// <summary>
        /// Attempts to construct a BooleanArray initialized according to the 
        /// given string.  The string should be in the same format as returned
        /// by ToString().
        /// </summary>
        /// <param name="bits">The string to parse</param>
        /// <returns>A BooleanArray initialize according to bits, or null if the
        /// string could not be parsed.</returns>
        public static BooleanArray TryParse(string bits)
        {
            bool defaultValue = false;

            if (bits.StartsWith("..."))
            {
                bits = bits.Substring(3);
                defaultValue = bits.StartsWith("1");
                bits = bits.Substring(3);
            }

            BooleanArray bools = new BooleanArray(defaultValue);

            int index = 0;
            for (int i = bits.Length - 1; i >= 0; i--)
            {
                switch (bits[i])
                {
                    case '0':
                        bools[index] = false;
                        break;

                    case '1':
                        bools[index] = true;
                        break;

                    default:
                        return null;
                }

                index++;
            }

            return bools;
        }

        /// <summary>
        /// Returns true if obj is a BooleanArray and stores the same elements as this instance, false otherwise.
        /// </summary>
        /// <param name="obj">The object to check for equality with this BooleanArray</param>
        public override bool Equals(object obj)
        {
            return this.Equals(obj as BooleanArray);
        }

        /// <summary>
        /// Returns true if other stores the same elements as this instance, false otherwise.
        /// </summary>
        /// <param name="other">The BooleanArray to check for equality with this BooleanArray</param>
        public bool Equals(BooleanArray other)
        {
            if (other == null)
            {
                return false;
            }

            if (this.mDefaultValue != other.mDefaultValue)
            {
                return false;
            }

            for (int i = 0; i < this.mBooleans.Length || i < other.mBooleans.Length; i++)
            {
                if (this.GetUInt(i) != other.GetUInt(i))
                {
                    return false;
                }
            }

            return true;
        }

        void IParseable.ToMessage(MessageBuffer message, Type parameterType)
        {
            int bitsUsed = this.mHighestUsedIndex + 1;
            Debug.Assert(bitsUsed < 128);

            byte header = (byte)bitsUsed;
            if (this.mDefaultValue)
            {
                header |= 0x80;
            }
            message.Write(header);

            int totalBytes = (bitsUsed + 7) / 8;
            int totalUInt = (totalBytes + 3) / 4;
            for (int i = 0; i < totalUInt && totalBytes > 0; i++)
            {
                uint current = this.GetUInt(i);

                for (int j = 0; j < 4 && totalBytes > 0; j++)
                {
                    message.Write((byte)(current & 0xFF));
                    current >>= 8;
                    totalBytes--;
                }
            }
        }

        void IParseable.FromMessage(MessageBuffer message)
        {
            byte header = message.ReadByte();
            int totalBits = header & 0x7F;
            this.mHighestUsedIndex = totalBits - 1;
            int totalBytes = (totalBits + 7) / 8;
            int totalUInt = (totalBytes + 3) / 4;
            this.mBooleans = new uint[totalUInt];
            this.SetAll((header & 0x80) > 0);

            for (int i = 0; i < this.mBooleans.Length && totalBytes > 0; i++)
            {
                for (int j = 0; j < 4 && totalBytes > 0; j++)
                {
                    uint next = (uint)message.ReadByte() << (j * 8);
                    uint mask = ~((uint)0xFF << (j * 8));
                    this.mBooleans[i] &= mask;
                    this.mBooleans[i] |= next;
                    totalBytes--;
                }
            }
        }
    }
}
