using System;
using System.Collections.Generic;
using System.Text;

using Badumna;
using Badumna.InterestManagement;
using Badumna.DataTypes;

namespace Badumna.Utilities
{
    class Geometry
    {
        public static double SphereVolume(float r)
        {
            if (r < 0.0F)
            {
                throw new ArgumentOutOfRangeException("r");
            }

            return 4.0 * Math.PI * Math.Pow(r, 3.0) / 3.0;
        }

        public static bool SpheresIntersect(Vector3 c1, float r1, Vector3 c2, float r2)
        {
            if (r1 < 0.0F)
            {
                throw new ArgumentOutOfRangeException("r1");
            }
            if (r2 < 0.0F)
            {
                throw new ArgumentOutOfRangeException("r2");
            }

            return (c1 - c2).Magnitude < r1 + r2;
        }

        public static double SphereIntersectVolume(Vector3 c1, float r1, Vector3 c2, float r2)
        {
            if (r1 < 0.0F)
            {
                throw new ArgumentOutOfRangeException("r1");
            }
            if (r2 < 0.0F)
            {
                throw new ArgumentOutOfRangeException("r2");
            }

            double d = (c1 - c2).Magnitude;

            if (d + 0.01 > r1 + r2)  // No intersection
            {
                return 0.0;
            }

            if (d - 0.01 <= Math.Abs(r1 - r2))  // Fully contained
            {
                return Geometry.SphereVolume(Math.Min(r1, r2));
            }

            return (Math.PI * Math.Pow(r1 + r2 - d, 2.0) * (d * d + 2.0 * d * r2 - 3.0 * r2 * r2 + 2.0 * d * r1 + 6.0 * r1 * r2 - 3.0 * r1 * r1) / (12.0 * d));
        }

        public static bool SphereContained(Vector3 containingCenter, float containingRadius, Vector3 containedCenter, float containedRadius)
        {
            if (containingRadius < 0.0f)
            {
                throw new ArgumentOutOfRangeException("containingRadius");
            }
            if (containedRadius < 0.0f)
            {
                throw new ArgumentOutOfRangeException("containedRadius");
            }

            return (containedCenter - containingCenter).Magnitude + containedRadius <= containingRadius;
        }

        public static bool PointContained(Vector3 containingCenter, float containingRadius, Vector3 point)
        {
            if (containingRadius < 0.0f)
            {
                throw new ArgumentOutOfRangeException("containingRadius");
            }

            return (point - containingCenter).Magnitude <= containingRadius;
        }

        /// <summary>
        /// Modifies the radius and center of sphere 2 so that it just touches sphere 1, with the
        /// constraints that the new center will be co-linear with the original centers, and the 
        /// point on sphere 2 intersected by this line furthest from sphere 1 will remain in a fixed position.
        /// 
        /// If c2 is contained within sphere 1 then this function will return false and leave c2 and r2
        /// unchanged.
        /// </summary>
        /// <param name="c1"></param>
        /// <param name="r1"></param>
        /// <param name="c2"></param>
        /// <param name="r2"></param>
        public static bool ShrinkSphere(Vector3 c1, float r1, ref Vector3 c2, ref float r2)
        {
            if (r1 < 0)
            {
                throw new ArgumentOutOfRangeException("r1");
            }
            if (r2 < 0)
            {
                throw new ArgumentOutOfRangeException("r2");
            }

            Vector3 x = c2 - c1;
            float xMag = x.Magnitude;

            if (xMag <= r1)
            {
                return false;
            }

            Vector3 a = c2 + (r2 / xMag) * x;
            Vector3 b = c1 + (r1 / xMag) * x;

            c2 = (a + b) / 2.0f;
            r2 = (a - b).Magnitude / 2.0f;

            return true;
        }


        public static void CalculateBoundingSphere(Vector3 c1, float r1, ref Vector3 c2, ref float r2)
        {
            // Find a bounding sphere by taking the line passing through the two centers and finding the outermost points
            // where the line intersects with a sphere.  These two points are used to define the sphere.

            Vector3 x = c1 - c2;
            float xMag = x.Magnitude;

            if (xMag == 0.0f)
            {
                r2 = Math.Max(r1, r2);
                return;
            }

            x /= xMag;


            // Calculate the four intersecting points
            Vector3[] points = new Vector3[4];

            points[0] = c1 + r1 * x;
            points[1] = c1 - r1 * x;
            points[2] = c2 + r2 * x;
            points[3] = c2 - r2 * x;

            float minValue = 0.0f;
            Vector3 minPoint = points[0];
            float maxValue = 0.0f;
            Vector3 maxPoint = points[0];

            for (int i = 1; i < 4; i++)
            {
                float value = x.Dot(points[i] - points[0]);

                if (value < minValue)
                {
                    minValue = value;
                    minPoint = points[i];
                }

                if (value > maxValue)
                {
                    maxValue = value;
                    maxPoint = points[i];
                }
            }

            c2 = (minPoint + maxPoint) / 2.0f;
            r2 = (minPoint - maxPoint).Magnitude / 2.0f;
        }
    }
}
