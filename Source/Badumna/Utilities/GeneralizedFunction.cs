﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.Diagnostics;

using Badumna.Core;

namespace Badumna.Utilities
{
    using Badumna.Utilities.Logging;

    /// <summary>
    /// Builds delegate instances from reflection information.
    /// </summary>
    /// <remarks>
    /// This is used to speed up the invocation of protocol methods.
    /// </remarks>
    class GeneralizedFunction
    {
#if !IOS
        static private Dictionary<int, Type> DelegateTypes = new Dictionary<int, Type>()
            {
                { 0, typeof(GenericCallBack) },
                { 1, typeof(GenericCallBack<>) },
                { 2, typeof(GenericCallBack<,>) },
                { 3, typeof(GenericCallBack<,,>) },
                { 4, typeof(GenericCallBack<,,,>) },
                { 5, typeof(GenericCallBack<,,,,>) },
                { 6, typeof(GenericCallBack<,,,,,>) } 
            };

        static private Dictionary<int, Type> ContainerTypes = new Dictionary<int, Type>()
            {
                { 0, typeof(GeneralizedFunction) },
                { 1, typeof(GeneralizedFunction<>) },
                { 2, typeof(GeneralizedFunction<,>) },
                { 3, typeof(GeneralizedFunction<,,>) },
                { 4, typeof(GeneralizedFunction<,,,>) },
                { 5, typeof(GeneralizedFunction<,,,,>) },
                { 6, typeof(GeneralizedFunction<,,,,,>) } 
            };
#endif

        /// <summary>
        /// Constructs an instance of GenneralizedFunction or a derived type with generic type parameters that
        /// match the parameters of the <paramref name="info"/>.
        /// </summary>
        /// <param name="target">The target object to invoke the method on</param>
        /// <param name="info">The method to invoke</param>
        /// <param name="factory">A factory that constructs instances of the given type, used when the GeneralizedFunction is invoking the method
        /// using parameters from a <see cref="MessageBuffer"/></param>
        /// <returns>The new instance</returns>
        static public GeneralizedFunction Make(object target, MethodInfo info, GenericCallBackReturn<object, Type> factory)
        {
#if IOS
			return new InstanceMethodInvocation(target, info, factory);
#else
            ParameterInfo[] paramInfos = info.GetParameters();
            if (paramInfos.Length > 6)
            {
                throw new NotSupportedException("Functions of more than 6 arguments are not supported");
            }

            Type[] paramTypes = null;

            Type delegateType = GeneralizedFunction.DelegateTypes[paramInfos.Length];
            if (delegateType.IsGenericType)
            {
                paramTypes = new Type[paramInfos.Length];
                for (int i = 0; i < paramInfos.Length; i++)
                {
                    paramTypes[i] = paramInfos[i].ParameterType;
                }

                delegateType = delegateType.MakeGenericType(paramTypes);
            }

            object callback = Delegate.CreateDelegate(delegateType, target, info);

            //ConstructorInfo delegateConstructor = delegateType.GetConstructor(new Type[] { typeof(object), typeof(IntPtr) });
            //object callback = delegateConstructor.Invoke(new object[] { target, info.MethodHandle.GetFunctionPointer() });

            Type containerType = GeneralizedFunction.ContainerTypes[paramInfos.Length];
            if (containerType.IsGenericType)
            {
                containerType = containerType.MakeGenericType(paramTypes);
            }

            ConstructorInfo containerConstructor = containerType.GetConstructor(new Type[] { delegateType, typeof(GenericCallBackReturn<object, Type>) });
            return (GeneralizedFunction)containerConstructor.Invoke(new object[] { callback, factory });
#endif
        }




        private GenericCallBack mFunction;

        /// <summary>
        /// A factory that constructs instances of the given type, used when the GeneralizedFunction is invoking the method
        /// using parameters from a <see cref="MessageBuffer"/>.
        /// </summary>
        protected GenericCallBackReturn<object, Type> factory;

        public GeneralizedFunction(GenericCallBack function, GenericCallBackReturn<object, Type> factory)
            : this(factory)
        {
            this.mFunction = function;
        }

        protected GeneralizedFunction(GenericCallBackReturn<object, Type> factory)
        {
            this.factory = factory;
        }

        [Conditional("TRACE")]
        protected void TraceCall(IEnvelope envelope, Delegate function, params object[] args)
        {
            Logger.TraceInformation(
                LogTag.RemoteCall,
                "<-- {0} {1}",
                envelope,
                Utils.DescribeCall(function, args));
        }

        public virtual void Call(params object[] args)
        {
            this.mFunction();
        }

        /// <summary>
        /// Calls the function, pulling the required arguments from the message buffer.
        /// </summary>
        public virtual void CallFromMessageWithStatistics(IEnvelope envelope, string layerName)
        {
            this.TraceCall(envelope, this.mFunction);
            this.Call();
        }

        public virtual object[] ReadParameters(IEnvelope envelope)
        {
            return new object[0];
        }
    }

	class InstanceMethodInvocation : GeneralizedFunction
	{
		private object target;

		private MethodInfo method;

		public InstanceMethodInvocation(object target, MethodInfo methodInfo, GenericCallBackReturn<object, Type> factory)
			: base(factory)
		{
			this.target = target;
			this.method = methodInfo;
		}

		public override void Call (params object[] args)
		{
			Logger.TraceInformation(LogTag.Utilities, "Invoking " + this.Signature());
			this.method.Invoke(target, args);
		}

        public override void CallFromMessageWithStatistics(IEnvelope envelope, string layerName)
        {
			List<object> parameters = new List<object>(this.method.GetParameters().Length);
			foreach (var param in this.method.GetParameters())
			{
				parameters.Add(envelope.Message.GetObjectWithStatistics(param.ParameterType, this.factory, layerName, this.method.Name));
			}

			this.Call(parameters.ToArray());
        }

        public override object[] ReadParameters(IEnvelope envelope)
        {
			List<object> parameters = new List<object>(this.method.GetParameters().Length);
			foreach (var param in this.method.GetParameters())
			{
				parameters.Add(envelope.Message.GetObject(param.ParameterType, this.factory));
			}

			return parameters.ToArray();
		}

		private string Signature()
		{
            StringBuilder call = new StringBuilder();
            call.Append(this.target.GetType().Name).Append(".");
            call.Append(method.Name).Append("(");
            System.Reflection.ParameterInfo[] parameters = this.method.GetParameters();
            for (int i = 0; i < parameters.Length; i++)
            {
                if (i != 0)
                {
                    call.Append(", ");
                }
                call.Append(parameters[i].ParameterType.ToString());
            }
            call.Append(")");

            return call.ToString();
		}
 	}

    class GeneralizedFunction<A> : GeneralizedFunction
    {
        private GenericCallBack<A> mFunction;

        public GeneralizedFunction(GenericCallBack<A> function, GenericCallBackReturn<object, Type> factory)
            : base(factory)
        {
            this.mFunction = function;
        }

        public override void Call(params object[] args)
        {
            this.mFunction((A)args[0]);
        }

        public override void CallFromMessageWithStatistics(IEnvelope envelope, string layerName)
        {
            string methodName = this.mFunction.Method.Name;
            var args = new object[]
            {
                envelope.Message.GetObjectWithStatistics(typeof(A), this.factory, layerName, methodName)
            };
            this.TraceCall(envelope, this.mFunction, args);
            this.Call(args);
        }

        public override object[] ReadParameters(IEnvelope envelope)
        {
            return new object[]
            {
                envelope.Message.GetObject(typeof(A), this.factory)
            };
        }
    }

    class GeneralizedFunction<A, B> : GeneralizedFunction
    {
        private GenericCallBack<A, B> mFunction;

        public GeneralizedFunction(GenericCallBack<A, B> function, GenericCallBackReturn<object, Type> factory)
            : base(factory)
        {
            this.mFunction = function;
        }

        public override void Call(params object[] args)
        {
            this.mFunction((A)args[0], (B)args[1]);
        }

        public override void CallFromMessageWithStatistics(IEnvelope envelope, string layerName)
        {
            string methodName = this.mFunction.Method.Name;
            var args = new object[]
            {
                envelope.Message.GetObjectWithStatistics(typeof(A), this.factory, layerName, methodName),
                envelope.Message.GetObjectWithStatistics(typeof(B), this.factory, layerName, methodName)
            };
            this.TraceCall(envelope, this.mFunction, args);
            this.Call(args);
        }

        public override object[] ReadParameters(IEnvelope envelope)
        {
            return new object[]
            {
                envelope.Message.GetObject(typeof(A), this.factory),
                envelope.Message.GetObject(typeof(B), this.factory)
            };
        }
    }

    class GeneralizedFunction<A, B, C> : GeneralizedFunction
    {
        private GenericCallBack<A, B, C> mFunction;

        public GeneralizedFunction(GenericCallBack<A, B, C> function, GenericCallBackReturn<object, Type> factory)
            : base(factory)
        {
            this.mFunction = function;
        }

        public override void Call(params object[] args)
        {
            this.mFunction((A)args[0], (B)args[1], (C)args[2]);
        }

        public override void CallFromMessageWithStatistics(IEnvelope envelope, string layerName)
        {
            string methodName = this.mFunction.Method.Name;
            var args = new object[]
            {
                envelope.Message.GetObjectWithStatistics(typeof(A), this.factory, layerName, methodName),
                envelope.Message.GetObjectWithStatistics(typeof(B), this.factory, layerName, methodName),
                envelope.Message.GetObjectWithStatistics(typeof(C), this.factory, layerName, methodName)
            };
            this.TraceCall(envelope, this.mFunction, args); 
            this.Call(args);
        }

        public override object[] ReadParameters(IEnvelope envelope)
        {
            return new object[]
            {
                envelope.Message.GetObject(typeof(A), this.factory),
                envelope.Message.GetObject(typeof(B), this.factory),
                envelope.Message.GetObject(typeof(C), this.factory)
            };
        }
    }

    class GeneralizedFunction<A, B, C, D> : GeneralizedFunction
    {
        private GenericCallBack<A, B, C, D> mFunction;

        public GeneralizedFunction(GenericCallBack<A, B, C, D> function, GenericCallBackReturn<object, Type> factory)
            : base(factory)
        {
            this.mFunction = function;
        }

        public override void Call(params object[] args)
        {
            this.mFunction((A)args[0], (B)args[1], (C)args[2], (D)args[3]);
        }

        public override void CallFromMessageWithStatistics(IEnvelope envelope, string layerName)
        {
            string methodName = this.mFunction.Method.Name;
            var args = new object[]
            {
                envelope.Message.GetObjectWithStatistics(typeof(A), this.factory, layerName, methodName),
                envelope.Message.GetObjectWithStatistics(typeof(B), this.factory, layerName, methodName),
                envelope.Message.GetObjectWithStatistics(typeof(C), this.factory, layerName, methodName),
                envelope.Message.GetObjectWithStatistics(typeof(D), this.factory, layerName, methodName)
            };
            this.TraceCall(envelope, this.mFunction, args);
            this.Call(args);
        }

        public override object[] ReadParameters(IEnvelope envelope)
        {
            return new object[]
            {
                envelope.Message.GetObject(typeof(A), this.factory),
                envelope.Message.GetObject(typeof(B), this.factory),
                envelope.Message.GetObject(typeof(C), this.factory),
                envelope.Message.GetObject(typeof(D), this.factory)
            };
        }
    }

    class GeneralizedFunction<A, B, C, D, E> : GeneralizedFunction
    {
        private GenericCallBack<A, B, C, D, E> mFunction;

        public GeneralizedFunction(GenericCallBack<A, B, C, D, E> function, GenericCallBackReturn<object, Type> factory)
            : base(factory)
        {
            this.mFunction = function;
        }

        public override void Call(params object[] args)
        {
            this.mFunction((A)args[0], (B)args[1], (C)args[2], (D)args[3], (E)args[4]);
        }

        public override void CallFromMessageWithStatistics(IEnvelope envelope, string layerName)
        {
            string methodName = this.mFunction.Method.Name;
            var args = new object[]
            {
                envelope.Message.GetObjectWithStatistics(typeof(A), this.factory, layerName, methodName),
                envelope.Message.GetObjectWithStatistics(typeof(B), this.factory, layerName, methodName),
                envelope.Message.GetObjectWithStatistics(typeof(C), this.factory, layerName, methodName),
                envelope.Message.GetObjectWithStatistics(typeof(D), this.factory, layerName, methodName),
                envelope.Message.GetObjectWithStatistics(typeof(E), this.factory, layerName, methodName)
            };
            this.TraceCall(envelope, this.mFunction, args);
            this.Call(args);
        }

        public override object[] ReadParameters(IEnvelope envelope)
        {
            return new object[]
            {
                envelope.Message.GetObject(typeof(A), this.factory),
                envelope.Message.GetObject(typeof(B), this.factory),
                envelope.Message.GetObject(typeof(C), this.factory),
                envelope.Message.GetObject(typeof(D), this.factory),
                envelope.Message.GetObject(typeof(E), this.factory)
            };
        }
    }

    class GeneralizedFunction<A, B, C, D, E, F> : GeneralizedFunction
    {
        private GenericCallBack<A, B, C, D, E, F> mFunction;

        public GeneralizedFunction(GenericCallBack<A, B, C, D, E, F> function, GenericCallBackReturn<object, Type> factory)
            : base(factory)
        {
            this.mFunction = function;
        }

        public override void Call(params object[] args)
        {
            this.mFunction((A)args[0], (B)args[1], (C)args[2], (D)args[3], (E)args[4], (F)args[5]);
        }

        public override void CallFromMessageWithStatistics(IEnvelope envelope, string layerName)
        {
            string methodName = this.mFunction.Method.Name;
            var args = new object[]
            {
                envelope.Message.GetObjectWithStatistics(typeof(A), this.factory, layerName, methodName),
                envelope.Message.GetObjectWithStatistics(typeof(B), this.factory, layerName, methodName),
                envelope.Message.GetObjectWithStatistics(typeof(C), this.factory, layerName, methodName),
                envelope.Message.GetObjectWithStatistics(typeof(D), this.factory, layerName, methodName),
                envelope.Message.GetObjectWithStatistics(typeof(E), this.factory, layerName, methodName),
                envelope.Message.GetObjectWithStatistics(typeof(F), this.factory, layerName, methodName)
            };
            this.TraceCall(envelope, this.mFunction, args);
            this.Call(args);
        }

        public override object[] ReadParameters(IEnvelope envelope)
        {
            return new object[]
            {
                envelope.Message.GetObject(typeof(A), this.factory),
                envelope.Message.GetObject(typeof(B), this.factory),
                envelope.Message.GetObject(typeof(C), this.factory),
                envelope.Message.GetObject(typeof(D), this.factory),
                envelope.Message.GetObject(typeof(E), this.factory),
                envelope.Message.GetObject(typeof(F), this.factory)
            };
        }
    }
}
