﻿// -----------------------------------------------------------------------
// <copyright file="QualifiedNameEqualityComparer.cs" company="Scalify">
//     Copyright (c) 2013 Scalify. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace Badumna.Utilities
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// Equality comparer for QualifiedName.
    /// </summary>
    /// <remarks>Required by MonoTouch, Unity IOS for dictionaries whose keys are QualifiedName.</remarks>
    internal class QualifiedNameEqualityComparer : IEqualityComparer<QualifiedName>
    {
        /// <inheritdoc/>
        bool IEqualityComparer<QualifiedName>.Equals(QualifiedName x, QualifiedName y)
        {
            return x.Equals(y);
        }

        /// <inheritdoc/>
        int IEqualityComparer<QualifiedName>.GetHashCode(QualifiedName obj)
        {
            return obj.GetHashCode();
        }
    }
}
