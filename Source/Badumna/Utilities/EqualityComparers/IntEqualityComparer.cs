//-----------------------------------------------------------------------
// <copyright file="IntEqualityComparer.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2012 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.Utilities
{
    using System.Collections.Generic;

    /// <summary>
    /// Equality comparer for integers.
    /// </summary>
    /// <remarks>Required by MonoTouch for dictionaries whose keys are integers.</remarks>
    internal class IntEqualityComparer : IEqualityComparer<int>
    {
        /// <inheritdoc/>
        bool IEqualityComparer<int>.Equals(int x, int y)
        {
            return x == y;
        }

        /// <inheritdoc/>
        int IEqualityComparer<int>.GetHashCode(int obj)
        {
            return obj.GetHashCode();
        }
    }
}

