//-----------------------------------------------------------------------
// <copyright file="ReferenceEqualityComparer.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2012 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.Utilities
{
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    /// <summary>
    /// Generic reference equality comparer.
    /// </summary>
    /// <remarks>Originally introduced so we can comparer instances of Unity objects in the network thread.</remarks>
    /// <typeparam name="T">The type of the objects to do a reference equality comparison on.</typeparam>
    internal class ReferenceEqualityComparer<T> : IEqualityComparer<T> where T : class
    {
        /// <summary>
        /// Initializes static members of the ReferenceEqualityComparer class.
        /// </summary>
        static ReferenceEqualityComparer()
        {
            Instance = new ReferenceEqualityComparer<T>();
        }

        /// <summary>
        /// Gets a static instance of the ReferenceEqualityComparer class.
        /// </summary>
        public static ReferenceEqualityComparer<T> Instance { get; private set; }

        /// <inheritdoc/>
        bool IEqualityComparer<T>.Equals(T x, T y)
        {
            return ReferenceEquals(x, y);
        }

        /// <inheritdoc/>
        int IEqualityComparer<T>.GetHashCode(T obj)
        {
            return RuntimeHelpers.GetHashCode(obj);
        }
    }
}

