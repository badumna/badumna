﻿//---------------------------------------------------------------------------------
// <copyright file="MonoAddressChangedDetector.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

using System.Collections.Generic;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Threading;
using Badumna.Core;
using Badumna.Transport;

namespace Badumna
{
    /// <summary>
    /// The network address changed handler.
    /// </summary>
    internal delegate void NetworkAddressChangedHandler();

    /// <summary>
    /// A detector to monitor network address changes. 
    /// </summary>
    internal class MonoAddressChangedDetector
    {
        /// <summary>
        /// A boolean flag showing whether the address has changed. 
        /// </summary>
        private volatile bool addressChangedFlag;

        /// <summary>
        /// A list of local addresses.
        /// </summary>
        private List<PeerAddress> localPeerAddresses;

        /// <summary>
        /// The network address changed handler.
        /// </summary>
        private NetworkAddressChangedHandler eventHandler;

        /// <summary>
        /// The network event for checking the addresses every second.
        /// </summary>
        private NetworkEvent networkEvent;

        /// <summary>
        /// The queue to use for scheduling events.
        /// </summary>
        private NetworkEventQueue eventQueue;

        /// <summary>
        /// The thread used to check address changed event in the background. This can't be done on the network
        /// event queue because it is quite time consuming. 
        /// </summary>
        private Thread checkThread;

        /// <summary>
        /// Whether the thread is running;
        /// </summary>
        private volatile bool isRunning;

        /// <summary>
        /// Initializes a new instance of the <see cref="MonoAddressChangedDetector"/> class.
        /// </summary>
        /// <param name="handler">The sleep event handler.</param>
        /// <param name="eventQueue">The queue to use for scheduling events.</param>
        public MonoAddressChangedDetector(NetworkAddressChangedHandler handler, NetworkEventQueue eventQueue)
        {
            this.isRunning = false;
            this.addressChangedFlag = false;
            this.eventQueue = eventQueue;
            this.eventHandler = handler;
            this.localPeerAddresses = Badumna.Platform.Net.GetLocalAddresses(0);
        }

        /// <summary>
        /// Starts this instance.
        /// </summary>
        public void Start()
        {
            this.networkEvent = this.eventQueue.Schedule(1000, this.AddressChangedDetectionTask);
            this.isRunning = true;
            this.checkThread = new Thread(this.CheckAddressChanged);
            this.checkThread.Start();
        }

        /// <summary>
        /// Stops this instance.
        /// </summary>
        public void Stop()
        {
            if (this.networkEvent != null)
            {
                this.eventQueue.Remove(this.networkEvent);
            }

            this.isRunning = false;
            this.checkThread.Join(3000);
        }

        /// <summary>
        /// Whether the address has changed.
        /// </summary>
        private void CheckAddressChanged()
        {
            while (this.isRunning)
            {
                List<PeerAddress> currentAddress = Badumna.Platform.Net.GetLocalAddresses(0);

                if (currentAddress.Count != this.localPeerAddresses.Count)
                {
                    this.localPeerAddresses = currentAddress;
                    this.addressChangedFlag = true;
                }

                foreach (PeerAddress address in currentAddress)
                {
                    if (!this.localPeerAddresses.Contains(address))
                    {
                        this.localPeerAddresses = currentAddress;
                        this.addressChangedFlag = true;
                    }
                }

                this.localPeerAddresses = currentAddress;

                Thread.Sleep(1000);
            }
        }

        /// <summary>
        /// Detect whether the address has changed and tries to handle it. 
        /// </summary>
        private void AddressChangedDetectionTask()
        {
            if (this.addressChangedFlag)
            {
                Utilities.Logger.TraceInformation(LogTag.Utilities, "Address Changed detected");

                // address has changed, handle it.
                if (this.eventHandler != null)
                {
                    this.HandleAddressChangedEvent();
                }

                this.addressChangedFlag = false;
            }

            this.networkEvent = this.eventQueue.Schedule(1000, this.AddressChangedDetectionTask);
        }

        /// <summary>
        /// Handles the address changed event.
        /// </summary>
        private void HandleAddressChangedEvent()
        {
            if (this.eventHandler != null)
            {
                this.eventHandler();
            }
        }
    }
}