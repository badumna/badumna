﻿//------------------------------------------------------------------------------
// <copyright file="CommonTask.cs" company="National ICT Australia Limited">
//     Copyright (c) 2012 National ICT Australia Limited. All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using Badumna.Core;
using Badumna.Utilities;

namespace Badumna.Utilities
{
    /// <summary>
    /// Allows multiple clients to schedule execution of a single task with a specified delay.
    /// </summary>
    /// <remarks>
    /// Once scheduled the task will be executed after the specified delay has elapsed.
    /// If the task is scheduled again before it has been executed it will be executed at the new delay, if that occurs before the original delay.
    /// If all schedulers of a task cancel the task because the delay elapses the task will not be run.
    /// </remarks>
    internal class CommonTask
    {
        /// <summary>
        /// The currently scheduled event.
        /// </summary>
        private NetworkEvent scheduledEvent;

        /// <summary>
        /// The event queue.
        /// </summary>
        private NetworkEventQueue eventQueue;

        /// <summary>
        /// The time keeper.
        /// </summary>
        private ITime timeKeeper;

        /// <summary>
        /// The delegate to execute after the delay.
        /// </summary>
        private GenericCallBack action;

        /// <summary>
        /// List of clients that scheduled the task.
        /// </summary>
        /// <remarks>
        /// This is used so that we only cancel the event if *all* activators cancel.
        /// </remarks>
        private List<object> activators = new List<object>();

        /// <summary>
        /// Initializes a new instance of the CommonTask class.
        /// </summary>
        /// <param name="eventQueue">The event queue</param>
        /// <param name="timeKeeper">The time keeper</param>
        /// <param name="action">The delegate to execute</param>
        public CommonTask(NetworkEventQueue eventQueue, ITime timeKeeper, GenericCallBack action)
        {
            this.eventQueue = eventQueue;
            this.timeKeeper = timeKeeper;
            this.action = action;
        }

        /// <summary>
        /// Schedules the action.
        /// </summary>
        /// <remarks>
        /// If an earlier event has already been scheduled then nothing will be done.
        /// Otherwise, a new event will be scheduled and any existing event will be cancelled.
        /// NB: If another client schedules the event earlier, and then cancels, but the event is not cancelled by all other clients, the event will still run at the earlier time.  Ideally this should be fixed.
        /// </remarks>
        /// <param name="delay">The desired delay before executing the action (may be executed earlier if other clients request it)</param>
        /// <param name="activator">
        /// An object representing who is scheduling the event, typically should be the caller.
        /// Used in a reference check when cancel is called to ensure all schedulers cancel before the event is cancelled.
        /// </param>
        public void Schedule(TimeSpan delay, object activator)
        {
            TimeSpan now = this.timeKeeper.Now;

            if (!this.activators.Contains(activator))
            {
                this.activators.Add(activator);
            }

            if (this.scheduledEvent != null && this.scheduledEvent.Time < now + delay)
            {
                return;
            }

            this.CancelEvent();  // Cancel any existing event before we (re)schedule.

            this.scheduledEvent = this.eventQueue.Schedule(
                delay.TotalMilliseconds,
                delegate()
                {
                    this.scheduledEvent = null;
                    this.activators.Clear();
                    this.action();
                });
        }

        /// <summary>
        /// Cancels the pending event for the specified activator.
        /// </summary>
        /// <remarks>
        /// The actual event is only cancelled if all activators cancel it.
        /// Note that Cancel only needs to be called once for a given activator, even if it has called Schedule multiple times.
        /// </remarks>
        /// <param name="activator">The object representing who scheduled the event.  <seealso cref="Schedule"/></param>
        public void Cancel(object activator)
        {
            this.activators.Remove(activator);

            if (this.activators.Count == 0)
            {
                this.CancelEvent();
            }
        }

        /// <summary>
        /// Cancels the event in the event queue.
        /// </summary>
        private void CancelEvent()
        {
            if (this.scheduledEvent != null)
            {
                this.eventQueue.Remove(this.scheduledEvent);
                this.scheduledEvent = null;
            }
        }
    }
}
