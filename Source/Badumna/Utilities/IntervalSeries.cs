﻿//-----------------------------------------------------------------------
// <copyright file="IntervalSeries.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.Utilities
{
    using System;
    using System.Collections.Generic;
    using Badumna.Core;

    /// <summary>
    /// Delegate for creating interval series.
    /// </summary>
    /// <param name="size">The size of the series.</param>
    /// <param name="timeKeeper">Provides access to the current time.</param>
    /// <returns>A new interval series.</returns>
    internal delegate IIntervalSeries IntervalSeriesFactory(int size, ITime timeKeeper);

    /// <summary>
    /// Interface for the DataSeries class.
    /// </summary>
    internal interface IIntervalSeries
    {
        /// <summary>
        /// Gets a count of the intervals in the series.
        /// </summary>
        int Count { get; }

        /// <summary>
        /// Gets the mean of the interval series.
        /// </summary>
        double Mean { get; }

        /// <summary>
        /// Gets the standard deviation of the interval series.
        /// </summary>
        double StdDev { get; }

        /// <summary>
        /// Gets the most recent time point recorded.
        /// </summary>
        TimeSpan LastTimePoint { get; }

        /// <summary>
        /// Gets a value indicating whether the series has been started.
        /// </summary>
        bool IsStarted { get; }

        /// <summary>
        /// Start the interval series from this moment.
        /// </summary>
        void Start();

        /// <summary>
        /// Add an interval to the series.
        /// </summary>
        void Add();

        /// <summary>
        /// Gets a time span representing the time since the series was started.
        /// </summary>
        /// <returns>The time since the series was started.</returns>
        TimeSpan GetTimeSinceStart();

        /// <summary>
        /// Resets the series to its initial empty state.
        /// </summary>
        void Reset();
    }

    /// <summary>
    /// Holds a series of sequential intervals defined as the time intervals between fixed time points.
    /// The series has a fixed maximum size, which when exceeded, will cause oldest points to be replaced.
    /// </summary>
    internal class IntervalSeries : IIntervalSeries
    {
        /// <summary>
        /// The number of intervals to store in the series.
        /// </summary>
        private readonly int size;

        /// <summary>
        /// Factory for creating data series.
        /// </summary>
        private readonly DataSeriesFactory dataSeriesFactory;

        /// <summary>
        /// Provides access to the current time.
        /// </summary>
        private readonly ITime timeKeeper;

        /// <summary>
        /// Holds the series of intervals and provides statistical measures.
        /// </summary>
        private IDataSeries dataSeries;

        /// <summary>
        /// A value indicating whether the interval series has begun.
        /// </summary>
        private bool started;

        /// <summary>
        /// The time the series was started.
        /// </summary>
        private TimeSpan startTime;

        /// <summary>
        /// A count of the number of intervals recorded.
        /// </summary>
        private int count;

        /// <summary>
        /// Initializes a new instance of the IntervalSeries class.
        /// </summary>
        /// <param name="size">The size of the series.</param>
        /// <param name="timeKeeper">Provides access to the current time.</param>
        public IntervalSeries(int size, ITime timeKeeper)
            : this(size, timeKeeper, s => new DataSeries(s))
        {
        }

        /// <summary>
        /// Initializes a new instance of the IntervalSeries class.
        /// </summary>
        /// <param name="size">The size of the series.</param>
        /// <param name="timeKeeper">Provides access to the current time.</param>
        /// <param name="dataSeriesFactory">Factory for creating data series.</param>
        public IntervalSeries(int size, ITime timeKeeper, DataSeriesFactory dataSeriesFactory)
        {
            if (timeKeeper == null)
            {
                throw new ArgumentNullException("timeKeeper");
            }

            if (dataSeriesFactory == null)
            {
                throw new ArgumentNullException("dataSeriesFactor");
            }

            this.size = size;
            this.timeKeeper = timeKeeper;
            this.dataSeriesFactory = dataSeriesFactory;

            this.Reset();
        }

        /// <inheritdoc/>
        public int Count
        {
            get { return this.count; }
        }

        /// <inheritdoc/>
        public double Mean
        {
            get { return this.dataSeries.Mean; }
        }

        /// <inheritdoc/>
        public double StdDev
        {
            get { return this.dataSeries.StdDev; }
        }

        /// <inheritdoc/>
        public TimeSpan LastTimePoint
        {
            get;
            private set;
        }

        /// <inheritdoc/>
        public bool IsStarted
        {
            get { return this.started; }
        }

        /// <inheritdoc/>
        public void Start()
        {
            this.startTime = this.timeKeeper.Now;
            this.LastTimePoint = this.startTime;
            this.started = true;
        }

        /// <inheritdoc/>
        public TimeSpan GetTimeSinceStart()
        {
            if (this.IsStarted)
            {
                return this.startTime;
            }
            else
            {
                return TimeSpan.FromTicks(0);
            }
        }

        /// <inheritdoc/>
        public void Add()
        {
            if (!this.started)
            {
                throw new InvalidOperationException("Must start interval series before adding interval.");
            }

            if (this.count < int.MaxValue)
            {
                this.count++;
            }

            TimeSpan now = this.timeKeeper.Now;
            double milliseconds = (now - this.LastTimePoint).TotalMilliseconds;
            this.LastTimePoint = now;

            this.dataSeries.Add(milliseconds);
        }

        /// <inheritdoc/>
        public void Reset()
        {
            this.started = false;
            this.count = 0;
            this.dataSeries = this.dataSeriesFactory.Invoke(this.size);
        }
    }
}
