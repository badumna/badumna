﻿using System;
using System.Collections.Generic;
using System.Text;

using Badumna.Core;

namespace Badumna.Utilities
{
    /// <summary>
    /// A list with fewer than 255 bytes. 
    /// </summary>
    /// <exclude/>
    class ShortList<T> : List<T>, IParseable
    {
        #region IParseable Members

        public void ToMessage(MessageBuffer message, Type parameterType)
        {
            if (message != null)
            {
                System.Diagnostics.Debug.Assert(this.Count < byte.MaxValue, "Short list is too long to parse.");
                message.Write((byte)this.Count);
                foreach (T item in this)
                {
                    message.PutObject(item, typeof(T));
                }
            }
        }

        public void FromMessage(MessageBuffer message)
        {
            if (message != null)
            {
                int length = (int)message.ReadByte();

                for (int i = 0; i < length; i++)
                {
                    this.Add((T)message.GetObject(typeof(T), f => { return Activator.CreateInstance(typeof(T), true); }));
                }
            }
        }

        #endregion
    }
}
