﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

using Badumna.Core;

namespace Badumna.Utilities
{
    class AcknowledgementSet : ICollection<CyclicalID.UShortID>, IEnumerable<CyclicalID.UShortID>, IParseable
    {
        private CyclicalID.UShortID mMinimum;
        private CyclicalID.UShortID mMaximmum;
        private InsertionSortedList<CyclicalID.UShortID> mExceptions;
        private int mCount;

        public int Count { get { return this.mCount; } }
        public int NumberMissing { get { return this.mExceptions.Count; } }

        public bool IsReadOnly { get { return false; } }

        public CyclicalID.UShortID Maximum { get { return this.mMaximmum; } }
        public CyclicalID.UShortID Minimum { get { return this.mMinimum; } }
        internal InsertionSortedList<CyclicalID.UShortID> Exceptions { get { return this.mExceptions; } }

        public AcknowledgementSet()
        {
            this.mMinimum = new CyclicalID.UShortID(0);
            this.mMaximmum = new CyclicalID.UShortID(0);
            this.mExceptions = new InsertionSortedList<CyclicalID.UShortID>();
        }

        public int ParseLengthBytes { get { return 2 + Math.Min(this.mCount, 2) * 2 + this.mExceptions.Count * 2; } }

        public void ClipToFirstMissing()
        {
            if (this.mExceptions.Count > 0)
            {
                CyclicalID.UShortID firstMissing = this.mExceptions[0];

                System.Diagnostics.Debug.Assert(firstMissing > this.mMinimum, "First missing ack is not greater than the minimum.");
                while (this.mMinimum < firstMissing.Previous)
                {
                    this.mCount--;
                    this.mMinimum.Increment();
                    System.Diagnostics.Debug.Assert(this.mCount >= 0, "Inconsistant count for ackknowledgement set.");
                }
                /*
#if DEBUG // TODO : Remove this check if its working
                foreach (CyclicalID.UShortID missing in this.mExceptions)
                {
                    if (!(missing > this.mMinimum && missing < this.mMaximmum))
                    {
                        System.Diagnostics.Debug.Fail("Missing acknowledgement is outside range.");
                    }
                }
#endif */
            }
            else if (this.mCount > 0)
            {
                this.mMinimum = this.mMaximmum;
                this.mCount = 1;
            }
        }

        #region ICollection<UShortID> Members

        public void Add(CyclicalID.UShortID item)
        {
            if (this.mCount == 0)
            {
                this.mMinimum = item;
                this.mMaximmum = item;


                this.mCount = 1;
                return;
            }

            if (item < this.mMinimum)
            {                
                // Change the minimum to the new item and add all intermediate values to the exceptions list
                CyclicalID.UShortID ack = item;

                ack.Increment();
                for (; ack < this.mMinimum; ack.Increment())
                {
                    this.mExceptions.Add(ack);
                }

                this.mMinimum = item;
                this.mCount++;
            }
            else if (item > this.mMaximmum)
            {
                // Change the maximum to the new item and add all intermediate values to the exceptions list
                CyclicalID.UShortID ack = this.mMaximmum;

                ack.Increment();
                for (; ack < item; ack.Increment())
                {
                    this.mExceptions.Add(ack);
                }

                this.mMaximmum = item;
                this.mCount++;
            }
            else
            {
                if (this.mExceptions.Contains(item))
                {
                    this.mExceptions.Remove(item);
                    this.mCount++;
                }
            }

            System.Diagnostics.Debug.Assert(this.mCount < ushort.MaxValue / 2, "Too many acknowledgements in a single set.");
        }

        public void Clear()
        {
            this.mExceptions.Clear();
            this.mCount = 0;
        }

        public bool Contains(CyclicalID.UShortID item)
        {
            if (this.mCount == 0)
            {
                return false;
            }

            return this.mMinimum <= item && this.mMaximmum >= item && !this.mExceptions.Contains(item);
        }

        public void CopyTo(CyclicalID.UShortID[] array, int arrayIndex)
        {
            foreach (CyclicalID.UShortID num in this)
            {
                array[arrayIndex++] = num;
            }
        }

        public bool Remove(CyclicalID.UShortID item)
        {
            if (this.Count == 0)
            {
                return false;
            }

            if (this.Count == 1)
            {
                if (this.mMinimum == item)
                {
                    this.Clear();
                    return true;
                }

                return false;
            }

            if (item == this.mMinimum)
            {
                CyclicalID.UShortID nextAck = this.mMinimum;

                do
                {
                    nextAck.Increment();
                }
                while (nextAck < this.mMaximmum && this.mExceptions.Contains(nextAck));

                this.mMinimum = nextAck;
                this.mCount--;
                return true;
            }
            else if (item == this.mMaximmum)
            {
                CyclicalID.UShortID nextAck = this.mMaximmum;

                do
                {
                    nextAck = nextAck.Previous;
                }
                while (nextAck > this.mMinimum && this.mExceptions.Contains(nextAck));

                this.mMaximmum = nextAck;
                this.mCount--;
                return true;
            } 
            else
            {
                if (item > this.mMinimum && item < this.mMaximmum)
                {
                    if (!this.mExceptions.Contains(item))
                    {
                        this.mExceptions.Add(item);
                        this.mCount--;
                        return true;
                    }
                }
            }

            return false;
        }

        #endregion

        #region IEnumerable<UShortID> Members

        public IEnumerator<CyclicalID.UShortID> GetEnumerator()
        {
            return new Enumerator(this);
        }

        class Enumerator : IEnumerator<CyclicalID.UShortID>
        {
            private AcknowledgementSet acknowledgementSet;
            private CyclicalID.UShortID iterator;
            private CyclicalID.UShortID current;
            private bool isValid;

            public Enumerator(AcknowledgementSet acknowledgementSet)
            {
                this.acknowledgementSet = acknowledgementSet;
                this.iterator = this.acknowledgementSet.Minimum;
                this.isValid = false;
            }

            #region IEnumerator Members

            object IEnumerator.Current
            {
                get
                {
                    if (this.isValid)
                    {
                        return this.current;
                    }
                    else
                    {
                        throw new InvalidOperationException();
                    }
                }
            }

            public bool MoveNext()
            {
                if (this.acknowledgementSet.Count <= 0)
                {
                    this.isValid = false;
                    return false;
                }

                while (this.iterator <= this.acknowledgementSet.Maximum)
                {
                    if (!this.acknowledgementSet.Exceptions.Contains(iterator))
                    {
                        this.current = this.iterator;
                        this.iterator.Increment();
                        this.isValid = true;
                        return true;
                    }

                    this.iterator.Increment();
                }

                this.isValid = false;
                return false;
            }

            public void Reset()
            {
                throw new NotSupportedException();
            }

            #endregion

            #region IEnumerator<UShortID> Members

            public CyclicalID.UShortID Current
            {
                get 
                {
                    if (this.isValid)
                    {
                        return this.current;
                    }
                    else
                    {
                        throw new InvalidOperationException();
                    }
                }
            }

            #endregion

            #region IDisposable Members

            public void Dispose()
            {
            }

            #endregion
        }
        #endregion

        #region IEnumerable Members

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        #endregion

        #region IParseable Members

        // Remember to change ParseLengthBytes if this is changed.
        /*public void ToMessage(MessageBuffer message, Type parameterType)
        {
            if (message != null)
            {
                message.Write((ushort)this.mCount);

                if (this.mCount > 0)
                {
                    this.mMinimum.ToMessage(message, typeof(CyclicalID.UShortID));
                }
                
                if (this.mCount > 1)
                {
                    this.mMaximmum.ToMessage(message, typeof(CyclicalID.UShortID));

                    message.Write(this.mExceptions.Count);
                    foreach (CyclicalID.UShortID ack in this.mExceptions)
                    {
                        ack.ToMessage(message, typeof(CyclicalID.UShortID));
                    }
                }
            }
        }

        public void FromMessage(MessageBuffer message)
        {
            if (message != null)
            {
                this.mCount = (int)message.ReadUShort();

                if (this.mCount > 0)
                {
                    this.mMinimum = message.Read<CyclicalID.UShortID>();

                    if (this.mCount > 1)
                    {
                        this.mMaximmum = message.Read<CyclicalID.UShortID>();

                        int count = message.ReadInt();
                        for (int i = 0; i < count; i++)
                        {
                            this.mExceptions.Add(message.Read<CyclicalID.UShortID>());
                        }
                    }
                    else
                    {
                        this.mMaximmum = this.mMinimum;
                    }
                }
            }
        }*/

        public void ToMessage(MessageBuffer message, Type parameterType)
        {
            if (message != null)
            {
                message.Write((ushort)this.mCount);
                this.mMinimum.ToMessage(message, typeof(CyclicalID.UShortID));
                this.mMaximmum.ToMessage(message, typeof(CyclicalID.UShortID));
                
                message.Write(this.mExceptions.Count);
                foreach (CyclicalID.UShortID ack in this.mExceptions)
                {
                    ack.ToMessage(message, typeof(CyclicalID.UShortID));
                }
            }
        }

        public void FromMessage(MessageBuffer message)
        {
            if (message != null)
            {
                this.mCount = (int)message.ReadUShort();
                this.mMinimum = message.Read<CyclicalID.UShortID>();
                this.mMaximmum = message.Read<CyclicalID.UShortID>();
                int count = message.ReadInt();
                for (int i = 0; i < count; i++)
                {
                    this.mExceptions.Add(message.Read<CyclicalID.UShortID>());
                }
            }
        }

        public override string ToString()
        {
            CyclicalID.UShortID min = this.Minimum;
            CyclicalID.UShortID max = this.Maximum;
            int count = this.mCount;
            int exceptionCount = this.mExceptions.Count;

            return string.Format("min : {0}, max: {1}, count: {2}, exceptionCount: {3}", min, max, count, exceptionCount);

            /*List<string> ids = new List<CyclicalID.UShortID>(this).ConvertAll<string>(
                delegate(CyclicalID.UShortID id) { return id.ToString(); });
            return details + ", Ack: " + String.Join(", ", ids.ToArray());*/
        }

        #endregion
    }
}
