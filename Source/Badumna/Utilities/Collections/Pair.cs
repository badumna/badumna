﻿using System;

namespace Badumna.Utilities
{
    class Pair<T, U>
    {
        private T mFirst;
        private U mSecond;

        public T First 
        {
            get { return this.mFirst; }
            set
            {
                if (value == null)
                {
                    throw new ArgumentException("argument can't be null in pair.");
                }

                this.mFirst = value;
            }
        }
        public U Second 
        {
            get { return this.mSecond; }
            set
            {
                if (value == null)
                {
                    throw new ArgumentException("argument can't be null in pair.");
                }

                this.mSecond = value;
            }
        }

        public Pair(T first, U second)
        {
            if (first == null || second == null)
            {
                throw new ArgumentException("argument can't be null in pair.");
            }

            this.First = first;
            this.Second = second;
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }

            if (!(obj is Pair<T, U>))
            {
                return false;
            }

            Pair<T, U> other = obj as Pair<T, U>;

            return this.First.Equals(other.First) && this.Second.Equals(other.Second);
        }

        public override int GetHashCode()
        {
            return this.First.GetHashCode() ^ this.Second.GetHashCode();
        }

        public override string ToString()
        {
            return String.Format("<#Pair:({0}, {1})>", this.First, this.Second);
        }
    }
}