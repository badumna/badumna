﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Badumna.Utilities
{
    /// <summary>
    /// Is most efficient when the items are inserted in order.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <exclude/>
    internal class InsertionSortedList<T> : IList<T> where T : IComparable<T>
    {
        private List<T> list;

        public InsertionSortedList()
        {
            this.list = new List<T>();
        }

        public InsertionSortedList(int capacity)
        {
            this.list = new List<T>(capacity);
        }

        public void Add(T item)
        {
            if (this.Count == 0)
            {
                this.list.Add(item);
            }
            else
            {
                int i = this.Count-1;
                if (this[i].CompareTo(item) < 0)
                {
                    this.list.Add(item);
                    return;
                }

                int min = 0;
                int max = i;

                while (min != max)
                {
                    int partition = min + (max - min) / 2;

                    if (this[partition].CompareTo(item) == 0)
                    {
                        min = max = partition;
                    }
                    else if (this[partition].CompareTo(item) < 0)
                    {
                        min = partition + 1;
                    }
                    else
                    {
                        max = partition;
                    }
                }

                this.list.Insert(min, item);
            }
        }

        #region IList<T> Members

        public int IndexOf(T item)
        {
            return this.list.IndexOf(item);
        }

        public void Insert(int index, T item)
        {
            this.list.Insert(index, item);
        }

        public void RemoveAt(int index)
        {
            this.list.RemoveAt(index);
        }

        public T this[int index]
        {
            get
            {
                return this.list[index];
            }
            set
            {
                this.list[index] = value;
            }
        }

        #endregion

        #region ICollection<T> Members


        public void Clear()
        {
            this.list.Clear();
        }

        public bool Contains(T item)
        {
            return this.list.Contains(item);
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            this.list.CopyTo(array, arrayIndex);
        }

        public int Count
        {
            get { return this.list.Count; }
        }

        public bool IsReadOnly
        {
            get { throw new NotImplementedException(); }
        }

        public bool Remove(T item)
        {
            return this.list.Remove(item);
        }

        #endregion

        #region IEnumerable<T> Members

        public IEnumerator<T> GetEnumerator()
        {
            return this.list.GetEnumerator();
        }

        #endregion

        #region IEnumerable Members

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return this.list.GetEnumerator();
        }

        #endregion
    }
}
