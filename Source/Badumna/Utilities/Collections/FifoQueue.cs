﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Badumna.Utilities
{
    class FifoQueue<T>
    {
        private T[] mItemArray;
        private int mInsertIndex;
        private int mRemoveIndex;

        // Insert lock takes precedence. That is, the the remove lock may be locked while the 
        // insert is locked but not the other way around.
        private object mInsertLock = new object();
        private object mRemoveLock = new object();

        public int Count
        {
            get
            {
                lock (this.mInsertLock)
                {
                    lock (this.mRemoveLock)
                    {
                        int distance = this.mInsertIndex - this.mRemoveIndex;

                        if (distance < 0)
                        {
                            return this.mItemArray.Length + distance;
                        }

                        return distance;
                    }
                }
            }
        }

        public FifoQueue(int capacity)
        {
            this.mItemArray = new T[capacity + 1]; // Add one because we can only store capacity - 1 items
            this.mInsertIndex = this.mRemoveIndex = 0;
        }

        public void Clear()
        {
            this.mInsertIndex = this.mRemoveIndex = 0;
        }

        public void Push(T item)
        {
            lock (this.mInsertLock)
            {
                this.mItemArray[this.mInsertIndex] = item;

                int nextIndex = (this.mInsertIndex + 1) % this.mItemArray.Length;
                if (nextIndex == this.mRemoveIndex)
                {
                    // Discard the next item to make room for future additions.
                    lock (this.mRemoveLock)
                    {
                        this.mRemoveIndex = (this.mRemoveIndex + 1) % this.mItemArray.Length;
                    }
                }

                this.mInsertIndex = nextIndex;
            }
        }

        public T Pop()
        {
            if (this.Count == 0)
            {
                return default(T);
            }

            lock (this.mRemoveLock)
            {
                int nextRemoveIndex = (this.mRemoveIndex + 1) % this.mItemArray.Length;

                T item = this.mItemArray[this.mRemoveIndex];
                this.mItemArray[this.mRemoveIndex] = default(T);

                this.mRemoveIndex = nextRemoveIndex; // Index is incremented after item has been taken from array to ensure thread safe.
                return item;
            }
        }

        public T Peek()
        {
            if (this.Count == 0)
            {
                return default(T);
            }

            lock (this.mRemoveLock)
            {
                return this.mItemArray[this.mRemoveIndex];
            }
        }

        public bool Contains(T obj)
        {
            lock (this.mRemoveLock)
            {
                if (this.Count <= 0)
                {
                    return false;
                }

                for (int i = this.mRemoveIndex; i != this.mInsertIndex; i = (i + 1) % this.mItemArray.Length)
                {
                    if (this.mItemArray[i].Equals(obj))
                    {
                        return true;
                    }
                }

                return false;
            }
        }
    }
}
