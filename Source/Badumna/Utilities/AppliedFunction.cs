﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Badumna.Utilities
{
#if DEBUG
    public  // required for simulator
#endif
    interface IInvokable
    {
        void Invoke();
    }

    static class Apply
    {
        static public AppliedFunction Func(GenericCallBack function)
        {
            return new AppliedFunction(function);
        }

        static public AppliedFunction<A> Func<A>(GenericCallBack<A> function, A paramA)
        {
            return new AppliedFunction<A>(function, paramA);
        }

        static public AppliedFunction<A, B> Func<A, B>(GenericCallBack<A, B> function, A paramA, B paramB)
        {
            return new AppliedFunction<A, B>(function, paramA, paramB);
        }

        static public AppliedFunction<A, B, C> Func<A, B, C>(GenericCallBack<A, B, C> function, A paramA, B paramB, C paramC)
        {
            return new AppliedFunction<A, B, C>(function, paramA, paramB, paramC);
        }

        static public AppliedFunction<A, B, C, D> Func<A, B, C, D>(GenericCallBack<A, B, C, D> function, A paramA, B paramB, C paramC, D paramD)
        {
            return new AppliedFunction<A, B, C, D>(function, paramA, paramB, paramC, paramD);
        }

        static public AppliedFunction<A, B, C, D, E> Func<A, B, C, D, E>(GenericCallBack<A, B, C, D, E> function, A paramA, B paramB, C paramC, D paramD, E paramE)
        {
            return new AppliedFunction<A, B, C, D, E>(function, paramA, paramB, paramC, paramD, paramE);
        }

        static public AppliedFunction<A, B, C, D, E, F> Func<A, B, C, D, E, F>(GenericCallBack<A, B, C, D, E, F> function, A paramA, B paramB, C paramC, D paramD, E paramE, F paramF)
        {
            return new AppliedFunction<A, B, C, D, E, F>(function, paramA, paramB, paramC, paramD, paramE, paramF);
        }


        static public string InvocationToString(Delegate handler, params object[] args)
        {
            string name = string.Empty;
#if TRACE
            try
            {
                if (handler != null)
                {
                    if (handler.Target is RegularTask || handler.Target is OneShotTask)
                    {
                        name = handler.Target.ToString();
                    }
                    else
                    {
                        if (handler.Target != null)
                        {
                            name = handler.Target.GetType().ToString() + ".";
                        }
                        name += handler.Method.Name;

                        if (args != null && args.Length > 0)
                        {
                            string seperator = "(";
                            foreach (object arg in args)
                            {
                                string argString = "<null>";
                                if (arg != null)
                                {
                                    argString = arg.ToString();
                                }
                                name += seperator + argString;
                                seperator = ",";
                            }
                            name += ")";
                        }
                        else
                        {
                            name += "()";
                        }
                    }
                }
            }
            catch
            {
                name = "";
            }
#endif
            return name;
        }
    }

    class AppliedFunction : IInvokable
    {
        private GenericCallBack mFunction;

        public AppliedFunction(GenericCallBack function)
        {
            this.mFunction = function;
        }

        public void Invoke()
        {
            this.mFunction();
        }

        public override string ToString()
        {
            return Apply.InvocationToString(this.mFunction);
        }
    }

    class AppliedFunction<A> : IInvokable
    {
        private GenericCallBack<A> mFunction;
        private A mA;

        public AppliedFunction(GenericCallBack<A> function, A paramA)
        {
            this.mFunction = function;
            this.mA = paramA;
        }

        public void Invoke()
        {
            this.mFunction(this.mA);
        }

        public override string ToString()
        {
            return Apply.InvocationToString(this.mFunction, this.mA);
        }
    }

    class AppliedFunction<A, B> : IInvokable
    {
        private GenericCallBack<A, B> mFunction;
        private A mA;
        private B mB;

        public AppliedFunction(GenericCallBack<A, B> function, A paramA, B paramB)
        {
            this.mFunction = function;
            this.mA = paramA;
            this.mB = paramB;
        }

        public void Invoke()
        {
            this.mFunction(this.mA, this.mB);
        }

        public override string ToString()
        {
            return Apply.InvocationToString(this.mFunction, this.mA, this.mB);
        }
    }

    class AppliedFunction<A, B, C> : IInvokable
    {
        private GenericCallBack<A, B, C> mFunction;
        private A mA;
        private B mB;
        private C mC;

        public AppliedFunction(GenericCallBack<A, B, C> function, A paramA, B paramB, C paramC)
        {
            this.mFunction = function;
            this.mA = paramA;
            this.mB = paramB;
            this.mC = paramC;
        }

        public void Invoke()
        {
            this.mFunction(this.mA, this.mB, this.mC);
        }

        public override string ToString()
        {
            return Apply.InvocationToString(this.mFunction, this.mA, this.mB, this.mC);
        }
    }

    class AppliedFunction<A, B, C, D> : IInvokable
    {
        private GenericCallBack<A, B, C, D> mFunction;
        private A mA;
        private B mB;
        private C mC;
        private D mD;

        public AppliedFunction(GenericCallBack<A, B, C, D> function, A paramA, B paramB, C paramC, D paramD)
        {
            this.mFunction = function;
            this.mA = paramA;
            this.mB = paramB;
            this.mC = paramC;
            this.mD = paramD;
        }

        public void Invoke()
        {
            this.mFunction(this.mA, this.mB, this.mC, this.mD);
        }

        public override string ToString()
        {
            return Apply.InvocationToString(this.mFunction, this.mA, this.mB, this.mC, this.mD);
        }
    }

    class AppliedFunction<A, B, C, D, E> : IInvokable
    {
        private GenericCallBack<A, B, C, D, E> mFunction;
        private A mA;
        private B mB;
        private C mC;
        private D mD;
        private E mE;

        public AppliedFunction(GenericCallBack<A, B, C, D, E> function, A paramA, B paramB, C paramC, D paramD, E paramE)
        {
            this.mFunction = function;
            this.mA = paramA;
            this.mB = paramB;
            this.mC = paramC;
            this.mD = paramD;
            this.mE = paramE;
        }

        public void Invoke()
        {
            this.mFunction(this.mA, this.mB, this.mC, this.mD, this.mE);
        }

        public override string ToString()
        {
            return Apply.InvocationToString(this.mFunction, this.mA, this.mB, this.mC, this.mD, this.mE);
        }
    }

    class AppliedFunction<A, B, C, D, E, F> : IInvokable
    {
        private GenericCallBack<A, B, C, D, E, F> mFunction;
        private A mA;
        private B mB;
        private C mC;
        private D mD;
        private E mE;
        private F mF;

        public AppliedFunction(GenericCallBack<A, B, C, D, E, F> function, A paramA, B paramB, C paramC, D paramD, E paramE, F paramF)
        {
            this.mFunction = function;
            this.mA = paramA;
            this.mB = paramB;
            this.mC = paramC;
            this.mD = paramD;
            this.mE = paramE;
            this.mF = paramF;
        }

        public void Invoke()
        {
            this.mFunction(this.mA, this.mB, this.mC, this.mD, this.mE, this.mF);
        }

        public override string ToString()
        {
            return Apply.InvocationToString(this.mFunction, this.mA, this.mB, this.mC, this.mD, this.mE, this.mF);
        }
    }

    class AppliedFunctionReturn<R> 
    {
        private GenericCallBackReturn<R> mFunction;

        public AppliedFunctionReturn(GenericCallBackReturn<R> function)
        {
            this.mFunction = function;
        }

        public R Invoke()
        {
            return this.mFunction();
        }

        public override string ToString()
        {
            return Apply.InvocationToString(this.mFunction);
        }
    }

    class AppliedFunctionReturn<R, A>
    {
        private GenericCallBackReturn<R, A> mFunction;
        private A mA;

        public AppliedFunctionReturn(GenericCallBackReturn<R, A> function, A paramA)
        {
            this.mFunction = function;
            this.mA = paramA;
        }

        public R Invoke()
        {
            return this.mFunction(this.mA);
        }

        public override string ToString()
        {
            return Apply.InvocationToString(this.mFunction, this.mA);
        }
    }

    class AppliedFunctionReturn<R, A, B> 
    {
        private GenericCallBackReturn<R, A, B> mFunction;
        private A mA;
        private B mB;

        public AppliedFunctionReturn(GenericCallBackReturn<R, A, B> function, A paramA, B paramB)
        {
            this.mFunction = function;
            this.mA = paramA;
            this.mB = paramB;
        }

        public R Invoke()
        {
            return this.mFunction(this.mA, this.mB);
        }

        public override string ToString()
        {
            return Apply.InvocationToString(this.mFunction, this.mA, this.mB);
        }
    }

    class AppliedFunctionReturn<R, A, B, C> 
    {
        private GenericCallBackReturn<R, A, B, C> mFunction;
        private A mA;
        private B mB;
        private C mC;

        public AppliedFunctionReturn(GenericCallBackReturn<R, A, B, C> function, A paramA, B paramB, C paramC)
        {
            this.mFunction = function;
            this.mA = paramA;
            this.mB = paramB;
            this.mC = paramC;
        }

        public R Invoke()
        {
            return this.mFunction(this.mA, this.mB, this.mC);
        }

        public override string ToString()
        {
            return Apply.InvocationToString(this.mFunction, this.mA, this.mB, this.mC);
        }
    }

    class AppliedFunctionReturn<R, A, B, C, D> 
    {
        private GenericCallBackReturn<R, A, B, C, D> mFunction;
        private A mA;
        private B mB;
        private C mC;
        private D mD;

        public AppliedFunctionReturn(GenericCallBackReturn<R, A, B, C, D> function, A paramA, B paramB, C paramC, D paramD)
        {
            this.mFunction = function;
            this.mA = paramA;
            this.mB = paramB;
            this.mC = paramC;
            this.mD = paramD;
        }

        public R Invoke()
        {
            return this.mFunction(this.mA, this.mB, this.mC, this.mD);
        }

        public override string ToString()
        {
            return Apply.InvocationToString(this.mFunction, this.mA, this.mB, this.mC, this.mD);
        }
    }

    class AppliedFunctionReturn<R, A, B, C, D, E> 
    {
        private GenericCallBackReturn<R, A, B, C, D, E> mFunction;
        private A mA;
        private B mB;
        private C mC;
        private D mD;
        private E mE;

        public AppliedFunctionReturn(GenericCallBackReturn<R, A, B, C, D, E> function, A paramA, B paramB, C paramC, D paramD, E paramE)
        {
            this.mFunction = function;
            this.mA = paramA;
            this.mB = paramB;
            this.mC = paramC;
            this.mD = paramD;
            this.mE = paramE;
        }

        public R Invoke()
        {
            return this.mFunction(this.mA, this.mB, this.mC, this.mD, this.mE);
        }

        public R Invoke(E paramE)
        {
            return this.mFunction(this.mA, this.mB, this.mC, this.mD, paramE);
        }

        public override string ToString()
        {
            return Apply.InvocationToString(this.mFunction, this.mA, this.mB, this.mC, this.mD, this.mE);
        }
    }

    class AppliedFunctionReturn<R, A, B, C, D, E, F> 
    {
        private GenericCallBackReturn<R, A, B, C, D, E, F> mFunction;
        private A mA;
        private B mB;
        private C mC;
        private D mD;
        private E mE;
        private F mF;

        public AppliedFunctionReturn(GenericCallBackReturn<R, A, B, C, D, E, F> function, A paramA, B paramB, C paramC, D paramD, E paramE, F paramF)
        {
            this.mFunction = function;
            this.mA = paramA;
            this.mB = paramB;
            this.mC = paramC;
            this.mD = paramD;
            this.mE = paramE;
            this.mF = paramF;
        }

        public R Invoke()
        {
            return this.mFunction(this.mA, this.mB, this.mC, this.mD, this.mE, this.mF);
        }

        public override string ToString()
        {
            return Apply.InvocationToString(this.mFunction, this.mA, this.mB, this.mC, this.mD, this.mE, this.mF);
        }
    }
}
