﻿using System;
using System.Collections.Generic;
using System.Text;

using Badumna.Core;

namespace Badumna.Utilities
{
    /// <summary>
    /// Smooths a value like Smoother, but the smoothing rate is decoupled from the sample rate.
    /// Smoother will converge faster if the sample rate is higher, whereas TimeIndependentSmoother
    /// converges at a constant rate regardless of the sample rate.
    /// </summary>
    class TimeIndependentSmoother
    {
        public double EstimatedValue { get { return this.mSmoothedMeanEstimator; } }
        

        private double mSmoothedMeanEstimator;
        private double mSmoothingFactor;
        private TimeSpan mLastSampleTime = TimeSpan.Zero;

        /// <summary>
        /// Provides access to the current time.
        /// </summary>
        private ITime timeKeeper;

        public TimeIndependentSmoother(TimeSpan smoothingPeriod, ITime timeKeeper)
        {
            this.timeKeeper = timeKeeper;
            this.mSmoothingFactor = Math.Exp(-1 / smoothingPeriod.TotalSeconds);
        }

        public virtual void ValueEvent(double value)
        {
            TimeSpan now = this.timeKeeper.Now;

            if (this.mLastSampleTime == TimeSpan.Zero)
            {
                this.mSmoothedMeanEstimator = value;
            }
            else
            {
                double elapsedSeconds = (now - this.mLastSampleTime).TotalSeconds;
                double factor = Math.Pow(this.mSmoothingFactor, elapsedSeconds);
                this.mSmoothedMeanEstimator *= factor;
                this.mSmoothedMeanEstimator += value * (1 - factor);
            }

            this.mLastSampleTime = now;
        }
    }
}
