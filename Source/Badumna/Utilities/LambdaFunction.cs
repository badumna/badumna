﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Badumna.Utilities
{
    class LambdaFunction
    {
        private Delegate mDelegate;
        private object[] mArguments;

        public object this[int index]
        {
            get { return this.mArguments[index]; }
            set { this.mArguments[index] = value; }
        }

        public LambdaFunction(Delegate function)
        {
            this.mDelegate = function;
            this.mArguments = new object[this.mDelegate.Method.GetParameters().Length];
        }

        public LambdaFunction(Delegate function, params object[] args)
        {
            this.mDelegate = function;
            this.mArguments = new object[this.mDelegate.Method.GetParameters().Length];
            args.CopyTo(this.mArguments, 0);
        }

        public void InvokeNoReturn()
        {
            this.mDelegate.Method.Invoke(this.mDelegate.Target, this.mArguments);
        }

        public object Invoke()
        {
            return this.mDelegate.Method.Invoke(this.mDelegate.Target, this.mArguments);
        }

        public object Invoke(object A)
        {
            this.mArguments[this.mArguments.Length - 1] = A;
            return this.mDelegate.Method.Invoke(this.mDelegate.Target, this.mArguments);
        }

        public object Invoke(object A, object B)
        {
            this.mArguments[this.mArguments.Length - 2] = A;
            this.mArguments[this.mArguments.Length - 1] = B;
            return this.mDelegate.Method.Invoke(this.mDelegate.Target, this.mArguments);
        }

        public object Invoke(object A, object B, object C)
        {
            this.mArguments[this.mArguments.Length - 3] = A;
            this.mArguments[this.mArguments.Length - 2] = B;
            this.mArguments[this.mArguments.Length - 1] = C;
            return this.mDelegate.Method.Invoke(this.mDelegate.Target, this.mArguments);
        }

        public object Invoke(object A, object B, object C, object D)
        {
            this.mArguments[this.mArguments.Length - 4] = A;
            this.mArguments[this.mArguments.Length - 3] = B;
            this.mArguments[this.mArguments.Length - 2] = C;
            this.mArguments[this.mArguments.Length - 1] = D;
            return this.mDelegate.Method.Invoke(this.mDelegate.Target, this.mArguments);
        }

        public object Invoke(object A, object B, object C, object D, object E)
        {
            this.mArguments[this.mArguments.Length - 5] = A;
            this.mArguments[this.mArguments.Length - 4] = B;
            this.mArguments[this.mArguments.Length - 3] = C;
            this.mArguments[this.mArguments.Length - 2] = D;
            this.mArguments[this.mArguments.Length - 1] = E;
            return this.mDelegate.Method.Invoke(this.mDelegate.Target, this.mArguments);
        }
    }
}
