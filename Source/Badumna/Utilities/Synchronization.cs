﻿//------------------------------------------------------------------------------
// <copyright file="Synchronization.cs" company="National ICT Australia Limited">
//     Copyright (c) 2012 National ICT Australia Limited. All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

namespace Badumna.Utilities
{
    using System.Threading;

    /// <summary>
    /// Locks that work around a bug in Mono.
    /// </summary>
    /// <remarks>
    /// These methods provide a workaround for an issue in Mono where when you try to aquire a lock it
    /// can potentially wait for 100ms without checking the lock (see mono_monitor_try_enter_internal
    /// in mono/metadata/monitor.c; the problem occurs as described in the comment beginning "Don't block forever here...").
    /// By specifying a timeout when trying to acquire the lock we can ensure the maximum wait is
    /// much smaller.
    /// </remarks>
    internal static class Synchronization
    {
        /// <summary>
        /// The maximum time to wait when attempting one try to acquire the lock.
        /// </summary>
        /// <remarks>
        /// When the race in Mono occurs, this is the length of time we'll end up waiting, so we want it to be small.
        /// If the lock is in use for a long period of time, we'll potentially wake up with this period, so we don't want it to be too small.
        /// However, locks shouldn't be held for long, so we're better off making it smaller.
        /// </remarks>
        private const int RepeatTimeoutMs = 2;

        /// <summary>
        /// Equivalent to Monitor.Enter(object) but works around the Mono bug.
        /// </summary>
        /// <param name="locker">Object to lock on</param>
        /// <remarks>
        /// <para>
        /// Using this method there's no way to guarantee that the lock will always
        /// be in a good state in the face of, e.g., ThreadAbortException.  But
        /// if there are thread aborts we're pretty screwed anyway.
        /// </para><para>
        /// The problem is that the call to this method is either outside the
        /// try-finally that releases the lock, in which case the abort can
        /// occur after we acquire but before we enter the try, so the lock won't
        /// be released.  Or, the call to this method is inside the try-finally, and
        /// the abort comes before we acquire, in which case we'll release when
        /// we haven't acquired.
        /// </para>
        /// </remarks>
        public static void Enter(object locker)
        {
            bool gotLock = false;
            while (!gotLock)
            {
                gotLock = Monitor.TryEnter(locker, Synchronization.RepeatTimeoutMs);
            }
        }

        /// <summary>
        /// Equivalent to Monitor.Exit(object).
        /// </summary>
        /// <param name="locker">Object to lock on</param>
        public static void Exit(object locker)
        {
            Monitor.Exit(locker);
        }

        /// <summary>
        /// Performs <paramref name="action"/> while holding the <paramref name="locker"/> lock.
        /// </summary>
        /// <param name="locker">The object to lock</param>
        /// <param name="action">The action to perform</param>
        public static void Lock(object locker, GenericCallBack action)
        {
            bool gotLock = false;
            try
            {
                while (!gotLock)
                {
                    gotLock = Monitor.TryEnter(locker, Synchronization.RepeatTimeoutMs);
                }

                action();
            }
            finally
            {
                if (gotLock)
                {
                    Monitor.Exit(locker);
                }
            }
        }

        /// <summary>
        /// Performs <paramref name="func"/> while holding the <paramref name="locker"/> lock.
        /// </summary>
        /// <param name="locker">The object to lock</param>
        /// <param name="func">The function to execute</param>
        /// <typeparam name="T">The return type of the function</typeparam>
        /// <returns>The result of <paramref name="func"/></returns>
        public static T Lock<T>(object locker, GenericCallBackReturn<T> func)
        {
            bool gotLock = false;
            try
            {
                while (!gotLock)
                {
                    gotLock = Monitor.TryEnter(locker, Synchronization.RepeatTimeoutMs);
                }

                return func();
            }
            finally
            {
                if (gotLock)
                {
                    Monitor.Exit(locker);
                }
            }
        }
    }
}
