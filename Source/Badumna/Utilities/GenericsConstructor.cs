﻿//-----------------------------------------------------------------------
// <copyright file="GenericsConstructor.cs" company="NICTA">
//     Copyright (c) 2012 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.Utilities
{
    using System.Collections.Generic;
    using Badumna.Transport;

    /// <summary>
    /// This class exists solely to for AOT compilation of required types that would otherwise be missed.
    /// </summary>
    public static class GenericsConstructor
    {
        /// <summary>
        /// Force AOT compilation.
        /// </summary>
        public static void Construct()
        {
            new Dictionary<int, PgpsScheduler<TransportEnvelope>.SourceGroup>();
            new SortedList<CyclicalID.UShortID, byte[]>();
        }
    }
}
