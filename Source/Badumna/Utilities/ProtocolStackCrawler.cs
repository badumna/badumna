using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.IO;

using Badumna.Core;
using Badumna.Transport;

namespace Badumna.Utilities
{
    abstract class ProtocolStackCrawler
    {
        public virtual void Begin<EnvelopeType>(ProtocolComponent<EnvelopeType> layer) where EnvelopeType : IEnvelope { }
        public virtual void End<EnvelopeType>(ProtocolComponent<EnvelopeType> layer) where EnvelopeType : IEnvelope { }
        public abstract void VisitLayer<EnvelopeType>(ProtocolComponent<EnvelopeType> layer) where EnvelopeType : IEnvelope;
        public virtual void LeaveLayer<EnvelopeType>(ProtocolComponent<EnvelopeType> layer) where EnvelopeType : IEnvelope { }
        public virtual void VisitMethod(Object target, MethodInfo method) { }
    }

    class TextCrawler : ProtocolStackCrawler
    {
        private TextWriter mOutputStream;
        private String mIndent = "";

        public TextCrawler(TextWriter outputStream)
        {
            this.mOutputStream = outputStream;
        }

        public override void VisitLayer<EnvelopeType>(ProtocolComponent<EnvelopeType> layer)
        {            
            this.mOutputStream.WriteLine("{0}{1}, {2}", this.mIndent, layer.GetType().ToString(), layer.GetType().Assembly.FullName);
            this.mOutputStream.WriteLine(this.mIndent + "{");
            this.mIndent += "  ";
        }

        public override void VisitMethod(object target, MethodInfo method)
        {
            this.mOutputStream.WriteLine("{0}{1}()", this.mIndent, method.Name);
        }

        public override void LeaveLayer<EnvelopeType>(ProtocolComponent<EnvelopeType> layer)
        {
            this.mIndent = this.mIndent.Remove(0, 2);
            this.mOutputStream.WriteLine(this.mIndent + "}");
        }
    }
}
