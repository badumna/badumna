﻿//------------------------------------------------------------------------------
// <copyright file="Nest.cs" company="National ICT Australia Limited">
//     Copyright (c) 2012 National ICT Australia Limited. All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;

namespace Badumna.Utilities
{
    /// <summary>
    /// Stores the canonical instances of known types and constructs objects that depend on these
    /// known types.
    /// </summary>
    internal class Nest
    {
        /// <summary>
        /// Maps known types to the canonical instance of that type.
        /// </summary>
        private Dictionary<Type, object> known = new Dictionary<Type, object>();

        /// <summary>
        /// Maps types to the constructor to use for that type.  Once a constructor has been selected it is
        /// always used in future, even if changes to the known types would change the constructor used if it were
        /// to be newly selected.
        /// </summary>
        private Dictionary<Type, Constructor> constructors = new Dictionary<Type, Constructor>();

        /// <summary>
        /// Sets the canonical instance of type <typeparamref name="T"/>.
        /// </summary>
        /// <remarks>
        /// Once a cannonical instance has been set for a type it cannot be changed.
        /// </remarks>
        /// <exception cref="ArgumentException">Thrown if another instance has already been set for <typeparamref name="T"/>.</exception>
        /// <typeparam name="T">The type of the dependency</typeparam>
        /// <param name="instance">The instance</param>
        public void Set<T>(T instance)
        {
            this.known.Add(typeof(T), instance);
        }

        /// <summary>
        /// Returns the canonical instance of type <typeparamref name="T"/>.
        /// </summary>
        /// <typeparam name="T">The type of the instance to get</typeparam>
        /// <returns>The instance</returns>
        public T Get<T>()
        {
            return (T)this.known[typeof(T)];
        }

        /// <summary>
        /// Constructs a new instance of the type <typeparamref name="T"/>.
        /// </summary>
        /// <remarks>
        /// The instance will be constructed using the constructor with the fewest parameters which are all known.
        /// Non-public constructors will also be considered when choosing a constructor.
        /// If no or multiple constructors would be selected by this criterion then a <see cref="InvalidOperationException"/>
        /// will be thrown.
        /// Once a constructor has been found, that constructor will always be used to construct instances of the given
        /// type, even new cannonical instances are later added which would change the selection of the constructor.
        /// TODO: Add ability to specify the desired constructor with an attribute?
        /// </remarks>
        /// <exception cref="InvalidOperationException">Thrown if a single best constructor with known dependencies could not be found.</exception>
        /// <typeparam name="T">The type to construct an instance of</typeparam>
        /// <returns>The newly constructed instance</returns>
        public T Construct<T>()
        {
            return (T)this.Construct(typeof(T));
        }

        /// <summary>
        /// Constructs a new instance of the type <paramref name="type"/>.
        /// </summary>
        /// <remarks>
        /// The instance will be constructed using the constructor with the fewest parameters which are all known.
        /// Non-public constructors will also be considered when choosing a constructor.
        /// If no or multiple constructors would be selected by this criterion then a <see cref="InvalidOperationException"/>
        /// will be thrown.
        /// Once a constructor has been found, that constructor will always be used to construct instances of the given
        /// type, even new cannonical instances are later added which would change the selection of the constructor.
        /// TODO: Add ability to specify the desired constructor with an attribute?
        /// </remarks>
        /// <exception cref="InvalidOperationException">Thrown if a single best constructor with known dependencies could not be found.</exception>
        /// <param name="type">The type to construct an instance of</param>
        /// <returns>The newly constructed instance</returns>
        public object Construct(Type type)
        {
            Constructor constructor;
            if (!this.constructors.TryGetValue(type, out constructor))
            {
                constructor = this.FindConstructor(type);
                if (constructor == null)
                {
                    throw new InvalidOperationException("No single best constructor with known parameters could be found");
                }

                this.constructors[type] = constructor;
            }

            ParameterInfo[] parameters = constructor.Parameters;
            object[] parameterValues = new object[parameters.Length];
            
            for (int i = 0; i < parameters.Length; ++i)
            {
                parameterValues[i] = this.known[parameters[i].ParameterType];
            }

            return constructor.Invoke(parameterValues);
        }

        /// <summary>
        /// Constructs a new instance of the type <typeparamref name="T"/> and sets it as the canonical
        /// instance for <typeparamref name="T"/>.
        /// </summary>
        /// <typeparam name="T">The type to construct an instance of</typeparam>
        /// <returns>The newly constructed instance</returns>
        public T ConstructAndSet<T>()
        {
            T instance = this.Construct<T>();
            this.Set(instance);
            return instance;
        }

        /// <summary>
        /// Finds the constructor for the type with the fewest parameters that are all known.
        /// </summary>
        /// <remarks>
        /// If no or multiple constructors would be selected by this criterion then this
        /// method returns <c>null</c>.
        /// </remarks>
        /// <param name="type">The type to find the constructor for</param>
        /// <returns>The best constructor</returns>
        private Constructor FindConstructor(Type type)
        {
            if (type.IsValueType)
            {
                // Value types (structs) have an implicit default constructor that initializes
                // all fields to default values.  It's not actually a real constructor though,
                // and doesn't appear in the metadata.  Based on the selection criteria we'd
                // always choose this parameterless 'constructor' for value types, so that's
                // done here.
                return new Constructor(type);
            }

            List<ConstructorInfo> constructors = new List<ConstructorInfo>(type.GetConstructors(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic));

            constructors.Sort((x, y) => x.GetParameters().Length.CompareTo(y.GetParameters().Length));

            ConstructorInfo bestConstructor = null;
            int minSize = 0;

            foreach (ConstructorInfo constructor in constructors)
            {
                ParameterInfo[] parameters = constructor.GetParameters();

                if (parameters.Length < minSize)
                {
                    continue;
                }

                if (bestConstructor != null && bestConstructor.GetParameters().Length < parameters.Length)
                {
                    // Found a valid constructor and all the rest are longer (array is sorted by parameter length).
                    break;
                }

                Dictionary<Type, bool> usedTypes = new Dictionary<Type, bool>();

                bool valid = true;
                foreach (ParameterInfo parameter in parameters)
                {
                    Type parameterType = parameter.ParameterType;

                    if (!this.known.ContainsKey(parameterType) || usedTypes.ContainsKey(parameterType))
                    {
                        // Don't know an instance of the type; or this constructor has multiples of the
                        // same type (in which case we can't use it because we only know a single instance
                        // of each type and presumably this constructor needs two different instances).
                        valid = false;
                        break;
                    }

                    usedTypes[parameterType] = true;
                }

                if (valid)
                {
                    if (bestConstructor == null)
                    {
                        bestConstructor = constructor;
                    }
                    else
                    {
                        // The current constructor is valid, but we've already found one of the same
                        // length that is also valid (it must be the same length because if it were
                        // shorter we would have chosen it at the top of this loop).
                        // Can't choose either of these constructors.
                        bestConstructor = null;
                        minSize = parameters.Length + 1;  // In case there is a third constructor of the same length with known parameters.
                    }
                }
            }

            return bestConstructor != null ? new Constructor(bestConstructor) : null;
        }

        /// <summary>
        /// Generalizes <see cref="ConstructorInfo"/> to allow representation of constructors
        /// for value types.
        /// </summary>
        private class Constructor
        {
            /// <summary>
            /// The constructor that this instance represents.  Will be <c>null</c> if
            /// we're representing the parameterless 'constructor' of a value type.
            /// </summary>
            private ConstructorInfo constructorInfo;

            /// <summary>
            /// The type to be constructed.  Only set if we're representing the parameterless
            /// 'constructor' of a value type.
            /// </summary>
            private Type type;

            /// <summary>
            /// Initializes a new instance of the Nest.Constructor class based on a
            /// <see cref="ConstructorInfo"/>.
            /// </summary>
            /// <param name="constructorInfo">The constructor info to represent</param>
            public Constructor(ConstructorInfo constructorInfo)
            {
                if (constructorInfo == null)
                {
                    throw new ArgumentNullException("constructorInfo");
                }

                this.constructorInfo = constructorInfo;
            }

            /// <summary>
            /// Initializes a new instance of the Nest.Constructor class for a value type.
            /// </summary>
            /// <param name="type">The value type to be constructed</param>
            public Constructor(Type type)
            {
                if (type == null)
                {
                    throw new ArgumentNullException("type");
                }

                if (!type.IsValueType)
                {
                    throw new ArgumentException("Must be a value type", "type");
                }

                this.type = type;
            }

            /// <summary>
            /// Gets the parameter info for the constructor.
            /// </summary>
            public ParameterInfo[] Parameters
            {
                get
                {
                    return this.constructorInfo != null ? this.constructorInfo.GetParameters() : new ParameterInfo[0];
                }
            }

            /// <summary>
            /// Invokes the constructor to create an instance.
            /// </summary>
            /// <param name="parameters">The constructor parameters</param>
            /// <returns>The new instance</returns>
            public object Invoke(object[] parameters)
            {
                return this.constructorInfo != null ? this.constructorInfo.Invoke(parameters) : Activator.CreateInstance(this.type, true);
            }
        }
    }
}
