﻿//-----------------------------------------------------------------------
// <copyright file="ConsoleWriter.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
#if !ANDROID && !IOS
namespace Badumna.Utilities
{
    using System;
    using System.IO;

    /// <summary>
    /// Interface for console writer.
    /// </summary>
    public interface IConsoleWriter
    {
        /// <summary>
        /// Occurs when the System.ConsoleModifiers.Control modifier key (CTRL) and System.ConsoleKey.C
        /// console key (C) are pressed simultaneously (CTRL+C).
        /// </summary>
        event ConsoleCancelEventHandler CancelKeyPress;

        /// <summary>
        /// Gets the standard output stream.
        /// </summary>
        TextWriter Out { get; }

        /// <summary>
        /// Writes the text representation of the specified array of objects, followed
        /// by the current line terminator, to the standard output stream using the specified
        /// format information.
        /// </summary>
        /// <param name="format">A composite format string.</param>
        /// <param name="args">An array of objects to write using format.</param>
        /// <exception cref="System.IO.IOException">An I/O error occurred.</exception>
        /// <exception cref="System.ArgumentNullException">format or arg is null.</exception>
        /// <exception cref="System.FormatException">The format specification in format is invalid.</exception>
        void Write(string format, params object[] args);

        /// <summary>
        /// Writes the text representation of the specified array of objects to the standard
        /// output stream using the specified format information.
        /// </summary>
        /// <param name="format">A composite format string.</param>
        /// <param name="args">An array of objects to write using format.</param>
        /// <exception cref="System.IO.IOException">An I/O error occurred.</exception>
        /// <exception cref="System.ArgumentNullException">format or arg is null.</exception>
        /// <exception cref="System.FormatException">The format specification in format is invalid.</exception>
        void WriteLine(string format, params object[] args);
    }

    /// <summary>
    /// Console writer that writes to the console.
    /// </summary>
    public class ConsoleWriter : IConsoleWriter
    {
        /// <inheritdoc/>
        public event ConsoleCancelEventHandler CancelKeyPress
        {
            add
            {
#if !ANDROID
                Console.CancelKeyPress += value;
#endif
            }

            remove
            {
#if !ANDROID
                Console.CancelKeyPress -= value;
#endif
            }
        }

        /// <inheritdoc/>
        public TextWriter Out
        {
            get
            {
                return Console.Out;
            }
        }
        
        /// <inheritdoc/>
        public void Write(string format, params object[] args)
        {
            Console.Write(format, args);
        }

        /// <inheritdoc/>
        public void WriteLine(string format, params object[] args)
        {
            Console.WriteLine(format, args);
        }
    }
}
#endif