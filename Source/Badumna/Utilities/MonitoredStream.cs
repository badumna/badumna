﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Badumna.Utilities
{
    class MonitoredStream : Stream
    {
        public override bool CanRead { get { return this.mStream.CanRead; } }
        public override bool CanWrite { get { return this.mStream.CanWrite; } }
        public override bool CanSeek { get { return this.mStream.CanSeek; } }
        public override bool CanTimeout { get { return this.mStream.CanTimeout; } }

        public override long Length { get { return this.mStream.Length; } }

        public override int ReadTimeout
        {
            get { return this.mStream.ReadTimeout; }
            set { this.mStream.ReadTimeout = value; }
        }

        public override int WriteTimeout
        {
            get { return this.mStream.WriteTimeout; }
            set { this.mStream.WriteTimeout = value; }
        }

        public override long Position
        {
            get
            {
                if (this.mInterrupted)
                {
                    if (this.mLastPosition < 0)
                    {
                        throw new NotSupportedException();
                    }

                    return this.mLastPosition;
                }

                return this.mStream.Position;
            }
            set
            {
                if (this.mInterrupted)
                {
                    return;
                }

                this.mStream.Position = value;
            }
        }

        private Action<int> bytesReadDelegate;
        public event Action<int> BytesRead
        {
            add { this.bytesReadDelegate += value; }
            remove { this.bytesReadDelegate -= value; }
        }

        private Action<int> bytesWrittenDelegate;
        public event Action<int> BytesWritten
        {
            add { this.bytesWrittenDelegate += value; }
            remove { this.bytesWrittenDelegate -= value; }
        }

        private Stream mStream;
        private long mLastPosition = -1;
        private bool mInterrupted;
        public bool Interrupted { get { return this.mInterrupted; } }

        public MonitoredStream(Stream streamToMonitor)
        {
            if (streamToMonitor == null)
            {
                throw new ArgumentNullException("streamToMonitor");
            }

            this.mStream = streamToMonitor;
        }

        public void Interrupt()
        {
            this.mInterrupted = true;

            if (this.CanSeek)
            {
                this.mLastPosition = this.Position;
            }

            this.mStream.Close();
        }

        private void OnBytesRead(int count)
        {
            Action<int> handler = this.bytesReadDelegate;
            if (handler != null)
            {
                handler(count);
            }
        }

        private void OnBytesWritten(int count)
        {
            Action<int> handler = this.bytesWrittenDelegate;
            if (handler != null)
            {
                handler(count);
            }
        }

        public override int Read(byte[] buffer, int offset, int count)
        {
            if (this.mInterrupted)
            {
                return 0;
            }

            int actualCount = this.mStream.Read(buffer, offset, count);
            this.OnBytesRead(actualCount);
            return actualCount;
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            if (this.mInterrupted)
            {
                return;
            }

            this.mStream.Write(buffer, offset, count);
            this.OnBytesWritten(count);
        }

        public override int ReadByte()
        {
            if (this.mInterrupted)
            {
                return -1;
            }

            int result = this.mStream.ReadByte();

            if (result >= 0)
            {
                this.OnBytesRead(1);
            }

            return result;
        }

        public override void WriteByte(byte value)
        {
            if (this.mInterrupted)
            {
                return;
            }

            this.mStream.WriteByte(value);
            this.OnBytesWritten(1);
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            if (this.mInterrupted)
            {
                return this.Position;
            }

            return this.mStream.Seek(offset, origin);
        }

        public override void SetLength(long value)
        {
            if (this.mInterrupted)
            {
                return;
            }

            this.mStream.SetLength(value);
        }

        public override void Flush()
        {
            if (this.mInterrupted)
            {
                return;
            }

            this.mStream.Flush();
        }

        public override void Close()
        {
            if (this.mInterrupted)
            {
                return;
            }

            this.mStream.Close();
        }
    }
}
