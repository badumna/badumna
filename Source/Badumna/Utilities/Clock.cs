﻿//------------------------------------------------------------------------------
// <copyright file="Clock.cs" company="National ICT Australia Limited">
//     Copyright (c) 2012 National ICT Australia Limited. All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System;

using Badumna.Core;
using Badumna.Security;
using Badumna.Utilities.Logging;

namespace Badumna.Utilities
{
    /// <summary>
    /// A time source that is loosely synchronised with some other source.
    /// </summary>
    internal class Clock
    {
        /// <summary>
        /// Provides access to the current time.
        /// </summary>
        private GetTime getTime;

        /// <summary>
        /// The reference time in the network's time scale.
        /// </summary>
        private DateTime networkReferenceTime;

        /// <summary>
        /// The reference time in the peer's time scale.
        /// </summary>
        private TimeSpan localReferenceTime;

        /// <summary>
        /// Indicates whether we're running in a simulator environment.
        /// </summary>
        private bool isSimulated;

        /// <summary>
        /// Initializes a new instance of the Clock class.
        /// </summary>
        /// <param name="eventQueue">The queue to use for scheduling events.</param>
        /// <param name="getTime">Provides access to the current time.</param>
        public Clock(NetworkEventQueue eventQueue, GetTime getTime)
        {
            this.isSimulated = eventQueue is Simulator;
            this.getTime = getTime;

#if DEBUG
            if (this.isSimulated)
            {
                this.IsSynchronized = true;
                this.networkReferenceTime = DateTime.UtcNow;
                this.localReferenceTime = this.getTime();
            }
#endif
        }

        /// <summary>
        /// Gets the current network time.
        /// </summary>
        public DateTime Now
        {
            get
            {
                if (!this.IsSynchronized)
                {
                    Logger.TraceError(LogTag.Event, "Clock.Now invoked before clock was synchronized.");
                    throw new InvalidOperationException("Must be synchronized first");
                }

                TimeSpan localOffset = this.getTime() - this.localReferenceTime;
                return this.networkReferenceTime.Add(localOffset);
            }
        }

        /// <summary>
        /// Gets a value indicating whether the time has a been synchronized with the network.
        /// </summary>
        public bool IsSynchronized { get; private set; }

        /// <summary>
        /// Synchronizes the clock with the given time.
        /// </summary>
        /// <param name="serverTime">The time to synchronize with.</param>
        public void Synchronize(DateTime serverTime)
        {
            Logger.TraceInformation(LogTag.Event, "Clock.Synchronize is called.");

            // Synchronize can be called many times in seconds (wall clock real time) during simulation, this is because when each peer 
            // calls ``login'', the TokenMonitor will get the Network Time Token and use it to synchronize the network clock.
            // We need to make sure Synchronize is called only once during simulation.
#if DEBUG
            if (this.isSimulated && this.IsSynchronized)
            {
                return;
            }
#endif

            this.IsSynchronized = true;
            this.networkReferenceTime = serverTime;
            this.localReferenceTime = this.getTime();
        }
    }
}
