﻿//------------------------------------------------------------------------------
// <copyright file="NullLog.cs" company="National ICT Australia Limited">
//     Copyright (c) 2013 National ICT Australia Limited. All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using Badumna.Utilities.Logging;

namespace Badumna.Utilities
{
    /// <summary>
    /// A log that does nothing.
    /// </summary>
    internal class NullLog : Log
    {
        /// <inheritdoc />
        public override void Trace(LogLevel level, LogTag tags, string output)
        {
        }

        /// <inheritdoc />
        public override void TraceException(LogLevel level, LogTag tags, Exception exception, string output)
        {
        }
    }
}
