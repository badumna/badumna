//------------------------------------------------------------------------------
// <copyright file="Logger.cs" company="National ICT Australia Limited">
//     Copyright (c) 2007, 2008, 2013 National ICT Australia Limited. All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Text;
using Badumna.Core;
using Badumna.Utilities.Logging;

namespace Badumna.Utilities
{
    /// <summary>
    /// The logging system.
    /// </summary>
    public static class Logger
    {
        /// <summary>
        /// The time source.
        /// </summary>
        private static ITime timeKeeper;

#if !ANDROID && !IOS
        /// <summary>
        /// For forwarding messages from the default Trace system to Badumna's logging system.
        /// </summary>
        private static ForwardingTraceListener forwardingListener;
#endif
        
        /// <summary>
        /// Backing field for Log.  This should only be accessed via the Log property to
        /// maintain the guarantee that Log is never null.
        /// </summary>
        private static Log log = new NullLog();

        /// <summary>
        /// Gets or sets the current log.
        /// </summary>
        public static Log Log
        {
            get { return Logger.log; }
            set { Logger.log = value ?? new NullLog(); }
        }

        /// <summary>
        /// Gets or sets the logging level.
        /// </summary>
        public static LogLevel Level { get; set; }

        /// <summary>
        /// Gets or sets the included tags.  If any of these are set in the message, it will be logged (unless excluded).
        /// </summary>
        public static LogTag IncludeTags { get; set; }

        /// <summary>
        /// Gets or sets the excluded tags.  If any of these are set in the message, it will be dropped.
        /// </summary>
        public static LogTag ExcludeTags { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to add a timestamp to the logged messages.
        /// </summary>
        public static bool LogTimestamp { get; set; }

        /// <summary>
        /// Gets or sets the object used for recording statistics.
        /// </summary>
        internal static IStatistics Statistics { get; set; }

        /// <summary>
        /// Initialize the logging system.
        /// </summary>
        /// <param name="timeKeeper">The time source</param>
        internal static void Initialize(ITime timeKeeper)
        {
            Logger.timeKeeper = timeKeeper;
        }

        /// <summary>
        /// Sets up the log according to the given options.
        /// </summary>
        /// <param name="options">The log options</param>
        [Conditional("TRACE")]
        internal static void Configure(LoggerModule options)
        {
            Logger.Level = options.LogLevel;
            Logger.IncludeTags = options.IncludeTags;
            Logger.ExcludeTags = options.ExcludeTags;
            Logger.LogTimestamp = options.LogTimestamp;

            if (options.LoggerType != LoggerType.None)
            {
                try
                {
                    Logger.Log = Log.Create(options.LoggerType, options.LoggerConfig);
                }
                catch (Exception e)
                {
                    throw new ConfigurationException(
                        ConfigurationException.ErrorCode.LoggingConfigurationError,
                        "Could not construct logger, see InnerException for details",
                        e);
                }
            }

#if (!ANDROID && !IOS)
            if (options.IsDotNetTraceForwardingEnabled)
            {
                if (Logger.forwardingListener == null)
                {
                    Logger.forwardingListener = new ForwardingTraceListener();
                }

                Trace.Listeners.Remove(Logger.forwardingListener);  // Always remove so it doesn't get added multiple times

                // If we add the forwarding listener while using the DotNetTraceLog we get an infinite loop
                if (!(Logger.Log is DotNetLog))
                {
                    Trace.Listeners.Add(Logger.forwardingListener);
                }
            }

            if (options.IsAssertUiSuppressed)
            {
                foreach (TraceListener listener in Trace.Listeners)
                {
                    if (listener is DefaultTraceListener)
                    {
                        (listener as DefaultTraceListener).AssertUiEnabled = false;
                    }
                }   
            }
#endif
        }

        /// <summary>
        /// Traces an error message.
        /// </summary>
        /// <param name="tags">Tags for the message</param>
        /// <param name="format">Format of the message</param>
        /// <param name="args">Arguments to the format string</param>
        [Conditional("TRACE")]
        internal static void TraceError(LogTag tags, string format, params object[] args)
        {
            Logger.DoTrace(LogLevel.Error, tags, format, args);
        }

        /// <summary>
        /// Traces a warning message.
        /// </summary>
        /// <param name="tags">Tags for the message</param>
        /// <param name="format">Format of the message</param>
        /// <param name="args">Arguments to the format string</param>
        [Conditional("TRACE")]
        internal static void TraceWarning(LogTag tags, string format, params object[] args)
        {
            Logger.DoTrace(LogLevel.Warning, tags, format, args);
        }

        /// <summary>
        /// Traces an informational message.
        /// </summary>
        /// <param name="tags">Tags for the message</param>
        /// <param name="format">Format of the message</param>
        /// <param name="args">Arguments to the format string</param>
        [Conditional("TRACE")]
        internal static void TraceInformation(LogTag tags, string format, params object[] args)
        {
            Logger.DoTrace(LogLevel.Information, tags, format, args);
        }

        /// <summary>
        /// Traces an exception.
        /// </summary>
        /// <param name="level">The level of the exception</param>
        /// <param name="tags">Tags for the exception</param>
        /// <param name="exception">The exception object</param>
        /// <param name="format">Format of the message</param>
        /// <param name="args">Arguments to the format string</param>
        [Conditional("TRACE")]
        internal static void TraceException(LogLevel level, LogTag tags, Exception exception, string format, params object[] args)
        {
            try
            {
                // Exceptions are always logged irrespective of any filter because exceptions are always bad.
                string message = Logger.BuildTraceMessage(level, tags, format, args);
                Logger.Log.TraceException(level, tags, exception, message);
            }
            catch (Exception)
            {
                // Logger shouldn't throw exceptions if it fails
            }
        }

        /////// <summary>
        /////// ***OBSOLETE*** Use the method with the <see cref="LogTag"/> parameter instead.
        /////// </summary>
        /////// <param name="message">*** OBSOLETE 1 ***</param>
        /////// <param name="args">*** OBSOLETE 2 ***</param>
        ////[Conditional("TRACE")]
        ////internal static void TraceCritical(string message, params object[] args)
        ////{
        ////    Logger.TraceError(message, args);
        ////}

        /////// <summary>
        /////// ***OBSOLETE*** Use the method with the <see cref="LogTag"/> parameter instead.
        /////// </summary>
        /////// <param name="message">*** OBSOLETE 1 ***</param>
        /////// <param name="args">*** OBSOLETE 2 ***</param>
        ////[Conditional("TRACE")]
        ////internal static void TraceError(string message, params object[] args)
        ////{
        ////    Logger.TraceError(LogTag.OldStyle, message, args);
        ////}

        /////// <summary>
        /////// ***OBSOLETE*** Use the method with the <see cref="LogTag"/> parameter instead.
        /////// </summary>
        /////// <param name="message">*** OBSOLETE 1 ***</param>
        /////// <param name="args">*** OBSOLETE 2 ***</param>
        ////[Conditional("TRACE")]
        ////internal static void TraceWarning(string message, params object[] args)
        ////{
        ////    Logger.TraceWarning(LogTag.OldStyle, message, args);
        ////}

        /////// <summary>
        /////// ***OBSOLETE*** Use the method with the <see cref="LogTag"/> parameter instead.
        /////// </summary>
        /////// <param name="message">*** OBSOLETE 1 ***</param>
        /////// <param name="args">*** OBSOLETE 2 ***</param>
        ////[Conditional("TRACE")]
        ////internal static void TraceVerbose(string message, params object[] args)
        ////{
        ////    Logger.TraceInformation(message, args);
        ////}

        /////// <summary>
        /////// ***OBSOLETE*** Use the method with the <see cref="LogTag"/> parameter instead.
        /////// </summary>
        /////// <param name="message">*** OBSOLETE 1 ***</param>
        /////// <param name="args">*** OBSOLETE 2 ***</param>
        ////[Conditional("TRACE")]
        ////internal static void TraceInformation(string message, params object[] args)
        ////{
        ////    Logger.TraceInformation(LogTag.OldStyle, message, args);
        ////}

        /// <summary>
        /// ***OBSOLETE*** Use the method with the <see cref="LogTag"/> parameter instead.
        /// </summary>
        /// <param name="level">*** OBSOLETE 1***</param>
        /// <param name="exception">*** OBSOLETE 2***</param>
        /// <param name="message">*** OBSOLETE 3 ***</param>
        /// <param name="args">*** OBSOLETE 4 ***</param>
        [Conditional("TRACE")]
        internal static void TraceException(LogLevel level, Exception exception, string message, params object[] args)
        {
            Logger.TraceException(level, LogTag.OldStyle, exception, message, args);
        }

        /// <summary>
        /// Records a statistic.
        /// </summary>
        /// <param name="group">Group name of the statistic</param>
        /// <param name="name">Item name of the statistic</param>
        /// <param name="value">Value of the statistic</param>
        [Conditional("TRACE")]
        internal static void RecordStatistic(string group, string name, double value)
        {
            if (Logger.Statistics != null)
            {
                Logger.Statistics.Record(group, name, value);
            }
        }

        /// <summary>
        /// Checks to see if the given parameters meet the current log filter.
        /// </summary>
        /// <param name="level">The level of the message</param>
        /// <param name="tags">The tags of the message</param>
        /// <returns><c>true</c> if the parameters pass the filter, <c>false</c> otherwise</returns>
        private static bool PassesFilter(LogLevel level, LogTag tags)
        {
            var meetsLevel = level >= Logger.Level;
            var hasSomeIncludedTag = (tags & Logger.IncludeTags) != 0;
            var hasNoExcludedTag = (tags & Logger.ExcludeTags) == 0;

            return meetsLevel && hasSomeIncludedTag && hasNoExcludedTag;
        }

        /// <summary>
        /// Formats a log message into a string.
        /// </summary>
        /// <param name="level">Level of the message</param>
        /// <param name="tags">Tags for the message</param>
        /// <param name="format">Format of the message</param>
        /// <param name="args">Arguments to the format string</param>
        /// <returns>The formatted log message</returns>
        private static string BuildTraceMessage(LogLevel level, LogTag tags, string format, params object[] args)
        {
            var sb = new StringBuilder();

            if (Logger.LogTimestamp && Logger.timeKeeper != null)
            {
                sb.Append(Logger.timeKeeper.Now.TotalMilliseconds).Append("; ");
            }

            sb.Append(level.ToString()).Append("; ");
            sb.Append(tags.ToString()).Append("; ");
            sb.AppendFormat(format, args);

            return sb.ToString();
        }

        /// <summary>
        /// Sends a log message to the log if it passes the filters.
        /// </summary>
        /// <param name="level">Level of the message</param>
        /// <param name="tags">Tags for the message</param>
        /// <param name="format">Format of the message</param>
        /// <param name="args">Arguments to the format string</param>
        private static void DoTrace(LogLevel level, LogTag tags, string format, params object[] args)
        {
            try
            {
                if (Logger.PassesFilter(level, tags))
                {
                    string message = Logger.BuildTraceMessage(level, tags, format, args);
                    Logger.Log.Trace(level, tags, message);
                }
            }
            catch (Exception)
            {
                // Logger shouldn't throw exceptions if it fails
            }
        }
    }
}
