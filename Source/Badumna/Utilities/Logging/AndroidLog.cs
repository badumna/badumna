﻿//-----------------------------------------------------------------------
// <copyright file="AndroidLog.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2011 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Reflection;
using System.Xml;
using Badumna.Core;
using Badumna.Utilities.Logging;

namespace Badumna.Utilities
{
    /// <summary>
    /// Logger for Android platform that uses the Android.Util.Log static class to generate messages that
    /// can be read via the Android Device Console view in Visual Studio.
    /// NOTE that this uses reflection to find the log type and to store logging methods. this is not ideal
    /// for two reasons:
    /// 1. It is not type safe or future proof.
    /// 2. All platforms have this code in them.
    /// An alternative is for Badumna to use an interface to a logger, and the application bootstrapper gave it the logger
    /// instance at run time. This is currently not possible because the base Log class is private and does
    /// not implement a publicly visible interface.
    /// </summary>
    public class AndroidLog : Log
    {
        /// <summary>
        /// Debug log with information level.
        /// </summary>
        private readonly MethodInfo logInfo;

        /// <summary>
        /// Debug warning.
        /// </summary>
        private readonly MethodInfo logWarning;

        /// <summary>
        /// Debug error.
        /// </summary>
        private readonly MethodInfo logError;

        /// <summary>
        /// The log tag used for all messages
        /// </summary>
        private const string Prefix = "BADUMNA";

        /// <summary>
        /// Initializes a new instance of the <see cref="AndroidLog"/> class.
        /// </summary>
        internal AndroidLog()
        {
            var asm = Assembly.Load("Mono.Android");
            if (asm == null)
            {
                return;
            }

            var logger = asm.GetType("Android.Util.Log", false);
            if (logger == null)
            {
                return;
            }

            var sig = new[] { typeof(string), typeof(string) };

            this.logInfo = logger.GetMethod("Info", sig);
            this.logWarning = logger.GetMethod("Warning", sig);
            this.logError = logger.GetMethod("Error", sig);

            this.logInfo.Invoke(null, new object[] { Prefix, "Badumna log started", null });
        }

        /// <inheritdoc />
        public override void Trace(LogLevel level, LogTag tags, string output)
        {
            var method = this.logInfo;

            switch (level)
            {
                case LogLevel.Error:
                    method = this.logError;
                    break;

                case LogLevel.Warning:
                    method = this.logWarning;
                    break;

                case LogLevel.Information:
                    method = this.logInfo;
                    break;
            }
            
            if (method != null)
            {
                method.Invoke(null, new object[] { Prefix, output });
            }
        }
    }
}
