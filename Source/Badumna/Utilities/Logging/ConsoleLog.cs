//------------------------------------------------------------------------------
// <copyright file="ConsoleLog.cs" company="National ICT Australia Limited">
//     Copyright (c) 2013 National ICT Australia Limited. All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System;
using Badumna.Utilities.Logging;

namespace Badumna.Utilities
{
    /// <summary>
    /// Logs to the console.
    /// </summary>
    public class ConsoleLog : Log
    {
        /// <inheritdoc />
        public override void Trace(LogLevel level, LogTag tags, string output)
        {
            this.SetColor(level);
            Console.WriteLine(output);
            this.ResetColor();
        }

        /// <summary>
        /// Sets the console colour based on the log level.
        /// </summary>
        /// <param name="level">The log level</param>
        private void SetColor(LogLevel level)
        {
#if !CS2MFA && !IOS && !ANDROID
            switch (level)
            {
                case LogLevel.Error:
                    Console.ForegroundColor = ConsoleColor.Red;
                    break;

                case LogLevel.Warning:
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    break;
            }
#endif
        }

        /// <summary>
        /// Resets the console colour.
        /// </summary>
        private void ResetColor()
        {
#if !CS2MFA && !IOS && !ANDROID
            Console.ResetColor();
#endif
        }
    }
}
