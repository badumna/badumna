//-----------------------------------------------------------------------
// <copyright file="UnityLog.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2011 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Reflection;
using System.Xml;
using System.Xml.XPath;
using Badumna.Core;
using Badumna.Utilities.Logging;

namespace Badumna.Utilities
{
    /// <summary>
    /// UnityTraceLogger class is used to pass the trace message from Badumna to 
    /// Unity log file.
    /// </summary>
    public class UnityLog : Log
    {
        /// <summary>
        /// Debug log with information level.
        /// </summary>
        private MethodInfo logInfo;

        /// <summary>
        /// Debug warning.
        /// </summary>
        private MethodInfo logWarning;

        /// <summary>
        /// Debug error.
        /// </summary>
        private MethodInfo logError;

        /// <summary>
        /// Initializes a new instance of the <see cref="UnityLog"/> class.
        /// </summary>
        public UnityLog()
        {
            Type logger = Type.GetType("UnityEngine.Debug, UnityEngine");

            if (logger != null)
            {
                this.logInfo = logger.GetMethod("Log", new Type[] { typeof(object) });
                this.logWarning = logger.GetMethod("LogWarning", new Type[] { typeof(object) });
                this.logError = logger.GetMethod("LogError", new Type[] { typeof(object) });
            }
        }

        /// <inheritdoc />
        public override void Trace(LogLevel level, LogTag tags, string output)
        {
            switch (level)
            {
                case LogLevel.Error:
                    if (this.logError != null)
                    {
                        this.logError.Invoke(null, new object[] { output });
                    }

                    break;

                case LogLevel.Warning:
                    if (this.logWarning != null)
                    {
                        this.logWarning.Invoke(null, new object[] { output });
                    }

                    break;

                case LogLevel.Information:
                    if (this.logInfo != null)
                    {
                        this.logInfo.Invoke(null, new object[] { output });
                    }

                    break;
            }
        }
    }
}
