//------------------------------------------------------------------------------
// <copyright file="DotNetLog.cs" company="National ICT Australia Limited">
//     Copyright (c) 2013 National ICT Australia Limited. All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using Badumna.Utilities.Logging;

namespace Badumna.Utilities
{
    /// <summary>
    /// A logger that passes log messages to the .NET Diagnostics.Trace system.
    /// </summary>
    public class DotNetLog : Log
    {
        /// <inheritdoc />
        public override void Trace(LogLevel level, LogTag tags, string output)
        {
            switch (level)
            {
                case LogLevel.Error:
                    System.Diagnostics.Trace.TraceError("{0}", output);
                    break;

                case LogLevel.Warning:
                    System.Diagnostics.Trace.TraceWarning("{0}", output);
                    break;

                case LogLevel.Information:
                    System.Diagnostics.Trace.TraceInformation("{0}", output);
                    break;
            }
        }
    }
}
