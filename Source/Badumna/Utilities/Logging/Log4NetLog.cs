﻿//---------------------------------------------------------------------------------
// <copyright file="Log4NetLog.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Reflection;
using System.Xml;
using Badumna.Core;
using Badumna.Utilities.Logging;

namespace Badumna.Utilities
{
    /// <summary>
    /// Document this class
    /// </summary>
    public class Log4NetLog : Log
    {
        /// <summary>
        /// log4net's dll filename.
        /// </summary>
        private readonly string log4netDllFileName = "log4net.dll";

        /// <summary>
        /// source name for the logger.
        /// </summary>
        private string sourceName = "Badumna";

        /// <summary>
        /// The log4net assembly.
        /// </summary>
        private Assembly assembly;

        /// <summary>
        /// Type of the logger.
        /// </summary>
        private Type loggerType;

        /// <summary>
        /// The logger object.
        /// </summary>
        private object loggerObject;

        /// <summary>
        /// Initializes a new instance of the <see cref="Log4NetLog"/> class.
        /// </summary>
        /// <param name="configString">The config.</param>
        public Log4NetLog(string configString)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(configString);
            var config = doc.DocumentElement;

            string sourceName = config.GetAttribute("Source", "");

            if (sourceName != null && sourceName.Length > 0)
            {
                this.sourceName = sourceName;
            }

            XmlElement configElement = (XmlElement)config.FirstChild;

            string fullFileName = new System.IO.FileInfo(this.log4netDllFileName).FullName;
            this.assembly = Assembly.LoadFile(fullFileName);
            this.ConfigLog4Net(configElement);
            this.GetLoggerMethodInfo();
        }

        /// <inheritdoc />
        public override void Trace(LogLevel level, LogTag tags, string output)
        {
            this.TraceException(level, tags, null, output);
        }

        /// <inheritdoc />
        public override void TraceException(LogLevel level, LogTag tags, Exception exception, string output)
        {
            switch (level)
            {
                case LogLevel.Error:
                    if (exception == null)
                    {
                        this.DoLog("Error", output);
                    }
                    else
                    {
                        this.DoLog("Error", output, exception);
                    }

                    break;

                case LogLevel.Warning:
                    if (exception == null)
                    {
                        this.DoLog("Warn", output);
                    }
                    else
                    {
                        this.DoLog("Warn", output, exception);
                    }

                    break;

                case LogLevel.Information:
                    if (exception == null)
                    {
                        this.DoLog("Info", output);
                    }
                    else
                    {
                        this.DoLog("Info", output, exception);
                    }

                    break;
            }
        }

        /// <summary>
        /// Configs the log4 net.
        /// </summary>
        /// <param name="element">The element.</param>
        private void ConfigLog4Net(XmlElement element)
        {
            foreach (Type type in this.assembly.GetTypes())
            {
                if (type.Name.Equals("XmlConfigurator"))
                {
                    MethodInfo mi = type.GetMethod("Configure", new Type[] { typeof(XmlElement) });
                    mi.Invoke(type, new object[] { element });
                }
            }
        }

        /// <summary>
        /// Gets the logger method info.
        /// </summary>
        private void GetLoggerMethodInfo()
        {
            foreach (Type type in this.assembly.GetTypes())
            {
                if (type.Name.Equals("LogManager"))
                {
                    MethodInfo mi = type.GetMethod("GetLogger", new Type[] { typeof(string) });
                    object logger = mi.Invoke(type, new object[] { this.sourceName });
                    if (logger != null)
                    {
                        this.loggerObject = logger;
                        this.loggerType = logger.GetType();
                    }
                }
            }
        }

        /// <summary>
        /// Does the log.
        /// </summary>
        /// <param name="methodName">Name of the method.</param>
        /// <param name="message">The message.</param>
        private void DoLog(string methodName, string message)
        {
            MethodInfo mi = this.loggerType.GetMethod(methodName, new Type[] { typeof(object) });
            mi.Invoke(this.loggerObject, new object[] { (object)message });
        }

        /// <summary>
        /// Does the log.
        /// </summary>
        /// <param name="methodName">Name of the method.</param>
        /// <param name="message">The message.</param>
        /// <param name="exception">The exception.</param>
        private void DoLog(string methodName, string message, Exception exception)
        {
            MethodInfo mi = this.loggerType.GetMethod(methodName, new Type[] { typeof(object), typeof(Exception) });
            mi.Invoke(this.loggerObject, new object[] { (object)message, exception });
        }
    }
}

