﻿//------------------------------------------------------------------------------
// <copyright file="FileLog.cs" company="National ICT Australia Limited">
//     Copyright (c) 2013 National ICT Australia Limited. All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

namespace Badumna.Utilities.Logging
{
    using System;
    using System.Diagnostics;
    using System.IO;

    /// <summary>
    /// Sends log messages to a file.
    /// </summary>
    public class FileLog : Log
    {
        /// <summary>
        /// Backing field for the lazily-initialized writer.
        /// </summary>
        private StreamWriter writer;

        /// <summary>
        /// Backing field for the filename.
        /// </summary>
        private string filename;

        /// <summary>
        /// Gets or sets the log filename.
        /// </summary>
        /// <remarks>
        /// Changing this only has an effect before the first log message is written.
        /// If no filename is set, then it defaults to [tempdir]\badumna\badumna-[ticks]-[pid].log.
        /// </remarks>
        public string Filename
        {
            get
            {
                if (this.filename == null)
                {
                    var directory = Path.Combine(Path.GetTempPath(), "badumna");
                    var name = string.Format("badumna-{0}-{1}.log", DateTime.Now.Ticks, Process.GetCurrentProcess().Id);
                    this.filename = Path.Combine(directory, name);
                }

                return this.filename;
            }

            set
            {
                this.filename = value;
            }
        }

        /// <summary>
        /// Gets the writer.
        /// </summary>
        private StreamWriter Writer
        {
            get
            {
                if (this.writer == null)
                {
                    var directory = Path.GetDirectoryName(this.Filename);
                    if (!Directory.Exists(directory))
                    {
                        Directory.CreateDirectory(directory);
                    }
                    
                    this.writer = new StreamWriter(this.Filename);
                    this.writer.AutoFlush = true;
                }

                return this.writer;
            }
        }

        /// <inheritdoc />
        public override void Trace(LogLevel level, LogTag tags, string output)
        {
            this.Writer.WriteLine(output);
        }
    }
}
