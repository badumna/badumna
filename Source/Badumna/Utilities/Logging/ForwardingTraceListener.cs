﻿//------------------------------------------------------------------------------
// <copyright file="ForwardingTraceListener.cs" company="National ICT Australia Limited">
//     Copyright (c) 2013 National ICT Australia Limited. All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

#if !ANDROID && !IOS

using System.Diagnostics;
using Badumna.Utilities.Logging;

namespace Badumna.Utilities
{
    /// <summary>
    /// A trace listener that forwards .NET Trace messages and assertions to the Badumna logging system.
    /// </summary>
    internal class ForwardingTraceListener : TraceListener
    {
        /// <summary>
        /// The tag to use for forwarded messages.
        /// </summary>
        private const LogTag Tag = LogTag.Wrapped;

        /// <summary>
        /// A buffer for partial messages.
        /// </summary>
        private string lineBuffer = "";

        /// <inheritdoc />
        public override void Fail(string message, string detailMessage)
        {
            string output = "Assert failed: " + message;
            if (detailMessage != null && detailMessage.Length > 0)
            {
                output += "[Detail: " + detailMessage + "]";
            }

            Logger.TraceError(ForwardingTraceListener.Tag, output);
        }

        /// <inheritdoc />
        public override void Write(string message)
        {
            this.lineBuffer += message;
        }

        /// <inheritdoc />
        public override void WriteLine(string message)
        {
            this.lineBuffer += message;
            Logger.TraceWarning(ForwardingTraceListener.Tag, this.lineBuffer);
            this.lineBuffer = "";
        }
    }
}

#endif
