﻿//------------------------------------------------------------------------------
// <copyright file="DelegateLog.cs" company="National ICT Australia Limited">
//     Copyright (c) 2013 National ICT Australia Limited. All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System;
using Badumna.Utilities.Logging;

namespace Badumna.Utilities
{
    /// <summary>
    /// Signature for delegates called to log a message.
    /// </summary>
    /// <param name="level">The level of the message</param>
    /// <param name="tags">The tags for the message</param>
    /// <param name="message">The text of the message</param>
    public delegate void TraceMessage(LogLevel level, LogTag tags, string message);

    /// <summary>
    /// Signature for delegates called to log an exception.
    /// </summary>
    /// <param name="level">The level of the exception</param>
    /// <param name="tags">The tags for the exception</param>
    /// <param name="exception">The exception object</param>
    /// <param name="message">The text describing the exception</param>
    public delegate void TraceException(LogLevel level, LogTag tags, Exception exception, string message);

    /// <summary>
    /// A logger that passes log messages to delegates.
    /// </summary>
    public class DelegateLog : Log
    {
        /// <summary>
        /// Backing field for <see cref="OnMessage"/>.
        /// </summary>
        private TraceMessage onMessageDelegate;

        /// <summary>
        /// Backing field for <see cref="OnException"/>.
        /// </summary>
        private TraceException onExceptionDelegate;

        /// <summary>
        /// Invoked when a message is logged.
        /// </summary>
        public event TraceMessage OnMessage
        {
            add { this.onMessageDelegate += value; }
            remove { this.onMessageDelegate -= value; }
        }

        /// <summary>
        /// Invoked when an exception is logged.
        /// </summary>
        public event TraceException OnException
        {
            add { this.onExceptionDelegate += value; }
            remove { this.onExceptionDelegate -= value; }
        }

        /// <inheritdoc />
        public override void Trace(LogLevel level, LogTag tags, string output)
        {
            TraceMessage messageHandler = this.onMessageDelegate;
            if (messageHandler != null)
            {
                messageHandler(level, tags, output);
            }
        }

        /// <inheritdoc />
        public override void TraceException(LogLevel level, LogTag tags, Exception exception, string output)
        {
            TraceException exceptionMessageHandler = this.onExceptionDelegate;
            if (exceptionMessageHandler != null)
            {
                exceptionMessageHandler(level, tags, exception, output);
            }
        }
    }
}
