//------------------------------------------------------------------------------
// <copyright file="Log.cs" company="National ICT Australia Limited">
//     Copyright (c) 2013 National ICT Australia Limited. All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System;
using Badumna.Utilities.Logging;

namespace Badumna.Utilities
{
    /// <summary>
    /// A class that can be specialized to send log messages to a desired location.
    /// </summary>
    public abstract class Log
    {
        /// <summary>
        /// Record a log message.
        /// </summary>
        /// <param name="level">The level of the message</param>
        /// <param name="tags">The tags for the message</param>
        /// <param name="output">The text of the message</param>
        public abstract void Trace(LogLevel level, LogTag tags, string output);

        /// <summary>
        /// Implementation of trace exception.
        /// </summary>
        /// <param name="level">Trace event type.</param>
        /// <param name="tags">The tags for the exception</param>
        /// <param name="exception">Exception need to be logged.</param>
        /// <param name="output">The output message.</param>
        public virtual void TraceException(LogLevel level, LogTag tags, Exception exception, string output)
        {
            output += String.Format(" [{0}: {1}]\n{2}", exception.GetType().Name, exception.Message, exception.StackTrace);
            while (exception.InnerException != null)
            {
                output += String.Format("\nInner = [{0}: {1}]\n{2}", exception.InnerException.GetType().Name, exception.InnerException.Message, exception.InnerException.StackTrace);
                exception = exception.InnerException;
            }

            this.Trace(level, tags, output);
        }

        /// <summary>
        /// Factory method for <see cref="Log"/> types.
        /// </summary>
        /// <param name="loggerType">The type of log to construct</param>
        /// <param name="config">Optional configuration for the log</param>
        /// <returns>The new log</returns>
        internal static Log Create(LoggerType loggerType, string config)
        {
            switch (loggerType)
            {
                case LoggerType.Log4Net:
                    return new Log4NetLog(config);

                case LoggerType.DotNetTrace:
                    return new DotNetLog();

                case LoggerType.UnityTrace:
                    return new UnityLog();

                case LoggerType.Android:
                    return new AndroidLog();

                case LoggerType.Console:
                    return new ConsoleLog();

                case LoggerType.File:
                    return new FileLog();

                default:
                    return null;
            }
        }
    }
}
