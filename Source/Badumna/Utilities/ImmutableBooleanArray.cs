﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Badumna.Utilities
{
    class ImmutableBooleanArray : BooleanArray
    {
        public static ImmutableBooleanArray All { get { return ImmutableBooleanArray.mAll; } }
        private static readonly ImmutableBooleanArray mAll = new ImmutableBooleanArray(true);


        public override bool this[int index]
        {
            get { return base[index]; }
            set { throw new NotSupportedException(); }
        }


        public ImmutableBooleanArray(bool value)
        {
            base.SetAll(value);
        }

        public ImmutableBooleanArray(bool[] values)
        {
            for (int i = 0; i < values.Length; i++)
            {
                base[i] = values[i];
            }
        }

        public override void SetAll(bool value)
        {
            throw new NotSupportedException();
        }

        public override void Or(BooleanArray other)
        {
            throw new NotSupportedException();
        }
    }
}
