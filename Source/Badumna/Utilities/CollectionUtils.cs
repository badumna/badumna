//-----------------------------------------------------------------------
// <copyright file="CollectionUtils.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2012 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Badumna.Utilities
{
    /// <summary>
    /// Utilities for operating on collections.
    /// </summary>
    internal static class CollectionUtils
    {
        /// <summary>
        /// Joins an enumeration with a string separator.
        /// </summary>
        /// <param name="enumerable">The enumerable.</param>
        /// <param name="joinText">The separator.</param>
        /// <returns>A string representation of all elements, separated by `joinText`</returns>
        public static string Join(IEnumerable enumerable, string joinText)
        {
            StringBuilder result = new StringBuilder();

            bool first = true;
            foreach (object obj in enumerable)
            {
                if (first)
                {
                    first = false;
                }
                else
                {
                    result.Append(joinText);
                }

                result.Append(obj.ToString());
            }

            return result.ToString();
        }

        /// <summary>
        /// Perform an operation on each element in an enumerable.
        /// </summary>
        /// <typeparam name="E">Element type</typeparam>
        /// <typeparam name="R">Result element type</typeparam>
        /// <param name="collection">The input collection</param>
        /// <param name="action">The operation to perform on each element</param>
        /// <returns>A collection that is the result of applying `action` to each element in `collection`</returns>
        public static List<R> Map<E, R>(IEnumerable<E> collection, GenericCallBackReturn<R, E> action)
        {
            List<R> results = new List<R>();
            foreach (E e in collection)
            {
                results.Add(action(e));
            }

            return results;
        }

        /// <summary>
        /// Finds the first element in a collection that satisfied the given fiter (predicate).
        /// </summary>
        /// <typeparam name="E">The element type.</typeparam>
        /// <param name="collection">The collection.</param>
        /// <param name="filter">The filter.</param>
        /// <returns>The first matching element, or null.</returns>
        public static E Find<E>(IEnumerable<E> collection, GenericCallBackReturn<bool, E> filter)
        {
            foreach (E e in collection)
            {
                if (filter(e))
                {
                    return e;
                }
            }

            return default(E);
        }

        /// <summary>
        /// Tests two sequences for equality.
        /// </summary>
        /// <typeparam name="T">The item type.</typeparam>
        /// <param name="a">First enumerable.</param>
        /// <param name="b">Secnd enumerable.</param>
        /// <returns>Whether the two sequences are equal.</returns>
        public static bool SequenceEqual<T>(IEnumerable<T> a, IEnumerable<T> b)
        {
            using (IEnumerator<T> iter = a.GetEnumerator())
            {
                foreach (T item in b)
                {
                    if (!iter.MoveNext())
                    {
                        return false; // b is longer
                    }

                    bool eq = item == null ? iter.Current == null : item.Equals(iter.Current);
                    if (!eq)
                    {
                        return false;
                    }
                }

                if (iter.MoveNext())
                {
                    return false; // a is longer
                }
            }

            return true;
        }

        /// <summary>
        /// Gets the length of an enumerable (in linear time)
        /// </summary>
        /// <typeparam name="T">The element type</typeparam>
        /// <param name="c">The collection.</param>
        /// <returns>The length of the collection</returns>
        public static int Length<T>(IEnumerable<T> c)
        {
            int length = 0;
            foreach (var i in c)
            {
                length += 1;
            }

            return length;
        }
    }
}
