//-----------------------------------------------------------------------
// <copyright file="RandomNumberGenerator.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2011 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.Utilities
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// Interface for a random number generator.
    /// </summary>
    public interface IRandomNumberGenerator
    {
        /// <summary>
        /// Get a random number between 0.0 and 1.0.
        /// </summary>
        /// <returns>A random number between 0.0 and 1.0.</returns>
        double NextDouble();

        /// <summary>
        /// Returns a random number within a specified range.
        /// </summary>
        /// <param name="minValue">The inclusive lower bound of the random number returned.</param>
        /// <param name="maxValue"> The exclusive upper bound of the random number returned.
        /// maxValue must be greater than or equal to minValue.</param>
        /// <returns>A 32-bit signed integer greater than or equal to minValue and less than maxValue;
        /// that is, the range of return values includes minValue but not maxValue. If minValue equals
        /// maxValue, minValue is returned.</returns>
        /// <exception cref="System.ArgumentOutOfRangeException">minValue is greater than maxValue.</exception>
        int Next(int minValue, int maxValue);

        /// <summary>
        /// Returns a nonnegative random number less than the specified maximum.
        /// </summary>
        /// <param name="maxValue"> The exclusive upper bound of the random number returned.
        /// maxValue must be greater than or equal to zero.</param>
        /// <returns>A 32-bit signed integer greater than or equal to zero and less than maxValue;
        /// that is, the range of return values includes zero but not maxValue. However, if maxValue equals
        /// zero, maxValue is returned.</returns>
        /// <exception cref="System.ArgumentOutOfRangeException">maxValue is less than zero.</exception>
        int Next(int maxValue);

        /// <summary>
        /// Returns a 32-bit random unsigned integer.
        /// </summary>
        /// <returns>A 32-bit unsigned integer.</returns>
        uint NextUInt();
    }

    /// <summary>
    /// A random number generator.
    /// </summary>
    internal class RandomNumberGenerator : IRandomNumberGenerator
    {
        /// <summary>
        /// Random number generator.
        /// </summary>
        private Random random;

        /// <summary>
        /// Initializes a new instance of the RandomNumberGenerator class with a seed
        /// calculated as a function of PID and time.
        /// </summary>
        public RandomNumberGenerator()
        {
            // Not sure why a PID and time-based seed is required?
            // The default time-based seed should introduce enough randomness.
            // However this has been retained from RandomSource as it shouldn't be any worse
            // and there may be some valid reason.
            this.random = new Random(this.GetSeed());
        }

        /// <summary>
        /// Initializes a new instance of the RandomNumberGenerator class with a given seed.
        /// </summary>
        /// <param name="seed">The seed value.</param>
        public RandomNumberGenerator(int seed)
        {
            this.random = new Random(seed);
        }

        /// <summary>
        /// Initialize the random number generator with a time-based seed.
        /// </summary>
        public void Initialize()
        {
            this.random = new Random();
        }

        /// <summary>
        /// Iniitalize the random number generator with a given seed.
        /// </summary>
        /// <param name="seed">The seed value.</param>
        public void Initialize(int seed)
        {
            this.random = new Random(seed);
        }

        /// <inheritdoc/>
        public double NextDouble()
        {
            return this.random.NextDouble();
        }

        /// <inheritdoc/>
        public int Next(int minValue, int maxValue)
        {
            return this.random.Next(minValue, maxValue);
        }

        /// <inheritdoc/>
        public int Next(int maxValue)
        {
            return this.random.Next(maxValue);
        }

        /// <inheritdoc/>
        public uint NextUInt()
        {
            byte[] buffer = new byte[4];
            RandomSource.Generator.NextBytes(buffer);
            return BitConverter.ToUInt32(buffer, 0);
        }

        /// <summary>
        /// Generate a seed for the random number generator as a function of PID and time.
        /// </summary>
        /// <returns>A seed value.</returns>
        private int GetSeed()
        {
#if IOS || ANDROID
            return (int)DateTime.UtcNow.TimeOfDay.TotalMilliseconds;
#else
            int seed = 0;

            try
            {
                DateTime currentDateTime = DateTime.UtcNow;
                seed = (int)currentDateTime.TimeOfDay.TotalMilliseconds;
                int pid = System.Diagnostics.Process.GetCurrentProcess().Id;

                return seed ^ (pid + (pid << 15));
            }
            catch
            {
                // pid will be inaccessible in unity 2.6's webplayer mode
                return (int)DateTime.UtcNow.TimeOfDay.TotalMilliseconds;
            }
#endif
        }
    }
}
