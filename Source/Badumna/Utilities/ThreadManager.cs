﻿//-----------------------------------------------------------------------
// <copyright file="ThreadManager.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2011 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace Badumna.Utilities
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Threading;

    /// <summary>
    /// ThreadManager class will handle the Badumna threads, ideally all Badumna threads are started by this class
    /// (Currently this will be only used by NetworkEventQueue thread).
    /// </summary>
    internal class ThreadManager
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ThreadManager"/> class.
        /// </summary>
        public ThreadManager()
        {
        }

        /// <summary>
        /// Gets the ID of the current thread.
        /// </summary>
        public static int CurrentThreadId
        {
            get
            {
                return Thread.CurrentThread.ManagedThreadId;
            }
        }

        /// <summary>
        /// Start a thread for a given fiber instance.
        /// </summary>
        /// <param name="fiber">Fiber instance.</param>
        public void Start(Fiber fiber)
        {   
            Thread thread = new Thread(
                (ThreadStart)
                delegate
                {
                    while (!fiber.IsComplete)
                    {
                        fiber.Body(fiber);

                        if (fiber.IsWaitingOn != null)
                        {
                            fiber.IsWaitingOn.WaitOne(fiber.WaitTimeout, false);
                            fiber.IsWaitingOn = null;
                        }
                    }
                });

            thread.Name = fiber.Name;
            thread.IsBackground = fiber.IsBackground;
            fiber.FiberThread = thread;
            thread.Start();
        }
    }
}
