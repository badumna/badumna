﻿//------------------------------------------------------------------------------
// <copyright file="TestableLock.cs" company="National ICT Australia Limited">
//     Copyright (c) 2013 National ICT Australia Limited. All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

namespace Badumna.Utilities
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Threading;

    /// <summary>
    /// A lock that can be tested to see whether it is locked by the current thread.
    /// </summary>
    internal class TestableLock
    {
        /// <summary>
        /// The object we lock on.
        /// </summary>
        private readonly object locker = new object();

        /// <summary>
        /// The number of times the lock has been entered by its current holder.
        /// </summary>
        private int lockCount;

        /// <summary>
        /// The id of the thread that's holding the lock.
        /// </summary>
        private int threadId;

        /// <summary>
        /// Acquires the lock.
        /// </summary>
        public void Lock()
        {
            Synchronization.Enter(this.locker);

            this.threadId = ThreadManager.CurrentThreadId;
            this.lockCount++;  // Count re-entrancy on the same thread, so we only clear threadId after the last exit
        }

        /// <summary>
        /// Releases the lock.
        /// </summary>
        public void Unlock()
        {
            this.lockCount--;
            if (this.lockCount == 0)
            {
                this.threadId = 0;  // Ensures we don't accidentally think we still hold the lock if we call IsHeldByMe while another thread is in the process of acquiring the lock.
            }

            Synchronization.Exit(this.locker);
        }

        /// <summary>
        /// Returns a value indicating whether the lock is currently held by the caller.
        /// </summary>
        /// <returns>True iff the lock is currently held by the caller.</returns>
        public bool IsHeldByMe()
        {
            // If we have the lock, then this is threadsafe.
            // If we don't have the lock, then threadId can't match our id, because
            // ints are written atomically, there's a memory barrier after we clear
            // our id when we previously held the lock, and while a thread is active its id
            // is guaranteed unique.
            return this.lockCount > 0 && this.threadId == ThreadManager.CurrentThreadId;
        }
    }
}
