﻿// -----------------------------------------------------------------------
// <copyright file="Set.cs" company="Scalify">
//     Copyright (c) 2013 Scalify Pty Ltd. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace Badumna.Utilities
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// Set class is used to store a set of objects with a given type T. 
    /// </summary>
    /// <typeparam name="T">Type of the set.</typeparam>
    internal class Set<T> : IEnumerable, IEnumerable<T>
    {
        /// <summary>
        /// Dictionary use the store set of items.
        /// </summary>
        private Dictionary<T, bool> dictionary = new Dictionary<T, bool>();

        /// <summary>
        /// Gets the numbers of item store in the set.
        /// </summary>
        public int Count
        {
            get { return this.dictionary.Count; }
        }

        /// <summary>
        /// Add a new item into the set.
        /// </summary>
        /// <param name="item">Item to be added.</param>
        public void Add(T item)
        {            
            this.dictionary[item] = false;
        }

        /// <summary>
        /// Remove an existing item from the set.
        /// </summary>
        /// <param name="item">Item to be removed.</param>
        public void Remove(T item)
        {
            this.dictionary.Remove(item);
        }

        /// <summary>
        /// Check whether the item is in the set.
        /// </summary>
        /// <param name="item">Item to be checked.</param>
        /// <returns>Returns true if item is part of the set.</returns>
        public bool Contains(T item)
        {
            return this.dictionary.ContainsKey(item);
        }

        /// <summary>
        /// Enumerate the items in the set.
        /// </summary>
        /// <returns>Returns items enumerator.</returns>
        public IEnumerator GetEnumerator()
        {
            return this.dictionary.Keys.GetEnumerator();
        }

        /// <summary>
        /// Enumerate items in the set
        /// </summary>
        /// <returns>Returns items enumerator.</returns>
        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            return this.dictionary.Keys.GetEnumerator();
        }

        /// <summary>
        /// Clear the set.
        /// </summary>
        public void Clear()
        {
            this.dictionary.Clear();
        }
    }
}
