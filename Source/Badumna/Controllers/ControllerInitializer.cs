﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Reflection;

using Badumna;
using Badumna.Core;
using Badumna.Utilities;
using Badumna.Transport;
using Badumna.DataTypes;
using Badumna.DistributedHashTable;

namespace Badumna.Controllers
{
    /// <summary>
    /// The controller initializer interface.
    /// </summary>
    interface IControllerInitializer
    {
        /// <summary>
        /// Start a controller of the given type on the network.
        /// The controller must have a constructor that takes a single string argument.
        /// </summary>
        /// <typeparam name="T">The type of the controller. Must be derived from DistributedController</typeparam>
        /// <param name="controllerUniqueName">The unique name of the controller.</param>
        /// <exception cref="MissingConstructorException">Thrown when the given type does not have a constructor that takes a single string argument</exception>
        void StartController<T>(String controllerUniqueName) where T : DistributedController;
        
        /// <summary>
        /// Stops the controller of the given type.
        /// </summary>
        /// <typeparam name="T">The type of the controller. Must be derived from DistributedController</typeparam>
        /// <param name="controllerUniqueName">The unique name of the controller.</param>
        void StopController<T>(String controllerUniqueName) where T : DistributedController;
    }

    /// <summary>
    /// An exception thrown when a type given to StartController does not have a constructor that takes a single string argument.
    /// </summary>
    public class MissingConstructorException : Badumna.Core.BadumnaException
    {
        internal MissingConstructorException()
            : base("Cannot add controller because it does not have a constructor that takes a string as its only argument")
        {
        }
    }

    /// <summary>
    /// An exception thrown when trying to start too many controllers on the local peer. 
    /// </summary>
    public class TooManyDistributedControllerException : Badumna.Core.BadumnaException
    {
        internal TooManyDistributedControllerException()
            : base("Cannot start more controller because the limit has already been reached.")
        {
        }
    }

    /// <summary>
    /// The ControllerInitializer class is used to start or stop controllers. 
    /// </summary>
    internal class ControllerInitializer : ApplicationProtocolLayer, IControllerInitializer
    {
        private readonly INetworkFacade networkFacade;
        private DhtFacade mFacade;
        private Dictionary<HashKey, DistributedController> mControllers;
        private Dictionary<String, ConstructorInfo> mControllerConstructors;
        private Dictionary<String, ControllerInitializerReplica> mStartedController;
        private IControllerManager mManager;
        private RegularTask mRenewalTask;
        private NetworkEventQueue eventQueue;
        private bool isInCppMode;

        /// <summary>
        /// The method to use to queue events that must run in the application's thread.
        /// </summary>
        private QueueInvokable queueApplicationEvent;

        /// <summary>
        /// Used to ensure that allocated ids are generated with DHT addresses.  This is relatively hacky...
        /// </summary>
        private GenericCallBack<HashKey> setNewIdsAddress;

        /// <summary>
        /// Initializes a new instance of the <see cref="ControllerInitializer"/> class.
        /// </summary>
        /// <param name="networkFacade">The parent network facade.</param>
        /// <param name="dhtFacade">The DHT facade.</param>
        /// <param name="eventQueue">The event queue to use for scheduling events.</param>
        /// <param name="queueApplicationEvent">The method to use to queue events that must run in the application's thread.</param>
        /// <param name="connectivityReporter">Reports on network connectivity status.</param>
        /// <param name="setNewIdsAddress">Used to ensure that allocated ids are generated with DHT addresses.</param>
        /// <param name="isInCppMode">Whether the local peer is running in the cpp mode.</param>
        internal ControllerInitializer(
            INetworkFacade networkFacade, 
            DhtFacade dhtFacade, 
            NetworkEventQueue eventQueue,
            QueueInvokable queueApplicationEvent,
            INetworkConnectivityReporter connectivityReporter,
            GenericCallBack<HashKey> setNewIdsAddress,
            bool isInCppMode)
            : this(networkFacade, dhtFacade, new ControllerManager(dhtFacade, eventQueue, queueApplicationEvent, connectivityReporter), eventQueue, queueApplicationEvent, connectivityReporter, setNewIdsAddress, isInCppMode)
        { 
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ControllerInitializer"/> class.
        /// </summary>
        /// <param name="networkFacade">The parent network facade.</param>
        /// <param name="dhtFacade">The DHT facade.</param>
        /// <param name="manager">The controller manager.</param>
        /// <param name="eventQueue">The event queue to use for scheduling events.</param>
        /// <param name="queueApplicationEvent">The method to use to queue events that must run in the application's thread.</param>
        /// <param name="connectivityReporter">Reports on network connectivity status.</param>
        /// <param name="setNewIdsAddress">Used to ensure that allocated ids are generated with DHT addresses.</param>
        /// <param name="isInCppMode">Whether the local peer is running in the cpp mode.</param>
        internal ControllerInitializer(
            INetworkFacade networkFacade,
            DhtFacade dhtFacade,
            IControllerManager manager,
            NetworkEventQueue eventQueue,
            QueueInvokable queueApplicationEvent,
            INetworkConnectivityReporter connectivityReporter,
            GenericCallBack<HashKey> setNewIdsAddress,
            bool isInCppMode)
            : base(dhtFacade)
        {
            if (networkFacade == null)
            {
                throw new ArgumentNullException("networkFacade");
            }

            this.isInCppMode = isInCppMode;
            this.networkFacade = networkFacade;
            this.setNewIdsAddress = setNewIdsAddress;

            this.mFacade = dhtFacade;
            this.eventQueue = eventQueue;
            this.queueApplicationEvent = queueApplicationEvent;
            this.mControllers = new Dictionary<HashKey, DistributedController>();
            this.mControllerConstructors = new Dictionary<string, ConstructorInfo>();
            this.mStartedController = new Dictionary<string, ControllerInitializerReplica>();
            this.mFacade.RegisterTypeForReplication<ControllerInitializerReplica>(this.ReplicaExpiredHandler, this.ReplicaArrivalHandler);
            this.mManager = manager;

            string renewalTaskName = "controller_initializer_renewal_task";
            TimeSpan renewalTaskInterval = Parameters.DistributedControllerRenewalTaskInterval;
            this.mRenewalTask = new RegularTask(renewalTaskName, renewalTaskInterval, eventQueue, connectivityReporter, this.ControllerInitializerRenewalTask);
            this.mRenewalTask.Start();
        }

        /// <summary>
        /// Handles the initializer replica expiration event. It removes expired controller. 
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="Badumna.Utilities.ExpireEventArgs"/> instance containing the event data.</param>
        private void ReplicaExpiredHandler(object sender, ExpireEventArgs e)
        {
            ControllerInitializerReplica controllerReplica = e.ExpiredObject as ControllerInitializerReplica;

            if (controllerReplica != null)
            {
                Logger.TraceInformation(LogTag.DistributedController, "Removing distributed controller initializer {0}", controllerReplica.UniqieName);
                this.queueApplicationEvent(Apply.Func(this.RemoveController, controllerReplica.UniqieName));
            }
        }

        /// <summary>
        /// Handles the initializer replica arrival event. It creates the controller. 
        /// </summary>
        /// <param name="replica">The replica.</param>
        /// <param name="isOriginal">if set to <c>true</c> if the replica is original.</param>
        private void ReplicaArrivalHandler(IReplicatedObject replica, bool isOriginal)
        {
            ControllerInitializerReplica controllerReplica = replica as ControllerInitializerReplica;

            if (controllerReplica != null)
            {
                Logger.TraceInformation(LogTag.DistributedController, "Adding distributed controller initializer {0}", controllerReplica.UniqieName);
                this.AddController(controllerReplica.ControllerTypeName, controllerReplica.UniqieName, controllerReplica.Guid, controllerReplica.CheckpointDataId);
            }
        }

        /// <summary>
        /// Processes this instance.
        /// </summary>
        public void Process()
        {
            this.mManager.Process();
        }

        /// <summary>
        /// Shutdowns this instance.
        /// </summary>
        public void Shutdown()
        {
            if (this.mRenewalTask.IsRunning)
            {
                this.mRenewalTask.Stop();
            }

            // call destroy on all controllers to make sure the cpp ones will get a chance to peacefully delete all 
            // originals and replicas. 
            foreach (DistributedController controller in this.mControllers.Values)
            {
                controller.Destroy();
            }
        }

        /// <summary>
        /// Start a controller of the given type on the network.
        /// The controller must have a constructor that takes a single string argument.
        /// </summary>
        /// <typeparam name="T">The type of the controller. Must be derived from DistributedController</typeparam>
        /// <param name="controllerUniqueName">The unique name of the controller.</param>
        /// <exception cref="MissingConstructorException">Thrown when the given type does not have a constructor that takes a single string argument</exception>
        public void StartController<T>(String controllerUniqueName) where T : DistributedController
        {
            ConstructorInfo constructorInfo = typeof(T).GetConstructor(new System.Type[] { typeof(string) });
            if (constructorInfo == null)
            {
                throw new MissingConstructorException();
            }

            if(this.mStartedController.Count >= Parameters.MaximumNumberOfDistributedController)
            {
                throw new TooManyDistributedControllerException();
            }

            String typeName = typeof(T).ToString();
            if (!this.mControllerConstructors.ContainsKey(typeName))
            {
                this.mControllerConstructors.Add(typeName, constructorInfo);
            }

            if (!this.mStartedController.ContainsKey(controllerUniqueName))
            {
                uint timeToLiveSeconds = (uint)Parameters.DistributedControllerInitializerReplicaTTL.TotalSeconds;
                ControllerInitializerReplica controllerReplica = new ControllerInitializerReplica(typeName, controllerUniqueName);
                this.mFacade.Replicate(controllerReplica, controllerReplica.Key, timeToLiveSeconds);

                this.mStartedController[controllerUniqueName] = controllerReplica;
            }
            else
            {
                // trying to start the controller that has been locally started.
                Logger.TraceInformation(LogTag.DistributedController, "The distributed controller named {0} has already been started.", controllerUniqueName);
                return;
            }
        }

        /// <summary>
        /// Stops renewing the controller of the given type so the controller itself will eventually be removed in less than 1 minute. 
        /// </summary>
        /// <typeparam name="T">The type of the controller. Must be derived from DistributedController</typeparam>
        /// <param name="controllerUniqueName">The unique name of the controller.</param>
        public void StopController<T>(String controllerUniqueName) where T : DistributedController
        {
            if (this.mStartedController.ContainsKey(controllerUniqueName))
            {
                this.mStartedController.Remove(controllerUniqueName);
            }

            this.StopController(typeof(T).ToString(), controllerUniqueName);
        }

        /// <summary>
        /// Stops the controller.
        /// </summary>
        /// <param name="typeName">Name of the type.</param>
        /// <param name="controllerUniqueName">Name of the controller unique.</param>
        internal void StopController(String typeName, String controllerUniqueName) 
        {
            // TODO : Implement this.
            // currently the ttl value for the controller initializer is small, the initializer will be expired in less 1 minute,
            // so if all related peers call StopController<controllerUniqueName>, the controller will be stopped in less than 1 minute anyway.
        }

        /// <summary>
        /// The regular task used to renew all controllers started by the local peer. 
        /// </summary>
        private void ControllerInitializerRenewalTask()
        {
            foreach (KeyValuePair<String, ControllerInitializerReplica> kvp in this.mStartedController)
            {
                uint timeToLiveSeconds = (uint)Parameters.DistributedControllerInitializerReplicaTTL.TotalSeconds;
                this.mFacade.Replicate(kvp.Value, kvp.Value.Key, timeToLiveSeconds);    
            }
        }

        /// <summary>
        /// Creates a controller specified by the type and name, adds it to the controller list. 
        /// </summary>
        /// <param name="typeName">Name of the type.</param>
        /// <param name="controllerName">Name of the controller.</param>
        /// <param name="guid">The GUID of the controller.</param>
        /// <param name="checkpointDataId">The checkpoint data id.</param>
        /// <returns>The specified controller.</returns>
        private DistributedController AddController(String typeName, String controllerName, BadumnaId guid, BadumnaId checkpointDataId)
        {
            HashKey key = HashKey.Hash(controllerName);
            DistributedController controller = null;            

            if (this.mControllers.TryGetValue(key, out controller))
            {
                return controller;
            }

            if (!ControllerInitializerHelper.ShouldInitializeController(this.isInCppMode, typeName, controllerName))
            {
                return null;
            }

            ConstructorInfo controllerConstructor = null;

            if (!this.mControllerConstructors.TryGetValue(typeName, out controllerConstructor))
            {
                Logger.TraceInformation(LogTag.DistributedController, "Failed to find controller constructor for {0}. Attempting reflection", typeName);

                controllerConstructor = ConstructorInfoManager.GetConstructorInfo(typeName);
                if (controllerConstructor == null)
                {
                    return null;
                }
            }

            controller = (DistributedController)controllerConstructor.Invoke(new object[] { controllerName });
            if (controller != null)
            {
                // TODO: This is horrible coupled like the setting of NetworkFacade below.  *Really* needs to be fixed.
                controller.PostConstructor(this.eventQueue, this.queueApplicationEvent, this.setNewIdsAddress);

                // This is a terrible hack - only temporary until I find a nice way to pass constructor arguments around.
                DistributedSceneController sceneController = controller as DistributedSceneController;
                if (sceneController != null)
                {
                    sceneController.SetNetworkFacade(this.networkFacade);

                    string[] parts = controllerName.Split('^');
                    if (parts.Length > 0)
                    {
                        sceneController.SceneName = parts[0];
                    }
                }

                this.mControllers.Add(key, controller);
                controller.Initialize(this, guid, checkpointDataId); // Initialize must be called before reference to Index
                this.mManager.RegisterController(controller);
                this.AddNode(controller.Index, controller);
            }
            else
            {
                Logger.TraceInformation(LogTag.DistributedController, "Failed to create controller {0}", typeName);
            }

            return controller;
        }

        /// <summary>
        /// Removes the controller.
        /// </summary>
        /// <param name="controllerName">Name of the controller to be removed.</param>
        private void RemoveController(String controllerName)
        {
            HashKey key = HashKey.Hash(controllerName);
            DistributedController controller = null;
            if (this.mControllers.TryGetValue(key, out controller))
            {
                // Un-registers the controller
                controller.Shutdown(); 
                this.mManager.RemoveController(controller);
                this.mControllers.Remove(key);

                controller.Destroy();
            }
        }

        #region for_unit_test

        internal int GetNumberOfStartedController()
        {
            return this.mStartedController.Count;
        }

        internal int GetNumberOfControllers()
        {
            return this.mControllers.Count;
        }

        internal int GetNumberOfControllerConstructors()
        {
            return this.mControllerConstructors.Count;
        }

        #endregion 
    }
}
