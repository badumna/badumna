﻿using System;
using System.Collections.Generic;
using System.Text;

using Badumna.Core;
using Badumna.Utilities;
using Badumna.DataTypes;
using Badumna.DistributedHashTable;

namespace Badumna.Controllers
{
    class DistributedControllerReplica : IReplicatedObject
    {
        #region IReplicatedObject Members

        private bool ignoreModificationNumber = false;
        public bool IgnoreModificationNumber
        {
            get { return this.ignoreModificationNumber; }
        }

        private BadumnaId mGuid;
        public BadumnaId Guid
        {
            get { return this.mGuid; }
            set { this.mGuid = value; }
        }

        private CyclicalID.UShortID mModificationNumber;
        public CyclicalID.UShortID ModificationNumber
        {
            get { return this.mModificationNumber; }
            set { this.mModificationNumber = value; }
        }

        private HashKey mObjectKey;
        public HashKey Key
        {
            get { return this.mObjectKey; }
            set { this.mObjectKey = value; }
        }

        private uint mTimeToLiveSeconds;
        public uint TimeToLiveSeconds
        {
            get { return this.mTimeToLiveSeconds; }
            set { this.mTimeToLiveSeconds = value; }
        }

        public int NumberOfReplicas { get { return 5; } }

        #endregion

        #region IExpireable Members

        private TimeSpan mExpirationTime;
        public TimeSpan ExpirationTime
        {
            get { return this.mExpirationTime; }
            set { this.mExpirationTime = value; }
        }

        #endregion

        private byte[] mCheckpointData;
        public byte[] CheckpointData
        {
            get { return this.mCheckpointData; }
            set { this.mCheckpointData = value; }
        }

        public DistributedControllerReplica()
        { }

        public DistributedControllerReplica(HashKey controllerKey, BadumnaId guid)
        {
            this.mGuid = guid;
            this.mObjectKey = controllerKey;
        }

        #region IParseable Members

        public void ToMessage(MessageBuffer message, Type parameterType)
        {
            if (this.CheckpointData != null)
            {
                message.Write((ushort)this.mCheckpointData.Length);
                message.Write(this.mCheckpointData, this.mCheckpointData.Length);
            }
            else
            {
                message.Write(0x00);
            }
        }

        public void FromMessage(MessageBuffer message)
        {
            int length = (int)message.ReadUShort();
            if (length > 0)
            {
                this.mCheckpointData = message.ReadBytes(length);
            }
            else
            {
                this.CheckpointData = new byte[0];
            }
        }

        #endregion
    }
}
