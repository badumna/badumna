﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

using Badumna.Core;
using Badumna.Utilities;
using Badumna.DataTypes;
using Badumna.Transport;
using Badumna.DistributedHashTable;

namespace Badumna.Controllers
{
    /// <summary>
    /// A Distributed controller is a persistent (migrateable) object. It can be used to execute code that is not
    /// related to particular peers, akin to a server process. The controller is addressable independantly of its physical 
    /// (IP) address. This allows the application developer to write code to control parts of the system (eg. running NPCs or region controllers) 
    /// without having to concern themselves with which physical machine the process is run on. 
    /// </summary>
    /// <remarks>
    /// Controllers must be registered with the NetworkFacade to be run.
    /// See <see cref="NetworkFacade.StartController{T}(string)"/>, <see cref="NetworkFacade.StartController{T}(string, string, ushort)"/>.
    /// 
    /// When developing a distributed controller you must be mindful of the fact that you do not have any control over which peer the 
    /// code will be run. This means that all operation of the code must be performed within the Wake/Sleep cycle described below and 
    /// all interaction with the object should be done through message passing.
    /// 
    /// After a controller has been registered the network will determine an appropriate peer for the code to be executed on. 
    /// This peer will create an instance of the derived distributed controller and call its Wake() method. 
    /// This method idicates that the local instance of the controller is now the recipient of all messages sent to the controller
    /// and is now responsible for handling the processing. Likewise, if the network layer decides another peer is more appropriate to run the 
    /// controller the local instance's Sleep() method will be called. 
    /// When the Sleep() method is called any checkpointed data (see <see cref="DistributedController.Checkpoint"/>) will be sent to the peer now responsible
    /// for the controller. 
    /// 
    /// The Replicate() method can be called at anytime the controller is awake. This will cause the network layer to call Checkpoint() and replicates
    /// the returned data to ensure that if the current controlling peer fails another peer will be able to continue from that point.
    /// </remarks>
    abstract public class DistributedController : ApplicationProtocolNode
    {
        private HashKey mHashKey;
        internal HashKey HashKey { get { return this.mHashKey; } }

        private struct QueuedMessage
        {
            public MessageStream Stream;
            public BadumnaId Source;
            public QualityOfService Qos;
        }

        private List<QueuedMessage> mQueuedMessages;

        private bool mIsControlledLocally;
        internal bool IsControlledLocally
        {
            get { return this.mIsControlledLocally; }
        }

        private DhtFacade mDhtFacade;
        internal DhtFacade DhtFacade { set { this.mDhtFacade = value; } } // Set by the facade when it is regisietered

        internal BadumnaId Guid
        {
            get { return this.Index; }
        }

        internal BadumnaId CheckpointDataId
        {
            get { return this.mReplica.Guid; }
        }

        private DistributedControllerReplica mReplica;
        private ControllerInitializer mInitializer;

        private String mUniqueName;
        internal String UniqueName { get { return this.mUniqueName; } }

        private byte[] mLastCheckpoint;
        internal byte[] LastCheckpoint { get { return this.mLastCheckpoint; } }

        /// <summary>
        /// The queue to use for scheduling events.
        /// </summary>
        /// <remarks>
        /// Ideally this would have access of (protected 'intersection' internal), but "protected interal" means (protected 'union' internal).
        /// Don't want to publically expose it, so it's just internal.
        /// </remarks>
        internal NetworkEventQueue eventQueue;

        /// <summary>
        /// The method to use to queue events that must run in the application's thread.
        /// </summary>
        /// <remarks>
        /// Ideally this would have access of (protected 'intersection' internal), but "protected interal" means (protected 'union' internal).
        /// Don't want to publically expose it, so it's just internal.
        /// </remarks>
        internal QueueInvokable queueApplicationEvent;

        /// <summary>
        /// Used to ensure that allocated ids are generated with DHT addresses.  This is relatively hacky...
        /// </summary>
        private GenericCallBack<HashKey> setNewIdsAddress;

        internal DistributedController(String uniqueName)
        {
            this.mUniqueName = uniqueName;
            this.mHashKey = HashKey.Hash(uniqueName);
        }

        /// <summary>
        /// This is used to initialize fields that are not initialized in the constructor.
        /// </summary>
        /// <remarks>
        /// TODO: This needs serious refactoring.  See also ControllerInitializer.AddController.
        /// </remarks>
        /// <param name="eventQueue">The queue to use for scheduling events</param>
        /// <param name="queueApplicationEvent">The method to use to queue events that must run in the application's thread</param>
        /// <param name="setNewIdsAddress">sed to ensure that allocated ids are generated with DHT addresses</param>
        internal void PostConstructor(NetworkEventQueue eventQueue, QueueInvokable queueApplicationEvent, GenericCallBack<HashKey> setNewIdsAddress)
        {
            this.eventQueue = eventQueue;
            this.queueApplicationEvent = queueApplicationEvent;
            this.setNewIdsAddress = setNewIdsAddress;
        }

        internal void Initialize(ControllerInitializer initializer, BadumnaId guid, BadumnaId checkpointDataId)
        {
            this.mInitializer = initializer;
            this.Index = guid;
            this.mReplica = new DistributedControllerReplica(this.mHashKey, checkpointDataId);
        }

        internal virtual void Shutdown()
        {
        }

        /// <summary>
        /// Remove the controller from the network
        /// </summary>
        // TODO : Should have to provide some type of authorization to do this.
        protected void Stop()
        {
            this.eventQueue.Schedule(1, this.mInitializer.StopController, this.GetType().ToString(), this.UniqueName);
        }

        internal virtual void InvokeWake(byte[] checkpointData)
        {
            this.queueApplicationEvent(Apply.Func(
                delegate()
                {
                    Logger.TraceInformation(LogTag.DistributedController, "Waking controller {0}", this.Guid);
                    try
                    {
                        this.setNewIdsAddress(this.mHashKey);
                        this.Wake();
                    }
                    catch (Exception e)
                    {
                        Logger.TraceException(LogLevel.Warning, e, "Application failed to wake controller {0}", this.Guid);
                        return;
                    }
                    finally
                    {
                        this.setNewIdsAddress(null);
                    }
                    this.mIsControlledLocally = true;
                }));

            if (checkpointData != null)
            {
                this.queueApplicationEvent(Apply.Func(this.InvokeRecover, checkpointData));
            }

            // Receive any queued messages.
            if (this.mQueuedMessages != null)
            {
                foreach (QueuedMessage queuedMessage in this.mQueuedMessages)
                {
                    this.queueApplicationEvent(Apply.Func(
                        delegate(QueuedMessage qm)
                        {
                            this.ProcessQueuedMessage(qm);
                        },
                        queuedMessage));
                }
                this.mQueuedMessages.Clear();
                this.mQueuedMessages = null;
            }
        }

        private void ProcessQueuedMessage(QueuedMessage qm)
        {
            try
            {
                qm.Stream.Position = 0;
                this.ReceiveMessage(qm.Stream, qm.Source);
            }
            catch (Exception e)
            {
                Logger.TraceException(LogLevel.Error, e, "Distributed controller has failed to handle message");
            }
        }

        internal void InvokeRecover(byte[] checkpointData)
        {
            try
            {
                this.setNewIdsAddress(this.mHashKey);

                if (checkpointData != null)
                {
                    using (Stream checkpointStream = new MemoryStream(checkpointData))
                    {
                        using (BinaryReader reader = new BinaryReader(checkpointStream))
                        {
                            ushort modification = reader.ReadUInt16();
                            modification++;
                            this.mReplica.ModificationNumber = new CyclicalID.UShortID(modification);
                            this.Recover(reader);
                        }
                    }
                }
                else
                {
                    // no checkpoint data, call the default value recover method to make sure all controller state are set to be valid values.
                    this.Recover();
                }
            }
            catch (Exception e)
            {
                Logger.TraceException(LogLevel.Warning, e, "Application failed to recover controller {0}", this.Guid);
            }
            finally
            {
                this.setNewIdsAddress(null);
            }
        }

        /// <summary>
        /// Destroys this instance.
        /// </summary>
        internal virtual void Destroy()
        {
        }

        /// <summary>
        /// Wake the controller. 
        /// This method is called when the network layer decides that the local peer is now responsible for the controller.
        /// </summary>
        protected virtual void Wake()
        {
        }

        internal virtual void InvokeSleep()
        {
            Logger.TraceInformation(LogTag.DistributedController, "Putting to sleep controller {0}", this.Guid);
            try
            {
                this.setNewIdsAddress(this.mHashKey);
                this.Sleep();
                this.mIsControlledLocally = false;
            }
            catch (Exception e)
            {
                Logger.TraceException(LogLevel.Warning, e, "Application failed to sleep controller {0}", this.Guid);
            }
            finally
            {
                this.setNewIdsAddress(null);
            }
        }

        /// <summary>
        /// Put to sleep the controller.
        /// This method is called when the network layer decides that another peer is now responsible for this controller.
        /// </summary>
        protected virtual void Sleep()
        {
        }

        internal virtual void InvokeProcess(TimeSpan delay)
        {
            try
            {
                this.setNewIdsAddress(this.mHashKey);
                this.Process(delay);
            }
            catch (Exception e)
            {
                Logger.TraceException(LogLevel.Warning, e, "Application threw in distributed controller Process() method");
            }
            finally
            {
                this.setNewIdsAddress(null);
            }
        }

        internal virtual void InvokeReplicate()
        {
            try
            {
                this.setNewIdsAddress(this.mHashKey);
                this.Replicate();
            }
            catch (CheckpointDataTooLongException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                Logger.TraceException(LogLevel.Warning, e, "Application threw in distributed controller Replicate() method");
            }
            finally
            {
                this.setNewIdsAddress(null);
            }
        }

        /// <summary>
        /// Replicates any checkpointable state.
        /// Calling this method will cause the Checkpoint() method to be called. The data returned will be replicated on a number of 
        /// peers to ensure that if the current running peer fails the next peer responsible for the controller can continue from this 
        /// point.
        /// </summary>
        /// <exception cref="CheckpointDataTooLongException">Throw when too much data is written to the stream passed to Checkpoint()</exception>
        protected void Replicate()
        {
            if (this.IsControlledLocally && this.mDhtFacade != null)
            {
                // This method is only called from application so NO need to catch exceptions
                using (Stream stream = new MemoryStream())
                {
                    using (BinaryWriter writer = new BinaryWriter(stream))
                    {
                        // save DistributedControllerReplica's modification number
                        writer.Write(this.mReplica.ModificationNumber.Value);
                        this.Checkpoint(writer);
                        stream.Flush();

                        if (stream.Length > Parameters.DistributedControllerMaxCheckpointSize)
                        {
                            throw new CheckpointDataTooLongException();
                        }

                        this.mLastCheckpoint = (stream as MemoryStream).ToArray();

                        if (this.mLastCheckpoint != null)
                        {
                            try
                            {
                                uint ttl = (uint)Parameters.DistributedControllerCheckpointImageTTL.TotalSeconds;
                                this.mReplica.CheckpointData = this.mLastCheckpoint;
                                this.mDhtFacade.Replicate(this.mReplica, this.mHashKey, ttl);
                            }
                            catch (Exception e)
                            {
                                Logger.TraceException(LogLevel.Warning, e, "Failed to checkpoint controller {0}", this.Guid);
                                return;
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Capture any state the controller would need if it were to run on another machine. 
        /// The data written to the given stream may be passed to the Recover() method, possibly on another machine.
        /// </summary>
        /// <remarks>
        /// You should ensure that all information needed for the controller to run in a consistent manner is included.
        /// </remarks>
        /// <param name="writer">A writer to which all state information should be written</param>
        protected abstract void Checkpoint(BinaryWriter writer);

        /// <summary>
        /// Read from the given stream all the state that was previously written to it by the Checkpoint() method.
        /// This method is called after a controller has migrated to another peer.
        /// </summary>
        /// <param name="reader">The reader to read the checkpoint data from.</param>
        protected abstract void Recover(BinaryReader reader);

        /// <summary>
        /// Recovers the controller to a default state. This method is called after a controller has migrated to another peer
        /// but the checkpoint data produced by the Checkpoint() method is not accessiable, for example, the controller migrated
        /// to the current local machine after its previous host crashed. This method makes sure that the controller's properties 
        /// such as position and velocity will be set to valid default values.   
        /// </summary>
        protected abstract void Recover();

        /// <summary>
        /// This method is called periodically (once for every call to NetworkFacade.ProcessNetworkState) and is called from the 
        /// application thread. 
        /// </summary>
        /// <param name="duration">The elapsed time since the last call to this method()</param>
        protected abstract void Process(TimeSpan duration);

        /// <summary>
        /// Process a message that has been sent to this controller. 
        /// </summary>
        /// <param name="message">The content of the message</param>
        /// <param name="source">The source of the message</param>
        protected abstract void ReceiveMessage(MessageStream message, BadumnaId source);

        internal override void HandleMessage(MessageStream message, BadumnaId source, QualityOfService qos)
        {
            if (!this.IsControlledLocally)
            {
                if (this.mQueuedMessages == null)
                {
                    this.mQueuedMessages = new List<QueuedMessage>();
                }
                QueuedMessage queuedMessage = new QueuedMessage();

                queuedMessage.Stream = message;
                queuedMessage.Source = source;
                queuedMessage.Qos = qos;

                this.mQueuedMessages.Add(queuedMessage);
            }
            else
            {
                this.ReceiveMessage(message, source);
            }
        }
    }

    /// <summary>
    /// An exception that indicates that the data returned from a call Checkpoint is too large. 
    /// </summary>
    public class CheckpointDataTooLongException : BadumnaException
    {
        internal CheckpointDataTooLongException()
            : base(String.Format("Checkpoint data too long. Must be less than {0}", Parameters.DistributedControllerMaxCheckpointSize))
        { }
    }
}
