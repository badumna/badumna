﻿//-----------------------------------------------------------------------
// <copyright file="ControllerStateManager.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using Badumna.Core;
using Badumna.DataTypes;
using Badumna.DistributedHashTable;
using Badumna.Utilities;

namespace Badumna.Controllers
{
    /// <summary>
    /// The controller state manager keeps all locally registered controllers and manage their state.  
    /// </summary>
    internal class ControllerStateManager : DhtProtocol
    {
        /// <summary>
        /// The Dht facade.
        /// </summary>
        private DhtFacade facade;

        /// <summary>
        /// The registered controllers.
        /// </summary>
        private Dictionary<BadumnaId, DistributedController> controllers;

        /// <summary>
        /// The stop watch used to provide elapsed time when calling controller's process method. 
        /// </summary>
        private TimeKeeper stopWatch;
        
        /// <summary>
        /// The queue to use for scheduling events.
        /// </summary>
        private NetworkEventQueue eventQueue;

        /// <summary>
        /// The method to use to queue events that must run in the application's thread.
        /// </summary>
        private QueueInvokable queueApplicationEvent;

        /// <summary>
        /// Initializes a new instance of the <see cref="ControllerStateManager"/> class.
        /// </summary>
        /// <param name="facade">The facade.</param>
        /// <param name="eventQueue">The queue to use for scheduling events.</param>
        /// <param name="queueApplicationEvent">The method to use to queue events that must run in the application's thread.</param>
        public ControllerStateManager(DhtFacade facade, NetworkEventQueue eventQueue, QueueInvokable queueApplicationEvent) 
            : base(facade)
        {
            this.eventQueue = eventQueue;
            this.queueApplicationEvent = queueApplicationEvent;
            this.controllers = new Dictionary<BadumnaId, DistributedController>();
            this.facade = facade;
            this.stopWatch = new TimeKeeper();
        }

        /// <summary>
        /// Gets the number of registered controllers.
        /// </summary>
        /// <value>The number of registered controllers.</value>
        public int Count
        {
            get { return this.controllers.Count; }
        }

        /// <summary>
        /// Registers the controller.
        /// </summary>
        /// <param name="controller">The controller.</param>
        /// <returns>Whether the controller is registered.</returns>
        public bool RegisterController(DistributedController controller)
        {
            if (!this.controllers.ContainsKey(controller.Guid))
            {
                this.controllers.Add(controller.Guid, controller);
                return true;
            }

            return false;
        }

        /// <summary>
        /// Removes the controller.
        /// </summary>
        /// <param name="controller">The controller.</param>
        /// <returns>Whether the controller is removed.</returns>
        public bool RemoveController(DistributedController controller)
        {
            // TODO:
            // what happens this is not controlled locally? 
            controller.DhtFacade = null;
            if (controller.IsControlledLocally)
            {
                controller.InvokeSleep();

                this.eventQueue.AcquirePerformLockForApplicationCode();
                try
                {
                    this.controllers.Remove(controller.Guid);
                }
                finally
                {
                    this.eventQueue.ReleasePerformLockForApplicationCode();
                }

                return true;
            }

            return false;
        }

        /// <summary>
        /// Processes all controllers.
        /// </summary>
        public void Process()
        {
            long timeSliceMilliseonds = (long)this.stopWatch.Elapsed.TotalMilliseconds;
            this.stopWatch.Reset();
            this.stopWatch.Start();
            TimeSpan duration = TimeSpan.FromMilliseconds(timeSliceMilliseonds);

            foreach (DistributedController controller in this.controllers.Values)
            {
                this.ProcessController(controller, duration);
            }
        }

        /// <summary>
        /// Processes the controller.
        /// </summary>
        /// <param name="controller">The controller.</param>
        /// <param name="duration">The duration since last process.</param>
        public void ProcessController(DistributedController controller, TimeSpan duration)
        {
            if (controller.IsControlledLocally)
            {
                if (!this.facade.MapsToLocalPeer(controller.HashKey, null))
                {
                    this.WakeOnAppropriatePeer(controller);
                }
                else
                {
                    controller.InvokeProcess(duration);
                }
            }
        }

        /*/// <summary>
        /// Gets all locally controlled controllers.
        /// </summary>
        /// <returns>A collection of locally controlled controllers.</returns>
        public ICollection<DistributedController> GetAllLocallyControlledControllers()
        {
            ICollection<DistributedController> collection = new List<DistributedController>();
            foreach (DistributedController controller in this.controllers.Values)
            {
                if (controller.IsControlledLocally)
                {
                    collection.Add(controller);
                }
            }

            return collection;
        }*/

        /// <summary>
        /// Handles controller replica expired event. 
        /// </summary>
        /// <param name="id">The controller id.</param>
        public void ControllerReplicaExpiredHandler(BadumnaId id)
        {
            DistributedController contoller = null;
            if (this.controllers.TryGetValue(id, out contoller))
            {
                this.queueApplicationEvent(Apply.Func(contoller.InvokeReplicate));
            }
        }

        /// <summary>
        /// Handles the neighbour arrival event.
        /// </summary>
        /// <param name="neighborKey">The neighbor's key.</param>
        public void NeighbourArrivalNotification(HashKey neighborKey)
        {
            foreach (DistributedController controller in this.controllers.Values)
            {
                if (controller.IsControlledLocally)
                {
                    HashKey controllerToNeighborKey = controller.HashKey.AbsoluteDistanceFrom(neighborKey);
                    HashKey controllerToLocalPeer = controller.HashKey.AbsoluteDistanceFrom(this.facade.LocalHashKey);
                    if (controllerToNeighborKey < controllerToLocalPeer)
                    {
                        // Called in application thread space
                        this.queueApplicationEvent(Apply.Func(this.WakeOnAppropriatePeer, controller));
                    }
                }
            }
        }

        /// <summary>
        /// Sleeps all controllers.
        /// </summary>
        public void SleepAll()
        {
            foreach (DistributedController controller in this.controllers.Values)
            {
                if (controller.IsControlledLocally)
                {
                    this.queueApplicationEvent(Apply.Func(this.WakeOnAppropriatePeer, controller));
                }
            }
        }

        /// <summary>
        /// Wakes all controllers when necessary.
        /// </summary>
        public void WakeAllControllersWhenNecessary()
        {
            foreach (DistributedController controller in this.controllers.Values)
            {
                if (!controller.IsControlledLocally)
                {
                    this.WakeController(controller, null);
                }
            }
        }

        /// <summary>
        /// Wakes the controller.
        /// </summary>
        /// <param name="controller">The controller.</param>
        /// <param name="checkpointData">The checkpoint data.</param>
        public void WakeController(DistributedController controller, byte[] checkpointData)
        {
            if (this.facade.MapsToLocalPeer(controller.HashKey, null))
            {
                if (checkpointData == null)
                {
                    // No replica specified - try and find one
                    DistributedControllerReplica replica =
                        this.facade.AccessReplica<DistributedControllerReplica>(controller.HashKey, controller.CheckpointDataId);

                    if (replica != null)
                    {
                        checkpointData = replica.CheckpointData;
                    }
                }

                this.queueApplicationEvent(Apply.Func(controller.InvokeWake, checkpointData));
            }
        }

        /// <summary>
        /// Wakes the controller on appropriate peer. This method should be called in application thread space.
        /// </summary>
        /// <param name="controller">The controller.</param>
        public void WakeOnAppropriatePeer(DistributedController controller)
        {
            if (controller.IsControlledLocally)
            {
                controller.InvokeReplicate();
                controller.InvokeSleep();

                byte[] checkpointData = controller.LastCheckpoint;
                if (checkpointData == null)
                {
                    checkpointData = new byte[0];
                }

                this.eventQueue.Push(this.SendWakeMessage, controller, checkpointData);
            }
        }

        /// <summary>
        /// Sends the wake message to the peer which is now holding the controller'key.
        /// </summary>
        /// <param name="controller">The controller.</param>
        /// <param name="checkpointData">The checkpoint data.</param>
        private void SendWakeMessage(DistributedController controller, byte[] checkpointData)
        {
            DhtEnvelope envelope = this.GetMessageFor(controller.HashKey, new QualityOfService(true));

            this.RemoteCall(envelope, this.MoveAndWakeController, controller.Guid, checkpointData);
            this.SendMessage(envelope);
        }

        /// <summary>
        /// Moves the and wake controller.
        /// </summary>
        /// <param name="controllerId">The controller id.</param>
        /// <param name="checkpointData">The checkpoint data.</param>
        [DhtProtocolMethod(DhtMethod.MoveAndWakeController)]
        private void MoveAndWakeController(BadumnaId controllerId, byte[] checkpointData)
        {
            DistributedController controller = null;
            if (this.controllers.TryGetValue(controllerId, out controller))
            {
                if (!controller.IsControlledLocally)
                {
                    this.WakeController(controller, checkpointData);
                    return;
                }
                else
                {
                    Logger.TraceInformation(LogTag.DistributedController, "Ignoring request to wake controller {0} because it is already awake.", controller.Guid);
                    return;
                }
            }

            Logger.TraceInformation(LogTag.DistributedController, "Failed to find controller with key {0}", controllerId);
        }
    }
}
