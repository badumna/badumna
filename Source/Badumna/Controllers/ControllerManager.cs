﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;

using Badumna;
using Badumna.Core;
using Badumna.Utilities;
using Badumna.DataTypes;
using Badumna.DistributedHashTable;

namespace Badumna.Controllers
{
    /// <summary>
    /// The controller manager interface. 
    /// </summary>
    interface IControllerManager
    {
        /// <summary>
        /// Registers the controller.
        /// </summary>
        /// <param name="controller">The controller.</param>
        void RegisterController(DistributedController controller);

        /// <summary>
        /// Removes the controller.
        /// </summary>
        /// <param name="controller">The controller.</param>
        void RemoveController(DistributedController controller);

        /// <summary>
        /// Processes all registered controllers.
        /// </summary>
        void Process();
    }

    /// <summary>
    /// The controller manager class is used to control the state (e.g. sleep -> awake) of controllers. 
    /// </summary>
    internal class ControllerManager : IControllerManager
    {
        /// <summary>
        /// Whether has been initialized. 
        /// </summary>
        private bool mIsInitialized;

        /// <summary>
        /// The Dht facade.
        /// </summary>
        private DhtFacade dhtFacade;

        /// <summary>
        /// The controller state manager.
        /// </summary>
        private ControllerStateManager controllerStateManager;

        /// <summary>
        /// The queue to use for scheduling events.
        /// </summary>
        private NetworkEventQueue eventQueue;

        /// <summary>
        /// Reports on network connectivity status.
        /// </summary>
        private INetworkConnectivityReporter connectivityReporter;

        /// <summary>
        /// Initializes a new instance of the <see cref="ControllerManager"/> class.
        /// </summary>
        /// <param name="facade">The Dht facade.</param>
        /// <param name="eventQueue">The event queue to use for scheduling events.</param>
        /// <param name="queueApplicationEvent">The method to use to queue events that must run in the application's thread.</param>
        /// <param name="connectivityReporter">Reports on network connectivity status.</param>
        public ControllerManager(DhtFacade facade, NetworkEventQueue eventQueue, QueueInvokable queueApplicationEvent, INetworkConnectivityReporter connectivityReporter)
            : this(facade, new ControllerStateManager(facade, eventQueue, queueApplicationEvent), eventQueue, connectivityReporter)
        {   
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ControllerManager"/> class.
        /// </summary>
        /// <param name="facade">The facade.</param>
        /// <param name="controllerStateManager">The controller state manager.</param>
        /// <param name="eventQueue">The event queue to use for scheduling events.</param>
        /// <param name="connectivityReporter">Reports on network connectivity status.</param>
        public ControllerManager(DhtFacade facade, ControllerStateManager controllerStateManager, NetworkEventQueue eventQueue, INetworkConnectivityReporter connectivityReporter)
        {
            this.eventQueue = eventQueue;
            this.connectivityReporter = connectivityReporter;
            this.mIsInitialized = false;
            this.dhtFacade = facade;
            this.controllerStateManager = controllerStateManager;

            this.connectivityReporter.NetworkOfflineEvent += this.SleepAll;
            this.connectivityReporter.NetworkShuttingDownEvent += this.SleepAll;

            this.dhtFacade.RegisterTypeForReplication<DistributedControllerReplica>(
                this.ControllerReplicaExpiredHandler,
                this.ReplicaArrivalHandler);

            this.dhtFacade.InitializationCompleteNotification += this.InitializationCompleteNotification;
            this.dhtFacade.NeighbourArrivalNotification += this.NeighbourArrivalNotification;
            this.dhtFacade.NeighbourDepartureNotification += this.NeighbourDepartureNotification;
        }

        /// <summary>
        /// Gets a value indicating whether this instance is initialized.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is initialized; otherwise, <c>false</c>.
        /// </value>
        public bool IsInitialized
        {
            get { return this.mIsInitialized; }
        }

        /// <summary>
        /// Registers the controller.
        /// </summary>
        /// <param name="controller">The controller.</param>
        public void RegisterController(DistributedController controller)
        {
            controller.DhtFacade = this.dhtFacade;
            bool added = this.controllerStateManager.RegisterController(controller);

            if (added && this.mIsInitialized)
            {
                this.controllerStateManager.WakeController(controller, null);
            }
        }

        /// <summary>
        /// Removes the controller. Could only be called in application thread space
        /// </summary>
        /// <param name="controller">The controller.</param>
        public void RemoveController(DistributedController controller)
        {
            this.controllerStateManager.RemoveController(controller);
        }

        /// <summary>
        /// Processes all registered controllers.
        /// </summary>
        public void Process()
        {
            this.controllerStateManager.Process();
        }

        /// <summary>
        /// The controller replica expired event handler. 
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="Badumna.Utilities.ExpireEventArgs"/> instance containing the event data.</param>
        public void ControllerReplicaExpiredHandler(object sender, ExpireEventArgs e)
        {
            DistributedControllerReplica replica = e.ExpiredObject as DistributedControllerReplica;

            if (replica != null)
            {
                this.controllerStateManager.ControllerReplicaExpiredHandler(replica.Guid);
            }
        }

        /// <summary>
        /// The controller replicas arrival event handler.
        /// </summary>
        /// <param name="replica">The replica.</param>
        /// <param name="isOriginal">if set to <c>true</c> [is original].</param>
        public void ReplicaArrivalHandler(IReplicatedObject replica, bool isOriginal)
        {
            // Don't need to do anything.
        }

        /// <summary>
        /// Put all locally controlled controllers to wake up on appropriate peers. 
        /// </summary>
        private void SleepAll()
        {
            this.controllerStateManager.SleepAll();
        }

        /// <summary>
        /// Initialization complete event handler.
        /// </summary>
        /// <param name="node">The node.</param>
        private void InitializationCompleteNotification(NodeInfo node)
        {
            // Check if any controllers should be controlled locally. It is delayed to allow enough time 
            // for MoveAndWakeController messages to arrive from neighbouring peers. These messages contain
            // checkpoint data so it is worth while waiting for them.
            this.eventQueue.Schedule(3000, this.DelayedCheckLocalControllers);
            this.mIsInitialized = true;
        }

        /// <summary>
        /// Neighbours arrival event handler.
        /// </summary>
        /// <param name="node">The neighbor node.</param>
        private void NeighbourArrivalNotification(NodeInfo node)
        {
            if (this.mIsInitialized)
            {
                this.controllerStateManager.NeighbourArrivalNotification(node.Key);
            }
        }

        /// <summary>
        /// Neighbours departural event handler.
        /// </summary>
        /// <param name="node">The departing neighbor</param>
        private void NeighbourDepartureNotification(NodeInfo node)
        {
            // Delay the check to allow any MoveAndWakeController calls to be made first which should happen
            // if the neighbour had a graceful exit, otherwise this notification will be significantly delayed
            // already so an extra 250ms shouldn't effect it too much.
            this.eventQueue.Schedule(250, this.DelayedCheckLocalControllers);
        }

        /// <summary>
        /// Checks all locally controlled controllers. 
        /// </summary>
        private void DelayedCheckLocalControllers()
        {
            if (this.mIsInitialized)
            {
                this.controllerStateManager.WakeAllControllersWhenNecessary();
            }
        }

        /// <summary>
        /// Wakes the controller on appropriate peer. This method should be called in application thread space.
        /// </summary>
        /// <param name="controller">The controller.</param>
        private void WakeOnAppropriatePeer(DistributedController controller)
        {
            this.controllerStateManager.WakeOnAppropriatePeer(controller);
        }
    }
}
