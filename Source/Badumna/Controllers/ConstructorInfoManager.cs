﻿//---------------------------------------------------------------------------------
// <copyright file="ConstructorInfoManager.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

using System;
using System.Reflection;
using Badumna.Utilities;

namespace Badumna.Controllers
{
    /// <summary>
    /// The manager class for constructor info. 
    /// </summary>
    internal class ConstructorInfoManager
    {
        /// <summary>
        /// Gets the constructor info by name.
        /// </summary>
        /// <param name="typeName">Name of the type.</param>
        /// <returns>The contructor info object or null on error.</returns>
        public static ConstructorInfo GetConstructorInfo(string typeName)
        {
            try
            {
                Assembly[] assemblies = System.AppDomain.CurrentDomain.GetAssemblies();
                Type controllerType = null;

                foreach (Assembly assembly in assemblies)
                {
                    controllerType = assembly.GetType(typeName);
                    if (controllerType != null)
                    {
                        break;
                    }
                }

                if (controllerType == null)
                {
                    Logger.TraceInformation(LogTag.DistributedController, "Failed to reflect type for controller {0}", typeName);
                    return null;
                }

                ConstructorInfo controllerConstructor = controllerType.GetConstructor(new System.Type[] { typeof(string) });
                if (controllerConstructor != null)
                {
                    return controllerConstructor;
                }
                else
                {
                    Logger.TraceInformation(LogTag.DistributedController, "No appropriate constructor found for controller type {0}", typeName);
                    return null;
                }
            }
            catch (Exception e)
            {
                Logger.TraceInformation(LogTag.DistributedController, "Failed to reflect controller constructor for type {0}", typeName);
                Logger.TraceException(LogLevel.Warning, e, "Failed reflection");
                return null;
            }
        }
    }
}
