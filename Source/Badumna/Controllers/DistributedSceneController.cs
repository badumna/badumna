﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

using Badumna.Utilities;
using Badumna.Chat;
using Badumna.Replication;
using Badumna.DataTypes;
using Badumna.SpatialEntities;
using Badumna.Core;
using System.Diagnostics;

namespace Badumna.Controllers
{
    /// <summary>
    /// A distributed controller with convenience methods for the control of entities within a scene.
    /// </summary>
    /// <remarks>
    /// The DistributedSceneController class provides methods for the creation, removal and migration of entities
    /// within a scene and is particularly useful for the controller of non-player characters. Because it is also a 
    /// distributed controller the process of choosing when and how to migrate/replicate is handled transparently to
    /// the developer.
    /// </remarks>
    /// <see cref="DistributedController"/>
    public abstract class DistributedSceneController : DistributedController
    {
        /// <summary>
        /// A reference to the NetworkScene that this controller is operating in.
        /// </summary>
        protected NetworkScene Scene { get; private set; }

        /// <summary>
        /// The name of the scene to which this controller belongs.
        /// </summary>
        internal string SceneName { get; set; }

        private BadumnaId mControlledEntityId;
        private uint mControlledEntityType;
        private ISpatialEntity mControlledOriginalEntity;

        private INetworkFacade networkFacade;

        /// <summary>
        /// Gets the Badumna network facade.
        /// </summary>
        /// <remarks>The facade will be available by the time that DistributedSceneController.TakeControlOfEntity is called.</remarks>
        protected INetworkFacade NetworkFacade
        {
            get { return this.networkFacade; }
        }

        internal void SetNetworkFacade(INetworkFacade network)
        {
            this.networkFacade = network;
        }

        // TODO : Need to change this or add some way of adding multiple parameters
        /// <summary>
        /// Construct a new DistributedSceneController with the specified identifier.
        /// </summary>
        /// <remarks>
        /// Each controller with the same uniqueName must have the same functionality.
        /// Exactly one DistributedSceneController instance for each unique name
        /// will be woken at any one time.
        /// </remarks>
        /// <param name="uniqueName">A unique string identifying the controller.</param>
        public DistributedSceneController(String uniqueName)
            : base(uniqueName)
        {
        }

        /// <summary>
        /// This method provides a means to construct a certain entity a single time in the entire 
        /// network.
        /// Construct an entity with the given tag. The TakeControlOfEntity() method will be called 
        /// on the instance of this controller that is currently awake. 
        /// </summary>
        /// <remarks>
        /// This method should only be called in the constructor.
        /// 
        /// The registration of entities should NOT be performed in the Wake() method as this will be called 
        /// multiple times in the lifetime of the controller and RegisterEntity() should not be called in the 
        /// constructor because the scene may not be initialized yet. This method specifies that an entity with the 
        /// given tag needs to be created on the currently awake instance of this controller and will ensure transparently
        /// that the entity continues to have a controller even as peers come and go.
        /// </remarks>
        /// <param name="entityType">The type id associated with the entity to be created.</param>
        protected void ConstructControlledEntity(uint entityType)
        {
            if (null == this.mControlledEntityId)
            {
                Badumna.DistributedHashTable.HashKey objectKey = Badumna.DistributedHashTable.HashKey.Hash(this.UniqueName);
                PeerAddress address = new PeerAddress(objectKey);
                ushort localid = (ushort)this.UniqueName.GetHashCode();

                this.mControlledEntityId = new BadumnaId(address, localid);
                this.mControlledEntityType = entityType;
            }
            else
            {
                Debug.Assert(false, "The controlled entity has been set. Each DistributedSceneController can only controll one entity.");
            }
        }

        /*
        /// <summary>
        /// Registers an original entity in the scene.
        /// </summary>
        /// <remarks> This method is takes an entity and records it as being controlled, meaning that future
        /// peers will have it passed to the <see cref="TakeControlOfEntity"/> method when they wake.
        /// This method will have no effect if this peer is not currently awake, for this reason it is recommended 
        /// that it is called only from a call to Process. It SHOULD NOT be called from the Awake() method as this 
        /// can get called on multiple machines and could lead to an explosion in the number of controlled entities.
        /// </remarks>
        /// <param name="localEntity">The local entity to register.</param>
        /// <param name="entityType">Type of the entity.</param>
        protected void AddEntity(ISpatialOriginal localEntity, uint entityType)
        {
            if (this.IsControlledLocally && localEntity != null)
            {
                localEntity.Guid = BadumnaId.GetUniqueId(this.HashKey);
                this.mControlledEntities.Add(localEntity.Guid, entityType); // TODO : Find a way to distribute this list if required.
                this.Scene.RegisterEntity(localEntity, entityType);
                this.NetworkFacade.FlagForUpdate(localEntity, new BooleanArray(true));
            }
        }*/

        /// <summary>
        /// This method provides notification that the current instance of the controller is now responsible for the 
        /// given entity. 
        /// This method takes an <see cref="ISpatialReplica"/> instance (the remote copy of the entity) and returns an <see cref="ISpatialOriginal"/> 
        /// (the new original copy of the entity).
        /// </summary>
        /// <remarks>The remoteEntity argument may be null for entities that have not yet been constructed, for eg. from a call to 
        /// <see cref="ConstructControlledEntity"/>.</remarks>
        /// <param name="entityId">This id of the entity</param>
        /// <param name="remoteEntity">The instance of the remote entity (may be null)</param>
        /// <param name="entityType">The type id associated with this entity, as passed to the ConstructControlledEntity()</param>
        /// <returns>A new ISpatialOriginal instance of the given remote entity</returns>
        protected virtual ISpatialEntity TakeControlOfEntity(BadumnaId entityId, ISpatialReplica remoteEntity, uint entityType)
        {
            return null;
        }

        internal override void InvokeWake(byte[] checkpointData)
        {
            if (this.Scene == null)
            {
                this.Scene = this.NetworkFacade.JoinScene(
                    this.SceneName,
                    this.OnInstantiateRemoteEntity,
                    this.OnRemoveEntity) as NetworkScene;

                // Call process network state to ensure that any calbacks to enter objects in the newly created scene are created before continuing.
                this.NetworkFacade.ProcessNetworkState();
            }

            if(this.mControlledEntityId != null)
            {
                this.queueApplicationEvent(Apply.Func(this.Takeover, this.mControlledEntityId, this.mControlledEntityType));
            }

            base.InvokeWake(checkpointData);
        }

        private void Takeover(BadumnaId id, uint entityType)
        {
            try
            {
                ISpatialReplica remoteEntity = null;

                // Check to see if a remote instance of the controled entity is found.
                if (this.Scene.Replicas.TryGetValue(id, out remoteEntity))
                {
                    // If so remove it because we will shortly enter it as a local entity.
                    this.Scene.RemoveReplica(remoteEntity);
                }

                ISpatialEntity localEntity = this.TakeControlOfEntity(id, remoteEntity, entityType);
                if (localEntity != null)
                {
                    this.mControlledOriginalEntity = localEntity;
                    this.mControlledOriginalEntity.Guid = id;
                    ISpatialOriginal original = localEntity as ISpatialOriginal;

                    // shortly before migration, some recent sent updates may be lost. because the migration the resent details are also 
                    // lost, which means such lost updates will never be resent. a full update is flagged here to make sure all replicas 
                    // will get a full update. 
                    if (original != null)
                    {
                        this.Scene.RegisterEntity(original, entityType);
                        this.queueApplicationEvent(Apply.Func(((INetworkFacadeInternal)this.NetworkFacade).FlagFullUpdate, original));
                    }
                    else
                    {
                        this.Scene.RegisterEntity(localEntity, entityType, localEntity.Radius, localEntity.AreaOfInterestRadius);
                        this.queueApplicationEvent(Apply.Func(((INetworkFacadeInternal)this.NetworkFacade).FlagFullUpdate, localEntity));
                    }
                }
            }
            catch (Exception e)
            {
                Logger.TraceException(LogLevel.Warning, e, "Failed to pass control to entity {0}", id);
            }
        }

        internal override void Shutdown()
        {
            if (this.Scene != null)
            {
                ISpatialEntity original = mControlledOriginalEntity;
                this.Scene.UnregisterEntity(original);
            }
        }

        internal override void InvokeSleep()
        {
            base.InvokeSleep();
            this.eventQueue.Push(this.LeaveScene);
        }

        private void LeaveScene()
        {
            if (this.Scene != null)
            {
                this.Scene.Leave();
                this.Scene = null;
            }
        }

        private ISpatialEntity OnInstantiateRemoteEntity(NetworkScene scene, BadumnaId entityId, uint entityType)
        {
            ISpatialEntity remoteEntity = null;

            try
            {
                remoteEntity = this.InstantiateRemoteEntity(entityId, entityType);
            }
            catch (Exception e)
            {
                Logger.TraceException(LogLevel.Warning, e, "Scene controller threw exception");
            }

            return remoteEntity;
        }

        /// <summary>
        /// InstantiateRemoteEntity is called when a new spatial entity arrives within the visible region of this distributed controller. 
        /// May occur when the controller is asleep.
        /// </summary>
        /// <param name="entityId">The entity id of the remote entity.</param>
        /// <param name="entityType">Type of the entity.</param>
        /// <returns>An instance of the spatial replica.</returns>
        protected abstract ISpatialEntity InstantiateRemoteEntity(BadumnaId entityId, uint entityType);

        private void OnRemoveEntity(NetworkScene scene, IReplicableEntity replica)
        {
            try
            {
                this.RemoveEntity(replica);
            }
            catch (Exception e)
            {
                Logger.TraceException(LogLevel.Warning, e, "Scene controller threw exception");
            }
        }

        /// <summary>
        /// RemoveEntity is called when an entity leaves the visible region of this distributed controller. 
        /// May occur when the controller is asleep.
        /// </summary>
        /// <param name="replica">The replica.</param>
        protected abstract void RemoveEntity(IReplicableEntity replica);
    }
}
