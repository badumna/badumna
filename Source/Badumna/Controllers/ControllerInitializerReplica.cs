﻿using System;
using System.Collections.Generic;
using System.Text;

using Badumna.Core;
using Badumna.Utilities;
using Badumna.DistributedHashTable;
using Badumna.DataTypes;

namespace Badumna.Controllers
{
    class ControllerInitializerReplica : IReplicatedObject
    {
        private String mUniqueName = String.Empty;
        public String UniqieName { get { return this.mUniqueName; } }

        private String mControllerTypeName = String.Empty;
        public String ControllerTypeName { get { return this.mControllerTypeName; } }

        private BadumnaId mCheckpointDataId;
        public BadumnaId CheckpointDataId
        {
            get { return this.mCheckpointDataId; }
            set { this.mCheckpointDataId = value; }
        }

        public ControllerInitializerReplica() 
        {
            // For IParseable
        }        

        internal ControllerInitializerReplica(String typeName, String uniqueName)
        {
            this.mUniqueName = uniqueName;
            this.mControllerTypeName = typeName;
            this.mObjectKey = HashKey.Hash(uniqueName);
            PeerAddress address = new PeerAddress(this.mObjectKey);
            ushort localid = (ushort)uniqueName.GetHashCode();
            this.mGuid = new BadumnaId(address, localid);
            this.mCheckpointDataId = new BadumnaId(address, (ushort)(localid + 1));
        }

        #region IReplicatedObject Members

        private bool ignoreModificationNumber = true;
        public bool IgnoreModificationNumber
        {
            get { return this.ignoreModificationNumber; }
        }

        private BadumnaId mGuid;
        public BadumnaId Guid
        {
            get { return this.mGuid; }
            set { this.mGuid = value; }
        }

        private CyclicalID.UShortID mModificationNumber;
        public CyclicalID.UShortID ModificationNumber
        {
            get { return this.mModificationNumber; }
            set { this.mModificationNumber = value; }
        }

        private HashKey mObjectKey;
        public HashKey Key
        {
            get { return this.mObjectKey; }
            set { this.mObjectKey = value; }
        }

        private uint mTimeToLiveSeconds;
        public uint TimeToLiveSeconds
        {
            get { return this.mTimeToLiveSeconds; }
            set { this.mTimeToLiveSeconds = value; }
        }

        public int NumberOfReplicas { get { return 5; } }


        #endregion

        #region IParseable Members

        public void ToMessage(MessageBuffer message, Type parameterType)
        {
            if (message != null)
            {
                message.Write(this.mGuid);
                message.Write(this.mCheckpointDataId);
                message.Write(this.mUniqueName);
                message.Write(this.mControllerTypeName);
            }
        }

        public void FromMessage(MessageBuffer message)
        {
            if (message != null)
            {
                this.mGuid = message.Read<BadumnaId>();
                this.mCheckpointDataId = message.Read<BadumnaId>();
                this.mUniqueName = message.ReadString();
                this.mControllerTypeName = message.ReadString();
            }
        }

        #endregion

        #region IExpireable Members

        private TimeSpan mExpirationTime;
        public TimeSpan ExpirationTime
        {
            get { return this.mExpirationTime; }
            set { this.mExpirationTime = value; }
        }

        #endregion
    }
}
