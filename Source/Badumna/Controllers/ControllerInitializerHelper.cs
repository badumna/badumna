﻿//-----------------------------------------------------------------------
// <copyright file="ControllerInitializerHelper.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2011 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

namespace Badumna.Controllers
{
    /// <summary>
    /// A helper class for the controller initializer. 
    /// </summary>
    internal class ControllerInitializerHelper
    {
        /// <summary>
        /// Checks whether the required controller name has been registered.
        /// </summary>
        /// <param name="name">The controller name.</param>
        /// <returns>Whether the controller name has been registered.</returns>
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        public static extern bool CallIsRequiredControllerNameRegistered(string name);

        /// <summary>
        /// Checks whether the required controller name has been registered.
        /// </summary>
        /// <param name="name">The controller name.</param>
        /// <returns>Whether the controller name has been registered.</returns>
        public static bool RequiredControllerNameRegistered(string name)
        {
            try
            {
                return CallIsRequiredControllerNameRegistered(name);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Checks whether controller initialization should continue.
        /// </summary>
        /// <param name="cppMode">Whether this peer is in cpp mode.</param>
        /// <param name="typeName">Name of the controller type.</param>
        /// <param name="controllerName">Name of the controller.</param>
        /// <returns>
        /// Whether controller initialization should continue
        /// </returns>
        public static bool ShouldInitializeController(bool cppMode, string typeName, string controllerName)
        {
            if (cppMode)
            {
                string name = GetTypeName(controllerName);
                
                // in cpp mode
                if (RequiredControllerNameRegistered(name))
                {
                    // the required controller type has been registered, we are a normal client peer.
                    return true;
                }
                else
                {
                    // controller type not registered, probably a cpp seed peer, a cpp overload arbitration server? 
                    return false;
                }
            }
            else
            {
                // not in cpp mode
                if (typeName == "Badumna.CppWrapperStub.DistributedControllerStub")
                {
                    // a c# service peer received a controller initialization request from cpp peer. nothing we can do. 
                    return false;
                }

                return true;
            }
        }

        /// <summary>
        /// Gets the name of the type.
        /// </summary>
        /// <param name="uniqueName">The unique name of controller.</param>
        /// <returns>The type name.</returns>
        public static string GetTypeName(string uniqueName)
        {
            string[] parts = uniqueName.Split('^');
            if (parts.Length == 2 && parts[1] != null)
            {
                string[] parts2 = parts[1].Split('_');
                if (parts2.Length == 2 && parts2[0] != null)
                {
                    return parts2[0];
                }
            }

            throw new ArgumentException("invalid unique name");
        }
    }
}
