using System;
using System.Collections.Generic;
using System.Text;

using Badumna.Utilities.LitJson;

namespace Badumna.DataTypes
{
    /// <summary>
    /// A 3 dimensional vector.
    /// </summary>
    [Serializable]
    public struct Vector3 
    {
        static Vector3()
        {
            JsonMapper.RegisterExporter(
                delegate(Vector3 vector, JsonWriter writer)
                {
                    writer.WriteObjectStart();
                    writer.WritePropertyName("X");
                    writer.Write(vector.X);
                    writer.WritePropertyName("Y");
                    writer.Write(vector.Y);
                    writer.WritePropertyName("Z");
                    writer.Write(vector.Z);
                    writer.WriteObjectEnd();
                });
        }

        /// <summary>
        /// Return an x unit vector (1, 0, 0).
        /// </summary>
        public static Vector3 UnitX { get { return new Vector3(1, 0, 0); } }

        /// <summary>
        /// Return a y unit vector (0, 1, 0).
        /// </summary>
        public static Vector3 UnitY { get { return new Vector3(0, 1, 0); } }

        /// <summary>
        /// Return a z unit vector (0, 0, 1).
        /// </summary>
        public static Vector3 UnitZ { get { return new Vector3(0, 0, 1); } }

        /// <summary>
        /// Return a vector with each component set to one.
        /// </summary>
        public static Vector3 One { get { return new Vector3(1, 1, 1); } }

        /// <summary>
        /// Return a vector with each component set to zero.
        /// </summary>
        public static Vector3 Zero { get { return new Vector3();} }

        internal static Vector3 FromJson(JsonData jsonData)
        {
            return new Vector3((float)jsonData["X"], (float)jsonData["Y"], (float)jsonData["Z"]);
        }


        private float mX;
        /// <summary>
        /// The X component of the vector.
        /// </summary>
        public float X
        {
            get { return this.mX; }
            set { this.mX = value; }
        }

        private float mY;        
        /// <summary>
        /// The Y component of the vector.
        /// </summary>
        public float Y
        {
            get { return this.mY; }
            set { this.mY = value; }
        }

        private float mZ;
        /// <summary>
        /// The Z component of the vector.
        /// </summary>
        public float Z
        {
            get { return this.mZ; }
            set { this.mZ = value; }
        }

        /// <summary>
        /// The length of the vector
        /// </summary>
        public float Magnitude { get { return (float)Math.Sqrt(this.mX * this.mX + this.mY * this.mY + this.mZ * this.mZ); } }

        /// <summary>
        /// Contructor
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        public Vector3(float x, float y, float z)
        {
            this.mX = x;
            this.mY = y;
            this.mZ = z;
        }

        /// <summary>
        /// Dot product of this and the given vector
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public float Dot(Vector3 other)
        {
            return Vector3.DotProduct(this, other);
        }

        /// <summary>
        /// Normalize this vector
        /// </summary>
        /// <returns></returns>
        public Vector3 Normalize()
        {
            return Vector3.Normalize(this);
        }

        /// <summary>
        /// Return the normalization of a given vector without changing the original
        /// </summary>
        /// <param name="vect"></param>
        /// <returns></returns>
        public static Vector3 Normalize(Vector3 vect)
        {
            double magnitude = vect.Magnitude;

            if (0 == magnitude)
            {
                return new Vector3(0, 0, 0);
            }

            return vect / vect.Magnitude;
        }

        /// <summary>
        /// Dot product of two given vectors
        /// </summary>
        /// <param name="vectorA"></param>
        /// <param name="vectorB"></param>
        /// <returns></returns>
        public static float DotProduct(Vector3 vectorA, Vector3 vectorB)
        {
            return vectorA.X * vectorB.X + vectorA.Y * vectorB.Y + vectorA.Z * vectorB.Z;
        }

        /// <summary>
        /// Addition of two vectors
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static Vector3 operator +(Vector3 left, Vector3 right)
        {
            return new Vector3(left.X + right.X, left.Y + right.Y, left.Z + right.Z);
        }

        /// <summary>
        /// Subtraction of two vectors (left - right)
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static Vector3 operator -(Vector3 left, Vector3 right)
        {
            return new Vector3(left.X - right.X, left.Y - right.Y, left.Z - right.Z);
        }

        /// <summary>
        /// Multiplication of a given vector
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static Vector3 operator *(Vector3 left, float right)
        {
            return new Vector3(left.X * right, left.Y * right, left.Z * right);
        }

        /// <summary>
        /// Multiplication of a given vector
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static Vector3 operator *(float left, Vector3 right)
        {
            return new Vector3(right.X * left, right.Y * left, right.Z * left);
        }

        /// <summary>
        /// Division of a given vector
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static Vector3 operator /(Vector3 left, float right)
        {
            return new Vector3(left.X / right, left.Y / right, left.Z / right);
        }

        /// <summary>
        /// Equality operator
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static bool operator ==(Vector3 left, Vector3 right)
        {
            return left.mX == right.mX && left.mY == right.mY && left.mZ == right.mZ; 
        }

        /// <summary>
        /// Inequality operator
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static bool operator !=(Vector3 left, Vector3 right)
        {
            return left.mX != right.mX || left.mY != right.mY || left.mZ != right.mZ;
        }
        
        /// <summary>
        /// Returns a value indicating whether the given object is equal to this instance.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns>True is the given object equals this Vector3 instance</returns>
        public override bool Equals(Object obj)
        {
            if (null == obj || !(obj is Vector3))
            {
                return false;
            }

            return this.Equals((Vector3)obj);
        }

        /// <summary>
        /// Returns the hash code of this instance.
        /// </summary>
        /// <remarks>As a warning treat as an error, removing override GetHashCode method and 
        /// keep the override Equals method will cause the build to fail. </remarks>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return base.GetHashCode(); 
        }

        /// <summary>
        /// eturns a value indicating whether the given Vector3 is equal to this instance.
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public bool Equals(Vector3 other)
        {
            return this.X == other.X && this.Y == other.Y && this.Z == other.Z;
        }

        /// <summary>
        /// Returns a human readable string represenation of the vector.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return String.Format(System.Globalization.NumberFormatInfo.CurrentInfo, "<{0},{1},{2}>", this.X, this.Y, this.Z);
        }
    }
}
