﻿//-----------------------------------------------------------------------
// <copyright file="BadumnaId.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2011 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------  

namespace Badumna.DataTypes
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;

    using Badumna.Core;
    using Badumna.Utilities;

    /// <summary>
    /// A globably unique identifier.
    /// </summary>
    public class BadumnaId : IParseable, IComparable<BadumnaId>, IEquatable<BadumnaId>
    {
        /// <summary>
        /// The local ID part of the ID.
        /// </summary>
        private ushort localId;

        /// <summary>
        /// The address part of the ID.
        /// </summary>
        private PeerAddress address;

        /// <summary>
        /// The address of the peer currently hosting the item the ID refers to.
        /// </summary>
        /// <remarks>If this is null, the item referred to by the ID is assumed to be hosted on the machine
        /// that created the ID whose address forms the address part of the ID.</remarks>
        private PeerAddress hostAddress;

        /// <summary>
        /// Initializes a new instance of the BadumnaId class.
        /// </summary>
        /// <remarks>Copy constuctor.</remarks>
        /// <param name="copy">The BadumnaId to copy.</param>
        public BadumnaId(BadumnaId copy)
            : this(copy.address, copy.localId)
        {
            this.hostAddress = copy.hostAddress;
        }

        /// <summary>
        /// Initializes a new instance of the BadumnaId class.
        /// </summary>
        /// <remarks>
        /// Do not use this constructor, it only exists for IParseable. Use UniqueNetworkId.GetNextId() instead.
        /// </remarks>
        internal BadumnaId()
        {
        }

        /// <summary>
        /// Initializes a new instance of the BadumnaId class.
        /// </summary>
        /// <param name="address">The public address of the creating machine.</param>
        /// <param name="localId">A local ID distinguishing this BadumnaId from others created on the same machine.</param>
        internal BadumnaId(PeerAddress address, ushort localId)
        {
            this.address = new PeerAddress(address);
            this.localId = localId;
        }

        /// <summary>
        /// Gets an empty identifier. 
        /// </summary>
        public static BadumnaId None
        {
            get { return new BadumnaId(PeerAddress.Nowhere, 0); }
        }        

        /// <summary>
        /// Gets a value indicating whether the current BadumnaId is validly formed (i.e. not equal to BadumnaId.None).
        /// </summary>
        /// <remarks>
        /// TODO:  How is this used?  It should probably check that Address is also valid?
        ///        At the moment it seems like it could accidentally return false if the
        ///        public address hasn't been discovered yet and the local id happened to be 0.
        ///        Should it always return false if the public address hasn't bee set?
        /// </remarks>
        public bool IsValid
        {
            get { return this.CompareTo(BadumnaId.None) != 0; }
        }

        /// <summary>
        /// Gets an id that is unique only on the local peer.
        /// </summary>
        internal ushort LocalId
        {
            get { return this.localId; }
        }

        /// <summary>
        /// Gets or sets the original address of the ID.
        /// </summary>
        internal PeerAddress OriginalAddress
        {
            get { return this.address; }
            set { this.address = value; }
        }

        /// <summary>
        /// Gets or sets the address of the peer that is currently hosting the item the ID refers to.
        /// </summary>
        /// <remarks>
        /// Defaults to the address the instance was created with.
        /// Setting a new address will not change the value of the Badumna ID (as used in equality tests
        /// and comparisons).
        /// </remarks>
        internal PeerAddress Address
        {
            get
            {
                if (this.hostAddress == null)
                {
                    return this.address;
                }
                else
                {
                    return this.hostAddress;
                }
            }

            set
            {
                this.hostAddress = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the ID was generated locally.
        /// </summary>
        /// <remarks>
        /// This is only intended to be set by BadumnaIdAllocator.
        /// TODO: Refactor this to eliminate the non-private setter.
        /// </remarks>
        internal bool GeneratedLocally { get; set; }  // Not sent across the wire, so remote copies always have false.

        /// <summary>
        /// Inequality operator. Returns true if the two given BadumnaIds are not equal.
        /// </summary>
        /// <param name="a">First comparand.</param>
        /// <param name="b">Second comparand.</param>
        /// <returns>True if a and b are not equal</returns>
        public static bool operator !=(BadumnaId a, BadumnaId b)
        {
            bool result;
            if (object.Equals(null, a))
            {
                result = !object.Equals(null, b);
            }
            else
            {
                result = !a.Equals(b);
            }

            return result;
        }

        /// <summary>
        /// Equality operator. Returns true only if the two given BadumnaIds are equal
        /// </summary>
        /// <param name="a">First comparand.</param>
        /// <param name="b">Second comparand.</param>
        /// <returns>True if a and b are equal</returns>
        public static bool operator ==(BadumnaId a, BadumnaId b)
        {
            bool result;
            if (object.Equals(null, a))
            {
                result = object.Equals(null, b);
            }
            else
            {
                result = a.Equals(b);
            }

            return result;
        }

        /// <summary>
        /// Copies the value from another BadumnaId object.
        /// </summary>
        /// <param name="other">The other BadumnaId object.</param>
        public void CopyFrom(BadumnaId other)
        {
            // TODO: Does not appear to be used. Remove?
            this.address = new PeerAddress(other.address);
            this.localId = other.localId;
        }

        /// <summary>
        /// Renders a BadumnaId as a string for debugging purposes.  No guarantee is made that the format
        /// of this string will not change.  BadumnaId should be treated as an opaque identifier.
        /// </summary>
        /// <returns>A string that represents the current BadumnaId.</returns>
        /// <remarks>
        /// TODO: This shouldn't print out the ip address!  Probably shouldn't print out anything... BadumnaId should
        ///        be as opaque as possible.
        /// </remarks> 
        public override string ToString()
        {
            string addressString = this.address != null ? this.address.ToString() : "UnknownAddress";
            if (this.hostAddress == null)
            {
                return string.Format("{0}-{1}", addressString, this.localId);
            }
            else
            {
                return string.Format("{0}-{1}-{2}", addressString, this.localId, this.hostAddress);
            }
        }

        /// <summary>
        /// Gets the hash code for the instance.
        /// </summary>
        /// <returns>A hash code for the current BadumnaId.</returns>
        public override int GetHashCode()
        {
            // This may cause excessive hash collisions, but it's important that the hash code doesn't change for
            // a given instance if, e.g., the peer address changes.
            return (int)this.LocalId;
        }

        /// <summary>
        /// Compare with another object for value equality.
        /// </summary>
        /// <param name="other">The object to compare with.</param>
        /// <returns><c>true</c> if the other object is a BadumnaId with equal value, otherwise <c>false</c>.</returns>
        public override bool Equals(object other)
        {
            return this.CompareTo(other as BadumnaId) == 0;
        }

        /// <summary>
        /// Compare with another BadumnaId for value equality.
        /// </summary>
        /// <param name="other">The BadumnaId to compare with</param>
        /// <returns>True if the instances represent the same id, false otherwise.</returns>
        public bool Equals(BadumnaId other)
        {
            return this.CompareTo(other) == 0;
        }

        /// <summary>
        /// Imposes a consistent but arbitrary order on BadumnaIds so they can be used in
        /// binary searches, etc.
        /// </summary>
        /// <param name="other">The other BadumnaId to compare with.</param>
        /// <returns>An integer less than zero, zero, or an integer greater than zero depending on
        /// whether this BadumnaId is less than, equal to, or greater than the other BadumanId respectively.</returns>
        public int CompareTo(BadumnaId other)
        {
            int result;

            if (other == null)
            {
                result = 1;  // IComparable spec says everything is greater than null
            }
            else
            {
                result = this.address.CompareTo(other.address);

                // Need to check the is generated locally property because the peers address might have changed,
                // which means address comparison will fail.
                // TODO: If the addressComparison fails, but they were both generated locally, we should see
                //       why the addresses haven't been updated to the current public address.
                if (result == 0 || (this.GeneratedLocally && other.GeneratedLocally))
                {
                    result = this.localId.CompareTo(other.localId);
                }
            }

            return result;
        }

        /// <summary>
        /// Create a unique string ID that will remain invariant.
        /// </summary>
        /// <exception cref="System.InvalidOperationException">Thrown if the local address is not set yet.</exception>
        /// <returns>A unique string ID that will remain invariant.</returns>
        public virtual string ToInvariantIDString()
        {
            if (this.address == null)
            {
                throw new InvalidOperationException("Cannot create invariant ID string before address is known.");
            }

            string addressString = this.address.ToString();
            return string.Format("{0}-{1}", addressString, this.localId);
        }

        /// <summary>
        /// Serializes the BadumnaId to a message buffer.
        /// </summary>
        /// <param name="message">The message buffer to write to.</param>
        /// <param name="parameterType">Unused parameter required by interface.</param>
        void IParseable.ToMessage(MessageBuffer message, Type parameterType)
        {
            Debug.Assert(this.address.IsValid, "Attempt to serialize invalid BadumnaId");

            if (null != message)
            {
                this.address.ToMessage(message, typeof(PeerAddress));
                message.Write(this.localId);
                if (this.hostAddress == null)
                {
                    message.Write(false);
                }
                else
                {
                    message.Write(true);
                    this.hostAddress.ToMessage(message, typeof(PeerAddress));
                }
            }
        }

        /// <summary>
        /// Initializes the BadumnaId with state read from a message buffer.
        /// </summary>
        /// <param name="message">The message buffer to read from.</param>
        void IParseable.FromMessage(MessageBuffer message)
        {
            if (null != message)
            {
                this.address = message.Read<PeerAddress>();
                this.localId = message.ReadUShort();
                bool hostSpecified = message.ReadBool();
                if (hostSpecified)
                {
                    this.hostAddress = message.Read<PeerAddress>();
                }
            }
        }

        /// <summary>
        /// Try and create create a BadumnaID using state parsed from a string.
        /// </summary>
        /// <param name="idString">The string describing a BadumnaId.</param>
        /// <returns>A new BadumanId or null, if the string was not correctliy formatted.</returns>
        internal static BadumnaId TryParse(string idString)
        {
            if (idString == null)
            {
                return null;
            }

            string[] parts = idString.Split('-');
            if (parts.Length < 2 || parts.Length > 3)
            {
                return null;
            }

            PeerAddress address = PeerAddress.FromString(parts[0]);
            if (address == null)
            {
                return null;
            }

            ushort localId;
            if (!ushort.TryParse(parts[1], out localId))
            {
                return null;
            }

            BadumnaId id = new BadumnaId(address, localId);

            if (parts.Length == 3)
            {
                PeerAddress hostAddress = PeerAddress.FromString(parts[2]);
                if (hostAddress == null)
                {
                    return null;
                }

                id.hostAddress = hostAddress;
            }

            return id;
        }

        /// <summary>
        /// Serialize this ID to a byte array.
        /// </summary>
        /// <returns>A new byte array containgin the serialized BadumnaId.</returns>
        internal byte[] ToBytes()
        {
            using (MessageBuffer buffer = new MessageBuffer())
            {
                ((IParseable)this).ToMessage(buffer, typeof(BadumnaId));
                return buffer.GetBuffer();
            }
        }

        /// <summary>
        /// Initialize this BadumnaId with state deserialized from a byte array.
        /// </summary>
        /// <param name="serializedBadumnaId">A byte array containing the serialized state of a BadumnaID.</param>
        internal void FromBytes(byte[] serializedBadumnaId)
        {
            using (MessageBuffer buffer = new MessageBuffer(serializedBadumnaId))
            {
                ((IParseable)this).FromMessage(buffer);
            }
        }
    }
}
