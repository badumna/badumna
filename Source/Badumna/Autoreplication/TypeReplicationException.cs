﻿// ------------------------------------------------------------------------------
//  <copyright file="TypeReplicationException.cs" company="National ICT Australia Limited">
//      Copyright (c) 2012-2012 National ICT Australia Limited. All rights reserved.
//  </copyright>
// ------------------------------------------------------------------------------
namespace Badumna.Autoreplication
{
    using System;
    using System.Runtime.Serialization;
    using Badumna.Core;

    /// <summary>
    /// Exception indicating that an entity has been found with a replicable property of a type
    /// that has not been registered with Badumna for replication.
    /// </summary>
    [Serializable]
    public sealed class TypeReplicationException : BadumnaException
    {
        /// <summary>
        /// Initializes a new instance of the TypeReplicationException class for a named type.
        /// </summary>
        /// <param name="type">The unregistered type.</param>
        internal TypeReplicationException(Type type)
            : this("Attempt to replicate unregistered type: " + type)
        {
        }

        /// <summary>
        /// Initializes a new instance of the TypeReplicationException class.
        /// </summary>
        internal TypeReplicationException()
            : this("Attempt to replicate unregistered type.")
        {
        }

        /// <summary>
        /// Initializes a new instance of the TypeReplicationException class.
        /// </summary>
        /// <param name="message">A message describing the reason for the exception being thrown.</param>
        internal TypeReplicationException(string message)
            : this(message, null)
        {
        }

        /// <summary>
        /// Initializes a new instance of the TypeReplicationException class.
        /// </summary>
        /// <param name="message">A message describing the reason for the exception being thrown.</param>
        /// <param name="innerException">The inner exception that resulted in this exception being thrown.</param>
        internal TypeReplicationException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        /// <summary>
        /// Initializes a new instance of the TypeReplicationException class by deserialization.
        /// </summary>
        /// <param name="serializationInfo">The serialized exception information.</param>
        /// <param name="streamingContext">The streaming context.</param>
        private TypeReplicationException(SerializationInfo serializationInfo, StreamingContext streamingContext)
            : base(serializationInfo, streamingContext)
        {
        }         
    }
}