﻿// -----------------------------------------------------------------------
// <copyright file="MatchAutoreplicationManager.cs" company="Scalify">
//     Copyright (c) 2013 Scalify Pty Ltd. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace Badumna.Autoreplication
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Text;
    using Badumna.Autoreplication.Serialization;
    using Badumna.Core;
    using Badumna.Replication;

    /// <summary>
    /// A delegate for sending a serialized RPC call.
    /// </summary>
    /// <param name="stream">A steam containing the serialized RPC call.</param>
    internal delegate void RPCSendMethod(MemoryStream stream);

    /// <summary>
    /// Concrete MatchAutoreplicationManager derived class, explicitly derived a concrete class from the generic Manager class due to AOT limitation in Unity.
    /// </summary>
    internal class MatchAutoreplicationManager : Manager<object, IOriginal, IReplica, IOriginalEntityWrapper, IReplicaEntityWrapper, KeyValuePair<float, float>, Replication.EntityManager>
    {
        /// <summary>
        /// Initializes a new instance of the MatchAutoreplicationManager class.
        /// </summary>
        /// <param name="serialization">For manipulating replicable types.</param>
        /// <param name="timeKeeper">Provides access to current time.</param>
        /// <param name="originalWrapperFactory">Factory for creating original wrappers.</param>
        /// <param name="replicaWrapperFactory">Factory for creating replica wrappers.</param>
        public MatchAutoreplicationManager(
           Serialization.Manager serialization,
           ITime timeKeeper,
           OriginalWrapperFactory<object, IOriginalEntityWrapper, KeyValuePair<float, float>> originalWrapperFactory,
           ReplicaWrapperFactory<object, IReplicaEntityWrapper> replicaWrapperFactory)
            : base(serialization, timeKeeper, originalWrapperFactory, replicaWrapperFactory)
        {
        }

        /// <summary>
        /// Call a method on a remote controller.
        /// </summary>
        /// <param name="callDispatcher">A delegate for sending the serialized RPC.</param>
        /// <param name="method">The method to remotely call.</param>
        public void CallMethodOnController(
            RPCSendMethod callDispatcher,
            RpcSignature method)
        {
            IReplicaEntityWrapper wrapper;
            if (!this.GetReplicaWrapperForTarget(method, out wrapper))
            {
                return;
            }

            this.DispatchRPC(callDispatcher, wrapper, method);
        }

        /// <summary>
        /// Call a method on a remote controller.
        /// </summary>
        /// <typeparam name="T1">The type of the method's first parameter.</typeparam>
        /// <param name="callDispatcher">A delegate for sending the serialized RPC.</param>
        /// <param name="method">The method to remotely call.</param>
        /// <param name="arg1">The first parameter.</param>
        public void CallMethodOnController<T1>(
            RPCSendMethod callDispatcher,
            RpcSignature<T1> method,
            T1 arg1)
        {
            IReplicaEntityWrapper wrapper;
            if (!this.GetReplicaWrapperForTarget(method, out wrapper))
            {
                return;
            }

            this.DispatchRPC(callDispatcher, wrapper, method, arg1);
        }

        /// <summary>
        /// Call a method on a remote controller.
        /// </summary>
        /// <typeparam name="T1">The type of the method's first parameter.</typeparam>
        /// <typeparam name="T2">The type of the method's second parameter.</typeparam>
        /// <param name="callDispatcher">A delegate for sending the serialized RPC.</param>
        /// <param name="method">The method to remotely call.</param>
        /// <param name="arg1">The first parameter.</param>
        /// <param name="arg2">The second parameter.</param>
        public void CallMethodOnController<T1, T2>(
            RPCSendMethod callDispatcher,
            RpcSignature<T1, T2> method,
            T1 arg1,
            T2 arg2)
        {
            IReplicaEntityWrapper wrapper;
            if (!this.GetReplicaWrapperForTarget(method, out wrapper))
            {
                return;
            }

            this.DispatchRPC(callDispatcher, wrapper, method, arg1, arg2);
        }

        /// <summary>
        /// Call a method on a remote controller.
        /// </summary>
        /// <typeparam name="T1">The type of the method's first parameter.</typeparam>
        /// <typeparam name="T2">The type of the method's second parameter.</typeparam>
        /// <typeparam name="T3">The type of the method's third parameter.</typeparam>
        /// <param name="callDispatcher">A delegate for sending the serialized RPC.</param>
        /// <param name="method">The method to remotely call.</param>
        /// <param name="arg1">The first parameter.</param>
        /// <param name="arg2">The second parameter.</param>
        /// <param name="arg3">The third parameter.</param>
        public void CallMethodOnController<T1, T2, T3>(
            RPCSendMethod callDispatcher,
            RpcSignature<T1, T2, T3> method,
            T1 arg1,
            T2 arg2,
            T3 arg3)
        {
            IReplicaEntityWrapper wrapper;
            if (!this.GetReplicaWrapperForTarget(method, out wrapper))
            {
                return;
            }

            this.DispatchRPC(callDispatcher, wrapper, method, arg1, arg2, arg3);
        }

        /// <summary>
        /// Call a method on a remote controller.
        /// </summary>
        /// <typeparam name="T1">The type of the method's first parameter.</typeparam>
        /// <typeparam name="T2">The type of the method's second parameter.</typeparam>
        /// <typeparam name="T3">The type of the method's third parameter.</typeparam>
        /// <typeparam name="T4">The type of the method's fourth parameter.</typeparam>
        /// <param name="callDispatcher">A delegate for sending the serialized RPC.</param>
        /// <param name="method">The method to remotely call.</param>
        /// <param name="arg1">The first parameter.</param>
        /// <param name="arg2">The second parameter.</param>
        /// <param name="arg3">The third parameter.</param>
        /// <param name="arg4">The fourth parameter.</param>
        public void CallMethodOnController<T1, T2, T3, T4>(
            RPCSendMethod callDispatcher,
            RpcSignature<T1, T2, T3, T4> method,
            T1 arg1,
            T2 arg2,
            T3 arg3,
            T4 arg4)
        {
            IReplicaEntityWrapper wrapper;
            if (!this.GetReplicaWrapperForTarget(method, out wrapper))
            {
                return;
            }

            this.DispatchRPC(callDispatcher, wrapper, method, arg1, arg2, arg3, arg4);
        }

        /// <summary>
        /// Call a method on a remote controller.
        /// </summary>
        /// <typeparam name="T1">The type of the method's first parameter.</typeparam>
        /// <typeparam name="T2">The type of the method's second parameter.</typeparam>
        /// <typeparam name="T3">The type of the method's third parameter.</typeparam>
        /// <typeparam name="T4">The type of the method's fourth parameter.</typeparam>
        /// <typeparam name="T5">The type of the method's fifth parameter.</typeparam>
        /// <param name="callDispatcher">A delegate for sending the serialized RPC.</param>
        /// <param name="method">The method to remotely call.</param>
        /// <param name="arg1">The first parameter.</param>
        /// <param name="arg2">The second parameter.</param>
        /// <param name="arg3">The third parameter.</param>
        /// <param name="arg4">The fourth parameter.</param>
        /// <param name="arg5">The fifth parameter.</param>
        public void CallMethodOnController<T1, T2, T3, T4, T5>(
            RPCSendMethod callDispatcher,
            RpcSignature<T1, T2, T3, T4, T5> method,
            T1 arg1,
            T2 arg2,
            T3 arg3,
            T4 arg4,
            T5 arg5)
        {
            IReplicaEntityWrapper wrapper;
            if (!this.GetReplicaWrapperForTarget(method, out wrapper))
            {
                return;
            }

            this.DispatchRPC(callDispatcher, wrapper, method, arg1, arg2, arg3, arg4, arg5);
        }

        /// <summary>
        /// Call a method on a remote controller.
        /// </summary>
        /// <typeparam name="T1">The type of the method's first parameter.</typeparam>
        /// <typeparam name="T2">The type of the method's second parameter.</typeparam>
        /// <typeparam name="T3">The type of the method's third parameter.</typeparam>
        /// <typeparam name="T4">The type of the method's fourth parameter.</typeparam>
        /// <typeparam name="T5">The type of the method's fifth parameter.</typeparam>
        /// <typeparam name="T6">The type of the method's sixth parameter.</typeparam>
        /// <param name="callDispatcher">A delegate for sending the serialized RPC.</param>
        /// <param name="method">The method to remotely call.</param>
        /// <param name="arg1">The first parameter.</param>
        /// <param name="arg2">The second parameter.</param>
        /// <param name="arg3">The third parameter.</param>
        /// <param name="arg4">The fourth parameter.</param>
        /// <param name="arg5">The fifth parameter.</param>
        /// <param name="arg6">The sixth parameter.</param>
        public void CallMethodOnController<T1, T2, T3, T4, T5, T6>(
            RPCSendMethod callDispatcher,
            RpcSignature<T1, T2, T3, T4, T5, T6> method,
            T1 arg1,
            T2 arg2,
            T3 arg3,
            T4 arg4,
            T5 arg5,
            T6 arg6)
        {
            IReplicaEntityWrapper wrapper;
            if (!this.GetReplicaWrapperForTarget(method, out wrapper))
            {
                return;
            }

            this.DispatchRPC(callDispatcher, wrapper, method, arg1, arg2, arg3, arg4, arg5, arg6);
        }

        /// <summary>
        /// Call a parameterless method on all the replicas of a given original.
        /// </summary>
        /// <param name="method">The corresponding method on the original.</param>
        public new void CallMethodOnReplicas(RpcSignature method)
        {
            IOriginalEntityWrapper wrapper = null;
            if (!this.GetOriginalWrapperForTarget(method, out wrapper))
            {
                // Silently ignore unknown originals as they cannot have replicas yet.
                return;
            }

            this.DispatchRPC(s => this.WrappedEntityManager.SendCustomMessageToReplicas(wrapper, s), wrapper, method);
        }

        /// <summary>
        /// Call a method on all the replicas of a given original.
        /// </summary>
        /// <typeparam name="T1">The type of the method's first parameter.</typeparam>
        /// <param name="method">The corresponding method on the original.</param>
        /// <param name="arg1">The first parameter.</param>
        public new void CallMethodOnReplicas<T1>(RpcSignature<T1> method, T1 arg1)
        {
            IOriginalEntityWrapper wrapper = null;
            if (!this.GetOriginalWrapperForTarget(method, out wrapper))
            {
                // Silently ignore unknown originals as they cannot have replicas yet.
                return;
            }

            this.DispatchRPC(s => this.WrappedEntityManager.SendCustomMessageToReplicas(wrapper, s), wrapper, method, arg1);
        }

        /// <summary>
        /// Call a method on all the replicas of a given original.
        /// </summary>
        /// <typeparam name="T1">The type of the method's first parameter.</typeparam>
        /// <typeparam name="T2">The type of the method's second parameter.</typeparam>
        /// <param name="method">The corresponding method on the original.</param>
        /// <param name="arg1">The first parameter.</param>
        /// <param name="arg2">The second parameter.</param>
        public new void CallMethodOnReplicas<T1, T2>(RpcSignature<T1, T2> method, T1 arg1, T2 arg2)
        {
            IOriginalEntityWrapper wrapper = null;
            if (!this.GetOriginalWrapperForTarget(method, out wrapper))
            {
                // Silently ignore unknown originals as they cannot have replicas yet.
                return;
            }

            this.DispatchRPC(s => this.WrappedEntityManager.SendCustomMessageToReplicas(wrapper, s), wrapper, method, arg1, arg2);
        }

        /// <summary>
        /// Call a method on all the replicas of a given original.
        /// </summary>
        /// <typeparam name="T1">The type of the method's first parameter.</typeparam>
        /// <typeparam name="T2">The type of the method's second parameter.</typeparam>
        /// <typeparam name="T3">The type of the method's third parameter.</typeparam>
        /// <param name="method">The corresponding method on the original.</param>
        /// <param name="arg1">The first parameter.</param>
        /// <param name="arg2">The second parameter.</param>
        /// <param name="arg3">The third parameter.</param>
        public new void CallMethodOnReplicas<T1, T2, T3>(
            RpcSignature<T1, T2, T3> method,
            T1 arg1,
            T2 arg2,
            T3 arg3)
        {
            IOriginalEntityWrapper wrapper = null;
            if (!this.GetOriginalWrapperForTarget(method, out wrapper))
            {
                // Silently ignore unknown originals as they cannot have replicas yet.
                return;
            }

            this.DispatchRPC(s => this.WrappedEntityManager.SendCustomMessageToReplicas(wrapper, s), wrapper, method, arg1, arg2, arg3);
        }

        /// <summary>
        /// Call a method on all the replicas of a given original.
        /// </summary>
        /// <typeparam name="T1">The type of the method's first parameter.</typeparam>
        /// <typeparam name="T2">The type of the method's second parameter.</typeparam>
        /// <typeparam name="T3">The type of the method's third parameter.</typeparam>
        /// <typeparam name="T4">The type of the method's fourth parameter.</typeparam>
        /// <param name="method">The corresponding method on the original.</param>
        /// <param name="arg1">The first parameter.</param>
        /// <param name="arg2">The second parameter.</param>
        /// <param name="arg3">The third parameter.</param>
        /// <param name="arg4">The fourth parameter.</param>
        public new void CallMethodOnReplicas<T1, T2, T3, T4>(
            RpcSignature<T1, T2, T3, T4> method,
            T1 arg1,
            T2 arg2,
            T3 arg3,
            T4 arg4)
        {
            IOriginalEntityWrapper wrapper = null;
            if (!this.GetOriginalWrapperForTarget(method, out wrapper))
            {
                // Silently ignore unknown originals as they cannot have replicas yet.
                return;
            }

            this.DispatchRPC(s => this.WrappedEntityManager.SendCustomMessageToReplicas(wrapper, s), wrapper, method, arg1, arg2, arg3, arg4);
        }

        /// <summary>
        /// Call a method on all the replicas of a given original.
        /// </summary>
        /// <typeparam name="T1">The type of the method's first parameter.</typeparam>
        /// <typeparam name="T2">The type of the method's second parameter.</typeparam>
        /// <typeparam name="T3">The type of the method's third parameter.</typeparam>
        /// <typeparam name="T4">The type of the method's fourth parameter.</typeparam>
        /// <typeparam name="T5">The type of the method's fifth parameter.</typeparam>
        /// <param name="method">The corresponding method on the original.</param>
        /// <param name="arg1">The first parameter.</param>
        /// <param name="arg2">The second parameter.</param>
        /// <param name="arg3">The third parameter.</param>
        /// <param name="arg4">The fourth parameter.</param>
        /// <param name="arg5">The fifth parameter.</param>
        public new void CallMethodOnReplicas<T1, T2, T3, T4, T5>(
            RpcSignature<T1, T2, T3, T4, T5> method,
            T1 arg1,
            T2 arg2,
            T3 arg3,
            T4 arg4,
            T5 arg5)
        {
            IOriginalEntityWrapper wrapper = null;
            if (!this.GetOriginalWrapperForTarget(method, out wrapper))
            {
                // Silently ignore unknown originals as they cannot have replicas yet.
                return;
            }

            this.DispatchRPC(s => this.WrappedEntityManager.SendCustomMessageToReplicas(wrapper, s), wrapper, method, arg1, arg2, arg3, arg4, arg5);
        }

        /// <summary>
        /// Call a method on all the replicas of a given original.
        /// </summary>
        /// <typeparam name="T1">The type of the method's first parameter.</typeparam>
        /// <typeparam name="T2">The type of the method's second parameter.</typeparam>
        /// <typeparam name="T3">The type of the method's third parameter.</typeparam>
        /// <typeparam name="T4">The type of the method's fourth parameter.</typeparam>
        /// <typeparam name="T5">The type of the method's fifth parameter.</typeparam>
        /// <typeparam name="T6">The type of the method's sixth parameter.</typeparam>
        /// <param name="method">The corresponding method on the original.</param>
        /// <param name="arg1">The first parameter.</param>
        /// <param name="arg2">The second parameter.</param>
        /// <param name="arg3">The third parameter.</param>
        /// <param name="arg4">The fourth parameter.</param>
        /// <param name="arg5">The fifth parameter.</param>
        /// <param name="arg6">The sixth parameter.</param>
        public new void CallMethodOnReplicas<T1, T2, T3, T4, T5, T6>(
            RpcSignature<T1, T2, T3, T4, T5, T6> method,
            T1 arg1,
            T2 arg2,
            T3 arg3,
            T4 arg4,
            T5 arg5,
            T6 arg6)
        {
            IOriginalEntityWrapper wrapper = null;
            if (!this.GetOriginalWrapperForTarget(method, out wrapper))
            {
                // Silently ignore unknown originals as they cannot have replicas yet.
                return;
            }

            this.DispatchRPC(s => this.WrappedEntityManager.SendCustomMessageToReplicas(wrapper, s), wrapper, method, arg1, arg2, arg3, arg4, arg5, arg6);
        }

        /// <summary>
        /// Call a method on all the original of a given replica.
        /// </summary>
        /// <param name="method">The corresponding method on the replica.</param>
        public new void CallMethodOnOriginal(RpcSignature method)
        {
            IReplicaEntityWrapper wrapper;
            if (!this.GetReplicaWrapperForTarget(method, out wrapper))
            {
                return;
            }

            this.DispatchRPC(s => this.WrappedEntityManager.SendCustomMessageToOriginal(wrapper, s), wrapper, method);
        }

        /// <summary>
        /// Call a method on all the original of a given replica.
        /// </summary>
        /// <typeparam name="T1">The type of the method's first parameter.</typeparam>
        /// <param name="method">The corresponding method on the replica.</param>
        /// <param name="arg1">The first parameter.</param>
        public new void CallMethodOnOriginal<T1>(RpcSignature<T1> method, T1 arg1)
        {
            IReplicaEntityWrapper wrapper;
            if (!this.GetReplicaWrapperForTarget(method, out wrapper))
            {
                return;
            }

            this.DispatchRPC(s => this.WrappedEntityManager.SendCustomMessageToOriginal(wrapper, s), wrapper, method, arg1);
        }

        /// <summary>
        /// Call a method on all the original of a given replica.
        /// </summary>
        /// <typeparam name="T1">The type of the method's first parameter.</typeparam>
        /// <typeparam name="T2">The type of the method's second parameter.</typeparam>
        /// <param name="method">The corresponding method on the replica.</param>
        /// <param name="arg1">The first parameter.</param>
        /// <param name="arg2">The second parameter.</param>
        public new void CallMethodOnOriginal<T1, T2>(RpcSignature<T1, T2> method, T1 arg1, T2 arg2)
        {
            IReplicaEntityWrapper wrapper;
            if (!this.GetReplicaWrapperForTarget(method, out wrapper))
            {
                return;
            }

            this.DispatchRPC(s => this.WrappedEntityManager.SendCustomMessageToOriginal(wrapper, s), wrapper, method, arg1, arg2);
        }

        /// <summary>
        /// Call a method on all the original of a given replica.
        /// </summary>
        /// <typeparam name="T1">The type of the method's first parameter.</typeparam>
        /// <typeparam name="T2">The type of the method's second parameter.</typeparam>
        /// <typeparam name="T3">The type of the method's third parameter.</typeparam>
        /// <param name="method">The corresponding method on the replica.</param>
        /// <param name="arg1">The first parameter.</param>
        /// <param name="arg2">The second parameter.</param>
        /// <param name="arg3">The third parameter.</param>
        public new void CallMethodOnOriginal<T1, T2, T3>(
            RpcSignature<T1, T2, T3> method,
            T1 arg1,
            T2 arg2,
            T3 arg3)
        {
            IReplicaEntityWrapper wrapper;
            if (!this.GetReplicaWrapperForTarget(method, out wrapper))
            {
                return;
            }

            this.DispatchRPC(s => this.WrappedEntityManager.SendCustomMessageToOriginal(wrapper, s), wrapper, method, arg1, arg2, arg3);
        }

        /// <summary>
        /// Call a method on all the original of a given replica.
        /// </summary>
        /// <typeparam name="T1">The type of the method's first parameter.</typeparam>
        /// <typeparam name="T2">The type of the method's second parameter.</typeparam>
        /// <typeparam name="T3">The type of the method's third parameter.</typeparam>
        /// <typeparam name="T4">The type of the method's fourth parameter.</typeparam>
        /// <param name="method">The corresponding method on the replica.</param>
        /// <param name="arg1">The first parameter.</param>
        /// <param name="arg2">The second parameter.</param>
        /// <param name="arg3">The third parameter.</param>
        /// <param name="arg4">The fourth parameter.</param>
        public new void CallMethodOnOriginal<T1, T2, T3, T4>(
            RpcSignature<T1, T2, T3, T4> method,
            T1 arg1,
            T2 arg2,
            T3 arg3,
            T4 arg4)
        {
            IReplicaEntityWrapper wrapper;
            if (!this.GetReplicaWrapperForTarget(method, out wrapper))
            {
                return;
            }

            this.DispatchRPC(s => this.WrappedEntityManager.SendCustomMessageToOriginal(wrapper, s), wrapper, method, arg1, arg2, arg3, arg4);
        }

        /// <summary>
        /// Call a method on all the original of a given replica.
        /// </summary>
        /// <typeparam name="T1">The type of the method's first parameter.</typeparam>
        /// <typeparam name="T2">The type of the method's second parameter.</typeparam>
        /// <typeparam name="T3">The type of the method's third parameter.</typeparam>
        /// <typeparam name="T4">The type of the method's fourth parameter.</typeparam>
        /// <typeparam name="T5">The type of the method's fifth parameter.</typeparam>
        /// <param name="method">The corresponding method on the replica.</param>
        /// <param name="arg1">The first parameter.</param>
        /// <param name="arg2">The second parameter.</param>
        /// <param name="arg3">The third parameter.</param>
        /// <param name="arg4">The fourth parameter.</param>
        /// <param name="arg5">The fifth parameter.</param>
        public new void CallMethodOnOriginal<T1, T2, T3, T4, T5>(
            RpcSignature<T1, T2, T3, T4, T5> method,
            T1 arg1,
            T2 arg2,
            T3 arg3,
            T4 arg4,
            T5 arg5)
        {
            IReplicaEntityWrapper wrapper;
            if (!this.GetReplicaWrapperForTarget(method, out wrapper))
            {
                return;
            }

            this.DispatchRPC(s => this.WrappedEntityManager.SendCustomMessageToOriginal(wrapper, s), wrapper, method, arg1, arg2, arg3, arg4, arg5);
        }

        /// <summary>
        /// Call a method on all the original of a given replica.
        /// </summary>
        /// <typeparam name="T1">The type of the method's first parameter.</typeparam>
        /// <typeparam name="T2">The type of the method's second parameter.</typeparam>
        /// <typeparam name="T3">The type of the method's third parameter.</typeparam>
        /// <typeparam name="T4">The type of the method's fourth parameter.</typeparam>
        /// <typeparam name="T5">The type of the method's fifth parameter.</typeparam>
        /// <typeparam name="T6">The type of the method's sixth parameter.</typeparam>
        /// <param name="method">The corresponding method on the replica.</param>
        /// <param name="arg1">The first parameter.</param>
        /// <param name="arg2">The second parameter.</param>
        /// <param name="arg3">The third parameter.</param>
        /// <param name="arg4">The fourth parameter.</param>
        /// <param name="arg5">The fifth parameter.</param>
        /// <param name="arg6">The sixth parameter.</param>
        public new void CallMethodOnOriginal<T1, T2, T3, T4, T5, T6>(
            RpcSignature<T1, T2, T3, T4, T5, T6> method,
            T1 arg1,
            T2 arg2,
            T3 arg3,
            T4 arg4,
            T5 arg5,
            T6 arg6)
        {
            IReplicaEntityWrapper wrapper;
            if (!this.GetReplicaWrapperForTarget(method, out wrapper))
            {
                return;
            }

            this.DispatchRPC(s => this.WrappedEntityManager.SendCustomMessageToOriginal(wrapper, s), wrapper, method, arg1, arg2, arg3, arg4, arg5, arg6);
        }

        /// <summary>
        /// Dispatch a remote procedure call.
        /// </summary>
        /// <param name="callDispatcher">A delegate for sending the serialized RPC.</param>
        /// <param name="entityWrapper">The wrapper holding the entity the remotable method is defined on.</param>
        /// <param name="method">The method to remotely call.</param>
        private void DispatchRPC(RPCSendMethod callDispatcher, IEntityWrapper entityWrapper, RpcSignature method)
        {
            using (var stream = new MemoryStream())
            using (var writer = new BinaryWriter(stream))
            {
                writer.Write(this.GetRemotableMethodIndex(entityWrapper, method.Method.Name));
                callDispatcher(stream);
            }
        }

        /// <summary>
        /// Call a method on all the replicas of a given original.
        /// </summary>
        /// <typeparam name="T1">The type of the method's first parameter.</typeparam>
        /// <param name="callDispatcher">A delegate for sending the serialized RPC.</param>
        /// <param name="entityWrapper">The wrapper holding the entity the remotable method is defined on.</param>
        /// <param name="method">The method to call remotely..</param>
        /// <param name="arg1">The first parameter.</param>
        private void DispatchRPC<T1>(
            RPCSendMethod callDispatcher,
            IEntityWrapper entityWrapper,
            RpcSignature<T1> method,
            T1 arg1)
        {
            using (var stream = new MemoryStream())
            using (var writer = new BinaryWriter(stream))
            {
                writer.Write(this.GetRemotableMethodIndex(entityWrapper, method.Method.Name));
                this.Serialization.SerializeParameter(arg1, writer);
                callDispatcher(stream);
            }
        }

        /// <summary>
        /// Serialize and dispatch a remote procedure call.
        /// </summary>
        /// <typeparam name="T1">The type of the method's first parameter.</typeparam>
        /// <typeparam name="T2">The type of the method's second parameter.</typeparam>
        /// <param name="callDispatcher">A delegate for sending the serialized RPC.</param>
        /// <param name="entityWrapper">The wrapper holding the entity the remotable method is defined on.</param>
        /// <param name="method">The method to call remotely.</param>
        /// <param name="arg1">The first parameter.</param>
        /// <param name="arg2">The second parameter.</param>
        private void DispatchRPC<T1, T2>(RPCSendMethod callDispatcher, IEntityWrapper entityWrapper, RpcSignature<T1, T2> method, T1 arg1, T2 arg2)
        {
            using (var stream = new MemoryStream())
            using (var writer = new BinaryWriter(stream))
            {
                writer.Write(this.GetRemotableMethodIndex(entityWrapper, method.Method.Name));
                this.Serialization.SerializeParameter(arg1, writer);
                this.Serialization.SerializeParameter(arg2, writer);
                callDispatcher(stream);
            }
        }

        /// <summary>
        /// Serialize and dispatch a remote procedure call.
        /// </summary>
        /// <typeparam name="T1">The type of the method's first parameter.</typeparam>
        /// <typeparam name="T2">The type of the method's second parameter.</typeparam>
        /// <typeparam name="T3">The type of the method's third parameter.</typeparam>
        /// <param name="callDispatcher">A delegate for sending the serialized RPC.</param>
        /// <param name="entityWrapper">The wrapper holding the entity the remotable method is defined on.</param>
        /// <param name="method">The method to call remotely.</param>
        /// <param name="arg1">The first parameter.</param>
        /// <param name="arg2">The second parameter.</param>
        /// <param name="arg3">The third parameter.</param>
        private void DispatchRPC<T1, T2, T3>(
            RPCSendMethod callDispatcher,
            IEntityWrapper entityWrapper,
            RpcSignature<T1, T2, T3> method,
            T1 arg1,
            T2 arg2,
            T3 arg3)
        {
            using (var stream = new MemoryStream())
            using (var writer = new BinaryWriter(stream))
            {
                writer.Write(this.GetRemotableMethodIndex(entityWrapper, method.Method.Name));
                this.Serialization.SerializeParameter(arg1, writer);
                this.Serialization.SerializeParameter(arg2, writer);
                this.Serialization.SerializeParameter(arg3, writer);
                callDispatcher(stream);
            }
        }

        /// <summary>
        /// Serialize and dispatch a remote procedure call.
        /// </summary>
        /// <typeparam name="T1">The type of the method's first parameter.</typeparam>
        /// <typeparam name="T2">The type of the method's second parameter.</typeparam>
        /// <typeparam name="T3">The type of the method's third parameter.</typeparam>
        /// <typeparam name="T4">The type of the method's fourth parameter.</typeparam>
        /// <param name="callDispatcher">A delegate for sending the serialized RPC.</param>
        /// <param name="entityWrapper">The wrapper holding the entity the remotable method is defined on.</param>
        /// <param name="method">The method to call remotely.</param>
        /// <param name="arg1">The first parameter.</param>
        /// <param name="arg2">The second parameter.</param>
        /// <param name="arg3">The third parameter.</param>
        /// <param name="arg4">The fourth parameter.</param>
        private void DispatchRPC<T1, T2, T3, T4>(
            RPCSendMethod callDispatcher,
            IEntityWrapper entityWrapper,
            RpcSignature<T1, T2, T3, T4> method,
            T1 arg1,
            T2 arg2,
            T3 arg3,
            T4 arg4)
        {
            using (var stream = new MemoryStream())
            using (var writer = new BinaryWriter(stream))
            {
                writer.Write(this.GetRemotableMethodIndex(entityWrapper, method.Method.Name));
                this.Serialization.SerializeParameter(arg1, writer);
                this.Serialization.SerializeParameter(arg2, writer);
                this.Serialization.SerializeParameter(arg3, writer);
                this.Serialization.SerializeParameter(arg4, writer);
                callDispatcher(stream);
            }
        }

        /// <summary>
        /// Serialize and dispatch a remote procedure call.
        /// </summary>
        /// <typeparam name="T1">The type of the method's first parameter.</typeparam>
        /// <typeparam name="T2">The type of the method's second parameter.</typeparam>
        /// <typeparam name="T3">The type of the method's third parameter.</typeparam>
        /// <typeparam name="T4">The type of the method's fourth parameter.</typeparam>
        /// <typeparam name="T5">The type of the method's fifth parameter.</typeparam>
        /// <param name="callDispatcher">A delegate for sending the serialized RPC.</param>
        /// <param name="entityWrapper">The wrapper holding the entity the remotable method is defined on.</param>
        /// <param name="method">The method to call remotely.</param>
        /// <param name="arg1">The first parameter.</param>
        /// <param name="arg2">The second parameter.</param>
        /// <param name="arg3">The third parameter.</param>
        /// <param name="arg4">The fourth parameter.</param>
        /// <param name="arg5">The fifth parameter.</param>
        private void DispatchRPC<T1, T2, T3, T4, T5>(
            RPCSendMethod callDispatcher,
            IEntityWrapper entityWrapper,
            RpcSignature<T1, T2, T3, T4, T5> method,
            T1 arg1,
            T2 arg2,
            T3 arg3,
            T4 arg4,
            T5 arg5)
        {
            using (var stream = new MemoryStream())
            using (var writer = new BinaryWriter(stream))
            {
                writer.Write(this.GetRemotableMethodIndex(entityWrapper, method.Method.Name));
                this.Serialization.SerializeParameter(arg1, writer);
                this.Serialization.SerializeParameter(arg2, writer);
                this.Serialization.SerializeParameter(arg3, writer);
                this.Serialization.SerializeParameter(arg4, writer);
                this.Serialization.SerializeParameter(arg5, writer);
                callDispatcher(stream);
            }
        }

        /// <summary>
        /// Serialize and dispatch a remote procedure call.
        /// </summary>
        /// <typeparam name="T1">The type of the method's first parameter.</typeparam>
        /// <typeparam name="T2">The type of the method's second parameter.</typeparam>
        /// <typeparam name="T3">The type of the method's third parameter.</typeparam>
        /// <typeparam name="T4">The type of the method's fourth parameter.</typeparam>
        /// <typeparam name="T5">The type of the method's fifth parameter.</typeparam>
        /// <typeparam name="T6">The type of the method's sixth parameter.</typeparam>
        /// <param name="callDispatcher">A delegate for sending the serialized RPC.</param>
        /// <param name="wrapper">The wrapper holding the entity the remotable method is defined on.</param>
        /// <param name="method">The method to call remotely.</param>
        /// <param name="arg1">The first parameter.</param>
        /// <param name="arg2">The second parameter.</param>
        /// <param name="arg3">The third parameter.</param>
        /// <param name="arg4">The fourth parameter.</param>
        /// <param name="arg5">The fifth parameter.</param>
        /// <param name="arg6">The sixth parameter.</param>
        private void DispatchRPC<T1, T2, T3, T4, T5, T6>(
            RPCSendMethod callDispatcher,
            IEntityWrapper wrapper,
            RpcSignature<T1, T2, T3, T4, T5, T6> method,
            T1 arg1,
            T2 arg2,
            T3 arg3,
            T4 arg4,
            T5 arg5,
            T6 arg6)
        {
            using (var stream = new MemoryStream())
            using (var writer = new BinaryWriter(stream))
            {
                writer.Write(this.GetRemotableMethodIndex(wrapper, method.Method.Name));
                this.Serialization.SerializeParameter(arg1, writer);
                this.Serialization.SerializeParameter(arg2, writer);
                this.Serialization.SerializeParameter(arg3, writer);
                this.Serialization.SerializeParameter(arg4, writer);
                this.Serialization.SerializeParameter(arg5, writer);
                this.Serialization.SerializeParameter(arg6, writer);
                callDispatcher(stream);
            }
        }
    }
}
