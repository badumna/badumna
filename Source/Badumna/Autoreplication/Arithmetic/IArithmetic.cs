﻿// ------------------------------------------------------------------------------
//  <copyright file="IArithmetic.cs" company="National ICT Australia Limited">
//      Copyright (c) 2012-2012 National ICT Australia Limited. All rights reserved.
//  </copyright>
// ------------------------------------------------------------------------------
namespace Badumna.Autoreplication
{
    using Badumna.DataTypes;

    /// <summary>
    /// Interface for providers of basic arithmetical operations on values of a given type.
    /// </summary>
    /// <typeparam name="T">The type to provide arithmetical operations for.</typeparam>
    public interface IArithmetic<T>
    {
        /// <summary>
        /// Add two values.
        /// </summary>
        /// <param name="first">The first addendum.</param>
        /// <param name="second">The second addendum.</param>
        /// <returns>The sum of the </returns>
        T Add(T first, T second);

        /// <summary>
        /// Scales a value by a factor.
        /// </summary>
        /// <param name="value">The value to scale.</param>
        /// <param name="factor">The factor to scale by.</param>
        /// <returns>The scaled value.</returns>
        T Scale(T value, double factor);

        /// <summary>
        /// Subtract one value (the subtrahend) from another (the minuend).
        /// </summary>
        /// <param name="minuend">The minuend.</param>
        /// <param name="subtrahend">The subtrahend.</param>
        /// <returns>The difference.</returns>
        T Subtract(T minuend, T subtrahend);
    }
}