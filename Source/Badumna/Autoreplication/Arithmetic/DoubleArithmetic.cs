﻿// ------------------------------------------------------------------------------
//  <copyright file="DoubleArithmetic.cs" company="National ICT Australia Limited">
//      Copyright (c) 2012-2012 National ICT Australia Limited. All rights reserved.
//  </copyright>
// ------------------------------------------------------------------------------
namespace Badumna.Autoreplication
{
    /// <summary>
    /// Provider of basic arithmetical operations on values of type double.
    /// </summary>
    public class DoubleArithmetic : IArithmetic<double>
    {
        /// <inheritdoc/>
        public double Add(double a, double b)
        {
            return a + b;
        }

        /// <inheritdoc/>
        public double Subtract(double minuend, double subtrahend)
        {
            return minuend - subtrahend;
        }

        /// <inheritdoc/>
        public double Scale(double value, double factor)
        {
            return value * factor;
        }
    }
}