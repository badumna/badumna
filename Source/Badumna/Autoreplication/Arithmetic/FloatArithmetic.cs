﻿
// ------------------------------------------------------------------------------
//  <copyright file="FloatArithmetic.cs" company="National ICT Australia Limited">
//      Copyright (c) 2012-2012 National ICT Australia Limited. All rights reserved.
//  </copyright>
// ------------------------------------------------------------------------------
namespace Badumna.Autoreplication
{
    /// <summary>
    /// Provider of basic arithmetical operations on values of type float.
    /// </summary>
    public class FloatArithmetic : IArithmetic<float>
    {
        /// <inheritdoc/>
        public float Add(float a, float b)
        {
            return a + b;
        }

        /// <inheritdoc/>
        public float Subtract(float minuend, float subtrahend)
        {
            return minuend - subtrahend;
        }

        /// <inheritdoc/>
        public float Scale(float value, double factor)
        {
            return value * (float)factor;
        }
    }
}