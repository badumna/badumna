﻿// ------------------------------------------------------------------------------
//  <copyright file="Vector3Arithmetic.cs" company="National ICT Australia Limited">
//      Copyright (c) 2012-2012 National ICT Australia Limited. All rights reserved.
//  </copyright>
// ------------------------------------------------------------------------------
namespace Badumna.Autoreplication
{
    using Badumna.DataTypes;

    /// <summary>
    /// Provider of basic arithmetical operations on values of Badumna's Vector3 type.
    /// </summary>
    public class Vector3Arithmetic : IArithmetic<Vector3>
    {
        /// <inheritdoc/>
        public Vector3 Add(Vector3 a, Vector3 b)
        {
            return a + b;
        }

        /// <inheritdoc/>
        public Vector3 Subtract(Vector3 minuend, Vector3 subtrahend)
        {
            return minuend - subtrahend;
        }

        /// <inheritdoc/>
        public Vector3 Scale(Vector3 value, double factor)
        {
            return value * (float)factor;
        }
    }
}