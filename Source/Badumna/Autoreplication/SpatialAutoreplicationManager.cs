﻿// -----------------------------------------------------------------------
// <copyright file="SpatialAutoreplicationManager.cs" company="Scalify">
//     Copyright (c) 2013 Scalify Pty Ltd. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace Badumna.Autoreplication
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Text;
    using Badumna.Autoreplication.Serialization;
    using Badumna.Core;
    using Badumna.SpatialEntities;

    /// <summary>
    /// Concrete SpatialAutoreplicationManager derived class, explicitly derived a concrete class from the generic Manager class due to AOT limitation in Unity.
    /// </summary>
    internal class SpatialAutoreplicationManager : Manager<IReplicableEntity, ISpatialOriginal, ISpatialReplica, IPositionalOriginalWrapper, IPositionalReplicaWrapper, KeyValuePair<float, float>, ISpatialEntityManager>
    {
        /// <summary>
        /// Initializes a new instance of the SpatialAutoreplicationManager class.
        /// </summary>
        /// <param name="serialization">For manipulating replicable types.</param>
        /// <param name="timeKeeper">Provides access to current time.</param>
        /// <param name="originalWrapperFactory">Factory for creating original wrappers.</param>
        /// <param name="replicaWrapperFactory">Factory for creating replica wrappers.</param>
        public SpatialAutoreplicationManager(
            Serialization.Manager serialization,
            ITime timeKeeper,
            OriginalWrapperFactory<IReplicableEntity, IPositionalOriginalWrapper, KeyValuePair<float, float>> originalWrapperFactory,
            ReplicaWrapperFactory<IReplicableEntity, IPositionalReplicaWrapper> replicaWrapperFactory)
            : base(serialization, timeKeeper, originalWrapperFactory, replicaWrapperFactory)
        {
        }

        /// <summary>
        /// Call a parameterless method on all the replicas of a given original.
        /// </summary>
        /// <param name="method">The corresponding method on the original.</param>
        public new void CallMethodOnReplicas(RpcSignature method)
        {
            IPositionalOriginalWrapper wrapper = null;
            if (!this.GetOriginalWrapperForTarget(method, out wrapper))
            {
                // Silently ignore unknown originals as they cannot have replicas yet.
                return;
            }

            using (var stream = new MemoryStream())
            using (var writer = new BinaryWriter(stream))
            {
                writer.Write(this.GetRemotableMethodIndex(wrapper, method.Method.Name));
                this.WrappedEntityManager.SendCustomMessageToReplicas(wrapper, stream);
            }
        }

        /// <summary>
        /// Call a method on all the replicas of a given original.
        /// </summary>
        /// <typeparam name="T1">The type of the method's first parameter.</typeparam>
        /// <param name="method">The corresponding method on the original.</param>
        /// <param name="arg1">The first parameter.</param>
        public new void CallMethodOnReplicas<T1>(RpcSignature<T1> method, T1 arg1)
        {
            IPositionalOriginalWrapper wrapper = null;
            if (!this.GetOriginalWrapperForTarget(method, out wrapper))
            {
                // Silently ignore unknown originals as they cannot have replicas yet.
                return;
            }

            using (var stream = new MemoryStream())
            using (var writer = new BinaryWriter(stream))
            {
                writer.Write(this.GetRemotableMethodIndex(wrapper, method.Method.Name));
                this.Serialization.SerializeParameter(arg1, writer);
                this.WrappedEntityManager.SendCustomMessageToReplicas(wrapper, stream);
            }
        }

        /// <summary>
        /// Call a method on all the replicas of a given original.
        /// </summary>
        /// <typeparam name="T1">The type of the method's first parameter.</typeparam>
        /// <typeparam name="T2">The type of the method's second parameter.</typeparam>
        /// <param name="method">The corresponding method on the original.</param>
        /// <param name="arg1">The first parameter.</param>
        /// <param name="arg2">The second parameter.</param>
        public new void CallMethodOnReplicas<T1, T2>(RpcSignature<T1, T2> method, T1 arg1, T2 arg2)
        {
            IPositionalOriginalWrapper wrapper = null;
            if (!this.GetOriginalWrapperForTarget(method, out wrapper))
            {
                // Silently ignore unknown originals as they cannot have replicas yet.
                return;
            }

            using (var stream = new MemoryStream())
            using (var writer = new BinaryWriter(stream))
            {
                writer.Write(this.GetRemotableMethodIndex(wrapper, method.Method.Name));
                this.Serialization.SerializeParameter(arg1, writer);
                this.Serialization.SerializeParameter(arg2, writer);
                this.WrappedEntityManager.SendCustomMessageToReplicas(wrapper, stream);
            }
        }

        /// <summary>
        /// Call a method on all the replicas of a given original.
        /// </summary>
        /// <typeparam name="T1">The type of the method's first parameter.</typeparam>
        /// <typeparam name="T2">The type of the method's second parameter.</typeparam>
        /// <typeparam name="T3">The type of the method's third parameter.</typeparam>
        /// <param name="method">The corresponding method on the original.</param>
        /// <param name="arg1">The first parameter.</param>
        /// <param name="arg2">The second parameter.</param>
        /// <param name="arg3">The third parameter.</param>
        public new void CallMethodOnReplicas<T1, T2, T3>(
            RpcSignature<T1, T2, T3> method,
            T1 arg1,
            T2 arg2,
            T3 arg3)
        {
            IPositionalOriginalWrapper wrapper = null;
            if (!this.GetOriginalWrapperForTarget(method, out wrapper))
            {
                // Silently ignore unknown originals as they cannot have replicas yet.
                return;
            }

            using (var stream = new MemoryStream())
            using (var writer = new BinaryWriter(stream))
            {
                writer.Write(this.GetRemotableMethodIndex(wrapper, method.Method.Name));
                this.Serialization.SerializeParameter(arg1, writer);
                this.Serialization.SerializeParameter(arg2, writer);
                this.Serialization.SerializeParameter(arg3, writer);
                this.WrappedEntityManager.SendCustomMessageToReplicas(wrapper, stream);
            }
        }

        /// <summary>
        /// Call a method on all the replicas of a given original.
        /// </summary>
        /// <typeparam name="T1">The type of the method's first parameter.</typeparam>
        /// <typeparam name="T2">The type of the method's second parameter.</typeparam>
        /// <typeparam name="T3">The type of the method's third parameter.</typeparam>
        /// <typeparam name="T4">The type of the method's fourth parameter.</typeparam>
        /// <param name="method">The corresponding method on the original.</param>
        /// <param name="arg1">The first parameter.</param>
        /// <param name="arg2">The second parameter.</param>
        /// <param name="arg3">The third parameter.</param>
        /// <param name="arg4">The fourth parameter.</param>
        public new void CallMethodOnReplicas<T1, T2, T3, T4>(
            RpcSignature<T1, T2, T3, T4> method,
            T1 arg1,
            T2 arg2,
            T3 arg3,
            T4 arg4)
        {
            IPositionalOriginalWrapper wrapper = null;
            if (!this.GetOriginalWrapperForTarget(method, out wrapper))
            {
                // Silently ignore unknown originals as they cannot have replicas yet.
                return;
            }

            using (var stream = new MemoryStream())
            using (var writer = new BinaryWriter(stream))
            {
                writer.Write(this.GetRemotableMethodIndex(wrapper, method.Method.Name));
                this.Serialization.SerializeParameter(arg1, writer);
                this.Serialization.SerializeParameter(arg2, writer);
                this.Serialization.SerializeParameter(arg3, writer);
                this.Serialization.SerializeParameter(arg4, writer);
                this.WrappedEntityManager.SendCustomMessageToReplicas(wrapper, stream);
            }
        }

        /// <summary>
        /// Call a method on all the replicas of a given original.
        /// </summary>
        /// <typeparam name="T1">The type of the method's first parameter.</typeparam>
        /// <typeparam name="T2">The type of the method's second parameter.</typeparam>
        /// <typeparam name="T3">The type of the method's third parameter.</typeparam>
        /// <typeparam name="T4">The type of the method's fourth parameter.</typeparam>
        /// <typeparam name="T5">The type of the method's fifth parameter.</typeparam>
        /// <param name="method">The corresponding method on the original.</param>
        /// <param name="arg1">The first parameter.</param>
        /// <param name="arg2">The second parameter.</param>
        /// <param name="arg3">The third parameter.</param>
        /// <param name="arg4">The fourth parameter.</param>
        /// <param name="arg5">The fifth parameter.</param>
        public new void CallMethodOnReplicas<T1, T2, T3, T4, T5>(
            RpcSignature<T1, T2, T3, T4, T5> method,
            T1 arg1,
            T2 arg2,
            T3 arg3,
            T4 arg4,
            T5 arg5)
        {
            IPositionalOriginalWrapper wrapper = null;
            if (!this.GetOriginalWrapperForTarget(method, out wrapper))
            {
                // Silently ignore unknown originals as they cannot have replicas yet.
                return;
            }

            using (var stream = new MemoryStream())
            using (var writer = new BinaryWriter(stream))
            {
                writer.Write(this.GetRemotableMethodIndex(wrapper, method.Method.Name));
                this.Serialization.SerializeParameter(arg1, writer);
                this.Serialization.SerializeParameter(arg2, writer);
                this.Serialization.SerializeParameter(arg3, writer);
                this.Serialization.SerializeParameter(arg4, writer);
                this.Serialization.SerializeParameter(arg5, writer);
                this.WrappedEntityManager.SendCustomMessageToReplicas(wrapper, stream);
            }
        }

        /// <summary>
        /// Call a method on all the replicas of a given original.
        /// </summary>
        /// <typeparam name="T1">The type of the method's first parameter.</typeparam>
        /// <typeparam name="T2">The type of the method's second parameter.</typeparam>
        /// <typeparam name="T3">The type of the method's third parameter.</typeparam>
        /// <typeparam name="T4">The type of the method's fourth parameter.</typeparam>
        /// <typeparam name="T5">The type of the method's fifth parameter.</typeparam>
        /// <typeparam name="T6">The type of the method's sixth parameter.</typeparam>
        /// <param name="method">The corresponding method on the original.</param>
        /// <param name="arg1">The first parameter.</param>
        /// <param name="arg2">The second parameter.</param>
        /// <param name="arg3">The third parameter.</param>
        /// <param name="arg4">The fourth parameter.</param>
        /// <param name="arg5">The fifth parameter.</param>
        /// <param name="arg6">The sixth parameter.</param>
        public new void CallMethodOnReplicas<T1, T2, T3, T4, T5, T6>(
            RpcSignature<T1, T2, T3, T4, T5, T6> method,
            T1 arg1,
            T2 arg2,
            T3 arg3,
            T4 arg4,
            T5 arg5,
            T6 arg6)
        {
            IPositionalOriginalWrapper wrapper = null;
            if (!this.GetOriginalWrapperForTarget(method, out wrapper))
            {
                // Silently ignore unknown originals as they cannot have replicas yet.
                return;
            }

            using (var stream = new MemoryStream())
            using (var writer = new BinaryWriter(stream))
            {
                writer.Write(this.GetRemotableMethodIndex(wrapper, method.Method.Name));
                this.Serialization.SerializeParameter(arg1, writer);
                this.Serialization.SerializeParameter(arg2, writer);
                this.Serialization.SerializeParameter(arg3, writer);
                this.Serialization.SerializeParameter(arg4, writer);
                this.Serialization.SerializeParameter(arg5, writer);
                this.Serialization.SerializeParameter(arg6, writer);
                this.WrappedEntityManager.SendCustomMessageToReplicas(wrapper, stream);
            }
        }

        /// <summary>
        /// Call a method on all the original of a given replica.
        /// </summary>
        /// <param name="method">The corresponding method on the replica.</param>
        public new void CallMethodOnOriginal(RpcSignature method)
        {
            IPositionalReplicaWrapper wrapper;
            if (!this.GetReplicaWrapperForTarget(method, out wrapper))
            {
                return;
            }

            using (var stream = new MemoryStream())
            using (var writer = new BinaryWriter(stream))
            {
                writer.Write(this.GetRemotableMethodIndex(wrapper, method.Method.Name));
                this.WrappedEntityManager.SendCustomMessageToOriginal(wrapper, stream);
            }
        }

        /// <summary>
        /// Call a method on all the original of a given replica.
        /// </summary>
        /// <typeparam name="T1">The type of the method's first parameter.</typeparam>
        /// <param name="method">The corresponding method on the replica.</param>
        /// <param name="arg1">The first parameter.</param>
        public new void CallMethodOnOriginal<T1>(RpcSignature<T1> method, T1 arg1)
        {
            IPositionalReplicaWrapper wrapper;
            if (!this.GetReplicaWrapperForTarget(method, out wrapper))
            {
                return;
            }

            using (var stream = new MemoryStream())
            using (var writer = new BinaryWriter(stream))
            {
                writer.Write(this.GetRemotableMethodIndex(wrapper, method.Method.Name));
                this.Serialization.SerializeParameter(arg1, writer);
                this.WrappedEntityManager.SendCustomMessageToOriginal(wrapper, stream);
            }
        }

        /// <summary>
        /// Call a method on all the original of a given replica.
        /// </summary>
        /// <typeparam name="T1">The type of the method's first parameter.</typeparam>
        /// <typeparam name="T2">The type of the method's second parameter.</typeparam>
        /// <param name="method">The corresponding method on the replica.</param>
        /// <param name="arg1">The first parameter.</param>
        /// <param name="arg2">The second parameter.</param>
        public new void CallMethodOnOriginal<T1, T2>(RpcSignature<T1, T2> method, T1 arg1, T2 arg2)
        {
            IPositionalReplicaWrapper wrapper;
            if (!this.GetReplicaWrapperForTarget(method, out wrapper))
            {
                return;
            }

            using (var stream = new MemoryStream())
            using (var writer = new BinaryWriter(stream))
            {
                writer.Write(this.GetRemotableMethodIndex(wrapper, method.Method.Name));
                this.Serialization.SerializeParameter(arg1, writer);
                this.Serialization.SerializeParameter(arg2, writer);
                this.WrappedEntityManager.SendCustomMessageToOriginal(wrapper, stream);
            }
        }

        /// <summary>
        /// Call a method on all the original of a given replica.
        /// </summary>
        /// <typeparam name="T1">The type of the method's first parameter.</typeparam>
        /// <typeparam name="T2">The type of the method's second parameter.</typeparam>
        /// <typeparam name="T3">The type of the method's third parameter.</typeparam>
        /// <param name="method">The corresponding method on the replica.</param>
        /// <param name="arg1">The first parameter.</param>
        /// <param name="arg2">The second parameter.</param>
        /// <param name="arg3">The third parameter.</param>
        public new void CallMethodOnOriginal<T1, T2, T3>(
            RpcSignature<T1, T2, T3> method,
            T1 arg1,
            T2 arg2,
            T3 arg3)
        {
            IPositionalReplicaWrapper wrapper;
            if (!this.GetReplicaWrapperForTarget(method, out wrapper))
            {
                return;
            }

            using (var stream = new MemoryStream())
            using (var writer = new BinaryWriter(stream))
            {
                writer.Write(this.GetRemotableMethodIndex(wrapper, method.Method.Name));
                this.Serialization.SerializeParameter(arg1, writer);
                this.Serialization.SerializeParameter(arg2, writer);
                this.Serialization.SerializeParameter(arg3, writer);
                this.WrappedEntityManager.SendCustomMessageToOriginal(wrapper, stream);
            }
        }

        /// <summary>
        /// Call a method on all the original of a given replica.
        /// </summary>
        /// <typeparam name="T1">The type of the method's first parameter.</typeparam>
        /// <typeparam name="T2">The type of the method's second parameter.</typeparam>
        /// <typeparam name="T3">The type of the method's third parameter.</typeparam>
        /// <typeparam name="T4">The type of the method's fourth parameter.</typeparam>
        /// <param name="method">The corresponding method on the replica.</param>
        /// <param name="arg1">The first parameter.</param>
        /// <param name="arg2">The second parameter.</param>
        /// <param name="arg3">The third parameter.</param>
        /// <param name="arg4">The fourth parameter.</param>
        public new void CallMethodOnOriginal<T1, T2, T3, T4>(
            RpcSignature<T1, T2, T3, T4> method,
            T1 arg1,
            T2 arg2,
            T3 arg3,
            T4 arg4)
        {
            IPositionalReplicaWrapper wrapper;
            if (!this.GetReplicaWrapperForTarget(method, out wrapper))
            {
                return;
            }

            using (var stream = new MemoryStream())
            using (var writer = new BinaryWriter(stream))
            {
                writer.Write(this.GetRemotableMethodIndex(wrapper, method.Method.Name));
                this.Serialization.SerializeParameter(arg1, writer);
                this.Serialization.SerializeParameter(arg2, writer);
                this.Serialization.SerializeParameter(arg3, writer);
                this.Serialization.SerializeParameter(arg4, writer);
                this.WrappedEntityManager.SendCustomMessageToOriginal(wrapper, stream);
            }
        }

        /// <summary>
        /// Call a method on all the original of a given replica.
        /// </summary>
        /// <typeparam name="T1">The type of the method's first parameter.</typeparam>
        /// <typeparam name="T2">The type of the method's second parameter.</typeparam>
        /// <typeparam name="T3">The type of the method's third parameter.</typeparam>
        /// <typeparam name="T4">The type of the method's fourth parameter.</typeparam>
        /// <typeparam name="T5">The type of the method's fifth parameter.</typeparam>
        /// <param name="method">The corresponding method on the replica.</param>
        /// <param name="arg1">The first parameter.</param>
        /// <param name="arg2">The second parameter.</param>
        /// <param name="arg3">The third parameter.</param>
        /// <param name="arg4">The fourth parameter.</param>
        /// <param name="arg5">The fifth parameter.</param>
        public new void CallMethodOnOriginal<T1, T2, T3, T4, T5>(
            RpcSignature<T1, T2, T3, T4, T5> method,
            T1 arg1,
            T2 arg2,
            T3 arg3,
            T4 arg4,
            T5 arg5)
        {
            IPositionalReplicaWrapper wrapper;
            if (!this.GetReplicaWrapperForTarget(method, out wrapper))
            {
                return;
            }

            using (var stream = new MemoryStream())
            using (var writer = new BinaryWriter(stream))
            {
                writer.Write(this.GetRemotableMethodIndex(wrapper, method.Method.Name));
                this.Serialization.SerializeParameter(arg1, writer);
                this.Serialization.SerializeParameter(arg2, writer);
                this.Serialization.SerializeParameter(arg3, writer);
                this.Serialization.SerializeParameter(arg4, writer);
                this.Serialization.SerializeParameter(arg5, writer);
                this.WrappedEntityManager.SendCustomMessageToOriginal(wrapper, stream);
            }
        }

        /// <summary>
        /// Call a method on all the original of a given replica.
        /// </summary>
        /// <typeparam name="T1">The type of the method's first parameter.</typeparam>
        /// <typeparam name="T2">The type of the method's second parameter.</typeparam>
        /// <typeparam name="T3">The type of the method's third parameter.</typeparam>
        /// <typeparam name="T4">The type of the method's fourth parameter.</typeparam>
        /// <typeparam name="T5">The type of the method's fifth parameter.</typeparam>
        /// <typeparam name="T6">The type of the method's sixth parameter.</typeparam>
        /// <param name="method">The corresponding method on the replica.</param>
        /// <param name="arg1">The first parameter.</param>
        /// <param name="arg2">The second parameter.</param>
        /// <param name="arg3">The third parameter.</param>
        /// <param name="arg4">The fourth parameter.</param>
        /// <param name="arg5">The fifth parameter.</param>
        /// <param name="arg6">The sixth parameter.</param>
        public new void CallMethodOnOriginal<T1, T2, T3, T4, T5, T6>(
            RpcSignature<T1, T2, T3, T4, T5, T6> method,
            T1 arg1,
            T2 arg2,
            T3 arg3,
            T4 arg4,
            T5 arg5,
            T6 arg6)
        {
            IPositionalReplicaWrapper wrapper;
            if (!this.GetReplicaWrapperForTarget(method, out wrapper))
            {
                return;
            }

            using (var stream = new MemoryStream())
            using (var writer = new BinaryWriter(stream))
            {
                writer.Write(this.GetRemotableMethodIndex(wrapper, method.Method.Name));
                this.Serialization.SerializeParameter(arg1, writer);
                this.Serialization.SerializeParameter(arg2, writer);
                this.Serialization.SerializeParameter(arg3, writer);
                this.Serialization.SerializeParameter(arg4, writer);
                this.Serialization.SerializeParameter(arg5, writer);
                this.Serialization.SerializeParameter(arg6, writer);
                this.WrappedEntityManager.SendCustomMessageToOriginal(wrapper, stream);
            }
        }
    }
}
