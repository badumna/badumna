﻿// ------------------------------------------------------------------------------
//  <copyright file="ReplicationException.cs" company="National ICT Australia Limited">
//      Copyright (c) 2012-2012 National ICT Australia Limited. All rights reserved.
//  </copyright>
// ------------------------------------------------------------------------------
namespace Badumna.Autoreplication
{
    using System;
    using System.Runtime.Serialization;
    using Badumna.Core;

    /// <summary>
    /// Exception indicating a problem with replication.
    /// </summary>
    [Serializable]
    public sealed class ReplicationException : BadumnaException
    {
        /// <summary>
        /// Initializes a new instance of the ReplicationException class.
        /// </summary>
        internal ReplicationException()
            : this("Unknown problem.")
        {
        }

        /// <summary>
        /// Initializes a new instance of the ReplicationException class.
        /// </summary>
        /// <param name="message">A message describing the reason for the exception being thrown.</param>
        internal ReplicationException(string message)
            : this(message, null)
        {
        }

        /// <summary>
        /// Initializes a new instance of the ReplicationException class.
        /// </summary>
        /// <param name="message">A message describing the reason for the exception being thrown.</param>
        /// <param name="innerException">The inner exception that resulted in this exception being thrown.</param>
        internal ReplicationException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        /// <summary>
        /// Initializes a new instance of the ReplicationException class by deserialization.
        /// </summary>
        /// <param name="serializationInfo">The serialized exception information.</param>
        /// <param name="streamingContext">The streaming context.</param>
        private ReplicationException(SerializationInfo serializationInfo, StreamingContext streamingContext)
            : base(serializationInfo, streamingContext)
        {
        }         
    }
}