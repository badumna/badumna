﻿// -----------------------------------------------------------------------
// <copyright file="EntityIDProvider.cs" company="National ICT Australia Limited">
//     Copyright (c) 2007-2012 National ICT Australia Limited. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace Badumna
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Badumna.DataTypes;
    using Badumna.SpatialEntities;

    /// <summary>
    /// Provides IDs for known entities.
    /// </summary>
    /// <typeparam name="T">The type of the entity.</typeparam>
    /// <param name="entity">The entity whose ID is requested.</param>
    /// <returns>The ID for the entity if it is known, otherwise false.</returns>
    internal delegate BadumnaId EntityIDProvider<T>(T entity);
}
