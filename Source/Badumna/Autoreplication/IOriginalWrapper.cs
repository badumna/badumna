﻿// -----------------------------------------------------------------------
// <copyright file="IOriginalWrapper.cs" company="National ICT Australia Limited">
//     Copyright (c) 2012 National ICT Australia Limited. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace Badumna.Autoreplication
{
    using System.Collections.Generic;

    /// <summary>
    /// Interface for an original entity wrapper.
    /// </summary>
    internal interface IOriginalWrapper : IEntityWrapper
    {
        /// <summary>
        /// Flag all properties as changed.
        /// </summary>
        void FlagFullUpdate();

        /// <summary>
        /// Prevent further access of the wrapped original's public members.
        /// </summary>
        void MarkInaccessible();
    }
}