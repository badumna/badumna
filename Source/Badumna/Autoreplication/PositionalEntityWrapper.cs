﻿// -----------------------------------------------------------------------
// <copyright file="PositionalEntityWrapper.cs" company="National ICT Australia Limited">
//     Copyright (c) 2012 National ICT Australia Limited. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace Badumna.Autoreplication
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Reflection;

    using Badumna.Autoreplication.Serialization;
    using Badumna.DataTypes;
    using Badumna.SpatialEntities;
    using Badumna.Utilities;

    /// <summary>
    /// Base class for replicable entity wrapper responsible for reflecting over
    /// replicable entity and setting facilities for tracking .
    /// </summary>
    internal abstract class PositionalEntityWrapper : IEntityWrapper, ISpatialEntity
    {
        /// <summary>
        /// For creation, comparison, Serialization and deserialization of replicated properties.
        /// </summary>
        private readonly Serialization.Manager serialization;

        /// <summary>
        /// The replicable entity being wrapped.
        /// </summary>
        private readonly IReplicableEntity replicableEntity;
        
        /// <summary>
        /// Holds a list of replicable fields.
        /// </summary>
        private readonly List<PropertyInfo> replicableProperties = new List<PropertyInfo>();

        /// <summary>
        /// Map replicable properties to their smoothing attributes, if present.
        /// </summary>
        private readonly Dictionary<PropertyInfo, SmoothingAttribute> smoothingAttributes =
            new Dictionary<PropertyInfo, SmoothingAttribute>();

        /// <summary>
        /// Derivative properties used for smoothing.
        /// </summary>
        /// Derivative properties are stored in a list rather than just iterating over keys
        /// in the dictionary so order is not dependent on Hash.
        private readonly List<PropertyInfo> derivatives = new List<PropertyInfo>();

        /// <summary>
        /// Maps derivatives to their variables for smoothing.
        /// </summary>
        private readonly Dictionary<PropertyInfo, PropertyInfo> derivativesToVariables =
            new Dictionary<PropertyInfo, PropertyInfo>();

        /////// <summary>
        /////// Maps replicable property indices to the index of their smoothing derivative.
        /////// </summary>
        ////private readonly List<KeyValuePair<int, int>> changeFlagPairs = new List<KeyValuePair<int, int>>();

        /// <summary>
        /// Holds a list of remotely callable methods.
        /// </summary>
        private readonly List<MethodInfo> remotableMethods = new List<MethodInfo>();

        /// <summary>
        /// Indices of methods available for RPC, by name.
        /// </summary>
        private readonly Dictionary<string, byte> rpcIndices = new Dictionary<string, byte>();

        /// <summary>
        /// RPC deserializers.
        /// </summary>
        private readonly List<IRPCDeserializer> rpcDeserializers = new List<IRPCDeserializer>();

        /// <summary>
        /// The position property.
        /// </summary>
        private PropertyInfo positionProperty;

        /// <summary>
        /// The position's smoothing attribute (if specified).
        /// </summary>
        private SmoothingAttribute positionSmoothingAttribute;

        /// <summary>
        /// The position property's derivative (if specified in smoothing attribute).
        /// </summary>
        private PropertyInfo velocityProperty;

        /// <summary>
        /// Initializes a new instance of the PositionalEntityWrapper class.
        /// </summary>
        /// <param name="serialization">For creation, comparison, Serialization
        /// and deserialization of replicated properties.</param>
        /// <param name="replicableEntity">The replicable entity being wrapped.</param>
        /// <param name="radius">The radius of the entity's bounding sphere.</param>
        /// <param name="sphereOfInterestRadius">The radius of the entity's sphere of interest.</param>
        protected PositionalEntityWrapper(
            Serialization.Manager serialization,
            IReplicableEntity replicableEntity,
            float radius,
            float sphereOfInterestRadius)
        {
            if (serialization == null)
            {
                throw new ArgumentNullException("serialization");
            }

            if (replicableEntity == null)
            {
                throw new ArgumentNullException("replicableEntity");
            }

            this.serialization = serialization;
            this.replicableEntity = replicableEntity;
            this.Radius = radius;
            this.AreaOfInterestRadius = sphereOfInterestRadius;

            Type type = this.replicableEntity.GetType();
            this.CheckForPositionSmoothing(type);
            this.PopulateReplicableProperties(type);
            this.PopulateRemotableMethods(type);
        }

        /// <inheritdoc/>
        public abstract Vector3 Position { get; set; }

        /// <inheritdoc/>
        public float Radius
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public float AreaOfInterestRadius
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public BadumnaId Guid
        {
            get;
            set;
        }

        /// <summary>
        /// Gets a serialization manager for creation, comparison, and serialization of replicated properties.
        /// </summary>
        protected Serialization.Manager Serialization
        {
            get { return this.serialization; }
        }

        /// <summary>
        /// Gets the replicable entity being wrapped.
        /// </summary>
        protected IReplicableEntity ReplicableEntity
        {
            get { return this.replicableEntity; }
        }

        /// <summary>
        /// Gets a list of replicable properties.
        /// </summary>
        protected IList<PropertyInfo> ReplicableProperties
        {
            get { return this.replicableProperties; }
        }

        /// <summary>
        /// Gets a map of replicable properties to their smoothing attributes.
        /// </summary>
        protected Dictionary<PropertyInfo, SmoothingAttribute> SmoothingAttribtues
        {
            get { return this.smoothingAttributes; }
        }

        /// <summary>
        /// Gets a list of remotable methods.
        /// </summary>
        protected IList<MethodInfo> RemotableMethods
        {
            get { return this.remotableMethods; }
        }

        /// <summary>
        /// Gets the replicable entity's position property.
        /// </summary>
        protected PropertyInfo PositionProperty
        {
            get { return this.positionProperty; }
        }

        /// <summary>
        /// Gets the replicable entity's position property's smothing attribute, if present.
        /// </summary>
        protected SmoothingAttribute PositionSmoothingAttribute
        {
            get { return this.positionSmoothingAttribute; }
        }

        /// <summary>
        /// Gets the replicable entity's position property's derivative, if specified.
        /// </summary>
        protected PropertyInfo VelocityProperty
        {
            get { return this.velocityProperty; }
        }

        /// <inheritdoc/>
        public void HandleEvent(Stream stream)
        {
            using (var reader = new BinaryReader(stream))
            {
                byte index = reader.ReadByte();
                this.rpcDeserializers[index].Invoke(reader);
            }
        }

        /// <inheritdoc/>
        public abstract void Tick(TimeSpan currentTime);

        /// <inheritdoc/>
        public byte GetRemotableMethodIndex(string methodName)
        {
            return this.rpcIndices[methodName];
        }

        /// <summary>
        /// Populate the list of replicable properties.
        /// </summary>
        /// <param name="replicableEntityType">The type of the replicable entity.</param>
        private void CheckForPositionSmoothing(Type replicableEntityType)
        {
            this.positionProperty = replicableEntityType.GetProperty("Position");
            this.positionSmoothingAttribute = Attribute.GetCustomAttribute(
                this.positionProperty,
                typeof(SmoothingAttribute))
                as SmoothingAttribute;
            if (this.positionSmoothingAttribute != null &&
                this.positionSmoothingAttribute.RateOfChangeProperty != null)
            {
                this.velocityProperty = replicableEntityType.GetProperty(
                    this.positionSmoothingAttribute.RateOfChangeProperty);
                if (this.velocityProperty == null)
                {
                    throw new ReplicationException(string.Format(
                        "Cannot find rate of change property \"{0}\" for Position property.",
                        this.positionSmoothingAttribute.RateOfChangeProperty));
                }
            }
        }

        /// <summary>
        /// Populate the list of replicable properties.
        /// </summary>
        /// <param name="replicableEntityType">The type of the replicable entity.</param>
        private void PopulateReplicableProperties(Type replicableEntityType)
        {
            PropertyInfo[] properties = replicableEntityType.GetProperties(
                BindingFlags.Instance |
                BindingFlags.Public |
                BindingFlags.NonPublic);
            foreach (var property in properties)
            {
                if (Attribute.IsDefined(property, typeof(ReplicableAttribute)))
                {
                    this.replicableProperties.Add(property);
                    var smoothingAttribute = (SmoothingAttribute)Attribute.GetCustomAttribute(property, typeof(SmoothingAttribute));
                    if (smoothingAttribute != null)
                    {
                        this.smoothingAttributes.Add(property, smoothingAttribute);
                    }
                }
            }

            // We don't want to replicate derivatives twice.
            foreach (var derivativeProperty in this.derivatives)
            {
                this.replicableProperties.Remove(derivativeProperty);
            }
        }

        /// <summary>
        /// Populate the list of remotable methods.
        /// </summary>
        /// <param name="replicableEntityType">The type of the replicable entity.</param>
        private void PopulateRemotableMethods(Type replicableEntityType)
        {
            MethodInfo[] methods = replicableEntityType.GetMethods(
                BindingFlags.Instance |
                BindingFlags.Public |
                BindingFlags.NonPublic);
            byte index = 0;
            foreach (var method in methods)
            {
                if (Attribute.IsDefined(method, typeof(ReplicableAttribute)))
                {
                    foreach (var other in this.remotableMethods)
                    {
                        if (method.Name == other.Name)
                        {
                            throw new RPCException(
                                "Attempt to register multiple overloads for method named: " + method.Name);
                        }
                    }

                    this.remotableMethods.Add(method);
                    this.rpcIndices.Add(method.Name, index++);
                    this.rpcDeserializers.Add(
                        this.serialization.CreateMethodDeserializer(this.replicableEntity, method));
                }
            }
        }
    }
}