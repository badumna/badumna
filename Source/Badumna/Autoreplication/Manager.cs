﻿//------------------------------------------------------------------------------
// <copyright file="Manager.cs" company="National ICT Australia Limited">
//     Copyright (c) 2007-2012 National ICT Australia Limited. All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

namespace Badumna.Autoreplication
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using Badumna.Autoreplication.Serialization;
    using Badumna.Core;
    using Badumna.DataTypes;
    using Badumna.Replication;
    using Badumna.Utilities;

    /// <summary>
    ///  Delegate for creating an original entity autoreplication wrapper.
    /// </summary>
    /// <typeparam name="E">The type of the entity.</typeparam>
    /// <typeparam name="O">The type of the original entity wrapper.</typeparam>
    /// <typeparam name="T">The type of the data required by the wrapper.</typeparam>
    /// <param name="originalEntity">The original entity to wrap.</param>
    /// <param name="data">The data required by the wrapper.</param>
    /// <returns>A new original entity autoreplication wrapper.</returns>
    internal delegate O OriginalWrapperFactory<E, O, T>(E originalEntity, T data);

    /// <summary>
    ///  Delegate for creating a replica entity autoreplication wrapper.
    /// </summary>
    /// <typeparam name="E">The type of the entity.</typeparam>
    /// <typeparam name="R">The type of the replica entity wrapper.</typeparam>
    /// <param name="replicaEntity">The original entity to wrap.</param>
    /// <returns>A new replica entity autoreplication wrapper.</returns>
    internal delegate R ReplicaWrapperFactory<E, R>(E replicaEntity);

    /// <summary>
    /// Interface to permit mocking of Manager class.
    /// </summary>
    /// <typeparam name="E">The type used by the application to represent entities.</typeparam>
    /// <typeparam name="OW">The type used by the autoreplication manager to wrap original entities.</typeparam>
    /// <typeparam name="RW">The type used by the autoreplication manager to wrap replica entities.</typeparam>
    /// <typeparam name="T">The type used to represent any additional data required by original wrappers.</typeparam>
    /// <typeparam name="M">The type of the client entity manager.</typeparam>
    internal interface IManager<E, OW, RW, T, M> : IRPCService
        where E : class
        where OW : class, IOriginalWrapper
        where RW : class, IReplicaWrapper
    {
        /// <summary>
        /// Gets the serialization manager.
        /// </summary>
        Serialization.Manager Serialization { get; }

        /// <summary>
        /// Pass in the client entity manager.
        /// TODO: Remove circular dependency, so this can be done in constructor.
        /// </summary>
        /// <param name="entityManager">The client entity manager</param>
        void Initialize(M entityManager);

        /// <summary>
        /// Register an original for autoreplication.
        /// </summary>
        /// <param name="original">The original entity to register.</param>
        /// <param name="data">Data required by the wrapper.</param>
        /// <returns>A new autoreplication wrapper for the original entity that
        /// Badumna can use for traditional replication.</returns>
        OW RegisterOriginal(E original, T data);

        /// <summary>
        /// Unregister an original for autoreplication.
        /// </summary>
        /// <param name="original">The original entity to unregister.</param>
        /// <returns>The autoreplication wrapper for the original entity.</returns>
        OW UnregisterOriginal(E original);

        /// <summary>
        /// Create an autoreplication wrapper for an entity replica.
        /// </summary>
        /// <param name="replica">The entity replica to wrap.</param>
        /// <returns>A new autoreplication wrapper.</returns>
        RW RegisterReplica(E replica);

        /// <summary>
        /// Unregister a replica for autoreplication.
        /// </summary>
        /// <param name="replicaWrapper">The autoreplication wrapper to discard.</param>
        /// <returns>The replica entity that has been unregistered.</returns>
        E UnregisterReplica(RW replicaWrapper);

        /// <summary>
        /// Flag all an original entity's properties for update.
        /// </summary>
        /// <param name="original">The origianl entity.</param>
        void FlagFullUpdate(E original);

        /// <summary>
        /// Check registered originals for changed properties.
        /// </summary>
        /// <param name="currentTime">The current time.</param>
        void Tick(TimeSpan currentTime);

        /// <summary>
        /// Gets the ID for a given entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns>The ID for the entity.</returns>
        BadumnaId GetEntityID(E entity);
    }

    /// <summary>
    /// Manages Auto-replication.
    /// Responsible for creating wrappers for replicable entities implementing
    /// ISpatialOriginal or ISpatialReplica as required, flagging properties for
    /// update, and serializing and deserializing state updates.
    /// </summary>
    /// <typeparam name="E">The type used by the application to represent entities.</typeparam>
    /// <typeparam name="O">The type used by the client entity manager to wrap original entities.</typeparam>
    /// <typeparam name="R">The type used by the client entity manager to wrap replica entities.</typeparam>
    /// <typeparam name="OW">The type used by the autoreplication manager to wrap original entities.</typeparam>
    /// <typeparam name="RW">The type used by the autoreplication manager to wrap replica entities.</typeparam>
    /// <typeparam name="T">The type used to represent any additional data required by original wrappers.</typeparam>
    /// <typeparam name="M">The type of the client entity manager.</typeparam>
    internal class Manager<E, O, R, OW, RW, T, M> : IManager<E, OW, RW, T, M>
        where E : class
        where OW : class, O, IOriginalWrapper
        where RW : class, R, IReplicaWrapper
        where M : IReplicationService<O, R>
    {
        /// <summary>
        /// For manipulating replicable types.
        /// </summary>
        private readonly Serialization.Manager serialization;

        /// <summary>
        /// Factory for creating original wrappers.
        /// </summary>
        private readonly OriginalWrapperFactory<E, OW, T> originalWrapperFactory;

        /// <summary>
        /// Factory for creating original wrappers.
        /// </summary>
        private readonly ReplicaWrapperFactory<E, RW> replicaWrapperFactory;

        /// <summary>
        /// Provides access to current time.
        /// </summary>
        private readonly ITime timeKeeper;

        /// <summary>
        /// Wrappers for original entities registered for autoreplication.
        /// </summary>
        /// <remarks>
        /// Uses a reference equality comparer to prevent entitys' Equals method being called in network thread.
        /// Unity objects don't like that.
        /// </remarks>
        private readonly Dictionary<E, OW> originalWrappers = new Dictionary<E, OW>(ReferenceEqualityComparer<E>.Instance);

        /// <summary>
        /// For synchronizing access to the origianlWrappers dictionary.
        /// </summary>
        private readonly object originalWrappersLock = new object();

        /// <summary>
        /// Replica entities by wrapper.
        /// </summary>
        private readonly Dictionary<RW, E> replicas = new Dictionary<RW, E>();

        /// <summary>
        /// Wrappers for replica entities.
        /// </summary>
        /// <remarks>
        /// Uses a reference equality comparer to prevent entitys' Equals method being called in network thread.
        /// Unity objects don't like that.
        /// </remarks>
        private readonly Dictionary<E, RW> replicaWrappers = new Dictionary<E, RW>(ReferenceEqualityComparer<E>.Instance);

        /// <summary>
        /// Entity manager for flagging updates and sending custom messages.
        /// TODO: Make read only, once this depency is injected.
        /// </summary>
        private M wrappedEntityManager;

        /////// <summary>
        /////// Initializes a new instance of the Manager class.
        /////// </summary>
        /////// <param name="entityManager">For flagging udpates and custom messages for wrapped entities.</param>
        /////// <param name="serialization">For manipulating replicable types.</param>
        /////// <param name="timeKeeper">Provides access to current time.</param>
        ////public Manager(
        ////    M entityManger,
        ////    Serialization.Manager serialization,
        ////    ITime timeKeeper)
        ////    : this(
        ////    entityManger,
        ////    serialization,
        ////    timeKeeper,
        ////    (o, d) => new OriginalEntityWrapper(o, entityManager, serialization),
        ////    r => new ReplicaEntityWrapper(r, serialization, timeKeeper))
        ////{
        ////}

        /// <summary>
        /// Initializes a new instance of the Manager class.
        /// </summary>
        /// <param name="serialization">For manipulating replicable types.</param>
        /// <param name="timeKeeper">Provides access to current time.</param>
        /// <param name="originalWrapperFactory">Factory for creating original wrappers.</param>
        /// <param name="replicaWrapperFactory">Factory for creating replica wrappers.</param>
        public Manager(
            Serialization.Manager serialization,
            ITime timeKeeper,
            OriginalWrapperFactory<E, OW, T> originalWrapperFactory,
            ReplicaWrapperFactory<E, RW> replicaWrapperFactory)
        {
            if (serialization == null)
            {
                throw new ArgumentNullException("serialization");
            }

            if (timeKeeper == null)
            {
                throw new ArgumentNullException("timeKeeper");
            }

            if (originalWrapperFactory == null)
            {
                throw new ArgumentNullException("originalWrapperFactory");
            }

            if (replicaWrapperFactory == null)
            {
                throw new ArgumentNullException("replicaWrapperFactory");
            }

            this.serialization = serialization;
            this.timeKeeper = timeKeeper;
            this.originalWrapperFactory = originalWrapperFactory;
            this.replicaWrapperFactory = replicaWrapperFactory;
        }

        /// <inheritdoc />
        public Serialization.Manager Serialization
        {
            get
            {
                return this.serialization;
            }
        }

        /// <summary>
        /// Gets the wrapped entity manager.
        /// </summary>
        protected M WrappedEntityManager
        {
            get
            {
                return this.wrappedEntityManager;
            }
        }

        /// <inheritdoc />
        public void Initialize(M entityManager)
        {
            if (entityManager == null)
            {
                throw new ArgumentNullException("entityManager");
            }

            this.wrappedEntityManager = entityManager;
        }

        /// <summary>
        /// Register an original for autoreplication.
        /// </summary>
        /// <param name="original">The original entity to register.</param>
        /// <param name="data">Data required by the wrapper.</param>
        /// <returns>A new autoreplication wrapper for the original entity that
        /// Badumna can use for traditional replication.</returns>
        public OW RegisterOriginal(E original, T data)
        {
            if (original == null)
            {
                throw new ArgumentNullException("original");
            }

            lock (this.originalWrappersLock)
            {
                if (this.originalWrappers.ContainsKey(original))
                {
                    throw new InvalidOperationException("Entity already registered.");
                }

                OW originalWrapper = this.originalWrapperFactory.Invoke(original, data);
                this.originalWrappers.Add(original, originalWrapper);
                return originalWrapper;
            }
        }

        /// <summary>
        /// Mark an original as inaccessible to prevent access of its members.
        /// </summary>
        /// <param name="original">The original entity to make as inaccessible.</param>
        public void MarkOriginalInaccessible(E original)
        {
            if (original == null)
            {
                throw new ArgumentNullException("original");
            }

            lock (this.originalWrappersLock)
            {
                OW wrapper;
                if (this.originalWrappers.TryGetValue(original, out wrapper))
                {
                    wrapper.MarkInaccessible();
                }
                else
                {
                    // This could occur under certain race conditions!
                    Logger.TraceInformation(LogTag.Autoreplication, "Could not mark entity inaccessible as not registered.");
                }
            }
        }

        /// <summary>
        /// Unregister an original for autoreplication.
        /// </summary>
        /// <param name="original">The original entity to unregister.</param>
        /// <returns>The autoreplication wrapper for the original entity.</returns>
        public OW UnregisterOriginal(E original)
        {
            if (original == null)
            {
                throw new ArgumentNullException("original");
            }

            lock (this.originalWrappersLock)
            {
                OW wrapper;
                if (this.originalWrappers.TryGetValue(original, out wrapper))
                {
                    this.originalWrappers.Remove(original);
                    return wrapper;
                }

                throw new InvalidOperationException("Cannot unregister unregistered originals.");
            }
        }

        /// <summary>
        /// Create an autoreplication wrapper for an entity replica.
        /// </summary>
        /// <param name="replica">The entity replica to wrap.</param>
        /// <returns>A new autoreplication wrapper.</returns>
        public RW RegisterReplica(E replica)
        {
            RW wrapper = this.replicaWrapperFactory(replica);
            this.replicas.Add(wrapper, replica);
            this.replicaWrappers.Add(replica, wrapper);
            return wrapper;
        }

        /// <summary>
        /// Unregister a replica for autoreplication.
        /// </summary>
        /// <param name="replicaWrapper">The autoreplication wrapper to discard.</param>
        /// <returns>The replica entity that has been unregistered.</returns>
        public E UnregisterReplica(RW replicaWrapper)
        {
            E replica;
            if (this.replicas.TryGetValue(replicaWrapper, out replica))
            {
                this.replicas.Remove(replicaWrapper);
                this.replicaWrappers.Remove(replica);
                return replica;
            }

            throw new InvalidOperationException("Trying to unregister unregistered replica.");
        }

        /// <inheritdoc/>
        public void FlagFullUpdate(E original)
        {
            lock (this.originalWrappersLock)
            {
                OW wrapper;
                if (this.originalWrappers.TryGetValue(original, out wrapper))
                {
                    wrapper.FlagFullUpdate();
                    return;
                }

                throw new InvalidOperationException("Cannot flag unregistered originals.");
            }
        }

        /// <summary>
        /// Check registered originals for changed properties.
        /// </summary>
        /// <param name="currentTime">The current time.</param>
        public void Tick(TimeSpan currentTime)
        {
            lock (this.originalWrappersLock)
            {
                foreach (var originalWrapper in this.originalWrappers.Values)
                {
                    originalWrapper.Tick(currentTime);
                }
            }

            foreach (var replicaWrapper in this.replicas.Keys)
            {
                replicaWrapper.Tick(currentTime);
            }
        }

        /// <inheritdoc />
        public BadumnaId GetEntityID(E entity)
        {
            lock (this.originalWrappersLock)
            {
                OW originalWrapper;
                if (this.originalWrappers.TryGetValue(entity, out originalWrapper))
                {
                    return originalWrapper.Guid;
                }
            }

            RW replicaWrapper;
            if (this.replicaWrappers.TryGetValue(entity, out replicaWrapper))
            {
                return replicaWrapper.Guid;
            }

            return null;
        }

        /// <summary>
        /// Call a parameterless method on all the replicas of a given original.
        /// </summary>
        /// <param name="method">The corresponding method on the original.</param>
        public void CallMethodOnReplicas(RpcSignature method)
        {
            OW wrapper = null;
            if (!this.GetOriginalWrapperForTarget(method, out wrapper))
            {
                // Silently ignore unknown originals as they cannot have replicas yet.
                return;
            }

            using (var stream = new MemoryStream())
            using (var writer = new BinaryWriter(stream))
            {
                writer.Write(this.GetRemotableMethodIndex(wrapper, method.Method.Name));
                this.wrappedEntityManager.SendCustomMessageToReplicas(wrapper, stream);
            }
        }

        /// <summary>
        /// Call a method on all the replicas of a given original.
        /// </summary>
        /// <typeparam name="T1">The type of the method's first parameter.</typeparam>
        /// <param name="method">The corresponding method on the original.</param>
        /// <param name="arg1">The first parameter.</param>
        public void CallMethodOnReplicas<T1>(RpcSignature<T1> method, T1 arg1)
        {
            OW wrapper = null;
            if (!this.GetOriginalWrapperForTarget(method, out wrapper))
            {
                // Silently ignore unknown originals as they cannot have replicas yet.
                return;
            }

            using (var stream = new MemoryStream())
            using (var writer = new BinaryWriter(stream))
            {
                writer.Write(this.GetRemotableMethodIndex(wrapper, method.Method.Name));
                this.serialization.SerializeParameter(arg1, writer);
                this.wrappedEntityManager.SendCustomMessageToReplicas(wrapper, stream);
            }
        }

        /// <summary>
        /// Call a method on all the replicas of a given original.
        /// </summary>
        /// <typeparam name="T1">The type of the method's first parameter.</typeparam>
        /// <typeparam name="T2">The type of the method's second parameter.</typeparam>
        /// <param name="method">The corresponding method on the original.</param>
        /// <param name="arg1">The first parameter.</param>
        /// <param name="arg2">The second parameter.</param>
        public void CallMethodOnReplicas<T1, T2>(RpcSignature<T1, T2> method, T1 arg1, T2 arg2)
        {
            OW wrapper = null;
            if (!this.GetOriginalWrapperForTarget(method, out wrapper))
            {
                // Silently ignore unknown originals as they cannot have replicas yet.
                return;
            }

            using (var stream = new MemoryStream())
            using (var writer = new BinaryWriter(stream))
            {
                writer.Write(this.GetRemotableMethodIndex(wrapper, method.Method.Name));
                this.serialization.SerializeParameter(arg1, writer);
                this.serialization.SerializeParameter(arg2, writer);
                this.wrappedEntityManager.SendCustomMessageToReplicas(wrapper, stream);
            }
        }

        /// <summary>
        /// Call a method on all the replicas of a given original.
        /// </summary>
        /// <typeparam name="T1">The type of the method's first parameter.</typeparam>
        /// <typeparam name="T2">The type of the method's second parameter.</typeparam>
        /// <typeparam name="T3">The type of the method's third parameter.</typeparam>
        /// <param name="method">The corresponding method on the original.</param>
        /// <param name="arg1">The first parameter.</param>
        /// <param name="arg2">The second parameter.</param>
        /// <param name="arg3">The third parameter.</param>
        public void CallMethodOnReplicas<T1, T2, T3>(
            RpcSignature<T1, T2, T3> method,
            T1 arg1,
            T2 arg2,
            T3 arg3)
        {
            OW wrapper = null;
            if (!this.GetOriginalWrapperForTarget(method, out wrapper))
            {
                // Silently ignore unknown originals as they cannot have replicas yet.
                return;
            }

            using (var stream = new MemoryStream())
            using (var writer = new BinaryWriter(stream))
            {
                writer.Write(this.GetRemotableMethodIndex(wrapper, method.Method.Name));
                this.serialization.SerializeParameter(arg1, writer);
                this.serialization.SerializeParameter(arg2, writer);
                this.serialization.SerializeParameter(arg3, writer);
                this.wrappedEntityManager.SendCustomMessageToReplicas(wrapper, stream);
            }
        }

        /// <summary>
        /// Call a method on all the replicas of a given original.
        /// </summary>
        /// <typeparam name="T1">The type of the method's first parameter.</typeparam>
        /// <typeparam name="T2">The type of the method's second parameter.</typeparam>
        /// <typeparam name="T3">The type of the method's third parameter.</typeparam>
        /// <typeparam name="T4">The type of the method's fourth parameter.</typeparam>
        /// <param name="method">The corresponding method on the original.</param>
        /// <param name="arg1">The first parameter.</param>
        /// <param name="arg2">The second parameter.</param>
        /// <param name="arg3">The third parameter.</param>
        /// <param name="arg4">The fourth parameter.</param>
        public void CallMethodOnReplicas<T1, T2, T3, T4>(
            RpcSignature<T1, T2, T3, T4> method,
            T1 arg1,
            T2 arg2,
            T3 arg3,
            T4 arg4)
        {
            OW wrapper = null;
            if (!this.GetOriginalWrapperForTarget(method, out wrapper))
            {
                // Silently ignore unknown originals as they cannot have replicas yet.
                return;
            }

            using (var stream = new MemoryStream())
            using (var writer = new BinaryWriter(stream))
            {
                writer.Write(this.GetRemotableMethodIndex(wrapper, method.Method.Name));
                this.serialization.SerializeParameter(arg1, writer);
                this.serialization.SerializeParameter(arg2, writer);
                this.serialization.SerializeParameter(arg3, writer);
                this.serialization.SerializeParameter(arg4, writer);
                this.wrappedEntityManager.SendCustomMessageToReplicas(wrapper, stream);
            }
        }

        /// <summary>
        /// Call a method on all the replicas of a given original.
        /// </summary>
        /// <typeparam name="T1">The type of the method's first parameter.</typeparam>
        /// <typeparam name="T2">The type of the method's second parameter.</typeparam>
        /// <typeparam name="T3">The type of the method's third parameter.</typeparam>
        /// <typeparam name="T4">The type of the method's fourth parameter.</typeparam>
        /// <typeparam name="T5">The type of the method's fifth parameter.</typeparam>
        /// <param name="method">The corresponding method on the original.</param>
        /// <param name="arg1">The first parameter.</param>
        /// <param name="arg2">The second parameter.</param>
        /// <param name="arg3">The third parameter.</param>
        /// <param name="arg4">The fourth parameter.</param>
        /// <param name="arg5">The fifth parameter.</param>
        public void CallMethodOnReplicas<T1, T2, T3, T4, T5>(
            RpcSignature<T1, T2, T3, T4, T5> method,
            T1 arg1,
            T2 arg2,
            T3 arg3,
            T4 arg4,
            T5 arg5)
        {
            OW wrapper = null;
            if (!this.GetOriginalWrapperForTarget(method, out wrapper))
            {
                // Silently ignore unknown originals as they cannot have replicas yet.
                return;
            }

            using (var stream = new MemoryStream())
            using (var writer = new BinaryWriter(stream))
            {
                writer.Write(this.GetRemotableMethodIndex(wrapper, method.Method.Name));
                this.serialization.SerializeParameter(arg1, writer);
                this.serialization.SerializeParameter(arg2, writer);
                this.serialization.SerializeParameter(arg3, writer);
                this.serialization.SerializeParameter(arg4, writer);
                this.serialization.SerializeParameter(arg5, writer);
                this.wrappedEntityManager.SendCustomMessageToReplicas(wrapper, stream);
            }
        }

        /// <summary>
        /// Call a method on all the replicas of a given original.
        /// </summary>
        /// <typeparam name="T1">The type of the method's first parameter.</typeparam>
        /// <typeparam name="T2">The type of the method's second parameter.</typeparam>
        /// <typeparam name="T3">The type of the method's third parameter.</typeparam>
        /// <typeparam name="T4">The type of the method's fourth parameter.</typeparam>
        /// <typeparam name="T5">The type of the method's fifth parameter.</typeparam>
        /// <typeparam name="T6">The type of the method's sixth parameter.</typeparam>
        /// <param name="method">The corresponding method on the original.</param>
        /// <param name="arg1">The first parameter.</param>
        /// <param name="arg2">The second parameter.</param>
        /// <param name="arg3">The third parameter.</param>
        /// <param name="arg4">The fourth parameter.</param>
        /// <param name="arg5">The fifth parameter.</param>
        /// <param name="arg6">The sixth parameter.</param>
        public void CallMethodOnReplicas<T1, T2, T3, T4, T5, T6>(
            RpcSignature<T1, T2, T3, T4, T5, T6> method,
            T1 arg1,
            T2 arg2,
            T3 arg3,
            T4 arg4,
            T5 arg5,
            T6 arg6)
        {
            OW wrapper = null;
            if (!this.GetOriginalWrapperForTarget(method, out wrapper))
            {
                // Silently ignore unknown originals as they cannot have replicas yet.
                return;
            }

            using (var stream = new MemoryStream())
            using (var writer = new BinaryWriter(stream))
            {
                writer.Write(this.GetRemotableMethodIndex(wrapper, method.Method.Name));
                this.serialization.SerializeParameter(arg1, writer);
                this.serialization.SerializeParameter(arg2, writer);
                this.serialization.SerializeParameter(arg3, writer);
                this.serialization.SerializeParameter(arg4, writer);
                this.serialization.SerializeParameter(arg5, writer);
                this.serialization.SerializeParameter(arg6, writer);
                this.wrappedEntityManager.SendCustomMessageToReplicas(wrapper, stream);
            }
        }

        /// <summary>
        /// Call a method on all the original of a given replica.
        /// </summary>
        /// <param name="method">The corresponding method on the replica.</param>
        public void CallMethodOnOriginal(RpcSignature method)
        {
            RW wrapper;
            if (!this.GetReplicaWrapperForTarget(method, out wrapper))
            {
                // Silently ignore unknown replica.
                return;
            }

            using (var stream = new MemoryStream())
            using (var writer = new BinaryWriter(stream))
            {
                writer.Write(this.GetRemotableMethodIndex(wrapper, method.Method.Name));
                this.wrappedEntityManager.SendCustomMessageToOriginal(wrapper, stream);
            }
        }

        /// <summary>
        /// Call a method on all the original of a given replica.
        /// </summary>
        /// <typeparam name="T1">The type of the method's first parameter.</typeparam>
        /// <param name="method">The corresponding method on the replica.</param>
        /// <param name="arg1">The first parameter.</param>
        public void CallMethodOnOriginal<T1>(RpcSignature<T1> method, T1 arg1)
        {
            RW wrapper;
            if (!this.GetReplicaWrapperForTarget(method, out wrapper))
            {
                // Silently ignore unknown replica.
                return;
            }

            using (var stream = new MemoryStream())
            using (var writer = new BinaryWriter(stream))
            {
                writer.Write(this.GetRemotableMethodIndex(wrapper, method.Method.Name));
                this.serialization.SerializeParameter(arg1, writer);
                this.wrappedEntityManager.SendCustomMessageToOriginal(wrapper, stream);
            }
        }

        /// <summary>
        /// Call a method on all the original of a given replica.
        /// </summary>
        /// <typeparam name="T1">The type of the method's first parameter.</typeparam>
        /// <typeparam name="T2">The type of the method's second parameter.</typeparam>
        /// <param name="method">The corresponding method on the replica.</param>
        /// <param name="arg1">The first parameter.</param>
        /// <param name="arg2">The second parameter.</param>
        public void CallMethodOnOriginal<T1, T2>(RpcSignature<T1, T2> method, T1 arg1, T2 arg2)
        {
            RW wrapper;
            if (!this.GetReplicaWrapperForTarget(method, out wrapper))
            {
                // Silently ignore unknown replica.
                return;
            }

            using (var stream = new MemoryStream())
            using (var writer = new BinaryWriter(stream))
            {
                writer.Write(this.GetRemotableMethodIndex(wrapper, method.Method.Name));
                this.serialization.SerializeParameter(arg1, writer);
                this.serialization.SerializeParameter(arg2, writer);
                this.wrappedEntityManager.SendCustomMessageToOriginal(wrapper, stream);
            }
        }

        /// <summary>
        /// Call a method on all the original of a given replica.
        /// </summary>
        /// <typeparam name="T1">The type of the method's first parameter.</typeparam>
        /// <typeparam name="T2">The type of the method's second parameter.</typeparam>
        /// <typeparam name="T3">The type of the method's third parameter.</typeparam>
        /// <param name="method">The corresponding method on the replica.</param>
        /// <param name="arg1">The first parameter.</param>
        /// <param name="arg2">The second parameter.</param>
        /// <param name="arg3">The third parameter.</param>
        public void CallMethodOnOriginal<T1, T2, T3>(
            RpcSignature<T1, T2, T3> method,
            T1 arg1,
            T2 arg2,
            T3 arg3)
        {
            RW wrapper;

            if (!this.GetReplicaWrapperForTarget(method, out wrapper))
            {
                // Silently ignore unknown replica.
                return;
            }

            using (var stream = new MemoryStream())
            using (var writer = new BinaryWriter(stream))
            {
                writer.Write(this.GetRemotableMethodIndex(wrapper, method.Method.Name));
                this.serialization.SerializeParameter(arg1, writer);
                this.serialization.SerializeParameter(arg2, writer);
                this.serialization.SerializeParameter(arg3, writer);
                this.wrappedEntityManager.SendCustomMessageToOriginal(wrapper, stream);
            }
        }

        /// <summary>
        /// Call a method on all the original of a given replica.
        /// </summary>
        /// <typeparam name="T1">The type of the method's first parameter.</typeparam>
        /// <typeparam name="T2">The type of the method's second parameter.</typeparam>
        /// <typeparam name="T3">The type of the method's third parameter.</typeparam>
        /// <typeparam name="T4">The type of the method's fourth parameter.</typeparam>
        /// <param name="method">The corresponding method on the replica.</param>
        /// <param name="arg1">The first parameter.</param>
        /// <param name="arg2">The second parameter.</param>
        /// <param name="arg3">The third parameter.</param>
        /// <param name="arg4">The fourth parameter.</param>
        public void CallMethodOnOriginal<T1, T2, T3, T4>(
            RpcSignature<T1, T2, T3, T4> method,
            T1 arg1,
            T2 arg2,
            T3 arg3,
            T4 arg4)
        {
            RW wrapper;
            if (!this.GetReplicaWrapperForTarget(method, out wrapper))
            {
                // Silently ignore unknown replica.
                return;
            }

            using (var stream = new MemoryStream())
            using (var writer = new BinaryWriter(stream))
            {
                writer.Write(this.GetRemotableMethodIndex(wrapper, method.Method.Name));
                this.serialization.SerializeParameter(arg1, writer);
                this.serialization.SerializeParameter(arg2, writer);
                this.serialization.SerializeParameter(arg3, writer);
                this.serialization.SerializeParameter(arg4, writer);
                this.wrappedEntityManager.SendCustomMessageToOriginal(wrapper, stream);
            }
        }

        /// <summary>
        /// Call a method on all the original of a given replica.
        /// </summary>
        /// <typeparam name="T1">The type of the method's first parameter.</typeparam>
        /// <typeparam name="T2">The type of the method's second parameter.</typeparam>
        /// <typeparam name="T3">The type of the method's third parameter.</typeparam>
        /// <typeparam name="T4">The type of the method's fourth parameter.</typeparam>
        /// <typeparam name="T5">The type of the method's fifth parameter.</typeparam>
        /// <param name="method">The corresponding method on the replica.</param>
        /// <param name="arg1">The first parameter.</param>
        /// <param name="arg2">The second parameter.</param>
        /// <param name="arg3">The third parameter.</param>
        /// <param name="arg4">The fourth parameter.</param>
        /// <param name="arg5">The fifth parameter.</param>
        public void CallMethodOnOriginal<T1, T2, T3, T4, T5>(
            RpcSignature<T1, T2, T3, T4, T5> method,
            T1 arg1,
            T2 arg2,
            T3 arg3,
            T4 arg4,
            T5 arg5)
        {
            RW wrapper;
            if (!this.GetReplicaWrapperForTarget(method, out wrapper))
            {
                // Silently ignore unknown replica.
                return;
            }

            using (var stream = new MemoryStream())
            using (var writer = new BinaryWriter(stream))
            {
                writer.Write(this.GetRemotableMethodIndex(wrapper, method.Method.Name));
                this.serialization.SerializeParameter(arg1, writer);
                this.serialization.SerializeParameter(arg2, writer);
                this.serialization.SerializeParameter(arg3, writer);
                this.serialization.SerializeParameter(arg4, writer);
                this.serialization.SerializeParameter(arg5, writer);
                this.wrappedEntityManager.SendCustomMessageToOriginal(wrapper, stream);
            }
        }

        /// <summary>
        /// Call a method on all the original of a given replica.
        /// </summary>
        /// <typeparam name="T1">The type of the method's first parameter.</typeparam>
        /// <typeparam name="T2">The type of the method's second parameter.</typeparam>
        /// <typeparam name="T3">The type of the method's third parameter.</typeparam>
        /// <typeparam name="T4">The type of the method's fourth parameter.</typeparam>
        /// <typeparam name="T5">The type of the method's fifth parameter.</typeparam>
        /// <typeparam name="T6">The type of the method's sixth parameter.</typeparam>
        /// <param name="method">The corresponding method on the replica.</param>
        /// <param name="arg1">The first parameter.</param>
        /// <param name="arg2">The second parameter.</param>
        /// <param name="arg3">The third parameter.</param>
        /// <param name="arg4">The fourth parameter.</param>
        /// <param name="arg5">The fifth parameter.</param>
        /// <param name="arg6">The sixth parameter.</param>
        public void CallMethodOnOriginal<T1, T2, T3, T4, T5, T6>(
            RpcSignature<T1, T2, T3, T4, T5, T6> method,
            T1 arg1,
            T2 arg2,
            T3 arg3,
            T4 arg4,
            T5 arg5,
            T6 arg6)
        {
            RW wrapper;
            if (!this.GetReplicaWrapperForTarget(method, out wrapper))
            {
                // Silently ignore unknown replica.
                return;
            }

            using (var stream = new MemoryStream())
            using (var writer = new BinaryWriter(stream))
            {
                writer.Write(this.GetRemotableMethodIndex(wrapper, method.Method.Name));
                this.serialization.SerializeParameter(arg1, writer);
                this.serialization.SerializeParameter(arg2, writer);
                this.serialization.SerializeParameter(arg3, writer);
                this.serialization.SerializeParameter(arg4, writer);
                this.serialization.SerializeParameter(arg5, writer);
                this.serialization.SerializeParameter(arg6, writer);
                this.wrappedEntityManager.SendCustomMessageToOriginal(wrapper, stream);
            }
        }    

        /// <summary>
        /// Gets the wrapper for the original entity that is the target of a give delegate.
        /// </summary>
        /// <param name="method">A method on an original entity.</param>
        /// <param name="originalWrapper">Set to the wrapper for the original entity, if found.</param>
        /// <returns><c>true</c> if the wrapper was found, otherwise <c>false</c>.</returns>
        /// <exception cref="RPCException">Thrown if the delegate's target is not a spatial entity.</exception>
        /// <remarks>The wrapper will not be found if the original has not joined a scene yet.</remarks>
        protected bool GetOriginalWrapperForTarget(Delegate method, out OW originalWrapper)
        {
            var target = method.Target as E;
            if (target == null)
            {
                throw new RPCException("Attempting to make RPC on entity not of type " + typeof(E).ToString());
            }

            lock (this.originalWrappersLock)
            {
                return this.originalWrappers.TryGetValue(target, out originalWrapper);
            }
        }

        /// <summary>
        /// Gets the wrapper for the replica entity that is the target of a given delegate.
        /// </summary>
        /// <param name="method">A method on a replica entity.</param>
        /// <param name="replicaWrapper">Set to the wrapper for the replica entity, if found.</param>
        /// <returns><c>true</c> if the wrapper was found, otherwise <c>false</c>.</returns>
        /// <exception cref="RPCException">Thrown if the delegate's target is not a spatial entity.</exception>
        protected bool GetReplicaWrapperForTarget(Delegate method, out RW replicaWrapper)
        {
            var target = method.Target as E;
            if (target == null)
            {
                throw new RPCException("Attempting to make RPC on entity not of type " + typeof(E).ToString());
            }

            if (!this.replicaWrappers.ContainsKey(target))
            {
                Logger.TraceWarning(LogTag.Autoreplication, "The method passed is not a method on a known replica. It is possible that the replica has just been removed or you have passed method from incorrect object.");
            }

            return this.replicaWrappers.TryGetValue(target, out replicaWrapper);
        }

        /// <summary>
        /// Gets the index assigned to a remotable method for use in an RPC call.
        /// </summary>
        /// <param name="wrapper">A wrapper for the entity the method belongs to.</param>
        /// <param name="methodName">The name of the method.</param>
        /// <returns>The index assigned to the method. </returns>
        protected byte GetRemotableMethodIndex(IEntityWrapper wrapper, string methodName)
        {
            try
            {
                return wrapper.GetRemotableMethodIndex(methodName);
            }
            catch (KeyNotFoundException)
            {
                throw new RPCException("Attempt to remotely call unregistered method: " + methodName);
            }
        }
    }
}
