﻿// ------------------------------------------------------------------------------
//  <copyright file="DeadReckoningSerializer.cs" company="National ICT Australia Limited">
//      Copyright (c) 2012-2012 National ICT Australia Limited. All rights reserved.
//  </copyright>
// ------------------------------------------------------------------------------
namespace Badumna.Autoreplication.Serialization
{
    using System;
    using System.IO;
    using System.Reflection;

    using Badumna.Utilities;

    /// <summary>
    /// For checking need to send updates for dead-reckoned properties, and serializing updates.
    /// </summary>
    /// <remarks>Updates are only required when the derivative changes.</remarks>
    internal class DeadReckoningSerializer : ISerializer
    {
        /// <summary>
        /// A serializer for the property's value itself.
        /// </summary>
        private readonly ISerializer variableSerialier;

        /// <summary>
        /// A serizlier for the derivative of the property value.
        /// </summary>
        private readonly ISerializer derivativeSerializer;

        /// <summary>
        /// Initializes a new instance of the DeadReckoningSerializer class.
        /// </summary>
        /// <param name="variableSerialier">The serializer for the variable property.</param>
        /// <param name="derivativeSerializer">The serializer for the derivative property.</param>
        public DeadReckoningSerializer(ISerializer variableSerialier, ISerializer derivativeSerializer)
        {
            if (variableSerialier == null)
            {
                throw new ArgumentNullException("variableSerializer");
            }

            if (derivativeSerializer == null)
            {
                throw new ArgumentNullException("derivativeSerializer");
            }

            this.variableSerialier = variableSerialier;
            this.derivativeSerializer = derivativeSerializer;
        }

        /// <inheritdoc />
        public bool Check()
        {
            return this.derivativeSerializer.Check();
        }

        /// <inheritdoc />
        public void Serialize(BinaryWriter writer)
        {
            this.variableSerialier.Serialize(writer);
            this.derivativeSerializer.Serialize(writer);
        }
    }
}