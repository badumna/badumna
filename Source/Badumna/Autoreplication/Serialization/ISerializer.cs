// ------------------------------------------------------------------------------
//  <copyright file="ISerializer.cs" company="National ICT Australia Limited">
//      Copyright (c) 2012-2012 National ICT Australia Limited. All rights reserved.
//  </copyright>
// ------------------------------------------------------------------------------

namespace Badumna.Autoreplication.Serialization
{
    using System.IO;

    /// <summary>
    /// Non-generic interface for generic ValueTypeSerializer.
    /// </summary>
    internal interface ISerializer
    {
        /// <summary>
        /// Check to see if the value of a property of an object has changed since last check.
        /// </summary>
        /// <returns><c>true</c> if the value has changed since last check, otherwise <c>false</c>.</returns>
        bool Check();

        /// <summary>
        /// Write the value of a property of an object to a stream.
        /// </summary>
        /// <param name="writer">A binary writer for writing to the stream.</param>
        void Serialize(BinaryWriter writer);
    }
}