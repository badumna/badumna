﻿// -----------------------------------------------------------------------
// <copyright file="RPCSignatureRegistrar.cs" company="National ICT Australia Limited">
//      Copyright (c) 2013 National ICT Australia Limited. All rights reserved.
//  </copyright>
// -----------------------------------------------------------------------

namespace Badumna.Autoreplication.Serialization
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Badumna.DataTypes;

    /// <summary>
    /// For registering a set of RPC signatures with an RPC manager.
    /// </summary>
    internal static class RPCSignatureRegistrar
    {
        /// <summary>
        /// Register a set of RPC signatures with an RPC manager.
        /// </summary>
        /// <param name="manager">The RPC manager.</param>
        internal static void RegisterSignatures(Serialization.Manager manager)
        {
            manager.RegisterRPCSignature<bool>();
            manager.RegisterRPCSignature<bool, bool>();
            manager.RegisterRPCSignature<bool, bool, bool>();
            manager.RegisterRPCSignature<bool, bool, int>();
            manager.RegisterRPCSignature<bool, bool, float>();
            manager.RegisterRPCSignature<bool, bool, string>();
            manager.RegisterRPCSignature<bool, bool, Vector3>();
            manager.RegisterRPCSignature<bool, int>();
            manager.RegisterRPCSignature<bool, int, bool>();
            manager.RegisterRPCSignature<bool, int, int>();
            manager.RegisterRPCSignature<bool, int, float>();
            manager.RegisterRPCSignature<bool, int, string>();
            manager.RegisterRPCSignature<bool, int, Vector3>();
            manager.RegisterRPCSignature<bool, float>();
            manager.RegisterRPCSignature<bool, float, bool>();
            manager.RegisterRPCSignature<bool, float, int>();
            manager.RegisterRPCSignature<bool, float, float>();
            manager.RegisterRPCSignature<bool, float, string>();
            manager.RegisterRPCSignature<bool, float, Vector3>();
            manager.RegisterRPCSignature<bool, string>();
            manager.RegisterRPCSignature<bool, string, bool>();
            manager.RegisterRPCSignature<bool, string, int>();
            manager.RegisterRPCSignature<bool, string, float>();
            manager.RegisterRPCSignature<bool, string, string>();
            manager.RegisterRPCSignature<bool, string, Vector3>();
            manager.RegisterRPCSignature<bool, Vector3>();
            manager.RegisterRPCSignature<bool, Vector3, bool>();
            manager.RegisterRPCSignature<bool, Vector3, int>();
            manager.RegisterRPCSignature<bool, Vector3, float>();
            manager.RegisterRPCSignature<bool, Vector3, string>();
            manager.RegisterRPCSignature<bool, Vector3, Vector3>();
            manager.RegisterRPCSignature<int>();
            manager.RegisterRPCSignature<int, bool>();
            manager.RegisterRPCSignature<int, bool, bool>();
            manager.RegisterRPCSignature<int, bool, int>();
            manager.RegisterRPCSignature<int, bool, float>();
            manager.RegisterRPCSignature<int, bool, string>();
            manager.RegisterRPCSignature<int, bool, Vector3>();
            manager.RegisterRPCSignature<int, int>();
            manager.RegisterRPCSignature<int, int, bool>();
            manager.RegisterRPCSignature<int, int, int>();
            manager.RegisterRPCSignature<int, int, float>();
            manager.RegisterRPCSignature<int, int, string>();
            manager.RegisterRPCSignature<int, int, Vector3>();
            manager.RegisterRPCSignature<int, float>();
            manager.RegisterRPCSignature<int, float, bool>();
            manager.RegisterRPCSignature<int, float, int>();
            manager.RegisterRPCSignature<int, float, float>();
            manager.RegisterRPCSignature<int, float, string>();
            manager.RegisterRPCSignature<int, float, Vector3>();
            manager.RegisterRPCSignature<int, string>();
            manager.RegisterRPCSignature<int, string, bool>();
            manager.RegisterRPCSignature<int, string, int>();
            manager.RegisterRPCSignature<int, string, float>();
            manager.RegisterRPCSignature<int, string, string>();
            manager.RegisterRPCSignature<int, string, Vector3>();
            manager.RegisterRPCSignature<int, Vector3>();
            manager.RegisterRPCSignature<int, Vector3, bool>();
            manager.RegisterRPCSignature<int, Vector3, int>();
            manager.RegisterRPCSignature<int, Vector3, float>();
            manager.RegisterRPCSignature<int, Vector3, string>();
            manager.RegisterRPCSignature<int, Vector3, Vector3>();
            manager.RegisterRPCSignature<float>();
            manager.RegisterRPCSignature<float, bool>();
            manager.RegisterRPCSignature<float, bool, bool>();
            manager.RegisterRPCSignature<float, bool, int>();
            manager.RegisterRPCSignature<float, bool, float>();
            manager.RegisterRPCSignature<float, bool, string>();
            manager.RegisterRPCSignature<float, bool, Vector3>();
            manager.RegisterRPCSignature<float, int>();
            manager.RegisterRPCSignature<float, int, bool>();
            manager.RegisterRPCSignature<float, int, int>();
            manager.RegisterRPCSignature<float, int, float>();
            manager.RegisterRPCSignature<float, int, string>();
            manager.RegisterRPCSignature<float, int, Vector3>();
            manager.RegisterRPCSignature<float, float>();
            manager.RegisterRPCSignature<float, float, bool>();
            manager.RegisterRPCSignature<float, float, int>();
            manager.RegisterRPCSignature<float, float, float>();
            manager.RegisterRPCSignature<float, float, string>();
            manager.RegisterRPCSignature<float, float, Vector3>();
            manager.RegisterRPCSignature<float, string>();
            manager.RegisterRPCSignature<float, string, bool>();
            manager.RegisterRPCSignature<float, string, int>();
            manager.RegisterRPCSignature<float, string, float>();
            manager.RegisterRPCSignature<float, string, string>();
            manager.RegisterRPCSignature<float, string, Vector3>();
            manager.RegisterRPCSignature<float, Vector3>();
            manager.RegisterRPCSignature<float, Vector3, bool>();
            manager.RegisterRPCSignature<float, Vector3, int>();
            manager.RegisterRPCSignature<float, Vector3, float>();
            manager.RegisterRPCSignature<float, Vector3, string>();
            manager.RegisterRPCSignature<float, Vector3, Vector3>();
            manager.RegisterRPCSignature<string>();
            manager.RegisterRPCSignature<string, bool>();
            manager.RegisterRPCSignature<string, bool, bool>();
            manager.RegisterRPCSignature<string, bool, int>();
            manager.RegisterRPCSignature<string, bool, float>();
            manager.RegisterRPCSignature<string, bool, string>();
            manager.RegisterRPCSignature<string, bool, Vector3>();
            manager.RegisterRPCSignature<string, int>();
            manager.RegisterRPCSignature<string, int, bool>();
            manager.RegisterRPCSignature<string, int, int>();
            manager.RegisterRPCSignature<string, int, float>();
            manager.RegisterRPCSignature<string, int, string>();
            manager.RegisterRPCSignature<string, int, Vector3>();
            manager.RegisterRPCSignature<string, float>();
            manager.RegisterRPCSignature<string, float, bool>();
            manager.RegisterRPCSignature<string, float, int>();
            manager.RegisterRPCSignature<string, float, float>();
            manager.RegisterRPCSignature<string, float, string>();
            manager.RegisterRPCSignature<string, float, Vector3>();
            manager.RegisterRPCSignature<string, string>();
            manager.RegisterRPCSignature<string, string, bool>();
            manager.RegisterRPCSignature<string, string, int>();
            manager.RegisterRPCSignature<string, string, float>();
            manager.RegisterRPCSignature<string, string, string>();
            manager.RegisterRPCSignature<string, string, Vector3>();
            manager.RegisterRPCSignature<string, Vector3>();
            manager.RegisterRPCSignature<string, Vector3, bool>();
            manager.RegisterRPCSignature<string, Vector3, int>();
            manager.RegisterRPCSignature<string, Vector3, float>();
            manager.RegisterRPCSignature<string, Vector3, string>();
            manager.RegisterRPCSignature<string, Vector3, Vector3>();
            manager.RegisterRPCSignature<Vector3>();
            manager.RegisterRPCSignature<Vector3, bool>();
            manager.RegisterRPCSignature<Vector3, bool, bool>();
            manager.RegisterRPCSignature<Vector3, bool, int>();
            manager.RegisterRPCSignature<Vector3, bool, float>();
            manager.RegisterRPCSignature<Vector3, bool, string>();
            manager.RegisterRPCSignature<Vector3, bool, Vector3>();
            manager.RegisterRPCSignature<Vector3, int>();
            manager.RegisterRPCSignature<Vector3, int, bool>();
            manager.RegisterRPCSignature<Vector3, int, int>();
            manager.RegisterRPCSignature<Vector3, int, float>();
            manager.RegisterRPCSignature<Vector3, int, string>();
            manager.RegisterRPCSignature<Vector3, int, Vector3>();
            manager.RegisterRPCSignature<Vector3, float>();
            manager.RegisterRPCSignature<Vector3, float, bool>();
            manager.RegisterRPCSignature<Vector3, float, int>();
            manager.RegisterRPCSignature<Vector3, float, float>();
            manager.RegisterRPCSignature<Vector3, float, string>();
            manager.RegisterRPCSignature<Vector3, float, Vector3>();
            manager.RegisterRPCSignature<Vector3, string>();
            manager.RegisterRPCSignature<Vector3, string, bool>();
            manager.RegisterRPCSignature<Vector3, string, int>();
            manager.RegisterRPCSignature<Vector3, string, float>();
            manager.RegisterRPCSignature<Vector3, string, string>();
            manager.RegisterRPCSignature<Vector3, string, Vector3>();
            manager.RegisterRPCSignature<Vector3, Vector3>();
            manager.RegisterRPCSignature<Vector3, Vector3, bool>();
            manager.RegisterRPCSignature<Vector3, Vector3, int>();
            manager.RegisterRPCSignature<Vector3, Vector3, float>();
            manager.RegisterRPCSignature<Vector3, Vector3, string>();
            manager.RegisterRPCSignature<Vector3, Vector3, Vector3>();
        }
    }
}
