// ------------------------------------------------------------------------------
//  <copyright file="ISmoothingDeserializer.cs" company="National ICT Australia Limited">
//      Copyright (c) 2012-2012 National ICT Australia Limited. All rights reserved.
//  </copyright>
// ------------------------------------------------------------------------------

namespace Badumna.Autoreplication.Serialization
{
    using System;

    /// <summary>
    /// Non-generic interface for generic smoothing deserializer.
    /// </summary>
    internal interface ISmoothingDeserializer : IDeserializer
    {
        /// <summary>
        /// Update the property with a smoothed value.
        /// </summary>
        /// <param name="currentTime">The current time.</param>
        void Tick(TimeSpan currentTime);
    }
}