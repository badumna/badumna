﻿// ------------------------------------------------------------------------------
//  <copyright file="ValueTypeSerializer.cs" company="National ICT Australia Limited">
//      Copyright (c) 2012-2012 National ICT Australia Limited. All rights reserved.
//  </copyright>
// ------------------------------------------------------------------------------
namespace Badumna.Autoreplication.Serialization
{
    using System;
    using System.IO;
    using System.Reflection;

    using Badumna.Utilities;

    /// <summary>
    /// For checking for changes in the value of a value type property of an object,
    /// and writing its value to a stream.
    /// </summary>
    /// <typeparam name="T">The type of the property being updated (must be a value type).</typeparam>
    /// <remarks>The type should be either a value type, or an immutable reference type.
    /// For mutable reference types, <see cref="ReferenceTypeSerializer{T}"/>
    /// should be used instead.</remarks>
    internal class ValueTypeSerializer<T> : ISerializer
    {
        /// <summary>
        /// Delegate for getting the value of the property.
        /// </summary>
        private readonly GenericCallBackReturn<T> getter;

        /// <summary>
        /// Delegate for writing a value of the type of the property to a stream.
        /// </summary>
        private readonly SerializationMethod<T> serializer;

        /// <summary>
        /// Baseline value against which checks for property value changes should be made.
        /// </summary>
        private T baseline;

        /// <summary>
        /// Initializes a new instance of the ValueTypeSerializer class.
        /// </summary>
        /// <param name="target">The object whose property is to be serialized.</param>
        /// <param name="property">The property to be serialized.</param>
        /// <param name="serializer">A delegate for writing the value of the property to a stream.</param>
        public ValueTypeSerializer(object target, PropertyInfo property, SerializationMethod<T> serializer)
        {
            // This line is force iOS-Unity AOT compiler to generate the correct type.
            this.getter = () => default(T);
            this.getter =
                (GenericCallBackReturn<T>)
                Delegate.CreateDelegate(typeof(GenericCallBackReturn<T>), target, property.GetGetMethod(true));
            this.serializer = serializer;
        }

        /// <inheritdoc />
        public bool Check()
        {
            T currentValue = this.getter.Invoke();
            if (Object.Equals(this.baseline, currentValue))
            {
                return false;
            }

            return true;
        }

        /// <inheritdoc />
        public void Serialize(BinaryWriter writer)
        {
            this.baseline = this.getter.Invoke();
            this.serializer.Invoke(this.baseline, writer);
        }
    }
}