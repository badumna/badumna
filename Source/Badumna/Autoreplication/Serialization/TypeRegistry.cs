﻿// ------------------------------------------------------------------------------
//  <copyright file="TypeRegistry.cs" company="National ICT Australia Limited">
//      Copyright (c) 2012-2012 National ICT Australia Limited. All rights reserved.
//  </copyright>
// ------------------------------------------------------------------------------
namespace Badumna
{
    using Badumna.Autoreplication.Serialization;

    /// <summary>
    /// Registry for replicable types.
    /// </summary>
    /// <remarks>Any replicable property on a spatial entity must have its type registred to
    /// enable Badumna to replicate its value over the network.
    /// The following .NET types are automatically registered:
    /// bool, byte, decimal, double, char, float, int, long, sbyte, short, string, uint, ulong, ushort,
    /// as is Badumna.Vector3.</remarks>
    public class TypeRegistry
    {
        /// <summary>
        /// Serialization manager to register the types with.
        /// </summary>
        private readonly Manager serialization;

        /// <summary>
        /// Initializes a new instance of the TypeRegistry class.
        /// </summary>
        /// <param name="serialization">The serialization manager to register the types with.</param>
        internal TypeRegistry(Manager serialization)
        {
            this.serialization = serialization;
        }

        /// <summary>
        /// Register a value type for replication.
        /// </summary>
        /// <typeparam name="T">The value type being registered.</typeparam>
        /// <param name="serializer">A delegate for serializing values of the type to a stream.</param>
        /// <param name="deserializer">A delegate for deserializing values of the type from a stream.</param>
        public void RegisterValueType<T>(SerializationMethod<T> serializer, DeserializationMethod<T> deserializer)
            where T : struct
        {
            this.serialization.RegisterValueType(serializer, deserializer);
        }

        /// <summary>
        /// Register an immutable reference type for replication.
        /// </summary>
        /// <typeparam name="T">The immutable reference type being registered.</typeparam>
        /// <param name="serializer">A delegate for serializing values of the type to a stream.</param>
        /// <param name="deserializer">A delegate for deserializing values of the type from a stream.</param>
        public void RegisterImmutableReferenceType<T>(
            SerializationMethod<T> serializer,
            DeserializationMethod<T> deserializer)
            where T : class
        {
            this.serialization.RegisterImmutableReferenceType(serializer, deserializer);
        }

        /// <summary>
        /// Register a mutable reference type for replication.
        /// </summary>
        /// <typeparam name="T">The mutable reference type being registered.</typeparam>
        /// <param name="serializer">A delegate for serializing values of the type to a stream.</param>
        /// <param name="deserializer">A delegate for deserializing values of the type from a stream.</param>
        /// <param name="copyMethod">A delegate for creating a new instance of the type, with a value
        /// equal to a given instance.</param>
        public void RegisterMutableReferenceType<T>(
            SerializationMethod<T> serializer,
            DeserializationMethod<T> deserializer,
            FactoryMethod<T> copyMethod)
            where T : class
        {
            this.serialization.RegisterMutableReferenceType(serializer, deserializer, copyMethod);
        }
    }
}