﻿//------------------------------------------------------------------------------
// <copyright file="Manager.cs" company="National ICT Australia Limited">
//     Copyright (c) 2012-2012 National ICT Australia Limited. All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

namespace Badumna.Autoreplication.Serialization
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Reflection;
    using System.Text;
    using Badumna.Core;
    using Badumna.DataTypes;
    using Badumna.Match;
    using Badumna.Utilities;

    /// <summary>
    /// Delegate for representing the signature of a method with no parameters.
    /// </summary>
    public delegate void RpcSignature();

    /// <summary>
    /// Delegate for representing the signature of a method with 1 parameter.
    /// </summary>
    /// <typeparam name="T1">The type of the first parameter.</typeparam>
    /// <param name="arg1">The first parameter.</param>
    public delegate void RpcSignature<T1>(T1 arg1);

    /// <summary>
    /// Delegate for representing the signature of a method with 2 parameters.
    /// </summary>
    /// <typeparam name="T1">The type of the first parameter.</typeparam>
    /// <typeparam name="T2">The type of the second parameter.</typeparam>
    /// <param name="arg1">The first parameter.</param>
    /// <param name="arg2">The second parameter.</param>
    public delegate void RpcSignature<T1, T2>(T1 arg1, T2 arg2);

    /// <summary>
    /// Delegate for representing the signature of a method with 3 parameters.
    /// </summary>
    /// <typeparam name="T1">The type of the first parameter.</typeparam>
    /// <typeparam name="T2">The type of the second parameter.</typeparam>
    /// <typeparam name="T3">The type of the third parameter.</typeparam>
    /// <param name="arg1">The first parameter.</param>
    /// <param name="arg2">The second parameter.</param>
    /// <param name="arg3">The third parameter.</param>
    public delegate void RpcSignature<T1, T2, T3>(T1 arg1, T2 arg2, T3 arg3);

    /// <summary>
    /// Delegate for representing the signature of a method with 4 parameters.
    /// </summary>
    /// <typeparam name="T1">The type of the first parameter.</typeparam>
    /// <typeparam name="T2">The type of the second parameter.</typeparam>
    /// <typeparam name="T3">The type of the third parameter.</typeparam>
    /// <typeparam name="T4">The type of the fourth parameter.</typeparam>
    /// <param name="arg1">The first parameter.</param>
    /// <param name="arg2">The second parameter.</param>
    /// <param name="arg3">The third parameter.</param>
    /// <param name="arg4">The fourth parameter.</param>
    public delegate void RpcSignature<T1, T2, T3, T4>(T1 arg1, T2 arg2, T3 arg3, T4 arg4);

    /// <summary>
    /// Delegate for representing the signature of a method with 5 parameters.
    /// </summary>
    /// <typeparam name="T1">The type of the first parameter.</typeparam>
    /// <typeparam name="T2">The type of the second parameter.</typeparam>
    /// <typeparam name="T3">The type of the third parameter.</typeparam>
    /// <typeparam name="T4">The type of the fourth parameter.</typeparam>
    /// <typeparam name="T5">The type of the fifth parameter.</typeparam>
    /// <param name="arg1">The first parameter.</param>
    /// <param name="arg2">The second parameter.</param>
    /// <param name="arg3">The third parameter.</param>
    /// <param name="arg4">The fourth parameter.</param>
    /// <param name="arg5">The fifth parameter.</param>
    public delegate void RpcSignature<T1, T2, T3, T4, T5>(T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5);

    /// <summary>
    /// Delegate for representing the signature of a method with 6 parameters.
    /// </summary>
    /// <typeparam name="T1">The type of the first parameter.</typeparam>
    /// <typeparam name="T2">The type of the second parameter.</typeparam>
    /// <typeparam name="T3">The type of the third parameter.</typeparam>
    /// <typeparam name="T4">The type of the fourth parameter.</typeparam>
    /// <typeparam name="T5">The type of the fifth parameter.</typeparam>
    /// <typeparam name="T6">The type of the sixth parameter.</typeparam>
    /// <param name="arg1">The first parameter.</param>
    /// <param name="arg2">The second parameter.</param>
    /// <param name="arg3">The third parameter.</param>
    /// <param name="arg4">The fourth parameter.</param>
    /// <param name="arg5">The fifth parameter.</param>
    /// <param name="arg6">The sixth parameter.</param>
    public delegate void RpcSignature<T1, T2, T3, T4, T5, T6>(T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6);

    /// <summary>
    /// Generic delegate type for serialization methods.
    /// </summary>
    /// <typeparam name="T">The type of the object to serialize.</typeparam>
    /// <param name="obj">The object to serialize.</param>
    /// <param name="writer">A writer for writing object data to a stream.</param>
    public delegate void SerializationMethod<T>(T obj, BinaryWriter writer);

    /// <summary>
    /// Non-generic delegate type for serialization methods.
    /// </summary>
    /// <param name="obj">The object to serialize.</param>
    /// <param name="writer">A writer for writing object data to a stream.</param>
    public delegate void SerializationMethod(object obj, BinaryWriter writer);

    /// <summary>
    /// Generic delegate type for deserialization methods.
    /// </summary>
    /// <typeparam name="T">The type of the object to deserialize.</typeparam>
    /// <param name="reader">A reader for reading object data from a stream.</param>
    /// <returns>A new object created with data deserialized from a stream.</returns>
    public delegate T DeserializationMethod<T>(BinaryReader reader);

    /// <summary>
    /// Non-generic delegate type for deserialization methods.
    /// </summary>
    /// <param name="reader">A reader for reading object data from a stream.</param>
    /// <returns>A new object created with data deserialized from a stream.</returns>
    public delegate object DeserializationMethod(BinaryReader reader);

    /// <summary>
    /// Generic delegate for copy constructor factory methods.
    /// </summary>
    /// <typeparam name="T">The type of objects the factory creates.</typeparam>
    /// <param name="other">The object to copy.</param>
    /// <returns>New copies of other.</returns>
    public delegate T FactoryMethod<T>(T other);

    /// <summary>
    /// Manages object construction, serialization, deserialization and equality comparison for
    /// types used in network communications.
    /// </summary>
    internal class Manager
    {
        /// <summary>
        /// Provides access to the current time.
        /// </summary>
        private readonly ITime timeKeeper;

        /// <summary>
        /// Factories for creating property serializers by type.
        /// </summary>
        private readonly Dictionary<Type, GenericCallBackReturn<ISerializer, object, PropertyInfo>> serializerFactories =
            new Dictionary<Type, GenericCallBackReturn<ISerializer, object, PropertyInfo>>();

        /// <summary>
        /// Factories for creating property deserializers by type.
        /// </summary>
        private readonly Dictionary<Type, GenericCallBackReturn<IDeserializer, object, PropertyInfo>> deserializerFactories =
            new Dictionary<Type, GenericCallBackReturn<IDeserializer, object, PropertyInfo>>();

        /// <summary>
        /// Factories for creating smoothing property deserializers by type.
        /// </summary>
        private readonly Dictionary<Type, GenericCallBackReturn<ISmoothingDeserializer, object, PropertyInfo, SmoothingAttribute>> smoothingDeserializerFactories =
            new Dictionary<Type, GenericCallBackReturn<ISmoothingDeserializer, object, PropertyInfo, SmoothingAttribute>>();

        /// <summary>
        /// Delegates for writing objects to a stream, by type.
        /// </summary>
        private readonly Dictionary<Type, SerializationMethod> serializationMethods =
            new Dictionary<Type, SerializationMethod>();

        /// <summary>
        /// Delegates for reading objects from a stream, by type.
        /// </summary>
        private readonly Dictionary<Type, DeserializationMethod> deserializationMethods =
            new Dictionary<Type, DeserializationMethod>();

        /// <summary>
        /// Factories for creating method deserializers, by string representation of method signature.
        /// </summary>
        private Dictionary<string, GenericCallBackReturn<IRPCDeserializer, object, MethodInfo>> rpcDeserializerFactories =
            new Dictionary<string, GenericCallBackReturn<IRPCDeserializer, object, MethodInfo>>();

        /// <summary>
        /// Initializes a new instance of the Manager class.
        /// </summary>
        /// <param name="timeKeeper">Provides access to the current time.</param>
        public Manager(ITime timeKeeper)
        {
            if (timeKeeper == null)
            {
                throw new ArgumentNullException("timeKeeper");
            }

            this.timeKeeper = timeKeeper;

            this.RegisterValueType<bool>((o, w) => w.Write(o), r => r.ReadBoolean());
            this.RegisterValueType<byte>((o, w) => w.Write(o), r => r.ReadByte());
            this.RegisterValueType<char>((o, w) => w.Write(o), r => r.ReadChar());
            this.RegisterValueType<decimal>((o, w) => w.Write(o), r => r.ReadDecimal());
            this.RegisterValueType<double>((o, w) => w.Write(o), r => r.ReadDouble(), new DoubleArithmetic());
            this.RegisterValueType<short>((o, w) => w.Write(o), r => r.ReadInt16());
            this.RegisterValueType<int>((o, w) => w.Write(o), r => r.ReadInt32());
            this.RegisterValueType<long>((o, w) => w.Write(o), r => r.ReadInt64());
            this.RegisterValueType<sbyte>((o, w) => w.Write(o), r => r.ReadSByte());
            this.RegisterValueType<float>((o, w) => w.Write(o), r => r.ReadSingle(), new FloatArithmetic());
            this.RegisterValueType<ushort>((o, w) => w.Write(o), r => r.ReadUInt16());
            this.RegisterValueType<uint>((o, w) => w.Write(o), r => r.ReadUInt32());
            this.RegisterValueType<ulong>((o, w) => w.Write(o), r => r.ReadUInt64());
            this.RegisterImmutableReferenceType<string>((o, w) => w.Write(o), r => r.ReadString());
            this.RegisterValueType<Vector3>(
                (o, w) =>
                    {
                        w.Write(((Vector3)o).X);
                        w.Write(((Vector3)o).Y);
                        w.Write(((Vector3)o).Z);
                    },
                r => new Vector3(r.ReadSingle(), r.ReadSingle(), r.ReadSingle()),
                new Vector3Arithmetic());

            this.RegisterImmutableReferenceType<MemberIdentity>(
                (o, w) => o.Serialize(w),
                (r) => new MemberIdentity(r));

            this.RegisterRPCSignature();
            RPCSignatureRegistrar.RegisterSignatures(this);
        }

        /// <summary>
        /// Register a type so that properties of that type can be replicated.
        /// </summary>
        /// <typeparam name="T">The type to register.</typeparam>
        /// <param name="serializer">A delegate for writing values of that type to a stream.</param>
        /// <param name="deserializer">A reading a value of that type from a stream.</param>
        public void RegisterValueType<T>(SerializationMethod<T> serializer, DeserializationMethod<T> deserializer) where T : struct
        {
            this.serializerFactories.Add(typeof(T), (o, p) => new ValueTypeSerializer<T>(o, p, serializer));
            this.deserializerFactories.Add(typeof(T), (o, p) => new ValueTypeDeserializer<T>(o, p, deserializer));
            this.serializationMethods.Add(typeof(T), (o, w) => serializer((T)o, w));
            this.deserializationMethods.Add(typeof(T), r => deserializer(r));
        }

        /// <summary>
        /// Register a type so that properties of that type can be replicated and smoothed.
        /// </summary>
        /// <typeparam name="T">The type to register.</typeparam>
        /// <param name="serializer">A delegate for writing values of that type to a stream.</param>
        /// <param name="deserializer">A reading a value of that type from a stream.</param>
        /// <param name="calculator">A calculator for performing simple arithmetic on instance of the value type.</param>
        public void RegisterValueType<T>(SerializationMethod<T> serializer, DeserializationMethod<T> deserializer, IArithmetic<T> calculator) where T : struct
        {
            this.RegisterValueType(serializer, deserializer);
            this.smoothingDeserializerFactories.Add(
                typeof(T),
                (o, p, s) =>
                {
                    if (s.UsesRateOfChangeProperty)
                    {
                        return new ReplicatedDerivativeSmoothingDeserializer<T>(o, p, s, deserializer, calculator, this.timeKeeper);
                    }
                    else
                    {
                        return new EstimatedDerivativeSmoothingDeserializer<T>(o, p, s, deserializer, calculator, this.timeKeeper);
                    }
                });
        }
        
        /// <summary>
        /// Register a type so that properties of that type can be replicated.
        /// </summary>
        /// <typeparam name="T">The type to register.</typeparam>
        /// <param name="serializer">A delegate for writing values of that type to a stream.</param>
        /// <param name="deserializer">A reading a value of that type from a stream.</param>
        public void RegisterImmutableReferenceType<T>(SerializationMethod<T> serializer, DeserializationMethod<T> deserializer) where T : class
        {
            this.serializerFactories.Add(typeof(T), (o, p) => new ReferenceTypeSerializer<T>(o, p, serializer));
            this.deserializerFactories.Add(typeof(T), (o, p) => new ReferenceTypeDeserializer<T>(o, p, deserializer));
            this.serializationMethods.Add(typeof(T), (o, w) => serializer((T)o, w));
            this.deserializationMethods.Add(typeof(T), r => deserializer(r));
        }

        /// <summary>
        /// Register a type so that properties of that type can be replicated.
        /// </summary>
        /// <typeparam name="T">The type to register.</typeparam>
        /// <param name="serializer">A delegate for writing values of that type to a stream.</param>
        /// <param name="deserializer">A reading a value of that type from a stream.</param>
        /// <param name="copyFactory">A delegate for creating new instances of the type that are copies of a given instance.</param>
        public void RegisterMutableReferenceType<T>(
            SerializationMethod<T> serializer,
            DeserializationMethod<T> deserializer,
            FactoryMethod<T> copyFactory) where T : class
        {
            this.serializerFactories.Add(typeof(T), (o, p) => new ReferenceTypeSerializer<T>(o, p, serializer, copyFactory));
            this.deserializerFactories.Add(typeof(T), (o, p) => new ReferenceTypeDeserializer<T>(o, p, deserializer));
            this.serializationMethods.Add(typeof(T), (o, w) => serializer((T)o, w));
            this.deserializationMethods.Add(typeof(T), r => deserializer(r));
        }

        /// <summary>
        /// Register a signature for a method with no parameters available via RPC.
        /// </summary>
        public void RegisterRPCSignature()
        {
            var signatureString = string.Empty;
            this.rpcDeserializerFactories.Add(
                signatureString,
                (o, m) => new RPCDeserializer(o, m));
        }

        /// <summary>
        /// Register a signature for an method with one parameter available via RPC.
        /// </summary>
        /// <typeparam name="T1">The type of the first parameter.</typeparam>
        public void RegisterRPCSignature<T1>()
        {
            var dm1 = this.GetDeserializationMethod(typeof(T1));

            var signatureString = typeof(T1).ToString();
            this.rpcDeserializerFactories.Add(
                signatureString,
                (o, m) => new RPCDeserializer<T1>(
                    o,
                    m,
                    r => (T1)dm1(r)));
        }

        /// <summary>
        /// Register a signature for an method with 2 parameters available via RPC.
        /// </summary>
        /// <typeparam name="T1">The type of the first parameter.</typeparam>
        /// <typeparam name="T2">The type of the second parameter.</typeparam>
        public void RegisterRPCSignature<T1, T2>()
        {
            var dm1 = this.GetDeserializationMethod(typeof(T1));
            var dm2 = this.GetDeserializationMethod(typeof(T2));

            var signatureString = typeof(T1).ToString() +
                typeof(T2).ToString();
            this.rpcDeserializerFactories.Add(
                signatureString,
                (o, m) => new RPCDeserializer<T1, T2>(
                    o,
                    m,
                    r => (T1)dm1(r),
                    r => (T2)dm2(r)));
        }

        /// <summary>
        /// Register a signature for an method with 3 parameters available via RPC.
        /// </summary>
        /// <typeparam name="T1">The type of the first parameter.</typeparam>
        /// <typeparam name="T2">The type of the second parameter.</typeparam>
        /// <typeparam name="T3">The type of the third parameter.</typeparam>
        public void RegisterRPCSignature<T1, T2, T3>()
        {
            var dm1 = this.GetDeserializationMethod(typeof(T1));
            var dm2 = this.GetDeserializationMethod(typeof(T2));
            var dm3 = this.GetDeserializationMethod(typeof(T3));

            var signatureString = typeof(T1).ToString() +
                typeof(T2).ToString() +
                typeof(T3).ToString();
            this.rpcDeserializerFactories.Add(
                signatureString,
                (o, m) => new RPCDeserializer<T1, T2, T3>(
                    o,
                    m,
                    r => (T1)dm1(r),
                    r => (T2)dm2(r),
                    r => (T3)dm3(r)));
        }

        /// <summary>
        /// Register a signature for an method with 4 parameters available via RPC.
        /// </summary>
        /// <typeparam name="T1">The type of the first parameter.</typeparam>
        /// <typeparam name="T2">The type of the second parameter.</typeparam>
        /// <typeparam name="T3">The type of the third parameter.</typeparam>
        /// <typeparam name="T4">The type of the fourth parameter.</typeparam>
        public void RegisterRPCSignature<T1, T2, T3, T4>()
        {
            var dm1 = this.GetDeserializationMethod(typeof(T1));
            var dm2 = this.GetDeserializationMethod(typeof(T2));
            var dm3 = this.GetDeserializationMethod(typeof(T3));
            var dm4 = this.GetDeserializationMethod(typeof(T4));

            var signatureString = typeof(T1).ToString() +
                typeof(T2).ToString() +
                typeof(T3).ToString() +
                typeof(T4).ToString();
            this.rpcDeserializerFactories.Add(
                signatureString,
                (o, m) => new RPCDeserializer<T1, T2, T3, T4>(
                    o,
                    m,
                    r => (T1)dm1(r),
                    r => (T2)dm2(r),
                    r => (T3)dm3(r),
                    r => (T4)dm4(r)));
        }

        /// <summary>
        /// Register a signature for an method with 5 parameters available via RPC.
        /// </summary>
        /// <typeparam name="T1">The type of the first parameter.</typeparam>
        /// <typeparam name="T2">The type of the second parameter.</typeparam>
        /// <typeparam name="T3">The type of the third parameter.</typeparam>
        /// <typeparam name="T4">The type of the fourth parameter.</typeparam>
        /// <typeparam name="T5">The type of the fifth parameter.</typeparam>
        public void RegisterRPCSignature<T1, T2, T3, T4, T5>()
        {
            var dm1 = this.GetDeserializationMethod(typeof(T1));
            var dm2 = this.GetDeserializationMethod(typeof(T2));
            var dm3 = this.GetDeserializationMethod(typeof(T3));
            var dm4 = this.GetDeserializationMethod(typeof(T4));
            var dm5 = this.GetDeserializationMethod(typeof(T5));

            var signatureString = typeof(T1).ToString() +
                typeof(T2).ToString() +
                typeof(T3).ToString() +
                typeof(T4).ToString() +
                typeof(T5).ToString();
            this.rpcDeserializerFactories.Add(
                signatureString,
                (o, m) => new RPCDeserializer<T1, T2, T3, T4, T5>(
                    o,
                    m,
                    r => (T1)dm1(r),
                    r => (T2)dm2(r),
                    r => (T3)dm3(r),
                    r => (T4)dm4(r),
                    r => (T5)dm5(r)));
        }

        /// <summary>
        /// Register a signature for an method available via RPC.
        /// </summary>
        /// <typeparam name="T1">The type of the first parameter.</typeparam>
        /// <typeparam name="T2">The type of the second parameter.</typeparam>
        /// <typeparam name="T3">The type of the third parameter.</typeparam>
        /// <typeparam name="T4">The type of the fourth parameter.</typeparam>
        /// <typeparam name="T5">The type of the fifth parameter.</typeparam>
        /// <typeparam name="T6">The type of the sixth parameter.</typeparam>
        public void RegisterRPCSignature<T1, T2, T3, T4, T5, T6>()
        {
            var dm1 = this.GetDeserializationMethod(typeof(T1));
            var dm2 = this.GetDeserializationMethod(typeof(T2));
            var dm3 = this.GetDeserializationMethod(typeof(T3));
            var dm4 = this.GetDeserializationMethod(typeof(T4));
            var dm5 = this.GetDeserializationMethod(typeof(T5));
            var dm6 = this.GetDeserializationMethod(typeof(T6));

            var signatureString = typeof(T1).ToString() +
                typeof(T2).ToString() +
                typeof(T3).ToString() +
                typeof(T4).ToString() +
                typeof(T5).ToString() +
                typeof(T6).ToString();
            this.rpcDeserializerFactories.Add(
                signatureString,
                (o, m) => new RPCDeserializer<T1, T2, T3, T4, T5, T6>(
                    o,
                    m,
                    r => (T1)dm1(r),
                    r => (T2)dm2(r),
                    r => (T3)dm3(r),
                    r => (T4)dm4(r),
                    r => (T5)dm5(r),
                    r => (T6)dm6(r)));
        }

        /// <summary>
        /// Create a property serializer for a property of an object.
        /// </summary>
        /// <param name="target">The object whose property is to be serialized.</param>
        /// <param name="property">The property to be serialized.</param>
        /// <returns>A serializer for the object's property.</returns>
        public ISerializer CreatePropertySerializer(object target, PropertyInfo property)
        {
            GenericCallBackReturn<ISerializer, object, PropertyInfo> serializerFactory;
            if (this.serializerFactories.TryGetValue(property.PropertyType, out serializerFactory))
            {
                return serializerFactory.Invoke(target, property);
            }

            throw new TypeReplicationException(property.PropertyType);
        }

        /// <summary>
        /// Create a property serializer for a dead-reckoned property of an object.
        /// </summary>
        /// <param name="target">The object whose property is to be serialized.</param>
        /// <param name="property">The property holding the value to be serialized.</param>
        /// <param name="smoothingAttribute">The attibute specifying the derivative property.</param>
        /// <returns>A serializer for the object's property and derivative.</returns>
        public ISerializer CreateDeadReckonedPropertySerializer(
            object target, PropertyInfo property, SmoothingAttribute smoothingAttribute)
        {
            ISerializer valueSerializer;
            GenericCallBackReturn<ISerializer, object, PropertyInfo> serializerFactory;
            if (this.serializerFactories.TryGetValue(property.PropertyType, out serializerFactory))
            {
                valueSerializer = serializerFactory.Invoke(target, property);
                var derivativeProperty = target.GetType().GetProperty(smoothingAttribute.RateOfChangeProperty);
                if (this.serializerFactories.TryGetValue(derivativeProperty.PropertyType, out serializerFactory))
                {
                    ISerializer derivativeSerializer = serializerFactory.Invoke(target, derivativeProperty);
                    return new DeadReckoningSerializer(valueSerializer, derivativeSerializer);
                }
            }

            throw new TypeReplicationException(property.PropertyType);
        }

        /// <summary>
        /// Create a property deserializer for a property of an object.
        /// </summary>
        /// <param name="target">The object whose property is to be deserialized.</param>
        /// <param name="property">The property to be deserialized.</param>
        /// <returns>A deserializer for the object's property.</returns>
        public IDeserializer CreatePropertyDeserializer(object target, PropertyInfo property)
        {
            GenericCallBackReturn<IDeserializer, object, PropertyInfo> deserializerFactory;
            if (this.deserializerFactories.TryGetValue(property.PropertyType, out deserializerFactory))
            {
                return deserializerFactory.Invoke(target, property);
            }

            throw new TypeReplicationException(property.PropertyType);
        }

        /// <summary>
        /// Create a property deserializer for a property of an object.
        /// </summary>
        /// <param name="target">The object whose property is to be deserialized.</param>
        /// <param name="property">The property to be deserialized.</param>
        /// <param name="smoothingAttribute">The attribute specifying how to smooth the value.</param>
        /// <returns>A deserializer for the object's property.</returns>
        public ISmoothingDeserializer CreateSmoothingPropertyDeserializer(
            object target, PropertyInfo property, SmoothingAttribute smoothingAttribute)
        {
            GenericCallBackReturn<ISmoothingDeserializer, object, PropertyInfo, SmoothingAttribute> factory;
            if (this.smoothingDeserializerFactories.TryGetValue(property.PropertyType, out factory))
            {
                return factory.Invoke(target, property, smoothingAttribute);
            }

            // This error message should be changed if registration of smoothable types is exposed to application.
            var stringBuilder = new StringBuilder("Smoothing is only supported on properties of the types: ");
            foreach (var type in this.smoothingDeserializerFactories.Keys)
            {
                stringBuilder.Append(type);
                stringBuilder.Append(", ");
            }

            stringBuilder.Remove(stringBuilder.Length - 2, 2);
            stringBuilder.Append(".");
            throw new TypeReplicationException(stringBuilder.ToString());
        }

        /// <summary>
        /// Create an RPC deserializer for a method of a given object.
        /// </summary>
        /// <param name="target">The instance whose method should be invoked by the RPC.</param>
        /// <param name="method">The method to invoke.</param>
        /// <returns>An RPC deserializer</returns>
        public IRPCDeserializer CreateMethodDeserializer(object target, MethodInfo method)
        {
            var signature = string.Empty;
            foreach (var parameter in method.GetParameters())
            {
                signature += parameter.ParameterType.ToString();
            }

            GenericCallBackReturn<IRPCDeserializer, object, MethodInfo> methodDeserializerFactory;
            if (this.rpcDeserializerFactories.TryGetValue(signature, out methodDeserializerFactory))
            {
                return methodDeserializerFactory.Invoke(target, method);
            }

            var messageBuilder = new StringBuilder();
            messageBuilder.AppendFormat(
                "To use method {0}.{1} as an RPC, you need to register its signature by calling: ",
                method.ReflectedType,
                method.Name);
            messageBuilder.Append("INetworFacade.RPC.RegisterRPCSignature<");
            bool firstParam = true;
            foreach (var param in method.GetParameters())
            {
                if (!firstParam)
                {
                    messageBuilder.Append(", ");
                    firstParam = false;
                }

                messageBuilder.Append(param.ParameterType.Name);
            }

            messageBuilder.Append(">();");
            throw new RPCException(messageBuilder.ToString());
        }

        /// <summary>
        /// Serialize a parameter of a given type to a stream using the serialization method registered
        /// for that type.
        /// </summary>
        /// <typeparam name="T">The type of the parameter to serialize.</typeparam>
        /// <param name="parameter">The parameter to serialize.</param>
        /// <param name="writer">A binary writer for writing to the stream.</param>
        public void SerializeParameter<T>(T parameter, BinaryWriter writer)
        {
            SerializationMethod serializer;
            if (!this.serializationMethods.TryGetValue(typeof(T), out serializer))
            {
                throw new RPCException(
                    "Attempt to send RPC with unregistered parameter type: " + typeof(T));
            }

            serializer.Invoke(parameter, writer);
        }

        /// <summary>
        /// Gets the deserialization method for the given type, throwing an
        /// appriopriate exception if it hasn't been registered.
        /// </summary>
        /// <param name="type">The given type</param>
        /// <returns>The deserialization method</returns>
        private DeserializationMethod GetDeserializationMethod(Type type)
        {
            DeserializationMethod method;
            if (!this.deserializationMethods.TryGetValue(type, out method))
            {
                throw new RPCException("Type " + type + " has not been registered with the TypeRegistry");
            }

            return method;
        }
    }
}