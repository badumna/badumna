﻿// ------------------------------------------------------------------------------
//  <copyright file="EstimatedDerivativeSmoothingDeserializer.cs" company="National ICT Australia Limited">
//      Copyright (c) 2012-2012 National ICT Australia Limited. All rights reserved.
//  </copyright>
// ------------------------------------------------------------------------------

namespace Badumna.Autoreplication.Serialization
{
    using System;
    using System.IO;
    using System.Reflection;

    using Badumna.Core;
    using Badumna.Utilities;

    /// <summary>
    /// For setting a value type property on an object with a smoothed value based on values read from a stream.
    /// </summary>
    /// <typeparam name="T">The type of the property being updated (must be a value type).</typeparam>
    internal class EstimatedDerivativeSmoothingDeserializer<T> : ISmoothingDeserializer
    {
        /// <summary>
        /// A delegate for setting the property.
        /// </summary>
        private readonly GenericCallBack<T> setter;

        /// <summary>
        /// A delegate for reading a value from a stream.
        /// </summary>
        private readonly DeserializationMethod<T> deserializer;

        /// <summary>
        /// Attribute detailing how to smooth the property's value.
        /// </summary>
        private readonly SmoothingAttribute smoothingAttribute;

        /// <summary>
        /// For performing arithmetic operations on values of type T.
        /// </summary>
        private readonly IArithmetic<T> calculator;

        /// <summary>
        /// Provides the current time.
        /// </summary>
        private readonly ITime timeKeeper;

        /// <summary>
        /// A smoother for smoothing values over time.
        /// </summary>
        private EstimatedDerivativeSmoother<T> smoother;

        /// <summary>
        /// Initializes a new instance of the EstimatedDerivativeSmoothingDeserializer class.
        /// </summary>
        /// <param name="target">The object whose property is to be set.</param>
        /// <param name="property">The type property to be set.</param>
        /// <param name="smoothingAttribute">Attribute detailing how to smooth the property's value.</param>
        /// <param name="deserializer">A delegate for reading a value for the property from a stream.</param>
        /// <param name="calculator">For performing arithmetic operations on values of type T.</param>
        /// <param name="timeKeeper">Provides the current time.</param>
        public EstimatedDerivativeSmoothingDeserializer(
            object target,
            PropertyInfo property,
            SmoothingAttribute smoothingAttribute,
            DeserializationMethod<T> deserializer,
            IArithmetic<T> calculator,
            ITime timeKeeper)
        {
            this.setter = (GenericCallBack<T>)Delegate.CreateDelegate(typeof(GenericCallBack<T>), target, property.GetSetMethod(true));
            this.deserializer = deserializer;
            this.smoothingAttribute = smoothingAttribute;
            this.timeKeeper = timeKeeper;
            this.calculator = calculator;
        }

        /// <summary>
        /// Set the property with a value read from a stream.
        /// </summary>
        /// <param name="reader">A binary reader for reading from the stream.</param>
        public void Deserialize(BinaryReader reader)
        {
            if (this.smoother == null)
            {
                this.smoother = new EstimatedDerivativeSmoother<T>(
                    this.deserializer.Invoke(reader),
                    this.smoothingAttribute.Interpolation,
                    this.smoothingAttribute.Extrapolation,
                    this.timeKeeper,
                    this.calculator);
                this.setter.Invoke(this.smoother.GetCurrentValue(this.timeKeeper.Now));
                return;
            }

            this.smoother.Update(this.deserializer.Invoke(reader));
        }

        /// <summary>
        /// Tick regularly to updated property with smoothed value.
        /// </summary>
        /// <param name="currentTime">The current time.</param>
        public void Tick(TimeSpan currentTime)
        {
            if (this.smoother.Changed)
            {
                this.setter.Invoke(this.smoother.GetCurrentValue(currentTime));
            }
        }
    }
}