﻿// ------------------------------------------------------------------------------
//  <copyright file="RPCManager.cs" company="National ICT Australia Limited">
//      Copyright (c) 2012-2012 National ICT Australia Limited. All rights reserved.
//  </copyright>
// ------------------------------------------------------------------------------

namespace Badumna
{
    using Badumna.Autoreplication;
    using Badumna.Autoreplication.Serialization;
    using Serialization = Badumna.Autoreplication.Serialization;
    using SpatialAutoreplicationManager = Badumna.Autoreplication.SpatialAutoreplicationManager;

    /// <summary>
    /// Manages remotely calling methods on original and replica entities.
    /// </summary>
    /// <remarks>Any remotable method on a spatial entity must have its signature registered to
    /// enable Badumna to call it via RPC.
    /// Any signature of up to six parameters of the following types are automatically
    /// registered:
    /// bool, float, int, string, Badumna.Vector3.
    /// Badumna supports RPCs for methods using up to six parameters of any types providing the
    /// signature has been registered by calling RPCManager.RegisterRPCSignature.
    /// </remarks>
    public class RPCManager
    {
        /// <summary>
        /// Serialization manager.
        /// </summary>
        private readonly Serialization.Manager serialization;

        /// <summary>
        /// Initializes a new instance of the RPCManager class.
        /// </summary>
        /// <param name="serialization">The serialization manager.</param>
        internal RPCManager(Serialization.Manager serialization)
        {
            this.serialization = serialization;
        }

        /// <summary>
        /// Register a signature for an method available via RPC.
        /// </summary>
        /// <typeparam name="T1">The type of the first parameter.</typeparam>
        public void RegisterRPCSignature<T1>()
        {
            this.serialization.RegisterRPCSignature<T1>();
        }

        /// <summary>
        /// Register a signature for an method with 2 parameters available via RPC.
        /// </summary>
        /// <typeparam name="T1">The type of the first parameter.</typeparam>
        /// <typeparam name="T2">The type of the second parameter.</typeparam>
        public void RegisterRPCSignature<T1, T2>()
        {
            this.serialization.RegisterRPCSignature<T1, T2>();
        }

        /// <summary>
        /// Register a signature for an method with 3 parameters available via RPC.
        /// </summary>
        /// <typeparam name="T1">The type of the first parameter.</typeparam>
        /// <typeparam name="T2">The type of the second parameter.</typeparam>
        /// <typeparam name="T3">The type of the third parameter.</typeparam>
        public void RegisterRPCSignature<T1, T2, T3>()
        {
            this.serialization.RegisterRPCSignature<T1, T2, T3>();
        }

        /// <summary>
        /// Register a signature for an method with 4 parameters available via RPC.
        /// </summary>
        /// <typeparam name="T1">The type of the first parameter.</typeparam>
        /// <typeparam name="T2">The type of the second parameter.</typeparam>
        /// <typeparam name="T3">The type of the third parameter.</typeparam>
        /// <typeparam name="T4">The type of the fourth parameter.</typeparam>
        public void RegisterRPCSignature<T1, T2, T3, T4>()
        {
            this.serialization.RegisterRPCSignature<T1, T2, T3, T4>();
        }

        /// <summary>
        /// Register a signature for an method with 5 parameters available via RPC.
        /// </summary>
        /// <typeparam name="T1">The type of the first parameter.</typeparam>
        /// <typeparam name="T2">The type of the second parameter.</typeparam>
        /// <typeparam name="T3">The type of the third parameter.</typeparam>
        /// <typeparam name="T4">The type of the fourth parameter.</typeparam>
        /// <typeparam name="T5">The type of the fifth parameter.</typeparam>
        public void RegisterRPCSignature<T1, T2, T3, T4, T5>()
        {
            this.serialization.RegisterRPCSignature<T1, T2, T3, T4, T5>();
        }

        /// <summary>
        /// Register a signature for an method available via RPC.
        /// </summary>
        /// <typeparam name="T1">The type of the first parameter.</typeparam>
        /// <typeparam name="T2">The type of the second parameter.</typeparam>
        /// <typeparam name="T3">The type of the third parameter.</typeparam>
        /// <typeparam name="T4">The type of the fourth parameter.</typeparam>
        /// <typeparam name="T5">The type of the fifth parameter.</typeparam>
        /// <typeparam name="T6">The type of the sixth parameter.</typeparam>
        public void RegisterRPCSignature<T1, T2, T3, T4, T5, T6>()
        {
            this.serialization.RegisterRPCSignature<T1, T2, T3, T4, T5, T6>();
        }
    }
}