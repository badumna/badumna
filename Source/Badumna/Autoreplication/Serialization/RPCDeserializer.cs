﻿// -----------------------------------------------------------------------
// <copyright file="RPCDeserializer.cs" company="National ICT Australia Limited">
//     Copyright (c) 2012-2012 National ICT Australia Limited. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace Badumna.Autoreplication.Serialization
{
    using System;
    using System.Diagnostics.CodeAnalysis;
    using System.IO;
    using System.Reflection;
    using Badumna.Utilities;

    /// <summary>
    /// Interface for invoking a method with parameters serialized in a stream.
    /// </summary>
    internal interface IRPCDeserializer
    {
        /// <summary>
        /// Invoke the method with parameter serialized in a stream.
        /// </summary>
        /// <param name="reader">The reader providing access to the stream.</param>
        void Invoke(BinaryReader reader);
    }

    /// <summary>
    /// For invoking a method with no parameters serialized in a stream.
    /// </summary>
    internal class RPCDeserializer : IRPCDeserializer
    {
        /// <summary>
        /// A delegate for invoking the method.
        /// </summary>
        private readonly GenericCallBack action;

        /// <summary>
        /// Initializes a new instance of the RPCDeserializer class.
        /// </summary>
        /// <param name="target">The target for the method invocation.</param>
        /// <param name="method">The method to be invoked.</param>
        public RPCDeserializer(
            object target,
            MethodInfo method)
        {
            this.action = (GenericCallBack)Delegate.CreateDelegate(
                typeof(GenericCallBack),
                target,
                method);
        }

        /// <summary>
        /// Invoke the method (ignoring stream as there are no parameters).
        /// </summary>
        /// <param name="reader">A binary reader providing access to the stream.</param>
        public void Invoke(BinaryReader reader)
        {
            this.action.Invoke();
        }
    }

    /// <summary>
    /// For invoking a method with 1 parameter serialized in a stream.
    /// </summary>
    /// <typeparam name="A">The type of the parameter.</typeparam>
    [SuppressMessage("Microsoft.StyleCop.CSharp.MaintainabilityRules", "SA1402:FileMayOnlyContainASingleClass", Justification = "Generics variants should happily cohabit here.")]
    internal class RPCDeserializer<A> : IRPCDeserializer
    {
        /// <summary>
        /// A delegate for invoking the method.
        /// </summary>
        private readonly GenericCallBack<A> action;

        /// <summary>
        /// A delegate for deserializing the parameter from a stream.
        /// </summary>
        private readonly GenericCallBackReturn<A, BinaryReader> deserializerA;

        /// <summary>
        /// Initializes a new instance of the RPCDeserializer class.
        /// </summary>
        /// <param name="target">The target for the method invocation.</param>
        /// <param name="method">The method to be invoked.</param>
        /// <param name="deserializerA">A delegate for deserialising the first paramter from a stream.</param>
        public RPCDeserializer(
            object target,
            MethodInfo method,
            GenericCallBackReturn<A, BinaryReader> deserializerA)
        {
            this.action = (GenericCallBack<A>)Delegate.CreateDelegate(
                typeof(GenericCallBack<A>),
                target,
                method);

            this.deserializerA = deserializerA;
        }

        /// <summary>
        /// Invoke the method using parameters serialized in a stream.
        /// </summary>
        /// <param name="reader">A binary reader providing access to the stream.</param>
        public void Invoke(BinaryReader reader)
        {
            this.action.Invoke(this.deserializerA(reader));
        }
    }

    /// <summary>
    /// For invoking a method with 2 parameters serialized in a stream.
    /// </summary>
    /// <typeparam name="A">The type of the first parameter.</typeparam>
    /// <typeparam name="B">The type of the second parameter.</typeparam>
    internal class RPCDeserializer<A, B> : IRPCDeserializer
    {
        /// <summary>
        /// A delegate for invoking the method.
        /// </summary>
        private readonly GenericCallBack<A, B> action;

        /// <summary>
        /// A delegate for deserializing the first parameter from a stream.
        /// </summary>
        private readonly GenericCallBackReturn<A, BinaryReader> deserializerA;

        /// <summary>
        /// A delegate for deserializing the second parameter from a stream.
        /// </summary>
        private readonly GenericCallBackReturn<B, BinaryReader> deserializerB;

        /// <summary>
        /// Initializes a new instance of the RPCDeserializer class.
        /// </summary>
        /// <param name="target">The target for the method invocation.</param>
        /// <param name="method">The method to be invoked.</param>
        /// <param name="deserializerA">A delegate for deserialising the first paramter from a stream.</param>
        /// <param name="deserializerB">A delegate for deserialising the second paramter from a stream.</param>
        public RPCDeserializer(
            object target,
            MethodInfo method,
            GenericCallBackReturn<A, BinaryReader> deserializerA,
            GenericCallBackReturn<B, BinaryReader> deserializerB)
        {
            this.action = (GenericCallBack<A, B>)Delegate.CreateDelegate(
                typeof(GenericCallBack<A, B>),
                target,
                method);

            this.deserializerA = deserializerA;
            this.deserializerB = deserializerB;
        }

        /// <summary>
        /// Invoke the method using parameters serialized in a stream.
        /// </summary>
        /// <param name="reader">A binary reader providing access to the stream.</param>
        public void Invoke(BinaryReader reader)
        {
            this.action.Invoke(
                this.deserializerA(reader),
                this.deserializerB(reader));
        }
    }

    /// <summary>
    /// For invoking a method with 3 parameters serialized in a stream.
    /// </summary>
    /// <typeparam name="A">The type of the first parameter.</typeparam>
    /// <typeparam name="B">The type of the second parameter.</typeparam>
    /// <typeparam name="C">The type of the third parameter.</typeparam>
    internal class RPCDeserializer<A, B, C> : IRPCDeserializer
    {
        /// <summary>
        /// A delegate for invoking the method.
        /// </summary>
        private readonly GenericCallBack<A, B, C> action;

        /// <summary>
        /// A delegate for deserializing the first parameter from a stream.
        /// </summary>
        private readonly GenericCallBackReturn<A, BinaryReader> deserializerA;

        /// <summary>
        /// A delegate for deserializing the second parameter from a stream.
        /// </summary>
        private readonly GenericCallBackReturn<B, BinaryReader> deserializerB;

        /// <summary>
        /// A delegate for deserializing the third parameter from a stream.
        /// </summary>
        private readonly GenericCallBackReturn<C, BinaryReader> deserializerC;

        /// <summary>
        /// Initializes a new instance of the RPCDeserializer class.
        /// </summary>
        /// <param name="target">The target for the method invocation.</param>
        /// <param name="method">The method to be invoked.</param>
        /// <param name="deserializerA">A delegate for deserialising the first paramter from a stream.</param>
        /// <param name="deserializerB">A delegate for deserialising the second paramter from a stream.</param>
        /// <param name="deserializerC">A delegate for deserialising the third paramter from a stream.</param>
        public RPCDeserializer(
            object target,
            MethodInfo method,
            GenericCallBackReturn<A, BinaryReader> deserializerA,
            GenericCallBackReturn<B, BinaryReader> deserializerB,
            GenericCallBackReturn<C, BinaryReader> deserializerC)
        {
            this.action = (GenericCallBack<A, B, C>)Delegate.CreateDelegate(
                typeof(GenericCallBack<A, B, C>),
                target,
                method);

            this.deserializerA = deserializerA;
            this.deserializerB = deserializerB;
            this.deserializerC = deserializerC;
        }

        /// <summary>
        /// Invoke the method using parameters serialized in a stream.
        /// </summary>
        /// <param name="reader">A binary reader providing access to the stream.</param>
        public void Invoke(BinaryReader reader)
        {
            this.action.Invoke(
                this.deserializerA(reader),
                this.deserializerB(reader),
                this.deserializerC(reader));
        }
    }

    /// <summary>
    /// For invoking a method with 4 parameters serialized in a stream.
    /// </summary>
    /// <typeparam name="A">The type of the first parameter.</typeparam>
    /// <typeparam name="B">The type of the second parameter.</typeparam>
    /// <typeparam name="C">The type of the third parameter.</typeparam>
    /// <typeparam name="D">The type of the fourth parameter.</typeparam>
    internal class RPCDeserializer<A, B, C, D> : IRPCDeserializer
    {
        /// <summary>
        /// A delegate for invoking the method.
        /// </summary>
        private readonly GenericCallBack<A, B, C, D> action;

        /// <summary>
        /// A delegate for deserializing the first parameter from a stream.
        /// </summary>
        private readonly GenericCallBackReturn<A, BinaryReader> deserializerA;

        /// <summary>
        /// A delegate for deserializing the second parameter from a stream.
        /// </summary>
        private readonly GenericCallBackReturn<B, BinaryReader> deserializerB;

        /// <summary>
        /// A delegate for deserializing the third parameter from a stream.
        /// </summary>
        private readonly GenericCallBackReturn<C, BinaryReader> deserializerC;

        /// <summary>
        /// A delegate for deserializing the fourth parameter from a stream.
        /// </summary>
        private readonly GenericCallBackReturn<D, BinaryReader> deserializerD;

        /// <summary>
        /// Initializes a new instance of the RPCDeserializer class.
        /// </summary>
        /// <param name="target">The target for the method invocation.</param>
        /// <param name="method">The method to be invoked.</param>
        /// <param name="deserializerA">A delegate for deserialising the first paramter from a stream.</param>
        /// <param name="deserializerB">A delegate for deserialising the second paramter from a stream.</param>
        /// <param name="deserializerC">A delegate for deserialising the third paramter from a stream.</param>
        /// <param name="deserializerD">A delegate for deserialising the fourth paramter from a stream.</param>
        public RPCDeserializer(
            object target,
            MethodInfo method,
            GenericCallBackReturn<A, BinaryReader> deserializerA,
            GenericCallBackReturn<B, BinaryReader> deserializerB,
            GenericCallBackReturn<C, BinaryReader> deserializerC,
            GenericCallBackReturn<D, BinaryReader> deserializerD)
        {
            this.action = (GenericCallBack<A, B, C, D>)Delegate.CreateDelegate(
                typeof(GenericCallBack<A, B, C, D>),
                target,
                method);

            this.deserializerA = deserializerA;
            this.deserializerB = deserializerB;
            this.deserializerC = deserializerC;
            this.deserializerD = deserializerD;
        }

        /// <summary>
        /// Invoke the method using parameters serialized in a stream.
        /// </summary>
        /// <param name="reader">A binary reader providing access to the stream.</param>
        public void Invoke(BinaryReader reader)
        {
            this.action.Invoke(
                this.deserializerA(reader),
                this.deserializerB(reader),
                this.deserializerC(reader),
                this.deserializerD(reader));
        }
    }

    /// <summary>
    /// For invoking a method with 5 parameters serialized in a stream.
    /// </summary>
    /// <typeparam name="A">The type of the first parameter.</typeparam>
    /// <typeparam name="B">The type of the second parameter.</typeparam>
    /// <typeparam name="C">The type of the third parameter.</typeparam>
    /// <typeparam name="D">The type of the fourth parameter.</typeparam>
    /// <typeparam name="E">The type of the fifth parameter.</typeparam>
    internal class RPCDeserializer<A, B, C, D, E> : IRPCDeserializer
    {
        /// <summary>
        /// A delegate for invoking the method.
        /// </summary>
        private readonly GenericCallBack<A, B, C, D, E> action;

        /// <summary>
        /// A delegate for deserializing the first parameter from a stream.
        /// </summary>
        private readonly GenericCallBackReturn<A, BinaryReader> deserializerA;

        /// <summary>
        /// A delegate for deserializing the second parameter from a stream.
        /// </summary>
        private readonly GenericCallBackReturn<B, BinaryReader> deserializerB;

        /// <summary>
        /// A delegate for deserializing the third parameter from a stream.
        /// </summary>
        private readonly GenericCallBackReturn<C, BinaryReader> deserializerC;

        /// <summary>
        /// A delegate for deserializing the fourth parameter from a stream.
        /// </summary>
        private readonly GenericCallBackReturn<D, BinaryReader> deserializerD;

        /// <summary>
        /// A delegate for deserializing the fifth parameter from a stream.
        /// </summary>
        private readonly GenericCallBackReturn<E, BinaryReader> deserializerE;

        /// <summary>
        /// Initializes a new instance of the RPCDeserializer class.
        /// </summary>
        /// <param name="target">The target for the method invocation.</param>
        /// <param name="method">The method to be invoked.</param>
        /// <param name="deserializerA">A delegate for deserialising the first paramter from a stream.</param>
        /// <param name="deserializerB">A delegate for deserialising the second paramter from a stream.</param>
        /// <param name="deserializerC">A delegate for deserialising the third paramter from a stream.</param>
        /// <param name="deserializerD">A delegate for deserialising the fourth paramter from a stream.</param>
        /// <param name="deserializerE">A delegate for deserialising the fifth paramter from a stream.</param>
        public RPCDeserializer(
            object target,
            MethodInfo method,
            GenericCallBackReturn<A, BinaryReader> deserializerA,
            GenericCallBackReturn<B, BinaryReader> deserializerB,
            GenericCallBackReturn<C, BinaryReader> deserializerC,
            GenericCallBackReturn<D, BinaryReader> deserializerD,
            GenericCallBackReturn<E, BinaryReader> deserializerE)
        {
            this.action = (GenericCallBack<A, B, C, D, E>)Delegate.CreateDelegate(
                typeof(GenericCallBack<A, B, C, D, E>),
                target,
                method);

            this.deserializerA = deserializerA;
            this.deserializerB = deserializerB;
            this.deserializerC = deserializerC;
            this.deserializerD = deserializerD;
            this.deserializerE = deserializerE;
        }

        /// <summary>
        /// Invoke the method using parameters serialized in a stream.
        /// </summary>
        /// <param name="reader">A binary reader providing access to the stream.</param>
        public void Invoke(BinaryReader reader)
        {
            this.action.Invoke(
                this.deserializerA(reader),
                this.deserializerB(reader),
                this.deserializerC(reader),
                this.deserializerD(reader),
                this.deserializerE(reader));
        }
    }

    /// <summary>
    /// For invoking a method with 6 parameters serialized in a stream.
    /// </summary>
    /// <typeparam name="A">The type of the first parameter.</typeparam>
    /// <typeparam name="B">The type of the second parameter.</typeparam>
    /// <typeparam name="C">The type of the third parameter.</typeparam>
    /// <typeparam name="D">The type of the fourth parameter.</typeparam>
    /// <typeparam name="E">The type of the fifth parameter.</typeparam>
    /// <typeparam name="F">The type of the sixth parameter.</typeparam>
    internal class RPCDeserializer<A, B, C, D, E, F> : IRPCDeserializer
    {
        /// <summary>
        /// A delegate for invoking the method.
        /// </summary>
        private readonly GenericCallBack<A, B, C, D, E, F> action;

        /// <summary>
        /// A delegate for deserializing the first parameter from a stream.
        /// </summary>
        private readonly GenericCallBackReturn<A, BinaryReader> deserializerA;

        /// <summary>
        /// A delegate for deserializing the second parameter from a stream.
        /// </summary>
        private readonly GenericCallBackReturn<B, BinaryReader> deserializerB;

        /// <summary>
        /// A delegate for deserializing the third parameter from a stream.
        /// </summary>
        private readonly GenericCallBackReturn<C, BinaryReader> deserializerC;

        /// <summary>
        /// A delegate for deserializing the fourth parameter from a stream.
        /// </summary>
        private readonly GenericCallBackReturn<D, BinaryReader> deserializerD;

        /// <summary>
        /// A delegate for deserializing the fifth parameter from a stream.
        /// </summary>
        private readonly GenericCallBackReturn<E, BinaryReader> deserializerE;

        /// <summary>
        /// A delegate for deserializing the sixth parameter from a stream.
        /// </summary>
        private readonly GenericCallBackReturn<F, BinaryReader> deserializerF;

        /// <summary>
        /// Initializes a new instance of the RPCDeserializer class.
        /// </summary>
        /// <param name="target">The target for the method invocation.</param>
        /// <param name="method">The method to be invoked.</param>
        /// <param name="deserializerA">A delegate for deserialising the first paramter from a stream.</param>
        /// <param name="deserializerB">A delegate for deserialising the second paramter from a stream.</param>
        /// <param name="deserializerC">A delegate for deserialising the third paramter from a stream.</param>
        /// <param name="deserializerD">A delegate for deserialising the fourth paramter from a stream.</param>
        /// <param name="deserializerE">A delegate for deserialising the fifth paramter from a stream.</param>
        /// <param name="deserializerF">A delegate for deserialising the sixth paramter from a stream.</param>
        public RPCDeserializer(
            object target,
            MethodInfo method,
            GenericCallBackReturn<A, BinaryReader> deserializerA,
            GenericCallBackReturn<B, BinaryReader> deserializerB,
            GenericCallBackReturn<C, BinaryReader> deserializerC,
            GenericCallBackReturn<D, BinaryReader> deserializerD,
            GenericCallBackReturn<E, BinaryReader> deserializerE,
            GenericCallBackReturn<F, BinaryReader> deserializerF)
        {
            this.action = (GenericCallBack<A, B, C, D, E, F>)Delegate.CreateDelegate(
                typeof(GenericCallBack<A, B, C, D, E, F>),
                target,
                method);

            this.deserializerA = deserializerA;
            this.deserializerB = deserializerB;
            this.deserializerC = deserializerC;
            this.deserializerD = deserializerD;
            this.deserializerE = deserializerE;
            this.deserializerF = deserializerF;
        }

        /// <summary>
        /// Invoke the method using parameters serialized in a stream.
        /// </summary>
        /// <param name="reader">A binary reader providing access to the stream.</param>
        public void Invoke(BinaryReader reader)
        {
            this.action.Invoke(
                this.deserializerA(reader),
                this.deserializerB(reader),
                this.deserializerC(reader),
                this.deserializerD(reader),
                this.deserializerE(reader),
                this.deserializerF(reader));
        }
    }
}
