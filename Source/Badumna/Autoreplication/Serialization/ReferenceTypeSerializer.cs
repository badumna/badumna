﻿// ------------------------------------------------------------------------------
//  <copyright file="ReferenceTypeSerializer.cs" company="National ICT Australia Limited">
//      Copyright (c) 2012-2012 National ICT Australia Limited. All rights reserved.
//  </copyright>
// ------------------------------------------------------------------------------
namespace Badumna.Autoreplication.Serialization
{
    using System;
    using System.IO;
    using System.Reflection;

    using Badumna.Utilities;

    /// <summary>
    /// For checking for changes in the value of a reference type property of an object
    /// and writing its value to a stream.
    /// </summary>
    /// <typeparam name="T">The type of the property being updated (must be a reference type).</typeparam>
    internal class ReferenceTypeSerializer<T> : ISerializer where T : class
    {
        /// <summary>
        /// Delegate for getting the value of the property.
        /// </summary>
        private readonly GenericCallBackReturn<T> getter;

        /// <summary>
        /// Delegate for writing a value of the type of the property to a stream.
        /// </summary>
        private readonly SerializationMethod<T> serializer;

        /// <summary>
        /// Delegate for creating a new instance of type T that has value equality with a given instance.
        /// </summary>
        private readonly FactoryMethod<T> copyFactory;

        /// <summary>
        /// Baseline value against which checks for property value changes should be made.
        /// </summary>
        private T baseline;

        /// <summary>
        /// Initializes a new instance of the ReferenceTypeSerializer class for immutable reference types.
        /// </summary>
        /// <param name="target">The object whose property is to be serialized.</param>
        /// <param name="property">The property to be serialized.</param>
        /// <param name="serializer">A delegate for writing the value of the property to a stream.</param>
        public ReferenceTypeSerializer(
            object target,
            PropertyInfo property,
            SerializationMethod<T> serializer)
            : this(target, property, serializer, null)
        {
        }

        /// <summary>
        /// Initializes a new instance of the ReferenceTypeSerializer class for mutable reference types.
        /// </summary>
        /// <param name="target">The object whose property is to be serialized.</param>
        /// <param name="property">The property to be serialized.</param>
        /// <param name="serializer">A delegate for writing the value of the property to a stream.</param>
        /// <param name="copyFactory">A delegate for creating a new instance of type T that
        /// has value equality with a given instance.</param>
        public ReferenceTypeSerializer(
            object target,
            PropertyInfo property,
            SerializationMethod<T> serializer,
            FactoryMethod<T> copyFactory)
        {
            // This line is force iOS-Unity AOT compiler to generate the correct type.
            this.getter = () => default(T);
            this.getter =
                (GenericCallBackReturn<T>)
                Delegate.CreateDelegate(typeof(GenericCallBackReturn<T>), target, property.GetGetMethod(true));
            this.serializer = serializer;
            this.copyFactory = copyFactory;
        }

        /// <inheritdoc />
        public bool Check()
        {
            if (Object.Equals(this.baseline, this.getter()))
            {
                return false;
            }

            return true;
        }

        /// <inheritdoc />
        public void Serialize(BinaryWriter writer)
        {
            T currentValue = this.getter();
            if (currentValue == null)
            {
                this.baseline = null;
                writer.Write(false);
            }
            else
            {
                this.baseline = this.copyFactory != null ?
                    this.copyFactory(currentValue) :
                    currentValue;
                writer.Write(true);
                this.serializer.Invoke(this.baseline, writer);
            }
        }
    }
}