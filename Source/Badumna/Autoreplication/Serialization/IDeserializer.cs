// ------------------------------------------------------------------------------
//  <copyright file="IDeserializer.cs" company="National ICT Australia Limited">
//      Copyright (c) 2012-2012 National ICT Australia Limited. All rights reserved.
//  </copyright>
// ------------------------------------------------------------------------------

namespace Badumna.Autoreplication.Serialization
{
    using System;
    using System.IO;

    /// <summary>
    /// Non-generic interface for generic deserializer.
    /// </summary>
    internal interface IDeserializer
    {
        /// <summary>
        /// Set the property of an object with a value read from a stream.
        /// </summary>
        /// <param name="reader">A binary reader for reading from the stream.</param>
        void Deserialize(BinaryReader reader);
    }
}