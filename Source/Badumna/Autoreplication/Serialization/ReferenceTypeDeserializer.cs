﻿// ------------------------------------------------------------------------------
//  <copyright file="ReferenceTypeDeserializer.cs" company="National ICT Australia Limited">
//      Copyright (c) 2012-2012 National ICT Australia Limited. All rights reserved.
//  </copyright>
// ------------------------------------------------------------------------------
namespace Badumna.Autoreplication.Serialization
{
    using System;
    using System.IO;
    using System.Reflection;

    using Badumna.Utilities;

    /// <summary>
    /// For setting a reference type property on an object with a value read from a stream.
    /// </summary>
    /// <typeparam name="T">The type of the property being updated (must be a reference type).</typeparam>
    internal class ReferenceTypeDeserializer<T> : IDeserializer where T : class 
    {
        /// <summary>
        /// A delegate for setting the property.
        /// </summary>
        private readonly GenericCallBack<T> setter;

        /// <summary>
        /// A delegate for reading a value from a stream.
        /// </summary>
        private readonly DeserializationMethod<T> deserializer;

        /// <summary>
        /// Initializes a new instance of the ReferenceTypeDeserializer class.
        /// </summary>
        /// <param name="target">The object whose property is to be set.</param>
        /// <param name="property">The type property to be set.</param>
        /// <param name="deserializer">A delegate for reading a value for the property from a stream.</param>
        public ReferenceTypeDeserializer(object target, PropertyInfo property, DeserializationMethod<T> deserializer)
        {
            this.setter = (GenericCallBack<T>)Delegate.CreateDelegate(typeof(GenericCallBack<T>), target, property.GetSetMethod(true));
            this.deserializer = deserializer;
        }

        /// <summary>
        /// Set the property with a value read from a stream.
        /// </summary>
        /// <param name="reader">A binary reader for reading from the stream.</param>
        public void Deserialize(BinaryReader reader)
        {
            T value = null;
            if (reader.ReadBoolean())
            {
                value = this.deserializer.Invoke(reader);
            }

            this.setter.Invoke(value);
        }
    }
}