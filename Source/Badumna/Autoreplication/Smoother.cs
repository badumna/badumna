﻿// ------------------------------------------------------------------------------
//  <copyright file="Smoother.cs" company="National ICT Australia Limited">
//      Copyright (c) 2012-2012 National ICT Australia Limited. All rights reserved.
//  </copyright>
// ------------------------------------------------------------------------------
namespace Badumna.Autoreplication
{
    using System;
    using System.Collections.Generic;    
    using Badumna.Core;

    /// <summary>
    /// Calculates smooth continuous values for replicated properties
    /// based on discrete updates using interpolation and extrapolation.
    /// </summary>
    /// <typeparam name="T">The type of the value.</typeparam>
    internal abstract class Smoother<T>
    {
        /// <summary>
        /// Provides access to the current time.
        /// </summary>
        private readonly ITime clock;

        /// <summary>
        /// Provides arithmetical operations on values of a given type.
        /// </summary>
        private readonly IArithmetic<T> arithmetic;
        
        /// <summary>
        /// Interval to delay scheduled values by to allow interpolation.
        /// </summary>
        private readonly TimeSpan interpolationInterval;

        /// <summary>
        /// Interval to extrapolate for after last scheduled value is reached.
        /// </summary>
        private readonly TimeSpan extrapolationInterval;

        /// <summary>
        /// Queue of scheduled discrete value updates.
        /// </summary>
        private readonly Queue<ScheduledValue> scheduledValues = new Queue<ScheduledValue>();

        /// <summary>
        /// The most recent past scheduled value (start of interpolation/extrapolation).
        /// </summary>
        private ScheduledValue pastScheduledValue;

        /// <summary>
        /// A value indicating whether the terminal smoothed value has been reached until another update is scheduled.
        /// </summary>
        private bool terminalValueReached = false;

        /// <summary>
        /// Initializes a new instance of the Smoother class.
        /// </summary>
        /// <param name="value">A value of a given type.</param>
        /// <param name="interpolationInterval">Amount of time (in milliseconds) to delay scheduled
        ///  values by to allow interpolation.</param>
        /// <param name="extrapolationInterval">Amount of time (in milliseconds) to extrapolate for
        ///  after last scheduled value is reached.</param>
        /// <param name="clock">Provides access to the current time.</param>
        /// <param name="arithmetic">Provider of arithmetical operations on values of the given type.</param>
        protected Smoother(
            T value,
            int interpolationInterval,
            int extrapolationInterval,
            ITime clock,
            IArithmetic<T> arithmetic)
        {
            if (clock == null)
            {
                throw new ArgumentNullException("clock");
            }

            if (arithmetic == null)
            {
                throw new ArgumentNullException("arithmetic");
            }

            if (interpolationInterval <= 0)
            {
                throw new ArgumentOutOfRangeException(
                    "interpolationInterval",
                    "The interpolation interval must be greater than zero to allow smoothing.");
            }

            if (extrapolationInterval < 0)
            {
                throw new ArgumentOutOfRangeException(
                    "interpolationInterval",
                    "The extrapolation interval must not be negative.");
            }

            this.clock = clock;
            this.arithmetic = arithmetic;
            this.interpolationInterval = TimeSpan.FromMilliseconds(interpolationInterval);
            this.extrapolationInterval = TimeSpan.FromMilliseconds(extrapolationInterval);

            var now = this.clock.Now;
            this.pastScheduledValue = new ScheduledValue(now, value);
            var newScheduledValue = new ScheduledValue(now + this.interpolationInterval, value);
            this.scheduledValues.Enqueue(newScheduledValue);
        }

        /// <summary>
        /// Gets a value indicating whether the smoothed value has changed since last retrieved.
        /// </summary>
        public bool Changed
        {
            get { return !this.terminalValueReached; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the terminal smoothed value has been reached
        /// (until another update is scheduled).
        /// </summary>
        protected bool TerminalValueReached
        {
            get { return this.terminalValueReached; }

            set { this.terminalValueReached = value; } 
        }

        /// <summary>
        /// Gets or sets the latest rate of change of the value.
        /// </summary>
        protected T Derivative { get; set; }

        /// <summary>
        /// Gets the queue of scheduled values.
        /// </summary>
        protected Queue<ScheduledValue> ScheduledValues
        {
            get { return this.scheduledValues; }
        }

        /// <summary>
        /// Gets the provider of arithmetical operations for values of type T.
        /// </summary>
        protected IArithmetic<T> Arithmetic
        {
            get { return this.arithmetic; }    
        }

        /// <summary>
        /// Receive a new discrete value update.
        /// </summary>
        /// <param name="value">The value.</param>
        public abstract void Update(T value);

        /// <summary>
        /// Get the current smoothed value.
        /// </summary>
        /// <param name="currentTime">The current time.</param>
        /// <returns>The smoothed value.</returns>
        public T GetCurrentValue(TimeSpan currentTime)
        {
            this.AdvanceQueue(currentTime);
            if (this.scheduledValues.Count > 0)
            {
                return this.Interpolate(this.pastScheduledValue, this.scheduledValues.Peek(), currentTime);
            }

            return this.Extrapolate(this.pastScheduledValue, currentTime);
        }

        /// <summary>
        /// Schedule a newly received discrete value update.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>The scheduled value.</returns>
        protected ScheduledValue ScheduleUpdate(T value)
        {
            var now = this.clock.Now;

            // Insert a scheduled value for the value extrapolated to if currently extrapolating.
            // Extrapolation will use existing derivative estimate, not new one.
            this.AdvanceQueue(now);
            if (this.scheduledValues.Count == 0)
            {
                this.pastScheduledValue = new ScheduledValue(now, this.Extrapolate(this.pastScheduledValue, now));
            }

            this.terminalValueReached = false;
            var newScheduledValue = new ScheduledValue(now + this.interpolationInterval, value);
            this.scheduledValues.Enqueue(newScheduledValue);
            return newScheduledValue;
        }

        /// <summary>
        /// Extrapolate the value forward in time using its estimated rate of change.
        /// </summary>
        /// <param name="start">The scheduled value to start from.</param>
        /// <param name="time">The time to extrapolate to.</param>
        /// <returns>The extrapolated value for the given time.</returns>
        private T Extrapolate(ScheduledValue start, TimeSpan time)
        {
            TimeSpan interval = time - start.Time;
            if (interval > this.extrapolationInterval)
            {
                this.terminalValueReached = true;
                interval = this.extrapolationInterval;
            }

            return this.arithmetic.Add(start.Value, this.arithmetic.Scale(this.Derivative, interval.TotalSeconds));
        }

        /// <summary>
        /// Calculate an interpolated value for a given time.
        /// </summary>
        /// <param name="start">The scheduled start value.</param>
        /// <param name="end">The scheduled end value.</param>
        /// <param name="time">The time to interplate the value for.</param>
        /// <returns>The interpolated value.</returns>
        private T Interpolate(ScheduledValue start, ScheduledValue end, TimeSpan time)
        {
            var timeStep = time - start.Time;
            var timeInterval = end.Time - start.Time;
            var proportion = (float)timeStep.Ticks / timeInterval.Ticks;
            var totalDisplacement = this.arithmetic.Subtract(end.Value, start.Value);
            var jump = this.arithmetic.Scale(totalDisplacement, proportion);
            return this.arithmetic.Add(start.Value, jump);            
        }

        /// <summary>
        /// Advance past all scheduled updates earlier than a given time.
        /// </summary>
        /// <param name="time">The time to advance to.</param>
        private void AdvanceQueue(TimeSpan time)
        {
            while (this.scheduledValues.Count > 0 && this.scheduledValues.Peek().Time < time)
            {
                this.pastScheduledValue = this.scheduledValues.Dequeue();
            }            
        }

        /// <summary>
        /// Holds a value and a time that the value pertains to.
        /// </summary>
        protected struct ScheduledValue
        {
            /// <summary>
            /// A moment in time to which the value pertains.
            /// </summary>
            private readonly TimeSpan time;

            /// <summary>
            /// The replicated value.
            /// </summary>
            private readonly T value;

            /// <summary>
            /// Initializes a new instance of the Smoother.ScheduledValue struct.
            /// </summary>
            /// <param name="time">A moment in time to which the value pertains.</param>
            /// <param name="value">A value that holds for at the given time.</param>
            public ScheduledValue(TimeSpan time, T value)
            {
                this.time = time;
                this.value = value;
            }

            /// <summary>
            /// Gets the moment in time to which the replicated state pertains.
            /// </summary>
            public TimeSpan Time
            {
                get { return this.time; }
            }

            /// <summary>
            /// Gets the replicated position of the entity 
            /// </summary>
            public T Value
            {
                get { return this.value; }
            }

            /// <summary>
            /// Return a descripton of the data values.
            /// </summary>
            /// <returns>A string descibing the data values.</returns>
            public override string ToString()
            {
                return string.Format("{0}, {1}", this.time, this.value);
            }
        }         
    }
}