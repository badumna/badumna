﻿// ------------------------------------------------------------------------------
//  <copyright file="EstimatedDerivativeSmoother.cs" company="National ICT Australia Limited">
//      Copyright (c) 2012-2012 National ICT Australia Limited. All rights reserved.
//  </copyright>
// ------------------------------------------------------------------------------
namespace Badumna.Autoreplication
{
    using System;
    using System.Diagnostics;
    using Badumna.Core;

    /// <summary>
    /// Calculates smooth continuous values for replicated properties
    /// based on discrete updates using interpolation and extrapolation,
    /// where the rate of change to use during extrapolation is estimated.
    /// </summary>
    /// <typeparam name="T">The type of the value.</typeparam>
    internal class EstimatedDerivativeSmoother<T> : Smoother<T>
    {
        /// <summary>
        /// The latest scheduled value (used for estimating derivative).
        /// </summary>
        private ScheduledValue latestScheduledValue;

        /// <summary>
        /// Initializes a new instance of the EstimatedDerivativeSmoother class.
        /// </summary>
        /// <param name="value">A value of a given type.</param>
        /// <param name="interpolationInterval">Amount of time (in milliseconds) to delay scheduled
        ///  values by to allow interpolation.</param>
        /// <param name="extrapolationInterval">Amount of time (in milliseconds) to extrapolate for
        ///  after last scheduled value is reached.</param>
        /// <param name="clock">Provides access to the current time.</param>
        /// <param name="arithmetic">Provider of arithmetical operations on values of the given type.</param>
        public EstimatedDerivativeSmoother(
            T value,
            int interpolationInterval,
            int extrapolationInterval,
            ITime clock,
            IArithmetic<T> arithmetic)
            : base(value, interpolationInterval, extrapolationInterval, clock, arithmetic)
        {
            this.latestScheduledValue = this.ScheduledValues.Peek();
        }

        /// <inheritdoc />
        public override void Update(T value)
        {
            var newScheduledValue = this.ScheduleUpdate(value);
            this.EstimateDerivative(this.latestScheduledValue, newScheduledValue);
            this.latestScheduledValue = newScheduledValue;
        }

        /// <summary>
        /// Estimate the rate of change of the value per second, based on the change between
        /// to scheduled values.
        /// </summary>
        /// <param name="start">The first scheduled value.</param>
        /// <param name="end">The second scheduled value.</param>
        private void EstimateDerivative(ScheduledValue start, ScheduledValue end)
        {
            Debug.Assert(start.Time <= end.Time, "Following updates should never arrive before a previous one.");
            
            // When an update is received immediately after creation, it's no good for estimating velocity.
            if (start.Time == end.Time)
            {
                return;
            }

            this.Derivative = this.Arithmetic.Scale(
                this.Arithmetic.Subtract(end.Value, start.Value),
                1 / (end.Time - start.Time).TotalSeconds);
        }
    }
}