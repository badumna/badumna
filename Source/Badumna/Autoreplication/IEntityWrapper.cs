﻿// -----------------------------------------------------------------------
// <copyright file="IEntityWrapper.cs" company="National ICT Australia Limited">
//     Copyright (c) 2012 National ICT Australia Limited. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace Badumna.Autoreplication
{
    using System;
    using System.Collections.Generic;
    using Badumna.Replication;

    /// <summary>
    /// Interface for an replicable entity wrapper.
    /// </summary>
    internal interface IEntityWrapper : IEntity
    {
        /// <summary>
        /// Called each game loop to perform regular processing.
        /// </summary>
        /// <param name="currentTime">The current time.</param>
        void Tick(TimeSpan currentTime);

        /// <summary>
        /// Gets the index of an remotable method, for serializing the RPC.
        /// </summary>
        /// <param name="methodName">The name of the method.</param>
        /// <returns>The index of the remotable method.</returns>
        /// <exception cref="KeyNotFoundException">Thrown if no method with that name is marked as remotable.</exception>
        byte GetRemotableMethodIndex(string methodName);
    }
}