﻿// ------------------------------------------------------------------------------
//  <copyright file="SmoothingAttribute.cs" company="National ICT Australia Limited">
//      Copyright (c) 2012-2012 National ICT Australia Limited. All rights reserved.
//  </copyright>
// ------------------------------------------------------------------------------
namespace Badumna
{
    using System;
    using System.Reflection;

    /// <summary>
    /// An attribute used to indicate that replicated value of a property should be smoothed by Badumna.
    /// <para/>
    /// Normally, a replicable attribute will be updated whenever Badumna receives an update from the
    /// network. If Smoothing is used, Badumna will instead update the property frequently with
    /// values that are interpolated (or extrapolated) from the most recent values received.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, Inherited = true)]
    public class SmoothingAttribute : Attribute
    {
        /// <summary>
        /// The delay to be used for interpolation. Defaults to 200 ms;
        /// </summary>
        private int interpolation = 200;

        /// <summary>
        /// The delay to be used for extrapolation. Defaults to 200 ms.
        /// </summary>
        private int extrapolation = 200;

        /// <summary>
        /// Gets or sets the delay to be used for interpolation in milliseconds (defaults to 200ms).
        /// </summary>
        /// <remarks>
        /// When interpolation is set to a non-zero value, replica property updates will be delayed
        /// for the specified interval. In the intervening time, the property value will be
        /// interpolated between recent updates.
        /// </remarks>
        public int Interpolation
        {
            get
            {
                return this.interpolation;
            }

            set
            {
                if (value < 0)
                {
                    throw new ArgumentOutOfRangeException("Interpolation cannot be less than zero.");
                }

                this.interpolation = value;
            }
        }

        /// <summary>
        /// Gets or sets the time limit for extrapolation in milliseconds (defaults to 200ms).
        /// </summary>
        /// <remarks>
        /// When extrapolation is set to a non-zero value, replica property updates will be
        /// extrapolated for the given time interval after the most recent update is applied,
        /// based on the rate of change of the property's value as estimated based on the most
        /// recent two updates received.
        /// </remarks>
        public int Extrapolation
        {
            get
            {
                return this.extrapolation;
            }

            set
            {
                if (value < 0)
                {
                    throw new ArgumentOutOfRangeException("Extrapolation cannot be less than zero.");
                }
                
                this.extrapolation = value;
            }            
        }

        /// <summary>
        /// Gets or sets the name of the property holding the rate of change of the smoothed property.
        /// </summary>
        public string RateOfChangeProperty { get; set; }

        /// <summary>
        /// Gets a value indicating whether to use a rate of change property (instead of estimating it).
        /// </summary>
        public bool UsesRateOfChangeProperty
        {
            get { return this.RateOfChangeProperty != null; }
        }
    }
}