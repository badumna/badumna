﻿// ------------------------------------------------------------------------------
//  <copyright file="ReplicatedDerivativeSmoother.cs" company="National ICT Australia Limited">
//      Copyright (c) 2012-2012 National ICT Australia Limited. All rights reserved.
//  </copyright>
// ------------------------------------------------------------------------------
namespace Badumna.Autoreplication
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;

    using Badumna.Core;

    /// <summary>
    /// Calculates smooth continuous values for replicated properties
    /// based on discrete updates using interpolation and extrapolation,
    /// where the rate of change to use during extrapolation is replicated.
    /// </summary>
    /// <typeparam name="T">The type of the value.</typeparam>
    internal class ReplicatedDerivativeSmoother<T> : Smoother<T>
    {
        /// <summary>
        /// Initializes a new instance of the ReplicatedDerivativeSmoother class.
        /// </summary>
        /// <param name="value">A value of a given type.</param>
        /// <param name="interpolationInterval">Amount of time (in milliseconds) to delay scheduled
        ///  values by to allow interpolation.</param>
        /// <param name="extrapolationInterval">Amount of time (in milliseconds) to extrapolate for
        ///  after last scheduled value is reached.</param>
        /// <param name="clock">Provides access to the current time.</param>
        /// <param name="arithmetic">Provider of arithmetical operations on values of the given type.</param>
        public ReplicatedDerivativeSmoother(
            T value,
            int interpolationInterval,
            int extrapolationInterval,
            ITime clock,
            IArithmetic<T> arithmetic)
            : base(value, interpolationInterval, extrapolationInterval, clock, arithmetic)
        {
        }

        /// <inheritdoc />
        public override void Update(T value)
        {
            this.ScheduleUpdate(value);
        }

        /// <summary>
        /// Update the current rate of change of the value with the replicated rate of change.
        /// </summary>
        /// <param name="derivative">The replicated rate of change.</param>
        public void UpdateDerivative(T derivative)
        {
            this.Derivative = derivative;
        }      
    }
}