﻿//-----------------------------------------------------------------------
// <copyright file="ReplicableAttribute.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2012 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna
{
    using System;

    /// <summary>
    /// Attribute for marking properties of a replicable entity that form part of
    /// its replicable state.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Method, Inherited = true)]
    public class ReplicableAttribute : Attribute
    {
    }
}