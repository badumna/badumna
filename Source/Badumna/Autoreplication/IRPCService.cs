﻿//------------------------------------------------------------------------------
// <copyright file="IRPCService.cs" company="National ICT Australia Limited">
//     Copyright (c) 2007-2012 National ICT Australia Limited. All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

namespace Badumna.Autoreplication
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using Badumna.Autoreplication.Serialization;
    using Badumna.Core;
    using Badumna.DataTypes;
    using Badumna.SpatialEntities;

    /// <summary>
    /// Manages Auto-replication.
    /// Responsible for creating wrappers for replicable entities implementing
    /// ISpatialOriginal or ISpatialReplica as required, flagging properties for
    /// update, and serializing and deserializing state updates.
    /// </summary>
    internal interface IRPCService
    {
        /// <summary>
        /// Call a parameterless method on all the replicas of a given original.
        /// </summary>
        /// <param name="method">The corresponding method on the original.</param>
        void CallMethodOnReplicas(RpcSignature method);

        /// <summary>
        /// Call a method on all the replicas of a given original.
        /// </summary>
        /// <typeparam name="T1">The type of the method's first parameter.</typeparam>
        /// <param name="method">The corresponding method on the original.</param>
        /// <param name="arg1">The first parameter.</param>
        void CallMethodOnReplicas<T1>(RpcSignature<T1> method, T1 arg1);

        /// <summary>
        /// Call a method on all the replicas of a given original.
        /// </summary>
        /// <typeparam name="T1">The type of the method's first parameter.</typeparam>
        /// <typeparam name="T2">The type of the method's second parameter.</typeparam>
        /// <param name="method">The corresponding method on the original.</param>
        /// <param name="arg1">The first parameter.</param>
        /// <param name="arg2">The second parameter.</param>
        void CallMethodOnReplicas<T1, T2>(RpcSignature<T1, T2> method, T1 arg1, T2 arg2);

        /// <summary>
        /// Call a method on all the replicas of a given original.
        /// </summary>
        /// <typeparam name="T1">The type of the method's first parameter.</typeparam>
        /// <typeparam name="T2">The type of the method's second parameter.</typeparam>
        /// <typeparam name="T3">The type of the method's third parameter.</typeparam>
        /// <param name="method">The corresponding method on the original.</param>
        /// <param name="arg1">The first parameter.</param>
        /// <param name="arg2">The second parameter.</param>
        /// <param name="arg3">The third parameter.</param>
        void CallMethodOnReplicas<T1, T2, T3>(
            RpcSignature<T1, T2, T3> method,
            T1 arg1,
            T2 arg2,
            T3 arg3);

        /// <summary>
        /// Call a method on all the replicas of a given original.
        /// </summary>
        /// <typeparam name="T1">The type of the method's first parameter.</typeparam>
        /// <typeparam name="T2">The type of the method's second parameter.</typeparam>
        /// <typeparam name="T3">The type of the method's third parameter.</typeparam>
        /// <typeparam name="T4">The type of the method's fourth parameter.</typeparam>
        /// <param name="method">The corresponding method on the original.</param>
        /// <param name="arg1">The first parameter.</param>
        /// <param name="arg2">The second parameter.</param>
        /// <param name="arg3">The third parameter.</param>
        /// <param name="arg4">The fourth parameter.</param>
        void CallMethodOnReplicas<T1, T2, T3, T4>(
            RpcSignature<T1, T2, T3, T4> method,
            T1 arg1,
            T2 arg2,
            T3 arg3,
            T4 arg4);

        /// <summary>
        /// Call a method on all the replicas of a given original.
        /// </summary>
        /// <typeparam name="T1">The type of the method's first parameter.</typeparam>
        /// <typeparam name="T2">The type of the method's second parameter.</typeparam>
        /// <typeparam name="T3">The type of the method's third parameter.</typeparam>
        /// <typeparam name="T4">The type of the method's fourth parameter.</typeparam>
        /// <typeparam name="T5">The type of the method's fifth parameter.</typeparam>
        /// <param name="method">The corresponding method on the original.</param>
        /// <param name="arg1">The first parameter.</param>
        /// <param name="arg2">The second parameter.</param>
        /// <param name="arg3">The third parameter.</param>
        /// <param name="arg4">The fourth parameter.</param>
        /// <param name="arg5">The fifth parameter.</param>
        void CallMethodOnReplicas<T1, T2, T3, T4, T5>(
            RpcSignature<T1, T2, T3, T4, T5> method,
            T1 arg1,
            T2 arg2,
            T3 arg3,
            T4 arg4,
            T5 arg5);

        /// <summary>
        /// Call a method on all the replicas of a given original.
        /// </summary>
        /// <typeparam name="T1">The type of the method's first parameter.</typeparam>
        /// <typeparam name="T2">The type of the method's second parameter.</typeparam>
        /// <typeparam name="T3">The type of the method's third parameter.</typeparam>
        /// <typeparam name="T4">The type of the method's fourth parameter.</typeparam>
        /// <typeparam name="T5">The type of the method's fifth parameter.</typeparam>
        /// <typeparam name="T6">The type of the method's sixth parameter.</typeparam>
        /// <param name="method">The corresponding method on the original.</param>
        /// <param name="arg1">The first parameter.</param>
        /// <param name="arg2">The second parameter.</param>
        /// <param name="arg3">The third parameter.</param>
        /// <param name="arg4">The fourth parameter.</param>
        /// <param name="arg5">The fifth parameter.</param>
        /// <param name="arg6">The sixth parameter.</param>
        void CallMethodOnReplicas<T1, T2, T3, T4, T5, T6>(
            RpcSignature<T1, T2, T3, T4, T5, T6> method,
            T1 arg1,
            T2 arg2,
            T3 arg3,
            T4 arg4,
            T5 arg5,
            T6 arg6);

        /// <summary>
        /// Call a method on all the original of a given replica.
        /// </summary>
        /// <param name="method">The corresponding method on the replica.</param>
        void CallMethodOnOriginal(RpcSignature method);

        /// <summary>
        /// Call a method on all the original of a given replica.
        /// </summary>
        /// <typeparam name="T1">The type of the method's first parameter.</typeparam>
        /// <param name="method">The corresponding method on the replica.</param>
        /// <param name="arg1">The first parameter.</param>
        void CallMethodOnOriginal<T1>(RpcSignature<T1> method, T1 arg1);

        /// <summary>
        /// Call a method on all the original of a given replica.
        /// </summary>
        /// <typeparam name="T1">The type of the method's first parameter.</typeparam>
        /// <typeparam name="T2">The type of the method's second parameter.</typeparam>
        /// <param name="method">The corresponding method on the replica.</param>
        /// <param name="arg1">The first parameter.</param>
        /// <param name="arg2">The second parameter.</param>
        void CallMethodOnOriginal<T1, T2>(RpcSignature<T1, T2> method, T1 arg1, T2 arg2);

        /// <summary>
        /// Call a method on all the original of a given replica.
        /// </summary>
        /// <typeparam name="T1">The type of the method's first parameter.</typeparam>
        /// <typeparam name="T2">The type of the method's second parameter.</typeparam>
        /// <typeparam name="T3">The type of the method's third parameter.</typeparam>
        /// <param name="method">The corresponding method on the replica.</param>
        /// <param name="arg1">The first parameter.</param>
        /// <param name="arg2">The second parameter.</param>
        /// <param name="arg3">The third parameter.</param>
        void CallMethodOnOriginal<T1, T2, T3>(
            RpcSignature<T1, T2, T3> method,
            T1 arg1,
            T2 arg2,
            T3 arg3);

        /// <summary>
        /// Call a method on all the original of a given replica.
        /// </summary>
        /// <typeparam name="T1">The type of the method's first parameter.</typeparam>
        /// <typeparam name="T2">The type of the method's second parameter.</typeparam>
        /// <typeparam name="T3">The type of the method's third parameter.</typeparam>
        /// <typeparam name="T4">The type of the method's fourth parameter.</typeparam>
        /// <param name="method">The corresponding method on the replica.</param>
        /// <param name="arg1">The first parameter.</param>
        /// <param name="arg2">The second parameter.</param>
        /// <param name="arg3">The third parameter.</param>
        /// <param name="arg4">The fourth parameter.</param>
        void CallMethodOnOriginal<T1, T2, T3, T4>(
            RpcSignature<T1, T2, T3, T4> method,
            T1 arg1,
            T2 arg2,
            T3 arg3,
            T4 arg4);

        /// <summary>
        /// Call a method on all the original of a given replica.
        /// </summary>
        /// <typeparam name="T1">The type of the method's first parameter.</typeparam>
        /// <typeparam name="T2">The type of the method's second parameter.</typeparam>
        /// <typeparam name="T3">The type of the method's third parameter.</typeparam>
        /// <typeparam name="T4">The type of the method's fourth parameter.</typeparam>
        /// <typeparam name="T5">The type of the method's fifth parameter.</typeparam>
        /// <param name="method">The corresponding method on the replica.</param>
        /// <param name="arg1">The first parameter.</param>
        /// <param name="arg2">The second parameter.</param>
        /// <param name="arg3">The third parameter.</param>
        /// <param name="arg4">The fourth parameter.</param>
        /// <param name="arg5">The fifth parameter.</param>
        void CallMethodOnOriginal<T1, T2, T3, T4, T5>(
            RpcSignature<T1, T2, T3, T4, T5> method,
            T1 arg1,
            T2 arg2,
            T3 arg3,
            T4 arg4,
            T5 arg5);

        /// <summary>
        /// Call a method on all the original of a given replica.
        /// </summary>
        /// <typeparam name="T1">The type of the method's first parameter.</typeparam>
        /// <typeparam name="T2">The type of the method's second parameter.</typeparam>
        /// <typeparam name="T3">The type of the method's third parameter.</typeparam>
        /// <typeparam name="T4">The type of the method's fourth parameter.</typeparam>
        /// <typeparam name="T5">The type of the method's fifth parameter.</typeparam>
        /// <typeparam name="T6">The type of the method's sixth parameter.</typeparam>
        /// <param name="method">The corresponding method on the replica.</param>
        /// <param name="arg1">The first parameter.</param>
        /// <param name="arg2">The second parameter.</param>
        /// <param name="arg3">The third parameter.</param>
        /// <param name="arg4">The fourth parameter.</param>
        /// <param name="arg5">The fifth parameter.</param>
        /// <param name="arg6">The sixth parameter.</param>
        void CallMethodOnOriginal<T1, T2, T3, T4, T5, T6>(
            RpcSignature<T1, T2, T3, T4, T5, T6> method,
            T1 arg1,
            T2 arg2,
            T3 arg3,
            T4 arg4,
            T5 arg5,
            T6 arg6);
    }
}
