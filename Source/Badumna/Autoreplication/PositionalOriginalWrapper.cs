﻿// -----------------------------------------------------------------------
// <copyright file="PositionalOriginalWrapper.cs" company="National ICT Australia Limited">
//     Copyright (c) 2012 National ICT Australia Limited. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace Badumna.Autoreplication
{
    using System;
    using System.Collections.Generic;
    using System.IO;

    using Badumna.Autoreplication.Serialization;
    using Badumna.DataTypes;
    using Badumna.SpatialEntities;
    using Badumna.Utilities;
    using Badumna.Utilities.Logging;

    /// <summary>
    /// Delegate for creating original wrappers.
    /// </summary>
    /// <param name="original">The original replicable entity to wrap.</param>
    /// <param name="radius">The radius of the entity's bounding sphere.</param>
    /// <param name="sphereOfInterestRadius">The radius of the entity's sphere of interest.</param>
    /// <returns>A new instance implementing IOriginalWrapper.</returns>
    internal delegate IPositionalOriginalWrapper PositionalOriginalWrapperFactory(IReplicableEntity original, float radius, float sphereOfInterestRadius);

    /// <summary>
    /// Interface for original wrapper to permit mocking.
    /// </summary>
    internal interface IPositionalOriginalWrapper : IOriginalWrapper, ISpatialOriginal
    {
    }

    /// <summary>
    /// OriginalWrapper wraps an original replicable entity and implements ISpatialEntity allowing
    /// Badumna to replicate state changes.
    /// </summary>
    internal class PositionalOriginalWrapper : PositionalEntityWrapper, IPositionalOriginalWrapper
    {
        /// <summary>
        /// The spatial entity manager for sending replication flags and and custom messages.
        /// </summary>
        private readonly ISpatialEntityManager spatialEntityManager;

        /// <summary>
        /// Serializer for position's derivate, if used.
        /// </summary>
        private readonly ISerializer velocitySerializer;

        /// <summary>
        /// Serializers for replicable properties of the original entity.
        /// </summary>
        private readonly List<ISerializer> propertySerializers = new List<ISerializer>();

        /// <summary>
        /// The last replicated position.
        /// </summary>
        private Vector3 baselinePosition;

        /// <summary>
        /// A value indicating that it is permitted to access the wrapped entity's members.
        /// </summary>
        private bool accessible = true;

        /// <summary>
        /// Initializes a new instance of the PositionalOriginalWrapper class.
        /// </summary>
        /// <param name="original">The original replicable entity to wrap.</param>
        /// <param name="radius">The radius of the entity's bounding sphere.</param>
        /// <param name="sphereOfInterestRadius">The radius of the entity's sphere of interest.</param>
        /// <param name="spatialEntityManager">The entity manager for flagging updates and sending custom messages.</param>
        /// <param name="serialization">For creation, and serialization of types.</param>
        public PositionalOriginalWrapper(
            IReplicableEntity original,
            float radius,
            float sphereOfInterestRadius,
            ISpatialEntityManager spatialEntityManager,
            Serialization.Manager serialization)
            : base(serialization, original, radius, sphereOfInterestRadius)
        {
            Logger.TraceInformation(LogTag.Autoreplication, "Creating original wrapper.");
            if (spatialEntityManager == null)
            {
                throw new ArgumentNullException("spatialEntityManager");
            }

            this.spatialEntityManager = spatialEntityManager;

            this.baselinePosition = this.ReplicableEntity.Position;

            if (this.VelocityProperty != null)
            {
                Logger.TraceInformation(LogTag.Autoreplication, "Original entity using replicated velocity.");
                this.velocitySerializer = this.Serialization.CreatePropertySerializer(
                    this.ReplicableEntity, this.VelocityProperty);
            }

            foreach (var property in this.ReplicableProperties)
            {
                SmoothingAttribute smoothingAttribute;
                if (this.SmoothingAttribtues.TryGetValue(property, out smoothingAttribute))
                {
                    if (smoothingAttribute.UsesRateOfChangeProperty)
                    {
                        this.propertySerializers.Add(
                            this.Serialization.CreateDeadReckonedPropertySerializer(
                                this.ReplicableEntity,
                                property,
                                smoothingAttribute));
                        continue;
                    }
                }

                this.propertySerializers.Add(
                    this.Serialization.CreatePropertySerializer(this.ReplicableEntity, property));
            }
        }

        /// <inheritdoc/>
        public override Vector3 Position
        {
            // Return baseline position to prevent replicable entity's Position getter being
            // called on the network thread.
            get { return this.baselinePosition; }

            // I'm not sure this should ever be called - if not then it should throw an exception.
            set { this.ReplicableEntity.Position = value; }
        }

        /// <inheritdoc/>
        public override void Tick(TimeSpan currentTime)
        {
            if (this.accessible)
            {
                var changedParts = this.FlagChangedProperties();
                if (changedParts.Any())
                {
                    this.spatialEntityManager.MarkForUpdate(this, changedParts);
                }
            }
        }

        /// <inheritdoc/>
        public void FlagFullUpdate()
        {
            if (!this.accessible)
            {
                this.spatialEntityManager.MarkForUpdate(this, new BooleanArray(true));
            }
        }

        /// <inheritdoc/>
        public void Serialize(BooleanArray requiredParts, Stream stream)
        {
            // If the entity is not accessible (i.e. is being unregistered) then
            // do not serialize anything. This can occur due to period update
            // being triggered.
            if (!this.accessible)
            {
                return;
            }

            var writer = new BinaryWriter(stream);
            if (requiredParts[(int)SpatialEntityStateSegment.Position])
            {
                if (this.velocitySerializer != null)
                {
                    Logger.TraceInformation(LogTag.Autoreplication, "Serializing velocity: " + this.VelocityProperty.GetValue(this.ReplicableEntity, null).ToString());
                    this.velocitySerializer.Serialize(writer);
                }
            }

            var propertyIndex = (int)SpatialEntityStateSegment.FirstAvailableSegment;
            foreach (var serializer in this.propertySerializers)
            {
                if (requiredParts[propertyIndex])
                {
                    serializer.Serialize(writer);
                }

                propertyIndex++;
            }
        }

        /// <summary>
        /// Prevent access of the wrapped entity's public members.
        /// </summary>
        public void MarkInaccessible()
        {
            this.accessible = false;
        }

        /// <summary>
        /// Create a BooleanArray with flags set for properties that have changed since last call.
        /// </summary>
        /// <returns>A BooleanArray with flags set for properties that have changed since last call.</returns>
        private BooleanArray FlagChangedProperties()
        {
            var flags = new BooleanArray();
            this.FlagPositionReplication(flags);
            var propertyIndex = (int)SpatialEntityStateSegment.FirstAvailableSegment;
            foreach (var serializer in this.propertySerializers)
            {
                if (serializer.Check())
                {
                    flags[propertyIndex] = true;                    
                }

                propertyIndex++;
            }

            return flags;
        }

        /// <summary>
        /// Flag position replication required (and velocity if dead-reckoned).
        /// </summary>
        /// <remarks>
        /// If dead-reckoning is used, position updates are only flagged when velocity has changed.</remarks>
        /// <param name="flags">The flag array to set the flag in.</param>
        private void FlagPositionReplication(BooleanArray flags)
        {
            Vector3 currentPosition = this.ReplicableEntity.Position;
            if (this.velocitySerializer != null)
            {
                if (this.velocitySerializer.Check())
                {
                    Logger.TraceInformation(LogTag.Autoreplication, "Flagging position change because velocity has changed.");
                    flags[(int)SpatialEntityStateSegment.Position] = true;
                }
                else
                {
                    Logger.TraceInformation(LogTag.Autoreplication, "Skipping position check because velocity has not changed.");
                }
            }
            else if (!currentPosition.Equals(this.baselinePosition))
            {
                Logger.TraceInformation(LogTag.Autoreplication, "Flagging position change because position has changed.");
                flags[(int)SpatialEntityStateSegment.Position] = true;
            }

            // Baseline position must always be updated as it is this value that is serialized
            // rather than entity's Position property's current value.
            this.baselinePosition = currentPosition;
        }
    }
}
