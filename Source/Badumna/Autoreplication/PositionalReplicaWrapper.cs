﻿// -----------------------------------------------------------------------
// <copyright file="PositionalReplicaWrapper.cs" company="National ICT Australia Limited">
//     Copyright (c) 2012 National ICT Australia Limited. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace Badumna.Autoreplication
{
    using System;
    using System.Collections.Generic;
    using System.IO;

    using Badumna.Autoreplication.Serialization;
    using Badumna.Core;
    using Badumna.DataTypes;
    using Badumna.SpatialEntities;
    using Badumna.Utilities;
    using Badumna.Utilities.Logging;

    /// <summary>
    /// Delegate for creating original wrappers.
    /// </summary>
    /// <param name="replica">The replica replicable entity to wrap.</param>
    /// <returns>A new instance implementing IReplicaWrapper.</returns>
    internal delegate IPositionalReplicaWrapper PositionalReplicaWrapperFactory(IReplicableEntity replica);

    /// <summary>
    /// Interface to mark replica wrappers.
    /// </summary>
    internal interface IPositionalReplicaWrapper : IReplicaWrapper, ISpatialReplica
    {
    }

    /// <summary>
    /// Autoreplication wrapper for replica replicable entities.
    /// Responsible for deserializing state updates and updating the wrapped
    /// entity's replicable properties.
    /// </summary>
    internal class PositionalReplicaWrapper : PositionalEntityWrapper, IPositionalReplicaWrapper
    {
        /// <summary>
        /// Provides access to current time.
        /// </summary>
        private readonly ITime timeKeeper;

        /// <summary>
        /// Deserializers for replicable properties of the original entity.
        /// </summary>
        private readonly List<IDeserializer> propertyDeserializers = new List<IDeserializer>();

        /// <summary>
        /// Smoothers that require ticking.
        /// </summary>
        private readonly List<ISmoothingDeserializer> smoothers = new List<ISmoothingDeserializer>();

        /// <summary>
        /// For smoothing position data.
        /// </summary>
        private Smoother<Vector3> positionSmoother;

        /// <summary>
        /// Delegate for setting the velocity property (as identified by the
        /// position property's smoothing attribute).
        /// </summary>
        private GenericCallBack<Vector3> velocitySetter;

        /// <summary>
        /// Holds smoothing parameters if present.
        /// </summary>
        private SmoothingAttribute smoothingAttribute;
        
        /// <summary>
        /// Initializes a new instance of the PositionalReplicaWrapper class.
        /// </summary>
        /// <param name="replica">The replica replicable entity to wrap.</param>
        /// <param name="serialization">For creation and serialization of types.</param>
        /// <param name="timeKeeper">Provides access to current time.</param>
        public PositionalReplicaWrapper(
            IReplicableEntity replica,
            Serialization.Manager serialization,
            ITime timeKeeper)
            : base(serialization, replica, 0f, 0f)
        {
            if (timeKeeper == null)
            {
                throw new ArgumentNullException("timeKeeper");
            }

            this.timeKeeper = timeKeeper;
            foreach (var property in this.ReplicableProperties)
            {
                SmoothingAttribute smoothingAttribute;
                if (this.SmoothingAttribtues.TryGetValue(property, out smoothingAttribute))
                {
                    var smoother = this.Serialization.CreateSmoothingPropertyDeserializer(this.ReplicableEntity, property, smoothingAttribute);
                    this.propertyDeserializers.Add(smoother);
                    this.smoothers.Add(smoother);
                }
                else
                {
                    this.propertyDeserializers.Add(
                        this.Serialization.CreatePropertyDeserializer(this.ReplicableEntity, property));
                }
            }

            var positionProperty = this.ReplicableEntity.GetType().GetProperty("Position");
            this.smoothingAttribute = Attribute.GetCustomAttribute(positionProperty, typeof(SmoothingAttribute))
                as SmoothingAttribute;
        }

        /// <inheritdoc/>
        public override Vector3 Position
        {
            get
            {
                throw new InvalidOperationException(
                    "Position getter is provided to fulfill interface, but should never be called");
            }

            set
            {
                if (this.PositionSmoothingAttribute != null)
                {
                    if (this.positionSmoother != null)
                    {
                        Logger.TraceInformation(LogTag.Autoreplication, "Deserialized position: " + value.ToString());
                        this.positionSmoother.Update(value);
                        return;
                    }

                    Logger.TraceInformation(LogTag.Autoreplication, "Replica lazily creating position smoother.");
                    this.CreatePositionSmoother(value);
                        
                    // Initialize position to intial value, as
                    // smoother may not be ticked immediately.
                    this.ReplicableEntity.Position = value;
                }
                else
                {
                    this.ReplicableEntity.Position = value;
                }
            }
        }

        /// <summary>
        /// Update entity position if dead reckoning is being used.
        /// </summary>
        /// <param name="currentTime">The current time.</param>
        public override void Tick(TimeSpan currentTime)
        {
            if (this.positionSmoother != null)
            {
                Vector3 smoothedPosition = this.positionSmoother.GetCurrentValue(currentTime);
                Logger.TraceInformation(LogTag.Autoreplication, "Smoothed position calculated as: " + smoothedPosition.ToString());
                this.ReplicableEntity.Position = smoothedPosition;
            }

            foreach (var smoother in this.smoothers)
            {
                smoother.Tick(currentTime);
            }
        }

        /// <inheritdoc/>
        public void Deserialize(BooleanArray includedParts, Stream stream, int estimatedMillisecondsSinceDeparture)
        {
            var reader = new BinaryReader(stream);
            if (this.velocitySetter != null && includedParts[(int)SpatialEntityStateSegment.Position])
            {
                var velocity = new Vector3(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());
                Logger.TraceInformation(LogTag.Autoreplication, "Deserialized velocity: {0}", velocity.ToString());
                ((ReplicatedDerivativeSmoother<Vector3>)this.positionSmoother).UpdateDerivative(velocity);
                this.velocitySetter(velocity);
            }

            var propertyIndex = (int)SpatialEntityStateSegment.FirstAvailableSegment;
            foreach (var deserializer in this.propertyDeserializers)
            {
                if (includedParts[propertyIndex])
                {
                    deserializer.Deserialize(reader);
                }

                propertyIndex++;
            }
        }

        /// <summary>
        /// Lazily create smoother for position property.
        /// </summary>
        /// <param name="initialPosition">The initial position.</param>
        private void CreatePositionSmoother(Vector3 initialPosition)
        {
            if (this.PositionSmoothingAttribute.UsesRateOfChangeProperty)
            {
                Logger.TraceInformation(LogTag.Autoreplication, "Replica using replicated derivate smoother.");
                this.positionSmoother = new ReplicatedDerivativeSmoother<Vector3>(
                    initialPosition,
                    this.PositionSmoothingAttribute.Interpolation,
                    this.PositionSmoothingAttribute.Extrapolation,
                    this.timeKeeper,
                    new Vector3Arithmetic());
                this.velocitySetter = (GenericCallBack<Vector3>)Delegate.CreateDelegate(
                    typeof(GenericCallBack<Vector3>), this.ReplicableEntity, this.VelocityProperty.GetSetMethod(true));
            }
            else
            {
                Logger.TraceInformation(LogTag.Autoreplication, "Replica using estimated derivate smoother.");
                this.positionSmoother = new EstimatedDerivativeSmoother<Vector3>(
                    initialPosition,
                    this.PositionSmoothingAttribute.Interpolation,
                    this.PositionSmoothingAttribute.Extrapolation,
                    this.timeKeeper,
                    new Vector3Arithmetic());
            }
        }
    }
}
