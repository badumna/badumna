﻿// -----------------------------------------------------------------------
// <copyright file="OriginalEntityWrapper.cs" company="National ICT Australia Limited">
//     Copyright (c) 2012 National ICT Australia Limited. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace Badumna.Autoreplication
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using Badumna.Autoreplication.Serialization;
    using Badumna.Replication;
    using Badumna.SpatialEntities;
    using Badumna.Utilities;
    using Badumna.Utilities.Logging;

    /// <summary>
    /// Delegate for creating original wrappers.
    /// </summary>
    /// <param name="original">The original replicable entity to wrap.</param>
    /// <returns>A new instance implementing IOriginalWrapper.</returns>
    internal delegate IOriginalEntityWrapper OriginalEntityWrapperFactory(object original);

    /// <summary>
    /// Interface for original wrapper to permit mocking.
    /// </summary>
    internal interface IOriginalEntityWrapper : IOriginalWrapper, Replication.IOriginal
    {
    }

    /// <summary>
    /// OriginalWrapper wraps an original replicable entity and implements ISpatialEntity allowing
    /// Badumna to replicate state changes.
    /// </summary>
    internal class OriginalEntityWrapper : EntityWrapper, IOriginalEntityWrapper
    {
        /// <summary>
        /// The network facade.
        /// </summary>
        private readonly IReplicationService<IOriginal, IReplica> entityManager;

        /// <summary>
        /// Serializers for replicable properties of the original entity.
        /// </summary>
        private readonly List<ISerializer> propertySerializers = new List<ISerializer>();

        /// <summary>
        /// A value indicating that it is permitted to access the wrapped entity's members.
        /// </summary>
        private bool accessible = true;

        /// <summary>
        /// Initializes a new instance of the OriginalEntityWrapper class.
        /// </summary>
        /// <param name="original">The original replicable entity to wrap.</param>
        /// <param name="entityManager">The replication service for flagging updates.</param>
        /// <param name="serialization">For creation, and serialization of types.</param>
        public OriginalEntityWrapper(
            object original,
            IReplicationService<IOriginal, IReplica> entityManager,
            Serialization.Manager serialization)
            : base(serialization, original)
        {
            Logger.TraceInformation(LogTag.Autoreplication, "Creating original wrapper.");
            if (entityManager == null)
            {
                throw new ArgumentNullException("networkFacade");
            }

            this.entityManager = entityManager;

            foreach (var property in this.ReplicableProperties)
            {
                SmoothingAttribute smoothingAttribute;
                if (this.SmoothingAttribtues.TryGetValue(property, out smoothingAttribute))
                {
                    if (smoothingAttribute.UsesRateOfChangeProperty)
                    {
                        this.propertySerializers.Add(
                            this.Serialization.CreateDeadReckonedPropertySerializer(
                                this.Entity,
                                property,
                                smoothingAttribute));
                        continue;
                    }
                }

                this.propertySerializers.Add(
                    this.Serialization.CreatePropertySerializer(this.Entity, property));
            }
        }

        /// <inheritdoc/>
        public override void Tick(TimeSpan currentTime)
        {
            if (this.accessible)
            {
                var changedParts = this.FlagChangedProperties();
                if (changedParts.Any())
                {
                    this.entityManager.MarkForUpdate(this, changedParts);
                }
            }
        }

        /// <inheritdoc/>
        public void FlagFullUpdate()
        {
            if (this.accessible)
            {
                this.entityManager.MarkForUpdate(this, new BooleanArray(true));
            }
        }

        /// <inheritdoc/>
        public BooleanArray Serialize(BooleanArray requiredParts, Stream stream)
        {
            // If the entity is not accessible (i.e. is being unregistered) then
            // do not serialize anything. This can occur due to period update
            // being triggered.
            if (!this.accessible)
            {
                return new BooleanArray();
            }

            var writer = new BinaryWriter(stream);
            var propertyIndex = (int)SpatialEntityStateSegment.FirstAvailableSegment;
            foreach (var serializer in this.propertySerializers)
            {
                if (requiredParts[propertyIndex])
                {
                    serializer.Serialize(writer);
                }

                propertyIndex++;
            }

            return requiredParts;
        }

        /// <summary>
        /// Prevent access of the wrapped entity's public members.
        /// </summary>
        public void MarkInaccessible()
        {
            this.accessible = false;
        }

        /// <summary>
        /// Create a BooleanArray with flags set for properties that have changed since last call.
        /// </summary>
        /// <returns>A BooleanArray with flags set for properties that have changed since last call.</returns>
        private BooleanArray FlagChangedProperties()
        {
            var flags = new BooleanArray();
            var propertyIndex = (int)SpatialEntityStateSegment.FirstAvailableSegment;
            foreach (var serializer in this.propertySerializers)
            {
                if (serializer.Check())
                {
                    flags[propertyIndex] = true;
                }

                propertyIndex++;
            }

            return flags;
        }
    }
}
