﻿// -----------------------------------------------------------------------
// <copyright file="IReplicaWrapper.cs" company="National ICT Australia Limited">
//     Copyright (c) 2012 National ICT Australia Limited. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace Badumna.Autoreplication
{
    using System.Collections.Generic;

    /// <summary>
    /// Interface for an replicable entity wrapper.
    /// </summary>
    internal interface IReplicaWrapper : IEntityWrapper
    {
    }
}