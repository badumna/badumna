﻿// -----------------------------------------------------------------------
// <copyright file="ReplicaEntityWrapper.cs" company="National ICT Australia Limited">
//     Copyright (c) 2012 National ICT Australia Limited. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace Badumna.Autoreplication
{
    using System;
    using System.Collections.Generic;
    using System.IO;

    using Badumna.Autoreplication.Serialization;
    using Badumna.Core;
    using Badumna.DataTypes;
    using Badumna.SpatialEntities;
    using Badumna.Utilities;
    using Badumna.Utilities.Logging;

    /////// <summary>
    /////// Delegate for creating original wrappers.
    /////// </summary>
    /////// <param name="replica">The replica replicable entity to wrap.</param>
    /////// <returns>A new instance implementing IReplicaWrapper.</returns>
    ////internal delegate IReplicaEntityWrapper ReplicaEntityWrapperFactory(object replica);

    /// <summary>
    /// Interface for replica wrapper to permit mocking.
    /// </summary>
    internal interface IReplicaEntityWrapper : IReplicaWrapper, Replication.IReplica
    {
    }

    /// <summary>
    /// Autoreplication wrapper for replica replicable entities.
    /// Responsible for deserializing state updates and updating the wrapped
    /// entity's replicable properties.
    /// </summary>
    internal class ReplicaEntityWrapper : EntityWrapper, IReplicaEntityWrapper
    {
        /// <summary>
        /// Provides access to current time.
        /// </summary>
        private readonly ITime timeKeeper;

        /// <summary>
        /// Deserializers for replicable properties of the original entity.
        /// </summary>
        private readonly List<IDeserializer> propertyDeserializers = new List<IDeserializer>();

        /// <summary>
        /// Smoothers that require ticking.
        /// </summary>
        private readonly List<ISmoothingDeserializer> smoothers = new List<ISmoothingDeserializer>();

        /// <summary>
        /// Initializes a new instance of the ReplicaEntityWrapper class.
        /// </summary>
        /// <param name="replica">The replica replicable entity to wrap.</param>
        /// <param name="serialization">For creation and serialization of types.</param>
        /// <param name="timeKeeper">Provides access to current time.</param>
        public ReplicaEntityWrapper(
            object replica,
            Serialization.Manager serialization,
            ITime timeKeeper)
            : base(serialization, replica)
        {
            if (timeKeeper == null)
            {
                throw new ArgumentNullException("timeKeeper");
            }

            this.timeKeeper = timeKeeper;
            foreach (var property in this.ReplicableProperties)
            {
                SmoothingAttribute smoothingAttribute;
                if (this.SmoothingAttribtues.TryGetValue(property, out smoothingAttribute))
                {
                    var smoother = this.Serialization.CreateSmoothingPropertyDeserializer(this.Entity, property, smoothingAttribute);
                    this.propertyDeserializers.Add(smoother);
                    this.smoothers.Add(smoother);
                }
                else
                {
                    this.propertyDeserializers.Add(
                        this.Serialization.CreatePropertyDeserializer(this.Entity, property));
                }
            }
        }

        /// <summary>
        /// Update entity position if dead reckoning is being used.
        /// </summary>
        /// <param name="currentTime">The current time.</param>
        public override void Tick(TimeSpan currentTime)
        {
            foreach (var smoother in this.smoothers)
            {
                smoother.Tick(currentTime);
            }
        }

        /// <inheritdoc/>
        public double Deserialize(BooleanArray includedParts, Stream stream, int estimatedMillisecondsSinceDeparture, BadumnaId identifier)
        {
            var reader = new BinaryReader(stream);
            var propertyIndex = (int)SpatialEntityStateSegment.FirstAvailableSegment;
            foreach (var deserializer in this.propertyDeserializers)
            {
                if (includedParts[propertyIndex])
                {
                    deserializer.Deserialize(reader);
                }

                propertyIndex++;
            }

            return 0;
        }
    }
}
