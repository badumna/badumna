﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.IO;
using System.Security.Cryptography;

using Badumna.Core;
using Badumna.Utilities;

namespace Badumna.Security
{
    /// <summary>
    /// A certificate token issued by a trusted authority to be verified against a TrustedAuthorityToken.
    /// </summary>
    /// <exclude/>
    public class CertificateToken : Token, ICertificateToken
    {
        /// <summary>
        /// The length in bits of the users key
        /// </summary>
        public const int KeyLengthBits = PublicKey.SignatureLengthBytes * 8;

        long ICertificateToken.UserId { get { return this.mUserId; } }
        Character ICertificateToken.Character { get { return this.mCharacter; } }

        private long mUserId;
        private Character mCharacter;
        private Key mKey;
        private const int mSignatureLength = 128;

        PublicKey ICertificateToken.PublicKey
        {
            get
            {
                if (this.mKey is PublicKey)
                {
                    return (PublicKey)this.mKey;
                }

                return ((PrivateKey)this.mKey).PublicKey;
            }
        }

        PrivateKey ICertificateToken.PrivateKey
        {
            get
            {
                return this.mKey as PrivateKey;
            }
        }

        private DateTime mTimeStamp;
        DateTime ICertificateToken.TimeStamp { get { return this.mTimeStamp; } }

        private Signature mSignature;

        Signature ICertificateToken.Signature
        {
            get { return this.mSignature; }
        }

        /// <summary>
        /// Constructs a certificate token.
        /// </summary>
        /// <param name="userId">The id of the user to which this certificate is issued.</param>
        /// <param name="character">The character for which this certificate is issued.</param>
        /// <param name="timeStamp">The time this certificate was issued in UTC</param>
        /// <param name="expiryTime">The expiry time of this certificate in UTC</param>
        /// <param name="signature">The signature of this certificate. Signed by the trusted authority.</param>
        /// <param name="privateKey">The crypto service provider for the users private key. The key length must be CertificateToken.KeyLengthBits </param>
        public CertificateToken(long userId, Character character, DateTime timeStamp, DateTime expiryTime, byte[] signature, RSACryptoServiceProvider privateKey)
            : base(expiryTime)
        {
            if (privateKey.KeySize != CertificateToken.KeyLengthBits)
            {
                throw new ArgumentException(String.Format("Private key size is invalid. Must be {0} bits.", CertificateToken.KeyLengthBits));
            }

            this.Init(userId, character, timeStamp, new PrivateKey(privateKey), new Signature(signature));
        }

        internal CertificateToken(long userId, Character character, DateTime timeStamp, DateTime expiryTime, PublicKey publicKey, Signature signature)
            : base(expiryTime)
        {
            this.Init(userId, character, timeStamp, publicKey, signature);
        }

        /// <summary>
        /// Private constructor used only with IParseable.FromMessage
        /// </summary>
        private CertificateToken()
            : base()
        {
        }

        /// <summary>
        /// Gets the character that this token is autorized for.
        /// </summary>
        public Character Character {
            get { return this.mCharacter; }
        }

        /// <inheritdoc/>
        public override string ToString()
        {
            return string.Format("<#DefaultUserCertificate {0}>", Utils.ObjectToString(new List<object> {
                ((ICertificateToken)this).UserId,
                ((ICertificateToken)this).Character,
                ((ICertificateToken)this).TimeStamp,
                ((ICertificateToken)this).Signature,
                ((ICertificateToken)this).PublicKey,
            }));
        }

        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// Note that this does not compare the private key, as that does not survive serialization.
        /// <param name="other">An object to compare with this object.</param>
        /// <returns>
        /// true if the current object is equal to the <paramref name="other"/> parameter; otherwise, false.
        /// </returns>
        bool IEquatable<ICertificateToken>.Equals(ICertificateToken other)
        {
            if (object.ReferenceEquals(other, null))
            {
                return false;
            }

            ICertificateToken ithis = ((ICertificateToken)this);

            return this.mUserId == other.UserId &&
                   (ithis.Character == null ? other.Character == null : ithis.Character.Equals(other.Character)) &&
                   ithis.PublicKey.Equals(other.PublicKey) &&
                   this.mSignature.Equals(other.Signature) &&
                   this.mTimeStamp == other.TimeStamp;
        }

        /// <summary>
        /// Determines whether the specified <see cref="System.Object"/> is equal to this instance.
        /// </summary>
        /// <param name="obj">The <see cref="System.Object"/> to compare with this instance.</param>
        /// <returns>
        /// 	<c>true</c> if the specified <see cref="System.Object"/> is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        /// <exception cref="T:System.NullReferenceException">The <paramref name="obj"/> parameter is null.</exception>
        public override bool Equals(object obj)
        {
            return ((IEquatable<ICertificateToken>)this).Equals(obj as ICertificateToken);
        }

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
        /// </returns>
        public override int GetHashCode()
        {
            return this.mKey.GetHashCode();
        }

        /// <summary>
        /// Indicates whether the users certificate is signed by the trusted authority and not expired.
        /// </summary>
        /// <param name="tokens">The security tokens</param>
        /// <returns>True if the users certificate is valid and false otherwise.</returns>
        bool ICertificateToken.Validate(ITokenCache tokens)
        {
            return this.OnValidate(tokens);
        }
        
        internal virtual bool OnValidate(ITokenCache tokens)
        {
            if (this.IsExpiredAt(tokens.NetworkTime))
            {
                Logger.TraceInformation(LogTag.Security, "Certificate has expired : {0}", this.ExpirationTime);
                return false;
            }
            
            if (this.mSignature == null)
            {

                Logger.TraceInformation(LogTag.Security, "Certificate has no signature.");
                return false;
            }

            TrustedAuthorityToken authorityToken = tokens.GetTrustedAuthorityToken();

            if (authorityToken == null)
            {
                Logger.TraceInformation(LogTag.Security, "Trusted authority token is invalid.");
                return false;
            }

            try
            {
                using (MessageBuffer serializedCertificateBuffer = new MessageBuffer())
                {
                    this.ToMessageWithoutSignature(serializedCertificateBuffer, true);
                    return authorityToken.Key.VerifySignature(serializedCertificateBuffer.GetBuffer(), this.mSignature);
                }
            }
            catch (Exception e)
            {
                Logger.TraceException(LogLevel.Error, e, "Failed to perform verification of user certificate.");
                return false;
            }
        }

        /// <summary>
        /// Create a signature for a certificate with the given properties using the given key.
        /// </summary>
        /// <param name="userId">The id of the user the certificate is for</param>
        /// <param name="character">The character the certificate is for</param>
        /// <param name="timeStamp">The time stamp of the certificate in UTC</param>
        /// <param name="expiryTime">The expiration time of the certificate in UTC</param>
        /// <param name="usersPublicKey">The serialized users public key</param>
        /// <param name="signingPrivateKey">The key used to perform the signature</param>
        /// <returns>The signature</returns>
        public static byte[] SignCertificate(long userId, Character character, DateTime timeStamp, DateTime expiryTime, byte[] usersPublicKey, byte[] signingPrivateKey)
        {
            CertificateToken temporayToken = new CertificateToken(userId, character, timeStamp, expiryTime, new PublicKey(usersPublicKey), new Signature());
            using (MessageBuffer serializedCertificateBuffer = new MessageBuffer())
            {
                PrivateKey key = new PrivateKey(signingPrivateKey);

                temporayToken.ToMessageWithoutSignature(serializedCertificateBuffer, true);

                return key.Sign(serializedCertificateBuffer.GetBuffer());
            }
        }

        #region serialization (IParseable and friends)

        /// <summary>
        /// For internal use we need to include the private key when serializing message
        /// </summary>
        /// (Only when we need someone else to act on our behalf, e.g the HTTP Tunnel)
        /// <param name="buffer">The buffer.</param>
        internal void ToMessageWithPrivateKey(MessageBuffer buffer)
        {
            Debug.Assert(this.mKey is PrivateKey);
            buffer.Write(this);
            buffer.Write(this.mKey.RawData.Length);
            buffer.Write(this.mKey.RawData, this.mKey.RawData.Length);
        }

        /// <summary>
        /// Deserialize a CertificateToken created with ToMessageWithPrivateKey()
        /// </summary>
        /// <param name="buffer">The buffer.</param>
        internal static CertificateToken FromMessageWithPrivateKey(MessageBuffer buffer)
        {
            CertificateToken result = buffer.Read<CertificateToken>();
            int length = buffer.ReadInt();
            result.mKey = new PrivateKey(buffer.ReadBytes(length));
            return result;
        }

        /// <summary>
        /// Serializes everything in this object except for the signature.
        /// </summary>
        /// <param name="buffer">The buffer.</param>
        /// <param name="includeParentFields">An awkward switch determining whether to include
        /// the parent classes fields explicitly, which (depending on the code path) may have
        /// already been serialized.</param>
        private void ToMessageWithoutSignature(MessageBuffer buffer, bool includeParentFields)
        {
            if (includeParentFields)
            {
                this.CommonTokenFieldsToMessage(buffer);
            }
            buffer.Write(((ICertificateToken)this).PublicKey);
            buffer.Write(this.mTimeStamp.Ticks);
            buffer.Write(this.mUserId);
            bool hasCharacter = this.mCharacter != null;

            buffer.Write(hasCharacter);
            if (hasCharacter)
            {
                buffer.Write(this.mCharacter);
            }
        }

        /// <inheritdoc/>
        internal override void ToMessageOverride(MessageBuffer buffer)
        {
            this.ToMessageWithoutSignature(buffer, false);
            buffer.Write(this.mSignature);
        }

        /// <inheritdoc/>
        internal override void FromMessageOverride(MessageBuffer buffer)
        {
            PublicKey publicKey = buffer.Read<PublicKey>();
            DateTime timeStamp = new DateTime(buffer.ReadLong());
            long userId = buffer.ReadLong();

            bool hasCharacter = buffer.ReadBool();
            Character character = null;
            if (hasCharacter)
            {
                character = buffer.Read<Character>();
            }

            Signature signature = buffer.Read<Signature>();
            this.Init(userId, character, timeStamp, publicKey, signature);
        }
        
        #endregion // serialization

        /// <summary>
        /// Initialize this object's fields. Extracted into a method to share between constructors and FromMessage()
        /// </summary>
        /// <param name="userId">The user ID</param>
        /// <param name="character">The character</param>
        /// <param name="timeStamp">The timestamp</param>
        /// <param name="key">The Key (PublicKey or PrivteKey)</param>
        /// <param name="signature">The signature</param>
        private void Init(long userId, Character character, DateTime timeStamp, Key key, Signature signature)
        {
            if (key == null)
            {
                throw new ArgumentNullException("key");
            }

            if (signature == null)
            {
                throw new ArgumentNullException("signature");
            }

            this.mUserId = userId;
            this.mCharacter = character;
            this.mTimeStamp = timeStamp;
            this.mSignature = signature;
            this.mKey = key;
        }
    }
}
