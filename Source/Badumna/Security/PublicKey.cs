﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Security.Cryptography;

using Badumna.Core;
using Badumna.Utilities;

namespace Badumna.Security
{
    class PublicKey : Key, IParseable
    {
        public static PublicKey GenerateKey()
        {
            PublicKey key = new PublicKey();
            key.Generate();
            return key;
        }

        private PublicKey() // For IParseable
            : base(false)
        {
        }

        public PublicKey(byte[] rawData)
            : base(rawData, false)
        {
        }
        
        public PublicKey(String xmlString)
            :base (xmlString, false)
        {
        }

        #region IParseable Members

        public void ToMessage(MessageBuffer message, Type parameterType)
        {
            if (message != null)
            {
                message.Write(this.RawData, PublicKey.KeyLengthBytes + 20);
            }
        }

        public void FromMessage(MessageBuffer message)
        {
            if (message != null)
            {
                this.mRawData = message.ReadBytes(PublicKey.KeyLengthBytes + 20);
                this.mRsaService = new RSACryptoServiceProvider(PublicKey.KeyLengthBytes * 8);
                this.mRsaService.ImportCspBlob(this.RawData);
            }
        }

        #endregion
    }
}
