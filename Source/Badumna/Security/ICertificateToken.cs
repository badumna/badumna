﻿//-----------------------------------------------------------------------
// <copyright file="ICertificateToken.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2012 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using Badumna.Core;

namespace Badumna.Security
{
    /// <summary>
    /// Internal interface for a CertificateToken
    /// </summary>
    internal interface ICertificateToken : IParseable, IEquatable<ICertificateToken>
    {
        /// <summary>
        /// Gets the public key.
        /// </summary>
        PublicKey PublicKey { get; }

        /// <summary>
        /// Gets the private key.
        /// </summary>
        PrivateKey PrivateKey { get; }

        /// <summary>
        /// Gets the signature.
        /// </summary>
        Signature Signature { get; }

        /// <summary>
        /// Gets the TimeStamp.
        /// </summary>
        DateTime TimeStamp { get; }

        /// <summary>
        /// Gets the user id.
        /// </summary>
        long UserId { get; }

        /// <summary>
        /// Gets the character.
        /// </summary>
        Character Character { get; }

        /// <summary>
        /// Validates the specified tokens.
        /// </summary>
        /// <param name="tokens">The tokens.</param>
        /// <returns>Whether the token is valid.</returns>
        bool Validate(ITokenCache tokens);

        /// <summary>
        /// Determines whether this token is expired at the specified network time.
        /// </summary>
        /// <param name="networkTime">The network time.</param>
        /// <returns>whether this token is expired at the specified network time.</returns>
        bool IsExpiredAt(DateTime networkTime);
    }
}
