﻿using System;
using System.Collections.Generic;
using System.Text;

using Badumna.Core;
using Badumna.Utilities;
using Badumna.Utilities.Logging;

namespace Badumna.Security
{
    /// <summary>
    /// Caches authorization tokens.  WARNING: This makes no attempts to be secure with its memory usage.
    /// </summary>
    class TokenCache : ITokenCache
    {
        private IIdentityProvider mIdentityProvider;

        private Dictionary<TokenType, TokenMonitor> mTokenMonitors = new Dictionary<TokenType, TokenMonitor>();

        /// <summary>
        /// A value indicating whether the Dei IdentityProvider is used.
        /// </summary>
        private bool isIdentityVerified;

        /// <summary>
        /// The event queue to schedule events on.
        /// </summary>
        private NetworkEventQueue eventQueue;

        /// <summary>
        /// Reports on the network connectivity status.
        /// </summary>
        private INetworkConnectivityReporter connectivityReporter;

        /// <summary>
        /// Provides an estimate of the current network time.
        /// </summary>
        private Clock networkTime;

        /// <summary>
        /// Initializes a new instance of the TokenCache class which will schedule a shutdown if certain tokens cannot be loaded.
        /// </summary>
        /// <param name="eventQueue">The event queue to schedule events on</param>
        /// <param name="timeKeeper">Provides the current time</param>
        /// <param name="connectivityReporter">Reports on the network connectivity status</param>
        public TokenCache(NetworkEventQueue eventQueue, ITime timeKeeper, INetworkConnectivityReporter connectivityReporter)
        {
            this.eventQueue = eventQueue;
            this.connectivityReporter = connectivityReporter;

            this.networkTime = new Clock(eventQueue, delegate { return timeKeeper.Now; });

            foreach (TokenType tokenType in Enum.GetValues(typeof(TokenType)))
            {
                this.mTokenMonitors.Add(tokenType, new TokenMonitor(tokenType, eventQueue, this.networkTime));
            }
        }
        
        /// <inheritdoc/>
        public DateTime NetworkTime
        {
            get { return this.networkTime.Now; }
        }

        /// <summary>
        /// Gets a value indicating whether the Dei Identity Provider is used.
        /// </summary>
        internal bool IsIdentityVerified
        {
            get { return this.isIdentityVerified; }
        }

        internal Character Character
        {
            get {
                var certificate = this.GetUserCertificateToken();
                return certificate == null ? null : certificate.Character;
           }
        }

        public void SetIdentityProvider(IIdentityProvider identityProvider)
        {
            if (identityProvider == null)
            {
                throw new ArgumentNullException("identityProvider");
            }

            this.InvalidateAll();
            identityProvider.Activate();
            this.mIdentityProvider = new SafeIdentityProvider(identityProvider);

            this.isIdentityVerified = !(identityProvider is UnverifiedIdentityProvider);

            foreach (TokenMonitor monitor in this.mTokenMonitors.Values)
            {
                monitor.SetIdentityProvider(this.mIdentityProvider);
            }
        }

        public bool PreCache()
        {
            if (this.mIdentityProvider == null)
            {
                return false;
            }

            try
            {
                // Order is important
                if (!this.mTokenMonitors[TokenType.NetworkTime].CacheAndMonitor())
                {
                    Logger.TraceInformation(LogTag.Security, "Unable to pre-cache time token");
                    return false;
                }

                if (!this.mTokenMonitors[TokenType.NetworkParticipatation].CacheAndMonitor())
                {
                    Logger.TraceInformation(LogTag.Security, "Unable to pre-cache network participation token");
                    return false;
                }

                if (!this.mTokenMonitors[TokenType.TrustedAuthority].CacheAndMonitor())
                {
                    Logger.TraceInformation(LogTag.Security, "Unable to pre-cache trusted authority token");
                    return false;
                }

                if (!this.mTokenMonitors[TokenType.PermissionList].CacheAndMonitor())
                {
                    Logger.TraceInformation(LogTag.Security, "Unable to pre-cache permission list token");
                    return false;
                }

                if (!this.mTokenMonitors[TokenType.UserCertificate].CacheAndMonitor())
                {
                    Logger.TraceInformation(LogTag.Security, "Unable to pre-cache certificate token");
                    return false;
                }
            }
            catch (Exception e)
            {
                Logger.TraceException(LogLevel.Error, e, "Failed to pre-cache tokens.");
                return false;
            }

            return true;
        }

        public T Get<T>(TokenType type) where T : Token
        {
            if (this.mIdentityProvider == null) 
            {
                Logger.TraceInformation(LogTag.Crypto | LogTag.Event, "Unable to get token of type {0}: IdentityProvider is null", type);
                return null;
            }

            TokenMonitor monitor;
            if (this.mTokenMonitors.TryGetValue(type, out monitor))
            {
                return (T)monitor.GetToken();
            }

            Logger.TraceInformation(LogTag.Crypto | LogTag.Event, "Unable to get token of type {0}: type not found in token monitors", type);
            return null;
        }

        public SymmetricKeyToken GetNetworkParticipationToken()
        {
            SymmetricKeyToken token = this.Get<SymmetricKeyToken>(TokenType.NetworkParticipatation);

            // TODO: This check seems really hacky (as in, attempting to retrive a token may have unexpected side-effect of shutting everything down), review it.
            if (token == null && this.connectivityReporter != null && this.connectivityReporter.Status == ConnectivityStatus.Online)
            {
                Logger.TraceInformation(LogTag.Security, "Failed to obtain a participation token. Shutting down.");
                this.ScheduleOffline();
            }

            return token;
        }

        public ICertificateToken GetUserCertificateToken()
        {
            CertificateToken token = this.Get<CertificateToken>(TokenType.UserCertificate);

            // TODO: This check seems really hacky (as in, attempting to retrive a token may have unexpected side-effect of shutting everything down), review it.
            if (token == null && this.connectivityReporter != null && this.connectivityReporter.Status == ConnectivityStatus.Online)
            {
                Logger.TraceInformation(LogTag.Security, "Failed to obtain a certificate token. Shutting down.");
                this.ScheduleOffline();
            }

            return token;
        }

        public PermissionListToken GetPermissionListToken()
        {
            return this.Get<PermissionListToken>(TokenType.PermissionList);
        }

        public Permission GetPermission(PermissionType permissionType)
        {
            PermissionListToken permissions = this.GetPermissionListToken();
            if (permissions == null)
            {
                return null;
            }

            return permissions.GetPermissionState(permissionType);
        }

        public PrivateKey GetPermissionKeyPair()
        {
            PermissionListToken permissions = this.GetPermissionListToken();
            if (permissions == null)
            {
                return null;
            }

            return permissions.UsersKeyPair;
        }

        /// <inheritdoc/>
        public TrustedAuthorityToken GetTrustedAuthorityToken()
        {
            TrustedAuthorityToken token = this.Get<TrustedAuthorityToken>(TokenType.TrustedAuthority);

            // TODO: This check seems really hacky (as in, attempting to retrive a token may have unexpected side-effect of shutting everything down), review it.
            if (token == null && this.connectivityReporter != null && this.connectivityReporter.Status == ConnectivityStatus.Online)
            {
                Logger.TraceInformation(LogTag.Security, "Failed to obtain a trusted authority token. Shutting down.");
                this.ScheduleOffline();
            }

            return token;
        }

        public void SubscribeToExpireEvent(TokenType type, EventHandler<TokenExpirationEventArgs> handler)
        {
            TokenMonitor monitor = null;

            if (this.mTokenMonitors.TryGetValue(type, out monitor))
            {
                monitor.TokenExpiredEvent += handler;
            }
        }

        public void ForceUpdate(TokenType type)
        {
            TokenMonitor monitor = null;

            if (this.mTokenMonitors.TryGetValue(type, out monitor))
            {
                monitor.ForceUpdate();
            }
        }

        public void InvalidateAll()
        {
            foreach (TokenMonitor monitor in this.mTokenMonitors.Values)
            {
                monitor.Invalidate();
            }

        }

        public void Dispose()
        {
            if (this.mIdentityProvider != null)
            {
                this.mIdentityProvider.Dispose();
            }
        }

        private void ScheduleOffline()
        {
            if (this.connectivityReporter != null && this.connectivityReporter.Status == ConnectivityStatus.Online)
            {
                this.eventQueue.Push(delegate() { this.connectivityReporter.SetStatus(ConnectivityStatus.ShuttingDown); });
                this.eventQueue.Schedule(1000, delegate() { this.connectivityReporter.SetStatus(ConnectivityStatus.Offline); });
            }
        }

        class SafeIdentityProvider : IIdentityProvider
        {
            private readonly object mSessionLock = new object();
            private IIdentityProvider mIdentityProvider;

            public SafeIdentityProvider(IIdentityProvider identityProvider)
            {
                if (identityProvider == null) throw new ArgumentNullException("identityProvider");

                this.mIdentityProvider = identityProvider;
            }

            #region IIdentityProvider Members

            public SymmetricKeyToken GetNetworkParticipationToken()
            {
                lock (this.mSessionLock)
                {
                    return this.mIdentityProvider.GetNetworkParticipationToken();
                }
            }

            public CertificateToken GetUserCertificateToken()
            {
                lock (this.mSessionLock)
                {
                    return this.mIdentityProvider.GetUserCertificateToken();
                }
            }

            public TrustedAuthorityToken GetTrustedAuthorityToken()
            {
                lock (this.mSessionLock)
                {
                    return this.mIdentityProvider.GetTrustedAuthorityToken();
                }
            }

            public PermissionListToken GetPermissionListToken()
            {
                lock (this.mSessionLock)
                {
                    return this.mIdentityProvider.GetPermissionListToken();
                }
            }

            public TimeToken GetTimeToken()
            {
                lock (this.mSessionLock)
                {
                    return this.mIdentityProvider.GetTimeToken();
                }
            }

            public void Activate()
            {
                lock (this.mSessionLock)
                {
                    this.mIdentityProvider.Activate();
                }
            }

            public void Dispose()
            {
                lock (this.mSessionLock)
                {
                    this.mIdentityProvider.Dispose();
                }
            }

            #endregion
        }
    }  
}
