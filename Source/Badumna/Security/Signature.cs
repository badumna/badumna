﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;

using Badumna.Core;

namespace Badumna.Security
{
    /// <summary>
    /// An IParseable representation of a signature
    /// </summary>
    class Signature : IParseable, IEquatable<Signature>
    {
        private byte[] mRawData = new byte[PublicKey.SignatureLengthBytes];
        public byte[] RawData { get { return this.mRawData; } }

        public Signature()
        { }

        public Signature(byte[] rawData)
        {
            Debug.Assert(rawData.Length == PublicKey.SignatureLengthBytes, "Invalid signature length");
            this.mRawData = rawData;
        }

        public Signature(PrivateKey keyPair, params object[] args)
        {
            this.mRawData = keyPair.Sign(this.SerializeArguments(args));
            Debug.Assert(this.mRawData.Length == PublicKey.SignatureLengthBytes, "Invalid signature length");
        }

        /// <summary>
        /// Verify that this signature was signed by the owner of the given public key, using the 
        /// serialized values of the given arguments as the signature content
        /// </summary>
        /// <param name="key"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        public bool Verify(PublicKey key, params object[] args)
        {
            return key.VerifySignature(this.SerializeArguments(args), this.mRawData);
        }

        /// <summary>
        /// Verify that this signature was signed by the owner of the given permission using the serialized
        /// value of the given arguments as the content. Note: the permission must be verfied seperately.
        /// </summary>
        /// <param name="permission"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        public bool Verify(Permission permission, params object[] args)
        {
            return permission.UsersPublicKey.VerifySignature(this.SerializeArguments(args), this.mRawData);
        }

        private byte[] SerializeArguments(object[] args)
        {
            using (MessageBuffer message = new MessageBuffer())
            {
                foreach (object arg in args)
                {
                    message.PutObject(arg, arg.GetType());
                }

                return message.GetBuffer();
            }
        }

        #region IParseable Members

        public void ToMessage(MessageBuffer message, Type parameterType)
        {
            if (message != null)
            {
                message.Write(this.mRawData, PublicKey.SignatureLengthBytes);
            }
        }

        public void FromMessage(MessageBuffer message)
        {
            if (message != null)
            {
                this.mRawData = message.ReadBytes(PublicKey.SignatureLengthBytes);
            }
        }

        #endregion

        public bool Equals(Signature other)
        {
            if (other == null)
            {
                return false;
            }

            if (this.mRawData.Length != other.mRawData.Length)
            {
                return false;
            }

            for (int i = 0; i < this.mRawData.Length; i++)
            {
                if (this.mRawData[i] != other.mRawData[i])
                {
                    return false;
                }
            }

            return true;
        }

        public static bool operator !=(Signature a, Signature b)
        {
            return !(a == b);
        }

        public static bool operator ==(Signature a, Signature b)
        {
            bool result;
            if (object.Equals(null, a))
            {
                result = object.Equals(null, b);
            }
            else
            {
                result = a.Equals(b);
            }

            return result;
        }

        public override bool Equals(object obj)
        {
            return this.Equals(obj as Signature);
        }

        public override int GetHashCode()
        {
            return BitConverter.ToInt32(this.mRawData, 0);
        }
    }
}
