﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

using Badumna.Utilities;
using Badumna.Core;

namespace Badumna.Security
{
    /// <summary>
    /// The base class for all security tokens issued by an identity provider.
    /// <see cref="IIdentityProvider"/>
    /// </summary>
    /// <remarks>Must be thread safe!</remarks>
    /// <exclude/>
    public abstract class Token : IParseable
    {
        /// <summary>
        /// Expiry time.  Stored in UTC for serialization.
        /// </summary>
        virtual internal DateTime ExpirationTime { get; private set; }

        /// <summary>
        /// Token constructor.
        /// </summary>
        /// <param name="expiryTime">The time when the token expires</param>
        public Token(DateTime expiryTime)
        {
            this.ExpirationTime = expiryTime;
        }

        /// <summary>
        /// Protected Token constructor, to be used only for IParseable construction.
        /// </summary>
        protected Token()
        {
        }

        /// <summary>
        /// Set this instance's ExpirationTime field
        /// </summary>
        /// <param name="ticks">Expiry time (ticks)</param>
        private void SetExpirationTime(long ticks)
        {
            this.ExpirationTime = new DateTime(ticks, DateTimeKind.Utc);
        }

        /// <summary>
        /// Returns a value indicating whether this permission will be expired at the given network time.
        /// </summary>
        /// <param name="networkTime">The network time to check against.</param>
        /// <returns>True iff the permission will be expired at networkTime.</returns>
        public bool IsExpiredAt(DateTime networkTime)
        {
            return networkTime >= this.ExpirationTime;
        }

        /// <summary>
        /// Serializes the token to the given binary writer.
        /// </summary>
        /// <param name="writer">The writer.</param>
        //// This method only exists because it's part of the public API and MessageBuffer is internal.
        //// Is there a better way than serializing to one type of buffer and then moving it to another?
        //// Do we need this functionality to be public?
        public void Serialize(BinaryWriter writer)
        {
            using (MessageBuffer buffer = new MessageBuffer())
            {
                ((IParseable)this).ToMessage(buffer, typeof(Token));
                writer.Write(buffer.GetBuffer(), 0, buffer.Length);
            }
        }

        /// <summary>
        /// Derived type specialization of <see cref="IParseable.ToMessage"/>
        /// </summary>
        /// <param name="buffer"></param>
        internal abstract void ToMessageOverride(MessageBuffer buffer);

        /// <summary>
        /// Derived type specialization of <see cref="IParseable.FromMessage"/>
        /// </summary>
        /// <param name="buffer"></param>
        internal abstract void FromMessageOverride(MessageBuffer buffer);

        /// <summary>
        /// Serializes the token to the given message buffer.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="type"></param>
        void IParseable.ToMessage(MessageBuffer message, Type type)
        {
            this.CommonTokenFieldsToMessage(message);
            this.ToMessageOverride(message);
        }

        /// <summary>
        /// Write the common fields for this base class to the given MessageBuffer.
        /// </summary>
        /// <remarks>
        /// Explicitly exposed for the use of subclasses who need to serialize themselves
        /// outside of ToMessageOverride()
        /// </remarks>
        internal void CommonTokenFieldsToMessage(MessageBuffer message)
        {
            message.Write(this.ExpirationTime.Ticks);
        }

        /// <summary>
        /// Construct this instance from a message buffer.
        /// </summary>
        /// <param name="message"></param>
        void IParseable.FromMessage(MessageBuffer message)
        {
            this.SetExpirationTime(message.ReadLong());
            this.FromMessageOverride(message);
        }
    }
}
