﻿//-----------------------------------------------------------------------
// <copyright file="ITokenCache.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2012 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;

namespace Badumna.Security
{
    /// <summary>
    /// An interface for TokenCache
    /// </summary>
    internal interface ITokenCache
    {
        /// <summary>
        /// Gets an estimate of the current network time.  `PreCache` must have been called successfully before any access to this property.
        /// </summary>
        DateTime NetworkTime { get; }

        /// <summary>
        /// Gets the trusted authority token.
        /// </summary>
        /// <returns>The TrustedAuthorityToken.</returns>
        TrustedAuthorityToken GetTrustedAuthorityToken();

        /// <summary>
        /// Gets the user's certificate token.
        /// </summary>
        /// <returns>The CertificateToken</returns>
        ICertificateToken GetUserCertificateToken();

        /// <summary>
        /// Subscribes to the expire event for the given token type.
        /// </summary>
        /// <param name="type">The token type.</param>
        /// <param name="handler">The expiration handler.</param>
        void SubscribeToExpireEvent(TokenType type, EventHandler<TokenExpirationEventArgs> handler);
    }
}
