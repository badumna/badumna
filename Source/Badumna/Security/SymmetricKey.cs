﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Security.Cryptography;
using Badumna.Core;
using Badumna.Utilities;

namespace Badumna.Security
{
    class SymmetricKey : IEquatable<SymmetricKey>
    {
        // RC2 used for it's small block size and speed, not for its security...
        private RC2 mEncryptorRC2; // We have two RC2's so that there is no thread contention between decrypt and encrypt
        private RC2 mDecryptorRC2;

        public const int KeyLengthBytes = 8;

        /// <summary>
        /// The ICryptoTransform object used to encrypt data.
        /// </summary>
        private ICryptoTransform encryptorTransform;

        /// <summary>
        /// When the ICryptoTransform object for encryption is created.
        /// </summary>
        private DateTime encryptorCreateTime;

        /// <summary>
        /// The ICryptoTransform object used to decrypt data.
        /// </summary>
        private ICryptoTransform decryptorTransform;

        /// <summary>
        /// The initialization vector used for the current ICryptoTransform object for decryption purpose. 
        /// </summary>
        private byte[] decryptorIV;

        private byte[] mKey = new byte[SymmetricKey.KeyLengthBytes];
        public byte[] Key { get { return this.mKey; } }

        public SymmetricKey()
        {            
            byte[] key = new byte[SymmetricKey.KeyLengthBytes];
            Utilities.RandomSource.Generator.NextBytes(key);
            this.Initialize(key);
        }

        public SymmetricKey(byte[] key)
        {
            this.Initialize(key);
        }

        private void Initialize(byte[] key)
        {
            this.CreateCipher(key);
            Array.Copy(key, this.mKey, SymmetricKey.KeyLengthBytes);
        }

        public bool Equals(SymmetricKey other)
        {
            if (other == null)
            {
                return false;
            }

            if (this.mKey.Length != other.mKey.Length)
            {
                return false;
            }

            for (int i = 0; i < this.mKey.Length; i++)
            {
                if (this.mKey[i] != other.mKey[i])
                {
                    return false;
                }
            }

            return true;
        }

        public override bool Equals(object obj)
        {
            return this.Equals(obj as SymmetricKey);
        }

        public override int GetHashCode()
        {
            return this.mKey[0] ^ this.mKey[1];
        }

        private void CreateCipher(byte[] key)
        {
            this.mEncryptorRC2 = RC2.Create();
            lock (this.mEncryptorRC2)
            {

                this.mEncryptorRC2.Key = key;
                this.mEncryptorRC2.Mode = CipherMode.CBC;
            }

            this.mDecryptorRC2 = RC2.Create();
            lock (this.mDecryptorRC2)
            {
                this.mDecryptorRC2.Key = key;
                this.mDecryptorRC2.Mode = CipherMode.CBC;
            }
        }

        internal byte[] Encrypt(byte[] toEncrypt)
        {
            int length;
            byte[] encrypted;
            byte[] result;

            lock (this.mEncryptorRC2)
            {
                this.UpdateEncryptorTransform();
                encrypted = this.encryptorTransform.TransformFinalBlock(toEncrypt, 0, toEncrypt.Length);
                result = new byte[this.mEncryptorRC2.IV.Length + encrypted.Length];
                Array.Copy(this.mEncryptorRC2.IV, 0, result, 0, this.mEncryptorRC2.IV.Length);  // TODO: Remove the need for these array copies
                length = this.mEncryptorRC2.IV.Length;
            }

            Array.Copy(encrypted, 0, result, length, encrypted.Length);
            return result;
        }

        internal byte[] Decrypt(byte[] toDecrypt)
        {
            lock (this.mDecryptorRC2)
            {
                this.UpdateDecryptorTransform(toDecrypt);
                int length = this.mDecryptorRC2.BlockSize / 8;
                return this.decryptorTransform.TransformFinalBlock(toDecrypt, length, toDecrypt.Length - length);
            }
        }

        /// <summary>
        /// Updates the encryptor transform. The encryptor is updated with a different IV every few seconds.
        /// </summary>
        private void UpdateEncryptorTransform()
        {
            if (!this.CanReuseEncryptorTransform())
            {
                this.encryptorCreateTime = DateTime.Now;
                this.mEncryptorRC2.GenerateIV();
                
                if (this.encryptorTransform != null)
                {
                    this.encryptorTransform.Dispose();
                }
                
                this.encryptorTransform = this.mEncryptorRC2.CreateEncryptor();
            }
        }

        /// <summary>
        /// Updates the decryptor transform. The decryptor is updated if the sender has applied a different IV. 
        /// </summary>
        /// <param name="toDecrypt">To decrypt.</param>
        private void UpdateDecryptorTransform(byte[] toDecrypt)
        {
            if (!this.CanReuseDecryptorTransform(toDecrypt))
            {
                if (this.decryptorTransform != null)
                {
                    this.decryptorTransform.Dispose();
                }

                byte[] iv = new byte[this.mDecryptorRC2.BlockSize / 8];
                Array.Copy(toDecrypt, iv, iv.Length);
                this.mDecryptorRC2.IV = iv;
                this.decryptorIV = iv;

                this.decryptorTransform = this.mDecryptorRC2.CreateDecryptor();
            }
        }

        /// <summary>
        /// Determines whether the current encryptor transform object can be reused..
        /// </summary>
        /// <returns>
        /// <c>true</c> if can be reused; otherwise, <c>false</c>.
        /// </returns>
        private bool CanReuseEncryptorTransform()
        {
            if (this.encryptorTransform == null)
            {
                return false;
            }

            if (DateTime.Now - this.encryptorCreateTime > TimeSpan.FromSeconds(10))
            {
                return false;
            }

            // on Mono 1.2.5, this always seems to be false 
            // possible related Mono bug report: http://www.mail-archive.com/mono-bugs@lists.ximian.com/msg44987.html
            return this.encryptorTransform.CanReuseTransform;
        }

        /// <summary>
        /// Determines whether the current dncryptor transform object can be reused..
        /// </summary>
        /// <param name="toDecrypt">The message waiting to be decrypted.</param>
        /// <returns>
        /// <c>true</c> if can be reused; otherwise, <c>false</c>.
        /// </returns>
        private bool CanReuseDecryptorTransform(byte[] toDecrypt)
        {
            if (this.decryptorTransform == null)
            {
                return false;
            }

            if (!this.decryptorTransform.CanReuseTransform)
            {
                return false;
            }

            if (this.decryptorIV != null && ArrayEqual(this.decryptorIV, toDecrypt, this.mDecryptorRC2.BlockSize / 8))
            {
                return true;
            }

            return false;
        }

        private static bool ArrayEqual(byte[] array1, byte[] array2, int length)
        {
            if (array1 == null || array2 == null)
            {
                throw new ArgumentNullException();
            }

            if (array1.Length < length || array2.Length < length)
            {
                throw new ArgumentOutOfRangeException();
            }

            for (int i = 0; i < length; i++)
            {
                if (array1[i] != array2[i])
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Cleanup internal data.
        /// </summary>
        public void Dispose()
        {
            lock (this.mEncryptorRC2)
            {
                this.mEncryptorRC2.Clear();
            }

            lock (this.mDecryptorRC2)
            {
                this.mDecryptorRC2.Clear();
            }

            this.mEncryptorRC2 = null;
            this.mDecryptorRC2 = null;
        }
    }
}
