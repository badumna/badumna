﻿using System;
using System.Collections.Generic;
using System.Text;

using System.Security.Cryptography;

namespace Badumna.Security
{
    public class PublicKeyToken : Token
    {
        private RSACryptoServiceProvider mPublicKey;

        public PublicKeyToken(TimeSpan validityPeriod, RSACryptoServiceProvider publicKey)
            : base(validityPeriod)
        {
            this.mPublicKey = publicKey;
        }



    }
}
