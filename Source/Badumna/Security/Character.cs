﻿//-----------------------------------------------------------------------
// <copyright file="Character.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2012 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using Badumna.Core;
using Badumna.Utilities;

namespace Badumna.Security
{
    /// <summary>
    /// <para>
    /// A character is the externally-visible identity of a logged in user. It consists of a name and a persistent ID.
    /// When users interact with each other using character-based services (such as private chat), each participant
    /// is represented by a Character.
    /// </para>
    /// <para>
    /// A user can have any number of characters, but acts as only one character at a time (selected at login).
    /// If an application makes use of a Dei server for secure login,
    /// character names and IDs are guaranteed to be unique, and character ownership is verified when communicating
    /// with peers (i.e. a user cannot masquerade as someone else's character).
    /// </para>
    /// <para>
    /// If an application uses the <see cref="Badumna.Security.UnverifiedIdentityProvider"/> class for login to the
    /// badumna network, no guarantees are made about the uniqueness or validity of character names and IDs.
    /// </para>
    /// </summary>
    public sealed class Character : IEquatable<Character>, IParseable
    {
        /// <summary>
        /// The null character, which can be used by servers and other peers
        /// which do not need to use any character-related services.
        /// </summary>
        public static readonly Character None = null;

        /// <summary>
        /// Initializes a new instance of the <see cref="Character"/> class.
        /// Not indended to be called by user code, used only for deserialization.
        /// </summary>
        public Character()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Character"/> class.
        /// </summary>
        /// <param name="id">The character id.</param>
        /// <param name="name">The character name.</param>
        public Character(long id, string name)
        {
            this.Id = id;
            this.Name = name;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Character"/> class.
        /// This character will not be considered valid, so can only be used for
        /// non-authenticated interactions.
        /// </summary>
        /// <param name="name">The character name.</param>
        internal Character(string name)
            : this(-1, name)
        {
        }

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        /// <remarks>This property should never be set manually, it is used by XML Deserialization</remarks>
        public long Id { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <remarks>This property should never be set manually, it is used by XML Deserialization</remarks>
        public string Name { get; set; }

        /// <summary>
        /// Gets a value indicating whether this instance is valid.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is valid; otherwise, <c>false</c>.
        /// </value>
        public bool IsValid
        {
            get
            {
                return !(this.Id < 0 || string.IsNullOrEmpty(this.Name));
            }
        }

        /// <inheritdoc/>
        public static bool operator ==(Character a, Character b)
        {
            bool result;
            if (object.Equals(null, a))
            {
                result = object.Equals(null, b);
            }
            else
            {
                result = a.Equals(b);
            }

            return result;
        }

        /// <inheritdoc/>
        public static bool operator !=(Character a, Character b)
        {
            return !(a == b);
        }

        /// <inheritdoc/>
        public bool Equals(Character other)
        {
            return other != null
                && this.Id == other.Id
                && this.Name == other.Name
                && this.IsValid == other.IsValid;
        }

        /// <inheritdoc/>
        public override bool Equals(object obj)
        {
            return this.Equals(obj as Character);
        }

        /// <inheritdoc/>
        public override int GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <inheritdoc/>
        void IParseable.ToMessage(MessageBuffer buffer, Type type)
        {
            buffer.Write(this.Id);
            buffer.Write(this.Name);
        }

        /// <inheritdoc/>
        void IParseable.FromMessage(MessageBuffer buffer)
        {
            this.Id = buffer.ReadLong();
            this.Name = buffer.ReadString();
        }

        /// <inheritdoc/>
        public override string ToString()
        {
            return string.Format("<#Character {0}: {1}>", this.Id, this.Name);
        }
    }
}
