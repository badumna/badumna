﻿using System;
using System.Collections.Generic;
using System.Text;

using Badumna.Core;
using Badumna.Utilities;

namespace Badumna.Security
{
    enum PermissionType : int
    {
        None = 0,
        Administrator = 1,          // Add/remove permissions
        IniatiateDiagnostics = 2,   // Able to request a remote peers to turn on diagnostics and connect to a given address
        UpdateComplaintProxy = 3,   // Able to specify the address to which peers should forward complaints.
        Blacklist = 4,              // Able to black list users
        Announce = 8,               // Able to announce service
    }

    // ITokenState serialization serializes only state (expiry time, username and permission type). This 
    // is the content whose digest is signed. This is used by the external identity provider to send and receive
    // the state prior to it being signed.
    // IPareseable serialization includes the signature as well the expiry time, the other state should be known 
    // prior to verfication by the remote peer. This serialization is used to share the state of the permission that
    // is not known and constitutes the permission proof.
    class Permission : /*ITokenState,*/ IParseable, IEquatable<Permission>
    {
        // TODO : Add timestamp 
        private DateTime mExpiryTime = DateTime.MinValue;
        private PermissionType mType = PermissionType.None;
        private PublicKey mUsersPublicKey;
        private byte[] mSignature;

        public DateTime ExpiryTime { get { return this.mExpiryTime; } }
        public byte[] Signature { set { this.mSignature = value; } }
        public PermissionType Type { get { return this.mType; } }
        public PublicKey UsersPublicKey 
        { 
            get { return this.mUsersPublicKey; }
            set { this.mUsersPublicKey = value; }
        }

        public Permission() {} // For IParseable

        public Permission(DateTime expiryTime, PermissionType type, PublicKey usersPublicKey)
        {
            this.mExpiryTime = expiryTime;
            this.mType = type;
            this.mUsersPublicKey = usersPublicKey;
        }

        /// <summary>
        /// Returns a value indicating whether this permission will be expired at the given network time.
        /// </summary>
        /// <param name="networkTime">The network time to check against.</param>
        /// <returns>True iff the permission will be expired at networkTime.</returns>
        public bool IsExpiredAt(DateTime networkTime)
        {
            return networkTime >= this.mExpiryTime;
        }

        /// <summary>
        /// Verifies that the given permission has been granted by the trusted authority.
        /// </summary>
        /// <param name="type">The permission that has been granted</param>
        /// <param name="tokens">The security tokens</param>
        /// <returns>True iff the permission has a valid signature and has not expired.</returns>
        public bool Verify(PermissionType type, TokenCache tokens)
        {
            if (this.mSignature == null || this.IsExpiredAt(tokens.NetworkTime))
            {
                return false;
            }

            // wtf?  But signature covers the type, so if sets to wrong type sig check will fail.
            this.mType = type;

            return tokens.GetTrustedAuthorityToken().Key.VerifySignature(this.Serialize(), this.mSignature);
        }
        
        #region ITokenState Members

        public byte[] Serialize()
        {
            // TODO: Unmagic

            byte[] result = new byte[160];

            Array.Copy(BitConverter.GetBytes(this.mExpiryTime.Ticks), 0, result, 0, 8);
            Array.Copy(BitConverter.GetBytes((int)this.mType), 0, result, 8, 4);
            Array.Copy(this.mUsersPublicKey.RawData, 0, result, 12, 148);

            return result;
        }

        public void Deserialize(byte[] data)
        {
            this.mExpiryTime = new DateTime(BitConverter.ToInt64(data, 0));
            this.mType = (PermissionType)BitConverter.ToInt32(data, 8);

            byte[] key = new byte[148];
            Array.Copy(data, 12, key, 0, 148);
            this.mUsersPublicKey = new PublicKey(key);
        }

        #endregion
        
        #region IParseable Members

        public void ToMessage(MessageBuffer message, Type parameterType)
        {
            if (message != null)
            {
                message.Write(this.mExpiryTime.Ticks);
                message.Write(this.UsersPublicKey);
                message.Write(this.mSignature, PublicKey.SignatureLengthBytes);
            }
        }

        public void FromMessage(MessageBuffer message)
        {
            if (message != null)
            {
                this.mExpiryTime = new DateTime(message.ReadLong());
                this.mUsersPublicKey = message.Read<PublicKey>();
                this.mSignature = message.ReadBytes(PublicKey.SignatureLengthBytes);
            }
        }

        #endregion

        public bool Equals(Permission other)
        {
            if (other == null)
            {
                return false;
            }

            if (this.mSignature != null && other.mSignature != null)
            {
                if (this.mSignature.Length != other.mSignature.Length)
                {
                    return false;
                }

                for (int i = 0; i < this.mSignature.Length; i++)
                {
                    if (this.mSignature[i] != other.mSignature[i])
                    {
                        return false;
                    }
                }
            }
            else
            {
                if (this.mSignature != other.mSignature)
                {
                    return false;
                }
            }

            return this.mExpiryTime == other.mExpiryTime &&
                   this.mType == other.mType &&
                   this.mUsersPublicKey.Equals(other.mUsersPublicKey);
        }

        public override bool Equals(object obj)
        {
            return this.Equals(obj as Permission);
        }

        public override int GetHashCode()
        {
            return this.mType.GetHashCode();
        }
    }
}
