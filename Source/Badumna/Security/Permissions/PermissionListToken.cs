﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

using Badumna.Core;

namespace Badumna.Security
{
    /// <summary>
    /// A token containing all the permissions granted to the local user.
    /// Permissions can be granted and revoked at any time.
    /// </summary>
    /// <exclude/>
    public class PermissionListToken : Token, IEquatable<PermissionListToken>
    {
        private Dictionary<PermissionType, Permission> mPermissions = new Dictionary<PermissionType, Permission>();
        private PrivateKey mPermissionKeyPair;

        internal PrivateKey UsersKeyPair { get { return this.mPermissionKeyPair; } }

        /// <summary>
        /// Gets the users public key.
        /// </summary>
        /// <value>The users public key.</value>
        public byte[] UsersPublicKey { get { return this.mPermissionKeyPair.PublicKey.RawData; } }

        /// <summary>
        /// Initializes a new instance of the <see cref="PermissionListToken"/> class.
        /// <remarks>
        /// Default constructor is required for tunnel server serialization.
        /// </remarks>
        /// </summary>
        public PermissionListToken()
            : base(DateTime.MaxValue)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PermissionListToken"/> class.
        /// </summary>
        /// <param name="privateKey">A given private key.</param>
        internal PermissionListToken(PrivateKey privateKey)
            : base(DateTime.MaxValue)
        {
            this.mPermissionKeyPair = privateKey;
        }

        /// <summary>
        /// Gets the expiration time.
        /// </summary>
        override internal DateTime ExpirationTime
        {
            get 
            {
                DateTime expirationTime = DateTime.MaxValue;
                foreach (Permission p in this.mPermissions.Values)
                {
                    if (p.ExpiryTime < expirationTime)
                    {
                        expirationTime = p.ExpiryTime;
                    }
                }

                return expirationTime;
            }
        }

        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// <param name="other">An object to compare with this object.</param>
        /// <returns>
        /// true if the current object is equal to the <paramref name="other"/> parameter; otherwise, false.
        /// </returns>
        public bool Equals(PermissionListToken other)
        {
            if (other == null)
            {
                return false;
            }

            if (this.mPermissions.Count != other.mPermissions.Count)
            {
                return false;
            }

            foreach (KeyValuePair<PermissionType, Permission> permission in this.mPermissions)
            {
                Permission otherPermission;
                if (!other.mPermissions.TryGetValue(permission.Key, out otherPermission))
                {
                    return false;
                }

                if (!permission.Value.Equals(otherPermission))
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Determines whether the specified <see cref="System.Object"/> is equal to this instance.
        /// </summary>
        /// <param name="obj">The <see cref="System.Object"/> to compare with this instance.</param>
        /// <returns>
        /// 	<c>true</c> if the specified <see cref="System.Object"/> is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        /// <exception cref="T:System.NullReferenceException">The <paramref name="obj"/> parameter is null.</exception>
        public override bool Equals(object obj)
        {
            return this.Equals(obj as PermissionListToken);
        }

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
        /// </returns>
        public override int GetHashCode()
        {
            return this.mPermissions.Count;  // TODO: Improve
        }

        /// <summary>
        /// Grant a permission to the local user
        /// </summary>
        /// <param name="serializedPermission">The serialized permission</param>
        /// <param name="signature">The signature of permission</param>
        public void AddPermission(byte[] serializedPermission, byte[] signature)
        {
            Permission permission = new Permission();
            if (signature == null || signature.Length != PublicKey.SignatureLengthBytes)
            {
                throw new SecurityException(String.Format("Invalid signature. Must be {0} bytes long", PublicKey.SignatureLengthBytes));
            }

            permission.Deserialize(serializedPermission);
            permission.Signature = signature;
            permission.UsersPublicKey = this.mPermissionKeyPair.PublicKey;

            if (this.mPermissions.ContainsKey(permission.Type))
            {
                this.mPermissions[permission.Type] = permission;
            }
            else
            {
                this.mPermissions.Add(permission.Type, permission);
            }
        }

        /// <summary>
        /// Revoke the given permission
        /// </summary>
        /// <param name="permissionType"></param>
        public void RemovePermission(int permissionType)
        {
            if (this.mPermissions.ContainsKey((PermissionType)permissionType))
            {
                this.mPermissions.Remove((PermissionType)permissionType);
            }
        }

        /// <summary>
        /// Generate a serialized permission ready for signing by the trusted authority
        /// </summary>
        /// <param name="expiryTime">The time the permission expires</param>
        /// <param name="permissionType">The type of permission</param>
        /// <param name="publicKey">The users public key.</param>
        /// <returns></returns>
        public static byte[] GeneratePermission(DateTime expiryTime, long permissionType, byte[] publicKey)
        {
            try
            {
                Permission permission = new Permission(expiryTime, (PermissionType)permissionType, new PublicKey(publicKey));
                return permission.Serialize();
            }
            catch
            {
                throw new SecurityException("Invalid permission.");
            }
        }

        /// <inheritdoc/>
        internal override void ToMessageOverride(MessageBuffer buffer)
        {
            buffer.Write(this.mPermissions.Count);
            foreach (KeyValuePair<PermissionType, Permission> permission in this.mPermissions)
            {
                buffer.Write((int)permission.Key);
                byte[] permissionData = permission.Value.Serialize();
                buffer.Write(permissionData.Length);
                buffer.Write(permissionData, permissionData.Length);
            }
        }

        /// <inheritdoc/>
        internal override void FromMessageOverride(MessageBuffer buffer)
        {
            int count = buffer.ReadInt();

            if (count < 0)
            {
                throw new ArgumentOutOfRangeException();
            }

            for (int i = 0; i < count; i++)
            {
                PermissionType type = (PermissionType)buffer.ReadInt();
                int dataLength = buffer.ReadInt();
                byte[] permissionData = buffer.ReadBytes(dataLength);
                Permission permission = new Permission();
                permission.Deserialize(permissionData);
                this.mPermissions[type] = permission;
            }
        }

        internal Permission GetPermissionState(PermissionType type)
        {
            Permission permission;
            this.mPermissions.TryGetValue(type, out permission);
            return permission;
        }
    }
}
