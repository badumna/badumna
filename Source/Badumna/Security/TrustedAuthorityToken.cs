﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

using Badumna.Core;

namespace Badumna.Security
{
    /// <summary>
    /// A Token issued by a trusted authority and used to verify the validity of UserCertificateTokens
    /// </summary>
    /// <exclude/>
    public class TrustedAuthorityToken : Token, IEquatable<TrustedAuthorityToken>
    {
        private PublicKey mKey;
        internal PublicKey Key { get { return this.mKey; } }

        /// <summary>
        /// Constructs a token for the trusted authority.
        /// </summary>
        /// <param name="expiryTime">The time when the token expires</param>
        /// <param name="key">The public key of the trusted authority</param>
        public TrustedAuthorityToken(DateTime expiryTime, byte[] key)
            : base(expiryTime)
        {
            this.ConstructKey(key);
        }

        /// <summary>
        /// Private constructor used only with IParseable.FromMessage
        /// </summary>
        private TrustedAuthorityToken()
            : base()
        {
        }

        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// <param name="other">An object to compare with this object.</param>
        /// <returns>
        /// true if the current object is equal to the <paramref name="other"/> parameter; otherwise, false.
        /// </returns>
        public bool Equals(TrustedAuthorityToken other)
        {
            if (other == null)
            {
                return false;
            }

            return this.mKey.Equals(other.mKey);
        }

        /// <summary>
        /// Determines whether the specified <see cref="System.Object"/> is equal to this instance.
        /// </summary>
        /// <param name="obj">The <see cref="System.Object"/> to compare with this instance.</param>
        /// <returns>
        /// 	<c>true</c> if the specified <see cref="System.Object"/> is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        /// <exception cref="T:System.NullReferenceException">The <paramref name="obj"/> parameter is null.</exception>
        public override bool Equals(object obj)
        {
            return this.Equals(obj as TrustedAuthorityToken);
        }

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
        /// </returns>
        public override int GetHashCode()
        {
            return this.mKey.GetHashCode();
        }

        private void ConstructKey(byte[] key)
        {
            if (null == key)
            {
                throw new ArgumentNullException("key");
            }

            if (!PublicKey.IsValidKey(key))
            {
                throw new InvalidOperationException("Key length is incorrect.");
            }

            this.mKey = new PublicKey(key);
        }

        internal override void ToMessageOverride(MessageBuffer buffer)
        {
            buffer.Write(this.Key.RawData.Length);
            buffer.Write(this.Key.RawData, this.Key.RawData.Length);
        }

        internal override void FromMessageOverride(MessageBuffer buffer)
        {
            int length = buffer.ReadInt();
            byte[] key = buffer.ReadBytes(length);
            this.ConstructKey(key);
        }
    }
}
