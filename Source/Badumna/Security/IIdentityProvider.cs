﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Badumna.Security
{
    /// <summary>
    /// Identity providers expose a mechanism for the Badumna library to obtain tokens required for various types of
    /// operations. Classes implementing this interface can be passed to the NetworkFacade.Login method.
    /// </summary>
    public interface IIdentityProvider
    {
        /// <summary>
        /// Activates this instance, triggering (for example) periodic token refresh.
        /// </summary>
        void Activate();

        /// <summary>
        /// Disposes of any resources or background tasks managed by this instance.
        /// </summary>
        void Dispose();

        /// <summary>
        /// Provide a token that permits the local peer to send and receive messages from other peers.
        /// </summary>
        /// <returns>A SymmetricKeyToken providing the unique session key for the network</returns>
        SymmetricKeyToken GetNetworkParticipationToken();

        /// <summary>
        /// Provide a token that can be used by remote peers to authenticate the the local user. The certificate  
        /// should be signed using the trusted authorities private key.
        /// </summary>
        /// <returns>A CertificateToken</returns>
        CertificateToken GetUserCertificateToken();

        /// <summary>
        /// Provide a token for the trusted authorities public key. The public key will be used to verify that other peer's users 
        /// are who they claim to be.
        /// </summary>
        /// <returns>A TrustedAuthorityToken</returns>
        TrustedAuthorityToken GetTrustedAuthorityToken();

        /// <summary>
        /// Provide a token that holds the permissions for the current user. Each permission should be signed using the trusted authorities
        /// private key.
        /// </summary>
        /// <see cref="PermissionListToken.GeneratePermission"/>
        /// <returns>A PermissionListoken</returns>
        PermissionListToken GetPermissionListToken();

        /// <summary>
        /// Provide a token that holds the current network time. The time is used to check the expiration of other tokens.
        /// Time should be in UTC.
        /// </summary>
        /// <returns>A time token</returns>
        TimeToken GetTimeToken();
    }

}
