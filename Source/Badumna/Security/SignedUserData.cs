﻿//-----------------------------------------------------------------------
// <copyright file="SignedUserData.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2012 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Diagnostics;
using Badumna.Core;
using Badumna.Security;
using Badumna.Utilities;

namespace Badumna.Chat
{
    /// <summary>
    /// An interface for data that can be considered to be "owned" by a given character.
    /// Used by SignedUserData
    /// </summary>
    internal interface IOwnedByCharacter
    {
        /// <summary>
        /// Gets the claimed owner for this object.
        /// </summary>
        /// When used in SignedUserData, the claimed owner must match the user's
        /// certificate token character for this object to validate successfully.
        Character Character { get; }
    }

    /// <summary>
    /// A generic container to wrap some piece of data in a signature.
    /// </summary>
    /// Validates that the user certificate is valid and matches (owns) the data contained.
    /// <typeparam name="T">The data to be signed / verified</typeparam>
    internal abstract class SignedUserData<T> : IParseable, IEquatable<SignedUserData<T>>
        where T : IParseable, IOwnedByCharacter, IEquatable<T>
    {
        /// <summary>
        /// The signature.
        /// </summary>
        private Signature signature;

        /// <summary>
        /// Initializes a new instance of the <see cref="SignedUserData&lt;T&gt;"/> class.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="userCertificate">The user certificate.</param>
        public SignedUserData(T value, ICertificateToken userCertificate)
        {
            this.Value = value;
            this.UserCertificate = userCertificate;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SignedUserData&lt;T&gt;"/> class.
        /// Used only by IParseable
        /// </summary>
        protected SignedUserData()
        {
        }

        /// <summary>
        /// Gets the value.
        /// </summary>
        public T Value { get; private set; }

        /// <summary>
        /// Gets the signature.
        /// </summary>
        internal Signature Signature
        {
            get
            {
                if (this.signature == null)
                {
                    this.signature = new Signature(this.UserCertificate.PrivateKey.Sign(this.MessageContents));
                }

                return this.signature;
            }
        }

        /// <summary>
        /// Gets the user certificate.
        /// </summary>
        internal ICertificateToken UserCertificate { get; private set; }

        /// <summary>
        /// Gets the value type - workaround for a limitation in actionscript compiler's generics support
        /// </summary>
        /// <value>
        /// The value type.
        /// </value>
        protected abstract Type ValueType { get; }

        /// <summary>
        /// Gets the byte contents of `this.Value`, for verifying against a signature.
        /// </summary>
        private byte[] MessageContents
        {
            get
            {
                using (MessageBuffer buffer = new MessageBuffer())
                {
                    buffer.Write(this.Value);
                    return buffer.GetBuffer();
                }
            }
        }

        /// <inheritdoc/>
        public bool Equals(SignedUserData<T> other)
        {
            // workaround Actionscript compiler bug - using `this.Value` inline compiles into a temporary variable
            // which is never assigned to (presumably a bug related to generic property types)
            T thisValue = this.Value;

            return !object.ReferenceEquals(other, null) &&
                thisValue.Equals(other.Value) &&
                this.UserCertificate.Equals(other.UserCertificate) &&
                this.Signature.Equals(other.Signature);
        }

        /// <inheritdoc/>
        public override bool Equals(object other)
        {
            return this.Equals(other as SignedUserData<T>);
        }

        /// <inheritdoc/>
        public override int GetHashCode()
        {
            return this.UserCertificate.GetHashCode();
        }

        /// <inheritdoc/>
        public override string ToString()
        {
            return String.Format("<#SignedUserData: {0} ({1})>", this.Value, this.signature == null ? "not signed" : "signed");
        }

        /// <inheritdoc/>
        void IParseable.ToMessage(MessageBuffer buffer, Type type)
        {
            buffer.Write(this.Value);
            buffer.Write(this.UserCertificate);
            buffer.Write(this.Signature);
        }

        /// <inheritdoc/>
        void IParseable.FromMessage(MessageBuffer buffer)
        {
            // workaround for actionscript compiler not supporting generic methods using type params
            this.Value = (T)buffer.Read(this.ValueType);

            this.UserCertificate = buffer.Read<CertificateToken>();
            this.signature = buffer.Read<Signature>();
        }

        /// <summary>
        /// Validates that this user's certificate is valid and that it signed this message
        /// </summary>
        /// <returns>Whether this object is valid</returns>
        /// <param name="tokens">Local token cache.</param>
        internal bool Validate(ITokenCache tokens)
        {
            if (!this.UserCertificate.Validate(tokens))
            {
                Logger.TraceInformation(LogTag.Security, "Invalid certificate received for " + this);
                return false;
            }

            if (this.UserCertificate.Character != this.Value.Character)
            {
                Logger.TraceInformation(LogTag.Security, "Certificate owner (" + this.UserCertificate.Character + ") does not match claimed character (" + this.Value.Character + ") in " + this);
                return false;
            }

            return this.UserCertificate.PublicKey.VerifySignature(this.MessageContents, this.Signature);
        }
    }
}
