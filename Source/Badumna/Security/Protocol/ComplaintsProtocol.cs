﻿using System;
using System.Collections.Generic;
using System.Text;

using Badumna.Core;
using Badumna.Utilities;
using Badumna.Transport;
using Badumna.DistributedHashTable;

namespace Badumna.Security
{
    /*
    class ComplaintsProtocol : DhtProtocol
    {
        private PeerAddress mProxyAddress;

        public ComplaintsProtocol(DhtProtocol parent)
            : base(parent)
        {

        }

        public void ComplainAbout(CertificateToken userCertificate, ComplaintProperties complaint)
        {
            DhtEnvelope envelope = this.GetMessageFor(this.mProxyAddress, QualityOfService.Reliable);
            CertificateToken localUserToken = CertificateToken.GetToken();

            this.RemoteCall(envelope, this.ForwardComplaint, userCertificate.SerializeToBytes(), localUserToken.SerializeToBytes(), complaint);
            this.SendMessage(envelope);
        }

        #region Protocol Methods

        [ConnectionfulProtocolMethod(ConnectionfulMethod.ProcessComplaint)]
        protected virtual void ForwardComplaint(byte[] serializedComplaineeCertificate, byte[] serializedComplainerCertificate, ComplaintProperties complaint)
        {
        }

        [ConnectionfulProtocolMethod(ConnectionfulMethod.ProcessComplaint)]
        protected virtual void ProcessComplaint(byte[] serializedComplaineeCertificate, byte[] serializedComplainerCertificate, ComplaintProperties complaint)
        {
        }

        #endregion
    }
     */
}
