﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;

using Badumna.Core;
using Badumna.Utilities;
using Badumna.Transport;
using Badumna.DistributedHashTable;

namespace Badumna.Security
{
    class ComplaintForwarder : TransportProtocol, IComplaintForwarder
    {
        protected List<PeerAddress> mProxyAddresses;

        protected INetworkAddressProvider addressProvider;

        private TokenCache tokens;

        public ComplaintForwarder(TransportProtocol parent, INetworkAddressProvider addressProvider, TokenCache tokens)
            : base(parent)
        {
            this.addressProvider = addressProvider;
            this.tokens = tokens;
            this.mProxyAddresses = new List<PeerAddress>();
            this.mProxyAddresses.Add(PeerAddress.GetAddress("secure.badumna.com", 25261));
        }

        private PeerAddress GetRandomProxyAddress()
        {
            if (this.mProxyAddresses.Count == 0)
            {
                throw new SecurityException("No known proxy addresses exist.");
            }

            int i = RandomSource.Generator.Next(this.mProxyAddresses.Count-1);

            if (this.mProxyAddresses[i].Equals(this.addressProvider.PublicAddress))
            {
                this.mProxyAddresses.RemoveAt(i);
                return this.GetRandomProxyAddress();
            }

            return this.mProxyAddresses[i];
        }

        public void AddProxyAddress(PeerAddress proxyAddress, PeerAddress destination)
        {
            Permission changeProxyPermission =  this.tokens.GetPermission(PermissionType.UpdateComplaintProxy);
            PrivateKey keyPair = this.tokens.GetPermissionKeyPair();

            if (changeProxyPermission != null && keyPair != null)
            {
                Signature signature = new Signature(keyPair, proxyAddress, this.addressProvider.PublicAddress);
                Debug.Assert(signature.Verify(keyPair.PublicKey, proxyAddress, this.addressProvider.PublicAddress),
                    "Signature cannot be verifed");

                TransportEnvelope envelope = this.GetMessageFor(destination, QualityOfService.Reliable);

                this.RemoteCall(envelope, this.ChangeProxyAddress, proxyAddress, changeProxyPermission, signature);
                this.SendMessage(envelope);
            }
            else
            {
                Logger.TraceInformation(LogTag.Security, "Local user does not have permission to change proxy addresses.");
            }
        }

        #region IComplaintForwarder Members

        public void ComplainAbout(ICertificateToken userCertificate, ComplaintProperties complaint)
        {
            TransportEnvelope envelope = this.GetMessageFor(this.GetRandomProxyAddress(), QualityOfService.Reliable);
            ICertificateToken localUserToken = this.tokens.GetUserCertificateToken();

            this.RemoteCall(envelope, this.ForwardComplaint, userCertificate, localUserToken, complaint);
            this.SendMessage(envelope);
        }

        #endregion

        #region Protocol Methods

        [ConnectionfulProtocolMethod(ConnectionfulMethod.ForwardComplaint)]
        protected virtual void ForwardComplaint(ICertificateToken complaineeCertificate, ICertificateToken complainerCertificate, ComplaintProperties complaint)
        {
            TransportEnvelope envelope = this.GetMessageFor(this.GetRandomProxyAddress(), QualityOfService.Reliable);

            this.RemoteCall(envelope, this.ForwardComplaint, complaineeCertificate, complainerCertificate, complaint);
            this.SendMessage(envelope);
        }

        [ConnectionfulProtocolMethod(ConnectionfulMethod.ChangeProxyAddress)]
        protected void ChangeProxyAddress(PeerAddress address, Permission changeProxyPermission, Signature signature)
        {
            Debug.Assert(this.CurrentEnvelope.Certificate != null, "No certificate attached to envelope.");

            if (changeProxyPermission.Verify(PermissionType.UpdateComplaintProxy, this.tokens))
            {
                if (signature.Verify(changeProxyPermission, address, this.CurrentEnvelope.Source))
                {
                    Logger.TraceInformation(LogTag.Security, "Changing addrees of complaint proxy to {0}", address);
                    this.mProxyAddresses.Clear();
                    if (!this.mProxyAddresses.Contains(address))
                    {
                        this.mProxyAddresses.Add(address);
                    }
                }
                else
                {
                    Logger.TraceInformation(LogTag.Security, "Failed to verify signature from {0}", this.CurrentEnvelope.Certificate.UserId);
                }
            }
            else
            {
                Logger.TraceInformation(LogTag.Security, "User {0} does not have permission to change proxy address.", this.CurrentEnvelope.Certificate.UserId);
            }
        }

        #endregion
    }
}
