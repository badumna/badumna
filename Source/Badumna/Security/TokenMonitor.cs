﻿using System;
using System.Collections.Generic;
using System.Text;

using Badumna.Core;
using Badumna.Utilities;
using System.Diagnostics;

namespace Badumna.Security
{
    class TokenExpirationEventArgs : EventArgs
    {
        private TokenType mType;
        public TokenType Type { get { return this.mType; } }

        private Token mOldToken;
        public Token OldToken { get { return this.mOldToken; } }

        private Token mNewToken;
        public Token NewToken
        {
            get { return this.mNewToken; }
            set { this.mNewToken = value; }
        }

        public TokenExpirationEventArgs(TokenType type, Token oldToken, Token newToken)
        {
            this.mType = type;
            this.mOldToken = oldToken;
            this.mNewToken = newToken;
        }
    }

    class TokenMonitor
    {
        private EventHandler<TokenExpirationEventArgs> tokenExpiredEventDelegate;
        public event EventHandler<TokenExpirationEventArgs> TokenExpiredEvent
        {
            add { this.tokenExpiredEventDelegate += value; }
            remove { this.tokenExpiredEventDelegate -= value; }
        }

        private TokenType mTokenType;
        private IIdentityProvider mIdentityProvider;
        private Token mToken;
        private NetworkEvent mRefreshEvent;
        private NetworkEvent mExpireEvent;

        /// <summary>
        /// The queue to use for scheduling events.
        /// </summary>
        private NetworkEventQueue eventQueue;

        /// <summary>
        /// Provides an estimate of the current network time.
        /// </summary>
        private Clock networkTime;

        public TokenMonitor(TokenType type, NetworkEventQueue eventQueue, Clock networkTime)
        {
            this.eventQueue = eventQueue;
            this.mTokenType = type;
            this.networkTime = networkTime;
        }

        public void SetIdentityProvider(IIdentityProvider identityProvider)
        {
            this.mIdentityProvider = identityProvider;
        }

        public bool CacheAndMonitor()
        {
            this.Invalidate();
            
            Token token = this.RetrieveToken();

            if (token != null)
            {
                this.SetExpireEvent(token);
                this.mToken = token;

                return true;
            }

            return false;
        }

        private void SetExpireEvent(Token token)
        {
            TimeSpan overhead = TimeSpan.FromSeconds(5);
            double delay_ms = ((token.ExpirationTime - this.networkTime.Now) - overhead).TotalMilliseconds;
            delay_ms = Math.Max(0.0, delay_ms);
            this.mExpireEvent = this.eventQueue.Schedule(delay_ms, this.ExpireToken, token);
        }

        private void ExpireToken(Token token)
        {
            if (this.mToken == null)
            {
                bool isMonitoring = this.CacheAndMonitor();
				if (!isMonitoring)
				{
					// TODO: Schedule another try in the future.
				}
				
                return;
            }

            Token expiredToken = this.mToken;
            Token newPrimaryToken = this.RetrieveToken();

            EventHandler<TokenExpirationEventArgs> handler = this.tokenExpiredEventDelegate;
            if (handler != null)
            {
                handler.Invoke(this, new TokenExpirationEventArgs(this.mTokenType, expiredToken, newPrimaryToken));
            }

            this.DisposeToken(expiredToken);

            if (newPrimaryToken != null && !newPrimaryToken.IsExpiredAt(this.networkTime.Now))
            {
                this.mToken = newPrimaryToken;
                this.SetExpireEvent(newPrimaryToken);
            }
            else
            {
                // FIXME: this will cause the local peer to be put into offline status. it should be given at least a 
                // second chance
                this.mToken = null;
                Logger.TraceInformation(LogTag.Security, "Unable to get next valid token.");
            }
        }

        public Token GetToken()
        {
            return this.mToken;
        }

        public void ForceUpdate()
        {
            this.Invalidate();
            this.CacheAndMonitor();
        }

        public void Invalidate()
        {
            this.DisposeToken(this.mToken);
            this.mToken = null;

            if (this.mExpireEvent != null)
            {
                this.eventQueue.Remove(this.mExpireEvent);
                this.mExpireEvent = null;
            }

            if (this.mRefreshEvent != null)
            {
                this.eventQueue.Remove(this.mRefreshEvent);
                this.mRefreshEvent = null;
            }
        }

        private void DisposeToken(Token token)
        {
            IDisposable disposable = token as IDisposable;
            if (disposable != null)
            {
                disposable.Dispose();
            }
        }

        private Token RetrieveToken()
        {
            Token token = null;

            try
            {
                switch (this.mTokenType)
                {
                    case TokenType.NetworkTime:
                        Logger.TraceInformation(LogTag.Security, "Getting time token");
                        token = this.mIdentityProvider.GetTimeToken();
                        this.networkTime.Synchronize(((TimeToken)token).CurrentNetworkTime);
                        break;

                    case TokenType.NetworkParticipatation:
                        Logger.TraceInformation(LogTag.Security, "Getting network participation token");
                        token = this.mIdentityProvider.GetNetworkParticipationToken();
                        break;

                    case TokenType.PermissionList:
                        Logger.TraceInformation(LogTag.Security, "Getting permission list token");
                        token = this.mIdentityProvider.GetPermissionListToken();
                        break;

                    case TokenType.TrustedAuthority:
                        Logger.TraceInformation(LogTag.Security, "Getting trusted authority token");
                        token = this.mIdentityProvider.GetTrustedAuthorityToken();
                        break;

                    case TokenType.UserCertificate:
                        Logger.TraceInformation(LogTag.Security, "Getting user certificate token");
                        token = this.mIdentityProvider.GetUserCertificateToken();
                        break;
                }
            }
            catch (Exception e)
            {
                Logger.TraceException(LogLevel.Error, e, "Failed to get token");
            }

            return token;
        }
    }
}
