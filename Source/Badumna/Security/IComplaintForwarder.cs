﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Badumna.Security
{
    interface IComplaintForwarder
    {
        void ComplainAbout(ICertificateToken userCertificate, ComplaintProperties complaint);
    }
}
