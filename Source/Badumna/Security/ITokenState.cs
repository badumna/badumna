﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Badumna.Security
{

    /// <summary>
    /// An interface for internal data associated with tokens.
    /// </summary>
    public interface ITokenState
    {
        /// <summary>
        /// A custom serialization method which can be used to serialize the state.
        /// </summary>
        /// <returns>The state serialized as an array of bytes</returns>
        byte[] Serialize();

        /// <summary>
        /// A custom deserialization method used to populate the token state with data previously returned by a call to Serialize.
        /// </summary>
        /// <param name="data"></param>
        void Deserialize(byte[] data);
    }
}
