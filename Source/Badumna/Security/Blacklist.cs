﻿using System;
using System.Collections.Generic;
using System.Text;

using Badumna.Utilities;
using Badumna.Core;

namespace Badumna.Security
{
    class Blacklist
    {
        class BlacklistEntry
        {
            public long UserId;
            public DateTime ExpiryTime;

            public BlacklistEntry(long id, DateTime expiryTime)
            {
                this.UserId = id;
                this.ExpiryTime =expiryTime;
            }
        }

        private Dictionary<long, BlacklistEntry> mBlacklist = new Dictionary<long, BlacklistEntry>();

        public Blacklist(NetworkEventQueue eventQueue, INetworkConnectivityReporter connectivityReporter)
        {
			// TODO: This class looks broken. Fix or remove.
            new RegularTask("Black list garbage collection", TimeSpan.FromHours(1), eventQueue, connectivityReporter, this.CollectGarbage);
        }

        public void CollectGarbage()
        {
            List<BlacklistEntry> oldEntries = new List<BlacklistEntry>();

            foreach (BlacklistEntry entry in this.mBlacklist.Values)
            {
                if (entry.ExpiryTime < DateTime.Now)
                {
                    oldEntries.Add(entry);
                }
            }

            foreach (BlacklistEntry entry in oldEntries)
            {
                this.mBlacklist.Remove(entry.UserId);
            }
        }

        public bool IsBlackListed(long userId)
        {
            BlacklistEntry entry = null;

            if (this.mBlacklist.TryGetValue(userId, out entry))
            {
                return entry.ExpiryTime < DateTime.Now;
            }

            return false;
        }

        public void Add(long userId, TimeSpan entryLifeSpan)
        {
            if (entryLifeSpan.TotalMilliseconds <= 0)
            {
                return;
            }

            BlacklistEntry entry = null;
            DateTime expiryTime = DateTime.Now + entryLifeSpan;

            if (this.mBlacklist.TryGetValue(userId, out entry))
            {
                if (entry.ExpiryTime < expiryTime)
                {
                    entry.ExpiryTime = expiryTime;
                }
            }
            else
            {
                this.mBlacklist.Add(userId, new BlacklistEntry(userId, expiryTime));
            }
        }

    }
}
