﻿using System;
using System.Collections.Generic;
using System.Text;

using Badumna.Core;

namespace Badumna.Security
{
    /// <summary>
    /// An exception indicating an security related problem 
    /// </summary>
    public class SecurityException : BadumnaException
    {
        internal SecurityException(String message)
            : base(message)
        { }
    }

    /// <summary>
    /// The token state passed to the Token constructor is not the TokenState passed to the IdentityProvider delegate.
    /// </summary>
    /// <exclude/>
    public class IncorrectTokenStateException : SecurityException
    {
        internal IncorrectTokenStateException()
            : base("The token state passed to the Token constructor is not the TokenState passed to the IdentityProvider delegate.")
        { }    
    }
}
