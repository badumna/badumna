﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Security.Cryptography;

using Badumna.Core;
using Badumna.Utilities;

namespace Badumna.Security
{
    class PrivateKey : Key
    {
        private PublicKey mPublicKey;
        public PublicKey PublicKey { get { return this.mPublicKey; } }

        public PrivateKey()
            : base(true)
        {
            this.mRawData = this.mRsaService.ExportCspBlob(true);
            this.mPublicKey = new PublicKey(this.mRsaService.ExportCspBlob(false));
        }

        public PrivateKey(byte[] privateKey)
            : base(privateKey, true)
        {
            this.mPublicKey = new PublicKey(this.mRsaService.ExportCspBlob(false));
        }

        public PrivateKey(string serializedKeyXml)
            :base(serializedKeyXml, true)
        {
            this.mPublicKey = new PublicKey(this.mRsaService.ExportCspBlob(false));
        }

        public PrivateKey(RSACryptoServiceProvider cryptoServiceProvider)
            :base(cryptoServiceProvider, true)
        {
            this.mPublicKey = new PublicKey(this.mRsaService.ExportCspBlob(false));
        }

        public byte[] Sign(byte[] content)
        {
            if (content == null)
            {
                throw new ArgumentNullException("content");
            }

            using (SHA1Managed sha = new SHA1Managed()) // TODO : Change to SHA256 when mono supports it.
            {
                byte[] hash = sha.ComputeHash(content);
                
                // With some network configurations, OID lookup can be very slow (>15 seconds!).
                // Passing null causes the defauly hash to be used (SHA1) and no lookup is performed.
                // See http://support.microsoft.com/kb/948080
                ////return this.mRsaService.SignHash(hash, CryptoConfig.MapNameToOID("SHA1"));
                return this.mRsaService.SignHash(hash, null);
            }
        }
    }
}
