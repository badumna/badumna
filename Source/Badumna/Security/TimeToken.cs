﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Badumna.Utilities;
using Badumna.Core;

namespace Badumna.Security
{
    /// <summary>
    /// A token issued by a trusted authority which specifies an approximate global (network wide) time.
    /// It is primarilty used to determine the correct expiry time of other tokens.
    /// </summary>
    /// <exclude/>
    public class TimeToken : Token, IEquatable<TimeToken>
    {
        private DateTime mConstructionTime;
        private DateTime mServerTimestamp;

        /// <summary>
        /// Gets the current network time.
        /// </summary>
        /// <value>The current network time.</value>
        public DateTime CurrentNetworkTime { get { return this.mServerTimestamp + (DateTime.Now - this.mConstructionTime); } }

        /// <summary>
        /// Initializes a new instance of the <see cref="TimeToken"/> class.
        /// </summary>
        /// <param name="networkTimestamp">The current time of the refence clock.</param>
        /// <param name="expiryTime">The time when the token expires</param>
        public TimeToken(DateTime networkTimestamp, DateTime expiryTime)
            : base(expiryTime)
        {
            this.mConstructionTime = DateTime.Now;
            this.mServerTimestamp = networkTimestamp;
        }

        /// <summary>
        /// Private constructor used only with IParseable.FromMessage
        /// </summary>
        private TimeToken()
            : base()
        {
        }

        /// <inheritdoc/>
        /// <remarks>Error will be introduced when the token is transmitted over the network but this should happen at most once.</remarks>
        internal override void FromMessageOverride(MessageBuffer buffer)
        {
            this.mServerTimestamp = new DateTime(buffer.ReadLong());
            this.mConstructionTime = DateTime.Now;
        }

        /// <inheritdoc/>
        internal override void ToMessageOverride(MessageBuffer buffer)
        {
            buffer.Write(this.CurrentNetworkTime.Ticks);
        }

        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// <param name="other">An object to compare with this object.</param>
        /// <returns>
        /// true if the current object is equal to the <paramref name="other"/> parameter; otherwise, false.
        /// </returns>
        public bool Equals(TimeToken other)
        {
            return other != null && this.CurrentNetworkTime.Equals(other.CurrentNetworkTime);
        }

        /// <summary>
        /// Determines whether the specified <see cref="System.Object"/> is equal to this instance.
        /// </summary>
        /// <param name="obj">The <see cref="System.Object"/> to compare with this instance.</param>
        /// <returns>
        /// 	<c>true</c> if the specified <see cref="System.Object"/> is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        /// <exception cref="T:System.NullReferenceException">The <paramref name="obj"/> parameter is null.</exception>
        public override bool Equals(object obj)
        {
            return this.Equals(obj as TimeToken);
        }

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
        /// </returns>
        public override int GetHashCode()
        {
            return this.mServerTimestamp.GetHashCode();
        }
    }
}
