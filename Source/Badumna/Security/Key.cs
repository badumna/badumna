﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Security.Cryptography;

using Badumna.Core;
using Badumna.Utilities;

namespace Badumna.Security
{
    abstract class Key : IEquatable<Key>
    {
        protected RSACryptoServiceProvider mRsaService;
        
        protected const int KeyLengthBytes = 128;
        public const int SignatureLengthBytes = 128;

        protected byte[] mRawData = new byte[Key.KeyLengthBytes + 20];
        public byte[] RawData { get { return this.mRawData; } }

        private bool mIncludesPrivateKey;

        public Key(bool includesPrivate)
        {
            this.mRsaService = new RSACryptoServiceProvider(Key.KeyLengthBytes * 8);
            this.mIncludesPrivateKey = includesPrivate;
        }

        public Key(byte[] rawData, bool includesPrivate)
        {
            this.mRsaService = new RSACryptoServiceProvider(Key.KeyLengthBytes * 8);
            this.mRsaService.ImportCspBlob(rawData);
            this.mRawData = rawData;
            this.mIncludesPrivateKey = includesPrivate;
        }

        public Key(RSACryptoServiceProvider serviceProvider, bool includesPrivate)
        {
            if (serviceProvider.KeySize != Key.KeyLengthBytes * 8)
            {
                throw new ArgumentException(String.Format("Key size is invalid. Must be {0} bits.", Key.KeyLengthBytes * 8));
            }

            this.mRsaService = serviceProvider;
            this.mRawData = this.mRsaService.ExportCspBlob(includesPrivate);
            this.mIncludesPrivateKey = includesPrivate;
        }

        public Key(String xmlString, bool includesPrivate)
        {
            this.mRsaService = new RSACryptoServiceProvider(Key.KeyLengthBytes * 8);
            this.mRsaService.FromXmlString(xmlString);
            this.mRawData = this.mRsaService.ExportCspBlob(includesPrivate);
            this.mIncludesPrivateKey = includesPrivate;
        }

        internal RSACryptoServiceProvider RsaService
        {
            get { return this.mRsaService; }
        }

        /// <summary>
        /// Use to generate a new RSA key pair.
        /// </summary>
        /// <param name="includesPrivate">A value indicating whether private key should be included.</param>
        /// <returns>Return RSA key pair in xml string format.</returns>
        public static string GenerateKeyPair(bool includesPrivate)
        {
            RSACryptoServiceProvider rsaService = new RSACryptoServiceProvider(Key.KeyLengthBytes * 8);
            return rsaService.ToXmlString(includesPrivate);
        }

        protected void Generate()
        {
            this.mRawData = this.mRsaService.ExportCspBlob(this.mIncludesPrivateKey);
        }

        public string ToXml()
        {
            return this.mRsaService.ToXmlString(this.mIncludesPrivateKey);
        }

        public static bool IsValidKey(byte[] key)
        {
            return key.Length == Key.KeyLengthBytes + 20;
        }

        public bool VerifySignature(byte[] contents, byte[] signature)
        {
            try
            {
                using (SHA1Managed sha1 = new SHA1Managed())
                {
                    byte[] digest = sha1.ComputeHash(contents);

                    return this.mRsaService.VerifyHash(digest, CryptoConfig.MapNameToOID("SHA1"), signature);
                }
            }
            catch (CryptographicException)
            {
                return false;
            }
        }

        public bool VerifySignature(byte[] contents, Signature signature)
        {
            return this.VerifySignature(contents, signature.RawData);
        }

        public byte[] Encrypt(byte[] content)
        {
            if (content == null)
            {
                throw new ArgumentNullException("content");
            }

            return this.mRsaService.Encrypt(content, false);
        }

        public byte[] Decrypt(byte[] content)
        {
            if (content == null)
            {
                throw new ArgumentNullException("content");
            }

            return this.mRsaService.Decrypt(content, false);
        }


        public bool Equals(Key other)
        {
            if (other == null)
            {
                return false;
            }

            if (this.mRawData.Length != other.mRawData.Length)
            {
                return false;
            }

            if (!this.GetType().Equals(other.GetType()))
            {
                return false;
            }

            for (int i = 0; i < this.mRawData.Length; i++)
            {
                if (this.mRawData[i] != other.mRawData[i])
                {
                    return false;
                }
            }

            return true;
        }

        public override bool Equals(object obj)
        {
            return this.Equals(obj as Key);
        }

        public override int GetHashCode()
        {
            System.Diagnostics.Debug.Assert(Key.KeyLengthBytes >= 4);
            return BitConverter.ToInt32(this.mRawData, 0);
        }
    }
}
