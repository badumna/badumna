﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Badumna.Security
{
    /// <summary>
    /// The type of token.
    /// </summary>
    enum TokenType
    {
        /// <summary>
        /// Default state. No authorization is given.
        /// </summary>
        None,

        /// <summary>
        /// The system wide time used to validate tokens.
        /// </summary>
        NetworkTime,  

        /// <summary>
        /// The given peer is permitted to send and receive messages from other peers.
        /// </summary>
        NetworkParticipatation,  

        /// <summary>
        /// A certificate for the user of this peer.
        /// </summary>
        UserCertificate,

        /// <summary>
        /// A token issued by a trusted authority to this peer to verfiy the authentication of user certificates.
        /// </summary>
        TrustedAuthority,

        /// <summary>
        /// A token containing the permissions of the local user
        /// </summary>
        PermissionList
    }
}
