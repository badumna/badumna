﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

using System.Security.Cryptography;

using Badumna.Core;
using Badumna.Utilities;

namespace Badumna.Security
{
    /// <summary>
    /// A Token that provides a symmetric key.
    /// </summary>
    /// <exclude/>
    public class SymmetricKeyToken : Token, IDisposable, IEquatable<SymmetricKeyToken>
    {
        private SymmetricKey mSymmetricKey;
        internal SymmetricKey Key { get { return this.mSymmetricKey; } }

        /// <summary>
        /// The legth in bytes of this key.
        /// </summary>
        public static int KeyLengthBytes = SymmetricKey.KeyLengthBytes;

        /// <summary>
        /// Construct a symmetric key authorization token from the given key
        /// </summary>
        /// <param name="expiryTime">The time the token expires</param>
        /// <param name="key">The symmetric key to use</param>
        public SymmetricKeyToken(DateTime expiryTime, byte[] key)
            : base(expiryTime)
        {
            this.ConstructKey(key);
        }

        /// <summary>
        /// Private constructor used only with IParseable.FromMessage
        /// </summary>
        private SymmetricKeyToken()
            : base()
        {
        }

        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// <param name="other">An object to compare with this object.</param>
        /// <returns>
        /// true if the current object is equal to the <paramref name="other"/> parameter; otherwise, false.
        /// </returns>
        public bool Equals(SymmetricKeyToken other)
        {
            if (other == null)
            {
                return false;
            }

            return this.mSymmetricKey.Equals(other.mSymmetricKey);
        }

        /// <summary>
        /// Determines whether the specified <see cref="System.Object"/> is equal to this instance.
        /// </summary>
        /// <param name="obj">The <see cref="System.Object"/> to compare with this instance.</param>
        /// <returns>
        /// 	<c>true</c> if the specified <see cref="System.Object"/> is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        /// <exception cref="T:System.NullReferenceException">The <paramref name="obj"/> parameter is null.</exception>
        public override bool Equals(object obj)
        {
            return this.Equals(obj as SymmetricKeyToken);
        }

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
        /// </returns>
        public override int GetHashCode()
        {
            return this.mSymmetricKey.GetHashCode();
        }

        private void ConstructKey(byte[] key)
        {
            if (null == key)
            {
                throw new ArgumentNullException("key");
            }

            if (key.Length != SymmetricKey.KeyLengthBytes)
            {
                throw new InvalidOperationException(String.Format("Key length is incorrect. Must be {0} bytes", SymmetricKey.KeyLengthBytes));
            }
            
            this.mSymmetricKey = new SymmetricKey(key);
        }

        /// <inheritdoc/>
        internal override void ToMessageOverride(MessageBuffer buffer)
        {
            buffer.Write(this.Key.Key.Length);
            buffer.Write(this.Key.Key, this.Key.Key.Length);
        }

        /// <inheritdoc/>
        internal override void FromMessageOverride(MessageBuffer buffer)
        {
            int length = buffer.ReadInt();
            byte[] key = buffer.ReadBytes(length);
            this.ConstructKey(key);
        }

        internal byte[] Encrypt(byte[] toEncrypt)
        {
            if (this.mSymmetricKey != null)
            {
                return this.mSymmetricKey.Encrypt(toEncrypt);
            }

            return null;
        }

        internal byte[] Decrypt(byte[] toDecrypt)
        {
            if (this.mSymmetricKey != null)
            {
                return this.mSymmetricKey.Decrypt(toDecrypt);
            }
            return null;
        }

        /// <summary>
        /// Cleanup internal data.
        /// </summary>
        public void Dispose()
        {
            this.mSymmetricKey.Dispose();
        }
    }
}
