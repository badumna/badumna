﻿using System;
using System.Collections.Generic;
using System.Text;

using Badumna.Core;

namespace Badumna.Security
{
    class ComplaintProperties : IParseable
    {
        private ComplaintTypes mType;
        public ComplaintTypes Type { get { return this.mType; } }

        public ComplaintProperties()
        {
        }

        public ComplaintProperties(ComplaintTypes type)
        {
            this.mType = type;
        }


        #region IParseable Members

        public void ToMessage(MessageBuffer message, Type parameterType)
        {
            if (message != null)
            {
                message.Write((byte)this.mType);
            }
        }

        public void FromMessage(MessageBuffer message)
        {
            if (message != null)
            {
                this.mType = (ComplaintTypes)message.ReadByte();
            }
        }

        #endregion
    }

    enum ComplaintTypes : byte
    {
        IDontLikeThisPerson
    }
}
