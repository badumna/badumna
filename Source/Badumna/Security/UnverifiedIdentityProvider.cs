﻿using System;
using System.Collections.Generic;
using System.Security;
using System.Security.Cryptography;
using System.Text;

using Badumna.Core;

namespace Badumna.Security
{
    /// <summary>
    /// An insecure, unverified identity provider.
    /// See Dei.IdentityProvider for a secure alternative to this class.
    /// </summary>
    public class UnverifiedIdentityProvider : IIdentityProvider
    {
        private DateTime NetworkTime { get { return DateTime.Now.ToUniversalTime(); } }
        private string characterName;

        /// <summary>
        /// Private key used for DefaultUserCertificate and PermisionListToken.
        /// </summary>
        private PrivateKey privateKey;

        internal UnverifiedIdentityProvider()
            : this(null)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UnverifiedIdentityProvider"/> class for the
        /// given character name.
        /// 
        /// A private key pair is generated automatically using `UnverifiedIdentityProvider.GenerateKeyPair()`.
        /// </summary>
        /// <param name="characterName">Name of the character.</param>
        public UnverifiedIdentityProvider(string characterName)
            : this(characterName, UnverifiedIdentityProvider.GenerateKeyPair())
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UnverifiedIdentityProvider"/> class for the
        /// given character name.
        /// </summary>
        /// <param name="characterName">Name of the character.</param>
        /// <param name="keyPairXml">The key pair from a previous call to <see cref="UnverifiedIdentityProvider.GenerateKeyPair"/>.</param>
        public UnverifiedIdentityProvider(string characterName, string keyPairXml)
        {
            this.characterName = characterName;
            try
            {
                this.privateKey = new PrivateKey(keyPairXml);
            }
            catch (CryptographicException e)
            {
                throw new ArgumentException("Invalid Rsa key pair XML string", e);
            }
            catch (XmlSyntaxException e)
            {
                throw new ArgumentException("Invalid Rsa key pair XML string", e);
            }
        }

        /// <summary>
        /// Generates a new RSA key pair and returns an XML string containing a newly generated key pair.
        /// </summary>
        /// <returns>Returns an XML string containing a newly generated key pair.</returns>
        public static string GenerateKeyPair()
        {
            return Key.GenerateKeyPair(true);
        }

        /// <inheritdoc />
        public SymmetricKeyToken GetNetworkParticipationToken()
        {
            return new SymmetricKeyToken(DateTime.MaxValue, new byte[SymmetricKey.KeyLengthBytes]);
        }

        /// <inheritdoc />
        public CertificateToken GetUserCertificateToken()
        {
            return DefaultUserCertificate.Create(-1, this.characterName, this.NetworkTime, this.privateKey);
        }

        /// <inheritdoc />
        public TrustedAuthorityToken GetTrustedAuthorityToken()
        {
            return new TrustedAuthorityToken(DateTime.MaxValue, new PublicKey(Resources.DefaultTrustedAuthorityKey).RawData);
        }

        /// <inheritdoc />
        public PermissionListToken GetPermissionListToken()
        {
            PermissionListToken token = new PermissionListToken(this.privateKey);

            // all peers will be issued the announce permission
            DateTime expireDate = this.NetworkTime + TimeSpan.FromDays(1);
            PublicKey publicKey = new PublicKey(token.UsersPublicKey);
            Permission permission = new Permission(expireDate, PermissionType.Announce, publicKey);
            
            // serialize the permission, sign it. 
            PrivateKey trustedAuthorityKey = new PrivateKey(Resources.DefaultTrustedAuthorityKey);
            byte[] serializedPermission = permission.Serialize();
            byte[] signature = trustedAuthorityKey.Sign(serializedPermission);

            // add to the permission list token
            token.AddPermission(serializedPermission, signature);

            return token;
        }

        /// <inheritdoc />
        public TimeToken GetTimeToken()
        {
            return new TimeToken(this.NetworkTime, this.NetworkTime + TimeSpan.FromDays(1));
        }

        /// <inheritdoc />
        public void Activate()
        {
        }

        /// <inheritdoc />
        public void Dispose()
        {
        }
    }
}
