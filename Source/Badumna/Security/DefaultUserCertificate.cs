﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Security.Cryptography;

using Badumna.Core;
using Badumna.Utilities;

namespace Badumna.Security
{
    class DefaultUserCertificate : CertificateToken
    {
        private DefaultUserCertificate(long userId, Character character, DateTime timeStamp, DateTime expiryTime, byte[] signature, RSACryptoServiceProvider privateKey)
            :base (userId, character, timeStamp, expiryTime, signature, privateKey)
        {
        }

        public static DefaultUserCertificate Create(long userId, string characterName, DateTime timeStamp, PrivateKey userPrivateKey)
        {
            Character character = characterName == null ? null : new Character(-1, characterName);
            return Create(userId, character, timeStamp, userPrivateKey);
        }

        public static DefaultUserCertificate Create(long userId, Character character, DateTime timeStamp, PrivateKey userPrivateKey)
        {
            DateTime expiryTime = timeStamp + TimeSpan.FromDays(2);

            PrivateKey trustedAuthorityKey = new PrivateKey(Resources.DefaultTrustedAuthorityKey);
            byte[] signature = CertificateToken.SignCertificate(userId, character, timeStamp, expiryTime, userPrivateKey.PublicKey.RawData, trustedAuthorityKey.RawData);

            return new DefaultUserCertificate(userId, character, timeStamp, expiryTime, signature, userPrivateKey.RsaService);
        }

        internal override bool OnValidate(ITokenCache tokens)
        {
            return true;
        }
    }
}
