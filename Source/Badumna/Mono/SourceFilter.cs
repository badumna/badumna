﻿#if ANDROID || MONOTOUCH

using System;

// error CS1591: Warning as Error: Missing XML comment for publicly visible type or member
#pragma warning disable 1591

namespace System.Diagnostics
{
    // Summary:
    //     Indicates whether a listener should trace a message based on the source of
    //     a trace.
    public class SourceFilter : TraceFilter
    {
        // Summary:
        //     Initializes a new instance of the System.Diagnostics.SourceFilter class,
        //     specifying the name of the trace source.
        //
        // Parameters:
        //   source:
        //     The name of the trace source.
        public SourceFilter(string source) { }
        // Summary:
        //     Determines whether the trace listener should trace the event.
        //
        // Parameters:
        //   cache:
        //     An object that represents the information cache for the trace event.
        //
        //   source:
        //     The name of the source.
        //
        //   eventType:
        //     One of the enumeration values that identifies the event type.
        //
        //   id:
        //     A trace identifier number.
        //
        //   formatOrMessage:
        //     The format to use for writing an array of arguments or a message to write.
        //
        //   args:
        //     An array of argument objects.
        //
        //   data1:
        //     A trace data object.
        //
        //   data:
        //     An array of trace data objects.
        //
        // Returns:
        //     true if the trace should be produced; otherwise, false.
        //
        // Exceptions:
        //   System.ArgumentNullException:
        //     source is null.
        public override bool ShouldTrace(TraceEventCache cache, string source, TraceEventType eventType, int id, string formatOrMessage, object[] args, object data1, object[] data)
        { return false; }
    }
}

#endif
