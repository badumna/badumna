﻿#if ANDROID || MONOTOUCH

using System;

// error CS1591: Warning as Error: Missing XML comment for publicly visible type or member
#pragma warning disable 1591

namespace System.Diagnostics
{
    // Summary:
    //     Directs tracing or debugging output to either the standard output or the
    //     standard error stream.
    public class ConsoleTraceListener : TextWriterTraceListener
    {
        // Summary:
        //     Initializes a new instance of the System.Diagnostics.ConsoleTraceListener
        //     class with trace output written to the standard output stream.
        public ConsoleTraceListener() { }
        //
        // Summary:
        //     Initializes a new instance of the System.Diagnostics.ConsoleTraceListener
        //     class with an option to write trace output to the standard output stream
        //     or the standard error stream.
        //
        // Parameters:
        //   useErrorStream:
        //     true to write tracing and debugging output to the standard error stream;
        //     false to write tracing and debugging output to the standard output stream.
        public ConsoleTraceListener(bool useErrorStream) { }

		public override void WriteLine (string message)
		{
			Console.WriteLine(message);
		}
	}
}

#endif
