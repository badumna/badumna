﻿#if ANDROID || MONOTOUCH

using System;

// error CS1591: Warning as Error: Missing XML comment for publicly visible type or member
#pragma warning disable 1591

namespace System.Diagnostics
{
    // Summary:
    //     Specifies trace data options to be written to the trace output.
    [Flags]
    public enum TraceOptions
    {
        // Summary:
        //     Do not write any elements.
        None = 0,
        //
        // Summary:
        //     Write the logical operation stack, which is represented by the return value
        //     of the System.Diagnostics.CorrelationManager.LogicalOperationStack property.
        LogicalOperationStack = 1,
        //
        // Summary:
        //     Write the date and time.
        DateTime = 2,
        //
        // Summary:
        //     Write the timestamp, which is represented by the return value of the System.Diagnostics.Stopwatch.GetTimestamp()
        //     method.
        Timestamp = 4,
        //
        // Summary:
        //     Write the process identity, which is represented by the return value of the
        //     System.Diagnostics.Process.Id property.
        ProcessId = 8,
        //
        // Summary:
        //     Write the thread identity, which is represented by the return value of the
        //     System.Threading.Thread.ManagedThreadId property for the current thread.
        ThreadId = 16,
        //
        // Summary:
        //     Write the call stack, which is represented by the return value of the System.Environment.StackTrace
        //     property.
        Callstack = 32,
    }
}

#endif
