﻿#if ANDROID || MONOTOUCH

using System;

// error CS1591: Warning as Error: Missing XML comment for publicly visible type or member
#pragma warning disable 1591

namespace System.Diagnostics
{
    public class TextWriterTraceListener : TraceListener
    {
        // Summary:
        //     Initializes a new instance of the System.Diagnostics.TextWriterTraceListener
        //     class with System.IO.TextWriter as the output recipient.
        public TextWriterTraceListener() { }
        //
        // Summary:
        //     Initializes a new instance of the System.Diagnostics.TextWriterTraceListener
        //     class, using the file as the recipient of the debugging and tracing output.
        //
        // Parameters:
        //   fileName:
        //     The name of the file the System.Diagnostics.TextWriterTraceListener writes
        //     to.
        //
        // Exceptions:
        //   System.ArgumentNullException:
        //     The file is null.
        public TextWriterTraceListener(string fileName) { }
        //
        // Summary:
        //     Writes a message to this instance's System.Diagnostics.TextWriterTraceListener.Writer
        //     followed by a line terminator. The default line terminator is a carriage
        //     return followed by a line feed (\r\n).
        //
        // Parameters:
        //   message:
        //     A message to write.
        public override void WriteLine(string message) { }
        public override void Write(string message) { }
        // Summary:x
        //     Closes the System.Diagnostics.TextWriterTraceListener.Writer so that it no
        //     longer receives tracing or debugging output.
        public override void Close() { }
        //
        // Summary:
        //     Flushes the output buffer for the System.Diagnostics.TextWriterTraceListener.Writer.
        public override void Flush() { }

    }
}

#endif
