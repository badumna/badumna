﻿#if ANDROID || MONOTOUCH

using System;

// error CS1591: Warning as Error: Missing XML comment for publicly visible type or member
#pragma warning disable 1591

namespace System.Diagnostics
{
    public abstract class TraceListener
    {
        private int mIndentSize;
        //
        // Summary:
        //     Gets or sets the number of spaces in an indent.
        //
        // Returns:
        //     The number of spaces in an indent. The default is four spaces.
        //
        // Exceptions:
        //   System.ArgumentOutOfRangeException:
        //     Set operation failed because the value is less than zero.
        public int IndentSize { get { return mIndentSize; } set{ mIndentSize = value;} }
        //
        // Summary:
        //     Gets and sets the trace filter for the trace listener.
        //
        // Returns:
        //     An object derived from the System.Diagnostics.TraceFilter base class.
        public TraceFilter Filter { get; set; }
        //
        // Summary:
        //     Gets or sets the trace output options.
        //
        // Returns:
        //     A bitwise combination of the enumeration values. The default is System.Diagnostics.TraceOptions.None.
        //
        // Exceptions:
        //   System.ArgumentOutOfRangeException:
        //     Set operation failed because the value is invalid.
        public TraceOptions TraceOutputOptions { get; set; }
        //
        // Summary:
        //     When overridden in a derived class, writes a message to the listener you
        //     create in the derived class, followed by a line terminator.
        //
        // Parameters:
        //   message:
        //     A message to write.
        public abstract void WriteLine(string message);
		
		public abstract void Write(string message);
        // Summary:
        //     When overridden in a derived class, closes the output stream so it no longer
        //     receives tracing or debugging output.
        public virtual void Close() { }
        //
        // Summary:
        //     When overridden in a derived class, flushes the output buffer.
        public virtual void Flush() { }
    }
}

#endif
