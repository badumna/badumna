﻿#if ANDROID || MONOTOUCH

using System;

// error CS1591: Warning as Error: Missing XML comment for publicly visible type or member
#pragma warning disable 1591

namespace System.Diagnostics
{
    // Summary:
    //     Provides trace event data specific to a thread and a process.
    public class TraceEventCache
    {
    }
}

#endif
