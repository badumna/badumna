﻿#if ANDROID || MONOTOUCH

using System;

// error CS1591: Warning as Error: Missing XML comment for publicly visible type or member
#pragma warning disable 1591

namespace System.Diagnostics
{
    public sealed class Trace
    {
        //
        // Summary:
        //     Writes an error message to the trace listeners in the System.Diagnostics.Trace.Listeners
        //     collection using the specified array of objects and formatting information.
        //
        // Parameters:
        //   format:
        //     A format string that contains zero or more format items, which correspond
        //     to objects in the args array.
        //
        //   args:
        //     An object array containing zero or more objects to format.
        [Conditional("TRACE")]
        public static void TraceError(string format, params object[] args)
        {
        }
        //
        // Summary:
        //     Writes a warning message to the trace listeners in the System.Diagnostics.Trace.Listeners
        //     collection using the specified array of objects and formatting information.
        //
        // Parameters:
        //   format:
        //     A format string that contains zero or more format items, which correspond
        //     to objects in the args array.
        //
        //   args:
        //     An object array containing zero or more objects to format.
        [Conditional("TRACE")]
        public static void TraceWarning(string format, params object[] args)
        {
        }

        //
        // Summary:
        //     Writes an informational message to the trace listeners in the System.Diagnostics.Trace.Listeners
        //     collection using the specified array of objects and formatting information.
        //
        // Parameters:
        //   format:
        //     A format string that contains zero or more format items, which correspond
        //     to objects in the args array.
        //
        //   args:
        //     An object array containing zero or more objects to format.
        [Conditional("TRACE")]
        public static void TraceInformation(string format, params object[] args)
        {
        }

        // Summary:
        //     Checks for a condition; if the condition is false, outputs a specified message
        //     and displays a message box that shows the call stack.
        //
        // Parameters:
        //   condition:
        //     The conditional expression to evaluate. If the condition is true, the specified
        //     message is not sent and the message box is not displayed.
        //
        //   message:
        //     The message to send to the System.Diagnostics.Trace.Listeners collection.
        [Conditional("TRACE")]
        public static void Assert(bool condition, string message)
        {
        }
    }
}

#endif
