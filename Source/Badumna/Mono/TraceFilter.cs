﻿#if ANDROID || MONOTOUCH

using System;

// error CS1591: Warning as Error: Missing XML comment for publicly visible type or member
#pragma warning disable 1591

namespace System.Diagnostics
{
    public abstract class TraceFilter
    {
        // Summary:
        //     Initializes a new instance of the System.Diagnostics.TraceFilter class.
        protected TraceFilter() { }
        // Summary:
        //     When overridden in a derived class, determines whether the trace listener
        //     should trace the event.
        //
        // Parameters:
        //   cache:
        //     The System.Diagnostics.TraceEventCache that contains information for the
        //     trace event.
        //
        //   source:
        //     The name of the source.
        //
        //   eventType:
        //     One of the System.Diagnostics.TraceEventType values specifying the type of
        //     event that has caused the trace.
        //
        //   id:
        //     A trace identifier number.
        //
        //   formatOrMessage:
        //     Either the format to use for writing an array of arguments specified by the
        //     args parameter, or a message to write.
        //
        //   args:
        //     An array of argument objects.
        //
        //   data1:
        //     A trace data object.
        //
        //   data:
        //     An array of trace data objects.
        //
        // Returns:
        //     true to trace the specified event; otherwise, false.
        public abstract bool ShouldTrace(TraceEventCache cache, string source, TraceEventType eventType, int id, string formatOrMessage, object[] args, object data1, object[] data);
    }
}

#endif
