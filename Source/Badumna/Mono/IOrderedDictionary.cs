﻿#if ANDROID || MONOTOUCH

// error CS1591: Warning as Error: Missing XML comment for publicly visible type or member
#pragma warning disable 1591

namespace System.Collections.Specialized
{
    // Summary:
    //     Represents an indexed collection of key/value pairs.
    public interface IOrderedDictionary : IDictionary, ICollection, IEnumerable
    {
        object this[int index] { get; set; }
        new IDictionaryEnumerator GetEnumerator();
        void Insert(int index, object key, object value);
        void RemoveAt(int index);
    }
}

#endif
