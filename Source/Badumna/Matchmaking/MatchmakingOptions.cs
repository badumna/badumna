﻿//-----------------------------------------------------------------------
// <copyright file="MatchmakingOptions.cs" company="Scalify">
//     Copyright (c) 2012 Scalify. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.Matchmaking
{
    using System;

    using Badumna.Arbitration;

    /// <summary>
    /// Matchmaking manager handle the matchmaking request from the client and passes to the matchmaking server.
    /// </summary>
    public class MatchmakingOptions
    {
        /// <summary>
        /// The arbitration name for the cloud matchmaking arbitration server.
        /// </summary>
        internal const string ArbitrationServer = "com.scalify.matchmaking";

        /// <summary>
        /// Minimum timeout value.
        /// </summary>
        private static TimeSpan minTimeout = TimeSpan.FromSeconds(10);

        /// <summary>
        /// Maximum timeout value.
        /// </summary>
        private static TimeSpan maxTimeout = TimeSpan.FromMinutes(15);

        /// <summary>
        /// The minimum players on the match (should only be between 2-50).
        /// </summary>
        private int minimumPlayers;

        /// <summary>
        /// The maximum players on the match (should only be between 2-50).
        /// </summary>
        private int maximumPlayers;

        /// <summary>
        /// The request timeout (defaults to 10 seconds). Should only be
        /// between 10 - 900 seconds.
        /// </summary>
        private TimeSpan timeout = TimeSpan.FromSeconds(10);

        /// <summary>
        /// Initializes a new instance of the <see cref="MatchmakingOptions"/> class.
        /// </summary>
        public MatchmakingOptions()
        {
        }

        /// <summary>
        /// Gets or sets the minimum players on the match.
        /// </summary>
        /// <remarks>
        /// Number of minimum players have to be between 2 and 50 only.
        /// </remarks>
        /// <exception cref="ArgumentException">Argument exception will be thrown if the value is not between 2 and 50.</exception>
        public int MinimumPlayers
        {
            get
            {
                return this.minimumPlayers;
            }

            set
            {
                this.CheckNumPlayers("minimum", value);
                this.minimumPlayers = value;
            }
        }

        /// <summary>
        /// Gets or sets the maximum players on the match.
        /// </summary>
        /// <remarks>
        /// Number of maximum players have to be between 2 and 50 only.
        /// </remarks>
        /// <exception cref="ArgumentException">Argument exception will be thrown if the value is not between 2 and 50.</exception>
        public int MaximumPlayers
        {
            get
            {
                return this.maximumPlayers;
            }

            set
            {
                this.CheckNumPlayers("maximum", value);
                this.maximumPlayers = value;
            }
        }

        /// <summary>
        /// Gets or sets the matchmaking timeout.
        /// </summary>
        /// <remarks>
        /// By default, the timeout is set to 10 seconds, the value has to be between 10 - 900 seconds (15 minutes).
        /// </remarks>
        /// <exception cref="ArgumentException">Argument exception will be thrown if the value is not between 10 seconds and 15 minutes.</exception>
        public TimeSpan Timeout
        {
            get
            {
                return this.timeout;
            }
            
            set
            {
                if (value < minTimeout || value > maxTimeout)
                {
                    throw new ArgumentException("Matchmaking request timeout must be between 10 seconds and 15 minutes.");
                }

                this.timeout = value;
            }
        }

        /// <summary>
        /// Gets or sets the player attributes.
        /// <para>
        /// PlayerAttribute property is used to mask different player's role on the game.
        /// This property will be ignored if it has a value of 0.
        /// It uses 32 bit mask with bitwise OR logic, a match will be started if the resulting players attributes on the match is 0xFFFFFFFF.
        /// This property is useful when player's role is important in a match.
        /// For an example, in a chess match, one player have to play white pieces and the other play black pieces.
        /// </para>
        /// </summary>
        public uint PlayerAttributes { get; set; }

        /// <summary>
        /// Gets or sets the player group.
        /// <para>
        /// PlayerGroup property is used to divide the players into smaller groups, only players with the same player group can be matched together.
        /// This property will be ignored if it has a value of 0.
        /// This property is useful to group players based on the skill level, different type of matches, different type of game maps, etc. 
        /// </para>
        /// </summary>
        public ushort PlayerGroup { get; set; }

        /// <summary>
        /// Requesting a matchmaking to matchmaking server.
        /// </summary>
        public void Validate()
        {
            this.CheckNumPlayers("minimum", this.MinimumPlayers);
            if (this.MinimumPlayers > this.MaximumPlayers)
            {
                throw new ArgumentException("Number of minimum players must be less than or equal to maximum players.");
            }
        }

        /// <summary>
        /// Checks that num players is in the valid range.
        /// </summary>
        /// <param name="description">The description.</param>
        /// <param name="value">The value.</param>
        private void CheckNumPlayers(string description, int value)
        {
            if (value < 2 || value > 50)
            {
                throw new ArgumentException("Number of " + description + " players must be between 2 and 50.");
            }
        }
    }
}
