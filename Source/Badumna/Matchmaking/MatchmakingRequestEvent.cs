﻿//-----------------------------------------------------------------------
// <copyright file="MatchmakingRequestEvent.cs" company="Scalify">
//     Copyright (c) 2012 Scalify. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.Matchmaking
{
    using System;
    using System.IO;
    using Badumna.Arbitration;

    /// <summary>
    /// Request for matchmaking by passing the information about the given 
    /// </summary>
    internal class MatchmakingRequestEvent : ArbitrationEvent
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MatchmakingRequestEvent"/> class.
        /// </summary>
        public MatchmakingRequestEvent()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MatchmakingRequestEvent"/> class.
        /// </summary>
        /// <param name="applicationId">The application id.</param>
        /// <param name="options">The options.</param>
        public MatchmakingRequestEvent(string applicationId, MatchmakingOptions options)
        {
            this.MinPlayers = options.MinimumPlayers;
            this.MaxPlayers = options.MaximumPlayers;
            this.Timeout = options.Timeout;
            this.PlayerAttributes = options.PlayerAttributes;
            this.PlayerGroup = options.PlayerGroup;
            this.ApplicationId = applicationId;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MatchmakingRequestEvent"/> class.
        /// </summary>
        /// <param name="reader">Binary reader.</param>
        public MatchmakingRequestEvent(BinaryReader reader)
        {
            this.MinPlayers = reader.ReadByte();
            this.MaxPlayers = reader.ReadByte();
            this.Timeout = TimeSpan.FromSeconds(reader.ReadUInt16());
            this.PlayerAttributes = reader.ReadUInt32();
            this.PlayerGroup = reader.ReadUInt16();
            this.ApplicationId = reader.ReadString();
        }

        /// <summary>
        /// Gets the minimum players.
        /// </summary>
        public int MinPlayers { get; private set; }

        /// <summary>
        /// Gets the maximum players.
        /// </summary>
        public int MaxPlayers { get; private set; }

        /// <summary>
        /// Gets the matchmaking timeout
        /// </summary>
        public TimeSpan Timeout { get; private set; }

        /// <summary>
        /// Gets the player attributes.
        /// </summary>
        public uint PlayerAttributes { get; private set; }

        /// <summary>
        /// Gets the player group.
        /// </summary>
        public ushort PlayerGroup { get; private set; }

        /// <summary>
        /// Gets the application id.
        /// </summary>
        public string ApplicationId { get; private set; }

        /// <summary>
        /// Serialize the class.
        /// </summary>
        /// <param name="writer">Binary writer.</param>
        public override void Serialize(BinaryWriter writer)
        {
            writer.Write((byte)this.MinPlayers);
            writer.Write((byte)this.MaxPlayers);
            writer.Write((ushort)this.Timeout.TotalSeconds);
            writer.Write(this.PlayerAttributes);
            writer.Write(this.PlayerGroup);
            writer.Write(this.ApplicationId);
        }
    }
}
