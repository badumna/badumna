﻿//------------------------------------------------------------------------------
// <copyright file="MatchmakingAsyncResult.cs" company="National ICT Australia Limited">
//     Copyright (c) 2013 National ICT Australia Limited. All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace Badumna.Matchmaking
{
    /// <summary>
    /// Asynchronous result of a `BeginMatchmaking` call.
    /// </summary>
    public class MatchmakingAsyncResult : IAsyncResult
    {
        /// <summary>
        /// Lock object for (private) state mutation.
        /// </summary>
        private object stateLock = new object();

        /// <summary>
        /// Wait Handle, created lazily on-demand.
        /// </summary>
        private ManualResetEvent waitHandle;

        /// <summary>
        /// Initializes a new instance of the <see cref="MatchmakingAsyncResult"/> class.
        /// </summary>
        public MatchmakingAsyncResult()
        {
            this.IsCompleted = false;
        }

        /// <inheritdoc/>
        public object AsyncState
        {
            get { return this.Match; }
        }

        /// <inheritdoc/>
        public WaitHandle AsyncWaitHandle
        {
            get
            {
                if (this.waitHandle == null)
                {
                    this.waitHandle = new ManualResetEvent(false);
                }

                return this.waitHandle;
            }
        }

        /// <inheritdoc/>
        public bool CompletedSynchronously
        {
            get { return false; }
        }

        /// <inheritdoc/>
        public bool IsCompleted { get; private set; }

        /// <summary>
        /// Gets a successful match result.
        /// </summary>
        public MatchmakingResult Match { get; private set; }

        /// <summary>
        /// Gets a value indicating whether this <see cref="MatchmakingAsyncResult"/> succeeded.
        /// </summary>
        /// <value>
        ///   <c>true</c> if the operation has completed successfully; otherwise, <c>false</c>.
        /// </value>
        public bool Succeeded
        {
            get
            {
                if (!this.IsCompleted)
                {
                    return false;
                }

                return this.Match != null;
            }
        }

        /// <summary>
        /// Gets the error if the operation failed, otherwise <c>null</c>.
        /// </summary>
        public MatchmakingError? Error { get; private set; }

        /// <summary>
        /// Fails the operation with the specified message.
        /// </summary>
        /// <param name="reason">The reason.</param>
        internal void Fail(MatchmakingError reason)
        {
            this.Error = reason;
            this.MarkComplete();
        }

        /// <summary>
        /// Completes the opertation with the specified result.
        /// </summary>
        /// <param name="result">The result.</param>
        internal void Complete(MatchmakingResult result)
        {
            this.Match = result;
            this.MarkComplete();
        }

        /// <summary>
        /// Marks the operation as complete.
        /// </summary>
        private void MarkComplete()
        {
            lock (this.stateLock)
            {
                if (this.IsCompleted)
                {
                    throw new InvalidOperationException("MatchmakingAsyncResult completed twice");
                }

                this.IsCompleted = true;
                if (this.waitHandle != null)
                {
                    this.waitHandle.Set();
                    ((IDisposable)this.waitHandle).Dispose();
                    this.waitHandle = null;
                }
            }
        }
    }
}
