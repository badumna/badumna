﻿//-----------------------------------------------------------------------
// <copyright file="MatchmakingRequest.cs" company="Scalify">
//     Copyright (c) 2012 Scalify. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.Matchmaking
{
    using System;

    using Badumna.Arbitration;
    using Badumna.Utilities;
    using Badumna.Utilities.Logging;

    /// <summary>
    /// HandleMatchmakingSuccess delegate will be called when the matchmaking completes.
    /// </summary>
    /// <param name="result">The MatchmakingAsyncResult.</param>
    public delegate void HandleMatchmakingResult(MatchmakingAsyncResult result);
    
    /// <summary>
    /// Matchmaking progress delegate is called to notify the user with the current request progress.
    /// </summary>
    /// <param name="update">The progress update.</param>
    public delegate void HandleMatchmakingProgress(MatchmakingProgressEvent update);

    /// <summary>
    /// Matchmaking manager handle the matchmaking request from the client and passes to the matchmaking server.
    /// </summary>
    internal class MatchmakingRequest
    {
        /// <summary>
        /// Arbitrator instance.
        /// </summary>
        private IArbitrator arbitrator;

        /// <summary>
        /// Matchmaking success handler.
        /// </summary>
        private HandleMatchmakingResult onCompletion;

        /// <summary>
        /// Handle matchmaking progress.
        /// </summary>
        private HandleMatchmakingProgress onProgress;

        /// <summary>
        /// The configuration options for the system.
        /// </summary>
        private Options badumnaOptions;

        /// <summary>
        /// Matchmaking Options
        /// </summary>
        private MatchmakingOptions matchmakingOptions;

        /// <summary>
        /// Matchmaking result
        /// </summary>
        private MatchmakingAsyncResult result;

        /// <summary>
        /// Initializes a new instance of the <see cref="MatchmakingRequest"/> class.
        /// </summary>
        /// <param name="arbitrator">The arbitrator.</param>
        /// <param name="badumnaOptions">The badumna options.</param>
        /// <param name="onCompletion">A delegate for notifying completion.</param>
        /// <param name="onProgress">A delegate for reporting progress.</param>
        /// <param name="matchmakingOptions">The matchmaking options.</param>
        internal MatchmakingRequest(
            IArbitrator arbitrator,
            Options badumnaOptions,
            HandleMatchmakingResult onCompletion,
            HandleMatchmakingProgress onProgress,
            MatchmakingOptions matchmakingOptions)
        {
            this.arbitrator = arbitrator;
            this.badumnaOptions = badumnaOptions;
            this.matchmakingOptions = matchmakingOptions;
            this.onCompletion = onCompletion;
            this.onProgress = onProgress;
        }

        /// <summary>
        /// Requesting a matchmaking to matchmaking server.
        /// </summary>
        /// <returns>The MatchmakingAsyncResult</returns>
        public MatchmakingAsyncResult Begin()
        {
            this.matchmakingOptions.Validate();

            if (this.result != null)
            {
                throw new InvalidOperationException("Request has already begun");
            }
            
            this.result = new MatchmakingAsyncResult();

            this.arbitrator.Connect(
                    this.HandleConnectionResult,
                    this.HandleConnectionFailure,
                    this.HandleServerMessage);

            return this.result;
        }

        /// <summary>
        /// Handles the arbitration server connection result.
        /// </summary>
        /// <param name="result">The result of the attempt to connect to the arbitration server.</param>
        private void HandleConnectionResult(ServiceConnectionResultType result)
        {
            if (result == ServiceConnectionResultType.Success)
            {
                this.AnnounceProgress(new MatchmakingProgressEvent(MatchmakingOperation.ConnectionEstablished));
                this.SendMatchRequest();
            }
            else
            {
                this.HandleConnectionFailure();
            }
        }

        /// <summary>
        /// Handles a arbitration server connection failure.
        /// </summary>
        private void HandleConnectionFailure()
        {
            this.MatchmakingFail(MatchmakingError.ConnectionFailed);
        }

        /// <summary>
        /// Handle messages from the arbitration server.
        /// </summary>
        /// <param name="message">A serialized arbitration event.</param>
        private void HandleServerMessage(byte[] message)
        {
            var reply = MatchmakingEventSet.Deserialize(message);

            if (!this.result.IsCompleted)
            {
                if (reply is MatchmakingReplyEvent)
                {
                    // Return the room name.
                    var matchmakingReply = reply as MatchmakingReplyEvent;

                    if (string.IsNullOrEmpty(matchmakingReply.RoomId))
                    {
                        this.MatchmakingFail(MatchmakingError.Timeout);
                    }
                    else
                    {
                        this.result.Complete(new MatchmakingResult(
                            matchmakingReply.RoomId,
                            matchmakingReply.ExpectedPlayers,
                            matchmakingReply.LocalId));

                        this.onCompletion(this.result);
                    }
                }
                else if (reply is MatchmakingProgressEvent)
                {
                    this.AnnounceProgress((reply as MatchmakingProgressEvent));
                }
            }
            else
            {
                Logger.TraceWarning(
                    LogTag.Arbitration | LogTag.Event,
                    "Received matchmaking reply {0}, but result is already complete! (error={1})",
                    reply,
                    this.result.Error);
            }
        }

        /// <summary>
        /// Send match request to the server.
        /// </summary>
        private void SendMatchRequest()
        {
            var request = new MatchmakingRequestEvent(this.badumnaOptions.CloudIdentifier, this.matchmakingOptions);
            this.AnnounceProgress(new MatchmakingProgressEvent(MatchmakingOperation.RequestSent));
            this.arbitrator.SendEvent(MatchmakingEventSet.Serialize(request));
        }

        /// <summary>
        /// Trigger the OnFail event with error message.
        /// </summary>
        /// <param name="reason">The reason.</param>
        private void MatchmakingFail(MatchmakingError reason)
        {
            if (!this.result.IsCompleted)
            {
                this.result.Fail(reason);
                this.onCompletion(this.result);
            }
            else
            {
                Logger.TraceWarning(
                    LogTag.Arbitration | LogTag.Event,
                    "Received matchmaking failure {0}, but result is already complete! (error={1})",
                    reason,
                    this.result.Error);
            }
        }

        /// <summary>
        /// Trigger the Progress event with progress information.
        /// </summary>
        /// <param name="evt">The progress event.</param>
        private void AnnounceProgress(MatchmakingProgressEvent evt)
        {
            var progress = this.onProgress;
            if (progress != null)
            {
                progress(evt);
            }
        }
    }
}
