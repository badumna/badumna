﻿//-----------------------------------------------------------------------
// <copyright file="MatchmakingReplyEvent.cs" company="Scalify">
//     Copyright (c) 2012 Scalify. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.Matchmaking
{
    using System.IO;
    using Badumna.Arbitration;

    /// <summary>
    /// MatchmakingReply event store the information about the room id (scene name) and the number 
    /// of expected players for this specific match.
    /// </summary>
    internal class MatchmakingReplyEvent : ArbitrationEvent
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MatchmakingReplyEvent"/> class.
        /// </summary>
        /// <param name="roomId">Room id (unique scene name)</param>
        /// <param name="expectedPlayers">Number of expected players on the match.</param>
        /// <param name="localId">Player local id on the match.</param>
        public MatchmakingReplyEvent(string roomId, short expectedPlayers, short localId)
        {
            this.RoomId = roomId;
            this.ExpectedPlayers = expectedPlayers;
            this.LocalId = localId;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MatchmakingReplyEvent"/> class.
        /// Deserialize the information from the Binary reader.
        /// </summary>
        /// <param name="reader">Binary reader.</param>
        public MatchmakingReplyEvent(BinaryReader reader)
        {
            this.RoomId = reader.ReadString();
            this.ExpectedPlayers = reader.ReadInt16();
            this.LocalId = reader.ReadInt16();
        }

        /// <summary>
        /// Gets the room id.
        /// </summary>
        public string RoomId { get; private set; }

        /// <summary>
        /// Gets the number of expected players on the match.
        /// </summary>
        public short ExpectedPlayers { get; private set; }

        /// <summary>
        /// Gets the player local id on the match.
        /// </summary>
        public short LocalId { get; private set; }

        /// <summary>
        /// Serialize the class.
        /// </summary>
        /// <param name="writer">Binary writer.</param>
        public override void Serialize(BinaryWriter writer)
        {
            writer.Write(this.RoomId);
            writer.Write(this.ExpectedPlayers);
            writer.Write(this.LocalId);
        }
    }
}
