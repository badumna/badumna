﻿// -----------------------------------------------------------------------
// <copyright file="MatchmakingError.cs" company="Scalify">
//     Copyright (c) 2013 Scalify. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace Badumna.Matchmaking
{
    /// <summary>
    /// Errors that may cause a Matchmaking request to fail.
    /// </summary>
    public enum MatchmakingError
    {
        /// <summary>
        /// The connection to the matchmaking server failed
        /// </summary>
        ConnectionFailed,

        /// <summary>
        /// The request timed out before a match was made
        /// </summary>
        Timeout,
    }
}
