﻿// -----------------------------------------------------------------------
// <copyright file="MatchmakingOperation.cs" company="Scalify">
//     Copyright (c) 2013 Scalify. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;

namespace Badumna.Matchmaking
{
    /// <summary>
    /// An enum describing possible update events in a Matchmaking request.
    /// </summary>
    public enum MatchmakingOperation
    {
        /// <summary>
        /// A connection to the matchmaking server has been made
        /// </summary>
        ConnectionEstablished,

        /// <summary>
        /// The matchamking request has been sent
        /// </summary>
        RequestSent,

        /// <summary>
        /// A player has been added to the match
        /// </summary>
        PlayerAdded,

        /// <summary>
        /// A player has been removed from the match
        /// </summary>
        PlayerRemoved
    }
}
