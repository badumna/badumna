﻿//-----------------------------------------------------------------------
// <copyright file="MatchmakingResult.cs" company="Scalify">
//     Copyright (c) 2012 Scalify. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;

namespace Badumna.Matchmaking
{
    /// <summary>
    /// The result of a successful Matchmaking request
    /// </summary>
    public class MatchmakingResult : IEquatable<MatchmakingResult>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MatchmakingResult"/> class.
        /// </summary>
        /// <param name="roomId">The room id.</param>
        /// <param name="expectedPlayers">The expected players.</param>
        /// <param name="playerId">The player id.</param>
        internal MatchmakingResult(string roomId, int expectedPlayers, int playerId)
        {
            this.RoomId = roomId;
            this.ExpectedPlayers = expectedPlayers;
            this.PlayerId = playerId;
        }

        /// <summary>
        /// Gets the room id.
        /// </summary>
        public string RoomId { get; private set; }

        /// <summary>
        /// Gets the number of expected players.
        /// </summary>
        public int ExpectedPlayers { get; private set; }

        /// <summary>
        /// Gets the id (between 0 and ExpectedPlayers) assigned to the current player.
        /// </summary>
        public int PlayerId { get; private set; }

        /// <inheritdoc/>
        bool IEquatable<MatchmakingResult>.Equals(MatchmakingResult other)
        {
            return other != null
                && other.RoomId == this.RoomId
                && other.ExpectedPlayers == this.ExpectedPlayers
                && other.PlayerId == this.PlayerId;
        }

        /// <inheritdoc/>
        public override string ToString()
        {
            return string.Format("<#MatchmakingResult[{0}], expected={1}, id={2}>", this.RoomId, this.ExpectedPlayers, this.PlayerId);
        }
    }
}
