﻿// -----------------------------------------------------------------------
// <copyright file="MatchmakingProgressEvent.cs" company="Scalify">
//     Copyright (c) 2012 Scalify. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace Badumna.Matchmaking
{
    using System.IO;

    using Badumna.Arbitration;

    /// <summary>
    /// Matchmaking progress event is used to pass the progress info from the server to client.
    /// </summary>
    public class MatchmakingProgressEvent : ArbitrationEvent
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MatchmakingProgressEvent"/> class.
        /// </summary>
        /// <param name="reader">The binary reader.</param>
        public MatchmakingProgressEvent(BinaryReader reader)
        {
            this.Operation = (MatchmakingOperation)reader.ReadByte();
            bool hasSubject = reader.ReadBoolean();
            if (hasSubject)
            {
                this.Subject = reader.ReadString();
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MatchmakingProgressEvent"/> class.
        /// </summary>
        /// <param name="op">The operation.</param>
        /// <param name="subject">The subject.</param>
        internal MatchmakingProgressEvent(MatchmakingOperation op, string subject)
        {
            this.Operation = op;
            this.Subject = subject;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MatchmakingProgressEvent"/> class.
        /// </summary>
        /// <param name="op">The operation.</param>
        internal MatchmakingProgressEvent(MatchmakingOperation op)
            : this(op, null)
        {
        }

        /// <summary>
        /// Gets the operation that has occurred.
        /// </summary>
        public MatchmakingOperation Operation { get; private set; }

        /// <summary>
        /// Gets the name of the subject of this update.
        /// </summary>
        /// <value>
        /// For player-related updates, this contains the player's name. Otherwise, it is null.
        /// </value>
        public string Subject { get; private set; }

        /// <summary>
        /// Returns a human-readable description of the update.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String"/> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            switch (this.Operation)
            {
                case MatchmakingOperation.ConnectionEstablished:
                    return "Connection Established";
                case MatchmakingOperation.RequestSent:
                    return "Request sent";
                case MatchmakingOperation.PlayerAdded:
                    return "Found player: " + this.Subject;
                case MatchmakingOperation.PlayerRemoved:
                    return "Removed player: " + this.Subject;
                default:
                    return "Unknown operation";
            }
        }
        
        /// <summary>
        /// Serialize the class.
        /// </summary>
        /// <param name="writer"> The Binary writer.</param>
        public override void Serialize(BinaryWriter writer)
        {
            writer.Write((byte)this.Operation);

            bool hasSubject = this.Subject != null;
            writer.Write(hasSubject);
            if (hasSubject)
            {
                writer.Write(this.Subject);
            }
        }
    }
}
