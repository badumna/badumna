﻿//-----------------------------------------------------------------------
// <copyright file="MatchmakingEventSet.cs" company="Scalify">
//     Copyright (c) 2012 Scalify. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.Matchmaking
{
    using Badumna.Arbitration;

    /// <summary>
    /// Events used for communication between arbitration server and client
    /// </summary>
    internal sealed class MatchmakingEventSet
    {
        /// <summary>
        /// The arbitrationEventSet to use to serialize and deserialize arbitration events.
        /// </summary>
        private static ArbitrationEventSet eventSet = CreateEventSet();

        /// <summary>
        /// Prevents a default instance of the MatchmakingEventSet class from being created.
        /// </summary>
        /// <remarks>Private constructor to prevent creation of public default constructor.</remarks>
        private MatchmakingEventSet()
        {
        }

        /// <summary>
        /// Deserialize an arbitration event.
        /// </summary>
        /// <param name="message">A serialized arbitration event.</param>
        /// <returns>The deserialized arbitration event.</returns>
        public static ArbitrationEvent Deserialize(byte[] message)
        {
            return eventSet.Deserialize(message);
        }

        /// <summary>
        /// Serialize an arbitrationEvent.
        /// </summary>
        /// <param name="arbitrationEvent">The arbitration event.</param>
        /// <returns>The serialized arbitration event.</returns>
        public static byte[] Serialize(ArbitrationEvent arbitrationEvent)
        {
            return eventSet.Serialize(arbitrationEvent);
        }

        /// <summary>
        /// Create an arbitration event set containing all arbitration events.
        /// </summary>
        /// <returns>A new arbitration event set.</returns>
        private static ArbitrationEventSet CreateEventSet()
        {
            eventSet = new ArbitrationEventSet();
            eventSet.Register(
                typeof(MatchmakingRequestEvent),
                typeof(MatchmakingReplyEvent),
                typeof(MatchmakingProgressEvent));
            return eventSet;
        }
    }
}
