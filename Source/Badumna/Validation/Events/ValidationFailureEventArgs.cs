﻿//-----------------------------------------------------------------------
// <copyright file="ValidationFailureEventArgs.cs" company="scalify pty ltd">
//     copyright (c) 2010 scalify pty ltd. all rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.Validation
{
    using System;

    /// <summary>
    /// Enumeration of possible validation failure reasons.
    /// </summary>
    public enum ValidationFailureReason
    {
        /// <summary>
        /// It is not possible to contact the master server.
        /// </summary>
        ServerUnreachable,

        /// <summary>
        /// No host was available to perform validation.
        /// </summary>
        NoHostAvailable,
    }

    /// <summary>
    /// Event arguments for validation failure events.
    /// </summary>
    public class ValidationFailureEventArgs : EventArgs
    {
        /// <summary>
        /// The reason validation has failed.
        /// </summary>
        private ValidationFailureReason reason;

        /// <summary>
        /// Initializes a new instance of the ValidationFailureEventArgs class.
        /// </summary>
        /// <param name="reason">The reason for the validation failure.</param>
        public ValidationFailureEventArgs(ValidationFailureReason reason)
        {
            this.reason = reason;
        }

        /// <summary>
        /// Gets the reason for the validation failure.
        /// </summary>
        public ValidationFailureReason Reason
        {
            get { return this.reason; }
        }
    }
}
