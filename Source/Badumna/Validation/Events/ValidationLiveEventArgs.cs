﻿//-----------------------------------------------------------------------
// <copyright file="ValidationLiveEventArgs.cs" company="scalify pty ltd">
//     copyright (c) 2010 scalify pty ltd. all rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.Validation
{
    using System;

    /// <summary>
    /// Event arguments for validation going live events.
    /// </summary>
    public class ValidatorOnlineEventArgs : EventArgs
    {
    }
}
