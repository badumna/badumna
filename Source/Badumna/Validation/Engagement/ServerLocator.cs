﻿//-----------------------------------------------------------------------
// <copyright file="ServerLocator.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.Validation.Engagement
{
    using System;
    using Badumna.Core;

    /// <summary>
    /// Interface for the server locator.
    /// </summary>
    internal interface IServerLocator
    {
        /// <summary>
        /// Finds the engagement server's address.
        /// </summary>
        /// <returns>The engagement server's address.</returns>
        PeerAddress FindServer();
    }

    /// <summary>
    /// The server locator will find the address of the engagment server by either:
    ///  - reading it from the config options, or,
    ///  - TODO: looking it up on the DHT.
    /// </summary>
    internal class ServerLocator : IServerLocator
    {
        /// <summary>
        /// Validation configuration options.
        /// </summary>
        private readonly IValidationModule validationModule;

        /// <summary>
        /// Initializes a new instance of the ServerLocator class.
        /// </summary>
        /// <param name="validationModule">Validation configuration options.</param>
        public ServerLocator(IValidationModule validationModule)
        {
            if (validationModule == null)
            {
                throw new ArgumentNullException("validationModule");
            }

            this.validationModule = validationModule;
        }

        /// <inheritdoc/>
        public PeerAddress FindServer()
        {
            if (this.validationModule.Servers.Count < 1)
            {
                return null;
            }

            return this.validationModule.Servers[0].PeerAddress;
        }
    }
}
