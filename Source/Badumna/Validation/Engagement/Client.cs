﻿//-----------------------------------------------------------------------
// <copyright file="Client.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.Validation.Engagement
{
    using System;
    using Badumna.Core;
    using Badumna.Transport;
    using Badumna.Utilities;
    using Badumna.Validation.Allocation;

    /// <summary>
    /// Delegate for handling engagement replies.
    /// </summary>
    /// <param name="result">A value indicating if the request was successful.</param>
    /// <param name="validatorAddress">The address of the allocated validator if succesful.</param>
    internal delegate void EngagementReplyHandler(EngagementRequestResult result, string validatorAddress);

    /// <summary>
    /// Delegate for client factory method.
    /// </summary>
    /// <param name="transportProtocol">The transport protocol.</param>
    /// <param name="eventSceduler">Event scheduler for scheduling timeouts.</param>
    /// <param name="serverProtocol">The server protocol.</param>
    /// <param name="serverLocator">Locator for finding server address.</param>
    /// <param name="networkAddressProvider">Network address provider.</param>
    /// <returns>A newly instantiated client.</returns>
    internal delegate IClient ClientFactory(
        ITransportProtocol transportProtocol,
        INetworkEventScheduler eventSceduler,
        IServerProtocol serverProtocol,
        IServerLocator serverLocator,
        INetworkAddressProvider networkAddressProvider);

    /// <summary>
    /// Interface for engagement client.
    /// </summary>
    internal interface IClient
    {
        /// <summary>
        /// Request a validator be assigned from the server.
        /// </summary>
        /// <param name="replyHandler">A callback for handling the reply.</param>
        /// <exception cref="InvalidOperationException">Thrown when there is an outstanding request.</exception>
        void RequestValidator(EngagementReplyHandler replyHandler);

        /// <summary>
        /// Handle allocation replies from the server.
        /// </summary>
        /// <param name="result">The result of the allocation request.</param>
        /// <param name="validatorAddress">The address of the allocated validator, if successful.</param>
        void HandleAllocationReply(AllocationRequestResult result, string validatorAddress);
    }

    /// <summary>
    /// Engagement client is responsible for requesting validator from server.
    /// </summary>
    internal class Client : IClient
    {
        /// <summary>
        /// Time allowed for full allocation process before timing out.
        /// </summary>
        private const double EngagementTimeoutMilliseconds = 15000.0; // 15 seconds
        
        /// <summary>
        /// The transport protocol.
        /// </summary>
        private readonly ITransportProtocol transportProtocol;

        /// <summary>
        /// For scheduling timeouts.
        /// </summary>
        private readonly INetworkEventScheduler eventScheduler;

        /// <summary>
        /// The protocol defining methods available via RPC on the server.
        /// </summary>
        private readonly IServerProtocol serverProtocol;

        /// <summary>
        /// Locator for finding the server address.
        /// </summary>
        private readonly IServerLocator serverLocator;

        /// <summary>
        /// Provides information about the local network addresses.
        /// </summary>
        private readonly INetworkAddressProvider networkAddressProvider;

        /// <summary>
        /// Flag indicating if there is an outstanding request.
        /// </summary>
        private bool requestInProgress;
        
        /// <summary>
        /// Callback for reporting validator request result.
        /// </summary>
        private EngagementReplyHandler replyHandler;

        /// <summary>
        /// Timeout for engagement requests.
        /// </summary>
        private NetworkEvent timeout;
        
        /// <summary>
        /// Initializes a new instance of the Client class.
        /// </summary>
        /// <param name="transportProtocol">The transport protocol.</param>
        /// <param name="eventScheduler">Event scheduler for scheduling timeouts.</param>
        /// <param name="serverProtocol">Protocol defining methods available via RPC on the server.</param>
        /// <param name="serverLocator">Locator for finding server address.</param>
        /// <param name="networkAddressProvider">Provider of local network address iformation.</param>
        public Client(
            ITransportProtocol transportProtocol,
            INetworkEventScheduler eventScheduler,
            IServerProtocol serverProtocol,
            IServerLocator serverLocator,
            INetworkAddressProvider networkAddressProvider)
        {
            if (transportProtocol == null)
            {
                throw new ArgumentNullException("transportProtocol");
            }

            if (eventScheduler == null)
            {
                throw new ArgumentNullException("eventScheduler");
            }

            if (serverProtocol == null)
            {
                throw new ArgumentNullException("serverProtocol");
            }

            if (serverLocator == null)
            {
                throw new ArgumentNullException("serverLocator");
            }

            if (networkAddressProvider == null)
            {
                throw new ArgumentNullException("networkAddressProvider");
            }

            this.transportProtocol = transportProtocol;
            this.eventScheduler = eventScheduler;
            this.serverProtocol = serverProtocol;
            this.serverLocator = serverLocator;
            this.networkAddressProvider = networkAddressProvider;
        }

        /// <inheritdoc/>
        public void RequestValidator(EngagementReplyHandler replyHandler)
        {
            if (this.requestInProgress)
            {
                throw new InvalidOperationException("An existing request is outstanding.");
            }
            else
            {
                PeerAddress serverAddress = this.serverLocator.FindServer();
                if (serverAddress == null)
                {
                    throw new InvalidOperationException("No server address provided in the configuration.");
                }

                // The server is not permitted to make requests, at present, as that would
                // require two possible reply routes (i.e. via transport protocol message,
                // or directly.
                // The server could be permitted to make requests, if there was a real
                // need for that.
                if (this.networkAddressProvider.HasPrivateAddress(serverAddress))
                {
                    throw new InvalidOperationException(
                        "The master server is not permitted to request a validator for itself.");
                }
                else
                {
                    Logger.TraceInformation(LogTag.Validation, "Sending allocation request to {0}.", serverAddress);
                    this.requestInProgress = true;
                    this.replyHandler = replyHandler;

                    TransportEnvelope envelope = this.transportProtocol.GetMessageFor(
                        serverAddress,
                        QualityOfService.Reliable);
                    this.transportProtocol.RemoteCall(envelope, this.serverProtocol.ValidatorAssignmentRequest);
                    this.transportProtocol.SendMessage(envelope);

                    this.timeout = this.eventScheduler.Schedule(Client.EngagementTimeoutMilliseconds, this.OnRequestTimeout);
                }
            }
        }

        /// <inheritdoc/>
        public void HandleAllocationReply(AllocationRequestResult result, string validatorAddress)
        {
            Logger.TraceInformation(LogTag.Validation, "Engagement client: received allocation reply.");
            if (this.timeout != null)
            {
                this.eventScheduler.Remove(this.timeout);
            }

            this.requestInProgress = false;

            EngagementRequestResult engagementResult;
            if (result == AllocationRequestResult.Success)
            {
                engagementResult = EngagementRequestResult.Success;
            }
            else
            {
                engagementResult = EngagementRequestResult.Failure;
            }

            if (this.replyHandler != null)
            {
                this.replyHandler.Invoke(engagementResult, validatorAddress);
            }
        }

        /// <summary>
        /// Triggered by scheduled timeout event.
        /// </summary>
        private void OnRequestTimeout()
        {
            this.timeout = null;
            this.requestInProgress = false;
            if (this.replyHandler != null)
            {
                this.replyHandler.Invoke(EngagementRequestResult.Timeout, null);
            }
        }
    }
}
