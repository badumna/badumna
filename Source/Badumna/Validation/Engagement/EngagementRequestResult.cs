﻿//-----------------------------------------------------------------------
// <copyright file="EngagementRequestResult.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.Validation.Allocation
{
    /// <summary>
    /// Result status of allocation requests.
    /// </summary>
    internal enum EngagementRequestResult
    {
        /// <summary>
        /// The request was successful and a validator allocated.
        /// </summary>
        Success = 0,

        /// <summary>
        /// The request failed as the server could not allocate a validator.
        /// </summary>
        Failure = 1,

        /// <summary>
        /// The request failed as no response was received in time.
        /// </summary>
        Timeout = 2,
    }
}
