﻿//-----------------------------------------------------------------------
// <copyright file="ServerProtocol.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.Validation.Engagement
{
    using System;
    using Badumna.Core;
    using Badumna.Transport;

    /// <summary>
    /// Interface for the server protocol.
    /// </summary>
    internal interface IServerProtocol
    {
        /// <summary>
        /// Sets the server to forward requests to.
        /// </summary>
        IServer Server { set; }

        /// <summary>
        /// Request the assignment of a validator.
        /// </summary>
        void ValidatorAssignmentRequest();
    }

    /// <summary>
    /// Protocol defining methods available via RPC on the server.
    /// </summary>
    internal class ServerProtocol : IServerProtocol
    {
        /// <summary>
        /// The transport protocol.
        /// </summary>
        private readonly ITransportProtocol transportProtocol;

        /// <summary>
        /// The server to pass RPC calls to.
        /// </summary>
        private IServer server;

        /// <summary>
        /// Initializes a new instance of the ServerProtocol class.
        /// </summary>
        /// <param name="transportProtocol">The transport protocol.</param>
        public ServerProtocol(ITransportProtocol transportProtocol)
        {
            if (transportProtocol == null)
            {
                throw new ArgumentNullException("transportProtocol");
            }

            this.transportProtocol = transportProtocol;

            this.transportProtocol.RegisterMethodsIn(this);
        }

        /// <inheritdoc/>
        public IServer Server
        {
            set { this.server = value; }
        }

        /// <inheritdoc/>
        [ConnectionfulProtocolMethod(ConnectionfulMethod.ValidationValidatorAssignmentRequest)]
        public void ValidatorAssignmentRequest()
        {
            if (this.server != null)
            {
                this.server.HandleValidatorAssignmentRequest(this.transportProtocol.CurrentEnvelope.Source);
            }
        }
    }
}
