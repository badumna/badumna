﻿//-----------------------------------------------------------------------
// <copyright file="ClientProtocol.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.Validation.Engagement
{
    using System;
    using Badumna.Core;
    using Badumna.Transport;
    using Badumna.Utilities;
    using Badumna.Validation.Allocation;

    /// <summary>
    /// Interface for the client protocol.
    /// </summary>
    internal interface IClientProtocol
    {
        /// <summary>
        /// Sets the client that messages from the server are forwarded to.
        /// </summary>
        IClient Client { set; }

        /// <summary>
        /// Receive an allocation reply from the server.
        /// </summary>
        /// <param name="result">The result of the allocation request.</param>
        /// <param name="validatorAddress">The address of the assigned validator, if successful.</param>
        void AllocationReply(AllocationRequestResult result, string validatorAddress);
    }

    /// <summary>
    /// The Client Protocol defines the methods available yto the server via RPC,
    /// and forwards replies to the client.
    /// </summary>
    internal class ClientProtocol : IClientProtocol
    {
        /// <summary>
        /// Client to forward server messages to.
        /// </summary>
        private IClient client;

        /// <summary>
        /// Initializes a new instance of the ClientProtocol class.
        /// </summary>
        /// <param name="transportProtocol">The transport protocol.</param>
        public ClientProtocol(ITransportProtocol transportProtocol)
        {
            if (transportProtocol == null)
            {
                throw new ArgumentNullException("transportProtocol");
            }

            transportProtocol.RegisterMethodsIn(this);
        }

        /// <inheritdoc/>
        public IClient Client
        {
            set
            {
                this.client = value;
            }
        }

        /// <inheritdoc/>
        [ConnectionfulProtocolMethod(ConnectionfulMethod.ValidationValidatorAssignmentReply)]
        public void AllocationReply(AllocationRequestResult result, string validatorAddress)
        {
            Logger.TraceInformation(LogTag.Validation, "Client protocol: received allocation reply.");
            if (this.client != null)
            {
                this.client.HandleAllocationReply(result, validatorAddress);
            }
        }
    }
}
