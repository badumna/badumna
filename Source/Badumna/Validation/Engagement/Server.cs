﻿//-----------------------------------------------------------------------
// <copyright file="Server.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.Validation.Engagement
{
    using System;
    using Badumna.Core;
    using Badumna.Transport;
    using Badumna.Utilities;
    using Badumna.Validation.Allocation;

    /// <summary>
    /// Delegate for server factory method.
    /// </summary>
    /// <param name="transportProtocol">The transport protocol.</param>
    /// <param name="clientProtocol">The client protocol.</param>
    /// <param name="allocator">The allocator.</param>
    /// <returns>A newly instantiated server.</returns>
    internal delegate IServer ServerFactory(
        ITransportProtocol transportProtocol,
        IClientProtocol clientProtocol,
        IAllocator allocator);

    /// <summary>
    /// Interface for server.
    /// </summary>
    internal interface IServer
    {
        /// <summary>
        /// Handle validator assignment requests.
        /// </summary>
        /// <param name="requestorAddress">The address of the requestor.</param>
        void HandleValidatorAssignmentRequest(PeerAddress requestorAddress);
    }

    /// <summary>
    /// Engagement Server responsible for handling validator assignment requests.
    /// </summary>
    internal class Server : IServer
    {
        /// <summary>
        /// The transport protocol.
        /// </summary>
        private readonly ITransportProtocol transportProtocol;

        /// <summary>
        /// Client protocol defining methods available via RPC.
        /// </summary>
        private readonly IClientProtocol clientProtocol;

        /// <summary>
        /// Allocator for allocating a validator.
        /// </summary>
        private readonly IAllocator allocator;

        /// <summary>
        /// Initializes a new instance of the Server class.
        /// </summary>
        /// <param name="transportProtocol">The transport protocol.</param>
        /// <param name="clientProtocol">Client protocol defining available RPCs.</param>
        /// <param name="allocator">Allocator for allocating a validator.</param>
        public Server(
            ITransportProtocol transportProtocol,
            IClientProtocol clientProtocol,
            IAllocator allocator)
        {
            if (transportProtocol == null)
            {
                throw new ArgumentNullException("transportProtocol");
            }

            if (clientProtocol == null)
            {
                throw new ArgumentNullException("clientProtocol");
            }

            if (allocator == null)
            {
                throw new ArgumentNullException("allocator");
            }

            this.transportProtocol = transportProtocol;
            this.clientProtocol = clientProtocol;
            this.allocator = allocator;
        }

        /// <inheritdoc/>
        public void HandleValidatorAssignmentRequest(PeerAddress requestorAddress)
        {
            Logger.TraceInformation(LogTag.Validation, "Received assignment request.");
            this.allocator.RequestValidator(
                requestorAddress,
                (r, a) => this.HandleAllocationReply(requestorAddress, r, a));
        }

        /// <summary>
        /// Handle allocation result.
        /// </summary>
        /// <param name="requestorAddress">The address of the requestor.</param>
        /// <param name="result">The allocation result.</param>
        /// <param name="validatorAddress">The address of the allocated validator, if successful.</param>
        private void HandleAllocationReply(
            PeerAddress requestorAddress,
            AllocationRequestResult result,
            string validatorAddress)
        {
            Logger.TraceInformation(LogTag.Validation, "Engagement server: received allocation reply - sending to client.");
            TransportEnvelope envelope = this.transportProtocol.GetMessageFor(
                requestorAddress,
                QualityOfService.Reliable);
            this.transportProtocol.RemoteCall(envelope, this.clientProtocol.AllocationReply, result, validatorAddress);
            this.transportProtocol.SendMessage(envelope);
        }
    }
}
