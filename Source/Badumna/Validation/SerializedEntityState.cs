﻿//-----------------------------------------------------------------------
// <copyright file="SerializedEntityState.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.Validation
{
    using System;
    using System.Collections.Generic;
    using Badumna.Core;
    using Badumna.Utilities;
    using Badumna.Validation;

    /// <summary>
    /// Interface for object storing entity state.
    /// </summary>
    /// <remarks>The entity state can hold part or all of an entity's state.</remarks>
    public interface ISerializedEntityState
    {
        /// <summary>
        /// Gets a set of flags indicating which properties' values are included in this instance.
        /// </summary>
        BooleanArray Flags { get; }

        /// <summary>
        /// Gets a byte array containing serialized property values.
        /// </summary>
        byte[] Data { get; }
    }

    /// <summary>
    /// A class for representing partial or full entity states.
    /// </summary>
    internal class SerializedEntityState : ISerializedEntityState, IParseable
    {
        /// <summary>
        /// A set of flags indicating which properties' values are included in this instance.
        /// </summary>
        private BooleanArray flags;

        /// <summary>
        /// Byte array containing serialized property values.
        /// </summary>
        private byte[] data;

        /// <summary>
        /// Initializes a new instance of the SerializedEntityState class.
        /// </summary>
        /// <remarks>Parameterless constructor required for instiantion prior to intialization as IParseable.</remarks>
        public SerializedEntityState()
        {
        }

        /// <summary>
        /// Initializes a new instance of the SerializedEntityState class.
        /// </summary>
        /// <param name="flags">A set of flags indicating which properties' values are included in this instance.</param>
        /// <param name="data">Byte array containing serialized property values.</param>
        public SerializedEntityState(BooleanArray flags, byte[] data)
        {
            this.flags = flags;
            this.data = data;
        }

        /// <inheritdoc/>
        public BooleanArray Flags
        {
            get { return this.flags; }
        }

        /// <inheritdoc/>
        public byte[] Data
        {
            get { return this.data; }
        }

        /// <inheritdoc/>
        void IParseable.ToMessage(MessageBuffer message, Type parameterType)
        {
            ((IParseable)this.flags).ToMessage(message, typeof(BooleanArray));
            message.PutObject(this.data, typeof(byte[]));
        }

        /// <inheritdoc/>
        void IParseable.FromMessage(MessageBuffer message)
        {
            this.flags = new BooleanArray();
            ((IParseable)this.flags).FromMessage(message);
            this.data = (byte[])message.GetObject(typeof(byte[]), f => { return Activator.CreateInstance(typeof(byte[]), true); });
        }
    }
}
