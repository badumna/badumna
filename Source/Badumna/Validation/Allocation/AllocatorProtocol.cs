﻿//-----------------------------------------------------------------------
// <copyright file="AllocatorProtocol.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.Validation.Allocation
{
    using System;
    using System.Collections.Generic;
    using Badumna.Core;
    using Badumna.DistributedHashTable;

    /// <summary>
    /// Interface for the allocator protocol.
    /// </summary>
    internal interface IAllocatorProtocol
    {
        /// <summary>
        /// Sets the allocator to forward protocol RPCs to.
        /// </summary>
        IAllocator Allocator { set; }

        /// <summary>
        /// Recieve an allocation reply.
        /// </summary>
        /// <param name="requestorKey">The hash key of the requestor.</param>
        /// <param name="acceptance"><c>true</c> if request was accepted, otherwise <c>false</c>.</param>
        void ReceiveAllocationReply(PeerAddress requestorKey, bool acceptance);
    }

    /// <summary>
    /// The allocator protocol defines the methods available via RPC to allocator
    /// and allocatee during allocation.
    /// </summary>
    internal class AllocatorProtocol : IAllocatorProtocol
    {
        /// <summary>
        /// The DHT protocol. 
        /// </summary>
        private readonly IDhtProtocol dhtProtocol;

        /// <summary>
        /// Allocator to pass RPCs from allocatee to.
        /// </summary>
        private IAllocator allocator;

        /// <summary>
        /// Initializes a new instance of the AllocatorProtocol class.
        /// </summary>
        /// <param name="dhtProtocol">The DHT protocol.</param>
        public AllocatorProtocol(IDhtProtocol dhtProtocol)
        {
            if (dhtProtocol == null)
            {
                throw new ArgumentNullException("dhtProtocol");
            }

            this.dhtProtocol = dhtProtocol;
            this.dhtProtocol.RegisterMethodsIn(this);
        }

        /// <inheritdoc/>
        public IAllocator Allocator
        {
            set
            {
                this.allocator = value;
            }
        }

        /// <inheritdoc/>
        [DhtProtocolMethod(DhtMethod.ValidationAllocationReply)]
        public virtual void ReceiveAllocationReply(PeerAddress requestorKey, bool acceptance)
        {
            if (this.allocator != null)
            {
                PeerAddress replierAddress = this.dhtProtocol.CurrentEnvelope.Source;
                this.allocator.ReceiveAllocationReply(requestorKey, replierAddress, acceptance);
            }
        }
    }
}
