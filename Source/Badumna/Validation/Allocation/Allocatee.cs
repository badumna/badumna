﻿//-----------------------------------------------------------------------
// <copyright file="Allocatee.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.Validation.Allocation
{
    using System;
    using Badumna.Core;
    using Badumna.DistributedHashTable;
    using Badumna.Utilities;

    /// <summary>
    /// Factory delegate for creating validator allocatees.
    /// </summary>
    /// <param name="protocol">The DHT protocol.</param>
    /// <param name="eventScheduler">The network event scheduler.</param>
    /// <param name="allocatorProtocol">The allocator protocol for making RPCs.</param>
    /// <returns>A newly created validator allocatee.</returns>
    internal delegate IAllocatee AllocateeFactory(
    IDhtProtocol protocol,
    INetworkEventScheduler eventScheduler,
    IAllocatorProtocol allocatorProtocol);

    /// <summary>
    /// Interface for Allocatee.
    /// </summary>
    internal interface IAllocatee
    {
        /// <summary>
        /// Gets or sets the delegate for deciding allocation requests.
        /// </summary>
        AllocationDecisionMethod AllocationDecisionMethod { get; set; }

        /// <summary>
        /// Handle a validator allocation request.
        /// </summary>
        /// <param name="requestorKey">The hash key of the requestor.</param>
        void HandleAllocationRequest(PeerAddress requestorKey);
    }

    /// <summary>
    /// The validator allocatee is responsible for responding to allocations.
    /// </summary>
    internal class Allocatee : IAllocatee
    {
        /// <summary>
        /// DHT protocol for sending queries etc.
        /// </summary>
        private readonly IDhtProtocol dhtProtocol;

        /// <summary>
        /// Protocol for replying to allocator.
        /// </summary>
        private readonly IAllocatorProtocol allocatorProtocol;

        /// <summary>
        /// Delegate for deciding allocation requests.
        /// </summary>
        private AllocationDecisionMethod decisionMethod = () => { return false; };

        /// <summary>
        /// Initializes a new instance of the Allocatee class.
        /// </summary>
        /// <param name="dhtProtocol">The DHT protocol.</param>
        /// <param name="eventScheduler">The network event scheduler.</param>
        /// <param name="allocatorProtocol">Protocol for replying to allocator.</param>
        public Allocatee(
            IDhtProtocol dhtProtocol,
            INetworkEventScheduler eventScheduler,
            IAllocatorProtocol allocatorProtocol)
        {
            if (dhtProtocol == null)
            {
                throw new ArgumentNullException("dhtProtocol");
            }

            // TODO: event scheduler not used. Remove.
            if (eventScheduler == null)
            {
                throw new ArgumentNullException("eventScheduler");
            }

            if (allocatorProtocol == null)
            {
                throw new ArgumentNullException("allocatorProtocol");
            }

            this.dhtProtocol = dhtProtocol;
            this.allocatorProtocol = allocatorProtocol;
        }

        /// <inheritdoc/>
        public AllocationDecisionMethod AllocationDecisionMethod
        {
            get
            {
                return this.decisionMethod;
            }

            set
            {
                this.decisionMethod = value;
            }
        }

        /// <summary>
        /// Handle a validator allocation request.
        /// </summary>
        /// <param name="requestorKey">The hash key of the requestor.</param>
        public void HandleAllocationRequest(PeerAddress requestorKey)
        {
            bool acceptance = this.decisionMethod();
            if (acceptance)
            {
                Logger.TraceInformation(LogTag.Validation, "Accepting allocation request.");
            }
            else
            {
                Logger.TraceInformation(LogTag.Validation, "Rejecting allocation request.");
            }

            DhtEnvelope envelope = this.dhtProtocol.GetMessageFor(
                this.dhtProtocol.CurrentEnvelope.Source,
                QualityOfService.Reliable);
            this.dhtProtocol.RemoteCall(envelope, this.allocatorProtocol.ReceiveAllocationReply, requestorKey, acceptance);
            this.dhtProtocol.SendMessage(envelope);
        }
    }
}
