﻿//-----------------------------------------------------------------------
// <copyright file="AllocateeProtocol.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.Validation.Allocation
{
    using System;
    using System.Collections.Generic;
    using Badumna.Core;
    using Badumna.DistributedHashTable;

    /// <summary>
    /// Interface for the allocatee protocol.
    /// </summary>
    internal interface IAllocateeProtocol
    {
        /// <summary>
        /// Sets the allocatee for protocol methods to be forwarded to.
        /// </summary>
        IAllocatee Allocatee { set; }

        /// <summary>
        /// Receive an allocation request.
        /// </summary>
        /// <param name="requestorKey">The hash key of the requestor.</param>
        void AllocationRequest(PeerAddress requestorKey);
    }

    /// <summary>
    /// The allocator protocol defines the methods available via RPC to allocator
    /// and allocatee during allocation.
    /// </summary>
    internal class AllocateeProtocol : IAllocateeProtocol
    {
        /// <summary>
        /// Allocatee to pass RPCs from allocator to.
        /// </summary>
        private IAllocatee allocatee;

        /// <summary>
        /// Initializes a new instance of the AllocateeProtocol class.
        /// </summary>
        /// <param name="dhtProtocol">The DHT protocol.</param>
        public AllocateeProtocol(IDhtProtocol dhtProtocol)
        {
            if (dhtProtocol == null)
            {
                throw new ArgumentNullException("dhtProtocol");
            }

            dhtProtocol.RegisterMethodsIn(this);
        }

        /// <inheritdoc/>
        public IAllocatee Allocatee
        {
            set
            {
                this.allocatee = value;
            }
        }

        /// <inheritdoc/>
        [DhtProtocolMethod(DhtMethod.ValidationAllocationRequest)]
        public void AllocationRequest(PeerAddress requestorKey)
        {
            if (this.allocatee != null)
            {
                this.allocatee.HandleAllocationRequest(requestorKey);
            }
        }
    }
}
