﻿//-----------------------------------------------------------------------
// <copyright file="Allocator.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.Validation.Allocation
{
    using System;
    using System.Collections.Generic;
    using Badumna.Core;
    using Badumna.DistributedHashTable;
    using Badumna.Utilities;

    /// <summary>
    /// Delegate for handling validator allocation replies.
    /// </summary>
    /// <param name="result">A value indicating if the request was successful.</param>
    /// <param name="validatorAddress">The address of the allocated validator if succesful.</param>
    internal delegate void AllocationReplyHandler(AllocationRequestResult result, string validatorAddress);

    /// <summary>
    /// Factory delegate for creating validator allocators.
    /// </summary>
    /// <param name="protocol">The DHT protocol.</param>
    /// <param name="eventScheduler">The network event scheduler.</param>
    /// <param name="allocateeProtocol">The allocator protocol for making RPCs.</param>
    /// <returns>A newly created validator allocator.</returns>
    internal delegate IAllocator AllocatorFactory(
    IDhtProtocol protocol,
    INetworkEventScheduler eventScheduler,
    IAllocateeProtocol allocateeProtocol);

    /// <summary>
    /// Interface for ValidatorAllocator.
    /// </summary>
    internal interface IAllocator
    {
        /// <summary>
        /// Makes a DHT query to find a validator for a given peer.
        /// </summary>
        /// <remarks>If there is already a query in progress, a new one will not be initiated.</remarks>
        /// <param name="requestorAddress">The address of the requeting peer.</param>
        /// <param name="replyHandler">Callback for handling allocation reply.</param>
        /// <returns><c>true</c> if the query was initiated, otherwise <c>false</c></returns>
        bool RequestValidator(PeerAddress requestorAddress, AllocationReplyHandler replyHandler);

        /// <summary>
        /// Handle a reply from an allocatee.
        /// </summary>
        /// <param name="requestorAddress">The address of the requestor.</param>
        /// <param name="replierAddress">The address of the reply.</param>
        /// <param name="acceptance"><c>true</c> if request was accepted, otherwise <c>false</c>.</param>
        void ReceiveAllocationReply(PeerAddress requestorAddress, PeerAddress replierAddress, bool acceptance);
    }

    /// <summary>
    /// The validator allocator is responsible for allocating a validator to a peer.
    /// </summary>
    /// <remarks>To allocate a validator, peers are sequentially queried to see if they can act
    /// as a validator, until one agrees, or the allocation process times out.</remarks>
    internal class Allocator : IAllocator
    {
        /// <summary>
        /// Time allowed for reply to allocation query before timeing out.
        /// </summary>
        private const double QueryTimeoutMilliseconds = 5000.0; // 5 seconds

        /// <summary>
        /// Time allowed for full allocation process before timing out.
        /// </summary>
        private const double AllocationTimeoutMilliseconds = 10000.0; // 10 seconds

        /// <summary>
        /// DHT protocol for sending queries etc.
        /// </summary>
        private readonly IDhtProtocol dhtProtocol;

        /// <summary>
        /// Used to schedule (and cancel) request timeouts.
        /// </summary>
        private readonly INetworkEventScheduler eventScheduler;

        /// <summary>
        /// Protocol for contacting allocatees.
        /// </summary>
        private readonly IAllocateeProtocol allocateeProtocol;

        /// <summary>
        /// Callbacks for handling allocation results.
        /// </summary>
        private Dictionary<PeerAddress, AllocationReplyHandler> replyHandlersByRequestorAddress =
            new Dictionary<PeerAddress, AllocationReplyHandler>();

        /// <summary>
        /// Timeouts for individual allocation queries, to be cancelled when replies are received.
        /// </summary>
        private Dictionary<PeerAddress, NetworkEvent> queryTimeoutsByRequestorKey =
            new Dictionary<PeerAddress, NetworkEvent>();

        /// <summary>
        /// Timeouts for full allocation process, to be cancelled upon success.
        /// </summary>
        private Dictionary<PeerAddress, NetworkEvent> allocationTimeoutsByRequestorKey =
            new Dictionary<PeerAddress, NetworkEvent>();

        /// <summary>
        /// Initializes a new instance of the Allocator class.
        /// </summary>
        /// <param name="dhtProtocol">The DHT protocol.</param>
        /// <param name="eventScheduler">The network event scheduler.</param>
        /// <param name="allocateeProtocol">The protocol for contacting allocatees.</param>
        public Allocator(
            IDhtProtocol dhtProtocol,
            INetworkEventScheduler eventScheduler,
            IAllocateeProtocol allocateeProtocol)
        {
            if (dhtProtocol == null)
            {
                throw new ArgumentNullException("dhtProtocol");
            }

            if (eventScheduler == null)
            {
                throw new ArgumentNullException("eventScheduler");
            }

            if (allocateeProtocol == null)
            {
                throw new ArgumentNullException("allocateeProtocol");
            }

            this.dhtProtocol = dhtProtocol;
            this.eventScheduler = eventScheduler;
            this.allocateeProtocol = allocateeProtocol;
        }

        /// <inheritdoc/>
        public bool RequestValidator(PeerAddress requestorKey, AllocationReplyHandler replyHandler)
        {
            if (this.IsAllocationInProgress(requestorKey))
            {
                return false;
            }

            this.replyHandlersByRequestorAddress.Add(requestorKey, replyHandler);

            this.SendAllocationQuery(requestorKey);

            this.ScheduleAllocationTimeout(requestorKey);

            return true;
        }

        /// <inheritdoc/>
        public void ReceiveAllocationReply(PeerAddress requestorKey, PeerAddress replierAddress, bool acceptance)
        {
            Logger.TraceInformation(LogTag.Validation, "Allocator: Received allocation reply: " + acceptance.ToString());
            if (this.IsAllocationInProgress(requestorKey))
            {
                this.CancelQueryTimeout(requestorKey);

                // Do not allow acceptance by original requestor.
                if (acceptance && !requestorKey.Equals(replierAddress))
                {
                    this.CancelAllocationTimeout(requestorKey);

                    AllocationReplyHandler handler = null;
                    if (this.replyHandlersByRequestorAddress.TryGetValue(requestorKey, out handler))
                    {
                        this.replyHandlersByRequestorAddress.Remove(requestorKey);
                        handler(AllocationRequestResult.Success, replierAddress.ToString());
                    }
                }
                else
                {
                    this.SendAllocationQuery(requestorKey);
                }
            }
        }
        
        /// <summary>
        /// Send a query to a validator allocation request to a randomly selected peer.
        /// </summary>
        /// <param name="requestorKey">The key of the requesting peer.</param>
        private void SendAllocationQuery(PeerAddress requestorKey)
        {
            Logger.TraceInformation(LogTag.Validation, "Sending allocation query.");

            HashKey destinationKey = HashKey.Random();
            DhtEnvelope envelope = this.dhtProtocol.GetMessageFor(destinationKey, QualityOfService.Reliable);
            this.dhtProtocol.RemoteCall(envelope, this.allocateeProtocol.AllocationRequest, requestorKey);
            this.dhtProtocol.SendMessage(envelope);

            this.ScheduleQueryTimeout(requestorKey);
        }

        /// <summary>
        /// Trigger new allocation query upon timeout of previous one.
        /// </summary>
        /// <remarks>Should be scheduled in the network event queue as a timeout.</remarks>
        /// <param name="requestorKey">The hash key of the requestor.</param>
        private void OnRequestTimeout(PeerAddress requestorKey)
        {
            this.queryTimeoutsByRequestorKey.Remove(requestorKey);
            this.SendAllocationQuery(requestorKey);
        }

        /// <summary>
        /// Notify clients of allocation failure by calling reply handler.
        /// </summary>
        /// <remarks>Should be scheduled in the network event queue as a timeout.</remarks>
        /// <param name="requestorKey">The key of the peer that requested the validator.</param>
        private void OnAllocationTimeout(PeerAddress requestorKey)
        {
            this.CancelQueryTimeout(requestorKey);
            this.allocationTimeoutsByRequestorKey.Remove(requestorKey);

            AllocationReplyHandler handler = null;
            if (this.replyHandlersByRequestorAddress.TryGetValue(requestorKey, out handler))
            {
                this.replyHandlersByRequestorAddress.Remove(requestorKey);
                handler.Invoke(AllocationRequestResult.Timeout, string.Empty);
            }
        }

        /// <summary>
        /// Checks to see if an allocation is in progress.
        /// </summary>
        /// <param name="requestorKey">The key of the allocation requestor.</param>
        /// <returns><c>true</c> if an allocation is in progress, otherwise <c>false</c>.</returns>
        private bool IsAllocationInProgress(PeerAddress requestorKey)
        {
            return this.replyHandlersByRequestorAddress.ContainsKey(requestorKey);
        }

        /// <summary>
        /// Schedules a query timeout. 
        /// </summary>
        /// <param name="requestorKey">The key of the allocation requestor.</param>
        private void ScheduleQueryTimeout(PeerAddress requestorKey)
        {
            NetworkEvent queryTimeout = this.eventScheduler.Schedule(
                Allocator.QueryTimeoutMilliseconds,
                () => this.OnRequestTimeout(requestorKey));
            this.queryTimeoutsByRequestorKey.Add(requestorKey, queryTimeout);
        }

        /// <summary>
        /// Schedules an allocation timeout. 
        /// </summary>
        /// <param name="requestorKey">The key of the allocation requestor.</param>
        private void ScheduleAllocationTimeout(PeerAddress requestorKey)
        {
            NetworkEvent allocationTimeout = this.eventScheduler.Schedule(
                Allocator.AllocationTimeoutMilliseconds,
                () => this.OnAllocationTimeout(requestorKey));
            this.allocationTimeoutsByRequestorKey.Add(requestorKey, allocationTimeout);
        }

        /// <summary>
        /// Cancels a query timeout. 
        /// </summary>
        /// <param name="requestorKey">The key of the allocation requestor.</param>
        private void CancelQueryTimeout(PeerAddress requestorKey)
        {
            NetworkEvent timeout = null;
            if (this.queryTimeoutsByRequestorKey.TryGetValue(requestorKey, out timeout))
            {
                this.queryTimeoutsByRequestorKey.Remove(requestorKey);
                this.eventScheduler.Remove(timeout);
            }
        }

        /// <summary>
        /// Cancels an allocation timeout. 
        /// </summary>
        /// <param name="requestorKey">The key of the allocation requestor.</param>
        private void CancelAllocationTimeout(PeerAddress requestorKey)
        {
            NetworkEvent allocationTimeout;
            if (this.allocationTimeoutsByRequestorKey.TryGetValue(requestorKey, out allocationTimeout))
            {
                this.allocationTimeoutsByRequestorKey.Remove(requestorKey);
                this.eventScheduler.Remove(allocationTimeout);
            }
        }
    }
}
