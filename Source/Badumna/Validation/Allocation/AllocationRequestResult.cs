﻿//-----------------------------------------------------------------------
// <copyright file="AllocationRequestResult.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.Validation.Allocation
{
    /// <summary>
    /// Result status of allocation requests.
    /// </summary>
    internal enum AllocationRequestResult
    {
        /// <summary>
        /// The request was successful and a validator allocated.
        /// </summary>
        Success = 0,

        /// <summary>
        /// The request failed as no response was received in time.
        /// </summary>
        Timeout = 1,
    }
}
