﻿//-----------------------------------------------------------------------
// <copyright file="Session.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.Validation.Hosting
{
    using System;
    using System.Collections.Generic;
    using Badumna.Core;
    using Badumna.DataTypes;
    using Badumna.SpatialEntities;
    using Badumna.Transport;
    using Badumna.Utilities;

    /// <summary>
    /// Delegate for session factory method.
    /// </summary>
    /// <param name="peerAddress">The address of the session client.</param>
    /// <param name="clientProtocol">The protocol defining RPCs available on the client.</param>
    /// <param name="transportProtocol">The transport protocol.</param>
    /// <param name="spatialEntityManager">For registering entities with a scene.</param>
    /// <param name="networkFacade">The network facade.</param>
    /// <param name="addressProvider">Provides the peer's public address.</param>
    /// <param name="validatedEntityFactoryMethod">Factory method for creating validated entities.</param>
    /// <param name="validatedEntityRemovalHandler">Callback for handling validated entity removal.</param>
    /// <returns>A new session.</returns>
    internal delegate ISession SessionFactory(
        PeerAddress peerAddress,
        IClientProtocol clientProtocol,
        ITransportProtocol transportProtocol,
        ISpatialEntityManager spatialEntityManager,
        INetworkFacade networkFacade,
        INetworkAddressProvider addressProvider,
        CreateValidatedEntity validatedEntityFactoryMethod,
        RemoveValidatedEntity validatedEntityRemovalHandler);

    /// <summary>
    /// Interface for validation session.
    /// </summary>
    internal interface ISession
    {
        /// <summary>
        /// Handle an entity registration request.
        /// </summary>
        /// <param name="entityType">An integer representing the entity type.</param>
        /// <param name="badumnaId">A unique ID used inside Badumna for this session.</param>
        /// <param name="milestone">The signed milestone state to initialize entity to.</param>
        /// <param name="sceneName">The name of the scene the entity is currently registered with.</param>
        void ReceiveEntityRegistrationRequest(
            uint entityType,
            BadumnaId badumnaId,
            ISignedEntityState milestone,
            QualifiedName sceneName);

        /// <summary>
        /// Handle an entity unregistration request.
        /// </summary>
        /// <param name="localId">The local ID of the entity.</param>
        void ReceiveEntityUnregistrationRequest(ushort localId);

        /// <summary>
        /// Handle entity input for an update.
        /// </summary>
        /// <param name="localId">The local ID of the entity.</param>
        /// <param name="serialNumber">The serial number of the update.</param>
        /// <param name="timePeriod">The interval for the update.</param>
        /// <param name="userInput">The input for the update, serialized in a byte array.</param>
        /// <param name="replyRequired">A value indicating if an update is required in reply.</param>
        void ReceiveEntityInput(
            ushort localId,
            CyclicalID.UShortID serialNumber,
            TimeSpan timePeriod,
            byte[] userInput,
            bool replyRequired);

        /// <summary>
        /// Handle a request to register an entity with a scene.
        /// </summary>
        /// <param name="localId">The local ID of the entity to register.</param>
        /// <param name="sceneName">The name of the scene to register with.</param>
        void ReceiveSceneRegistrationRequest(
            ushort localId,
            QualifiedName sceneName);

        /// <summary>
        /// Handle a request to unregister an entity from a scene.
        /// </summary>
        /// <param name="localId">The local ID of the entity to unregister.</param>
        /// <param name="sceneName">The name of the scene to unregister from.</param>
        void ReceiveSceneUnregistrationRequest(
            ushort localId,
            QualifiedName sceneName);

        /// <summary>
        /// Handle a request to terminate the hosting of all entities for the session client.
        /// </summary>
        void ReceiveTerminationNotice();

        /// <summary>
        /// Send milestones of latest state for each entity.
        /// </summary>
        void SendMilestones();
    }

    /// <summary>
    /// A session is a contiguous period of interaction between a client and server.
    /// </summary>
    internal class Session : ISession
    {
        /// <summary>
        /// The address of the session client.
        /// </summary>
        private readonly PeerAddress clientAddress;

        /// <summary>
        /// The protocol for defining methods available via RPC on the client.
        /// </summary>
        private readonly IClientProtocol clientProtocol;

        /// <summary>
        /// The transport protocol.
        /// </summary>
        private readonly ITransportProtocol transportProtocol;

        /// <summary>
        /// Factory method for creating validated entities.
        /// </summary>
        private readonly CreateValidatedEntity validatedEntityFactoryMethod;
    
        /// <summary>
        /// Callback for handling validated entity removal.
        /// </summary>
        private readonly RemoveValidatedEntity validatedEntityRemovalHandler;
    
        /// <summary>
        /// Delegate for creating hosted entity wrappers.
        /// </summary>
        private readonly HostedEntityWrapperFactory wrapperFactory;

        /// <summary>
        /// Spatial entity manager.
        /// </summary>
        private readonly ISpatialEntityManager spatialEntityManager;

        /// <summary>
        /// Wrappers by entity ID.
        /// </summary>
        private Dictionary<ushort, IHostedEntityWrapper> wrappers = new Dictionary<ushort, IHostedEntityWrapper>();

        /// <summary>
        /// Provides the peer's public address.
        /// </summary>
        private INetworkAddressProvider addressProvider;

        /// <summary>
        /// Initializes a new instance of the Session class.
        /// </summary>
        /// <param name="clientAddress">The address of the session client.</param>
        /// <param name="clientProtocol">The protocol for contacting the client.</param>
        /// <param name="transportProtocol">The transport protocol.</param>
        /// <param name="spatialEntityManager">For registering entities with a scene.</param>
        /// <param name="networkFacade">The network facade for passing to the entity factory.</param>
        /// <param name="addressProvider">Provides the peer's public address.</param>
        /// <param name="validatedEntityFactoryMethod">Factory method for creating validated entities.</param>
        /// <param name="validatedEntityRemovalHandler">Callback for handling validated entity removal.</param>
        public Session(
            PeerAddress clientAddress,
            IClientProtocol clientProtocol,
            ITransportProtocol transportProtocol,
            ISpatialEntityManager spatialEntityManager,
            INetworkFacade networkFacade,
            INetworkAddressProvider addressProvider,
            CreateValidatedEntity validatedEntityFactoryMethod,
            RemoveValidatedEntity validatedEntityRemovalHandler)
            : this(
                clientAddress,
                clientProtocol,
                transportProtocol,
                spatialEntityManager,
                networkFacade,
                addressProvider,
                validatedEntityFactoryMethod,
                validatedEntityRemovalHandler,
                (tp, cp, e, et, bi, sn, ca, em) => new HostedEntityWrapper(tp, cp, e, et, bi, sn, ca, em))
        {
        }

        /// <summary>
        /// Initializes a new instance of the Session class.
        /// </summary>
        /// <param name="clientAddress">The address of the session client.</param>
        /// <param name="clientProtocol">The protocol for contacting the client.</param>
        /// <param name="transportProtocol">The transport protocol.</param>
        /// <param name="spatialEntityManager">For registering entities with a scene.</param>
        /// <param name="networkFacade">The network facade for passing to the entity factory.</param>
        /// <param name="addressProvider">Provides the peer's public address.</param>
        /// <param name="validatedEntityFactoryMethod">Factory method for creating validated entities.</param>
        /// <param name="validatedEntityRemovalHandler">Callback for handling validated entity removal.</param>
        /// <param name="wrapperFactory">Delegate for creating hosted entity wrappers.</param>
        public Session(
            PeerAddress clientAddress,
            IClientProtocol clientProtocol,
            ITransportProtocol transportProtocol,
            ISpatialEntityManager spatialEntityManager,
            INetworkFacade networkFacade,
            INetworkAddressProvider addressProvider,
            CreateValidatedEntity validatedEntityFactoryMethod,
            RemoveValidatedEntity validatedEntityRemovalHandler,
            HostedEntityWrapperFactory wrapperFactory)
        {
            if (clientAddress == null)
            {
                throw new ArgumentNullException("clientAddress");
            }

            if (clientProtocol == null)
            {
                throw new ArgumentNullException("clientProtocol");
            }

            if (transportProtocol == null)
            {
                throw new ArgumentNullException("transportProtocol");
            }

            if (spatialEntityManager == null)
            {
                throw new ArgumentNullException("spatialEntityManager");
            }

            if (networkFacade == null)
            {
                throw new ArgumentNullException("networkFacade");
            }

            if (addressProvider == null)
            {
                throw new ArgumentNullException("addressProvider");
            }

            if (validatedEntityFactoryMethod == null)
            {
                throw new ArgumentNullException("validatedEntityFactoryMethod");
            }

            if (validatedEntityRemovalHandler == null)
            {
                throw new ArgumentNullException("validatedEntityRemovalHandler");
            }

            if (wrapperFactory == null)
            {
                throw new ArgumentNullException("wrapperFactory");
            }

            this.clientAddress = clientAddress;
            this.clientProtocol = clientProtocol;
            this.transportProtocol = transportProtocol;
            this.spatialEntityManager = spatialEntityManager;
            this.addressProvider = addressProvider;
            this.validatedEntityFactoryMethod = validatedEntityFactoryMethod;
            this.validatedEntityRemovalHandler = validatedEntityRemovalHandler;
            this.wrapperFactory = wrapperFactory;
        }

        /// <inheritdoc/>
        public void ReceiveEntityRegistrationRequest(
            uint entityType,
            BadumnaId badumnaId,
            ISignedEntityState milestone,
            QualifiedName sceneName)
        {
            // TODO: Handle re-registration (or re-transition) requests.
            // - if out-of-date milestone provided, force jump to latest milestone
            // - will need to ignore buffered input that predates milestone, but this
            //   should be handled automatically by message serialization.
            if (!this.wrappers.ContainsKey(badumnaId.LocalId))
            {
                // Instantiate entity
                IValidatedEntity entity = this.validatedEntityFactoryMethod.Invoke(entityType);

                // Ignore the request if the factory did not create an entity.
                if (entity != null)
                {
                    badumnaId.Address = this.addressProvider.PublicAddress;
                    entity.Guid = badumnaId;

                    CyclicalID.UShortID initialSerialNumber = new CyclicalID.UShortID(0);

                    if (milestone != null)
                    {
                        // TODO: validated digital signature (need to extend ISignedEntityState to include
                        // authorisation information).
                        entity.RestoreState(milestone.SerialEntityState.Item);
                        initialSerialNumber = milestone.SerialEntityState.SerialNumber;
                    }

                    var wrapper = this.wrapperFactory.Invoke(
                        this.transportProtocol,
                        this.clientProtocol,
                        entity,
                        entityType,
                        badumnaId,
                        initialSerialNumber,
                        this.clientAddress,
                        this.spatialEntityManager);
                    this.wrappers.Add(badumnaId.LocalId, wrapper);

                    if (sceneName != default(QualifiedName))
                    {
                        wrapper.RegisterWithScene(sceneName);
                    }
                }
            }            
        }

        /// <inheritdoc/>
        public void ReceiveEntityUnregistrationRequest(ushort localId)
        {
            IHostedEntityWrapper wrapper;
            if (this.wrappers.TryGetValue(localId, out wrapper))
            {
                wrapper.Terminate();
                this.validatedEntityRemovalHandler(wrapper.Entity);
                this.wrappers.Remove(localId);
            }

            // Ignore unregistration requests for unknown entities.
        }

        /// <inheritdoc/>
        public void ReceiveEntityInput(
            ushort localId,
            CyclicalID.UShortID serialNumber,
            TimeSpan timePeriod,
            byte[] userInput,
            bool replyRequired)
        {
            IHostedEntityWrapper wrapper;
            if (this.wrappers.TryGetValue(localId, out wrapper))
            {
                wrapper.ReceiveInput(serialNumber, timePeriod, userInput, replyRequired);
            }
            else
            {
                this.transportProtocol.SendRemoteCall(
                    this.clientAddress,
                    QualityOfService.Reliable,
                    this.clientProtocol.ReceiveServerError,
                    localId);
            }
        }

        /// <inheritdoc/>
        public void ReceiveSceneRegistrationRequest(ushort localId, QualifiedName sceneName)
        {
            IHostedEntityWrapper wrapper;
            if (this.wrappers.TryGetValue(localId, out wrapper))
            {
                wrapper.RegisterWithScene(sceneName);
            }

            // Ignore scene registration requests for unknown entities.
        }

        /// <inheritdoc/>
        public void ReceiveSceneUnregistrationRequest(ushort localId, QualifiedName sceneName)
        {
            IHostedEntityWrapper wrapper;
            if (this.wrappers.TryGetValue(localId, out wrapper))
            {
                wrapper.UnregisterFromScene(sceneName);
            }

            // Ignore scene unregistration requests for unknown entities.
        }

        /// <inheritdoc/>
        public void ReceiveTerminationNotice()
        {
            foreach (IHostedEntityWrapper wrapper in this.wrappers.Values)
            {
                wrapper.Terminate();
                this.validatedEntityRemovalHandler(wrapper.Entity);
            }

            this.wrappers.Clear();
        }

        /// <inheritdoc/>
        public void SendMilestones()
        {
            foreach (IHostedEntityWrapper wrapper in this.wrappers.Values)
            {
                wrapper.SendMilestone();
            }
        }
    }
}
