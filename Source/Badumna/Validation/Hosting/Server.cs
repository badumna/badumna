﻿//-----------------------------------------------------------------------
// <copyright file="Server.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.Validation.Hosting
{
    using System;
    using System.Collections.Generic;
    using Badumna.Core;
    using Badumna.DataTypes;
    using Badumna.SpatialEntities;
    using Badumna.Transport;
    using Badumna.Utilities;

    /// <summary>
    /// Factory method for creating validation servers.
    /// </summary>
    /// <param name="transportProtocol">The transport protocol.</param>
    /// <param name="clientProtocol">The protocol for sending messages to the client.</param>
    /// <param name="spatialEntityManager">For registering entities with scenes.</param>
    /// <param name="networkFacade">The network facade to inject into validated entities.</param>
    /// <param name="timeKeeper">Provides access to the current time.</param>
    /// <param name="addressProvider">Provides the peer's public address.</param>
    /// <param name="queueApplicationEvent">The method to use to queue events that must run in the application's thread.</param>
    /// <returns>A newly created validation server.</returns>
    internal delegate IServer ServerFactory(
        ITransportProtocol transportProtocol,
        IClientProtocol clientProtocol,
        ISpatialEntityManager spatialEntityManager,
        INetworkFacade networkFacade,
        ITime timeKeeper,
        INetworkAddressProvider addressProvider,
        QueueInvokable queueApplicationEvent);

    /// <summary>
    /// Interface for validation server.
    /// </summary>
    internal interface IServer
    {
        /// <summary>
        /// Configure the server with methods for creating entities and handling their removal.
        /// </summary>
        /// <param name="validatedEntityFactoryMethod">Factory method for creating validated entities.</param>
        /// <param name="validatedEntityRemovalHandler">Callback for handling validated entity removal.</param>
        void Configure(CreateValidatedEntity validatedEntityFactoryMethod, RemoveValidatedEntity validatedEntityRemovalHandler);

        /// <summary>
        /// Handle an entity registration request from client.
        /// </summary>
        /// <param name="source">The source of the request.</param>
        /// <param name="entityType">An integer representing the entity type.</param>
        /// <param name="badumnaId">A unique ID used inside Badumna for this session.</param>
        /// <param name="milestone">The signed milestone state to initialize entity to.</param>
        /// <param name="sceneName">The name of a scene to register with (or an empty string if no scene).</param>
        void ReceiveEntityRegistrationRequest(
            PeerAddress source,
            uint entityType,
            BadumnaId badumnaId,
            ISignedEntityState milestone,
            QualifiedName sceneName);

        /// <summary>
        /// Handle an entity unregistration request from a client.
        /// </summary>
        /// <param name="source">The source of the request.</param>
        /// <param name="localId">The local ID of the entity.</param>
        void ReceiveEntityUnregistrationRequest(PeerAddress source, ushort localId);

        /// <summary>
        /// Handle entity input from client.
        /// </summary>
        /// <param name="clientAddress">The client address</param>
        /// <param name="localId">The local ID of the entity.</param>
        /// <param name="serialNumber">The serial number of the update cycle the input is for.</param>
        /// <param name="timePeriod">The time period of the update cycle.</param>
        /// <param name="userInput">The user input for the entity serialized in a byte array.</param>
        /// <param name="replyRequired">A value indicating if an update is required in reply.</param>
        void ReceiveEntityInput(
            PeerAddress clientAddress,
            ushort localId,
            CyclicalID.UShortID serialNumber,
            TimeSpan timePeriod,
            byte[] userInput,
            bool replyRequired);

        /// <summary>
        /// Handle entity scene registration request from client.
        /// </summary>
        /// <param name="clientAddress">The client address</param>
        /// <param name="localId">The local ID of the entity.</param>
        /// <param name="sceneName">The name of the scene to register with.</param>
        void ReceiveSceneRegistrationRequest(
            PeerAddress clientAddress,
            ushort localId,
            QualifiedName sceneName);

        /// <summary>
        /// Handle entity scene unregistration request from client.
        /// </summary>
        /// <param name="clientAddress">The client address</param>
        /// <param name="localId">The local ID of the entity.</param>
        /// <param name="sceneName">The name of the scene to unregister from.</param>
        void ReceiveSceneUnregistrationRequest(
            PeerAddress clientAddress,
            ushort localId,
            QualifiedName sceneName);

        /// <summary>
        /// Handle a termination notice received from a client.
        /// </summary>
        /// <param name="clientAddress">The client address.</param>
        void ReceiveTerminationNotice(PeerAddress clientAddress);

        /// <summary>
        /// Handle a ping request from a client.
        /// </summary>
        /// <param name="clientAddress">The client Address</param>
        void ReceivePingRequest(PeerAddress clientAddress);

        /// <summary>
        /// Perform regular processing.
        /// </summary>
        void RegularProcessing();

        /// <summary>
        /// Notify validation clients of shutdown.
        /// </summary>
        void Shutdown();
    }

    /// <summary>
    /// The server is responsible for acting upon requests from the client.
    /// </summary>
    internal class Server : IServer
    {
        /// <summary>
        /// Timeout period for clients in milliseconds.
        /// </summary>
        private const int ClientTimeoutMilliseconds = 10000;

        /// <summary>
        /// The transport protocol.
        /// </summary>
        private readonly ITransportProtocol transportProtocol;

        /// <summary>
        /// Client protocol defining methods available via RPC.
        /// </summary>
        private readonly IClientProtocol clientProtocol;

        /// <summary>
        /// SpatialEntityManager for scene registrations.
        /// </summary>
        private readonly ISpatialEntityManager spatialEntityManager;

        /// <summary>
        /// The network facade needs to be injected into validated entities when they are created.
        /// </summary>
        private readonly INetworkFacade networkFacade;
        
        /// <summary>
        /// Provides access to current time.
        /// </summary>
        private readonly ITime timeKeeper;

        /// <summary>
        /// The method to use to queue events that must run in the application's thread.
        /// </summary>
        private readonly QueueInvokable queueApplicationEvent;

        /// <summary>
        /// Provides the peer's public address.
        /// </summary>
        private readonly INetworkAddressProvider addressProvider;

        /// <summary>
        /// Factory method for creating client sessions.
        /// </summary>
        private readonly SessionFactory sessionFactory;

        /// <summary>
        /// Client sessions by client address.
        /// </summary>
        private Dictionary<PeerAddress, ISession> sessions = new Dictionary<PeerAddress, ISession>();

        /// <summary>
        /// Record of when the last message from each client was received (used for timing sessions out).
        /// </summary>
        private Dictionary<PeerAddress, TimeSpan> lastMessageTimes = new Dictionary<PeerAddress, TimeSpan>();

        /// <summary>
        /// Factory method for creating validated entities.
        /// </summary>
        private CreateValidatedEntity validatedEntityFactoryMethod;

        /// <summary>
        /// Callback for handling validated entity removal.
        /// </summary>
        private RemoveValidatedEntity validatedEntityRemovalHandler;

        /// <summary>
        /// Initializes a new instance of the Server class.
        /// </summary>
        /// <param name="transportProtocol">The transport protocol.</param>
        /// <param name="clientProtocol">Client protocol defining available RPCs.</param>
        /// <param name="spatialEntityManager">For registering entities with scenes.</param>
        /// <param name="networkFacade">The network facade to inject into validated entities.</param>
        /// <param name="timeKeeper">Provides access to the current time.</param>
        /// <param name="addressProvider">Provides the peer's public address.</param>
        /// <param name="queueApplicationEvent">The method to use to queue events that must run in the application's thread.</param>
        public Server(
            ITransportProtocol transportProtocol,
            IClientProtocol clientProtocol,
            ISpatialEntityManager spatialEntityManager,
            INetworkFacade networkFacade,
            ITime timeKeeper,
            INetworkAddressProvider addressProvider,
            QueueInvokable queueApplicationEvent)
            : this(
                transportProtocol,
                clientProtocol,
                spatialEntityManager,
                networkFacade,
                timeKeeper,
                addressProvider,
                queueApplicationEvent,
                (ca, cp, tp, em, nf, ap, ef, rh) => new Session(ca, cp, tp, em, nf, ap, ef, rh))                
        {
        }
        
        /// <summary>
        /// Initializes a new instance of the Server class.
        /// </summary>
        /// <param name="transportProtocol">The transport protocol.</param>
        /// <param name="clientProtocol">Client protocol defining available RPCs.</param>
        /// <param name="spatialEntityManager">For registering entities with scenes.</param>
        /// <param name="networkFacade">The network facade to inject into validated entities.</param>
        /// <param name="timeKeeper">Provides access to the current time.</param>
        /// <param name="addressProvider">Provides the peer's public address.</param>
        /// <param name="queueApplicationEvent">The method to use to queue events that must run in the application's thread.</param>
        /// <param name="sessionFactory">Factory method for creating client sessions.</param>
        public Server(
            ITransportProtocol transportProtocol,
            IClientProtocol clientProtocol,
            ISpatialEntityManager spatialEntityManager,
            INetworkFacade networkFacade,
            ITime timeKeeper,
            INetworkAddressProvider addressProvider,
            QueueInvokable queueApplicationEvent,
            SessionFactory sessionFactory)
        {
            if (transportProtocol == null)
            {
                throw new ArgumentNullException("transportProtocol");
            }

            if (clientProtocol == null)
            {
                throw new ArgumentNullException("clientProtocol");
            }

            if (spatialEntityManager == null)
            {
                throw new ArgumentNullException("spatialEntityManager");
            }

            if (networkFacade == null)
            {
                throw new ArgumentNullException("networkFacade");
            }

            if (timeKeeper == null)
            {
                throw new ArgumentNullException("timeKeeper");
            }

            if (addressProvider == null)
            {
                throw new ArgumentNullException("addressProvider");
            }

            if (queueApplicationEvent == null)
            {
                throw new ArgumentNullException("queueApplicationEvent");
            }

            if (sessionFactory == null)
            {
                throw new ArgumentNullException("sessionFactory");
            }

            this.transportProtocol = transportProtocol;
            this.clientProtocol = clientProtocol;
            this.spatialEntityManager = spatialEntityManager;
            this.networkFacade = networkFacade;
            this.timeKeeper = timeKeeper;
            this.addressProvider = addressProvider;
            this.queueApplicationEvent = queueApplicationEvent;
            this.sessionFactory = sessionFactory;
        }

        /// <inheritdoc/>
        public void Configure(
            CreateValidatedEntity validatedEntityFactoryMethod,
            RemoveValidatedEntity validatedEntityRemovalHandler)
        {
            if (validatedEntityFactoryMethod == null)
            {
                throw new ArgumentNullException("validatedEntityFactoryMethod");
            }

            if (validatedEntityRemovalHandler == null)
            {
                throw new ArgumentNullException("validatedEntityRemovalHandler");
            }

            this.validatedEntityFactoryMethod = validatedEntityFactoryMethod;
            this.validatedEntityRemovalHandler = validatedEntityRemovalHandler;
        }

        /// <inheritdoc/>
        public void ReceiveEntityRegistrationRequest(
            PeerAddress source,
            uint entityType,
            BadumnaId badumnaId,
            ISignedEntityState milestone,
            QualifiedName sceneName)
        {
            if (this.validatedEntityFactoryMethod == null ||
                this.validatedEntityRemovalHandler == null)
            {
                throw new InvalidOperationException("Cannot receive entity registration requests before Configure is called.");
            }

            // TODO: Entity ownership needs to be verified against the requestor's
            // account using Dei-authenticated user ID.
            ISession session = null;
            if (!this.sessions.TryGetValue(source, out session))
            {
                session = this.sessionFactory.Invoke(
                    source,
                    this.clientProtocol,
                    this.transportProtocol,
                    this.spatialEntityManager,
                    this.networkFacade,
                    this.addressProvider,
                    this.validatedEntityFactoryMethod,
                    this.validatedEntityRemovalHandler);
                this.sessions.Add(source, session);
            }

            IInvokable entityRegistrationEvent = Apply.Func(
                session.ReceiveEntityRegistrationRequest,
                entityType,
                badumnaId,
                milestone,
                sceneName);
            this.queueApplicationEvent(entityRegistrationEvent);

            this.lastMessageTimes[source] = this.timeKeeper.Now;
        }

        /// <inheritdoc/>
        public void ReceiveEntityUnregistrationRequest(PeerAddress clientAddress, ushort localId)
        {
            ISession session = null;
            if (this.sessions.TryGetValue(clientAddress, out session))
            {
                this.queueApplicationEvent(
                    Apply.Func(session.ReceiveEntityUnregistrationRequest, localId));
            }   
        
            // Do not send error notification for unknown client, as no corrective action required.
        }

        /// <inheritdoc/>
        public void ReceiveEntityInput(
            PeerAddress clientAddress,
            ushort localId,
            CyclicalID.UShortID serialNumber,
            TimeSpan timePeriod,
            byte[] userInput,
            bool replyRequired)
        {
            this.lastMessageTimes[clientAddress] = this.timeKeeper.Now;
            
            ISession session = null;
            if (this.sessions.TryGetValue(clientAddress, out session))
            {
                this.queueApplicationEvent(
                    Apply.Func(session.ReceiveEntityInput, localId, serialNumber, timePeriod, userInput, replyRequired));
            }
            else
            {
                this.transportProtocol.SendRemoteCall(
                    clientAddress,
                    QualityOfService.Reliable,
                    this.clientProtocol.ReceiveServerError,
                    localId);
            }
        }

        /// <inheritdoc/>
        public void ReceiveSceneRegistrationRequest(PeerAddress clientAddress, ushort localId, QualifiedName sceneName)
        {
            ISession session = null;
            if (this.sessions.TryGetValue(clientAddress, out session))
            {
                session.ReceiveSceneRegistrationRequest(localId, sceneName);
            }

            // Messages from unknown clients can be ignored, as any input messages
            // from the client will trigger an error notification.
        }

        /// <inheritdoc/>
        public void ReceiveSceneUnregistrationRequest(PeerAddress clientAddress, ushort localId, QualifiedName sceneName)
        {
            ISession session = null;
            if (this.sessions.TryGetValue(clientAddress, out session))
            {
                session.ReceiveSceneUnregistrationRequest(localId, sceneName);
            }

            // Messages from unknown clients can be ignored, as any input messages
            // from the client will trigger an error notification.
        }

        /// <inheritdoc/>
        public void ReceiveTerminationNotice(PeerAddress clientAddress)
        {
            ISession session = null;
            if (this.sessions.TryGetValue(clientAddress, out session))
            {
                this.queueApplicationEvent(
                    Apply.Func(session.ReceiveTerminationNotice));
                this.sessions.Remove(clientAddress);
                this.lastMessageTimes.Remove(clientAddress);
            }

            // Messages from unknown clients can be ignored, as no corrective action required.
        }

        /// <inheritdoc/>
        public void ReceivePingRequest(PeerAddress clientAddress)
        {
            this.transportProtocol.SendRemoteCall(
                clientAddress,
                QualityOfService.Reliable,
                this.clientProtocol.ReceivePingReply,
                true);
        }

        /// <inheritdoc/>
        public void RegularProcessing()
        {
            this.TimeoutSessions();
        }

        /// <inheritdoc/>
        public void Shutdown()
        {
            foreach (KeyValuePair<PeerAddress, ISession> sessionPair in this.sessions)
            {
                sessionPair.Value.SendMilestones();
                this.transportProtocol.SendRemoteCall(
                    sessionPair.Key,
                    QualityOfService.Reliable,
                    this.clientProtocol.ReceiveTerminationNotice);
                
                // TODO: Need to distinguish between client-instigated termination, and server-instigated
                // shutdown, as replicas should be actively halted in the former case, but should be
                // left to ben picked up by the new validator in the latter.
                sessionPair.Value.ReceiveTerminationNotice();
            }

            this.sessions.Clear();
        }

        /// <summary>
        /// Terminate any sessions that have been dormant for too long.
        /// </summary>
        private void TimeoutSessions()
        {
            TimeSpan now = this.timeKeeper.Now;
            List<PeerAddress> clientsToTimeout = new List<PeerAddress>();
            foreach (KeyValuePair<PeerAddress, TimeSpan> entry in this.lastMessageTimes)
            {
                if (now - entry.Value > TimeSpan.FromMilliseconds(Server.ClientTimeoutMilliseconds))
                {
                    clientsToTimeout.Add(entry.Key);
                }
            }

            foreach (var client in clientsToTimeout)
            {
                // Will be queued for application thread, even though already in the application thread.
                // Not that big of a deal.
                this.ReceiveTerminationNotice(client);
            }
        }
    }
}
