﻿//-----------------------------------------------------------------------
// <copyright file="Flagged.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.Validation.Hosting
{
    using System;
    using Badumna.Utilities;

    /// <summary>
    /// Interface for a flagged item.
    /// </summary>
    /// <typeparam name="T">The type of the item.</typeparam>
    internal interface IFlagged<T>
    {
        /// <summary>
        /// Gets a value indicating whether the item is flagged.
        /// </summary>
        bool Flag { get; }

        /// <summary>
        /// Gets the item.
        /// </summary>
        T Item { get; }
    }

    /// <summary>
    /// An item with an associated boolean flag.
    /// </summary>
    /// <typeparam name="T">The type of the item.</typeparam>
    [Serializable]
    internal class Flagged<T> : IFlagged<T>
    {
        /// <summary>
        /// A boolean flag.
        /// </summary>
        private readonly bool flag;

        /// <summary>
        /// The item being flagged.
        /// </summary>
        private readonly T item;

        /// <summary>
        /// Initializes a new instance of the Flagged class.
        /// </summary>
        /// <param name="flag">The boolean flag associtated with the item.</param>
        /// <param name="item">The item being flagged.</param>
        public Flagged(bool flag, T item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("item");
            }

            this.flag = flag;
            this.item = item;
        }

        /// <inheritdoc/>
        public bool Flag
        {
            get { return this.flag; }
        }

        /// <inheritdoc/>
        public T Item
        {
            get { return this.item; }
        }
    }
}
