﻿//-----------------------------------------------------------------------
// <copyright file="TimedInput.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;

namespace Badumna.Validation.Hosting
{
    /// <summary>
    /// Represents user input for a given time period.
    /// </summary>
    internal interface ITimedInput
    {
        /// <summary>
        /// Gets the time period the user input applies for.
        /// </summary>
        TimeSpan TimePeriod { get; }

        /// <summary>
        /// Gets the user input to be applied for the given period.
        /// </summary>
        byte[] UserInput { get; }
    }

    /// <inheritdoc/>
    internal class TimedInput : ITimedInput
    {
        /// <summary>
        /// The time period the user input applies for.
        /// </summary>
        private readonly TimeSpan timePeriod;

        /// <summary>
        /// The user input to be applied for the given period.
        /// </summary>
        private readonly byte[] userInput;

        /// <summary>
        /// Initializes a new instance of the TimedInput class.
        /// </summary>
        /// <param name="timePeriod">The time period the user input applies for.</param>
        /// <param name="userInput">The user input to be applied for the given period.</param>
        public TimedInput(TimeSpan timePeriod, byte[] userInput)
        {
            if (userInput == null)
            {
                throw new ArgumentNullException("userInput");
            }

            this.timePeriod = timePeriod;
            this.userInput = userInput;
        }

        /// <inheritdoc/>
        public TimeSpan TimePeriod
        {
            get { return this.timePeriod; }
        }

        /// <inheritdoc/>
        public byte[] UserInput
        {
            get { return this.userInput; }
        }
    }
}
