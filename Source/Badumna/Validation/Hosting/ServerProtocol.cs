﻿//-----------------------------------------------------------------------
// <copyright file="ServerProtocol.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.Validation.Hosting
{
    using System;
    using Badumna.Core;
    using Badumna.DataTypes;
    using Badumna.Transport;
    using Badumna.Utilities;

    /// <summary>
    /// Interface for the server protocol.
    /// </summary>
    internal interface IServerProtocol
    {
        /// <summary>
        /// Sets the server that messages from the server are forwarded to.
        /// </summary>
        IServer Server { set; }

        /// <summary>
        /// Handle entity registration request from client.
        /// </summary>
        /// <param name="entityType">An integer representing the type of the entity.</param>
        /// <param name="badumnaId">A unique ID used inside Badumna for this session.</param>
        /// <param name="sceneName">The name of a scene to register with (or an empty string if no scene).</param>
        void ReceiveEntityRegistrationRequest(
            uint entityType,
            BadumnaId badumnaId,
            QualifiedName sceneName);

        /// <summary>
        /// Handle entity transition request from client.
        /// </summary>
        /// <param name="entityType">An integer representing the type of the entity.</param>
        /// <param name="badumnaId">A unique ID used inside Badumna for this session.</param>
        /// <param name="milestone">The signed milestone state to initialize entity to.</param>
        /// <param name="sceneName">The name of a scene to register with (or an empty string if no scene).</param>
        void ReceiveEntityTransitionRequest(
            uint entityType,
            BadumnaId badumnaId,
            SignedEntityState milestone,
            QualifiedName sceneName);

        /// <summary>
        /// Handle an entity unregistration request from a client.
        /// </summary>
        /// <param name="localId">The local ID of the entity.</param>
        void ReceiveEntityUnregistrationRequest(ushort localId);

        /// <summary>
        /// Handle entity input from client.
        /// </summary>
        /// <param name="localId">The local ID of the entity.</param>
        /// <param name="serialNumber">The serial number of the update cycle the input is for.</param>
        /// <param name="timePeriodTicks">The time period of the update cycle in ticks.</param>
        /// <param name="userInput">The user input for the entity serialized in a byte array.</param>
        /// <param name="replyRequired">A value indicating if an update is required in reply.</param>
        void ReceiveEntityInput(
            ushort localId,
            CyclicalID.UShortID serialNumber,
            long timePeriodTicks,
            byte[] userInput,
            bool replyRequired);

        /// <summary>
        /// Handle a request to register an entity with a scene.
        /// </summary>
        /// <param name="localId">The local ID of the entity.</param>
        /// <param name="sceneName">The name of the scene to register with.</param>
        void ReceiveSceneRegistrationRequest(
            ushort localId,
            QualifiedName sceneName);

        /// <summary>
        /// Handle a request to unregister an entity from a scene.
        /// </summary>
        /// <param name="localId">The local ID of the entity.</param>
        /// <param name="sceneName">The name of the scene to unregister from.</param>
        void ReceiveSceneUnregistrationRequest(
            ushort localId,
            QualifiedName sceneName);

        /// <summary>
        /// Handle a termination notice received from a client.
        /// </summary>
        void ReceiveTerminationNotice();

        /// <summary>
        /// Handle a ping from a client making sure the server is still available.
        /// </summary>
        void ReceivePingRequest();
    }

    /// <summary>
    /// The client protocol defines methods available via RPC to the server. It
    /// is responsible for handling messages from the server and forwarding them
    /// to the client.
    /// </summary>
    internal class ServerProtocol : IServerProtocol
    {
        /// <summary>
        /// The transport protocol.
        /// </summary>
        private readonly ITransportProtocol transportProtocol;

        /// <summary>
        /// Server to forward client messages to.
        /// </summary>
        private IServer server;

        /// <summary>
        /// Initializes a new instance of the ServerProtocol class.
        /// </summary>
        /// <param name="transportProtocol">The transport protocol.</param>
        public ServerProtocol(ITransportProtocol transportProtocol)
        {
            if (transportProtocol == null)
            {
                throw new ArgumentNullException("transportProtocol");
            }

            this.transportProtocol = transportProtocol;

            this.transportProtocol.RegisterMethodsIn(this);
        }

        /// <inheritdoc/>
        public IServer Server
        {
            set
            {
                this.server = value;
            }
        }

        /// <inheritdoc/>
        [ConnectionfulProtocolMethod(ConnectionfulMethod.ValidationReceiveEntityRegistrationRequest)]
        public void ReceiveEntityRegistrationRequest(
            uint entityType,
            BadumnaId badumnaId,
            QualifiedName sceneName)
        {
            Logger.TraceInformation(LogTag.Validation, "Received entity registration request.");
            if (this.server != null)
            {
                this.server.ReceiveEntityRegistrationRequest(
                    this.transportProtocol.CurrentEnvelope.Source,
                    entityType,
                    badumnaId,
                    null,
                    sceneName);
            }
        }

        /// <inheritdoc/>
        [ConnectionfulProtocolMethod(ConnectionfulMethod.ValidationReceiveEntityTransitionRequest)]
        public void ReceiveEntityTransitionRequest(
            uint entityType,
            BadumnaId badumnaId,
            SignedEntityState milestone,
            QualifiedName sceneName)
        {
            Logger.TraceInformation(LogTag.Validation, "Received entity transition request.");
            if (this.server != null)
            {
                this.server.ReceiveEntityRegistrationRequest(
                    this.transportProtocol.CurrentEnvelope.Source,
                    entityType,
                    badumnaId,
                    milestone,
                    sceneName);
            }
        }

        /// <inheritdoc/>
        [ConnectionfulProtocolMethod(ConnectionfulMethod.ValidationReceiveEntityUnregistrationRequest)]
        public void ReceiveEntityUnregistrationRequest(ushort localId)
        {
            Logger.TraceInformation(LogTag.Validation, "Received entity unregistration request.");
            if (this.server != null)
            {
                this.server.ReceiveEntityUnregistrationRequest(
                    this.transportProtocol.CurrentEnvelope.Source,
                    localId);
            }
        }

        /// <inheritdoc/>
        [ConnectionfulProtocolMethod(ConnectionfulMethod.ValidationReceiveEntityInput)]
        public void ReceiveEntityInput(
            ushort localId,
            CyclicalID.UShortID serialNumber,
            long timePeriodTicks,
            byte[] userInput,
            bool replyRequired)
        {
            Logger.TraceInformation(LogTag.Validation, "Received entity input.");
            if (this.server != null)
            {
                // TO DO: pass ID for client.
                this.server.ReceiveEntityInput(
                    this.transportProtocol.CurrentEnvelope.Source,
                    localId,
                    serialNumber,
                    TimeSpan.FromTicks(timePeriodTicks),
                    userInput,
                    replyRequired);
            }
        }

        /// <inheritdoc/>
        [ConnectionfulProtocolMethod(ConnectionfulMethod.ValidationReceiveSceneRegistrationRequest)]
        public void ReceiveSceneRegistrationRequest(ushort localId, QualifiedName sceneName)
        {
            Logger.TraceInformation(LogTag.Validation, "Received entity input.");
            if (this.server != null)
            {
                // TO DO: pass ID for client.
                this.server.ReceiveSceneRegistrationRequest(
                    this.transportProtocol.CurrentEnvelope.Source,
                    localId,
                    sceneName);
            }
        }

        /// <inheritdoc/>
        [ConnectionfulProtocolMethod(ConnectionfulMethod.ValidationReceiveSceneUnregistrationRequest)]
        public void ReceiveSceneUnregistrationRequest(ushort localId, QualifiedName sceneName)
        {
            Logger.TraceInformation(LogTag.Validation, "Received entity input.");
            if (this.server != null)
            {
                // TO DO: pass ID for client.
                this.server.ReceiveSceneUnregistrationRequest(
                    this.transportProtocol.CurrentEnvelope.Source,
                    localId,
                    sceneName);
            }
        }

        /// <inheritdoc/>
        [ConnectionfulProtocolMethod(ConnectionfulMethod.ValidationReceiveClientTerminationNotice)]
        public void ReceiveTerminationNotice()
        {
            Logger.TraceInformation(LogTag.Validation, "Received termination notice from client.");
            if (this.server != null)
            {
                // TO DO: pass ID for client.
                this.server.ReceiveTerminationNotice(
                    this.transportProtocol.CurrentEnvelope.Source);
            }
        }

        /// <inheritdoc/>
        [ConnectionfulProtocolMethod(ConnectionfulMethod.ValidationReceivePingRequest)]
        public void ReceivePingRequest()
        {
            Logger.TraceInformation(LogTag.Validation, "Received ping request from client.");
            if (this.server != null)
            {
                // TO DO: pass ID for client.
                this.server.ReceivePingRequest(this.transportProtocol.CurrentEnvelope.Source);
            }
        }
    }
}
