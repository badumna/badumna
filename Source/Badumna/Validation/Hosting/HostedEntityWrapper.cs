﻿//-----------------------------------------------------------------------
// <copyright file="HostedEntityWrapper.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.Validation.Hosting
{
    using System;
    using Badumna.Arbitration;
    using Badumna.Core;
    using Badumna.DataTypes;
    using Badumna.SpatialEntities;
    using Badumna.Transport;
    using Badumna.Utilities;

    /// <summary>
    /// Delegate for hosted entity wrapper factory.
    /// </summary>
    /// <param name="transportProtocol">The transport protocol.</param>
    /// <param name="clientProtocol">The protocol for contacting the client.</param>
    /// <param name="entity">The entity.</param>
    /// <param name="entityType">An integer representing the type of the entity.</param>
    /// <param name="badumnaId">A unique ID used inside Badumna for this session.</param>
    /// <param name="initialSerialNumber">The initial serial number for updates to clients.</param>
    /// <param name="clientAddress">The address of the client.</param>
    /// <param name="spatialEntityManager">For registering entity with a scene.</param>
    /// <returns>A new hosted entity wrapper.</returns>
    internal delegate IHostedEntityWrapper HostedEntityWrapperFactory(
        ITransportProtocol transportProtocol,
        IClientProtocol clientProtocol,
        IValidatedEntity entity,
        uint entityType,
        BadumnaId badumnaId,
        CyclicalID.UShortID initialSerialNumber,
        PeerAddress clientAddress,
        ISpatialEntityManager spatialEntityManager);

    /// <summary>
    /// Interface for hosted entity wrapper.
    /// </summary>
    internal interface IHostedEntityWrapper
    {
        /// <summary>
        /// Gets the validated entity.
        /// </summary>
        IValidatedEntity Entity { get; }

        /// <summary>
        /// Receive an update message from the client.
        /// </summary>
        /// <param name="serialNumber">The serial number for the update message.</param>
        /// <param name="timePeriod">The time interval for the update.</param>
        /// <param name="input">The entity input serialized in a byte array.</param>
        /// <param name="replyRequired">A value indicating if an update is required in reply.</param>
        void ReceiveInput(CyclicalID.UShortID serialNumber, TimeSpan timePeriod, byte[] input, bool replyRequired);

        /// <summary>
        /// Register the entity with a scene to allow it to be replicated in that scene.
        /// </summary>
        /// <param name="sceneName">The name of the scene to register with.</param>
        void RegisterWithScene(QualifiedName sceneName);

        /// <summary>
        /// Register the entity with a scene to stop it being replicated in that scene.
        /// </summary>
        /// <param name="sceneName">The name of the scene to unregister from.</param>
        void UnregisterFromScene(QualifiedName sceneName);

        /// <summary>
        /// Terminate the hosting of the entity.
        /// </summary>
        void Terminate();

        /// <summary>
        /// Send a milestone for the current state to the client.
        /// </summary>
        /// <remarks>Used to send latest milestone before shutting down.</remarks>
        void SendMilestone();
    }

    /// <summary>
    /// Wrapper for validated entity on the host validator.
    /// Responsible for:
    /// - instantiating entity,
    /// - receiving updates from the client,
    /// - updating the entity.
    /// - managing interactions (TODO).
    /// </summary>
    internal class HostedEntityWrapper : IHostedEntityWrapper
    {
        /// <summary>
        /// The number of updates before a milestone update is required.
        /// </summary>
        private const ushort MilestonePeriod = 50;

        /// <summary>
        /// A unique ID used inside Badumna for this session.
        /// </summary>
        private readonly BadumnaId badumnaId;

        /// <summary>
        /// The transort protocol.
        /// </summary>
        private readonly ITransportProtocol transportProtocol;

        /// <summary>
        /// The protocol for contacting the client.
        /// </summary>
        private readonly IClientProtocol clientProtocol;

        /// <summary>
        /// Sequencer for ordering input messages.
        /// </summary>
        private readonly ISequencer<IFlagged<IOrdered<ITimedInput>>> inputSequencer;

        /// <summary>
        /// The entity being wrapped.
        /// </summary>
        private readonly IValidatedEntity entity;

        /// <summary>
        /// An integer representing the type of the entity.
        /// </summary>
        /// <remarks>Passed to entity factory methods (CreateValidatedEntity, and CreateSpatialReplica).</remarks>
        private readonly uint entityType;

        /// <summary>
        /// For registering entity with scenes.
        /// </summary>
        private readonly ISpatialEntityManager spatialEntityManager;

        /// <summary>
        /// The address of the client.
        /// </summary>
        private PeerAddress clientAddress;

        /// <summary>
        /// The scene the entity is currently registered with.
        /// </summary>
        private NetworkScene currentScene;

        /////// <summary>
        /////// The latest acknowledged state.
        /////// </summary>
        ////private IEntityState latestAcknowledgedState;

        /// <summary>
        /// Initializes a new instance of the HostedEntityWrapper class.
        /// </summary>
        /// <param name="transportProtocol">The transport protocol.</param>
        /// <param name="clientProtocol">The protocol for contacting the client.</param>
        /// <param name="entity">The entity being wrapped.</param>
        /// <param name="entityType">An integer representing the type of the entity.</param>
        /// <param name="badumnaId">A unique ID used inside Badumna for this session.</param>
        /// <param name="initialSerialNumber">The initial serial number for updates to clients.</param>
        /// <param name="clientAddress">The address of the client.</param>
        /// <param name="spatialEntityManager">For registering entity with a scene.</param>
        public HostedEntityWrapper(
            ITransportProtocol transportProtocol,
            IClientProtocol clientProtocol,
            IValidatedEntity entity,
            uint entityType,
            BadumnaId badumnaId,
            CyclicalID.UShortID initialSerialNumber,
            PeerAddress clientAddress,
            ISpatialEntityManager spatialEntityManager)
            : this(
                transportProtocol,
                clientProtocol,
                h => new Sequencer<IFlagged<IOrdered<ITimedInput>>>(h),
                entity,
                entityType,
                badumnaId,
                initialSerialNumber,
                clientAddress,
                spatialEntityManager)
        {
        }

        /// <summary>
        /// Initializes a new instance of the HostedEntityWrapper class.
        /// </summary>
        /// <param name="transportProtocol">The transport protocol.</param>
        /// <param name="clientProtocol">The protocol for contacting the client.</param>
        /// <param name="sequencerFactory">Factory for input sequencer.</param>
        /// <param name="entity">The entity being wrapped.</param>
        /// <param name="entityType">An integer representing the type of the entity.</param>
        /// <param name="badumnaId">A unique ID used inside Badumna for this session.</param>
        /// <param name="initialSerialNumber">The initial serial number for updates to clients.</param>
        /// <param name="clientAddress">The address of the client.</param>
        /// <param name="spatialEntityManager">For registering entity with a scene.</param>
        public HostedEntityWrapper(
            ITransportProtocol transportProtocol,
            IClientProtocol clientProtocol,
            SequencerFactory<IFlagged<IOrdered<ITimedInput>>> sequencerFactory,
            IValidatedEntity entity,
            uint entityType,
            BadumnaId badumnaId,
            CyclicalID.UShortID initialSerialNumber,
            PeerAddress clientAddress,
            ISpatialEntityManager spatialEntityManager)
        {
            if (transportProtocol == null)
            {
                throw new ArgumentNullException("transportProtocol");
            }

            if (clientProtocol == null)
            {
                throw new ArgumentNullException("clientProtocol");
            }

            if (sequencerFactory == null)
            {
                throw new ArgumentNullException("sequencerFactory");
            }

            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }

            if (badumnaId == null)
            {
                throw new ArgumentNullException("badumnaId");
            }

            if (clientAddress == null)
            {
                throw new ArgumentNullException("clientAddress");
            }

            if (spatialEntityManager == null)
            {
                throw new ArgumentNullException("spatialEntityManager");
            }

            this.transportProtocol = transportProtocol;
            this.clientProtocol = clientProtocol;
            this.inputSequencer = sequencerFactory(i => this.UpdateEntity(i));
            Logger.TraceInformation(LogTag.Validation, "HostedEntityWrapper expecting update {0}.", initialSerialNumber.Value);
            this.inputSequencer.Initialize(initialSerialNumber);
            this.entity = entity;
            this.entityType = entityType;
            this.badumnaId = badumnaId;
            this.clientAddress = clientAddress;
            this.spatialEntityManager = spatialEntityManager;

            this.SendMilestoneToClient(initialSerialNumber);

            ////this.badumnaId.HostAddress = NetworkContext.CurrentContext.PublicAddress;
            this.entity.AreaOfInterestRadius = 0;
        }

        /// <inheritdoc/>
        public IValidatedEntity Entity
        {
            get { return this.entity; }
        }

        /// <inheritdoc/>
        public void ReceiveInput(
            CyclicalID.UShortID serialNumber,
            TimeSpan timePeriod,
            byte[] input,
            bool replyRequired)
        {
            Logger.TraceInformation(
                LogTag.Validation,
                "HostedEntityWrapper: Received input ({0}, {1}, {2}).",
                serialNumber.Value,
                timePeriod.TotalMilliseconds,
                input.ToString());
            var timedInput = new TimedInput(timePeriod, input);
            var serialTimedInput = new Ordered<ITimedInput>(serialNumber, timedInput);
            var flaggedSerialTimedInput = new Flagged<IOrdered<ITimedInput>>(replyRequired, serialTimedInput);
            this.inputSequencer.ReceiveEvent(serialNumber, flaggedSerialTimedInput);
        }

        /// <inheritdoc/>
        public void RegisterWithScene(QualifiedName sceneName)
        {
            if (this.currentScene != null)
            {
                if (this.currentScene.QualifiedName != sceneName)
                {
                    this.currentScene.UnregisterEntity(this.entity);
                    this.currentScene.Leave();
                }
                else
                {
                    return;
                }
            }

            // Note: currently the Validation module can only joining the normal scene.
            this.currentScene = this.spatialEntityManager.JoinScene(
                sceneName,
                this.CreateSpatialReplica,
                this.RemoveSpatialReplica,
                false) as NetworkScene;
            this.currentScene.RegisterEntity(this.entity, this.entityType);
        }

        /// <inheritdoc/>
        public void UnregisterFromScene(QualifiedName sceneName)
        {
            if (this.currentScene != null)
            {
                if (this.currentScene.QualifiedName == sceneName)
                {
                    this.currentScene.UnregisterEntity(this.entity);
                    this.currentScene.Leave();
                    this.currentScene = null;
                }
            }
        }

        /// <inheritdoc/>
        public void Terminate()
        {
            if (this.currentScene != null)
            {
                this.currentScene.UnregisterEntity(this.entity);
                this.currentScene.Leave();
                this.currentScene = null;
            }
        }

        /// <inheritdoc/>
        [DontPerformCoverage]
        public void SendMilestone()
        {
            // TODO: Send milestone.

            // The milestone is to be sent upon termination.
            // At the moment, it is awkward to send one as all updates and
            // milestones are numbered to match a corresponding input.
        }

        /// <summary>
        /// Process an updated delivered in order by the sequencer.
        /// </summary>
        /// <param name="serialTimedInput">Ordered input with update interval.</param>
        private void UpdateEntity(IFlagged<IOrdered<ITimedInput>> serialTimedInput)
        {
            // Predict latest update
            Logger.TraceInformation(LogTag.Validation, "Updating hosted validated entity");
            this.entity.Update(
                serialTimedInput.Item.Item.TimePeriod,
                serialTimedInput.Item.Item.UserInput,
                UpdateMode.Authoritative);
            if (serialTimedInput.Flag)
            {
                CyclicalID.UShortID serialNumber = serialTimedInput.Item.SerialNumber;
                serialNumber.Increment();
                this.SendUpdateToClient(serialNumber);
            }
        }

        /// <summary>
        /// Send a milestone to the client with a full state update of the entity.
        /// </summary>
        /// <param name="serialNumber">The serial number for the update.</param>
        private void SendMilestoneToClient(CyclicalID.UShortID serialNumber)
        {
            Logger.TraceInformation(LogTag.Validation, "Sending milestone to client.");
            ISerializedEntityState update = this.entity.SaveState();

            // TODO: signing of updates.
            this.transportProtocol.SendRemoteCall(
                this.clientAddress,
                QualityOfService.Reliable,
                this.clientProtocol.ReceiveEntityStateMilestone,
                this.badumnaId.LocalId,
                serialNumber,
                update as SerializedEntityState,
                new byte[0]);

            // Flag for update for replication to other peers.
            this.spatialEntityManager.MarkForUpdate(this.entity, update.Flags);
        }

        /// <summary>
        /// Sends an update to the client as a partial update or a signed milestone.
        /// </summary>
        /// <param name="serialNumber">The serialNumber for the update.</param>
        private void SendUpdateToClient(CyclicalID.UShortID serialNumber)
        {
            // TODO: There's probably a better way to decide whether to send
            // milestone or partial update.
            Logger.TraceInformation(LogTag.Validation, "Creating entity update (or milestone).");

            if (serialNumber.Value % HostedEntityWrapper.MilestonePeriod == 0)
            {
                this.SendMilestoneToClient(serialNumber);
            }
            else
            {
                Logger.TraceInformation(LogTag.Validation, "Sending update to client.");

                ISerializedEntityState update = this.entity.CreateUpdate();
                this.transportProtocol.SendRemoteCall(
                    this.clientAddress,
                    QualityOfService.Reliable,
                    this.clientProtocol.ReceiveEntityStateUpdate,
                    this.badumnaId.LocalId,
                    serialNumber,
                    update as SerializedEntityState);

                // Flag for update for replication to other peers.
                this.spatialEntityManager.MarkForUpdate(this.entity, update.Flags);                
            }
        }

        /// <summary>
        /// Stub delegate for passing to JoinScene - should never actually be called,
        /// but will make sure that it is a separate scene to any one the application joins.
        /// </summary>
        /// <param name="scene">The scene that the replica is from.</param>
        /// <param name="entityId">The replica's ID.</param>
        /// <param name="entityType">An integer indicating the type of entity.</param>
        /// <exception cref="ValidationException">Thrown every call.</exception>
        /// <returns>Nothing, as it will always throw an exception.</returns>
        private IReplicableEntity CreateSpatialReplica(NetworkScene scene, BadumnaId entityId, uint entityType)
        {
            throw new ValidationException("Validators should never create replicas.");
        }

        /// <summary>
        /// Stub delegate for passing to JoinScene - should never actually be called,
        /// but will make sure that it is a separate scene to any one the application joins.
        /// </summary>
        /// <param name="scene">The scene that the replica is from.</param>
        /// <param name="replica">The replica to remove.</param>
        /// <exception cref="ValidationException">Thrown every call.</exception>
        private void RemoveSpatialReplica(NetworkScene scene, IReplicableEntity replica)
        {
            throw new ValidationException("Validators should never remove replicas.");
        }
    }
}
