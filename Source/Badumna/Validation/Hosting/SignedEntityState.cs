﻿//-----------------------------------------------------------------------
// <copyright file="SignedEntityState.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.Validation.Hosting
{
    using System;
    using Badumna.Core;
    using Badumna.Utilities;

    /// <summary>
    /// Interface for a signed entity state.
    /// </summary>
    internal interface ISignedEntityState
    {
        /// <summary>
        /// Gets the serial memento.
        /// </summary>
        IOrdered<ISerializedEntityState> SerialEntityState { get; }

        /// <summary>
        /// Verify the integrity of the digitally signed serial memento.
        /// </summary>
        /// <param name="publicKey">The public key of the signer.</param>
        /// <returns><c>true</c> if signature is verified, othersize <c>false</c>.</returns>
        bool Verify(object publicKey);
    }
    
    /// <summary>
    /// A signed entity state holds:
    ///  - a serial memento storing an entity's state at a given sequence point.
    ///  - a digital signature verifying the integrity of the memento
    /// </summary>
    [Serializable]
    internal class SignedEntityState : ISignedEntityState, IParseable
    {
        /// <summary>
        /// A memento storing an entity's state.
        /// </summary>
        private IOrdered<ISerializedEntityState> serialEntityState;

        /// <summary>
        /// The signature.
        /// </summary>
        private byte[] signature;

        /// <summary>
        /// Initializes a new instance of the SignedEntityState class.
        /// </summary>
        /// <param name="serialMemento">The state of an entity at a particular moment.</param>
        /// <param name="signature">A digital signature for the memento.</param>
        public SignedEntityState(IOrdered<ISerializedEntityState> serialMemento, byte[] signature)
        {
            if (serialMemento == null)
            {
                throw new ArgumentNullException("serialMemento");
            }

            if (signature == null)
            {
                throw new ArgumentNullException("signature");
            }

            this.serialEntityState = serialMemento;
            this.signature = signature;
        }

        /// <summary>
        /// Initializes a new instance of the SignedEntityState class.
        /// </summary>
        /// <remarks>Parameterless constructor for IParseable.</remarks>
        public SignedEntityState()
        {
        }

        /// <inheritdoc/>
        public IOrdered<ISerializedEntityState> SerialEntityState
        {
            get { return this.serialEntityState; }
        }

        /// <inheritdoc/>
        public bool Verify(object publicKey)
        {
            // TODO: Implement verification.
            return true;
        }

        /// <inheritdoc/>
        public void ToMessage(MessageBuffer message, Type parameterType)
        {
            this.SerialEntityState.SerialNumber.ToMessage(message, typeof(CyclicalID.UShortID));
            ((IParseable)((SerializedEntityState)this.serialEntityState.Item)).ToMessage(message, null);
            message.PutObject(this.signature, typeof(byte[]));
        }

        /// <inheritdoc/>
        public void FromMessage(MessageBuffer message)
        {
            CyclicalID.UShortID serialNumber = new CyclicalID.UShortID();
            serialNumber.FromMessage(message);
            SerializedEntityState state = new SerializedEntityState();
            ((IParseable)state).FromMessage(message);
            this.signature = (byte[])message.GetObject(typeof(byte[]), f => { return Activator.CreateInstance(typeof(byte[]), true); });
            this.serialEntityState = new Ordered<ISerializedEntityState>(serialNumber, state);
        }
    }
}
