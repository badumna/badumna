﻿//-----------------------------------------------------------------------
// <copyright file="ClientProtocol.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.Validation.Hosting
{
    using System;
    using System.Collections.Generic;
    using Badumna.Core;
    using Badumna.Transport;
    using Badumna.Utilities;

    /// <summary>
    /// Interface for the client protocol.
    /// </summary>
    internal interface IClientProtocol
    {
        /// <summary>
        /// Sets the client that messages from the server are forwarded to.
        /// </summary>
        IClient Client { set; }

        /// <summary>
        /// Handle entity initial state received from server.
        /// </summary>
        /// <param name="localId">The local ID of the entity.</param>
        /// <param name="sequenceNumber">The sequence number for the update.</param>
        /// <param name="state">The serialized state, or partial state, of the entity.</param>
        void ReceiveEntityStateUpdate(ushort localId, CyclicalID.UShortID sequenceNumber, SerializedEntityState state);

        /// <summary>
        /// Handle entity milestone state received from server.
        /// </summary>
        /// <param name="localId">The local ID of the entity.</param>
        /// <param name="serialNumber">The serial number for the update.</param>
        /// <param name="milestone">The milestone state of the entity.</param>
        /// <param name="signature">A digital signature for the ordered update.</param>
        void ReceiveEntityStateMilestone(
            ushort localId,
            CyclicalID.UShortID serialNumber,
            SerializedEntityState milestone,
            byte[] signature);

        /// <summary>
        /// Handle a termination notice received from the server.
        /// </summary>
        void ReceiveTerminationNotice();

        /// <summary>
        /// Handle error responses from the server.
        /// </summary>
        /// <param name="localId">The local ID of the entity whose request caused the error.</param>
        void ReceiveServerError(ushort localId);

        /// <summary>
        /// Handle a ping reply from the server.
        /// </summary>
        /// <param name="serverIsAvailable">A value indicating whether the server is still available.</param>
        void ReceivePingReply(bool serverIsAvailable);
    }

    /// <summary>
    /// The client protocol defines methods available via RPC to the server. It
    /// is responsible for handling messages from the server and forwarding them
    /// to the client.
    /// </summary>
    internal class ClientProtocol : IClientProtocol
    {
        /// <summary>
        /// The transport protocol.
        /// </summary>
        private readonly ITransportProtocol transportProtocol;

        /// <summary>
        /// Client to forward server messages to.
        /// </summary>
        private IClient client;

        /// <summary>
        /// Initializes a new instance of the ClientProtocol class.
        /// </summary>
        /// <param name="transportProtocol">The transport protocol.</param>
        public ClientProtocol(ITransportProtocol transportProtocol)
        {
            if (transportProtocol == null)
            {
                throw new ArgumentNullException("transportProtocol");
            }

            this.transportProtocol = transportProtocol;
            this.transportProtocol.RegisterMethodsIn(this);
        }

        /// <inheritdoc/>
        public IClient Client
        {
            set
            {
                this.client = value;
            }
        }

        /// <inheritdoc/>
        [ConnectionfulProtocolMethod(ConnectionfulMethod.ValidationReceiveEntityStateUpdate)]
        public void ReceiveEntityStateUpdate(ushort localId, CyclicalID.UShortID sequenceNumber, SerializedEntityState state)
        {
            Logger.TraceInformation(LogTag.Validation, "Received entity state update.");
            if (this.client != null)
            {
                this.client.ReceiveEntityStateUpdate(
                    this.transportProtocol.CurrentEnvelope.Source,
                    localId,
                    new Ordered<ISerializedEntityState>(sequenceNumber, state));
            }
        }

        /// <inheritdoc/>
        [ConnectionfulProtocolMethod(ConnectionfulMethod.ValidationReceiveEntityStateMilestone)]
        public void ReceiveEntityStateMilestone(
            ushort localId,
            CyclicalID.UShortID serialNumber,
            SerializedEntityState milestone,
            byte[] signature)
        {
            Logger.TraceInformation(LogTag.Validation, "Received milestone.");
            if (this.client != null)
            {
                this.client.ReceiveEntityStateMilestone(
                    this.transportProtocol.CurrentEnvelope.Source,
                    localId,
                    new SignedEntityState(new Ordered<ISerializedEntityState>(serialNumber, milestone), signature));
            }
        }

        /// <inheritdoc/>
        [ConnectionfulProtocolMethod(ConnectionfulMethod.ValidationReceiveServerTerminationNotice)]
        public void ReceiveTerminationNotice()
        {
            Logger.TraceInformation(LogTag.Validation, "Received termination notice.");
            if (this.client != null)
            {
                this.client.ReceiveTerminationNotice(this.transportProtocol.CurrentEnvelope.Source);
            }
        }

        /// <inheritdoc/>
        [ConnectionfulProtocolMethod(ConnectionfulMethod.ValidationReceiveServerError)]
        public void ReceiveServerError(ushort localId)
        {
            Logger.TraceInformation(LogTag.Validation, "Received termination notice.");
            if (this.client != null)
            {
                this.client.ReceiveServerError(this.transportProtocol.CurrentEnvelope.Source, localId);
            }
        }

        /// <inheritdoc/>
        [ConnectionfulProtocolMethod(ConnectionfulMethod.ValidationReceivePingReply)]
        public void ReceivePingReply(bool serverIsAvailable)
        {
            Logger.TraceInformation(LogTag.Validation, "Received ping reply.");
            if (this.client != null)
            {
                this.client.ReceivePingReply(this.transportProtocol.CurrentEnvelope.Source, serverIsAvailable);
            }
        }
    }
}
