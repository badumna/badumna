﻿//-----------------------------------------------------------------------
// <copyright file="Client.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.Validation.Hosting
{
    using System;
    using System.Collections.Generic;
    using Badumna.Core;
    using Badumna.DataTypes;
    using Badumna.SpatialEntities;
    using Badumna.Transport;
    using Badumna.Utilities;
    using Badumna.Validation.Allocation;

    /// <summary>
    /// Factory method for creating validation clients.
    /// </summary>
    /// <param name="transportProtocol">The transport protocol.</param>
    /// <param name="serverProtocol">The server protocol.</param>
    /// <param name="engagementClient">Client for engaging services of a validator.</param>
    /// <param name="peerConnectivityNotifier">Notifies peer connection establishment and loss.</param>
    /// <param name="timeKeeper">Provides access to current time.</param>
    /// <param name="queueApplicationEvent">The method to use to queue events that must run in the application's thread.</param>
    /// <param name="generateBadumnaId">A delegate that will generate a unique BadumnaId associated with this peer.</param>
    /// <returns>A newly created validation client.</returns>
    internal delegate IClient ClientFactory(
            ITransportProtocol transportProtocol,
            IServerProtocol serverProtocol,
            Engagement.IClient engagementClient,
            IPeerConnectionNotifier peerConnectivityNotifier,
            ITime timeKeeper,
            QueueInvokable queueApplicationEvent,
            GenericCallBackReturn<BadumnaId> generateBadumnaId);

    /// <summary>
    /// Interface for validation client.
    /// </summary>
    internal interface IClient
    {
        /// <summary>
        /// Triggered when validation status changes.
        /// </summary>
        /// <remarks>Valida</remarks>
        event EventHandler<ValidatorOnlineEventArgs> ValidatorOnline;

        /// <summary>
        /// Triggered when validation status changes.
        /// </summary>
        /// <remarks>Valida</remarks>
        event EventHandler<ValidationFailureEventArgs> ValidationFailure;

        /// <summary>
        /// Gets or sets a function for calculating validator timeout periods.
        /// </summary>
        ValidatorTimeoutCalculationMethod ValidationTimeoutCalculationFunction { get; set; }

        /// <summary>
        /// Register a validated entity for validation.
        /// </summary>
        /// <param name="entity">The validated entity to be registered.</param>
        /// <param name="entityType">An integer representing the entity type.</param>
        /// <param name="initialState">The serialized initial state.</param>
        /// <exception cref="System.ArgumentException">Thrown when an entity is already registered with the given ID.</exception>
        void RegisterValidatedEntity(
            IValidatedEntity entity,
            uint entityType,
            ISerializedEntityState initialState);

        /// <summary>
        /// Unregister a validated entity for validation.
        /// </summary>
        /// <param name="entity">The validated entity to be registered.</param>
        /// <exception cref="System.ArgumentException">Thrown when no entity is registered with the given ID.</exception>
        void UnregisterValidatedEntity(IValidatedEntity entity);

        /// <summary>
        /// Calculate local update based on user input, and send user input
        /// to the validator.
        /// </summary>
        /// <param name="entity">The validated entity to be registered.</param>
        /// <param name="interval">The time interval for the update.</param>
        /// <param name="userInput">The user input.</param>
        /// <exception cref="System.ArgumentException">Thrown when no entity is registered with the given ID.</exception>
        void UpdateEntity(IValidatedEntity entity, TimeSpan interval, byte[] userInput);

        /// <summary>
        /// Handle entity initial state received from server.
        /// </summary>
        /// <param name="source">The source of the message.</param>
        /// <param name="localId">The local ID of the entity.</param>
        /// <param name="state">The entity state update.</param>
        void ReceiveEntityStateUpdate(PeerAddress source, ushort localId, IOrdered<ISerializedEntityState> state);

        /// <summary>
        /// Handle entity milestone state received from server.
        /// </summary>
        /// <param name="source">The source of the message.</param>
        /// <param name="localId">The local ID of the entity.</param>
        /// <param name="milestone">The signed milestone state of the entity.</param>
        void ReceiveEntityStateMilestone(PeerAddress source, ushort localId, ISignedEntityState milestone);

        /// <summary>
        /// Handle a termination notice received from the server.
        /// </summary>
        /// <param name="source">The source of the message.</param>
        void ReceiveTerminationNotice(PeerAddress source);

        /// <summary>
        /// Handle a ping reply from a server.
        /// </summary>
        /// <param name="source">The address of the server.</param>
        /// <param name="reply">A valud indicating whether the server is still available.</param>
        void ReceivePingReply(PeerAddress source, bool reply);

        /// <summary>
        /// Handle an error notification received from the server.
        /// </summary>
        /// <param name="source">The source of the message.</param>
        /// <param name="localId">The local ID of the entity whose request caused the error.</param>
        void ReceiveServerError(PeerAddress source, ushort localId);

        /// <summary>
        /// Register a validated entity with a scene.
        /// </summary>
        /// <param name="entity">The validated entity to be registered.</param>
        /// <param name="scene">The scene to register the entity with.</param>
        /// <exception cref="System.ArgumentException">Thrown when no entity is registered with the given ID.</exception>
        void RegisterEntityWithScene(IValidatedEntity entity, NetworkScene scene);

        /// <summary>
        /// Unregister a validated entity from a scene.
        /// </summary>
        /// <param name="entity">The validated entity to be registered.</param>
        /// <exception cref="System.ArgumentException">Thrown when no entity is registered with the given ID.</exception>
        void UnregisterEntityFromScene(IValidatedEntity entity);

        /// <summary>
        /// Notify assigned validator of shutdown.
        /// </summary>
        void Shutdown();

        /// <summary>
        /// Perform regular processing.
        /// </summary>
        void RegularProcessing();
    }

    /// <summary>
    /// The client is responsible for sending messages to the server, and passing
    /// server messages to their appropriate destination.
    /// </summary>
    internal class Client : IClient
    {
        /// <summary>
        /// The number of backup validators to maintain in reserve.
        /// </summary>
        private const int NumBackupsToMaintain = 1;

        /// <summary>
        /// Interval to wait before pinging backup validators.
        /// </summary>
        private const double BackupPingIntervalSeconds = 10;

        /// <summary>
        /// Default timeout period for validator.
        /// </summary>
        private const double DefaultValidatorTimeoutMilliseconds = 1000;

        /// <summary>
        /// The number of message intervals to use when calculating stats.
        /// </summary>
        private const int NumMessageIntervalsToAverage = 60 * 60;

        /// <summary>
        /// The transport protocol.
        /// </summary>
        private readonly ITransportProtocol transportProtocol;

        /// <summary>
        /// The protocol defining methods available via RPC on the server.
        /// </summary>
        private readonly IServerProtocol serverProtocol;

        /// <summary>
        /// Client for engaging the services of a validator.
        /// </summary>
        private readonly Engagement.IClient engagementClient;

        /// <summary>
        /// Notifies peer connection establishment and loss.
        /// </summary>
        private readonly IPeerConnectionNotifier peerConnectivityNotifier;

        /// <summary>
        /// Factoroy for creating local entity wrappers.
        /// </summary>
        private readonly LocalEntityWrapperFactory entityWrapperFactory;

        /// <summary>
        /// Provides access to the time.
        /// </summary>
        private readonly ITime timeKeeper;

        /// <summary>
        /// The method to use to queue events that must run in the application's thread.
        /// </summary>
        private readonly QueueInvokable queueApplicationEvent;

        /// <summary>
        /// Factory method for assigning BadumnaIds.
        /// </summary>
        private readonly GenericCallBackReturn<BadumnaId> generateBadumnaId;

        /// <summary>
        /// Interval series for calculating statistics on rate of server message arrival.
        /// </summary>
        private IIntervalSeries serverMessageIntervals;

        /// <summary>
        /// A value indicating whether there is a validator engagement request outstanding.
        /// </summary>
        private bool validatorEngagementPending;

        /// <summary>
        /// Address of the allocated validator.
        /// </summary>
        private PeerAddress validatorAddress;

        /// <summary>
        /// Backup validators that can be switched to if current validator
        /// goes offline, and the time they were last known to be available.
        /// </summary>
        private List<PeerAddress> backupValidators = new List<PeerAddress>();

        /// <summary>
        /// Scheduled pings for backup servers.
        /// </summary>
        private Dictionary<PeerAddress, TimeSpan> scheduledBackupPings = new Dictionary<PeerAddress, TimeSpan>();

        /// <summary>
        /// The local entity wrappers by local ID.
        /// </summary>
        /// <remarks>This is the local ID from the BadumnaId sent to the validator, which is not assigned to the
        /// validated entity locally.</remarks>
        private Dictionary<ushort, ILocalEntityWrapper> wrappersById =
            new Dictionary<ushort, ILocalEntityWrapper>();

        /// <summary>
        /// The local IDs sent to the validator to identify validated entities.
        /// </summary>
        private Dictionary<IValidatedEntity, ushort> localIdsByEntity =
            new Dictionary<IValidatedEntity, ushort>();

        /// <summary>
        /// A value indicating whether a message has been received from the validator since last update.
        /// </summary>
        private bool messageReceivedFromValidator;

        /// <summary>
        /// Delegate for validator online events.
        /// </summary>
        private EventHandler<ValidatorOnlineEventArgs> validatorOnlineDelegate;

        /// <summary>
        /// Delegate for validation failure events.
        /// </summary>
        private EventHandler<ValidationFailureEventArgs> validationFailureDelegate;

        /// <summary>
        /// Initializes a new instance of the Client class.
        /// </summary>
        /// <param name="transportProtocol">The transport protocol.</param>
        /// <param name="serverProtocol">Protocol defining methods available via RPC on the server.</param>
        /// <param name="engagementClient">Client for engaging the service of a validator.</param>
        /// <param name="peerConnectivityNotifier">Notifies peer connection establishment and loss.</param>
        /// <param name="timeKeeper">Provides access to current time.</param>
        /// <param name="queueApplicationEvent">The method to use to queue events that must run in the application's thread.</param>
        /// <param name="generateBadumnaId">A delegate that will generate a unique BadumnaId associated with this peer.</param>
        public Client(
            ITransportProtocol transportProtocol,
            IServerProtocol serverProtocol,
            Engagement.IClient engagementClient,
            IPeerConnectionNotifier peerConnectivityNotifier,
            ITime timeKeeper,
            QueueInvokable queueApplicationEvent,
            GenericCallBackReturn<BadumnaId> generateBadumnaId)
            : this(
                transportProtocol,
                serverProtocol,
                engagementClient,
                peerConnectivityNotifier,
                timeKeeper,
                queueApplicationEvent,
                (tp, sp, e, et, bi, ss, va) => new LocalEntityWrapper(tp, sp, e, et, bi, ss, va),
                (s, t) => new IntervalSeries(s, t),
                generateBadumnaId)
        {
        }

        /// <summary>
        /// Initializes a new instance of the Client class.
        /// </summary>
        /// <param name="transportProtocol">The transport protocol.</param>
        /// <param name="serverProtocol">Protocol defining methods available via RPC on the server.</param>
        /// <param name="engagementClient">Client for engaging the service of a validator.</param>
        /// <param name="peerConnectivityNotifier">Notifies peer connection establishment and loss.</param>
        /// <param name="timeKeeper">Provides access to current time.</param>
        /// <param name="queueApplicationEvent">The method to use to queue events that must run in the application's thread.</param>
        /// <param name="entityWrapperFactory">Factory for local entity wrappers.</param>
        /// <param name="intervalSeriesFactory">Factory for interval series.</param>
        /// <param name="generateBadumnaId">A delegate that will generate a unique BadumnaId associated with this peer.</param>
        public Client(
            ITransportProtocol transportProtocol,
            IServerProtocol serverProtocol,
            Engagement.IClient engagementClient,
            IPeerConnectionNotifier peerConnectivityNotifier,
            ITime timeKeeper,
            QueueInvokable queueApplicationEvent,
            LocalEntityWrapperFactory entityWrapperFactory,
            IntervalSeriesFactory intervalSeriesFactory,
            GenericCallBackReturn<BadumnaId> generateBadumnaId)
        {
            if (transportProtocol == null)
            {
                throw new ArgumentNullException("transportProtocol");
            }

            if (serverProtocol == null)
            {
                throw new ArgumentNullException("serverProtocol");
            }

            if (engagementClient == null)
            {
                throw new ArgumentNullException("engagementClient");
            }

            if (peerConnectivityNotifier == null)
            {
                throw new ArgumentNullException("peerConnectivityNotifier");
            }

            if (timeKeeper == null)
            {
                throw new ArgumentNullException("timeKeeper");
            }

            if (queueApplicationEvent == null)
            {
                throw new ArgumentNullException("queueApplicationEvent");
            }

            if (entityWrapperFactory == null)
            {
                throw new ArgumentNullException("entityWrapperFactory");
            }

            if (generateBadumnaId == null)
            {
                throw new ArgumentNullException("generateBadumnaId");
            }

            this.transportProtocol = transportProtocol;
            this.serverProtocol = serverProtocol;
            this.engagementClient = engagementClient;
            this.peerConnectivityNotifier = peerConnectivityNotifier;
            this.timeKeeper = timeKeeper;
            this.queueApplicationEvent = queueApplicationEvent;
            this.entityWrapperFactory = entityWrapperFactory;
            this.serverMessageIntervals = intervalSeriesFactory.Invoke(
                Client.NumMessageIntervalsToAverage,
                this.timeKeeper);
            this.generateBadumnaId = generateBadumnaId;

            // TODO: Make disposable, so event subscription can be removed.
            // At present client lifetime is presumed to match application,
            // so unsubscription is not essential.
            this.peerConnectivityNotifier.ConnectionLostEvent += this.OnConnectionLost;
        }

        /// <inheritdoc/>
        public event EventHandler<ValidatorOnlineEventArgs> ValidatorOnline
        {
            add { this.validatorOnlineDelegate += value; }
            remove { this.validatorOnlineDelegate -= value; }
        }

        /// <inheritdoc/>
        public event EventHandler<ValidationFailureEventArgs> ValidationFailure
        {
            add { this.validationFailureDelegate += value; }
            remove { this.validationFailureDelegate -= value; }
        }

        /// <inheritdoc/>
        public ValidatorTimeoutCalculationMethod ValidationTimeoutCalculationFunction { get; set; }

        /// <inheritdoc/>
        public void ReceiveEntityStateUpdate(PeerAddress source, ushort localId, IOrdered<ISerializedEntityState> state)
        {
            if (source.Equals(this.validatorAddress))
            {
                this.messageReceivedFromValidator = true;

                ILocalEntityWrapper wrapper;
                if (this.wrappersById.TryGetValue(localId, out wrapper))
                {
                    this.queueApplicationEvent(
                        Apply.Func(wrapper.ReceiveUpdate, state));
                }

                // TODO: warn if wrapper not found?
            }
        }

        /// <inheritdoc/>
        public void ReceiveEntityStateMilestone(PeerAddress source, ushort localId, ISignedEntityState milestone)
        {
            if (source.Equals(this.validatorAddress))
            {
                this.messageReceivedFromValidator = true;

                ILocalEntityWrapper wrapper;
                if (this.wrappersById.TryGetValue(localId, out wrapper))
                {
                    this.queueApplicationEvent(
                        Apply.Func(wrapper.ReceiveMilestone, milestone));
                }

                // TODO: warn if wrapper not found?
            }
        }

        /// <inheritdoc/>
        public void ReceiveTerminationNotice(PeerAddress source)
        {
            this.OnConnectionLost(source);
        }

        /// <inheritdoc/>
        public void ReceivePingReply(PeerAddress source, bool serverIsAvailable)
        {
            if (this.backupValidators.Contains(source))
            {
                if (serverIsAvailable)
                {
                    this.scheduledBackupPings[source] =
                        this.timeKeeper.Now + TimeSpan.FromSeconds(Client.BackupPingIntervalSeconds);
                }
                else
                {
                    this.backupValidators.Remove(source);
                    this.scheduledBackupPings.Remove(source);
                }
            }
        }

        /// <inheritdoc/>
        public void ReceiveServerError(PeerAddress source, ushort localId)
        {
            // Ignore errors that don't come from the current validator.
            if (source.Equals(this.validatorAddress))
            {
                // Ignore errors about unknown entities.
                ILocalEntityWrapper wrapper;
                if (this.wrappersById.TryGetValue(localId, out wrapper))
                {
                    // Notify the loss, and re-allocate.
                    this.queueApplicationEvent(
                        Apply.Func(wrapper.OnValidatorLost, this.validatorAddress));
                    this.queueApplicationEvent(
                        Apply.Func(wrapper.OnValidatorAllocation, this.validatorAddress));
                }
            }
        }

        /// <inheritdoc/>
        public void RegisterValidatedEntity(
            IValidatedEntity entity,
            uint entityType,
            ISerializedEntityState initialState)
        {
            if (this.localIdsByEntity.ContainsKey(entity))
            {
                throw new ArgumentException("Entity already registered.", "entity");
            }

            if (this.validatorAddress == null)
            {
                this.RequestValidator();
            }

            BadumnaId badumnaId = this.generateBadumnaId();

            Logger.TraceInformation(LogTag.Validation, "creating ID for validated entity: {0}", badumnaId);

            ILocalEntityWrapper wrapper = this.entityWrapperFactory.Invoke(
                this.transportProtocol,
                this.serverProtocol,
                entity,
                entityType,
                badumnaId,
                initialState,
                this.validatorAddress);

            this.localIdsByEntity.Add(entity, badumnaId.LocalId);
            this.wrappersById.Add(badumnaId.LocalId, wrapper);
        }

        /// <inheritdoc/>
        public void UnregisterValidatedEntity(
            IValidatedEntity entity)
        {
            ushort localId;
            if (!this.localIdsByEntity.TryGetValue(entity, out localId))
            {
                throw new ArgumentException("Entity not registered.", "entity");
            }

            this.localIdsByEntity.Remove(entity);
            this.wrappersById.Remove(localId);

            if (this.validatorAddress != null)
            {
                this.transportProtocol.SendRemoteCall(
                    this.validatorAddress,
                    QualityOfService.Reliable,
                    this.serverProtocol.ReceiveEntityUnregistrationRequest,
                    localId);
            }
        }

        /// <inheritdoc/>
        public void UpdateEntity(IValidatedEntity entity, TimeSpan interval, byte[] userInput)
        {
            ILocalEntityWrapper wrapper = this.GetWrapper(entity);
            this.queueApplicationEvent(
                        Apply.Func(wrapper.UpdateEntity, interval, userInput));
        }

        /// <inheritdoc/>
        public void RegisterEntityWithScene(IValidatedEntity entity, NetworkScene scene)
        {
            this.GetWrapper(entity).RegisterEntityWithScene(scene);
        }

        /// <inheritdoc/>
        public void UnregisterEntityFromScene(IValidatedEntity entity)
        {
            this.GetWrapper(entity).UnregisterEntityFromScene();
        }

        /// <inheritdoc/>
        public void Shutdown()
        {
            if (this.validatorAddress != null)
            {
                this.transportProtocol.SendRemoteCall(
                    this.validatorAddress,
                    QualityOfService.Reliable,
                    this.serverProtocol.ReceiveTerminationNotice);
            }
        }

        /// <inheritdoc/>
        public void RegularProcessing()
        {
            if (this.wrappersById.Count > 0)
            {
                this.MaintainBackupValidators();

                // Record server message arrival intervals.
                if (this.messageReceivedFromValidator)
                {
                    this.messageReceivedFromValidator = false;
                    if (this.serverMessageIntervals.IsStarted)
                    {
                        this.serverMessageIntervals.Add();
                    }
                    else
                    {
                        this.serverMessageIntervals.Start();
                    }
                }

                // See if server should be timed out.
                if (this.serverMessageIntervals.IsStarted)
                {
                    TimeSpan latestInterval = this.timeKeeper.Now - this.serverMessageIntervals.LastTimePoint;
                    if (latestInterval > this.CalculateServerTimeoutInterval())
                    {
                        this.OnConnectionLost(this.validatorAddress);
                    }
                }
            }
        }

        /// <summary>
        /// Request a validator be allocated.
        /// </summary>
        private void RequestValidator()
        {
            if (!this.validatorEngagementPending)
            {
                this.validatorEngagementPending = true;
                this.engagementClient.RequestValidator(this.HandleAllocationReply);
            }
        }

        /// <summary>
        /// Keeps required number of backups maintained, requesting new ones when required
        /// and checking existing ones are still available.
        /// </summary>
        private void MaintainBackupValidators()
        {
            if (this.backupValidators.Count < Client.NumBackupsToMaintain)
            {
                this.RequestValidator();
            }

            // Ping backup validators to make sure they are still available.
            TimeSpan now = this.timeKeeper.Now;
            foreach (PeerAddress backupValidator in this.backupValidators)
            {
                if (this.scheduledBackupPings[backupValidator] <= now)
                {
                    this.transportProtocol.SendRemoteCall(
                        backupValidator,
                        QualityOfService.Reliable,
                        this.serverProtocol.ReceivePingRequest);

                    // Next ping will be properly scheduled on ping reply receipt.
                    this.scheduledBackupPings[backupValidator] = TimeSpan.MaxValue;
                }
            }
        }

        /// <summary>
        /// Handle the reply to an validator allocation request.
        /// </summary>
        /// <param name="result">The result of the request.</param>
        /// <param name="validatorAddress">The address of the allocated validator, if successful.</param>
        private void HandleAllocationReply(EngagementRequestResult result, string validatorAddress)
        {
            this.validatorEngagementPending = false;

            if (result == EngagementRequestResult.Success)
            {
                if (this.validatorAddress == null)
                {
                    this.OnNewValidator(PeerAddress.FromString(validatorAddress));
                    var liveHandler = this.validatorOnlineDelegate;
                    if (liveHandler != null)
                    {
                        this.queueApplicationEvent(
                            Apply.Func(liveHandler.Invoke, this, new ValidatorOnlineEventArgs()));
                    }
                }
                else
                {
                    if (this.backupValidators.Count < Client.NumBackupsToMaintain)
                    {
                        PeerAddress newAddress = PeerAddress.FromString(validatorAddress);
                        if (!this.backupValidators.Contains(newAddress))
                        {
                            this.backupValidators.Add(newAddress);
                        }

                        this.scheduledBackupPings[newAddress] =
                                this.timeKeeper.Now + TimeSpan.FromSeconds(Client.BackupPingIntervalSeconds);
                    }
                }
            }
            else
            {
                // If allocation failed and we have not allocator at all, then this
                // is a potentially fatal error that needs to be notified to all validated
                // entities and the application.
                // If we do have an allocator, then failures of backup allocation can just be ignored
                // and we can try again.
                if (this.validatorAddress == null)
                {
                    if (this.wrappersById.Count > 0)
                    {
                        this.RequestValidator();

                        foreach (ILocalEntityWrapper wrapper in this.wrappersById.Values)
                        {
                            this.queueApplicationEvent(
                                Apply.Func(wrapper.OnValidationFailure));
                        }

                        ValidationFailureReason reason;
                        if (result == EngagementRequestResult.Failure)
                        {
                            reason = ValidationFailureReason.NoHostAvailable;
                        }
                        else
                        {
                            reason = ValidationFailureReason.ServerUnreachable;
                        }

                        var statusHandler = this.validationFailureDelegate;
                        if (statusHandler != null)
                        {
                            this.queueApplicationEvent(
                                Apply.Func(statusHandler.Invoke, this, new ValidationFailureEventArgs(reason)));
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Called on allocation of a new validator.
        /// </summary>
        /// <param name="validatorAddress">The address of the new validator.</param>
        private void OnNewValidator(PeerAddress validatorAddress)
        {
            this.validatorAddress = validatorAddress;

            foreach (ILocalEntityWrapper wrapper in this.wrappersById.Values)
            {
                this.queueApplicationEvent(
                    Apply.Func(wrapper.OnValidatorAllocation, this.validatorAddress));
            }
        }

        /// <summary>
        /// Handle notifications of peer connections being lost.
        /// </summary>
        /// <param name="address">The address of the connection that was lost.</param>
        private void OnConnectionLost(PeerAddress address)
        {
            if (address.Equals(this.validatorAddress))
            {
                this.serverMessageIntervals.Reset();

                foreach (ILocalEntityWrapper wrapper in this.wrappersById.Values)
                {
                    this.queueApplicationEvent(
                        Apply.Func(wrapper.OnValidatorLost, address));
                }

                this.validatorAddress = null;

                // If there's a backup validator available use it.
                if (this.backupValidators.Count > 0)
                {
                    PeerAddress newValidator = this.backupValidators[0];
                    this.backupValidators.RemoveAt(0);
                    this.scheduledBackupPings.Remove(newValidator);
                    this.OnNewValidator(newValidator);
                }
                else
                {
                    this.RequestValidator();
                }
            }
            else
            {
                if (this.backupValidators.Contains(address))
                {
                    this.backupValidators.Remove(address);
                    this.scheduledBackupPings.Remove(address);
                }
            }
        }

        /// <summary>
        /// Get the wrapper for an entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns>The entity wrapper.</returns>
        /// <exception cref="System.ArgumentException">Thrown when no entity is registered with the given id.</exception>
        private ILocalEntityWrapper GetWrapper(IValidatedEntity entity)
        {
            ushort localId;
            if (this.localIdsByEntity.TryGetValue(entity, out localId))
            {
                return this.wrappersById[localId];
            }
            else
            {
                throw new InvalidOperationException("Entity not registered.");
            }
        }

        /// <summary>
        /// Calculate how long to wait before timing out a validator.
        /// </summary>
        /// <returns>A time span representing the validator timeout period.</returns>
        private TimeSpan CalculateServerTimeoutInterval()
        {
            var func = this.ValidationTimeoutCalculationFunction;
            if (func != null)
            {
                double timeout = func.Invoke(
                    this.serverMessageIntervals.Mean,
                    this.serverMessageIntervals.StdDev,
                    this.serverMessageIntervals.GetTimeSinceStart().TotalMilliseconds,
                    this.serverMessageIntervals.Count);
                return TimeSpan.FromMilliseconds(timeout);
            }
            else
            {
                return TimeSpan.FromMilliseconds(Client.DefaultValidatorTimeoutMilliseconds);
            }
            ////if (this.serverMessageIntervals.GetTimeSinceStart() < TimeSpan.FromSeconds(3))
            ////{
            ////    return TimeSpan.FromSeconds(3);
            ////}

            ////double timeoutIntervalMilliseconds = this.serverMessageIntervals.Mean;
            ////timeoutIntervalMilliseconds += 3 * this.serverMessageIntervals.StdDev;

            ////return TimeSpan.FromMilliseconds(timeoutIntervalMilliseconds);
        }
    }
}
