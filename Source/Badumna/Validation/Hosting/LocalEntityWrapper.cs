﻿//-----------------------------------------------------------------------
// <copyright file="LocalEntityWrapper.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.Validation.Hosting
{
    using System;
    using System.Collections.Generic;
    using Badumna.Arbitration;
    using Badumna.Core;
    using Badumna.DataTypes;
    using Badumna.SpatialEntities;
    using Badumna.Transport;
    using Badumna.Utilities;

    /// <summary>
    /// Delegate for local entity wrapper factory .
    /// </summary>
    /// <param name="transportProtocol">The transport protocol.</param>
    /// <param name="serverProtocol">The protocol for contacting the validator.</param>
    /// <param name="entity">The entity.</param>
    /// <param name="entityType">An integer representing the type of the entity.</param>
    /// <param name="badumnaId">A unique ID used inside Badumna for this session.</param>
    /// <param name="initialState">The serialized initial state.</param>
    /// <param name="validatorAddress">The address of the validator.</param>
    /// <returns>A new local entity wrapper.</returns>
    internal delegate ILocalEntityWrapper LocalEntityWrapperFactory(
        ITransportProtocol transportProtocol,
        IServerProtocol serverProtocol,
        IValidatedEntity entity,
        uint entityType,
        BadumnaId badumnaId,
        ISerializedEntityState initialState,
        PeerAddress validatorAddress);

    /// <summary>
    /// Interface for local entity wrapper.
    /// </summary>
    internal interface ILocalEntityWrapper
    {
        /// <summary>
        /// Called to notify wrapper of new validator.
        /// </summary>
        /// <param name="validatorAddress">The address of the allocated validator.</param>
        void OnValidatorAllocation(PeerAddress validatorAddress);

        /// <summary>
        /// Called to notify wrapper of validation connection loss.
        /// </summary>
        /// <param name="validatorAddress">The address of the validator whose connection was lost.</param>
        void OnValidatorLost(PeerAddress validatorAddress);

        /// <summary>
        /// Called to notify the wrapper of validator assignment failure.
        /// </summary>
        void OnValidationFailure();

        /// <summary>
        /// Receive a signed milestone state from a validator.
        /// </summary>
        /// <param name="signedState">The signed milestone state.</param>
        void ReceiveMilestone(ISignedEntityState signedState);

        /// <summary>
        /// Receive a state update from a validator.
        /// </summary>
        /// <param name="update">The signed update.</param>
        void ReceiveUpdate(IOrdered<ISerializedEntityState> update);

        /// <summary>
        /// Update the entity state using user input.
        /// </summary>
        /// <remarks>The validated entity predicts its's state based upon user input since
        /// the last verified state's timestamp.</remarks>
        /// <param name="timePeriod">The update interval.</param>
        /// <param name="input">The user input for a given update.</param>
        void UpdateEntity(TimeSpan timePeriod, byte[] input);

        /// <summary>
        /// Register a validated entity with a scene.
        /// </summary>
        /// <param name="scene">The scene to register the entity with.</param>
        void RegisterEntityWithScene(NetworkScene scene);

        /// <summary>
        /// Unregister a validated entity from the scene it is currently registered with.
        /// </summary>
        void UnregisterEntityFromScene();
    }

    /// <summary>
    /// Wrapper for validate entity on the originating peer.
    /// Responsible for:
    /// - buffering user input,
    /// - sending user input to the host,
    /// - receiving updates from the host,
    /// - updating the entity.
    /// </summary>
    /// <remarks>
    /// Validated entity behaviour will be locally predicted if there is a validator assigned, or
    /// validator transitioning has not failed. If the entity has never had a validator, or
    /// transitioning has failed, updates will be ignored.
    /// Entity status is updated to inform application code of what is going on, but cannot be
    /// relied upon since it can be changed by application code (hard to protect against, as the
    /// entity class could be client code).
    /// </remarks>
    internal class LocalEntityWrapper : ILocalEntityWrapper
    {
        /// <summary>
        /// The transort protocol.
        /// </summary>
        private readonly ITransportProtocol transportProtocol;

        /// <summary>
        /// The protocol for contacting the validator.
        /// </summary>
        private readonly IServerProtocol serverProtocol;

        /// <summary>
        /// Sequencer for ordering state updates.
        /// </summary>
        private readonly ISequencer<IOrdered<ISerializedEntityState>> stateUpdateSequencer;

        /// <summary>
        /// Sequence generator for numbering updates.
        /// </summary>
        private readonly ISequenceGenerator sequenceGenerator;

        /// <summary>
        /// The entity being wrapped.
        /// </summary>
        private readonly IValidatedEntity entity;

        /// <summary>
        /// An integer representing the type of the entity.
        /// </summary>
        private readonly uint entityType;

        /// <summary>
        /// A unique ID used inside Badumna for this session.
        /// </summary>
        private readonly BadumnaId badumnaId;

        /// <summary>
        /// The address of the current validator.
        /// </summary>
        private PeerAddress validatorAddress;

        /// <summary>
        /// A flag indicating if the entity is registered with the current validator.
        /// </summary>
        private bool registeredWithValidator;

        /// <summary>
        /// A flag indicating whether the entity behaviour can be predicted.
        /// </summary>
        /// <remarks>
        /// Prediction is permitted if there is a validator assinged, or there was a validator
        /// assigned and transitioning has not failed yet.</remarks>
        private bool canPredict;

        /// <summary>
        /// The latest verified state.
        /// </summary>
        private IOrdered<ISerializedEntityState> latestVerifiedState;

        /// <summary>
        /// The latest milestone state signed by an authorised validator.
        /// </summary>
        private ISignedEntityState latestMilestone;

        /// <summary>
        /// A flag indicating if the milestone has changed since last update.
        /// </summary>
        private bool verifiedStateChangedSinceLastUpdate;

        /// <summary>
        /// The scene the entity is currently registered with.
        /// </summary>
        private NetworkScene currentScene;

        /// <summary>
        /// All input since last milestone is buffered.
        /// </summary>
        /// <remarks>
        /// Input since last verified state is reapplied each update.
        /// Input since last milestone is sent to new validator after registration.
        /// </remarks>
        private List<IOrdered<ITimedInput>> inputs = new List<IOrdered<ITimedInput>>();

        /// <summary>
        /// Initializes a new instance of the LocalEntityWrapper class.
        /// </summary>
        /// <param name="transportProtocol">The transport protocol.</param>
        /// <param name="serverProtocol">The protocol for contacting the validator.</param>
        /// <param name="entity">The entity being wrapped.</param>
        /// <param name="entityType">An integer representing the type of the entity.</param>
        /// <param name="badumnaId">A unique ID used inside Badumna for this session.</param>
        /// <param name="initialState">The serialized initial state.</param>
        /// <param name="validatorAddress">The address of the current validator.</param>
        public LocalEntityWrapper(
            ITransportProtocol transportProtocol,
            IServerProtocol serverProtocol,
            IValidatedEntity entity,
            uint entityType,
            BadumnaId badumnaId,
            ISerializedEntityState initialState,
            PeerAddress validatorAddress)
            : this(
                transportProtocol,
                serverProtocol,
                h => new Sequencer<IOrdered<ISerializedEntityState>>(h),
                new SequenceGenerator(),
                entity,
                entityType,
                badumnaId,
                initialState,
                validatorAddress)
        {
        }

        /// <summary>
        /// Initializes a new instance of the LocalEntityWrapper class.
        /// </summary>
        /// <param name="transportProtocol">The transport protocol.</param>
        /// <param name="serverProtocol">The protocol for contacting the validator.</param>
        /// <param name="sequencerFactory">Factory for state update sequencer.</param>
        /// <param name="sequenceGenerator">A sequence generator for numbering updates.</param>
        /// <param name="entity">The entity being wrapped.</param>
        /// <param name="entityType">An integer representing the type of the entity.</param>
        /// <param name="badumnaId">A unique ID used inside Badumna for this session.</param>
        /// <param name="initialState">The serialized initial state.</param>
        /// <param name="validatorAddress">The address of the current validator.</param>
        /////// <param name="proxyFactory">A factory for creaing listening proxy originals</param>
        public LocalEntityWrapper(
            ITransportProtocol transportProtocol,
            IServerProtocol serverProtocol,
            SequencerFactory<IOrdered<ISerializedEntityState>> sequencerFactory,
            ISequenceGenerator sequenceGenerator,
            IValidatedEntity entity,
            uint entityType,
            BadumnaId badumnaId,
            ISerializedEntityState initialState,
            PeerAddress validatorAddress)
        {
            if (transportProtocol == null)
            {
                throw new ArgumentNullException("transportProtocol");
            }

            if (serverProtocol == null)
            {
                throw new ArgumentNullException("serverProtocol");
            }

            if (sequencerFactory == null)
            {
                throw new ArgumentNullException("sequencerFactory");
            }

            if (sequenceGenerator == null)
            {
                throw new ArgumentNullException("sequenceGenerator");
            }

            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }

            if (badumnaId == null)
            {
                throw new ArgumentNullException("badumnaId");
            }

            this.transportProtocol = transportProtocol;
            this.serverProtocol = serverProtocol;
            this.stateUpdateSequencer = sequencerFactory(u => this.ProcessUpdate(u));
            this.stateUpdateSequencer.Initialize(new CyclicalID.UShortID(0));
            this.sequenceGenerator = sequenceGenerator;
            this.entity = entity;
            this.entityType = entityType;
            this.badumnaId = badumnaId;

            this.entity.Status = EntityStatus.Paused;
            if (initialState != null)
            {
                var serialEntityState = new Ordered<ISerializedEntityState>(
                    new CyclicalID.UShortID(0),
                    initialState);
                this.latestMilestone = new SignedEntityState(serialEntityState, new byte[0]);
            }

            if (validatorAddress != null)
            {
                this.OnValidatorAllocation(validatorAddress);
            }
        }

        /// <inheritdoc/>
        public void OnValidatorAllocation(PeerAddress validatorAddress)
        {
            if (validatorAddress == null)
            {
                throw new ArgumentNullException("validatorAddress");
            }

            this.validatorAddress = validatorAddress;
            if (this.latestMilestone == null)
            {
                this.SendRegistrationRequest();
            }
            else
            {
                this.SendTransitionRequest();
            }
        }

        /// <inheritdoc/>
        public void OnValidatorLost(PeerAddress validatorAddress)
        {
            this.validatorAddress = null;
            this.registeredWithValidator = false;
            this.entity.Status = EntityStatus.Transitioning;
        }

        /// <inheritdoc/>
        public void OnValidationFailure()
        {
            this.canPredict = false;
            this.entity.Status = EntityStatus.Paused;
            this.inputs.Clear();

            // If there is no milestone, it is not necessary to reset the sequence, as it must still be zero.
            if (this.latestMilestone != null)
            {
                this.sequenceGenerator.Reset(this.latestMilestone.SerialEntityState.SerialNumber.Value);
            }
            else
            {
                System.Diagnostics.Debug.Assert(
                    this.sequenceGenerator.NextSequenceNumber.Value == 0,
                    "Sequence number should be zero if the wrapper has never received a milestone.");
            }
        }

        /// <inheritdoc/>
        public void ReceiveMilestone(ISignedEntityState signedState)
        {
            Logger.TraceInformation(
                LogTag.Validation,
                "LocalEntityWrapper: Received milestone ({0}).",
                signedState.SerialEntityState.SerialNumber.Value);

            this.canPredict = true;
            this.entity.Status = EntityStatus.Validating;

            // TODO: Verify signature?
            if (this.latestMilestone == null ||
                this.registeredWithValidator == false ||
                this.latestMilestone.SerialEntityState.SerialNumber < signedState.SerialEntityState.SerialNumber)
            {
                this.latestMilestone = signedState;
            }

            // Buffered input older than the latest milestone can now be cleared.
            int index = this.inputs.FindIndex(
                i => i.SerialNumber >= this.latestMilestone.SerialEntityState.SerialNumber);
            if (index >= 0)
            {
                Logger.TraceWarning(
                    LogTag.Validation,
                    "Ditching buffered input older than {0}.",
                    this.latestMilestone.SerialEntityState.SerialNumber.Value);
                this.inputs.RemoveRange(0, index);
            }

            // If it's a new validator we need to send buffered input since milestone.
            if (this.registeredWithValidator == false)
            {
                // No reply is required  for buffered input.
                bool replyRequired = false;
                foreach (IOrdered<ITimedInput> input in this.inputs)
                {
                    Logger.TraceInformation(
                        LogTag.Validation,
                        "Sending buffered input to new validator: {0}",
                        input.SerialNumber.Value);
                    this.SendInput(input.SerialNumber, input.Item.TimePeriod, input.Item.UserInput, replyRequired);
                }

                // Process the milestone as an update immediately, without queuing.
                Logger.TraceInformation(
                    LogTag.Validation,
                    "Processing milestone {0} as state update directly (not queued).",
                    signedState.SerialEntityState.SerialNumber.Value);
                this.ProcessUpdate(signedState.SerialEntityState);

                if (this.inputs.Count > 0)
                {
                    System.Diagnostics.Debug.Assert(
                        this.inputs[this.inputs.Count - 1].SerialNumber.Value == this.sequenceGenerator.NextSequenceNumber.Value - 1,
                        "Inputs and sequence numbers are out of sync.");
                }

                Logger.TraceInformation(LogTag.Validation, "setting expected update number to {0}", this.sequenceGenerator.NextSequenceNumber);

                // The next expected state update will in response to the next input, since no updates will be
                // returned for the buffered inputs sent above, so the expected serial number will be the next update
                // serial number plus one.
                var expectedUpdateSerialNumber = new CyclicalID.UShortID(this.sequenceGenerator.NextSequenceNumber.Value);
                expectedUpdateSerialNumber.Increment();
                this.stateUpdateSequencer.Initialize(expectedUpdateSerialNumber);

                this.registeredWithValidator = true;
            }
            else
            {
                Logger.TraceInformation(
                    LogTag.Validation,
                    "Queuing milestone update {0} for processing as update.",
                    signedState.SerialEntityState.SerialNumber.Value);

                // Milestones are also regular updates.
                this.ReceiveUpdate(signedState.SerialEntityState);
            }
        }

        /// <inheritdoc/>
        public void ReceiveUpdate(IOrdered<ISerializedEntityState> update)
        {
            Logger.TraceInformation(
                LogTag.Validation,
                "LocalEntityWrapper: Received update {0} (queuing).",
                update.SerialNumber.Value);

            this.stateUpdateSequencer.ReceiveEvent(update.SerialNumber, update);
        }

        /// <inheritdoc/>
        public void UpdateEntity(TimeSpan timePeriod, byte[] input)
        {
            // TODO: It would be nice to avoid sending empty udpates.
            // - period of null input must be indicated somehow.
            // - keep alive's will be required too.
            // - There are implications for interactions:
            //   - it will be harder to order interactions with input when
            //     input is only sent sporadically.
            // TODO: Consider fixed timestep.
            Logger.TraceInformation(LogTag.Validation, "local wrapper: UpdateEntity");

            if (this.canPredict)
            {
                // TODO: Throw if not ready?
                if (this.latestVerifiedState != null)
                {
                    if (this.verifiedStateChangedSinceLastUpdate)
                    {
                        Logger.TraceInformation(LogTag.Validation, "verified state changed since last update.");
                        this.verifiedStateChangedSinceLastUpdate = false;

                        Logger.TraceInformation(
                            LogTag.Validation,
                            "Restoring state {0}",
                            this.latestVerifiedState.SerialNumber.Value);

                        // Set state to last verified state
                        this.entity.RestoreState(this.latestVerifiedState.Item);

                        // Reapply all updates since last verified state.
                        foreach (IOrdered<ITimedInput> bufferedInput in this.inputs)
                        {
                            if (bufferedInput.SerialNumber >= this.latestVerifiedState.SerialNumber)
                            {
                                Logger.TraceInformation(
                                    LogTag.Validation,
                                    "Reapplying update for input {0}.",
                                    bufferedInput.SerialNumber.Value);
                                this.entity.Update(
                                    bufferedInput.Item.TimePeriod,
                                    bufferedInput.Item.UserInput,
                                    UpdateMode.Synchronisation);
                            }
                        }
                    }
                    else
                    {
                        Logger.TraceInformation(LogTag.Validation, "verified state NOT changed since last update.");
                    }

                    // Predict latest update
                    this.entity.Update(timePeriod, input, UpdateMode.Prediction);
                }

                // Buffer input
                CyclicalID.UShortID serialNumber = this.sequenceGenerator.ConsumeSequenceNumber();
                IOrdered<ITimedInput> serialTimedInput = new Ordered<ITimedInput>(
                    serialNumber,
                    new TimedInput(timePeriod, input));
                this.inputs.Add(serialTimedInput);

                // Send to validator, if registered.
                if (this.entity.Status == EntityStatus.Validating)
                {
                    // Latest input requires a reply.
                    bool replyRequired = true;
                    this.SendInput(serialNumber, timePeriod, input, replyRequired);
                }
            }
        }

        /// <inheritdoc/>
        public void RegisterEntityWithScene(NetworkScene scene)
        {
            this.currentScene = scene;

            // TODO: entity should have zero radius to act as listener only.
            this.entity.Radius = 0;
            scene.RegisterEntity(this.entity, this.entityType);

            // If there is currently no validator, then the scene registration request will be
            // automatically inferred on the server during entity registration or transition.
            if (this.validatorAddress != null)
            {
                this.transportProtocol.SendRemoteCall(
                    this.validatorAddress,
                    QualityOfService.Reliable,
                    this.serverProtocol.ReceiveSceneRegistrationRequest,
                    this.badumnaId.LocalId,
                    scene.QualifiedName);
            }
        }

        /// <inheritdoc/>
        public void UnregisterEntityFromScene()
        {
            if (this.currentScene != null)
            {
                this.currentScene.UnregisterEntity(this.entity);

                // If there is currently no validator, then the scene unregistration request will be
                // automatically inferred on the server during entity registration or transition.
                if (this.validatorAddress != null)
                {
                    this.transportProtocol.SendRemoteCall(
                        this.validatorAddress,
                        QualityOfService.Reliable,
                        this.serverProtocol.ReceiveSceneUnregistrationRequest,
                        this.badumnaId.LocalId,
                        this.currentScene.QualifiedName);
                }

                this.currentScene = null;
            }
        }

        /// <summary>
        /// Send a registration request to the validator.
        /// </summary>
        private void SendRegistrationRequest()
        {
            Logger.TraceInformation(
                LogTag.Validation,
                "Sending registration request to : {0}",
                this.validatorAddress.ToString());

            var sceneName = this.currentScene != null ? this.currentScene.QualifiedName : default(QualifiedName);
            this.transportProtocol.SendRemoteCall(
                this.validatorAddress,
                QualityOfService.Reliable,
                this.serverProtocol.ReceiveEntityRegistrationRequest,
                this.entityType,
                this.badumnaId,
                sceneName);
        }

        /// <summary>
        /// Send a registration request to the validator.
        /// </summary>
        private void SendTransitionRequest()
        {
            Logger.TraceInformation(
                LogTag.Validation,
                "Sending transition request to : {0} with milestone @ {1}",
                this.validatorAddress,
                this.latestMilestone.SerialEntityState.SerialNumber.Value);

            var sceneName = this.currentScene != null ? this.currentScene.QualifiedName : default(QualifiedName);
            this.transportProtocol.SendRemoteCall(
                this.validatorAddress,
                QualityOfService.Reliable,
                this.serverProtocol.ReceiveEntityTransitionRequest,                
                this.entityType,
                this.badumnaId,
                this.latestMilestone as SignedEntityState,
                sceneName);
        }

        /// <summary>
        /// Process state updates delivered in order by the sequencer.
        /// </summary>
        /// <remarks>State updates are only used the user input is processed, so the entity
        /// is only updated when the application calls update.</remarks>
        /// <param name="update">The state update for the entity.</param>
        private void ProcessUpdate(IOrdered<ISerializedEntityState> update)
        {
           Logger.TraceInformation(
                LogTag.Validation,
                "Local wrapper: Processing update {0}.",
                update.SerialNumber.Value);
            this.verifiedStateChangedSinceLastUpdate = true;

            if (this.latestVerifiedState == null)
            {
                this.latestVerifiedState = update;
            }
            else
            {
                var latestState = this.latestVerifiedState.Item;
                var newState = this.entity.MergeStateUpdates(latestState, update.Item);
                this.latestVerifiedState = new Ordered<ISerializedEntityState>(
                    update.SerialNumber,
                    newState);
            }

            Logger.TraceInformation(
                LogTag.Validation,
                "Latest verified state now: {0}.",
                this.latestVerifiedState.SerialNumber.Value);
        }

        /// <summary>
        /// Send user input to the validator.
        /// </summary>
        /// <param name="serialNumber">The serial number for the update.</param>
        /// <param name="timePeriod">The time period for the update.</param>
        /// <param name="input">The user input for the update.</param>
        /// <param name="replyRequired">A value indicating if an update is required in reply.</param>
        private void SendInput(
            CyclicalID.UShortID serialNumber,
            TimeSpan timePeriod,
            byte[] input,
            bool replyRequired)
        {
            Logger.TraceInformation(LogTag.Validation, "Sending user input to {0}", this.validatorAddress);
            this.transportProtocol.SendRemoteCall(
                this.validatorAddress,
                QualityOfService.Reliable,
                this.serverProtocol.ReceiveEntityInput,
                this.badumnaId.LocalId,
                serialNumber,
                timePeriod.Ticks,
                input,
                replyRequired);
        }
    }
}
