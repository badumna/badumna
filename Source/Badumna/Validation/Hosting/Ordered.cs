﻿//-----------------------------------------------------------------------
// <copyright file="Ordered.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.Validation.Hosting
{
    using System;
    using Badumna.Utilities;

    /// <summary>
    /// Interface for an ordered item.
    /// </summary>
    /// <typeparam name="T">The type of the item.</typeparam>
    internal interface IOrdered<T>
    {
        /// <summary>
        /// Gets the serial number.
        /// </summary>
        CyclicalID.UShortID SerialNumber { get; }

        /// <summary>
        /// Gets the item.
        /// </summary>
        T Item { get; }

        /// <summary>
        /// Tests if a given ordered item is the successor to this one.
        /// </summary>
        /// <param name="other">The potential successor to test.</param>
        /// <returns><c>true</c> if the item is the successor to this one, otherwise <c>false</c>.</returns>
        bool IsNext(Ordered<T> other);
    }

    /// <summary>
    /// An item with an associated serial number.
    /// </summary>
    /// <typeparam name="T">The type of the item.</typeparam>
    [Serializable]
    internal class Ordered<T> : IOrdered<T>
    {
        /// <summary>
        /// A serial number.
        /// </summary>
        private readonly CyclicalID.UShortID serialNumber;

        /// <summary>
        /// The entity state.
        /// </summary>
        private readonly T item;

        /// <summary>
        /// Initializes a new instance of the Ordered class.
        /// </summary>
        /// <param name="serialNumber">The serial number associtated with the item.</param>
        /// <param name="item">The ordered item.</param>
        public Ordered(CyclicalID.UShortID serialNumber, T item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("item");
            }

            this.serialNumber = serialNumber;
            this.item = item;
        }

        /// <inheritdoc/>
        public CyclicalID.UShortID SerialNumber
        {
            get { return this.serialNumber; }
        }

        /// <inheritdoc/>
        public T Item
        {
            get { return this.item; }
        }

        /// <inheritdoc/>
        public bool IsNext(Ordered<T> other)
        {
            if (other == null)
            {
                throw new ArgumentNullException("other");
            }

            return other.SerialNumber.Previous == this.SerialNumber;
        }
    }
}
