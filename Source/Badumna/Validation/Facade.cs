﻿//-----------------------------------------------------------------------
// <copyright file="Facade.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.Validation
{
    using System;

    using Badumna.Core;
    using Badumna.DataTypes;
    using Badumna.DistributedHashTable;
    using Badumna.SpatialEntities;
    using Badumna.Transport;
    using Badumna.Utilities;
    using Badumna.Utilities.Logging;
    using Badumna.Validation.Allocation;

    /// <summary>
    /// Delegate for deciding whether to accept a validator allocation request.
    /// </summary>
    /// <returns><c>true</c> if the peer is willing to be a validator for the requestor, otherwise
    /// <c>false</c>.</returns>
    public delegate bool AllocationDecisionMethod();

    /// <summary>
    /// Delegate for creating validated entities on validators.
    /// </summary>
    /// <param name="entityType">An integer indicating the type of entity to create.</param>
    /// <returns>A new validated entity.</returns>
    public delegate IValidatedEntity CreateValidatedEntity(uint entityType);

    /// <summary>
    /// Delegate for removing validated entities on validators.
    /// </summary>
    /// <param name="entity">The entity to remove.</param>
    public delegate void RemoveValidatedEntity(IValidatedEntity entity);

    /// <summary>
    /// Delegate for calculating validator timeouts as a function of statistics about validator message arrival.
    /// </summary>
    /// <param name="messageIntervalMillisecondsMean">The mean interval between validator messages.</param>
    /// <param name="messageIntervalMillisecondsStdDev">The standard deviation of the intervals between messages.</param>
    /// <param name="sessionDurationMilliseconds">The duration of the validator session.</param>
    /// <param name="messageCount">The number of messages received from the validator.</param>
    /// <returns>The number of milliseconds without a message from the validator before it is timed out.</returns>
    public delegate double ValidatorTimeoutCalculationMethod(
        double messageIntervalMillisecondsMean,
        double messageIntervalMillisecondsStdDev,
        double sessionDurationMilliseconds,
        int messageCount);

    /// <summary>
    /// Interface for Validation Facade.
    /// </summary>
    public interface IFacade
    {
        /// <summary>
        /// Triggered when validation is unable to proceed.
        /// </summary>
        event EventHandler<ValidationFailureEventArgs> ValidationFailed;

        /// <summary>
        /// Triggered when a validator comes online.
        /// </summary>
        event EventHandler<ValidatorOnlineEventArgs> ValidatorOnline;

        /// <summary>
        /// Sets a delegate for calculating validator timeouts.
        /// </summary>
        ValidatorTimeoutCalculationMethod TimeoutCalculationMethod { set; }

        /// <summary>
        /// Configure the peer as a validator.
        /// </summary>
        /// <param name="allocationDecisionMethod">A method for deciding availability as a validator.</param>
        /// <param name="validatedEntityFactoryMethod">A factory method for creating validated entities.</param>
        /// <param name="onValidatedEntityRemoval">A callback for handling validator removal.</param>
        void ConfigureValidator(
            AllocationDecisionMethod allocationDecisionMethod,
            CreateValidatedEntity validatedEntityFactoryMethod,
            RemoveValidatedEntity onValidatedEntityRemoval);

        /// <summary>
        /// Register a validated entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="entityType">An integer representing the type of the entity.</param>
        /// <exception cref="System.ArgumentException">Thrown when the entity is already registered.</exception>
        void RegisterValidatedEntity(
            IValidatedEntity entity,
            uint entityType);

        /// <summary>
        /// Register a validated entity with a specified intial state.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="entityType">An integer representing the type of the entity.</param>
        /// <param name="initialState">The serialized initial state.</param>
        /// <exception cref="System.ArgumentException">Thrown when an entity is already registered with the given ID.</exception>
        void RegisterValidatedEntity(
            IValidatedEntity entity,
            uint entityType,
            ISerializedEntityState initialState);

        /// <summary>
        /// Unregister a validated entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <exception cref="System.ArgumentException">Thrown when the entity is not registered.</exception>
        void UnregisterValidatedEntity(IValidatedEntity entity);

        /// <summary>
        /// Specify the interval and user input to be used for the next update of a given entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="interval">The time interval for the update.</param>
        /// <param name="userInput">The user input, serialized to a byte array.</param>
        /// <exception cref="System.ArgumentException">Thrown when no entity is registered with the given ID.</exception>
        void UpdateEntity(IValidatedEntity entity, TimeSpan interval, byte[] userInput);

        /// <summary>
        /// Register a validated entity with a scene.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="scene">The scene to register the entity with.</param>
        /// <exception cref="System.ArgumentException">Thrown when the entity is not registered.</exception>
        void RegisterEntityWithScene(IValidatedEntity entity, NetworkScene scene);

        /// <summary>
        /// Unregister a validated entity from a scene.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <exception cref="System.ArgumentException">Thrown when the entity is not registered.</exception>
        void UnregisterEntityFromScene(IValidatedEntity entity);

        /// <summary>
        /// Cease any ongoing validation (client or server) and shutdown.
        /// </summary>
        void Shutdown();
    }

    /// <summary>
    /// The validation facade is responsible for managing central validation services
    /// such as validator allocation and ... TODO.
    /// </summary>
    internal class Facade : IFacade
    {
        /// <summary>
        /// The network event queue.
        /// All methods should be queued for execution in the network event thread.
        /// </summary>
        private readonly INetworkEventScheduler eventScheduler;

        /// <summary>
        /// Client for making engagement requests.
        /// </summary>
        private readonly Engagement.IClient engagementClient;

        /// <summary>
        /// Server for handling engagement requests.
        /// </summary>
        private readonly Engagement.IServer engagementServer;

        /// <summary>
        /// Allocates validators to peers.
        /// </summary>
        private readonly IAllocator allocator;

        /// <summary>
        /// Handles validator allocations.
        /// </summary>
        private readonly IAllocatee allocatee;

        /// <summary>
        /// Client for distributed entity validation.
        /// </summary>
        private readonly Hosting.IClient hostingClient;

        /// <summary>
        /// Server for distributed entity validation.
        /// </summary>
        private readonly Hosting.IServer hostingServer;

        /// <summary>
        /// Delegate for validation failure events.
        /// </summary>
        private EventHandler<ValidationFailureEventArgs> validationFailedDelegate;

        /// <summary>
        /// Delegate for validator online events.
        /// </summary>
        private EventHandler<ValidatorOnlineEventArgs> validatorOnlineDelegate;

        /// <summary>
        /// Initializes a new instance of the Facade class.
        /// </summary>
        /// <param name="validationModule">The validation configuration options.</param>
        /// <param name="transportProtocol">The transport protocol.</param>
        /// <param name="dhtProtocol">The DHT protocol.</param>
        /// <param name="eventScheduler">The network event scheduler.</param>
        /// <param name="peerConnectionNotifier">Notifies peer connection establishment and loss.</param>
        /// <param name="addressProvider">Network address provider.</param>
        /// <param name="spatialEntityManager">For registering entities with scenes.</param>
        /// <param name="networkFacade">The network facade to inject into validated entities.</param>
        /// <param name="timeKeeper">Provides access to the current time.</param>
        /// <param name="queueApplicationEvent">The method to use to queue events that must run in the application's thread.</param>
        /// <param name="generateBadumnaId">A delegate that will generate a unique BadumnaId associated with this peer.</param>
        public Facade(
            IValidationModule validationModule,
            ITransportProtocol transportProtocol,
            IDhtProtocol dhtProtocol,
            INetworkEventScheduler eventScheduler,
            IPeerConnectionNotifier peerConnectionNotifier,
            INetworkAddressProvider addressProvider,
            ISpatialEntityManager spatialEntityManager,
            INetworkFacade networkFacade,
            ITime timeKeeper,
            QueueInvokable queueApplicationEvent,
            GenericCallBackReturn<BadumnaId> generateBadumnaId) :
            this(
                validationModule,
                transportProtocol,
                dhtProtocol,
                eventScheduler,
                peerConnectionNotifier,
                addressProvider,
                spatialEntityManager,
                networkFacade,
                timeKeeper,
                queueApplicationEvent,
                generateBadumnaId,
                new Engagement.ServerLocator(validationModule),
                new Engagement.ClientProtocol(transportProtocol),
                new Engagement.ServerProtocol(transportProtocol),
                (tp, es, sp, sl, ap) => new Engagement.Client(tp, es, sp, sl, ap),
                (tp, cp, a) => new Engagement.Server(tp, cp, a),
                new AllocatorProtocol(dhtProtocol),
                new AllocateeProtocol(dhtProtocol),
                (dp, e, ape) => new Allocator(dp, e, ape),
                (dp, e, apr) => new Allocatee(dp, e, apr),
                new Hosting.ClientProtocol(transportProtocol),
                new Hosting.ServerProtocol(transportProtocol),
                (tp, sp, ec, cn, tk, eq, gb) => new Hosting.Client(tp, sp, ec, cn, tk, eq, gb),
                (tp, cp, em, nf, tk, ap, eq) => new Hosting.Server(tp, cp, em, nf, tk, ap, eq))
        {
        }

        /// <summary>
        /// Initializes a new instance of the Facade class.
        /// </summary>
        /// <param name="validationModule">The validation configuration options.</param>
        /// <param name="transportProtocol">The transport protocol.</param>
        /// <param name="dhtProtocol">The DHT protocol.</param>
        /// <param name="eventScheduler">The network event scheduler.</param>
        /// <param name="peerConnectionNotifier">Notifies peer connection establishment and loss.</param>
        /// <param name="networkAddressProvider">Network address provider.</param>
        /// <param name="spatialEntityManager">Spatial entity manager.</param>
        /// <param name="networkFacade">The network facade to inject into Validated entities.</param>
        /// <param name="timeKeeper">Provides access to the current time.</param>
        /// <param name="queueApplicationEvent">The method to use to queue events that must run in the application's thread.</param>
        /// <param name="generateBadumnaId">A delegate that will generate a unique BadumnaId associated with this peer.</param>
        /// <param name="serverLocator">Locator for finding master server address.</param>
        /// <param name="engagementClientProtocol">Protocol for making RPCs on engagement client.</param>
        /// <param name="engagementServerProtocol">Protocol for making RPCs on engagement server.</param>
        /// <param name="engagementClientFactory">Factory method for creating engagement client.</param>
        /// <param name="engagementServerFactory">Factory method for creating engagement server.</param>
        /// <param name="allocatorProtocol">Protocol for making RPCs on the allocator.</param>
        /// <param name="allocateeProtocol">Protocol for making RPCs on the allocatee.</param>
        /// <param name="allocatorFactory">A validator allocator factory.</param>
        /// <param name="allocateeFactory">A validator allocatee factory.</param>
        /// <param name="hostingClientProtocol">Protocol for making RPCs on hosting client.</param>
        /// <param name="hostingServerProtocol">Protocol for making RPCs on hosting server.</param>
        /// <param name="hostingClientFactory">Factory method for creating hosting client.</param>
        /// <param name="hostingServerFactory">Factory method for creating hosting server.</param>
        public Facade(
            IValidationModule validationModule,
            ITransportProtocol transportProtocol,
            IDhtProtocol dhtProtocol,
            INetworkEventScheduler eventScheduler,
            IPeerConnectionNotifier peerConnectionNotifier,
            INetworkAddressProvider networkAddressProvider,
            ISpatialEntityManager spatialEntityManager,
            INetworkFacade networkFacade,
            ITime timeKeeper,
            QueueInvokable queueApplicationEvent,
            GenericCallBackReturn<BadumnaId> generateBadumnaId,
            Engagement.IServerLocator serverLocator,
            Engagement.IClientProtocol engagementClientProtocol,
            Engagement.IServerProtocol engagementServerProtocol,
            Engagement.ClientFactory engagementClientFactory,
            Engagement.ServerFactory engagementServerFactory,
            IAllocatorProtocol allocatorProtocol,
            IAllocateeProtocol allocateeProtocol,
            AllocatorFactory allocatorFactory,
            AllocateeFactory allocateeFactory,
            Hosting.IClientProtocol hostingClientProtocol,
            Hosting.IServerProtocol hostingServerProtocol,
            Hosting.ClientFactory hostingClientFactory,
            Hosting.ServerFactory hostingServerFactory)
        {
            if (transportProtocol == null)
            {
                throw new ArgumentNullException("transportProtocol");
            }

            if (dhtProtocol == null)
            {
                throw new ArgumentNullException("dhtProtocol");
            }

            if (eventScheduler == null)
            {
                throw new ArgumentNullException("eventScheduler");
            }

            if (peerConnectionNotifier == null)
            {
                throw new ArgumentNullException("peerConnectionNotifier");
            }

            if (networkAddressProvider == null)
            {
                throw new ArgumentNullException("networkAddressProvider");
            }

            if (spatialEntityManager == null)
            {
                throw new ArgumentNullException("spatialEntityManager");
            }

            if (networkFacade == null)
            {
                throw new ArgumentNullException("networkFacade");
            }

            if (timeKeeper == null)
            {
                throw new ArgumentNullException("timeKeeper");
            }

            if (queueApplicationEvent == null)
            {
                throw new ArgumentNullException("queueApplicationEvent");
            }

            if (generateBadumnaId == null)
            {
                throw new ArgumentNullException("generateBadumnaId");
            }

            if (serverLocator == null)
            {
                throw new ArgumentNullException("serverlocator");
            }

            if (engagementClientProtocol == null)
            {
                throw new ArgumentNullException("engagementClientProtocol");
            }

            if (engagementServerProtocol == null)
            {
                throw new ArgumentNullException("engagementServerProtocol");
            }

            if (engagementClientFactory == null)
            {
                throw new ArgumentNullException("engagementClientFactory");
            }

            if (engagementServerFactory == null)
            {
                throw new ArgumentNullException("engagementServerFactory");
            }

            if (allocatorProtocol == null)
            {
                throw new ArgumentNullException("allocatorProtocol");
            }

            if (allocateeProtocol == null)
            {
                throw new ArgumentNullException("allocateeProtocol");
            }

            if (allocatorFactory == null)
            {
                throw new ArgumentNullException("allocatorFactory");
            }

            if (allocateeFactory == null)
            {
                throw new ArgumentNullException("allocateeFactory");
            }

            if (hostingClientProtocol == null)
            {
                throw new ArgumentNullException("hostingClientProtocol");
            }

            if (hostingServerProtocol == null)
            {
                throw new ArgumentNullException("hostingServerProtocol");
            }

            if (hostingClientFactory == null)
            {
                throw new ArgumentNullException("hostingClientFactory");
            }

            if (hostingServerFactory == null)
            {
                throw new ArgumentNullException("hostingServerFactory");
            }

            this.eventScheduler = eventScheduler;

            this.allocator = allocatorFactory(dhtProtocol, eventScheduler, allocateeProtocol);
            this.allocatee = allocateeFactory(dhtProtocol, eventScheduler, allocatorProtocol);
            allocatorProtocol.Allocator = this.allocator;
            allocateeProtocol.Allocatee = this.allocatee;

            this.engagementClient = engagementClientFactory(
                transportProtocol,
                eventScheduler,
                engagementServerProtocol,
                serverLocator,
                networkAddressProvider);
            this.engagementServer = engagementServerFactory(
                transportProtocol,
                engagementClientProtocol,
                this.allocator);
            engagementClientProtocol.Client = this.engagementClient;
            engagementServerProtocol.Server = this.engagementServer;

            this.hostingClient = hostingClientFactory(
                transportProtocol,
                hostingServerProtocol,
                this.engagementClient,
                peerConnectionNotifier,
                timeKeeper,
                queueApplicationEvent,
                generateBadumnaId);
            this.hostingServer = hostingServerFactory(
                transportProtocol,
                hostingClientProtocol,
                spatialEntityManager,
                networkFacade,
                timeKeeper,
                networkAddressProvider,
                queueApplicationEvent);
            hostingClientProtocol.Client = this.hostingClient;
            hostingServerProtocol.Server = this.hostingServer;

            this.hostingClient.ValidationFailure += this.OnValidationFailure;
            this.hostingClient.ValidatorOnline += this.OnValidatorOnline;
        }

        /// <inheritdoc/>
        public event EventHandler<ValidationFailureEventArgs> ValidationFailed
        {
            add { this.validationFailedDelegate += value; }
            remove { this.validationFailedDelegate -= value; }
        }

        /// <inheritdoc/>
        public event EventHandler<ValidatorOnlineEventArgs> ValidatorOnline
        {
            add { this.validatorOnlineDelegate += value; }
            remove { this.validatorOnlineDelegate -= value; }
        }

        /// <inheritdoc/>
        public ValidatorTimeoutCalculationMethod TimeoutCalculationMethod
        {
            set
            {
                this.hostingClient.ValidationTimeoutCalculationFunction = value;
            }
        }

        /// <inheritdoc/>
        public void ConfigureValidator(
            AllocationDecisionMethod allocationDecisionMethod,
            CreateValidatedEntity validatedEntityFactoryMethod,
            RemoveValidatedEntity onValidatedEntityRemoval)
        {
            this.allocatee.AllocationDecisionMethod = allocationDecisionMethod;
            this.hostingServer.Configure(validatedEntityFactoryMethod, onValidatedEntityRemoval);
        }

        /// <summary>
        /// Perform regular processing.
        /// </summary>
        public void RegularProcessing()
        {
            Logger.TraceInformation(LogTag.Validation | LogTag.Periodic, "Facade: RegularProcessing.");
            this.hostingClient.RegularProcessing();
            this.hostingServer.RegularProcessing();
        }

        /////// <inheritdoc/>
        /////// <exception cref="InvalidOperationException">Thrown if there is already an outstanding request.</exception>
        ////public void RequestValidator(EngagementReplyHandler replyHandler)
        ////{
        ////    // TODO: Establish conditions under which validator requests should be rejected?
        ////    //  - Should the master server ascertain the availability of a previously assigned
        ////    //    validator?
        ////    //  - What if there are outstanding requests? - Only one concurrent request is currently permitted.
        ////    this.eventScheduler.Push(this.engagementClient.RequestValidator, replyHandler);
        ////}

        /// <inheritdoc/>
        public void RegisterValidatedEntity(
            IValidatedEntity entity,
            uint entityType)
        {
            Badumna.Utilities.Logger.TraceInformation(LogTag.Validation, "Facade: Registering validated entity.");
            this.eventScheduler.Push(this.hostingClient.RegisterValidatedEntity, entity, entityType, (ISerializedEntityState)null);
        }

        /// <inheritdoc/>
        public void RegisterValidatedEntity(
            IValidatedEntity entity,
            uint entityType,
            ISerializedEntityState initialState)
        {
            Badumna.Utilities.Logger.TraceInformation(LogTag.Validation, "Facade: Registering validated entity with initial state.");
            this.eventScheduler.Push(this.hostingClient.RegisterValidatedEntity, entity, entityType, initialState);
        }

        /// <inheritdoc/>
        public void UnregisterValidatedEntity(IValidatedEntity entity)
        {
            Badumna.Utilities.Logger.TraceInformation(LogTag.Validation, "Facade: Unregistering validated entity.");
            this.eventScheduler.Push(this.hostingClient.UnregisterValidatedEntity, entity);
        }

        /// <inheritdoc/>
        public void UpdateEntity(IValidatedEntity entity, TimeSpan interval, byte[] userInput)
        {
            Badumna.Utilities.Logger.TraceInformation(
                LogTag.Validation,
                "Facade: Updating entity ({0}, {1}).",
                interval.TotalMilliseconds,
                userInput.ToString());
            this.eventScheduler.Push(this.hostingClient.UpdateEntity, entity, interval, userInput);
        }

        /// <inheritdoc/>
        public void RegisterEntityWithScene(IValidatedEntity entity, NetworkScene scene)
        {
            this.eventScheduler.Push(
                this.hostingClient.RegisterEntityWithScene,
                entity,
                (NetworkScene)scene);
        }

        /// <inheritdoc/>
        public void UnregisterEntityFromScene(IValidatedEntity entity)
        {
            this.eventScheduler.Push(
                this.hostingClient.UnregisterEntityFromScene,
                entity);
        }

        /// <inheritdoc/>
        public void Shutdown()
        {
            Logger.TraceInformation(LogTag.Validation, "Shutting down.");
            this.hostingServer.Shutdown();
            this.hostingClient.Shutdown();
        }

        /// <summary>
        /// Triggers validation failure event.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The failure event arguments.</param>
        private void OnValidationFailure(object sender, ValidationFailureEventArgs e)
        {
            var failureHandler = this.validationFailedDelegate;
            if (failureHandler != null)
            {
                failureHandler(this, e);
            }
        }

        /// <summary>
        /// Triggers validator online event.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The failure event arguments.</param>
        private void OnValidatorOnline(object sender, ValidatorOnlineEventArgs e)
        {
            var onlineHandler = this.validatorOnlineDelegate;
            if (onlineHandler != null)
            {
                onlineHandler(this, e);
            }
        }
    }
}
