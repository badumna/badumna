﻿//-----------------------------------------------------------------------
// <copyright file="ValidatedEntity.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.Validation
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Reflection;
    using Badumna;
    using Badumna.DataTypes;
    using Badumna.SpatialEntities;
    using Badumna.Utilities;

    /// <summary>
    /// Base class automagically implementing most of IValidatedEntity interface.
    /// </summary>
    public abstract class ValidatedEntity : IValidatedEntity
    {
        /// <summary>
        /// The network facade.
        /// </summary>
        protected readonly INetworkFacade NetworkFacade;

        /// <summary>
        ///  Maps types to read operations.
        /// </summary>
        private static readonly Dictionary<Type, GenericCallBackReturn<object, BinaryReader>> Reader =
            new Dictionary<Type, GenericCallBackReturn<object, BinaryReader>>
            {
                { typeof(byte), reader => reader.ReadByte() },
                { typeof(int), reader => reader.ReadInt32() },
                { typeof(float), reader => reader.ReadSingle() },
                { typeof(bool), reader => reader.ReadBoolean() },
                { typeof(string), reader => 
                    {
                        if (reader.ReadBoolean())
                        {
                            return reader.ReadString();
                        }
                        else
                        {
                            return null;
                        };
                    }
                },
                { typeof(Vector3), reader => 
                    {
                        return new Vector3(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());
                    }
                },
            };

        /// <summary>
        ///  Maps types to write operations.
        /// </summary>
        private static readonly Dictionary<Type, GenericCallBack<object, BinaryWriter>> Writer =
            new Dictionary<Type, GenericCallBack<object, BinaryWriter>>
            {
                { typeof(byte), (value, writer) => writer.Write((byte)value) },
                { typeof(int), (value, writer) => writer.Write((int)value) },
                { typeof(float), (value, writer) => writer.Write((float)value) },
                { typeof(bool), (value, writer) => writer.Write((bool)value) },
                { typeof(string), (value, writer) => 
                    {
                        writer.Write(value != null);
                        if (value != null)
                        {
                            writer.Write((string)value);
                        }
                    }
                },
                { typeof(Vector3), (value, writer) => 
                    {
                        writer.Write(((Vector3)value).X);
                        writer.Write(((Vector3)value).Y);
                        writer.Write(((Vector3)value).Z);
                    }
                },
            };

        /// <summary>
        /// Holds a list of replicable fields.
        /// </summary>
        private List<PropertyInfo> replicableProperties = new List<PropertyInfo>();

        /// <summary>
        /// The last replicated position.
        /// </summary>
        private Vector3 baselinePosition;

        /// <summary>
        /// Maps member names to baseline values for calculating deltas.
        /// </summary>
        private Dictionary<string, object> baselines = new Dictionary<string, object>();

        /// <summary>
        /// Maps property names to their getters.
        /// </summary>
        private Dictionary<string, Delegate> getters = new Dictionary<string, Delegate>();

        /// <summary>
        /// Maps property names to their setters.
        /// </summary>
        private Dictionary<string, Delegate> setters = new Dictionary<string, Delegate>();

        /// <summary>
        /// Initializes a new instance of the ValidatedEntity class.
        /// </summary>
        /// <param name="networkFacade">The network facade.</param>
        protected ValidatedEntity(INetworkFacade networkFacade)
        {
            if (networkFacade == null)
            {
                throw new ArgumentNullException("networkFacade");
            }

            this.NetworkFacade = networkFacade;

            this.baselinePosition = this.Position;

            Type type = this.GetType();
            PropertyInfo[] properties = type.GetProperties(
                BindingFlags.Instance |
                BindingFlags.Public |
                BindingFlags.NonPublic);
            foreach (var property in properties)
            {
                object[] attributes = property.GetCustomAttributes(typeof(ReplicableAttribute), true);
                if (attributes.Length == 1)
                {
                    if (ValidatedEntity.IsSupported(property.PropertyType))
                    {
                        this.replicableProperties.Add(property);
                        Delegate getter = Delegate.CreateDelegate(
                            typeof(GenericCallBackReturn<>).MakeGenericType(property.PropertyType),
                            this,
                            property.GetGetMethod(true));
                        Delegate setter = Delegate.CreateDelegate(
                            typeof(GenericCallBack<>).MakeGenericType(property.PropertyType),
                            this,
                            property.GetSetMethod(true));
                        this.getters.Add(property.Name, getter);
                        this.setters.Add(property.Name, setter);
                        this.baselines.Add(property.Name, getter.DynamicInvoke());
                    }
                    else
                    {
                        string message = string.Format(
                                "Replicaiton of properties of type {0} is not supported.",
                                property.PropertyType.Name);
                        throw new NotSupportedException(message);
                    }
                }
            }
        }

        /// <inheritdoc/>
        public EntityStatus Status { get; set; }

        /// <inheritdoc/>
        public Vector3 Position { get; set; }

        /// <inheritdoc/>
        public float Radius { get; set; }

        /// <inheritdoc/>
        public float AreaOfInterestRadius { get; set; }

        /// <inheritdoc/>
        public BadumnaId Guid { get; set; }

        /// <inheritdoc/>
        public void RestoreState(ISerializedEntityState entityState)
        {
            if (entityState == null)
            {
                throw new ArgumentNullException("entityState");
            }

            MemoryStream stream = new MemoryStream(entityState.Data);
            if (entityState.Flags[(int)SpatialEntityStateSegment.Position])
            {
                BinaryReader reader = new BinaryReader(stream);
                this.Position = new Vector3(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());
            }

            this.Deserialize(entityState.Flags, stream, 0);
        }

        /// <inheritdoc/>
        public ISerializedEntityState SaveState()
        {
            BooleanArray flags = new BooleanArray(true);
            return this.CreateUpdate(flags);
        }

        /// <inheritdoc/>
        public abstract void Update(TimeSpan timeInterval, byte[] input, UpdateMode mode);

        /// <inheritdoc/>
        public ISerializedEntityState CreateUpdate()
        {
            return this.CreateUpdate(this.FlagChangedProperties());
        }

        /// <inheritdoc/>
        public abstract void HandleEvent(Stream stream);

        /// <inheritdoc/>
        public ISerializedEntityState MergeStateUpdates(ISerializedEntityState currentState, ISerializedEntityState stateUpdate)
        {
            if (currentState == null)
            {
                throw new ArgumentNullException("currentState");
            }

            if (stateUpdate == null)
            {
                throw new ArgumentNullException("stateUpdate");
            }

            BooleanArray newFlags = new BooleanArray(currentState.Flags);
            BinaryReader currentReader = new BinaryReader(new MemoryStream(currentState.Data));
            BinaryReader updateReader = new BinaryReader(new MemoryStream(stateUpdate.Data));
            BinaryWriter writer = new BinaryWriter(new MemoryStream());

            if (stateUpdate.Flags[(int)SpatialEntityStateSegment.Position])
            {
                Vector3 val = new Vector3(updateReader.ReadSingle(), updateReader.ReadSingle(), updateReader.ReadSingle());
                writer.Write(val.X);
                writer.Write(val.Y);
                writer.Write(val.Z);
                newFlags[(int)SpatialEntityStateSegment.Position] = true;

                // Need to read the current state stream anyway to get to the next property.
                if (currentState.Flags[(int)SpatialEntityStateSegment.Position])
                {
                    currentReader.ReadSingle();
                    currentReader.ReadSingle();
                    currentReader.ReadSingle();                    
                }
            }
            else if (currentState.Flags[(int)SpatialEntityStateSegment.Position])
            {
                Vector3 val = new Vector3(currentReader.ReadSingle(), currentReader.ReadSingle(), currentReader.ReadSingle());
                writer.Write(val.X);
                writer.Write(val.Y);
                writer.Write(val.Z);
            }

            int propertyIndex = (int)SpatialEntityStateSegment.FirstAvailableSegment;
            foreach (var property in this.replicableProperties)
            {
                if (stateUpdate.Flags[propertyIndex])
                {
                    object val = Reader[property.PropertyType](updateReader);
                    Writer[property.PropertyType](val, writer);
                    newFlags[propertyIndex] = true;

                    // Need to read the current state stream anyway to get to the next property.
                    if (currentState.Flags[propertyIndex])
                    {
                        Reader[property.PropertyType](currentReader);
                    }
                }
                else if (currentState.Flags[propertyIndex])
                {
                    object val = Reader[property.PropertyType](currentReader);
                    Writer[property.PropertyType](val, writer);
                }
                
                propertyIndex++;
            }

            int dataLength = (int)writer.BaseStream.Length;
            byte[] newData = new byte[dataLength];
            writer.BaseStream.Position = 0;
            writer.BaseStream.Read(newData, 0, dataLength);

            return new SerializedEntityState(newFlags, newData);
        }

        /// <inheritdoc/>
        public void Serialize(BooleanArray requiredParts, Stream stream)
        {
            int propertyIndex = (int)SpatialEntityStateSegment.FirstAvailableSegment;
            BinaryWriter writer = new BinaryWriter(stream);
            foreach (var property in this.replicableProperties)
            {
                if (requiredParts[propertyIndex])
                {
                    object value = this.getters[property.Name].DynamicInvoke();
                    ValidatedEntity.Writer[property.PropertyType](value, writer);
                }

                propertyIndex++;
            }
        }

        /// <inheritdoc/>
        public void Deserialize(BooleanArray includedParts, Stream stream, int estimatedMillisecondsSinceDeparture)
        {
            int propertyIndex = (int)SpatialEntityStateSegment.FirstAvailableSegment;
            BinaryReader reader = new BinaryReader(stream);
            foreach (var property in this.replicableProperties)
            {
                if (includedParts[propertyIndex])
                {
                    this.setters[property.Name].DynamicInvoke(
                        ValidatedEntity.Reader[property.PropertyType](reader));
                }

                propertyIndex++;
            }
        }

        /// <summary>
        /// Indicates whether replication of properties with a particular type is supported.
        /// </summary>
        /// <param name="type">The type to query.</param>
        /// <returns><c>true</c> if the property is supported, otherwise <c>false</c></returns>
        private static bool IsSupported(Type type)
        {
            return ValidatedEntity.Reader.ContainsKey(type);
        }

        /// <summary>
        /// Create a BooleanArray with flags set for properties that have changed since last call.
        /// </summary>
        /// <returns>A BooleanArray with flags set for properties that have changed since last call.</returns>
        private BooleanArray FlagChangedProperties()
        {
            BooleanArray flags = new BooleanArray();

            if (this.Position != this.baselinePosition)
            {
                flags[(int)SpatialEntityStateSegment.Position] = true;
                this.baselinePosition = this.Position;
            }

            int propertyIndex = (int)SpatialEntityStateSegment.FirstAvailableSegment;
            foreach (var property in this.replicableProperties)
            {
                object baseline = this.baselines[property.Name];
                object currentValue = this.getters[property.Name].DynamicInvoke();
                if (!currentValue.Equals(baseline))
                {
                    flags[propertyIndex] = true;
                    this.baselines[property.Name] = currentValue;
                }

                propertyIndex++;
            }

            return flags;
        }

        /// <summary>
        /// Creates a partial update with serialized property values for flagged properties.
        /// </summary>
        /// <param name="flags">Flags indicating which property values ot include.</param>
        /// <returns>A partial state containing serialized property values for flagged properties.</returns>
        private ISerializedEntityState CreateUpdate(BooleanArray flags)
        {
            MemoryStream stream = new MemoryStream();

            if (flags[(int)SpatialEntityStateSegment.Position])
            {
                BinaryWriter writer = new BinaryWriter(stream);
                writer.Write(this.Position.X);
                writer.Write(this.Position.Y);
                writer.Write(this.Position.Z);
            }

            this.Serialize(flags, stream);
            byte[] data = new byte[stream.Length];
            stream.Position = 0;

            // TODO: Force overflow checking for cast and raise exception for too large validated entity.
            // (entities that large should not be supported).
            stream.Read(data, 0, (int)stream.Length);
            return new SerializedEntityState(flags, data);
        }
    }
}
