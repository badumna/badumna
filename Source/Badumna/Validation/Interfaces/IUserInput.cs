﻿//-----------------------------------------------------------------------
// <copyright file="IUserInput.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.Validation
{    
    /// <summary>
    /// Interface for classes storing user input.
    /// </summary>
    public interface IUserInput
    {
        /// <summary>
        /// Deserialize the user input from a byte array.
        /// </summary>
        /// <returns>A byte array containing serialized user input.</returns>
        byte[] ToBytes();
    }
}
