﻿//-----------------------------------------------------------------------
// <copyright file="IValidatedEntity.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.Validation
{
    using System;
    using Badumna.SpatialEntities;

    /// <summary>
    /// Modes of updating a validated entity.
    /// </summary>
    public enum UpdateMode
    {
        /// <summary>
        /// The update is being performed by an authorised validator to give the authoritative state
        /// of the entity.
        /// </summary>
        Authoritative,

        /// <summary>
        /// The update is being performed to synchronise a predicted state with the latest known
        /// authoritative state.
        /// </summary>
        Synchronisation,

        /// <summary>
        /// The update is being performed for client-side prediction.
        /// </summary>
        Prediction
    }

    /// <summary>
    /// Status of the validated entity.
    /// </summary>
    /// <remarks>Entity status will be set by Badumna. Entities should begin with status set to
    /// <c>Paused</c>. Once a validator has been assigned and validation is ready to proceed, the
    /// status will change to <c>Validating</c>. If the entity needs to migrate to a new validator,
    /// its status will change to <c>Transitioning</c> until validation resumes on the new
    /// validator, at which point it will change back to <c>Validating</c>. If there is a failure to
    /// allocate a new validator during transitioning, the status will change to <c>Paused</c> until
    /// a new validator is found and validation resumes.</remarks>
    public enum EntityStatus
    {
        /// <summary>
        /// The entity cannot be updated, as it has no validator.
        /// </summary>
        Paused = 0, // Default value

        /// <summary>
        /// The entity has a live validator.
        /// </summary>
        Validating,

        /// <summary>
        /// The entity is transitioning.
        /// </summary>
        Transitioning,
    }

    /// <summary>
    /// Interface for validated entities.
    /// </summary>
    public interface IValidatedEntity : ISpatialOriginal, ISpatialReplica
    {
        /// <summary>
        /// Gets or sets the status of the validated entity.
        /// </summary>
        EntityStatus Status { get; set; }

        /// <summary>
        /// Restore the entity's replicable state from a stored state.
        /// </summary>
        /// <param name="state">The new state for the entity.</param>
        void RestoreState(ISerializedEntityState state);

        /// <summary>
        /// Save the object's replicable state.
        /// </summary>
        /// <returns>A memento of the entity's replicable state.</returns>
        ISerializedEntityState SaveState();

        /// <summary>
        /// Update the entity state according to the game logic.
        /// </summary>
        /// <param name="timeInterval">The time interval since last update.</param>
        /// <param name="input">User input serialized as a byte array.</param>
        /// <param name="mode">The update mode (autoritative update,
        /// synchronisation, or prediction).</param>
        /// <remarks>On the validator, update will be called with <c>mode</c> set to
        /// <see cref="UpdateMode.Authoritative" /> once for each update received from the originating
        /// peer. On originating peers, update will be called with <c>mode</c> set to
        /// <see cref="UpdateMode.Prediction" /> once for each update supplied by calls to
        /// <see cref="IFacade.UpdateEntity" />, and multiple times with <c>mode</c> set to
        /// <see cref="UpdateMode.Synchronisation" /> to synchronize predicted state with authoritative
        /// state received from the validator.</remarks>
        void Update(TimeSpan timeInterval, byte[] input, UpdateMode mode);

        /// <summary>
        /// Create an update consisting of the state change since last update was created.
        /// </summary>
        /// <returns>A state update.</returns>
        ISerializedEntityState CreateUpdate();

        /// <summary>
        /// Merge two states by by partially or completely overwriting one with another.
        /// </summary>
        /// <param name="current">The baseline state.</param>
        /// <param name="update">The new state to apply.</param>
        /// <returns>A new state representing the merging of the two states.</returns>
        ISerializedEntityState MergeStateUpdates(ISerializedEntityState current, ISerializedEntityState update);
    }
}
