using System;
using System.Collections.Generic;
using System.Text;

using Badumna.Core;

namespace Badumna.InterestManagement
{
    [Serializable]
    class ImsException : BadumnaException
    {
        internal virtual RequestStatus.Statuses Status { get { return RequestStatus.Statuses.ExceptionThrownByOperation; } }

        public ImsException(String message)
            : base(message)
        {
        }
    }

    [Serializable]
    class ImsInitializationException : ImsException
    {
        public ImsInitializationException() 
            : base (Resources.ImsInitializationException_Message) 
        {
        }
    }

    [Serializable]
    class ImsInvalidRegionException : ImsException
    {
        public ImsInvalidRegionException()
            : base(Resources.ImsInvalidSpatialRegionException_Message)
        {
        }
    }

    [Serializable]
    class ImsInvalidOperationException : ImsException
    {
        public ImsInvalidOperationException(String message)
            : base(message)
        {
        }
    }

    [Serializable]
    class ImsInvalidArgumentException : ImsException
    {
        private String mArgumentName;
        public String ArgumentName { get { return this.mArgumentName; } }

        public ImsInvalidArgumentException(String argumentName)
            : base(Resources.ImsInvalidArgumentException_Message)
        {
            this.mArgumentName = argumentName;
        }
    }

    [Serializable]
    class ImsNoSuchServiceException : ImsException
    {
        internal override RequestStatus.Statuses Status { get { return RequestStatus.Statuses.NoSuchService; } }

        public ImsNoSuchServiceException(String message)
            : base(message)
        {
        }
    }
}
