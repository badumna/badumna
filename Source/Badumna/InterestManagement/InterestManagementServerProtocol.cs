//-----------------------------------------------------------------------
// <copyright file="InterestManagementServerProtocol.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2010 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using Badumna.Utilities;

namespace Badumna.InterestManagement
{
    /// <summary>
    /// TODO: documentation.
    /// </summary>
    internal abstract class InterestManagementServerProtocol
    {
        /// <summary>
        /// Request a region be inserted.
        /// </summary>
        /// <param name="sceneName">The name of the scene the region belongs to.</param>
        /// <param name="region">The region to insert.</param>
        /// <param name="timeToLiveSeconds">The time the region should live for.</param>
        public abstract void InsertRequest(QualifiedName sceneName, ImsRegion region, uint timeToLiveSeconds);

        /// <summary>
        /// Request a region be removed.
        /// </summary>
        /// <param name="sceneName">The name of the scene the region belongs to.</param>
        /// <param name="region">The region to remove.</param>
        public abstract void RemoveRequest(QualifiedName sceneName, ImsRegion region);

        /// <summary>
        /// Request a region be removed, with no notifications sent.
        /// </summary>
        /// <remarks>For use when it is assumed the object has only moved accross cell borders - not completely removed.</remarks>
        /// <param name="sceneName">The name of the scene.</param>
        /// <param name="region">The region </param>
        public abstract void RemoveWithoutNotificationRequest(QualifiedName sceneName, ImsRegion region);

        /// <summary>
        /// Request a region be modified.
        /// </summary>
        /// <param name="sceneName">The name of the scene the region belongs to.</param>
        /// <param name="region">The region to modify.</param>
        /// <param name="timeToLiveSeconds">The time the region should live for.</param>
        /// <param name="currentNumberOfInteresections">The number of intersections the region currently has.</param>
        public abstract void ModifyRequest(QualifiedName sceneName, ImsRegion region, uint timeToLiveSeconds, byte currentNumberOfInteresections);
    }
}
