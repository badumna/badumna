//-----------------------------------------------------------------------
// <copyright file="IDistributedImServerProxy.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2010 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.InterestManagement
{
    /// <summary>
    /// Interface for distributed IM server proxy.
    /// </summary>
    internal interface IDistributedImServerProxy : IInterestManagementServerProxy
    {
        /// <summary>
        /// Gets or sets the destination key string.
        /// </summary>
        string DestinationKeyString { get; set; }
    }
}
