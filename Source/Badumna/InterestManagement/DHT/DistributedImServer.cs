//-----------------------------------------------------------------------
// <copyright file="DistributedImServer.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2010 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.InterestManagement
{
    using System.Collections.Generic;

    using Badumna.Core;
    using Badumna.DistributedHashTable;
    using Badumna.Utilities;

    /// <summary>
    /// Server for distributed IM service.
    /// </summary>
    internal class DistributedImServer : InterestManagementServer
    {
        /// <summary>
        /// The client proxy (using proxy pattern).
        /// </summary>
        private IInterestManagementClientProxy clientProxy;

        /// <summary>
        /// The facade through which DHT functionalily is accessed.
        /// </summary>
        private IDhtFacade dhtFacade;
        
        /// <summary>
        /// Initializes a new instance of the DistributedImServer class.
        /// </summary>
        /// <param name="dhtFacade">The facade through which DHT functionalily is accessed.</param>
        /// <param name="protocol">The DHT protocol used for communication.</param>
        /// <param name="clientProxy">The client proxy (using proxy pattern).</param>
        /// <param name="eventQueue">The queue to use for scheduling events.</param>
        /// <param name="connectivityReporter">Reports on network connectivity status.</param>
        public DistributedImServer(
            IDhtFacade dhtFacade,
            IDhtProtocol protocol,
            IInterestManagementClientProxy clientProxy,
            INetworkEventScheduler eventQueue,
            INetworkConnectivityReporter connectivityReporter)
            : base(protocol.Parser, eventQueue, connectivityReporter)
        {
            this.dhtFacade = dhtFacade;
            this.dhtFacade.RegisterTypeForReplication<ImsRegion>(this.DistributedImServerExpirationHandler);

            this.clientProxy = clientProxy;
        }

        /// <summary>
        /// Gets the client proxy.
        /// </summary>
        protected override IInterestManagementClientProxy ClientProxy
        {
            get { return this.clientProxy; }
        }

        /// <summary>
        /// Handle region insertion requests made via RPC.
        /// </summary>
        /// <param name="sceneName">The scene the region belongs to.</param>
        /// <param name="region">The region to insert.</param>
        /// <param name="timeToLiveSeconds">The TTL for the region.</param>
        [DhtProtocolMethod(DhtMethod.InsertRequest)]
        public override void InsertRequest(QualifiedName sceneName, ImsRegion region, uint timeToLiveSeconds)
        {
            base.InsertRequest(sceneName, region, timeToLiveSeconds);
        }

        /// <summary>
        /// Handle region removal requests made via RPC.
        /// </summary>
        /// <param name="sceneName">The name of the scene the region belongs to.</param>
        /// <param name="region">The region to remove.</param>
        [DhtProtocolMethod(DhtMethod.RemoveRequest)]
        public override void RemoveRequest(QualifiedName sceneName, ImsRegion region)
        {
            base.RemoveRequest(sceneName, region);
        }

        /// <summary>
        /// Handle silent region removal requests made via RPC.
        /// </summary>
        /// <param name="sceneName">The name of the scene the region belongs to.</param>
        /// <param name="region">The region to remove.</param>
        [DhtProtocolMethod(DhtMethod.RemoveWithoutNotificationRequest)]
        public override void RemoveWithoutNotificationRequest(QualifiedName sceneName, ImsRegion region)
        {
            base.RemoveWithoutNotificationRequest(sceneName, region);
        }

        /// <summary>
        /// Handle region modification requests made via RPC.
        /// </summary>
        /// <param name="sceneName">The name of the scene the region belongs to.</param>
        /// <param name="region">The region to remove.</param>
        /// <param name="timeToLiveSeconds">The TTL for the region.</param>
        /// <param name="currentNumberOfInteresections">The number of existing intersections the region has.</param>
        [DhtProtocolMethod(DhtMethod.ModifyRequest)]
        public override void ModifyRequest(
            QualifiedName sceneName,
            ImsRegion region,
            uint timeToLiveSeconds,
            byte currentNumberOfInteresections)
        {
            base.ModifyRequest(sceneName, region, timeToLiveSeconds, currentNumberOfInteresections);
        }

        /// <inheritdoc />
        protected override ISpatialObjectStore CreateObjectStore(QualifiedName sceneName)
        {
            ISpatialObjectStore store = new DistributedObjectStore(sceneName, this.dhtFacade);
            this.Scenes[sceneName] = store;
            return store;
        }

        /// <summary>
        /// Handle region expiration events.
        /// </summary>
        /// <param name="sender">The event sender.</param>
        /// <param name="e">The expiration event arguments.</param>
        private void DistributedImServerExpirationHandler(object sender, ExpireEventArgs e)
        {
            ImsRegion spatialObject = e.ExpiredObject as ImsRegion;

            if (null != spatialObject)
            {
                this.ExpirationHandler(null, spatialObject);
            }
        }
    }
}
