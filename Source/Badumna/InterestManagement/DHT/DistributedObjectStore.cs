//-----------------------------------------------------------------------
// <copyright file="DistributedObjectStore.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2010 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace Badumna.InterestManagement
{
    using System;
    using System.Collections.Generic;
    using Badumna.Core;
    using Badumna.DataTypes;
    using Badumna.DistributedHashTable;
    using Badumna.Utilities;

    /// <summary>
    /// TODO: documentation.
    /// </summary>
    internal class DistributedObjectStore : ISpatialObjectStore
    {
        /// <summary>
        /// Delegate for intersection notification events.
        /// </summary>
        private EventHandler<IntersectionEventArgs> intersectionNotificationDelegate;

        /// <summary>
        /// Delegate for separation notification events.
        /// </summary>
        private EventHandler<IntersectionEventArgs> seperationNotificationDelegate;

        /// <summary>
        /// The facacde through which to access the DHT.
        /// </summary>
        private IDhtFacade dhtFacade;

        /// <summary>
        /// TODO: documentation.
        /// </summary>
        private HashKey sceneKey;

        /// <summary>
        /// Initializes a new instance of the DistributedObjectStore class.
        /// </summary>
        /// <param name="sceneName">The name of the scene the store belongs to.</param>
        /// <param name="dhtFacade">The facade for accessing the DHT.</param>
        public DistributedObjectStore(QualifiedName sceneName, IDhtFacade dhtFacade)
        {
            // TODO: It is pointless to just return here.
            // The constructor should either create a valid object, or throw an exception.
            ////if (Parameters.TotalNumberOfRedundantCellServers == 1)
            ////{
                ////return;
                // Maybe do this instead?
                // throw new NotSupportedException("Cannot create DistributedObjectStore when total number of redundant cell servers is 1.");
            ////}

            this.dhtFacade = dhtFacade;
            this.sceneKey = HashKey.Hash(sceneName.ToString());
        }

        /// <summary>
        /// Event for notifying intersections.
        /// </summary>
        public event EventHandler<IntersectionEventArgs> IntersectionNotification
        {
            add { this.intersectionNotificationDelegate += value; }
            remove { this.intersectionNotificationDelegate -= value; }
        }

        /// <summary>
        /// Event for notifying separations.
        /// </summary>
        public event EventHandler<IntersectionEventArgs> SeperationNotification
        {
            add { this.seperationNotificationDelegate += value; }
            remove { this.seperationNotificationDelegate -= value; }
        }

        /// <summary>
        /// Gets a value indicating whether the store is empty.
        /// </summary>
        public bool IsEmpty
        {
            get { return 0 == this.ObjectCount; }
        }

        /// <summary>
        /// Gets a count of the number of objects in the store.
        /// </summary>
        public int ObjectCount
        {
            get
            {
                if (this.dhtFacade.AccessReplicas<ImsRegion>(this.sceneKey) == null)
                {
                    return 0;
                }

                return this.dhtFacade.AccessReplicas<ImsRegion>(this.sceneKey).Count;
            }
        }

        /// <summary>
        /// Add an object to the store, or update it if it is alrady in the store.
        /// </summary>
        /// <param name="newRegion">The region to add.</param>
        /// <param name="timeToLive">The time the region should live for in seconds.</param>
        /// <param name="clientsKnownCollisions">The number of known existing intersections.</param>
        public void AddOrModifyObject(ImsRegion newRegion, uint timeToLive, byte clientsKnownCollisions)
        {
            if (newRegion.Radius <= 0)
            {
                throw new ImsInvalidRegionException();
            }

            this.ResetSceneKey();

            IEnumerable<ImsRegion> regionList = this.dhtFacade.AccessReplicas<ImsRegion>(this.sceneKey) ?? new List<ImsRegion>();
            ImsRegion existingRegion = this.dhtFacade.AccessReplica<ImsRegion>(this.sceneKey, newRegion.Guid);

            if (existingRegion != null)
            {
                bool requiresFullList = false;

                Logger.TraceInformation(
                    LogTag.InterestManagement,
                    "Modifying object {0} from {1}, r{2} to {3}, r{4}",
                    newRegion.Guid,
                    existingRegion.Centroid,
                    existingRegion.Radius,
                    newRegion.Centroid,
                    newRegion.Radius);

                if (clientsKnownCollisions < byte.MaxValue)
                {
                    // Create a list of objects that are intersecting with the old object.
                    List<BadumnaId> currentIntersections = new List<BadumnaId>();

                    foreach (ImsRegion region in regionList)
                    {
                        if (!region.Guid.Equals(newRegion.Guid))
                        {
                            if (newRegion.RequiresIntersectionNotification(region) && region.IsIntersecting(existingRegion))
                            {
                                currentIntersections.Add(region.Guid);
                            }
                        }
                    }

                    if (clientsKnownCollisions < currentIntersections.Count)
                    {
                        // Set the requires full list flag to indicate that we should send a 
                        // notification for all intersections regardless of whether we think 
                        // we have notified the client of them before.
                        requiresFullList = true;
                        Logger.TraceInformation(LogTag.InterestManagement, "Object {0} requires full list of intersections", newRegion.Guid);
                    }
                }

                List<ImsRegion> regionListToSend = new List<ImsRegion>();
                foreach (ImsRegion region in regionList)
                {
                    if (!region.Guid.Equals(newRegion.Guid))
                    {
                        bool wasIntersecting = region.IsIntersecting(existingRegion);
                        bool isIntersecting = region.IsIntersecting(newRegion);

                        if (wasIntersecting)
                        {
                            Logger.TraceInformation(LogTag.InterestManagement, "WAS intersecting {0}  {1}, r{2}", region.Guid, region.Centroid, region.Radius);
                        }
                        else
                        {
                            Logger.TraceInformation(LogTag.InterestManagement, "WAS NOT intersecting {0}  {1}, r{2}", region.Guid, region.Centroid, region.Radius);
                        }

                        if (isIntersecting)
                        {
                            Logger.TraceInformation(LogTag.InterestManagement, "IS intersecting {0}  {1}, r{2}", region.Guid, region.Centroid, region.Radius);
                        }
                        else
                        {
                            Logger.TraceInformation(LogTag.InterestManagement, "IS NOT intersecting {0}  {1}, r{2}", region.Guid, region.Centroid, region.Radius);
                        }

                        if (wasIntersecting && !isIntersecting)
                        {
                            EventHandler<IntersectionEventArgs> handler = this.seperationNotificationDelegate;
                            if (handler != null)
                            {
                                handler(this, new IntersectionEventArgs(region, newRegion));
                                handler(this, new IntersectionEventArgs(newRegion, region));
                            }
                        }

                        if (isIntersecting)
                        {
                            if (!wasIntersecting)
                            {
                                EventHandler<IntersectionEventArgs> handler = this.intersectionNotificationDelegate;
                                if (handler != null)
                                {
                                    handler(this, new IntersectionEventArgs(region, newRegion));
                                    handler(this, new IntersectionEventArgs(newRegion, region));
                                }
                            }
                            else if (requiresFullList && newRegion.RequiresIntersectionNotification(region))
                            {
                                regionListToSend.Add(region);
                            }
                        }
                    }
                }

                if (requiresFullList && regionListToSend.Count > 0)
                {
                    EventHandler<IntersectionEventArgs> handler = this.intersectionNotificationDelegate;
                    if (handler != null)
                    {
                        handler(this, new IntersectionEventArgs(newRegion, regionListToSend));
                    }
                }

                // use the new region to update the existing region 
                newRegion.ModificationNumber = existingRegion.ModificationNumber;
                newRegion.Key = this.sceneKey;
                this.dhtFacade.UpdateReplica(newRegion, timeToLive);
            }
            else
            {
                Logger.TraceInformation(LogTag.InterestManagement, "Entering object {0} {1}, r{2}", newRegion.Guid, newRegion.Centroid, newRegion.Radius);

                foreach (ImsRegion region in regionList)
                {
                    if (region.IsIntersecting(newRegion) && !region.Guid.Equals(newRegion.Guid))
                    {
                        EventHandler<IntersectionEventArgs> handler = this.intersectionNotificationDelegate;
                        if (handler != null)
                        {
                            handler(this, new IntersectionEventArgs(region, newRegion));
                            handler(this, new IntersectionEventArgs(newRegion, region));
                        }
                    }
                }

                this.dhtFacade.Replicate(newRegion, this.sceneKey, timeToLive);
            }
        }

        /// <summary>
        /// Keep an object in the store alice for an extended period.
        /// </summary>
        /// <param name="objectId">The ID of the object.</param>
        /// <param name="timeToLiveSeconds">The number of seconds the object should stay alive for.</param>
        public void KeepAlive(BadumnaId objectId, uint timeToLiveSeconds)
        {
            ImsRegion region = this.dhtFacade.AccessReplica<ImsRegion>(this.sceneKey, objectId);

            if (region == null)
            {
                return;
            }

            Logger.TraceInformation(LogTag.InterestManagement, "Keeping region {0} alive", objectId);
            this.dhtFacade.KeepReplicaAlive(region, timeToLiveSeconds);
        }

        /// <summary>
        /// Remove an object from the store.
        /// </summary>
        /// <param name="obj">The object to remove.</param>
        public void RemoveObject(ImsRegion obj)
        {
            this.ResetSceneKey();

            ImsRegion existingRegion = this.dhtFacade.AccessReplica<ImsRegion>(this.sceneKey, obj.Guid);
            if (existingRegion == null)
            {
                return;
            }

            IEnumerable<ImsRegion> regionList = this.dhtFacade.AccessReplicas<ImsRegion>(this.sceneKey);

            foreach (ImsRegion region in regionList)
            {
                if (!region.Guid.Equals(obj.Guid) && obj.IsIntersecting(region))
                {
                    EventHandler<IntersectionEventArgs> handler = this.seperationNotificationDelegate;
                    if (handler != null)
                    {
                        handler(this, new IntersectionEventArgs(obj, region));
                        handler(this, new IntersectionEventArgs(region, obj));
                    }
                }
            }

            this.dhtFacade.RemoveReplica(existingRegion);
        }

        /// <summary>
        /// Silently remove an object from the store.
        /// </summary>
        /// <param name="obj">The object to remove.</param>
        public void RemoveWithoutNotification(ImsRegion obj)
        {
            // TODO: refactor removal methods to use shared private method.
            this.ResetSceneKey();

            ImsRegion existingRegion = this.dhtFacade.AccessReplica<ImsRegion>(this.sceneKey, obj.Guid);
            if (existingRegion == null)
            {
                return;
            }

            this.dhtFacade.RemoveReplica(existingRegion);
        }

        /// <summary>
        /// Get all the objects within a given region.
        /// </summary>
        /// <param name="region">The region.</param>
        /// <returns>A list of the objects in the given region.</returns>
        public IList<ImsRegion> GetObjectsWithinRegion(ImsRegion region)
        {
            if (region.Radius <= 0)
            {
                throw new ImsInvalidRegionException();
            }

            IList<ImsRegion> replyList = new List<ImsRegion>();

            IEnumerable<ImsRegion> regionList = this.dhtFacade.AccessReplicas<ImsRegion>(this.sceneKey);

            if (null != regionList)
            {
                foreach (ImsRegion otherRegion in regionList)
                {
                    if (otherRegion.IsIntersecting(region))
                    {
                        replyList.Add(otherRegion);
                    }
                }
            }

            return replyList;
        }

        /// <summary>
        /// Resets the scene key with the destination key.
        /// </summary>
        private void ResetSceneKey()
        {
            // the scene key is initially created as the hash result of the
            // scene name. but on receiving any further request, the actual
            // destination key and the scene key can be different due to the
            // change of key space on Dht. 
            this.sceneKey = this.dhtFacade.CurrentEnvelope.DestinationKey;
        }
    }
}