//-----------------------------------------------------------------------
// <copyright file="DistributedImService.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2010 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.InterestManagement
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using Badumna.Core;
    using Badumna.DataTypes;
    using Badumna.DistributedHashTable;
    using Badumna.Utilities;
    using Badumna.Utilities.Logging;

    // TODO : Check the allowable delay for requests (Especially subscribe to region) to ensure piggybacking.

    /// <summary>
    /// Factory for creating distributed IM client proxies.
    /// </summary>
    /// <param name="protocolLayer">The DHT protocol for the proxy.</param>
    /// <param name="clientObject">The client protocol for the proxy.</param>
    /// <returns>A new distributed IM server proxy.</returns>
    internal delegate IInterestManagementClientProxy DistributedIMClientProxyFactory(
        IDhtProtocol protocolLayer,
        InterestManagementClientProtocol clientObject);

    /// <summary>
    /// TODO: documentation.
    /// </summary>
    internal class DistributedImService : InterestManagementService
    {
        /// <summary>
        /// The default side length of the cells.
        /// </summary>
        private const float DefaultCellSize = 500f;

        /// <summary>
        /// The dhtCellSideLength will be set to cellSideLengthFactor times of the inflated area of interest.
        /// Using a value of 10 gives roughly 50% chance that a subscription region will intersect only one cell.
        /// [Fraction of space where a subscription intersects one cell is given by f = (1 - 2 r / c) ^ 3, where
        /// r is the inflated radius and c is the cell side length.]
        /// </summary>
        private const int CellSideLengthFactor = 10;

        /// <summary>
        /// Factory for creating subscription data objects (to support unit testing).
        /// </summary>
        private SubscriptionDataFactory subscriptionDataFactory;

        /// <summary>
        /// TODO: documentation.
        /// </summary>
        private Dictionary<BadumnaId, ISubscriptionData> subscriptions = new Dictionary<BadumnaId, ISubscriptionData>();

        /// <summary>
        /// The IM service client.
        /// </summary>
        private IInterestManagementClient client;

        /// <summary>
        /// The IM server proxy (using proxy pattern).
        /// </summary>
        private IDistributedImServerProxy serverProxy;

        /// <summary>
        /// Provides access to the current time.
        /// </summary>
        private ITime timeKeeper;

        /// <summary>
        /// The cell dimensions.
        /// </summary>
        private Vector3 cellSpan = new Vector3(DistributedImService.DefaultCellSize, DistributedImService.DefaultCellSize, DistributedImService.DefaultCellSize);

        /// <summary>
        /// The maximum area of interest radius for all entities. 
        /// </summary>
        private float maximumInterestRadius = 0.0f;

        /// <summary>
        /// Initializes a new instance of the DistributedImService class.
        /// </summary>
        /// <param name="dhtFacade">The facade through which the DHT is accessed.</param>
        /// <param name="allInclusiveFacade">The facade through which the all-inclusive DHT is accessed.</param>
        /// <param name="eventQueue">The queue to use for scheduling events.</param>
        /// <param name="timeKeeper">Provides access to the current time.</param>
        /// <param name="connectivityReporter">Reports on network connectivity status.</param>
        public DistributedImService(
            DhtFacade dhtFacade,
            DhtFacade allInclusiveFacade,
            NetworkEventQueue eventQueue,
            ITime timeKeeper,
            INetworkConnectivityReporter connectivityReporter)
        {
            // TO DO: It would be preferable to use constructor chaining here, but interdependency
            // of the server proxy on the client prevents this. It may be possible to refactor.
            DhtProtocol protocolLayer = new DhtProtocol(dhtFacade);
            DhtProtocol allInclusiveProtocolLayer = new DhtProtocol(allInclusiveFacade);
            DistributedImClient client = new DistributedImClient(allInclusiveFacade);
            DistributedImServer server = new DistributedImServer(
                dhtFacade,
                protocolLayer,
                new DistributedImClientProxy(allInclusiveProtocolLayer, client),
                eventQueue,
                connectivityReporter);
            DistributedImServerProxy serverProxy = new DistributedImServerProxy(protocolLayer, server);

            this.CommonConstructor(
                client,
                serverProxy,
                timeKeeper,
                (s, r, t, n, u, k) => new SubscriptionData(s, r, t, n, u, k));
        }

        /// <summary>
        /// Initializes a new instance of the DistributedImService class.
        /// </summary>
        /// <remarks>For unit testing.</remarks>
        /// <param name="client">The distributed IM client.</param>
        /// <param name="serverProxy">The distributed IM server proxy.</param>
        /// <param name="timeKeeper">Provides access to the current time.</param>
        /// <param name="subscriptionDataFactory">A factory for creating subscription data objects.</param>
        public DistributedImService(
            IInterestManagementClient client,
            IDistributedImServerProxy serverProxy,
            ITime timeKeeper,
            SubscriptionDataFactory subscriptionDataFactory)
        {
            this.CommonConstructor(
                client,
                serverProxy,
                timeKeeper,
                subscriptionDataFactory);
        }

        /// <summary>
        /// Gets the IM service client.
        /// </summary>
        public IInterestManagementClient Client
        {
            get { return this.client; }
        }

        /// <inheritdoc />
        public override void Optimize(float maximumInterestRadius, float maximumSpeed)
        {
            base.Optimize(maximumInterestRadius, maximumSpeed);

            this.maximumInterestRadius = Math.Max(this.maximumInterestRadius, maximumInterestRadius);
            float inflatedInterestRadius = this.maximumInterestRadius + this.SubscriptionInflation;
            float cellSize = inflatedInterestRadius * DistributedImService.CellSideLengthFactor;

            this.cellSpan = new Vector3(cellSize, cellSize, cellSize);

            Logger.TraceInformation(LogTag.InterestManagement | LogTag.Event, "Inflated interest radius = {0}", inflatedInterestRadius);
            Logger.TraceInformation(LogTag.InterestManagement | LogTag.Event, "DHT cell size = {0}", cellSize);

            // it is possible to have zero inflation amount when this method is being called. however the inflaction amount
            // should eventually be set to a positive value or a error message will be produced when RegisterEntity is called. 
            // TODO : check the Distributed Controller code to see when the controller migrate to a seed/overload peer, will 
            // RegisterEntity be called at any stage. 
            Debug.Assert(this.SubscriptionInflation >= 0.0f && cellSize > 0.0f, "Optimize produced invalid cell size or inflation amount values.");
        }

        /// <summary>
        /// Modify a region.
        /// </summary>
        /// <param name="sceneName">The name of the scene the region belongs to.</param>
        /// <param name="region">The region to modify.</param>
        /// <param name="timeToLiveSeconds">The new TTL for the region.</param>
        /// <param name="clientsKnownCollisions">The number of known existing collisions.</param>
        public override void ModifyRegion(
            QualifiedName sceneName,
            ImsRegion region,
            uint timeToLiveSeconds,
            byte clientsKnownCollisions)
        {
            if (region == null || region.Radius <= 0)
            {
                throw new ImsInvalidRegionException();
            }

            ISubscriptionData subscriptionData;
            this.subscriptions.TryGetValue(region.Guid, out subscriptionData);
            if (null == subscriptionData)
            {
                throw new ImsInvalidOperationException("The subscription must be valid, having been returned from SubscribeToRegion");
            }

            Logger.RecordStatistic("Gossip", "IMCalls", 1.0);

            region.Guid = this.subscriptions[region.Guid].Region.Guid;

            ICollection<Vector3> oldCellList = CellList.IntersectingCells(subscriptionData.Region, this.cellSpan);
            ICollection<Vector3> newCellList = CellList.IntersectingCells(region, this.cellSpan);

            Logger.TraceInformation(LogTag.InterestManagement, "Using cell span {0}", this.cellSpan.ToString());

            foreach (Vector3 cell in oldCellList)
            {
                Logger.TraceInformation(LogTag.InterestManagement, "Subscription region used to intersect with cell {0}", cell.ToString());
                var destinationCell = new QualifiedName(sceneName.Qualifier, sceneName.Name + cell.ToString());
                this.serverProxy.DestinationKeyString = destinationCell.ToString();

                if (CellList.IsIntersectingWithCell(cell, region, this.cellSpan))
                {
                    this.serverProxy.ModifyRequest(destinationCell, region, timeToLiveSeconds, clientsKnownCollisions);
                }
                else
                {
                    this.serverProxy.RemoveWithoutNotificationRequest(destinationCell, region);
                }

                if (newCellList.Contains(cell))
                {
                    newCellList.Remove(cell);
                }
            }

            foreach (Vector3 cell in newCellList)
            {
                Logger.TraceInformation(LogTag.InterestManagement, "Subscribtion region now intersects with cell {0}", cell.ToString());

                var destinationCell = new QualifiedName(sceneName.Qualifier, sceneName.Name + cell.ToString());
                this.serverProxy.DestinationKeyString = destinationCell.ToString();
                this.serverProxy.InsertRequest(destinationCell, region, timeToLiveSeconds);
            }

            subscriptionData.Region.Centroid = region.Centroid;
            subscriptionData.Region.Radius = region.Radius;
            subscriptionData.ResetExpirationTime(timeToLiveSeconds);
        }

        /// <summary>
        /// Remove a region.
        /// </summary>
        /// <param name="sceneName">The name of the scene the region belongs to.</param>
        /// <param name="region">The region to remove.</param>
        public override void RemoveRegion(QualifiedName sceneName, ImsRegion region)
        {
            this.RemoveRegion(sceneName, region, true);
        }

        /// <summary>
        /// Silently remove a region.
        /// </summary>
        /// <param name="sceneName">The name of the scene the region belongs to.</param>
        /// <param name="region">The region to remove.</param>
        public override void RemoveRegionWithoutNotification(QualifiedName sceneName, ImsRegion region)
        {
            this.RemoveRegion(sceneName, region, false);
        }

        /// <summary>
        /// TODO: documentation.
        /// </summary>
        /// <param name="sceneName">The name of the scene the region belongs to.</param>
        /// <param name="region">The region to unsubscribe from.</param>
        public override void DetachSubscription(QualifiedName sceneName, ImsRegion region)
        {
            if (this.subscriptions.ContainsKey(region.Guid))
            {
                this.subscriptions.Remove(region.Guid);
            }
        }

        /// <summary>
        /// Insert a region.
        /// </summary>
        /// <param name="sceneName">The name of the scene the region belongs to.</param>
        /// <param name="region">The region to insert.</param>
        /// <param name="timeToLiveSeconds">The number of seconds the region should live for.</param>
        /// <param name="enterNotificationHandler">A handler for intersection events.</param>
        /// <param name="statusHandler">A handler for status update events.</param>
        protected override void InsertRegionImplementation(
            QualifiedName sceneName,
            ImsRegion region,
            uint timeToLiveSeconds,
            EventHandler<ImsEventArgs> enterNotificationHandler,
            EventHandler<ImsStatusEventArgs> statusHandler)
        {
            if (region == null || region.Radius <= 0)
            {
                throw new ImsInvalidRegionException();
            }

            if (this.subscriptions.ContainsKey(region.Guid))
            {
                throw new ImsInvalidOperationException("Must not subscribe to the same region more than once");
            }

            Logger.RecordStatistic("Gossip", "IMCalls", 1.0);

            ISubscriptionData subscriptionData = this.subscriptionDataFactory(
                sceneName,
                region,
                timeToLiveSeconds,
                enterNotificationHandler,
                statusHandler,
                this.timeKeeper);

            this.subscriptions.Add(region.Guid, subscriptionData);

            foreach (Vector3 cell in CellList.IntersectingCells(region, this.cellSpan))
            {
                var destinationCell = new QualifiedName(sceneName.Qualifier, sceneName.Name + cell.ToString());
                this.serverProxy.DestinationKeyString = destinationCell.ToString();
                this.serverProxy.InsertRequest(destinationCell, region, timeToLiveSeconds);
            }
        }

        /// <summary>
        /// Common logic for constructors.
        /// </summary>
        /// <param name="client">The distributed IM client.</param>
        /// <param name="serverProxy">The distributed IM server proxy.</param>
        /// <param name="timeKeeper">Provides access to the current time.</param>
        /// <param name="subscriptionDataFactory">A factory for creating subscription data objects.</param>
        private void CommonConstructor(
            IInterestManagementClient client,
            IDistributedImServerProxy serverProxy,
            ITime timeKeeper,
            SubscriptionDataFactory subscriptionDataFactory)
        {
            this.client = client;
            this.serverProxy = serverProxy;
            this.timeKeeper = timeKeeper;
            this.subscriptionDataFactory = subscriptionDataFactory;

            this.client.ResultNotification += this.ResultNotificationHandler;
            this.client.StatusNotification += this.StatusNotificationHandler;
        }

        /// <summary>
        /// Remove a region.
        /// </summary>
        /// <param name="sceneName">The name of the scene the region belongs to.</param>
        /// <param name="region">The region to remove.</param>
        /// <param name="notify">A value indicating whether notifications should be sent.</param>
        private void RemoveRegion(QualifiedName sceneName, ImsRegion region, bool notify)
        {
            if (!this.subscriptions.ContainsKey(region.Guid))
            {
                // We no longer throw exceptions for unknown regions, since the region is not inserted immediately
                // and may not be present yet. Regions are only inserted once an update has actually been serialized
                // (see SpatialOriginalWrapper). We can restore this exception if either (a) regions are inserted
                // on wrapper creation, or (b) region manager shutdown checks for insertion before attempting removal.                
                return;
                ////throw new ImsInvalidArgumentException("subscription");
            }

            foreach (Vector3 cell in CellList.IntersectingCells(region, this.cellSpan))
            {
                var destinationCell = new QualifiedName(sceneName.Qualifier, sceneName.Name + cell.ToString());
                this.serverProxy.DestinationKeyString = destinationCell.ToString();

                if (notify)
                {
                    this.serverProxy.RemoveRequest(destinationCell, region);
                }
                else
                {
                    this.serverProxy.RemoveWithoutNotificationRequest(destinationCell, region);
                }
            }

            this.subscriptions.Remove(region.Guid);
        }

        /// <summary>
        /// Handle status notification events, passing them on to subscribers.
        /// </summary>
        /// <param name="sender">The event sender.</param>
        /// <param name="e">The event arguments.</param>
        private void StatusNotificationHandler(object sender, ImsStatusEventArgs e)
        {
            ISubscriptionData subscription;
            this.subscriptions.TryGetValue(e.Id, out subscription);

            if (subscription != null)
            {
                subscription.Status(this, e.Status);
            }
        }

        /// <summary>
        /// Handle status update events.
        /// </summary>
        /// <param name="sender">The event sender.</param>
        /// <param name="e">The status event arguments.</param>
        private void ResultNotificationHandler(object sender, ImsStatusEventArgs e)
        {
            ISubscriptionData subscription;
            this.subscriptions.TryGetValue(e.Id, out subscription);

            if (subscription != null)
            {
                foreach (ImsEventArgs obj in e.EnterList)
                {
                    subscription.EnterNotification(this, obj);
                }

                if (e.Status.IsComplete)
                {
                    this.subscriptions.Remove(e.Id);
                }
            }
            else
            {
                Logger.TraceWarning(LogTag.InterestManagement, "Result notification for unknown request {0}", e.Id);
            }
        }
    }
}
