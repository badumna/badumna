//-----------------------------------------------------------------------
// <copyright file="CellList.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2010 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.InterestManagement
{
    using System;
    using System.Collections.Generic;

    using Badumna.Core;
    using Badumna.DataTypes;

    /// <summary>
    /// TODO: documentation.
    /// </summary>
    internal class CellList
    {
        /// <summary>
        /// Creates the set of cells intersecting a given region.
        /// </summary>
        /// <param name="queryRegion">The region.</param>
        /// <param name="cellSpan">The dimensions of the cells</param>
        /// <returns>The set of cells intersecting the region.</returns>
        public static ICollection<Vector3> IntersectingCells(ImsRegion queryRegion, Vector3 cellSpan)
        {
            List<Vector3> cellList = new List<Vector3>();
            
            float minCellX = CellList.CalculateCellForDimension(queryRegion.Centroid.X - queryRegion.Radius, cellSpan.X);
            float maxCellX = CellList.CalculateCellForDimension(queryRegion.Centroid.X + queryRegion.Radius, cellSpan.X);

            float minCellY = CellList.CalculateCellForDimension(queryRegion.Centroid.Y - queryRegion.Radius, cellSpan.Y);
            float maxCellY = CellList.CalculateCellForDimension(queryRegion.Centroid.Y + queryRegion.Radius, cellSpan.Y);

            float minCellZ = CellList.CalculateCellForDimension(queryRegion.Centroid.Z - queryRegion.Radius, cellSpan.Z);
            float maxCellZ = CellList.CalculateCellForDimension(queryRegion.Centroid.Z + queryRegion.Radius, cellSpan.Z);

            for (float x = minCellX; x <= maxCellX; x += cellSpan.X)
            {
                for (float y = minCellY; y <= maxCellY; y += cellSpan.Y)
                {
                    for (float z = minCellZ; z <= maxCellZ; z += cellSpan.Z)
                    {
                        Vector3 cellCenter = new Vector3(x, y, z);

                        if (CellList.IsIntersectingWithCell(cellCenter, queryRegion, cellSpan))
                        {
                            cellList.Add(cellCenter);
                        }
                    }
                }
            }

            return cellList;
        }

        /// <summary>
        /// Test if a region intersects a cell.
        /// </summary>
        /// <param name="cellCenter">The centre of the cell.</param>
        /// <param name="region">The region.</param>
        /// <param name="cellSpan">The dimensions of the cells</param>
        /// <returns>True if the region intersects the cell, otherwise false.</returns>
        public static bool IsIntersectingWithCell(Vector3 cellCenter, ImsRegion region, Vector3 cellSpan)
        {
            Vector3 distance = new Vector3(0, 0, 0);
            Vector3 max = cellCenter + (cellSpan / 2);
            Vector3 min = cellCenter - (cellSpan / 2);

            // Closest X
            if (region.Centroid.X < min.X)
            {
                distance.X = region.Centroid.X - min.X;
            }
            else if (region.Centroid.X > max.X)
            {
                distance.X = region.Centroid.X - max.X;
            }

            // Closest Y
            if (region.Centroid.Y < min.Y)
            {
                distance.Y = region.Centroid.Y - min.Y;
            }
            else if (region.Centroid.Y > max.Y)
            {
                distance.Y = region.Centroid.Y - max.Y;
            }

            // Closest Z
            if (region.Centroid.Z < min.Z)
            {
                distance.Z = region.Centroid.Z - min.Z;
            }
            else if (region.Centroid.Z > max.Z)
            {
                distance.Z = region.Centroid.Z - max.Z;
            }

            return distance.Magnitude < region.Radius;
        }

        /// <summary>
        /// For a given dimension calculate the center of the cell in which the
        /// object resides, thus finding the appropriate cell centroid. 
        /// </summary>
        /// <param name="centroid">Coordinate of object centroid in dimension considered.</param>
        /// <param name="span">The cell span in the dimension considered.</param>
        /// <returns>The coordinate of the centroid of the found cell in the dimension considered.</returns>
        private static float CalculateCellForDimension(float centroid, float span)
        {
            // The amount needed to round so that the position of the object
            // when rounded down by the cast to int returns the correct cell.
            double round = Math.Sign(centroid) / 2.000000001;

            // Calculate the number cells between the desired cell and the
            // origin cell, by the integer division of the position of the
            // object by the width of cells, plus a rounding offset.
            int cell = (int)((centroid / span) + round);

            // Calculate the position of the centroid of the desired cell given
            // the number of cells it is from the origin times the width of a cell.
            return cell * span;
        }
    }
}
