//-----------------------------------------------------------------------
// <copyright file="DistributedImClientProxy.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2010 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace Badumna.InterestManagement
{
    using System.Diagnostics;

    using Badumna.Core;
    using Badumna.DistributedHashTable;

    /// <summary>
    /// Proxy for distributed IM client (using proxy pattern).
    /// </summary>
    internal class DistributedImClientProxy : InterestManagementClientProxy
    {
        /// <summary>
        /// The DHT protocol.
        /// </summary>
        private IDhtProtocol protocolLayer;

        /// <summary>
        /// Initializes a new instance of the DistributedImClientProxy class.
        /// </summary>
        /// <param name="protocol">The DHT protocol.</param>
        /// <param name="clientObject">The protocol of the proxy subject.</param>
        public DistributedImClientProxy(IDhtProtocol protocol, InterestManagementClientProtocol clientObject)
            : base(protocol.Parser, clientObject)
        {
            this.protocolLayer = protocol;
        }

        /// <summary>
        /// Prepare an envelope for a messages to the subject.
        /// </summary>
        /// <returns>The envelope.</returns>
        protected override IEnvelope PrepareReplyMessage()
        {
            return this.protocolLayer.GetMessageFor(this.MessageDestination, this.MessageQos);
        }

        /// <summary>
        /// Send a message to the subject.
        /// </summary>
        /// <param name="envelope">The envelope for sending the message.</param>
        protected override void DispatchMessage(IEnvelope envelope)
        {
            Debug.Assert(null != envelope, "The envelope cannot be null.");
            Debug.Assert(envelope is DhtEnvelope, "Distributed IM client messages must use DHT envelopes.");
            this.protocolLayer.SendMessage(envelope as DhtEnvelope);
        }
    }
}
