//-----------------------------------------------------------------------
// <copyright file="DistributedImServerProxy.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2010 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.InterestManagement
{
    using System.Diagnostics;
    using Badumna.Core;
    using Badumna.DistributedHashTable;
    using Badumna.Utilities;

    /// <summary>
    /// Proxy for the distributed IM server (using proxy pattern).
    /// </summary>
    internal class DistributedImServerProxy : InterestManagementServerProxy, IDistributedImServerProxy
    {
        /// <summary>
        /// The DHT protocol for communications.
        /// </summary>
        private IDhtProtocol protocolLayer;

        /// <summary>
        /// TODO: documentation.
        /// </summary>
        private string destinationKeyString;

        /// <summary>
        /// Initializes a new instance of the DistributedImServerProxy class.
        /// </summary>
        /// <param name="protocolLayer">The DHT protocol for communications.</param>
        /// <param name="serverObject">The server object (for accessing method for RPC calls).</param>
        public DistributedImServerProxy(IDhtProtocol protocolLayer, InterestManagementServerProtocol serverObject)
            : base(protocolLayer.Parser, serverObject)
        {
            this.protocolLayer = protocolLayer;
        }

        /// <summary>
        /// Gets or sets the destination key string.
        /// </summary>
        public string DestinationKeyString
        {
            get { return this.destinationKeyString; }
            set { this.destinationKeyString = value; }
        }

        /// <summary>
        /// Prepares an envelope for messages to the server.
        /// </summary>
        /// <returns>The envelope.</returns>
        protected override IEnvelope PrepareRequestMessage()
        {
            Debug.Assert(this.destinationKeyString != null, "The destination key string cannot be null.");

            HashKey destinationKey = HashKey.Hash(this.destinationKeyString);
            Logger.TraceInformation(LogTag.InterestManagement, "Sending request to key : {0} -- {1}", this.destinationKeyString, destinationKey);

            // all requests will be sent to the destination and its redundent peer. 
            IEnvelope envelope = this.protocolLayer.GetMessageFor(
                destinationKey,
                QualityOfService.Reliable,
                Parameters.TotalNumberOfRedundantCellServers);
            Debug.Assert(null != envelope, "The envelope cannot be null.");

            this.destinationKeyString = null;
            return envelope;
        }

        /// <summary>
        /// Send messages to the server.
        /// </summary>
        /// <param name="envelope">The envelope containing the message.</param>
        protected override void DispatchMessage(IEnvelope envelope)
        {
            Debug.Assert(null != envelope, "The envelope cannot be null.");
            Debug.Assert(envelope is DhtEnvelope, "The envelope must be a DHT envelope.");

            this.protocolLayer.SendMessage(envelope as DhtEnvelope);
        }
    }
}
