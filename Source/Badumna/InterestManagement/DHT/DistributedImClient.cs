//-----------------------------------------------------------------------
// <copyright file="DistributedImClient.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2010 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.InterestManagement
{
    using System.Collections.Generic;
    using Badumna.Core;
    using Badumna.DataTypes;
    using Badumna.DistributedHashTable;

    /// <summary>
    /// Client for distributed IM service.
    /// </summary>
    internal class DistributedImClient : InterestManagementClient
    {
        /// <summary>
        /// Initializes a new instance of the DistributedImClient class.
        /// </summary>
        /// <param name="protocol">The DHT protocol.</param>
        public DistributedImClient(IDhtProtocol protocol)
            : base(protocol.Parser)
        {
        }

        /// <summary>
        /// Handle status responses.
        /// </summary>
        /// <param name="id">TODO: id documentation.</param>
        /// <param name="requestInfo">The status response to a request.</param>
        [DhtProtocolMethod(DhtMethod.StatusResponse)]
        public override void StatusResponse(BadumnaId id, RequestStatus requestInfo)
        {
            base.StatusResponse(id, requestInfo);
        }

        /// <summary>
        /// Handle intersection notifications.
        /// </summary>
        /// <param name="id">TODO: id documentation.</param>
        /// <param name="enterList">A list of event arguments for intersections being signalled.</param>
        [DhtProtocolMethod(DhtMethod.IntersectionNotification)]
        public override void IntersectionNotification(BadumnaId id, IList<ImsEventArgs> enterList)
        {
            base.IntersectionNotification(id, enterList);
        }
    }
}
