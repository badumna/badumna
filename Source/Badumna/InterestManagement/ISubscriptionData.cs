//-----------------------------------------------------------------------
// <copyright file="ISubscriptionData.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2010 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Badumna.InterestManagement
{
    using System;
    using Badumna.Core;
    using Badumna.Utilities;

    /// <summary>
    /// TODO: documentation.
    /// </summary>
    internal interface ISubscriptionData
    {
        /// <summary>
        /// Gets the IMS region.
        /// </summary>
        ImsRegion Region { get; }

        /// <summary>
        /// Gets the scene name.
        /// </summary>
        QualifiedName SceneName { get; }

        /// <summary>
        /// Gets the estimated expiration time.
        /// </summary>
        TimeSpan EstimatedExpirationTime { get; }

        /// <summary>
        /// Pass enter notifications to supplied handler.
        /// </summary>
        /// <param name="sender">The event sender.</param>
        /// <param name="args">The IMS event arguments.</param>
        void EnterNotification(object sender, ImsEventArgs args);

        /// <summary>
        /// Pass status notification to supplied handler.
        /// </summary>
        /// <param name="sender">The event sender.</param>
        /// <param name="info">The request status info.</param>
        void Status(object sender, RequestStatus info);

        /// <summary>
        /// Reste the expiration time.
        /// </summary>
        /// <param name="timeToLiveSeconds">The time from now (in seconds) to reset to.</param>
        void ResetExpirationTime(uint timeToLiveSeconds);
    }
}
