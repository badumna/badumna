//-----------------------------------------------------------------------
// <copyright file="InterestManagementClientProxy.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2010 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.InterestManagement
{
    using System.Collections.Generic;
    using System.Diagnostics;

    using Badumna.Core;
    using Badumna.DataTypes;

    /// <summary>
    /// TODO: documentation.
    /// </summary>
    internal abstract class InterestManagementClientProxy : IInterestManagementClientProxy
    {
        /// <summary>
        /// Where messgaes should be sent to.
        /// </summary>
        private PeerAddress messageDestination;

        /// <summary>
        /// The QoS level to use for sending messages.
        /// </summary>
        private QualityOfService messageQos;

        /// <summary>
        /// The message parser used to for RPCs.
        /// </summary>
        private MessageParser messageParser;
        
        /// <summary>
        /// The IM client protocol defining methods that can be called...to do...
        /// </summary>
        private InterestManagementClientProtocol clientProtocol; 

        /// <summary>
        /// Initializes a new instance of the InterestManagementClientProxy class.
        /// </summary>
        /// <param name="parser">The message parser for creating RPCs.</param>
        /// <param name="clientProtocol">The client protocol defining methods that can be called.</param>
        public InterestManagementClientProxy(MessageParser parser, InterestManagementClientProtocol clientProtocol)
        {
            this.messageParser = parser;
            this.clientProtocol = clientProtocol;
        }

        /// <summary>
        /// Gets or sets the destination for messages sent via the proxy.
        /// </summary>
        public PeerAddress MessageDestination
        {
            get { return this.messageDestination; }
            set { this.messageDestination = value; }
        }

        /// <summary>
        /// Gets or sets the QoS to use for messages.
        /// </summary>
        public QualityOfService MessageQos
        {
            get { return this.messageQos; }
            set { this.messageQos = value; }
        }

        /// <summary>
        /// Deliver responses to the requests received from the client.
        /// </summary>
        /// <param name="id">ID of the object involved in the operation whose request is being responded to.</param>
        /// <param name="requestInfo">The status of the request.</param>
        public void StatusResponse(BadumnaId id, RequestStatus requestInfo)
        {
            this.CheckParameters();

            IEnvelope envelope = this.PrepareReplyMessage();
            envelope.Qos.Priority = QosPriority.High;
             this.messageParser.RemoteCall(envelope, this.clientProtocol.StatusResponse, id, requestInfo);
            this.DispatchMessage(envelope);
            
            this.ClearParameters();
        }

        /// <summary>
        /// Deliver intersection notifications to the client.
        /// </summary>
        /// <param name="id">The ID of the object that has intersected.</param>
        /// <param name="enterList">The IDs of the objects that have been intersected with.</param>
        public void IntersectionNotification(BadumnaId id, IList<ImsEventArgs> enterList)
        {
            this.CheckParameters();

            IEnvelope envelope = this.PrepareReplyMessage();
            envelope.Qos.Priority = QosPriority.High;
            this.messageParser.RemoteCall(envelope, this.clientProtocol.IntersectionNotification, id, enterList);
            this.DispatchMessage(envelope);

            this.ClearParameters();
        }

        /// <summary>
        /// To be overridden to prepare a message with a destination and QoS etc.
        /// </summary>
        /// <returns>An envelope for a message.</returns>
        protected abstract IEnvelope PrepareReplyMessage();

        /// <summary>
        /// To be overriden to dispatch messages.
        /// </summary>
        /// <param name="envelope">The envelope containing the message.</param>
        protected abstract void DispatchMessage(IEnvelope envelope);

        /// <summary>
        /// Checks that message parameters exist (destination and QoS).
        /// </summary>
        protected virtual void CheckParameters()
        {
            Debug.Assert(this.messageDestination != null, "Destination is required.");
            Debug.Assert(this.messageQos != null, "QoS is required.");
        }

        /// <summary>
        /// Clears destination and QoS.
        /// </summary>
        protected virtual void ClearParameters()
        {
            this.messageDestination = null;
            this.messageQos = null;
        }
    }
}
