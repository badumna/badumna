//-----------------------------------------------------------------------
// <copyright file="ImsEventArgs.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2010 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace Badumna.InterestManagement
{
    using System;

    using Badumna.Core;
    using Badumna.DataTypes;

    /// <summary>
    /// TODO: documentation.
    /// </summary>
    internal class ImsEventArgs : EventArgs, IParseable
    {
        /// <summary>
        /// The ID of the other region.
        /// </summary>
        private BadumnaId otherRegionId;

        /// <summary>
        /// The type of the other region.
        /// </summary>
        private RegionType otherRegionType;

        /// <summary>
        /// Initializes a new instance of the ImsEventArgs class.
        /// </summary>
        /// <remarks>For IParseable.</remarks>
        public ImsEventArgs()
        {
        }

        /// <summary>
        /// Initializes a new instance of the ImsEventArgs class.
        /// </summary>
        /// <param name="otherRegion">The other region ... to do...</param>
        public ImsEventArgs(ImsRegion otherRegion)
        {
            this.otherRegionId = otherRegion.Guid;
            this.otherRegionType = otherRegion.Type;
        }

        /// <summary>
        /// Initializes a new instance of the ImsEventArgs class.
        /// </summary>
        /// <param name="otherRegionId">The ID of the other region.</param>
        /// <param name="otherRegionType">The type of the other region.</param>
        public ImsEventArgs(BadumnaId otherRegionId, RegionType otherRegionType)
        {
            this.otherRegionId = otherRegionId;
            this.otherRegionType = otherRegionType;
        }

        /// <summary>
        /// Gets the ID of the other region.
        /// </summary>
        public BadumnaId OtherRegionId
        {
            get { return this.otherRegionId; }
        }

        /// <summary>
        /// Gets the type of the other region.
        /// </summary>
        public RegionType OtherRegionType
        {
            get { return this.otherRegionType; }
        }

        /// <summary>
        /// Serialize this object to a message buffer.
        /// </summary>
        /// <remarks>For IParseable.</remarks>
        /// <param name="message">The message to write data to.</param>
        /// <param name="parameterType">The parameter is not used.</param>
        public void ToMessage(MessageBuffer message, Type parameterType)
        {
            message.PutObject(this.otherRegionId, typeof(BadumnaId));
            message.Write((byte)this.otherRegionType);
        }

        /// <summary>
        /// Initialize this object with data from a message buffer.
        /// </summary>
        /// <remarks>For IParseable.</remarks>
        /// <param name="message">The messgae buffer read data from.</param>
        public void FromMessage(MessageBuffer message)
        {
            this.otherRegionId = message.Read<BadumnaId>();
            this.otherRegionType = (RegionType)message.ReadByte();
        }

        /// <summary>
        /// Write the ID and type of the other region to a string.
        /// </summary>
        /// <returns>A string containing the other region's ID and type.</returns>
        public override string ToString()
        {
            return this.otherRegionId.ToString() + "[" + this.otherRegionType.ToString() + "]";
        } 
    }
}
