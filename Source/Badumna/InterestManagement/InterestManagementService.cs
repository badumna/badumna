﻿//------------------------------------------------------------------------------
// <copyright file="InterestManagementService.cs" company="National ICT Australia Limited">
//     Copyright (c) 2012 National ICT Australia Limited. All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using Badumna.Utilities;
using Badumna.Utilities.Logging;

namespace Badumna.InterestManagement
{
    /// <summary>
    /// Code common to all interest management services.
    /// </summary>
    internal abstract class InterestManagementService : IInterestManagementService
    {
        /// <summary>
        /// The number of seconds it takes for the entity to cross the inflated area.
        /// </summary>
        private const int InflationFactor = 3;

        /// <summary>
        /// The default amount of inflation for subscription regions.
        /// </summary>
        private const float DefaultInflation = 30.0f;

        /// <summary>
        /// A value indicating whether the cell size is fixed.
        /// </summary>
        private bool cellSizeFixed;

        /// <summary>
        /// The maximum speed of all entities.
        /// </summary>
        private float maximumSpeed = 0.0f;

        /// <summary>
        /// The maximum interest radius registered in <see cref="Optimize"/> call.
        /// </summary>
        private float maximumInterestRadius = 0f;

        /// <summary>
        /// Initializes a new instance of the InterestManagementService class.
        /// </summary>
        public InterestManagementService()
        {
            this.SubscriptionInflation = InterestManagementService.DefaultInflation;
        }

        /// <inheritdoc />
        public float MaximumInterestRadius
        {
            get { return this.maximumInterestRadius; }
        }

        /// <inheritdoc />
        public float SubscriptionInflation { get; private set; }

        /// <inheritdoc />
        public void InsertRegion(QualifiedName sceneName, ImsRegion region, uint timeToLiveSeconds, EventHandler<ImsEventArgs> enterNotificationHandler, EventHandler<ImsStatusEventArgs> statusHandler)
        {
            this.cellSizeFixed = true;
            this.InsertRegionImplementation(sceneName, region, timeToLiveSeconds, enterNotificationHandler, statusHandler);
        }

        /// <inheritdoc />
        public abstract void ModifyRegion(QualifiedName sceneName, ImsRegion region, uint timeToLiveSeconds, byte clientsKnownCollisions);

        /// <inheritdoc />
        public abstract void RemoveRegion(QualifiedName sceneName, ImsRegion region);

        /// <inheritdoc />
        public abstract void RemoveRegionWithoutNotification(QualifiedName sceneName, ImsRegion region);

        /// <inheritdoc />
        public abstract void DetachSubscription(QualifiedName sceneName, ImsRegion region);

        /// <inheritdoc />
        public virtual void Optimize(float maximumInterestRadius, float maximumSpeed)
        {
            if (this.cellSizeFixed)
            {
                throw new InvalidOperationException("Cannot optimize interest management after it is already underway.");
            }

            this.maximumInterestRadius = Math.Max(maximumInterestRadius, this.maximumInterestRadius);
            this.maximumSpeed = Math.Max(this.maximumSpeed, maximumSpeed);

            // The entity should take at least InflationFactor seconds to cross the inflated area. 
            this.SubscriptionInflation = this.maximumSpeed * InterestManagementService.InflationFactor;

            Logger.TraceInformation(LogTag.InterestManagement | LogTag.Event, "Subscription inflation = {0}", this.SubscriptionInflation);
        }

        /// <inheritdoc />
        protected abstract void InsertRegionImplementation(QualifiedName sceneName, ImsRegion region, uint timeToLiveSeconds, EventHandler<ImsEventArgs> enterNotificationHandler, EventHandler<ImsStatusEventArgs> statusHandler);
    }
}
