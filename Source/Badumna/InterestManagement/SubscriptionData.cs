//-----------------------------------------------------------------------
// <copyright file="SubscriptionData.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2010 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Badumna.InterestManagement
{
    using System;
    using Badumna.Core;
    using Badumna.Utilities;

    /// <summary>
    /// Delegate for creating SubscriptionData objects.
    /// </summary>
    /// <param name="sceneName">The name of the scene.</param>
    /// <param name="region">The region subscribed to.</param>
    /// <param name="timeToLiveSeconds">TODO: documentation.</param>
    /// <param name="enterNotificationHandler">A delegate for handling enter notifications.</param>
    /// <param name="statusHandler">A delegate for handling status notifications.</param>
    /// <param name="timeKeeper">The time keeper.</param>
    /// <returns>A new SubscriptionData object.</returns>
    internal delegate ISubscriptionData SubscriptionDataFactory(
        QualifiedName sceneName,
        ImsRegion region,
        uint timeToLiveSeconds,
        EventHandler<ImsEventArgs> enterNotificationHandler,
        EventHandler<ImsStatusEventArgs> statusHandler,
        ITime timeKeeper);

    /// <summary>
    /// TODO: documentation.
    /// </summary>
    internal class SubscriptionData : ISubscriptionData
    {
        /// <summary>
        /// A delegate for handling enter notifications.
        /// </summary>
        private EventHandler<ImsEventArgs> enterNotificationHandler;
        
        /// <summary>
        /// A delegate for handling status notifications.
        /// </summary>
        private EventHandler<ImsStatusEventArgs> statusHandler;

        /// <summary>
        /// The IMS region.
        /// </summary>
        private ImsRegion region;
        
        /// <summary>
        /// The name of the scene.
        /// </summary>
        private QualifiedName sceneName;

        /// <summary>
        /// The estimated expiration time (relative to Network Event Queue time).
        /// </summary>
        private TimeSpan estimatedExpirationTime;

        /// <summary>
        /// Provides access to the current time.
        /// </summary>
        private ITime timeKeeper;

        /// <summary>
        /// Initializes a new instance of the SubscriptionData class.
        /// </summary>
        /// <param name="sceneName">The name of the scene.</param>
        /// <param name="region">The region subscribed to.</param>
        /// <param name="timeToLiveSeconds">TODO: documentation.</param>
        /// <param name="enterNotificationHandler">A delegate for handling enter notifications.</param>
        /// <param name="statusHandler">A delegate for handling status notifications.</param>
        /// <param name="timeKeeper">Provides access to the current time.</param>
        public SubscriptionData(
            QualifiedName sceneName,
            ImsRegion region,
            uint timeToLiveSeconds,
            EventHandler<ImsEventArgs> enterNotificationHandler,
            EventHandler<ImsStatusEventArgs> statusHandler,
            ITime timeKeeper)
        {
            this.timeKeeper = timeKeeper;
            this.region = new ImsRegion(region);
            this.sceneName = sceneName;
            this.ResetExpirationTime(timeToLiveSeconds);
            this.enterNotificationHandler = enterNotificationHandler;
            this.statusHandler = statusHandler;
        }

        /// <summary>
        /// Gets the IMS region.
        /// </summary>
        public ImsRegion Region
        {
            get { return this.region; }
        }

        /// <summary>
        /// Gets the scene name.
        /// </summary>
        public QualifiedName SceneName
        {
            get { return this.sceneName; }
        }

        /// <summary>
        /// Gets the estimated expiration time.
        /// </summary>
        public TimeSpan EstimatedExpirationTime
        {
            get { return this.estimatedExpirationTime; }
        }

        /// <summary>
        /// Pass enter notifications to supplied handler.
        /// </summary>
        /// <param name="sender">The event sender.</param>
        /// <param name="args">The IMS event arguments.</param>
        public void EnterNotification(object sender, ImsEventArgs args)
        {
            if (this.enterNotificationHandler != null)
            {
                this.enterNotificationHandler(sender, args);
            }
        }

        /// <summary>
        /// Pass status notification to supplied handler.
        /// </summary>
        /// <param name="sender">The event sender.</param>
        /// <param name="info">The request status info.</param>
        public void Status(object sender, RequestStatus info)
        {
            if (this.statusHandler != null)
            {
                this.statusHandler(sender, new ImsStatusEventArgs(this.region.Guid, info));
            }
        }

        /// <summary>
        /// Reste the expiration time.
        /// </summary>
        /// <param name="timeToLiveSeconds">The time from now (in seconds) to reset to.</param>
        public void ResetExpirationTime(uint timeToLiveSeconds)
        {
            this.estimatedExpirationTime = this.timeKeeper.Now + TimeSpan.FromSeconds(timeToLiveSeconds);
        }
    }
}
