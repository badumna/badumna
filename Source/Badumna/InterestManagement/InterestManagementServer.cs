//-----------------------------------------------------------------------
// <copyright file="InterestManagementServer.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2010 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.InterestManagement
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;

    using Badumna.Core;
    using Badumna.Utilities;

    /// <summary>
    /// TODO: documentation.
    /// </summary>
    internal abstract class InterestManagementServer : InterestManagementServerProtocol
    {
        /// <summary>
        /// TODO: this only seems to be used by test code? Should it be removed?
        /// </summary>
        public const int GarbageCollectionTimeoutSeconds = FlatObjectStore.GarbageCollectionTimeoutSeconds;

        private readonly ObjectStoreFactory objectStoreFactory;

        /// <summary>
        /// Maps scene names to object stores.
        /// </summary>
        private Dictionary<QualifiedName, ISpatialObjectStore> scenes = new Dictionary<QualifiedName, ISpatialObjectStore>(new QualifiedNameEqualityComparer());

        /// <summary>
        /// The queue to use for scheduling events.
        /// </summary>
        private INetworkEventScheduler eventQueue;

        /// <summary>
        /// The regular task used to periodically remove empty ISpatialObjectStores
        /// </summary>
        private RegularTask garbageStoreCollectionTask;

        /// <summary>
        /// Initializes a new instance of the InterestManagementServer class.
        /// </summary>
        /// <param name="protocolLayer">The protocol layer to register methods on.</param>
        /// <param name="eventQueue">The event queue to schedule events on.</param>
        /// <param name="connectivityReporter">Reports on network connectivity status.</param>
        public InterestManagementServer(MessageParser protocolLayer, INetworkEventScheduler eventQueue, INetworkConnectivityReporter connectivityReporter)
            : this(protocolLayer, eventQueue, connectivityReporter, (q) => { return new FlatObjectStore(q); })
        {
        }

        /// <summary>
        /// Initializes a new instance of the InterestManagementServer class.
        /// </summary>
        /// <param name="protocolLayer">The protocol layer to register methods on.</param>
        /// <param name="eventQueue">The event queue to schedule events on.</param>
        /// <param name="connectivityReporter">Reports on network connectivity status.</param>
        /// <param name="objectStoreFactory">A factory for object stores.</param>
        public InterestManagementServer(
            MessageParser protocolLayer,
            INetworkEventScheduler eventQueue,
            INetworkConnectivityReporter connectivityReporter,
            ObjectStoreFactory objectStoreFactory)
        {
            this.eventQueue = eventQueue;

            if (objectStoreFactory == null)
            {
                throw new ArgumentNullException("objectStoreFactory");
            }

            this.objectStoreFactory = objectStoreFactory;

            this.garbageStoreCollectionTask = new RegularTask(
                "IMStoreCollectionTask", 
                Parameters.DefaultStoreGarbageCollectionInterval,
                eventQueue,
                connectivityReporter,
                this.RemoveEmptyStores);
            this.garbageStoreCollectionTask.Start();

            protocolLayer.RegisterMethodsIn(this);
        }

        /// <summary>
        /// Gets the dictionary of object stores by scene.
        /// </summary>
        protected Dictionary<QualifiedName, ISpatialObjectStore> Scenes
        {
            get { return this.scenes; }
        }

        /// <summary>
        /// Gets the IM client proxy.
        /// </summary>
        protected abstract IInterestManagementClientProxy ClientProxy { get; }

        /// <inheritdoc />
        public override void InsertRequest(QualifiedName sceneName, ImsRegion region, uint timeToLiveSeconds)
        {
            ISpatialObjectStore scene = null;
            RequestStatus result = new RequestStatus();

            if (null == region || region.Radius <= 0)
            {
                result.Status = RequestStatus.Statuses.InvalidRegion;
            }
            else
            {
                try
                {
                    scene = this.GetOrCreateObjectStore(sceneName);


                    if (scene != null)
                    {
                        Logger.TraceInformation(
                            LogTag.InterestManagement,
                            "Inserted region {0} {1},{2},{3} r{4}",
                            region.Guid,
                            region.Centroid.X,
                            region.Centroid.Y,
                            region.Centroid.Z,
                            region.Radius);
                        scene.AddOrModifyObject(region, timeToLiveSeconds, 0);
                    }
                    else
                    {
                        // TO DO: This can only be hit if CreateObjectStore throws an ImsException
                        // in a class derived from this one, and there are currently no derived
                        // classes that will do that.

                        // TODO: should this be a NoSuchScene status?
                        result.Status = RequestStatus.Statuses.NoSuchService;
                    }
                }
                catch (ImsException e)
                {
                    Logger.TraceWarning(LogTag.InterestManagement, "Caught exception in SpatialObjectStore.AddOrModifyObject() : {0}", e.Message);
                    result.Status = e.Status;
                }
            }

            this.SetClientProxyDestination(scene, region.Guid.Address, this.GetQoS(false));
            this.ClientProxy.StatusResponse(region.Guid, result);
        }

        /// <inheritdoc />
        public override void RemoveRequest(QualifiedName sceneName, ImsRegion region)
        {
            Debug.Assert(region != null, "region cannot be null");
            ISpatialObjectStore scene = null;
            RequestStatus result = new RequestStatus();

            try
            {
                scene = this.GetOrCreateObjectStore(sceneName);

                // Scene cannot be null, unless future child classes make it so.....
                Debug.Assert(scene != null, "Scene creation cannot fail, can it?");

                if (scene != null)
                {
                    Logger.TraceInformation(LogTag.InterestManagement, "Removing region {0}", region.Guid);
                    scene.RemoveObject(region);
                }
            }
            catch (ImsException e)
            {
                Logger.TraceWarning(LogTag.InterestManagement, "Caught exception in SpatialObjectStore.RemoveObject() : {0}", e.Message);
                result.Status = e.Status;
            }

            this.SetClientProxyDestination(scene, region.Guid.Address, QualityOfService.Unreliable);
            this.ClientProxy.StatusResponse(region.Guid, result);
        }

        /// <inheritdoc />
        public override void RemoveWithoutNotificationRequest(QualifiedName sceneName, ImsRegion region)
        {
            Debug.Assert(region != null, "region cannot be null");
            ISpatialObjectStore scene = null;
            RequestStatus result = new RequestStatus();

            try
            {
                scene = this.GetOrCreateObjectStore(sceneName);

                // TO DO: scene (which should be called 'store'!) will never be null...
                if (scene != null)
                {
                    Logger.TraceInformation(LogTag.InterestManagement, "Removing without notification, region {0}", region.Guid);
                    scene.RemoveWithoutNotification(region);
                }
            }
            catch (ImsException e)
            {
                Logger.TraceWarning(LogTag.InterestManagement, "Caught exception in SpatialObjectStore.RemoveObject() : {0}", e.Message);
                result.Status = e.Status;
            }

            this.SetClientProxyDestination(scene, region.Guid.Address, QualityOfService.Unreliable);
            this.ClientProxy.StatusResponse(region.Guid, result);
        }

        /// <inheritdoc />
        public override void ModifyRequest(
            QualifiedName sceneName,
            ImsRegion region,
            uint timeToLiveSeconds,
            byte currentNumberOfInteresections)
        {
            Debug.Assert(region != null, "region cannot be null");
            
            ISpatialObjectStore scene = null;
            RequestStatus result = new RequestStatus();

            if (region.Radius <= 0)
            {
                result.Status = RequestStatus.Statuses.InvalidRegion;
            }
            else
            {
                try
                {
                    scene = this.GetOrCreateObjectStore(sceneName);

                    if (scene != null)
                    {
                        Logger.TraceInformation(LogTag.InterestManagement, "User modifies subscription {0}", region.Guid);
                        scene.AddOrModifyObject(region, timeToLiveSeconds, currentNumberOfInteresections);
                    }
                }
                catch (ImsException e)
                {
                    Logger.TraceWarning(LogTag.InterestManagement, "Caught exception in SpatialObjectStore.AddOrModifyObject() : " + e.Message);
                    result.Status = e.Status;
                }
            }

            if (result.Status != RequestStatus.Statuses.Complete)
            {
                this.SetClientProxyDestination(scene, region.Guid.Address, this.GetQoS(false));
                this.ClientProxy.StatusResponse(region.Guid, result);
            }
        }

        /// <summary>
        /// Gets the object store for a given scene name.
        /// </summary>
        /// <param name="sceneName">The scene name.</param>
        /// <returns>The associated object store.</returns>
        protected virtual ISpatialObjectStore GetObjectStore(QualifiedName sceneName)
        {
            ISpatialObjectStore store;
            this.Scenes.TryGetValue(sceneName, out store);
            return store;
        }

        /// <summary>
        /// Creates an object store and associates it with a given scene name.
        /// </summary>
        /// <param name="sceneName">The scene name to associate the object store with.</param>
        /// <returns>The newly created object store.</returns>
        protected virtual ISpatialObjectStore CreateObjectStore(QualifiedName sceneName)
        {
            ISpatialObjectStore store = this.objectStoreFactory.Invoke(eventQueue);

            // The check to see if the store is a flat store allows the factory
            // to return a mock ISpatialObjectStore for testing.
            FlatObjectStore flatStore = store as FlatObjectStore;
            if (flatStore != null)
            {
                flatStore.ExpirationNotification += this.ExpirationHandler;
            }

            this.Scenes[sceneName] = store;
            return store;
        }

        /// <summary>
        /// Remove an object store by scene names.
        /// </summary>
        /// <param name="sceneName">The scene name.</param>
        protected virtual void RemoveObjectStore(QualifiedName sceneName)
        {
            this.Scenes.Remove(sceneName);
        }

        /// <summary>
        /// TODO: documentation.
        /// </summary>
        /// <param name="sourceStore">TODO: sourceStore documentation. </param>
        /// <param name="destinationPeer">TODO: destinationPeer documentation.</param>
        /// <param name="qos">The desired quality of service for... to do.</param>
        protected virtual void SetClientProxyDestination(
            ISpatialObjectStore sourceStore,
            PeerAddress destinationPeer,
            QualityOfService qos)
        {
            this.ClientProxy.MessageDestination = destinationPeer;
            this.ClientProxy.MessageQos = qos;
        }

        /// <summary>
        /// Handle expiration notifications from object stores.
        /// </summary>
        /// <param name="sender">The event sender.</param>
        /// <param name="e">The event arguments.</param>
        protected void ExpirationHandler(object sender, ExpireEventArgs e)
        {
            if (null != e && e.ExpiredObject is ImsRegion)
            {
                this.ExpirationHandler((ISpatialObjectStore)sender, (ImsRegion)e.ExpiredObject);
            }
        }

        /// <summary>
        /// Handle IMS Region expiration notifications.
        /// </summary>
        /// <param name="store">The store sending the notification.</param>
        /// <param name="region">The region that has expired.</param>
        protected void ExpirationHandler(ISpatialObjectStore store, ImsRegion region)
        {
            if (!region.Guid.Address.IsValid)
            {
                Logger.TraceError(LogTag.InterestManagement, "Invalid address attached to spatial object {0}", region.Guid);
                return;
            }

            this.SetClientProxyDestination(store, region.Guid.Address, this.GetQoS(true));
            this.ClientProxy.StatusResponse(region.Guid, new RequestStatus(RequestStatus.Statuses.Expired));
        }

        /// <summary>
        /// Handle intersection events.
        /// </summary>
        /// <param name="sender">The event sender.</param>
        /// <param name="e">The event arguments.</param>
        protected void IntersectionHandler(object sender, IntersectionEventArgs e)
        {
            List<ImsEventArgs> intersectingRegions = new List<ImsEventArgs>();

            foreach (ImsRegion region in e.ChangedRegions)
            {
                if (e.Region.RequiresIntersectionNotification(region))
                {
                    intersectingRegions.Add(new ImsEventArgs(region));
                }
            }

            if (intersectingRegions.Count == 0)
            {
                return;
            }

            Logger.TraceInformation(LogTag.InterestManagement, "Intersection detected between {0} and [{1}]", e.Region.Guid, CollectionUtils.Join(intersectingRegions, ", "));

            this.SetClientProxyDestination(sender as ISpatialObjectStore, e.Region.Guid.Address, this.GetQoS(true));
            this.ClientProxy.IntersectionNotification(e.Region.Guid, intersectingRegions);
        }

        /// <summary>
        /// Gets an object store, creating it if required.
        /// </summary>
        /// <param name="sceneName">The scene name associated with the store.</param>
        /// <returns>The object store associated with a given scene name.</returns>
        private ISpatialObjectStore GetOrCreateObjectStore(QualifiedName sceneName)
        {
            ISpatialObjectStore store = this.GetObjectStore(sceneName);

            if (store == null)
            {
                try
                {
                    store = this.CreateObjectStore(sceneName);

                    // TODO : Make sure that these are un-referenced when this
                    //        peer is no longer responsible for this IMS cell.
                    store.IntersectionNotification += this.IntersectionHandler;

                    Logger.TraceInformation(LogTag.InterestManagement, "Added scene store {0}", sceneName);
                }
                catch (ImsException e)
                {
                    Logger.TraceException(LogLevel.Warning, e, "Caught exception when trying to create ims region store");
                }
            }

            return store;
        }
        
        /// <summary>
        /// Gets the quality of service.
        /// </summary>
        /// <param name="delayable">The parameter is not used.</param>
        /// <returns>A quality of service object.</returns>
        private QualityOfService GetQoS(bool delayable)
        {
            // TODO: Don't create a new base QoS each time
            return QualityOfService.Reliable;
            ////return new FailNotificationQos(new QualityOfService(true, delayable ? QualityOfService.DefaultAllowableDelayMilliseconds : 0), 
            ////    new LambdaFunction(delegate(int nthTry)
            ////    {
            ////    }));
        }

        /// <summary>
        /// Removes the empty stores.
        /// </summary>
        private void RemoveEmptyStores()
        {
            var toRemove = new List<QualifiedName>();

            foreach(var kvp in this.scenes)
            {
                if (kvp.Value != null && kvp.Value.IsEmpty)
                {
                    toRemove.Add(kvp.Key);
                }
            }

            foreach (var key in toRemove)
            {
                ISpatialObjectStore store = null;
                if (this.scenes.TryGetValue(key, out store))
                {
                    store.IntersectionNotification -= this.IntersectionHandler;

                    this.scenes.Remove(key);
                }
            }
        }
    }
}
