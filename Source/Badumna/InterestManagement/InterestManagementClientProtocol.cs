//-----------------------------------------------------------------------
// <copyright file="InterestManagementClientProtocol.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2010 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace Badumna.InterestManagement
{
    using System.Collections.Generic;

    using Badumna.DataTypes;

    /// <summary>
    /// TODO: documentation.
    /// </summary>
    internal abstract class InterestManagementClientProtocol
    {
        /// <summary>
        /// Called to return the result of an operation to the client.
        /// </summary>
        /// <param name="id">UniqueNetworkId owned by the client indicating the object involved in the operation</param>
        /// <param name="requestInfo">Result of operation</param>
        public abstract void StatusResponse(BadumnaId id, RequestStatus requestInfo);

        /// <summary>
        /// Notifies the client of new intersections.
        /// </summary>
        /// <param name="id">UniqueNetworkId owned by the client indicating the object getting intersections</param>
        /// <param name="enterList">List of newly intersecting objects</param>
        public abstract void IntersectionNotification(BadumnaId id, IList<ImsEventArgs> enterList);
    }
}
