//-----------------------------------------------------------------------
// <copyright file="ImsStatusEventArgs.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2010 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace Badumna.InterestManagement
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    using Badumna.Core;
    using Badumna.DataTypes;

    /// <summary>
    /// TODO: docuementation.
    /// </summary>
    internal class ImsStatusEventArgs : EventArgs
    {
        /// <summary>
        /// TODO: docuementation.
        /// </summary>
        private BadumnaId id;

        /// <summary>
        /// TODO: docuementation.
        /// </summary>
        private RequestStatus status;

        /// <summary>
        /// TODO: docuementation.
        /// </summary>
        private IList<ImsEventArgs> enterList;

        /// <summary>
        /// Initializes a new instance of the ImsStatusEventArgs class.
        /// </summary>
        /// <param name="id">TODO: ID documentation.</param>
        /// <param name="status">TODO: status documentation.</param>
        public ImsStatusEventArgs(BadumnaId id, RequestStatus status)
            : this(id, status, null)
        {
        }

        /// <summary>
        /// Initializes a new instance of the ImsStatusEventArgs class.
        /// </summary>
        /// <param name="id">TODO: ID documentation.</param>
        /// <param name="status">TODO: status documentation.</param>
        /// <param name="enterList">TODO: enter list documentation.</param>
        public ImsStatusEventArgs(BadumnaId id, RequestStatus status, IList<ImsEventArgs> enterList)
        {
            this.id = id;
            this.status = status;
            this.enterList = enterList ?? new List<ImsEventArgs>();
        }

        /// <summary>
        /// Gets ... to do: documentation.
        /// </summary>
        public BadumnaId Id
        {
            get { return this.id; }
        }

        /// <summary>
        /// Gets ... to do: documentation.
        /// </summary>
        public RequestStatus Status
        {
            get { return this.status; }
        }

        /// <summary>
        /// Gets ... to do: documentation.
        /// </summary>
        public IList<ImsEventArgs> EnterList
        {
            get { return this.enterList; }
        }
    }
}
