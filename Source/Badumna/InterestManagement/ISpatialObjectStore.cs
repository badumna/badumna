//-----------------------------------------------------------------------
// <copyright file="ISpatialObjectStore.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2010 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace Badumna.InterestManagement
{
    using System;
    using System.Collections.Generic;

    using Badumna.Core;
    using Badumna.DataTypes;

    /// <summary>
    /// Delegate for creating object stores.
    /// </summary>
    /// <param name="eventQueue">The queue to schedule network events in.</param>
    /// <returns>A newly created object store.</returns>
    internal delegate ISpatialObjectStore ObjectStoreFactory(INetworkEventScheduler eventQueue);

    /// <summary>
    /// TODO: documentation.
    /// </summary>
    internal interface ISpatialObjectStore
    {
        /// <summary>
        /// Triggered when an intersection occurs.
        /// </summary>
        event EventHandler<IntersectionEventArgs> IntersectionNotification;

        /// <summary>
        /// Triggered when a separation occurs.
        /// </summary>
        event EventHandler<IntersectionEventArgs> SeperationNotification;

        /// <summary>
        /// Gets a value indicating whether... to do.
        /// </summary>
        bool IsEmpty { get; }

        /// <summary>
        /// Gets a count of the objects in the store.
        /// </summary>
        int ObjectCount { get; }

        /// <summary>
        /// Add an object to the store, or modify it, if it already exists.
        /// </summary>
        /// <param name="obj">The object to add.</param>
        /// <param name="timeToLive">The time the object should persist for.</param>
        /// <param name="clientsKnownCollisions">TODO: clientsKnownCollisions documentation.</param>
        void AddOrModifyObject(ImsRegion obj, uint timeToLive, byte clientsKnownCollisions);

        /// <summary>
        /// Remove an object from the store.
        /// </summary>
        /// <param name="obj">The object to remove.</param>
        void RemoveObject(ImsRegion obj);

        /// <summary>
        /// Remove an object from the store without... to do.
        /// </summary>
        /// <param name="obj">The object to remove.</param>
        void RemoveWithoutNotification(ImsRegion obj);

        /// <summary>
        /// Keep an object in the store alive for a new period of time.
        /// </summary>
        /// <param name="objectID">The ID of the object.</param>
        /// <param name="timeToLiveSeconds">The further amount of time to persist the object for.</param>
        void KeepAlive(BadumnaId objectID, uint timeToLiveSeconds);

        /// <summary>
        /// Get a list of the objects within a given region.
        /// </summary>
        /// <param name="region">The region to look within.</param>
        /// <returns>A list of objects within the region.</returns>
        IList<ImsRegion> GetObjectsWithinRegion(ImsRegion region);
    }
}
