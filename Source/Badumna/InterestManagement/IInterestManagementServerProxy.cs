//-----------------------------------------------------------------------
// <copyright file="IInterestManagementServerProxy.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2010 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using Badumna.Utilities;

namespace Badumna.InterestManagement
{
    /// <summary>
    /// Proxy for interest management server for use by the client (proxy pattern).
    /// </summary>
    internal interface IInterestManagementServerProxy
    {
        /// <summary>
        /// Request that a region be inserted.
        /// </summary>
        /// <param name="sceneName">The name of the scene the region belongs to.</param>
        /// <param name="region">The region to insert.</param>
        /// <param name="timeToLiveSeconds">The time the region should live for.</param>
        void InsertRequest(QualifiedName sceneName, ImsRegion region, uint timeToLiveSeconds);

        /// <summary>
        /// Request that a region be removed.
        /// </summary>
        /// <param name="sceneName">The name of the scene the region belongs to.</param>
        /// <param name="region">The region to remove.</param>
        void RemoveRequest(QualifiedName sceneName, ImsRegion region);

        /// <summary>
        /// Request that a region be removed without sending notifications.
        /// </summary>
        /// <param name="sceneName">The name of the scene the region belongs to.</param>
        /// <param name="region">The region to remove.</param>
        void RemoveWithoutNotificationRequest(QualifiedName sceneName, ImsRegion region);

        /// <summary>
        /// Request that a region be modified.
        /// </summary>
        /// <param name="sceneName">The name of the scene the region belongs to.</param>
        /// <param name="region">The region to modify.</param>
        /// <param name="timeToLiveSeconds">The time the region should live for.</param>
        /// <param name="clientsKnownCollisions">The number of known collisions the region has.</param>
        void ModifyRequest(QualifiedName sceneName, ImsRegion region, uint timeToLiveSeconds, byte clientsKnownCollisions);
    }
}
