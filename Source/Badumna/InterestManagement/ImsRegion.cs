//-----------------------------------------------------------------------
// <copyright file="ImsRegion.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2010 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace Badumna.InterestManagement
{
    using System;
    using Badumna.Core;
    using Badumna.DataTypes;
    using Badumna.DistributedHashTable;
    using Badumna.Utilities;

    /// <summary>
    /// ImsRegions define regions of 3D space as spheres or inverse spheres.
    /// There are two types: sources and sinks.  The type determines whether
    /// the region needs notifications of intersections with other regions.
    /// </summary>
    internal class ImsRegion : IParseable, IReplicatedObject, IExpireable
    {
        /// <summary>
        /// The region type.
        /// </summary>
        private RegionType type;

        /// <summary>
        /// The region options.
        /// </summary>
        private RegionOptions options;

        /// <summary>
        /// The centroid of the region.
        /// </summary>
        private Vector3 centroid;

        /// <summary>
        /// The radius of the region.
        /// </summary>
        private float radius;

        /// <summary>
        /// A flag indicating if modification numbers should be ignored when
        /// storing the region on the DHT. Modification numbers are used to
        /// prevent the overwriting of new data with old data.
        /// </summary>
        private bool ignoreModificationNumber = false;

        /// <summary>
        /// The ID of the region.
        /// </summary>
        private BadumnaId guid;

        /// <summary>
        /// A modification number used to prevent the overwriting of new data
        /// with old data.
        /// </summary>
        private CyclicalID.UShortID modificationNumber;

        /// <summary>
        /// The key used to store the region on the DHT.
        /// </summary>
        private HashKey objectKey;

        /// <summary>
        /// The lenght of time (in seconds) the region should remain on the DHT.
        /// </summary>
        private uint timeToLiveSeconds;

        /// <summary>
        /// The time after which the region should expire (and be garbage
        /// collected).
        /// </summary>
        private TimeSpan expirationTime = new TimeSpan();

        /// <summary>
        /// The name of the scene the region belongs to.
        /// </summary>
        private QualifiedName sceneName;

        /// <summary>
        /// Initializes a new instance of the ImsRegion class.
        /// </summary>
        /// <remarks>Default constructor for use as IParseable.</remarks>
        public ImsRegion()
        {
        }

        /// <summary>
        /// Initializes a new instance of the ImsRegion class.
        /// </summary>
        /// <param name="region">The region to copy.</param>
        public ImsRegion(ImsRegion region)
        {
            if (null == region)
            {
                throw new ArgumentNullException("region");
            }

            this.centroid = region.Centroid;
            this.Radius = region.Radius;
            this.guid = region.Guid;
            this.type = region.type;
            this.options = region.options;
            this.sceneName = region.SceneName;
        }

        /// <summary>
        /// Initializes a new instance of the ImsRegion class.
        /// </summary>
        /// <param name="centroid">The centroid of the region.</param>
        /// <param name="radius">The radius of the region.</param>
        /// <param name="guid">The ID of the region.</param>
        /// <param name="type">The type of the region.</param>
        /// <param name="sceneName">The name of the scene the region belongs to.</param>
        public ImsRegion(Vector3 centroid, float radius, BadumnaId guid, RegionType type, QualifiedName sceneName)
        {
            this.centroid = centroid;
            this.Radius = radius;
            this.guid = guid;
            this.type = type;
            this.sceneName = sceneName;
        }

        /// <summary>
        /// Gets the type of the region.
        /// </summary>
        public RegionType Type
        {
            get { return this.type; }
        }

        /// <summary>
        /// Gets or sets the region options.
        /// </summary>
        public RegionOptions Options
        {
            get { return this.options; }
            set { this.options = value; }
        }

        /// <summary>
        /// Gets a value indicating whether the region is an inverse sphere.
        /// </summary>
        public bool IsInverse
        {
            get { return (this.options & RegionOptions.Inverse) > 0; }
        }

        /// <summary>
        /// Gets a value indicating whether the region should recieve notifications of intersections.
        /// </summary>
        public bool IsUninterested
        {
            get { return (this.options & RegionOptions.Uninterested) > 0; }
        }

        /// <summary>
        /// Gets or sets the centroid of the region.
        /// </summary>
        public Vector3 Centroid
        {
            get { return this.centroid; }
            set { this.centroid = value; }
        }

        /// <summary>
        /// Gets or sets the x coordinate of the centroid of the region.
        /// </summary>
        public float X
        {
            get { return this.centroid.X; }
            set { this.centroid.X = value; }
        }

        /// <summary>
        /// Gets or sets the y coordinate of the centroid of the region.
        /// </summary>
        public float Y
        {
            get { return this.centroid.Y; }
            set { this.centroid.Y = value; }
        }

        /// <summary>
        /// Gets or sets the z coordinate of the centroid of the region.
        /// </summary>
        public float Z
        {
            get { return this.centroid.Z; }
            set { this.centroid.Z = value; }
        }

        /// <summary>
        /// Gets or sets the radius of the region.
        /// </summary>
        public float Radius
        {
            get { return this.radius; }
            set { this.radius = value; }
        }

        /// <summary>
        /// Gets a value indicating whether modification numbers should be ignored when
        /// overwriting this replicated object.
        /// </summary>
        public bool IgnoreModificationNumber
        {
            get { return this.ignoreModificationNumber; }
        }

        /// <summary>
        /// Gets or sets the ID of the region.
        /// </summary>
        public BadumnaId Guid
        {
            get { return this.guid; }
            set { this.guid = value; }
        }

        /// <summary>
        /// Gets or sets a modification number (part of IReplicatedObject used to prevent new data
        /// being overwritten by old data).
        /// </summary>
        public CyclicalID.UShortID ModificationNumber
        {
            get { return this.modificationNumber; }
            set { this.modificationNumber = value; }
        }

        /// <summary>
        /// Gets or sets the HashKey (part of IRelicatedObject used to determine where on DHT to
        /// store the object).
        /// </summary>
        public HashKey Key
        {
            get { return this.objectKey; }
            set { this.objectKey = value; }
        }

        /// <summary>
        /// Gets or sets the time (in seconds) this object should remain on the DHT for (part of
        /// IReplicatedObject).
        /// </summary>
        public uint TimeToLiveSeconds
        {
            get { return this.timeToLiveSeconds; }
            set { this.timeToLiveSeconds = value; }
        }

        /// <summary>
        /// Gets or sets the region's expiration time.
        /// </summary>
        /// <remarks>Part of IExpirable interface. Client code will typically set this value
        /// according to the current time and the TTL upon creation, and it will be used by
        /// the garbage collector to get rid of expired regions.</remarks>
        public TimeSpan ExpirationTime
        {
            get { return this.expirationTime; }
            set { this.expirationTime = value; }
        }

        /// <summary>
        /// Gets the name of the scene the region belongs to.
        /// </summary>
        public QualifiedName SceneName
        {
            get { return this.sceneName; }
        }

        /// <summary>
        /// Gets the number of times the region should be replicared on the DHT.
        /// </summary>
        /// <remarks>Part of IReplicatedObject.</remarks>
        public int NumberOfReplicas
        {
            // TODO: exorcise this magic number.
            get { return 5; }
        }

        /// <summary>
        /// Indicates if this IMS region intersects with another.
        /// </summary>
        /// <param name="region">The other IMS region.</param>
        /// <returns><c>true</c> if the regions intersect, otherwise <c>false</c>.</returns>
        public bool IsIntersecting(ImsRegion region)
        {
            if (null == region)
            {
                return false;
            }

            if (this.sceneName != region.SceneName)
            {
                return false;
            }

            // Two inverse spheres always intersect.
            if (this.IsInverse && region.IsInverse)
            {
                return true;
            }

            if (this.IsInverse)
            {
                return !Geometry.SphereContained(this.Centroid, this.Radius, region.Centroid, region.Radius);
            }

            if (region.IsInverse)
            {
                return !Geometry.SphereContained(region.Centroid, region.Radius, this.Centroid, this.Radius);
            }

            return Geometry.SpheresIntersect(this.Centroid, this.Radius, region.Centroid, region.Radius);
        }

        /// <summary>
        /// Indicate whether the region requires intersection notification from another specific region.
        /// </summary>
        /// <param name="region">The other IMS region notifications may be required from.</param>
        /// <returns><c>true</c> if notification is required, otherwise <c>false</c>.</returns>
        public bool RequiresIntersectionNotification(ImsRegion region)
        {
            if (this.IsUninterested)
            {
                return false;  // Uninterested regions don't require any notifications
            }

            if (region.Type == RegionType.Sink)
            {
                // Sources get notified of sinks
                // Previously, notifications were only sent if the source and sink were on different peers
                // but that would prevent validated entities from being replicated to the validator itself.
                // This should not cause too much additional traffic, as an original should not self-intersect
                // more than once, but could cause additional intersection notifications if there are many
                // originals on a peer, and they separate and rejoin often.
                return this.Type == RegionType.Source; // && !this.Guid.Address.Equals(region.Guid.Address);
            }

            return false;
        }

        /// <summary>
        /// Sertialize to message (part of IParseable).
        /// </summary>
        /// <param name="message">The message to write to.</param>
        /// <param name="parameterType">Not used (required by interface).</param>
        public void ToMessage(MessageBuffer message, Type parameterType)
        {
            if (null != message)
            {
                message.Write(this.centroid.X);
                message.Write(this.centroid.Y);
                message.Write(this.centroid.Z);
                message.Write(this.radius);
                message.Write(this.guid);
                message.Write((byte)this.type);
                message.Write((byte)this.options);
                message.Write(this.sceneName);
            }
        }

        /// <summary>
        /// Initialize from message data (part of IParseable).
        /// </summary>
        /// <param name="message">The message.</param>
        public void FromMessage(MessageBuffer message)
        {
            if (null != message)
            {
                this.centroid.X = message.ReadFloat();
                this.centroid.Y = message.ReadFloat();
                this.centroid.Z = message.ReadFloat();
                this.radius = message.ReadFloat();
                this.guid = message.Read<BadumnaId>();
                this.type = (RegionType)message.ReadByte();
                this.options = (RegionOptions)message.ReadByte();
                this.sceneName = message.Read<QualifiedName>();
            }
        }

        /// <summary>
        /// Return a string containing GUID and type.
        /// </summary>
        /// <returns>The string.</returns>
        public override string ToString()
        {
            return this.guid.ToString() + "[" + this.type.ToString() + "]";
        }

        /* 
        public void Render(Graphics context)
        {
            float x = this.Centroid.X - this.Radius;
            float y = -this.Centroid.Z - this.Radius;
            float width = this.Radius * 2.0f;
            float height = width;

            using (Font font = new Font("Courier", 20.0f))
            {
                context.DrawString(this.Guid.ToString(), font, Brushes.Black, x, y);
            }

            if (this.Type == RegionType.ImService)
            {
                if (!this.IsInverse)
                {
                    using (System.Drawing.Brush brush = new System.Drawing.SolidBrush(System.Drawing.Color.FromArgb(75, System.Drawing.Color.Orange)))
                    {
                        context.FillEllipse(brush, x, y, width, height);
                    }
                }
                //else
                //{
                //    using (System.Drawing.Brush brush = new System.Drawing.SolidBrush(System.Drawing.Color.FromArgb(30, System.Drawing.Color.Orange)))
                //    {
                //        System.Drawing.Region region = new System.Drawing.Region();
                //        region.MakeInfinite();

                //        System.Drawing.Drawing2D.GraphicsPath path = new System.Drawing.Drawing2D.GraphicsPath();
                //        path.AddEllipse(x, y, width, height);
                //        region.Xor(path);

                //        context.FillRegion(brush, region);

                //        path.Dispose();
                //        region.Dispose();
                //    }
                //}
            }

            if (this.Type == RegionType.Sink)
            {
                using (System.Drawing.Pen pen = new System.Drawing.Pen(System.Drawing.Brushes.Beige))
                {
                    if (this.IsInverse)
                    {
                        pen.DashPattern = new float[] { 3.0f, 1.0f };
                    }

                    context.DrawEllipse(pen, x, y, width, height);
                }
            }

            if (this.Type == RegionType.Source)
            {
                using (System.Drawing.Pen pen = new System.Drawing.Pen(System.Drawing.Brushes.Red))
                {
                    if (this.IsInverse)
                    {
                        pen.DashPattern = new float[] { 3.0f, 1.0f };
                    }

                    context.DrawEllipse(pen, x, y, width, height);
                }
            }
        }
         */
    }
}