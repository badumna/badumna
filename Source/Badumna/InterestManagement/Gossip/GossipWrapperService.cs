﻿//-----------------------------------------------------------------------
// <copyright file="GossipWrapperService.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2010 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace Badumna.InterestManagement
{
    using System;
    using System.Diagnostics;
    using Badumna.Core;
    using Badumna.DistributedHashTable;
    using Badumna.Transport;
    using Badumna.Utilities;

    /// <summary>
    /// Used to control the interaction of the dht ims and the gossip ims by dropping some modify requests. 
    /// </summary>
    internal class GossipWrapperService : IInterestManagementService
    {
        /// <summary>
        /// The gossip IM service.
        /// </summary>
        private readonly IGossipImService gossipIms;

        /// <summary>
        /// The distributed IM service.
        /// </summary>
        private readonly IInterestManagementService distributedIms;

        /// <summary>
        /// A source of random numbers.
        /// </summary>
        private readonly IRandomNumberGenerator randomNumberGenerator;

        /// <summary>
        /// Initializes a new instance of the GossipWrapperService class.
        /// </summary>
        /// <param name="facade">DHT facade for the distributed IM service.</param>
        /// <param name="allInclusiveFacade">All-inclusive DHT facade for the distributed IM service.</param>
        /// <param name="parent">The parent transport protocol.</param>
        /// <param name="eventQueue">The queue to use for scheduling events.</param>
        /// <param name="timeKeeper">Provides access to the current time.</param>
        /// <param name="connectivityReporter">Reports on network connectivity status.</param>
        public GossipWrapperService(
            DhtFacade facade,
            DhtFacade allInclusiveFacade,
            TransportProtocol parent,
            NetworkEventQueue eventQueue,
            ITime timeKeeper,
            INetworkConnectivityReporter connectivityReporter)
        {
            DistributedImService distributedIms = new DistributedImService(facade, allInclusiveFacade, eventQueue, timeKeeper, connectivityReporter);
            this.gossipIms = new GossipImService(parent, distributedIms.Client, eventQueue, timeKeeper, connectivityReporter);
            this.distributedIms = distributedIms;
            this.randomNumberGenerator = new RandomNumberGenerator();
        }

        /// <summary>
        /// Initializes a new instance of the GossipWrapperService class.
        /// </summary>
        /// <param name="distributedImService">The distributed IM service.</param>
        /// <param name="gossipImService">The gossip IM service.</param>
        /// <param name="randomNumberGenerator">A random number generator.</param>
        public GossipWrapperService(
            IInterestManagementService distributedImService,
            IGossipImService gossipImService,
            IRandomNumberGenerator randomNumberGenerator)
        {
            if (distributedImService == null)
            {
                throw new ArgumentNullException("distributedImService");
            }

            if (gossipImService == null)
            {
                throw new ArgumentNullException("gossipimService");
            }

            if (randomNumberGenerator == null)
            {
                throw new ArgumentNullException("randomNumberGenerator");
            }

            this.distributedIms = distributedImService;
            this.gossipIms = gossipImService;
            this.randomNumberGenerator = randomNumberGenerator;
        }

        /// <inheritdoc />
        public float MaximumInterestRadius
        {
            get
            {
                return this.distributedIms.MaximumInterestRadius;
            }
        }

        #region IInterestManagementService Members

        /// <inheritdoc />
        public float SubscriptionInflation
        {
            get
            {
                // The DistributedImService and the GossipImService both share the same code
                // for calculating the subscription inflation.  Quick check to make sure they
                // match because if they don't we could have issues with missing entities.
                Debug.Assert(Math.Abs(this.distributedIms.SubscriptionInflation - this.gossipIms.SubscriptionInflation) < 0.001, "Distributed and Gossip inflation amounts must match");
                return this.distributedIms.SubscriptionInflation; 
            }
        }

        /// <inheritdoc />
        public void Optimize(float maximumInterestRadius, float maximumSpeed)
        {
            this.distributedIms.Optimize(maximumInterestRadius, maximumSpeed);
            this.gossipIms.Optimize(maximumInterestRadius, maximumSpeed);
        }

        /// <inheritdoc />
        public void InsertRegion(
            QualifiedName sceneName,
            ImsRegion region,
            uint timeToLiveSeconds,
            EventHandler<ImsEventArgs> enterNotificationHandler,
            EventHandler<ImsStatusEventArgs> statusHandler)
        {
            this.distributedIms.InsertRegion(
                sceneName,
                region,
                timeToLiveSeconds,
                enterNotificationHandler,
                statusHandler);
            this.gossipIms.InsertRegion(
                sceneName,
                region,
                timeToLiveSeconds,
                enterNotificationHandler,
                statusHandler);
        }

        /// <inheritdoc />
        public void ModifyRegion(
            QualifiedName sceneName,
            ImsRegion region,
            uint timeToLiveSeconds,
            byte clientsKnownCollisions)
        {
            double ran = this.randomNumberGenerator.NextDouble(); 

            if (this.gossipIms.EstimatedNumberOfActualRegions == 0
                || ran < (double)Parameters.GossipThreshold / this.gossipIms.EstimatedNumberOfActualRegions)
            {
                this.distributedIms.ModifyRegion(sceneName, region, timeToLiveSeconds, clientsKnownCollisions);
            }

            this.gossipIms.ModifyRegion(sceneName, region, timeToLiveSeconds, clientsKnownCollisions);
        }

        /// <inheritdoc />
        public void RemoveRegion(QualifiedName sceneName, ImsRegion region)
        {
            this.distributedIms.RemoveRegion(sceneName, region);
            this.gossipIms.RemoveRegion(sceneName, region);
        }

        /// <inheritdoc />
        public void RemoveRegionWithoutNotification(QualifiedName sceneName, ImsRegion region)
        {
            this.distributedIms.RemoveRegionWithoutNotification(sceneName, region);
            this.gossipIms.RemoveRegionWithoutNotification(sceneName, region);
        }

        /// <inheritdoc />
        public void DetachSubscription(QualifiedName sceneName, ImsRegion region)
        {
            this.distributedIms.DetachSubscription(sceneName, region);
            this.gossipIms.DetachSubscription(sceneName, region);
        }

        #endregion
    }
}
