﻿//-----------------------------------------------------------------------
// <copyright file="GossipImService.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2010 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.InterestManagement
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using Badumna.Core;
    using Badumna.DataTypes;
    using Badumna.Transport;
    using Badumna.Utilities;

    /// <summary>
    /// Factory for creating IGossipRegion objects.
    /// </summary>
    /// <param name="region">The IMS region.</param>
    /// <param name="secondsToLive">The number of seconds the gossip region should live for.</param>
    /// <param name="timeKeeper">The time keeper for the gossip region.</param>
    /// <returns>A new IGossipRegion.</returns>
    internal delegate GossipImService.IGossipRegion GossipRegionFactory(
        ImsRegion region,
        uint secondsToLive,
        ITime timeKeeper);

    /// <summary>
    /// Factory for creating ISmoother objects.
    /// </summary>
    /// <param name="smothingFactor">The factor by which values should be smoothed.</param>
    /// <param name="initialValue">The initial value.</param>
    /// <returns>A newly created ISmoother object.</returns>
    internal delegate ISmoother SmootherFactory(
        double smothingFactor,
        double initialValue);

    /// <summary>
    /// Interface for gossip IM service.
    /// </summary>
    internal interface IGossipImService : IInterestManagementService
    {
        /// <summary>
        /// Gets the number of actual regions estimated from the number of known
        /// regions and the gossip capacity
        /// </summary>
        double EstimatedNumberOfActualRegions { get; }
    }

    /// <summary>
    /// TODO: documentation.
    /// </summary>
    internal class GossipImService : InterestManagementService, IGossipImService
    {
        /// <summary>
        /// For notifications and queries about network connectivity.
        /// </summary>
        private readonly INetworkConnectivityReporter networkConnectivityReporter;

        /// <summary>
        /// Factory for creating gossip regions.
        /// </summary>
        private readonly GossipRegionFactory gossipRegionFactory;

        /// <summary>
        /// Factory for creating regular tasks.
        /// </summary>
        private readonly SubscriptionDataFactory subscriptionDataFactory;

        /// <summary>
        /// Factory for creating regular tasks.
        /// </summary>
        private readonly RegularTaskFactory regularTaskFactory;

        /// <summary>
        /// Subscriptions mapped by region ID.
        /// </summary>
        private Dictionary<BadumnaId, ISubscriptionData> subscriptions = new Dictionary<BadumnaId, ISubscriptionData>();

        /// <summary>
        /// Gossip regions, mapped by ID.
        /// </summary>
        private Dictionary<BadumnaId, IGossipRegion> allRegions = new Dictionary<BadumnaId, IGossipRegion>();

        /// <summary>
        /// List of remote regions' IDs.
        /// </summary>
        private List<BadumnaId> remoteRegionIds = new List<BadumnaId>();

        /// <summary>
        /// List of local regions' IDs.
        /// </summary>
        private List<BadumnaId> localRegionIds = new List<BadumnaId>();

        /// <summary>
        /// The bandwidth consumption permitted (bytes per second).
        /// </summary>
        private double bandwidthToConsume = Parameters.MaximumGossipBandwidth;

        /// <summary>
        /// TODO: documentation.
        /// </summary>
        private double capacity;

        /// <summary>
        /// TODO: documentation.
        /// </summary>
        private double estimateBase;

        /// <summary>
        /// TODO: documentation.
        /// </summary>
        private double unknownIntroductions;

        /// <summary>
        /// TODO: documentation.
        /// </summary>
        private double totalIntroductions;

        /// <summary>
        /// TODO: documentation.
        /// </summary>
        private IRegularTask estimatorCalculation;

        /// <summary>
        /// TODO: documentation.
        /// </summary>
        private ISmoother numberOfRegionsEstimator;

        /// <summary>
        /// The queue to use for scheduling events.
        /// </summary>
        private INetworkEventScheduler eventQueue;

        /// <summary>
        /// Provides access to the current time.
        /// </summary>
        private ITime timeKeeper;

        /// <summary>
        /// The transport protocol.
        /// </summary>
        private TransportProtocol protocol;

        /// <summary>
        /// Initializes a new instance of the GossipImService class.
        /// </summary>
        /// <param name="protocol">The transport protocol.</param>
        /// <param name="clientToWatch">The IM client.</param>
        /// <param name="eventQueue">The network event queue to schedule events on.</param>
        /// <param name="timeKeeper">The time keeper.</param>
        /// <param name="networkConnectivityReporter">Reporter of network connectivity status and events.</param>
        public GossipImService(
            TransportProtocol protocol,
            IInterestManagementClient clientToWatch,
            INetworkEventScheduler eventQueue,
            ITime timeKeeper,
            INetworkConnectivityReporter networkConnectivityReporter)
            : this(
            protocol,
            clientToWatch,
            eventQueue,
            timeKeeper,
            networkConnectivityReporter,
            (r, t, k) => new GossipRegion(r, t, k),
            (d, p, q, c, t) => new RegularTask(d, p, q, c, t),
            (s, r, t, n, u, k) => new SubscriptionData(s, r, t, n, u, k),
            (s, i) => new PositiveSmoother(s, i))
        {
        }

        /// <summary>
        /// Initializes a new instance of the GossipImService class.
        /// </summary>
        /// <param name="protocol">The transport protocol.</param>
        /// <param name="clientToWatch">The IM client.</param>
        /// <param name="eventQueue">The queue to schedule events on.</param>
        /// <param name="timeKeeper"> The time keeper.</param>
        /// <param name="networkConnectivityReporter">Reporter of network connectivity status and events.</param>
        /// <param name="gossipRegionFactory">A factory for creating gossip regions.</param>
        /// <param name="regularTaskFactory">A factory for creating regular tasks.</param>
        /// <param name="subscriptionDataFactory">A factory for creating subscription data objects.</param>
        /// <param name="smootherFactory">A factory for creating a value smoother.</param>
        public GossipImService(
            TransportProtocol protocol,
            IInterestManagementClient clientToWatch,
            INetworkEventScheduler eventQueue,
            ITime timeKeeper,
            INetworkConnectivityReporter networkConnectivityReporter,
            GossipRegionFactory gossipRegionFactory,
            RegularTaskFactory regularTaskFactory,
            SubscriptionDataFactory subscriptionDataFactory,
            SmootherFactory smootherFactory)
        {
            if (protocol == null)
            {
                throw new ArgumentNullException("parent");
            }

            if (clientToWatch == null)
            {
                throw new ArgumentNullException("clientToWatch");
            }

            if (eventQueue == null)
            {
                throw new ArgumentNullException("eventQueue");
            }

            if (timeKeeper == null)
            {
                throw new ArgumentNullException("timeKeeper");
            }

            if (gossipRegionFactory == null)
            {
                throw new ArgumentNullException("gossipRegionFactory");
            }

            if (networkConnectivityReporter == null)
            {
                throw new ArgumentNullException("networkConnectivityReporter");
            }

            if (regularTaskFactory == null)
            {
                throw new ArgumentNullException("regularTaskFactory");
            }

            if (subscriptionDataFactory == null)
            {
                throw new ArgumentNullException("contextualSubscriptionDataFactory");
            }

            if (smootherFactory == null)
            {
                throw new ArgumentNullException("smootherFactory");
            }

            this.protocol = protocol;
            this.protocol.RegisterMethodsIn(this);

            this.eventQueue = eventQueue;
            this.timeKeeper = timeKeeper;
            this.gossipRegionFactory = gossipRegionFactory;
            this.networkConnectivityReporter = networkConnectivityReporter;
            this.regularTaskFactory = regularTaskFactory;
            this.subscriptionDataFactory = subscriptionDataFactory;

            clientToWatch.ResultNotification += this.InterceptInteresection;
            this.networkConnectivityReporter.NetworkOnlineEvent += this.DoGossip;

            if (this.networkConnectivityReporter.Status == ConnectivityStatus.Online)
            {
                this.DoGossip();
            }

            // TODO: exorcise these magic numbers!

            // Calculate the estimated number of intersections that can be maintained by gossip.
            // 5.0 = overhead in a single packet
            // 30.0 = number of bytes taken to gossip about a single region
            this.capacity = ((Parameters.MaximumGossipBandwidth * Parameters.DefaultSubscriptionTTL.TotalSeconds / 2.0) -
                (5.0 + Parameters.UDPHeaderLength)) / 30.0;

            // Calculate the base for the exponent of error fudge in EstimatedNumberOfActualRegions(get)
            // 2.35 = a constant used to esimate the best base.
            // base = e^(ln(x)/x) where x is a relative to capacity
            this.estimateBase = Math.Exp(Math.Log(this.capacity * 2.35) / (this.capacity * 2.35));

            this.numberOfRegionsEstimator = smootherFactory(5.0, 0.0);
            
            this.estimatorCalculation = this.regularTaskFactory.Invoke(
                "Calculate number of regions",
                TimeSpan.FromSeconds(10.0),
                eventQueue,
                networkConnectivityReporter,
                this.CalculateEstimateOfActualRegions);

            this.estimatorCalculation.Start();
        }

        /// <summary>
        /// Interface for gossip regions.
        /// </summary>
        internal interface IGossipRegion
        {
            /// <summary>
            /// Gets the region.
            /// </summary>
            ImsRegion Region { get; }

            /// <summary>
            /// Gets the number of seconds the region should live for.
            /// </summary>
            uint TimeToLiveSeconds { get; }

            /// <summary>
            /// Gets or sets the expiration time.
            /// </summary>
            TimeSpan ExpirationTime
            {
                get;

                // TODO: does this need to be public?
                set;
            }

            /// <summary>
            /// Update the region.
            /// </summary>
            /// <param name="region">The new region.</param>
            /// <param name="timeToLive">The new number of seconds the region should live for.</param>
            void Update(ImsRegion region, uint timeToLive);
        }

        /// <summary>
        /// Gets the number of actual regions estimated from the number of known
        /// regions and the gossip capacity
        /// </summary>
        public double EstimatedNumberOfActualRegions
        {
            get
            {
                if (this.numberOfRegionsEstimator.EstimatedValue >= 1.0)
                {
                    return double.MaxValue;
                }

                // The estimate works only for smaller numbers of peers. I.e. the error becomes very large when the
                // number of actual objects is greater than the capacity to gossip. For this reason I have added a long
                // view fudge factor. It basically gives an over estimate of the number of peers to ensure that the
                // watch IM does not become overloaded.
                double estimate = (double)this.allRegions.Count /
                    Math.Pow((double)(1.0 - this.numberOfRegionsEstimator.EstimatedValue), 1.25);
                double longView = Math.Pow(this.estimateBase, estimate);

                if (longView < estimate)
                {
                    return estimate;
                }

                if (longView > 2.0 * estimate)
                {
                    return estimate * 2.0;
                }

                return longView;
            }
        }

        /// <inheritdoc />
        public override void ModifyRegion(QualifiedName sceneName, ImsRegion region, uint timeToLiveSeconds, byte clientsKnownCollisions)
        {
            ISubscriptionData data = null;

            if (this.subscriptions.TryGetValue(region.Guid, out data))
            {
                data.Region.Centroid = region.Centroid;
                data.Region.Radius = region.Radius;
                data.ResetExpirationTime(timeToLiveSeconds);
            }
            else
            {
                Logger.TraceError(LogTag.InterestManagement, "Gossip IM ignores modify requests for regions that have not been previously inserted.");
                return;
            }

            IGossipRegion knownRegion = null;

            if (this.allRegions.TryGetValue(region.Guid, out knownRegion))
            {
                knownRegion.Update(region, (ushort)timeToLiveSeconds);
            }
            else
            {
                Logger.TraceWarning(LogTag.InterestManagement, "Had to re-insert subscibed to region back into gossip list.");
                this.allRegions.Add(region.Guid, this.gossipRegionFactory(region, timeToLiveSeconds, this.timeKeeper));
            }
        }

        /// <inheritdoc />
        public override void RemoveRegion(QualifiedName sceneName, ImsRegion region)
        {
            this.RemoveRegion(region);
        }

        /// <inheritdoc />
        public override void RemoveRegionWithoutNotification(QualifiedName sceneName, ImsRegion region)
        {
            this.RemoveRegion(region);
        }

        /// <inheritdoc />
        public override void DetachSubscription(QualifiedName sceneName, ImsRegion region)
        {
            this.RemoveRegionWithoutNotification(sceneName, region);
        }

        /// <summary>
        /// Receive gossip information about a region.
        /// </summary>
        /// <param name="region">The region neing gossiped about.</param>
        /// <param name="timeToLive">The region's time to live.</param>
        [ConnectionfulProtocolMethod(ConnectionfulMethod.GossipAboutRegion)]
        public void GossipAboutRegion(ImsRegion region, uint timeToLive)
        {
            IGossipRegion knownRegion = null;

            this.totalIntroductions++;

            if (this.allRegions.TryGetValue(region.Guid, out knownRegion))
            {
                // Don't update local regions
                if (!this.subscriptions.ContainsKey(region.Guid))
                {
                    knownRegion.Update(region, timeToLive);
                }
            }
            else
            {
                this.InsertGossipRegion(region, timeToLive);
                bool wasPreviouslyUnknown = false;

                // Notify each subscription if the new region is of interest, scheduling the 
                // removal of any expired subscriptions in the process
                foreach (ISubscriptionData subscription in this.subscriptions.Values)
                {
                    if (subscription.EstimatedExpirationTime > this.timeKeeper.Now)
                    {
                        wasPreviouslyUnknown = wasPreviouslyUnknown || subscription.Region.IsIntersecting(region);

                        if (subscription.Region.RequiresIntersectionNotification(region))
                        {
                            subscription.EnterNotification(this, new ImsEventArgs(region));
                        }
                    }
                    else
                    {
                        this.eventQueue.Push(this.ExpireSubscription, subscription);
                    }
                }

                if (wasPreviouslyUnknown)
                {
                    this.unknownIntroductions++;
                }
            }
        }

        /// <inheritdoc />
        protected override void InsertRegionImplementation(
            QualifiedName sceneName,
            ImsRegion region,
            uint timeToLiveSeconds,
            EventHandler<ImsEventArgs> enterNotificationHandler,
            EventHandler<ImsStatusEventArgs> statusHandler)
        {
            this.localRegionIds.Remove(region.Guid);
            this.remoteRegionIds.Remove(region.Guid);

            ISubscriptionData data = this.subscriptionDataFactory(
                sceneName,
                region,
                timeToLiveSeconds,
                enterNotificationHandler,
                statusHandler,
                this.timeKeeper);
            this.subscriptions[region.Guid] = data;
            this.allRegions[region.Guid] = this.gossipRegionFactory(region, timeToLiveSeconds, this.timeKeeper);
            this.localRegionIds.Add(region.Guid);
        }

        /// <summary>
        /// Select a gossip region randomly.
        /// </summary>
        /// <param name="idList">List of region IDs.</param>
        /// <param name="destinationRegion">Optional destination region that the selected region must intersect.
        /// If null, any region can be selected.</param>
        /// <returns>The selected gossip region, or null if there is none.</returns>
        private IGossipRegion SelectRegionAtRandom(ref List<BadumnaId> idList, ImsRegion destinationRegion)
        {
            if (idList.Count > 0)
            {
                int i = RandomSource.Generator.Next(idList.Count);

                IGossipRegion region = null;

                for (int n = 0; n < idList.Count; n++)
                {
                    int index = (i + n) % idList.Count;

                    if (!this.allRegions.TryGetValue(idList[index], out region))
                    {
                        // TODO: defer removal as it looks like this will skip the subsequent list entry.
                        idList.RemoveAt(index);
                        continue;
                    }

                    if (destinationRegion == null || region.Region.IsIntersecting(destinationRegion))
                    {
                        return region;
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Remove regions whose time to live has expired.
        /// </summary>
        private void RemoveOldRegions()
        {
            List<BadumnaId> oldRegions = new List<BadumnaId>();

            foreach (IGossipRegion region in this.allRegions.Values)
            {
                if (region.TimeToLiveSeconds <= 0)
                {
                    oldRegions.Add(region.Region.Guid);
                }
            }

            foreach (BadumnaId regionId in oldRegions)
            {
                this.allRegions.Remove(regionId);
            }
        }

        /// <summary>
        /// Determine whether a region is a candidate for gossip (i.e. live, non-zero, different but intersecting).
        /// </summary>
        /// <param name="regionToGossipAbout">The region to consider.</param>
        /// <param name="regionToGossipWith">The region to gossip with.</param>
        /// <returns>True if the region is a candidate, otherwise false.</returns>
        private bool IsCandidateForGossip(IGossipRegion regionToGossipAbout, ImsRegion regionToGossipWith)
        {
            return regionToGossipAbout != null
                && regionToGossipAbout.TimeToLiveSeconds > 0
                && regionToGossipAbout.Region.Radius > 0f
                && !regionToGossipWith.Guid.Address.Equals(regionToGossipAbout.Region.Guid.Address)
                && regionToGossipWith.IsIntersecting(regionToGossipAbout.Region);
        }

        /// <summary>
        /// Pick gossip regions and send gossip messages.
        /// </summary>
        private void DoGossip()
        {
            if (this.networkConnectivityReporter.Status == ConnectivityStatus.Online)
            {
                this.RemoveOldRegions();

                // TODO: exorcise magic number.

                // Default delay - to prevent flooding event queue if an exception is thrown below.
                int delayMilliseconds = 10000;

                // always do gossip. 
                if (this.allRegions.Count > 0)
                {
                    try
                    {
                        IGossipRegion regionToGossipWith = this.SelectRegionAtRandom(ref this.remoteRegionIds, null);
                        List<IGossipRegion> regionsToGossipAbout = new List<IGossipRegion>();

                        if (regionToGossipWith != null)
                        {
                            // Always gossip about a local region.
                            int j = Badumna.Utilities.RandomSource.Generator.Next(this.localRegionIds.Count);

                            // TODO: why is there a maximum number of local regions to gossip about?
                            // TODO: exoricise magic number.
                            for (int n = 0; n < this.localRegionIds.Count && regionsToGossipAbout.Count < 2; n++)
                            {
                                IGossipRegion localRegion = null;

                                int index = (j + n) % this.localRegionIds.Count;

                                if (!this.allRegions.TryGetValue(this.localRegionIds[index], out localRegion))
                                {
                                    this.localRegionIds.RemoveAt(index);
                                    continue;
                                }

                                if (localRegion.Region.IsIntersecting(regionToGossipWith.Region))
                                {
                                    regionsToGossipAbout.Add(localRegion);
                                }
                            }

                            for (int i = 0; i < 5; i++)
                            {
                                IGossipRegion regionToGossipAbout = this.SelectRegionAtRandom(
                                    ref this.remoteRegionIds,
                                    regionToGossipWith.Region);

                                // TODO: why are all checks not included in candidature check?
                                if (this.IsCandidateForGossip(regionToGossipAbout, regionToGossipWith.Region)
                                    && !regionsToGossipAbout.Contains(regionToGossipAbout))
                                {
                                    regionsToGossipAbout.Add(regionToGossipAbout);
                                }
                            }

                            // Send the list of gossip regions and alter the delay until next event to match bandwidth
                            // usage.
                            if (regionsToGossipAbout.Count > 0)
                            {
                                TransportEnvelope envelope = this.protocol.GetMessageFor(
                                    regionToGossipWith.Region.Guid.Address,
                                    QualityOfService.Unreliable);

                                foreach (IGossipRegion gossipRegion in regionsToGossipAbout)
                                {
                                    this.protocol.RemoteCall(
                                        envelope,
                                        this.GossipAboutRegion,
                                        gossipRegion.Region,
                                        gossipRegion.TimeToLiveSeconds);
                                }

                                this.protocol.SendMessage(envelope);
                                delayMilliseconds = (int)TimeSpan.FromSeconds(envelope.Length / this.bandwidthToConsume).TotalMilliseconds;
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        Logger.TraceException(LogLevel.Error, e, "Failed to perform gossip");
                    }
                }

                this.eventQueue.Schedule(delayMilliseconds, this.DoGossip);
            }
        }

        /// <summary>
        /// Seed the list of gossip-able regions, upon receipt of an intersection event.
        /// </summary>
        /// <remarks>
        /// This method is used to seed the list of gossip-able regions. At this point only the region's
        /// id is available so we give the region a default (and invalid) value. This is checked in the 
        /// DoGossip method. It means that we can gossip to the peer responsible for the region but not 
        /// about the region.
        /// </remarks>
        /// <param name="sender">The intersection event sender.</param>
        /// <param name="e">The intersection event arguments.</param>
        private void InterceptInteresection(object sender, ImsStatusEventArgs e)
        {
            foreach (ImsEventArgs arg in e.EnterList)
            {
                ImsRegion defaultRegion = new ImsRegion(new Vector3(0, 0, 0), 0f, arg.OtherRegionId, arg.OtherRegionType, new QualifiedName("", "Default"));
                this.InsertGossipRegion(defaultRegion, (ushort)Parameters.DefaultSubscriptionTTL.TotalSeconds);
            }
        }

        /// <summary>
        /// Insert a gossip region into the collections of gossip regions.
        /// </summary>
        /// <param name="region">The IMS region to insert.</param>
        /// <param name="timeToLiveSeconds">The region's time to live.</param>
        private void InsertGossipRegion(ImsRegion region, uint timeToLiveSeconds)
        {
            if (this.subscriptions.ContainsKey(region.Guid))
            {
                // Already know about it; probably an object registered with a DHT address based BadumnaId on the local peer.
                return;
            }

            if (this.allRegions.ContainsKey(region.Guid))
            {
                this.allRegions.Remove(region.Guid);
                this.remoteRegionIds.Remove(region.Guid);
            }

            this.allRegions.Add(region.Guid, this.gossipRegionFactory(region, timeToLiveSeconds, this.timeKeeper));
            this.remoteRegionIds.Add(region.Guid);
        }

        /// <inheritdoc />
        private void CalculateEstimateOfActualRegions()
        {
            double numberOfKnownRegions = this.allRegions.Count;

            // TODO: Don't test equality on double. Counts should be integral values.
            // Cast to double for ratio below if required.
            if (numberOfKnownRegions == 0.0)
            {
                this.numberOfRegionsEstimator.ValueEvent(0);
            }
            else if (this.totalIntroductions != 0.0)
            {
                double ratio = this.unknownIntroductions / this.totalIntroductions;

                this.numberOfRegionsEstimator.ValueEvent(ratio);
            }

            //// NetworkContext.CurrentContext.RecordStatistic("Gossip", "IntroRatio", (1.0 / Math.Pow((double)(1.0 - this.mNumberOfRegionsEstimator.EstimatedValue), 1.25)) / 3.0); //this.mNumberOfRegionsEstimator.EstimatedValue / 3.0);
            //// NetworkContext.CurrentContext.RecordStatistic("Gossip", "KnownIntersections", this.EstimatedNumberOfActualRegions / 3.0);

            this.unknownIntroductions = this.totalIntroductions = 0;
        }

        /// <inheritdoc />
        private void RemoveRegion(ImsRegion region)
        {
            if (this.subscriptions.ContainsKey(region.Guid))
            {
                this.subscriptions.Remove(region.Guid);
            }

            if (this.allRegions.ContainsKey(region.Guid))
            {
                this.allRegions.Remove(region.Guid);
            }

            if (this.remoteRegionIds.Contains(region.Guid))
            {
                this.remoteRegionIds.Remove(region.Guid);
            }
        }

        /// <summary>
        /// Remove expired subscriptions.
        /// </summary>
        /// <param name="subscription">The subscription that has expired.</param>
        private void ExpireSubscription(ISubscriptionData subscription)
        {
            if (subscription.EstimatedExpirationTime < this.timeKeeper.Now)
            {
                Logger.TraceInformation(LogTag.InterestManagement, "Subscription {0} has expired in Gossip IMS", subscription.Region.Guid);
                this.subscriptions.Remove(subscription.Region.Guid);
            }
        }

        /// <summary>
        /// TODO: documentation.
        /// </summary>
        private class GossipRegion : IGossipRegion, IExpireable
        {
            /// <summary>
            /// The region.
            /// </summary>
            private ImsRegion region;

            /// <summary>
            /// Provides access to the current time.
            /// </summary>
            private ITime timeKeeper;

            /// <summary>
            /// Initializes a new instance of the GossipImService.GossipRegion class.
            /// </summary>
            /// <param name="region">The region.</param>
            /// <param name="timeToLive">The number of seconds the region should live for.</param>
            /// <param name="timeKeeper">Provides access to the current time.</param>
            public GossipRegion(ImsRegion region, uint timeToLive, ITime timeKeeper)
            {
                this.timeKeeper = timeKeeper;
                this.region = new ImsRegion(region);
                this.ExpirationTime = this.timeKeeper.Now + TimeSpan.FromSeconds((double)timeToLive);
            }

            /// <summary>
            /// Gets the region.
            /// </summary>
            public ImsRegion Region
            {
                get { return this.region; }
            }

            /// <summary>
            /// Gets the number of seconds the region should live for.
            /// </summary>
            public uint TimeToLiveSeconds
            {
                get { return (uint)Math.Max(0.0, (this.ExpirationTime - this.timeKeeper.Now).TotalSeconds); }
            }

            /// <summary>
            /// Gets or sets the expiration time.
            /// </summary>
            public TimeSpan ExpirationTime
            {
                get;

                // TODO: does this need to be public?
                set;
            }

            /// <summary>
            /// Update the region.
            /// </summary>
            /// <param name="region">The new region.</param>
            /// <param name="timeToLive">The new number of seconds the region should live for.</param>
            public void Update(ImsRegion region, uint timeToLive)
            {
                if (region.Guid.Equals(this.region.Guid))
                {
                    this.region.Centroid = region.Centroid;
                    this.region.Radius = region.Radius;
                    this.ExpirationTime = this.timeKeeper.Now + TimeSpan.FromSeconds((double)timeToLive);
                }
            }
        }
    }
}
