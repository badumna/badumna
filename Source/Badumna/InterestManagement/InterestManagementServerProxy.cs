//-----------------------------------------------------------------------
// <copyright file="InterestManagementServerProxy.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2010 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.InterestManagement
{
    using Badumna.Core;
    using Badumna.Utilities;

    /// <summary>
    /// Proxy for interest management server for use by the client (proxy pattern).
    /// </summary>
    internal abstract class InterestManagementServerProxy : IInterestManagementServerProxy
    {
        /// <summary>
        /// Message parser for creating RPC messages.
        /// </summary>
        private MessageParser messageParser;

        /// <summary>
        /// TODO: documentation.
        /// </summary>
        private InterestManagementServerProtocol serverProtocol;

        /// <summary>
        /// Initializes a new instance of the InterestManagementServerProxy class.
        /// </summary>
        /// <param name="parser">The message parser to use.</param>
        /// <param name="serverProtocol">TODO: documentation.</param>
        public InterestManagementServerProxy(MessageParser parser, InterestManagementServerProtocol serverProtocol)
        {
            this.messageParser = parser;
            this.serverProtocol = serverProtocol;
        }

        /// <summary>
        /// Request that a region be inserted.
        /// </summary>
        /// <param name="sceneName">The name of the scene the region belongs to.</param>
        /// <param name="region">The region to insert.</param>
        /// <param name="timeToLiveSeconds">The time the region should live for.</param>
        public void InsertRequest(QualifiedName sceneName, ImsRegion region, uint timeToLiveSeconds)
        {
            Logger.TraceInformation(LogTag.InterestManagement, "Insert request for region {0} in scene {1}", region.Guid, sceneName);
            IEnvelope envelope = this.PrepareRequestMessage();
            envelope.Qos.Priority = QosPriority.High;
            this.messageParser.RemoteCall(envelope, this.serverProtocol.InsertRequest, sceneName, region, timeToLiveSeconds);
            this.DispatchMessage(envelope);
        }

        /// <summary>
        /// Request that a region be removed.
        /// </summary>
        /// <param name="sceneName">The name of the scene the region belongs to.</param>
        /// <param name="region">The region to remove.</param>
        public void RemoveRequest(QualifiedName sceneName, ImsRegion region)
        {
            IEnvelope envelope = this.PrepareRequestMessage();
            envelope.Qos.Priority = QosPriority.High;
            this.messageParser.RemoteCall(envelope, this.serverProtocol.RemoveRequest, sceneName, region);
            this.DispatchMessage(envelope);
        }

        /// <summary>
        /// Request that a region be removed without sending notifications.
        /// </summary>
        /// <param name="sceneName">The name of the scene the region belongs to.</param>
        /// <param name="region">The region to remove.</param>
        public void RemoveWithoutNotificationRequest(QualifiedName sceneName, ImsRegion region)
        {
            IEnvelope envelope = this.PrepareRequestMessage();
            envelope.Qos.Priority = QosPriority.High;
            this.messageParser.RemoteCall(envelope, this.serverProtocol.RemoveWithoutNotificationRequest, sceneName, region);
            this.DispatchMessage(envelope);
        }

        /// <summary>
        /// Request that a region be modified.
        /// </summary>
        /// <param name="sceneName">The name of the scene the region belongs to.</param>
        /// <param name="region">The region to modify.</param>
        /// <param name="timeToLiveSeconds">The time the region should live for.</param>
        /// <param name="clientsKnownCollisions">The number of known collisions the region has.</param>
        public void ModifyRequest(QualifiedName sceneName, ImsRegion region, uint timeToLiveSeconds, byte clientsKnownCollisions)
        {
            IEnvelope envelope = this.PrepareRequestMessage();
            envelope.Qos.Priority = QosPriority.High;
            this.messageParser.RemoteCall(envelope, this.serverProtocol.ModifyRequest, sceneName, region, timeToLiveSeconds, clientsKnownCollisions);
            this.DispatchMessage(envelope);
        }

        /// <summary>
        /// Create an envelope for sending a request message.
        /// </summary>
        /// <returns>An envelope for the message.</returns>
        protected abstract IEnvelope PrepareRequestMessage();

        /// <summary>
        /// Dispatch a meesage to the interest management server.
        /// </summary>
        /// <param name="envelope">Te envelope containing the message.</param>
        protected abstract void DispatchMessage(IEnvelope envelope);
    }
}
