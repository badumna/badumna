//-----------------------------------------------------------------------
// <copyright file="IInterestManagementService.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2010 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.InterestManagement
{
    using System;
    using Badumna.DataTypes;
    using Badumna.Utilities;

    /// <summary>
    /// TODO: documentation.
    /// </summary>
    internal interface IInterestManagementService
    {
        /// <summary>
        /// Gets the maximum interest radius passed in calls to <see cref="Optimize"/>.
        /// </summary>
        float MaximumInterestRadius { get; }

        /// <summary>
        /// Gets the number of units added to a subscription radius when the subscription is
        /// inserted / modified.
        /// </summary>
        /// <remarks>
        /// The subscription radius is increased by this amount to improve perfomance.  If the position
        /// of the subscription changes but the subscription still falls within the previous inflated
        /// zone then no update need be made.
        /// </remarks>
        float SubscriptionInflation { get; }

        /// <summary>
        /// Optimized the interest management system based on the given values.
        /// </summary>
        /// <remarks>
        /// This method can be called multiple times (e.g. once per entity type).
        /// The interest management system will optimize itself according to the maximum values
        /// passed to this method.
        /// </remarks>
        /// <param name="maximumInterestRadius">The maximum interest radius of an entity</param>
        /// <param name="maximumSpeed">The maximum speed of an entity</param>
        void Optimize(float maximumInterestRadius, float maximumSpeed);

        /// <summary>
        /// Insert a region.
        /// </summary>
        /// <param name="sceneName">The scene the region belongs to.</param>
        /// <param name="region">The region to insert.</param>
        /// <param name="timeToLiveSeconds">The time the region should persist for.</param>
        /// <param name="enterNotificationHandler">Callback for handling enter notifications.</param>
        /// <param name="statusHandler">Call back for handling... to do.</param>
        void InsertRegion(
            QualifiedName sceneName,
            ImsRegion region,
            uint timeToLiveSeconds,
            EventHandler<ImsEventArgs> enterNotificationHandler, 
            EventHandler<ImsStatusEventArgs> statusHandler);
        
        /// <summary>
        /// Modify a region.
        /// </summary>
        /// <param name="sceneName">The scene the region belongs to.</param>
        /// <param name="region">The region to modify.</param>
        /// <param name="timeToLiveSeconds">The time the region should persist for.</param>
        /// <param name="clientsKnownCollisions">TODO: clientsKnownCollisions documentation.</param>
        void ModifyRegion(QualifiedName sceneName, ImsRegion region, uint timeToLiveSeconds, byte clientsKnownCollisions);
        
        /// <summary>
        /// Remove a region.
        /// </summary>
        /// <remarks>Notifies... to do.</remarks>
        /// <param name="sceneName">The name of the scene the region belongs to.</param>
        /// <param name="region">The region to remove.</param>
        void RemoveRegion(QualifiedName sceneName, ImsRegion region);
        
        /// <summary>
        /// Remove a region without... to do.
        /// </summary>
        /// <param name="sceneName">The name of the scene the region belongs to.</param>
        /// <param name="region">The region to remove.</param>
        void RemoveRegionWithoutNotification(QualifiedName sceneName, ImsRegion region);

        /// <summary>
        /// TODO: documentation.
        /// </summary>
        /// <param name="sceneName">The name of the scene.</param>
        /// <param name="region">The region.</param>
        void DetachSubscription(QualifiedName sceneName, ImsRegion region);
    }
}
