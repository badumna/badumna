//-----------------------------------------------------------------------
// <copyright file="FlatObjectStore.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2010 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace Badumna.InterestManagement
{
    using System;
    using System.Collections.Generic;
    using Badumna.Core;
    using Badumna.DataTypes;
    using Badumna.Utilities;

    /// <summary>
    /// Interface for flat object stores.
    /// </summary>
    internal interface IFlatObjectStore : ISpatialObjectStore
    {
        /// <summary>
        /// Triggered when an object in the store has expired.
        /// </summary>
        event EventHandler<ExpireEventArgs> ExpirationNotification;
    }

    /// <summary>
    /// Holds modifiable IMS regions and manages notifications when regions' intersections change.
    /// </summary>
    internal class FlatObjectStore : IFlatObjectStore
    {
        /// <summary>
        /// TODO: why is this constant defined here?
        /// </summary>
        public const int GarbageCollectionTimeoutSeconds = 5;

        /// <summary>
        /// Delegate for intersection notification events.
        /// </summary>
        private EventHandler<IntersectionEventArgs> intersectionNotificationDelegate;

        /// <summary>
        /// Delegate for separation notification events.
        /// </summary>
        private EventHandler<IntersectionEventArgs> seperationNotificationDelegate;

        /// <summary>
        /// Delegate for expiration notification events.
        /// </summary>
        private EventHandler<ExpireEventArgs> expirationNotificationDelegate;

        /// <summary>
        /// Maps object IDs to stored objects.
        /// </summary>
        private Dictionary<BadumnaId, ImsRegion> spatialObjects = new Dictionary<BadumnaId, ImsRegion>();
        
        /// <summary>
        /// Maps objects to the sets of objects they intersect with.
        /// </summary>
        private Dictionary<BadumnaId, List<BadumnaId>> collisions = new Dictionary<BadumnaId, List<BadumnaId>>();
        
        /// <summary>
        /// Maps objects to their scheduled expiration events.
        /// </summary>
        private Dictionary<BadumnaId, NetworkEvent> expirationEvents = new Dictionary<BadumnaId, NetworkEvent>(); // Not very efficient - but this class is only used for testing anyway.

        /// <summary>
        /// Indicates whether the store has been shutdown.
        /// </summary>
        private bool hasShutdown;

        /// <summary>
        /// The queue to use for scheduling events.
        /// </summary>
        private INetworkEventScheduler eventQueue;

        /// <summary>
        /// Initializes a new instance of the FlatObjectStore class.
        /// </summary>
        /// <param name="eventQueue">The queue to use for scheduling events.</param>
        public FlatObjectStore(INetworkEventScheduler eventQueue)
        {
            this.eventQueue = eventQueue;
        }

        /// <summary>
        /// Triggered when a region has intersected one or more other regions.
        /// </summary>
        public event EventHandler<IntersectionEventArgs> IntersectionNotification
        {
            add { this.intersectionNotificationDelegate += value; }
            remove { this.intersectionNotificationDelegate -= value; }
        }

        /// <summary>
        /// Triggered when a region has separated from one or more other regions. 
        /// </summary>
        public event EventHandler<IntersectionEventArgs> SeperationNotification
        {
            add { this.seperationNotificationDelegate += value; }
            remove { this.seperationNotificationDelegate -= value; }
        }

        /// <summary>
        /// Triggered when an object in the store has expired.
        /// </summary>
        public event EventHandler<ExpireEventArgs> ExpirationNotification
        {
            add { this.expirationNotificationDelegate += value; }
            remove { this.expirationNotificationDelegate -= value; }
        }

        /// <summary>
        /// Gets a value indicating whether the store is empty.
        /// </summary>
        public bool IsEmpty
        {
            get { return 0 == this.spatialObjects.Count; }
        }

        /// <summary>
        /// Gets a count of the number of objectgs in the store.
        /// </summary>
        public int ObjectCount
        {
            get { return this.spatialObjects.Count; }
        }

        /// <summary>
        /// Gets a value indicating whether the store has been shutdown.
        /// </summary>
        public bool HasShutdown
        {
            get { return this.hasShutdown; }
        }

        /////// <summary>
        /////// Gets or sets the dictionary of objects.
        /////// </summary>
        ////protected Dictionary<BadumnaId, ImsRegion> SpatialObjects
        ////{
        ////    get { return this.spatialObjects; }
        ////    set { this.spatialObjects = value; }
        ////}

        /////// <summary>
        /////// Gets or sets the dictionary of collisions between objects.
        /////// </summary>
        ////protected Dictionary<BadumnaId, List<BadumnaId>> Collisions
        ////{
        ////    get { return this.collisions; }
        ////    set { this.collisions = value; }
        ////}

        /// <summary>
        /// Add a region to the store or update an existing one with max TTL.
        /// </summary>
        /// <param name="region">The region to add.</param>
        public void AddOrModifyObject(ImsRegion region)
        {
            this.AddOrModifyObject(region, uint.MaxValue, byte.MaxValue);
        }

        /// <summary>
        /// Add a region to the store, or update its TTL and intersections if it exists.
        /// </summary>
        /// <remarks>
        /// The client known collision count can be used to force re-notifications of all existing collisions
        /// if there are more existing collisions than were known.
        /// </remarks>
        /// <param name="region">The region to add or update.</param>
        /// <param name="timeToLiveSeconds">The number of seconds it should stay alive for.</param>
        /// <param name="clientsKnownCollisions">The number of currently known collisions. Pass byte.MaxValue to
        /// suppress re-notification of all exisitng collisions when there are more collisions than previsouly
        /// known.</param>
        public virtual void AddOrModifyObject(ImsRegion region, uint timeToLiveSeconds, byte clientsKnownCollisions)
        {
            if (this.hasShutdown)
            {
                throw new InvalidOperationException("FlatObjectStore has been shutdown");
            }

            if (region.Radius <= 0)
            {
                throw new ImsInvalidRegionException();
            }

            this.SetRegion(region, timeToLiveSeconds);

            if (clientsKnownCollisions < byte.MaxValue)
            {
                List<BadumnaId> currentCollision = null;

                // Check to see if the client sees less collisions that we expect it to.
                if (this.collisions.TryGetValue(region.Guid, out currentCollision))
                {
                    if (currentCollision.Count > clientsKnownCollisions)
                    {
                        // Clear the list of stored intersections - this forces all notification for 
                        // all intersections regardless of whether we think we have sent notifications in the past.
                        currentCollision.Clear();
                    }
                }
            }

            List<ImsRegion> intersections = new List<ImsRegion>();
            List<ImsRegion> separations = new List<ImsRegion>();

            foreach (KeyValuePair<BadumnaId, ImsRegion> pair in this.spatialObjects)
            {
                if (pair.Value.Guid != region.Guid)
                {
                    if (pair.Value.IsIntersecting(region))
                    {
                        if (!this.collisions.ContainsKey(pair.Key))
                        {
                            this.collisions[pair.Key] = new List<BadumnaId>();
                        }

                        if (!this.collisions.ContainsKey(region.Guid))
                        {
                            this.collisions[region.Guid] = new List<BadumnaId>();
                        }

                        if (!this.collisions[pair.Key].Contains(region.Guid))
                        {
                            this.collisions[pair.Key].Add(region.Guid);
                            this.TriggerIntersectionNotification(pair.Value, region);
                        }

                        if (!this.collisions[region.Guid].Contains(pair.Key))
                        {
                            this.collisions[region.Guid].Add(pair.Key);
                            intersections.Add(pair.Value);
                        }
                    }
                    else
                    {
                        if (this.collisions.ContainsKey(pair.Key) && this.collisions[pair.Key].Contains(region.Guid))
                        {
                            this.collisions[pair.Key].Remove(region.Guid);
                            this.TriggerSeparationNotification(pair.Value, region);
                        }

                        if (this.collisions.ContainsKey(region.Guid) && this.collisions[region.Guid].Contains(pair.Key))
                        {
                            this.collisions[region.Guid].Remove(pair.Key);
                            separations.Add(pair.Value);
                        }
                    }
                }
            }

            if (intersections.Count > 0)
            {
                this.TriggerIntersectionNotification(region, intersections);
            }

            if (separations.Count > 0)
            {
                this.TriggerSeparationNotification(region, separations);
            }
        }

        /// <summary>
        /// TODO: documentation.
        /// </summary>
        /// <remarks>TODO: Is this used? If so, where are existing expiration events removed?</remarks>
        /// <param name="regionId">The region to keep alive.</param>
        /// <param name="timeToLiveSeconds">The number of seconds to keep it alive for.</param>
        public void KeepAlive(BadumnaId regionId, uint timeToLiveSeconds)
        {
            if (this.hasShutdown)
            {
                throw new InvalidOperationException("FlatObjectStore has been shutdown");
            }

            if (this.spatialObjects.ContainsKey(regionId))
            {
                this.expirationEvents[regionId] = this.eventQueue.Schedule(
                    timeToLiveSeconds * 1000.0,
                    this.ExpireHandler,
                    this.spatialObjects[regionId]);
            }
        }

        /// <summary>
        /// Remove a region from the store.
        /// </summary>
        /// <remarks>Notifications of any resulting separations will be issued.
        /// TODO: refactor with RemoveWithoutNotification.</remarks>
        /// <param name="obj">the region to remove.</param>
        public void RemoveObject(ImsRegion obj)
        {
            if (this.hasShutdown)
            {
                throw new InvalidOperationException("FlatObjectStore has been shutdown");
            }

            if (this.spatialObjects.ContainsKey(obj.Guid))
            {
                this.RemoveRegion(obj.Guid);

                if (this.collisions.ContainsKey(obj.Guid))
                {
                    foreach (BadumnaId intersectingObjectKey in this.collisions[obj.Guid])
                    {
                        if (this.spatialObjects.ContainsKey(intersectingObjectKey))
                        {
                            this.TriggerSeparationNotification(obj, this.spatialObjects[intersectingObjectKey]);
                            this.TriggerSeparationNotification(this.spatialObjects[intersectingObjectKey], obj);

                            if (this.collisions[intersectingObjectKey].Contains(obj.Guid))
                            {
                                this.collisions[intersectingObjectKey].Remove(obj.Guid);
                            }
                        }
                    }

                    this.collisions.Remove(obj.Guid);
                }
            }
        }

        /// <summary>
        /// Remove a region from the store without notifying intersecting regions of the separation.
        /// </summary>
        /// <remarks>TODO: refactor with RemoveObject.</remarks>
        /// <param name="obj">The region to remove.</param>
        public void RemoveWithoutNotification(ImsRegion obj)
        {
            if (this.hasShutdown)
            {
                throw new InvalidOperationException("FlatObjectStore has been shutdown");
            }

            if (this.spatialObjects.ContainsKey(obj.Guid))
            {
                this.RemoveRegion(obj.Guid);

                if (this.collisions.ContainsKey(obj.Guid))
                {
                    foreach (BadumnaId intersectingObjectKey in this.collisions[obj.Guid])
                    {
                        if (this.spatialObjects.ContainsKey(intersectingObjectKey))
                        {
                            if (this.collisions[intersectingObjectKey].Contains(obj.Guid))
                            {
                                this.collisions[intersectingObjectKey].Remove(obj.Guid);
                            }
                        }
                    }

                    this.collisions.Remove(obj.Guid);
                }
            }
        }

        /// <summary>
        /// Remove all regions from the store.
        /// </summary>
        public void RemoveAllObjects()
        {
            if (this.hasShutdown)
            {
                throw new InvalidOperationException("FlatObjectStore has been shutdown");
            }

            this.RemoveAllRegions();
            this.collisions.Clear();
        }

        /// <summary>
        /// Gets a list of all the regions that intersect with a given region.
        /// </summary>
        /// <param name="region">The region to look at.</param>
        /// <returns>The regions that intersect with the given region.</returns>
        public IList<ImsRegion> GetObjectsWithinRegion(ImsRegion region)
        {
            if (this.hasShutdown)
            {
                throw new InvalidOperationException("FlatObjectStore has been shutdown");
            }

            IList<ImsRegion> replyList = new List<ImsRegion>();

            if (region.Radius <= 0)
            {
                throw new ImsInvalidRegionException();
            }

            foreach (KeyValuePair<BadumnaId, ImsRegion> pair in this.spatialObjects)
            {
                if (pair.Value.IsIntersecting(region))
                {
                    replyList.Add(pair.Value);
                }
            }

            return replyList;
        }

        /// <summary>
        /// TODO: documentation.
        /// </summary>
        public virtual void Shutdown()
        {
            if (!this.hasShutdown)
            {
                this.hasShutdown = true;
            }
        }

        /// <summary>
        /// Notify interested parties of an intersection.
        /// </summary>
        /// <param name="region">A given region.</param>
        /// <param name="intersectingRegion">The region that now intersects with it.</param>
        protected void TriggerIntersectionNotification(ImsRegion region, ImsRegion intersectingRegion)
        {
            this.TriggerIntersectionNotification(region, new List<ImsRegion>(new ImsRegion[] { intersectingRegion }));
        }

        /// <summary>
        /// Notify interested parties of an intersection.
        /// </summary>
        /// <param name="region">A given region.</param>
        /// <param name="intersectingRegions">The regions that now intersect with it.</param>
        protected virtual void TriggerIntersectionNotification(ImsRegion region, List<ImsRegion> intersectingRegions)
        {
            EventHandler<IntersectionEventArgs> handler = this.intersectionNotificationDelegate;
            if (handler != null)
            {
                handler.Invoke(this, new IntersectionEventArgs(region, intersectingRegions));
            }
        }

        /// <summary>
        /// Notify interested parties of a separation.
        /// </summary>
        /// <param name="region">The region.</param>
        /// <param name="separatingRegion">The region that has separated from it.</param>
        protected void TriggerSeparationNotification(ImsRegion region, ImsRegion separatingRegion)
        {
            this.TriggerSeparationNotification(region, new List<ImsRegion>(new ImsRegion[] { separatingRegion }));
        }

        /// <summary>
        /// Notify interest parties of a separation.
        /// </summary>
        /// <param name="region">The region.</param>
        /// <param name="separatingRegions">The regions that have separated from it.</param>
        protected virtual void TriggerSeparationNotification(ImsRegion region, List<ImsRegion> separatingRegions)
        {
            EventHandler<IntersectionEventArgs> handler = this.seperationNotificationDelegate;
            if (handler != null)
            {
                handler.Invoke(this, new IntersectionEventArgs(region, separatingRegions));
            }
        }

        /// <summary>
        /// Remove all regions.
        /// </summary>
        protected virtual void RemoveAllRegions()
        {
            this.spatialObjects.Clear();
        }

        /// <summary>
        /// Remove a region.
        /// </summary>
        /// <param name="id">The ID of the region to remove.</param>
        protected virtual void RemoveRegion(BadumnaId id)
        {
            if (this.spatialObjects.ContainsKey(id))
            {
                this.spatialObjects.Remove(id);
            }

            NetworkEvent expirationEvent = null;
            if (this.expirationEvents.TryGetValue(id, out expirationEvent))
            {
                this.eventQueue.Remove(expirationEvent);
                this.expirationEvents.Remove(id);
            }
        }

        /// <summary>
        /// Store a region for a given period.
        /// </summary>
        /// <param name="region">The object to store.</param>
        /// <param name="timeToLiveSeconds">The amount of time the object should persist for.</param>
        protected virtual void SetRegion(ImsRegion region, uint timeToLiveSeconds)
        {
            NetworkEvent expirationEvent = null;
            if (this.expirationEvents.TryGetValue(region.Guid, out expirationEvent))
            {
                this.eventQueue.Remove(expirationEvent);
                this.expirationEvents.Remove(region.Guid);
            }

            if (timeToLiveSeconds > 0)
            {
                this.expirationEvents[region.Guid] = this.eventQueue.Schedule(
                    timeToLiveSeconds * 1000.0,
                    this.ExpireHandler,
                    region);
            }

            this.spatialObjects[region.Guid] = region;
        }

        /// <summary>
        /// Handle region expirations.
        /// </summary>
        /// <param name="expirable">The region that has expired.</param>
        private void ExpireHandler(ImsRegion expirable)
        {
            if (null != expirable)
            {
                this.RemoveObject(expirable);
                EventHandler<ExpireEventArgs> handler = this.expirationNotificationDelegate;
                if (handler != null)
                {
                    handler.Invoke(this, new ExpireEventArgs(expirable));
                }
            }
        }
    }
}
