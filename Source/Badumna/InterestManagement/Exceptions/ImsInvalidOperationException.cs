//-----------------------------------------------------------------------
// <copyright file="ImsInvalidOperationException.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2010 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.InterestManagement
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    using Badumna.Core;

    /// <summary>
    /// Exception indicating an invalid operation.
    /// </summary>
    [Serializable]
    internal class ImsInvalidOperationException : ImsException
    {
        /// <summary>
        /// Initializes a new instance of the ImsInvalidOperationException class.
        /// </summary>
        /// <param name="message">A message describing why the exception was thrown.</param>
        public ImsInvalidOperationException(string message)
            : base(message)
        {
        }
    }
}
