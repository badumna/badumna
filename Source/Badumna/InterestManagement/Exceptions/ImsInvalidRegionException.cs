//-----------------------------------------------------------------------
// <copyright file="ImsInvalidRegionException.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2010 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.InterestManagement
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    using Badumna.Core;

    /// <summary>
    /// Exception indicating an invalid region.
    /// </summary>
    [Serializable]
    internal class ImsInvalidRegionException : ImsException
    {
        /// <summary>
        /// Initializes a new instance of the ImsInvalidRegionException class.
        /// </summary>
        public ImsInvalidRegionException()
            : base(Resources.ImsInvalidSpatialRegionException_Message)
        {
        }
    }
}
