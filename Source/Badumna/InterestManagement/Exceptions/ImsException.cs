//-----------------------------------------------------------------------
// <copyright file="ImsException.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2010 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.InterestManagement
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    using Badumna.Core;

    /// <summary>
    /// Base exception for Interest Management exceptions.
    /// </summary>
    [Serializable]
    internal class ImsException : BadumnaException
    {
        /// <summary>
        /// Initializes a new instance of the ImsException class.
        /// </summary>
        /// <param name="message">A message describing why the exception was thrown.</param>
        public ImsException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Gets a request status indicating that an exception was thrown.
        /// </summary>
        internal virtual RequestStatus.Statuses Status
        {
            get { return RequestStatus.Statuses.ExceptionThrownByOperation; }
        }
    }
}
