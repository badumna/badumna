//-----------------------------------------------------------------------
// <copyright file="ImsNoSuchServiceException.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2010 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.InterestManagement
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    using Badumna.Core;

    /// <summary>
    /// Exception indicating an IMS could not be found.
    /// </summary>
    [Serializable]
    internal class ImsNoSuchServiceException : ImsException
    {
        /// <summary>
        /// Initializes a new instance of the ImsNoSuchServiceException class.
        /// </summary>
        /// <param name="message">A message describing why the exception was thrown.</param>
        public ImsNoSuchServiceException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Gets a request status indicating that an IMS service could not be found.
        /// </summary>
        internal override RequestStatus.Statuses Status
        {
            get { return RequestStatus.Statuses.NoSuchService; }
        }
    }
}
