//-----------------------------------------------------------------------
// <copyright file="ImsInvalidArgumentException.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2010 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.InterestManagement
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    using Badumna.Core;

    /// <summary>
    /// Exception indicating an invalid argument.
    /// </summary>
    [Serializable]
    internal class ImsInvalidArgumentException : ImsException
    {
        /// <summary>
        /// The name of the invalid argument.
        /// </summary>
        private string argumentName;

        /// <summary>
        /// Initializes a new instance of the ImsInvalidArgumentException class.
        /// </summary>
        /// <param name="argumentName">The name of the invalid argument.</param>
        public ImsInvalidArgumentException(string argumentName)
            : base(Resources.ImsInvalidArgumentException_Message)
        {
            this.argumentName = argumentName;
        }

        /// <summary>
        /// Gets the name of the invalid argument.
        /// </summary>
        public string ArgumentName
        {
            get { return this.argumentName; }
        }
    }
}
