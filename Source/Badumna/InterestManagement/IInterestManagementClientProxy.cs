//-----------------------------------------------------------------------
// <copyright file="IInterestManagementClientProxy.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2010 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.InterestManagement
{
    using System.Collections.Generic;
    using System.Diagnostics;

    using Badumna.Core;
    using Badumna.DataTypes;

    /// <summary>
    /// TODO: documentation.
    /// </summary>
    internal interface IInterestManagementClientProxy
    {
        /// <summary>
        /// Gets or sets the destination for messages sent via the proxy.
        /// </summary>
        PeerAddress MessageDestination { get; set; }

        /// <summary>
        /// Gets or sets the QoS to use for messages.
        /// </summary>
        QualityOfService MessageQos { get; set; }

        /// <summary>
        /// Deliver responses to the requests received from the client.
        /// </summary>
        /// <param name="id">ID of the object involved in the operation whose request is being responded to.</param>
        /// <param name="requestInfo">The status of the request.</param>
        void StatusResponse(BadumnaId id, RequestStatus requestInfo);

        /// <summary>
        /// Deliver intersection notifications to the client.
        /// </summary>
        /// <param name="id">The ID of the object that has intersected.</param>
        /// <param name="enterList">The IDs of the objects that have been intersected with.</param>
        void IntersectionNotification(BadumnaId id, IList<ImsEventArgs> enterList);
    }
}
