//-----------------------------------------------------------------------
// <copyright file="IImsRegion.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2010 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace Badumna.InterestManagement
{
    using System;
    using Badumna.Core;
    using Badumna.DataTypes;
    using Badumna.DistributedHashTable;
    using Badumna.Utilities;

    /// <summary>
    /// Region types.
    /// </summary>
    public enum RegionType : byte
    {
        /// <summary>
        /// No type is specified.
        /// </summary>
        None = 0,

        /// <summary>
        /// TODO: documentation.
        /// </summary>
        Source,

        /// <summary>
        /// TODO: documentation.
        /// </summary>
        Sink,
    }

    /// <summary>
    /// Region options.
    /// </summary>
    [Flags]
    public enum RegionOptions : byte
    {
        /// <summary>
        /// No options are specified.
        /// </summary>
        None = 0,

        /// <summary>
        /// The region is an inverse sphere.
        /// </summary>
        Inverse = 0x01,

        /// <summary>
        /// Prevents a region from receiving any notifications of intersections
        /// </summary>
        Uninterested = 0x02,
    }
}