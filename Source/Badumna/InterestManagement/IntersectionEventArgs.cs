//-----------------------------------------------------------------------
// <copyright file="IntersectionEventArgs.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2010 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.InterestManagement
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Arguments for IMS region intersection events.
    /// </summary>
    internal class IntersectionEventArgs : EventArgs
    {
        /// <summary>
        /// TODO: documentation.
        /// </summary>
        private ImsRegion region;

        /// <summary>
        /// TODO: documentation.
        /// </summary>
        private List<ImsRegion> changedRegions;

        /// <summary>
        /// Initializes a new instance of the IntersectionEventArgs class.
        /// </summary>
        /// <param name="region">The region.</param>
        /// <param name="changedRegions">The changed regions.</param>
        public IntersectionEventArgs(ImsRegion region, List<ImsRegion> changedRegions)
        {
            this.region = region;
            this.changedRegions = changedRegions;
        }

        /// <summary>
        /// Initializes a new instance of the IntersectionEventArgs class.
        /// </summary>
        /// <param name="firstObject">The region.</param>
        /// <param name="secondObject">A changed region.</param>
        public IntersectionEventArgs(ImsRegion firstObject, ImsRegion secondObject)
            : this(firstObject, new List<ImsRegion>(new ImsRegion[] { secondObject }))
        {
        }

        /// <summary>
        /// Gets the region.
        /// </summary>
        public ImsRegion Region
        {
            get { return this.region; }
        }

        /// <summary>
        /// Gets a list of the changed regions.
        /// </summary>
        public List<ImsRegion> ChangedRegions
        {
            get { return this.changedRegions; }
        }
    }
}
