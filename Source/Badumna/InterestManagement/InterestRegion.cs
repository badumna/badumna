//-----------------------------------------------------------------------
// <copyright file="InterestRegion.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2010 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace Badumna.InterestManagement
{
    using Badumna.Core;
    using Badumna.DataTypes;
    using Badumna.Utilities;

    /// <summary>
    /// InterestRegion is just an ImsRegion of type 'Sink'.
    /// TODO: documentation.
    /// </summary>
    internal class InterestRegion : ImsRegion
    {
        /// <summary>
        /// Initializes a new instance of the InterestRegion class.
        /// </summary>
        /// <remarks>Default constructor for when created as Parseable.</remarks>
        public InterestRegion() // For IParseable 
        {
        }

        /// <summary>
        /// Initializes a new instance of the InterestRegion class.
        /// </summary>
        /// <param name="region">The region to copy.</param>
        public InterestRegion(ImsRegion region)
            : base(region)
        {
        }

        /// <summary>
        /// Initializes a new instance of the InterestRegion class.
        /// </summary>
        /// <param name="entityId">The ID of the region.</param>
        /// <param name="centroid">The centroid of the region.</param>
        /// <param name="radius">The radius of the region.</param>
        /// <param name="sceneName">The name of the scene the region belongs to.</param>
        public InterestRegion(BadumnaId entityId, Vector3 centroid, float radius, QualifiedName sceneName)
            : base(centroid, radius, entityId, RegionType.Sink, sceneName)
        {
        }
    }
}
