//-----------------------------------------------------------------------
// <copyright file="EventRegion.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2010 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace Badumna.InterestManagement
{
    using Badumna.Core;
    using Badumna.DataTypes;
    using Badumna.Utilities;

    /// <summary>
    /// EventRegion is just an IMS region with type 'Source'.
    /// TODO: better documentation.
    /// </summary>
    internal class EventRegion : ImsRegion
    {
        /// <summary>
        /// Initializes a new instance of the EventRegion class.
        /// </summary>
        /// <remarks>For use when creating as an IParseable.</remarks>
        public EventRegion() 
        {
        }

        /// <summary>
        /// Initializes a new instance of the EventRegion class.
        /// </summary>
        /// <param name="entityId">The ID of the region.</param>
        /// <param name="position">The centroid of the region.</param>
        /// <param name="radius">The radius of the region.</param>
        /// <param name="sceneName">The name of the scene the region belongs to.</param>
        public EventRegion(BadumnaId entityId, Vector3 position, float radius, QualifiedName sceneName)
            : base(position, radius, entityId, RegionType.Source, sceneName)
        {
        }

        /// <summary>
        /// Initializes a new instance of the EventRegion class.
        /// </summary>
        /// <param name="region">The IMS region to copy.</param>
        public EventRegion(ImsRegion region)
            : base(region)
        {
        }
    }
}
