//-----------------------------------------------------------------------
// <copyright file="RegionManager.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2010 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.InterestManagement
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using Badumna.Core;
    using Badumna.DataTypes;
    using Badumna.Utilities;

    /// <summary>
    /// Manages an interest management region's insertion/removal/update/keep-alive in an
    /// interest management service, and the resulting notifications.
    /// </summary>
    internal class RegionManager
    {
        /// <summary>
        /// The network connectivity reporter for notifying connectivity changes and querying connectivity status.
        /// </summary>
        private readonly INetworkConnectivityReporter networkConnectivityReporter;

        /// <summary>
        /// A factory for creating regular tasks.
        /// </summary>
        private readonly RegularTaskFactory regularTaskFactory;

        /// <summary>
        /// Delegate for intersection events.
        /// </summary>
        private EventHandler<ImsEventArgs> intersectionEventDelegate;

        /// <summary>
        /// TODO: documentation.
        /// </summary>
        private List<BadumnaId> intersections = new List<BadumnaId>();

        /// <summary>
        /// TODO: documentation.
        /// </summary>
        private byte numberOfActualIntersections;

        /// <summary>
        /// TODO: documentation.
        /// </summary>
        private uint regionTimeToLiveSeconds;

        /// <summary>
        /// TODO: documentation.
        /// </summary>
        private IRegularTask keepAliveTask;

        /// <summary>
        /// TODO: documentation.
        /// </summary>
        private TimeSpan lastSubscriptionRegionUpdateTime;

        /// <summary>
        /// Provides access to the current time.
        /// </summary>
        private ITime timeKeeper;

        /// <summary>
        /// Indicates whether the region has been inserted into the IMS.
        /// </summary>
        private bool hasBeenInserted;

        /// <summary>
        /// The interest management service.
        /// </summary>
        private IInterestManagementService interestManagementService;

        /// <summary>
        /// Initializes a new instance of the RegionManager class.
        /// <remarks>
        /// Initializes everything, but doesn't make any IMS requests until the first call to ModifyRegion.
        /// </remarks>
        /// </summary>
        /// <param name="sceneName">The name of the scene the region manager belongs to.</param>
        /// <param name="interestManagementService">The interest management service.</param>
        /// <param name="region">TODO: region.</param>
        /// <param name="regionTimeToLive">TODO: regionTimeToLive.</param>
        /// <param name="eventQueue">The queue to use for scheduling events.</param>
        /// <param name="timeKeeper">Provides access to the current time.</param>
        /// <param name="networkConnectivityReporter">Connectivity reporter for notifying/querying network connectivity changes.</param>
        public RegionManager(
            QualifiedName sceneName,
            IInterestManagementService interestManagementService,
            ImsRegion region,
            TimeSpan regionTimeToLive,
            INetworkEventScheduler eventQueue,
            ITime timeKeeper,
            INetworkConnectivityReporter networkConnectivityReporter) :
            this(
                sceneName,
                interestManagementService,
                region,
                regionTimeToLive,
                eventQueue,
                timeKeeper,
                networkConnectivityReporter,
                (d, p, q, c, t) => new RegularTask(d, p, q, c, t))
        {
        }

        /// <summary>
        /// Initializes a new instance of the RegionManager class.
        /// <remarks>
        /// Initializes everything, but doesn't make any IMS requests until the first call to ModifyRegion.
        /// </remarks>
        /// </summary>
        /// <param name="sceneName">The name of the scene the region manager belongs to.</param>
        /// <param name="interestManagementService">The interest management service.</param>
        /// <param name="region">TODO: region.</param>
        /// <param name="regionTimeToLive">TODO: regionTimeToLive.</param>
        /// <param name="eventQueue">The queue to schedule events on.</param>
        /// <param name="timeKeeper"> The time keeper.</param>
        /// <param name="networkConnectivityReporter">Connectivity reporter for notifying/querying network connectivity changes.</param>
        /// <param name="regularTaskFactory">Factory for creating regular tasks.</param>
        public RegionManager(
            QualifiedName sceneName,
            IInterestManagementService interestManagementService,
            ImsRegion region,
            TimeSpan regionTimeToLive,
            INetworkEventScheduler eventQueue,
            ITime timeKeeper,
            INetworkConnectivityReporter networkConnectivityReporter,
            RegularTaskFactory regularTaskFactory)
        {
            if (region == null)
            {
                throw new ArgumentNullException("region");
            }

            if (region.Type != RegionType.Sink && region.Type != RegionType.Source)
            {
                throw new ArgumentException("'region' has an invalid Type");
            }

            if (interestManagementService == null)
            {
                throw new ArgumentNullException("interestManagementService");
            }

            if (regionTimeToLive.TotalSeconds < 1.0)
            {
                throw new ArgumentOutOfRangeException("regionTimeToLive");
            }

            if (eventQueue == null)
            {
                throw new ArgumentNullException("eventQueue");
            }

            if (timeKeeper == null)
            {
                throw new ArgumentNullException("timeKeeper");
            }

            if (networkConnectivityReporter == null)
            {
                throw new ArgumentNullException("networkConnectivityReporter");
            }

            if (regularTaskFactory == null)
            {
                throw new ArgumentNullException("regularTaskFactory");
            }

            this.timeKeeper = timeKeeper;
            this.networkConnectivityReporter = networkConnectivityReporter;
            this.regularTaskFactory = regularTaskFactory;

            this.SceneName = sceneName;
            this.interestManagementService = interestManagementService;

            this.lastSubscriptionRegionUpdateTime = this.timeKeeper.Now;
            this.SubscriptionRegion = new ImsRegion(region);
            this.SubscriptionRegion.Radius = 0.0f;  // Force RecalculateSubscriptionRegion to calculate required size
            this.RecalcuateSubscriptionRegion(region.Centroid, region.Radius);
            this.regionTimeToLiveSeconds = (uint)Math.Max(0, regionTimeToLive.TotalSeconds);

            // TODO: Keep alive period should take into account RTTs?
            this.keepAliveTask = this.regularTaskFactory(
                "RegionManager keep alive",
                TimeSpan.FromSeconds(this.regionTimeToLiveSeconds * 0.3),
                eventQueue,
                networkConnectivityReporter,
                delegate
                {
                    Logger.TraceInformation(LogTag.InterestManagement, "Keep alive {0}", this.SubscriptionRegion);
                    this.MakeModifyRequest();  // Keep alive
                });

            this.networkConnectivityReporter.NetworkOnlineEvent += this.MakeModifyRequest;
        }

        /// <summary>
        /// Occurs when there is a new intersection.
        /// </summary>
        public event EventHandler<ImsEventArgs> IntersectionEvent
        {
            add { this.intersectionEventDelegate += value; }
            remove { this.intersectionEventDelegate -= value; }
        }

        /// <summary>
        /// Gets the interest management region.
        /// </summary>
        public ImsRegion Region
        {
            get
            {
                return this.SubscriptionRegion;
            }
        }

        /// <summary>
        /// Gets or sets the name of the scene the region manager belongs to.
        /// </summary>
        private QualifiedName SceneName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets ... to do: documentation.
        /// </summary>
        private ImsRegion SubscriptionRegion
        {
            get;
            set;
        }

        /// <summary>
        /// TODO: documentation.
        /// </summary>
        public void ShutDown()
        {
            this.ShutDown(true);

            // TODO: Shouldn't this be in the Shutdown(bool notify)? - Geoff.
            this.networkConnectivityReporter.NetworkOnlineEvent -= this.MakeModifyRequest;
        }

        /// <summary>
        /// TODO: documentation.
        /// </summary>
        /// <param name="notify">A flag indicating ... to do: documentation.</param>
        public void ShutDown(bool notify)
        {
            if (notify)
            {
                this.interestManagementService.RemoveRegion(this.SceneName, this.SubscriptionRegion);
            }
            else
            {
                // The subscription is removed but the IMS is not notified because the objects is still valid.
                this.interestManagementService.DetachSubscription(this.SceneName, this.SubscriptionRegion);
            }

            this.keepAliveTask.Stop();
        }

        /// <summary>
        /// TODO: documentation.
        /// </summary>
        /// <param name="centroid">The centroid of the region.</param>
        /// <param name="radius">The radious of the region.</param>
        public void ModifyRegion(Vector3 centroid, float radius)
        {
            this.ModifyRegion(centroid, radius, this.intersections.Count);
        }

        /// <summary>
        /// TODO: documentation.
        /// </summary>
        /// <param name="centroid">The centroid of the region.</param>
        /// <param name="radius">The radious of the region.</param>
        /// <param name="numberOfActualIntersections">TODO: number of intersections.</param>
        public void ModifyRegion(Vector3 centroid, float radius, int numberOfActualIntersections)
        {
            if (!this.keepAliveTask.IsRunning)
            {
                this.keepAliveTask.Start();
            }

            this.numberOfActualIntersections = (byte)numberOfActualIntersections;

            bool subscriptionChanged = this.RecalcuateSubscriptionRegion(centroid, radius);

            if (!this.hasBeenInserted)
            {
                this.interestManagementService.InsertRegion(
                    this.SceneName,
                    this.SubscriptionRegion,
                    this.regionTimeToLiveSeconds,
                    this.IntersectionHandler,
                    this.ImsStatusNotification);
                this.hasBeenInserted = true;
            }
            else if (subscriptionChanged)
            {
                this.MakeModifyRequest();
                this.keepAliveTask.Delay();
            }
        }

        /// <summary>
        /// TODO: documentation.
        /// </summary>
        /// <param name="regionId">The ID of the region.</param>
        public void ForceRemovalOfRegion(BadumnaId regionId)
        {
            Logger.TraceInformation(LogTag.InterestManagement, "RegionManager {0} forcing removal of {1}", this.SubscriptionRegion, regionId);

            if (this.intersections.Contains(regionId))
            {
                this.intersections.Remove(regionId);
            }
        }

        /// <summary>
        /// TODO: documentation.
        /// </summary>
        /// <param name="sender">The sender of the event.</param>
        /// <param name="e">The event arguments.</param>
        private void ImsStatusNotification(object sender, ImsStatusEventArgs e)
        {
            Debug.Assert(sender is IInterestManagementService, "RegionManager.ImsStatusNotification was invoked by something other than an IMS");
            Logger.TraceInformation(LogTag.InterestManagement, "Region manager {0} : IMS result {1}", this.SubscriptionRegion.Guid, e.Status);
        }

        /// <summary>
        /// TODO: documentation.
        /// </summary>
        /// <param name="sender">The sender of the event.</param>
        /// <param name="e">The event arguments.</param>
        private void IntersectionHandler(object sender, ImsEventArgs e)
        {
            Logger.TraceInformation(LogTag.InterestManagement, "RegionManager {0} got +{1}", this.SubscriptionRegion, e);

            EventHandler<ImsEventArgs> handler = this.intersectionEventDelegate;
            if (handler != null)
            {
                handler.Invoke(this, e);
            }

            // Store a list of intersections so we can give pass the count in a modify request
            if (!this.intersections.Contains(e.OtherRegionId))
            {
                this.intersections.Add(e.OtherRegionId);
            }
        }

        /// <summary>
        /// TODO: documentation.
        /// </summary>
        /// <param name="newCentroid">The centroid of the subscription region.</param>
        /// <param name="newRadius">The radius of the subscription region.</param>
        /// <returns>A value indicating whether the subscription region was recalculated.</returns>
        private bool RecalcuateSubscriptionRegion(Vector3 newCentroid, float newRadius)
        {
            // TODO: Shrink sometimes?
            if (!Geometry.SphereContained(this.SubscriptionRegion.Centroid, this.SubscriptionRegion.Radius, newCentroid, newRadius))
            {
                this.SubscriptionRegion.Centroid = newCentroid;
                this.SubscriptionRegion.Radius = newRadius;
                this.SubscriptionRegion.Radius += this.interestManagementService.SubscriptionInflation;
                this.lastSubscriptionRegionUpdateTime = this.timeKeeper.Now;

                return true;
            }

            if (this.timeKeeper.Now - this.lastSubscriptionRegionUpdateTime >= Parameters.MaxRegionSubscriptionSyncInterval)
            {
                if (!this.SubscriptionRegion.Centroid.Equals(newCentroid) || this.SubscriptionRegion.Radius != (newRadius + this.interestManagementService.SubscriptionInflation))
                {
                    this.SubscriptionRegion.Centroid = newCentroid;
                    this.SubscriptionRegion.Radius = newRadius;
                    this.SubscriptionRegion.Radius += this.interestManagementService.SubscriptionInflation;

                    this.lastSubscriptionRegionUpdateTime = this.timeKeeper.Now;
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// TODO: documentation.
        /// </summary>
        private void MakeModifyRequest()
        {
            if (this.networkConnectivityReporter.Status == ConnectivityStatus.Online)
            {
                Logger.TraceInformation(LogTag.InterestManagement, "Making modify request for {0}", this.SubscriptionRegion.Guid);

                // TO DO: Remove Min operation, as variable cannot exceed datatype maximum anyway.
                this.interestManagementService.ModifyRegion(
                    this.SceneName,
                    this.SubscriptionRegion,
                    (uint)this.regionTimeToLiveSeconds,
                    (byte)Math.Min(byte.MaxValue, this.numberOfActualIntersections));
            }
        }
    }
}
