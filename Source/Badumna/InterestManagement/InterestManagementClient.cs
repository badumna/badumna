//-----------------------------------------------------------------------
// <copyright file="InterestManagementClient.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2010 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.InterestManagement
{
    using System;
    using System.Collections.Generic;

    using Badumna.Core;
    using Badumna.DataTypes;

    /// <summary>
    /// TODO: documentation.
    /// </summary>
    internal abstract class InterestManagementClient : InterestManagementClientProtocol, IInterestManagementClient
    {
        /// <summary>
        /// Delegate for status response events.
        /// </summary>
        private EventHandler<ImsStatusEventArgs> statusNotificationDelegate;

        /// <summary>
        /// Delegate for intersection notification events.
        /// </summary>
        private EventHandler<ImsStatusEventArgs> resultNotificationDelegate;

        /// <summary>
        /// Initializes a new instance of the InterestManagementClient class.
        /// </summary>
        /// <param name="protocolLayer">Message parser to be configured for RPCs.</param>
        public InterestManagementClient(MessageParser protocolLayer)
        {
            protocolLayer.RegisterMethodsIn(this);
        }

        /// <summary>
        /// Triggered upon receipt of a status response.
        /// </summary>
        public event EventHandler<ImsStatusEventArgs> StatusNotification
        {
            add { this.statusNotificationDelegate += value; }
            remove { this.statusNotificationDelegate -= value; }
        }

        /// <summary>
        /// Triggered upon receipt of an intersection.
        /// </summary>
        public event EventHandler<ImsStatusEventArgs> ResultNotification
        {
            add { this.resultNotificationDelegate += value; }
            remove { this.resultNotificationDelegate -= value; }
        }

        /// <inheritdoc/>
        public override void StatusResponse(BadumnaId id, RequestStatus requestInfo)
        {
            this.InvokeStatusNotification(this, new ImsStatusEventArgs(id, requestInfo));
        }

        /// <inheritdoc/>
        public override void IntersectionNotification(BadumnaId id, IList<ImsEventArgs> enterList)
        {
            this.InvokeResultNotification(this, new ImsStatusEventArgs(id, new RequestStatus(), enterList));
        }

        /// <summary>
        /// Pass status notifications on to subscribers.
        /// </summary>
        /// <param name="sender">The notification sender.</param>
        /// <param name="e">The status event arguments.</param>
        protected void InvokeStatusNotification(object sender, ImsStatusEventArgs e)
        {
            EventHandler<ImsStatusEventArgs> statusNotification = this.statusNotificationDelegate;
            if (statusNotification != null)
            {
                statusNotification(sender, e);
            }
        }

        /// <summary>
        /// Pass result notifications on to subscribers.
        /// </summary>
        /// <param name="sender">The event sender.</param>
        /// <param name="e">The statis event arguments.</param>
        protected void InvokeResultNotification(object sender, ImsStatusEventArgs e)
        {
            EventHandler<ImsStatusEventArgs> resultNotification = this.resultNotificationDelegate;
            if (resultNotification != null)
            {
                resultNotification(sender, e);
            }
        }
    }
}
