//-----------------------------------------------------------------------
// <copyright file="IInterestManagementClient.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2010 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.InterestManagement
{
    using System;
    using System.Collections.Generic;
    using Badumna.DataTypes;

    /// <summary>
    /// TODO: documentation.
    /// </summary>
    internal interface IInterestManagementClient
    {
        /// <summary>
        /// Triggered upon receipt of a status response.
        /// </summary>
        event EventHandler<ImsStatusEventArgs> StatusNotification;

        /// <summary>
        /// Triggered upon receipt of an intersection notification.
        /// </summary>
        event EventHandler<ImsStatusEventArgs> ResultNotification;

        /// <summary>
        /// Called to return the result of an operation to the client.
        /// </summary>
        /// <param name="id">UniqueNetworkId owned by the client indicating the object involved in the operation</param>
        /// <param name="requestInfo">Result of operation</param>
        void StatusResponse(BadumnaId id, RequestStatus requestInfo);

        /// <summary>
        /// Notifies the client of new intersections.
        /// </summary>
        /// <param name="id">UniqueNetworkId owned by the client indicating the object getting intersections</param>
        /// <param name="enterList">List of newly intersecting objects</param>
        void IntersectionNotification(BadumnaId id, IList<ImsEventArgs> enterList);
    }
}
