//-----------------------------------------------------------------------
// <copyright file="RequestStatus.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2010 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
  
namespace Badumna.InterestManagement
{
    using System;

    using Badumna.Core;

    /// <summary>
    /// TODO: documentation.
    /// </summary>
    [Serializable]
    internal class RequestStatus : IParseable
    {
        /// <summary>
        /// The status.
        /// </summary>
        private Statuses status;

        /// <summary>
        /// Initializes a new instance of the RequestStatus class.
        /// </summary>
        /// <remarks>
        /// used by IParseable
        /// </remarks>
        public RequestStatus()
        {
        }

        /// <summary>
        /// Initializes a new instance of the RequestStatus class.
        /// </summary>
        /// <param name="status">The status to specify.</param>
        public RequestStatus(Statuses status)
        {
            this.status = status;
        }

        /// <summary>
        /// Initializes a new instance of the RequestStatus class.
        /// </summary>
        /// <param name="other">A request status to copy.</param>
        public RequestStatus(RequestStatus other)
        {
            this.status = other.status;
        }

        /// <summary>
        /// Status of interest management requests.
        /// </summary>
        public enum Statuses : byte
        {
            /// <summary>
            /// The requested operation was carried out successfully.
            /// </summary>
            Success = 0,

            /// <summary>
            /// The requested operation has been completed.
            /// </summary>
            Complete = 0x80,

            /// <summary>
            /// The request referred to a scene that didn't exist.
            /// </summary>
            NoSuchScene = 0x81,

            /// <summary>
            /// An exception was thrown during the operation. 
            /// </summary>
            ExceptionThrownByOperation = 0x82,

            /// <summary>
            /// The scene name in the request is already used.
            /// </summary>
            SceneNameAlreadyUsed = 0x83,

            /// <summary>
            /// The request referred to an invalid scene.
            /// </summary>
            InvalidRegion = 0x84,

            /// <summary>
            /// Enumeration value not used.
            /// </summary>
            /// <remarks>
            /// TODO: Objects appear to be synonymous with regions. A single term should be used.
            /// </remarks>
            InvalidObject = 0x85,

            /// <summary>
            /// The region in the request has expired.
            /// </summary>
            Expired = 0x86,

            /// <summary>
            /// Used to indicate that a service has been shutdown.
            /// </summary>
            NoSuchService = 0x87,
        }

        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        public Statuses Status
        {
            get { return this.status; }
            set { this.status = value; }
        }

        /// <summary>
        /// Gets a value indicating whether the request has completed.
        /// </summary>
        public bool IsComplete
        {
            // TODO: Implies success is not complete - is success misnamed?
            // TODO: Statuses.Complete is also > 0.
            get { return this.Status > 0 || RequestStatus.Statuses.Complete == this.Status; }
        }
        
        /// <summary>
        /// Gets a value indicating whether the request has failed.
        /// </summary>
        public bool HasFailed
        {
            // TODO: is Statuses.Success misnamed?
            get { return this.Status > 0 && RequestStatus.Statuses.Complete != this.Status; }
        }

        #region IParseable Members

        /// <inheritdoc />
        public void ToMessage(MessageBuffer message, Type parameterType)
        {
            if (null != message)
            {
                message.Write((byte)this.Status);
            }
        }

        /// <inheritdoc />
        public void FromMessage(MessageBuffer message)
        {
            if (null != message)
            {
                this.status = (Statuses)message.ReadByte();
            }
        }

        #endregion //IParseable Members

        /// <inheritdoc />
        public override string ToString()
        {
            return String.Format("Status = {0}", this.status);
        }
    }
}
