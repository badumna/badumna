﻿//-----------------------------------------------------------------------
// <copyright file="NetworkScene.cs" company="NICTA">
//     Copyright (c) 2008 NICTA. All right reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.SpatialEntities
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    
    using Badumna.Autoreplication;
    using Badumna.Autoreplication.Serialization;
    using Badumna.Core;
    using Badumna.DataTypes;
    using Badumna.InterestManagement;
    using Badumna.Utilities;
    
    using SpatialAutoreplicationManager = Badumna.Autoreplication.SpatialAutoreplicationManager;

    /// <summary>
    /// A delegate called by the network layer when a new spatial entity arrives within the visible region for a given network scene.
    /// The delegate should return an instance of <see cref="ISpatialReplica"/> to which remote updates and custom messages will be applied.
    /// </summary>
    /// <remarks>A delegate of this type should be passed to JoinScene when using auto-replication.</remarks>
    /// <param name="scene">The instance of the scene in which the new spatial entity has arrived</param>
    /// <param name="entityId">The id of the new entity</param>
    /// <param name="entityType">The application level type id that was associated with this entity when the entity was registered on the 
    /// owning peer.</param>
    /// <returns>An instance of the new entity</returns>
    public delegate IReplicableEntity CreateSpatialReplica(NetworkScene scene, BadumnaId entityId, uint entityType);

    /// <summary>
    /// A delegate called by the network layer when an entity leaves the visible region for a given network scene.
    /// This delegate gives the application layer the opportunity to cleanup any references to the given replica. It indicates
    /// that no more updates or custom messages will arrive for this replica.
    /// </summary>
    /// <remarks>A delegate of this type should be passed to JoinScene when using auto-replication.</remarks>
    /// <param name="scene">The scene from which the replica is being removed.</param>
    /// <param name="replica">The replica being removed</param>
    public delegate void RemoveSpatialReplica(NetworkScene scene, IReplicableEntity replica);

    /// <summary>
    /// A distinct virtual space that entities exist in.
    /// </summary>
    /// <remarks>
    /// A NetworkScene provides a space for entities that is disjoint from all other scenes.  Entities that
    /// are registered on a given scene can potentially see each other, but entities that don't share a scene
    /// cannot.
    /// NetworkScenes are completely identified by a string name, so logically separate scenes require distinct names.
    /// NetworkScene instances can be retrieved by calling NetworkFacade.JoinScene().
    /// </remarks>
    public abstract class NetworkScene
    {
        /// <summary>
        /// Manages auto replication wrappers for replicable entities.
        /// </summary>
        private readonly SpatialAutoreplicationManager autoreplicationManager;

        /// <summary>
        /// A value indicating whether the scene is a mini scene.
        /// </summary>
        private bool miniScene;

        /// <summary>
        /// Called when a new entity needs to be instantiated into the scene.
        /// </summary>
        private CreateSpatialReplica createReplica;

        /// <summary>
        /// Called when an entity in the scene departs.
        /// </summary>
        private RemoveSpatialReplica removeReplica;

        /// <summary>
        /// Map of unique IDs to original spatial entities.
        /// </summary>
        private Dictionary<BadumnaId, ISpatialOriginal> originals;

        /// <summary>
        /// Map of unique IDs to replicated spatial entities.
        /// </summary>
        private Dictionary<BadumnaId, ISpatialReplica> replicas;

        /// <summary>
        /// The queue to use for scheduling events.
        /// </summary>
        private NetworkEventQueue eventQueue;

        /// <summary>
        /// The method to use to queue events that must run in the application's thread.
        /// </summary>
        private QueueInvokable queueApplicationEvent;

        /// <summary>
        /// The name of the scene.
        /// </summary>
        private QualifiedName qualifiedName;

        /// <summary>
        /// Initializes a new instance of the NetworkScene class.
        /// </summary>
        /// <param name="name">Unique name of the scene.</param>
        /// <param name="createReplica">Called when a new entity needs to be instantiated into the scene.</param>
        /// <param name="removeReplica">Called when an entity in the scene departs.</param>
        /// <param name="autoreplicationManager">Manages auto replication wrappers for replicated entities.</param>
        /// <param name="eventQueue">The event queue to schedule events on.</param>
        /// <param name="queueApplicationEvent">The method to use to queue events that must run in the application's thread.</param>
        /// <param name="miniScene">A value indicating whether the scene is a mini scene.</param>        
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.StyleCop.CSharp.ReadabilityRules", "SA1118:ParameterMustNotSpanMultipleLines", Justification = "Exception for #if")]
        internal NetworkScene(
            QualifiedName name,
            CreateSpatialReplica createReplica,
            RemoveSpatialReplica removeReplica,
            SpatialAutoreplicationManager autoreplicationManager,
            NetworkEventQueue eventQueue,
            QueueInvokable queueApplicationEvent,
            bool miniScene)
        {
            ////if (autoreplicationManager == null)
            ////{
            ////    throw new ArgumentNullException("autoreplicationManager");
            ////}

            if (eventQueue == null)
            {
                throw new ArgumentNullException("eventQueue");
            }

            if (queueApplicationEvent == null)
            {
                throw new ArgumentNullException("queueApplicationEvent");
            }

            this.autoreplicationManager = autoreplicationManager;
            this.eventQueue = eventQueue;
            this.queueApplicationEvent = queueApplicationEvent;
            this.miniScene = miniScene;

            this.qualifiedName = name;
            this.createReplica = createReplica;
            this.removeReplica = removeReplica;
            this.originals = new Dictionary<BadumnaId, ISpatialOriginal>();
            this.replicas = new Dictionary<BadumnaId, ISpatialReplica>();
        }

        /// <summary>
        /// Gets the unique name of the scene.
        /// </summary>
        public string Name 
        {
            get { return this.QualifiedName.Name; } 
        }

        /// <summary>
        /// Gets a value indicating whether the scene is a mini scene.
        /// </summary>
        public bool MiniScene               
        {
            get { return this.miniScene; }
        }

        /// <summary>
        /// Gets the collection of original entities instantiated in this scene on this peer.
        /// </summary>
        public Dictionary<BadumnaId, ISpatialOriginal> Originals
        {
            get { return this.originals; }
        }

        /// <summary>
        /// Gets the collection of remote entities instantiated in this scene on this peer.
        /// </summary>
        public Dictionary<BadumnaId, ISpatialReplica> Replicas
        {
            get { return this.replicas; }
        }

        /// <summary>
        /// Gets the unique, qualified name of the scene.
        /// </summary>
        internal QualifiedName QualifiedName
        {
            get { return this.qualifiedName; }
        }

        /// <summary>
        /// Registers the given entity with the scene.
        /// </summary>
        /// <remarks>
        /// An entity must be registered on a scene in order to receive updates from other entities on that scene, and
        /// to propagate its updates to other entities on that scene.  An entity can only be registered on a single
        /// scene at a time.  If the entity is currently registered with a different scene calling this method will
        /// automatically unregister it from that scene.
        /// Once registered, the entity will send updates to all other entities that have a sphere of interest
        /// that intersects this entity's bounding sphere.
        /// An entity's bounding sphere is described by an its position and its radius.
        /// An entity's sphere of interest is described by is position and sphere of interest radius.
        /// by Position and Radius.
        /// </remarks>
        /// <param name="entity">The entity to register.</param>
        /// <param name="entityType">An integer representing the type of the entity.  This integer will be passed to
        /// the CreateReplica delegate on remote peers.</param>
        /// <param name="radius">The radius of the entity's bounding sphere.</param>
        /// <param name="sphereOfInterestRadius">The radius of the entity's sphere of interest.</param>
        public void RegisterEntity(
            IReplicableEntity entity,
            uint entityType,
            float radius,
            float sphereOfInterestRadius)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }

            ISpatialOriginal wrapper = this.autoreplicationManager.RegisterOriginal(entity, new KeyValuePair<float, float>(radius, sphereOfInterestRadius));
            this.RegisterEntity(wrapper, entityType);
        }
       
        /// <summary>
        /// Unregister the given entity with the scene.
        /// </summary>
        /// <remarks>
        /// Once the entity has been unregistered from the scene it will no longer receive updates from
        /// entities on that scene (unless they share another scene).  Equally, updates from the unregistered
        /// entity will not be propagated to entities on that scene.  An entity removal event will be propagated
        /// peers that are interested in the entity at the time.
        /// </remarks>
        /// <param name="entity">The locally owned entity to remove from the current scene.</param>
        public void UnregisterEntity(IReplicableEntity entity)
        {
            ISpatialOriginal wrapper = this.autoreplicationManager.UnregisterOriginal(entity);
            this.UnregisterEntity(wrapper);
        }

        /// <summary>
        /// Registers the given entity with the scene.
        /// </summary>
        /// <remarks>
        /// An entity must be registered on a scene in order to receive updates from other entities on that scene, and
        /// to propagate its updates to other entities on that scene.  An entity can only be registered on a single
        /// scene at a time.  If the entity is currently registered with a different scene calling this method will
        /// automatically unregister it from that scene.  The first time an entity is registered on a scene its
        /// Guid must be null. Badumna will generate the Guid for the entity.
        /// </remarks>
        /// <param name="entity">The entity to register.</param>
        /// <param name="entityType">An integer representing the type of the entity.  This integer will be passed to
        /// the CreateReplica delegate on remote peers.</param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.StyleCop.CSharp.DocumentationRules", "SA1650:ElementDocumentationMustBeSpelledCorrectly", Justification = "Exception for Guid and Badumna")]
        public void RegisterEntity(ISpatialOriginal entity, uint entityType)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }

            if (entity.AreaOfInterestRadius < 0)
            {
                throw new ArgumentOutOfRangeException("entity", "Area of interest radius cannot be negative");
            }

            this.ValidateInterestRadius(entity);

            //// TODO: Enforce that the user cannot initialize the guid.  Note that guid != null doesn't
            ////       mean that the user initialized it because the entity may have previously been
            ////       registered on another scene.

            this.eventQueue.AcquirePerformLockForApplicationCode();
            try
            {
                this.RegisterEntityImplementation(entity, entityType);
                Debug.Assert(entity.Guid != null && entity.Guid.IsValid, "ISpatialOriginal has an invalid unique ID.");
                this.originals[entity.Guid] = entity;
            }
            finally
            {
                this.eventQueue.ReleasePerformLockForApplicationCode();
            }
        }

        /// <summary>
        /// Unregister the given entity with the scene.
        /// </summary>
        /// <remarks>
        /// Once the entity has been unregistered from the scene it will no longer receive updates from
        /// entities on that scene (unless they share another scene).  Equally, updates from the unregistered
        /// entity will not be propagated to entities on that scene.  An entity removal event will be propagated
        /// peers that are interested in the entity at the time.
        /// </remarks>
        /// <param name="entity">The locally owned entity to remove from the current scene.</param>
        public void UnregisterEntity(ISpatialOriginal entity)
        {
            this.eventQueue.AcquirePerformLockForApplicationCode();
            try
            {
                this.originals.Remove(entity.Guid);
                this.UnregisterEntityImplementation(entity);
            }
            finally
            {
                this.eventQueue.ReleasePerformLockForApplicationCode();
            }
        }

        /// <summary>
        /// Depart from the given scene. 
        /// </summary>
        /// <remarks>
        /// Any local entities that were registered on the scene will be unregistered automatically.
        /// </remarks>
        public void Leave()
        {
            this.eventQueue.AcquirePerformLockForApplicationCode();
            try
            {
                this.LeaveImplimentation();
                this.queueApplicationEvent(Apply.Func(delegate { this.originals.Clear(); }));
                this.queueApplicationEvent(Apply.Func(delegate { this.replicas.Clear(); }));
            }
            finally
            {
                this.eventQueue.ReleasePerformLockForApplicationCode();
            }
        }
        
        /// <summary>
        /// Remotely call a method on all the replicas of a given original.
        /// </summary>
        /// <param name="method">The method on the original.</param>
        public void CallMethodOnReplicas(RpcSignature method)
        {
            this.autoreplicationManager.CallMethodOnReplicas(method);
        }

        /// <summary>
        /// Remotely call a method on all the replicas of a given original.
        /// </summary>
        /// <typeparam name="T1">The type of the method's parameter.</typeparam>
        /// <param name="method">The method on the original.</param>
        /// <param name="arg1">The parameter.</param>
        public void CallMethodOnReplicas<T1>(RpcSignature<T1> method, T1 arg1)
        {
            this.autoreplicationManager.CallMethodOnReplicas(method, arg1);
        }

        /// <summary>
        /// Remotely call a method on all the replicas of a given original.
        /// </summary>
        /// <typeparam name="T1">The type of the method's first parameter.</typeparam>
        /// <typeparam name="T2">The type of the method's second parameter.</typeparam>
        /// <param name="method">The method on the original.</param>
        /// <param name="arg1">The first parameter.</param>
        /// <param name="arg2">The second parameter.</param>
        public void CallMethodOnReplicas<T1, T2>(RpcSignature<T1, T2> method, T1 arg1, T2 arg2)
        {
            this.autoreplicationManager.CallMethodOnReplicas(method, arg1, arg2);
        }

        /// <summary>
        /// Call a method on all the replicas of a given original.
        /// </summary>
        /// <typeparam name="T1">The type of the method's first parameter.</typeparam>
        /// <typeparam name="T2">The type of the method's second parameter.</typeparam>
        /// <typeparam name="T3">The type of the method's third parameter.</typeparam>
        /// <param name="method">The corresponding method on the original.</param>
        /// <param name="arg1">The first parameter.</param>
        /// <param name="arg2">The second parameter.</param>
        /// <param name="arg3">The third parameter.</param>
        public void CallMethodOnReplicas<T1, T2, T3>(
            RpcSignature<T1, T2, T3> method,
            T1 arg1,
            T2 arg2,
            T3 arg3)
        {
            this.autoreplicationManager.CallMethodOnReplicas(method, arg1, arg2, arg3);
        }

        /// <summary>
        /// Call a method on all the replicas of a given original.
        /// </summary>
        /// <typeparam name="T1">The type of the method's first parameter.</typeparam>
        /// <typeparam name="T2">The type of the method's second parameter.</typeparam>
        /// <typeparam name="T3">The type of the method's third parameter.</typeparam>
        /// <typeparam name="T4">The type of the method's fourth parameter.</typeparam>
        /// <param name="method">The corresponding method on the original.</param>
        /// <param name="arg1">The first parameter.</param>
        /// <param name="arg2">The second parameter.</param>
        /// <param name="arg3">The third parameter.</param>
        /// <param name="arg4">The fourth parameter.</param>
        public void CallMethodOnReplicas<T1, T2, T3, T4>(
            RpcSignature<T1, T2, T3, T4> method,
            T1 arg1,
            T2 arg2,
            T3 arg3,
            T4 arg4)
        {
            this.autoreplicationManager.CallMethodOnReplicas(method, arg1, arg2, arg3, arg4);
        }

        /// <summary>
        /// Call a method on all the replicas of a given original.
        /// </summary>
        /// <typeparam name="T1">The type of the method's first parameter.</typeparam>
        /// <typeparam name="T2">The type of the method's second parameter.</typeparam>
        /// <typeparam name="T3">The type of the method's third parameter.</typeparam>
        /// <typeparam name="T4">The type of the method's fourth parameter.</typeparam>
        /// <typeparam name="T5">The type of the method's fifth parameter.</typeparam>
        /// <param name="method">The corresponding method on the original.</param>
        /// <param name="arg1">The first parameter.</param>
        /// <param name="arg2">The second parameter.</param>
        /// <param name="arg3">The third parameter.</param>
        /// <param name="arg4">The fourth parameter.</param>
        /// <param name="arg5">The fifth parameter.</param>
        public void CallMethodOnReplicas<T1, T2, T3, T4, T5>(
            RpcSignature<T1, T2, T3, T4, T5> method,
            T1 arg1,
            T2 arg2,
            T3 arg3,
            T4 arg4,
            T5 arg5)
        {
            this.autoreplicationManager.CallMethodOnReplicas(method, arg1, arg2, arg3, arg4, arg5);
        }

        /// <summary>
        /// Remotely call a method on all the replicas of a given original.
        /// </summary>
        /// <typeparam name="T1">The type of the method's first parameter.</typeparam>
        /// <typeparam name="T2">The type of the method's second parameter.</typeparam>
        /// <typeparam name="T3">The type of the method's third parameter.</typeparam>
        /// <typeparam name="T4">The type of the method's fourth parameter.</typeparam>
        /// <typeparam name="T5">The type of the method's fifth parameter.</typeparam>
        /// <typeparam name="T6">The type of the method's sixth parameter.</typeparam>
        /// <param name="method">The corresponding method on the original.</param>
        /// <param name="arg1">The first parameter.</param>
        /// <param name="arg2">The second parameter.</param>
        /// <param name="arg3">The third parameter.</param>
        /// <param name="arg4">The fourth parameter.</param>
        /// <param name="arg5">The fifth parameter.</param>
        /// <param name="arg6">The sixth parameter.</param>
        public void CallMethodOnReplicas<T1, T2, T3, T4, T5, T6>(
            RpcSignature<T1, T2, T3, T4, T5, T6> method,
            T1 arg1,
            T2 arg2,
            T3 arg3,
            T4 arg4,
            T5 arg5,
            T6 arg6)
        {
            this.autoreplicationManager.CallMethodOnReplicas(method, arg1, arg2, arg3, arg4, arg5, arg6);
        }

        /// <summary>
        /// Remotely call a method on the original of a given replica.
        /// </summary>
        /// <param name="method">The replica's method.</param>
        public void CallMethodOnOriginal(RpcSignature method)
        {
            this.autoreplicationManager.CallMethodOnOriginal(method);
        }

        /// <summary>
        /// Remotely call a method on the original of a given replica.
        /// </summary>
        /// <typeparam name="T1">The type of the method's first parameter.</typeparam>
        /// <param name="method">The replica's method.</param>
        /// <param name="arg1">The first parameter.</param>
        public void CallMethodOnOriginal<T1>(RpcSignature<T1> method, T1 arg1)
        {
            this.autoreplicationManager.CallMethodOnOriginal(method, arg1);
        }

        /// <summary>
        /// Remotely call a method on the original of a given replica.
        /// </summary>
        /// <typeparam name="T1">The type of the method's first parameter.</typeparam>
        /// <typeparam name="T2">The type of the method's second parameter.</typeparam>
        /// <param name="method">The replica's method.</param>
        /// <param name="arg1">The first parameter.</param>
        /// <param name="arg2">The second parameter.</param>
        public void CallMethodOnOriginal<T1, T2>(RpcSignature<T1, T2> method, T1 arg1, T2 arg2)
        {
            this.autoreplicationManager.CallMethodOnOriginal(method, arg1, arg2);
        }

        /// <summary>
        /// Call a method on all the original of a given replica.
        /// </summary>
        /// <typeparam name="T1">The type of the method's first parameter.</typeparam>
        /// <typeparam name="T2">The type of the method's second parameter.</typeparam>
        /// <typeparam name="T3">The type of the method's third parameter.</typeparam>
        /// <param name="method">The corresponding method on the replica.</param>
        /// <param name="arg1">The first parameter.</param>
        /// <param name="arg2">The second parameter.</param>
        /// <param name="arg3">The third parameter.</param>
        public void CallMethodOnOriginal<T1, T2, T3>(
            RpcSignature<T1, T2, T3> method,
            T1 arg1,
            T2 arg2,
            T3 arg3)
        {
            this.autoreplicationManager.CallMethodOnOriginal(method, arg1, arg2, arg3);
        }

        /// <summary>
        /// Call a method on all the original of a given replica.
        /// </summary>
        /// <typeparam name="T1">The type of the method's first parameter.</typeparam>
        /// <typeparam name="T2">The type of the method's second parameter.</typeparam>
        /// <typeparam name="T3">The type of the method's third parameter.</typeparam>
        /// <typeparam name="T4">The type of the method's fourth parameter.</typeparam>
        /// <param name="method">The corresponding method on the replica.</param>
        /// <param name="arg1">The first parameter.</param>
        /// <param name="arg2">The second parameter.</param>
        /// <param name="arg3">The third parameter.</param>
        /// <param name="arg4">The fourth parameter.</param>
        public void CallMethodOnOriginal<T1, T2, T3, T4>(
            RpcSignature<T1, T2, T3, T4> method,
            T1 arg1,
            T2 arg2,
            T3 arg3,
            T4 arg4)
        {
            this.autoreplicationManager.CallMethodOnOriginal(method, arg1, arg2, arg3, arg4);
        }

        /// <summary>
        /// Call a method on all the original of a given replica.
        /// </summary>
        /// <typeparam name="T1">The type of the method's first parameter.</typeparam>
        /// <typeparam name="T2">The type of the method's second parameter.</typeparam>
        /// <typeparam name="T3">The type of the method's third parameter.</typeparam>
        /// <typeparam name="T4">The type of the method's fourth parameter.</typeparam>
        /// <typeparam name="T5">The type of the method's fifth parameter.</typeparam>
        /// <param name="method">The corresponding method on the replica.</param>
        /// <param name="arg1">The first parameter.</param>
        /// <param name="arg2">The second parameter.</param>
        /// <param name="arg3">The third parameter.</param>
        /// <param name="arg4">The fourth parameter.</param>
        /// <param name="arg5">The fifth parameter.</param>
        public void CallMethodOnOriginal<T1, T2, T3, T4, T5>(
            RpcSignature<T1, T2, T3, T4, T5> method,
            T1 arg1,
            T2 arg2,
            T3 arg3,
            T4 arg4,
            T5 arg5)
        {
            this.autoreplicationManager.CallMethodOnOriginal(method, arg1, arg2, arg3, arg4, arg5);
        }

        /// <summary>
        /// Call a method on all the original of a given replica.
        /// </summary>
        /// <typeparam name="T1">The type of the method's first parameter.</typeparam>
        /// <typeparam name="T2">The type of the method's second parameter.</typeparam>
        /// <typeparam name="T3">The type of the method's third parameter.</typeparam>
        /// <typeparam name="T4">The type of the method's fourth parameter.</typeparam>
        /// <typeparam name="T5">The type of the method's fifth parameter.</typeparam>
        /// <typeparam name="T6">The type of the method's sixth parameter.</typeparam>
        /// <param name="method">The corresponding method on the replica.</param>
        /// <param name="arg1">The first parameter.</param>
        /// <param name="arg2">The second parameter.</param>
        /// <param name="arg3">The third parameter.</param>
        /// <param name="arg4">The fourth parameter.</param>
        /// <param name="arg5">The fifth parameter.</param>
        /// <param name="arg6">The sixth parameter.</param>
        public void CallMethodOnOriginal<T1, T2, T3, T4, T5, T6>(
            RpcSignature<T1, T2, T3, T4, T5, T6> method,
            T1 arg1,
            T2 arg2,
            T3 arg3,
            T4 arg4,
            T5 arg5,
            T6 arg6)
        {
            this.autoreplicationManager.CallMethodOnOriginal(method, arg1, arg2, arg3, arg4, arg5, arg6);
        }

        /// <summary>
        /// Instantiate a replicated spatial entity.
        /// </summary>
        /// <param name="entityId">Unique ID of the intended entity.</param>
        /// <param name="entityType">Type of the intended entity.</param>
        /// <returns>A new replicated spatial entity if ID is unique and entity was successfully
        /// created, an old entity if the ID is already in use, or otherwise null.</returns>
        internal ISpatialReplica InstantiateReplica(BadumnaId entityId, uint entityType)
        {
            this.eventQueue.AssertIsApplicationThread();

            // Never create a replica on the scene instance that stores the original
            if (this.createReplica == null ||
                this.originals.ContainsKey(entityId))
            {
                return null;
            }

            if (this.replicas.ContainsKey(entityId))
            {
                return this.replicas[entityId];
            }

            IReplicableEntity replica = this.createReplica(this, entityId, entityType);
            if (replica == null)
            {
                // This allows scenes to ignore replicas (presumably by design?)
                return null;
            }

            ISpatialReplica spatialReplica = replica as ISpatialReplica;
            if (spatialReplica == null)
            {
                if (this.autoreplicationManager == null)
                {
                    throw new NotSupportedException("Autoreplication is not available.");
                }

                spatialReplica = this.autoreplicationManager.RegisterReplica(replica);
            }

            spatialReplica.Guid = entityId;
            this.replicas[entityId] = spatialReplica;
            return spatialReplica;
        }

        /// <summary>
        /// Remove a replicated spatial entity.
        /// </summary>
        /// <param name="replica">The entity to be removed.</param>
        internal void RemoveReplica(ISpatialReplica replica)
        {
            this.eventQueue.AssertIsApplicationThread();

            if (!this.replicas.ContainsKey(replica.Guid))
            {
                return;
            }

            this.replicas.Remove(replica.Guid);

            if (this.removeReplica == null)
            {
                return;
            }

            IReplicableEntity entity = replica;
            if (replica is PositionalReplicaWrapper)
            {
                entity = this.autoreplicationManager.UnregisterReplica(replica as PositionalReplicaWrapper);
            }

            this.removeReplica.Invoke(this, entity);
        }

        /// <summary>
        /// Check whether the scene contains an original spatial entity using a particular unique ID.
        /// </summary>
        /// <param name="id">Unique ID to check.</param>
        /// <returns>True if an entity was found using the particular ID, otherwise false.</returns>
        internal bool ContainsOriginalEntity(BadumnaId id)
        {
            return this.originals.ContainsKey(id);
        }

        /// <summary>
        /// Check whether the scene contains any original spatial entities using a listed unique ID.
        /// </summary>
        /// <param name="ids">List of unique IDs to check.</param>
        /// <returns>True if any entities were found using a listed unique ID, otherwise false.</returns>
        internal bool ContainsOriginalEntities(List<BadumnaId> ids)
        {
            foreach (BadumnaId id in ids)
            {
                if (this.ContainsOriginalEntity(id))
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Derived specialization of the leave scene implementation.
        /// </summary>
        protected abstract void LeaveImplimentation();

        /// <summary>
        /// Registers an entity in the scene.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="entityType">Type of the entity.</param>
        protected abstract void RegisterEntityImplementation(ISpatialOriginal entity, uint entityType);

        /// <summary>
        /// Unregisters the entity from the scene.
        /// </summary>
        /// <param name="entity">The entity.</param>
        protected abstract void UnregisterEntityImplementation(ISpatialOriginal entity);

        /// <summary>
        /// Check the entity radius an AOI are within limits.
        /// </summary>
        /// <param name="entity">The entity.</param>
        protected virtual void ValidateInterestRadius(ISpatialOriginal entity)
        {
        }
    }
}