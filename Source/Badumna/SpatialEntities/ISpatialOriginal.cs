﻿//-----------------------------------------------------------------------
// <copyright file="ISpatialOriginal.cs" company="NICTA">
//     Copyright (c) 2009 NICTA. All right reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.SpatialEntities
{
    using System;
    using System.IO;

    using Badumna.Replication;
    using Badumna.Utilities;

    /// <summary>
    /// An original spatial entity.  Original entities are replicated to interested peers.
    /// </summary>
    public interface ISpatialOriginal : ISpatialEntity, IEntity
    {
        /// <summary>
        /// Serialize the requiredParts of the entity on to the stream.
        /// </summary>
        /// <param name="requiredParts">Indicates which parts should be serialized.  Exactly these parts must be written to the stream.</param>
        /// <param name="stream">The stream to serialize to.  This stream should not be closed.</param>
        void Serialize(BooleanArray requiredParts, Stream stream);
    }

    /// <summary>
    /// Indicates that the Serialize method should only be called with one user part (>= SpatialEntityStateSegment.FirstAvailableSegment)
    /// per call.
    /// </summary>
    /// <remarks>
    /// This is mostly a hack to get the tunnel working.  On the tunnel client we request each
    /// changed user part individually and forward them to the tunnel server.  This is required
    /// so that if Badumna needs to request a segment for resend from the tunnel server we can
    /// provide it.  However, Badumna will also request combinations of parts when multiple parts
    /// have been flagged for update at once.  On the tunnel server we have no information about
    /// how the client application would package multiple parts into one update (e.g. what order
    /// they would be packed in), so we have to send each part in a separate update.
    /// </remarks>
    [AttributeUsage(AttributeTargets.Method)]
    internal class OnePartPerCallAttribute : Attribute
    {
    }
}
