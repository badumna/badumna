//-----------------------------------------------------------------------
// <copyright file="IDeadReckonable.cs" company="NICTA">
//     Copyright (c) 2008 NICTA. All right reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.SpatialEntities
{
    using Badumna.DataTypes;

    /// <summary>
    /// Indicates the implemented entity should have its position extrapolated and smoothed between updates.
    /// </summary>
    /// <remarks>
    /// It is important that all copies (remote and local) of an entity implement this interface if any of them do.
    /// Dead reckonable entity are ISpatialEntity%s that have their position changes applied over a period of time
    /// as opposed to once only when an update arrives. This smoothing helps to make remote entity's positional changes
    /// appear more natural as well as attempting to compensate for the effects of latency and jitter.
    /// The Velocity property should be set on the peer that is responsible for the entity. 
    /// The AttemptMovement() method will be called on the remote copies of the entity inside NetworkFacade.ProcessNetworkState(). The 
    /// method should check that the proposed movement is valid (i.e. check for collisions etc.) and set the position accordingly.
    /// </remarks>
    public interface IDeadReckonable : ISpatialEntity
    {
        /// <summary>
        /// Gets or sets the known velocity of the network entity.
        /// </summary>
        /// <remarks>
        /// This only needs to be set on the peer responsible for the entity.
        /// </remarks>
        Vector3 Velocity { get; set; }

        /// <summary>
        /// This method is called on remote copies inside the NetworkFacade.ProcessNetworkState() method.
        /// The reckonedPosition is the current estimated position of the controlling entity.
        /// The implementation of this method should check that the new position is valid (i.e. check for collisions etc) and apply it if so. 
        /// </summary>
        /// <param name="reckonedPosition">The estimated position of the entity</param>
        void AttemptMovement(Vector3 reckonedPosition);
    }
}
