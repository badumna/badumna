//-----------------------------------------------------------------------
// <copyright file="DeadReckoningHelper.cs" company="NICTA">
//     Copyright (c) 2008 NICTA. All right reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.SpatialEntities
{
    using System;
    using System.Collections.Generic;

    using Badumna.Core;
    using Badumna.DataTypes;
    using Badumna.Utilities;

    /// <summary>
    /// Dead reckoning helper class.
    /// </summary>
    internal class DeadReckoningHelper
    {
        /// <summary>
        /// Map of unique IDs to lists of dead reckoning data.
        /// </summary>
        private Dictionary<BadumnaId, List<ReckoningData>> deadReckonableEntities = new Dictionary<BadumnaId, List<ReckoningData>>();

        /// <summary>
        /// Keeps track of elapsed time.
        /// </summary>
        private TimeKeeper stopWatch = new TimeKeeper();

        /// <summary>
        /// Reset the helper.
        /// </summary>
        public void Reset()
        {
            this.stopWatch.Reset();
            this.deadReckonableEntities.Clear();
        }

        /// <summary>
        /// Perform dead reckoning on asscoiated dead reckonable entities.
        /// </summary>
        public void PerformDeadReckoning()
        {
            if (this.deadReckonableEntities.Count == 0)
            {
                return;
            }

            if (!this.stopWatch.IsRunning)
            {
                this.stopWatch.Start();
                return;
            }

            long timeSliceMilliseonds = (long)this.stopWatch.Elapsed.TotalMilliseconds;
            if (timeSliceMilliseonds == 0)
            {
                return;
            }

            this.stopWatch.Reset();
            this.stopWatch.Start();

            lock (this.deadReckonableEntities)
            {
                foreach (List<ReckoningData> reckonableList in this.deadReckonableEntities.Values)
                {
                    foreach (ReckoningData data in reckonableList)
                    {
                        data.AttemptMovement(timeSliceMilliseonds);
                    }
                }
            }
        }

        /// <summary>
        /// Get the destination of the dead reckonable entity.
        /// </summary>
        /// <param name="deadReckonable">The dead reckonable entity.</param>
        /// <returns>Destination of the entity.</returns>
        public Vector3 GetDestination(IDeadReckonable deadReckonable)
        {
            ReckoningData data = this.FindReckoningData(deadReckonable);
            if (data == null)
            {
                throw new InvalidOperationException("Specified dead reckonable is not registered");
            }

            return data.Destination;
        }

        /// <summary>
        /// Snap dead reckonable entity to its destination.
        /// </summary>
        /// <param name="deadReckonable">Dead reckonable entity.</param>
        public void SnapToDestination(IDeadReckonable deadReckonable)
        {
            ReckoningData data = this.FindReckoningData(deadReckonable);
            if (data == null)
            {
                throw new InvalidOperationException("Specified dead reckonable is not registered");
            }

            data.SnapToDestination();
        }

        /// <summary>
        /// Update the position and velocity of a particular entity.
        /// </summary>
        /// <param name="entity">Dead reckonable entity.</param>
        /// <param name="newPosition">Desired position.</param>
        /// <param name="newVelocity">Desired velocity.</param>
        /// <param name="estimatedDelayMilliseconds">Estimated time since serialization.</param>
        internal void UpdateApplied(IDeadReckonable entity, Vector3 newPosition, Vector3 newVelocity, int estimatedDelayMilliseconds)
        {
            if (entity == null)
            {
                return;
            }

            lock (this.deadReckonableEntities)
            {
                Vector3 predictedCurrentRemotePosition = newPosition; // +(newVelocity * (float)TimeSpan.FromMilliseconds(estimatedDelayMilliseconds).TotalSeconds);

                this.EnsureAdded(entity, predictedCurrentRemotePosition, newVelocity);

                foreach (ReckoningData data in this.deadReckonableEntities[entity.Guid])
                {
                    data.SetDestination(predictedCurrentRemotePosition, newVelocity);
                }
            }
        }

        /// <summary>
        /// Remove dead reckonable entity from list.
        /// </summary>
        /// <param name="reckonable">Dead reckonable entity to remove.</param>
        internal void Remove(IDeadReckonable reckonable)
        {
            lock (this.deadReckonableEntities)
            {
                List<ReckoningData> reckonableDataList;

                if (!this.deadReckonableEntities.TryGetValue(reckonable.Guid, out reckonableDataList))
                {
                    return;
                }

                reckonableDataList.RemoveAll(delegate(ReckoningData data) { return data.ReckonableEntity == reckonable; });

                if (reckonableDataList.Count == 0)
                {
                    this.deadReckonableEntities.Remove(reckonable.Guid);
                }
            }
        }

        /// <summary>
        /// Get dead reckoning data for entity.
        /// </summary>
        /// <param name="deadReckonable">Dead reckonable entity.</param>
        /// <returns>Dead reckoning data.</returns>
        private ReckoningData FindReckoningData(IDeadReckonable deadReckonable)
        {
            List<ReckoningData> reckonableList;
            if (!this.deadReckonableEntities.TryGetValue(deadReckonable.Guid, out reckonableList))
            {
                return null;
            }

            return reckonableList.Find(data => data.ReckonableEntity == deadReckonable);
        }

        /// <summary>
        /// Makes sure that dead reckonable entity has already been added.
        /// </summary>
        /// <param name="reckonable">Dead reckonable entity to check.</param>
        /// <param name="initialPosition">Initial position to set entity.</param>
        /// <param name="initialVelocity">Initial velocity to set entity.</param>
        /// <remarks>
        /// Returns true if we had to add it
        /// </remarks>
        private void EnsureAdded(IDeadReckonable reckonable, Vector3 initialPosition, Vector3 initialVelocity)
        {
            lock (this.deadReckonableEntities)
            {
                List<ReckoningData> reckonableDataList;

                if (!this.deadReckonableEntities.TryGetValue(reckonable.Guid, out reckonableDataList))
                {
                    reckonableDataList = new List<ReckoningData>();
                    this.deadReckonableEntities[reckonable.Guid] = reckonableDataList;
                }

                if (reckonableDataList.Exists(delegate(ReckoningData data) { return data.ReckonableEntity == reckonable; }))
                {
                    return;
                }

                reckonable.Position = initialPosition;
                reckonable.Velocity = initialVelocity;
                reckonableDataList.Add(new ReckoningData(reckonable));
            }
        }

        /// <summary>
        /// Class containing data used for performing dead reckoning on entities.
        /// </summary>
        private class ReckoningData
        {
            /// <summary>
            /// Dead reckonable entity.
            /// </summary>
            private IDeadReckonable reckonableEntity;

            /// <summary>
            /// The time remaing for which we apply smoothing
            /// </summary>
            private long millisecondsOfAdjustmentRemaining;

            /// <summary>
            /// The position of the entity as given in the latest update
            /// </summary>
            private Vector3 destination = new Vector3();

            /// <summary>
            /// The velocity of the entity as given in the latest update
            /// </summary>
            private Vector3 velocity = new Vector3(0, 0, 0);

            /// <summary>
            /// The velocity change required to move the entity to the desired position
            /// </summary>
            private Vector3 velocityAdjustment = new Vector3(0, 0, 0);

            /// <summary>
            /// Dead reckoned position to move the entity to.
            /// </summary>
            private Vector3 reckonedPosition;

            /// <summary>
            /// Initializes a new instance of the ReckoningData class.
            /// </summary>
            /// <param name="reckonableEntity">Entity to perform dead reckoning on.</param>
            public ReckoningData(IDeadReckonable reckonableEntity)
            {
                this.reckonableEntity = reckonableEntity;
                this.reckonedPosition = reckonableEntity.Position;
            }

            /// <summary>
            /// Gets the dead reckonable entity.
            /// </summary>
            public IDeadReckonable ReckonableEntity
            {
                get { return this.reckonableEntity; }
            }

            /// <summary>
            /// Gets the position of the entity as given in the latest update.
            /// </summary>
            public Vector3 Destination
            {
                get { return this.destination; }
            }

            /// <summary>
            /// Set the destination of the dead reckonable entity.
            /// </summary>
            /// <param name="currentPosition">Position to set the entity to.</param>
            /// <param name="velocity">Velocity of the entity.</param>
            public void SetDestination(Vector3 currentPosition, Vector3 velocity)
            {
                this.destination = currentPosition;
                Vector3 displacement = currentPosition - this.reckonableEntity.Position;
                this.velocity = velocity;
              ////  this.mReckonedPosition = currentPosition;
                
              /*  if (velocity.Magnitude <= 0.00001f
                    && displacement.Magnitude < this.mReckonableObject.AreaOfInterestRadius / 2.0) // TODO : Should be removed or configurable.
                {
                    // Don't perform any smoothing if the entity is static
                    this.mMillisecondsOfAdjustmentRemaining = 0;
                }
                else*/
                {
                    // Smooth over the estimated time till the next update
                    this.millisecondsOfAdjustmentRemaining = (long)Parameters.MaximumUpdateRate.TotalMilliseconds;
                    this.velocityAdjustment = displacement / (float)Parameters.MaximumUpdateRate.TotalSeconds;
                }
            }

            /// <summary>
            /// Snap dead reckoning entity to destination.
            /// </summary>
            public void SnapToDestination()
            {
                if (this.millisecondsOfAdjustmentRemaining <= 0)
                {
                    return;
                }

                this.millisecondsOfAdjustmentRemaining = 0;
                this.reckonedPosition = this.destination;
                this.reckonableEntity.Velocity = this.velocity;
                this.reckonableEntity.AttemptMovement(this.reckonedPosition);
            }

            /// <summary>
            /// Calculate the movement of the entity.
            /// </summary>
            /// <param name="millisecondsDelay">Time duration.</param>
            public void AttemptMovement(long millisecondsDelay)
            {
                if (this.millisecondsOfAdjustmentRemaining == 0)
                {
                    // Smoothing is complete or not required 
                    this.reckonableEntity.Velocity = this.velocity;
                    this.reckonedPosition += this.velocity * (float)TimeSpan.FromMilliseconds(millisecondsDelay).TotalSeconds;
                    this.reckonableEntity.AttemptMovement(this.reckonedPosition);
                    return;
                }

                Vector3 adjustment = this.velocityAdjustment;

                if (millisecondsDelay > this.millisecondsOfAdjustmentRemaining)
                {
                    // The amount of smoothing time remaining is a fraction of the given delay
                    adjustment *= (float)this.millisecondsOfAdjustmentRemaining / (float)millisecondsDelay;
                    this.millisecondsOfAdjustmentRemaining = 0;
                }
                else
                {
                    this.millisecondsOfAdjustmentRemaining -= millisecondsDelay;
                }

                this.reckonableEntity.Velocity = this.velocityAdjustment;
                this.reckonedPosition += adjustment * (float)TimeSpan.FromMilliseconds(millisecondsDelay).TotalSeconds;
                this.reckonableEntity.AttemptMovement(this.reckonedPosition);
            }
        }
    }
}
