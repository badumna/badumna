﻿//-----------------------------------------------------------------------
// <copyright file="ISpatialEntity.cs" company="NICTA">
//     Copyright (c) 2009 NICTA. All right reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.SpatialEntities
{
    using Badumna.DataTypes;
    using Badumna.Replication;

    /// <summary>
    /// Names for the enity state segment indexes used by the spatial entity system.
    /// When spatial entities are defined they should start numbering state segments
    /// from FirstUserSegment onwards (inclusive).
    /// </summary>
    public enum SpatialEntityStateSegment
    {
        /// <summary>
        /// Index for state segment holding the scene name
        /// </summary>
        Scene,

        /// <summary>
        /// Index for state segment holding the position
        /// </summary>
        Position,

        /// <summary>
        /// Index for state segment holding the velocity (Only used for IDeadReckonable entities)
        /// </summary>
        Velocity,  // Only applicable for dead reckonables

        /// <summary>
        /// Index for state segment holding the radius
        /// </summary>
        Radius,

        /// <summary>
        /// Index for state segment holding the interest radius
        /// </summary>
        InterestRadius,

        // TODO: Move match related indices elsewhere!

        /// <summary>
        /// Index for state segment holding the match id.
        /// </summary>
        Match,

        /// <summary>
        /// Index of the first unused state segment index. 
        /// </summary>
        FirstAvailableSegment
    }

    /// <summary>
    /// A collection of properties common to spatial entities.
    /// </summary>
    public interface ISpatialEntity : IEntity, IReplicableEntity
    {
        /// <summary>
        /// Gets or sets the radius of the entity's bounding sphere.
        /// </summary>
        /// <remarks>
        /// This entity will send updates to all other entities that have a sphere of interest
        /// that intersects this entity's bounding sphere.  The bounding sphere is described
        /// by Position and Radius.
        /// </remarks>
        /// <seealso cref="AreaOfInterestRadius"/>
        float Radius { get; set; }

        /// <summary>
        /// Gets or sets the radius of the entity's sphere of interest.
        /// </summary>
        /// <remarks>
        /// The sphere of interest is described by Position and AreaOfInterestRadius.
        /// </remarks>
        /// <seealso cref="Radius"/>
        float AreaOfInterestRadius { get; set; }
    }
}
