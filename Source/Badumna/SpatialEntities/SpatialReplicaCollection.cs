﻿//-----------------------------------------------------------------------
// <copyright file="SpatialReplicaCollection.cs" company="NICTA">
//     Copyright (c) 2009 NICTA. All right reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.SpatialEntities
{
    using System.Collections.Generic;
    using System.IO;

    using Badumna.Core;
    using Badumna.DataTypes;
    using Badumna.Replication;
    using Badumna.Utilities;

    /// <summary>
    /// from replication module's point of view, this is just a IReplica which means it represents
    /// an original entity's replica.
    /// </summary>
    internal class SpatialReplicaCollection : IReplica, ISynchronizable
    {
        /// <summary>
        /// The last value of attention weight calculated.
        /// </summary>
        private double lastAttention = 0.1;  // Assume some attention initially, otherwise we wouldn't have been created

        /// <summary>
        /// Spatial entity manager.
        /// </summary>
        private SpatialEntityManager spatialEntityManager;

        /// <summary>
        /// The type of the entities.
        /// </summary>
        private uint entityType;

        /// <summary>
        /// The centroid of the spatial replicas in the collection.
        /// </summary>
        private Vector3 position;

        /// <summary>
        /// The velocity of the spatial replicas in the collection.
        /// </summary>
        private Vector3 velocity;

        /// <summary>
        /// The radius of the bounding sphere of the spatial replicas in the collection.
        /// </summary>
        private float radius;

        /// <summary>
        /// The radius of the area of interest of the spatial replicas in the collection.
        /// </summary>
        private float interestRadius;

        /// <summary>
        /// Name of the joined scene.
        /// </summary>
        private QualifiedName sceneName;

        /// <summary>
        /// This dictionary contains instances of ISpatialReplica created by the client application.  Each NetworkScene
        /// in the key set represents the same named scene (which matches mSceneName), but may have different delegates
        /// for creating and removing replicas.  The standard case for this is where a client peer runs both application
        /// code and distributed controller code.  These pieces of code are conceptually distinct (i.e. they shouldn't
        /// interact directly with each other).  As such, the application code will specify create/remove delegates
        /// that create instances of application classes (e.g. avatars with 3D models) whereas the distributed controller
        /// code could join the same scene and specify create/remove delegates that create simple representations of the
        /// same entities.
        /// The ISpatialReplica entity stored in the following dict is created by the CreateSpatialReplica delegate registered
        /// with its NetworkScene key. 
        /// NetworkScenes in the following dictionary has the same name, but different create/remove delegates.
        /// </summary>
        private Dictionary<NetworkScene, ISpatialReplica> spatialReplicas = new Dictionary<NetworkScene, ISpatialReplica>();

        /// <summary>
        /// The queue to use for scheduling events.
        /// </summary>
        private NetworkEventQueue eventQueue;

        /// <summary>
        /// The method to use to queue events that must run in the application's thread.
        /// </summary>
        private QueueInvokable queueApplicationEvent;

        /// <summary>
        /// Initializes a new instance of the SpatialReplicaCollection class.
        /// </summary>
        /// <param name="spatialEntityManager">Spatial entity manager.</param>
        /// <param name="entityId">Unique ID of the spatial replicas.</param>
        /// <param name="entityType">Entity type of the spatial replicas.</param>
        /// <param name="eventQueue">The event queue to schedule events on.</param>
        /// <param name="queueApplicationEvent">The method to use to queue events that must run in the application's thread.</param>
        public SpatialReplicaCollection(
            SpatialEntityManager spatialEntityManager,
            BadumnaId entityId,
            uint entityType,
            NetworkEventQueue eventQueue,
            QueueInvokable queueApplicationEvent)
        {
            this.eventQueue = eventQueue;
            this.queueApplicationEvent = queueApplicationEvent;
            this.spatialEntityManager = spatialEntityManager;
            this.Guid = entityId;
            this.entityType = entityType;
        }

        /// <summary>
        /// Gets or sets the unique identifier of the spatial replicas in the collection.
        /// </summary>
        public BadumnaId Guid
        {
            get; set;
        }

        /// <summary>
        /// Gets the centroid of the spatial replicas in the collection.
        /// </summary>
        public Vector3 Position
        {
            get { return this.position; }
        }

        /// <summary>
        /// Gets the radius of the bounding sphere of the spatial replicas in the collection.
        /// </summary>
        public float Radius
        {
            get { return this.radius; }
        }

        /// <summary>
        /// Gets the unique name of the joined scene.
        /// </summary>
        public QualifiedName SceneName
        {
            get { return this.sceneName; }
        }
        
        /// <summary>
        /// Gets or sets the last calculated attention weight.
        /// </summary>
        public double LastAttention 
        {
            get { return this.lastAttention; }
            set { this.lastAttention = value; }
        }

        /// <summary>
        /// Gets the total number of spatial replicas.
        /// </summary>
        public int Count
        {
            get { return this.spatialReplicas.Count; }
        }
        
        /// <summary>
        /// Add spatial replica to a network scene.
        /// </summary>
        /// <param name="scene">Network scene to add to.</param>
        public void AddToScene(NetworkScene scene)
        {
            this.eventQueue.AssertIsApplicationThread();

            if (this.spatialReplicas.ContainsKey(scene))
            {
                return;
            }

            // this will also keep the replica in the scene object. 
            ISpatialReplica spatialReplica = scene.InstantiateReplica(this.Guid, this.entityType);

            if (spatialReplica == null)
            {
                // Application has apparently chosen not to create a spatial replica for this entity.
                return;
            }

            spatialReplica.Guid = this.Guid;
            Logger.TraceInformation(LogTag.SpatialEntities, "Application has created entity {0}", this.Guid);

            this.spatialReplicas[scene] = spatialReplica;
        }

        /// <summary>
        /// Remove spatial replica from a network scene.
        /// </summary>
        /// <param name="scene">Network scene to remove from.</param>
        public void RemoveFromScene(NetworkScene scene)
        {
            ISpatialReplica spatialReplica;
            if (this.spatialReplicas.TryGetValue(scene, out spatialReplica))
            {
                this.queueApplicationEvent(Apply.Func(delegate { scene.RemoveReplica(spatialReplica); }));
                this.spatialReplicas.Remove(scene);

                if (spatialReplica is IDeadReckonable)
                {
                    this.spatialEntityManager.DeadReckoningHelper.Remove((IDeadReckonable)spatialReplica);
                }
            }
        }

        /// <summary>
        /// Remove spatial replica from all network scenes.
        /// </summary>
        public void RemoveFromAllScenes()
        {
            foreach (KeyValuePair<NetworkScene, ISpatialReplica> replica in this.spatialReplicas)
            {
                NetworkScene scene = replica.Key;
                ISpatialReplica spatialReplica = replica.Value;
                this.queueApplicationEvent(Apply.Func(delegate { scene.RemoveReplica(spatialReplica); }));

                if (replica.Value is IDeadReckonable)
                {
                    this.spatialEntityManager.DeadReckoningHelper.Remove((IDeadReckonable)replica.Value);
                }
            }

            this.spatialReplicas.Clear();
        }

        /// <inheritdoc />
        public void Synchronize(object obj)
        {
            if (null == obj)
            {
                return;
            }

            if (obj is List<BadumnaId>)
            {
                List<BadumnaId> list = (List<BadumnaId>)obj;
                List<NetworkScene> toRemove = new List<NetworkScene>();

                if (null == list || list.Count == 0)
                {
                    return;
                }

                foreach (KeyValuePair<NetworkScene, ISpatialReplica> kvp in this.spatialReplicas)
                {
                    if (!kvp.Key.ContainsOriginalEntities(list))
                    {
                        toRemove.Add(kvp.Key);
                    }
                }

                foreach (NetworkScene sc in toRemove)
                {
                    this.RemoveFromScene(sc);
                }
            }
        }

        /// <summary>
        /// Deserialize entity states from the given memory stream.
        /// </summary>
        /// <param name="includedParts">List of parts included in the serialization.</param>
        /// <param name="stream">Memory stream containing the serialized data.</param>
        /// <param name="estimatedMillisecondsSinceDeparture">Estimated time since serialization.</param>
        /// <param name="id">Unique ID of the entity.</param>
        /// <returns>Attention weight.</returns>
        public double Deserialize(BooleanArray includedParts, Stream stream, int estimatedMillisecondsSinceDeparture, BadumnaId id)
        {
            this.eventQueue.AssertIsApplicationThread();

            using (BinaryReader reader = new BinaryReader(stream))
            {
                //// Scenes are processed first so we can add / remove ISpatialReplicas as required before applying the
                //// rest of the update.

                //// Consider there are existing players in the flatchat app, when it JoinScene, a NetworkScene object
                //// is created with the scene name and create/remove replica delegates. When the avater intersect with
                //// others, they will be notified and start sending their updates. The avater will realise it need to
                //// instantiate the replica and it will call the create replica provided by SpatialEntityManager to
                //// create an IReplica which is actually a SpatialReplicaCollection. At this moment, it will be empty
                //// and no details regarding the scene name. When the full update comes in, it deserializes it and the first
                //// part it update will be the scene, thus it calls UpdateScene. This gives the IReplica the chance to call 
                //// AddToScene which will in turn call the CreateSpatialReplica delegate specified by the game app to create 
                //// the ISpatialReplica. Created ISpatialReplica would be kept in mSpatialReplicas, all of them will then be
                //// deserialized in the following loop in this method. 

                if (includedParts[(int)SpatialEntityStateSegment.Scene])
                {
                    this.UpdateScene(new QualifiedName(reader.ReadString()), id);
                }
                
                if (includedParts[(int)SpatialEntityStateSegment.Position])
                {
                    this.position.X = reader.ReadSingle();
                    this.position.Y = reader.ReadSingle();
                    this.position.Z = reader.ReadSingle();
                }

                if (includedParts[(int)SpatialEntityStateSegment.Velocity])
                {
                    this.velocity.X = reader.ReadSingle();
                    this.velocity.Y = reader.ReadSingle();
                    this.velocity.Z = reader.ReadSingle();
                }

                if (includedParts[(int)SpatialEntityStateSegment.Radius])
                {
                    this.radius = reader.ReadSingle();
                }

                if (includedParts[(int)SpatialEntityStateSegment.InterestRadius])
                {
                    this.interestRadius = reader.ReadSingle();
                }

                long position = stream.Position;
                foreach (ISpatialReplica spatialReplica in this.spatialReplicas.Values)
                {
                    if (includedParts[(int)SpatialEntityStateSegment.Position])
                    {
                        if (spatialReplica is IDeadReckonable)
                        {
                            // Position and Velocity are always serialized together for dead reckonables.
                            this.spatialEntityManager.DeadReckoningHelper.UpdateApplied(
                                (IDeadReckonable)spatialReplica,
                                this.position,
                                this.velocity,
                                estimatedMillisecondsSinceDeparture);
                        }
                        else
                        {
                            spatialReplica.Position = this.position;
                        }
                    }

                    if (includedParts[(int)SpatialEntityStateSegment.Radius])
                    {
                        spatialReplica.Radius = this.radius;
                    }

                    if (includedParts[(int)SpatialEntityStateSegment.InterestRadius])
                    {
                        spatialReplica.AreaOfInterestRadius = this.interestRadius;
                    }

                    spatialReplica.Deserialize(includedParts, stream, estimatedMillisecondsSinceDeparture);
                    stream.Position = position;
                }

                this.lastAttention = this.spatialEntityManager.CalculateAttention(this);
                this.spatialEntityManager.DetectSeparation(this);
                return this.lastAttention;
            }
        }

        /// <summary>
        /// Handle incoming messages.
        /// </summary>
        /// <param name="stream">Badumna stream.</param>
        public void HandleEvent(Stream stream)
        {
            long position = stream.Position;
            foreach (ISpatialReplica spatialReplica in this.spatialReplicas.Values)
            {
                spatialReplica.HandleEvent(stream);
                stream.Position = position;
            }
        }

        /// <summary>
        /// Update the network scene that the spatial entity is in.
        /// </summary>
        /// <param name="newSceneName">Unique name of network scene.</param>
        /// <param name="id">Unique ID of spatial entity.</param>
        private void UpdateScene(QualifiedName newSceneName, BadumnaId id)
        {
            this.eventQueue.AssertIsApplicationThread();

            if (this.sceneName != newSceneName)
            {
                this.RemoveFromAllScenes();
                this.sceneName = newSceneName;
            }

            // return a list of scenes with the same name but possiblely different delegates for 
            // creating/removing replicas. 
            List<NetworkScene> newScenes = this.spatialEntityManager.TryGetScenes(newSceneName);
            if (newScenes != null)
            {
                foreach (NetworkScene scene in newScenes)
                {
                    // add to each of these scene
                    if (scene.ContainsOriginalEntity(id))
                    {
                        this.AddToScene(scene);
                    }
                }
            }
        }
    }
}
