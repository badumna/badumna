﻿//-----------------------------------------------------------------------
// <copyright file="ISpatialReplica.cs" company="NICTA">
//     Copyright (c) 2009 NICTA. All right reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.SpatialEntities
{
    using System.IO;

    using Badumna.Replication;
    using Badumna.Utilities;

    /// <summary>
    /// A replicated entity with position and size.
    /// </summary>
    public interface ISpatialReplica : ISpatialEntity, IEntity
    {
        /// <summary>
        /// Deserialize the spatial entity state from the given stream.
        /// </summary>
        /// <remarks>
        /// The data in stream is the same as was written by the original spatial entity in
        /// ISpatialOriginal.Serialize(...).  includedParts specifies which sections are included in
        /// the stream.  State changes are guaranteed to be replicated eventually, however intermediate
        /// states may be skipped (i.e. if a particular piece of state changes twice quickly, the first
        /// state change may not make it to all (any) replicas).  Properties defined on ISpatialEntity
        /// (e.g. Position) are serialized/deserialized automatically and will be updated prior to
        /// this method being called (these properties still need to be flagged for update when they change).
        /// </remarks>
        /// <param name="includedParts">List of parts included in the serialization.</param>
        /// <param name="stream">Memory stream containing the serialized data.</param>
        /// <param name="estimatedMillisecondsSinceDeparture">Estimated time since serialization.</param>
        void Deserialize(BooleanArray includedParts, Stream stream, int estimatedMillisecondsSinceDeparture);
    }
}
