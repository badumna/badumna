﻿//-----------------------------------------------------------------------
// <copyright file="SpatialOriginalWrapper.cs" company="NICTA">
//     Copyright (c) 2009 NICTA. All right reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.SpatialEntities
{
    using System;
    using System.IO;
    using System.Reflection;

    using Badumna.Core;
    using Badumna.DataTypes;
    using Badumna.InterestManagement;
    using Badumna.Replication;
    using Badumna.Utilities;

    /// <summary>
    /// Wrapper class that for spatial originals that manages interactions
    /// with interest management and replication.
    /// </summary>
    internal class SpatialOriginalWrapper : IOriginal
    {
        /// <summary>
        /// Indicates that the Serialize method should only be called with one user part per call.
        /// </summary>
        private bool requiresOnePartPerCall;

        /// <summary>
        /// Backing field for SpatialOriginal.  Don't use this directly, use this.SpatialOriginal.
        /// </summary>
        private ISpatialOriginal spatialOriginal;

        /// <summary>
        /// Type of the entity.
        /// </summary>
        private uint entityType;

        /// <summary>
        /// Flag indicating whether the spatial original is derived from IDeadReckonable.
        /// </summary>
        private bool isDeadReckonable;

        /// <summary>
        /// Interst management service.
        /// </summary>
        private RegionManager interestManager;

        /// <summary>
        /// Event managerment service.
        /// </summary>
        private RegionManager eventManager;

        /// <summary>
        /// The interest management service.
        /// </summary>
        private IInterestManagementService interestManagementService;

        /// <summary>
        /// The entity manager.
        /// </summary>
        private IEntityManager entityManager;

        /// <summary>
        /// The network scene.
        /// </summary>
        private NetworkScene scene;

        /// <summary>
        /// The queue to use for scheduling events.
        /// </summary>
        private NetworkEventQueue eventQueue;
        
        /// <summary>
        /// Provides access to the current time.
        /// </summary>
        private ITime timeKeeper;

        /// <summary>
        /// Reports on network connectivity status.
        /// </summary>
        private INetworkConnectivityReporter connectivityReporter;

        /// <summary>
        /// A delegate that will generate a unique BadumnaId associated with this peer.
        /// </summary>
        private GenericCallBackReturn<BadumnaId, PeerAddress> generateBadumnaIdWithAddress;

        /// <summary>
        /// A value indicating whether the interest and event region has been inserted into Interest Management system.
        /// </summary>
        /// <remarks>
        /// Static interest and event region should only be used for mini scene. 
        /// </remarks>
        private bool hasStaticRegionBeenInserted = false;

        /// <summary>
        /// Initializes a new instance of the SpatialOriginalWrapper class.
        /// </summary>
        /// <param name="spatialOriginal">Spatial original.</param>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="scene">Network scene.</param>
        /// <param name="interestManagementService">The interest management service.</param>
        /// <param name="entityManager">Entity manager.</param>
        /// <param name="eventQueue">The event queue to schedule events on.</param>
        /// <param name="timeKeeper">The time source.</param>
        /// <param name="connectivityReporter">Reports on network connectivity status.</param>
        /// <param name="generateBadumnaIdWithAddress">A delegate that will generate a unique BadumnaId associated with the given address.</param>
        public SpatialOriginalWrapper(
            ISpatialOriginal spatialOriginal,
            uint entityType,
            NetworkScene scene,
            IInterestManagementService interestManagementService,
            IEntityManager entityManager,
            NetworkEventQueue eventQueue,
            ITime timeKeeper,
            INetworkConnectivityReporter connectivityReporter,
            GenericCallBackReturn<BadumnaId, PeerAddress> generateBadumnaIdWithAddress)
        {
            this.eventQueue = eventQueue;
            this.timeKeeper = timeKeeper;
            this.connectivityReporter = connectivityReporter;
            this.SpatialOriginal = spatialOriginal;
            this.isDeadReckonable = spatialOriginal is IDeadReckonable;
            this.entityType = entityType;
            this.interestManagementService = interestManagementService;
            this.entityManager = entityManager;
            this.generateBadumnaIdWithAddress = generateBadumnaIdWithAddress;

            this.ChangeScene(scene);

            this.connectivityReporter.NetworkOnlineEvent += this.CheckImsRegions;
        }

        /// <summary>
        /// Gets or sets the unique identifier of the spatial original.
        /// </summary>
        public BadumnaId Guid
        {
            get { return this.SpatialOriginal.Guid; }
            set { this.SpatialOriginal.Guid = value; }
        }

        /// <summary>
        /// Gets the centroid of the spatial original.
        /// </summary>
        public Vector3 Position
        {
            get { return this.SpatialOriginal.Position; }
        }

        /// <summary>
        /// Gets the interest management region.
        /// </summary>
        public ImsRegion InterestRegion
        {
            get
            {
                return this.interestManager != null ? this.interestManager.Region : null;
            }
        }

        /// <summary>
        /// Gets or sets the spatial original.
        /// </summary>
        public ISpatialOriginal SpatialOriginal
        {
            get
            {
                return this.spatialOriginal;
            }

            set
            {
                this.spatialOriginal = value;

                MethodInfo methodInfo = this.spatialOriginal.GetType().GetMethod("Serialize", new Type[] { typeof(BooleanArray), typeof(Stream) });  // Must succeed because 'value' is ISpatialOriginal
                this.requiresOnePartPerCall = methodInfo.GetCustomAttributes(typeof(OnePartPerCallAttribute), false).Length > 0;
            }
        }

        /// <summary>
        /// Gets the type of the entity.
        /// </summary>
        public uint EntityType
        {
            get { return this.entityType; }
        }

        /// <summary>
        /// Move entity to new scene.
        /// </summary>
        /// <param name="newScene">Network scene.</param>
        public void ChangeScene(NetworkScene newScene)
        {
            if (this.eventManager != null)
            {
                this.eventManager.ShutDown();
            }

            if (this.interestManager != null)
            {
                this.interestManager.ShutDown();
            }

            this.scene = newScene;

            this.hasStaticRegionBeenInserted = false;
            Vector3 position = new Vector3(0, 0, 0);
            float radius = 1.0f;
            float areaOfInterestRadius = 1.0f;

            if (!this.scene.MiniScene)
            {
                position = this.SpatialOriginal.Position;
                radius = this.SpatialOriginal.Radius;
                areaOfInterestRadius = this.SpatialOriginal.AreaOfInterestRadius;
            }

            if (radius > 0)
            {
                // TODO: When there is no interest manager, should the EventManager auto inflate?
                this.eventManager = new RegionManager(
                    newScene.QualifiedName,
                    this.interestManagementService,
                    new EventRegion(this.generateBadumnaIdWithAddress(this.SpatialOriginal.Guid.Address), position, radius, this.scene.QualifiedName),
                    Parameters.DefaultSubscriptionTTL,
                    this.eventQueue,
                    this.timeKeeper,
                    this.connectivityReporter);

                this.eventManager.IntersectionEvent += this.InterestNotfication;
            }
            else
            {
                Logger.TraceInformation(LogTag.SpatialEntities, "this.mSpatialOriginal.Radius <= 0, mEventManager is set to null.");
            }

            if (areaOfInterestRadius > 0)
            {
                this.interestManager = new RegionManager(
                    newScene.QualifiedName,
                    this.interestManagementService,
                    new InterestRegion(this.SpatialOriginal.Guid, position, areaOfInterestRadius, this.scene.QualifiedName),
                    Parameters.DefaultSubscriptionTTL,
                    this.eventQueue,
                    this.timeKeeper,
                    this.connectivityReporter);
            }
            else
            {
                Logger.TraceInformation(LogTag.SpatialEntities, "this.mSpatialOriginal.AreaOfInterestRadius <=0, this.mInterestManager is set to null.");
            }
        }

        /// <summary>
        /// Shut down.
        /// </summary>
        /// <param name="removeFromScene">Flag whether to remove entity from network scene.</param>
        public void Shutdown(bool removeFromScene)
        {
            if (null != this.eventManager)
            {
                this.eventManager.ShutDown(removeFromScene);
                this.eventManager = null;
            }

            if (null != this.interestManager)
            {
                this.interestManager.ShutDown(removeFromScene);
                this.interestManager = null;
            }

            this.connectivityReporter.NetworkOnlineEvent -= this.CheckImsRegions;
        }

        /// <summary>
        /// Mark parts for update.
        /// </summary>
        /// <param name="changedParts">List of parts to update.</param>
        public void MarkForUpdate(BooleanArray changedParts)
        {
            this.entityManager.MarkForUpdate(this, changedParts);
        }

        /// <summary>
        /// Mark parts for update.
        /// </summary>
        /// <param name="changedPartIndex">Index of part to update.</param>
        public void MarkForUpdate(int changedPartIndex)
        {
            this.entityManager.MarkForUpdate(this, changedPartIndex);
        }

        /// <summary>
        /// Check whether remote entity intersects interest region.
        /// </summary>
        /// <param name="sceneName">Unique name of network scene.</param>
        /// <param name="remoteEntityPosition">Position of remote entity.</param>
        /// <param name="remoteEntityRadius">Radius of remote entity.</param>
        /// <returns>True, if remote entity intersects interest region.</returns>
        public bool IsInterestedIn(QualifiedName sceneName, Vector3 remoteEntityPosition, float remoteEntityRadius)
        {
            if (this.scene.QualifiedName != sceneName)
            {
                return false;
            }

            // need to be consistent with the way how IMS detects intersections. 
            // inflated radius
            float inflatedRadius = this.SpatialOriginal.AreaOfInterestRadius + this.interestManagementService.SubscriptionInflation;
            //// the subscribed region is actually different from the physical position
            inflatedRadius = inflatedRadius + this.interestManagementService.SubscriptionInflation;

            float remoteInflatedRadius = remoteEntityRadius + this.interestManagementService.SubscriptionInflation;
            remoteInflatedRadius = remoteInflatedRadius + this.interestManagementService.SubscriptionInflation;

            return Geometry.SpheresIntersect(this.SpatialOriginal.Position, inflatedRadius, remoteEntityPosition, remoteInflatedRadius);
        }

        /// <summary>
        /// Check whether spatial entity is in a particular scene.
        /// </summary>
        /// <param name="sceneName">Unique name of scene.</param>
        /// <returns>True, if entity is in indicated scene.</returns>
        public bool IsInScene(QualifiedName sceneName)
        {
            return this.scene.QualifiedName == sceneName;
        }

        /// <summary>
        /// Check whether spatial entity is controlled by a particular scene.
        /// </summary>
        /// <param name="scene">Network scene.</param>
        /// <returns>True, if entity is controlled by indicated scene.</returns>
        public bool IsControlledByScene(NetworkScene scene)
        {
            return this.scene == scene;
        }

        /// <summary>
        /// Called when the peer is no longer interested in an entity.
        /// </summary>
        /// <param name="region">Uninterested entity.</param>
        public void PeerUninterestedFeedback(BadumnaId region)
        {
            Logger.TraceInformation(LogTag.SpatialEntities, "Seperation has been detected with peer {0}", region);
            this.eventManager.ForceRemovalOfRegion(region);
            this.entityManager.RemoveInterested(this, region);
        }

        /// <summary>
        /// Update interest management service regions.
        /// </summary>
        public void CheckImsRegions()
        {
            if (this.connectivityReporter.Status != ConnectivityStatus.Online)
            {
                return;
            }

            if (this.scene.MiniScene)
            {
                if (!this.hasStaticRegionBeenInserted)
                {
                    this.interestManager.ModifyRegion(this.interestManager.Region.Centroid, this.interestManager.Region.Radius);
                    this.eventManager.ModifyRegion(this.eventManager.Region.Centroid, this.eventManager.Region.Radius, this.entityManager.InterestedCount(this));
                    this.hasStaticRegionBeenInserted = true;
                }

                return;
            }

            if (null != this.interestManager)
            {
                this.interestManager.ModifyRegion(this.SpatialOriginal.Position, this.SpatialOriginal.AreaOfInterestRadius);
            }

            if (null != this.eventManager)
            {
                this.eventManager.ModifyRegion(this.SpatialOriginal.Position, this.SpatialOriginal.Radius, this.entityManager.InterestedCount(this));
            }
        }

        /// <summary>
        /// Serialize entity states to the given memory stream.
        /// </summary>
        /// <param name="requiredParts">List of parts to include in the serialization.</param>
        /// <param name="stream">Memory stream to hold the serialized data.</param>
        /// <returns>List of parts that haven't been serialized yet.</returns>
        public BooleanArray Serialize(BooleanArray requiredParts, Stream stream)
        {
            Logger.TraceInformation(LogTag.SpatialEntities, "SpatialOriginalWrapper.Serialize for original {0}", this.spatialOriginal.Guid);
            //// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            //// !!! If this code is changed then UpdateEntityRequest.SerializeUpdate and related code must also be reviewed. !!!
            //// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            //// TODO: Refactor to remove duplicated logic.

            if (this.isDeadReckonable)
            {
                // Position <=> Velocity
                requiredParts[(int)SpatialEntityStateSegment.Position] |= requiredParts[(int)SpatialEntityStateSegment.Velocity];
                requiredParts[(int)SpatialEntityStateSegment.Velocity] |= requiredParts[(int)SpatialEntityStateSegment.Position];
            }
            else
            {
                // Support for all flags true for inital update, etc without worrying about whether the entity actually has velocity or not
                requiredParts[(int)SpatialEntityStateSegment.Velocity] = false;
            }

            BooleanArray remainingParts = new BooleanArray();  // Will hold parts that need to be written but we're not writing this time around

            using (BinaryWriter writer = new BinaryWriter(stream))
            {
                if (requiredParts[(int)SpatialEntityStateSegment.Scene])
                {
                    writer.Write(this.scene.QualifiedName.ToString());
                }

                if (requiredParts[(int)SpatialEntityStateSegment.Position])
                {
                    writer.Write(this.SpatialOriginal.Position.X);
                    writer.Write(this.SpatialOriginal.Position.Y);
                    writer.Write(this.SpatialOriginal.Position.Z);
                }

                if (requiredParts[(int)SpatialEntityStateSegment.Velocity])
                {
                    writer.Write(((IDeadReckonable)this.SpatialOriginal).Velocity.X);
                    writer.Write(((IDeadReckonable)this.SpatialOriginal).Velocity.Y);
                    writer.Write(((IDeadReckonable)this.SpatialOriginal).Velocity.Z);
                }

                if (requiredParts[(int)SpatialEntityStateSegment.Radius])
                {
                    writer.Write(this.SpatialOriginal.Radius);
                }

                if (requiredParts[(int)SpatialEntityStateSegment.InterestRadius])
                {
                    writer.Write(this.SpatialOriginal.AreaOfInterestRadius);
                }

                if (this.requiresOnePartPerCall)
                {
                    // TODO: Make this more efficient by somehow not scanning over all indices.
                    for (int i = (int)SpatialEntityStateSegment.FirstAvailableSegment; i <= BooleanArray.MaxIndex; i++)
                    {
                        if (requiredParts[i])
                        {
                            remainingParts[i] = false;  // We're dealing with this part now

                            long oldPosition = stream.Position;
                            this.SpatialOriginal.Serialize(new BooleanArray(i), stream);
                            if (stream.Position > oldPosition)
                            {
                                // Actually wrote an update, good to go

                                // Clear any other user parts bits because we're not sending them in this update
                                // TODO: Implement a BooleanArray.And method so this is easier
                                for (int j = i + 1; j <= BooleanArray.MaxIndex; j++)
                                {
                                    if (requiredParts[j])
                                    {
                                        requiredParts[j] = false;
                                        remainingParts[j] = true;
                                    }
                                }

                                return remainingParts;  // These are the parts we haven't sent yet
                            }
                            else
                            {
                                // We're not sending this part at all because it's empty.  The flag is probably
                                // just set to true because we've been asked to serialize *everything* with
                                // requiredParts = new BooleanArray(true).
                                requiredParts[i] = false;  
                            }
                        }
                    }
                }
                else
                {
                    this.SpatialOriginal.Serialize(requiredParts, stream);
                }
            }

            this.CheckImsRegions();

            return remainingParts;
        }

        /// <summary>
        /// Handle incoming messages.
        /// </summary>
        /// <param name="stream">Badumna stream.</param>
        public void HandleEvent(Stream stream)
        {
            this.SpatialOriginal.HandleEvent(stream);
        }

        /// <summary>
        /// Called when an entity intersects the interest region.
        /// </summary>
        /// <param name="sender">The sender of the event.</param>
        /// <param name="e">The event arguments.</param>
        private void InterestNotfication(object sender, ImsEventArgs e)
        {
            // Don't add local peer to the list.
            // (Unless the original is a validated entity.)
            if (e.OtherRegionId.Address.Equals(this.SpatialOriginal.Guid.OriginalAddress))
            {
                Logger.TraceInformation(LogTag.SpatialEntities, "Ignoring interest notification for {0} because it shares the same address as the entity {1}", e.OtherRegionId, this.SpatialOriginal.Guid);
                return;
            }

            this.entityManager.AddInterested(this, e.OtherRegionId);
        }
    }
}
