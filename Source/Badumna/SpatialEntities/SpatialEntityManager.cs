﻿//-----------------------------------------------------------------------
// <copyright file="SpatialEntityManager.cs" company="NICTA">
//     Copyright (c) 2009 NICTA. All right reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.SpatialEntities
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;

    using Badumna.Core;
    using Badumna.DataTypes;
    using Badumna.InterestManagement;
    using Badumna.Replication;
    using Badumna.Utilities;
    using Badumna.Autoreplication;

    using SpatialAutoreplicationManager = Badumna.Autoreplication.SpatialAutoreplicationManager;

    /// <summary>
    /// Interface for SpatialEntityManager.
    /// </summary>
    internal interface ISpatialEntityManager : IReplicationService<ISpatialOriginal, ISpatialReplica>
    {
        /// <summary>
        /// Creates a new scene to join.
        /// </summary>
        /// <param name="sceneName">Unique name of the scene.</param>
        /// <param name="createReplica">Called when a new entity needs to be instantiated into the scene.</param>
        /// <param name="removeReplica">Called when an entity in the scene departs.</param>
        /// <param name="miniScene">A value indicating whether the scene is a mini scene.</param>
        /// <returns>The new scene.</returns>
        /// <remarks>
        /// Be aware, the delegates CreateSpatialReplica and RemoveSpatialReplica are specific to the game app and they are
        /// different to the CreateReplica/RemoveReplica delegates (in network scene). They will be associated with the scene. 
        /// CreateReplica/RemoveReplica will be associted with EntityType. 
        /// </remarks>
        NetworkScene JoinScene(QualifiedName sceneName, CreateSpatialReplica createReplica, RemoveSpatialReplica removeReplica, bool miniScene);
    }

    /// <summary>
    /// Class which manages all the spatial originals and replicas across all the network scenes.
    /// </summary>
    /// <remarks>
    /// TODO: !!! Make sure these methods are all called from wherever's appropriate.
    /// </remarks>
    internal class SpatialEntityManager : ISpatialEntityManager
    {
        // For supporting autoreplication of replicable entities.
        private readonly SpatialAutoreplicationManager autoreplicationManager;

        /// <summary>
        /// Entity manager.
        /// </summary>
        private IEntityManager entityManager;

        /// <summary>
        /// Dead reckoning helper.
        /// </summary>
        private DeadReckoningHelper deadReckoningHelper = new DeadReckoningHelper();

        /// <summary>
        /// One record per original entity
        /// </summary>
        private Dictionary<BadumnaId, SpatialOriginalWrapper> originals = new Dictionary<BadumnaId, SpatialOriginalWrapper>();

        /// <summary>
        /// One record per entity. The game app code can call JoinScene as many times as it wants. Each entity (uniquely identified by the
        /// BadumnaID as the key in the dictionary) will thus be deserialized for each of these scenes (see the loop in SpatialReplicaCollection's 
        /// deserialize method). 
        /// These replicas all have the same id and they will be kept in the same spatialreplicacollection.
        /// </summary>
        private Dictionary<BadumnaId, SpatialReplicaCollection> replicas = new Dictionary<BadumnaId, SpatialReplicaCollection>();

        /// <summary>
        /// The interest management service.
        /// </summary>
        private IInterestManagementService interestManagementService;

        /// <summary>
        /// Map of scene names to lists of network scenes.
        /// </summary>
        private Dictionary<QualifiedName, List<NetworkScene>> scenes = new Dictionary<QualifiedName, List<NetworkScene>>(new QualifiedNameEqualityComparer());

        /// <summary>
        /// The regular task for garbage collection.
        /// </summary>
        private RegularTask garbageCollectionTask;

        /// <summary>
        /// The queue to use for scheduling events.
        /// </summary>
        private NetworkEventQueue eventQueue;

        /// <summary>
        /// The method to use to queue events that must run in the application's thread.
        /// </summary>
        private QueueInvokable queueApplicationEvent;

        /// <summary>
        /// Provides access to the current time.
        /// </summary>
        private ITime timeKeeper;

        /// <summary>
        /// Reports on network connectivity status.
        /// </summary>
        private INetworkConnectivityReporter connectivityReporter;

        /// <summary>
        /// A delegate that will generate a unique BadumnaId associated with this peer.
        /// </summary>
        private GenericCallBackReturn<BadumnaId> generateBadumnaId;

        /// <summary>
        /// A delegate that will generate a unique BadumnaId associated with this peer.
        /// </summary>
        private GenericCallBackReturn<BadumnaId, PeerAddress> generateBadumnaIdWithAddress;

        /// <summary>
        /// Provides the current public address.
        /// </summary>
        private INetworkAddressProvider addressProvider;

        /// <summary>
        /// Initializes a new instance of the SpatialEntityManager class.
        /// </summary>
        /// <param name="entityManager">Entity manager.</param>
        /// <param name="interestManagementService">The interest management service.</param>
        /// <param name="autoreplicationManager">For supporting autoreplication of replicable entities.</param>
        /// <param name="eventQueue">The event queue to schedule events on.</param>
        /// <param name="queueApplicationEvent">The method to use to queue events that must run in the application's thread.</param>
        /// <param name="timeKeeper">The time source.</param>
        /// <param name="connectivityReporter">Reports on network connectivity status.</param>
        /// <param name="connectionNotifier">Provides notification of peer connections and disconnections.</param>
        /// <param name="addressProvider">Provides the current public address.</param>
        /// <param name="generateBadumnaId">A delegate that will generate a unique BadumnaId associated with this peer.</param>
        /// <param name="generateBadumnaIdWithAddress">A delegate that will generate a unique BadumnaId associated with the given address.</param>
        public SpatialEntityManager(
            IEntityManager entityManager,
            IInterestManagementService interestManagementService,
            SpatialAutoreplicationManager autoreplicationManager,
            NetworkEventQueue eventQueue,
            QueueInvokable queueApplicationEvent,
            ITime timeKeeper,
            INetworkConnectivityReporter connectivityReporter,
            IPeerConnectionNotifier connectionNotifier,
            INetworkAddressProvider addressProvider,
            GenericCallBackReturn<BadumnaId> generateBadumnaId,
            GenericCallBackReturn<BadumnaId, PeerAddress> generateBadumnaIdWithAddress)
        {
            this.autoreplicationManager = autoreplicationManager;
            this.eventQueue = eventQueue;
            this.queueApplicationEvent = queueApplicationEvent;
            this.timeKeeper = timeKeeper;
            this.connectivityReporter = connectivityReporter;
            this.addressProvider = addressProvider;
            this.generateBadumnaId = generateBadumnaId;
            this.generateBadumnaIdWithAddress = generateBadumnaIdWithAddress;

            this.entityManager = entityManager;
            this.entityManager.Parser.RegisterMethodsIn(this);
            this.interestManagementService = interestManagementService;

            this.garbageCollectionTask = new RegularTask(
                "spatial_entity_manager_gc",
                Parameters.EntityManagerGarbageCollectionPeriod,
                eventQueue,
                connectivityReporter,
                this.GarbageCollection);
            this.garbageCollectionTask.Start();

            connectionNotifier.ConnectionLostEvent += this.ConnectionLostWith;
            this.entityManager.Register((byte)EntityGroup.Spatial, this.CreateReplica, this.RemoveReplica);
        }

        /// <summary>
        /// Gets the dead reckoning helper.
        /// </summary>
        public DeadReckoningHelper DeadReckoningHelper
        {
            get { return this.deadReckoningHelper; }
        }

        /// <summary>
        /// Gets the total number of spatial replicas.
        /// </summary>
        public int ReplicaCount
        {
            get
            {
                int count = 0;
                foreach (SpatialReplicaCollection replica in this.replicas.Values)
                {
                    count += replica.Count;
                }

                return count;
            }
        }

        /// <summary>
        /// Reset the spatial entity manager.
        /// </summary>
        public void Reset()
        {
            this.scenes = new Dictionary<QualifiedName, List<NetworkScene>>(new QualifiedNameEqualityComparer());
            this.deadReckoningHelper.Reset();
        }

        /// <summary>
        /// Get a list of joined scenes with the given name.  The list returned must not be modified!
        /// </summary>
        /// <param name="sceneName">Name of scene to check for.</param>
        /// <returns>List of joined scenes with the given name.</returns>
        public List<NetworkScene> TryGetScenes(QualifiedName sceneName)
        {
            List<NetworkScene> scenes;
            this.scenes.TryGetValue(sceneName, out scenes);
            return scenes;
        }

        /// <summary>
        /// Creates a new list and fills it with the (unqualified) names of the scenes that are currently joined.
        /// </summary>
        /// <returns>List of joined scene names.</returns>
        public List<string> GetCurrentSceneNames()
        {
            var sceneNames = new List<string>();
            foreach (var sceneName in this.scenes.Keys)
            {
                sceneNames.Add(sceneName.Name);
            }

            return sceneNames;
        }

        /// <inheritdoc/>
        public NetworkScene JoinScene(QualifiedName sceneName, CreateSpatialReplica createReplica, RemoveSpatialReplica removeReplica, bool miniScene)
        {
            NetworkScene newScene = new LocalNetworkScene(
                sceneName,
                this,
                createReplica,
                removeReplica,
                this.autoreplicationManager,
                this.interestManagementService,
                this.eventQueue,
                this.queueApplicationEvent,
                this.addressProvider,
                this.generateBadumnaId,
                miniScene);
 
            List<NetworkScene> sceneList;
            if (!this.scenes.TryGetValue(sceneName, out sceneList))
            {
                sceneList = new List<NetworkScene>();
                this.scenes[sceneName] = sceneList;
            }

            sceneList.Add(newScene);

            return newScene;
        }

        /// <summary>
        /// Removes all spatial entities from the scene.
        /// </summary>
        /// <param name="scene">Network scene.</param>
        public void LeaveScene(NetworkScene scene)
        {
            // Logger.TraceInformation(LogTag.SpatialEntities, "LeaveScene is called.");

            // Try and remove the replicas from the exiting scene
            // This should happen immediately (still in the application thread), while the rest of the work in
            // leaving a scene needs to happen on the network event queue to make sure it happens after the scene has been joined.
            foreach (KeyValuePair<BadumnaId, SpatialReplicaCollection> replicaCollectionPair in this.replicas)
            {
                replicaCollectionPair.Value.RemoveFromScene(scene);
                //// For interest check
                replicaCollectionPair.Value.LastAttention = this.CalculateAttention(replicaCollectionPair.Value);
                this.DetectSeparation(replicaCollectionPair.Value);
            }

            this.eventQueue.Push(this.OnLeaveScene, scene);
        }
        
        /// <summary>
        /// Add spatial original to network scene.
        /// </summary>
        /// <param name="spatialOriginal">Spatial original to add.</param>
        /// <param name="entityType">Type of spatial original.</param>
        /// <param name="scene">Network scene.</param>
        public void AddOriginal(ISpatialOriginal spatialOriginal, uint entityType, NetworkScene scene)
        {
            SpatialOriginalWrapper originalWrapper;
            if (this.originals.TryGetValue(spatialOriginal.Guid, out originalWrapper))
            {
                originalWrapper.ChangeScene(scene);
            }
            else
            {
                originalWrapper = new SpatialOriginalWrapper(
                    spatialOriginal,
                    entityType,
                    scene,
                    this.interestManagementService,
                    this.entityManager,
                    this.eventQueue,
                    this.timeKeeper,
                    this.connectivityReporter,
                    this.generateBadumnaIdWithAddress);
                this.originals[spatialOriginal.Guid] = originalWrapper;
                this.entityManager.RegisterEntity(originalWrapper, new EntityTypeId(0, entityType));
            }

            // IMS region insertions needs to be triggered here, as waiting for the first serialization attempt
            // will mean that non-moving entities are not replicated until after a delay.
            originalWrapper.CheckImsRegions();
        }

        /// <inheritdoc/>
        public void MarkForUpdate(ISpatialOriginal spatialOriginal, BooleanArray changedParts)
        {
            if (null == spatialOriginal)
            {
                throw new ArgumentNullException("spatialOriginal cannot be null.");
            }

            if (null == spatialOriginal.Guid)
            {
                return;
            }

            // Called in event queue to ensure there is no conflict with online events and other threading issues.
            this.eventQueue.Push(this.MarkForUpdateImplementation, spatialOriginal, changedParts);
        }

        /// <summary>
        /// Schedule marks parts for update in network event queue.
        /// </summary>
        /// <param name="spatialOriginal">Spatial original to update.</param>
        /// <param name="changedPartIndex">Index of part to update.</param>
        public void MarkForUpdate(ISpatialOriginal spatialOriginal, int changedPartIndex)
        {
            if (null == spatialOriginal)
            {
                throw new ArgumentNullException("spatialOriginal cannot be null.");
            }

            if (null == spatialOriginal.Guid)
            {
                return;
            }

            // Called in event queue to ensure there is no conflict with online events and other threading issues.
            this.eventQueue.Push(this.MarkForUpdateImplementation, spatialOriginal, changedPartIndex);
        }

        /// <summary>
        /// Remove spatial original from network scenes.
        /// </summary>
        /// <param name="original">Spatial original.</param>
        /// <param name="removeFromSceneEvenIfDistributed">Flag to remove the spatial original even if it is distributed.</param>
        public void RemoveOriginal(ISpatialOriginal original, bool removeFromSceneEvenIfDistributed)
        {
            SpatialOriginalWrapper originalWrapper;
            if (!this.originals.TryGetValue(original.Guid, out originalWrapper))
            {
                return;
            }

            // TODO : Make a better way to detect dht ids.
            bool isDistributed = originalWrapper.Guid.Address.IsDhtAddress;
            bool removeFromScene =
                (removeFromSceneEvenIfDistributed || !isDistributed) &&
                    (this.connectivityReporter.Status == ConnectivityStatus.Online ||
                     this.connectivityReporter.Status == ConnectivityStatus.ShuttingDown);

            // NOTE: NotifyOfDisinterest should be called first before calling entityManager.UnregisterEntity(..)
            // because UnregisterEntity will clear the list of the associated replicas.
            List<BadumnaId> list = this.entityManager.Association.GetAssociatedReplicas(original.Guid);
            if (list != null)
            {
                foreach (BadumnaId replicaId in list)
                {
                    this.NotifyOfDisinterest(replicaId, original.Guid);
                }
            }
            
            originalWrapper.Shutdown(removeFromScene);
            this.entityManager.UnregisterEntity(originalWrapper, removeFromScene);
            this.originals.Remove(original.Guid);
        }

        /////// <summary>
        /////// Marks parts to update.
        /////// </summary>
        /////// <param name="spatialOriginal">Spatial original to update.</param>
        /////// <param name="changedParts">List of parts to update.</param>
        ////public void MarkForUpdate(ISpatialOriginal spatialOriginal, BooleanArray changedParts)
        ////{
        ////    if (null == spatialOriginal)
        ////    {
        ////        throw new ArgumentNullException("spatialOriginal cannot be null.");
        ////    }

        ////    if (null == spatialOriginal.Guid)
        ////    {
        ////        return;
        ////    }

        ////    SpatialOriginalWrapper wrapper = null;
        ////    if (this.originals.TryGetValue(spatialOriginal.Guid, out wrapper))
        ////    {
        ////        wrapper.MarkForUpdate(changedParts);
        ////    }
        ////}

        /////// <summary>
        /////// Marks parts for update.
        /////// </summary>
        /////// <param name="spatialOriginal">Spatial original to update.</param>
        /////// <param name="changedPartIndex">Index of part to update.</param>
        ////public void MarkForUpdate(ISpatialOriginal spatialOriginal, int changedPartIndex)
        ////{
        ////    if (null == spatialOriginal)
        ////    {
        ////        throw new ArgumentNullException("spatialOriginal cannot be null.");
        ////    }

        ////    if (null == spatialOriginal.Guid)
        ////    {
        ////        return;
        ////    }

        ////    SpatialOriginalWrapper wrapper = null;
        ////    if (this.originals.TryGetValue(spatialOriginal.Guid, out wrapper))
        ////    {
        ////        wrapper.MarkForUpdate(changedPartIndex);
        ////    }
        ////}

        /// <summary>
        /// Send a custom message to a spatial original's replica.
        /// </summary>
        /// <param name="spatialOriginal">Spatial original whose replicas should receive the message.</param>
        /// <param name="messageData">Custom message.</param>
        public void SendCustomMessageToReplicas(ISpatialOriginal spatialOriginal, MemoryStream messageData)
        {
            SpatialOriginalWrapper originalWrapper;
            if (!this.originals.TryGetValue(spatialOriginal.Guid, out originalWrapper))
            {
                throw new InvalidOperationException("Entity must be registered first");
            }

            this.entityManager.SendCustomMessageToReplicas(originalWrapper, messageData);
        }

        /// <summary>
        /// Send a custom message to a spatial original.
        /// </summary>
        /// <param name="spatialReplica">Spatial replica whose original should receive the message.</param>
        /// <param name="messageData">Custom message.</param>
        public void SendCustomMessageToOriginal(ISpatialReplica spatialReplica, MemoryStream messageData)
        {
            SpatialReplicaCollection replicaCollection;
            if (!this.replicas.TryGetValue(spatialReplica.Guid, out replicaCollection))
            {
                throw new InvalidOperationException("Unknown replica");
            }

            this.entityManager.SendCustomMessageToOriginal(replicaCollection, messageData);
        }

        /// <summary>
        /// Calculate the attention weight of a spatial replica.
        /// </summary>
        /// <param name="spatialReplicaCollection">Spatial replica entity.</param>
        /// <returns>Attention weight.</returns>
        public double CalculateAttention(SpatialReplicaCollection spatialReplicaCollection)
        {
            double attentionWeight = 0.0;

            Vector3 remoteEntityPosition = spatialReplicaCollection.Position;
            float remoteEntityRadius = spatialReplicaCollection.Radius;

            foreach (SpatialOriginalWrapper originalWrapper in this.originals.Values)
            {
                if (null == originalWrapper)
                {
                    continue;
                }

                if (originalWrapper.IsInterestedIn(spatialReplicaCollection.SceneName, remoteEntityPosition, remoteEntityRadius))
                {
                    // TODO : The position of the local entity is used here. But really it should be the position of the camera!
                    // The position of the interest region doesn't change often enough to be useful here.
                    
                    // the distance between two entities. 
                    double distance = (remoteEntityPosition - originalWrapper.Position).Magnitude;
                    //// the attention value will be 1 if the distance is less than fullAttentionDistance
                    double fullAttentionDistance = originalWrapper.SpatialOriginal.AreaOfInterestRadius + remoteEntityRadius;

                    if (originalWrapper.InterestRegion == null)
                    {
                        if (originalWrapper.Guid.Address.Equals(spatialReplicaCollection.Guid.Address))
                        {
                            // TODO: check whether this is still valid. 
                            // original and replica are on the same peer (i.e. distributed controller)
                            attentionWeight = Math.Max(attentionWeight, 0.1);
                        }
                        else
                        {
                            // an item (e.g. a weapon that can be seen by all nearby avatars)
                        }

                        continue;
                    }

                    if (distance > fullAttentionDistance)
                    {
                        // calcuated as:
                        // 1.0 - (distance - fullAttentionDistance) / (fullAttentionDistance + 4 * ConfigurableParameters.SubscriptionInflateAmount - fullAttentionDistance);
                        // when the distance is full attention distance, the attention value should be 1.0, attention will become 0 when the distance value is equals to the 
                        // smallest value that is enough to trigger seperation.
                        double currentAttentionWeight = 1.0 - ((distance - fullAttentionDistance) / (4 * this.interestManagementService.SubscriptionInflation));
                        
                        // the replica's attention weight its highest weight with any of the original entity on the local peer. 
                        attentionWeight = Math.Max(attentionWeight, currentAttentionWeight);
                    }
                    else
                    {
                        // the distance between two entities is no more than the full attention distance, then the attention value is always 1.0. 
                        attentionWeight = 1.0;
                        break;
                    }
                }
            }

            return attentionWeight;
        }

        /// <summary>
        /// Check whether any entities had left the interest region of the specified spatial entity.
        /// </summary>
        /// <param name="spatialReplicaCollection">spatial entity.</param>
        public void DetectSeparation(SpatialReplicaCollection spatialReplicaCollection)
        {
            Vector3 remoteEntityPosition = spatialReplicaCollection.Position;
            float remoteEntityRadius = spatialReplicaCollection.Radius;
            bool notInterestedByAnyOriginal = true;

            // Hopefully if the scene has been joined multiple times, it's always joined as a miniscene...
            // Also, if we don't have an instance for the scene, we assume it's not a miniscene.
            // Having no instance implies we have no originals on the scene, so in this case sending
            // a separation notification is correct (separation notifications shouldn't be sent
            // to miniscene originals based on entity positions / interest radii, but they should be
            // sent if the scene has changed).
            List<NetworkScene> scenes;
            var isOnMiniScene =
                this.scenes.TryGetValue(spatialReplicaCollection.SceneName, out scenes) &&
                scenes.Count > 0 &&
                scenes[0].MiniScene;

            List<BadumnaId> assocatedOriginals = this.entityManager.Association.GetAssociatedOriginals(spatialReplicaCollection.Guid);
            if (assocatedOriginals != null)
            {
                foreach (BadumnaId id in assocatedOriginals)
                {
                    SpatialOriginalWrapper originalWrapper = null;
                    if (!this.originals.TryGetValue(id, out originalWrapper))
                    {
                        Logger.TraceInformation(LogTag.SpatialEntities, "Association inconsistency detected. original id {0} couldn't be accessed in SpatialEntityManager but still exists in the association object.", id);
                        continue;
                    }

                    if (!isOnMiniScene && !originalWrapper.IsInterestedIn(spatialReplicaCollection.SceneName, remoteEntityPosition, remoteEntityRadius) ||
                        isOnMiniScene && !originalWrapper.IsInScene(spatialReplicaCollection.SceneName))
                    {
                        this.NotifyOfDisinterest(spatialReplicaCollection.Guid, originalWrapper.Guid);
                    }
                    else
                    {
                        notInterestedByAnyOriginal = false;
                    }
                }
            }

            if (notInterestedByAnyOriginal)
            {
                Logger.TraceInformation(LogTag.SpatialEntities, "ReplicaCollection {0} is not interested by any original. ", spatialReplicaCollection.Guid);
            }
        }

        /// <summary>
        /// Called when process network state is called.
        /// </summary>
        public void OnProcessNetworkState()
        {
            this.DeadReckoningHelper.PerformDeadReckoning();
        }

        /// <summary>
        /// Called when the uninteresting entity moves out of the interest region of the target entity.
        /// </summary>
        /// <param name="targetId">Unique ID of target entity.</param>
        /// <param name="uninterestingId">Unique ID of uninteresting entity.</param>
        public void NotifyOfDisinterest(BadumnaId targetId, BadumnaId uninterestingId)
        {
            Logger.TraceInformation(LogTag.SpatialEntities, "Notify of disinterest in {0}", uninterestingId);
            QualityOfService qos = new QualityOfService(true);
            qos.MustBeInOrder = false;
            EntityEnvelope envelope = this.entityManager.GetMessageFor(targetId, qos);
            this.entityManager.RemoteCall(envelope, this.InterestSeperation, uninterestingId);
            this.entityManager.Router.DirectSend(envelope);
        }

        /// <summary>
        /// Marks parts to update.
        /// </summary>
        /// <param name="spatialOriginal">Spatial original to update.</param>
        /// <param name="changedParts">List of parts to update.</param>
        private void MarkForUpdateImplementation(ISpatialOriginal spatialOriginal, BooleanArray changedParts)
        {
            SpatialOriginalWrapper wrapper = null;
            if (this.originals.TryGetValue(spatialOriginal.Guid, out wrapper))
            {
                wrapper.MarkForUpdate(changedParts);
            }
        }

        /// <summary>
        /// Marks parts to update.
        /// </summary>
        /// <param name="spatialOriginal">Spatial original to update.</param>
        /// <param name="changedPartIndex">Index of part to update.</param>
        private void MarkForUpdateImplementation(ISpatialOriginal spatialOriginal, int changedPartIndex)
        {
            SpatialOriginalWrapper wrapper = null;
            if (this.originals.TryGetValue(spatialOriginal.Guid, out wrapper))
            {
                wrapper.MarkForUpdate(changedPartIndex);
            }
        }

        /// <summary>
        /// Called when connection is lost.
        /// </summary>
        /// <param name="address">Address of peer.</param>
        private void ConnectionLostWith(PeerAddress address)
        {
            this.PeerUninterestedFeedbackOnConnectionLost(address);
        }

        /// <summary>
        /// Stop sending updates to the remote peer which the local peer just lost connection with. 
        /// </summary>
        /// <param name="address">The remote peer address.</param>
        private void PeerUninterestedFeedbackOnConnectionLost(PeerAddress address)
        {
            List<Pair<BadumnaId, BadumnaId>> uninterested = new List<Pair<BadumnaId, BadumnaId>>();
            foreach (BadumnaId id in this.originals.Keys)
            {
                // get the interested peer for each local original entity. 
                InterestedPeer peer = this.entityManager.GetInterestedPeer(id, address);
                if (peer != null && peer.Count > 0)
                {
                    foreach (InterestedEntity entity in peer)
                    {
                        if (entity.EntityId.Address.IsDhtAddress)
                        {
                            continue;
                        }

                        uninterested.Add(new Pair<BadumnaId, BadumnaId>(id, entity.EntityId));
                    }
                }
            }

            foreach (Pair<BadumnaId, BadumnaId> pair in uninterested)
            {
                SpatialOriginalWrapper original;
                if (this.originals.TryGetValue(pair.First, out original))
                {
                    original.PeerUninterestedFeedback(pair.Second);
                }
            }
        }

        /// <summary>
        /// Garbage collection task. 
        /// </summary>
        private void GarbageCollection()
        {
            // detect and remove stale interested peers, stop sending updates to them.
            this.RemoveStaleInterestedPeers();
        }

        /// <summary>
        /// Removes the stale interested peers that didn't send any flow control feedback message for an extended period of time. 
        /// </summary>
        private void RemoveStaleInterestedPeers()
        {
            List<Pair<BadumnaId, BadumnaId>> deadReplicas = this.entityManager.GetTimeOutPeers();
            if (deadReplicas != null)
            {
                foreach (Pair<BadumnaId, BadumnaId> pair in deadReplicas)
                {
                    BadumnaId originalId = pair.First;
                    BadumnaId uninterestedId = pair.Second;

                    SpatialOriginalWrapper original;
                    if (this.originals.TryGetValue(originalId, out original))
                    {
                        Logger.TraceInformation(LogTag.SpatialEntities, "RemoveStaleInterestedPeers is going to call PeerUninterestedFeedback, id = {0}", uninterestedId);
                        original.PeerUninterestedFeedback(uninterestedId);
                    }
                }
            }
        }

        /// <summary>
        /// Creates the replica. This method wil be registered to the entity manager (in the replication module), so every time when there
        /// is a new replica (i.e. previously unknown to the replication) need to be updated in entity manager's ProcessInboundUpdates()
        /// method, this CreateReplica can be called to create the replica.
        /// see EntityManager.remote.cs.SafeEntityArrivalHandler on how it is actually used.
        /// </summary>
        /// <param name="entityId">The entity id.</param>
        /// <param name="entityType">Type of the entity.</param>
        /// <returns>New replica.</returns>
        private IReplica CreateReplica(BadumnaId entityId, EntityTypeId entityType)
        {
            SpatialReplicaCollection replicaCollection;
            if (!this.replicas.TryGetValue(entityId, out replicaCollection))
            {
                replicaCollection = new SpatialReplicaCollection(this, entityId, entityType.Id, this.eventQueue, this.queueApplicationEvent);
                this.replicas[entityId] = replicaCollection;
            }

            return replicaCollection;
        }

        /// <summary>
        /// Remove the replica from all scenes.
        /// </summary>
        /// <param name="replica">Replica to remove.</param>
        private void RemoveReplica(IReplica replica)
        {
            SpatialReplicaCollection replicaCollection = (SpatialReplicaCollection)replica;
            replicaCollection.RemoveFromAllScenes();
            this.replicas.Remove(replicaCollection.Guid);
        }

        /// <summary>
        /// Indicates that the peer who sent this envelope is not interested in the entity at all
        /// </summary>
        /// <param name="uninterestedId">Unique ID of uninterested entity.</param>
        [EntityProtocolMethod(EntityMethod.InterestSeperation)]
        private void InterestSeperation(BadumnaId uninterestedId)
        {
            BadumnaId destination = this.entityManager.CurrentEnvelope.DestinationEntity;
            SpatialOriginalWrapper originalWrapper;
            if (this.originals.TryGetValue(destination, out originalWrapper))
            {
                Logger.TraceInformation(LogTag.SpatialEntities, "Interest seperation of {0}", destination);
                originalWrapper.PeerUninterestedFeedback(uninterestedId);
            }
        }

        /// <summary>
        /// Do tidy up on leaving a scene.
        /// Must be called on network event queue to make sure the scene joining and entity registration/unregistration
        /// happens in order first.
        /// </summary>
        /// <param name="scene">The scene that has been left.</param>
        private void OnLeaveScene(NetworkScene scene)
        {
            List<ISpatialOriginal> originalsToRemove = new List<ISpatialOriginal>();

            // Remove any local entities controlled by the exiting scene
            foreach (KeyValuePair<BadumnaId, SpatialOriginalWrapper> originalWrapperPair in this.originals)
            {
                if (originalWrapperPair.Value.IsControlledByScene(scene))
                {
                    originalsToRemove.Add(originalWrapperPair.Value.SpatialOriginal);
                }
            }

            foreach (ISpatialOriginal localEntity in originalsToRemove)
            {
                this.RemoveOriginal(localEntity, false);
            }

            List<NetworkScene> sceneList;
            if (this.scenes.TryGetValue(scene.QualifiedName, out sceneList))
            {
                sceneList.Remove(scene);
                if (sceneList.Count == 0)
                {
                    this.scenes.Remove(scene.QualifiedName);
                }
            }
        }
    }
}
