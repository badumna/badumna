﻿//-----------------------------------------------------------------------
// <copyright file="IReplicableEntity.cs" company="NICTA">
//     Copyright (c) 2009 NICTA. All right reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.SpatialEntities
{
    using Badumna.DataTypes;

    /// <summary>
    /// A replicable entity.
    /// </summary>
    public interface IReplicableEntity
    {
        /// <summary>
        /// Gets or sets the centroid of the entity.
        /// </summary>
        Vector3 Position { get; set; }
    }
}
