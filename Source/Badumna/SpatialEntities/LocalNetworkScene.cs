﻿//-----------------------------------------------------------------------
// <copyright file="LocalNetworkScene.cs" company="NICTA">
//     Copyright (c) 2009 NICTA. All right reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Badumna.SpatialEntities
{
    using System;
    using System.Collections.Generic;
    using Badumna.Autoreplication;
    using Badumna.Core;
    using Badumna.DataTypes;
    using Badumna.InterestManagement;
    using Badumna.Utilities;

    using SpatialAutoreplicationManager = Badumna.Autoreplication.SpatialAutoreplicationManager;
    
    /// <summary>
    /// Local network scene.
    /// </summary>
    internal class LocalNetworkScene : NetworkScene
    {
        /// <summary>
        /// Spatial entity manager.
        /// </summary>
        private SpatialEntityManager spatialEntityManager;

        /// <summary>
        /// The queue to use for scheduling events.
        /// </summary>
        private NetworkEventQueue eventQueue;

        /// <summary>
        /// A delegate that will generate a unique BadumnaId associated with this peer.
        /// </summary>
        private GenericCallBackReturn<BadumnaId> generateBadumnaId;

        /// <summary>
        /// Provides the current public address.
        /// </summary>
        private INetworkAddressProvider addressProvider;

        /// <summary>
        /// For checking entity radius and AOI are within limits.
        /// </summary>
        private readonly IInterestManagementService interestManagementService;

        /// <summary>
        /// Initializes a new instance of the LocalNetworkScene class.
        /// </summary>
        /// <param name="name">Unique name of the scene.</param>
        /// <param name="spatialEntityManager">Spatial entity manager.</param>
        /// <param name="addReplica">Called when a new entity needs to be instantiated into the scene.</param>
        /// <param name="removeReplica">Called when an entity in the scene departs.</param>
        /// <param name="eventQueue">The event queue to schedule events on.</param>
        /// <param name="autoreplicationManager">For managing autoreplication wrappers for replicable entities.</param>
        /// <param name="interestManagementService">For checking entity radius and AOI are withing limits.</param>
        /// <param name="queueApplicationEvent">The method to use to queue events that must run in the application's thread.</param>
        /// <param name="addressProvider">Provides the current public address.</param>
        /// <param name="generateBadumnaId">A delegate that will generate a unique BadumnaId associated with this peer.</param>
        /// <param name="miniScene">A value indicating whether the scene is a mini scene.</param>
        public LocalNetworkScene(
            QualifiedName name,
            SpatialEntityManager spatialEntityManager,
            CreateSpatialReplica addReplica,
            RemoveSpatialReplica removeReplica,
            SpatialAutoreplicationManager autoreplicationManager,
            IInterestManagementService interestManagementService,
            NetworkEventQueue eventQueue,
            QueueInvokable queueApplicationEvent,
            INetworkAddressProvider addressProvider,
            GenericCallBackReturn<BadumnaId> generateBadumnaId,
            bool miniScene)
            : base(
                name,
                addReplica,
                removeReplica,
                autoreplicationManager,
                eventQueue,
                queueApplicationEvent,
                miniScene)
        {
            if (interestManagementService == null)
            {
                throw new ArgumentNullException("interestManagementService");
            }

            this.spatialEntityManager = spatialEntityManager;
            this.interestManagementService = interestManagementService;
            this.eventQueue = eventQueue;
            this.addressProvider = addressProvider;
            this.generateBadumnaId = generateBadumnaId;
        }

        /// <summary>
        /// Derived specialization of the leave scene implementation.
        /// </summary>
        protected override void LeaveImplimentation()
        {
            this.spatialEntityManager.LeaveScene(this);
        }

        /// <summary>
        /// Registers an entity in the scene.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="entityType">Type of the entity.</param>
        protected override void RegisterEntityImplementation(ISpatialOriginal entity, uint entityType)
        {
            if (entity.Guid == null ||
                !entity.Guid.IsValid ||
                (!entity.Guid.Address.IsDhtAddress && !entity.Guid.Address.Equals(this.addressProvider.PublicAddress)))
            {
                // Need to generate a GUID because we haven't got a valid GUID, or the GUID belongs to
                // an old public address (if, e.g., the stack was restarted when we connected to a new
                // network but the same originals are being reused).
                entity.Guid = this.generateBadumnaId();
            }

            this.eventQueue.Push(this.spatialEntityManager.AddOriginal, entity, entityType, this);
        }

        /// <summary>
        /// Unregisters the entity from the scene.
        /// </summary>
        /// <param name="entity">The entity.</param>
        protected override void UnregisterEntityImplementation(ISpatialOriginal entity)
        {
            this.eventQueue.Push(this.spatialEntityManager.RemoveOriginal, entity, true);
        }

        /// <inheritdoc />
        protected override void ValidateInterestRadius(ISpatialOriginal entity)
        {
            if (this.MiniScene)
            {
                return;
            }

            if (entity.Radius > this.interestManagementService.MaximumInterestRadius ||
                entity.AreaOfInterestRadius > this.interestManagementService.MaximumInterestRadius)
            {
                throw new InvalidOperationException(
                    "Entity radius and AOI radius must be less than or equal to max interest radius registered by calling NetworkFacade.RegisterEntityDetails.");
            }
        }
    }
}
