﻿//---------------------------------------------------------------------------------
// <copyright file="ForwardingManager.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

using System;

using Badumna.Core;
using Badumna.DataTypes;
using Badumna.Replication;
using Badumna.Transport;

namespace Badumna.Overload
{
    /// <summary>
    /// The forwarding manager is used to create forwarding groups. 
    /// </summary>
    internal class ForwardingManager
    {
        /// <summary>
        /// The envelope forwarder.
        /// </summary>
        private EnvelopeForwarder forwarder;

        private IForwardingGroupFactory forwardingGroupFactory;

        /// <summary>
        /// The queue to use for scheduling events.
        /// </summary>
        private NetworkEventQueue eventQueue;

        /// <summary>
        /// Provides access to the current time.
        /// </summary>
        private ITime timeKeeper;

        /// <summary>
        /// The configuration options for the overload system.
        /// </summary>
        private OverloadModule overloadOptions;

        /// <summary>
        /// Provides the public peer address.
        /// </summary>
        private INetworkAddressProvider addressProvider;

        /// <summary>
        /// Provides notification of peer connections and disconnections.
        /// </summary>
        private IPeerConnectionNotifier connectionNotifier;

        /// <summary>
        /// Initializes a new instance of the <see cref="ForwardingManager"/> class.
        /// </summary>
        /// <param name="overloadOptions">The overload configuration options.</param>
        /// <param name="transportProtocol">The transport protocol.</param>
        /// <param name="connectionTable">The connection table.</param>
        /// <param name="envelopeForwarderFactory">The envelope forwarder factory.</param>
        /// <param name="forwardingGroupFactory">The forwarding group factory.</param>
        /// <param name="eventQueue">The event queue to schedule events on.</param>
        /// <param name="timeKeeper">The time source.</param>
        /// <param name="addressProvider">Provides the public peer address.</param>
        /// <param name="connectivityReporter">Reports on network connectivity status.</param>
        /// <param name="connectionNotifier">Provides notification of peer connections and disconnections.</param>
        public ForwardingManager(
            OverloadModule overloadOptions,
            TransportProtocol transportProtocol, 
            ConnectionTable connectionTable, 
            IEnvelopeForwarderFactory envelopeForwarderFactory,
            IForwardingGroupFactory forwardingGroupFactory,
            NetworkEventQueue eventQueue,
            ITime timeKeeper,
            INetworkAddressProvider addressProvider,
            INetworkConnectivityReporter connectivityReporter,
            IPeerConnectionNotifier connectionNotifier)
        {
            this.overloadOptions = overloadOptions;

            this.eventQueue = eventQueue;
            this.timeKeeper = timeKeeper;

            this.addressProvider = addressProvider;
            this.connectionNotifier = connectionNotifier;

            this.forwarder = envelopeForwarderFactory.Create(transportProtocol, connectionTable, this.overloadOptions, eventQueue, this.timeKeeper, connectivityReporter, addressProvider);
            this.forwardingGroupFactory = forwardingGroupFactory;
        }

        /// <summary>
        /// Gets a value indicating whether this instance is forwarding enabled.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is forwarding enabled; otherwise, <c>false</c>.
        /// </value>
        public bool IsForwardingEnabled
        {
            //// TODO: This doesn't guarantee that the overload peer is actually reachable.  Need
            ////       to ensure that clients of the forwarding service can recover successfully if
            ////       a connection cannot be established to the overload peer.
            get
            {
                return this.overloadOptions.IsClientEnabled;
            }
        }

        public OverloadStatus GetOverloadServerStatus()
        {
            return this.forwarder.OverloadStatus;
        }

        /// <summary>
        /// Shutdowns this instance.
        /// </summary>
        public void Shutdown()
        {
            this.forwarder.Shutdown();
        }

        /// <summary>
        /// Sets the entity manager.
        /// </summary>
        /// <value>The entity manager.</value>
        public EntityManager EntityManager
        {
            set { this.forwarder.EntityManager = value; }
        }

        /// <summary>
        /// Creates the forwarding group.
        /// </summary>
        /// <param name="sourceEntityId">The source entity id.</param>
        /// <returns>A forwarding group.</returns>
        public IForwardingGroup CreateForwardingGroup(BadumnaId sourceEntityId)
        {
            if (!this.IsForwardingEnabled)
            {
                throw new InvalidOperationException();
            }

            if (this.overloadOptions.IsDistributedLookupUsed)
            {
                return this.forwardingGroupFactory.Create(this.forwarder, PeerAddress.Nowhere, sourceEntityId, this.overloadOptions, this.eventQueue, this.timeKeeper, this.addressProvider, this.connectionNotifier);
            }
            else
            {
                return this.forwardingGroupFactory.Create(this.forwarder, this.overloadOptions.OverloadPeerAddress, sourceEntityId, this.overloadOptions, this.eventQueue, this.timeKeeper, this.addressProvider, this.connectionNotifier);
            }
        }
        }
}
