﻿//-----------------------------------------------------------------------
// <copyright file="IForwardingGroupFactory.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using Badumna.Core;
using Badumna.DataTypes;

namespace Badumna.Overload
{
    /// <summary>
    /// document this interface.
    /// </summary>
    internal interface IForwardingGroupFactory
    {
        /// <summary>
        /// Creates the specified forwarder.
        /// </summary>
        /// <param name="forwarder">The forwarder.</param>
        /// <param name="overloadPeerAddress">The overload peer address.</param>
        /// <param name="groupId">The group id.</param>
        /// <param name="overloadOptions">The configuration options for the overload system.</param>
        /// <param name="eventQueue">The queue to use for scheduling events.</param>
        /// <param name="timeKeeper">Provides access to the current time.</param>
        /// <param name="addressProvider">Provides the public peer address.</param>
        /// <param name="connectionNotifier">Provides notification of peer connections and disconnections.</param>
        /// <returns>The forwarding group object.</returns>
        IForwardingGroup Create(
            EnvelopeForwarder forwarder,
            PeerAddress overloadPeerAddress,
            BadumnaId groupId,
            OverloadModule overloadOptions,
            NetworkEventQueue eventQueue,
            ITime timeKeeper,
            INetworkAddressProvider addressProvider,
            IPeerConnectionNotifier connectionNotifier);
    }
}