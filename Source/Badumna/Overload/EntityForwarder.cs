﻿//---------------------------------------------------------------------------------
// <copyright file="EntityForwarder.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

using System;
using System.Collections.Generic;

using Badumna.Core;
using Badumna.DataTypes;
using Badumna.Replication;
using Badumna.Transport;

namespace Badumna.Overload
{
    /// <summary>
    /// The entity protocol used to forward updates between the forwarding peer and replicas. 
    /// </summary>
    internal class EntityForwarder : IMessageConsumer<EntityEnvelope>
    {
        /// <summary>
        /// The entity router.
        /// </summary>
        private IEntityRouter router;

        /// <summary>
        /// The entity manager.
        /// </summary>
        private EntityManager entityManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="EntityForwarder"/> class.
        /// </summary>
        /// <param name="parent">The parent transport protocol.</param>
        /// <param name="factory">The entity router factor.</param>
        /// <param name="addressProvider">Provides the current public address.</param>
        public EntityForwarder(TransportProtocol parent, IEntityRouterFactory factory, INetworkAddressProvider addressProvider)
        {
            this.router = factory.Create(parent, this);
        }

        /// <summary>
        /// Gets the router.
        /// </summary>
        /// <value>The router.</value>
        public IEntityRouter Router 
        { 
            get { return this.router; } 
        }

        /// <summary>
        /// Sets the entity manager.
        /// </summary>
        /// <value>The entity manager.</value>
        public EntityManager EntityManager
        {
            set { this.entityManager = value; }
        }

        /// <summary>
        /// Forwards the update to entities.
        /// </summary>
        /// <param name="payload">The update payload.</param>
        /// <param name="entities">The entities.</param>
        public void ForwardToEntities(EntityEnvelope payload, List<BadumnaId> entities)
        {
            foreach (BadumnaId entity in entities)
            {
                payload.DestinationEntity = entity;
                this.Router.DirectSend(payload);
            }
        }

        #region IMessageConsumer<EntityEnvelope> Members

        /// <summary>
        /// Processes the message.
        /// </summary>
        /// <param name="envelope">The envelope.</param>
        public void ProcessMessage(EntityEnvelope envelope)
        {
            if (this.entityManager != null)
            {
                this.entityManager.ProcessMessage(envelope);
            }
        }

        #endregion
    }
}