﻿//---------------------------------------------------------------------------------
// <copyright file="ForwardingGroup.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Security.Cryptography;
using System.Text;

using Badumna.Core;
using Badumna.DataTypes;
using Badumna.Replication;
using Badumna.ServiceDiscovery;
using Badumna.Transport;
using Badumna.Utilities;

namespace Badumna.Overload
{
    /// <summary>
    /// The forwarding group is used by the local entity to implement overload forwarding. The group contains
    /// a list of registered entities that the local entity should send its updates to. The local peer would 
    /// send the update to a forwarding peer and let that peer to forward the update to all registered entities.
    /// </summary>
    internal class ForwardingGroup : ServiceConnection, IForwardingGroup
    {
        /// <summary>
        /// The peer address of the forwarding peer.
        /// </summary>
        private PeerAddress forwardingPeer;

        /// <summary>
        /// The envelope forwarder.
        /// </summary>
        private EnvelopeForwarder forwarder;

        /// <summary>
        /// The Id of the group.
        /// </summary>
        private BadumnaId groupId;

        /// <summary>
        /// A list of entity Ids that have been registered.
        /// </summary>
        private List<BadumnaId> registeredEntities = new List<BadumnaId>();

        /// <summary>
        /// A list of entity ids that have been added, they will be registered during the next forwarding operation. 
        /// </summary>
        private List<BadumnaId> addedEntities = new List<BadumnaId>();

        /// <summary>
        /// A list of entity ids that will be removed during the next forwarding operation.
        /// </summary>
        private List<BadumnaId> removedEntities = new List<BadumnaId>();

        /// <summary>
        /// The hash value of the group configuration. It is determined by the registered entities. 
        /// </summary>
        private int groupConfigHash;

        /// <summary>
        /// Whether the group has been registered with the overload server.
        /// </summary>
        private bool hasRegistered;

        /// <summary>
        /// The callback function to be called when the forwarding peer become offline.
        /// </summary>
        private OnServiceBecomeOffline onForwradingPeerBecomeOffline;

        /// <summary>
        /// The queue to use for scheduling events.
        /// </summary>
        private NetworkEventQueue eventQueue;

        /// <summary>
        /// The configuration options for the overload system.
        /// </summary>
        private OverloadModule overloadOptions;

        /// <summary>
        /// Provides the public peer address.
        /// </summary>
        private INetworkAddressProvider addressProvider;

        /// <summary>
        /// Initializes a new instance of the <see cref="ForwardingGroup"/> class.
        /// </summary>
        /// <param name="forwarder">The forwarder.</param>
        /// <param name="forwardingPeer">The peer address of the forwarding peer.</param>
        /// <param name="groupId">The group id.</param>
        /// <param name="overloadOptions">The configuration options for the overload system.</param>
        /// <param name="eventQueue">The event queue to schedule events on.</param>
        /// <param name="timeKeeper">The time source.</param>
        /// <param name="addressProvider">Provides the public peer address.</param>
        /// <param name="connectionNotifier">Provides notification of peer connections and disconnections.</param>
        public ForwardingGroup(
            EnvelopeForwarder forwarder,
            PeerAddress forwardingPeer,
            BadumnaId groupId,
            OverloadModule overloadOptions,
            NetworkEventQueue eventQueue,
            ITime timeKeeper,
            INetworkAddressProvider addressProvider,
            IPeerConnectionNotifier connectionNotifier)
            : base(eventQueue)
        {
			// TODO: timeKeeper is not used. Remove.
            this.overloadOptions = overloadOptions;
            this.eventQueue = eventQueue;
            this.addressProvider = addressProvider;

            // always assume the forwarding peer is available unless there is reported error
            this.forwarder = forwarder;
            this.forwardingPeer = forwardingPeer;
            this.groupId = groupId;
            this.forwarder.RegisterForwardingGroup(this.groupId, this);
            this.IsConnected = false;
            this.IsConnecting = false;

            connectionNotifier.ConnectionLostEvent += this.ConnectionLostEvent;

            this.Connect();
        }

        private ForwardingGroupLostEventHandler forwardingGroupLostDelegate;
        public event ForwardingGroupLostEventHandler ForwardingGroupLost
        {
            add { this.forwardingGroupLostDelegate += value; }
            remove { this.forwardingGroupLostDelegate -= value; }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is forwarding peer available.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is forwarding peer available; otherwise, <c>false</c>.
        /// </value>
        public bool IsForwardingPeerAvailable
        {
            get
            {
                if (this.forwardingPeer.Equals(PeerAddress.Nowhere))
                {
                    return false;
                }
                else
                {
                    return this.IsAvailable();
                }
            }
        }

        /// <summary>
        /// Gets the forwarding peer.
        /// </summary>
        /// <value>The forwarding peer.</value>
        public PeerAddress ForwardingPeer
        {
            get { return this.forwardingPeer; }
        }

        /// <summary>
        /// Gets a value indicating whether [confirm connected].
        /// </summary>
        /// <value><c>true</c> if [confirm connected]; otherwise, <c>false</c>.</value>
        public bool ConfirmConnected
        {
            get 
            {
                if (this.forwardingPeer.Equals(PeerAddress.Nowhere))
                {
                    return false;
                }

                if (this.IsConnecting)
                {
                    return false;
                }

                return this.IsConnected;
            }
        }

        /// <summary>
        /// Gets the group id.
        /// </summary>
        /// <value>The group id.</value>
        public BadumnaId GroupId
        {
            get { return this.groupId; }
        }

        /// <summary>
        /// Gets or sets the on forwrading peer become offline callback function.
        /// </summary>
        /// <value>The on forwrading peer become offline.</value>
        public OnServiceBecomeOffline OnForwradingPeerBecomeOffline
        {
            get { return this.onForwradingPeerBecomeOffline; }
            set { this.onForwradingPeerBecomeOffline = value; }
        }

        /// <summary>
        /// Calculates the group hash value.
        /// </summary>
        /// <param name="ids">The registered entity Ids.</param>
        /// <returns>The hash value.</returns>
        public static int CalculateGroupHash(List<BadumnaId> ids)
        {
            string registeredEntitiesString = string.Empty;
            byte[] tmpSource;
            byte[] tmpHash;

            foreach (BadumnaId id in ids)
            {
                registeredEntitiesString += id.ToString();
            }

            tmpSource = ASCIIEncoding.ASCII.GetBytes(registeredEntitiesString);
            tmpHash = new MD5CryptoServiceProvider().ComputeHash(tmpSource);

            if (tmpHash.Length >= 4)
            {
                int length = tmpHash.Length;
                byte[] bytes = new byte[4];
                bytes[0] = tmpHash[length - 1];
                bytes[1] = tmpHash[length - 2];
                bytes[2] = tmpHash[length - 3];
                bytes[3] = tmpHash[length - 4];

                // If the system architecture is little-endian (that is, little end first),
                // reverse the byte array.
                if (BitConverter.IsLittleEndian)
                {
                    Array.Reverse(bytes);
                }

                return BitConverter.ToInt32(bytes, 0);
            }
            else
            {
                throw new InvalidOperationException("Failed to generate the MD5 hash for the forwarding group config.");
            }
        }

        /// <summary>
        /// Switches to new forwarding peer.
        /// </summary>
        /// <param name="address">The peer address of the new forwarding peer.</param>
        public void SwitchToNewForwardingPeer(PeerAddress address)
        {
            this.forwardingPeer = address;
            this.IsConnected = false;

            this.Connect();
        }

        /// <summary>
        /// Imports the specified existing group.
        /// </summary>
        /// <param name="existingGroup">The existing group.</param>
        public void Import(ForwardingGroup existingGroup)
        {
            if (this.hasRegistered)
            {
                Logger.TraceInformation(LogTag.Overload, "Group already registered, import settings aborted.");
                return;
            }

            foreach (BadumnaId id in existingGroup.registeredEntities)
            {
                if (!this.addedEntities.Contains(id) && !existingGroup.removedEntities.Contains(id))
                {
                    this.addedEntities.Add(id);
                }
            }

            foreach (BadumnaId id in existingGroup.addedEntities)
            {
                if (!this.addedEntities.Contains(id) && !existingGroup.removedEntities.Contains(id))
                {
                    this.addedEntities.Add(id);
                }
            }
        }

        /// <summary>
        /// Adds the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        public void Add(BadumnaId entity)
        {
            if (this.removedEntities.Contains(entity))
            {
                this.removedEntities.Remove(entity);
                return;
            }

            if (this.registeredEntities.Contains(entity) || this.addedEntities.Contains(entity))
            {
                return;
            }

            this.addedEntities.Add(entity);
        }

        /// <summary>
        /// Removes the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        public void Remove(BadumnaId entity)
        {
            if (this.addedEntities.Contains(entity))
            {
                this.addedEntities.Remove(entity);
                return;
            }

            if (!this.registeredEntities.Contains(entity) || this.removedEntities.Contains(entity))
            {
                return;
            }

            this.removedEntities.Add(entity);
        }

        /// <summary>
        /// Removes all.
        /// </summary>
        public void RemoveAll()
        {
            this.addedEntities.Clear();

            foreach (BadumnaId entity in this.registeredEntities)
            {
                this.Remove(entity);
            }
        }

        /// <summary>
        /// Sets the forwarding peer online confirmed.
        /// </summary>
        public void SetForwardingPeerOnlineConfirmed()
        {
            if (!this.IsConnecting)
            {
                Logger.TraceInformation(LogTag.Overload, "Received a forwarding peer online confirmation message when not connecting, message ignored.");
                return;
            }

            this.IsConnected = true;
            this.IsConnecting = false;
            this.CancelConnectionTimeoutEvent();
        }

        /// <summary>
        /// Forwards the envelope to all registered entities in the group.
        /// </summary>
        /// <param name="payload">The payload.</param>
        public void ForwardEnvelope(EntityEnvelope payload)
        {
            if (!this.IsForwardingPeerAvailable)
            {
                // the forwarding peer points to PeerAddress.Nowhere, maybe because the distributed lookup service has not returned any
                // result yet, just ignore the request for now.  
                return;
            }

            TransportEnvelope forwarded = this.MakeUpdateGroupEnvelope();
            this.forwarder.Protocol.RemoteCall(forwarded, this.forwarder.ForwardToGroup, this.groupId, payload, this.groupConfigHash);
            this.forwarder.Protocol.SendMessage(forwarded);
        }

        /// <summary>
        /// Sends the registered entities to the forwarding peer to synchronize the settings.
        /// </summary>
        public void SendRegisteredEntities()
        {
            QualityOfService qos = this.MakeQos(this.OnForwardingPeerFailure);
            TransportEnvelope envelope = this.forwarder.Protocol.GetMessageFor(this.forwardingPeer, qos);
            this.forwarder.Protocol.RemoteCall(envelope, this.forwarder.CreateForwardingGroup, this.groupId, this.registeredEntities);
            this.forwarder.Protocol.SendMessage(envelope);
        }

        /// <summary>
        /// Connections lost event handler.
        /// </summary>
        /// <param name="address">The peer address of the connection that is lost.</param>
        private void ConnectionLostEvent(PeerAddress address)
        {
            if (address.Equals(this.forwardingPeer))
            {
                if (this.IsConnected)
                {
                    if (this.overloadOptions.IsDistributedLookupUsed)
                    {
                        this.OnForwardingPeerFailure();
                    }
                    else
                    {
                        // no longer connected
                        this.IsConnected = false;
                        // try reconnect after a while
                        this.ScheduleReconnect();
                    }

                    ForwardingGroupLostEventHandler handler = this.forwardingGroupLostDelegate;
                    if (handler != null && this.addedEntities != null)
                    {
                        handler.Invoke(this.registeredEntities);
                    }
                }
            }
        }

        /// <summary>
        /// Schedules to reconnect.
        /// </summary>
        private void ScheduleReconnect()
        {
            this.eventQueue.Schedule(Parameters.OverloadReconnectInterval.TotalMilliseconds, this.Connect);
        }

        /// <summary>
        /// Called on the failure of the forwarding peer.
        /// </summary>
        private void OnForwardingPeerFailure()
        {
            this.IsConnected = false;

            if (null != this.onForwradingPeerBecomeOffline)
            {
                ServiceDescription serviceType = new ServiceDescription(ServerType.Overload);
                this.onForwradingPeerBecomeOffline(serviceType);
            }
        }

        /// <summary>
        /// Makes and returns a FailureHandlerQos.
        /// </summary>
        /// <param name="forwardingPeerFailure">The forwarding peer failure callback function.</param>
        /// <returns>Failure handler qos.</returns>
        private QualityOfService MakeQos(GenericCallBack forwardingPeerFailure)
        {
            // must be reliable qos here.
            QualityOfService baseQos = QualityOfService.Reliable;

            if (forwardingPeerFailure == null)
            {
                return baseQos;
            }

            FailHandlerQos errorHandledQos = new FailHandlerQos(QualityOfService.Reliable, new SendFailureEventArgs());
            errorHandledQos.NumberOfAttempts = Parameters.MaximumNumberOfRetries;

            errorHandledQos.FailureEvent += delegate(object sender, SendFailureEventArgs e)
            {
            };

            return errorHandledQos;
        }

        /// <summary>
        /// Makes the update group envelope.
        /// </summary>
        /// <returns>Transport envelope.</returns>
        private TransportEnvelope MakeUpdateGroupEnvelope()
        {
            QualityOfService qos = this.MakeQos(this.OnForwardingPeerFailure);
            TransportEnvelope envelope = this.forwarder.Protocol.GetMessageFor(this.forwardingPeer, qos);

            if (!this.hasRegistered)
            {
                this.forwarder.Protocol.RemoteCall(envelope, this.forwarder.CreateForwardingGroup, this.groupId, this.addedEntities);
                this.hasRegistered = true;
                this.registeredEntities = this.addedEntities;
                this.groupConfigHash = ForwardingGroup.CalculateGroupHash(this.registeredEntities);
                this.addedEntities = new List<BadumnaId>();
                Debug.Assert(this.removedEntities.Count == 0, "removed entities's count is not zero.");
            }
            else if (this.removedEntities.Count > 0)
            {
                this.forwarder.Protocol.RemoteCall(envelope, this.forwarder.RemoveFromForwardingGroup, this.groupId, this.removedEntities);
                foreach (BadumnaId entity in this.removedEntities)
                {
                    this.registeredEntities.Remove(entity);
                }

                this.groupConfigHash = ForwardingGroup.CalculateGroupHash(this.registeredEntities);
                this.removedEntities.Clear();
            }
            else if (this.addedEntities.Count > 0)
            {
                this.forwarder.Protocol.RemoteCall(envelope, this.forwarder.AddToForwardingGroup, this.groupId, this.addedEntities);
                this.registeredEntities.AddRange(this.addedEntities);
                this.groupConfigHash = ForwardingGroup.CalculateGroupHash(this.registeredEntities);
                this.addedEntities.Clear();
            }

            return envelope;
        }

        /// <summary>
        /// Pings the forwarding peer.
        /// </summary>
        private void Connect()
        {
            if (this.forwardingPeer.Equals(PeerAddress.Nowhere))
            {
                return;
            }

            this.IsConnecting = true;
            this.ScheduleConnectionTimeoutEvent();

            TransportEnvelope envelope = this.forwarder.Protocol.GetMessageFor(this.forwardingPeer, QualityOfService.Reliable);
            this.forwarder.Protocol.RemoteCall(envelope, this.forwarder.ReceiveForwardingGroupPing, this.addressProvider.PublicAddress, this.groupId);
            this.forwarder.Protocol.SendMessage(envelope);
        }

        protected override void ConnectionTimeoutHandler()
        {
            this.ScheduleReconnect();
        }

        /// <summary>
        /// Gets the added entities.
        /// </summary>
        /// <returns>A list of added entities.</returns>
        public List<BadumnaId> GetAddedEntities()
        {
            return this.addedEntities;
        }
    }
}