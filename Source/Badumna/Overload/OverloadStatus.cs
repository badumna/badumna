﻿//---------------------------------------------------------------------------------
// <copyright file="OverloadStatus.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

using System;
using Badumna.Core;

namespace Badumna.Overload
{
    /// <summary>
    /// The status of the overload server.
    /// </summary>
    internal class OverloadStatus
    {
        /// <summary>
        /// The number of peers sending their updates to the overload server.
        /// </summary>
        private int numberOfClients;

        /// <summary>
        /// The number of peers receving updates from the overload server. 
        /// </summary>
        private int numberOfReceivingPeers;

        /// <summary>
        /// The number of total incoming updates received in the last interval;
        /// </summary>
        private int numberOfIncomingUpdates;

        /// <summary>
        /// The number of total outgoing updates sent in the last interval;
        /// </summary>
        private int numberOfOutgoingUpdates;

        /// <summary>
        /// The interval.
        /// </summary>
        private TimeSpan interval;

        /// <summary>
        /// Last report time.  
        /// </summary>
        private TimeSpan lastReportTime;

        /// <summary>
        /// Provides access to the current time.
        /// </summary>
        private ITime timeKeeper;

        /// <summary>
        /// Initializes a new instance of the <see cref="OverloadStatus"/> class.
        /// </summary>
        /// <param name="timeKeeper">Provides access to the current time.</param>
        public OverloadStatus(ITime timeKeeper)
        {
            this.timeKeeper = timeKeeper;
            this.lastReportTime = this.timeKeeper.Now;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="OverloadStatus"/> class, copying
        /// the details of the given instance.
        /// </summary>
        /// <param name="other">The instance to copy details from.</param>
        public OverloadStatus(OverloadStatus other)
        {
            this.timeKeeper = other.timeKeeper;
            this.lastReportTime = other.lastReportTime;
            this.interval = other.interval;

            this.NumberOfClients = other.NumberOfClients;
            this.NumberOfIncomingUpdates = other.NumberOfIncomingUpdates;
            this.NumberOfOutgoingUpdates = other.NumberOfOutgoingUpdates;
            this.NumberofReceivingPeers = other.NumberofReceivingPeers;
        }

        /// <summary>
        /// Gets or sets the number of clients.
        /// </summary>
        /// <value>The number of clients.</value>
        public int NumberOfClients
        {
            get { return this.numberOfClients; }
            set { this.numberOfClients = value; }
        }

        /// <summary>
        /// Gets or sets the numberof receiving peers.
        /// </summary>
        /// <value>The numberof receiving peers.</value>
        public int NumberofReceivingPeers
        {
            get { return this.numberOfReceivingPeers; }
            set { this.numberOfReceivingPeers = value; }
        }

        /// <summary>
        /// Gets or sets the number of incoming updates.
        /// </summary>
        /// <value>The number of incoming updates.</value>
        public int NumberOfIncomingUpdates
        {
            get { return this.numberOfIncomingUpdates; }
            set { this.numberOfIncomingUpdates = value; }
        }

        /// <summary>
        /// Gets or sets the number of outgoing updates.
        /// </summary>
        /// <value>The number of outgoing updates.</value>
        public int NumberOfOutgoingUpdates
        {
            get { return this.numberOfOutgoingUpdates; }
            set { this.numberOfOutgoingUpdates = value; }
        }

        /// <summary>
        /// Gets the interval seconds.
        /// </summary>
        /// <value>The interval seconds.</value>
        public double IntervalSeconds
        {
            get { return this.interval.TotalSeconds; }
        }

        /// <summary>
        /// Updates the interval stats.
        /// </summary>
        public void UpdateInterval()
        {
            this.interval = this.timeKeeper.Now - this.lastReportTime;
            this.lastReportTime = this.timeKeeper.Now;
        }
    }
}
