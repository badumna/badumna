﻿//---------------------------------------------------------------------------------
// <copyright file="EnvelopeForwarder.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Diagnostics;

using Badumna.Core;
using Badumna.DataTypes;
using Badumna.Replication;
using Badumna.Transport;
using Badumna.Utilities;

namespace Badumna.Overload
{
    /// <summary>
    /// The envelope forwarder is used to forward the update envelope from the original entity to the overload server.
    /// then the overload server (i.e. EnvelopeForwarder) will use the entity forwarder to forward the envelope to
    /// all involved replicas.
    /// </summary>
    internal class EnvelopeForwarder
    {
        /// <summary>
        /// The entity forwarder.
        /// </summary>
        private EntityForwarder entityForwarder;

        /// <summary>
        /// The parent transport protocol.
        /// </summary>
        private TransportProtocol parent;

        /// <summary>
        /// The regular task for garbage collection.
        /// </summary>
        private RegularTask garbageCollectionTask;

        /// <summary>
        /// The regular task used to collect overload server stats.
        /// </summary>
        private RegularTask statsCollectionTask;

        /// <summary>
        /// The overload server stats.
        /// </summary>
        private OverloadStatus overloadServerStatus;

        /// <summary>
        /// All known groups and their settings. This collection is only valid on the overload server. 
        /// </summary>
        private Dictionary<BadumnaId, GroupConfig> groups = new Dictionary<BadumnaId, GroupConfig>();

        /// <summary>
        /// Associated groups. This collection is only valid on regular peers that have UseOverload option enabled. 
        /// </summary>
        private Dictionary<BadumnaId, ForwardingGroup> associatedForwardGroups = new Dictionary<BadumnaId, ForwardingGroup>();

        /// <summary>
        /// Provides access to the current time.
        /// </summary>
        private ITime timeKeeper;

        /// <summary>
        /// The configuration options for the overload system.
        /// </summary>
        private OverloadModule overloadOptions;

        /// <summary>
        /// Initializes a new instance of the <see cref="EnvelopeForwarder"/> class.
        /// </summary>
        /// <param name="parent">The parent transport protocol.</param>
        /// <param name="connectionTable">The connection table.</param>
        /// <param name="overloadOptions">The overload configuration options.</param>
        /// <param name="eventQueue">The event queue to schedule events on.</param>
        /// <param name="timeKeeper">The time source.</param>
        /// <param name="connectivityReporter">Reports on network connectivity status.</param>
        /// <param name="addressProvider">Provides the current public address.</param>
        public EnvelopeForwarder(TransportProtocol parent, ConnectionTable connectionTable, OverloadModule overloadOptions, NetworkEventQueue eventQueue, ITime timeKeeper, INetworkConnectivityReporter connectivityReporter, INetworkAddressProvider addressProvider)
            : this(parent, connectionTable.ProcessConnectionfulMessageBody, overloadOptions, eventQueue, timeKeeper, connectivityReporter, addressProvider)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="EnvelopeForwarder"/> class.
        /// </summary>
        /// <param name="parent">The parent transport protocol.</param>
        /// <param name="processMessage">The process message callback function.</param>
        /// <param name="overloadOptions">The overload configuration options.</param>
        /// <param name="eventQueue">The event queue to schedule events on.</param>
        /// <param name="timeKeeper">The time source.</param>
        /// <param name="connectivityReporter">Reports on network connectivity status.</param>
        /// <param name="addressProvider">Provides the current public address.</param>
        public EnvelopeForwarder(
            TransportProtocol parent,
            GenericCallBack<TransportEnvelope, PeerAddress, PeerAddress> processMessage,
            OverloadModule overloadOptions,
            NetworkEventQueue eventQueue,
            ITime timeKeeper,
            INetworkConnectivityReporter connectivityReporter,
            INetworkAddressProvider addressProvider)
        {
			// TODO: processMessage parameter is not used. Remove it?
            this.timeKeeper = timeKeeper;

            this.overloadOptions = overloadOptions;

            this.entityForwarder = new EntityForwarder(parent, new EntityRouterFactory(), addressProvider);
            this.parent = parent;
            this.parent.Parser.RegisterMethodsIn(this);

            this.garbageCollectionTask = new RegularTask("envelope_forwarder_garbage_collection_task", Parameters.OverloadGarbageCollectionTaskInterval, eventQueue, connectivityReporter, this.GarbageCollection);
            this.garbageCollectionTask.Start();

            if (this.overloadOptions.IsServer)
            {
                this.overloadServerStatus = new OverloadStatus(this.timeKeeper);
                this.statsCollectionTask = new RegularTask("overload_server_stats_collection_task", Parameters.OverloadStatsCollectionTaskInterval, eventQueue, connectivityReporter, this.CollectOverloadServerStats);
                this.statsCollectionTask.Start();
            }
        }

        /// <summary>
        /// Gets the overload status.
        /// </summary>
        /// <value>The overload status.</value>
        public OverloadStatus OverloadStatus
        {
            get { return this.overloadServerStatus; }
        }

        /// <summary>
        /// Gets the parent transport protocol.
        /// </summary>
        /// <value>The protocol.</value>
        public TransportProtocol Protocol 
        { 
            get { return this.parent; } 
        }

        /// <summary>
        /// Sets the entity manager.
        /// </summary>
        /// <value>The entity manager.</value>
        public EntityManager EntityManager
        {
            set { this.entityForwarder.EntityManager = value; }
        }

        /// <summary>
        /// Gets the groups.
        /// </summary>
        /// <value>The groups indexed by their group id.</value>
        public Dictionary<BadumnaId, GroupConfig> Groups
        {
            get { return this.groups; }
        }

        /// <summary>
        /// Gets the forward groups.
        /// </summary>
        /// <value>The forward groups indexed by their group ids.</value>
        public Dictionary<BadumnaId, ForwardingGroup> ForwardGroups
        {
            get { return this.associatedForwardGroups; }
        }

        /// <summary>
        /// Shutdowns this instance.
        /// </summary>
        public void Shutdown()
        {
            if (this.garbageCollectionTask != null && this.garbageCollectionTask.IsRunning)
            {
                this.garbageCollectionTask.Stop();
            }

            if (this.statsCollectionTask != null && this.statsCollectionTask.IsRunning)
            {
                this.statsCollectionTask.Stop();
            }
        }

        /// <summary>
        /// Registers the forwarding group.
        /// </summary>
        /// <param name="id">The id of the group.</param>
        /// <param name="group">The group.</param>
        public void RegisterForwardingGroup(BadumnaId id, ForwardingGroup group)
        {
            if (!this.associatedForwardGroups.ContainsKey(id))
            {
                this.associatedForwardGroups[id] = group;
            }
            else
            {
                Logger.TraceInformation(LogTag.Overload, "The forwarding group with id {0} has already been registered.", id);
            }
        }

        /// <summary>
        /// Creates a new forwarding group.
        /// </summary>
        /// <param name="groupId">The group id.</param>
        /// <param name="destinations">The destinations.</param>
        /// <remarks>
        /// The forwarding group is uniquely identified by the public address of
        /// the group's creator and the numeric id supplied by the creator.
        /// </remarks>
        [ConnectionfulProtocolMethod(ConnectionfulMethod.CreateForwardingGroup)]
        public void CreateForwardingGroup(BadumnaId groupId, List<BadumnaId> destinations)
        {
            if (!this.overloadOptions.IsServer)
            {
                return;
            }

            GroupConfig config = new GroupConfig(destinations, this.timeKeeper);
            this.groups[groupId] = config;
        }

        /// <summary>
        /// Adds new destinations to the specified forwarding group.
        /// </summary>
        /// <param name="groupId">The group id.</param>
        /// <param name="newDestinations">The new destinations.</param>
        [ConnectionfulProtocolMethod(ConnectionfulMethod.AddToForwardingGroup)]
        public void AddToForwardingGroup(BadumnaId groupId, List<BadumnaId> newDestinations)
        {
            if (!this.overloadOptions.IsServer)
            {
                return;
            }

            GroupConfig config;
            if (!this.groups.TryGetValue(groupId, out config))
            {
                // if the group identified by the group id doesn't exist, it will be created here. 
                // it is possible to have inconsistent configurations but such inconsistency will be 
                // detected and corrected when the group is actually used (i.e. when ForwardToGroup is called). 
                config = new GroupConfig(this.timeKeeper);
                this.groups[groupId] = config;
            }

            config.AddEntities(newDestinations);
        }

        /// <summary>
        /// Removes destinations from the specified forwarding group.
        /// </summary>
        /// <param name="groupId">The group id.</param>
        /// <param name="oldDestinations">The old destinations.</param>
        [ConnectionfulProtocolMethod(ConnectionfulMethod.RemoveFromForwardingGroup)]
        public void RemoveFromForwardingGroup(BadumnaId groupId, List<BadumnaId> oldDestinations)
        {
            if (!this.overloadOptions.IsServer)
            {
                return;
            }

            GroupConfig config;
            if (!this.groups.TryGetValue(groupId, out config))
            {
                return;
            }

            config.RemoveEntities(oldDestinations);
        }

        /// <summary>
        /// Removes the forwarding group specified by the group id.
        /// </summary>
        /// <param name="groupId">The group id.</param>
        [ConnectionfulProtocolMethod(ConnectionfulMethod.RemoveForwardingGroup)]
        public void RemoveForwardingGroup(BadumnaId groupId)
        {
            if (!this.overloadOptions.IsServer)
            {
                return;
            }

            if (this.groups.ContainsKey(groupId))
            {
                this.groups.Remove(groupId);
        }
        }

        /// <summary>
        /// Forwards the update payload to the specified group.
        /// </summary>
        /// <param name="groupId">The group id.</param>
        /// <param name="envelope">The update payload envelope.</param>
        /// <param name="registeredEntitiesHash">The registered entities hash.</param>
        [ConnectionfulProtocolMethod(ConnectionfulMethod.ForwardToGroup)]
        public void ForwardToGroup(BadumnaId groupId, EntityEnvelope envelope, int registeredEntitiesHash)
        {
            if (!this.overloadOptions.IsServer)
            {
                return;
            }

            // now enforce the forward to group envelope to be reliable message.
            // we need the ack to notify the source peer when the forwarding peer becomes unavailable. 
            // Debug.Assert(this.Protocol.CurrentEnvelope.Qos.IsReliable, "ForwardingGroup only supports reliable messages");
            PeerAddress address = this.Protocol.CurrentEnvelope.Source;

            GroupConfig config;
            if (!this.groups.TryGetValue(groupId, out config))
            {
                this.RequestToSendRegisteredEntities(address, groupId);
                return;
            }

            if (config.GroupConfigHash != registeredEntitiesHash)
            {
                this.RequestToSendRegisteredEntities(address, groupId);
                return;
            }

            this.UpdateLastForwardTime(groupId);

            // set the source, then forward it to the entity forwarder which will forward the payload to
            // all group members. 
            envelope.SourceEntity = groupId;
            config.RecordIncomingUpdates();
            this.entityForwarder.ForwardToEntities(envelope, config.GroupEntities);
            config.RecordOutgoingUpdates();
        }

        /// <summary>
        /// Handles the forwarding group ping.
        /// </summary>
        /// <param name="source">The peer address of the source peer that sent the ping.</param>
        /// <param name="groupId">The group id.</param>
        [ConnectionfulProtocolMethod(ConnectionfulMethod.ReceiveForwardingGroupPing)]
        public void ReceiveForwardingGroupPing(PeerAddress source, BadumnaId groupId)
        {
            if (!this.overloadOptions.IsServer)
            {
                return;
            }

            TransportEnvelope transportEnvelope = this.parent.GetMessageFor(source, QualityOfService.Reliable);
            this.Protocol.RemoteCall(transportEnvelope, this.ReceiveForwardingGroupPong, groupId);
            this.Protocol.SendMessage(transportEnvelope);
        }

        /// <summary>
        /// Get the group setting, for unit testing only
        /// </summary>
        /// <returns></returns>
        public List<BadumnaId> GetGroup()
        {
            if (groups.Count == 0)
            {
                return null;
            }

            List<BadumnaId> keys = new List<BadumnaId>(groups.Keys);
            BadumnaId firstKey = keys[0];
            return groups[firstKey].GroupEntities;
        }

        /// <summary>
        /// Handles the forwarding group pong.
        /// </summary>
        /// <param name="groupId">The group id.</param>
        [ConnectionfulProtocolMethod(ConnectionfulMethod.ReceiveForwardingGroupPong)]
        private void ReceiveForwardingGroupPong(BadumnaId groupId)
        {
            if (this.associatedForwardGroups.ContainsKey(groupId))
            {
                this.associatedForwardGroups[groupId].SetForwardingPeerOnlineConfirmed();
            }
        }

        /// <summary>
        /// Request the specified peer to send the registered entities.
        /// </summary>
        /// <param name="address">The peer address.</param>
        /// <param name="groupId">The group id.</param>
        private void RequestToSendRegisteredEntities(PeerAddress address, BadumnaId groupId)
        {
            TransportEnvelope envelope = this.Protocol.GetMessageFor(address, QualityOfService.Reliable);
            this.Protocol.RemoteCall(envelope, this.SendRegisteredEntities, groupId);
            this.Protocol.SendMessage(envelope);
        }

        /// <summary>
        /// Sends the registered entities.
        /// </summary>
        /// <param name="groupId">The group id.</param>
        [ConnectionfulProtocolMethod(ConnectionfulMethod.SendRegisteredEntities)]
        private void SendRegisteredEntities(BadumnaId groupId)
        {
            ForwardingGroup group = null;
            if (!this.associatedForwardGroups.TryGetValue(groupId, out group))
            {
                return;
            }

            group.SendRegisteredEntities();
        }

        /// <summary>
        /// Updates the time when last forwarding operation is performed.
        /// </summary>
        /// <param name="groupId">The group key.</param>
        protected void UpdateLastForwardTime(BadumnaId groupId)
        {
            GroupConfig group;
            if (this.groups.TryGetValue(groupId, out group))
            {
                group.LastForwardTime = this.timeKeeper.Now;
            }
        }

        /// <summary>
        /// Garbage collection task to remove redundent forwarding groups.
        /// </summary>
        private void GarbageCollection()
        {
            List<BadumnaId> toRemove = new List<BadumnaId>();
            foreach (BadumnaId key in this.groups.Keys)
            {
                if (this.timeKeeper.Now - this.groups[key].LastForwardTime >= Parameters.OverloadGroupTTL)
                {
                    toRemove.Add(key);
                }
            }

            foreach (BadumnaId key in toRemove)
            {
                this.groups.Remove(key);
            }
        }

        private void CollectOverloadServerStats()
        {
            int numberOfGroups = this.groups.Count;
            int numberOfReceivingPeers = 0;
            int totalIncomingUpdates = 0;
            int totalOutgoingUpdates = 0;

            foreach(GroupConfig currentGroup in this.groups.Values)
            {
                numberOfReceivingPeers += currentGroup.EntityCount;
                totalIncomingUpdates += currentGroup.IncomingUpdates;
                totalOutgoingUpdates += currentGroup.OutgoingUpdates;

                currentGroup.ResetStats();
            }

            this.overloadServerStatus.NumberOfClients = numberOfGroups;
            this.overloadServerStatus.NumberofReceivingPeers = numberOfReceivingPeers;
            this.overloadServerStatus.NumberOfIncomingUpdates = totalIncomingUpdates;
            this.overloadServerStatus.NumberOfOutgoingUpdates = totalOutgoingUpdates;
            this.overloadServerStatus.UpdateInterval();
        }
    }
}
