﻿//-----------------------------------------------------------------------
// <copyright file="IForwardingGroup.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System.Collections.Generic;
using Badumna.Core;
using Badumna.DataTypes;
using Badumna.Replication;
using Badumna.ServiceDiscovery;

namespace Badumna.Overload
{
    /// <summary>
    /// The forwarding group lost event handler delegate.
    /// </summary>
    /// <param name="entities">The added entities.</param>
    internal delegate void ForwardingGroupLostEventHandler(List<BadumnaId> entities);

    /// <summary>
    /// document this class.
    /// </summary>
    internal interface IForwardingGroup
    {
        /// <summary>
        /// Occurs when forwarding group lost.
        /// </summary>
        event ForwardingGroupLostEventHandler ForwardingGroupLost;
        
        /// <summary>
        /// Gets a value indicating whether this instance is forwarding peer available.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is forwarding peer available; otherwise, <c>false</c>.
        /// </value>
        bool IsForwardingPeerAvailable
        {
            get;
        }

        /// <summary>
        /// Gets a value indicating whether [confirm connected].
        /// </summary>
        /// <value><c>true</c> if [confirm connected]; otherwise, <c>false</c>.</value>
        bool ConfirmConnected
        {
            get;
        }

        /// <summary>
        /// Gets or sets the on forwrading peer become offline.
        /// </summary>
        /// <value>The on forwrading peer become offline.</value>
        OnServiceBecomeOffline OnForwradingPeerBecomeOffline
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the forwarding peer.
        /// </summary>
        /// <value>The forwarding peer.</value>
        PeerAddress ForwardingPeer
        {
            get;
        }

        /// <summary>
        /// Gets the group id of the forwarding group.
        /// </summary>
        /// <value>The group id.</value>
        BadumnaId GroupId
        {
            get;
        }

        /// <summary>
        /// Adds the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        void Add(BadumnaId entity);

        /// <summary>
        /// Removes the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        void Remove(BadumnaId entity);

        /// <summary>
        /// Removes all entities.
        /// </summary>
        void RemoveAll();

        /// <summary>
        /// Forwards the envelope.
        /// </summary>
        /// <param name="payload">The payload to be forwarded.</param>
        void ForwardEnvelope(EntityEnvelope payload);

        /// <summary>
        /// Switches to new forwarding peer.
        /// </summary>
        /// <param name="address">The address of the new forwarding peer.</param>
        void SwitchToNewForwardingPeer(PeerAddress address);
    }
}