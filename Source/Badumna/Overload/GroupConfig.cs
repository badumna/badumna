﻿//---------------------------------------------------------------------------------
// <copyright file="GroupConfig.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

using System;
using System.Collections.Generic;

using Badumna.Core;
using Badumna.DataTypes;

namespace Badumna.Overload
{
    /// <summary>
    /// The group config class is a container class that contains all required data for a forwarding group
    /// </summary>
    internal class GroupConfig
    {
        /// <summary>
        /// A list of entity Ids contained in the group.
        /// </summary>
        private List<BadumnaId> groupEntities;

        /// <summary>
        /// The group configuration hash value.
        /// </summary>
        private int groupConfigHash;

        /// <summary>
        /// When is the last forward time for the group.
        /// </summary>
        private TimeSpan lastForwardTime;

        /// <summary>
        /// The number of update received.
        /// </summary>
        private int incomingUpdates;

        /// <summary>
        /// The number of update sent. 
        /// </summary>
        private int outgoingUpdates;

        /// <summary>
        /// Initializes a new instance of the <see cref="GroupConfig"/> class.
        /// </summary>
        public GroupConfig(ITime timeKeeper)
        {
            this.groupEntities = new List<BadumnaId>();
            this.groupConfigHash = ForwardingGroup.CalculateGroupHash(this.groupEntities);
            this.lastForwardTime = timeKeeper.Now;

            this.incomingUpdates = 0;
            this.outgoingUpdates = 0;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GroupConfig"/> class.
        /// </summary>
        /// <param name="ids">The badumna id list.</param>
        /// <param name="timeKeeper">The time source.</param>
        public GroupConfig(List<BadumnaId> ids, ITime timeKeeper)
        {
            this.groupEntities = ids;
            this.groupConfigHash = ForwardingGroup.CalculateGroupHash(this.groupEntities);
            this.lastForwardTime = timeKeeper.Now;
        }

        /// <summary>
        /// Gets the group entities.
        /// </summary>
        /// <value>The group entities.</value>
        public List<BadumnaId> GroupEntities
        {
            get { return this.groupEntities; }
        }

        /// <summary>
        /// Gets the the number of entities in the group.
        /// </summary>
        /// <value>The entity count.</value>
        public int EntityCount
        {
            get { return this.groupEntities.Count; }
        }

        /// <summary>
        /// Gets the group config hash.
        /// </summary>
        /// <value>The group config hash.</value>
        public int GroupConfigHash
        {
            get { return this.groupConfigHash; }
        }

        /// <summary>
        /// Gets or sets the last forward time.
        /// </summary>
        /// <value>The last forward time.</value>
        public TimeSpan LastForwardTime
        {
            get { return this.lastForwardTime; }
            set { this.lastForwardTime = value; }
        }

        /// <summary>
        /// Gets the the number of incoming updates.
        /// </summary>
        /// <value>The number of incoming updates.</value>
        public int IncomingUpdates
        {
            get { return this.incomingUpdates; }
        }

        /// <summary>
        /// Gets the number of outgoing updates.
        /// </summary>
        /// <value>The number of outgoing updates.</value>
        public int OutgoingUpdates
        {
            get { return this.outgoingUpdates; }
        }

        /// <summary>
        /// Records the incoming updates.
        /// </summary>
        public void RecordIncomingUpdates()
        {
            this.incomingUpdates++;
        }

        /// <summary>
        /// Records the outgoing updates.
        /// </summary>
        public void RecordOutgoingUpdates()
        {
            this.outgoingUpdates += this.EntityCount;
        }

        /// <summary>
        /// Adds the entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        public void AddEntity(BadumnaId entity)
        {
            if (!this.GroupEntities.Contains(entity))
            {
                this.GroupEntities.Add(entity);
            }

            this.groupConfigHash = ForwardingGroup.CalculateGroupHash(this.groupEntities);
        }

        /// <summary>
        /// Adds the entities.
        /// </summary>
        /// <param name="ids">The badumna id to be added.</param>
        public void AddEntities(List<BadumnaId> ids)
        {
            foreach (BadumnaId id in ids)
            {
                if (!this.GroupEntities.Contains(id))
                {
                    this.GroupEntities.Add(id);
                }
            }

            this.groupConfigHash = ForwardingGroup.CalculateGroupHash(this.groupEntities);
        }

        /// <summary>
        /// Removes the entities.
        /// </summary>
        /// <param name="ids">The badumna ids to remove.</param>
        public void RemoveEntities(List<BadumnaId> ids)
        {
            List<BadumnaId> toRemove = new List<BadumnaId>();
            foreach (BadumnaId id in ids)
            {
                if (this.GroupEntities.Contains(id))
                {
                    toRemove.Add(id);
                }
            }

            foreach (BadumnaId id in toRemove)
            {
                this.groupEntities.Remove(id);
            }

            this.groupConfigHash = ForwardingGroup.CalculateGroupHash(this.groupEntities);
        }

        /// <summary>
        /// Resets the stats.
        /// </summary>
        public void ResetStats()
        {
            this.incomingUpdates = 0;
            this.outgoingUpdates = 0;
        }
    }
}