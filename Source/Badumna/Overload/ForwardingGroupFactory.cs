﻿//-----------------------------------------------------------------------
// <copyright file="ForwardingGroupFactory.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using Badumna.Core;
using Badumna.DataTypes;

namespace Badumna.Overload
{
    /// <summary>
    /// Document this class.
    /// </summary>
    internal class ForwardingGroupFactory : IForwardingGroupFactory
    {
        /// <inheritdoc/>
        [DontPerformCoverage]
        public IForwardingGroup Create(
            EnvelopeForwarder forwarder,
            PeerAddress overloadPeerAddress,
            BadumnaId groupId,
            OverloadModule overloadOptions,
            NetworkEventQueue eventQueue,
            ITime timeKeeper,
            INetworkAddressProvider addressProvider,
            IPeerConnectionNotifier connectionNotifier)
        {
            return new ForwardingGroup(forwarder, overloadPeerAddress, groupId, overloadOptions, eventQueue, timeKeeper, addressProvider, connectionNotifier);
        }
    }
}