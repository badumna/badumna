﻿//-----------------------------------------------------------------------
// <copyright file="IEnvelopeForwarderFactory.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using Badumna.Core;
using Badumna.Transport;

namespace Badumna.Overload
{
    /// <summary>
    /// document this interface.
    /// </summary>
    internal interface IEnvelopeForwarderFactory
    {
        /// <summary>
        /// Creates the specified transport protocol.
        /// </summary>
        /// <param name="transportProtocol">The transport protocol.</param>
        /// <param name="connectionTable">The connection table.</param>
        /// <param name="overloadOptions">The configuration options for the overload system.</param>
        /// <param name="eventQueue">The queue to use for scheduling events.</param>
        /// <param name="timeKeeper">Provides access to the current time.</param>
        /// <param name="connectivityReporter">Reports on network connectivity status.</param>
        /// <param name="addressProvider">Provides the current public address.</param>
        /// <returns>The envelope forwarder.</returns>
        EnvelopeForwarder Create(TransportProtocol transportProtocol, ConnectionTable connectionTable, OverloadModule overloadOptions, NetworkEventQueue eventQueue, ITime timeKeeper, INetworkConnectivityReporter connectivityReporter, INetworkAddressProvider addressProvider);
    }
}