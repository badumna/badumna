﻿using System;
using System.Collections.Generic;
using System.Text;

using Badumna.Core;

namespace Badumna.Transport
{
    /// <summary>
    /// The Ids for channel groups on the connection scheduler. If you add categories to this enum,
    /// make sure you create the group on the Connection and DhtTransport schedulers (see their constructors).
    /// </summary>
    enum ChannelGroup
    {
        NoGroup = -1, // Should be less than zero (must NOT be used as an index)
        EntityUpdates = 0,
        Streaming = 1,
    }

    interface IChannelFunnel<EnvelopeType> where EnvelopeType : BaseEnvelope
    {
        /// <summary>
        /// Returns the amount of bandwidth that a new channel of the specified weight would currently receive.
        /// This value is smoothed over recent time.  If the peerAddress is not known this will return
        /// double.PositiveInfinity indicating that there is probably some non-zero amount of bandwidth available
        /// and a connection should be attempted.
        /// </summary>
        double GetAvailableBandwidth(PeerAddress peerAddress, double newChannelWeight);

        /// <summary>
        /// This gets the loss rate for a given connection.
        /// </summary>
        /// <remarks>
        /// This is currently used by OverloadSwitchingManager to determine when to switch.
        /// This is a HACK, and should be removed (see comment in OverloadSwitchingManager).
        /// </remarks>
        /// <param name="address">The address of the connection</param>
        /// <returns>The connection's loss rate</returns>
        double GetConnectionLossRate(PeerAddress address);

        /// <summary>
        /// Gets the total outbound traffic from this peer in bytes/second.
        /// </summary>
        /// <returns>The total outbound traffic (i.e. total bytes sent per second).</returns>
        double GetTotalOutboundTraffic();

        void AddChannel(PeerAddress peerAddress, ISource<EnvelopeType> channel, ChannelGroup group);
        void RemoveChannel(PeerAddress peerAddress, ISource<EnvelopeType> channel);
    }
}
