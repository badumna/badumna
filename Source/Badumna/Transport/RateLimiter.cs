﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;

using Badumna.Core;
using Badumna.Utilities;

namespace Badumna.Transport
{
    class RateLimiter<T> where T : ISized
    {
        /// <summary>
        /// The time that we'll retrigger after if Gate failed.  Have to make sure we do retrigger
        /// because otherwise the rate limiter will stop altogether (the only other trigger is
        /// an empty -> non-empty event, but that won't happen because we're not removing items).
        /// Don't want this to be too short because it might cause a storm of trigger events if
        /// the exceptional condition doesn't go away.
        /// </summary>
        private static readonly TimeSpan FailureRetriggerTime = TimeSpan.FromMilliseconds(200);


        public GenericCallBackReturn<double> BytesPerSecondGetter { get; set; }
        public double BytesPerSecond { get { return this.BytesPerSecondGetter(); } }

        // Only settable by the constructor because we need to ensure it doesn't change between ChannelUnemptied() and GateOne()
        private ISource<T> mSource;
        public ISource<T> Source { get { return this.mSource; } }

        private GenericCallBackReturn<int, T> mSink;
        public GenericCallBackReturn<int, T> Sink
        {
            get { return this.mSink; }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException("Sink");
                }
                this.mSink = value;
            }
        }

        private OneShotTask mGateTask;

        private TimeSpan mStartTime;
        private long mBytesSent;

        private bool mGating;
        private bool IsGateRunning
        {
            get { return this.mGateTask.IsPending || this.mGating; }
        }


        private double mLastBytesSent;
        private TimeSpan mLastBytesSentTime;
        private double mCurrentSendRate;
        private double mTotalBytesActuallySunk;

        /// <summary>
        /// The total number of bytes sent by this RateLimiter.  This value attempts to estimate the
        /// actual number of bytes sent and so should increase steadily and may report fractional bytes
        /// (i.e. it doesn't just jump when a new packet is sent, but extrapolates over time).  It is
        /// guaranteed non-decreasing.
        /// </summary>
        public double TotalBytesSent
        {
            get
            {
                double sent = this.mLastBytesSent + (this.timeKeeper.Now - this.mLastBytesSentTime).TotalSeconds * this.mCurrentSendRate;
                return Math.Min(sent, this.mTotalBytesActuallySunk);
            }
        }

        /// <summary>
        /// The queue to use for scheduling events.
        /// </summary>
        private NetworkEventQueue eventQueue;

        /// <summary>
        /// Provides access to the current time.
        /// </summary>
        private ITime timeKeeper;

        public RateLimiter(
            GenericCallBackReturn<double> bytesPerSecondGetter,
            ISource<T> source,
            GenericCallBackReturn<int, T> sink,
            NetworkEventQueue eventQueue,
            ITime timeKeeper)
        {
            if (source == null)
            {
                throw new ArgumentNullException("source");
            }

            this.eventQueue = eventQueue;
            this.timeKeeper = timeKeeper;

            this.mGateTask = new OneShotTask(this.eventQueue);

            this.BytesPerSecondGetter = bytesPerSecondGetter;
            this.mSource = source;
            this.Sink = sink;

            this.mSource.SetUnemptiedCallback(this.ChannelUnemptied);  // This will start things up if the channel already has items
        }

        private void ChannelUnemptied(ISource<T> channel)
        {
            if(!this.IsGateRunning)
            {
                this.eventQueue.Push(delegate()
                {
                    this.mStartTime = this.timeKeeper.Now;
                    this.mBytesSent = 0;
                });
            }

            this.mGateTask.TriggerNow(Apply.Func(this.Gate));
        }

        private void Gate()
        {
            if (this.mGating)  // Only one at a time (call to this.Source.Get() may trigger another ChannelUnemptied)
            {
                throw new InvalidOperationException("Two instances of gate running at the same time.");
            }

            TimeSpan now = this.timeKeeper.Now;

            this.mCurrentSendRate = 0;
            this.mLastBytesSent = this.mTotalBytesActuallySunk;
            this.mLastBytesSentTime = now;

            if (this.Source.IsEmpty)
            {
                return;
            }

            TimeSpan delay = RateLimiter<T>.FailureRetriggerTime;
            this.mGating = true;
            try
            {
                TimeSpan elapsed = now - this.mStartTime;
                double realtotalAllowableBytes = Math.Max(0.0, Math.Round(this.BytesPerSecond * elapsed.TotalSeconds));
                long totalAllowableBytes;
                try
                {
                    checked
                    {
                        totalAllowableBytes = (long)realtotalAllowableBytes;
                    }
                }
                catch (OverflowException)
                {
                    // realtotalAllowableBytes > long.MaxValue (Math.Max call earlier ensured it is > 0).
                    // Clamp totalAllowableBytes to long.MaxValue.
                    totalAllowableBytes = long.MaxValue;
                }

                this.mCurrentSendRate = this.BytesPerSecond;

                while (totalAllowableBytes > this.mBytesSent && !this.Source.IsEmpty)
                {
                    T item = this.Source.Get();
                    // If it is null, we throw a NRE and let the finally handler do its work
                    Debug.Assert(item != null, "Got null from ISource<T>.Get when it should be non-empty");
                    var length = this.mSink(item);
                    this.mBytesSent += length;
                    this.mTotalBytesActuallySunk += length;
                } 

                delay = TimeSpan.FromSeconds(Math.Max(0.01, (double)(this.mBytesSent - totalAllowableBytes) / this.BytesPerSecond));
                if (delay > RateLimiter<T>.FailureRetriggerTime)
                {
                    delay = RateLimiter<T>.FailureRetriggerTime;
                }

                // Adjust mBytesSent (and mStartTime) so we don't overflow
                this.mBytesSent -= totalAllowableBytes;
                if (this.mBytesSent < 0)
                {
                    this.mBytesSent = 0;
                }

                this.mStartTime += TimeSpan.FromSeconds((double)totalAllowableBytes / this.BytesPerSecond);
            }
            finally
            {
                this.mGating = false;
                this.mGateTask.Trigger(delay, Apply.Func(this.Gate));
            }
        }
    }
}
