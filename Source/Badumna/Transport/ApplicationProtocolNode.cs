﻿using System;
using System.Collections.Generic;
using System.Text;

using Badumna.Core;
using Badumna.Utilities;
using Badumna.DataTypes;

namespace Badumna.Transport
{
    /// <summary>
    /// An indexable protocol node. 
    /// </summary>
    public abstract class ApplicationProtocolNode
    {
        private BadumnaId mIndex;
        internal BadumnaId Index 
        { 
            get { return this.mIndex; }
            set { this.mIndex = value; }
        }

        /// <summary>
        /// The identifier of this resource.
        /// </summary>
        public BadumnaId Id { get { return new BadumnaId(this.mIndex); } }

        private ApplicationProtocolLayer mLayer;
        internal ApplicationProtocolLayer Layer 
        {
            get { return this.mLayer; }
            set { this.mLayer = value; } 
        }

        internal MessageStream GetDirectStream(BadumnaId destination, QualityOfService qos)
        {
            return this.mLayer.GetDirectStream(this.Index, destination, qos);
        }

        internal MessageStream GetBroadcastStream(QualityOfService qos)
        {
            return this.mLayer.GetBroadcastStream(this.Index, qos);
        }

        abstract internal void HandleMessage(MessageStream message, BadumnaId source, QualityOfService qos);
    }
}
