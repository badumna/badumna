﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.Collections;

using Badumna.Core;
using Badumna.Utilities;

namespace Badumna.Transport
{
    /// <summary>
    /// A Packet-by-packet Generalized Processor Sharing scheduler. 
    /// </summary>
    /// <remarks>
    /// See "A Generalized Processor Sharing Approach to Flow Control in 
    /// Integrated Services Networks: The Single Node Case",  Abhay K. Parekh and Robert G. Gallager
    /// [http://www.tecknowbasic.com/tnet1.pdf] for details of the algorithm.
    /// 
    /// Note that the implementation in the paper assumes a constant rate of 1 byte / sec.  The
    /// implementation below has modifications that deal with a variable rate.
    /// </remarks>
    /// <typeparam name="T"></typeparam>
    class PgpsScheduler<T> : ISource<T> where T : ISized
    {
        public class SourceGroup : IEnumerable<SourceInfo>
        {
            public int Count { get { return this.mSources.Count; } }

            private List<SourceInfo> mSources = new List<SourceInfo>();

            private double mGroupWeight;
            private double? mTotalIndividualWeight;

            private double TotalIndividualWeight
            {
                get
                {
                    if (this.mTotalIndividualWeight == null)
                    {
                        this.mTotalIndividualWeight = 0;
                        foreach (SourceInfo source in this.mSources)
                        {
                            this.mTotalIndividualWeight += source.IndividualWeight;
                        }
                    }
                    return (double)this.mTotalIndividualWeight;
                }
            }

            public SourceGroup(double groupWeight)
            {
                this.mGroupWeight = groupWeight;
            }

            public void Add(SourceInfo sourceInfo)
            {
                if (sourceInfo.SourceGroup != null)
                {
                    sourceInfo.SourceGroup.Remove(sourceInfo);
                }

                Debug.Assert(!this.mSources.Contains(sourceInfo));

                this.mSources.Add(sourceInfo);
                sourceInfo.SourceGroup = this;
                this.mTotalIndividualWeight = null;
            }

            public void Remove(SourceInfo sourceInfo)
            {
                Debug.Assert(sourceInfo.SourceGroup == this);
                Debug.Assert(this.mSources.Contains(sourceInfo));

                this.mSources.Remove(sourceInfo);
                sourceInfo.SourceGroup = null;
                this.mTotalIndividualWeight = null;
            }

            public void Clear()
            {
                this.mSources.Clear();
            }

            public double CalculateWeight(SourceInfo sourceInfo)
            {
                Debug.Assert(this.mSources.Contains(sourceInfo));

                return sourceInfo.IndividualWeight * this.mGroupWeight / this.TotalIndividualWeight;
            }

            public IEnumerator<SourceInfo> GetEnumerator()
            {
                return this.mSources.GetEnumerator();
            }

            IEnumerator IEnumerable.GetEnumerator()
            {
                return this.GetEnumerator();
            }
        }

        public class SourceInfo : IComparable<SourceInfo>, IComparable, IDisposable
        {
            public ISource<T> Source;

            public double Weight
            {
                get
                {
                    if (this.SourceGroup != null)
                    {
                        return this.SourceGroup.CalculateWeight(this);
                    }

                    return this.IndividualWeight;
                }
            }

            public double IndividualWeight { get; private set; }

            public SourceGroup SourceGroup { get; set; }

            /// <summary>
            /// Set to true if the source has been removed.  Using this the
            /// source can be dropped from the schedule when it gets to the
            /// head, rather than scanning the whole heap for it.
            /// </summary>
            public bool Removed;

            /// <summary>
            /// This is set to the *virtual time* that the source will become idle.
            /// If the value is less than the current virtual time, or if the value
            /// is double.MinValue, then the source is currently idle.
            /// </summary>
            public double NextFinishTime = double.MinValue;


            public SourceInfo(ISource<T> source, double weight)
            {
                this.Source = source;
                this.IndividualWeight = weight;
            }

            public void Dispose()
            {
                if (this.SourceGroup != null)
                {
                    this.SourceGroup.Remove(this);
                }
            }

            public int CompareTo(SourceInfo other)
            {
                return Math.Sign(this.NextFinishTime - other.NextFinishTime);
            }

            public int CompareTo(object obj)
            {
                return this.CompareTo(obj as SourceInfo);
            }
        }

        // Private implementation of Channel<T> to get the benefit of the base class
        // implementation without exposing the ISink part of the interface.
        private class GpsSimulator : Channel<T>
        {
            // Contains sources which have packets to send.  Note that a packet can be removed from here
            // before the simulated GPS system treats is as having finished.
            private BinaryHeap<SourceInfo> mSchedule = new BinaryHeap<SourceInfo>(10);

            // All of our SourceInfos.  Used to keep track of current activity in the simulated GPS system.
            private Dictionary<ISource<T>, SourceInfo> mSources = new Dictionary<ISource<T>, SourceInfo>();

            private double mLastBytesSent;
            private double mVirtualTime;
            private double VirtualTime
            {
                get
                {
                    this.UpdateVirtualTime();
                    return this.mVirtualTime;
                }
            }

            public void Shutdown()
            {
                if (null != this.mSources)
                {
                    this.mSources.Clear();
                }
            }

            public override bool IsEmpty
            {
                get
                {
                    this.CleanSchedule();
                    return this.mSchedule.Count == 0;
                }
            }

            public override double GeneratingBytesPerSecond
            {
                get
                {
                    double rate = 0;
                    foreach (ISource<T> source in this.mSources.Keys)
                    {
                        rate += source.GeneratingBytesPerSecond;
                    }
                    return rate;
                }
            }

            public double RecentActivityWeight { get { return this.mRecentActivityWeight.EstimatedValue; } }


            private double mLastTotalBytesSent;
            private TimeSpan mLastTotalBytesSentTime;
            private double mCurrentSendRate;
            private double mTotalBytesActuallySunk;
            private TimeIndependentSmoother mRecentActivityWeight;

            /// <summary>
            /// Provides access to the current time.
            /// </summary>
            private ITime timeKeeper;

            /// <summary>
            /// The total number of bytes sent by this GpsSimulator.  This value attempts to estimate the
            /// actual number of bytes sent and so should increase steadily and may report fractional bytes
            /// (i.e. it doesn't just jump when a new packet is sent, but extrapolates over time).  It is
            /// guaranteed non-decreasing.
            /// </summary>
            private double TotalBytesSent
            {
                get
                {
                    double sent = this.mLastTotalBytesSent + (this.timeKeeper.Now - this.mLastTotalBytesSentTime).TotalSeconds * this.mCurrentSendRate;
                    return Math.Min(sent, this.mTotalBytesActuallySunk);
                }
            }

            public GpsSimulator(ITime timeKeeper)
                : base(timeKeeper)
            {
                this.timeKeeper = timeKeeper;
                this.mRecentActivityWeight = new TimeIndependentSmoother(Parameters.BandwidthEstimateSettlingTime, this.timeKeeper);
            }

            private void UpdateVirtualTime()
            {
                double totalBytesSent = this.TotalBytesSent;
                double bytesSentDelta = totalBytesSent - this.mLastBytesSent;
                this.mLastBytesSent = totalBytesSent;

                while (bytesSentDelta > 0.0)
                {
                    double earliestFinishTime = double.MaxValue;
                    double activityWeights = 0.0;
                    foreach (SourceInfo sourceInfo in this.mSources.Values)
                    {
                        if (sourceInfo.NextFinishTime != double.MinValue && sourceInfo.NextFinishTime > this.mVirtualTime)
                        {
                            activityWeights += sourceInfo.Weight;
                            earliestFinishTime = Math.Min(earliestFinishTime, sourceInfo.NextFinishTime);
                        }
                    }

                    this.mRecentActivityWeight.ValueEvent(activityWeights);

                    if (activityWeights > 0.0)
                    {
                        // Virtual 'time' is really bytes sent / weight.  Finish times are set total bytes / weight
                        // for each packet.  Then virtual time increases at a rate equal to the number of bytes
                        // sent per active weight.

                        double virtualDelta = bytesSentDelta / activityWeights;

                        if (earliestFinishTime < this.mVirtualTime + virtualDelta) // Find next possible time activityWeights changes
                        {
                            double bytesDelta = (earliestFinishTime - this.mVirtualTime) * activityWeights;
                            bytesSentDelta -= bytesDelta;
                            this.mVirtualTime = earliestFinishTime;
                        }
                        else
                        {
                            bytesSentDelta = 0;
                            this.mVirtualTime = this.mVirtualTime + virtualDelta;
                        }
                    }
                    else
                    {
                        // activityWeights is zero so we must be idle, reset everything

                        // TODO: What happens if we never become idle?  Will double start running out of resolution?

                        // The first packet in a busy period is always dispatched as soon as it arrives.  Its virtual finish
                        // time must be later than this, so in this case activityWeights will only go to zero after mSchedule
                        // is empty.  If a second packet arrives during the time that activityWeights is positive, it will
                        // be dispatched once the first has completed (at the full bandwidth rate).  If these packets are
                        // on the same source, then the full bandwidth will be allocated to that source, and the virtual
                        // finish time of the first packet should be the actual time the packet completes [there could be
                        // problems if the virtual time isn't synced properly to the actual number of bytes sent].
                        // Again, the virtual finish time of the second packet will be well after the packet has been dispatched.
                        //
                        // If multiple sources are active, the bandwidth is spread more thinly, but for any activity period
                        // the final packet should finish at the same time as activity goes back to zero, so it must have
                        // been dispatched (started) some time earlier.

                        this.CleanSchedule();
                        if (this.mSchedule.Count != 0)
                        {
                            // This shouldn't happen, but if it does the best option is to reschedule everything.  Otherwise
                            // stuff still on the scheduler will have a later virtual time than new stuff.
                            Logger.TraceWarning(LogTag.Transport, "activityWeights zero, but schedule count is not zero it's {0}", this.mSchedule.Count);
                            this.mSchedule.Clear();
                        }

                        this.mVirtualTime = 0.0;
                        foreach (SourceInfo sourceInfo in this.mSources.Values)
                        {
                            sourceInfo.NextFinishTime = double.MinValue;
                            this.UpdateSchedule(sourceInfo, 0.0);
                        }

                        break;
                    }
                }
            }

            private void SourceUnemptied(ISource<T> source)
            {
                SourceInfo sourceInfo;
                if (!this.mSources.TryGetValue(source, out sourceInfo))
                {
                    // Debug.Fail("Got SourceUnemptied for unknown source");
                    return;
                }

                // This function is the only place where an empty -> non-empty transistion can occur
                bool wasEmpty = this.IsEmpty;
                this.UpdateSchedule(sourceInfo);
                if (wasEmpty)
                {
                    this.InvokeUnemptiedCallback();
                }
            }

            /// <summary>
            /// Adds an input source.
            /// </summary>
            /// <param name="source">The source.</param>
            /// <param name="weight">The weight.</param>
            /// <param name="group">The group.</param>
            public void AddInputSource(ISource<T> source, double weight, SourceGroup group)
            {
                if (weight <= 0.0)
                {
                    throw new ArgumentOutOfRangeException("weight");
                }
                if (this.mSources.ContainsKey(source))
                {
                    throw new InvalidOperationException();
                }

                SourceInfo sourceInfo = new SourceInfo(source, weight);
                if (group != null)
                {
                    group.Add(sourceInfo);
                }
                this.mSources[source] = sourceInfo;
                source.SetUnemptiedCallback(this.SourceUnemptied);  // Also causes notification if the source is not empty initially
            }

            public void RemoveInputSource(ISource<T> source)
            {
                SourceInfo sourceInfo;
                if (this.mSources.TryGetValue(source, out sourceInfo))
                {
                    this.mSources.Remove(source);
                    source.SetUnemptiedCallback(null);
                    sourceInfo.Removed = true;
                    sourceInfo.Dispose();
                }
            }

            /// <summary>
            /// Remove any empty sources from the head of the schedule.  After calling this,
            /// the first source on the schedule is guaranteed to return something from a Get().
            /// </summary>
            private void CleanSchedule()
            {
                while (this.mSchedule.Count > 0)
                {
                    SourceInfo sourceInfo = this.mSchedule.Peek();

                    if (!sourceInfo.Removed && !sourceInfo.Source.IsEmpty)
                    {
                        break;
                    }

                    // The next scheduled source has become empty or was removed from the scheduler, remove it from the schedule
                    this.mSchedule.Pop();
                }
            }

            // Must be called exactly once for each packet on the source.
            private void UpdateSchedule(SourceInfo sourceInfo)
            {
                this.UpdateSchedule(sourceInfo, this.VirtualTime);
            }

            // Overload so that we can reset the schedule inside UpdateVirtualTime without getting re-entered.
            private void UpdateSchedule(SourceInfo sourceInfo, double virtualTime)
            {
                if (!sourceInfo.Source.IsEmpty)
                {
                    sourceInfo.NextFinishTime = Math.Max(sourceInfo.NextFinishTime, virtualTime) + sourceInfo.Source.Peek().Length / sourceInfo.Weight;
                    this.mSchedule.Push(sourceInfo);
                }

                // Don't reset sourceInfo.NextFinishTime just because ISource.IsEmpty - it's probably still being transmitted in the virtual system.
            }

            protected override T DoGet()
            {
                // A new get implies that the previous get has been fully dealt with (i.e. sent).  So update
                // our TotalBytesSent based on that assumption.
                TimeSpan now = this.timeKeeper.Now;

                double elapsed = (now - this.mLastTotalBytesSentTime).TotalSeconds;
                if (elapsed > 0)
                {
                    this.mCurrentSendRate = (this.mTotalBytesActuallySunk - this.mLastTotalBytesSent) / elapsed; // Estimate next send rate as equal to previous send rate
                }

                this.mLastTotalBytesSent = this.mTotalBytesActuallySunk;
                this.mLastTotalBytesSentTime = now;


                this.CleanSchedule();
                if (this.mSchedule.Count == 0)
                {
                    return default(T);
                }

                SourceInfo sourceInfo = this.mSchedule.Pop();
                T item = sourceInfo.Source.Get();  // Source is guaranteed not empty and not removed by CleanSchedule
                this.mTotalBytesActuallySunk += item.Length;

                this.UpdateSchedule(sourceInfo);

                return item;
            }


            public override T Peek()
            {
                throw new InvalidOperationException();
            }

            protected override void DoPut(T item)
            {
                throw new InvalidOperationException();
            }

            public override void Clear()
            {
                throw new InvalidOperationException();
            }
        }

        private GpsSimulator mGpsSimulator;

        private int mNextGroupId = 0;
		private Dictionary<int, SourceGroup> mSourceGroups = new Dictionary<int, SourceGroup>(new IntEqualityComparer());

        public double RecentActivityWeight { get { return this.mGpsSimulator.RecentActivityWeight; } }

        public PgpsScheduler(ITime timeKeeper)
        {
            this.mGpsSimulator = new PgpsScheduler<T>.GpsSimulator(timeKeeper);
        }

        /// <summary>
        /// Creates a new source group.  A source group can be used to fix to total weight
        /// of all sources in the group to a specified value.  As sources are added/removed,
        /// each source in the group will have its weight updated proportionally to maintain
        /// the total.
        /// </summary>
        /// <returns></returns>
        public void CreateSourceGroup(ChannelGroup groupIdex, double groupWeight)
        {
            this.mNextGroupId = Math.Max(this.mNextGroupId + 1, (int)groupIdex + 1);
            this.mSourceGroups[(int)groupIdex] = new SourceGroup(groupWeight);
        }
        
        /// <summary>
        /// Creates a new source group.  A source group can be used to fix to total weight
        /// of all sources in the group to a specified value.  As sources are added/removed,
        /// each source in the group will have its weight updated proportionally to maintain
        /// the total.
        /// </summary>
        /// <returns></returns>
        public int CreateSourceGroup(double groupWeight)
        {
            this.mNextGroupId++;
            this.mSourceGroups[this.mNextGroupId] = new SourceGroup(groupWeight);
            return this.mNextGroupId;
        }

        /// <summary>
        /// Removes the source group.  Any sources still in the group will also be removed from
        /// the scheduler.
        /// </summary>
        /// <param name="id"></param>
        public void RemoveSourceGroup(int id)
        {
            SourceGroup group;
            if (!this.mSourceGroups.TryGetValue(id, out group))
            {
                return;
            }

            foreach (SourceInfo sourceInfo in group)
            {
                this.RemoveInputSource(sourceInfo.Source);
            }
            this.mSourceGroups.Remove(id);
        }

        /// <summary>
        /// Returns the number of sources in the specified source group,
        /// or 0 if the source group does not exist.
        /// </summary>
        public int GetSourceGroupSize(int groupId)
        {
            SourceGroup group;
            if (!this.mSourceGroups.TryGetValue(groupId, out group))
            {
                return 0;
            }

            return group.Count;
        }

        public void AddInputSource(ISource<T> source, double weight, int groupId)
        {
            SourceGroup group;
            if (!this.mSourceGroups.TryGetValue(groupId, out group))
            {
                throw new ArgumentException("groupId");
            }

            this.mGpsSimulator.AddInputSource(source, weight, group);
        }

        public void AddInputSource(ISource<T> source, double weight)
        {
            this.mGpsSimulator.AddInputSource(source, weight, null);
        }

        public void RemoveInputSource(ISource<T> source)
        {
            this.mGpsSimulator.RemoveInputSource(source);
        }

        public SourceGroup GetSourceGroup(int groupId)
        {
            SourceGroup group;
            if (!this.mSourceGroups.TryGetValue(groupId, out group))
            {
                throw new ArgumentException("groupId");
            }

            return group;
        }

        public void Shutdown()
        {
            foreach (KeyValuePair<int, SourceGroup> kvp in this.mSourceGroups)
            {
                SourceGroup sg = kvp.Value;
                if (null != sg)
                {
                    sg.Clear();    
                }
            }
            
            this.mGpsSimulator.Shutdown();
        }

        public bool IsEmpty { get { return this.mGpsSimulator.IsEmpty; } }

        public double GeneratingBytesPerSecond { get { return this.mGpsSimulator.GeneratingBytesPerSecond; } }

        public T Get()
        {
            return this.mGpsSimulator.Get();
        }

        public T Peek()
        {
            return this.mGpsSimulator.Peek();
        }

        public void SetUnemptiedCallback(GenericCallBack<ISource<T>> unemptiedDelegate)
        {
            this.mGpsSimulator.SetUnemptiedCallback(unemptiedDelegate);
        }
    }
}
