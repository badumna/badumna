using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Net;
using System.Diagnostics;
using System.Threading;

using Badumna.Core;
using Badumna.Platform;
using Badumna.Utilities;
using Badumna.Utilities.Logging;

namespace Badumna.Transport
{
    class ConnectionEventArgs : EventArgs
    {
        private bool mIsConnected;
        public bool IsConnected { get { return this.mIsConnected; } }

        private PeerAddress mAddress;
        public PeerAddress Address { get { return this.mAddress; } }

        public ConnectionEventArgs(PeerAddress address, bool isConnected)
        {
            this.mIsConnected = isConnected;
            this.mAddress = address;
        }
    }


    class TcpTransportLayer : IMessageDispatcher<TransportEnvelope>, IMessageProducer<TransportEnvelope>
    {
        private const int MaximumAllowableMessageSize = 100 * 1024;


        private class ConnectionState : IDisposable
        {
            private byte[] mBuffer = new byte[10000];

            private System.Net.Sockets.TcpClient mTcpClient;

            /// <summary>
            /// Communcation socket. Initially null, once initialized this will never change value;
            /// although it may get closed.
            /// </summary>
            private Socket mSocket;
            private Socket Socket
            {
                get { return this.mSocket; }
                set
                {
                    if (this.mSocket != null)
                    {
                        Debug.Assert(false, "Attempt to re-set Socket");
                        throw new InvalidOperationException();
                    }
                    this.mSocket = value;
                }
            }

            private PeerAddress mClientAddress;
            public PeerAddress ClientAddress { get { return this.mClientAddress; } }

            private Queue<MessageBuffer> mInboundMessageQueue = new Queue<MessageBuffer>();

            private List<TransportEnvelope> mOutboundMessageQueue = new List<TransportEnvelope>();

            private TcpTransportLayer mTcpTransportLayer;

            private volatile int mAttemptingConnection;  // Used to ensure only one connection attempt at a time.  Using this int instead of a monitor because it needs to be released by a different thread.
            private readonly object mOutboundMessageQueueLock = new object();  // Used to ensure that messages aren't queued onto mOutboundMessageQueue after it has been processed


            public ConnectionState(TcpTransportLayer tcpTransportLayer)
            {
                this.mTcpTransportLayer = tcpTransportLayer;
            }

            public ConnectionState(TcpTransportLayer tcpTransportLayer, Socket clientSocket)
            {
                this.mTcpTransportLayer = tcpTransportLayer;
                this.StartListen(clientSocket);
            }


            private void StartListen(Socket socket)
            {
                if (socket == null)
                {
                    throw new ArgumentNullException("socket");
                }

                socket.SendBufferSize = this.mBuffer.Length;
                socket.ReceiveBufferSize = this.mBuffer.Length;
                this.mClientAddress = new PeerAddress(socket.RemoteEndPoint as IPEndPoint, NatType.Unknown);
                this.Socket = socket;  // Configure socket first, because as soon as we set this.Socket things might start using it

                this.mTcpTransportLayer.OnConnectionChanged(this.mClientAddress, true);

                // TODO: One thread per socket may get excessive...
                Thread readThread = new Thread(this.ReadLoop);
                readThread.IsBackground = true;
                readThread.Start();  
            }

            private void ReadLoop()
            {
                try
                {
                    byte[] messageLengthBuffer = new byte[4];
                    int messageLengthBufferOffset = 0;
                    MessageBuffer message = null;
                    int messageBytesLeft = 0;

                    int bytesReceived;
                    while ((bytesReceived = this.Socket.Receive(this.mBuffer)) > 0)  // If mSocket has been closed or set to null then this will just throw us out
                    {
                        int bufferOffset = 0;

                        while (bytesReceived > 0)
                        {
                            if (message == null)
                            {
                                // No message length has been read yet.
                                int numberToReadForLength = Math.Min(messageLengthBuffer.Length - messageLengthBufferOffset, bytesReceived);

                                Array.Copy(this.mBuffer, bufferOffset, messageLengthBuffer, messageLengthBufferOffset, numberToReadForLength);
                                messageLengthBufferOffset += numberToReadForLength;
                                bufferOffset += numberToReadForLength;
                                bytesReceived -= numberToReadForLength;

                                if (messageLengthBufferOffset == messageLengthBuffer.Length)
                                {
                                    if (!BitConverter.IsLittleEndian)
                                    {
                                        Array.Reverse(messageLengthBuffer);
                                    }
                                    messageBytesLeft = BitConverter.ToInt32(messageLengthBuffer, 0);

                                    if (messageBytesLeft < 0 || messageBytesLeft > TcpTransportLayer.MaximumAllowableMessageSize)
                                    {
                                        // Parsed message length doesn't look sensible, bail on the connection
                                        Logger.TraceError(LogTag.Transport, "Got a TCP message length of {0} which doesn't look right, abandoning this connection", messageBytesLeft);
                                        throw new InvalidOperationException();
                                    }

                                    messageLengthBufferOffset = 0;
                                    message = new MessageBuffer();
                                }
                                else
                                {
                                    continue;  // Need a new packet for the rest of the message length
                                }
                            }

                            int copyLength = Math.Min(messageBytesLeft, bytesReceived);

                            message.Write(this.mBuffer, bufferOffset, copyLength);
                            bytesReceived -= copyLength;
                            bufferOffset += copyLength;
                            messageBytesLeft -= copyLength;

                            if (messageBytesLeft == 0)
                            {
                                lock (this.mInboundMessageQueue)
                                {
                                    this.mInboundMessageQueue.Enqueue(message);
                                }
                                message = null;
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    Logger.TraceException(LogLevel.Information, LogTag.Connection | LogTag.Event, e, "TCP Connection lost:");
                }
                finally
                {
                    Logger.TraceInformation(LogTag.Connection | LogTag.Event, "TCP Connection ended with {0}", this.ClientAddress);
                    this.mTcpTransportLayer.RemoveConnection(this.ClientAddress);
                }
            }

            public void ProcessMessages(IMessageConsumer<TransportEnvelope> consumer)
            {
                int count;  // Used to make sure we don't keep processing messages forever if new messages arrive as we're working
                lock (this.mInboundMessageQueue)
                {
                    count = this.mInboundMessageQueue.Count;
                }

                while (count > 0)
                {
                    MessageBuffer message;
                    lock (this.mInboundMessageQueue)
                    {
                        if (this.mInboundMessageQueue.Count == 0)
                        {
                            break;
                        }

                        message = this.mInboundMessageQueue.Dequeue();
                    }
                    count--;

                    consumer.ProcessMessage(new TransportEnvelope(message, this.ClientAddress, false, TimeSpan.MinValue));  // TODO: Set correct arrival time
                }
            }

            public void SendMessage(TransportEnvelope envelope)
            {
                if (this.Socket == null)
                {
                    lock (this.mOutboundMessageQueueLock)
                    {
                        if (this.mOutboundMessageQueue != null)
                        {
                            this.mOutboundMessageQueue.Add(envelope);
                            this.AttemptConnection(envelope.Destination);
                            return;
                        }

                        // If mOutboundMessageQueue has gone to null then we should be connected
                    }
                }

                this.SendOneMessage(envelope);
            }

            private void SendOneMessage(TransportEnvelope envelope)
            {
                Debug.Assert(this.Socket != null);

                try
                {
                    Debug.Assert(envelope.Destination.Equals(this.mClientAddress), "SendMessage destination does not match socket endpoint");

                    byte[] lengthArray = BitConverter.GetBytes(envelope.Length);
                    if (!BitConverter.IsLittleEndian)
                    {
                        Array.Reverse(lengthArray);
                    }
                    int numBytesSent = this.Socket.Send(lengthArray);
                    numBytesSent += this.Socket.Send(envelope.Message.GetBuffer());

                    Logger.TraceInformation(LogTag.Transport, "Sent {0} bytes to {1}", numBytesSent, envelope.Destination);
                }
                catch (Exception e)
                {
                    Logger.TraceError(LogTag.Transport, "Failed to send message to {0} : {1}", envelope.Destination, e.Message);
                }
            }

            private void AttemptConnection(PeerAddress destination)
            {
#pragma warning disable 420  // A reference to a volatile field will not be treated as volatile.  We want volatile for the reset below, and we're using Interlocked here, so this warning is just an annoyance.
                if (Interlocked.CompareExchange(ref this.mAttemptingConnection, 1, 0) != 0)
#pragma warning restore 420
                {
                    return;  // Connection attempt already in progress
                }

                if (this.mTcpClient != null)
                {
                    this.mTcpClient.Close();
                }
                this.mTcpClient = new System.Net.Sockets.TcpClient();
                this.mTcpClient.BeginConnect(destination.Address, destination.Port, this.ConnectCallback, destination);
            }

            private void ConnectCallback(IAsyncResult asyncResult)
            {
                if (this.mTcpTransportLayer.connectivityReporter.Status == ConnectivityStatus.ShuttingDown ||
                    this.mTcpTransportLayer.connectivityReporter.Status == ConnectivityStatus.Offline)
                {
                    return;
                }

                PeerAddress destination = asyncResult.AsyncState as PeerAddress;
                System.Net.Sockets.TcpClient tcpClient = this.mTcpClient;  // For thread safety (although TcpClient probably isn't thread safe enough for this...)
                if (tcpClient == null)
                {
                    // I guess we got disposed in the meantime
                    return;
                }

                try
                {
                    tcpClient.EndConnect(asyncResult);

                    this.StartListen(new DotNetSocket(tcpClient.Client));

                    lock (this.mOutboundMessageQueueLock)
                    {
                        foreach (TransportEnvelope envelope in this.mOutboundMessageQueue)
                        {
                            this.SendOneMessage(envelope);
                        }

                        this.mOutboundMessageQueue = null;
                    }
                }
                catch (Exception e)
                {
                    tcpClient.Close();
                    this.mTcpClient = null;

                    Logger.TraceWarning(LogTag.Transport, "Failed to connect to {1} : {0}", e.Message, destination);

                    if (this.mOutboundMessageQueue.Count > 0)
                    {
                        Logger.TraceWarning(LogTag.Transport, "{0} queued messages to {1} discarded.", this.mOutboundMessageQueue.Count, destination);
                        this.mOutboundMessageQueue.Clear();
                    }
                }
                finally
                {
                    this.mAttemptingConnection = 0;
                }
            }

            public void Dispose()
            {
                System.Net.Sockets.TcpClient tcpClient = this.mTcpClient;  // In case mTcpClient is nulled by another thread
                if (tcpClient != null)
                {
                    tcpClient.Close();
                    this.mTcpClient = null;
                }

                if (this.Socket != null)  // Socket can't go non-null to null so this is fine
                {
                    this.Socket.Close();
                }
            }
        }




        private System.Net.Sockets.TcpListener mServer;

        private EventHandler<ConnectionEventArgs> connectionChangedDelegate;
        public event EventHandler<ConnectionEventArgs> ConnectionChanged
        {
            add { this.connectionChangedDelegate += value; }
            remove { this.connectionChangedDelegate -= value; }
        }

        private Dictionary<PeerAddress, ConnectionState> mConnections = new Dictionary<PeerAddress, ConnectionState>();

        private IMessageConsumer<TransportEnvelope> mMessageConsumer;
        public IMessageConsumer<TransportEnvelope> MessageConsumer { set { this.mMessageConsumer = value; } }

        private INetworkConnectivityReporter connectivityReporter;

        public TcpTransportLayer(INetworkConnectivityReporter connectivityReporter)
        {
            this.connectivityReporter = connectivityReporter;
        }

        public void Initialize(int port)
        {
            if (this.mServer != null)
            {
                throw new InvalidOperationException();
            }

            this.mServer = new System.Net.Sockets.TcpListener(IPAddress.Any, port);
            this.mServer.Start();

            Thread listenThread = new Thread(this.Listen);
            listenThread.IsBackground = true;
            listenThread.Start(this.mServer);  // Stopping server will cause thread to throw out
        }

        // Server is passed as a parameter so that this thread won't get the new server if we're re-initialized
        public void Listen(object serverAsObject)
        {
            try
            {
                System.Net.Sockets.TcpListener server = (System.Net.Sockets.TcpListener)serverAsObject;

                while (true)
                {
                    Socket clientSocket = null;

                    try
                    {
                        clientSocket = new DotNetSocket(server.AcceptSocket());
                    }
                    catch (InvalidOperationException e)
                    {
                        Logger.TraceError(LogTag.Transport, "Failed to accept socket : {0}", e.Message);
                    }

                    if (clientSocket != null && clientSocket.Connected)
                    {
                        ConnectionState state = new ConnectionState(this, clientSocket);

                        lock (this.mConnections)
                        {
                            this.RemoveConnection(state.ClientAddress);  // Remove old connection, if any
                            this.mConnections[state.ClientAddress] = state;
                        }
                    }
                }
            }
            catch
            {
                // Silent thread death on exception
            }
        }

        private void OnConnectionChanged(PeerAddress address, bool isConnected)
        {
            EventHandler<ConnectionEventArgs> handler = this.connectionChangedDelegate;
            if (handler != null)
            {
                handler(this, new ConnectionEventArgs(address, isConnected));
            }
        }

        private void RemoveConnection(PeerAddress address)
        {
            ConnectionState connectionState;

            lock (this.mConnections)
            {
                if (this.mConnections.TryGetValue(address, out connectionState))
                {
                    this.mConnections.Remove(address);
                }
            }

            if (connectionState != null)
            {
                Logger.TraceInformation(LogTag.Transport, "Closing connection with  : {0}", address);
                connectionState.Dispose();
                this.OnConnectionChanged(address, false);
            }
        }

        public void ProcessMessages()
        {
            if (this.mMessageConsumer == null)
            {
                Logger.TraceError(LogTag.Transport, "No process message delegate specified in TcpTransportLayer");
                return;
            }

            lock (this.mConnections)
            {
                foreach (ConnectionState state in this.mConnections.Values)
                {
                    state.ProcessMessages(this.mMessageConsumer);
                }
            }
        }

        public void SendMessage(TransportEnvelope envelope)
        {
            if (this.connectivityReporter.Status != ConnectivityStatus.Online)
            {
                return;
            }

            lock (this.mConnections)
            {
                ConnectionState state;
                if (!this.mConnections.TryGetValue(envelope.Destination, out state))
                {
                    state = new ConnectionState(this);
                    this.mConnections[envelope.Destination] = state;
                }

                state.SendMessage(envelope);
            }
        }

        public void ShutDown()
        {
            if (this.mServer != null)
            {
                this.mServer.Stop();
                this.mServer = null;
            }
        }
    }
}
