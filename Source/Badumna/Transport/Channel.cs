﻿using Badumna.Core;
using Badumna.Utilities;


namespace Badumna.Transport
{
    /// <summary>
    /// A channel of Ts; essentially just a producer of Ts.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    abstract class Channel<T> : ISource<T> where T : ISized
    {
        public abstract bool IsEmpty { get; }
        public virtual double GeneratingBytesPerSecond { get { return this.mGeneratingRate.EstimatedRate; } }


        private GenericCallBack<ISource<T>> mUnemptiedDelegate;
        private GenericCallBack<Channel<T>> mEmptiedDelegate;

        protected RateSmoother mGeneratingRate;

        public Channel(ITime timeKeeper)
        {
            this.mGeneratingRate = new RateSmoother(
                Parameters.DesiredRateSamplingPeriod,
                Parameters.DesiredRateSettlingPeriod,
                timeKeeper);
        }

        public void SetUnemptiedCallback(GenericCallBack<ISource<T>> unemptiedDelegate)
        {
            this.mUnemptiedDelegate = unemptiedDelegate;
            if (!this.IsEmpty)
            {
                this.InvokeUnemptiedCallback();
            }
        }

        /// <summary>
        /// Sets the function to be called when the channel has no more items available. This is invoke only when the 
        /// state has changed, i.e. the channel used to not be empty but now is. 
        /// </summary>
        /// <param name="emptiedDelegate"></param>
        public void SetEmptiedCallback(GenericCallBack<Channel<T>> emptiedDelegate)
        {
            this.mEmptiedDelegate = emptiedDelegate;
        }

        protected void InvokeUnemptiedCallback()
        {
            GenericCallBack<ISource<T>> handler = this.mUnemptiedDelegate;
            if (handler != null)
            {
                handler(this);
            }
        }

        protected void InvokeEmptiedCallback()
        {
            GenericCallBack<Channel<T>> handler = this.mEmptiedDelegate;
            if (handler != null)
            {
                handler(this);
            }
        }

        /// <summary>
        /// Puts an item into the channel.  The order that items put into the channel
        /// get retrieved depends on the specific implementation of Channel&lt;T&gt;.
        /// </summary>
        /// <param name="item">The T to put into the channel</param>
        public void Put(T item)
        {
            bool wasEmpty = this.IsEmpty;
            this.DoPut(item);
            this.mGeneratingRate.ValueEvent(item.Length);
            if (wasEmpty)
            {
                this.InvokeUnemptiedCallback();
            }
        }

        protected abstract void DoPut(T item);

        public T Get()
        {
            bool wasEmpty = this.IsEmpty;
            T nextItem = this.DoGet();

            if (!wasEmpty && this.IsEmpty)
            {
                this.InvokeEmptiedCallback();
            }

            return nextItem;
        }

        protected abstract T DoGet();

        public abstract T Peek();

        /// <summary>
        /// Clears the channel, all Ts currently on the channel are discarded.
        /// </summary>
        public abstract void Clear();
    }
}
