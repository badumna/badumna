﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Badumna.Transport
{
    /// <exclude/>  
    public interface ISized
    {
        /// <summary>
        /// For data, returns length in bytes.
        /// </summary>
        int Length { get; }
    }
}
