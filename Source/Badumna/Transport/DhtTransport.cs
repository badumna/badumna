﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;

using Badumna.Core;
using Badumna.DistributedHashTable;

namespace Badumna.Transport
{
    class DhtTransport : DhtProtocol
    {
        public PgpsScheduler<TransportEnvelope> Scheduler { get { return this.mScheduler; } }

        private ConnectionTable mConnectionTable;
        private PgpsScheduler<TransportEnvelope> mScheduler;

        public DhtTransport(
            ConnectionTable connectionTable,
            DhtFacade facade,
            NetworkEventQueue eventQueue,
            ITime timeKeeper)
            : base(facade)
        {
            this.mConnectionTable = connectionTable;
            this.mScheduler = new PgpsScheduler<TransportEnvelope>(timeKeeper);
            this.mScheduler.CreateSourceGroup(ChannelGroup.EntityUpdates, 1.0);
            this.mScheduler.CreateSourceGroup(ChannelGroup.Streaming, 1.0);
            // TODO: This rate limiter appears to never be used.
			new RateLimiter<TransportEnvelope>(
                delegate { return 10.0 * 1024 * 1024 * 1024; },  // Effectively unlimited, will be limited by the actual connection's limiter
                this.mScheduler,
                this.Send,
                eventQueue,
                timeKeeper);
        }

        public int Send(TransportEnvelope envelope)
        {
            Debug.Assert(envelope.Destination.HashAddress != null, "Invalid destination for hash addressed envelope");
            DhtEnvelope dhtEnvelope = this.GetMessageFor(envelope.Destination.HashAddress, envelope.Qos);

            this.RemoteCall(dhtEnvelope, this.ProcessMessage, envelope);
            this.SendMessage(dhtEnvelope);
            return dhtEnvelope.Length;
        }

        [DhtProtocolMethod(DhtMethod.DhtTransportProcessMessage)]
        private void ProcessMessage(TransportEnvelope envelope)
        {
            this.mConnectionTable.ProcessConnectionfulMessageBody(envelope, this.CurrentEnvelope.Source, 
                new PeerAddress(this.CurrentEnvelope.DestinationKey));
        }
    }
}
