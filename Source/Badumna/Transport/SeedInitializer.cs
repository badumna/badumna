//------------------------------------------------------------------------------
// <copyright file="SeedInitializer.cs" company="National ICT Australia Limited">
//     Copyright (c) 2012 National ICT Australia Limited. All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;

using Badumna.Core;
using Badumna.Utilities;

namespace Badumna.Transport
{
    /// <summary>
    /// A method for finding peers on the network, used to form an initial connection to the network.
    /// </summary>
    internal interface IPeerFinder
    {
        /// <summary>
        /// Gets a description identifying the method used to find peers.
        /// </summary>
        string Description { get; }

        /// <summary>
        /// Gets the local time of the last query.
        /// </summary>
        DateTime LastQueryTime { get; }

        /// <summary>
        /// Gets the result of the last query (success, failure, etc).
        /// </summary>
        string LastQueryStatus { get; }

        /// <summary>
        /// Gets the number of peers returned by the last query.
        /// </summary>
        int LastResultCount { get; }

        /// <summary>
        /// Request a set of peers to use for forming initial connections to the network.
        /// </summary>
        /// <param name="resultCallback">The callback to invoke with the results.  May be called during the invocation
        /// of this method, or some time later (possibly from a different thread).</param>
        void QueryForInitialPeerList(GenericCallBack<IEnumerable<PeerAddress>> resultCallback);
    }

    /// <summary>
    /// Provides a list of known peers for making initial connections to the network.
    /// </summary>
    internal class SeedPeerFinder : IPeerFinder
    {
        /// <summary>
        /// The known seed peers.
        /// </summary>
        private List<string> addresses;

        /// <summary>
        /// Initializes a new instance of the SeedPeerFinder class.
        /// </summary>
        /// <param name="seedHosts">The known seed peers</param>
        public SeedPeerFinder(IEnumerable<string> seedHosts)
        {
            this.LastQueryStatus = "";
            this.Description = "Seed";
            this.addresses = new List<string>(seedHosts);
        }

        /// <inheritdoc />
        public string Description { get; private set; }

        /// <inheritdoc />
        public DateTime LastQueryTime { get; private set; }

        /// <inheritdoc />
        public int LastResultCount { get; private set; }

        /// <inheritdoc />
        public string LastQueryStatus { get; private set; }

        /// <inheritdoc />
        public void QueryForInitialPeerList(GenericCallBack<IEnumerable<PeerAddress>> resultCallback)
        {
            if (resultCallback == null)
            {
                throw new ArgumentNullException("resultCallback");
            }

            this.LastQueryTime = DateTime.Now;
            this.LastResultCount = this.addresses.Count;
            this.LastQueryStatus = "Succeeded";

            List<PeerAddress> results = new List<PeerAddress>();
            foreach (string address in this.addresses)
            {
                PeerAddress peerAddress = PeerAddress.GetAddress(address, NatType.Open);                

                if (peerAddress.IsValid)
                {
                    results.Add(peerAddress);
                }
                else
                {
                    Logger.TraceWarning(LogTag.Transport, "Ignoring seed {0} becuase it is invalid or could not be resolved.", address);
                }
            }

            resultCallback(results);
        }
    }
}
