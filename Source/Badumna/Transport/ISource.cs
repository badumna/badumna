﻿using System;
using Badumna.Utilities;

namespace Badumna.Transport
{
    interface ISource<T> where T : ISized
    {
        /// <summary>
        /// True if the source is currently empty.  If false, a call to Get() is guaranteed
        /// to return an item (assuming the source isn't cleared / etc by anyone in the meantime).
        /// </summary>
        bool IsEmpty { get; }

        /// <summary>
        /// The rate that bytes are being generated/added to this source.  Used to calculate the
        /// total desired send rate.
        /// </summary>
        double GeneratingBytesPerSecond { get; }
        
        /// <summary>
        /// Gets the next T in the source, removing it from the source.
        /// </summary>
        /// <returns>The next T in the source, or default(T) if the source is empty.</returns>
        T Get();

        /// <summary>
        /// Returns the next T in the source, but leaves it in the source.
        /// </summary>
        /// <returns>The next T in the source, or default(T) if the source is empty.</returns>
        T Peek();

        /// <summary>
        /// Sets the function to be called when the source has items available.  This is only guaranteed to be
        /// called if the source was empty before hand (although it may be called at other times).
        /// Once called, the sink should keep trying to retrieve items from the source until it becomes empty.
        /// </summary>
        void SetUnemptiedCallback(GenericCallBack<ISource<T>> unemptiedDelegate);
    }
}
