﻿using System;
using System.Collections.Generic;
using System.Text;

using Badumna.Core;
using Badumna.Utilities;
using Badumna.DistributedHashTable;
using Badumna.DataTypes;

namespace Badumna.Transport
{
    class ApplicationProtocolLayer : DhtProtocol
    {
        private Dictionary<BadumnaId, ApplicationProtocolNode> mNodes;
        private DhtFacade mDhtFacade;
        internal DhtFacade DhtFacade { get { return this.mDhtFacade; } }

        private struct QueuedMessage
        {
            public MessageStream Stream;
            public BadumnaId Source;
            public BadumnaId Destination;
            public QualityOfService Qos;
        }

        private List<QueuedMessage> mQueuedMessages = new List<QueuedMessage>();
        private List<QueuedMessage> mQueuedBroadcastMessages = new List<QueuedMessage>();

        public ApplicationProtocolLayer(DhtFacade dhtFacade)
            : base(dhtFacade)
        {
            this.mNodes = new Dictionary<BadumnaId, ApplicationProtocolNode>();
            this.mDhtFacade = dhtFacade;
        }

        public bool AddNode(BadumnaId index, ApplicationProtocolNode node)
        {
            if (this.mNodes.ContainsKey(index))
            {
                Logger.TraceError(LogTag.Replication,"Application node {0} already added", index);
                return false;
            }

            node.Index = index;
            this.mNodes.Add(node.Index, node);
            node.Layer = this;

            return true;
        }

        // Called in application thread
        public void ProcessMessages()
        {
            foreach (QueuedMessage message in this.mQueuedMessages)
            {
                ApplicationProtocolNode node = null;

                if (this.mNodes.TryGetValue(message.Destination, out node))
                {
                    try
                    {
                        message.Stream.Position = 0;
                        node.HandleMessage(message.Stream, message.Source, message.Qos);
                    }
                    catch (Exception e)
                    {
                        Logger.TraceException(LogLevel.Warning, e, "Application failed to handle message");
                    }
                }
                else
                {
                    Logger.TraceError(LogTag.Replication,"Message arrived for unknown application node {0}", message.Destination);
                }
            }

            foreach (QueuedMessage message in this.mQueuedBroadcastMessages)
            {
                foreach (ApplicationProtocolNode node in this.mNodes.Values)
                {
                    try
                    {
                        message.Stream.Position = 0;
                        node.HandleMessage(message.Stream, message.Source, message.Qos);
                    }
                    catch (Exception e)
                    {
                        Logger.TraceException(LogLevel.Warning, e, "Application failed to handle message");
                    }
                }
            }

            this.mQueuedMessages.Clear();
            this.mQueuedBroadcastMessages.Clear();
        }

        #region Protocol

        public MessageStream GetDirectStream(BadumnaId source, BadumnaId destination, QualityOfService qos)
        {
            DhtEnvelope envelope = this.GetMessageFor(destination.Address, qos);

            this.RemoteCall(envelope, this.ReceiveMessage, source.LocalId, destination.LocalId);
            return new MessageWriteStream(new LambdaFunction(new GenericCallBack<DhtEnvelope>(this.SendMessage), envelope), envelope);
        }

        public MessageStream GetBroadcastStream(BadumnaId sourceId, QualityOfService qos)
        {
            DhtEnvelope envelope = this.GetMessageFor((HashKey)null, qos);

            this.RemoteCall(envelope, this.ReceiveBroadcastMessage, sourceId.LocalId);
            return new MessageWriteStream(new LambdaFunction(new GenericCallBack<DhtEnvelope>(this.mDhtFacade.Broadcast), envelope), envelope);
        }

        [DhtProtocolMethod(DhtMethod.ApplicationProtocolReceiveMessage)]
        public void ReceiveMessage(ushort sourcesLocalId, ushort desinationsLocalId)
        {
            BadumnaId destinationId= null;

            if (this.CurrentEnvelope.Destination != null)
            {
                destinationId = new BadumnaId(new BadumnaId(this.CurrentEnvelope.Destination, desinationsLocalId));
            }
            else
            {
                destinationId = new BadumnaId(new BadumnaId(
                    new PeerAddress(this.CurrentEnvelope.DestinationKey), desinationsLocalId));
            }

            QueuedMessage queuedMessage = new QueuedMessage();

            queuedMessage.Stream = new MessageReadStream(this.CurrentEnvelope);
            queuedMessage.Source = new BadumnaId(new BadumnaId(this.CurrentEnvelope.Source, sourcesLocalId));
            queuedMessage.Destination = destinationId;
            queuedMessage.Qos = this.CurrentEnvelope.Qos;

            this.mQueuedMessages.Add(queuedMessage);

            // Set the position at the end to ensure that no further parsing on this message takes place.
            this.CurrentEnvelope.Message.ReadOffset = this.CurrentEnvelope.Length;
        }

        [DhtProtocolMethod(DhtMethod.ApplicationProtocolReceiveBroadcastMessage)]
        public void ReceiveBroadcastMessage(ushort sourcesLocalId)
        {
            QueuedMessage queuedMessage = new QueuedMessage();

            queuedMessage.Stream = new MessageReadStream(this.CurrentEnvelope);
            queuedMessage.Source = new BadumnaId(new BadumnaId(this.CurrentEnvelope.Source, sourcesLocalId));
            queuedMessage.Destination = null;
            queuedMessage.Qos = this.CurrentEnvelope.Qos;

            this.mQueuedBroadcastMessages.Add(queuedMessage);

            // Set the position at the end to ensure that no further parsing on this message takes place.
            this.CurrentEnvelope.Message.ReadOffset = this.CurrentEnvelope.Length;
        }

        #endregion

    }
}
