﻿using System;
using System.Collections.Generic;
using System.Text;

using Badumna.Core;
using Badumna.Utilities;
using Badumna.Security;

namespace Badumna.Transport
{
    class DiscoveryLayer : TransportProtocol, IMessageConsumer<TransportEnvelope>
    {
        private ConnectionTable mConnectionTable;
        private RegularTask mDiscoverTask;
        private int broadcastPort;
        private INetworkConnectivityReporter connectivityReporter;
        private INetworkAddressProvider addressProvider;

        public DiscoveryLayer(
            IMessageDispatcher<TransportEnvelope> dispatcher,
            IMessageProducer<TransportEnvelope> messageProducer,
            ConnectionTable connectionTable,
            int broadcastPort,
            NetworkEventQueue eventQueue,
            INetworkConnectivityReporter connectivityReporter,
            INetworkAddressProvider addressProvider,
            GenericCallBackReturn<object, Type> factory)
            : base("DiscoveryLayer", typeof(DiscoveryProtocolMethodAttribute), factory)
        {
            messageProducer.MessageConsumer = this;
            this.DispatcherMethod = dispatcher.SendMessage;
            this.mConnectionTable = connectionTable;
            this.broadcastPort = broadcastPort;
            this.connectivityReporter = connectivityReporter;
            this.addressProvider = addressProvider;

            this.mDiscoverTask = new RegularTask("Local discovery", Parameters.DiscoveryPeriod, eventQueue, connectivityReporter, this.Discover);
            connectivityReporter.NetworkOnlineEvent += this.TriggerAdvertisement;
            connectivityReporter.NetworkOfflineEvent += this.CancelAdvertisement;
        }

        public void Start()
        {
            this.TriggerAdvertisement();
        }

        private void TriggerAdvertisement()
        {
            this.mDiscoverTask.Start();
        }

        private void CancelAdvertisement()
        {
            this.mDiscoverTask.Stop();
        }

        private void Discover()
        {
            if (this.connectivityReporter.Status == ConnectivityStatus.Online)
            {
                TransportEnvelope envelope = new TransportEnvelope(PeerAddress.GetBroadcast(this.broadcastPort), QualityOfService.Unreliable);

                this.RemoteCall(envelope, this.Polo, this.addressProvider.PrivatePort, this.addressProvider.PublicAddress);
                this.SendMessage(envelope);
            }
        }

        /*
        public void DiscoverPeer(PeerAddress publicAddress)
        {
            TransportEnvelope envelope = new TransportEnvelope(PeerAddress.GetSubnetBroadcast(ConnectivityModule.Instance.BroadcastPort), QualityOfService.Unreliable);

            this.RemoteCall(envelope, this.WhoIs, publicAddress, NetworkContext.CurrentContext.PrivateAddress.Port, NetworkContext.CurrentContext.PublicAddress);
            this.SendMessage(envelope);
        }

        [DiscoveryProtocolMethod(DiscoveryMethod.WhoIs)]
        private void WhoIs(PeerAddress publicAddress, int sourceLocalPort, PeerAddress sourcePublicAddress)
        {
            if (publicAddress.Equals(NetworkContext.CurrentContext.PublicAddress))
            {
                this.Polo(sourceLocalPort, sourcePublicAddress);
            }
        }
        */

        [DiscoveryProtocolMethod(DiscoveryMethod.Polo)]
        private void Polo(int port, PeerAddress publicAddress)
        {
            PeerAddress source = new PeerAddress(this.CurrentEnvelope.Source.Address, port, NatType.Internal);

            if (!this.addressProvider.HasPrivateAddress(source)) // Broadcast sends to self
            {
                this.mConnectionTable.TryInternalConnection(source, publicAddress);
            }
        }

        #region IMessageConsumer<TransportEnvelope> Members

        public void ProcessMessage(TransportEnvelope envelope)
        {
            envelope.SetLabel("Discovery message");
            this.ParseMessage(envelope, 0);
        }

        #endregion
    }
}
