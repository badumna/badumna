using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;

using Badumna.Core;
using Badumna.Utilities;

namespace Badumna.Transport
{
    /// <summary>
    /// Interface for transport protocol.
    /// </summary>
    internal interface ITransportProtocol : IProtocolComponent<TransportEnvelope>
    {
        /// <summary>
        /// Get an envelope for sending a message to a known peer address.
        /// </summary>
        /// <param name="address">The peer address to send the message to.</param>
        /// <param name="qos">The quality of service to send the message with.</param>
        /// <returns>A newly created envelope.</returns>
        TransportEnvelope GetMessageFor(PeerAddress address, QualityOfService qos);

        /// <summary>
        /// Sends a remote call.
        /// </summary>
        /// <param name="destination">The address to send the remote call to.</param>
        /// <param name="qos">The quality of service to send the remote call with.</param>
        /// <param name="function">The function.</param>
        void SendRemoteCall(PeerAddress destination, QualityOfService qos, GenericCallBack function);

        /// <summary>
        /// Sends a remote call.
        /// </summary>
        /// <typeparam name="A">The type of the first parameter.</typeparam>
        /// <param name="destination">The address to send the remote call to.</param>
        /// <param name="qos">The quality of service to send the remote call with.</param>
        /// <param name="function">The function.</param>
        /// <param name="a">The first parameter.</param>
        void SendRemoteCall<A>(PeerAddress destination, QualityOfService qos, GenericCallBack<A> function, A a);

        /// <summary>
        /// Sends a remote call.
        /// </summary>
        /// <typeparam name="A">The type of the first parameter.</typeparam>
        /// <typeparam name="B">The type of the second parameter</typeparam>
        /// <param name="destination">The address to send the remote call to.</param>
        /// <param name="qos">The quality of service to send the remote call with.</param>
        /// <param name="function">The function.</param>
        /// <param name="a">The first parameter.</param>
        /// <param name="b">The second parameter.</param>
        void SendRemoteCall<A, B>(PeerAddress destination, QualityOfService qos, GenericCallBack<A, B> function, A a, B b);

        /// <summary>
        /// Sends a remote call.
        /// </summary>
        /// <typeparam name="A">The type of the first parameter.</typeparam>
        /// <typeparam name="B">The type of the second parameter.</typeparam>
        /// <typeparam name="C">The type of the third parameter.</typeparam>
        /// <param name="destination">The address to send the remote call to.</param>
        /// <param name="qos">The quality of service to send the remote call with.</param>
        /// <param name="function">The function.</param>
        /// <param name="a">The first parameter.</param>
        /// <param name="b">The second parameter.</param>
        /// <param name="c">The third parameter.</param>
        void SendRemoteCall<A, B, C>(PeerAddress destination, QualityOfService qos, GenericCallBack<A, B, C> function, A a, B b, C c);

        /// <summary>
        /// Sends a remote call.
        /// </summary>
        /// <typeparam name="A">The type of the first parameter.</typeparam>
        /// <typeparam name="B">The type of the second parameter.</typeparam>
        /// <typeparam name="C">The type of the third parameter.</typeparam>
        /// <typeparam name="D">The type of the forth parameter.</typeparam>
        /// <param name="destination">The address to send the remote call to.</param>
        /// <param name="qos">The quality of service to send the remote call with.</param>
        /// <param name="function">The function.</param>
        /// <param name="a">The first parameter.</param>
        /// <param name="b">The second parameter.</param>
        /// <param name="c">The third parameter.</param>
        /// <param name="d">The forth parameter</param>
        void SendRemoteCall<A, B, C, D>(PeerAddress destination, QualityOfService qos, GenericCallBack<A, B, C, D> function, A a, B b, C c, D d);

        /// <summary>
        /// Sends a remote call.
        /// </summary>
        /// <typeparam name="A">The type of the first parameter.</typeparam>
        /// <typeparam name="B">The type of the second parameter.</typeparam>
        /// <typeparam name="C">The type of the third parameter.</typeparam>
        /// <typeparam name="D">The type of the forth parameter.</typeparam>
        /// <typeparam name="E">The type of the fifth parameter.</typeparam>
        /// <param name="destination">The address to send the remote call to.</param>
        /// <param name="qos">The quality of service to send the remote call with.</param>
        /// <param name="function">The function.</param>
        /// <param name="a">The first parameter.</param>
        /// <param name="b">The second parameter.</param>
        /// <param name="c">The third parameter.</param>
        /// <param name="d">The forth parameter.</param>
        /// <param name="e">The fifth parameter.</param>
        void SendRemoteCall<A, B, C, D, E>(PeerAddress destination, QualityOfService qos, GenericCallBack<A, B, C, D, E> function, A a, B b, C c, D d, E e);

        /// <summary>
        /// Sends a remote call.
        /// </summary>
        /// <typeparam name="A">The type of the first parameter.</typeparam>
        /// <typeparam name="B">The type of the second parameter.</typeparam>
        /// <typeparam name="C">The type of the third parameter.</typeparam>
        /// <typeparam name="D">The type of the forth parameter.</typeparam>
        /// <typeparam name="E">The type of the fifth parameter.</typeparam>
        /// <typeparam name="F">The type of the sixth parameter.</typeparam>
        /// <param name="destination">The address to send the remote call to.</param>
        /// <param name="qos">The quality of service to send the remote call with.</param>
        /// <param name="function">The function.</param>
        /// <param name="a">The first parameter.</param>
        /// <param name="b">The second parameter.</param>
        /// <param name="c">The third parameter.</param>
        /// <param name="d">The forth parameter.</param>
        /// <param name="e">The fifth parameter.</param>
        /// <param name="f">The sixth parameter.</param>
        void SendRemoteCall<A, B, C, D, E, F>(PeerAddress destination, QualityOfService qos, GenericCallBack<A, B, C, D, E, F> function, A a, B b, C c, D d, E e, F f);
    }

    class TransportProtocol : ProtocolComponent<TransportEnvelope>, ITransportProtocol
    {
        public TransportProtocol(string layerName, Type protocolAttributeType, GenericCallBackReturn<object, Type> factory)
            : base(layerName, protocolAttributeType, factory)
        {
        }

        public TransportProtocol(TransportProtocol parent)
            : base(parent)
        { }

        public TransportEnvelope GetMessageFor(PeerAddress address, QualityOfService qos)
        {
            TransportEnvelope envelope = new TransportEnvelope(address, qos);

            this.PrepareMessageForDeparture(ref envelope);

            return envelope;
        }

        /// <inheritdoc/>
        public void SendRemoteCall(PeerAddress destination, QualityOfService qos, GenericCallBack function)
        {
            TransportEnvelope envelope = this.GetMessageFor(destination, qos);
            this.RemoteCall(envelope, function);
            this.SendMessage(envelope);
        }

        /// <inheritdoc/>
        public void SendRemoteCall<A>(PeerAddress destination, QualityOfService qos, GenericCallBack<A> function, A a)
        {
            TransportEnvelope envelope = this.GetMessageFor(destination, qos);
            this.RemoteCall(envelope, function, a);
            this.SendMessage(envelope);
        }

        /// <inheritdoc/>
        public void SendRemoteCall<A, B>(PeerAddress destination, QualityOfService qos, GenericCallBack<A, B> function, A a, B b)
        {
            TransportEnvelope envelope = this.GetMessageFor(destination, qos);
            this.RemoteCall(envelope, function, a, b);
            this.SendMessage(envelope);
        }

        /// <inheritdoc/>
        public void SendRemoteCall<A, B, C>(PeerAddress destination, QualityOfService qos, GenericCallBack<A, B, C> function, A a, B b, C c)
        {
            TransportEnvelope envelope = this.GetMessageFor(destination, qos);
            this.RemoteCall(envelope, function, a, b, c);
            this.SendMessage(envelope);
        }

        /// <inheritdoc/>
        public void SendRemoteCall<A, B, C, D>(PeerAddress destination, QualityOfService qos, GenericCallBack<A, B, C, D> function, A a, B b, C c, D d)
        {
            TransportEnvelope envelope = this.GetMessageFor(destination, qos);
            this.RemoteCall(envelope, function, a, b, c, d);
            this.SendMessage(envelope);
        }

        /// <inheritdoc/>
        public void SendRemoteCall<A, B, C, D, E>(PeerAddress destination, QualityOfService qos, GenericCallBack<A, B, C, D, E> function, A a, B b, C c, D d, E e)
        {
            TransportEnvelope envelope = this.GetMessageFor(destination, qos);
            this.RemoteCall(envelope, function, a, b, c, d, e);
            this.SendMessage(envelope);
        }

        /// <inheritdoc/>
        public void SendRemoteCall<A, B, C, D, E, F>(PeerAddress destination, QualityOfService qos, GenericCallBack<A, B, C, D, E, F> function, A a, B b, C c, D d, E e, F f)
        {
            TransportEnvelope envelope = this.GetMessageFor(destination, qos);
            this.RemoteCall(envelope, function, a, b, c, d, e, f);
            this.SendMessage(envelope);
        }
    }
}
