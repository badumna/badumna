﻿//---------------------------------------------------------------------------------
// <copyright file="TransportLimiter.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using Badumna.Core;
using Badumna.Utilities;

namespace Badumna.Transport
{
    /// <summary>
    /// The limiter to enforce hard gloabl outbound bandwidth limit.
    /// It is implemented as:
    /// The bandwidth consumption is counted for each samplingRateInMs, all stats of the past 1 second are kept in the
    /// list named as samples. This is used to maintain the bandwidth consumption of the last 1 second. When a packet
    /// is to be sent, the method ProcessPacket is called. Packets will be allowed to be sent if the bandwidth 
    /// consumption of the last second doesn't reach the bandwidth limit, or the limiter check whether the limit for
    /// the current sampling period is reached, the packet and all further outgoing packets sent in that sampling period
    /// will be dropped if so.
    /// This implementation drops all packets sent during a sampling period after the limit is reached for that sampling 
    /// period, such design is pretty naive. 
    /// </summary>
    internal class TransportLimiter
    {
        /// <summary>
        /// The outgoing bandwidth limit.
        /// </summary>
        private int bandwidthLimit;

        /// <summary>
        /// The outgoing bandwidth limit per sampling period.
        /// </summary>
        private int bandwidthLimitPerSamplingPeriod;

        /// <summary>
        /// The package drop rate. 
        /// </summary>
        private int dropRate;

        /// <summary>
        /// The collected samples.
        /// </summary>
        private List<int> samples;

        /// <summary>
        /// The sampling rate.
        /// </summary>
        private int samplingRateInMs = 25;

        /// <summary>
        /// The number of samples to keep;
        /// </summary>
        private int maxSamplesToKeep = 40;

        /// <summary>
        /// The network event queue.
        /// </summary>
        private NetworkEventQueue eventQueue;

        /// <summary>
        /// The bandwidth of all collected samples.
        /// </summary>
        private int totalBandwidthConsumption;

        /// <summary>
        /// The bandwidth consumption for the current sampling period.
        /// </summary>
        private int currentBandwidthConsumption;

        /// <summary>
        /// The sampling task;
        /// </summary>
        private RegularTask samplingTask;

        /// <summary>
        /// Initializes a new instance of the <see cref="TransportLimiter"/> class.
        /// </summary>
        /// <param name="queue">The network event queue.</param>
        /// <param name="connectivityReporter">Reports on network connectivity status.</param>
        /// <param name="bandwidthLimit">The bandwidth limit.</param>
        public TransportLimiter(NetworkEventQueue queue, INetworkConnectivityReporter connectivityReporter, int bandwidthLimit)
            : this(queue, connectivityReporter, bandwidthLimit, 0)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TransportLimiter"/> class.
        /// </summary>
        /// <param name="queue">The network event queue.</param>
        /// <param name="connectivityReporter">Reports on network connectivity status.</param>
        /// <param name="bandwidthLimit">The bandwidth limit in bytes per second.</param>
        /// <param name="dropRate">The packet drop rate.</param>
        public TransportLimiter(NetworkEventQueue queue, INetworkConnectivityReporter connectivityReporter, int bandwidthLimit, int dropRate)
        {
            if (dropRate >= 100 || dropRate < 0)
            {
                throw new ArgumentException("invalid drop rate.");
            }

            if (bandwidthLimit < 0)
            {
                throw new ArgumentException("invalid bandwidth limit.");
            }

            if (bandwidthLimit < 10000)
            {
                Logger.TraceWarning(LogTag.Transport, "very small bandwidth limit.");
            }

            this.eventQueue = queue;
            this.bandwidthLimit = bandwidthLimit;
            this.bandwidthLimitPerSamplingPeriod = this.bandwidthLimit / this.maxSamplesToKeep;
            this.dropRate = dropRate;
            this.samples = new List<int>();

            this.samplingTask = new RegularTask(
                "the transport limiter sampling task", 
                TimeSpan.FromMilliseconds(this.samplingRateInMs), 
                this.eventQueue,
                connectivityReporter,
                this.UpdateSamples);
            this.samplingTask.Start();
        }

        /// <summary>
        /// Processes the packet and determines whether this packet can be added to the send queue.
        /// </summary>
        /// <param name="length">The length of the packet.</param>
        /// <returns>Whether the packet can be added.</returns>
        public bool ProcessPacket(int length)
        {
            int rand = RandomSource.Generator.Next(100);
            if (rand < this.dropRate)
            {
                return false;
            }

            if (this.totalBandwidthConsumption + this.currentBandwidthConsumption + length <= this.bandwidthLimit)
            {
                this.currentBandwidthConsumption += length;
                
                return true;
            }
            else
            {
                if (this.currentBandwidthConsumption + length <= this.bandwidthLimitPerSamplingPeriod)
                {
                    this.currentBandwidthConsumption += length;

                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        /// <summary>
        /// Updates the samples.
        /// </summary>
        private void UpdateSamples()
        {
            if (this.samples.Count >= this.maxSamplesToKeep)
            {
                int sampleToRemove = this.samples[0];
                this.samples.RemoveAt(0);
                this.totalBandwidthConsumption -= sampleToRemove;
            }

            this.samples.Add(this.currentBandwidthConsumption);
            this.totalBandwidthConsumption += this.currentBandwidthConsumption;
            this.currentBandwidthConsumption = 0;
        }
    }
}
