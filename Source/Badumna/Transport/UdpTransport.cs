//------------------------------------------------------------------------------
// <copyright file="UdpTransport.cs" company="National ICT Australia Limited">
//     Copyright (c) 2007-2012 National ICT Australia Limited. All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Net.NetworkInformation;
using System.Threading;

using Badumna.Core;
using Badumna.DataTypes;
using Badumna.Platform;
using Badumna.Utilities;

namespace Badumna.Transport
{
    /// <summary>
    /// Sends and receives messages over the network via UDP.
    /// </summary>
    internal class UdpTransport : Transport
    {
        /// <summary>
        /// Length of the send and receive buffers.
        /// </summary>
        private const int BufferLength = 65527;

        /// <summary>
        /// Indicates whether or not to attempt to keep stateful firewalls open.
        /// </summary>
        private bool sendKeepFirewallOpenMessages;

        /// <summary>
        /// The time the last packet was sent.
        /// </summary>
        private TimeSpan timeOfLastSend;

        /// <summary>
        /// The thread used for background initialization.
        /// </summary>
        private Thread initializationThread;

        /// <summary>
        /// Provides access to the current time.
        /// </summary>
        private ITime timeKeeper;

        /// <summary>
        /// Used to duplicate messages and pass them to the STUN client if neccessary.
        /// </summary>
        private SocketSiphon socketSiphon;

        /// <summary>
        /// The sending thread.
        /// </summary>
        private UDPSendThread sendThread;

        /// <summary>
        /// The receiving thread.
        /// </summary>
        private Thread receiveThread;

        /// <summary>
        /// Used to signal the receiving thread to stop.
        /// </summary>
        private volatile bool continueToReceiveMessages;

        /// <summary>
        /// The port forwarder, which attempts to map private ports to public ports on routers.
        /// </summary>
        private IPortForwarder portForwarder;

        /// <summary>
        /// A regular task that periodically refreshes the port mapping.
        /// </summary>
        private RegularTask renewPortMappingTask;

        /// <summary>
        /// The result/status of the port mapping.
        /// </summary>
        private PortForwardingResult portForwardingResult;

        /// <summary>
        /// The last known valid address, used to detect when the public address has changed.
        /// </summary>
        private PeerAddress lastValidAddress;

        /// <summary>
        /// The outbound bandwidth limiter (used for testing purposes).
        /// </summary>
        private TransportLimiter limiter;

        /// <summary>
        /// Initializes a new instance of the UdpTransport class that sends firewall keep-open packets.
        /// </summary>
        /// <param name="connectivityOptions">The configuration options.</param>
        /// <param name="eventQueue">The event queue to use for scheduling events.</param>
        /// <param name="timeKeeper">The time source.</param>
        /// <param name="connectivityReporter">The network connectivity reporter.</param>
        public UdpTransport(ConnectivityModule connectivityOptions, NetworkEventQueue eventQueue, ITime timeKeeper, INetworkConnectivityReporter connectivityReporter)
            : this(connectivityOptions, true, eventQueue, timeKeeper, connectivityReporter)
        {
        }

        /// <summary>
        /// Initializes a new instance of the UdpTransport class.
        /// </summary>
        /// <param name="connectivityOptions">The configuration options.</param>
        /// <param name="sendKeepFirewallOpenMessages">Indicates whether or not to attempt to keep stateful firewalls open.</param>
        /// <param name="eventQueue">The event queue to use for scheduling events.</param>
        /// <param name="timeKeeper">The time source.</param>
        /// <param name="connectivityReporter">The network connectivity reporter.</param>
        public UdpTransport(ConnectivityModule connectivityOptions, bool sendKeepFirewallOpenMessages, NetworkEventQueue eventQueue, ITime timeKeeper, INetworkConnectivityReporter connectivityReporter)
            : base(eventQueue, timeKeeper, connectivityReporter)
        {
            this.ConnectivityOptions = connectivityOptions;
            this.timeKeeper = timeKeeper;

            this.sendKeepFirewallOpenMessages = sendKeepFirewallOpenMessages;
            this.timeOfLastSend = this.timeKeeper.Now;

            if (this.ConnectivityOptions.IsTransportLimiterEnabled && this.ConnectivityOptions.BandwidthLimit > 0)
            {
                this.limiter = new TransportLimiter(eventQueue, this.ConnectivityReporter, this.ConnectivityOptions.BandwidthLimit, 0);
            }
        }

        /// <summary>
        /// Gets a value indicating whether a UPnP mapping has been made successfully.
        /// </summary>
        /// <remarks>
        /// If UPnP is disabled then this will return false.
        /// </remarks>
        public bool UPnPSucceeded { get; private set; }

        /// <summary>
        /// Gets or sets the communication socket.
        /// </summary>
        protected Socket Socket { get; set; }

        /// <summary>
        /// Gets the configuration options related to connectivity.
        /// </summary>
        protected ConnectivityModule ConnectivityOptions { get; private set; }

        /// <summary>
        /// Initialize the transport layer using the port specified in the connectivity options.
        /// </summary>
        /// <remarks>
        /// See HasFinishedInitializing and IsInitialized for the status of the initialization.
        /// </remarks>
        public override void Initialize()
        {
            this.Initialize(false);
        }

        /// <summary>
        /// Initialize the transport layer using the port specified in the connectivity options.
        /// </summary>
        /// <param name="blockUntilComplete">A value indicating whether this call should be synchronous.</param>
        /// <remarks>
        /// See HasFinishedInitializing and IsInitialized for the status of the initialization.
        /// </remarks>
        public override void Initialize(bool blockUntilComplete)
        {
            if (blockUntilComplete)
            {
                this.PerformInitialization(this.ConnectivityOptions.StartPortRange, this.ConnectivityOptions.EndPortRange);
                return;
            }

            this.initializationThread = new Thread(delegate() { this.PerformInitialization(this.ConnectivityOptions.StartPortRange, this.ConnectivityOptions.EndPortRange); });
            this.initializationThread.Start();
        }

        /// <summary>       
        /// Initialize the transport layer binding to the given port number.
        /// </summary>
        /// <remarks>
        /// See HasFinishedInitializing and IsInitialized for the status of the initialization.
        /// </remarks>
        /// <param name="port">The port to bind to.</param>
        public void Initialize(int port)
        {
            this.initializationThread = new Thread(delegate() { this.PerformInitialization(port, port); });
            this.initializationThread.Start();
        }

        /// <summary>
        /// Stop listening on the socket and join all threads.
        /// </summary>
        public override void Shutdown()
        {
            if (this.portForwarder != null)
            {
                this.portForwarder.Dispose();
                this.portForwarder = null;
            }

            if (this.renewPortMappingTask != null)
            {
                this.renewPortMappingTask.Stop();
                this.renewPortMappingTask = null;
            }

            if (this.IsInitialized)
            {
                if (null != this.initializationThread && this.initializationThread.IsAlive)
                {
                    this.initializationThread.Abort();
                    this.initializationThread.Join();
                    this.initializationThread = null;
                }

                this.IsInitialized = false;
            }

            if (this.receiveThread != null)
            {
                this.continueToReceiveMessages = false;
                this.receiveThread.Join(500);
            }

            if (this.sendThread != null)
            {
                this.sendThread.Shutdown();
            }

            //// TODO(lni): the following code will throw exception when Shutdown is called shortly after program 
            //// launch, it seems to be be fine if Shutdown is called 30-60 seconds after the program launch. 
            //// need to check what is causing the exception. 
            try
            {
                if (this.Socket != null)
                {
                    this.Socket.Blocking = false;
                    this.Socket.Shutdown(System.Net.Sockets.SocketShutdown.Both);
                    this.Socket.Close();
                    this.Socket = null;
                }
            }
            catch
            {
            }
        }

        /// <summary>
        /// Asyncronously attempt to check the public address, re-initializing the transport layer if necessary.
        /// </summary>
        public void CheckConnectivityStatus()
        {
            if (!this.ConnectivityOptions.PerformStun)
            {
                // we can't check the public address if we are using LAN test mode. 
                return;
            }

            if (this.ConnectivityReporter.Status == ConnectivityStatus.Initializing ||
                this.ConnectivityReporter.Status == ConnectivityStatus.ShuttingDown)
            {
                return;
            }

            if (this.Socket == null)
            {
                // The peer has not managed to initialize in the past.
                this.ConnectivityReporter.SetStatus(ConnectivityStatus.Initializing);
                return;
            }

            if (this.socketSiphon == null)
            {
                // The peer is not currently checking the status
                this.socketSiphon = new SocketSiphon(this.Socket, this.EventQueue);
                Thread thread = new Thread(this.CheckPublicAddress);
                thread.Start();
            }
        }

        /// <summary>
        /// Attempts to create and initialize a socket for UDP communication on the given port.
        /// Can perform STUN and Port forwarding depending on the configuration options in the ConnectivityModule.
        /// </summary>
        /// <param name="port">The port to try.</param>
        /// <returns>True if the initialization was successful</returns>
        protected virtual bool InitializeSocket(int port)
        {
            Socket socket = null;

            try
            {
                List<PeerAddress> localAddresses = Badumna.Platform.Net.GetLocalAddresses(port);

                if (localAddresses.Count == 0)
                {
                    Logger.TraceInformation(LogTag.Transport, "Cannot initialize transport layer because no local addresses are known.");
                    return false;
                }

                Logger.TraceInformation(LogTag.Transport, "Attempting to use port {0}", port);

                socket = this.CreateSocket(new IPEndPoint(IPAddress.Any, port), false);
                if (null == socket)
                {
                    Logger.TraceWarning(LogTag.Transport, "Failed to create socket");
                    return false;
                }

                if (this.AttemptInitialize(socket, localAddresses))
                {
                    this.Socket = socket;
                    this.sendThread = new UDPSendThread(this.Socket, new Action<int>(this.UpdateSendStatistics));
                    return true;
                }

                socket.Close();

                return false;
            }
            catch (Exception e)
            {
                Logger.TraceException(LogLevel.Error, e, "Failed attempt to intialize socket");
                return false;
            }
            finally
            {
                // Must set the public address to something.
                if (this.PublicAddress == null)
                {
                    this.SetAddresses(PeerAddress.Nowhere);
                }
            }
        }

        /// <summary>
        /// Creates a socket to use for UDP communication. 
        /// </summary>
        /// <param name="localEndPoint">The local address to bind to.</param>
        /// <param name="reuse">Specifies whether to set the resuse socket opetion, useful for broadcast.</param>
        /// <returns>The newly created socket if successful, null otherwise</returns>
        protected Socket CreateSocket(IPEndPoint localEndPoint, bool reuse)
        {
            Socket socket = null;

            try
            {
                socket = Socket.Create(localEndPoint.AddressFamily, System.Net.Sockets.SocketType.Dgram, System.Net.Sockets.ProtocolType.Udp);
                socket.SendBufferSize = UdpTransport.BufferLength;
                socket.ReceiveBufferSize = UdpTransport.BufferLength;
                socket.EnableBroadcast = true;

                if (reuse)
                {
                    socket.SetSocketOption(System.Net.Sockets.SocketOptionLevel.Socket, System.Net.Sockets.SocketOptionName.ReuseAddress, 1);
                }

                if (localEndPoint.Address.AddressFamily == System.Net.Sockets.AddressFamily.InterNetworkV6)
                {
                    Logger.TraceInformation(LogTag.Transport, "Using IPv6 socket.");

                    // Set the socket to dual IPv4 and IPv6 mode.
                    socket.SetSocketOption(System.Net.Sockets.SocketOptionLevel.IPv6, (System.Net.Sockets.SocketOptionName)27, 0);
                }

                if (ExecutionPlatform.IsPlatformWindows)
                {
                    // Set option to stop invalid exceptions for udp socket.
                    const int SIO_UDP_CONNRESET = -1744830452;
                    byte[] inValue = new byte[] { 0, 0, 0, 0 };     // == false
                    byte[] outValue = new byte[] { 0, 0, 0, 0 };    // initialize to 0
                    socket.IOControl(SIO_UDP_CONNRESET, inValue, outValue);
                }

                socket.Bind(localEndPoint);
            }
            catch (System.Net.Sockets.SocketException)
            {
                Logger.TraceInformation(LogTag.Transport, "Failed to bind to port {0}", localEndPoint.Port);
                return null;
            }
            catch (Exception e)
            {
                Logger.TraceException(LogLevel.Error, e, "Failed to initialize socket on port {0}", localEndPoint.Port);
                return null;
            }

            return socket;
        }

        /// <summary>
        /// A specialization of PreProcessMessage. It primarily provides the ability 
        /// to pass packets to the STUN client before initialization of the transport layer is complete. 
        /// </summary>
        /// <param name="envelope">The received message envelope</param>
        /// <returns>False when the message should not be processed by the protocol stack.</returns>
        protected override bool PreProcessMessage(TransportEnvelope envelope)
        {
            // If a socket siphon exists (used by STUN client)
            if (this.socketSiphon != null)
            {
                // Then copy the message and pass to the siphon
                this.socketSiphon.ReceivePacket(new TransportEnvelope(envelope));
            }

            return base.PreProcessMessage(envelope);
        }

        /// <inheritdoc />
        protected override void SendPacket(TransportEnvelope envelope)
        {
            if (!this.IsInitialized)
            {
                Logger.TraceError(LogTag.Transport, "Cannot send message since BaseTransportLayer is uninitialized. Dropping message.");
                return;
            }

            this.timeOfLastSend = this.timeKeeper.Now; // Record the time of the last send to an external address.

            if (this.ConnectivityReporter.Status != ConnectivityStatus.Offline)
            {
                byte[] data = envelope.Message.GetBuffer();

                if (this.ConnectivityOptions.IsTransportLimiterEnabled)
                {
                    if (!this.limiter.ProcessPacket(data.Length))
                    {
                        return;
                    }
                }

                this.sendThread.Add(data, envelope.Destination.EndPoint);
            }
        }

        /// <summary>
        /// Attempt to bind to a socket in the range of ports specified. If successful a call to BeginListen is made.
        /// </summary>
        /// <param name="portRangeStart">The start of the port range to select a port from (inclusive).</param>
        /// <param name="portRangeEnd">The end of the port range to select a port from (inclusive).</param>
        private void PerformInitialization(int portRangeStart, int portRangeEnd)
        {
            int startPort = Math.Min(portRangeStart, portRangeEnd);
            int endPort = Math.Max(portRangeStart, portRangeEnd);
            this.HasFinishedInitializing = false; // In case of re-initialization

            // Pick a port in the range at random to avoid using the same port each time, which can confuse some gateway routers.
            int portRange = endPort - startPort + 1;
            int randomOffset = RandomSource.Generator.Next(portRange);

            int port = startPort + randomOffset;
            int i = 0;

            int maxPortsToTry = this.ConnectivityOptions.MaxPortsToTry;
            if (maxPortsToTry <= 0)
            {
                maxPortsToTry = int.MaxValue;
            }

            // Try each port in the range sequentially, wrapping to start if required, 
            // until successfull or all ports have failed.
            do
            {
                try
                {
                    if (this.InitializeSocket(port))
                    {
                        Logger.TraceInformation(LogTag.Transport, "Transport layer initialization successful (listening on port {0})", port);

                        if (this.EventQueue.IsRunning)
                        {
                            // Set the listen in a call back to avoid thread issues.
                            this.EventQueue.Push(this.BeginListen);
                        }
                        else
                        {
                            // No NetworkEventQueue running yet so no problem if we just execute this method directly.
                            // (Probably called from the NetworkFacade class to determine whether we need tunnelling or not)
                            this.BeginListen();
                        }

                        return;
                    }
                }
                catch (Exception e)
                {
                    Logger.TraceInformation(LogTag.Transport, "Initialization failed : {0}", e.Message);
                }

                i += 1;
                port = startPort + ((randomOffset + i) % portRange);
            }
            while (port >= 0 && i < portRange && i < maxPortsToTry);
            this.HasFinishedInitializing = true;
        }

        /// <summary>
        /// Start receiving and processing messages.
        /// </summary>
        private void BeginListen()
        {
            this.IsInitialized = true;

            this.receiveThread = new Thread(this.ReceiveMessages);
            this.receiveThread.IsBackground = true;
            this.receiveThread.Start();
            
            this.HasFinishedInitializing = true;

            if (this.sendKeepFirewallOpenMessages)
            {
                this.EventQueue.Schedule(Parameters.MinimumSendPacketTimeout.TotalMilliseconds, this.KeepFirewallOpen);
            }
        }

        /// <summary>
        /// Periodically send messages somewhere to keep stateful firewalls open.
        /// </summary>
        private void KeepFirewallOpen()
        {
            TimeSpan now = this.timeKeeper.Now;

            // If the maximum allowable time since a send has expired then must send a message somewhere.
            bool mustSend = (now - this.timeOfLastSend) >= Parameters.MinimumSendPacketTimeout;
            if (mustSend)
            {
                this.timeOfLastSend = now;
            }

            TimeSpan delayUntilNextCheck = (this.timeOfLastSend + Parameters.MinimumSendPacketTimeout) - now;

            // Push the next event as soon as possible in case some later call throws an exception
            this.EventQueue.Schedule(delayUntilNextCheck.TotalMilliseconds, this.KeepFirewallOpen);

            if (mustSend)
            {
                // Pick an private address to send to.
                PeerAddress destination = PeerAddress.GetAddress("10.0.0.1", 9999);

                if (destination.Address.Equals(this.PublicAddress.Address))
                {
                    destination = PeerAddress.GetAddress("10.0.0.2", 9999);
                }

                TransportEnvelope envelope = new TransportEnvelope(destination, QualityOfService.Unreliable);
                this.SendMessage(envelope);
            }
        }        

        /// <summary>
        /// Try to initialize the given socket.
        /// Depending on configuration options this will attempt portforwarding and then STUN
        /// to determine the public address of the port. If no public address is found then the 
        /// initialization will fail. 
        /// </summary>
        /// <param name="socket">The socket </param>
        /// <param name="localAddresses">The addresses of the local interfaces</param>
        /// <returns>True if STUN was successful and the public address is valid, false otherwise</returns>
        private bool AttemptInitialize(Socket socket, List<PeerAddress> localAddresses)
        {
            int mappedExternalPort = -1;

            try
            {
                // try port forwarding if enabled.
                if (this.ConnectivityOptions.IsPortForwardingEnabled && localAddresses.Count > 0)
                {
                    if (this.portForwarder != null)
                    {
                        this.portForwarder.Dispose();
                        this.portForwarder = null;
                    }

                    Logger.TraceInformation(LogTag.Transport, "Attempting to forward port {0}", localAddresses[0].Port);

                    IPAddress externalAddress = null;
                    IPortForwarder portForwarder = new CombinedPortForwarder();
                    if (portForwarder.Initialize(localAddresses[0].Address))
                    {
                        externalAddress = portForwarder.GetExternalAddress();
                        if (externalAddress != null)
                        {
                            Logger.TraceInformation(LogTag.Transport, "External address is : {0}", externalAddress);
                        }
                        else
                        {
                            Logger.TraceWarning(LogTag.Transport, "Failed to get the external ip from the port forwarder.");
                        }
                    }
                    else
                    {
                        Logger.TraceWarning(LogTag.Transport, "Port forwarder failed to initialize.");
                    }

                    TimeSpan lease = TimeSpan.FromSeconds(600 + Parameters.PortMappingLeaseTime.TotalSeconds);
                    this.portForwardingResult = portForwarder.TryMapPort(localAddresses[0].EndPoint, lease);
                    if (this.portForwardingResult.Succeed)
                    {
                        this.UPnPSucceeded = true;

                        this.portForwarder = portForwarder;
                        mappedExternalPort = this.portForwardingResult.ExternalPort;
                        if (this.portForwardingResult.LeaseInSeconds > 0)
                        {
                            this.renewPortMappingTask = new RegularTask(
                                "Renew port mapping", 
                                Parameters.PortMappingLeaseTime,
                                this.EventQueue,
                                this.ConnectivityReporter,
                                this.RenewPortMappingLeaseTask);
                            this.renewPortMappingTask.Start();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Logger.TraceException(LogLevel.Warning, e, "Failed to perform port forwarding request.");
            }

            // Perform STUN query if enabled.
            try
            {
                if (this.ConnectivityOptions.PerformStun)
                {
                    return this.PerformStunQuery(socket, localAddresses, mappedExternalPort);
                }
                else
                {
                    PeerAddress address = new PeerAddress(localAddresses[0].EndPoint, NatType.Open);
                    Logger.TraceInformation(LogTag.Transport, "No STUN servers defined, NAT type assumed to be {0}", address);

                    this.SetAddresses(localAddresses, address);

                    return true;
                }
            }
            catch (Exception e)
            {
                Logger.TraceException(LogLevel.Warning, e, "Attempted STUN query failed");
                return false;
            }
        }

        /// <summary>
        /// Performs a STUN query with the given parameters.
        /// </summary>
        /// <param name="socket">The socket to use</param>
        /// <param name="localAddresses">The addresses of the local interfaces</param>
        /// <param name="expectedExternalPort">The external port expected to be returned by the STUN query if known (e.g. as a result of port mapping), or -1 if unknown</param>
        /// <returns>True if the STUN query was successful.</returns>
        private bool PerformStunQuery(Socket socket, List<PeerAddress> localAddresses, int expectedExternalPort)
        {
            StunClient stunClient = new StunClient(socket, localAddresses, expectedExternalPort);
            PeerAddress publicAddress = null;
            Mutex mutex = new Mutex(false, "BadumnaStunQueryMutex");
            mutex.WaitOne();
            try 
            {
                publicAddress = this.PerformStunQuery(stunClient);
            }
            finally
            {
                mutex.ReleaseMutex();
            }

            if (null != publicAddress && publicAddress.NatType != NatType.Unknown && publicAddress.NatType != NatType.Blocked)
            {
                // for symmetric nat peer, the public port obtained during the stun query
                // is the external port used for contacting that particular stun server. we can't afford to keep sending
                // keep alive packets to the stun server and thus couldn't make sure the above mentioned external
                // port will always be reserved by the NAT. 
                // the random port is used here to identify the peer. 
                if (publicAddress.NatType == NatType.SymmetricNat)
                {
                    int randomPort = RandomSource.Generator.Next(65535 - 1024) + 1024;
                    Debug.Assert(randomPort >= 1024 && randomPort < 65535, "Port must be in range 1024 - 65534");
                    publicAddress.Port = randomPort;
                }

                Logger.TraceInformation(LogTag.Transport, "Setting public address as : {0}", publicAddress);
                Logger.TraceInformation(LogTag.Transport, "Address is {0}, local port is {1}", publicAddress.Address, publicAddress.Port);
                this.SetAddresses(localAddresses, publicAddress);

                if (publicAddress.IsValid)
                {
                    this.lastValidAddress = publicAddress;
                }

                return true;
            }

            Logger.TraceError(LogTag.Transport, "Failed STUN queries. Giving up.");
            return false;
        }

        /// <summary>
        /// Performs a STUN query using the given StunClient.
        /// </summary>
        /// <param name="stunClient">The StunClient to use</param>
        /// <returns>The public address</returns>
        private PeerAddress PerformStunQuery(StunClient stunClient)
        {
            foreach (string stunServer in this.ConnectivityOptions.StunServers)
            {
                try
                {
                    Logger.TraceInformation(LogTag.Transport, "Attempting stun request with {0}", stunServer);

                    PeerAddress serverEndPoint = PeerAddress.GetAddress(stunServer, 3478);
                    if (!serverEndPoint.IsValid)
                    {
                        Logger.TraceWarning(LogTag.Transport, "Invalid STUN address {0}", serverEndPoint);
                        continue;
                    }

                    stunClient.InitiateRequest(serverEndPoint.EndPoint);
                    if (stunClient.NatType != NatType.Unknown && stunClient.NatType != NatType.Blocked)
                    {
                        Logger.TraceInformation(LogTag.Transport, "NAT type : {0}", stunClient.PublicAddress.HumanReadableNAT());
                        return stunClient.PublicAddress;
                    }

                    Logger.TraceWarning(LogTag.Transport, "STUN failure : {0}", stunClient.FailureReason);
                }
                catch (Exception e)
                {
                    Logger.TraceWarning(LogTag.Transport, "Failed STUN query with {0} with exception : {1}", stunServer, e.Message);
                }
            }

            Logger.TraceError(LogTag.Transport, "Failed STUN queries. Giving up.");
            return new PeerAddress(PeerAddress.Nowhere.EndPoint, NatType.Blocked);
        }

        /// <summary>
        /// Peform a STUN query to determine the current public address of the peer. If the 
        /// public address has changed or was unknown the appropriate network context status will be triggered,
        /// possibly resulting in the re-initialization of the protocol stack.
        /// </summary>
        /// <remarks>
        /// This method is called in a separate thread.
        /// </remarks>
        private void CheckPublicAddress()
        {
            Debug.Assert(this.Socket != null, "Attempting to check status when the socket is null");
            Debug.Assert(this.socketSiphon != null, "Attempting to check status when the socket siphon is null");

            Logger.TraceInformation(LogTag.Transport, "Checking connectivity status");

            int mappedExternalPort = -1;
            
            try
            {
                List<PeerAddress> localAddresses = Badumna.Platform.Net.GetLocalAddresses((this.Socket.LocalEndPoint as IPEndPoint).Port);
                if (localAddresses.Count == 0)
                {
                    Logger.TraceInformation(LogTag.Transport, "Abandoning connectivity check because the local address is unknown");
                    return;
                }

                // Do nothing when initializing and shutting down.
                if (this.ConnectivityReporter.Status == ConnectivityStatus.Initializing ||
                    this.ConnectivityReporter.Status == ConnectivityStatus.ShuttingDown)
                {
                    Logger.TraceInformation(LogTag.Transport, "Abandoning connectivity check because the current state is in transition");
                    return;
                }

                try
                {
                    // UPnP is enabled.
                    if (this.ConnectivityOptions.IsPortForwardingEnabled && localAddresses.Count > 0 &&
                        this.portForwarder != null)
                    {
                        TimeSpan lease = TimeSpan.FromSeconds(600 + Parameters.PortMappingLeaseTime.TotalSeconds);
                        PortForwardingResult result = this.portForwarder.TryMapPort(localAddresses[0].EndPoint, lease);
                        if (result != null && result.Succeed)
                        {
                            mappedExternalPort = result.ExternalPort;
                            if (result.LeaseInSeconds > 0)
                            {
                                if (this.renewPortMappingTask != null && this.renewPortMappingTask.IsRunning)
                                {
                                    this.renewPortMappingTask.Stop();
                                }
                                else
                                {
                                    this.renewPortMappingTask = new RegularTask(
                                        "Renew port mapping", 
                                        Parameters.PortMappingLeaseTime,
                                        this.EventQueue,
                                        this.ConnectivityReporter,
                                        this.RenewPortMappingLeaseTask);
                                    this.renewPortMappingTask.Start();
                                }
                            }
                        }
                    }

                    StunClient stunClient = new StunClient(this.socketSiphon, localAddresses, mappedExternalPort);
                    PeerAddress newPublicAddress = this.PerformStunQuery(stunClient);

                    // check the return new public address, notify the user if the public address has changed.
                    this.NotifyChangedPublicAddress(newPublicAddress);
                    
                    // Check again for shutting down and initializing state to ensure they didn't change while performing STUN query.
                    if (this.ConnectivityReporter.Status == ConnectivityStatus.Initializing ||
                        this.ConnectivityReporter.Status == ConnectivityStatus.ShuttingDown)
                    {
                        Logger.TraceInformation(LogTag.Transport, "Abandoning connectivity check because the current state is in transition");
                        return;
                    }

                    if (this.ConnectivityReporter.Status == ConnectivityStatus.Online)
                    {
                        if (newPublicAddress.Equals(this.PublicAddress) &&
                            newPublicAddress.NatType == this.PublicAddress.NatType)
                        {
                            Logger.TraceInformation(LogTag.Transport, "Network connectivity status unchanged.");
                            return;
                        }

                        // Either connectivity is lost or the public address has changed.
                        this.EventQueue.Push(delegate() { this.ConnectivityReporter.SetStatus(ConnectivityStatus.ShuttingDown); });
                        this.EventQueue.Schedule(1000, delegate() { this.ConnectivityReporter.SetStatus(ConnectivityStatus.Offline); });

                        if (!newPublicAddress.IsValid || newPublicAddress.NatType == NatType.Blocked || newPublicAddress.NatType == NatType.Unknown)
                        {
                            // No address was found so we should go offline.
                            return;
                        }
                    }

                    // else continue to schedule coming online again
                    if (this.ConnectivityReporter.Status == ConnectivityStatus.Offline &&
                        newPublicAddress.NatType == NatType.Blocked)
                    {
                        // Connectivity has been lost
                        Logger.TraceInformation(LogTag.Transport, "Network connectivity is still offline");
                        return;
                    }

                    // Either the peer was offline or has just scheduled to go offline so schedule to come online
                    this.EventQueue.Schedule(
                        1001,
                        delegate(List<PeerAddress> privateAddresses, PeerAddress publicAddress)
                        {
                            Logger.TraceInformation(LogTag.Transport, "Public address has changed to {0}. Scheduling restart.", publicAddress);
                            this.SetAddresses(privateAddresses, publicAddress);
                            this.ConnectivityReporter.SetStatus(ConnectivityStatus.Initializing);
                        },
                        localAddresses,
                        newPublicAddress);

                    this.EventQueue.Schedule(
                        2000,
                        delegate() 
                        {
                            this.ConnectivityReporter.SetStatus(ConnectivityStatus.Online); 
                        });
                }
                catch (Exception e)
                {
                    Logger.TraceException(LogLevel.Warning, e, "Failed STUN query");
                }
            }
            finally
            {
                this.socketSiphon = null;
            }
        }

        /// <summary>
        /// Checks whether the given address is different from the last known public address,
        /// and triggers change notifications if so.
        /// </summary>
        /// <param name="newPublicAddress">The new public address</param>
        private void NotifyChangedPublicAddress(PeerAddress newPublicAddress)
        {
            if (this.lastValidAddress != null)
            {
                if (this.lastValidAddress.NatType != newPublicAddress.NatType)
                {
                    // NAT type changed.
                    this.InvokePublicAddressChangedHandlers();
                }
                else if (!this.lastValidAddress.Address.Equals(newPublicAddress.Address))
                {
                    // IP address changed.
                    this.InvokePublicAddressChangedHandlers();
                }
                else if (this.lastValidAddress.NatType != NatType.SymmetricNat)
                {
                    // not symmetric nat. 
                    if (this.lastValidAddress.Port != newPublicAddress.Port)
                    {
                        // port changed.
                        this.InvokePublicAddressChangedHandlers();
                    }
                }
                else
                {
                    // is symmetric nat
                    newPublicAddress.Port = this.lastValidAddress.Port;
                }
            }
            else
            {
                if (newPublicAddress != null && newPublicAddress.IsValid)
                {
                    this.lastValidAddress = newPublicAddress;
                }
            }
        }

        /// <summary>
        /// Renews an existing port mapping.
        /// </summary>
        private void RenewPortMappingLeaseTask()
        {
            try
            {
                if (this.portForwardingResult == null || this.portForwardingResult.LeaseInSeconds == 0)
                {
                    Debug.Assert(false, "The port mapping lease task should not be started.");
                }

                PortForwardingResult result;
                TimeSpan lease = TimeSpan.FromSeconds(600 + Parameters.PortMappingLeaseTime.TotalSeconds);
                result = this.portForwarder.TryMapPort(this.PrivateAddressesList[0].EndPoint, lease);
                if (!result.Succeed)
                {
                    Logger.TraceError(LogTag.Transport, "Failed to renew port forwarding.");
                    return;
                }

                if (result.ExternalPort != this.portForwardingResult.ExternalPort)
                {
                    Logger.TraceInformation(
                        LogTag.Transport,
                        "Renewed port mapping is different. Used to be {0} now it is {1}", 
                        this.portForwardingResult.ExternalPort,
                        result.ExternalPort);
                    this.portForwardingResult = result;
                    this.EventQueue.Push(this.CheckConnectivityStatus);
                }
            }
            catch (Exception e)
            {
                Logger.TraceException(LogLevel.Error, e, "Exception caught when trying to renew port mapping.");
            }
        }

        /// <summary>
        /// Continuously read messages off the socket.
        /// </summary>
        private void ReceiveMessages()
        {
            int timeoutMilliSeconds = 300;
            int timeoutMicroSeconds = timeoutMilliSeconds * 1000;
            this.Socket.ReceiveTimeout = timeoutMilliSeconds;

            this.continueToReceiveMessages = true;
            byte[] buffer = new byte[UdpTransport.BufferLength];

            while (this.continueToReceiveMessages && this.IsInitialized)
            {
                try
                {
                    if (!this.Socket.Poll(timeoutMicroSeconds, System.Net.Sockets.SelectMode.SelectRead))
                    {
                        continue;
                    }

                    EndPoint receiveEndPoint = new IPEndPoint(IPAddress.Any, 0);
                    int numBytesRead = this.Socket.ReceiveFrom(buffer, ref receiveEndPoint);
                    if (numBytesRead == 0)
                    {
                        continue;
                    }

                    // Create a message with the read data.
                    var message = new MessageBuffer();
                    message.Write(buffer, numBytesRead);
                    var envelope = new TransportEnvelope(
                        message,
                        new PeerAddress((IPEndPoint)receiveEndPoint, NatType.Unknown),
                        Parameters.EncryptTraffic,
                        this.timeKeeper.Now);

                    this.ProcessMessageArrival(envelope);
                }
                catch (Exception e)
                {
                    Logger.TraceException(LogLevel.Error, e, "Exception caught when handling packet");
                }
            }

            Logger.TraceInformation(LogTag.Transport, "UDPTransport (or derived) ReceiveMessages exited");
        }
    }
}
