﻿//---------------------------------------------------------------------------------
// <copyright file="UPnPService.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

namespace Badumna.Transport.Upnp
{
    /// <summary>
    /// The UPnP service class.
    /// </summary>
    internal class UPnPService
    {
        /// <summary>
        /// The service type.
        /// </summary>
        private string serviceType;

        /// <summary>
        /// The service id.
        /// </summary>
        private string serviceId;

        /// <summary>
        /// The control url.
        /// </summary>
        private string controlURL;

        /// <summary>
        /// The event sub url.
        /// </summary>
        private string eventSubURL;

        /// <summary>
        /// The SCPD url.
        /// </summary>
        private string scpdURL;

        /// <summary>
        /// Initializes a new instance of the <see cref="UPnPService"/> class.
        /// </summary>
        public UPnPService()
        {
        }

        /// <summary>
        /// Gets or sets the type of the service.
        /// </summary>
        /// <value>The type of the service.</value>
        public string ServiceType
        {
            get { return this.serviceType; }
            set { this.serviceType = value; }
        }

        /// <summary>
        /// Gets or sets the service id.
        /// </summary>
        /// <value>The service id.</value>
        public string ServiceId
        {
            get { return this.serviceId; }
            set { this.serviceId = value; }
        }

        /// <summary>
        /// Gets or sets the control URL.
        /// </summary>
        /// <value>The control URL.</value>
        public string ControlURL
        {
            get { return this.controlURL; }
            set { this.controlURL = value; }
        }

        /// <summary>
        /// Gets or sets the event sub URL.
        /// </summary>
        /// <value>The event sub URL.</value>
        public string EventSubURL
        {
            get { return this.eventSubURL; }
            set { this.eventSubURL = value; }
        }

        /// <summary>
        /// Gets or sets the SCPDURL.
        /// </summary>
        /// <value>The SCPDURL.</value>
        public string SCPDURL
        {
            get { return this.scpdURL; }
            set { this.scpdURL = value; }
        }

        /// <summary>
        /// Copies from.
        /// </summary>
        /// <param name="rhs">The service to copy.</param>
        public void CopyFrom(UPnPService rhs)
        {
            this.controlURL = rhs.controlURL;
            this.eventSubURL = rhs.eventSubURL;
            this.scpdURL = rhs.scpdURL;
            this.serviceId = rhs.serviceId;
            this.serviceType = rhs.serviceType;
        }
    }
}