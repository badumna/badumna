﻿//---------------------------------------------------------------------------------
// <copyright file="SimpleServiceDiscovery.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Text;

using Badumna.Platform;
using Badumna.Utilities;

namespace Badumna.Transport.Upnp
{
    /// <summary>
    /// Service discovery class used to discover UPnP devices on the local network. 
    /// </summary>
    internal class SimpleServiceDiscovery
    {
        /// <summary>
        /// The number of attempts to try.
        /// </summary>
        private const int NumberOfAttempts = 3;

        /// <summary>
        /// Receive timeout value in seconds.
        /// </summary>
        private const int ReceiveTimeoutSecoonds = 3;

        /// <summary>
        /// The default port.
        /// </summary>
        private const int SSDPDefaultPort = 1900;

        /// <summary>
        /// The local IP address.
        /// </summary>
        private IPAddress localAddress;

        /// <summary>
        /// Initializes a new instance of the <see cref="SimpleServiceDiscovery"/> class.
        /// </summary>
        /// <param name="localAddress">The local address.</param>
        public SimpleServiceDiscovery(IPAddress localAddress)
        {
            this.localAddress = localAddress;
        }

        /// <summary>
        /// Discovers the specified device.
        /// </summary>
        /// <param name="device">The device.</param>
        /// <returns>SSDP response or null on error.</returns>
        public SsdpResponse Discover(string device)
        {
            return this.Discover(SSDPDefaultPort, device, SimpleServiceDiscovery.ReceiveTimeoutSecoonds, null);
        }

        /// <summary>
        /// Discovers the specified device.
        /// </summary>
        /// <param name="device">The device.</param>
        /// <param name="gatewayAddress">The gateway address.</param>
        /// <returns>SSDP response or null on error.</returns>
        public SsdpResponse Discover(string device, IPAddress gatewayAddress)
        {
            return this.Discover(SSDPDefaultPort, device, SimpleServiceDiscovery.ReceiveTimeoutSecoonds, gatewayAddress);
        }

        /// <summary>
        /// Discovers the specified device.
        /// </summary>
        /// <param name="port">The device port.</param>
        /// <param name="searchTarget">The search target.</param>
        /// <param name="responseDelaySeconds">The response delay seconds.</param>
        /// <param name="gatewayAddress">The gateway address.</param>
        /// <returns>SSDP response or null on error.</returns>
        public SsdpResponse Discover(int port, string searchTarget, int responseDelaySeconds, IPAddress gatewayAddress)
        {
            IPEndPoint broadcastEndpoint = new IPEndPoint(new IPAddress(new byte[] { 239, 255, 255, 250 }), port);
            //// The gateway is usually user's NAT box, the M-SEARCH message can thus be directly sent to the 
            //// gateway/NAT. This is required because some buggy NATs simply ignore the above broadcast message. 
            //// This workaround method is borrowed from the Thunder P2P Downloader.
            IPEndPoint gatewayEndpoint = null;
            bool guessedGatewayUsed = false;

            if (null != gatewayAddress)
            {
                gatewayEndpoint = new IPEndPoint(gatewayAddress, port);
            }

            using (Socket broadcastSocket = this.CreateSocket(true),
                          udpSocket = this.CreateSocket(false))
            {
                // byte[] requestBuffer = null;

                // Send the request few times to provide better reliability
                for (int i = 0; i < SimpleServiceDiscovery.NumberOfAttempts; i++)
                {
                    byte[] requestBuffer = this.GetRequest(broadcastEndpoint, searchTarget, ReceiveTimeoutSecoonds);
                    broadcastSocket.SendTo(requestBuffer, broadcastEndpoint);

                    if (null != gatewayAddress)
                    {
                        requestBuffer = this.GetRequest(gatewayEndpoint, searchTarget, ReceiveTimeoutSecoonds);
                        int sent = udpSocket.SendTo(requestBuffer, gatewayEndpoint);
                        if (sent != requestBuffer.Length)
                        {
                            return null;
                        }
                    }
                    else
                    {
                        // when we can't determine the gateway address, we try to send the discovery message to some 
                        // guessed gateway addresses. 
                        // e.g. when running on the 1.2.5 version of mono (unity 2.6), its network interface class is 
                        // not implemented and thus we will not get the gateway address from the mono runtime 
                        // environment. Also, buggy routers will not respond to the discovery search broadcast. 
                        guessedGatewayUsed = this.SendDiscoveryRequestToGuessedGateway(udpSocket, searchTarget);
                    }
                }

                // Set the timeout on the socket to a little greater than the specified response delay.
                double totalMilliseconds = TimeSpan.FromSeconds(responseDelaySeconds).TotalMilliseconds;
                int receiveTimeoutMs = ((int)totalMilliseconds * 1000) + 500;
                SsdpResponse udpResponse = null;

                do
                {
                    DateTime start = DateTime.Now;
                    List<Socket> readSockets = new List<Socket>();
                    readSockets.Add(broadcastSocket);

                    if (null != gatewayAddress || guessedGatewayUsed)
                    {
                        readSockets.Add(udpSocket);
                    }

                    // timeout after 1000000 microseconds
                    Socket.Select(readSockets, null, null, 1000000);
                    receiveTimeoutMs = receiveTimeoutMs - (int)((DateTime.Now - start).TotalMilliseconds * 1000);

                    foreach (Socket socket in readSockets)
                    {
                        // response to the broadcasted message received.
                        if (socket == broadcastSocket)
                        {
                            SsdpResponse ssdpResponse = this.GetResponse(broadcastSocket, searchTarget);
                            if (null != ssdpResponse)
                            {
                                // such response is good enough
                                return ssdpResponse;
                            }
                        }

                        if ((null != gatewayAddress || guessedGatewayUsed) && socket == udpSocket)
                        {
                            // we have the response, but we will wait to see whether it is still possible to get
                            // response to our broadcast message. 
                            udpResponse = this.GetResponse(udpSocket, searchTarget);
                        }
                    }
                }
                while (receiveTimeoutMs > 0); // While time remains. In case another packet is 

                return udpResponse;
            }
        }

        /// <summary>
        /// Sends the discovery request to guessed gateway.
        /// </summary>
        /// <param name="socket">The socket to use.</param>
        /// <param name="searchTarget">The search target.</param>
        /// <returns>Whether the discovery requests are sent successfully.</returns>
        private bool SendDiscoveryRequestToGuessedGateway(Socket socket, string searchTarget)
        {
            bool guessedGatewayUsed = false;
            List<IPAddress> guessedAddresses = new List<IPAddress>();
            //// many routers has such kind of gateway
            guessedAddresses.Add(this.GetGuessedGatewayAddress(1));
            //// e.g. billion 7401 router
            guessedAddresses.Add(this.GetGuessedGatewayAddress(254));

            foreach (IPAddress address in guessedAddresses)
            {
                if (address != null)
                {
                    IPEndPoint endpoint = new IPEndPoint(address, SSDPDefaultPort);
                    byte[] requestBuffer = this.GetRequest(endpoint, searchTarget, ReceiveTimeoutSecoonds);
                    int sent = socket.SendTo(requestBuffer, endpoint);
                    if (sent == requestBuffer.Length)
                    {
                        guessedGatewayUsed = true;
                    }
                }
            }

            return guessedGatewayUsed;
        }

        /// <summary>
        /// Gets a guessed gateway address.
        /// </summary>
        /// <param name="x">The last byte of the address.</param>
        /// <returns>The guessed gateway address.</returns>
        private IPAddress GetGuessedGatewayAddress(byte x)
        {
            byte[] addressBytes = this.localAddress.GetAddressBytes();
            if (addressBytes != null && addressBytes.Length == 4)
            {
                if (addressBytes[3] != x)
                {
                    addressBytes[3] = x;
                    IPAddress address = new IPAddress(addressBytes);
                    return address;
                }
            }

            // address length is more than 4 bytes (ipv6?). ignore it. 
            return null;
        }

        /// <summary>
        /// Gets the discovery request.
        /// </summary>
        /// <param name="endpoint">The endpoint.</param>
        /// <param name="searchTarget">The search target.</param>
        /// <param name="responseDelaySeconds">The response delay seconds.</param>
        /// <returns>The request buffer.</returns>
        private byte[] GetRequest(IPEndPoint endpoint, string searchTarget, int responseDelaySeconds)
        {
            // Construst the discovery request message
            using (MemoryStream requestStream = new MemoryStream())
            {
                StreamWriter writer = new StreamWriter(requestStream);

                writer.Write("M-SEARCH * HTTP/1.1\r\n");
                writer.Write(String.Format("Host:{0}\r\n", endpoint.ToString()));
                writer.Write(String.Format("ST:{0}\r\n", searchTarget));
                writer.Write("Man:\"ssdp:discover\"\r\n");
                writer.Write(String.Format("MX:{0}\r\n\r\n", responseDelaySeconds));
                writer.Flush();

                return requestStream.ToArray();
            }
        }

        /// <summary>
        /// Gets the SSDP response.
        /// </summary>
        /// <param name="socket">The socket.</param>
        /// <param name="searchTarget">The search target.</param>
        /// <returns>The SSDP response or null on error.</returns>
        private SsdpResponse GetResponse(Socket socket, string searchTarget)
        {
            byte[] responseBuffer = new byte[10240];

            try
            {
                System.Net.Sockets.SocketError error;
                int length = socket.Receive(responseBuffer, 0, responseBuffer.Length, System.Net.Sockets.SocketFlags.None, out error);
                if (length == 0 && error == System.Net.Sockets.SocketError.TimedOut)
                {
                    return null;
                }
            }
            catch (System.Net.Sockets.SocketException)
            {
                return null;
            }

            using (TextReader reader = new StringReader(Encoding.UTF8.GetString(responseBuffer)))
            {
                // Check that the search target on the response matches the query.
                SsdpResponse response = new SsdpResponse(reader);
                string responseSearchTarget = response.GetSearchTarget();
                if (!string.IsNullOrEmpty(responseSearchTarget) &&
                    string.Compare(responseSearchTarget, searchTarget) == 0)
                {
                    return response;
                }
            }

            return null;
        }

        /// <summary>
        /// Creates the socket.
        /// </summary>
        /// <param name="broadcast">enable broadcast if set to <c>true</c>.</param>
        /// <returns>The required socket.</returns>
        private Socket CreateSocket(bool broadcast)
        {
            Socket socket = Socket.Create(System.Net.Sockets.AddressFamily.InterNetwork, System.Net.Sockets.SocketType.Dgram, System.Net.Sockets.ProtocolType.Udp);

            if (broadcast)
            {
                socket.EnableBroadcast = true;
            }

            return socket;
        }
    }
}