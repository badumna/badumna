﻿//---------------------------------------------------------------------------------
// <copyright file="SsdpResponse.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.IO;

using Badumna.Utilities;

namespace Badumna.Transport.Upnp
{
    /// <summary>
    /// The SSDP response used by the UPnP device discovery. 
    /// </summary>
    internal class SsdpResponse
    {
        /// <summary>
        /// All fields contained in the response.
        /// </summary>
        public readonly Dictionary<string, string> Fields = new Dictionary<string, string>();

        /// <summary>
        /// Initializes a new instance of the <see cref="SsdpResponse"/> class.
        /// </summary>
        /// <param name="response">The response.</param>
        public SsdpResponse(TextReader response)
        {
            try
            {
                string line = response.ReadLine();

                if (!line.Contains("OK"))
                {
                    return;
                }

                while ((line = response.ReadLine()) != null)
                {
                    string[] substrings = line.Split(new char[] { ':' }, 2);
                    if (substrings.Length == 2)
                    {
                        this.Fields.Add(substrings[0].Trim().ToLower(), substrings[1].Trim());
                    }
                }
            }
            catch (Exception)
            {
            }
        }

        /// <summary>
        /// Gets the search target.
        /// </summary>
        /// <returns>The search target string.</returns>
        public string GetSearchTarget()
        {
            string searchTarget = null;
            this.Fields.TryGetValue("st", out searchTarget);

            return searchTarget;
        }

        /// <summary>
        /// Gets the location.
        /// </summary>
        /// <returns>The location string.</returns>
        public string GetLocation()
        {
            string location = null;
            this.Fields.TryGetValue("location", out location);

            return location;
        }
    }
}
