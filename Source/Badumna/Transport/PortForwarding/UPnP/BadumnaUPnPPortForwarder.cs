﻿//---------------------------------------------------------------------------------
// <copyright file="BadumnaUPnPPortForwarder.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

using System;
using System.Net;
using System.Net.Sockets;

namespace Badumna.Transport.Upnp
{
    /// <summary>
    /// The UPnP port forwarder used for configuring port forwarding on UPnP devinces.  
    /// </summary>
    public class BadumnaUPnPPortForwarder : IPortForwarder
    {
        /// <summary>
        /// The upnp port forwarder.
        /// </summary>
        private UpnpPortForwarder portForwarder;

        /// <summary>
        /// Initializes a new instance of the <see cref="BadumnaUPnPPortForwarder"/> class.
        /// </summary>
        public BadumnaUPnPPortForwarder()
        {
            this.portForwarder = new UpnpPortForwarder();
        }

        /// <summary>
        /// Initializes the port forwarder.
        /// </summary>
        /// <param name="localAddress">The local peer address.</param>
        /// <returns>
        /// Whether the initialization succeeded or not.
        /// </returns>
        public bool Initialize(IPAddress localAddress)
        {
            return this.portForwarder.Initialize(localAddress);
        }

        /// <summary>
        /// Gets the external address.
        /// </summary>
        /// <returns>The external address or null on error.</returns>
        public IPAddress GetExternalAddress()
        {
            return this.portForwarder.GetExternalAddress();
        }

        /// <summary>
        /// Tries to map the ports.
        /// </summary>
        /// <param name="localAddress">The local address.</param>
        /// <param name="mappingLease">The mapping lease.</param>
        /// <returns>Port forwarding result object.</returns>
        public PortForwardingResult TryMapPort(IPEndPoint localAddress, TimeSpan mappingLease)
        {
            PortForwardingResult result = new PortForwardingResult();
            result.Succeed = false;

            result = this.portForwarder.TryMapPort(ProtocolType.Udp, localAddress, localAddress.Port, mappingLease);
            if (!result.Succeed)
            {
                if ((uint)mappingLease.TotalSeconds != 0)
                {
                    TimeSpan permanentMappingLease = TimeSpan.FromMilliseconds(0);
                    result = this.portForwarder.TryMapPort(
                        ProtocolType.Udp,
                        localAddress,
                        localAddress.Port,
                        permanentMappingLease);
                }
            }

            return result;
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            this.portForwarder.Dispose();
        }
    }
}