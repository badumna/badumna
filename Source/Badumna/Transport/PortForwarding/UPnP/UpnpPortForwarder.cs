﻿//---------------------------------------------------------------------------------
// <copyright file="UpnpPortForwarder.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

using System;
using System.IO;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;

namespace Badumna.Transport.Upnp
{
    /// <summary>
    /// The UPnP port forwarder is used to configure port forwarding on UPnP devinces. Each port forwarder manages exact
    /// one forwarded port. That means if the application process needs two different ports be mapped to two different
    /// external ports, then two UPnP port forwarders should be created to let them each manage one port mapping. The 
    /// mapped port is created with a lease duration set by the application, it is application code's responsibility to
    /// keep renewing the mapped port before the expiration time. The mapped port will be removed from the UPnP devices
    /// when the port forwarder is disposed. 
    /// </summary>
    public class UpnpPortForwarder : IDisposable
    {
        /// <summary>
        /// Internet gateway device namespace.
        /// </summary>
        private const string InternetGatewayDeviceNamespace = @"urn:schemas-upnp-org:device:InternetGatewayDevice:1";

        /// <summary>
        /// Soap envelope namespace
        /// </summary>
        private const string SoapEnvelopeNamespace = @"http://schemas.xmlsoap.org/soap/envelope/";

        /// <summary>
        /// Connection service.
        /// </summary>
        private UPnPService connectionService;

        /// <summary>
        /// Port mapping details.
        /// </summary>
        private UPnPPortMappingEntry portMappingEntry;

        /// <summary>
        /// The used connection namespace, should be one of WanIPConnectionNamespace and WanPPPConnectionNamespace.
        /// </summary>
        private string connectionNamespace;

        /// <summary>
        /// The random number generator.
        /// </summary>
        private Random random;

        /// <summary>
        /// Whether the forwarder has been disposed.
        /// </summary>
        private bool disposed;

        /// <summary>
        /// Initializes a new instance of the <see cref="UpnpPortForwarder"/> class.
        /// </summary>
        public UpnpPortForwarder()
        {
            this.disposed = false;
            this.random = new Random(Environment.TickCount);
        }

        /// <summary>
        /// Initializes the port forwarder. During the initialization, the port forwarder uses the SSDP protocol to 
        /// locate the UPnP devices.  
        /// </summary>
        /// <param name="localAddress">The local peer address.</param>
        /// <returns>Whether the initialization succeeded or not.</returns>
        public bool Initialize(IPAddress localAddress)
        {
            SimpleServiceDiscovery discoveryProtocol = new SimpleServiceDiscovery(localAddress);
            IPAddress gatewayAddress = this.GetGatewayAddress(localAddress);

            try
            {
                SsdpResponse discoveryResponse = discoveryProtocol.Discover(
                    UpnpPortForwarder.InternetGatewayDeviceNamespace,
                    gatewayAddress);
                if (discoveryResponse == null)
                {
                    return false;
                }

                string internetGatewayDeviceLocation = discoveryResponse.GetLocation();
                if (string.IsNullOrEmpty(internetGatewayDeviceLocation))
                {
                    return false;
                }

                this.connectionService = this.GetWanIpConnectionService(internetGatewayDeviceLocation);
            }
            catch
            { 
            }

            return this.connectionService != null;
        }

        /// <summary>
        /// Performs application-defined tasks to free all allocated resources, removing the mapped port in this case.
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Gets the external address from the router.
        /// </summary>
        /// <exception cref="System.InvalidOperationException">
        /// Thrown when this method is called on an uninitialized port forwarder object.
        /// </exception>
        /// <returns>The external IP address or null on error.</returns>
        public IPAddress GetExternalAddress()
        {
            if (this.connectionService == null)
            {
                throw new InvalidOperationException("UPnP forwarder has not been initialized.");
            }

            UPnPExternalIPAddress response = new UPnPExternalIPAddress();
            using (SimpleWebResponse webResponse =
                this.SendRequest(this.connectionService.ControlURL, new UPnPExternalIPAddress(), "GetExternalIPAddress"))
            {
                this.ReadResponse(
                    webResponse,
                    ref response,
                    typeof(UPnPExternalIPAddress),
                    "GetExternalIPAddressResponse");
            }

            try
            {
                return IPAddress.Parse(response.ExternalIPAddress);
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Tries to setup port mapping for new mapping or renew the existing mapped port.
        /// </summary>
        /// <param name="protocol">The protocol.</param>
        /// <param name="localAddress">The local address.</param>
        /// <param name="externalPort">The external port.</param>
        /// <param name="mappingLease">The lease of the mapping. Note that some routers only support permanent mapping,
        /// which means the mapped port will never expire. The mappingLease must be set to 0 for such routers.</param>
        /// <returns>
        /// A port forwarding result object that contains the details of the port forwarding operation results.
        /// </returns>
        /// <exception cref="System.InvalidOperationException">
        /// Thrown when this method is called on an uninitialized port forwarder object or when trying to reuse the
        /// port forwarder to manage more than one port mapping.
        /// </exception>
        /// <exception cref="System.ArgumentNullException">Thrown when the local address parameter is null.</exception>
        /// <exception cref="System.ArgumentException">Thrown when the protocol type is not TCP or UDP.</exception>
        internal PortForwardingResult TryMapPort(
            ProtocolType protocol, 
            IPEndPoint localAddress, 
            int externalPort,
            TimeSpan mappingLease)
        {
            if (protocol != ProtocolType.Tcp && protocol != ProtocolType.Udp)
            {
                throw new ArgumentException("Only TCP and UDP are supported.");
            }

            if (localAddress == null)
            {
                throw new ArgumentNullException("local address can not be null.");
            }
            
            if (this.connectionService == null || this.connectionNamespace == null)
            {
                throw new InvalidOperationException("UPnP forwarder has not been initialized.");
            }

            if (this.IsReusingForwarder(protocol, localAddress, externalPort))
            {
                throw new InvalidOperationException("UPnP forwarder can not be reused.");
            }

            PortForwardingResult result = new PortForwardingResult();
            result.Succeed = false;
            uint lease = (uint)mappingLease.TotalSeconds;

            UPnPPortMappingEntry portMapping = new UPnPPortMappingEntry();
            UPnPPortMappingEntry portMappingResponse = null;

            portMapping.InternalPort = localAddress.Port;
            portMapping.ExternalPort = externalPort;
            portMapping.Description = this.GetPortMappingDescription();
            portMapping.InternalClient = localAddress.Address.ToString();
            portMapping.Protocol = protocol;
            portMapping.IsEnabled = true;
            portMapping.RemoteHost = string.Empty;
            portMapping.LeaseDuration = lease;

            // the external port has already been mapped to others, just abort. 
            if (!this.IsExternalPortAvailable(protocol, externalPort, localAddress.Port, portMapping.InternalClient))
            {
                return result;
            }

            using (SimpleWebResponse webResponse =
                    this.SendRequest(this.connectionService.ControlURL, portMapping, "AddPortMapping"))
            {
                if (!this.ReadResponse(webResponse, ref portMappingResponse, null, "AddPortMappingResponse"))
                {
                    return result;
                }
            }

            // record the result
            result.Succeed = true;
            result.InternalPort = localAddress.Port;
            result.ExternalPort = externalPort;
            result.LeaseInSeconds = lease;

            // required when removing the port mapping on shutdown
            this.portMappingEntry = portMapping;

            return result;
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; 
        /// <c>false</c> to release only unmanaged resources.</param>
        protected virtual void Dispose(bool disposing)
        {
            // If you need thread safety, use a lock around these 
            // operations, as well as in your methods that use the resource.
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (this.portMappingEntry != null)
                    {
                        try
                        {
                            this.RemovePortMapping(this.portMappingEntry);
                        }
                        catch 
                        { 
                        }
                    }
                }

                // Indicate that the instance has been disposed.
                this.portMappingEntry = null;
                this.disposed = true;
            }
        }

        /// <summary>
        /// Determines whether the forwarder is being reused to manage another port mapping. 
        /// </summary>
        /// <param name="protocol">The protocol type.</param>
        /// <param name="localAddress">The local address.</param>
        /// <param name="externalPort">The external port.</param>
        /// <returns>
        /// <c>true</c> if is being reused; otherwise, <c>false</c>.
        /// </returns>
        private bool IsReusingForwarder(ProtocolType protocol, IPEndPoint localAddress, int externalPort)
        {
            if (this.portMappingEntry == null)
            {
                // no port mapping has been setup, not reusing.
                return false;
            }

            if (this.portMappingEntry.Protocol == protocol &&
               this.portMappingEntry.InternalPort == localAddress.Port &&
               this.portMappingEntry.ExternalPort == externalPort)
            {
                // trying to renew the mapping, not reusing. 
                return false;
            }

            // resuing.
            return true;
        }

        /// <summary>
        /// Removes the specified port mapping.
        /// </summary>
        /// <param name="portMapping">The port mapping.</param>
        /// <returns>Whether the port mapping is successfully removed.</returns>
        private bool RemovePortMapping(UPnPPortMappingEntry portMapping)
        {
            return this.RemovePortMapping(portMapping.Protocol, portMapping.ExternalPort, portMapping.RemoteHost);
        }

        /// <summary>
        /// Removes the port mapping.
        /// </summary>
        /// <exception cref="System.InvalidOperationException">
        /// Thrown when this method is called on an uninitialized port forwarder object.
        /// </exception>
        /// <exception cref="System.ArgumentException">Thrown when the protocol type is not TCP or UDP.</exception>
        /// <param name="protocol">The protocol.</param>
        /// <param name="externalPort">The external port.</param>
        /// <param name="remoteHost">The remote host.</param>
        /// <returns>Whether the port mapping is successfully removed.</returns>
        private bool RemovePortMapping(ProtocolType protocol, int externalPort, string remoteHost)
        {
            UPnPPortMappingId id = new UPnPPortMappingId();

            if (this.connectionService == null)
            {
                throw new InvalidOperationException("UPnP forwarder has not been initialized.");
            }

            if (protocol == ProtocolType.Udp)
            {
                id.Protocol = "UDP";
            }
            else if (protocol == ProtocolType.Tcp)
            {
                id.Protocol = "TCP";
            }
            else
            {
                throw new ArgumentException("Only TCP and UDP are supported.");
            }

            id.ExternalPort = externalPort;
            id.RemoteHost = remoteHost;

            UPnPPortMappingEntry portMappingResponse = null;
            using (SimpleWebResponse webResponse =
                this.SendRequest(this.connectionService.ControlURL, id, "DeletePortMapping"))
            {
                return this.ReadResponse(webResponse, ref portMappingResponse, null, "DeletePortMappingResponse");
            }
        }

        /// <summary>
        /// Gets the port mapping description.
        /// </summary>
        /// <returns>The port mapping description used for identifying the current port mapping entry</returns>
        private string GetPortMappingDescription()
        {
            if (this.portMappingEntry == null)
            {
                return String.Format("Badumna|{0}", this.random.Next());
            }
            else
            {
                return this.portMappingEntry.Description;
            }
        }

        /// <summary>
        /// Determines whether the external port available for mapping.
        /// </summary>
        /// <param name="protocol">The protocol.</param>
        /// <param name="externalPort">The external port.</param>
        /// <param name="internalPort">The internal port.</param>
        /// <param name="internalClient">The internal client.</param>
        /// <returns>
        /// <c>true</c> if the external port is available; otherwise, <c>false</c>.
        /// </returns>
        private bool IsExternalPortAvailable(
            ProtocolType protocol, 
            int externalPort, 
            int internalPort,
            string internalClient)
        {
            if (this.connectionService == null)
            {
                throw new InvalidOperationException("UPnP forwarder has not been initialized.");
            }

            if (protocol != ProtocolType.Tcp && protocol != ProtocolType.Udp)
            {
                throw new ArgumentException("Only TCP and UDP are supported.");
            }

            string mappingDescription = this.GetPortMappingDescription();

            try
            {
                // Find the first port mapping that is free.
                UPnPPortMappingEntry existingPortMapping = this.GetSpecificPortMappingEntry(
                        protocol,
                        this.connectionService.ControlURL,
                        internalPort,
                        externalPort);

                if (existingPortMapping == null)
                {
                    // external port available, ok to create new mapping.
                    return true;
                }
                else if (existingPortMapping.Description == mappingDescription &&
                         existingPortMapping.InternalClient == internalClient)
                {
                    // port mapping exists but it is owned by the current process, ok to renew it.
                    return true;
                }
                else
                {
                    // the specified external port is mapped by others
                    return false;
                }
            }
            catch
            {
                // exception caught when invoking GetSpecificPortMappingEntry, usually caused by http 404/500 errors.
                return false;
            }
        }

        /// <summary>
        /// Gets the gateway address.
        /// </summary>
        /// <param name="address">The local peer address.</param>
        /// <returns>The gateway for the local peer address or null on error.</returns>
        private IPAddress GetGatewayAddress(IPAddress address)
        {
            IPAddress gatewayAddress = null;
            int matchLength = -1;

            try
            {
                NetworkInterface[] interfaces = NetworkInterface.GetAllNetworkInterfaces();
                foreach (NetworkInterface networkInterface in interfaces)
                {
                    if (networkInterface.NetworkInterfaceType != NetworkInterfaceType.Loopback &&
                        networkInterface.OperationalStatus == OperationalStatus.Up)
                    {
                        foreach (UnicastIPAddressInformation addressInformation in
                            networkInterface.GetIPProperties().UnicastAddresses)
                        {
                            if (addressInformation.Address.Equals(address))
                            {
                                foreach (GatewayIPAddressInformation gi in
                                    networkInterface.GetIPProperties().GatewayAddresses)
                                {
                                    if (null != gi && null != gi.Address)
                                    {
                                        int match = this.GetMatchingPrefixLength(address, gi.Address);
                                        if (match > matchLength)
                                        {
                                            matchLength = match;
                                            gatewayAddress = gi.Address;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch 
            { 
            }

            return gatewayAddress;
        }

        /// <summary>
        /// Gets the length of the matching prefix.
        /// </summary>
        /// <param name="address">The address.</param>
        /// <param name="gatewayAddress">The gateway address.</param>
        /// <returns>The matching prefix length</returns>
        private int GetMatchingPrefixLength(IPAddress address, IPAddress gatewayAddress)
        {
            byte[] addressBytes = address.GetAddressBytes();
            byte[] gatewayAddressBytes = gatewayAddress.GetAddressBytes();

            if (addressBytes == null || addressBytes.Length < 4 || gatewayAddressBytes == null ||
                gatewayAddressBytes.Length < 4)
            {
                return 0;
            }

            for (int i = 0; i < 4; i++)
            {
                if (addressBytes[i] != gatewayAddressBytes[i])
                {
                    return i;
                }
            }

            return 4;
        }

        /// <summary>
        /// Gets the wan ip connection service.
        /// </summary>
        /// <param name="internetGatewayDeviceUrl">The internet gateway device URL.</param>
        /// <returns>The WAN IP connection service or null on error.</returns>
        private UPnPService GetWanIpConnectionService(string internetGatewayDeviceUrl)
        {
            SimpleHttpWebRequest request = this.PrepareGetRequest(internetGatewayDeviceUrl);

            try
            {
                using (SimpleWebResponse response = request.GetResponse())
                {
                    if (response == null)
                    {
                        return null;
                    }

                    try
                    {
                        int responseLength = (int)response.GetResponseStream().Length;
                        if (responseLength == 0)
                        {
                            return null;
                        }

                        byte[] responseByteArray = new byte[responseLength];
                        response.GetResponseStream().Read(responseByteArray, 0, responseLength);
                        string content = System.Text.ASCIIEncoding.ASCII.GetString(responseByteArray);

                        IGDDescParser parser = new IGDDescParser();
                        parser.Parse(content);
                       
                        UPnPService wanIpConnectionService = null;
                        if (parser.Result.First != null && parser.Result.First.ControlURL != null &&
                            parser.Result.First.ControlURL != string.Empty)
                        {
                            this.connectionNamespace = parser.Result.First.ServiceType;
                            wanIpConnectionService = new UPnPService();
                            wanIpConnectionService.CopyFrom(parser.Result.First);
                        }
                        else if (parser.Result.Second != null && parser.Result.Second.ControlURL != string.Empty)
                        {
                            this.connectionNamespace = parser.Result.Second.ServiceType;
                            wanIpConnectionService = new UPnPService();
                            wanIpConnectionService.CopyFrom(parser.Result.Second);
                        }

                        if (wanIpConnectionService != null)
                        {
                            bool hasURLBase = false;

                            if (!string.IsNullOrEmpty(parser.Result.URLBase))
                            {
                                try
                                {
                                    new System.Uri(parser.Result.URLBase);
                                    hasURLBase = true;
                                }
                                catch (UriFormatException)
                                {
                                    hasURLBase = false;
                                }
                            }

                            if (hasURLBase)
                            {
                                // get the full url.
                                wanIpConnectionService.ControlURL = parser.Result.URLBase.TrimEnd('/') + '/' +
                                    wanIpConnectionService.ControlURL.TrimStart('/');
                                wanIpConnectionService.SCPDURL = parser.Result.URLBase.TrimEnd('/') + '/' +
                                    wanIpConnectionService.SCPDURL.TrimStart('/');
                            }
                            else
                            {
                                // assuming the url base will be http://gateway_ip:gateway_port/
                                System.Uri uri = new System.Uri(internetGatewayDeviceUrl);
                                string urlBase = uri.Scheme + "://" +
                                    uri.GetComponents(UriComponents.HostAndPort, UriFormat.UriEscaped);
                                wanIpConnectionService.ControlURL = urlBase.TrimEnd('/') + '/' +
                                    wanIpConnectionService.ControlURL.TrimStart('/');
                                wanIpConnectionService.SCPDURL = urlBase.TrimEnd('/') + '/' +
                                    wanIpConnectionService.SCPDURL.TrimStart('/');
                            }

                            return wanIpConnectionService;
                        }
                    }
                    catch (Exception)
                    {
                    }
                }
            }
            catch (WebException)
            {
                // usually due to a 404 error. buggy router says the xml is there at that url, but actually not. 
            }

            return null;
        }

        /// <summary>
        /// Gets the specific port mapping entry.
        /// </summary>
        /// <param name="protocol">The protocol.</param>
        /// <param name="controlUrl">The control URL.</param>
        /// <param name="internalPort">The internal port.</param>
        /// <param name="externalPort">The external port.</param>
        /// <returns>The specified port mapping entry object.</returns>
        private UPnPPortMappingEntry GetSpecificPortMappingEntry(
            ProtocolType protocol,
            string controlUrl,
            int internalPort,
            int externalPort)
        {
            UPnPPortMappingEntry portMappingResponse = new UPnPPortMappingEntry();
            UPnPPortMappingEntry portMappingRequest = new UPnPPortMappingEntry();

            portMappingRequest.InternalPort = internalPort;
            portMappingRequest.ExternalPort = externalPort;
            portMappingRequest.Protocol = protocol;

            // Request current port mappings for UDP
            using (SimpleWebResponse webResponse =
                this.SendRequest(controlUrl, portMappingRequest, "GetSpecificPortMappingEntry"))
            {
                bool success = this.ReadResponse(
                    webResponse,
                    ref portMappingResponse,
                    typeof(UPnPPortMappingEntry),
                    "GetSpecificPortMappingEntryResponse");

                if (success && portMappingResponse != null && portMappingResponse.IsEnabled)
                {
                    return portMappingResponse;
                }
            }

            return null;
        }

        /// <summary>
        /// Sends the request to the specified url.
        /// </summary>
        /// <typeparam name="T">Type of the request.</typeparam>
        /// <param name="url">The URL for the request.</param>
        /// <param name="request">The request.</param>
        /// <param name="requestName">Name of the request.</param>
        /// <returns>A web response object.</returns>
        private SimpleWebResponse SendRequest<T>(string url, T request, string requestName)
            where T : IXMLSerializable
        {
            System.Text.ASCIIEncoding encoding = new System.Text.ASCIIEncoding();
            string content = request.Serialize(this.connectionNamespace, requestName);
            byte[] bytes = encoding.GetBytes(content);

            SimpleHttpWebRequest webRequest = null;
            webRequest = this.PreparePostRequest(url, requestName);
            webRequest.ContentLength = bytes.Length;
            webRequest.GetRequestStream().Write(bytes, 0, bytes.Length);

            try
            {
                return webRequest.GetResponse();
            }
            catch (WebException)
            {
                // usually caused by HTTP 404/500 error.
                return null;
            }
        }

        /// <summary>
        /// Reads the response.
        /// </summary>
        /// <typeparam name="T">Type of the response.</typeparam>
        /// <param name="webResponse">The web response.</param>
        /// <param name="response">The response.</param>
        /// <param name="type">The response type.</param>
        /// <param name="responseName">Name of the response.</param>
        /// <returns>Whether request succeed or not.</returns>
        private bool ReadResponse<T>(SimpleWebResponse webResponse, ref T response, Type type, string responseName)
        {
            if (webResponse == null)
            {
                return false;
            }

            try
            {
                Stream s = webResponse.GetResponseStream();
                byte[] responseArray = new byte[s.Length];
                s.Read(responseArray, 0, (int)s.Length);
                string content = System.Text.ASCIIEncoding.ASCII.GetString(responseArray);

                if (type == null)
                {
                    if (content.Contains("<s:Fault>"))
                    {
                        // the response is an error message 
                        return false;
                    }
                }
                else
                {
                    response = (T)Activator.CreateInstance(type, content);
                }

                return true;
            }
            catch 
            { 
            }

            return false;
        }

        /// <summary>
        /// Prepares the HTTP POST request.
        /// </summary>
        /// <param name="url">The URL of the request.</param>
        /// <param name="soapAction">The SOAP action.</param>
        /// <returns>The http request.</returns>
        private SimpleHttpWebRequest PreparePostRequest(string url, string soapAction)
        {
            SimpleHttpWebRequest request = SimpleHttpWebRequest.Create(url);

            request.Method = "POST";
            request.ContentType = "text/xml";

            request.UserAgent = "Badumna";
            request.KeepAlive = false;

            // timeout in milliseconds. 
            request.Timeout = 3000;

            if (!string.IsNullOrEmpty(soapAction))
            {
                request.Headers.Add(@"SOAPAction", String.Format("\"{0}#{1}\"", this.connectionNamespace, soapAction));
            }

            return request;
        }

        /// <summary>
        /// Prepares the HTTP GET request.
        /// </summary>
        /// <param name="url">The URL of the request.</param>
        /// <returns>The http request.</returns>
        private SimpleHttpWebRequest PrepareGetRequest(string url)
        {
            SimpleHttpWebRequest request = SimpleHttpWebRequest.Create(url);

            request.Method = "GET";
            request.UserAgent = "Badumna";
            request.KeepAlive = false;
            request.Timeout = 3000;

            return request;
        }
    }
}
