﻿//---------------------------------------------------------------------------------
// <copyright file="SimpleXMLException.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

using System;

namespace Badumna.Transport.Upnp
{
    /// <summary>
    /// Simple XML Exception.
    /// </summary>
    internal class SimpleXMLException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SimpleXMLException"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        public SimpleXMLException(string message)
            : base(message)
        {
        }
    }
}