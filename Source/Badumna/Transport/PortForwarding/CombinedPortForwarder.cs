﻿//---------------------------------------------------------------------------------
// <copyright file="CombinedPortForwarder.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

using System;
using System.Net;

namespace Badumna.Transport
{
    /// <summary>
    /// Type of port forwarder.
    /// </summary>
    internal enum PortForwarderType
    {
        /// <summary>
        /// UPnP port forwarder.
        /// </summary>
        UPnP = 1,

        /// <summary>
        /// NAT_PMP port forwarder.
        /// </summary>
        NAT_PMP = 2
    }

    /// <summary>
    /// A wrapper class for both the UPnP and NATPMP forwarder. This wrapper class decides whether UPnP or NATPMP will 
    /// be employed to manage the port mapping. The policy is to always try UPnP first, NATPMP will be used if there is 
    /// no UPnP internet gateway device can be identified during UPnP's dicovery process. 
    /// </summary>
    internal class CombinedPortForwarder : IPortForwarder
    {
        /// <summary>
        /// The UPnP forwarder.
        /// </summary>
        private IPortForwarder upnpForwarder;

        /// <summary>
        /// The NAT-PMP forwarder.
        /// </summary>
        private IPortForwarder natpmpForwarder;

        /// <summary>
        /// The type of the port forwarder being used.
        /// </summary>
        private PortForwarderType usedType;

        /// <summary>
        /// Initializes a new instance of the <see cref="CombinedPortForwarder"/> class.
        /// </summary>
        public CombinedPortForwarder()
            : this(new Upnp.BadumnaUPnPPortForwarder(), new NAT_PMP.PmpPortForwarder())
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CombinedPortForwarder"/> class.
        /// </summary>
        /// <param name="upnpForwarder">The upnp forwarder.</param>
        /// <param name="natpmpForwarder">The natpmp forwarder.</param>
        public CombinedPortForwarder(IPortForwarder upnpForwarder, IPortForwarder natpmpForwarder)
        {
            this.upnpForwarder = upnpForwarder;
            this.natpmpForwarder = natpmpForwarder;
            this.usedType = PortForwarderType.UPnP;
        }

        /// <summary>
        /// Initializes the port forwarder.
        /// </summary>
        /// <param name="localAddress">The local peer address.</param>
        /// <returns>
        /// Whether the initialization succeeded or not.
        /// </returns>
        public bool Initialize(IPAddress localAddress)
        {
            if (!this.upnpForwarder.Initialize(localAddress))
            {
                if (this.natpmpForwarder.Initialize(localAddress))
                {
                    this.usedType = PortForwarderType.NAT_PMP;
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                this.usedType = PortForwarderType.UPnP;
                return true;
            }
        }

        /// <summary>
        /// Gets the external address.
        /// </summary>
        /// <returns>The external address or null on error.</returns>
        public IPAddress GetExternalAddress()
        {
            if (this.usedType == PortForwarderType.UPnP)
            {
                return this.upnpForwarder.GetExternalAddress();
            }
            else 
            {
                return this.natpmpForwarder.GetExternalAddress();
            }
        }

        /// <summary>
        /// Tries to map the ports.
        /// </summary>
        /// <param name="localAddress">The local address.</param>
        /// <param name="mappingLease">The mapping lease.</param>
        /// <returns>Port forwarding result object.</returns>
        public PortForwardingResult TryMapPort(IPEndPoint localAddress, TimeSpan mappingLease)
        {
            if (this.usedType == PortForwarderType.UPnP)
            {
                return this.upnpForwarder.TryMapPort(localAddress, mappingLease);
            }
            else 
            {
                return this.natpmpForwarder.TryMapPort(localAddress, mappingLease);
            }
        }

        #region IDisposable Members

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            this.upnpForwarder.Dispose();
            this.natpmpForwarder.Dispose();
        }

        #endregion
    }
}