﻿//---------------------------------------------------------------------------------
// <copyright file="PmpPortForwarder.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

using System;
using System.Net;
using System.Net.NetworkInformation;

using Badumna.Core;
using Badumna.Platform;

namespace Badumna.Transport.NAT_PMP
{
    /// <summary>
    /// This class implements Apple's NAT-PMP client protocol. 
    /// The protocol is defined in http://tools.ietf.org/html/draft-cheshire-nat-pmp-03, a quick summary is available at
    /// http://miniupnp.free.fr/nat-pmp.html. Apple airport wireless router is known to support this protocol. The 
    /// Linksys router with tomato firmware also comes with NAT-PMP support. 
    /// </summary>
    internal class PmpPortForwarder : IPortForwarder
    {
        /// <summary>
        /// Receive timeout.
        /// </summary>
        private static readonly int TimeoutInMilliseconds = 1000;

        /// <summary>
        /// Max retry.
        /// </summary>
        private static readonly int MaxTry = 3;

        /// <summary>
        /// The local address.
        /// </summary>
        private IPAddress localAddress;

        /// <summary>
        /// The UDP socket used to talk to the gateway. 
        /// </summary>
        private Socket socket;

        /// <summary>
        /// forwarded port.
        /// </summary>
        private PortForwardingResult forwarededPort;
       
        /// <summary>
        /// Initializes a new instance of the <see cref="PmpPortForwarder"/> class.
        /// </summary>
        public PmpPortForwarder()
        {
            this.socket = this.CreateSocket();
        }

        /// <summary>
        /// Initializes the port forwarder.
        /// </summary>
        /// <param name="localAddress">The local peer address.</param>
        /// <returns>
        /// Whether the initialization succeeded or not.
        /// </returns>
        public bool Initialize(IPAddress localAddress)
        {
            this.localAddress = localAddress;
            
            return true;
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            if (this.forwarededPort != null)
            {
                this.RemoveForwardedPort();
            }

            this.socket.Close();
        }

        /// <summary>
        /// Gets the external address.
        /// </summary>
        /// <returns>The external address or null on error.</returns>
        public IPAddress GetExternalAddress()
        {
            byte[] cmd = this.GetPublicAddressRequestCommand();
            bool success = this.SendRequest(cmd);
            if (!success)
            {
                return null;
            }

            byte[] response = this.GetResponse();
            if (response != null)
            {
                return this.GetPublicAddressFromResponse(response);
            }

            return null;
        }

        /// <summary>
        /// Tries the map port.
        /// </summary>
        /// <param name="localAddress">The local address.</param>
        /// <param name="mappingLeaseDuration">Duration of the mapping lease.</param>
        /// <returns>The result object that contains the result details of the port forwarding operation.</returns>
        public PortForwardingResult TryMapPort(IPEndPoint localAddress, TimeSpan mappingLeaseDuration)
        {
            PortForwardingResult result = new PortForwardingResult();
            result.Succeed = false;

            int portToUse;
            if (this.forwarededPort != null)
            {
                portToUse = this.forwarededPort.ExternalPort;
            }
            else
            {
                portToUse = localAddress.Port;
            }

            for (int i = 0; i < MaxTry; i++)
            {
                byte[] cmd = this.GetPortMappingRequest(localAddress.Port, portToUse, (int)mappingLeaseDuration.TotalSeconds);
                this.SendRequest(cmd);
            }

            for (int i = 0; i < MaxTry; i++)
            {
                byte[] response = this.GetResponse();
                if (this.PortMappingCreated(response, localAddress.Port, portToUse))
                {
                    if (this.forwarededPort == null)
                    {
                        result.ExternalPort = portToUse;
                        result.InternalPort = localAddress.Port;
                        result.LeaseInSeconds = (uint)mappingLeaseDuration.TotalSeconds;
                        result.Succeed = true;
                        this.forwarededPort = result;

                        return result;
                    }
                    else
                    {
                        return this.forwarededPort;
                    }
                 }
            }

            return result;
        }

        /// <summary>
        /// Gets the public address request command.
        /// </summary>
        /// <returns>The command.</returns>
        private byte[] GetPublicAddressRequestCommand()
        {
            byte[] cmd = new byte[2];
            cmd[0] = 0;
            cmd[1] = 0;

            return cmd;
        }

        /// <summary>
        /// Gets the port mapping request.
        /// </summary>
        /// <param name="privatePort">The private port.</param>
        /// <param name="publicPort">The public port.</param>
        /// <param name="lifetime">The lifetime of the mapping.</param>
        /// <returns>The request buffer.</returns>
        private byte[] GetPortMappingRequest(int privatePort, int publicPort, int lifetime)
        {
            using (MessageBuffer message = new MessageBuffer())
            {
                byte version = 0;
                byte opcode = 1;
                short reserved = 0;

                message.Write(version);
                message.Write(opcode);
                message.Write(reserved);
                message.Write(IPAddress.HostToNetworkOrder((short)privatePort));
                message.Write(IPAddress.HostToNetworkOrder((short)publicPort));
                message.Write(IPAddress.HostToNetworkOrder(lifetime));

                return message.GetBuffer();
            }
        }

        /// <summary>
        /// Whether the requested port mapping has been created.
        /// </summary>
        /// <param name="response">The response.</param>
        /// <param name="privatePort">The private port.</param>
        /// <param name="publicPort">The public port.</param>
        /// <returns>
        /// Whether the requested port mapping has been correctly created.
        /// </returns>
        private bool PortMappingCreated(byte[] response, int privatePort, int publicPort)
        {
            if (response == null || response.Length < 16)
            {
                // invalid response
                return false;
            }

            byte opcode;
            short resultCode;
            ushort resultPrivatePort;
            ushort resultPublicPort;

            MessageBuffer message = new MessageBuffer();
            message.Write(response, response.Length);

            message.ReadByte(); // version - Not Used.
            opcode = message.ReadByte();
            resultCode = IPAddress.NetworkToHostOrder(message.ReadShort());
            IPAddress.NetworkToHostOrder(message.ReadInt()); // secondsSinceInitialized - Not used.
            resultPrivatePort = (ushort)IPAddress.NetworkToHostOrder(message.ReadShort());
            resultPublicPort = (ushort)IPAddress.NetworkToHostOrder(message.ReadShort());
            IPAddress.NetworkToHostOrder(message.ReadInt()); // lifetime - Not used.

            if (opcode != 129 || resultCode != 0)
            {
                return false;
            }

            if (resultPrivatePort != privatePort || resultPublicPort != publicPort)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Gets the public address from response.
        /// </summary>
        /// <param name="response">The response.</param>
        /// <returns>The public IP address.</returns>
        private IPAddress GetPublicAddressFromResponse(byte[] response)
        {
            if (response.Length < 12)
            {
                // incomplete response, ignore
                return null;
            }

            byte opcode;
            short resultCode;
            int publicIPAddress;

            MessageBuffer message = new MessageBuffer();
            message.Write(response, response.Length);

            message.ReadByte(); // version - not used.
            opcode = message.ReadByte();
            resultCode = IPAddress.NetworkToHostOrder(message.ReadShort());
            IPAddress.NetworkToHostOrder(message.ReadInt()); // secondsSinceInitialized - Not Used.
            publicIPAddress = IPAddress.NetworkToHostOrder(message.ReadInt());

            if (opcode != 128 || resultCode != 0)
            {
                return null;
            }

            byte[] addressBytes = new byte[4];
            addressBytes[3] = (byte)(publicIPAddress & 0xff);
            addressBytes[2] = (byte)((publicIPAddress & 0xff00) >> 8);
            addressBytes[1] = (byte)((publicIPAddress & 0xff0000) >> 16);
            addressBytes[0] = (byte)((publicIPAddress & 0xff000000) >> 24);

            return new IPAddress(addressBytes);
        }

        /// <summary>
        /// Gets the gateway address.
        /// </summary>
        /// <param name="address">The address.</param>
        /// <returns>The gateway address</returns>
        private IPAddress GetGatewayAddress(IPAddress address)
        {
            IPAddress gatewayAddress = null;

            try
            {
                gatewayAddress = this.TryGetGatewayAddress(address);
            }
            catch
            {
            }

            if (gatewayAddress == null)
            {
                gatewayAddress = this.GetGuessedGatewayAddress(1);
            }

            return gatewayAddress;
        }

        /// <summary>
        /// Gets a guessed gateway address.
        /// </summary>
        /// <param name="x">The last byte of the address.</param>
        /// <returns>The guessed gateway address.</returns>
        private IPAddress GetGuessedGatewayAddress(byte x)
        {
            byte[] addressBytes = this.localAddress.GetAddressBytes();
            if (addressBytes != null && addressBytes.Length == 4)
            {
                if (addressBytes[3] != x)
                {
                    addressBytes[3] = x;
                    IPAddress address = new IPAddress(addressBytes);
                    return address;
                }
            }

            return null;
        }

        // TODO:
        // Methods like TryGetGatewayAddress, GetMatchingPrefixLength are duplicated code, they were quickly copy pasted
        // from other modules during proof-of-concept phase. Need to clean it up and keep these shared code in a better
        // place. 

        /// <summary>
        /// Gets the gateway address.
        /// </summary>
        /// <param name="address">The local peer address.</param>
        /// <returns>The gateway for the local peer address or null on error.</returns>
        private IPAddress TryGetGatewayAddress(IPAddress address)
        {
            IPAddress gatewayAddress = null;
            int matchLength = -1;

            NetworkInterface[] interfaces = NetworkInterface.GetAllNetworkInterfaces();
            foreach (NetworkInterface networkInterface in interfaces)
            {
                if (networkInterface.NetworkInterfaceType != NetworkInterfaceType.Loopback && 
                    networkInterface.OperationalStatus == OperationalStatus.Up)
                {
                    foreach (UnicastIPAddressInformation addressInformation in networkInterface.GetIPProperties().UnicastAddresses)
                    {
                        if (addressInformation.Address.Equals(address))
                        {
                            foreach (GatewayIPAddressInformation gi in networkInterface.GetIPProperties().GatewayAddresses)
                            {
                                if (null != gi && null != gi.Address)
                                {
                                    int match = this.GetMatchingPrefixLength(address, gi.Address);
                                    if (match > matchLength)
                                    {
                                        matchLength = match;
                                        gatewayAddress = gi.Address;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return gatewayAddress;
        }

        /// <summary>
        /// Gets the length of the matching prefix.
        /// </summary>
        /// <param name="address">The address.</param>
        /// <param name="gatewayAddress">The gateway address.</param>
        /// <returns>The matching prefix length</returns>
        private int GetMatchingPrefixLength(IPAddress address, IPAddress gatewayAddress)
        {
            // Debug.Assert(address != null && gatewayAddress != null, "address and gateway address can't be null.");
            byte[] addressBytes = address.GetAddressBytes();
            byte[] gatewayAddressBytes = gatewayAddress.GetAddressBytes();

            if (addressBytes == null || addressBytes.Length < 4 || gatewayAddressBytes == null || gatewayAddressBytes.Length < 4)
            {
                return 0;
            }

            for (int i = 0; i < 4; i++)
            {
                if (addressBytes[i] != gatewayAddressBytes[i])
                {
                    return i;
                }
            }

            return 4;
        }

        /// <summary>
        /// Creates the socket.
        /// </summary>
        /// <returns>The required socket.</returns>
        private Socket CreateSocket()
        {
            return Socket.Create(System.Net.Sockets.AddressFamily.InterNetwork, System.Net.Sockets.SocketType.Dgram, System.Net.Sockets.ProtocolType.Udp);
        }

        /// <summary>
        /// Sends the request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>Whether succeed.</returns>
        private bool SendRequest(byte[] request)
        {
            IPAddress gatewayAddress = this.GetGatewayAddress(this.localAddress);
            if (gatewayAddress != null)
            {
                IPEndPoint endpoint = new IPEndPoint(gatewayAddress, 5351);
                int sent = this.socket.SendTo(request, endpoint);
                if (sent != request.Length)
                {
                    return false;
                }

                return true;
            }

            return false;
        }

        /// <summary>
        /// Gets the response.
        /// </summary>
        /// <returns>The response.</returns>
        private byte[] GetResponse()
        {
            try
            {
                System.Net.Sockets.SocketError error;
                byte[] response = new byte[16];

                // timeout is set to 1 second, should be enough for LAN. 
                this.socket.ReceiveTimeout = TimeoutInMilliseconds;
                int responseLength = this.socket.Receive(response, 0, response.Length, System.Net.Sockets.SocketFlags.None, out error);
                if (responseLength > 0)
                {
                    return response;
                }

                return null;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Removes the forwarded port.
        /// </summary>
        private void RemoveForwardedPort()
        {
            if (this.forwarededPort != null)
            {
                IPEndPoint endpoint = new IPEndPoint(this.localAddress, this.forwarededPort.InternalPort);
                this.TryMapPort(endpoint, TimeSpan.FromSeconds(0));
            }
        }
    }
}
