/***************************************************************************
 *  File Name: StunClient.cs
 *
 *  Copyright (C) 2007 All Rights Reserved.
 * 
 *  Written by
 *      Scott Douglas <scdougl@csse.unimelb.edu.au>
 ****************************************************************************/

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;

using Badumna.Core;
using Badumna.Platform;
using Badumna.Utilities;

namespace Badumna.Transport
{
    // TODO : Refactor this class - it is UGLY!!
    class StunClient
    {
        private class SocketWrapper : ISocketWrapper
        {
            private Socket mSocket;

            public void SendTo(byte[] message, IPEndPoint destination)
            {
                this.mSocket.SendTo(message, destination);
            }

            public int ReceiveFrom(byte[] buffer, ref EndPoint endpoint, int timeoutMilliseconds)
            {
                this.mSocket.ReceiveTimeout = timeoutMilliseconds;
                return this.mSocket.ReceiveFrom(buffer, ref endpoint);
            }

            public SocketWrapper(Socket socket)
            {
                this.mSocket = socket;
            }
        }

        private List<PeerAddress> mPrivateAddresses;

        private PeerAddress mPublicAddress = new PeerAddress();
        public PeerAddress PublicAddress { get { return this.mPublicAddress; } }
        public NatType NatType { get { return this.mPublicAddress.NatType; } }

        private const Int16 mBindAddressRequest = 0x0001;
        private const Int16 mBindAddressResponse = 0x0101;

        private const Int16 mMappedAddress = 0x0001;
        private const Int16 mResponseAddress = 0x0002;
        private const Int16 mChangeRequest = 0x0003;
        private const Int16 mSourceAddress = 0x0004;
        private const Int16 mChangedAddress = 0x0005;
        private const Int16 mUsername = 0x0006;
        private const Int16 mPassword = 0x0007;
        private const Int16 mMessageIntegrity = 0x0008;
        private const Int16 mErrorCode = 0x0009;
        private const Int16 mUnknownAttributes = 0x000a;
        private const Int16 mReflectedFrom = 0x000b;

        private const Int32 mNoRequest = 0x0000000;
        private const Int32 mChangeAddress = 0x0000005; // For some reason some servers respond to 3rd bit and some respond to the 1st - so I use both.
        private const Int32 mChangePort = 0x0000002;

        private byte[] mIdentifier = new byte[16];

        private PeerAddress mMappedEndPointA = null;
        private PeerAddress mMappedEndPointB = null;

        private PeerAddress mFirstServerEndPoint = PeerAddress.Nowhere;
        private PeerAddress mSecondServerEndPoint = PeerAddress.Nowhere;

        private String mFailureReason = "No reason";
        public String FailureReason { get { return this.mFailureReason; } }

        private ISocketWrapper mSocket;

        private int mExpectedExternalPort = -1; // The external port that we believe has been mapped to our internal socket.

        public StunClient(ISocketWrapper socketWrapper, List<PeerAddress> localAddresses, int expectedExternalPort)
        {
            this.mSocket = socketWrapper;
            this.mPrivateAddresses = localAddresses;
            this.mExpectedExternalPort = expectedExternalPort;
        }

        public StunClient(Socket socket, List<PeerAddress> localAddresses, int expectedExternalPort)
        {
            this.mSocket = new SocketWrapper(socket);
            this.mPrivateAddresses = localAddresses;
            this.mExpectedExternalPort = expectedExternalPort;
        }

        public void InitiateRequest(IPEndPoint serverEndPoint)
        {
            try
            {
                this.mMappedEndPointA = null;
                this.mMappedEndPointB = null; ;
                this.mFirstServerEndPoint = new PeerAddress(serverEndPoint, NatType.Unknown);

                this.mFailureReason = "No reason";
                this.EchoAddressRequest();
            }
            catch (System.Net.Sockets.SocketException se)
            {
                this.mFailureReason = "Socket exception : " + se.Message;
                Logger.TraceException(LogLevel.Warning, se, "StunClient InitiateRequest failed");
            }
        }

        private bool EchoAddressRequest()
        {
            this.SendRequest(this.mFirstServerEndPoint.EndPoint, StunClient.mNoRequest, Parameters.STUNNumberOfAttempts, -1);
            bool received = this.ReceiveResponse(Parameters.STUNRequestTimeoutMilliseonds);

            if (!received)
            {
                this.mPublicAddress = new PeerAddress(IPAddress.Any, 0, NatType.Blocked);
                this.mFailureReason = "UDP is blocked.";
                return false;
            }

            if (null == this.mMappedEndPointA)
            {
                this.mFailureReason = "Failed to initiate stun request.";
                return false;
            }

            // After a port has been mapped the outbound packet may still not get its source port re-written by the NAT device
            // so we need to check that the external port that we have mapped provides a reachable route to this peer. We 
            // do this by sending another STUN query, this time asking the server to reply using the external port. If we 
            // get a response then all subsequent STUN queries will ask to use this port.
            // We only need to do this if the mapped external port is differs from the last STUN query.
            if (this.mExpectedExternalPort > 0 && this.mMappedEndPointA.Port != this.mExpectedExternalPort)
            {
                this.SendRequest(this.mFirstServerEndPoint.EndPoint, StunClient.mNoRequest, Parameters.STUNNumberOfAttempts, this.mExpectedExternalPort);
                received = this.ReceiveResponse(Parameters.STUNRequestTimeoutMilliseonds);
                if (!received)
                {
                    // The port is not working - so don't use it in subsequent requests.
                    this.mExpectedExternalPort = -1;
                    Logger.TraceInformation(LogTag.Transport, "Expected external port is not active.");
                }
                else
                {
                    Logger.TraceInformation(LogTag.Transport, "Expected external port is active.");
                }
            }
            else
            {
                this.mExpectedExternalPort = -1;
                Logger.TraceInformation(LogTag.Transport, "Expected external is unknown or the same as the mapped external port.");
            }

            if (this.mPrivateAddresses.Contains(this.mMappedEndPointA))
            {
                return this.ChangeAddressRequest1();
            }

            return ChangeAddressRequest2();
        }

        private bool ChangeAddressRequest1()
        {
            this.SendRequest(this.mFirstServerEndPoint.EndPoint, StunClient.mChangeAddress | StunClient.mChangePort, Parameters.STUNNumberOfAttempts, this.mExpectedExternalPort);
            bool received = this.ReceiveResponse(Parameters.STUNRequestTimeoutMilliseonds);

            if (!received)
            {
                this.mPublicAddress = new PeerAddress(this.mMappedEndPointA.EndPoint, NatType.SymmetricFirewall);
                return false;
            }

            if (this.mPrivateAddresses.Contains(this.mMappedEndPointB))
            {
                this.mPublicAddress = new PeerAddress(this.mMappedEndPointA.EndPoint, NatType.Open);
                return true;
            }

            return false;
        }

        private bool ChangeAddressRequest2()
        {
            this.SendRequest(this.mFirstServerEndPoint.EndPoint, StunClient.mChangeAddress | StunClient.mChangePort, Parameters.STUNNumberOfAttempts, this.mExpectedExternalPort);
            bool received = this.ReceiveResponse(Parameters.STUNRequestTimeoutMilliseonds);

            if (!received)
            {
                return this.EchoAddressRequest2();
            }

            // Try to detect port weirdness!
            this.mMappedEndPointB = null;

            this.SendRequest(this.mSecondServerEndPoint.EndPoint, StunClient.mNoRequest, Parameters.STUNNumberOfAttempts, this.mExpectedExternalPort);
            received = this.ReceiveResponse(Parameters.STUNRequestTimeoutMilliseonds);

            // Check if the ports are preserved
            if (!received || !this.mMappedEndPointA.Equals(this.mMappedEndPointB))
            {
                if (this.mExpectedExternalPort > 0)
                {
                    this.mPublicAddress = new PeerAddress(this.mMappedEndPointA.Address, this.mExpectedExternalPort, NatType.MangledFullCone);
                }
                else
                {
                    this.mPublicAddress = new PeerAddress(this.mMappedEndPointA.EndPoint, NatType.MangledFullCone);
                }

                Logger.TraceInformation(LogTag.Transport, "NAT ports are NOT preserved.");
                return true;
            }
            this.mPublicAddress = new PeerAddress(this.mMappedEndPointA.EndPoint, NatType.FullCone);

            return true;
        }

        private bool EchoAddressRequest2()
        {
            this.mMappedEndPointB = null;

            this.SendRequest(this.mSecondServerEndPoint.EndPoint, StunClient.mNoRequest, Parameters.STUNNumberOfAttempts, this.mExpectedExternalPort);
            bool received = this.ReceiveResponse(Parameters.STUNRequestTimeoutMilliseonds);

            if (!received || null == this.mMappedEndPointB)
            {
                this.mFailureReason = "Failed to recieve echo request 2.";
                return false;
            }

            if (!this.mMappedEndPointA.Equals(this.mMappedEndPointB))
            {
                this.mPublicAddress = new PeerAddress(this.mMappedEndPointA.EndPoint, NatType.SymmetricNat);
                return false;
            }

            return ChangePortRequest();
        }

        private bool ChangePortRequest()
        {
            this.SendRequest(this.mFirstServerEndPoint.EndPoint, StunClient.mChangePort, Parameters.STUNNumberOfAttempts, this.mExpectedExternalPort);
            bool received = this.ReceiveResponse(Parameters.STUNRequestTimeoutMilliseonds);

            if (!received)
            {
                this.mPublicAddress = new PeerAddress(this.mMappedEndPointA.EndPoint, NatType.RestrictedPort);
                return true;
            }

            this.mPublicAddress = new PeerAddress(this.mMappedEndPointA.EndPoint, NatType.RestrictedCone);

            return true;
        }

        private void GenerateRandomId()
        {
            RandomSource.Generator.NextBytes(this.mIdentifier);
        }

        private void SendRequest(IPEndPoint destination, Int32 request, int numberOfAttempts, int replyPort)
        {
            Logger.TraceInformation(LogTag.Transport, "Sending STUN request to {0}", destination);
            MessageBuffer message = new MessageBuffer();

            this.GenerateRandomId();
            this.WriteTypeToMessage(message, StunClient.mBindAddressRequest);
            this.WriteLengthToMessage(message, 0x0000);
            this.WriteIDToMessage(message, this.mIdentifier);

            if (request != StunClient.mNoRequest)
            {
                this.WriteRequestToMessage(message, request);
            }

            if (this.mMappedEndPointA != null && replyPort > 0)
            {
                this.WriteResponseAddress(message, new IPEndPoint(this.mMappedEndPointA.Address, replyPort));
            }

            try
            {
                for (int i = 0; i < numberOfAttempts; i++)
                {
                    this.mSocket.SendTo(message.GetBuffer(), destination);
                }
            }
            catch (System.Net.Sockets.SocketException se)
            {
                this.mFailureReason = "Failed to send : " + se.Message;
                throw;
            }
        }

        private bool ReceiveResponse(int milliseconds)
        {
            if (milliseconds <= 0)
            {
                return false;
            }

            EndPoint endPoint = new IPEndPoint(IPAddress.Any, 0);
            MessageBuffer message = new MessageBuffer();

            byte[] response = new byte[1000];
            int len = 0;

            DateTime start = DateTime.Now;
            try
            {
                len = this.mSocket.ReceiveFrom(response, ref endPoint, milliseconds);
                milliseconds -= (int)(DateTime.Now - start).TotalMilliseconds;
            }
            catch (System.Net.Sockets.SocketException se)
            {
                this.mFailureReason = "Failed to receive : " + se.Message;
                return false;
            }

            if (0 == len)
            {
                this.mFailureReason = "Receive timedout.";
                this.mPublicAddress = new PeerAddress(IPAddress.Any, 0, NatType.Blocked);
                return false;
            }

            message.Write(response, len);
            if (!this.HandleReply(message))
            {
                // The packet was not a valid STUN reply - keep listening.
                return this.ReceiveResponse(milliseconds);
            }

            return true;
        }

        private void WriteTypeToMessage(MessageBuffer message, Int16 value)
        {
            byte[] bytes = BitConverter.GetBytes(value);

            if (BitConverter.IsLittleEndian)
            {
                Array.Reverse(bytes);
            }

            message.WriteOffset = 0;
            message.Write(bytes, bytes.Length);
        }

        private void WriteLengthToMessage(MessageBuffer message, Int16 value)
        {
            byte[] bytes = BitConverter.GetBytes(value);

            if (BitConverter.IsLittleEndian)
            {
                Array.Reverse(bytes);
            }

            message.WriteOffset = 2;
            message.Write(bytes, bytes.Length);
        }

        private void WriteIDToMessage(MessageBuffer message, byte[] id)
        {
            Debug.Assert(id.Length == 16, "Invalid length for STUN identifier.");
            message.WriteOffset = 4;
            message.Write(id, id.Length);
        }


        private void WriteRequestToMessage(MessageBuffer message, Int32 request)
        {
            byte[] type = BitConverter.GetBytes(StunClient.mChangeRequest);
            byte[] value = BitConverter.GetBytes(request);
            byte[] len = BitConverter.GetBytes((Int16)value.Length);

            if (BitConverter.IsLittleEndian)
            {
                Array.Reverse(type);
                Array.Reverse(len);
                Array.Reverse(value);
            }

            message.WriteOffset = 20;
            message.Write(type, type.Length);
            message.Write(len, len.Length);
            message.Write(value, value.Length);

            this.WriteLengthToMessage(message, (Int16)(type.Length + len.Length + value.Length));
        }


        private Int16 ReadInt16FromMessage(MessageBuffer message)
        {
            this.CheckLength(message, 2);
            byte[] bytes = message.ReadBytes(2);

            if (BitConverter.IsLittleEndian)
            {
                Array.Reverse(bytes); // to host order.
            }

            return BitConverter.ToInt16(bytes, 0);
        }

        private UInt16 ReadUInt16FromMessage(MessageBuffer message)
        {
            this.CheckLength(message, 2);
            byte[] bytes = message.ReadBytes(2);

            if (BitConverter.IsLittleEndian)
            {
                Array.Reverse(bytes); // to host order.
            }

            return BitConverter.ToUInt16(bytes, 0);
        }

        private byte[] ReadBytesFromMessage(MessageBuffer message, int length)
        {
            this.CheckLength(message, length);
            byte[] bytes = message.ReadBytes(length);

            if (BitConverter.IsLittleEndian)
            {
                Array.Reverse(bytes); // to host order.
            }

            return bytes;
        }

        public bool HandleReply(MessageBuffer message)
        {
            try
            {
                this.CheckLength(message, 20); // header length
            }
            catch
            {
                return false;
            }

            Int16 responseType = this.ReadInt16FromMessage(message);
            // TODO: Does this unsed integer (response length) need to be serialized/deserialized?
			this.ReadInt16FromMessage(message);
            byte[] responseIdentifier = message.ReadBytes(this.mIdentifier.Length); // Order independant.

            if (responseType == StunClient.mBindAddressResponse)
            {
                // Check identifier
                if (this.IsCorrectIdentifier(responseIdentifier))
                {
                    while (message.ReadOffset < message.Length)
                    {
                        try
                        {
                            this.ReadAttribute(message);
                        }
                        catch (Exception e)
                        {
                            this.mFailureReason = "Invalid reply message : " + e.Message;
                            Logger.TraceError(LogTag.Transport, "Failed to read attribute : {0}", e.Message);
                            throw;
                        }
                    }

                    return true;
                }
            }

            return false;
        }

        private void CheckLength(MessageBuffer message, int length)
        {
            if ((message.Length - message.ReadOffset) < length)
            {
                throw new Exception("Not enough bytes.");
            }
        }

        private bool IsCorrectIdentifier(byte[] identifier)
        {
            if (this.mIdentifier.Length != identifier.Length)
                return false;

            for (int i = 0; i < this.mIdentifier.Length; i++)
                if (this.mIdentifier[i] != identifier[i])
                    return false;

            return true;
        }

        private void ReadAttribute(MessageBuffer message)
        {
            Int16 type = this.ReadInt16FromMessage(message);
            Int16 length = this.ReadInt16FromMessage(message);

            switch (type)
            {
                case StunClient.mMappedAddress:
                    PeerAddress endPoint = this.ReadAddress(message);

                    if (null == this.mMappedEndPointA)
                    {
                        this.mMappedEndPointA = endPoint;
                        Logger.TraceInformation(LogTag.Transport, "Mapped address attribute : " + this.mMappedEndPointA.ToString());
                    }
                    else if (null == this.mMappedEndPointB)
                    {
                        this.mMappedEndPointB = endPoint;
                        Logger.TraceInformation(LogTag.Transport, "Mapped address attribute : " + this.mMappedEndPointB.ToString());
                    }

                    return;

                case StunClient.mChangedAddress:
                    this.mSecondServerEndPoint = this.ReadAddress(message);

                    Logger.TraceInformation(LogTag.Transport, "Try server address : " + this.mSecondServerEndPoint.ToString());
                    return;

                case StunClient.mSourceAddress:
                    IPEndPoint source = this.ReadAddress(message).EndPoint;

                    Logger.TraceInformation(LogTag.Transport, "Source : " + source.ToString());
                    return;


                default:
                    break;
            }

			//  I assume this is for extensibility? - Geoff.
            this.ReadBytesFromMessage(message, length);
        }

        private PeerAddress ReadAddress(MessageBuffer message)
        {
            this.ReadInt16FromMessage(message); // address family (not used).
            UInt16 port = this.ReadUInt16FromMessage(message);
            byte[] address = this.ReadBytesFromMessage(message, 4);

            if (BitConverter.IsLittleEndian)
            {
                Array.Reverse(address);
            }

            return new PeerAddress(new IPAddress(address), (int)port, NatType.Unknown);
        }

        // Writes an attribute to request the reply be sent to a spcecific address.
        private void WriteResponseAddress(MessageBuffer message, IPEndPoint address)
        {
            byte[] familyBytes = BitConverter.GetBytes((UInt16)1);
            byte[] portBytes = BitConverter.GetBytes((UInt16)address.Port);
            byte[] addressBytes = address.Address.GetAddressBytes();
            byte[] type = BitConverter.GetBytes(StunClient.mResponseAddress);
            byte[] len = BitConverter.GetBytes((Int16)(familyBytes.Length + portBytes.Length + addressBytes.Length));

            if (BitConverter.IsLittleEndian)
            {
                Array.Reverse(type);
                Array.Reverse(len);
                Array.Reverse(familyBytes);
                Array.Reverse(portBytes);
                //   address bytes are already in network order
            }

            message.WriteOffset = message.Length;
            message.Write(type, type.Length);
            message.Write(len, len.Length);
            message.Write(familyBytes, familyBytes.Length);
            message.Write(portBytes, portBytes.Length);
            message.Write(addressBytes, addressBytes.Length);

            this.WriteLengthToMessage(message, (Int16)(message.Length - 20));
        }
    }
}
