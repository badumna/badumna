﻿/***************************************************************************
 *  File Name: BroadcastLayer.cs
 *
 *  Copyright (C) 2007 All Rights Reserved.
 * 
 *  Written by
 *      Scott Douglas <sdouglas@nicta.com.au>
 ****************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Net.Sockets;

using Badumna.Core;
using Badumna.Utilities;
using Badumna.Security;

namespace Badumna.Transport
{
    /// <summary>
    /// A transport layer that broadcasts on the local network.
    /// Used for discovery of peers on private network.
    /// </summary>
    class BroadcastLayer : UdpTransport
    {
        private byte[] mSalt;
        private PeerAddress mDestination;


        private GenericCallBackReturn<SymmetricKeyToken> getNetworkParticipationToken;

        // ******************************************************************************************************************

        public BroadcastLayer(ConnectivityModule connectivityOptions, NetworkEventQueue eventQueue, ITime timeKeeper, GenericCallBackReturn<SymmetricKeyToken> getNetworkParticipationToken, INetworkConnectivityReporter connectivityReporter)
            : base(connectivityOptions, false, eventQueue, timeKeeper, connectivityReporter)
        {
            this.getNetworkParticipationToken = getNetworkParticipationToken;
        }

        // ******************************************************************************************************************

        /// <summary>
        /// Encrypt and broadcast the given message
        /// </summary>
        /// <param name="envelope"></param>
        public override void SendMessage(TransportEnvelope envelope)
        {
            if (envelope.Length == 0)
            {
                return; // Firewall keep alive.
            }

            if (this.ConnectivityReporter.Status != ConnectivityStatus.Offline)
            {
                TransportEnvelope encryptedEnvelope = null;
                SymmetricKeyToken token = this.getNetworkParticipationToken();
                if (token != null)
                {
                    encryptedEnvelope = envelope.Encrypt(token.Key, this.mSalt);
                }
                else
                {
                    Logger.TraceWarning(LogTag.Transport, "Failed to find authorization token for JoinNetwork. Dropping outbound connectionless packet!");
                    return;
                }

                if (encryptedEnvelope == null)
                {
                    Logger.TraceWarning(LogTag.Transport, "Failed to encrypt connectionless message for {0}. Dropping.", envelope.Source);
                    return;
                }

                base.SendMessage(encryptedEnvelope);
            }
        }

        // ******************************************************************************************************************

        /// <summary>
        /// Initialize the broadcast layer
        /// </summary>
        /// <param name="port"></param>
        /// <returns></returns>
        protected override bool InitializeSocket(int port)
        {
            this.mDestination = PeerAddress.GetBroadcast(port);
            this.mSalt = new ChecksumSalter(this.ConnectivityOptions.ApplicationName).GetSalt(this.mDestination);
            PeerAddress address = new PeerAddress(IPAddress.Any, port, NatType.Internal);
            this.Socket = this.CreateSocket(address.EndPoint, true);

            if (this.Socket == null)
            {
                Logger.TraceWarning(LogTag.Transport, "Failed to create the broadcast socket.");
            }

            return true;
        }

        /// <inheritdoc />
        protected override void SendPacket(TransportEnvelope envelope)
        {
            // this.socket will be null if there are multiple peers running on the same Mac using Mono 1.2.5. 
            if (this.ConnectivityReporter.Status != ConnectivityStatus.Offline && this.Socket != null)
            {
                byte[] data = envelope.Message.GetBuffer();
                this.Socket.SendTo(data, envelope.Destination.EndPoint);
            }
        }

        /// <summary>
        /// Decrypt the message prior to it being processed.
        /// </summary>
        /// <param name="envelope"></param>
        /// <returns>False if the decryption fails, true otherwise</returns>
        protected override bool PreProcessMessage(TransportEnvelope envelope)
        {
            SymmetricKeyToken token = this.getNetworkParticipationToken();
            if (token != null)
            {
                if (!envelope.Decrypt(token.Key, this.mSalt))
                {
                    Logger.TraceWarning(LogTag.Transport, "Failed to decrypt broadcast message. Dropping packet!");
                    Logger.RecordStatistic("Transport", "UndecryptableBytesDropped", envelope.Length);
                    return false;
                }
            }
            else
            {
                Logger.TraceWarning(LogTag.Transport, "Failed to find authorization token for JoinNetwork. Dropping inbound broadcast packet!");
                Logger.RecordStatistic("Transport", "UndecryptableBytesDropped", envelope.Length);
                return false;
            }

            return base.PreProcessMessage(envelope);
        }
    }
}
