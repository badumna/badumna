//------------------------------------------------------------------------------
// <copyright file="TransportEnvelope.cs" company="National ICT Australia Limited">
//     Copyright (c) 2013 National ICT Australia Limited. All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

namespace Badumna.Transport
{
    using System;
    using Badumna.Core;
    using Badumna.Security;
    using Badumna.Transport.Connections;
    using Badumna.Utilities;

    /// <summary>
    /// An interface for an envelope that is directly sent or received by a transport.
    /// </summary>
    internal interface ITransportEnvelope : IEnvelope
    {
        /// <summary>
        /// Gets the certificate associated with the source of the envelope.
        /// </summary>
        ICertificateToken Certificate { get; }

        /// <summary>
        /// Gets the source address of the envelope.
        /// </summary>
        PeerAddress Source { get; }
    }

    /// <summary>
    /// An envelope that is directly sent or received by a transport.
    /// </summary>
    internal class TransportEnvelope : BaseEnvelope, ITransportEnvelope
    {
        /// <summary>
        /// Initializes a new instance of the TransportEnvelope class.
        /// </summary>
        public TransportEnvelope()
        {
        }

        /// <summary>
        /// Initializes a new instance of the TransportEnvelope class by copying an existing envelope.
        /// </summary>
        /// <param name="copy">The envelope to copy</param>
        public TransportEnvelope(TransportEnvelope copy)
            : base(copy)
        {
            this.Source = copy.Source;
            this.Destination = copy.Destination;
            this.ArrivalTime = copy.ArrivalTime;
            this.SetLabel(copy.Label);
        }

        /// <summary>
        /// Initializes a new instance of the TransportEnvelope class for a message to be sent.
        /// </summary>
        /// <param name="destination">The destination</param>
        /// <param name="qos">The quality of service</param>
        public TransportEnvelope(PeerAddress destination, QualityOfService qos)
            : base(qos)
        {
            this.Destination = destination;
        }

        /// <summary>
        /// Initializes a new instance of the TransportEnvelope class for a messsage received.
        /// </summary>
        /// <param name="message">The received message</param>
        /// <param name="source">The source of the message</param>
        /// <param name="isEncrypted">Whether the message is encrypted</param>
        /// <param name="arrivalTime">The arrival time of the message</param>
        internal TransportEnvelope(MessageBuffer message, PeerAddress source, bool isEncrypted, TimeSpan arrivalTime)
            : base(message, isEncrypted)
        {
            this.Source = source;
            this.ArrivalTime = arrivalTime;
        }

        /// <summary>
        /// Gets or sets the envelope's header.
        /// </summary>
        public ConnectionHeader Header { get; set; }

        /// <inheritdoc />
        public PeerAddress Source { get; set; }

        /// <inheritdoc />
        public ICertificateToken Certificate { get; set; }

        /// <summary>Gets or sets the connection salt.</summary>
        /// <remarks>
        /// A random number that is established at connection. Can be used to prevent replay because it should only be known 
        /// to the two peers involved in a connection
        /// </remarks>
        public long ConnectionSalt { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the envelope was relayed.
        /// </summary>
        public bool WasRelayed { get; set; }

        /// <summary>
        /// Gets the destination of the envelope.
        /// </summary>
        public PeerAddress Destination { get; private set; }

        /// <summary>
        /// Gets or sets the time that the envelope was received by the transport layer (but see remarks!).
        /// </summary>
        /// <remarks>
        /// This value is not set correctly in all places.  It works for UDPTransportLayer.
        /// However, it is not currently working for TcpTransportLayer.
        /// </remarks>
        public TimeSpan ArrivalTime { get; set; }

        /// <summary>
        /// Gets the estimated time that the envelope left the source.
        /// </summary>
        public TimeSpan EstimatedTimeOfDeparture { get; private set; }

        /// <summary>
        /// Gets the estimated RTT at the time the envelope was received.
        /// </summary>
        public TimeSpan EstimatedRoundTripTime { get; private set; }

        /// <summary>
        /// Gets the number of times we have attempted to send this envelope.
        /// </summary>
        public int RetryCount { get; private set; }

        /// <summary>
        /// Records another attempted send.
        /// </summary>
        public void IncrementRetryCount()
        {
            this.RetryCount += 1;
        }

        /// <summary>
        /// Sets the destination of the message
        /// </summary>
        /// <param name="destination">The new destination</param>
        public void SetDestination(PeerAddress destination)
        {
            this.Destination = destination;
        }

        /// <summary>
        /// Creates an encrypted copy of the messsage.
        /// </summary>
        /// <param name="key">The key to use for encryption</param>
        /// <param name="checksumSalt">The salt to use to help verify decryption</param>
        /// <returns>The encrpyted envelope</returns>
        public new TransportEnvelope Encrypt(Badumna.Security.SymmetricKey key, byte[] checksumSalt)
        {
            MessageBuffer encryptedBuffer = base.Encrypt(key, checksumSalt);

            if (encryptedBuffer == null)
            {
                Logger.TraceError(LogTag.Transport, "Failed to encrypt message for. Dropping.");
                return null;
            }

            TransportEnvelope encryptedEnvelope = new TransportEnvelope(encryptedBuffer, this.Source, true, this.ArrivalTime);

            encryptedEnvelope.Message = encryptedBuffer;
            encryptedEnvelope.Destination = this.Destination;
            encryptedEnvelope.Qos = this.Qos;
            encryptedEnvelope.Header = this.Header;
            encryptedEnvelope.SetLabel(this.Label);
            encryptedEnvelope.Certificate = this.Certificate;
            encryptedEnvelope.ConnectionSalt = this.ConnectionSalt;
            encryptedEnvelope.WasRelayed = this.WasRelayed;

            return encryptedEnvelope;
        }

        /// <summary>
        /// Sets the estimated RTT for the envelope.
        /// </summary>
        /// <remarks>
        /// This must be called as soon after receiving the message as practical.
        /// </remarks>
        /// <param name="now">The current time</param>
        /// <param name="rttMs">The estimated round trip time</param>
        public void SetEstimatedRoundTripTime(TimeSpan now, double rttMs)
        {
            this.EstimatedRoundTripTime = TimeSpan.FromMilliseconds(rttMs);
            this.EstimatedTimeOfDeparture = now - TimeSpan.FromMilliseconds(rttMs / 2.0);
        }

        /// <inheritdoc />
        public override void ToMessage(MessageBuffer message, Type parameterType)
        {
            if (null != message)
            {
                base.ToMessage(message, typeof(BaseEnvelope));
            }
        }

        /// <inheritdoc />
        public override void FromMessage(MessageBuffer message)
        {
            if (null != message)
            {
                base.FromMessage(message);
            }
        }

#if TRACE
        /// <inheritdoc />
        public override string ToString()
        {
            string result = "src=" + (this.Source != null ? this.Source.ToString() : "<not set>");
            result += ", dst=" + (this.Destination != null ? this.Destination.ToString() : "<not set>");
            result += ", " + (this.Header != null ? this.Header.ToString() : "<header not set>");
            return result;
        }
#endif
    }
}
