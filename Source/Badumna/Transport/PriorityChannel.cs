﻿using System;
using System.Collections.Generic;
using System.Text;

using Badumna.Utilities;
using Badumna.Core;


namespace Badumna.Transport
{
    class PriorityChannel<T> : Channel<T> where T : IComparable<T>, ISized
    {
        public override bool IsEmpty
        {
            get { return this.mHeap.Count == 0; }
        }

        private BinaryHeap<T> mHeap;


        public PriorityChannel(ITime timeKeeper)
            : base(timeKeeper)
        {
            this.mHeap = new BinaryHeap<T>();
        }

        public PriorityChannel(int capacity, ITime timeKeeper)
            : base(timeKeeper)
        {
            this.mHeap = new BinaryHeap<T>(capacity);
        }

        protected override void DoPut(T item)
        {
            this.mHeap.Push(item);
        }

        protected override T DoGet()
        {
            return this.mHeap.Pop();
        }

        public override T Peek()
        {
            return this.mHeap.Peek();
        }

        public override void Clear()
        {
            this.mHeap.Clear();
        }
    }
}
