using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

using Badumna.Core;
using Badumna.Utilities;


namespace Badumna.Transport
{
    [Serializable]
    class InvalidDestinationException : BadumnaException
    {
        public InvalidDestinationException()
            : base(Resources.InvalidDestination_Message) { }

        public InvalidDestinationException(String message)
            : base(message) { }

        public InvalidDestinationException(String message, Exception innerException)
            : base(message, innerException) { }

        protected InvalidDestinationException(SerializationInfo serializationInfo, StreamingContext streamingContext)
            : base(serializationInfo, streamingContext) { }

    }
}
