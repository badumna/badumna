﻿//-----------------------------------------------------------------------
// <copyright file="FragmentProcessor.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2011 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Diagnostics;
using Badumna.Core;
using Badumna.Utilities;

using SequenceNumber = Badumna.Utilities.CyclicalID.UShortID;

namespace Badumna.Transport.Connections
{
    /// <summary>
    /// Handles the fragmentation and reassembly of large packets.
    /// </summary>
    internal class FragmentProcessor
    {
        /// <summary>
        /// Received fragments that can't yet be reassembled into a complete packet.
        /// </summary>
        private Dictionary<SequenceNumber, Expireable<TransportEnvelope>> defragmentationQueue;

        /// <summary>
        /// Action to take when a packet has been reassembled.
        /// </summary>
        private GenericCallBack<TransportEnvelope> reassembledPacketHandler;

        /// <summary>
        /// Delegate to create a new envelope for a fragment.
        /// </summary>
        private GenericCallBackReturn<TransportEnvelope, PeerAddress, QualityOfService> createEnvelope;

        /// <summary>
        /// Delegate to dispatch a fragment envelope.
        /// </summary>
        private GenericCallBack<TransportEnvelope> sendEnvelope;

        /// <summary>
        /// Provides access to the current time.
        /// </summary>
        private ITime timeKeeper;

        /// <summary>
        /// Initializes a new instance of the FragmentProcessor class.
        /// </summary>
        /// <param name="reassembledPacketHandler">The action to invoke when a packet has been reassembled</param>
        /// <param name="createEnvelope">Delegate to create a new envelope for sending a fragment</param>
        /// <param name="sendEnvelope">Delegate to send a fragment envelope</param>
        /// <param name="eventQueue">The queue to use for scheduling events.</param>
        /// <param name="timeKeeper">Provides access to the current time.</param>
        /// <param name="connectivityReporter">Reports on network connectivity status.</param>
        public FragmentProcessor(
            GenericCallBack<TransportEnvelope> reassembledPacketHandler,
            GenericCallBackReturn<TransportEnvelope, PeerAddress, QualityOfService> createEnvelope,
            GenericCallBack<TransportEnvelope> sendEnvelope,
            NetworkEventQueue eventQueue,
            ITime timeKeeper,
            INetworkConnectivityReporter connectivityReporter)
        {
            if (reassembledPacketHandler == null)
            {
                throw new ArgumentNullException("reassembledPacketHandler");
            }

            if (createEnvelope == null)
            {
                throw new ArgumentNullException("createEnvelope");
            }

            if (sendEnvelope == null)
            {
                throw new ArgumentNullException("sendEnvelope");
            }

            if (timeKeeper == null)
            {
                throw new ArgumentNullException("timeKeeper");
            }

            if (connectivityReporter == null)
            {
                throw new ArgumentNullException("connectivityReporter");
            }

            this.reassembledPacketHandler = reassembledPacketHandler;
            this.createEnvelope = createEnvelope;
            this.sendEnvelope = sendEnvelope;
            this.timeKeeper = timeKeeper;

            this.defragmentationQueue = new Dictionary<SequenceNumber, Expireable<TransportEnvelope>>();

            // TODO: This regular task is never started!
            // TODO: Need to make sure the regular task gets shut down when this instance is no longer required
            new RegularTask("Incomplete fragment collector", Parameters.FragmentQueueTime, eventQueue, connectivityReporter, this.CollectOldFragments);
        }

        /// <summary>
        /// Processes a single fragment, reassembling it into a complete packet if all fragments have
        /// arrived, or queuing it for later processing otherwise.
        /// </summary>
        /// <param name="header">The header of the fragment</param>
        /// <param name="envelope">The fragment envelope</param>
        public void ProcessFragment(ConnectionHeader header, TransportEnvelope envelope)
        {
            Debug.Assert(header.IsFragmented, "Attempting to defragment unfragmented message");

            if (this.defragmentationQueue.ContainsKey(header.SequenceNumber))
            {
                this.defragmentationQueue.Remove(header.SequenceNumber);
            }

            /* The general idea here is to try and find message fragments that have arrived in the past and then
             * recursively merge them until the complete message is known. If a fragment is missing then the 
             * merged fragment will be stored until it arrives.
             *
             * Comment legend : 
             *   .  Another (possibly fragmented) message 
             *   *  A known part of the current fragmented message
             *   ^  This part of the current fragmented message
             *   ?  An unknown part or parts of the current fragmented message
             *   _  A part that has not yet arrived
             */

            envelope.Header = header;
            if (header.IsFirstFragmentedMessage)
            {
                // ..^??
                SequenceNumber nextId = new SequenceNumber(header.SequenceNumber.Value);

                nextId.Increment();
                Expireable<TransportEnvelope> fragment = null;

                if (this.defragmentationQueue.TryGetValue(nextId, out fragment))
                {
                    // ..^*?
                    TransportEnvelope trailingEnvelopeFragment = fragment.Object;
                    this.defragmentationQueue.Remove(nextId);
                    envelope.Message.WriteOffset = envelope.Length;
                    trailingEnvelopeFragment.Message.CopyBytes(envelope.Message, trailingEnvelopeFragment.Message.ReadOffset, trailingEnvelopeFragment.Available);

                    if (trailingEnvelopeFragment.Header.IsLastFragmentedMessage)
                    {
                        // ..^*..
                        // Set the sequence number of the entire message to be the that of the last fragment
                        envelope.Header.SequenceNumber = trailingEnvelopeFragment.Header.SequenceNumber;
                        this.reassembledPacketHandler(envelope);
                    }
                    else
                    {
                        // ..^*?
                        trailingEnvelopeFragment.Header.IsFirstFragmentedMessage = true;
                        this.ProcessFragment(trailingEnvelopeFragment.Header, envelope);
                    }
                }
                else
                {
                    // ..^_?
                    Expireable<TransportEnvelope> expirable = new Expireable<TransportEnvelope>(envelope);

                    expirable.ExpirationTime = this.timeKeeper.Now + Parameters.FragmentQueueTime;
                    this.defragmentationQueue.Add(header.SequenceNumber, expirable);
                }
            }
            else
            {
                // ..??^?
                // Store this fragmnet
                Expireable<TransportEnvelope> expirable = new Expireable<TransportEnvelope>(envelope);

                expirable.ExpirationTime = this.timeKeeper.Now + Parameters.FragmentQueueTime;
                this.defragmentationQueue.Add(header.SequenceNumber, expirable);

                SequenceNumber previousId = new SequenceNumber(header.SequenceNumber.Value);
                Expireable<TransportEnvelope> fragment = null;

                previousId = previousId.Previous;

                // Try and find the first fragment to recursively merge.
                while (this.defragmentationQueue.TryGetValue(previousId, out fragment))
                {
                    TransportEnvelope preceedingEnvelope = fragment.Object;

                    if (preceedingEnvelope.Header.IsFirstFragmentedMessage)
                    {
                        this.ProcessFragment(preceedingEnvelope.Header, preceedingEnvelope);
                        return;
                    }

                    previousId = previousId.Previous;
                }
            }
        }

        /// <summary>
        /// Clears all outstanding fragments from the reassembly queue.
        /// </summary>
        public void Clear()
        {
            this.defragmentationQueue.Clear();
        }

        /// <summary>
        /// Split a message into smaller fragments.
        /// </summary>
        /// <param name="envelope">The message to split into fragments</param>
        public void FragmentMessage(TransportEnvelope envelope)
        {
            this.FragmentMessage(envelope, null);
        }

        /// <summary>
        /// Recursively split a message into smaller fragments.
        /// </summary>
        /// <param name="envelope">The message to split into fragments</param>
        /// <param name="previousFragments">Set to null when calling.  Recursive calls use this to tag each
        /// fragment with a reference to all other fragments.</param>
        private void FragmentMessage(TransportEnvelope envelope, LinkedList<TransportEnvelope> previousFragments)
        {
            Debug.Assert(envelope.Header.IsFragmented == false, "Attempting to re-fragment an already fragmented message.");

            if (previousFragments == null)
            {
                previousFragments = new LinkedList<TransportEnvelope>();
            }

            if (envelope.Length > Parameters.MaximumFragmentSize)
            {
                int overflow = envelope.Length - Parameters.MaximumFragmentSize;
                TransportEnvelope trailingEnvelope = this.createEnvelope(envelope.Destination, envelope.Qos);

                envelope.Message.CopyBytes(trailingEnvelope.Message, Parameters.MaximumFragmentSize, overflow);
                envelope.Message.DropLastNBytes(overflow);
                Debug.Assert(envelope.Length <= Parameters.MaximumFragmentSize, "Fragmented message is still too large.");

                envelope.Header.IsFragmented = true;
                envelope.Header.IsFirstFragmentedMessage = previousFragments.Count == 0;
                envelope.Header.IsLastFragmentedMessage = false;
                this.sendEnvelope(envelope);

                previousFragments.AddLast(envelope);
                this.FragmentMessage(trailingEnvelope, previousFragments);
            }
            else
            {
                envelope.Header.IsFragmented = true;
                envelope.Header.IsFirstFragmentedMessage = previousFragments.Count == 0;
                envelope.Header.IsLastFragmentedMessage = true;

                previousFragments.AddLast(envelope);

                this.sendEnvelope(envelope);
            }
        }

        /// <summary>
        /// Removes stale fragments from the reassembly queue.
        /// </summary>
        private void CollectOldFragments()
        {
            List<SequenceNumber> itemsToRemove = new List<SequenceNumber>();

            foreach (KeyValuePair<SequenceNumber, Expireable<TransportEnvelope>> fragmentKVP in this.defragmentationQueue)
            {
                if (fragmentKVP.Value.ExpirationTime < this.timeKeeper.Now)
                {
                    itemsToRemove.Add(fragmentKVP.Key);
                }
            }

            foreach (SequenceNumber fragmentId in itemsToRemove)
            {
                this.defragmentationQueue.Remove(fragmentId);
            }
        }
    }
}
