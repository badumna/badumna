﻿using System;
using System.Collections.Generic;
using System.Text;

using Badumna.Core;
using Badumna.Utilities;

namespace Badumna.Transport
{

    /// <summary>
    /// The interface visible to Connection from ConnectionTable.
    /// </summary>
    interface IConnectionTable 
    {
        bool IsKnown(PeerAddress address);
        bool IsConnected(PeerAddress address);
        bool IsRelayed(PeerAddress address);
        void RemoveConnection(PeerAddress address);
        void ConfirmConnection(PeerAddress address);
        IEnumerable<IConnection> AllConnections { get; }
        TransportEnvelope GetMessageFor(PeerAddress destination, QualityOfService qos);
    }

    class ProtocolRoot : TransportProtocol, IConnectionTable
    {
        private ConnectionTable mConnectionTable;

        public ProtocolRoot(ConnectionTable connectionTable, GenericCallBackReturn<object, Type> factory)
            : base("Connectionful root", typeof(ConnectionfulProtocolMethodAttribute), factory)
        {
            this.mConnectionTable = connectionTable;
            this.DispatcherMethod = this.mConnectionTable.DispatchMessage;
        }

        public override void PrepareMessageForDeparture(ref TransportEnvelope envelope)
        {
            this.mConnectionTable.PrepareMessage(ref envelope);
        }

        #region IConnectionTable Members

        public bool IsKnown(PeerAddress address)
        {
            return this.mConnectionTable.IsKnown(address);
        }

        public bool IsConnected(PeerAddress address)
        {
            return this.mConnectionTable.IsConnected(address);
        }

        public bool IsRelayed(PeerAddress address)
        {
            return this.mConnectionTable.IsRelayed(address);
        }

        public void RemoveConnection(PeerAddress address)
        {
            this.mConnectionTable.RemoveConnection(address);
        }

        public void ConfirmConnection(PeerAddress address)
        {
            this.mConnectionTable.ConfirmConnection(address);
        }

        public IEnumerable<IConnection> AllConnections
        {
            get { throw new NotImplementedException(); }
        }

        #endregion

    }
}
