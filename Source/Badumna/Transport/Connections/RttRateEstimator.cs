﻿using System;
using System.Collections.Generic;
using System.Text;

using Badumna.Core;
using Badumna.Utilities;
using Badumna.Utilities.Logging;

namespace Badumna.Transport
{
    interface IRttRateEstimator
    {
        double RetransmissionTimeoutMs { get; }

        /// <summary>
        /// An estimate of the round-trip-time in milliseconds.
        /// </summary>
        double RoundTripTimeEstimate { get; set; }

        /// <summary>
        /// Update the round-trip-time estimation.
        /// </summary>
        /// <param name="sample"></param>
        void UpdateEstimate(TimeSpan sample);
    }

    class RttRateEstimator : IRttRateEstimator
    {
        /// <summary>
        /// To ensure timeout is strictly greater than ack return time (accounts for processing time and rtt deviation of zero
        /// And to account for processing delay which can be as much as 200ms.
        /// </summary>
        /// <remarks>
        /// TODO: (DGC) I think we need to test this.  Doesn't the RTT converge to the time that
        ///       acks generally take to come back?  Which should be more or less exactly the
        ///       right amount of time to wait for the ack.  200ms seems quite large -- I would've
        ///       thought we could schedule/process events much more accurately/quickly.
        /// </remarks>
        internal const int ExtraTimeoutDelay = 200;

        /// <summary>
        /// A lower bound on the RTT estimate we report, in milliseconds.
        /// </summary>
        /// <remarks>
        /// This lower bound was added as a safety measure for code that performs
        /// calcuations with the reciprocal of RTT.
        /// </remarks>
        private const double RttLowerBoundMs = 0.001;

        /// <summary>
        /// An estimate of the round-trip-time in milliseconds.
        /// </summary>
        public double RoundTripTimeEstimate
        {
            get { return Math.Max(RttRateEstimator.RttLowerBoundMs, this.mSmoothedRTTEstimator); }
            set
            {
                this.mSmoothedRTTEstimator = value;
                this.mSmoothedMeanDeviationRTTEstimator = 0;
                this.UpdateTimeouts();
            }
        }

        public double RetransmissionTimeoutMs
        {
            get { return this.mRetransmissionTimeoutMs; }
        }

        private double mSmoothedRTTEstimator;
        private double mSmoothedMeanDeviationRTTEstimator;
        private int mNumberOfRTTCalculations;

        private double mRetransmissionTimeoutMs;

        public RttRateEstimator(TimeSpan initialEstimate)
        {
            this.RoundTripTimeEstimate = initialEstimate.TotalMilliseconds;
        }

        public void UpdateEstimate(TimeSpan sample)
        {
            double delta = sample.TotalMilliseconds - this.mSmoothedRTTEstimator;

            // The first time we make a calculation we reset the smoothed estimator,
            // this prevents the estimate being inaccurate for a long time if the default value is very inaccurate.
            if (this.mNumberOfRTTCalculations == 0)
            {
                this.mSmoothedRTTEstimator = sample.TotalMilliseconds;
                delta = 0;
            }

            // Limit the size of delta so that dropped packets do not interfere too much with the estimate.
            delta = Math.Min(delta, 0.95 * this.mSmoothedRTTEstimator); // Effectivley limits sample to 1.95 * this.mSmoothedRTTEstimator 

            // Calculate the running average of the RTT and its deviation.
            this.mSmoothedRTTEstimator += (delta / 8.0);
            this.mSmoothedMeanDeviationRTTEstimator += (System.Math.Abs(delta) - this.mSmoothedMeanDeviationRTTEstimator) / 4;

            // Only calculate the retransmission timeout when we have enough samples to make an accurate estimate.
            if (this.mNumberOfRTTCalculations > 8)
            {
                this.UpdateTimeouts();
            }
            else
            {
                this.mNumberOfRTTCalculations++;
            }

            Logger.TraceInformation(LogTag.Connection, "RTT estimate updated to {0}", this.RoundTripTimeEstimate);
        }

        private void UpdateTimeouts()
        {
            double rtt = this.mSmoothedRTTEstimator + this.mSmoothedMeanDeviationRTTEstimator;  // allows for some variation in the RTT

            // Retransmission timeout is based on the RTT estimate plus a small constant delay.  Part of the delay is the maximum time that
            // an ack can be queued for on the remote machine - normally acks will be sent back earlier than this because they'll be piggybacked
            // on to another packet.  Over time the RTT estimate will converge to the average ack response time which is likely to be less than
            // the maximum time (also, the RTT estimate should eventually be fixed to match the actually RTT rather than ack response time).  The
            // maximum ack delay is added to prevent an unnecessary resend which would occur if acks are normally piggybacked but occasionally queued.
            // A small extra delay is added in case the estimated deviation is zero.
            // Minimum timeout is one millisecond.
            this.mRetransmissionTimeoutMs = Math.Max(RttRateEstimator.ExtraTimeoutDelay, rtt + Parameters.AckDelay.TotalMilliseconds + RttRateEstimator.ExtraTimeoutDelay);
        }
    }

    /// <summary>
    /// MobileRttRateEstimator is used only on mobile platforms.
    /// <remarks>
    /// Constant rtt is return for mobile platforms, as tfrc algorithm is disabled.
    /// </remarks>
    /// </summary>
    class MobileRttRateEstimator : IRttRateEstimator
    {
        public double RetransmissionTimeoutMs
        {
            get
            {
                return this.RoundTripTimeEstimate + Parameters.AckDelay.TotalMilliseconds + RttRateEstimator.ExtraTimeoutDelay;
            }
        }

        public double RoundTripTimeEstimate
        {
            get
            {
                // Constant rtt time in milliseconds
                return 300.0;
            }
            
            set
            {
                // Do nothing.
            }       
        }

        public void UpdateEstimate(TimeSpan sample)
        {
        }
    }
}
