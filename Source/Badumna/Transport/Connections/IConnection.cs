﻿//---------------------------------------------------------------------------------
// <copyright file="IConnection.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using Badumna.Core;

namespace Badumna.Transport
{
    /// <summary>
    /// The connection interface.
    /// </summary>
    internal interface IConnection
    {
        /// <summary>
        /// Gets the last receive time.
        /// </summary>
        /// <value>The last receive time.</value>
        TimeSpan LastReceiveTime
        {
            get;
        }

        /// <summary>
        /// Gets the peers public address.
        /// </summary>
        /// <value>The peers public address.</value>
        PeerAddress PeersPublicAddress
        {
            get;
        }
    }
}
