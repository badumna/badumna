﻿using System;
using System.Collections.Generic;
using System.Text;

using Badumna.Utilities;
using Badumna.Core;
using Badumna.Security;


namespace Badumna.Transport
{
    using Badumna.Transport.Connections;

    partial class ConnectionTable
    {
        public override void SendDirectConfirmation(PeerAddress address, byte[] encryptedRequestId)
        {
            TransportEnvelope envelope = this.GetConnectionlessMessageFor(address);

#if TRACE 
            envelope.AppendLabel("direct ");
#endif
            this.RemoteCall(envelope, this.TransportConfirmation, this.addressProvider.PublicAddress, false, (byte)0, encryptedRequestId);
            this.SendConnectionlessMessage(envelope, address, true);
        }

        public override void RelayConnectionAttemptNotification(PeerAddress address)
        {
            TransportEnvelope envelope = this.GetConnectionlessMessageFor(address);

#if TRACE
            envelope.AppendLabel("relay ");
#endif
            this.RemoteCall(envelope, this.TransportConfirmation, this.addressProvider.PublicAddress, true, (byte)1, new byte[] { 1 });
            this.SendConnectionlessMessage(envelope, address, true);
        }

        [ConnectionlessProtocolMethod(ConnectionlessMethod.TransportConfirmation)]
        protected void TransportConfirmation(PeerAddress source, bool isRequest, byte method, byte[] proof)
        {
            if (isRequest)
            {
                this.ConfirmConnection(source);

                TransportEnvelope envelope = this.GetConnectionlessMessageFor(source);

#if TRACE
                envelope.AppendLabel("relay reply ");
#endif

                this.RemoteCall(envelope, this.TransportConfirmation, this.addressProvider.PublicAddress, false, (byte)1, new byte[] { 1 });
                this.SendConnectionlessMessage(envelope, source, true);
            }
            else
            {
                Connection connection = null;

                if (this.mConnections.TryGetValue(source, out connection))
                {
                    if (method == 0)
                    {
                        connection.ConfirmDirectTransport(proof);
                    }
                    else
                    {
                        connection.ConfirmRelayTransport();
                    }
                }
            }
        }

        public void InitiateHolePunchRequest(PeerAddress destination)
        {
            TransportEnvelope envelope = this.GetConnectionlessMessageFor(destination);

            this.RemoteCall(envelope, this.HolePunchNotification, this.addressProvider.PublicAddress);
            this.SendConnectionlessMessage(envelope, destination, true);
        }

        [ConnectionlessProtocolMethod(ConnectionlessMethod.HolePunchNotification)]
        protected void HolePunchNotification(PeerAddress source)
        {
            Logger.TraceInformation(LogTag.Connection, "Processing hole punch request from {0}...", source);
            Connection connection = this.GetOrCreateConnection(source);

            if (connection != null)
            {
                if (this.addressProvider.PublicAddress.NatType == NatType.SymmetricNat)
                {
                    if (!connection.IsConnected && !connection.IsInitializing)
                    {
                        connection.BeginInitialization();
                    }
                    else
                    {
                        Logger.TraceInformation(LogTag.Connection, "Ignoring hole punch request with {0} because the connection is already active.", source);
                    }
                }
                else
                {
                    this.PunchUDPHole(source);

                    TransportEnvelope envelope = this.GetConnectionlessMessageFor(source);
                    this.RemoteCall(envelope, this.HolePunchConfirmation, this.addressProvider.PublicAddress);
                    this.SendConnectionlessMessage(envelope, source, true);
                }
            }
        }

        [ConnectionlessProtocolMethod(ConnectionlessMethod.HolePunchConfirmation)]
        protected void HolePunchConfirmation(PeerAddress source)
        {
            Logger.TraceInformation(LogTag.Connection, "Processing hole punch confirmation from {0}...", source);
            Connection connection = null;

            if (this.mConnections.TryGetValue(source, out connection))
            {
                if (!connection.IsConnected && !connection.IsInitializing && !connection.IsExpired)
                {
                    connection.BeginInitialization();
                }
            }
            else
            {
                Logger.TraceWarning(LogTag.Connection, "Nothing known about connection with {0}. Not doing anything", source);
            }
        }

        private void AttemptRelayWith(Connection oldConnection)
        {
            // Remove all references to a connection in the connection list.
            this.DeleteConnection(oldConnection.PeersPublicAddress);
            this.DeleteConnection(oldConnection.PeersPrivateAddress);

            Logger.TraceInformation(LogTag.Connection, "Attempting relay connection with {0}", oldConnection.PeersPublicAddress);
            Connection newConnection = this.InitiateConnection(oldConnection.PeersPublicAddress, null, CommunicationMethod.Relay);

            newConnection.UsurpConnection(oldConnection);
            oldConnection.Shutdown();
        }

        public void PunchUDPHole(PeerAddress address)
        {
            if (!this.mPunchedHoles.ContainsKey(address))
            {
                this.mPunchedHoles.Add(address, address);
            }

            Logger.TraceInformation(LogTag.Connection, "Sending empty hole punch messagae to {0}", address);
            this.SendEmptyHolePunchMessage(address);
        }

        private void SendEmptyHolePunchMessage(PeerAddress address)
        {
            TransportEnvelope envelope = new TransportEnvelope(address, QualityOfService.Unreliable);
            envelope.Qos.Priority = QosPriority.High;
            this.mDispatcher.SendMessage(envelope);
        }
       
        private void RemovePunchedHole(PeerAddress address)
        {
            // stop renewing the hole
            if (this.mPunchedHoles.ContainsKey(address))
            {
                this.mPunchedHoles.Remove(address);
            }
        }

        public void TryInternalConnection(PeerAddress peersPrivateAddress, PeerAddress peersPublicAddress)
        {
            if (this.connectivityReporter.Status == ConnectivityStatus.Online)
            {
                Connection currentConnection = null;

                if (null != peersPrivateAddress)
                {
                    if (!this.mConnections.TryGetValue(peersPublicAddress, out currentConnection)
                        || !currentConnection.PeersPrivateAddress.Equals(peersPrivateAddress)
                        || currentConnection.IsGarbage)
                    {
                        this.TestInternalConnectivity(peersPrivateAddress, peersPublicAddress);
                    }
                }
            }
        }

        public void TestInternalConnectivity(PeerAddress internalAddress, PeerAddress publicAddress)
        {
            Logger.TraceInformation(LogTag.Connection, "Testing direct internal connection with {0} using {1}", publicAddress, internalAddress);

            TransportEnvelope envelope = this.GetConnectionlessMessageFor(internalAddress);

            this.RemoteCall(envelope, this.RespondToInternalConnectivityTest,
                this.addressProvider.PrivatePort, this.addressProvider.PublicAddress);
            this.SendConnectionlessMessage(envelope, publicAddress, false);
        }

        internal TransportEnvelope GetConnectionlessMessageFor(PeerAddress dispatchAddress)
        {
            TransportEnvelope envelope = new TransportEnvelope(dispatchAddress, QualityOfService.Unreliable);
            ConnectionHeader header = new ConnectionHeader();
            header.IsConnectionless = true;
            envelope.Message.Write(header);
            return envelope;
        }

        // The destination address may not the public address, so we need to include the publicAddress argument so that 
        // the checksum is correct.
        internal void SendConnectionlessMessage(TransportEnvelope envelope, PeerAddress publicAddress, bool useRelay)
        {
            TransportEnvelope encryptedEnvelope = null;
            SymmetricKeyToken token = this.tokens.GetNetworkParticipationToken();
            if (token != null)
            {
                encryptedEnvelope = envelope.Encrypt(token.Key, this.GetSalt(publicAddress));
            }
            else
            {
                Logger.TraceWarning(LogTag.Connection, "Failed to find authorization token for JoinNetwork. Dropping outbound connectionless packet!");
                return;
            }

            if (encryptedEnvelope == null)
            {
                Logger.TraceWarning(LogTag.Connection, "Failed to encrypt connectionless message for {0}. Dropping.", publicAddress);
                return;
            }

            if (useRelay)
            {
                encryptedEnvelope.SetDestination(publicAddress);
                this.mRelayManager.RelayMessage(encryptedEnvelope, true, Parameters.RelayedConnectionlessMessageRedundancyFactor);
            }
            else
            {
                this.mDispatcher.SendMessage(encryptedEnvelope);
            }
        }

        [ConnectionlessProtocolMethod(ConnectionlessMethod.RespondToInternalConnectivityTest)]
        private void RespondToInternalConnectivityTest(int port, PeerAddress publicAddress)
        {
            PeerAddress internalAddress = new PeerAddress(this.CurrentEnvelope.Source.Address, port, NatType.Internal);

            if (!internalAddress.IsValid || this.addressProvider.HasPrivateAddress(internalAddress) ||
                !publicAddress.IsValid)  // Hopefully this shouldn't happen...
            {
                return; // Don't send to self
            }

            // Create a connetion mapping so that any initial messages sent by the peer are not dropped because it
            // is not sent from the public address.
            Connection connection = null;

            if (!this.mConnections.ContainsKey(internalAddress))
            {
                if (this.mConnections.TryGetValue(publicAddress, out connection))
                {
                    Logger.TraceInformation(LogTag.Connection, "Adding internal index ({0}) for connection {1}", internalAddress, publicAddress);
                    this.AddConnection(internalAddress, connection);
                    connection.PeersPrivateAddress = internalAddress;
                }
                else
                {
                    Logger.TraceInformation(LogTag.Connection, "Internal address {0} not indexed and neither is public address {1}", internalAddress, publicAddress);
                    Logger.TraceInformation(LogTag.Connection, "Creating connection instance for address pair {0} - {1}", internalAddress, publicAddress);
                    connection = this.CreateConnection(publicAddress);

                    if (!internalAddress.Equals(publicAddress))
                    {
                        Logger.TraceInformation(LogTag.Connection, "Adding internal address ({1}) index for public address {0}", publicAddress, internalAddress);
                        connection.PeersPrivateAddress = internalAddress;
                        this.AddConnection(internalAddress, connection);
                    }
                }
            }

            TransportEnvelope envelope = this.GetConnectionlessMessageFor(internalAddress);
            this.RemoteCall(envelope, this.InternalConnectionAvailable, this.addressProvider.PublicAddress);
            this.SendConnectionlessMessage(envelope, publicAddress, false);
        }

        [ConnectionlessProtocolMethod(ConnectionlessMethod.InternalConnectionAvailable)]
        private void InternalConnectionAvailable(PeerAddress publicAddress)
        {
            PeerAddress peersPrivateAddress = this.CurrentEnvelope.Source;
            Connection currentConnection = null;

            peersPrivateAddress.NatType = NatType.Internal;

            // Try and find any connections indexed by the public address.
            if (this.mConnections.TryGetValue(publicAddress, out currentConnection))
            {
                // If the connection exists and is NOT relayed then we can set the private address to the source and return.
                if (!currentConnection.IsRelayed)
                {
                    if (!currentConnection.PeersPrivateAddress.Equals(peersPrivateAddress))
                    {
                        Logger.TraceInformation(LogTag.Connection, "Changing private address of {0} to {1}", publicAddress, peersPrivateAddress);
                        currentConnection.PeersPrivateAddress = peersPrivateAddress;
                        if (!this.mConnections.ContainsKey(peersPrivateAddress))
                        {
                            this.AddConnection(peersPrivateAddress, currentConnection);
                        }
                    }

                    if (currentConnection.IsGarbage)
                    {
                        this.CleanupConnection(publicAddress);
                    }
                    else
                    {
                        Logger.TraceInformation(LogTag.Connection, "Ignoring internal connection request because active direct connection exists.");
                        return;
                    }
                }
            }
            else if (this.mConnections.ContainsKey(peersPrivateAddress))
            {
                Logger.TraceInformation(LogTag.Connection, "Removing connection indexed by {0} to make room for new internal conneciton", peersPrivateAddress);
                this.DeleteConnection(peersPrivateAddress);
            }

            // Should only reach here if the connection doesn't exist, is relayed or was garbage.
            Connection internalConnection = this.InitiateConnection(publicAddress, peersPrivateAddress, CommunicationMethod.Direct);

            if (internalConnection == null)
            {
                Logger.TraceError(LogTag.Connection, "Failed to connect with {0} using address {1}", publicAddress, peersPrivateAddress);
                return;
            }

            if (currentConnection != null)
            {
                internalConnection.UsurpConnection(currentConnection);
            }
        }
    }
}