//------------------------------------------------------------------------------
// <copyright file="ConnectionHeader.cs" company="National ICT Australia Limited">
//     Copyright (c) 2007,2012 National ICT Australia Limited. All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

namespace Badumna.Transport.Connections
{
    using System;
    using Badumna.Core;
    using Badumna.Utilities;

    /// <summary>
    /// The packet header.
    /// </summary>
    internal class ConnectionHeader : IParseable
    {
        /// <summary>
        /// Serialized length of the header in bytes.
        /// </summary>
        public const int Length = 4;

        /// <summary>
        /// The flags set for the packet.
        /// </summary>
        private Flags flags = Flags.EnforceOrder;

        /// <summary>
        /// The sequence number of the packet.
        /// </summary>
        private CyclicalID.UShortID sequenceNumber;

        /// <summary>
        /// Initializes a new instance of the ConnectionHeader class.
        /// </summary>
        public ConnectionHeader()
        {
            this.sequenceNumber = new CyclicalID.UShortID();
        }

        /// <summary>
        /// The set of possible header flags.
        /// </summary>
        [Flags]
        private enum Flags : ushort
        {
            /// <summary>
            /// Indicates a packet not associated with a connection (for connection establishment, discovery, etc).
            /// </summary>
            Connectionless = 1 << 0,

            /// <summary>
            /// Indicates a reliable packet.
            /// </summary>
            Reliable = 1 << 1,

            /// <summary>
            /// Inidicates the initial packet (for connection establishment).
            /// </summary>
            Initial = 1 << 2,

            /// <summary>
            /// Indicates that the packet must be passed to the receiving layer in order.
            /// Out-of-order packets with this flag set are dropped.
            /// </summary>
            EnforceOrder = 1 << 3,

            /// <summary>
            /// Indicates the packet is part of a fragmented set.
            /// </summary>
            Fragmented = 1 << 4,

            /// <summary>
            /// Indicates the last fragment of a fragmented set.
            /// </summary>
            LastFragmented = 1 << 5,

            /// <summary>
            /// Indicates the first fragment of a fragmented set.
            /// </summary>
            FirstFragmented = 1 << 6,

            /// <summary>
            /// Indicates the packet includes acknowledgements.
            /// </summary>
            Acknowledgments = 1 << 7,

            /// <summary>
            /// Indicates a realtime packet.
            /// Currently ignored.
            /// </summary>
            RealtimeMessage = 1 << 8,

            /// <summary>
            /// Indicates the packet contains congestion feedback.
            /// </summary>
            CongestionFeedback = 1 << 9,
        }

        /// <summary>
        /// Gets or sets a value indicating whether this is a realtime packet.
        /// Currently ignored.
        /// </summary>
        public bool IsRealtimeMessage
        {
            get { return this.GetFlag(Flags.RealtimeMessage); }
            set { this.SetFlags(Flags.RealtimeMessage, value); }
        }
        
        /// <summary>
        /// Gets or sets a value indicating whether this is a packet not associated with a connection (for connection establishment, discovery, etc).
        /// </summary>
        public bool IsConnectionless
        {
            get { return this.GetFlag(Flags.Connectionless); }
            set { this.SetFlags(Flags.Connectionless, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this is a reliable packet.
        /// </summary>
        public bool IsReliable
        {
            get { return this.GetFlag(Flags.Reliable); }
            set { this.SetFlags(Flags.Reliable, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this is the initial packet (for connection establishment).
        /// </summary>
        public bool IsInitial
        {
            get { return this.GetFlag(Flags.Initial); }
            set { this.SetFlags(Flags.Initial, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the packet is part of a fragmented set.
        /// </summary>
        public bool IsFragmented
        {
            get { return this.GetFlag(Flags.Fragmented); }
            set { this.SetFlags(Flags.Fragmented, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the packet is the last fragment of a fragmented set.
        /// </summary>
        public bool IsLastFragmentedMessage
        {
            get { return this.GetFlag(Flags.LastFragmented); }
            set { this.SetFlags(Flags.LastFragmented, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether  the packet is the first fragment of a fragmented set.
        /// </summary>
        public bool IsFirstFragmentedMessage
        {
            get { return this.GetFlag(Flags.FirstFragmented); }
            set { this.SetFlags(Flags.FirstFragmented, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the packet must be passed to the receiving layer in order.
        /// Out-of-order packets with this flag set are dropped.
        /// </summary>
        public bool ShouldEnforceOrder
        {
            get { return this.GetFlag(Flags.EnforceOrder); }
            set { this.SetFlags(Flags.EnforceOrder, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the packet includes acknowledgements.
        /// </summary>
        public bool ContainsAcknowledgements
        {
            get { return this.GetFlag(Flags.Acknowledgments); }
            set { this.SetFlags(Flags.Acknowledgments, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the packet contains congestion feedback.
        /// </summary>
        public bool HasCongestionFeedback
        {
            get { return this.GetFlag(Flags.CongestionFeedback); }
            set { this.SetFlags(Flags.CongestionFeedback, value); }
        }

        /// <summary>
        /// Gets a value indicating whether the header contains a valid sequence number.
        /// </summary>
        public bool HasSequenceNumber { get; private set; }

        /// <summary>
        /// Gets or sets the sequence number in the header.
        /// </summary>
        public CyclicalID.UShortID SequenceNumber
        {
            get
            {
                return this.sequenceNumber;
            }

            set
            {
                this.sequenceNumber = value;
                this.HasSequenceNumber = true;
            }
        }

        /// <summary>
        /// Gets the offset of the header within the message.
        /// </summary>
        public int PositionInMessage { get; private set; }

        /// <inheritdoc />
        public void ToMessage(MessageBuffer message, Type parameterType)
        {
            this.PositionInMessage = message.WriteOffset;
            message.Write((ushort)this.flags);
            message.Write(this.sequenceNumber, this.sequenceNumber.GetType());
        }

        /// <inheritdoc />
        public void FromMessage(MessageBuffer message)
        {
            if (message.Available < 3)
            {
                throw new ArgumentException("Message is not long enough to parse ConnectionHeader.");
            }

            this.PositionInMessage = message.ReadOffset;
            this.flags = (Flags)message.ReadUShort();
            this.sequenceNumber.FromMessage(message);
            this.HasSequenceNumber = !this.IsConnectionless || this.IsInitial;  // TODO: Confirm this logic
        }

#if TRACE
        /// <inheritdoc />
        public override string ToString()
        {
            return "seq=" + this.SequenceNumber.ToString() + ", flags=" + 
                (this.IsReliable ? "r" : "") +
                (this.IsInitial ? "i" : "") +
                (this.IsFragmented ? "f" : "");
        }
#endif

        /// <summary>
        /// Sets the specified flags to the given value.
        /// </summary>
        /// <param name="flags">The flags to set</param>
        /// <param name="value">The value to set</param>
        private void SetFlags(Flags flags, bool value)
        {
            if (value)
            {
                this.flags |= flags;
            }
            else
            {
                this.flags &= ~flags;
            }
        }

        /// <summary>
        /// Gets a value indicating whether any of the specified flags are set.
        /// </summary>
        /// <param name="flag">The flags to check</param>
        /// <returns><c>true</c> if any of the specified flags are set, <c>false</c> otherwise</returns>
        private bool GetFlag(Flags flag)
        {
            return (this.flags & flag) != 0;
        }
    }
}
