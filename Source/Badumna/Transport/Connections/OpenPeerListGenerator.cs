﻿//---------------------------------------------------------------------------------
// <copyright file="OpenPeerListGenerator.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Net;
using Badumna.Core;

namespace Badumna.Transport
{
    /// <summary>
    /// add summary here.
    /// </summary>
    internal class OpenPeerListGenerator
    {
        /// <summary>
        /// The connection table.
        /// </summary>
        private IConnectionTable connectionTable;

        /// <summary>
        /// Initializes a new instance of the <see cref="OpenPeerListGenerator"/> class.
        /// </summary>
        /// <param name="connectionTable">The connection table.</param>
        public OpenPeerListGenerator(IConnectionTable connectionTable)
        {
            this.connectionTable = connectionTable;
        }

        /// <summary>
        /// Gets the IPEndPoints of recent active open peers.
        /// </summary>
        /// <returns>A list recently active open peers. </returns>
        public List<IPEndPoint> GetRecentActiveOpenPeers()
        {
            List<IConnection> openPeerConnections = this.GetOpenPeerConnections();

            ushort num = Parameters.NumberOfOpenPeerAddressesToSave;
            return GetMostRecentActiveConnections(openPeerConnections, num);
        }

        /// <summary>
        /// Gets the most recently active connections.
        /// </summary>
        /// <param name="connections">The connections.</param>
        /// <param name="num">The num of connections required.</param>
        /// <returns>No more than num recently active open connections.</returns>
        private static List<IPEndPoint> GetMostRecentActiveConnections(List<IConnection> connections, ushort num)
        {
            if (connections == null)
            {
                throw new ArgumentNullException();
            }

            if (connections.Count <= num)
            {
                return GetConnectionAddresses(connections);
            }
            else
            {
                connections.Sort(new ConnectionComparer());
                List<IConnection> results = new List<IConnection>();

                foreach (IConnection connection in connections)
                {
                    if (results.Count >= num)
                    {
                        break;
                    }
                    else
                    {
                        results.Add(connection);
                    }
                }

                return GetConnectionAddresses(results);
            }
        }

        /// <summary>
        /// Gets the connection addresses.
        /// </summary>
        /// <param name="connections">The connections.</param>
        /// <returns>A list of IPEndPoint of the connections.</returns>
        private static List<IPEndPoint> GetConnectionAddresses(List<IConnection> connections)
        {
            List<IPEndPoint> addresses = new List<IPEndPoint>();

            foreach (IConnection connection in connections)
            {
                IPEndPoint endpoint = connection.PeersPublicAddress.EndPoint;
                if (endpoint != null)
                {
                    addresses.Add(endpoint);
                }
            }

            return addresses;
        }

        /// <summary>
        /// Gets all open peer connections.
        /// </summary>
        /// <returns>All open connections in the connection table.</returns>
        private List<IConnection> GetOpenPeerConnections()
        {
            ushort max = (ushort)(Parameters.NumberOfOpenPeerAddressesToSave * 5);
            List<IConnection> openPeerConnections = new List<IConnection>();
            foreach (IConnection connection in this.connectionTable.AllConnections)
            {
                if (connection.PeersPublicAddress.NatType == NatType.FullCone ||
                    connection.PeersPublicAddress.NatType == NatType.Open)
                {
                    if (!connection.PeersPublicAddress.IsAddressPrivate() &&
                        !connection.PeersPublicAddress.IsDhtAddress)
                    {
                        openPeerConnections.Add(connection);

                        // make sure we don't return some stupid number of connections
                        // to overload the seedpeer. 
                        if (openPeerConnections.Count > max)
                        {
                            return openPeerConnections;
                        }
                    }
                }
            }

            return openPeerConnections;
        }

        /// <summary>
        /// The comparer used to sort connection based on their last receive time.
        /// </summary>
        private class ConnectionComparer : IComparer<IConnection>
        {
            /// <summary>
            /// Compares two objects and returns a value indicating whether one is less than, equal to, or greater than the other.
            /// </summary>
            /// <param name="x">The first object to compare.</param>
            /// <param name="y">The second object to compare.</param>
            /// <returns>
            /// Value
            /// Condition
            /// Less than zero
            /// <paramref name="x"/> is less than <paramref name="y"/>.
            /// Zero
            /// <paramref name="x"/> equals <paramref name="y"/>.
            /// Greater than zero
            /// <paramref name="x"/> is greater than <paramref name="y"/>.
            /// </returns>
            public int Compare(IConnection x, IConnection y)
            {
                if (x == null || y == null)
                {
                    throw new ArgumentNullException();
                }

                return y.LastReceiveTime.CompareTo(x.LastReceiveTime);
            }
        }
    }
}
