﻿using System;
using System.Collections.Generic;
using System.Text;

using Badumna.Core;
using Badumna.Utilities;
using SequenceNumber = Badumna.Utilities.CyclicalID.UShortID;
using System.Diagnostics;


namespace Badumna.Transport
{
    /// <summary>
    /// Interface for congestion control.
    /// </summary>
    interface ICongestionControl
    {
        double SendLimitBytesPerSecond { get; }
        TcpFriendlyRateControl.Mode LimitMode { get; }

        double LossEventRate { get; }
        double RemoteLossEventRate { get; }

        void Sent(int length);

        void ReceivedInitial(SequenceNumber sequenceNumber, int length);
        void Received(SequenceNumber sequenceNumber, int length, bool feedbackOnly);

        Feedback GetFeedback();

        /// <summary>
        /// Processes congestion feedback from the remote end.
        /// </summary>
        /// <param name="feedback">The feedback information</param>
        /// <param name="arrivalTime">The time the feedback arrived</param>
        void CongestionFeedback(Feedback feedback, TimeSpan arrivalTime);

        void Reset();
        void Shutdown();
    }

    /// <summary>
    /// Interfaces for all external methods / properties required by the TFRC algorithm; for easier testing.
    /// </summary>
    interface ITcpFriendlyRateControlDelegate
    {
        TimeSpan Now { get; }

        NetworkEventQueue EventQueue { get; }

        double RoundTripTimeEstimateMs { get; }
        double SentBytesPerSecond { get; }

        void ScheduleFeedback(TimeSpan maximumDelay);
    }

    class DefaultTfrcDelegate : ITcpFriendlyRateControlDelegate
    {
        private Connection mConnection;
        private TransportProtocol mTransportProtocol;

        /// <summary>
        /// Provides access to the current time.
        /// </summary>
        private ITime timeKeeper;

        /// <summary>
        /// A task that will send an empty envelope after a period of time.
        /// Used to ensure feedback is sent even if there is no other outgoing traffic.
        /// </summary>
        private readonly CommonTask delayedMessageSender;

        public DefaultTfrcDelegate(
            Connection connection,
            TransportProtocol transportProtocol,
            NetworkEventQueue eventQueue,
            ITime timeKeeper,
            CommonTask delayedMessageSender)
        {
            this.mConnection = connection;
            this.mTransportProtocol = transportProtocol;
            this.EventQueue = eventQueue;
            this.timeKeeper = timeKeeper;
            this.delayedMessageSender = delayedMessageSender;
        }

        public TimeSpan Now
        {
            get { return this.timeKeeper.Now; }
        }

        public NetworkEventQueue EventQueue { get; private set; }

        public double RoundTripTimeEstimateMs
        {
            get { return this.mConnection.RoundTripTimeEstimate; }
        }

        public double SentBytesPerSecond
        {
            get { return this.mConnection.SentBytesPerSecond; }
        }

        public void ScheduleFeedback(TimeSpan maximumDelay)
        {
            this.delayedMessageSender.Schedule(maximumDelay, this);
        }

        public void CancelScheduledFeedback()
        {
            this.delayedMessageSender.Cancel(this);
        }
    }

    /// <summary>
    /// NullCongestionControl class is used only for mobile platforms.
    /// This used to disable tfrc congestion control algorithm.
    /// </summary>
    class NullCongestionControl : ICongestionControl
    {
        public double SendLimitBytesPerSecond
        {
            get { return double.PositiveInfinity; }
        }

        public TcpFriendlyRateControl.Mode LimitMode
        {
            get { return TcpFriendlyRateControl.Mode.Initial; }
        }

        public double LossEventRate
        {
            get { return 0.0; }
        }

        public double RemoteLossEventRate
        {
            get { return 0.0; }
        }

        public void Sent(int length)
        {
        }

        public void ReceivedInitial(SequenceNumber sequenceNumber, int length)
        {
        }

        public void Received(SequenceNumber sequenceNumber, int length, bool feedbackOnly)
        {
        }

        public Feedback GetFeedback()
        {
            return null;
        }

        public void CongestionFeedback(Feedback feedback, TimeSpan arrivalTime)
        {
        }

        public void Reset()
        {
        }

        public void Shutdown()
        {
        }
    }

    class Feedback : IParseable
    {
        public const int SerializedSize = 10;

        public ushort LossEventRate { get; private set; }

        public float ReceivedBytesPerSecond { get; private set; }

        public short SendersTimeMs { get; private set; }

        public short ReceiversLastTimePlusDelay { get; private set; }

        private Feedback()
        {
            // For IParseable deserialization.
        }

        public Feedback(ushort lossEventRate, float receivedBytesPerSecond, short sendersTimeMs, short receiversLastTimePlusDelay)
        {
            this.LossEventRate = lossEventRate;
            this.ReceivedBytesPerSecond = receivedBytesPerSecond;
            this.SendersTimeMs = sendersTimeMs;
            this.ReceiversLastTimePlusDelay = receiversLastTimePlusDelay;
        }

        public void ToMessage(MessageBuffer message, Type parameterType)
        {
            message.Write(this.LossEventRate);
            message.Write(this.ReceivedBytesPerSecond);
            message.Write(this.SendersTimeMs);
            message.Write(this.ReceiversLastTimePlusDelay);
        }

        public void FromMessage(MessageBuffer message)
        {
            this.LossEventRate = message.ReadUShort();
            this.ReceivedBytesPerSecond = message.ReadFloat();
            this.SendersTimeMs = message.ReadShort();
            this.ReceiversLastTimePlusDelay = message.ReadShort();
        }
    }

    /// <summary>
    /// Measures packet loss by checking for missing sequence numbers in the set of incoming packets received.
    /// Based on RFC 5348: TCP-Friendly Rate Control.
    /// </summary>
    /// 
    /// <remarks>
    /// During normal operation the LossEventRate will be small but non-zero because previous loss events are
    /// never completely forgotten.  This means that the connection will be limited by either the receive
    /// rate or by XBps.  XBps = (s / R) * f(p).  s - segment size, R - RTT, p - loss event rate.  i.e.
    /// f(p) calculates the number of segments we can send per RTT according to the loss rate.  A high RTT,
    /// a low segment size, or a high loss rate will cause the connection to be limited by XBps.  For our
    /// system we fix s, so only RTT and p affect the mode of operation.  It is important that RTT is accurate
    /// otherwise we will incorrectly decrease our outbound rate on the assumption of large feedback delay.
    /// 
    /// We cannot detected dropped resends of reliable messages; however, we assume that these messages form only a
    /// small fraction of all messages sent.  Original reliable messages are treated in the same manner as unreliable
    /// messages; a dropped original reliable message will be detected so long as three newer messages have been received
    /// before the resend arrives.  Overall this should give a fairly useful/accurate estimate of the loss rate on the link.
    /// </remarks>
    class TcpFriendlyRateControl : ICongestionControl
    {
        public enum Mode
        {
            BeforeFeedback,
            Initial,
            ReceiveLimit,
            Xbps,
            Doubled,
            NoFeedbackHalved
        }

        /// <summary>
        /// The estimate average segment size in bytes. It is set to 512 bytes at the moment, but 
        /// it might be better if the average segment size is smaller.
        /// </summary>
        internal const int AverageSegmentSize = 512;

        private const double MinimumSendRate = Parameters.MinimumSendRate;
        private const int NDUPACK = 3;
        private const int LossIntervalHistorySize = 8;
        private static readonly double[] LossIntervalWeights;
        private const double DiscountFactorThreshold = 0.25;

        static TcpFriendlyRateControl()
        {
            LossIntervalWeights = new double[LossIntervalHistorySize];
            for (int i = 0; i < LossIntervalWeights.Length; i++)
            {
                if (i < LossIntervalHistorySize / 2)
                {
                    LossIntervalWeights[i] = 1.0;
                }
                else
                {
                    LossIntervalWeights[i] = 2.0 * (LossIntervalHistorySize - i) / (LossIntervalHistorySize + 2);
                }
            }
        }


        public double SendLimitBytesPerSecond
        {
            get { return this.mSendLimitBytesPerSecond; }
        }

        public Mode LimitMode { get; private set; }

        public double LossEventRate { get; private set; }
        public double RemoteLossEventRate { get; private set; }


        private ITcpFriendlyRateControlDelegate mDelegateInterface;

        private SequenceNumber mLastInorder;
        private SequenceNumber mHighestReceived;
        private TimeSpan mLastInorderTime;

        // Range of sequence numbers in the sorted list at any time should be fairly small, so the wrap-around shouldn't
        // affect the sorting algorithm.
        // TODO: Could probably turn this into an array because it shouldn't ever have more than NDUPACK elements
        SortedList<SequenceNumber, TimeSpan> mOutOfOrderArrivals = new SortedList<SequenceNumber, TimeSpan>();

        private TimeSpan mLastLossEventTime;
        private bool HadLossEvent { get { return this.mLastLossEventTime != new TimeSpan(); } }
        private SequenceNumber mLastLossEvent;

        private uint[] mLossIntervalHistory;
        private int mLossIntervalHistoryHead;
        private int mLossIntervalHistoryCount;
        private double[] mDiscountFactors;
        private double mCurrentDiscountFactor;

        // From the spec it seems like the LatestLossInterval (I_0) should be calculated as
        // this.mLastLossEvent.DistanceTo(this.mHighestReceived), but in cases of high
        // loss this significantly overestimates the loss interval because it includes
        // missing segments that are probably lost.  To prevent estimating a low loss
        // rate in this case I've changed it to use DistanceTo(mLastInorder).  This may
        // overestimate the loss rate in the presence of reordering, but this should be
        // a smaller error and should not cause the algorithm to be less fair.
        private uint LatestLossInterval { get { return this.mLastLossEvent.DistanceTo(this.mLastInorder); } }

        private SortedList<TimeSpan, float> mReceiveRateSet = new SortedList<TimeSpan, float>();
        private TimeSpan mTimeoutInterval;
        private double mAverageSegmentSize;
        private double mSendLimitBytesPerSecond;

        private TimeSpan mTimeLastDoubled;

        private OneShotTask mNoFeedbackTimer;
        private bool mSenderIdleSinceTimerSet;
        private bool mGotRttOrFeedback;
        private TimeSpan mLastCongestionFeedbackSent;
        private int mBytesSinceLastFeedback;

        private bool feedbackOutstanding;

        private IRttRateEstimator mRttRateEstimator;

        private TimeSpan mLastCongestionFeedbackReceived;
        private short mLastTimestampReceived;

        private double InitialRate
        {
            get
            {
                if (this.mDelegateInterface.RoundTripTimeEstimateMs > 0)
                {
                    double wInit = Math.Min(4 * Parameters.MaximumPacketSize, Math.Max(2 * Parameters.MaximumPacketSize, 4380));
                    return wInit * 1000 / this.mDelegateInterface.RoundTripTimeEstimateMs;
                }

                return Parameters.MaximumPacketSize;
            }
        }

        private double XBps
        {
            get
            {
                double s = this.mAverageSegmentSize;
                double R = this.mDelegateInterface.RoundTripTimeEstimateMs / 1000;
                double p = this.LossEventRate;

                return s / (R * (Math.Sqrt(2 * p / 3) + 12 * Math.Sqrt(3 * p / 8) * p * (1 + 32 * p * p)));
            }
        }


        public TcpFriendlyRateControl(ITcpFriendlyRateControlDelegate delegateInterface, IRttRateEstimator rttRateEstimator)
        {
            this.mDelegateInterface = delegateInterface;
            this.mRttRateEstimator = rttRateEstimator;
            this.mNoFeedbackTimer = new OneShotTask(this.mDelegateInterface.EventQueue);
            this.Reset();
        }

        public void Shutdown()
        {
            this.mNoFeedbackTimer.Cancel();
        }
                
        public void Reset()
        {
            this.SetRateLimit(Mode.BeforeFeedback, Parameters.MaximumPacketSize);

            this.LossEventRate = 0;
            this.RemoteLossEventRate = 0;

            this.mLastInorder = new SequenceNumber();
            this.mHighestReceived = new SequenceNumber();
            this.mLastInorderTime = new TimeSpan();

            this.mOutOfOrderArrivals.Clear();

            this.mLastLossEventTime = new TimeSpan();
            this.mLastLossEvent = new SequenceNumber();

            this.mLossIntervalHistory = new uint[LossIntervalHistorySize];
            this.mLossIntervalHistoryHead = 0;
            this.mLossIntervalHistoryCount = 0;

            this.mTimeoutInterval = new TimeSpan();

            this.mAverageSegmentSize = TcpFriendlyRateControl.AverageSegmentSize;  // TODO: Calculate the correct average

            this.mTimeLastDoubled = new TimeSpan();

            this.mNoFeedbackTimer.Cancel();

            this.mSenderIdleSinceTimerSet = false;
            this.mGotRttOrFeedback = false;

            this.mLastCongestionFeedbackSent = TimeSpan.FromSeconds(-1);  // initial sentinel
            this.mBytesSinceLastFeedback = 0;

            this.mReceiveRateSet.Clear();
            this.mReceiveRateSet[this.mDelegateInterface.Now] = float.PositiveInfinity;

            this.mDiscountFactors = new double[LossIntervalHistorySize];
            for (int i = 0; i < this.mDiscountFactors.Length; i++)
            {
                this.mDiscountFactors[i] = 1;
            }
            this.mCurrentDiscountFactor = 1;
        }

        private void SetRateLimit(Mode mode, double value)
        {
            this.mSendLimitBytesPerSecond = Math.Max(MinimumSendRate, value);
            this.LimitMode = mode;
        }

        public void ReceivedInitial(SequenceNumber sequenceNumber, int length)
        {
            this.mHighestReceived = sequenceNumber;
            this.mLastInorder = sequenceNumber;
            this.mLastInorderTime = this.mDelegateInterface.Now;
            this.ScheduleFeedback(true, length);
        }

        public void Received(SequenceNumber sequenceNumber, int length, bool feedbackOnly)
        {
            if (sequenceNumber > this.mHighestReceived)
            {
                this.mHighestReceived = sequenceNumber;
            }

            if (sequenceNumber <= this.mLastInorder)
            {
                // Probably old duplicate, ignore
                return;
            }

            if (sequenceNumber.Previous == this.mLastInorder)
            {
                // The hopefully normal case where sequence numbers just arrive in order
                this.mLastInorder = sequenceNumber;

                // TODO: Technically I think the calculation of which packets are part of a given
                //       loss event is supposed to be based on the send time (which should be
                //       included in each packet), not the receive time.
                this.mLastInorderTime = this.mDelegateInterface.Now;
            }
            else
            {
                this.mOutOfOrderArrivals[sequenceNumber] = this.mDelegateInterface.Now;
            }

            double oldLossRate = this.RemoteLossEventRate;
            this.SummarizeAndCheckForLosses();

            if (!feedbackOnly)
            {
                this.ScheduleFeedback(this.RemoteLossEventRate > oldLossRate, length);
            }
        }

        public void Sent(int length)
        {
            this.mSenderIdleSinceTimerSet = false;
        }

        private void SummarizeOutOfOrders()
        {
            SequenceNumber next = this.mLastInorder;
            next.Increment();
            while (this.mOutOfOrderArrivals.Count > 0 && this.mOutOfOrderArrivals.Keys[0] == next)
            {
                this.mLastInorder = next;
                this.mLastInorderTime = this.mOutOfOrderArrivals.Values[0];
                this.mOutOfOrderArrivals.Remove(next);
                next.Increment();
            }
        }

        private void SummarizeAndCheckForLosses()
        {
            this.SummarizeOutOfOrders();
            while (this.mOutOfOrderArrivals.Count >= NDUPACK)  // Got enough following packets to mark some as lost
            {
                // TODO: May need to ensure that the time used for interpolation is greater than the last in-order time.
                //       Worst that will happen is that we may treat a loss as part of a previous loss event when
                //       it should really start a new loss event; and so we may underestimate the loss rate a bit
                //       in the presence of heavy packet reordering.
                SequenceNumber nextGoodArrival = this.mOutOfOrderArrivals.Keys[0];

                double b = this.mLastInorder.DistanceTo(nextGoodArrival);
                double tb = this.mOutOfOrderArrivals.Values[0].TotalMilliseconds - this.mLastInorderTime.TotalMilliseconds;

                SequenceNumber lost = this.mLastInorder;
                lost.Increment();
                while (lost != nextGoodArrival)
                {
                    double a = this.mLastInorder.DistanceTo(lost);
                    TimeSpan estimatedLostTime = this.mLastInorderTime + TimeSpan.FromMilliseconds(tb * a / b);

                    if (!this.HadLossEvent ||
                        estimatedLostTime > this.mLastLossEventTime + TimeSpan.FromMilliseconds(this.mDelegateInterface.RoundTripTimeEstimateMs))
                    {
                        // We have a lost packet at least one RTT later than the last lost packet, treat it as a separate
                        // loss event.

                        if (this.HadLossEvent)
                        {
                            this.mLossIntervalHistory[this.mLossIntervalHistoryHead] = this.mLastLossEvent.DistanceTo(lost);
                            for (int i = 0; i < this.mDiscountFactors.Length; i++)
                            {
                                this.mDiscountFactors[i] *= this.mCurrentDiscountFactor;
                            }
                            this.mDiscountFactors[this.mLossIntervalHistoryHead] = this.mCurrentDiscountFactor;
                            this.mCurrentDiscountFactor = 1;

                            this.mLossIntervalHistoryCount = Math.Min(this.mLossIntervalHistoryCount + 1, LossIntervalHistorySize);
                            this.mLossIntervalHistoryHead = (this.mLossIntervalHistoryHead + 1) % LossIntervalHistorySize;
                        }
                        else
                        {
                            // TODO: Implement Section 6.3.1 for estimating the initial loss event rate
                        }

                        this.mLastLossEvent = lost;
                        this.mLastLossEventTime = estimatedLostTime;
                    }
                    lost.Increment();
                }

                this.mLastInorder = nextGoodArrival;
                this.mLastInorderTime = this.mOutOfOrderArrivals.Values[0];
                this.mOutOfOrderArrivals.Remove(nextGoodArrival);
                this.SummarizeOutOfOrders();
            }

            this.UpdateDiscountFactor();
            this.UpdateLossEventRate();
        }

        private void UpdateLossEventRate()
        {
            if (!this.HadLossEvent)
            {
                return;
            }

            int k = this.mLossIntervalHistoryCount;

            if (k == 0)
            {
                return;   // If the "initial loss history" calculation was implemented this wouldn't be required.
            }

            int historyStartIndex = -1;
            if (this.mLossIntervalHistoryCount == LossIntervalHistorySize)
            {
                historyStartIndex = this.mLossIntervalHistoryHead;
            }

            double iTot0 = this.LatestLossInterval * LossIntervalWeights[0];
            double wTot0 = LossIntervalWeights[0];

            double iTot1 = 0;
            double wTot1 = 0;
            double w;

            for (int i = 1; i <= k; i++)
            {
                int index = (historyStartIndex + i) % LossIntervalHistorySize;

                if (i < k)
                {
                    w = LossIntervalWeights[i] * this.mDiscountFactors[index] * this.mCurrentDiscountFactor;
                    iTot0 += this.mLossIntervalHistory[index] * w;
                    wTot0 += w;
                }

                w = LossIntervalWeights[i - 1] * this.mDiscountFactors[index];
                iTot1 += this.mLossIntervalHistory[index] * w;
                wTot1 += w;
            }

            this.RemoteLossEventRate = Math.Min(wTot0 / iTot0, wTot1 / iTot1);
        }

        private void UpdateDiscountFactor()
        {
            if (this.mLossIntervalHistoryCount == 0)
            {
                return;
            }

            int historyStartIndex = -1;
            if (this.mLossIntervalHistoryCount == LossIntervalHistorySize)
            {
                historyStartIndex = this.mLossIntervalHistoryHead;
            }

            double iTot = 0;
            double wTot = 0;

            for (int i = 1; i <= this.mLossIntervalHistoryCount; i++)
            {
                int index = (historyStartIndex + i) % LossIntervalHistorySize;

                double w = LossIntervalWeights[i - 1] * this.mDiscountFactors[index];
                iTot += this.mLossIntervalHistory[index] * w;
                wTot += w;
            }

            double iMean = iTot / wTot;

            if (this.LatestLossInterval > 2 * iMean)
            {
                this.mCurrentDiscountFactor = Math.Max(DiscountFactorThreshold, 2 * iMean / this.LatestLossInterval);
            }
            else
            {
                this.mCurrentDiscountFactor = 1;
            }
        }

        private float MaximizeReceiveSet(float newSample)
        {
            foreach (float receiveRate in this.mReceiveRateSet.Values)
            {
                if (!float.IsInfinity(receiveRate))
                {
                    newSample = Math.Max(newSample, receiveRate);
                }
            }

            this.mReceiveRateSet.Clear();
            this.mReceiveRateSet[this.mDelegateInterface.Now] = newSample;
            return newSample;
        }

        private float UpdateReceiveSet(float newSample)
        {
            TimeSpan now = this.mDelegateInterface.Now;
            TimeSpan expired = now - TimeSpan.FromMilliseconds(2 * this.mDelegateInterface.RoundTripTimeEstimateMs);

            while (this.mReceiveRateSet.Count > 0 && this.mReceiveRateSet.Keys[0] < expired)
            {
                this.mReceiveRateSet.RemoveAt(0);
            }

            float max = newSample;
            for (int i = 0; i < this.mReceiveRateSet.Count; i++)
            {
                max = Math.Max(max, this.mReceiveRateSet.Values[i]);
            }

            this.mReceiveRateSet[now] = newSample;

            return max;
        }

        private void ScheduleFeedback(bool forceSend, int length)
        {
            // Need to check this before incrementing mPacketsSinceLastFeedback because
            // mFeedbacksSinceLastFeedback is only incremented *after* we're called (as
            // a result of the parsing process).  So we're really counting slightly
            // separate things, and will send an extra feedback packet as a result before
            // going quiet; but! this is what we want because we need to exchange our
            // RTT estimate (i.e. the first congestion feedback packet should trigger one
            // feedback in response, but the second shouldn't unless there has been some
            // other traffic too).

            this.mBytesSinceLastFeedback += length;

            TimeSpan timeSinceLastFeedback = this.mDelegateInterface.Now - this.mLastCongestionFeedbackSent;
            bool sendFeedbackTimeoutExpired = timeSinceLastFeedback.TotalMilliseconds > this.mDelegateInterface.RoundTripTimeEstimateMs;
            bool haventSentFeedback = this.mLastCongestionFeedbackSent < TimeSpan.Zero;
            bool receiveRateWillBeZero = this.mBytesSinceLastFeedback == 0; // Generally don't want to send in this case because it'll choke the sender

            if ((forceSend || sendFeedbackTimeoutExpired) && (!receiveRateWillBeZero || haventSentFeedback))
            {
                this.feedbackOutstanding = true;
                this.mDelegateInterface.ScheduleFeedback(Parameters.CongestionFeedbackDelay);
            }
        }

        /// <summary>
        /// Gets outstanding feedback data that needs to be sent.
        /// </summary>
        /// <returns>The outstanding feedback data, or <c>null</c> if no feedback is outstanding.</returns>
        public Feedback GetFeedback()
        {
            if (!this.feedbackOutstanding)
            {
                return null;
            }
            
            // OK, it is time to actually dispatch the feedback!
            TimeSpan now = this.mDelegateInterface.Now;

            float receivedBytesPerSecond = 0;
            if (this.mLastCongestionFeedbackSent >= TimeSpan.Zero)
            {
                // If we've had some RTTs where we didn't get any packets (and thus didn't send
                // feedback) we have to make sure we only use the last RTT for calculating the
                // receive rate.  Otherwise we report an artificially low receive rate which can
                // choke the sender.
                // If we received packets then we will have sent feedback, so this is still reasonably
                // accurate in terms of the bytes counted (although now with piggybacking of feedback
                // information, the feedback message may have been delayed, which affects the calculation
                // somewhat).
                // TODO: Actually keep track of bytes received in the last RTT period rather than trying to guess.

                double rttSeconds = this.mDelegateInterface.RoundTripTimeEstimateMs / 1000;
                double elapsed = Math.Min((now - this.mLastCongestionFeedbackSent).TotalSeconds, rttSeconds);
                receivedBytesPerSecond = (float)(this.mBytesSinceLastFeedback / elapsed);
            }

            short myTimeMs = (short)(now.TotalMilliseconds % short.MaxValue);
            int delayMs = (int)(now - this.mLastCongestionFeedbackReceived).TotalMilliseconds;
            short theirDelayedTime = 0;
            if (delayMs < short.MaxValue - 5000 &&  // Don't want to send a sample if it's been delayed excessively (i.e. too close to short.MaxValue [5000 is allowance for up to 5 seconds of actual RTT], so the subtraction on the other machine may wrap incorrectly) because then the mod arithmetic is not valid
                this.mLastTimestampReceived != 0)  // 0 is the intial value before we've received a timestamp.  It may occur during normal processing, but not frequently so it shouldn't affect the estimate much if we drop those samples.
            {
                theirDelayedTime = (short)((this.mLastTimestampReceived + delayMs) % short.MaxValue);
            }

            var feedback = new Feedback(
                (ushort)(this.RemoteLossEventRate * ushort.MaxValue),
                receivedBytesPerSecond,
                myTimeMs,
                theirDelayedTime);

            this.mLastCongestionFeedbackSent = now;
            this.mBytesSinceLastFeedback = 0;
            this.feedbackOutstanding = false;

            return feedback;
        }

        public void CongestionFeedback(Feedback feedback, TimeSpan arrivalTime)
        {
            TimeSpan now = this.mDelegateInterface.Now;

            // Update RTT estimate
            if (feedback.ReceiversLastTimePlusDelay != 0)
            {
                short returnTimeMs = (short)(arrivalTime.TotalMilliseconds % short.MaxValue);
                Debug.Assert(feedback.ReceiversLastTimePlusDelay >= 0 && feedback.ReceiversLastTimePlusDelay < short.MaxValue);
                int rtt = (returnTimeMs + (short.MaxValue - feedback.ReceiversLastTimePlusDelay)) % short.MaxValue;
                this.mRttRateEstimator.UpdateEstimate(TimeSpan.FromMilliseconds(rtt));
                Logger.TraceInformation(LogTag.Connection, "TFRC: New RTT sample of {0}ms (return time = {1}, send time = {2})", rtt, returnTimeMs, feedback.ReceiversLastTimePlusDelay);
            }
            this.mLastTimestampReceived = feedback.SendersTimeMs;
            this.mLastCongestionFeedbackReceived = arrivalTime;

            if (!this.mGotRttOrFeedback)
            {
                this.mTimeLastDoubled = now;
                this.SetRateLimit(Mode.Initial, this.InitialRate);
                this.mGotRttOrFeedback = true;
                Logger.TraceInformation(LogTag.Connection, "TFRC: Send Limit = Initial ({0:F1} kB/s)", this.SendLimitBytesPerSecond / 1024);
            }

            double newLossEventRate = (double)feedback.LossEventRate / ushort.MaxValue;

            Logger.TraceInformation(LogTag.Connection, "TFRC: Got congestion feedback: loss = {0:F3} ({1}), recv = {2:F1}kB/s", newLossEventRate, feedback.LossEventRate,
                feedback.ReceivedBytesPerSecond / 1024);

            bool lossIncreased = newLossEventRate > this.LossEventRate;
            this.LossEventRate = newLossEventRate;

            this.mTimeoutInterval = TimeSpan.FromSeconds(
                Math.Max(4 * this.mDelegateInterface.RoundTripTimeEstimateMs / 1000,
                         2 * this.mAverageSegmentSize / this.SendLimitBytesPerSecond));

            this.UpdateSendRate(lossIncreased, feedback.ReceivedBytesPerSecond);

            this.StartNoFeedbackTimer(this.mTimeoutInterval.TotalSeconds);
        }

        private void UpdateSendRate(bool lossIncreased, float receivedBytesPerSecond)
        {
            TimeSpan now = this.mDelegateInterface.Now;

            double receiveLimit;

            // This if-statement represents the predicate "the entire interval covered by the feedback packet
            // was a data-limited interval".  It's not a terribly faithful implementation because SentBytesPerSecond
            // averages over a much longer period than the last feedback packet.  Hopefully this won't result
            // in unnecessary reduction of the receive limit.
            if (this.mDelegateInterface.SentBytesPerSecond < this.SendLimitBytesPerSecond)
            {
                if (lossIncreased)
                {
                    for (int i = 0; i < this.mReceiveRateSet.Count; i++)
                    {
                        this.mReceiveRateSet[this.mReceiveRateSet.Keys[i]] *= 0.5f;  // TODO: Is there a better way to do this?
                    }
                    receiveLimit = this.MaximizeReceiveSet(receivedBytesPerSecond * 0.85f);
                    Logger.TraceInformation(LogTag.Connection, "TFRC: Data-limited and loss increased");
                }
                else
                {
                    receiveLimit = 2 * this.MaximizeReceiveSet(receivedBytesPerSecond);
                    Logger.TraceInformation(LogTag.Connection, "TFRC: Data-limited");
                }
            }
            else
            {
                receiveLimit = 2 * this.UpdateReceiveSet(receivedBytesPerSecond);
                Logger.TraceInformation(LogTag.Connection, "TFRC: Not data-limited");
            }


            if (this.LossEventRate > 0)
            {
                double xbps = this.XBps;
                if (receiveLimit < xbps)
                {
                    this.SetRateLimit(Mode.ReceiveLimit, receiveLimit);
                    Logger.TraceInformation(LogTag.Connection, "TFRC: Send Limit = Receive Limit ({0:F1} kB/s)", this.SendLimitBytesPerSecond / 1024);
                }
                else
                {
                    this.SetRateLimit(Mode.Xbps, xbps);
                    Logger.TraceInformation(LogTag.Connection, "TFRC: Send Limit = XBps ({0:F1} kB/s)", this.SendLimitBytesPerSecond / 1024);
                }
            }
            else if ((now - this.mTimeLastDoubled).TotalMilliseconds >= this.mDelegateInterface.RoundTripTimeEstimateMs)
            {
                this.mTimeLastDoubled = now;
                //this.SendLimitBytesPerSecond = Math.Max(Math.Min(2 * this.SendLimitBytesPerSecond, receiveLimit), this.InitialRate);
                double doubled = 2 * this.SendLimitBytesPerSecond;
                double initial = this.InitialRate;

                if (initial > Math.Min(doubled, receiveLimit))
                {
                    this.SetRateLimit(Mode.Initial, initial);
                    Logger.TraceInformation(LogTag.Connection, "TFRC: Send Limit = No Loss Initial ({0:F1} kB/s)", this.SendLimitBytesPerSecond / 1024);
                }
                else if (doubled < receiveLimit)
                {
                    this.SetRateLimit(Mode.Doubled, doubled);
                    Logger.TraceInformation(LogTag.Connection, "TFRC: Send Limit = No Loss Doubled ({0:F1} kB/s)", this.SendLimitBytesPerSecond / 1024);
                }
                else
                {
                    this.SetRateLimit(Mode.ReceiveLimit, receiveLimit);
                    Logger.TraceInformation(LogTag.Connection, "TFRC: Send Limit = No Loss Receive Limit ({0:F1} kB/s)", this.SendLimitBytesPerSecond / 1024);
                }
            }
        }

        private void StartNoFeedbackTimer(double seconds)
        {
            seconds = Math.Max(0.1, seconds);  // not sure yet if this improves things
            this.mSenderIdleSinceTimerSet = true;
            this.mNoFeedbackTimer.Trigger(TimeSpan.FromSeconds(seconds), Apply.Func(this.NoFeedbackTimerElapsed));
        }

        private void NoFeedbackTimerElapsed()
        {
            float xRecv = 0;
            for (int i = 0; i < this.mReceiveRateSet.Count; i++)
            {
                xRecv = Math.Max(xRecv, this.mReceiveRateSet.Values[i]);
            }

            double recoverRate = this.InitialRate;

            if (!this.mGotRttOrFeedback && !this.mSenderIdleSinceTimerSet)
            {
                this.SetRateLimit(Mode.NoFeedbackHalved, this.SendLimitBytesPerSecond * 0.5);
                Logger.TraceInformation(LogTag.Connection, "TFRC: Send Limit = No Feedback Halved ({0:F1} kB/s)", this.SendLimitBytesPerSecond / 1024);
            }
            else if (this.mSenderIdleSinceTimerSet &&
                ((this.LossEventRate > 0 && xRecv < recoverRate) ||
                 (this.LossEventRate == 0 && this.SendLimitBytesPerSecond < 2 * recoverRate)))
            {
                // Don't halve the rate in this case
            }
            else if (this.LossEventRate == 0)
            {
                this.SetRateLimit(Mode.NoFeedbackHalved, this.SendLimitBytesPerSecond * 0.5);
                Logger.TraceInformation(LogTag.Connection, "TFRC: Send Limit = No Feedback Halved ({0:F1} kB/s)", this.SendLimitBytesPerSecond / 1024);
            }
            else if (this.XBps > 2 * xRecv)
            {
                this.UpdateLimits(xRecv);
            }
            else
            {
                this.UpdateLimits(this.XBps / 2);
            }

            double timerSeconds = Math.Max(4 * this.mDelegateInterface.RoundTripTimeEstimateMs / 1000, 2 * this.mAverageSegmentSize / this.SendLimitBytesPerSecond);
            this.StartNoFeedbackTimer(timerSeconds);
        }

        private void UpdateLimits(double timerLimit)
        {
            timerLimit = Math.Max(timerLimit, MinimumSendRate);
            this.mReceiveRateSet.Clear();
            this.mReceiveRateSet[this.mDelegateInterface.Now] = (float)(timerLimit / 2);
            this.UpdateSendRate(false, (float)(timerLimit / 2));
        }
    }
}
