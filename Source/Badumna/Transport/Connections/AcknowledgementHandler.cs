﻿using System;
using System.Collections.Generic;
using System.Text;

using Badumna.Core;
using Badumna.Transport.Connections;
using Badumna.Utilities;
using System.Diagnostics;
using SequenceNumber = Badumna.Utilities.CyclicalID.UShortID;

namespace Badumna.Transport
{
    internal delegate void SendDedicatedAckEnvelope(QualityOfService qos, PeerAddress address);

    class AcknowledgementHandler
    {
        class ResendMessageEvent : NetworkEvent
        {
            public ConnectionHeader Header;
            public TransportEnvelope MessageEnvelope;

            private NetworkEventQueue eventQueue;

            public ResendMessageEvent(
                ConnectionHeader header,
                TransportEnvelope envelope,
                GenericCallBack<TransportEnvelope> callback,
                NetworkEventQueue eventQueue)
                : base(new TimeSpan(), Apply.Func(callback, envelope))
            {
                this.Header = header;
                this.MessageEnvelope = envelope;
                this.Name = "Resend";
                this.eventQueue = eventQueue;
            }

            public void SendFailed(PeerAddress peersPublicAddress)
            {
                // Handle failure in an event becuase it may result in resending a message which might affect
                // the current enumeration of the mPendingAcknowledgements collection.
                this.eventQueue.Push(
                    delegate()
                    {
                        this.MessageEnvelope.Qos.HandleFailure(peersPublicAddress, -1);// -1 indicates will not try again
                    });
            }

            public void ConnectionFailed()
            {
                this.eventQueue.Push(
                    delegate()
                    {
                        this.MessageEnvelope.Qos.HandleConnectionFailure(this.MessageEnvelope.Destination);
                    });
            }
        }

        class AcknowledgementState
        {
            public ResendMessageEvent ResendEvent;
            public GenericCallBack OnAcknowledgmentAction;
            public SequenceNumber SequenceNumber;

            public AcknowledgementState(SequenceNumber sequenceNumber)
            {
                this.SequenceNumber = sequenceNumber;
            }
        }

        /// <summary>
        /// Used to send a message when there are acks waiting to be sent and there's no other
        /// traffic for them to be piggybacked on.
        /// </summary>
        private CommonTask delayedMessageSender;

        private Dictionary<SequenceNumber, AcknowledgementState> mPendingAcknowledgements = new Dictionary<SequenceNumber, AcknowledgementState>();
        private List<SequenceNumber> mAcksToSend = new List<SequenceNumber>();

        private GenericCallBackReturn<PeerAddress> mGetPeersPublicAddress;
        private PeerAddress PeersPublicAddress { get { return this.mGetPeersPublicAddress(); } }

        /// <summary>
        /// The queue to use for scheduling events.
        /// </summary>
        private NetworkEventQueue eventQueue;

        public AcknowledgementHandler(
            GenericCallBackReturn<PeerAddress> getPeersPublicAddress,
            CommonTask delayedMessageSender,
            NetworkEventQueue eventQueue)
        {
            if (delayedMessageSender == null)
            {
                throw new ArgumentNullException("delayedMessageSender");
            }

            this.mGetPeersPublicAddress = getPeersPublicAddress;
            this.delayedMessageSender = delayedMessageSender;

            this.eventQueue = eventQueue;
        }

        // TODO: This could do with some refactoring
        public void ResetAcksToSendAfterInitialMessage()
        {
            lock (this.mAcksToSend)
            {
                // Clear old acks from mAcksToSend (but don't remove the pending ack for the initial message just received)
                // We can do this because they have just been queued.
                if (this.mAcksToSend.Count > 0)
                {
                    SequenceNumber initialMessageNumber = this.mAcksToSend[this.mAcksToSend.Count - 1];
                    this.mAcksToSend.Clear();
                    this.mAcksToSend.Add(initialMessageNumber);
                }
            }
        }

        public void Shutdown()
        {
            if (this.mPendingAcknowledgements.Count > 0)
            {
                Logger.TraceWarning(LogTag.Connection, "Closing connection to {0} with {1} outstanding acknowledgements", this.PeersPublicAddress, this.mPendingAcknowledgements.Count);

                this.CancelResendEvents(
                    delegate(AcknowledgementState ackState)
                    {
                        ackState.ResendEvent.ConnectionFailed();
                        return true;
                    });
            }

            this.mAcksToSend.Clear();
        }

        private void CancelResendEvents(GenericCallBackReturn<bool, AcknowledgementState> shouldCancel)
        {
            shouldCancel = shouldCancel ?? (a => true);

            // Remove all the resend events from the given connection and put in send queue for this connection.
            foreach (AcknowledgementState ackState in this.mPendingAcknowledgements.Values)
            {
                if (ackState.OnAcknowledgmentAction != null)
                {
                    Logger.TraceInformation(LogTag.Connection, "Removing acknowledgement action for message {0}", ackState.SequenceNumber);
                }

                if (ackState.ResendEvent != null && shouldCancel(ackState))
                {
                    this.eventQueue.Remove(ackState.ResendEvent);
                }
            }

            this.mPendingAcknowledgements.Clear();
        }

        public void CancelResendEvents()
        {
            this.CancelResendEvents(false);
        }

        public void CancelResendEvents(bool expectingReplyToInitialMessage)
        {
            Dictionary<SequenceNumber, AcknowledgementState> newAckStates = new Dictionary<SequenceNumber, AcknowledgementState>();

            this.CancelResendEvents(
                delegate(AcknowledgementState ackState)
                {
                    if (!ackState.ResendEvent.Header.IsInitial)
                    {
                        ackState.ResendEvent.SendFailed(this.PeersPublicAddress);
                        return true;
                    }

                    if (!expectingReplyToInitialMessage)
                    {
                        return true;
                    }

                    // Keep the resend event for this message for expected initialization replies. We need to do this since the 
                    // request may have originated from a hole punch notification in which case the sequence number and connectionId
                    // may not correlate to the same message.
                    newAckStates.Add(ackState.SequenceNumber, ackState);

                    Logger.TraceInformation(LogTag.Connection, "Keeping inital message to {0} in resend queue.", this.PeersPublicAddress);
                    return false;
                });

            Logger.TraceInformation(LogTag.Connection, "DROPPING {0} unconfirmed reliable messages until connection is established.",
                this.mPendingAcknowledgements.Count - newAckStates.Count);
            this.mPendingAcknowledgements = newAckStates;
        }

        /// <summary>
        /// WARNING! Make sure your sequence number is valid!  SendMessage does not set a sequence
        /// number.  Use FinalizeMessage directly instead of SendMessage if you require knowledge
        /// of the sequence number.  SendMessage routes the message via the scheduler whereas
        /// FinalizeMessage dispatches it directly to the rate limiter after assigning a seq num, etc.
        /// </summary>
        /// <param name="sequenceNumber"></param>
        /// <param name="action"></param>
        public void SetOnAcknowledgementAction(SequenceNumber sequenceNumber, GenericCallBack action)
        {
            AcknowledgementState ackState;
            if (this.mPendingAcknowledgements.TryGetValue(sequenceNumber, out ackState))
            {
                if (ackState.OnAcknowledgmentAction != null)
                {
                    Logger.TraceWarning(LogTag.Connection, "Overriding on acknowledgement action for message {0}", sequenceNumber);
                }
            }
            else
            {
                ackState = new AcknowledgementState(sequenceNumber);
                this.mPendingAcknowledgements.Add(sequenceNumber, ackState);
            }

            ackState.OnAcknowledgmentAction = action;
        }

        public bool WriteAcknowledgments(TransportEnvelope envelope)
        {
            MessageBuffer message = envelope.Message;

            Debug.Assert(!envelope.Header.ContainsAcknowledgements, "Already written acks!");
            Debug.Assert(!envelope.Message.HasBeenChecksummed, "Message has been checksumed before writing acks!");
            
            int spaceLeft = Parameters.MaximumPayloadSize - envelope.Length;
            int totalSlots = (spaceLeft - 1) / 2;  // 1 byte for count, 2 bytes per slot.
            totalSlots = Math.Min(totalSlots, byte.MaxValue);  // Limit of count byte

            lock (this.mAcksToSend)
            {
                if (this.mAcksToSend.Count == 0 || totalSlots <= 0)
                {
                    return false;
                }

                int ackCount = 0;
                for (; ackCount < this.mAcksToSend.Count && ackCount < totalSlots; ackCount++)
                {
                    message.Write(this.mAcksToSend[ackCount], typeof(SequenceNumber));
                }
                message.Write((byte)ackCount);

                Debug.Assert(message.Length >= ConnectionHeader.Length + 1 + ackCount * 2, "Message is too short.");
                Debug.Assert(message.Length <= Parameters.MaximumPayloadSize, "Message is too long after writing acks");

                AcknowledgementHandler.TraceSentAcknowledgements(this.mAcksToSend, ackCount, envelope);
                this.mAcksToSend.RemoveRange(0, ackCount);

                // Cancels the dedicated ack envelope event, but only if there are no
                // pending acks to be sent. If the event is not cancelled when the queue
                // is empty, then the next ack added to the queue may be sent prematurely.
                if (this.mAcksToSend.Count == 0)
                {
                    this.delayedMessageSender.Cancel(this);
                }

                return true;
            }
        }

        /// <summary>
        /// Removes any attached acknowledgements from the given message, so that they do not get parsed.
        /// </summary>
        public static void DiscardAcknowledgments(TransportEnvelope envelope)
        {
            if (envelope == null || envelope.Header == null ||
                !envelope.Header.ContainsAcknowledgements || envelope.Length < 1)
            {
                return;
            }

            MessageBuffer message = envelope.Message;
            int initialReadOffset = message.ReadOffset;

            message.ReadOffset = message.Length - 1;
            int numberOfAcknowledgements = (int)message.ReadByte();
            int numberOfBytes = (numberOfAcknowledgements * 2) + 1;

            if (message.Length < numberOfBytes)
            {
                Debug.Assert(false, "Not enough bytes in message to read acknowledgements.");
                Logger.TraceError(LogTag.Connection, "Not enough bytes in message to read acknowledgements.");
                return;
            }

            message.DropLastNBytes(numberOfBytes);
            message.ReadOffset = initialReadOffset;

            envelope.Header.ContainsAcknowledgements = false;
        }

        /// <summary>
        /// Indicates whether the acknowledgement handler is currently awaiting an ack for the
        /// given sequence number.
        /// </summary>
        /// <param name="sequenceNumber">The sequence number to check.</param>
        /// <returns>True iff the ack is awaited</returns>
        public bool IsAwaitingAcknowledgement(SequenceNumber sequenceNumber)
        {
            return this.mPendingAcknowledgements.ContainsKey(sequenceNumber);
        }

        [Conditional("TRACE")]
        private static void TraceSentAcknowledgements(List<SequenceNumber> acknowledgements, int count, TransportEnvelope envelope)
        {
            if (count == 0)
            {
                return;
            } 
            
            StringBuilder builder = new StringBuilder();

            for (int i = 0; i < count; i++)
            {
                if (i > 0)
                {
                    builder.Append(", ");
                }

                builder.Append(acknowledgements[i]);
            }

            Logger.TraceInformation(LogTag.Connection, "Sending acknowledgements {0} to {1}", builder.ToString(), envelope.Destination);
#if TRACE
            envelope.AppendLabel(string.Format("[acks {0}]", builder.ToString()));
#endif
        }

        public bool ReadAcknowledgments(TransportEnvelope envelope)
        {
            if (envelope == null || envelope.Header == null || !envelope.Header.ContainsAcknowledgements)
            {
                return false;
            }

            MessageBuffer message = envelope.Message;

            if (message.Length < 1)
            {
                Debug.Assert(false, "Not enough bytes in message to read acknowledgements.");
                Logger.TraceError(LogTag.Connection, "Not enough bytes in message to read acknowledgements.");
                return false;
            }

            List<SequenceNumber> acknowledegments = new List<SequenceNumber>();
            int initialReadOffset = message.ReadOffset;

            message.ReadOffset = message.Length - 1;
            int numberOfAcknowledgements = (int)message.ReadByte();
            int numberOfBytes = (numberOfAcknowledgements * 2) + 1;

            if (message.Length < numberOfBytes)
            {
                Debug.Assert(false, "Not enough bytes in message to read acknowledgements.");
                Logger.TraceError(LogTag.Connection, "Not enough bytes in message to read acknowledgements.");
                return false;
            }

            message.ReadOffset = message.Length - numberOfBytes;
            for (int i = 0; i < numberOfAcknowledgements; i++)
            {
                acknowledegments.Add(message.Read<SequenceNumber>());
            }
            message.DropLastNBytes(numberOfBytes);
            message.ReadOffset = initialReadOffset;
            envelope.Header.ContainsAcknowledgements = false;

            Logger.TraceInformation(LogTag.Connection, "ACK: Received ack(s) {0} dest={1}", CollectionUtils.Join(acknowledegments, ", "), this.PeersPublicAddress);

            foreach (SequenceNumber sequenceNumber in acknowledegments)
            {
                this.ReceiveAcknowledgment(sequenceNumber);
            }

            return true;
        }

        private void ReceiveAcknowledgment(SequenceNumber ackNumber)
        {
            AcknowledgementState ackState = null;
            if (!this.mPendingAcknowledgements.TryGetValue(ackNumber, out ackState))
            {
                Logger.TraceInformation(LogTag.Connection, "Nothing known about message {0} to {1}", ackNumber, this.PeersPublicAddress);
                return;
            }

            this.eventQueue.Remove(ackState.ResendEvent);
            this.mPendingAcknowledgements.Remove(ackNumber);

            if (ackState.OnAcknowledgmentAction != null)
            {
                try
                {
                    Logger.TraceInformation(LogTag.Connection, "Performing acknowledgement action for message {0}", ackNumber);
                    ackState.OnAcknowledgmentAction.Invoke();
                }
                catch (Exception e)
                {
                    Logger.TraceException(LogLevel.Error, e, "Failed to perform acknowledgement action.");
                }
            }
        }

        public void ExpectAck(TimeSpan timeout, TransportEnvelope messageEnvelope, GenericCallBack<TransportEnvelope> retryAction)
        {
            // Push the resend event.
            GenericCallBack<TransportEnvelope> incrementAndRetry =
                e =>
                {
                    e.IncrementRetryCount();
                    retryAction(e);
                };

            ResendMessageEvent resendEvent = new ResendMessageEvent(messageEnvelope.Header, messageEnvelope, incrementAndRetry, this.eventQueue);
            this.eventQueue.Schedule((long)timeout.TotalMilliseconds, resendEvent);

            // Store the resend event so we can delete it when an ack is recieved.
            AcknowledgementState ackState = null;
            if (!this.mPendingAcknowledgements.TryGetValue(messageEnvelope.Header.SequenceNumber, out ackState))
            {
                ackState = new AcknowledgementState(messageEnvelope.Header.SequenceNumber);
                this.mPendingAcknowledgements.Add(messageEnvelope.Header.SequenceNumber, ackState);
            }
            ackState.ResendEvent = resendEvent;
        }

        /// <summary>
        /// Sends the ack.
        /// </summary>
        /// <param name="sequenceNumber">The sequence number.</param>
        public void SendAck(SequenceNumber sequenceNumber)
        {
            lock (this.mAcksToSend)
            {
                this.mAcksToSend.Add(sequenceNumber);
            }

            this.delayedMessageSender.Schedule(Parameters.AckDelay, this);
        }
    }
}
