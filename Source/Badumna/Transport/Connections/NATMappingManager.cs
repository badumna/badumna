﻿//---------------------------------------------------------------------------------
// <copyright file="NATMappingManager.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using Badumna.Core;
using Badumna.Utilities;

namespace Badumna.Transport
{
    /// <summary>
    /// The NAT mapping manager is used to renewal mappings and punched holes on NAT.
    /// </summary>
    internal class NATMappingManager
    {
        /// <summary>
        /// All unique connnections
        /// </summary>
        private List<Connection> connectionSet;

        /// <summary>
        /// All connections
        /// </summary>
        private Dictionary<PeerAddress, Connection> connections;

        /// <summary>
        /// All punched holes
        /// </summary>
        private Dictionary<PeerAddress, PeerAddress> punchedHoles;

        /// <summary>
        /// The address to send keep alive messages to.
        /// </summary>
        private PeerAddress keepAliveAddress;

        /// <summary>
        /// Whether the keep alive address is a random address random. 
        /// </summary>
        private bool keepAliveAddressIsRandom;

        /// <summary>
        /// The time source.
        /// </summary>
        private ITime timeKeeper;

        /// <summary>
        /// The network event queue.
        /// </summary>
        private NetworkEventQueue eventQueue;

        /// <summary>
        /// The regular task. 
        /// </summary>
        private RegularTask natMappingRenewalTask;

        /// <summary>
        /// The delegate used to send hole punch message.
        /// </summary>
        private SendEmptyHolePunchMessage sendEmptyHolePunchMessage;

        /// <summary>
        /// Provides the public peer address.
        /// </summary>
        private INetworkAddressProvider addressProvider;

        /// <summary>
        /// Initializes a new instance of the <see cref="NATMappingManager"/> class.
        /// </summary>
        /// <param name="connections">The connections.</param>
        /// <param name="connectionSet">The connection set.</param>
        /// <param name="punchedHoles">The punched holes.</param>
        /// <param name="timeKeeper">The time keeper.</param>
        /// <param name="eventQueue">The event queue.</param>
        /// <param name="sender">The delegate used to send hole punch message.</param>
        /// <param name="addressProvider">Provides the public peer address.</param>
        /// <param name="connectivityReporter">Reports on network connectivity status.</param>
        public NATMappingManager(
            Dictionary<PeerAddress, Connection> connections,
            List<Connection> connectionSet,
            Dictionary<PeerAddress, PeerAddress> punchedHoles,
            ITime timeKeeper,
            NetworkEventQueue eventQueue,
            SendEmptyHolePunchMessage sender,
            INetworkAddressProvider addressProvider,
            INetworkConnectivityReporter connectivityReporter)
        {
            this.connections = connections;
            this.connectionSet = connectionSet;
            this.punchedHoles = punchedHoles;
            this.timeKeeper = timeKeeper;
            this.eventQueue = eventQueue;
            this.sendEmptyHolePunchMessage = sender;
            this.addressProvider = addressProvider;

            this.keepAliveAddress = null;
            this.keepAliveAddressIsRandom = false;

            this.natMappingRenewalTask = new RegularTask(
                "NAT mapping renewal task",
                Parameters.HolePunchKeepAlivePeriod,
                this.eventQueue,
                connectivityReporter,
                this.RefreshNATMappings);
        }

        /// <summary>
        /// The delegate for sending hole punch message. 
        /// </summary>
        /// <param name="address">The address.</param>
        internal delegate void SendEmptyHolePunchMessage(PeerAddress address);

        /// <summary>
        /// Starts this instance.
        /// </summary>
        public void Start()
        {
            this.natMappingRenewalTask.Start();
        }

        /// <summary>
        /// Stops this instance.
        /// </summary>
        public void Stop()
        {
            this.natMappingRenewalTask.Stop();
        }

        /// <summary>
        /// Determines whether is acceptable address.
        /// </summary>
        /// <param name="address">The address.</param>
        /// <returns>
        /// <c>true</c> if is acceptable address; otherwise, <c>false</c>.
        /// </returns>
        private static bool IsAcceptableAddress(IPAddress address)
        {
            byte fb = address.GetAddressBytes()[0];
            if (fb == 0)
            {
                // reserved address, "this network"
                return false;
            }

            // see http://tools.ietf.org/html/rfc5735
            if (fb == 10 || fb == 127 || fb == 169 || fb == 172 || fb == 192) 
            {
                // private and loop back addresses
                return false;
            }

            if (fb == 198 || fb == 203 || fb == 240 || fb == 255)
            {
                // test, reserved addresses
                return false;
            }

            if (fb >= 224 && fb <= 239)
            {
                // multicast address
                return false;
            }

            return true;
        }

        /// <summary>
        /// Refreshes the NAT mappings.
        /// </summary>
        private void RefreshNATMappings()
        {
            TimeSpan now = this.timeKeeper.Now;

            // according to some online discussion, the UDP holes created on NATs and stateful firewalls also need to be
            // refreshed periodically to keep alive. The interval of such refresh is currently set to 15 seconds to 
            // ensure some NATs that have 20 seconds hole expire time can work happily with badumna.
            // see:
            // http://forums.adobe.com/thread/638240?tstart=0
            // http://zgp.org/pipermail/p2p-hackers/2005-June/002665.html
            // http://www.linuxjournal.com/article/9004?page=0,1

            // punched holes need to be renewed
            foreach (PeerAddress address in this.punchedHoles.Keys)
            {
                Connection connection = null;
                if (this.connections.TryGetValue(address, out connection))
                {
                    if (connection.IsConnected &&
                        now - connection.LastSendTime > Parameters.NATMappingKeepAliveMinimumInterval)
                    {
                        this.sendEmptyHolePunchMessage(address);
                        connection.LastSendTime = now;
                    }
                }
            }

            // the internal/external ip/port mappings on symmetric nat also need to be renewed.  
            if (this.addressProvider.PublicAddress.NatType == NatType.SymmetricNat)
            {
                foreach (Connection connection in this.connectionSet)
                {
                    if (connection.IsConnected && !connection.IsRelayed &&
                        now - connection.LastSendTime > Parameters.NATMappingKeepAliveMinimumInterval)
                    {
                        this.sendEmptyHolePunchMessage(connection.PeersPublicAddress);
                        connection.LastSendTime = now;
                    }
                }
            }

            // send an empty pkt to a random destination to keep the NAT mapping alive. 
            if (this.addressProvider.PublicAddress.NatType == NatType.FullCone ||
                this.addressProvider.PublicAddress.NatType == NatType.MangledFullCone)
            {
                this.UpdateNATMappingKeepAliveAddress();
                Debug.Assert(this.keepAliveAddress != null, "invalid keep alive msg destination address");
                this.sendEmptyHolePunchMessage(this.keepAliveAddress);
            }

            // TODO: check whether the port mappings on other NAT types also need such keep alive refresh.  
        }

        /// <summary>
        /// Updates the NAT mapping keep alive address.
        /// </summary>
        private void UpdateNATMappingKeepAliveAddress()
        {
            // initially we don't have the address
            if (this.keepAliveAddress == null)
            {
                this.TryAssignAddress(true);
            }
            else
            {
                if (this.keepAliveAddressIsRandom)
                {
                    // we have a random address, it is always better to use a valid address from the connection table
                    // but it shouldn't change to a different random address. 
                    this.TryAssignAddress(false);
                }
                else
                {
                    // we have a address, it is not randomly generated, we check whether it is still in the connection
                    // table, i.e. the other side is still online
                    if (!this.AddressStillValid())
                    {
                        this.TryAssignAddress(true);
                    }
                }
            }
        }

        /// <summary>
        /// Gets the NAT mapping keep alive address from connection set.
        /// </summary>
        /// <returns>The peer address to use or null on failure.</returns>
        private PeerAddress GetNATMappingKeepAliveAddressFromConnectionSet()
        {
            if (this.connectionSet.Count == 0)
            {
                return null;
            }

            int r = RandomSource.Generator.Next(this.connectionSet.Count);
            for (int i = 0; i < this.connectionSet.Count; i++)
            {
                PeerAddress p = this.connectionSet[(i + r) % this.connectionSet.Count].PeersPublicAddress;
                if (p == null || p.Address == null)
                {
                    continue;
                }

                if (IsAcceptableAddress(p.Address))
                {
                    return p;
                }
            }

            return null;
        }

        /// <summary>
        /// Gets the random address.
        /// </summary>
        /// <returns>Random address.</returns>
        private PeerAddress GetRandomAddress()
        {
            byte[] randomBytes = new byte[4];
            randomBytes[0] = (byte)(RandomSource.Generator.Next(116) + 11);  // returns 11-126
            randomBytes[1] = (byte)RandomSource.Generator.Next(255);
            randomBytes[2] = (byte)RandomSource.Generator.Next(255);
            randomBytes[3] = (byte)RandomSource.Generator.Next(255);
            int port = 9; // UDP discard protocol. 

            return new PeerAddress(new System.Net.IPAddress(randomBytes), port, NatType.Unknown);
        }

        /// <summary>
        /// Whether the address is still considered as valid keep alive address.
        /// </summary>
        /// <returns>Whether the address is still valid.</returns>
        private bool AddressStillValid()
        {
            return this.connections.ContainsKey(this.keepAliveAddress);
        }

        /// <summary>
        /// Tries to assign the keep alive message destination address.
        /// </summary>
        /// <param name="useRandomAddress">if set to <c>true</c> then use random address when necessary.</param>
        private void TryAssignAddress(bool useRandomAddress)
        {
            PeerAddress p = this.GetNATMappingKeepAliveAddressFromConnectionSet();
            if (p == null)
            {
                if (useRandomAddress)
                {
                    this.keepAliveAddress = this.GetRandomAddress();
                    this.keepAliveAddressIsRandom = true;
                }
            }
            else
            {
                this.keepAliveAddress = p;
                this.keepAliveAddressIsRandom = false;
            }
        }
    }
}
