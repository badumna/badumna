﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;

using Badumna.Utilities;
using Badumna.Core;

namespace Badumna.Transport
{
    class OutboundProcessor : Channel<TransportEnvelope>
    {
        public override bool IsEmpty
        {
            get
            {
                return this.mDispatchQueue.Count == 0 &&
                       (!this.CanUseSource || this.mSource.IsEmpty);
            }
        }

        private Connection mConnection;
        private ISource<TransportEnvelope> mSource;

        /// <summary>
        /// All envelopes will be temporarily placed on this queue after they have
        /// undergone final processing.  This is to support (e.g.) fragments which once
        /// generated should all be sent at once.
        /// </summary>
        private Queue<TransportEnvelope> mDispatchQueue = new Queue<TransportEnvelope>();

        /// <summary>
        /// Reports on network connectivity status.
        /// </summary>
        private INetworkConnectivityReporter connectivityReporter;

        /// <summary>
        /// If true, we can take messages from our source and pass them to the finalizer.
        /// Otherwise, the source is blocked.  The connection initialization code passes
        /// its messages directly to the finalizer so is not affected by this.
        /// </summary>
        private bool CanUseSource
        {
            get
            {
                return this.mConnection.IsConnected &&
                       this.connectivityReporter.Status != ConnectivityStatus.Initializing;  // TODO: IsServerConnected => !Connectivity.Initializing?
            }
        }


        public OutboundProcessor(Connection connection, ISource<TransportEnvelope> source, ITime timeKeeper, INetworkConnectivityReporter connectivityReporter)
            : base(timeKeeper)
        {
            this.mConnection = connection;
            this.mSource = source;
            this.connectivityReporter = connectivityReporter;
            source.SetUnemptiedCallback(delegate { this.InvokeUnemptiedCallback(); });
        }

        public void OnConnected()
        {
            if (!this.IsEmpty)
            {
                this.InvokeUnemptiedCallback();
            }
        }

        protected override void DoPut(TransportEnvelope item)
        {
            this.mDispatchQueue.Enqueue(item);
        }

        public override TransportEnvelope Peek()
        {
            throw new NotSupportedException();
        }

        public override void Clear()
        {
            this.mDispatchQueue.Clear();
        }

        protected override TransportEnvelope DoGet()
        {
            while (this.mDispatchQueue.Count == 0)
            {
                TransportEnvelope envelope = this.mSource.Get();

                if (envelope == null)
                {
                    break;
                }

                this.mConnection.FinalizeEnvelope(envelope);  // FinalizeEnvelope is expected to queue an envelope on mDispatchQueue (Connection is happily coupled with OutboundProcessor).
            }

            if (this.mDispatchQueue.Count > 0)
            {
                return this.mDispatchQueue.Dequeue();
            }

            return null;
        }
    }
}
