/***************************************************************************
 *  File Name: ConnectionTable.cs
 *
 *  Copyright (C) 2007 All Rights Reserved.
 * 
 *  Written by
 *      Scott Douglas <scdougl@csse.unimelb.edu.au>
 ****************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Diagnostics;

using Badumna.Utilities;
using Badumna.Core;
using Badumna.Security;
using Badumna.Transport.Connections;

using SequenceNumber = Badumna.Utilities.CyclicalID.UShortID;

namespace Badumna.Transport
{
    abstract class ConnectionProtocol : TransportProtocol
    {
        public ConnectionProtocol(string layerName, Type protocolAttributeType, GenericCallBackReturn<object, Type> factory)
            : base(layerName, protocolAttributeType, factory)
        {
        }

        internal abstract ProtocolRoot ProtocolRoot { get; }

        public abstract void ProcessInitialMessage(long connectionID, PeerAddress publicAddress, CertificateToken userCertificate);

        public abstract void SynchronizeSession(SequenceNumber nextSequenceNumber, byte[] encryptedSessionKey, byte[] signature);

        public abstract void ProcessMessageBody(TransportEnvelope envelope, PeerAddress sourcesPublicAddress, PeerAddress destination);

        public abstract void SendDirectConfirmation(PeerAddress address, byte[] encryptedRequestId);

        public abstract void RelayConnectionAttemptNotification(PeerAddress address);

        public abstract void ConnectionEstablishedWith(PeerAddress address);

        public abstract byte[] GetSalt(PeerAddress address);
    }

    /// <summary>
    /// A collection of Connection instances. Provides connectivity with all peers.
    /// </summary>
    /// <remarks>
    /// As a protocol layer the primary interaction with the ConnectionTable is through the SendMessage() and HandleMessage() methods.
    /// The SendMessage() method will pass the message to the appropriate Connection, creating a new instance if one doesn't exist already.
    /// The HandleMessage() method will notif the appropriate Connection of the arrival of the message and pass it to higher protocol levels
    /// if the Connection's IsAcceptable() method returns true.
    /// The ConnectionTable class references an instance of the RendezvousService class (passed to the constructor), used to initiate communication
    /// with peers behind various NAT types. 
    /// </remarks>
    /// <see cref="Connection"/>
    partial class ConnectionTable : ConnectionProtocol, IConnectionTable, IMessageConsumer<TransportEnvelope>, IChannelFunnel<TransportEnvelope>, IPeerConnectionNotifier
    {
        public double MaximumPacketLossRate { get; private set; }
        public double AveragePacketLossRate { get; private set; }
        public double TotalSendLimitBytesPerSecond { get; private set; }
        public double MaximumSendLimitBytesPerSecond { get; private set; }
        public long TotalBytesSentPerSecond { get; private set; }
        public long TotalBytesReceivedPerSecond { get; private set; }
        public IEnumerable<Connection> Connections { get { return this.mConnectionSet; } }

        public IEnumerable<IConnection> AllConnections
        {
            get
            {
                // Suspending the event queue is heavy-handed, but we don't have a finer-grained
                // lock for the connection set.
                this.eventQueue.AcquirePerformLock();
                try
                {
                    var connections = new List<IConnection>();
                    foreach (var connection in this.mConnectionSet)
                    {
                        connections.Add(connection);
                    }

                    return connections;
                }
                finally
                {
                    this.eventQueue.ReleasePerformLock();
                }
            }
        }

        /// <summary>
        /// Delegate for connection established event.
        /// </summary>
        private ConnectionHandler connectionEstablishedEventDelegate;

        // <inheritdoc/>
        public event ConnectionHandler ConnectionEstablishedEvent
        {
            add { this.connectionEstablishedEventDelegate += value; }
            remove { this.connectionEstablishedEventDelegate -= value; }
        }

        /// <summary>
        /// Delegate for connection lost event.
        /// </summary>
        private ConnectionHandler connectionLostEventDelegate;
        
        // <inheritdoc/>
        public event ConnectionHandler ConnectionLostEvent
        {
            add { this.connectionLostEventDelegate += value; }
            remove { this.connectionLostEventDelegate -= value; }
        }

        protected Dictionary<PeerAddress, Connection> mConnections = new Dictionary<PeerAddress, Connection>();
        private Blacklist mBlacklist;

        // Contains exactly one instance of each Connection.  (i.e. solves issues
        // with enumerating mConnections.Values because a given connection may be
        // aliased multiple times in that dictionary).
        private List<Connection> mConnectionSet = new List<Connection>();

        protected IRelayManager mRelayManager;

        private DhtTransport mDhtTranport;
        private RegularTask mAttemptReconnectTask;
        private RegularTask mGarbageTask;
        private RegularTask mStatisticsTask;

        private Dictionary<PeerAddress, PeerAddress> mPunchedHoles;
        
        private const int mConnectionGarbageDelayMilliseconds = 15000;
        private static readonly TimeSpan StatisticsUpdatePeriod = TimeSpan.FromSeconds(2);

        private IMessageDispatcher<TransportEnvelope> mDispatcher;
        private ProtocolRoot mProtocolRoot;
        internal override ProtocolRoot ProtocolRoot { get { return this.mProtocolRoot; } }

        private byte[] mChecksumSalt;

        /// <summary>
        /// Makes checksum salts.
        /// </summary>
        private ChecksumSalter checksumSalter;

        private OpenPeerListGenerator openPeerListGenerator;

        /// <summary>
        /// The queue to use for scheduling events.
        /// </summary>
        private NetworkEventQueue eventQueue;

        /// <summary>
        /// Provides access to the current time.
        /// </summary>
        private ITime timeKeeper;

        /// <summary>
        /// Network initializer, for finding peers when none are known.
        /// </summary>
        private IPeerFinder peerFinder;

        /// <summary>
        /// The NAT mapping manager.
        /// </summary>
        private NATMappingManager natMappingManager;

        /// <summary>
        /// Provides the public peer address.
        /// </summary>
        private INetworkAddressProvider addressProvider;

        /// <summary>
        /// Reports on network connectivity status.
        /// </summary>
        private INetworkConnectivityReporter connectivityReporter;

        /// <summary>
        /// Provides security tokens.
        /// </summary>
        private TokenCache tokens;

        /// <summary>
        /// The queue to use for scheduling key encryption.
        /// </summary>
        private NetworkEventQueue encryptionQueue;

        public ConnectionTable(
            IMessageDispatcher<TransportEnvelope> dispatcher,
            IMessageProducer<TransportEnvelope> messageProducer,
            IPeerFinder peerFinder,
            NetworkEventQueue eventQueue,
            NetworkEventQueue encryptionQueue,
            ITime timeKeeper,
            INetworkAddressProvider addressProvider,
            INetworkConnectivityReporter connectivityReporter,
            string applicationName,
            TokenCache tokens,
            GenericCallBackReturn<object, Type> factory)
            : base("Connectionless", typeof(ConnectionlessProtocolMethodAttribute), factory)
        {
            messageProducer.MessageConsumer = this;
            this.mDispatcher = dispatcher;
            this.peerFinder = peerFinder;
            this.eventQueue = eventQueue;
            this.encryptionQueue = encryptionQueue;
            this.timeKeeper = timeKeeper;
            this.addressProvider = addressProvider;
            this.connectivityReporter = connectivityReporter;
            this.tokens = tokens;

            this.checksumSalter = new ChecksumSalter(applicationName);

            this.mBlacklist = new Blacklist(eventQueue, connectivityReporter);

            this.DispatcherMethod = this.DispatchMessage;
            this.mPunchedHoles = new Dictionary<PeerAddress, PeerAddress>();
            this.mAttemptReconnectTask = new RegularTask(
                "Attempt reconnect",
                Parameters.AttemptReconnectPeriod,
                eventQueue,
                connectivityReporter,
                this.InitializeConnections);
            this.mGarbageTask = new RegularTask(
                "Collect connection garbage",
                TimeSpan.FromMilliseconds(ConnectionTable.mConnectionGarbageDelayMilliseconds),
                eventQueue,
                connectivityReporter,
                this.CollectGarbage);
            this.mStatisticsTask = new RegularTask(
                "Calculate connection statistics",
                ConnectionTable.StatisticsUpdatePeriod,
                eventQueue,
                connectivityReporter,
                this.UpdateStatistics);
         
            this.natMappingManager = new NATMappingManager(
                this.mConnections, 
                this.mConnectionSet, 
                this.mPunchedHoles, 
                this.timeKeeper, 
                this.eventQueue, 
                this.SendEmptyHolePunchMessage,
                this.addressProvider,
                this.connectivityReporter);

            this.mProtocolRoot = new ProtocolRoot(this, factory);
            this.openPeerListGenerator = new OpenPeerListGenerator(this);
            this.mProtocolRoot.Parser.RegisterMethodsIn(this);            
        }

        public override byte[] GetSalt(PeerAddress address)
        {
            return this.checksumSalter.GetSalt(address);
        }

        private void AddConnection(PeerAddress key, Connection connection)
        {
            if (key == null)
            {
                throw new ArgumentNullException("key");
            }

            if (connection == null)
            {
                throw new ArgumentNullException("connection");
            }

            if (this.mConnections.ContainsKey(key))
            {
                this.DeleteConnection(key);
            }

            this.mConnections[key] = connection;
            if (!this.mConnectionSet.Contains(connection))
            {
                this.mConnectionSet.Add(connection);
            }
        }

        private void DeleteConnection(PeerAddress key)
        {
            Connection connection;
            if (this.mConnections.TryGetValue(key, out connection))
            {
                this.mConnections.Remove(key);

                this.RemovePunchedHole(key);

                // Scan through all connections here so we can maintain mConnectionSet.
                // Better than trying to calculate the set of connections dynamically
                // each time we need it.
                foreach (Connection remainingConnection in this.mConnections.Values)
                {
                    if (connection == remainingConnection)
                    {
                        // Still exists, don't remove it from set
                        return;
                    }
                }

                this.mConnectionSet.Remove(connection);
            }
        }

        private void ClearConnections()
        {
            this.mConnections.Clear();
            this.mConnectionSet.Clear();
        }

        public void Initialize(IRelayManager relayManager, DhtTransport dhtTransport)
        {
            this.mRelayManager = relayManager;

            this.mRelayManager.MessageArrivalEvent += this.ProcessRelayMessage;

            this.mDhtTranport = dhtTransport;

            // Subscribe to the event for network status online event. This will reinitialize the RendezvousService, via the 
            // Initialize() method.
            this.connectivityReporter.NetworkInitializingEvent += this.OnInitializing;
            this.connectivityReporter.NetworkOnlineEvent += this.OnlineHandler;
            this.connectivityReporter.NetworkOfflineEvent += this.OfflineHandler;

            this.tokens.SubscribeToExpireEvent(TokenType.UserCertificate, this.UserCertificateExpirationHandler);

            this.mGarbageTask.Start();
            this.mStatisticsTask.Start();
            this.natMappingManager.Start();

            // Only initialize if connected.
            if (this.connectivityReporter.Status == ConnectivityStatus.Online)
            {
                this.OnlineHandler();
            }            
        }

        /// <summary>
        /// Gets a snapshot of diagnostic information describing the state of this class.
        /// </summary>
        /// <remarks>
        /// WARNING: This method is currently not thread-safe!
        /// TODO: Figure out what kind of thread safety this method should support.
        /// </remarks>
        /// <returns>The diagnostic information.</returns>
        public DiagnosticInfo GetDiagnosticInformation()
        {
            DiagnosticInfo result = new DiagnosticInfo();

            DiagnosticInfo connections = new DiagnosticInfo();
            result.Add("connections", connections);

            // TODO: Make this enumeration operation thread-safe.
            foreach (KeyValuePair<PeerAddress, Connection> connection in this.mConnections)
            {
                connections.Add(connection.Key.ToString(), connection.Value.GetDiagnosticInformation());
            }

            return result;
        }

        #region Connection count info

        public int GetActiveConnectionCount()
        {
            int count = 0;
            foreach (Connection connection in this.Connections)
            {
                if (connection.IsConnected)
                {
                    count++;
                }
            }
            return count;
        }

        public int GetInitializingConnectionCount()
        {
            int count = 0;
            foreach (Connection connection in this.Connections)
            {
                if (connection.IsInitializing)
                {
                    count++;
                }
            }
            return count;
        }

        #endregion

        // these methods are used to export/load open addresses for seedpeers.  
        #region export open connections

        /// <summary>
        /// Gets the IPEndPoints of recent active open peers.
        /// </summary>
        /// <returns>A list recently active open peers. </returns>
        public List<IPEndPoint> GetRecentActiveOpenPeers()
        {
            return this.openPeerListGenerator.GetRecentActiveOpenPeers();
        }

        /// <summary>
        /// Adds the initial connections.
        /// </summary>
        /// <param name="peers">The peers.</param>
        public void AddInitialConnections(List<IPEndPoint> peers)
        {
            if (peers == null)
            {
                throw new ArgumentNullException();
            }

            this.eventQueue.Schedule(5000, this.ScheduleInitialConnections, peers);
        }

        /// <summary>
        /// Schedules the initial connections.
        /// </summary>
        /// <param name="peers">The peers.</param>
        private void ScheduleInitialConnections(List<IPEndPoint> peers)
        {
            if (peers == null)
            {
                throw new ArgumentNullException();
            }

            foreach (IPEndPoint ep in peers)
            {
                PeerAddress address = new PeerAddress(ep, NatType.Unknown);
                this.ConfirmConnection(address);
            }
        }

        #endregion

        #region Network Context handlers

        private void OnInitializing()
        {
            foreach (Connection connection in this.Connections)
            {
                connection.Shutdown();
            }

            this.ClearConnections();
        }

        private void OnlineHandler()
        {
            this.mChecksumSalt = this.GetSalt(this.addressProvider.PublicAddress);
            if (!this.mAttemptReconnectTask.IsRunning)
            {
                this.mAttemptReconnectTask.Start();
            }
        }

        protected void OfflineHandler()
        {
            foreach (Connection connection in this.Connections)
            {
                connection.Shutdown();
            }

            this.ClearConnections();
        }

        private void UserCertificateExpirationHandler(object sender, TokenExpirationEventArgs args)
        {
            CertificateToken newCertificate = args.NewToken as CertificateToken;

            if (newCertificate == null)
            {
                Logger.TraceWarning(LogTag.Connection, "New user certificate is invalid. Cannot update other peers. ALL connections will be lost.");
                return;
            }

            // Send the new certificate to all the current active peers.
            foreach (KeyValuePair<PeerAddress, Connection> pair in this.mConnections)
            {
                if (pair.Value.IsConnected 
                    && pair.Value.PeersPublicAddress.Equals(pair.Key)) // Perfom only once per connection i.e. Ignore connections with multiple indecies.
                {
                    TransportEnvelope envelope = this.GetMessageFor(pair.Key, QualityOfService.Reliable);
                    envelope.Header.IsConnectionless = true; // Needs to be connectionless so it is not processed by the connectionful stack.

                    this.RemoteCall(envelope, this.UpdateCertificate, newCertificate);
                    this.DispatchMessage(envelope);
                }
            }

        }

        #endregion

        #region Reconnect task

        public void InitializeConnections()
        {
            if (!this.mAttemptReconnectTask.IsRunning)
            {
                this.mAttemptReconnectTask.Start();
            }

            if (this.IsIsolated)
            {
                Logger.TraceInformation(LogTag.Connection, "Local peer is isolated, using the initializer to connect it to the network.");
                this.peerFinder.QueryForInitialPeerList(this.PeerListQueryCompleteHandler);
            }
        }

        private void PeerListQueryCompleteHandler(IEnumerable<PeerAddress> initialPeerList)
        {
            this.eventQueue.Schedule(1, delegate(IEnumerable<PeerAddress> initialPeers)
            {
                Logger.TraceInformation(LogTag.Connection, "{0} is joining :", this.addressProvider.PublicAddress);
                if (initialPeers != null)
                {
                    foreach (PeerAddress peer in initialPeers)
                    {
                        Logger.TraceInformation(LogTag.Connection, "   {0}", peer);
                        this.ConfirmConnection(peer);
                    }
                }
            }, initialPeerList);
        }

        private bool IsIsolated
        {
            get
            {
                // doesn't know another other peer
                if (this.mConnectionSet.Count == 0)
                {
                    Logger.TraceInformation(LogTag.Connection, "Doesn't know any other peer, the local peer is regarded as isolated.");
                    return true;
                }

                // know some other peers, but connected to none of them 
                bool hasConnected = false;
                foreach (Connection connection in this.mConnectionSet)
                {
                    if (connection.IsConnected)
                    {
                        Logger.TraceInformation(LogTag.Connection, "At least one connected peer in the connection table : {0}, not isolated", connection.PeersPublicAddress);
                        hasConnected = true;
                        break;
                    }
                }

                if (!hasConnected)
                {
                    Logger.TraceInformation(LogTag.Connection, "None of the known peer is connected. The local peer is isolated.");
                    return true;
                }

                // do we have connection to any open peer? 
                foreach (Connection connection in this.mConnectionSet)
                {
                    if(!connection.IsConnected)
                    {
                        continue;
                    }

                    if (connection.PeersPublicAddress.NatType == NatType.Open || connection.PeersPublicAddress.NatType == NatType.FullCone)
                    {
                        // we have connection to a open peer
                        return false;
                    }
                }

                return true;
            }
        }

        #endregion

        #region IConnectionTable methods

        public bool IsKnown(PeerAddress address)
        {
            return this.mConnections.ContainsKey(address);
        }

        public bool IsConnected(PeerAddress address)
        {
            Connection connection = null;

            if (this.mConnections.TryGetValue(address, out connection))
            {
                return connection.IsConnected;
            }

            return false;
        }

        public bool IsRelayed(PeerAddress address)
        {
            Connection connection = null;

            if (this.mConnections.TryGetValue(address, out connection))
            {
                return connection.IsRelayed;
            }

            return false;
        }

        public void ConfirmConnection(PeerAddress address)
        {
            if (address == null || address.Equals(this.addressProvider.PublicAddress))
            {
                return;
            }

            Connection connection = null;
            if (!this.mConnections.TryGetValue(address, out connection))
            {
                // This will form and initialize a connection correctly, performing all the necessary checks.
                // we don't send the message because it is just a waste of bandwidth. It would also mean the first message 
                // is junk and sent immediately which prevents piggybacking of real messages.
                this.GetMessageFor(address, QualityOfService.Unreliable);
            }
            else
            {
                if (connection.IsConnected)
                {
                    connection.PerformStatusCheck();
                }
                else if (!connection.IsInitializing)
                {
                    connection.BeginInitialization();
                }
            }
        }

        public void RemoveConnection(PeerAddress address)
        {
            Connection connection = null;

            if (this.mConnections.TryGetValue(address, out connection))
            {
                this.ConnectionLostWith(connection.PeersPublicAddress);
                this.eventQueue.Schedule(5, this.CleanupConnection, address);
            }
        }

        #endregion

        #region Connection Protocol Methods

        [ConnectionlessProtocolMethod(ConnectionlessMethod.ProcessInitialMessage)]
        public override void ProcessInitialMessage(long connectionID, PeerAddress publicAddress, CertificateToken remoteUsersCertificateImpl)
        {
            if (!publicAddress.IsValid)
            {
                return;
            }

            ICertificateToken remoteUsersCertificate = (ICertificateToken) remoteUsersCertificateImpl;

            bool messageWasRelayed = this.CurrentEnvelope.WasRelayed;
            
            // used to track the execution path of this method when things go ugly. 
            List<int> path = new List<int>();

            // We have to check that the connection is not with the local peer which can happen if the router is misbehaving,
            // If we make the alias then it effectively sends all messages indended for a neighbour to itself!
            if (this.addressProvider.PublicAddress.Equals(publicAddress))
            {
                if (!messageWasRelayed)
                {
                    // Attempt an alternate method of connection.
                    Logger.TraceInformation(LogTag.Connection, "NOT Attempting alternative connection method with {0} becuase message was hairpined to self.", publicAddress);
                }
                else
                {
                    Logger.TraceError(LogTag.Connection, "Initial relay message has been routed to self.");
                }

                return;
            }

            try
            {
                if (remoteUsersCertificate == null || !remoteUsersCertificate.Validate(this.tokens))
                {
                    Logger.TraceInformation(LogTag.Connection, "Failed to authenticate user on peer {0}. Dropping connection", publicAddress);
                    return;
                }
                else
                {
                    Logger.TraceInformation(LogTag.Connection, "Peer {0} user has successfully authenticated as '{1}'", publicAddress, remoteUsersCertificate.Character);
                }

                if (this.mBlacklist.IsBlackListed(remoteUsersCertificate.UserId))
                {
                    Logger.TraceInformation(LogTag.Connection, "Not forming connection with {0} (user #{1}) because it has been blacklisted.", remoteUsersCertificate.Character, remoteUsersCertificate.UserId);
                    return;
                }
            }
            catch (Exception e) // Catch because the code is executed in application supplied code.
            {
                Logger.TraceException(LogLevel.Error, e, "Failed to create certificate.");
                return;
            }

            Connection publicAddressConnection; // The connection indexed by the public address

            if (this.mConnections.TryGetValue(publicAddress, out publicAddressConnection))
            {
                path.Add(1);
                Logger.TraceInformation(LogTag.Connection, "Public address {0} is present in connection table.", publicAddress);

                if (!this.CurrentEnvelope.Source.Equals(publicAddress))
                {
                    path.Add(2);
                    Logger.TraceInformation(LogTag.Connection, "Public address {0} is not the same as the source address {1}", publicAddress, this.CurrentEnvelope.Source);

                    if (!publicAddressConnection.PeersPrivateAddress.Equals(this.CurrentEnvelope.Source))
                    {
                        path.Add(3);
                        Logger.TraceInformation(LogTag.Connection, "Connection indexed by public address does not have the same private address ({0}) as the source ({1})",
                            publicAddressConnection.PeersPrivateAddress, this.CurrentEnvelope.Source);

                        Connection privateAddressConnection = null; // The connection indexed by the private address

                        if (this.mConnections.TryGetValue(this.CurrentEnvelope.Source, out privateAddressConnection))
                        {
                            path.Add(4);
                            Logger.TraceInformation(LogTag.Connection, "Source address {0} is present in connection table.", this.CurrentEnvelope.Source);

                            if (!privateAddressConnection.PeersPublicAddress.Equals(publicAddress))
                            {
                                path.Add(5);
                                Logger.TraceWarning(LogTag.Connection, "A connection request from an already known peer was recieved that has different public address.");
                                this.CleanupConnection(publicAddress);
                                this.CleanupConnection(this.CurrentEnvelope.Source);

                                publicAddressConnection = this.GetOrCreateConnection(publicAddress);
                                publicAddressConnection.PeersPrivateAddress = this.CurrentEnvelope.Source;
                                this.AddConnection(publicAddress, publicAddressConnection);
                                this.AddConnection(this.CurrentEnvelope.Source, publicAddressConnection);
                            }
                            else
                            {
                                path.Add(6);
                                Logger.TraceInformation(LogTag.Connection, "Connection indexed by source has the same public address.");

                                if (publicAddressConnection == privateAddressConnection)
                                {
                                    path.Add(7);
                                    Logger.TraceInformation(LogTag.Connection, "Connection instances are the same.");

                                    // The private address on public connection must be inconsistent
                                    publicAddressConnection.PeersPrivateAddress = this.CurrentEnvelope.Source;
                                }
                                else
                                {
                                    path.Add(8);
                                    // Remove the private address connection and replace with the public one
                                    publicAddressConnection.PeersPrivateAddress = this.CurrentEnvelope.Source;
                                    this.AddConnection(this.CurrentEnvelope.Source, publicAddressConnection);
                                    privateAddressConnection.Shutdown();
                                }
                            }
                        }
                        else
                        {
                            path.Add(9);
                            Logger.TraceInformation(LogTag.Connection, "Adding private address index ({1}) to the connection table for {0}", publicAddress, this.CurrentEnvelope.Source);

                            // Index the connection by the private address as well.
                            publicAddressConnection.PeersPrivateAddress = this.CurrentEnvelope.Source;
                            this.AddConnection(publicAddressConnection.PeersPrivateAddress, publicAddressConnection);
                        }
                    }
                    else
                    {
                        path.Add(10);
                        Connection privateConnection;
                        if (this.mConnections.TryGetValue(this.CurrentEnvelope.Source, out privateConnection))
                        {
                            path.Add(11);
                            if (!privateConnection.PeersPublicAddress.Equals(publicAddress))
                            {
                                path.Add(12);
                                this.CleanupConnection(this.CurrentEnvelope.Source);
                                publicAddressConnection.PeersPrivateAddress = this.CurrentEnvelope.Source;
                                this.AddConnection(this.CurrentEnvelope.Source, publicAddressConnection);
                            }
                        }
                        else
                        {
                            path.Add(13);
                            this.AddConnection(this.CurrentEnvelope.Source, publicAddressConnection);
                        }

                        Logger.TraceInformation(LogTag.Connection, "Connection indexed by public address {0} has the same private address as the source ({1})",
                            publicAddress, this.CurrentEnvelope.Source);
                    }

                    if (publicAddressConnection.IsRelayed && publicAddressConnection.PeersPrivateAddress.NatType == NatType.Internal)
                    {
                        path.Add(14);
                        Logger.TraceInformation(LogTag.Connection, "Relayed connection has internal source. Changing transport method.");
                        publicAddressConnection.SetDispatcher(this.mDispatcher);
                    }
                }
                else
                {
                    path.Add(15);
                    Logger.TraceInformation(LogTag.Connection, "Source and public address are the same.");
                }
            }
            else
            {
                path.Add(16);
                Logger.TraceInformation(LogTag.Connection, "No connection indexed by the public address {0}", publicAddress);

                Connection privateAddressConnection = null; // The connection indexed by the private address

                if (!this.mConnections.TryGetValue(this.CurrentEnvelope.Source, out privateAddressConnection))
                {
                    path.Add(17);
                    Logger.TraceInformation(LogTag.Connection, "No connection indexed by the source address {0} either", this.CurrentEnvelope.Source);
                    if (this.CurrentEnvelope.Source.NatType == NatType.Internal)
                    {
                        path.Add(18);
                        Logger.TraceInformation(LogTag.Connection, "Source address is internal. Initializing with public address {0}", publicAddress);
                        Debug.Assert(publicAddress.NatType != NatType.Internal, "Public address cannot be Internal.");

                        if (publicAddress.NatType == NatType.Internal)
                        {
                            path.Add(19);
                            Logger.TraceError(LogTag.Connection, "Public address cannot be Internal. Ignoring connection request.");
                            return;
                        }
                        else
                        {
                            path.Add(20);
                            Logger.TraceInformation(LogTag.Connection, "Creating connection instance for public address {0}", publicAddress);
                            privateAddressConnection = publicAddressConnection = this.GetOrCreateConnection(publicAddress);
                            if (!this.CurrentEnvelope.Source.Equals(publicAddress))
                            {
                                path.Add(21);
                                Logger.TraceInformation(LogTag.Connection, "Indexing connection {0} with source address {1}", publicAddress, this.CurrentEnvelope.Source);
                                publicAddressConnection.PeersPrivateAddress = this.CurrentEnvelope.Source;
                                this.AddConnection(this.CurrentEnvelope.Source, privateAddressConnection);
                            }
                        }
                    }
                    else
                    {
                        path.Add(22);
                        Logger.TraceInformation(LogTag.Connection, "Creating connection instance for source address {0}", this.CurrentEnvelope.Source);
                        privateAddressConnection = this.GetOrCreateConnection(this.CurrentEnvelope.Source);

                        if (!this.CurrentEnvelope.Source.Equals(publicAddress))
                        {
                            path.Add(23);
                            Logger.TraceInformation(LogTag.Connection, "Adding public address ({0}) index for source address {1}", publicAddress, this.CurrentEnvelope.Source);
                            privateAddressConnection.SetPublicAddress(publicAddress);
                            this.AddConnection(publicAddress, privateAddressConnection);
                        }
                    }
                }
                else
                {
                    path.Add(24);
                    if (!privateAddressConnection.PeersPublicAddress.Equals(publicAddress))
                    {
                        path.Add(25);
                        // inconsistent private/public addresses, assume the remote peer has been restarted and thus its addresses have changed. 
                        Logger.TraceInformation(LogTag.Connection, "inconsistent private/public addresses, assume the remote peer has been restarted and thus its addresses have changed.");
                        this.CleanupConnection(this.CurrentEnvelope.Source);
                        privateAddressConnection = this.GetOrCreateConnection(publicAddress);
                        privateAddressConnection.PeersPrivateAddress = this.CurrentEnvelope.Source;
                        this.AddConnection(this.CurrentEnvelope.Source, privateAddressConnection);
                    }

                    Logger.TraceInformation(LogTag.Connection, "Adding public address ({0}) index for private connection {1}", publicAddress, this.CurrentEnvelope.Source);
                    this.AddConnection(publicAddress, privateAddressConnection);
                }

                Debug.Assert(privateAddressConnection != null, "Private address connection is null");
                Debug.Assert(privateAddressConnection.PeersPublicAddress != null, "private connection's public address is null");
                Debug.Assert(publicAddress != null, "Public address is null");
                Debug.Assert(privateAddressConnection.PeersPublicAddress.Equals(publicAddress), "Connection indexed by source has inconsistent public address.");
                Debug.Assert(privateAddressConnection.PeersPrivateAddress.Equals(this.CurrentEnvelope.Source), "Connection indexed by source does not have correct private address.");

                if (!privateAddressConnection.PeersPublicAddress.Equals(publicAddress))
                {
                    path.Add(26);
                    Logger.TraceError(LogTag.Connection, "Connection indexed by source {0} has different public address ({1}) than that stated in the initialization message ({2})",
                        this.CurrentEnvelope.Source, privateAddressConnection.PeersPublicAddress, publicAddress);
                    return;
                }

                publicAddressConnection = privateAddressConnection;
            }

            Debug.Assert(publicAddressConnection != null, "Public address connection is null.");
            Debug.Assert(this.mConnections.ContainsKey(this.CurrentEnvelope.Source), "Private address not present in connection table.");
            Debug.Assert(this.mConnections.ContainsKey(publicAddress), "Public address not present in connection table.");
            Debug.Assert(this.mConnections[this.CurrentEnvelope.Source] != null, "Private address is null.");
            Debug.Assert(this.mConnections[publicAddress] != null, "Public address is null.");
            Debug.Assert(this.CurrentEnvelope.Source.Equals(publicAddress) || this.mConnections[this.CurrentEnvelope.Source].PeersPrivateAddress.Equals(this.CurrentEnvelope.Source), "Connection table inconsistency 1." + GetPath(path));
            Debug.Assert(this.mConnections[this.CurrentEnvelope.Source].PeersPublicAddress.Equals(publicAddress), "Connection table inconsistency 2." + GetPath(path));
            Debug.Assert(this.CurrentEnvelope.Source.Equals(publicAddress) || this.mConnections[publicAddress].PeersPrivateAddress.Equals(this.CurrentEnvelope.Source), "Connection table inconsistency 3." + GetPath(path));
            Debug.Assert(this.mConnections[publicAddress].PeersPublicAddress.Equals(publicAddress), "Connection table inconsistency 4." + GetPath(path));

            if (publicAddressConnection != null)
            {
                if (this.ProcessHeader(this.CurrentEnvelope))
                {
                    Logger.TraceInformation(LogTag.Connection, "Initializing connection ...");
                    
                    if (publicAddressConnection.ShouldTryRelay && messageWasRelayed)
                    {
                        Logger.TraceInformation(LogTag.Connection, "Using relay for connection with {0} from now on.", publicAddress);
                        publicAddressConnection.SetDispatcher(new RelayTransport(this.mRelayManager));
                    }

                    publicAddressConnection.ProcessInitializationRequest(connectionID, publicAddress, remoteUsersCertificate, messageWasRelayed);
                }
                else
                {
                    Logger.TraceInformation(LogTag.Connection, "NOT processing initial message {0} from {1}", this.CurrentEnvelope.Header.SequenceNumber, this.CurrentEnvelope.Source);
                }
            }
            else
            {
                Logger.TraceError(LogTag.Connection, "No connection available to initialize.");
                Debug.Assert(false, "No connection available to initialize.");
            }


            // Need to discard acknowledgements if they weren't removed already.
            AcknowledgementHandler.DiscardAcknowledgments(this.CurrentEnvelope);
        }

        private static string GetPath(List<int> path)
        {
            string result = string.Empty;

            result += " path: ";

            foreach (int p in path)
            {
                result += p + ", ";
            }

            return result;
        }

        [ConnectionlessProtocolMethod(ConnectionlessMethod.SynchronizeSession)]
        public override void SynchronizeSession(SequenceNumber nextSequenceNumber, byte[] encryptedSessionKey, byte[] signature)
        {
            Connection connection = null;

            if (this.mConnections.TryGetValue(this.CurrentEnvelope.Source, out connection))
            {
                if (this.ProcessHeader(this.CurrentEnvelope))
                {
                    Logger.TraceInformation(LogTag.Connection, "Synchronising connection ...");
                    connection.SynchronizeSession(nextSequenceNumber, encryptedSessionKey, signature);
                    return;
                }
                else
                {
                    Logger.TraceInformation(LogTag.Connection, "NOT processing synchronisation message {0} from {1}", this.CurrentEnvelope.Header.SequenceNumber, this.CurrentEnvelope.Source);
                }
            }

            // Need to discard acknowledgements if they weren't removed already.
            AcknowledgementHandler.DiscardAcknowledgments(this.CurrentEnvelope);
        }

        [ConnectionlessProtocolMethod(ConnectionlessMethod.UpdateCertificate)]
        private void UpdateCertificate(CertificateToken token)
        {
            // Check the token is valid
            if (token == null || !((ICertificateToken)token).Validate(this.tokens))
            {
                Logger.TraceInformation(LogTag.Connection, "Ignoring certificate update request because the certificate cannot be validated.");
                return;
            }

            Connection connection = null;
            if (this.mConnections.TryGetValue(this.CurrentEnvelope.Source, out connection))
            {
                if (this.ProcessHeader(this.CurrentEnvelope))
                {
                    connection.UpdateUserCertificate(token);
                    return;
                }
                else
                {
                    Logger.TraceInformation(LogTag.Connection, "NOT processing UpdateCertificate message {0} from {1}", 
                        this.CurrentEnvelope.Header.SequenceNumber, this.CurrentEnvelope.Source);
                }
            }

            // Need to discard acknowledgements if they weren't removed already.
            AcknowledgementHandler.DiscardAcknowledgments(this.CurrentEnvelope);
        }

        #endregion

        public void BlacklistUser(long userId, TimeSpan lifespan)
        {
            Logger.TraceInformation(LogTag.Connection, "User {0} has been blacklisted", userId);

            this.mBlacklist.Add(userId, lifespan);

            foreach (KeyValuePair<PeerAddress, Connection> connectionPair in this.mConnections)
            {
                if (connectionPair.Key.Equals(connectionPair.Value.PeersPublicAddress)) // Only check public addresses. Private indexes will be remove in cleanup
                {
                    if (connectionPair.Value.UserId == userId)
                    {
                        this.RemoveConnection(connectionPair.Key);
                    }
                }
            }
        }

        #region Channels

        public double GetAvailableBandwidth(PeerAddress peerAddress, double newChannelWeight)
        {
            Connection connection;
            if (!this.mConnections.TryGetValue(peerAddress, out connection))
            {
                return double.PositiveInfinity;  // Don't know about this connection, assume there's enough bandwidth available
            }

            double currentWeight = connection.Scheduler.RecentActivityWeight;
            return connection.SentBytesPerSecond * newChannelWeight / (currentWeight + newChannelWeight);
        }

        public void AddChannel(PeerAddress peerAddress, ISource<TransportEnvelope> channel, ChannelGroup group)
        {
            PgpsScheduler<TransportEnvelope> scheduler;

            if (peerAddress.IsDhtAddress)
            {
                scheduler = this.mDhtTranport.Scheduler;
            }
            else
            {
                Connection connection = this.GetOrInitiateConnection(peerAddress);
                scheduler = connection.Scheduler;
            }

            if (group == ChannelGroup.NoGroup)
            {
                scheduler.AddInputSource(channel, 1.0);
            }
            else
            {
                scheduler.AddInputSource(channel, 1.0, (int)group);
            }
        }

        public void RemoveChannel(PeerAddress peerAddress, ISource<TransportEnvelope> channel)
        {
            PgpsScheduler<TransportEnvelope> scheduler;

            if (peerAddress.IsDhtAddress)
            {
                scheduler = this.mDhtTranport.Scheduler;
            }
            else
            {
                Connection connection;
                if (!this.mConnections.TryGetValue(peerAddress, out connection))
                {
                    return;  // No connection, hence no scheduler to remove the channel from
                }
                scheduler = connection.Scheduler;
            }

            scheduler.RemoveInputSource(channel);
        }

        #endregion

        #region Connection garbage

        private void CollectGarbage()
        {
            foreach (Connection connection in this.Connections)
            {
                if (connection.IsGarbage)
                {
                    // If the connection is initializing (been a long time since any messages have been received) then
                    // it is possible that a direct connection is not possible. If a relay has been confirmed then instead of
                    // discarding the connection we should attempt a relay connection first.
                    if (connection.IsInitializing && connection.ShouldTryRelay)
                    {
                        this.AttemptRelayWith(connection);
                        break;
                    }

                    Logger.TraceInformation(LogTag.Connection, "Collecting garbage connection with {0}", connection.PeersPublicAddress);
                    this.eventQueue.Push(this.CleanupConnection, connection.PeersPublicAddress);
                    continue;
                }
            }
        }

        private void UpdateStatistics()
        {
            double maxPacketLoss = 0;
            double averagePacketLoss = 0;
            double totalSendLimit = 0;
            double maxSendLimit = 0;
            long totalBytesSentPerSecond = 0;
            long totalBytesReceivedPerSecond = 0;

            int seen = 0;
            foreach (Connection connection in this.Connections)
            {
                seen++;
                maxPacketLoss = Math.Max(maxPacketLoss, connection.PacketLossRate);
                averagePacketLoss += connection.PacketLossRate;
                totalSendLimit += connection.SendLimitBytesPerSecond;
                maxSendLimit = Math.Max(maxSendLimit, connection.SendLimitBytesPerSecond);
                totalBytesSentPerSecond += (long) connection.SentBytesPerSecond;
                totalBytesReceivedPerSecond += (long)connection.ReceivedBytesPerSecond;
            }

            if (seen > 0)
            {
                averagePacketLoss /= (double)seen;
            }

            this.MaximumPacketLossRate = maxPacketLoss;
            this.AveragePacketLossRate = averagePacketLoss;
            this.TotalSendLimitBytesPerSecond = totalSendLimit;
            this.MaximumSendLimitBytesPerSecond = maxSendLimit;
            this.TotalBytesSentPerSecond = totalBytesSentPerSecond;
            this.TotalBytesReceivedPerSecond = totalBytesReceivedPerSecond;
        }

        private void CleanupConnection(PeerAddress address)
        {
            Connection connection = null;
            bool wasConnected = false;

            if (this.mConnections.TryGetValue(address, out connection))
            {
                Logger.TraceInformation(LogTag.Connection, "Clean up connection {0}", address);

                // Remove all references to a connection in the connection list.
                this.DeleteConnection(connection.PeersPublicAddress);
                this.DeleteConnection(connection.PeersPrivateAddress);
                
                if (this.mConnections.ContainsKey(address))
                {
                    // TODO:
                    // The following Debug.Fail is removed because it is possible to have the address to be a public address X while the connection's 
                    // public and private addresses are all set to a private address. But I am not 100% sure. 

                    // Debug.Fail("Should have removed connection by now.");
                    this.DeleteConnection(address);
                }

                wasConnected = !connection.IsInitializing;
                connection.Shutdown();
            }

            if (this.mConnections.Count == 0
                && wasConnected && !this.mAttemptReconnectTask.IsRunning)
            {
                this.InitializeConnections();
            }
        }

        #endregion

        #region Connection initialialization

        private Connection GetOrCreateConnection(PeerAddress address)
        {
            Connection connection = null;

            if (!this.mConnections.TryGetValue(address, out connection))
            {
                Logger.TraceInformation(LogTag.Connection, "Message from new peer : {0}", address);

                if (address.NatType == NatType.Internal)
                {
                    // Can't use the internal address to create a connection because the public address is not know.
                    return null;
                }
                else
                {
                    // Create a connection for the new source.
                    connection = this.CreateConnection(address);
                }
            }

            return connection;
        }

        private Connection CreateConnection(PeerAddress peerAddress)
        {
            IMessageDispatcher<TransportEnvelope> dispatcher = this.mDispatcher;
            CommunicationMethod connectionMethod = RendezvousMatrix.GetInitiationMethodFor(this.addressProvider.PublicAddress.NatType, peerAddress.NatType);

            if (CommunicationMethod.Relay == connectionMethod)
            {
                Logger.TraceInformation(LogTag.Connection, "Using relay for connection with {0}", peerAddress);
                dispatcher = new RelayTransport(this.mRelayManager);
            }

            return this.CreateConnection(peerAddress, dispatcher);
        }

        private Connection CreateConnection(PeerAddress peerAddress, IMessageDispatcher<TransportEnvelope> dispatcher)
        {
            Connection connection = new Connection(peerAddress, dispatcher, this, this.eventQueue, this.encryptionQueue, this.timeKeeper, this.addressProvider, this.connectivityReporter, this.tokens);

            if (!peerAddress.IsValid)
            {
                Logger.TraceError(LogTag.Connection, "Attempt to create connection with invalid address.");
                return null;
            }

            Debug.Assert(!this.mConnections.ContainsKey(peerAddress), "Attempting to insert a connection when one already exists.");

            connection.MaximumRetriesReachedEvent += this.FailedRetryHandler;

            // The connection should be added prior to being initaizlized. Initialize will call ConnectionTable.MessageDeparture()
            // which inturn calls CreateConnection() if the connection is not already added, causing an infinite loop.
            this.AddConnection(peerAddress, connection);

            Debug.Assert(this.mConnections.ContainsKey(peerAddress));
            return connection;
        }

        private Connection InitiateConnection(PeerAddress peersPublicAddress, PeerAddress peersPrivateAddress, CommunicationMethod initiationMethod)
        {
            IMessageDispatcher<TransportEnvelope> dispatcher = this.mDispatcher;

            if (null == peersPublicAddress || !peersPublicAddress.IsValid)
            {
                Logger.TraceError(LogTag.Connection, "Attempt to create connection with invalid address.");
                return null;
            }

            if (CommunicationMethod.Relay == initiationMethod)
            {
                Logger.TraceInformation(LogTag.Connection, "Using relay for connection with {0}", peersPublicAddress);
                dispatcher = new RelayTransport(this.mRelayManager);
            }

            Connection connection = null;

            // If the private address is valid use that and alias it with the public address,
            // otherwise just use the public address.
            if (null != peersPrivateAddress && peersPrivateAddress.IsValid)
            {
                if (this.mConnections.TryGetValue(peersPrivateAddress, out connection))
                {
                    // By this point the connection should not have a public address index, and if it has 
                    // a private address index then the connection should not be connected.
                    Debug.Assert(!connection.IsConnected, "Connection that is only indexed on private address is still connected.");
                    this.DeleteConnection(peersPrivateAddress);
                }

                connection = this.CreateConnection(peersPrivateAddress, dispatcher);
                this.SetAliasAddress(peersPublicAddress, peersPrivateAddress);
            }
            else
            {
                connection = this.CreateConnection(peersPublicAddress, dispatcher);
            }

            if (connection != null)
            {
                // Begin initialization process
                try
                {
                    switch (initiationMethod)
                    {
                        case CommunicationMethod.Direct:
                            connection.BeginInitialization();
                            break;

                        case CommunicationMethod.None:
                            Logger.TraceWarning(LogTag.Connection, "Cannot form connection with {0}", peersPublicAddress);
                            this.DeleteConnection(peersPublicAddress);
                            return null;

                        case CommunicationMethod.Relay:
                            Logger.TraceInformation(LogTag.Connection, "Using relay for connection with {0}", peersPublicAddress);
                            Debug.Assert(connection.IsRelayed, "Trying to initiate a connection using relay but dispatcher is incorrect.");                            
                            connection.BeginInitialization();
                            break;

                        case CommunicationMethod.UnidirectionalHolePunch:
                            Logger.TraceInformation(LogTag.Connection, "Initiating uni-direction hole punch.");
                            this.InitiateHolePunchRequest(peersPublicAddress);

                            NatType localNatType = this.addressProvider.PublicAddress.NatType;
                            if ((peersPublicAddress.NatType == NatType.SymmetricNat)
                                && (localNatType == NatType.RestrictedPort || localNatType == NatType.RestrictedCone))
                            {
                                this.PunchUDPHole(peersPublicAddress);
                            }
                            // The connection will begin initialization once the hole punch is confirmed.
                            // It is delayed because we would simply be wasting traffic and attempts, which can mean
                            // the connection takes longer to connect.

                            break;
                    }
                }
                catch (Exception e)
                {
                    Logger.TraceException(LogLevel.Error, e, "Failed to initialize connection to {0} using {1}", peersPublicAddress, initiationMethod);
                    this.CleanupConnection(peersPublicAddress);

                    return null;
                }

            }

            return connection;
        }

        /// <summary>
        /// Index the the given actual address with the given alias address.        
        /// </summary>
        /// <remarks>
        /// If the actual address has not been previously entered into the ConnectionTable, this operation will return immediately.
        /// </remarks>
        /// <param name="publicAddress">The public alias of the actual address.</param>
        /// <param name="actualAddress">The address of the previously entered connection.</param>
        public void SetAliasAddress(PeerAddress publicAddress, PeerAddress actualAddress)
        {
            Connection actualConnection = null;
            Connection aliasedConnection = null;

            if (publicAddress.Equals(actualAddress) || !this.mConnections.TryGetValue(actualAddress, out actualConnection))
            {
                return;
            }

            Debug.Assert(actualConnection.PeersPublicAddress.Equals(actualAddress) || actualConnection.PeersPublicAddress.Equals(publicAddress),
                "Attempting to alias the same connection more than once.");

            if (this.mConnections.TryGetValue(publicAddress, out aliasedConnection))
            {
                if (aliasedConnection.PeersPrivateAddress.Equals(actualAddress))
                {
                    return;
                }

                actualConnection.UsurpConnection(aliasedConnection);
            }

            this.AddConnection(publicAddress, actualConnection);

            actualConnection.SetPublicAddress(publicAddress);
            Logger.TraceInformation(LogTag.Connection, "{0} has been aliased as {1}", actualAddress, publicAddress);
        }

        #endregion

        #region Message handling

        private Connection GetOrInitiateConnection(PeerAddress destination)
        {
            if (null == destination || !destination.IsValid)
            {
                Debug.Assert(false, "Attempt to create connection with invalid address.");
                Logger.TraceError(LogTag.Connection, "Attempt to create connection with invalid address.");
                throw new ArgumentException("destination");
            }

            Connection connection;
            if (this.mConnections.TryGetValue(destination, out connection))
            {
                if (connection.IsGarbage)
                {
                    connection.BeginInitialization();
                }
            }
            else
            {
                connection = this.InitiateConnection(destination, null, RendezvousMatrix.GetInitiationMethodFor(this.addressProvider.PublicAddress.NatType, destination.NatType));
                if (connection == null)
                {
                    Logger.TraceError(LogTag.Connection, "Failed to initialize connection with {0}", destination);
                    return null;
                }
            }

            return connection;
        }

        public void PrepareMessage(ref TransportEnvelope envelope)
        {
            this.eventQueue.AssertHasPerformLock();

            if (null == envelope.Destination || !envelope.Destination.IsValid)
            {
                Debug.Assert(false, "Attempt to create connection with invalid address.");
                Logger.TraceError(LogTag.Connection, "Attempt to create connection with invalid address.");
                return;
            }

            if (envelope.Destination.Equals(this.addressProvider.PublicAddress))
            {
                // The envelope will be handled at this layer so no need for parent indexing.
                envelope.Message.DropLastNBytes(envelope.Length);
                return;
            }

            if (envelope.Destination.IsDhtAddress)
            {
                // Dht messages are entered at the connection table level - so no preliminary stuff is required.
                envelope.Message.DropLastNBytes(envelope.Length);
                return;
            }

            Connection connection = this.GetOrInitiateConnection(envelope.Destination);
            if (connection == null)
            {
                return;  // TODO: This matches old behaviour, and it probably never happens.  Check whether it can, and recover appropriately if necessary.
            }

            this.PiggyBackMessage(connection, ref envelope);
            Connection.WriteHeader(envelope);
            envelope.SetDestination(connection.PeersPublicAddress);
            Debug.Assert(envelope.Header != null, "No header");
        }

        public override void PrepareMessageForDeparture(ref TransportEnvelope envelope)
        {
            this.PrepareMessage(ref envelope);
        }

        private void PiggyBackMessage(Connection connection, ref TransportEnvelope envelope)
        {
            TransportEnvelope piggybackEnvelope = connection.GetMessageForPiggyback(envelope.Qos);

            if (piggybackEnvelope != null)
            {
#if TRACE
                piggybackEnvelope.SetLabel(piggybackEnvelope.Label + envelope.Label);
#endif
                TransportEnvelope preambleEnvelope = new TransportEnvelope(envelope.Destination, envelope.Qos);

                MessageBuffer preamble = preambleEnvelope.Message;
                MessageBuffer newMessage = piggybackEnvelope.Message;
                while (preamble.Available > 0)
                {
                    newMessage.Write(preamble.ReadByte());
                }

                envelope = piggybackEnvelope;
            }
        }

        public void FailedRetryHandler(TransportEnvelope messageEnvelope, PeerAddress address)
        {
            Logger.TraceWarning(LogTag.Connection, "Failed to send reliable message {0} to {1}. Max retries reached.", messageEnvelope, address);

            Connection connection = null;
            if (this.mConnections.TryGetValue(address, out connection))
            {
                if (connection.IsInitializing)
                {
                    if (connection.ShouldTryRelay)
                    {
                        // Attempt an alternate method of connection.
                        this.AttemptRelayWith(connection);
                        return;
                    }
                }

                if (connection.IsConnected)
                {
                    // clean up the dead connection
                    Logger.TraceInformation(LogTag.Connection, "Going to trigger the connection lost event and clean up the connection.");
                    this.ConnectionLostWith(connection.PeersPublicAddress);
                    this.CleanupConnection(address);
                }
            }
        }

        /// <summary>
        /// Returns the loss rate for the connection to the specified address, or 0 if 
        /// no connection is found.
        /// </summary>
        /// <param name="address">The address for the connection</param>
        /// <returns>The connection's loss rate, or 0 if the connection was not found.</returns>
        public double GetConnectionLossRate(PeerAddress address)
        {
            Connection connection;
            if (!this.mConnections.TryGetValue(address, out connection))
            {
                return 0;
            }

            return connection.PacketLossRate;
        }

        /// <summary>
        /// Returns the total outbound traffic for this peer.
        /// </summary>
        /// <returns>The total outbound traffic (i.e. Total bytes sent per second).</returns>
        public double GetTotalOutboundTraffic()
        {
            return (double)this.TotalBytesSentPerSecond;
        }

        public void DispatchMessage(TransportEnvelope envelope)
        {
            this.eventQueue.AssertHasPerformLock();

            if (null == envelope.Destination || !envelope.Destination.IsValid)
            {
                return;
            }

            if (envelope.Length > Parameters.MaximumMessageLength)
            {
                Logger.TraceError(LogTag.Connection, "Dropping message to {0} because it is too large", envelope.Destination);
            }

            if (envelope.Destination.Equals(this.addressProvider.PublicAddress))
            {
                TransportEnvelope newEnvelope = new TransportEnvelope(envelope.Message, this.addressProvider.PublicAddress, envelope.IsEncrypted, this.timeKeeper.Now);
                
                // since we're not *really* sending the envelope via the transport stack,
                // the certificate doesn't get populated via normal means - we just assign it directly.
                newEnvelope.Certificate = this.tokens.GetUserCertificateToken();

                Logger.RecordStatistic("Transport", "ConnectionTableLoopbackBytes", newEnvelope.Length);
                Logger.RecordStatistic("Transport", "ConnectionTableLoopbackPackets", 1.0);

                // Schedule the arrival as a network event to prevent any re-entrancy issues that might be caused by
                // processing the message directly here.
                newEnvelope.Message.ReadOffset = 0;
                this.eventQueue.Push(this.ProcessConnectionfulMessageBody, newEnvelope,
                    this.addressProvider.PublicAddress, this.addressProvider.PublicAddress);
                return;
            }

            if (envelope.Destination.IsDhtAddress)
            {
                if (null != this.mDhtTranport)
                {
                    this.mDhtTranport.Send(envelope);
                }
                else
                {
                    Logger.TraceError(LogTag.Connection, "Dropping message intended for dht because there is no dht transport available");
                }
                return;
            }

            Connection connection = null;

            if (!this.mConnections.TryGetValue(envelope.Destination, out connection))
            {
                Logger.TraceError(LogTag.Connection, "Failed to find connection to {0} in Connection Table", envelope.Destination);

                // failed to find the connection to the specified destination, handle the failure by passing it
                // to the qos. 
                QualityOfService qos = envelope.Qos;
                if (null != qos)
                {
                    this.eventQueue.Push(
                    delegate()
                    {
                        qos.HandleConnectionFailure(envelope.Destination);
                    });
                }
                

                return;
            }

            connection.SendMessage(envelope);
        }

        public bool ProcessHeader(TransportEnvelope envelope)
        {
            PeerAddress sourcesPublicAddress = null;

            return this.ProcessHeader(envelope, out sourcesPublicAddress);
        }

        public bool ProcessHeader(TransportEnvelope envelope, out PeerAddress sourcesPublicAddress)
        {
            try
            {
                Connection connection;
                if (this.mConnections.TryGetValue(envelope.Source, out connection))
                {
                    sourcesPublicAddress = connection.PeersPublicAddress;
                    return connection.ShouldAcceptAndProcessMessage(envelope, envelope.Header);
                }
            }
            catch (Exception ex)
            {
                Logger.TraceException(LogLevel.Warning, ex, "Got exception while trying to process header");
            }

            sourcesPublicAddress = null;
            return false;
        }

        public void ProcessRelayMessage(object sender, RelayMessageArrivalEventArgs e)
        {
            e.MessageEnvelope.WasRelayed = true;

            try
            {
                this.ProcessMessage(e.MessageEnvelope);
            }
            catch (Exception ex)
            {
                Logger.TraceException(LogLevel.Error, ex, "Failed to process relay message");
            }
        }

        private ConnectionHeader ReadHeader(TransportEnvelope envelope)
        {
            envelope.Message.ReadOffset = 0;
            if (envelope.Available < ConnectionHeader.Length)
            {
                Logger.TraceWarning(LogTag.Connection, "Message from {0} is not long enough to contain a header.", envelope.Source);
                return null;
            }

            envelope.Header = envelope.Message.Read<ConnectionHeader>();
            return envelope.Header;
        }

        private bool DecryptAndValidateMessage(TransportEnvelope envelope)
        {
            Connection connection = null;

            if (this.mConnections.TryGetValue(envelope.Source, out connection))
            {
                if (connection.DecryptMessage(envelope, this.mChecksumSalt))
                {
                    envelope.SetEstimatedRoundTripTime(this.timeKeeper.Now, connection.RoundTripTimeEstimate);
                    ConnectionHeader connectionHeader = this.ReadHeader(envelope);
                    if (connectionHeader != null)
                    {
                        return true;
                    }
                    else
                    {
                        Logger.TraceWarning(LogTag.Connection, "Failed to read the connection header.");
                        return false;
                    }
                }
            }

            // connection not found, drop all non connectionless messages
            if (envelope.Header != null && !envelope.Header.IsConnectionless)
            {
                Logger.TraceWarning(LogTag.Connection, "Dropping a connectionful message, the claimed connection does not exist.");
                return false;
            }

            SymmetricKeyToken token = this.tokens.GetNetworkParticipationToken();
            if (token != null)
            {
                try
                {
                    if (!envelope.Decrypt(token.Key, this.mChecksumSalt))
                    {
                        Logger.TraceWarning(LogTag.Connection, "envelope.decrypt failed.");
                        return false;
                    }
                }
                catch (ArgumentException)
                {
                    Logger.TraceWarning(LogTag.Connection, "ArgumentException when trying to decrypt the message using token.key.");
                }
            }
            else
            {
                Logger.TraceWarning(LogTag.Connection, "Failed to find authorization token for JoinNetwork. Dropping inbound packet!");
                Logger.RecordStatistic("Transport", "UndecryptableBytesDropped", envelope.Length);
                return false;
            }

            // Check that the message is connectionless. If it is not we must ignore it, otherwise it could be possible 
            // to encrypt a message using the commonly known network participation key which would be accepted.
            ConnectionHeader header = this.ReadHeader(envelope);
            if (header != null)
            {
                // Unfortunately, we have to process the header if it exists because it may contain acknowledgements that 
                // were encrypted with the participation key prior to the session key being set. 

                // TODO : Come up with a better way to do this, it is a potential security hole. Malicious peers may be able to
                // spoof acknowledgements. There might be other places where acks are accepted using the participation key 
                // too. Check them all.
                if (!header.IsConnectionless && connection != null)
                {
                    // Should be false but it can't because it may be the acknowledgement required to comlete
                    // the connection initialization process.
                    return !connection.IsConnected;
                }

                return true;
            }

            return false;
        }

        #region IMessageConsumer<TransportEnvelope> Members

        // Process a message that has just arrived on this peer.
        public void ProcessMessage(TransportEnvelope envelope)
        {
            if (envelope.Length == 0)
            {
                return;
            }

            if (!this.DecryptAndValidateMessage(envelope)) // will attach the header if true
            {
                Logger.TraceWarning(LogTag.Connection, "Failed to decrypt message. Message dropped");
                Logger.RecordStatistic("Transport", "UndecryptableBytesDropped", envelope.Length);
                return;
            }

            if (envelope.Header != null)
            {
                if (!envelope.Header.IsConnectionless) // Internal and connectionless messages are accepted automaticaly.
                {
                    PeerAddress sourcesPublicAddress = null;
                    if (this.ProcessHeader(envelope, out sourcesPublicAddress))
                    {
                        Logger.TraceInformation(LogTag.Connection, "Processing header message {0} from {1}", envelope.Header.SequenceNumber, sourcesPublicAddress);
                        this.ProcessConnectionfulMessageBody(envelope, sourcesPublicAddress, this.addressProvider.PublicAddress);
                        return;
                    }
                    else
                    {
                        return;
                    }
                }
            }
            else
            {
                Logger.TraceError(LogTag.Connection, "Message from {0} does not have a header. Dropping message.", envelope.Source);
                return;
            }

#if TRACE
            envelope.SetLabel("Connectionless message");
#endif
            base.ParseMessage(envelope, envelope.Message.ReadOffset);
        }

        public override void ProcessMessageBody(TransportEnvelope envelope, PeerAddress sourcesPublicAddress, PeerAddress destination)
        {
            ConnectionHeader header = envelope.Header;
            Logger.TraceInformation(LogTag.Connection, "Processing message body {0} from {1}", header.SequenceNumber, envelope.Source);

            if (!header.IsConnectionless)
            {
                this.ProcessConnectionfulMessageBody(envelope, sourcesPublicAddress, destination);
            }
            else
            {
                base.ParseMessage(envelope, envelope.Message.ReadOffset);
            }
        }

        public void ProcessConnectionfulMessageBody(TransportEnvelope envelope, PeerAddress sourcesPublicAddress, PeerAddress destination)
        {
            while (envelope.Available > 0)
            {
                envelope.Source = sourcesPublicAddress;
                envelope.SetDestination(destination);
                this.mProtocolRoot.ParseMessage(envelope, envelope.Message.ReadOffset);
            }
        }

        #endregion

        #endregion

        /// <summary>
        /// Indicates that a connection has been formed with the peer at the given address.
        /// </summary>
        /// <remarks>
        /// This should only be be called by Connection.
        /// </remarks>
        /// <param name="address"></param>
        public override void ConnectionEstablishedWith(PeerAddress address)
        {
            if (address == null)
            {
                throw new ArgumentNullException("address");
            } 
            
            if (this.mAttemptReconnectTask.IsRunning)
            {
                this.mAttemptReconnectTask.Stop();
            }

            ConnectionHandler establishedHandler = this.connectionEstablishedEventDelegate;
            if (establishedHandler != null)
            {
                this.eventQueue.Push(delegate() { establishedHandler(address); });
            }
        }

        private void ConnectionLostWith(PeerAddress address)
        {
            if (address == null)
            {
                throw new ArgumentNullException("address");
            }

            ConnectionHandler lostHandler = this.connectionLostEventDelegate;
            if (lostHandler != null)
            {
                this.eventQueue.Push(delegate() { lostHandler(address); });
            }
        }
    }
}
