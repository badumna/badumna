//-----------------------------------------------------------------------
// <copyright file="PersistentHttpConnection.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2010 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Net;
using System.Net.Sockets;

using Badumna.Utilities;

namespace Badumna.Transport
{
    /// <summary>
    /// Maintains a persistent connection with an HTTP server for sending SimpleHttpWebRequests.
    /// </summary>
    internal class PersistentHttpConnection
    {
        /// <summary>
        /// The TcpClient connection to the HTTP server
        /// </summary>
        private TcpClient tcpClient = PersistentHttpConnection.MakeTcpClient();

        /// <summary>
        /// The URI of the HTTP server
        /// </summary>
        private Uri serverUri;

        /// <summary>
        /// Initializes a new instance of the PersistentHttpConnection class.
        /// </summary>
        /// <param name="serverUri">The URI of the server to persistently connect to.</param>
        public PersistentHttpConnection(Uri serverUri)
        {
            this.serverUri = serverUri;
        }

        /// <summary>
        /// Gets the response for a SimpleHttpWebRequest using the persistent connection.
        /// </summary>
        /// <param name="request">The request to make</param>
        /// <returns>The reponse from the request</returns>
        public SimpleWebResponse GetResponse(SimpleHttpWebRequest request)
        {
            int retries = 2;  // At the end of our retries we'll throw an exception

            while (true)
            {
                retries--;

                try
                {
                    this.EnsureConnected();
                }
                catch (Exception e)
                {
                    throw new WebException("Failed to connect to server", e, WebExceptionStatus.ConnectFailure, null);
                }

                try
                {
                    bool mustClose;
                    SimpleWebResponse response = request.GetResponse(this.tcpClient, out mustClose);

                    if (mustClose)
                    {
                        this.tcpClient.Close();
                        this.tcpClient = PersistentHttpConnection.MakeTcpClient();
                        Logger.TraceInformation(LogTag.Transport, "Closing persistent connection on server request");
                    }

                    return response;
                }
                catch (WebException webException)
                {
                    if (retries <= 0 || (webException.Status != WebExceptionStatus.UnknownError && webException.Status != WebExceptionStatus.ConnectionClosed))
                    {
                        throw;
                    }

                    Logger.TraceInformation(LogTag.Transport, "Retrying HTTP connection");

                    /* Assuming that the error was that the server closed our persistent connect, we should
                     * form a new connection and retry the request.  TODO: Properly check that the server
                     * did in fact close our connection. */

                    this.tcpClient.Close();
                    this.tcpClient = PersistentHttpConnection.MakeTcpClient();
                }
            }
        }

        /// <summary>
        /// Closes the connection.  The instance cannot be used after this method has been called.
        /// </summary>
        public void Close()
        {
            this.tcpClient.Close();
        }

        /// <summary>
        /// Creates a TcpClient configured for the persistent connection.
        /// </summary>
        /// <returns>The new TcpClient</returns>
        private static TcpClient MakeTcpClient()
        {
            TcpClient client = new TcpClient();
            client.NoDelay = true;
            return client;
        }

        /// <summary>
        /// Ensure the the TCP client has formed a connection to the server at some point (it
        /// may have failed by now).
        /// </summary>
        private void EnsureConnected()
        {
            if (!this.tcpClient.Connected)
            {
                IPAddress address;

                if (IPAddress.TryParse(this.serverUri.Host, out address))
                {
                    this.tcpClient.Connect(address, this.serverUri.Port);
                }
                else
                {
                    this.tcpClient.Connect(this.serverUri.Host, this.serverUri.Port);
                }
            }
        }
    }
}
