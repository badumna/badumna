﻿//---------------------------------------------------------------------------------
// <copyright file="UDPSendThread.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Net;
using System.Threading;

using Badumna.Platform;
using Badumna.Utilities;

namespace Badumna.Transport
{
    /// <summary>
    /// The out going udp packets are all sent in this UDP send thread so the Network Event Queue will
    /// not be blocked on I/O. 
    /// </summary>
    internal class UDPSendThread
    {
        /// <summary>
        /// The lock for the queue.
        /// </summary>
        private readonly object queueLock = new object();

        /// <summary>
        /// The queue used for storing packet details.
        /// </summary>
        private Queue<PacketDetails> queue;

        /// <summary>
        /// The send thread.
        /// </summary>
        private Thread thread;

        /// <summary>
        /// The push signal.
        /// </summary>
        private AutoResetEvent pushSignal;

        /// <summary>
        /// Whether the thread is requested to be terminated.
        /// </summary>
        private volatile bool sendThreadTerminated;

        /// <summary>
        /// The socket used to send packet.
        /// </summary>
        private Socket socket;

        /// <summary>
        /// The delegate used for collecting stats.
        /// </summary>
        private Action<int> statsCollector;

        /// <summary>
        /// Initializes a new instance of the <see cref="UDPSendThread"/> class.
        /// </summary>
        /// <param name="socket">The socket.</param>
        /// <param name="statsCollector">The stats collector.</param>
        public UDPSendThread(Socket socket, Action<int> statsCollector)
        {
            this.socket = socket;
            this.statsCollector = statsCollector;
            this.queue = new Queue<PacketDetails>();
            this.pushSignal = new AutoResetEvent(false);
            this.sendThreadTerminated = false;

            this.thread = new Thread(this.SendThreadLoop);
            this.thread.IsBackground = true;
            this.thread.Start();
        }

        /// <summary>
        /// Adds the specified data to the queue.
        /// </summary>
        /// <param name="data">The data of the packet.</param>
        /// <param name="endpoint">The endpoint.</param>
        public void Add(byte[] data, IPEndPoint endpoint)
        {
            this.AddPacket(data, endpoint);
        }

        /// <summary>
        /// Shutdowns this instance.
        /// </summary>
        public void Shutdown()
        {
            this.sendThreadTerminated = true;
            this.pushSignal.Set();

            this.thread.Join(TimeSpan.FromSeconds(1));
        }

        /// <summary>
        /// Gets the packet.
        /// </summary>
        /// <returns>The packet in the top of the queue.</returns>
        private PacketDetails GetPacket()
        {
            lock (this.queueLock)
            {
                return this.queue.Dequeue();
            }
        }

        /// <summary>
        /// Adds the packet.
        /// </summary>
        /// <param name="data">The data array.</param>
        /// <param name="endpoint">The endpoint.</param>
        private void AddPacket(byte[] data, IPEndPoint endpoint)
        {
            PacketDetails pd = new PacketDetails(data, endpoint);

            lock (this.queueLock)
            {
                this.queue.Enqueue(pd);
            }

            this.pushSignal.Set();
        }

        /// <summary>
        /// The main loop of the send thread.
        /// </summary>
        private void SendThreadLoop()
        {
            while (!this.sendThreadTerminated)
            {
                this.pushSignal.WaitOne(1000, false);

                while (this.queue.Count > 0)
                {
                    PacketDetails pd = this.GetPacket();
                    if (pd != null)
                    {
                        this.SendPacket(pd);
                    }

                    if (this.sendThreadTerminated)
                    {
                        break;
                    }
                }
            }
        }

        /// <summary>
        /// Sends the packet.
        /// </summary>
        /// <param name="packet">The packet.</param>
        private void SendPacket(PacketDetails packet)
        {
            try
            {
                int numSent = this.socket.SendTo(packet.Data, packet.EndPoint);
                this.statsCollector(numSent);
            }
            catch (Exception e)
            {
                Logger.TraceException(
                    LogLevel.Warning,
                    e,
                    "SendPacket failed, packet length = {0}, endpoint = {1}",
                    packet.Data.Length,
                    packet.EndPoint);
            }
        }

        /// <summary>
        /// The packet details.
        /// </summary>
        private class PacketDetails
        {
            /// <summary>
            /// The end point.
            /// </summary>
            private IPEndPoint endpoint;

            /// <summary>
            /// The packet data.
            /// </summary>
            private byte[] data;

            /// <summary>
            /// Initializes a new instance of the <see cref="UDPSendThread.PacketDetails"/> class.
            /// </summary>
            /// <param name="data">The data of the packet.</param>
            /// <param name="endpoint">The endpoint.</param>
            public PacketDetails(byte[] data, IPEndPoint endpoint)
            {
                this.data = data;
                this.endpoint = endpoint;
            }

            /// <summary>
            /// Gets or sets the data.
            /// </summary>
            /// <value>The data array.</value>
            public byte[] Data
            {
                get { return this.data; }
                set { this.data = value; }
            }

            /// <summary>
            /// Gets or sets the end point.
            /// </summary>
            /// <value>The end point.</value>
            public IPEndPoint EndPoint
            {
                get { return this.endpoint; }
                set { this.endpoint = value; }
            }
        }
    }
}
