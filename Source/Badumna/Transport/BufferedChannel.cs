﻿using System;
using System.Collections.Generic;
using System.Text;
using Badumna.Core;

namespace Badumna.Transport
{
    // TODO: Turn this into a Random Early Discard buffer?
    class BufferedChannel<T> : Channel<T> where T : ISized
    {
        public int Count { get { return this.mQueue.Count; } }

        public override bool IsEmpty
        {
            get { return this.mQueue.Count == 0; }
        }

        private Queue<T> mQueue = new Queue<T>();

        public BufferedChannel(ITime timeKeeper)
            : base(timeKeeper)
        {
        }

        protected override void DoPut(T item)
        {
            this.mQueue.Enqueue(item);
        }

        protected override T DoGet()
        {
            if (this.mQueue.Count == 0)
            {
                return default(T);
            }

            return this.mQueue.Dequeue();
        }

        public override T Peek()
        {
            if (this.mQueue.Count == 0)
            {
                return default(T);
            }

            return this.mQueue.Peek();
        }

        public override void Clear()
        {
            this.mQueue.Clear();
        }
    }
}
