﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Net;

using Badumna.Core;
using Badumna.Platform;
using Badumna.Utilities;

namespace Badumna.Transport
{

    interface ISocketWrapper
    {
        void SendTo(byte[] message, IPEndPoint destination);
        int ReceiveFrom(byte[] buffer, ref EndPoint endpoint, int timeoutMilliseconds);
    }

    class SocketSiphon : ISocketWrapper
    {
        private Socket mSocket;
        private AutoResetEvent mPacketSignal = new AutoResetEvent(false);
        private Queue<TransportEnvelope> mQueue = new Queue<TransportEnvelope>();

        /// <summary>
        /// The queue to use for scheduling events.
        /// </summary>
        private NetworkEventQueue eventQueue;

        public SocketSiphon(Socket socket, NetworkEventQueue eventQueue)
        {
            this.mSocket = socket;
            this.eventQueue = eventQueue;
        }
        
        public void ReceivePacket(TransportEnvelope envelope)
        {
            lock (this.mQueue)
            {
                this.mQueue.Enqueue(envelope);
            }
            this.mPacketSignal.Set();
        }

        private void Send(TransportEnvelope envelope)
        {
            this.mSocket.SendTo(envelope.Message.GetBuffer(), envelope.Destination.EndPoint);
        }

        #region ISocketWrapper Members

        public void SendTo(byte[] message, IPEndPoint destination)
        {
            TransportEnvelope envelope = new TransportEnvelope(new PeerAddress(destination, NatType.Unknown), QualityOfService.Unreliable);

            envelope.Message.Write(message, message.Length);
            this.eventQueue.Push(this.Send, envelope);
        }

        public int ReceiveFrom(byte[] buffer, ref EndPoint endpoint, int timeoutMilliseconds)
        {
            if (this.mQueue.Count == 0)
            {
                if (!this.mPacketSignal.WaitOne(timeoutMilliseconds, false))
                {
                    return 0;
                }
            }

            TransportEnvelope envelope = null;
 
            lock (this.mQueue)
            {
                if (this.mQueue.Count > 0)
                {
                    envelope = this.mQueue.Dequeue();
                }
            }

            if (envelope != null)
            {
                endpoint = envelope.Source.EndPoint;
                return envelope.Message.ReadBytes(buffer, buffer.Length);
            }

            return this.ReceiveFrom(buffer, ref endpoint, timeoutMilliseconds);
        }

        #endregion
    }
}
