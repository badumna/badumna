﻿//------------------------------------------------------------------------------
// <copyright file="ITransport.cs" company="National ICT Australia Limited">
//     Copyright (c) 2012 National ICT Australia Limited. All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using Badumna.Core;

namespace Badumna.Transport
{
    /// <summary>
    /// A transport that sends and receives TransportEnvelopes.
    /// </summary>
    internal interface ITransport : IMessageDispatcher<TransportEnvelope>, IMessageProducer<TransportEnvelope>, INetworkAddressProvider
    {
        /// <summary>
        /// Gets the estimated inbound bandwidth use in bytes/second.
        /// </summary>
        double InboundBytesPerSecond { get; }

        /// <summary>
        /// Gets the estimated outbound bandwidth use in bytes/second.
        /// </summary>
        double OutboundBytesPerSecond { get; }

        /// <summary>
        /// Gets a value indicating whether initialization has completed (regardless of its success).
        /// </summary>
        /// <remarks>
        /// Once this value is true the outcome of the initialization can be retrieved from IsInitialized.
        /// </remarks>
        bool HasFinishedInitializing { get; }

        /// <summary>
        /// Gets a value indicating whether initialization has completed successfully and the socket is listening for packets.
        /// </summary>
        /// <remarks>
        /// This value is only valid when HasFinishedInitializating is true.
        /// </remarks>
        bool IsInitialized { get; }

        /// <summary>
        /// Initializes the transport layer;
        /// </summary>
        /// <remarks>
        /// See HasFinishedInitializing and IsInitialized for the status of the initialization.
        /// </remarks>
        void Initialize();

        /// <summary>
        /// Initializes the transport layer.
        /// </summary>
        /// <param name="blockUntilComplete">A value indicating whether the initialization should be synchronous.</param>
        /// <remarks>
        /// See HasFinishedInitializing and IsInitialized for the status of the initialization.
        /// </remarks>
        void Initialize(bool blockUntilComplete);

        /// <summary>
        /// Shuts down the transport layer.
        /// </summary>
        void Shutdown();
    }
}
