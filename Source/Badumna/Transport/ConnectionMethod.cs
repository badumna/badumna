﻿using System;
using System.Collections.Generic;
using System.Text;

using Badumna.Core;

namespace Badumna.Transport
{
    enum CommunicationMethod
    {
        None,
        Direct,
        UnidirectionalHolePunch,
        Relay
    }

    static class RendezvousMatrix
    {
        #region Connection method matrix
        private static CommunicationMethod[,] mNatTraversalMatrix = null;

        /// <summary>
        /// Returns a value indicating the communication method to use for the given pair of NAT types.
        /// </summary>
        /// <param name="localNatType">The NAT type of this peer.</param>
        /// <param name="destinationNatType">The NAT type of the destination.</param>
        /// <returns>The communication method that should be used.</returns>
        public static CommunicationMethod GetInitiationMethodFor(NatType localNatType, NatType destinationNatType)
        {
            if (destinationNatType == NatType.Internal)
            {
                return CommunicationMethod.Direct;
            }

            if (null == RendezvousMatrix.mNatTraversalMatrix)
            {
                RendezvousMatrix.InitializeTraversalMatrix();
            }

            return RendezvousMatrix.mNatTraversalMatrix[(int)localNatType, (int)destinationNatType];
        }

        private static void InitializeTraversalMatrix()
        {
            RendezvousMatrix.mNatTraversalMatrix = new CommunicationMethod[9, 9];

            RendezvousMatrix.mNatTraversalMatrix[(int)NatType.Blocked, (int)NatType.Blocked] = CommunicationMethod.None;
            RendezvousMatrix.mNatTraversalMatrix[(int)NatType.Blocked, (int)NatType.FullCone] = CommunicationMethod.None;
            RendezvousMatrix.mNatTraversalMatrix[(int)NatType.Blocked, (int)NatType.MangledFullCone] = CommunicationMethod.None;
            RendezvousMatrix.mNatTraversalMatrix[(int)NatType.Blocked, (int)NatType.Open] = CommunicationMethod.None;
            RendezvousMatrix.mNatTraversalMatrix[(int)NatType.Blocked, (int)NatType.RestrictedCone] = CommunicationMethod.None;
            RendezvousMatrix.mNatTraversalMatrix[(int)NatType.Blocked, (int)NatType.RestrictedPort] = CommunicationMethod.None;
            RendezvousMatrix.mNatTraversalMatrix[(int)NatType.Blocked, (int)NatType.SymmetricFirewall] = CommunicationMethod.None;
            RendezvousMatrix.mNatTraversalMatrix[(int)NatType.Blocked, (int)NatType.SymmetricNat] = CommunicationMethod.None;
            RendezvousMatrix.mNatTraversalMatrix[(int)NatType.Blocked, (int)NatType.Unknown] = CommunicationMethod.None;

            RendezvousMatrix.mNatTraversalMatrix[(int)NatType.FullCone, (int)NatType.Blocked] = CommunicationMethod.None;
            RendezvousMatrix.mNatTraversalMatrix[(int)NatType.FullCone, (int)NatType.FullCone] = CommunicationMethod.Direct;
            RendezvousMatrix.mNatTraversalMatrix[(int)NatType.FullCone, (int)NatType.MangledFullCone] = CommunicationMethod.Direct;
            RendezvousMatrix.mNatTraversalMatrix[(int)NatType.FullCone, (int)NatType.Open] = CommunicationMethod.Direct;
            RendezvousMatrix.mNatTraversalMatrix[(int)NatType.FullCone, (int)NatType.RestrictedCone] = CommunicationMethod.UnidirectionalHolePunch;
            RendezvousMatrix.mNatTraversalMatrix[(int)NatType.FullCone, (int)NatType.RestrictedPort] = CommunicationMethod.UnidirectionalHolePunch;
            RendezvousMatrix.mNatTraversalMatrix[(int)NatType.FullCone, (int)NatType.SymmetricFirewall] = CommunicationMethod.UnidirectionalHolePunch;
            RendezvousMatrix.mNatTraversalMatrix[(int)NatType.FullCone, (int)NatType.SymmetricNat] = CommunicationMethod.UnidirectionalHolePunch;
            RendezvousMatrix.mNatTraversalMatrix[(int)NatType.FullCone, (int)NatType.Unknown] = CommunicationMethod.Direct;

            RendezvousMatrix.mNatTraversalMatrix[(int)NatType.MangledFullCone, (int)NatType.Blocked] = CommunicationMethod.None;
            RendezvousMatrix.mNatTraversalMatrix[(int)NatType.MangledFullCone, (int)NatType.FullCone] = CommunicationMethod.Direct;
            RendezvousMatrix.mNatTraversalMatrix[(int)NatType.MangledFullCone, (int)NatType.MangledFullCone] = CommunicationMethod.Direct;
            RendezvousMatrix.mNatTraversalMatrix[(int)NatType.MangledFullCone, (int)NatType.Open] = CommunicationMethod.Direct;
            RendezvousMatrix.mNatTraversalMatrix[(int)NatType.MangledFullCone, (int)NatType.RestrictedCone] = CommunicationMethod.UnidirectionalHolePunch;
            RendezvousMatrix.mNatTraversalMatrix[(int)NatType.MangledFullCone, (int)NatType.RestrictedPort] = CommunicationMethod.Relay;
            RendezvousMatrix.mNatTraversalMatrix[(int)NatType.MangledFullCone, (int)NatType.SymmetricFirewall] = CommunicationMethod.Relay;
            RendezvousMatrix.mNatTraversalMatrix[(int)NatType.MangledFullCone, (int)NatType.SymmetricNat] = CommunicationMethod.Relay;
            RendezvousMatrix.mNatTraversalMatrix[(int)NatType.MangledFullCone, (int)NatType.Unknown] = CommunicationMethod.Direct;

            RendezvousMatrix.mNatTraversalMatrix[(int)NatType.Open, (int)NatType.Blocked] = CommunicationMethod.None;
            RendezvousMatrix.mNatTraversalMatrix[(int)NatType.Open, (int)NatType.FullCone] = CommunicationMethod.Direct;
            RendezvousMatrix.mNatTraversalMatrix[(int)NatType.Open, (int)NatType.MangledFullCone] = CommunicationMethod.Direct;
            RendezvousMatrix.mNatTraversalMatrix[(int)NatType.Open, (int)NatType.Open] = CommunicationMethod.Direct;
            RendezvousMatrix.mNatTraversalMatrix[(int)NatType.Open, (int)NatType.RestrictedCone] = CommunicationMethod.UnidirectionalHolePunch;
            RendezvousMatrix.mNatTraversalMatrix[(int)NatType.Open, (int)NatType.RestrictedPort] = CommunicationMethod.UnidirectionalHolePunch;
            RendezvousMatrix.mNatTraversalMatrix[(int)NatType.Open, (int)NatType.SymmetricFirewall] = CommunicationMethod.UnidirectionalHolePunch;
            RendezvousMatrix.mNatTraversalMatrix[(int)NatType.Open, (int)NatType.SymmetricNat] = CommunicationMethod.UnidirectionalHolePunch;
            RendezvousMatrix.mNatTraversalMatrix[(int)NatType.Open, (int)NatType.Unknown] = CommunicationMethod.Direct;

            RendezvousMatrix.mNatTraversalMatrix[(int)NatType.RestrictedCone, (int)NatType.Blocked] = CommunicationMethod.None;
            RendezvousMatrix.mNatTraversalMatrix[(int)NatType.RestrictedCone, (int)NatType.FullCone] = CommunicationMethod.Direct;
            RendezvousMatrix.mNatTraversalMatrix[(int)NatType.RestrictedCone, (int)NatType.MangledFullCone] = CommunicationMethod.Direct;
            RendezvousMatrix.mNatTraversalMatrix[(int)NatType.RestrictedCone, (int)NatType.Open] = CommunicationMethod.Direct;
            RendezvousMatrix.mNatTraversalMatrix[(int)NatType.RestrictedCone, (int)NatType.RestrictedCone] = CommunicationMethod.UnidirectionalHolePunch;
            RendezvousMatrix.mNatTraversalMatrix[(int)NatType.RestrictedCone, (int)NatType.RestrictedPort] = CommunicationMethod.UnidirectionalHolePunch;
            RendezvousMatrix.mNatTraversalMatrix[(int)NatType.RestrictedCone, (int)NatType.SymmetricFirewall] = CommunicationMethod.UnidirectionalHolePunch;
            RendezvousMatrix.mNatTraversalMatrix[(int)NatType.RestrictedCone, (int)NatType.SymmetricNat] = CommunicationMethod.UnidirectionalHolePunch;
            RendezvousMatrix.mNatTraversalMatrix[(int)NatType.RestrictedCone, (int)NatType.Unknown] = CommunicationMethod.Direct;

            RendezvousMatrix.mNatTraversalMatrix[(int)NatType.RestrictedPort, (int)NatType.Blocked] = CommunicationMethod.None;
            RendezvousMatrix.mNatTraversalMatrix[(int)NatType.RestrictedPort, (int)NatType.FullCone] = CommunicationMethod.Direct;
            RendezvousMatrix.mNatTraversalMatrix[(int)NatType.RestrictedPort, (int)NatType.MangledFullCone] = CommunicationMethod.Relay;
            RendezvousMatrix.mNatTraversalMatrix[(int)NatType.RestrictedPort, (int)NatType.Open] = CommunicationMethod.Direct;
            RendezvousMatrix.mNatTraversalMatrix[(int)NatType.RestrictedPort, (int)NatType.RestrictedCone] = CommunicationMethod.UnidirectionalHolePunch;
            RendezvousMatrix.mNatTraversalMatrix[(int)NatType.RestrictedPort, (int)NatType.RestrictedPort] = CommunicationMethod.UnidirectionalHolePunch;
            RendezvousMatrix.mNatTraversalMatrix[(int)NatType.RestrictedPort, (int)NatType.SymmetricFirewall] = CommunicationMethod.UnidirectionalHolePunch;
            RendezvousMatrix.mNatTraversalMatrix[(int)NatType.RestrictedPort, (int)NatType.SymmetricNat] = CommunicationMethod.Relay;
            RendezvousMatrix.mNatTraversalMatrix[(int)NatType.RestrictedPort, (int)NatType.Unknown] = CommunicationMethod.Direct;

            RendezvousMatrix.mNatTraversalMatrix[(int)NatType.SymmetricFirewall, (int)NatType.Blocked] = CommunicationMethod.None;
            RendezvousMatrix.mNatTraversalMatrix[(int)NatType.SymmetricFirewall, (int)NatType.FullCone] = CommunicationMethod.Direct;
            RendezvousMatrix.mNatTraversalMatrix[(int)NatType.SymmetricFirewall, (int)NatType.MangledFullCone] = CommunicationMethod.UnidirectionalHolePunch;
            RendezvousMatrix.mNatTraversalMatrix[(int)NatType.SymmetricFirewall, (int)NatType.Open] = CommunicationMethod.Direct;
            RendezvousMatrix.mNatTraversalMatrix[(int)NatType.SymmetricFirewall, (int)NatType.RestrictedCone] = CommunicationMethod.UnidirectionalHolePunch;
            RendezvousMatrix.mNatTraversalMatrix[(int)NatType.SymmetricFirewall, (int)NatType.RestrictedPort] = CommunicationMethod.UnidirectionalHolePunch;
            RendezvousMatrix.mNatTraversalMatrix[(int)NatType.SymmetricFirewall, (int)NatType.SymmetricFirewall] = CommunicationMethod.UnidirectionalHolePunch;
            RendezvousMatrix.mNatTraversalMatrix[(int)NatType.SymmetricFirewall, (int)NatType.SymmetricNat] = CommunicationMethod.Relay;
            RendezvousMatrix.mNatTraversalMatrix[(int)NatType.SymmetricFirewall, (int)NatType.Unknown] = CommunicationMethod.Direct;

            RendezvousMatrix.mNatTraversalMatrix[(int)NatType.SymmetricNat, (int)NatType.Blocked] = CommunicationMethod.None;
            RendezvousMatrix.mNatTraversalMatrix[(int)NatType.SymmetricNat, (int)NatType.FullCone] = CommunicationMethod.Direct;
            RendezvousMatrix.mNatTraversalMatrix[(int)NatType.SymmetricNat, (int)NatType.MangledFullCone] = CommunicationMethod.Relay;
            RendezvousMatrix.mNatTraversalMatrix[(int)NatType.SymmetricNat, (int)NatType.Open] = CommunicationMethod.Direct;
            RendezvousMatrix.mNatTraversalMatrix[(int)NatType.SymmetricNat, (int)NatType.RestrictedCone] = CommunicationMethod.UnidirectionalHolePunch;
            RendezvousMatrix.mNatTraversalMatrix[(int)NatType.SymmetricNat, (int)NatType.RestrictedPort] = CommunicationMethod.Relay;
            RendezvousMatrix.mNatTraversalMatrix[(int)NatType.SymmetricNat, (int)NatType.SymmetricFirewall] = CommunicationMethod.Relay;
            RendezvousMatrix.mNatTraversalMatrix[(int)NatType.SymmetricNat, (int)NatType.SymmetricNat] = CommunicationMethod.Relay;
            RendezvousMatrix.mNatTraversalMatrix[(int)NatType.SymmetricNat, (int)NatType.Unknown] = CommunicationMethod.Direct;

            // Unknown is assumed to be open
            RendezvousMatrix.mNatTraversalMatrix[(int)NatType.Unknown, (int)NatType.Blocked] = CommunicationMethod.None;
            RendezvousMatrix.mNatTraversalMatrix[(int)NatType.Unknown, (int)NatType.FullCone] = CommunicationMethod.Direct;
            RendezvousMatrix.mNatTraversalMatrix[(int)NatType.Unknown, (int)NatType.MangledFullCone] = CommunicationMethod.Direct;
            RendezvousMatrix.mNatTraversalMatrix[(int)NatType.Unknown, (int)NatType.Open] = CommunicationMethod.Direct;
            RendezvousMatrix.mNatTraversalMatrix[(int)NatType.Unknown, (int)NatType.RestrictedCone] = CommunicationMethod.UnidirectionalHolePunch;
            RendezvousMatrix.mNatTraversalMatrix[(int)NatType.Unknown, (int)NatType.RestrictedPort] = CommunicationMethod.UnidirectionalHolePunch;
            RendezvousMatrix.mNatTraversalMatrix[(int)NatType.Unknown, (int)NatType.SymmetricFirewall] = CommunicationMethod.UnidirectionalHolePunch;
            RendezvousMatrix.mNatTraversalMatrix[(int)NatType.Unknown, (int)NatType.SymmetricNat] = CommunicationMethod.UnidirectionalHolePunch;
            RendezvousMatrix.mNatTraversalMatrix[(int)NatType.Unknown, (int)NatType.Unknown] = CommunicationMethod.Direct;
        }

        #endregion
    }

}
