﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using Badumna.Core;


namespace Badumna.Transport
{
    /// <summary>
    /// Wraps a channel that forwards Ts, making a channel that accepts Ts and
    /// forwards them as Us.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="U"></typeparam>
    class ChannelAdapter<T,U> : Channel<U> where T : ISized where U : ISized
    {
        public delegate U Converter(T item);


        public override bool IsEmpty
        {
            get { return this.mSourceChannel.IsEmpty; }
        }

        private Channel<T> mSourceChannel;
        private Converter mConverter;


        public ChannelAdapter(Channel<T> sourceChannel, Converter converter, ITime timeKeeper)
            : base(timeKeeper)
        {
            this.mSourceChannel = sourceChannel;
            this.mConverter = converter;
            sourceChannel.SetUnemptiedCallback(this.ForwardNotify);
        }

        private void ForwardNotify(ISource<T> source)
        {
            Debug.Assert(source == this.mSourceChannel);
            this.InvokeUnemptiedCallback();
        }

        public void Insert(T item)
        {
            this.mSourceChannel.Put(item);
            this.mGeneratingRate.ValueEvent(item.Length);
        }

        ////public new void Put(U item)
        ////{
        ////    throw new InvalidOperationException();
        ////}
        
        protected override void DoPut(U item)
        {
            throw new InvalidOperationException();
        }

        protected override U DoGet()
        {
            return this.mConverter(this.mSourceChannel.Get());
        }

        public override U Peek()
        {
            return this.mConverter(this.mSourceChannel.Peek());
        }

        public override void Clear()
        {
            this.mSourceChannel.Clear();
        }
    }
}
