﻿//------------------------------------------------------------------------------
// <copyright file="Transport.cs" company="National ICT Australia Limited">
//     Copyright (c) 2012 National ICT Australia Limited. All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Diagnostics;

using Badumna.Core;
using Badumna.Utilities;
using Badumna.Utilities.Logging;

namespace Badumna.Transport
{
    /// <summary>
    /// Provides code common to all transports.
    /// </summary>
    internal abstract class Transport : ITransport
    {
        /// <summary>
        /// Delegate for the public address changed event.
        /// </summary>
        private GenericCallBack publicAddressChangedDelegate;

        /// <summary>
        /// Estimates the receiving rate in bytes / sec.
        /// </summary>
        private RateSmoother inboundRate;

        /// <summary>
        /// Estimates the sending rate in bytes / sec.
        /// </summary>
        private RateSmoother outboundRate;

        /// <summary>
        /// Initializes a new instance of the Transport class.
        /// </summary>
        /// <param name="eventQueue">The event queue to use for scheduling events.</param>
        /// <param name="timeKeeper">The time source</param>
        /// <param name="connectivityReporter">The network connectivity reporter</param>
        protected Transport(NetworkEventQueue eventQueue, ITime timeKeeper, INetworkConnectivityReporter connectivityReporter)
        {
            this.EventQueue = eventQueue;
            this.ConnectivityReporter = connectivityReporter;

            this.inboundRate = new RateSmoother(Parameters.BandwidthSamplingInterval, Parameters.BandwidthEstimateSettlingTime, timeKeeper);
            this.outboundRate = new RateSmoother(Parameters.BandwidthSamplingInterval, Parameters.BandwidthEstimateSettlingTime, timeKeeper);

            this.PublicAddress = PeerAddress.Nowhere;
            this.PrivateAddressesList = new List<PeerAddress>();

            this.ConnectivityReporter.NetworkOfflineEvent += delegate
            {
                this.SetAddresses(PeerAddress.Nowhere);
            };
        }

        /// <inheritdoc />
        public event GenericCallBack PublicAddressChanged
        {
            add { this.publicAddressChangedDelegate += value; }
            remove { this.publicAddressChangedDelegate -= value; }
        }

        /// <inheritdoc />
        public bool IsInitialized { get; protected set; }

        /// <inheritdoc />
        public bool HasFinishedInitializing { get; protected set; }

        /// <inheritdoc />
        public double InboundBytesPerSecond 
        {
            get { return this.inboundRate.EstimatedRate; }
        }

        /// <inheritdoc />
        public double OutboundBytesPerSecond
        {
            get { return this.outboundRate.EstimatedRate; }
        }

        /// <inheritdoc />
        public IMessageConsumer<TransportEnvelope> MessageConsumer { private get; set; }

        /// <inheritdoc />
        public bool IsAddressKnownAndValid
        {
            get
            {
                return this.PublicAddress != null && this.PublicAddress.NatType != NatType.Blocked && this.PublicAddress.IsValid && this.PrivateAddressesList.Count > 0;
            }
        }

        /// <inheritdoc />
        public PeerAddress PublicAddress { get; protected set; }

        /// <inheritdoc />
        public IEnumerable<PeerAddress> PrivateAddresses
        {
            get { return this.PrivateAddressesList; }
        }

        /// <inheritdoc />
        public int PrivatePort
        {
            get
            {
                if (this.PrivateAddressesList.Count > 0)
                {
                    return this.PrivateAddressesList[0].Port;
                }

                return -1;
            }
        }

        /// <summary>
        /// Gets the modifiable list of private addresses, with protected accessibility.
        /// </summary>
        protected List<PeerAddress> PrivateAddressesList { get; private set; }

        /// <summary>
        /// Gets the event queue to use for scheduling events.
        /// </summary>
        protected NetworkEventQueue EventQueue { get; private set; }

        /// <summary>
        /// Gets the network connectivity reporter.
        /// </summary>
        protected INetworkConnectivityReporter ConnectivityReporter { get; private set; }

        /// <inheritdoc />
        public bool HasPrivateAddress(PeerAddress address)
        {
            return this.PrivateAddressesList.Contains(address);
        }

        /// <summary>
        /// Initializes the transport layer.
        /// </summary>
        /// <remarks>
        /// Derived classes which override this method need not call this implementation -- they
        /// should simply ensure that IsInitialized and HasFinishedInitializing are set appropriately.
        /// </remarks>
        public virtual void Initialize()
        {
            this.IsInitialized = true;
            this.HasFinishedInitializing = true;
        }

        /// <inheritdoc />
        public virtual void Initialize(bool blockUntilComplete)
        {
            throw new NotSupportedException("Only used in cloud at present.");
        }

        /// <inheritdoc />
        public virtual void Shutdown()
        {
        }

        /// <inheritdoc />
        /// TODO: This method would be better not virtual -- the extension point is the abstract method SendPacket(TransportEnvelope)
        public virtual void SendMessage(TransportEnvelope envelope)
        {
            if (!envelope.Destination.IsValid)
            {
                Logger.TraceError(LogTag.Transport, "Failed to get destination address. Dropping message.");
                return;
            }

            if (this.PrivateAddressesList.Contains(envelope.Destination))
            {
                Debug.Assert(false, "Why am I sending to myself?");
                Logger.TraceError(LogTag.Transport, "Outbound message is addressed to self. Dropping.");
                return;
            }

            Debug.Assert(envelope.Message.Length == 0 || envelope.IsEncrypted, "Attempting to send an un-encrypted message.");
            Debug.Assert(envelope.Message.Length == 0 || envelope.Message.HasBeenChecksummed, "Attempting to send a message that wasn't checksummed.");
            Debug.Assert(envelope.Length <= Parameters.MaximumPacketSize, String.Format("Attempting to send a message that is too long ({0}b): {1}", envelope.Length, envelope));

            try
            {
                this.SendPacket(envelope);

                Logger.TraceInformation(
                    LogTag.Transport,
                    "Sending message {1} to {0}",
                    envelope.Destination,
                    null == envelope.Header ? "n/a" : envelope.Header.SequenceNumber.ToString());
            }
            catch (Exception e)
            {
                Logger.TraceException(LogLevel.Warning, e, "Failed to send message to {0}", envelope.Destination);
            }
        }

        /// <summary>
        /// Sends the given envelope.
        /// </summary>
        /// <remarks>
        /// This method must be overridden in derived classes to perform the actual sending via the transport.
        /// The implementation must ensure that the entire packet is sent, and that once the packet has been sent
        /// that UpdateSendStatistics(int) is called.
        /// </remarks>
        /// <param name="envelope">The envelope to be sent.</param>
        protected abstract void SendPacket(TransportEnvelope envelope);

        /// <summary>
        /// Updates the statistics associated with outbound packets.
        /// </summary>
        /// <remarks>
        /// This method should be called by the implementation of SendPacket(TransportEnvelope)
        /// once per packet sent.
        /// </remarks>
        /// <param name="bytesSent">The number of bytes sent in the packet</param>
        protected void UpdateSendStatistics(int bytesSent)
        {
            this.outboundRate.ValueEvent(bytesSent);
            Logger.RecordStatistic("Transport", "TotalBytesSent", bytesSent + Parameters.UDPHeaderLength);
            Logger.RecordStatistic("Transport", "PacketsSent", 1.0);
        }

        /// <summary>
        /// Processes a newly arrived packet.
        /// </summary>
        /// <remarks>
        /// This method must be called by derived classes upon receiving a packet.
        /// </remarks>
        /// <param name="envelope">The envelope of the new message</param>
        protected void ProcessMessageArrival(TransportEnvelope envelope)
        {
            int originalLength = envelope.Length;
            this.inboundRate.ValueEvent(originalLength);

            Logger.RecordStatistic("Transport", "TotalBytesReceived", originalLength + Parameters.UDPHeaderLength);
            Logger.RecordStatistic("Transport", "UDPHeaderBytesReceived", Parameters.UDPHeaderLength);
            Logger.RecordStatistic("Transport", "PacketsReceived", 1.0);

            Logger.TraceInformation(LogTag.Transport, "Received {0} bytes from {1}", envelope.Length, envelope.Source.ToString());

            if (this.PreProcessMessage(envelope))
            {
                if (this.MessageConsumer != null)
                {
                    this.EventQueue.PushDiscardable(this.MessageConsumer.ProcessMessage, envelope);
                }
                else
                {
                    Logger.TraceError(LogTag.Transport, "No message consumer specified. Cannot process messages. Dropping message.");
                }
            }
        }

        /// <summary>
        /// Perform any operations on the message before it is processed. Returning a value to indicate whether
        /// processing should take place.
        /// </summary>
        /// <param name="envelope">The message to pre-process.</param>
        /// <returns>True if the message should be processed as normal, false if no further processing is required.</returns>
        protected virtual bool PreProcessMessage(TransportEnvelope envelope)
        {
            if (this.ConnectivityReporter.Status == ConnectivityStatus.Offline)
            {
                return false; // Don't process the message if offline.
            }

            return true;
        }

        /// <summary>
        /// Sets the public address.
        /// </summary>
        /// <param name="publicAddress">The new public address</param>
        protected void SetAddresses(PeerAddress publicAddress)
        {
            this.SetAddresses(new List<PeerAddress>(), publicAddress);
        }

        /// <summary>
        /// Sets the private and public addresses.
        /// </summary>
        /// <param name="privateAddresses">The list of new private addresses</param>
        /// <param name="publicAddress">The new public address</param>
        protected void SetAddresses(List<PeerAddress> privateAddresses, PeerAddress publicAddress)
        {
            this.PrivateAddressesList.Clear();
            foreach (PeerAddress address in privateAddresses)
            {
                if (address != null && address.IsValid && address.Address.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                {
                    Logger.TraceInformation(LogTag.Transport, "Possible private address : {0}", address);
                    this.PrivateAddressesList.Add(new PeerAddress(address.EndPoint, NatType.Internal));
                }
            }

            this.PublicAddress = publicAddress;
            Logger.TraceInformation(LogTag.Transport | LogTag.Event, "PublicAddress set {0}", this.PublicAddress);
        }

        /// <summary>
        /// Invokes any event handlers subscribed to the PublicAddressChanged event.
        /// </summary>
        protected void InvokePublicAddressChangedHandlers()
        {
            GenericCallBack handler = this.publicAddressChangedDelegate;
            if (handler != null)
            {
                handler();
            }
        }
    }
}
