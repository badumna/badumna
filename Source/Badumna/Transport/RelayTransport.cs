using System;
using System.Collections.Generic;
using System.Text;

using Badumna.Utilities;
using Badumna.Core;

namespace Badumna.Transport
{
    internal class RelayTransport : IMessageDispatcher<TransportEnvelope>
    {
        private IRelayManager mRelayManager = null;

        public RelayTransport(IRelayManager relayManager)
        {
            this.mRelayManager = relayManager;
        }        

        public void SendMessage(TransportEnvelope envelope)
        {
            if (null == envelope.Destination || !envelope.Destination.IsValid)
            {
                throw new ArgumentException("Failed to get send to invalid address");
            }

            Logger.TraceInformation(LogTag.Transport, "Relay message {1} to {0}", envelope.Destination, envelope.Header.SequenceNumber);
            this.mRelayManager.RelayMessage(envelope, false);
        }

        public void InitializePath(PeerAddress destination)
        {
            this.mRelayManager.BuildRoutePath(destination);
        }

        public void ClosePath(PeerAddress destination)
        {
            this.mRelayManager.TearDownRoutePath(destination);
        }
    }
}
