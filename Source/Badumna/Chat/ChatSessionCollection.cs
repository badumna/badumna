//-----------------------------------------------------------------------
// <copyright file="ChatSessionCollection.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2012 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System.Collections.Generic;
using Badumna.DataTypes;
using Badumna.Security;
using Badumna.Utilities;
using Badumna.Utilities.Logging;

namespace Badumna.Chat
{
    /// <summary>
    /// Maintains a thread-safe collection of ChatSession objects, and allows performing actions across
    /// all sessions that contain a given channel.
    /// </summary>
    //// TODO: make this collection more efficient, and enforce uniqueness on Session IDs (perhaps a MulltiDict?)
    internal class ChatSessionCollection : IEnumerable<IInternalChatSession>
    {
        /// <summary>
        /// The managed sessions.
        /// </summary>
        private Dictionary<Character, IInternalChatSession> sessions;

        /// <summary>
        /// A lock object to protect access to `sessions`
        /// </summary>
        private object sessionsLock = new object();

        /// <summary>
        /// Initializes a new instance of the ChatSessionCollection class.
        /// </summary>
        public ChatSessionCollection()
        {
            this.sessions = new Dictionary<Character, IInternalChatSession>();
        }

        /// <summary>
        /// Gets the underlying IInternalChatSession values.
        /// </summary>
        /// <remarks>This accessor (and its result) should only be used while holding `this.sessionsLock`</remarks>
        private Dictionary<Character, IInternalChatSession>.ValueCollection Values
        {
            get
            {
                return this.sessions.Values;
            }
        }

        /// <summary>
        /// Perform an action on each session that has a subscription to `channel`.
        /// </summary>
        /// <param name="channel">The channel ID</param>
        /// <param name="action">The action to perform.</param>
        /// <returns>Whether the action was performed.</returns>
        public bool SessionPerform(ChatChannelId channel, GenericCallBack<IInternalChatSession> action)
        {
            int count = 0;
            lock (this.sessionsLock)
            {
                foreach (IInternalChatSession session in this.Values)
                {
                    if (session.HasChannel(channel))
                    {
                        count++;
                        action(session);
                    }
                }
            }

            if (count == 0)
            {
                Logger.TraceWarning(LogTag.Chat | LogTag.Event, "No sessions found that contain channel: " + channel + " - unable to perform action");
                return false;
            }
            else
            {
                Logger.TraceInformation(LogTag.Chat | LogTag.Event, "Dispatched action on channel " + channel + " to " + count + " sessions.");
                return true;
            }
        }

        /// <summary>
        /// Perform an action on the session with `sessionId`.
        /// </summary>
        /// <param name="sessionId">The session Id.</param>
        /// <param name="action">The action to perform.</param>
        /// <returns>Whether the action was performed.</returns>
        public bool SessionPerform(BadumnaId sessionId, GenericCallBack<IInternalChatSession> action)
        {
            lock (this.sessionsLock)
            {
                foreach (IInternalChatSession session in this.Values)
                {
                    if (session.Id == sessionId)
                    {
                        action(session);
                        Logger.TraceInformation(LogTag.Chat | LogTag.Event, "Dispatched action on session " + sessionId);
                        return true;
                    }
                }
            }

            Logger.TraceWarning(LogTag.Chat | LogTag.Event, "No session found: " + sessionId + " - unable to perform action");
            return false;
        }

        /// <summary>
        /// Add a session.
        /// </summary>
        /// <param name="session">The session.</param>
        public void Add(IInternalChatSession session)
        {
            lock (this.sessionsLock)
            {
                this.sessions.Add(session.Character, session);
            }
        }

        /// <summary>
        /// Clear the list of sessions.
        /// </summary>
        public void Clear()
        {
            lock (this.sessionsLock)
            {
                this.sessions.Clear();
            }
        }

        /// <summary>
        /// Removes the specified session.
        /// </summary>
        /// <param name="session">The session.</param>
        public void Remove(IInternalChatSession session)
        {
            lock (this.sessionsLock)
            {
                var mySession = this.sessions[session.Character];
                if (mySession != session)
                {
                    throw new KeyNotFoundException(session.Character.ToString());
                }

                this.sessions.Remove(session.Character);
            }
        }

        /// <summary>
        /// Returns the user for the given character.
        /// Throws ArgumentError if there is no such session.
        /// </summary>
        /// <param name="character">The character.</param>
        /// <returns>A chat session.</returns>
        public IInternalChatSession ForCharacter(Character character)
        {
            lock (this.sessionsLock)
            {
                return this.sessions[character];
            }
        }

        /// <summary>
        /// Returns an enumerator that iterates through the sessions.
        /// </summary>
        /// <returns>
        /// An enumerator.
        /// </returns>
        /// <remarks>This makes a copy of the elements, to avoid thread contention.</remarks>
        public IEnumerator<IInternalChatSession> GetEnumerator()
        {
            List<IInternalChatSession> result;
            lock (this.sessionsLock)
            {
                result = new List<IInternalChatSession>(this.Values);
            }

            return result.GetEnumerator();
        }

        /// <summary>
        /// Returns an enumerator that iterates through the sessions.
        /// </summary>
        /// <returns>
        /// An enumerator.
        /// </returns>
        /// <remarks>This makes a copy of the elements, to avoid thread contention.</remarks>
        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }
    }
}
