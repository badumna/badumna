using System;
using System.Collections.Generic;
using System.Text;

using Badumna.Core;
using Badumna.Utilities;
using Badumna.Multicast;
using Badumna.DataTypes;
using Badumna.Security;

namespace Badumna.Chat
{
    interface IMulticastChatProtocol
    {
        void JoinGroup(BadumnaId channelId);
        void LeaveGroup(BadumnaId channelId);
        void SendChatMessage(BadumnaId channelId, BadumnaId sessionId, String message);
        void ChangePresence(BadumnaId channelId, UserPresenceStatus presence, ChatNotificationType notificationType);
        void SendDirectPresence(BadumnaId channelId, PeerAddress destination, UserPresenceStatus presence, ChatNotificationType notificationType);
    }

    delegate IMulticastChatProtocol MulticastChatProtocolFactory(IInternalChatService chatService);

    class MulticastChatProtocol : MulticastProtocol, IMulticastChatProtocol
    {
        internal static MulticastChatProtocolFactory Factory(MulticastFacade facade)
        {
            return (chatService => new MulticastChatProtocol(facade, chatService));
        }

        private MulticastFacade mFacade;
        private IInternalChatService mChatService;
        private Dictionary<BadumnaId, BadumnaId> mMemberIds = new Dictionary<BadumnaId, BadumnaId>();

        public MulticastChatProtocol(MulticastFacade multicastFacade, IInternalChatService chatService)
            : base(multicastFacade)
        {
            this.mFacade = multicastFacade;
            this.mChatService = chatService;
        }

        public void JoinGroup(BadumnaId channelId)
        {
            if (!this.mMemberIds.ContainsKey(channelId))
            {
                BadumnaId memberId = this.mFacade.AddGroup(channelId);
                if (null != memberId)
                {
                    this.mMemberIds.Add(channelId, memberId);
                }
            }
        }

        public void LeaveGroup(BadumnaId channelId)
        {
            if (this.mMemberIds.ContainsKey(channelId))
            {
                this.mFacade.LeaveGroup(channelId);
                this.mMemberIds.Remove(channelId);
            }
        }

        public void SendChatMessage(BadumnaId channelId, BadumnaId sessionId, String message)
        {
            BadumnaId memberId = null;

            this.mMemberIds.TryGetValue(channelId, out memberId);
            if (null != memberId)
            {
                MulticastEnvelope envelope = this.GetMessageFor(channelId, QualityOfService.Reliable);

                this.RemoteCall(envelope, this.ReceiveChatMessage, sessionId.LocalId, message);
                this.SendMessage(envelope);
            }
        }

        public void ChangePresence(BadumnaId channelId, UserPresenceStatus presence, ChatNotificationType notificationType)
        {
            BadumnaId memberId = null;

            this.mMemberIds.TryGetValue(channelId, out memberId);
            if (null != memberId)
            {
                MulticastEnvelope envelope = this.GetMessageFor(channelId, QualityOfService.Reliable);
                ChatStatusNotification notification = new ChatStatusNotification(presence.Status, notificationType);
                this.RemoteCall(envelope, this.PresenceEvent, channelId, presence.SessionId.LocalId, presence.Character, notification);
                this.SendMessage(envelope);
            }
        }

        public void SendDirectPresence(BadumnaId channelId, PeerAddress destination, UserPresenceStatus presence, ChatNotificationType notificationType)
        {
            BadumnaId memberId = null;

            this.mMemberIds.TryGetValue(channelId, out memberId);
            if (null != memberId)
            {
                MulticastEnvelope envelope = this.GetDirectEnvelope(destination, channelId, QualityOfService.Reliable);
                ChatStatusNotification presenceNotification = new ChatStatusNotification(presence.Status, notificationType);

                this.RemoteCall(envelope, this.PresenceEvent, channelId, presence.SessionId.LocalId, presence.Character, presenceNotification);
                this.SendMessage(envelope);
            }
        }

        #region Protocol methods

        [MulticastProtocolMethod(MulticastMethod.ReceiveChatMessage)]
        private void ReceiveChatMessage(ushort localId, String message)
        {
            this.mChatService.ReceiveMessage(this.CurrentChannelId(), this.CurrentUserId(localId), message);
        }

        [MulticastProtocolMethod(MulticastMethod.PresenceEvent)]
        private void PresenceEvent(BadumnaId channelId, ushort localUserId, Character character, ChatStatusNotification presenceNotification)
        {
            var user = new ChatUser(this.CurrentUserId(localUserId), character);
            var presence = new UserPresenceStatus(user, presenceNotification.Status);
            this.mChatService.ReceivePresence(this.CurrentChannelId(), presence, presenceNotification.Type);
        }

        #endregion

        /// <summary>
        /// Return a channel ID for the current envelope's channel.
        /// </summary>
        /// <returns>A group channel ID</returns>
        private ChatChannelId CurrentChannelId()
        {
            return new ChatChannelId(ChatChannelType.Group, this.CurrentEnvelope.GroupKey);
        }

        /// <summary>
        /// Return the user known by `localId` in the context of the current envelope.
        /// </summary>
        /// <param name="localId"></param>
        /// <returns>A unique user ID</returns>
        private BadumnaId CurrentUserId(ushort localId)
        {
            return new BadumnaId(this.CurrentEnvelope.Source, localId);
        }
    }
}
