﻿//-----------------------------------------------------------------------
// <copyright file="PresenceSubscriptionEventArgs.cs" company="NICTA">
//     Copyright (c) 2011 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using Badumna.DataTypes;

namespace Badumna.Chat
{
    /// <summary>
    /// Presence subscription event argument.
    /// </summary>
    public class PresenceSubscriptionEventArgs : EventArgs
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PresenceSubscriptionEventArgs"/> class.
        /// </summary>
        /// <param name="requester">The requesting user.</param>
        /// <param name="subject">The subject of the request.</param>
        internal PresenceSubscriptionEventArgs(ChatUser requester, BadumnaId subject)
        {
            this.Requester = requester;
            this.Subject = subject;
        }

        /// <summary>
        /// Gets the username.
        /// </summary>
        internal ChatUser Requester { get; private set; }

        /// <summary>
        /// Gets the subject of the invitation.
        /// </summary>
        internal BadumnaId Subject { get; private set; }
    }
}
