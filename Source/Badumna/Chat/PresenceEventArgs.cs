﻿//-----------------------------------------------------------------------
// <copyright file="PresenceEventArgs.cs" company="NICTA">
//     Copyright (c) 2011 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using Badumna.DataTypes;

namespace Badumna.Chat
{
    /// <summary>
    /// Presence event argument. 
    /// </summary>
    public class PresenceEventArgs : EventArgs
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PresenceEventArgs"/> class.
        /// </summary>
        /// <param name="status">The status.</param>
        internal PresenceEventArgs(UserPresenceStatus status)
        {
            this.Status = status;
        }

        /// <summary>
        /// Gets the status.
        /// </summary>
        internal UserPresenceStatus Status { get; private set; }
    }
}