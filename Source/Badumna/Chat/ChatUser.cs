﻿//-----------------------------------------------------------------------
// <copyright file="ChatUser.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2012 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Diagnostics;
using Badumna.Core;
using Badumna.DataTypes;
using Badumna.Security;

namespace Badumna.Chat
{
    /// <summary>
    /// An immutable chat user identity.
    /// Combines character and ID (which refers to the user's ChatSession)
    /// </summary>
    internal class ChatUser : object, IParseable, IOwnedByCharacter, IEquatable<ChatUser>
    {
        /// <summary>
        /// Initializes a new instance of the ChatUser class.
        /// </summary>
        /// <param name="sessionId">The user's session ID</param>
        /// <param name="character">The user's current character.</param>
        internal ChatUser(BadumnaId sessionId, Character character)
        {
            this.SessionId = sessionId;
            this.Character = character;
        }

        /// <summary>
        /// Initializes a new instance of the ChatUser class. For use ONLY by IParseable.
        /// </summary>
        internal ChatUser()
            : this(null, null)
        {
        }

        /// <summary>
        /// Gets the character.
        /// </summary>
        public Character Character { get; private set; }
        
        /// <summary>
        /// Gets the session id.
        /// </summary>
        public BadumnaId SessionId { get; private set; }

        /// <inheritdoc/>
        public static bool operator ==(ChatUser a, ChatUser b)
        {
            bool result;
            if (object.Equals(null, a))
            {
                result = object.Equals(null, b);
            }
            else
            {
                result = a.Equals(b);
            }

            return result;
        }

        /// <inheritdoc/>
        public static bool operator !=(ChatUser a, ChatUser b)
        {
            return !(a == b);
        }

        /// <inheritdoc/>
        public override bool Equals(object obj)
        {
            return this.Equals(obj as ChatUser);
        }

        /// <inheritdoc/>
        public bool Equals(ChatUser other)
        {
            return (!object.ReferenceEquals(other, null))
                && other.Character == this.Character
                && other.SessionId == this.SessionId;
        }

        /// <inheritdoc/>
        public override int GetHashCode()
        {
            return this.Character.GetHashCode() | this.SessionId.GetHashCode();
        }

        /// <inheritdoc/>
        public override string ToString()
        {
            return String.Format("<#ChatUser {0} [{1}]>", this.Character, this.SessionId);
        }

        /// <inheritdoc/>
        void IParseable.ToMessage(MessageBuffer message, Type ignored)
        {
            message.Write(this.SessionId);
            message.Write(this.Character);
        }

        /// <inheritdoc/>
        void IParseable.FromMessage(MessageBuffer message)
        {
            this.SessionId = message.Read<BadumnaId>();
            this.Character = message.Read<Character>();
        }
    }
}
