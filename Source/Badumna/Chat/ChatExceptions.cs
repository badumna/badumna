﻿//---------------------------------------------------------------------------------
// <copyright file="ChatExceptions.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

using System;
using System.Runtime.Serialization;

using Badumna.Core;

namespace Badumna.Chat
{
    /// <summary>
    /// An exception thrown when the legth of a chat message is too large.
    /// </summary>
    [Serializable]
    public class ChatMessageLengthException : BadumnaException
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ChatMessageLengthException"/> class.
        /// </summary>
        internal ChatMessageLengthException()
            : this(String.Format("Chat messages cannot be greater than {0} bytes", Parameters.MaximimumChatMessageLength)) 
        { 
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ChatMessageLengthException"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        internal ChatMessageLengthException(string message)
            : base(message) 
        { 
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ChatMessageLengthException"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="innerException">The inner exception.</param>
        internal ChatMessageLengthException(string message, Exception innerException) 
            : base(message, innerException) 
        { 
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ChatMessageLengthException"/> class.
        /// </summary>
        /// <param name="serializationInfo">The serialization info.</param>
        /// <param name="streamingContext">The streaming context.</param>
        internal ChatMessageLengthException(SerializationInfo serializationInfo, StreamingContext streamingContext)
            : base(serializationInfo, streamingContext) 
        { 
        }
    }
}
