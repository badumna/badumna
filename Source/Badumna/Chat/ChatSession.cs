//-----------------------------------------------------------------------
// <copyright file="ChatSession.cs" company="NICTA">
//     Copyright (c) 2011 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using Badumna.Configuration;
using Badumna.Core;
using Badumna.DataTypes;
using Badumna.Security;
using Badumna.SpatialEntities;
using Badumna.Utilities;
using Badumna.Utilities.Logging;

namespace Badumna.Chat
{
    /// <summary>
    /// Factory for creating a new ChatSession given a ChatService
    /// </summary>
    /// <param name="tokenCache">The user's token cache.</param>
    /// <param name="service">The chat service.</param>
    /// <param name="chatProtocolFacade">The chat protocol facade.</param>
    /// <returns>A new ChatSession</returns>
    internal delegate IInternalChatSession ChatSessionFactory(
        ITokenCache tokenCache,
        IInternalChatService service,
        IChatProtocolFacade chatProtocolFacade);

    /// <summary>
    /// Internal ChatSession interface
    /// </summary>
    internal interface IInternalChatSession : IChatSession
    {
        /// <summary>
        /// Occurs when logging out.
        /// </summary>
        event EventHandler LoggingOutEvent;

        /// <summary>
        /// Gets the ID of this session.
        /// </summary>
        BadumnaId Id { get; }

        /// <summary>
        /// Gets the session's current status.
        /// </summary>
        UserPresenceStatus UserStatus { get; }

        /// <summary>
        /// Gets the user this session belongs to.
        /// </summary>
        ChatUser User { get; }

        /// <summary>
        /// Gets the character this session is bound to
        /// </summary>
        Character Character { get; }

        /// <summary>
        /// Gets all channels belonging to this session.
        /// </summary>
        IEnumerable<IInternalChatChannel> Channels { get; }

        /// <summary>
        /// Subscribe to a chat channel.
        /// </summary>
        /// <param name="channelId">The channel ID</param>
        /// <param name="messageHandler">A message handler</param>
        /// <param name="presenceHandler">A presence handler</param>
        /// <returns>The chat channel.</returns>
        IChatChannel SubscribeToChatChannel(
            ChatChannelId channelId,
            ChatMessageHandler messageHandler,
            ChatPresenceHandler presenceHandler);

        /// <summary>
        /// Shuts down the chat session.
        /// </summary>
        /// <remarks><para>This method sets the user's status to Offline and unsubscribes from all open channels in this
        /// session. It is automatically called by NetworkFacade.Shutdown(), but you can call it on its own to
        /// end an individual chat session.</para>
        /// <para>You should not attempt to use this session object (or any IChatChannel objects belonging to it)
        /// for any purpose after calling Shutdown()</para></remarks>
        void Shutdown();

        /// <summary>
        /// Removes the specified channel.
        /// </summary>
        /// <param name="channel">The channel.</param>
        void RemoveChannel(IInternalChatChannel channel);

        /// <summary>
        /// Detemines whether this session is subscribed to the given channel.
        /// </summary>
        /// <param name="channel">The channel ID.</param>
        /// <returns>Whether this session contains the given channel.</returns>
        bool HasChannel(ChatChannelId channel);

        /// <summary>
        /// Handles the received message.
        /// </summary>
        /// <param name="channelId">The channel id.</param>
        /// <param name="senderId">The sender id.</param>
        /// <param name="message">The message.</param>
        void ReceiveMessage(ChatChannelId channelId, BadumnaId senderId, string message);

        /// <summary>
        /// Handles the presence event. 
        /// </summary>
        /// <param name="channel">The channel id.</param>
        /// <param name="status">The presence status.</param>
        /// <param name="notificationType">The chat notificaiton type.</param>
        void ReceivePresence(ChatChannelId channel, UserPresenceStatus status, ChatNotificationType notificationType);

        /// <summary>
        /// Handles the chat invitation event.
        /// </summary>
        /// <param name="channel">The channel id.</param>
        /// <param name="requester">The requesting user's details.</param>
        void ReceiveInvitation(ChatChannelId channel, ChatUser requester);
    }

    /// <summary>
    /// A chat session. 
    /// </summary>
    internal class ChatSession : IInternalChatSession
    {
        /// <summary>
        /// For providing the ID assigned to an entity.
        /// </summary>
        private readonly EntityIDProvider<IReplicableEntity> entityIDProvider;

        /// <summary>
        /// Unique id for this chat session.
        /// </summary>
        private BadumnaId id;

        /// <summary>
        /// The chat service object.
        /// </summary>
        private IInternalChatService service;

        /// <summary>
        /// Current chat presence status. 
        /// </summary>
        private ChatStatus status;

        /// <summary>
        /// All channels created by this session.
        /// </summary>
        private Dictionary<ChatChannelId, IInternalChatChannel> channels = new Dictionary<ChatChannelId, IInternalChatChannel>();

        /// <summary>
        /// A lock object for `channels`.
        /// </summary>
        private object channelsLock = new object();

        /// <summary>
        /// The invitation handler for private chat. 
        /// </summary>
        private ChatInvitationHandler invitationHandler;

        /// <summary>
        /// The queue to use for scheduling events.
        /// </summary>
        private NetworkEventQueue eventQueue;

        /// <summary>
        /// Chat chatProtocolFacade.
        /// </summary>
        private IChatProtocolFacade chatProtocolFacade;

        /// <summary>
        /// The method to use to queue events that must run in the application's thread.
        /// </summary>
        private QueueInvokable queueApplicationEvent;

        /// <summary>
        /// A delegate that will generate a unique BadumnaId associated with this peer.
        /// </summary>
        private GenericCallBackReturn<BadumnaId> generateBadumnaId;

        /// <summary>
        /// Delegate for the logging out event
        /// </summary>
        private EventHandler loggingOutEventDelegate;

        /// <summary>
        /// Badumna feature policy.
        /// </summary>
        private IFeaturePolicy featurePolicy;

        /// <summary>
        /// Initializes a new instance of the <see cref="ChatSession"/> class.
        /// </summary>
        /// <param name="tokenCache">The user's security token cache.</param>
        /// <param name="chatProtocolFacade">The chat protocol facade.</param>
        /// <param name="service">The service.</param>
        /// <param name="featurePolicy">The feature policy.</param>
        /// <param name="eventQueue">The queue to use for scheduling events.</param>
        /// <param name="queueApplicationEvent">The method to use to queue events that must run in the application's thread.</param>
        /// <param name="generateBadumnaId">A delegate that will generate a unique BadumnaId associated with this peer.</param>
        /// <param name="entityIDProvider">For providing the ID assigned to an entity.</param>
        internal ChatSession(
            ITokenCache tokenCache,
            IChatProtocolFacade chatProtocolFacade,
            IInternalChatService service,
            IFeaturePolicy featurePolicy,
            NetworkEventQueue eventQueue,
            QueueInvokable queueApplicationEvent,
            GenericCallBackReturn<BadumnaId> generateBadumnaId,
            EntityIDProvider<IReplicableEntity> entityIDProvider)
        {
            if (entityIDProvider == null)
            {
                throw new ArgumentNullException("entityIDProvider");
            }

            this.entityIDProvider = entityIDProvider;

            this.id = generateBadumnaId();
            this.Character = tokenCache.GetUserCertificateToken().Character;
            if (this.Character == null)
            {
                throw new NullReferenceException("no Character attached to UserCertificateToken");
            }

            this.eventQueue = eventQueue;
            this.queueApplicationEvent = queueApplicationEvent;
            this.generateBadumnaId = generateBadumnaId;
            this.chatProtocolFacade = chatProtocolFacade;
            this.service = service;
            this.featurePolicy = featurePolicy;
            this.Status = ChatStatus.Offline;
        }

        /// <summary>
        /// Change presence delegate.
        /// </summary>
        /// <param name="channel">The channel.</param>
        /// <param name="userId">The user Id.</param>
        /// <param name="displayName">The user displayer name.</param>
        /// <param name="status">The presence status.</param>
        private delegate void ChannelChangePresence(
            ChatChannelId channel,
            BadumnaId userId,
            string displayName,
            ChatStatus status);

        /// <summary>
        /// Handle chat message delegate.
        /// </summary>
        /// <param name="userId">The user Id.</param>
        /// <param name="message">The chat message.</param>
        private delegate void ChannelHandleMessage(BadumnaId userId, string message);

        /// <inheritdoc/>
        public event EventHandler LoggingOutEvent
        {
            add { this.loggingOutEventDelegate += value; }
            remove { this.loggingOutEventDelegate -= value; }
        }

        /// <inheritdoc/>
        BadumnaId IInternalChatSession.Id
        {
            get { return this.id; }
        }

        /// <inheritdoc/>
        public Character Character { get; private set; }

        /// <inheritdoc/>
        UserPresenceStatus IInternalChatSession.UserStatus
        {
            get
            {
                return new UserPresenceStatus(((IInternalChatSession)this).User, this.status);
            }
        }

        /// <inheritdoc/>
        ChatUser IInternalChatSession.User
        {
            // TODO: store & maintain this instead of creating it on demand?
            get { return new ChatUser(this.id, this.Character); }
        }

        /// <inheritdoc/>
        IEnumerable<IInternalChatChannel> IInternalChatSession.Channels
        {
            get { return this.channels.Values; }
        }

        /// <summary>
        /// Gets or sets the current chat status.
        /// </summary>
        private ChatStatus Status
        {
            get
            {
                return this.status;
            }

            set
            {
                this.status = value;
            }
        }

        /// <inheritdoc/>
        bool IInternalChatSession.HasChannel(ChatChannelId channel)
        {
            return this.channels.ContainsKey(channel);
        }

        #region IChatSession Members

        /// <inheritdoc/>
        public void ChangePresence(ChatStatus status)
        {
            if ((byte)status == 0)
            {
                throw new ArgumentException("invalid chat status");
            }

            Logger.TraceInformation(LogTag.Chat | LogTag.Event | LogTag.Presence, "Presence of user " + this.Character + " is being changed to " + status);
            this.Status = status;
            this.eventQueue.Schedule(25, this.PublishCurrentStatus);
        }

        /// <inheritdoc/>
        IChatChannel IInternalChatSession.SubscribeToChatChannel(
            ChatChannelId channelId, 
            ChatMessageHandler messageHandler, 
            ChatPresenceHandler presenceHandler)
        {
            Logger.TraceInformation(LogTag.Chat | LogTag.Event | LogTag.Presence, "user " + this.Character + " is subscribing to channel " + channelId);
            ChatChannel channel = new ChatChannel(channelId, messageHandler, presenceHandler, this.eventQueue, this.chatProtocolFacade, this, this.service);

            this.eventQueue.Push(this.JoinChatChannel, channel);
            return channel;
        }

        /// <inheritdoc/>
        public IChatChannel SubscribeToProximityChannel(
            BadumnaId entityId,
            ChatMessageHandler messageHandler)
        {
            if (null == entityId)
            {
                throw new ArgumentNullException("entityId");
            }

            var channelId = new ChatChannelId(
                ChatChannelType.Proximity,
                entityId,
                String.Format("Proximity channel for {0}", entityId));

            return ((IInternalChatSession)this).SubscribeToChatChannel(channelId, messageHandler, null);
        }

        /// <inheritdoc/>
        public IChatChannel SubscribeToProximityChannel(
            IReplicableEntity entity,
            ChatMessageHandler messageHandler)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }

            BadumnaId entityId = this.entityIDProvider(entity);
            if (entityId == null)
            {
                throw new InvalidOperationException(
                    "Entities must be registered with a scene before they can subscribe to proximity chat.");
            }

            var channelId = new ChatChannelId(
                ChatChannelType.Proximity,
                entityId,
                String.Format("Proximity channel for {0}", entityId));

            return ((IInternalChatSession)this).SubscribeToChatChannel(channelId, messageHandler, null);
        }

        /// <inheritdoc/>
        public void OpenPrivateChannels(ChatInvitationHandler invitationHandler)
        {
            this.featurePolicy.AccessPrivateChat();
            if (this.invitationHandler != null)
            {
                throw new InvalidOperationException("Private channels have already been opened for this session");
            }

            this.invitationHandler = invitationHandler;

            // We open an unimportant anonymous private channel (that nobody will talk to us on)
            // to ensure this session will receive presence events for the given user.
            // When we receive any presence with a status of Ask (regardless of the channel, since we cannot know that
            // in advance), the invitation handler will be called with the new channel (so this channel ID is irrelevant).
            Logger.TraceInformation(LogTag.Event | LogTag.Chat, "User " + this.Character + " is opening private channels.");
            ((IInternalChatSession)this).SubscribeToChatChannel(
                new ChatChannelId(ChatChannelType.Private, this.generateBadumnaId(), "invitation dummy"),
                null,
                this.PrivateChannelPresenceHandler);
        }

        /// <inheritdoc/>
        public void InviteUserToPrivateChannel(string displayName)
        {
            if (this.invitationHandler == null)
            {
                throw new InvalidOperationException("You must call OpenPrivateChannels() before inviting a user to private chat");
            }

            this.service.DoWhenOnline(() => this.InviteUserWhenOnline(displayName));
        }

        /// <inheritdoc/>
        public IChatChannel AcceptInvitation(
            ChatChannelId channel,
            ChatMessageHandler messageHandler,
            ChatPresenceHandler presenceHandler)
        {
            return ((IInternalChatSession)this).SubscribeToChatChannel(channel, messageHandler, presenceHandler);
        }

        /// <inheritdoc/>
        void IInternalChatSession.Shutdown()
        {
            Logger.TraceInformation(LogTag.Chat | LogTag.Event | LogTag.State, "ChatSession {0} logging out", this);
            this.Status = ChatStatus.Offline;
            this.PublishCurrentStatus();

            List<IInternalChatChannel> activeChannels;
            lock (this.channelsLock)
            {
                activeChannels = new List<IInternalChatChannel>(this.channels.Values);
            }

            // channel.Unsubscribe() mutates the `this.channels` collection, which is why we
            // have to take a copy above.
            foreach (var channel in activeChannels)
            {
                channel.Unsubscribe();
            }

            if (this.loggingOutEventDelegate != null)
            {
                this.loggingOutEventDelegate(this, null);
            }
        }
        
        #endregion

        /// <inheritdoc/>
        void IInternalChatSession.RemoveChannel(IInternalChatChannel channel)
        {
            ChatChannelId channelId = channel.Id;
            lock (this.channelsLock)
            {
                if (this.channels.ContainsKey(channelId))
                {
                    Logger.TraceInformation(LogTag.Chat | LogTag.Event | LogTag.Connection, "Removing channel {}", channelId);
                    this.channels.Remove(channelId);
                    this.eventQueue.Push(this.chatProtocolFacade.LeaveChannel, channel, ((IInternalChatSession)this).User);
                }
            }
        }

        /// <inheritdoc/>
        void IInternalChatSession.ReceiveMessage(ChatChannelId channelId, BadumnaId senderId, string message)
        {
            IInternalChatChannel channel;
            if (channelId == null)
            {
                Logger.TraceWarning(LogTag.Chat | LogTag.Event, "Session received message for NULL channel");
                return;
            }

            lock (this.channelsLock)
            {
                if (this.channels.TryGetValue(channelId, out channel))
                {
                    this.queueApplicationEvent(
                        Apply.Func(channel.HandleMessage, new BadumnaId(senderId), message));
                }
                else
                {
                    Logger.TraceWarning(LogTag.Chat | LogTag.Event, "ChatSession received message on channel " + channelId + ": " + message);
                }
            }
        }

        /// <inheritdoc/>
        void IInternalChatSession.ReceiveInvitation(ChatChannelId channelId, ChatUser requester)
        {
            this.ReceiveInvitation(channelId, requester.Character.Name);
        }

        /// <inheritdoc/>
        void IInternalChatSession.ReceivePresence(ChatChannelId channelId, UserPresenceStatus presence, ChatNotificationType notificationType)
        {
            Logger.TraceInformation(LogTag.Chat | LogTag.Event | LogTag.Presence, "Session received new status " + presence.Status + " for user " + presence.Character + " on channel " + channelId);

            IInternalChatChannel channel;
            if (this.channels.TryGetValue(channelId, out channel))
            {
                ChatStatus status = presence.Status;
                if (notificationType == ChatNotificationType.Ask)
                {
                    // the user would like a response with our own status
                    this.chatProtocolFacade.SendPresenceToSpecificPeer(channelId, presence.SessionId, ((IInternalChatSession)this).UserStatus);
                }

                this.queueApplicationEvent(Apply.Func(channel.PresenceChanged, presence));
            }
        }

        /// <summary>
        /// The default factory for creating a ChatSession
        /// </summary>
        /// <param name="eventQueue">Eventy queue</param>
        /// <param name="featurePolicy">The feature policy.</param>
        /// <param name="queueApplicationEvent">Queue application event</param>
        /// <param name="generateBadumnaId">A delegate that will generate a unique BadumnaId associated with this peer.</param>
        /// <param name="entityIDProvider">For providing the ID assigned to an entity.</param>
        /// <returns>A ChatSessionFactory</returns>
        internal static ChatSessionFactory Factory(
            NetworkEventQueue eventQueue,
            IFeaturePolicy featurePolicy,
            QueueInvokable queueApplicationEvent,
            GenericCallBackReturn<BadumnaId> generateBadumnaId,
            EntityIDProvider<IReplicableEntity> entityIDProvider)
        {
            return (tokenCache, service, chatProtocolFacade) =>
                new ChatSession(
                    tokenCache,
                    chatProtocolFacade,
                    service,
                    featurePolicy,
                    eventQueue,
                    queueApplicationEvent,
                    generateBadumnaId,
                    entityIDProvider);
        }

        /// <summary>
        /// Publish the current user's status globally.
        /// </summary>
        internal void PublishCurrentStatus()
        {
            this.service.DoWhenOnlineReplacingExisting(this.id, () => this.chatProtocolFacade.PublishPresenceGlobally(this));
        }

        /// <summary>
        /// Invites the user when online.
        /// </summary>
        /// <param name="username">The user name.</param>
        private void InviteUserWhenOnline(string username)
        {
            this.eventQueue.Schedule(1000, this.chatProtocolFacade.RequestPresenceSubscription, ((IInternalChatSession)this).User, username);
        }

        /// <summary>
        /// Join a channel.
        /// </summary>
        /// <param name="channel">The chat channel.</param>
        private void JoinChatChannel(ChatChannel channel)
        {
            this.channels[channel.Id] = channel;
            this.chatProtocolFacade.JoinChannel(channel.Id, ((IInternalChatSession)this).User);
            this.eventQueue.Schedule(25, this.chatProtocolFacade.PublishPresenceOnChannel, channel, ((IInternalChatSession)this).UserStatus, ChatNotificationType.DontReply);
        }

        /// <summary>
        /// Invokes the invitation handler.
        /// </summary>
        /// <param name="channelId">The channel id.</param>
        /// <param name="displayName">The display name.</param>
        private void InvokeInvitationHandler(ChatChannelId channelId, string displayName)
        {
            this.invitationHandler(channelId, displayName);
        }

        /// <summary>
        /// An internal handler for a subscription to our dummy private chat channel, which is used to handle
        /// invitations.
        /// </summary>
        /// <param name="channel_ignored">The dummy channel that the presence came through (not relevant).</param>
        /// <param name="sessionId">The user session of the requestor.</param>
        /// <param name="displayName">The display name.</param>
        /// <param name="status">The status (unused).</param>
        private void PrivateChannelPresenceHandler(
            IChatChannel channel_ignored,
            BadumnaId sessionId,
            string displayName,
            ChatStatus status)
        {
            var newChannelId = new ChatChannelId(ChatChannelType.Private, sessionId, displayName);
            this.ReceiveInvitation(newChannelId, displayName);
        }

        /// <summary>
        /// Handle an incoming invitation, triggering this session's invitation handler
        /// if present and the channel ID is new.
        /// </summary>
        /// <param name="channelId">The channel ID</param>
        /// <param name="requester">The requester's character name</param>
        private void ReceiveInvitation(ChatChannelId channelId, string requester)
        {
            Logger.TraceInformation(LogTag.Chat | LogTag.Event, "Session received new invitation from user " + requester + " with channel " + channelId);
            if (null != this.invitationHandler)
            {
                IInternalChatChannel channel;
                if (this.channels.TryGetValue(channelId, out channel))
                {
                    Logger.TraceInformation(LogTag.Chat | LogTag.Presence, "already subscribed to channel - responding with presence request");
                    this.chatProtocolFacade.PublishPresenceOnChannel(channel, ((IInternalChatSession)this).UserStatus, ChatNotificationType.Ask);
                }
                else
                {
                    Logger.TraceInformation(LogTag.Chat | LogTag.Presence, "this is a new channel - invoking invitation handler");
                    this.queueApplicationEvent(Apply.Func(this.InvokeInvitationHandler, channelId, requester));
                }
            }
        }
    }
}
