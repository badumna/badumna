﻿//---------------------------------------------------------------------------------
// <copyright file="ChatChannelId.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

using System;

using Badumna.Core;
using Badumna.DataTypes;

namespace Badumna.Chat
{
    /// <summary>
    /// An handle for chat channels.
    /// </summary>
    public sealed class ChatChannelId : IComparable<ChatChannelId>, IEquatable<ChatChannelId>
    {
        /// <summary>
        /// A chat channel that represents all users.
        /// NOT currently available.
        /// </summary>
        internal static readonly ChatChannelId Global =
            new ChatChannelId(ChatChannelType.Group, new BadumnaId(PeerAddress.GetLoopback(100), 0), "Global");

        /// <summary>
        /// Initializes a new instance of the <see cref="ChatChannelId"/> class.
        /// </summary>
        /// <param name="other">The other channel id.</param>
        public ChatChannelId(ChatChannelId other)
            : this(other.Type, other.TransportId, other.Name)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ChatChannelId"/> class.
        /// </summary>
        /// <param name="type">The channel type.</param>
        /// <param name="guid">The channel's guid.</param>
        /// <param name="name">The name of the channel.</param>
        internal ChatChannelId(ChatChannelType type, BadumnaId guid, string name)
        {
            this.Type = type;
            this.Name = name;
            this.TransportId = guid;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ChatChannelId"/> class with no Name.
        /// </summary>
        /// <param name="type">The channel type.</param>
        /// <param name="guid">The channel guid.</param>
        internal ChatChannelId(ChatChannelType type, BadumnaId guid)
            : this(type, guid, "")
        {
        }

        /// <summary>
        /// Gets the channel type.
        /// </summary>
        public ChatChannelType Type { get; private set; }

        /// <summary>
        /// Gets or sets the friendly channel name.
        /// This is for descriptive purposes only, and does not contribute to channel ID equality.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets the channel guid. This should only be used for message
        /// routing, where the Type of a channel is known in advance.
        /// </summary>
        internal BadumnaId TransportId { get; private set; }

        /// <summary>
        /// Inequality operator. Returns true if the two given ChatChannelIds are not equal.
        /// </summary>
        /// <param name="a">First comparand.</param>
        /// <param name="b">Second comparand.</param>
        /// <returns>True if a and b are not equal</returns>
        public static bool operator !=(ChatChannelId a, ChatChannelId b)
        {
            bool result;
            if (object.Equals(null, a))
            {
                result = !object.Equals(null, b);
            }
            else
            {
                result = !a.Equals(b);
            }

            return result;
        }

        /// <summary>
        /// Equality operator. Returns true only if the two given ChatChannelIds are equal
        /// </summary>
        /// <param name="a">First comparand.</param>
        /// <param name="b">Second comparand.</param>
        /// <returns>True if a and b are equal</returns>
        public static bool operator ==(ChatChannelId a, ChatChannelId b)
        {
            bool result;
            if (object.Equals(null, a))
            {
                result = object.Equals(null, b);
            }
            else
            {
                result = a.Equals(b);
            }

            return result;
        }

        /// <summary>
        /// Renders a ChatChannelId as a string for debugging purposes.  No guarantee is made that the format
        /// of this string will not change.  ChatChannelId should be treated as an opaque identifier.
        /// </summary>
        /// <returns>A string that represents the current ChatChannelId.</returns>
        public override string ToString()
        {
            return string.Format("<#ChatChannelId[{0}] {1} (\"{2}\")>", this.Type, this.TransportId.ToString(), this.Name);
        }

        /// <inheritdoc/>
        public string ToInvariantIDString()
        {
            return string.Format("{0}|{1}", this.Type, this.TransportId.ToInvariantIDString());
        }

        /// <inheritdoc/>
        public int CompareTo(ChatChannelId other)
        {
            int result;
            if (other == null)
            {
                return 1;  // IComparable spec says everything is greater than null
            }
            
            // compare type first, then ID (if types are equal)
            // (casting to (int) is just to make the actionscript compiler happy)
            result = ((int)this.Type).CompareTo((int)other.Type);

            if (result == 0)
            {
                result = this.TransportId.CompareTo(other.TransportId);
            }

            return result;
        }

        /// <summary>
        /// Compare with another object for value equality.
        /// </summary>
        /// <param name="other">The object to compare with.</param>
        /// <returns><c>true</c> if the other object is a ChatChannelId with equal value, otherwise <c>false</c>.</returns>
        public override bool Equals(object other)
        {
            return this.CompareTo(other as ChatChannelId) == 0;
        }

        /// <summary>
        /// Compare with another BadumnaId for value equality.
        /// </summary>
        /// <param name="other">The BadumnaId to compare with</param>
        /// <returns>True if the instances represent the same id, false otherwise.</returns>
        public bool Equals(ChatChannelId other)
        {
            return this.CompareTo(other) == 0;
        }

        /// <summary>
        /// Gets the hash code for the instance.
        /// </summary>
        /// <returns>A hash code for the current BadumnaId.</returns>
        public override int GetHashCode()
        {
            return this.TransportId.GetHashCode();
        }

        /// <summary>
        /// Parse a <see cref="ChatChannelId"/> from the given string.
        /// </summary>
        /// <remarks>The string should be formatted as per <see cref="ToInvariantIDString"/></remarks>
        /// <param name="idString">The string to parse</param>
        /// <returns>The <see cref="ChatChannelId"/>, or <c>null</c> if the string is badly formatted</returns>
        internal static ChatChannelId TryParse(string idString)
        {
            if (idString == null)
            {
                return null;
            }

            string[] parts = idString.Split(new char[] { '|' }, 2);
            if (parts.Length != 2)
            {
                return null;
            }

            ChatChannelType type;
            try
            {
                type = (ChatChannelType)Enum.Parse(typeof(ChatChannelType), parts[0]);
            }
            catch (ArgumentException)
            {
                return null;
            }

            BadumnaId guid = BadumnaId.TryParse(parts[1]);

            if (guid == null)
            {
                return null;
            }

            return new ChatChannelId(type, guid);
        }
    }
}
