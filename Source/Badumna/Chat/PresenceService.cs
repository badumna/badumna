//-----------------------------------------------------------------------
// <copyright file="PresenceService.cs" company="NICTA">
//     Copyright (c) 2011 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;

using Badumna.Core;
using Badumna.DataTypes;
using Badumna.DistributedHashTable;
using Badumna.Utilities;
using Badumna.Utilities.Logging;
using System.Diagnostics;
using Badumna.Security;

namespace Badumna.Chat
{
    /// <summary>
    /// The presence service.
    /// </summary>
    internal class PresenceService : DhtProtocol, IPresenceService
    {
        /// <summary>
        /// The Dht facade.
        /// </summary>
        private DhtFacade facade;

        /// <summary>
        /// The set of actively maintained UserPresenceStatus objects, keyed by user ID.
        /// </summary>
        private Dictionary<Character, SignedUserPresenceStatus> activeUsers;

        /// <summary>
        /// The network event queue;
        /// </summary>
        private NetworkEventQueue eventQueue;

        /// <summary>
        /// The time keeper.
        /// </summary>
        private ITime timeKeeper;

        /// <summary>
        /// A list of active presence subscriptions for each user ID.
        /// </summary>
        private Dictionary<Character, List<PresenceSubscriptionDetails>> activeRequests;

        /// <summary>
        /// The regular task used to renew presence replicas. 
        /// </summary>
        private RegularTask presenceReplicaRenewalTask;

        /// <summary>
        /// Reports on network connectivity status.
        /// </summary>
        private INetworkConnectivityReporter connectivityReporter;

        /// <summary>
        /// A delegate that will generate a unique BadumnaId associated with this peer.
        /// </summary>
        private GenericCallBackReturn<BadumnaId> generateBadumnaId;

        /// <summary>
        /// Delegate for presence subscription events.
        /// </summary>
        private EventHandler<PresenceSubscriptionEventArgs> presenceSubscriptionEventDelegate;

        /// <summary>
        /// Delegate for presence update events.
        /// </summary>
        private EventHandler<PresenceEventArgs> presenceEventDelegate;

        /// <summary>
        /// Secure token cache
        /// </summary>
        private ITokenCache tokenCache;

        /// <summary>
        /// Initializes a new instance of the <see cref="PresenceService"/> class.
        /// </summary>
        /// <param name="facade">The Dht facade.</param>
        /// <param name="eventQueue">The event queue to schedule events on.</param>
        /// <param name="timeKeeper">The time source.</param>
        /// <param name="tokenCache">The token cache.</param>
        /// <param name="connectivityReporter">Reports on network connectivity status.</param>
        /// <param name="generateBadumnaId">A delegate that will generate a unique BadumnaId associated with this peer.</param>
        public PresenceService(
            DhtFacade facade,
            NetworkEventQueue eventQueue,
            ITime timeKeeper,
            INetworkConnectivityReporter connectivityReporter,
            ITokenCache tokenCache,
            GenericCallBackReturn<BadumnaId> generateBadumnaId)
            : base(facade)
        {
            this.eventQueue = eventQueue;
            this.timeKeeper = timeKeeper;
            this.tokenCache = tokenCache;
            this.tokenCache.SubscribeToExpireEvent(TokenType.UserCertificate, this.UserCertificateExpirationHandler);
            this.connectivityReporter = connectivityReporter;
            this.generateBadumnaId = generateBadumnaId;
            this.facade = facade;
            this.facade.RegisterTypeForReplication<PresenceReplica>(null);
            this.activeRequests = new Dictionary<Character, List<PresenceSubscriptionDetails>>();
            this.activeUsers = new Dictionary<Character, SignedUserPresenceStatus>();
            this.presenceReplicaRenewalTask = new RegularTask(
                "presence_replica_renewal_task",
                Parameters.PresenceRenewalTaskInterval, 
                this.eventQueue,
                this.connectivityReporter,
                this.PresenceReplicaRenewalTask);
            this.presenceReplicaRenewalTask.Start();

            this.facade.InitializationCompleteNotification += this.ForceRenewPresenceReplica;
        }

        /// <summary>
        /// Occurs when a precence subscription is received.
        /// </summary>
        public event EventHandler<PresenceSubscriptionEventArgs> PresenceSubscriptionEvent
        {
            add { this.presenceSubscriptionEventDelegate += value; }
            remove { this.presenceSubscriptionEventDelegate -= value; }
        }

        /// <summary>
        /// Occurs when a precence update is received.
        /// </summary>
        public event EventHandler<PresenceEventArgs> PresenceEvent
        {
            add { this.presenceEventDelegate += value; }
            remove { this.presenceEventDelegate -= value; }
        }
        /// <summary>
        /// Shutdowns this instance.
        /// </summary>
        public void Shutdown()
        {
            if (this.presenceReplicaRenewalTask.IsRunning)
            {
                this.presenceReplicaRenewalTask.Stop();
            }

            this.activeRequests.Clear();
            this.activeUsers.Clear();

            this.facade.InitializationCompleteNotification -= this.ForceRenewPresenceReplica;
        }

        /// <summary>
        /// Sets the current presence status for a user.
        /// </summary>
        /// <param name="unsignedUserStatus">The presence status.</param>
        public void SetStatus(UserPresenceStatus unsignedUserStatus)
        {
            this.SetStatus(new SignedUserPresenceStatus(unsignedUserStatus, this.tokenCache.GetUserCertificateToken()));
        }

        /// <summary>
        /// Sets the current presence status for a user.
        /// </summary>
        /// <param name="userStatus">The presence status.</param>
        //// TODO: Do we want this override? it's only used by tests as a more direct way than stubbing IIdentityProvider.GetUserCertificateToken()
        internal void SetStatus(SignedUserPresenceStatus userStatus)
        {
            Debug.Assert(userStatus.UserCertificate.Character == userStatus.Character);

            SignedUserPresenceStatus oldStatus;
            this.activeUsers.TryGetValue(userStatus.Character, out oldStatus);
            if(userStatus.Equals(oldStatus))
            {
                return;
            }
            
            this.ValidateOwnership(userStatus);
            this.eventQueue.Push(this.UpdatePresenceReplica, userStatus);

            if(userStatus.ChatStatus == ChatStatus.Offline)
            {
                // Stop updating the presence replica after the user goes offline
                if(oldStatus != null)
                {
                    this.activeUsers.Remove(userStatus.Character);
                }
            }
            else
            {
                this.activeUsers[userStatus.Character] = userStatus;
            }
        }

        /// <summary>
        /// Validates a signed message.
        /// </summary>
        /// Throws a SecurityException if the message is not valid.
        /// <param name="message">The message.</param>
        private void ValidateOwnership<T>(SignedUserData<T> message) where T : IParseable, IOwnedByCharacter, IEquatable<T>
        {
            if (!message.Validate(this.tokenCache))
            {
                throw new SecurityException("Invalid " + message.GetType());
            }
        }

        /// <summary>
        /// Updates the presence replica for the given user.
        /// </summary>
        /// <param name="userStatus">The user's current presence status.</param>
        public void UpdatePresenceReplica(SignedUserPresenceStatus userStatus)
        {
            DhtEnvelope envelope = this.GetMessageFor(
                HashKey.Hash(userStatus.Character.Name),
                QualityOfService.Reliable,
                Parameters.ChatPresenceRedundancyFactor);
            this.RemoteCall(envelope, this.ChangePresence, userStatus);
            this.SendMessage(envelope);
        }

        /// <summary>
        /// Requests subscription to another user's presence.
        /// </summary>
        /// <param name="unsignedRequester">The requesting user.</param>
        /// <param name="userName">Name of the user.</param>
        public void RequestSubscriptionTo(ChatUser unsignedRequester, string userName)
        {
            Logger.TraceInformation(LogTag.Presence | LogTag.Event, "User " + unsignedRequester + " is requesting a subscription to user " + userName);

            SignedChatUser requester = new SignedChatUser(unsignedRequester, this.tokenCache.GetUserCertificateToken());
            Debug.Assert(unsignedRequester.Character == requester.UserCertificate.Character);

            this.SendRequestSubscriptionTo(requester, userName);

            List<PresenceSubscriptionDetails> requests;
            if (this.activeRequests.TryGetValue(requester.Character, out requests))
            {
                foreach (PresenceSubscriptionDetails details in requests)
                {
                    if (details.Id.Equals(requester.SessionId) && details.Name == userName)
                    {
                        Logger.TraceInformation(LogTag.Presence | LogTag.Event, "Request (" + requester + " -> " + userName + ") is already known - ignoring duplicate");
                        return;
                    }
                }
            }
            else
            {
                requests = new List<PresenceSubscriptionDetails>();
                this.activeRequests.Add(requester.Character, requests);
            }
            requests.Add(new PresenceSubscriptionDetails(requester, userName, this.timeKeeper.Now));
        }

        /// <summary>
        /// Handles the change presence request.
        /// </summary>
        /// <param name="signedStatus">The presence status.</param>
        protected virtual void HandleChangePresence(SignedUserPresenceStatus signedStatus)
        {
            this.ValidateOwnership(signedStatus);

            BadumnaId id = signedStatus.SessionId;
            PresenceReplica replica = this.GetReplica(signedStatus.Character.Name);
            if (null != replica)
            {
                replica.PresenceStatus = signedStatus;

                // If the status is online then send pending requests
                if (signedStatus.ChatStatus != ChatStatus.Offline)
                {
                    replica.ForEachPendingRequest(
                        delegate(SignedChatUser requestingUser)
                        {
                            this.PushSubscriptionRequest(requestingUser, replica.Guid);
                        });
                }

                this.facade.UpdateReplica(replica, (uint)Parameters.PresenceReplicaTTL.TotalSeconds);
            }
            else
            {
                replica = new PresenceReplica(signedStatus);

                this.facade.Replicate(
                    replica,
                    this.CurrentEnvelope.DestinationKey,
                    (uint)Parameters.PresenceReplicaTTL.TotalSeconds);
            }
        }

        /// <summary>
        /// Handles the forwarder subscription request.
        /// </summary>
        /// <param name="sourceUser">The source user.</param>
        /// <param name="destinationUsername">The destination username (subject).</param>
        protected virtual void HandleForwarderSubscriptionRequest(
            SignedChatUser sourceUser,
            string destinationUsername)
        {
            PresenceReplica replica = this.GetReplica(destinationUsername);

            if (null == replica)
            {
                // No presense replica for the given user has been found
                // Create a replica containing the pending request
                replica = new PresenceReplica(destinationUsername, this.generateBadumnaId());

                // Store the pending request with the replica
                replica.AddPendingRequest(sourceUser);
                this.facade.Replicate(
                    replica,
                    this.CurrentEnvelope.DestinationKey,
                    (uint)Parameters.PresenceReplicaOfflineModeTTL.TotalSeconds);

                return;
            }
            else
            {
                if (!replica.StatusKnown)
                {
                    // the peer is offline, just record the pending request.
                    replica.AddPendingRequest(sourceUser);
                    this.facade.UpdateReplica(replica, (uint)Parameters.PresenceReplicaOfflineModeTTL.TotalSeconds);
                    return;
                }
            }

            if (replica.StatusKnown)
            {
                Logger.TraceInformation(LogTag.Presence | LogTag.Event, "presence replica for " + destinationUsername + " is online!");
                this.PushSubscriptionRequest(sourceUser, replica.Guid);
            }
        }

        #region Protocol methods

        /// <summary>
        /// Changes the presence status.
        /// </summary>
        /// <param name="status">The user's new presence status.</param>
        [DhtProtocolMethod(DhtMethod.ChangePresence)]
        protected void ChangePresence(SignedUserPresenceStatus status)
        {
            try
            {
                this.HandleChangePresence(status);
            }
            catch (SecurityException e)
            {
                Logger.TraceWarning(LogTag.Crypto | LogTag.Event, "SecurityException: {0}", e.Message);
            }
        }

        /// <summary>
        /// Forwards the subscription request.
        /// </summary>
        /// <param name="sourceUser">The source user.</param>
        /// <param name="destinationUsername">The destination username.</param>
        [DhtProtocolMethod(DhtMethod.ForwardSubscriptionRequest)]
        protected void ForwardSubscriptionRequest(SignedChatUser sourceUser, string destinationUsername)
        {
            this.HandleForwarderSubscriptionRequest(sourceUser, destinationUsername);
        }

        /// <summary>
        /// Handles a subscription request.
        /// </summary>
        /// <param name="source">The source (requester) of this event.</param>
        /// <param name="subject">The subject of this event.</param>
        [DhtProtocolMethod(DhtMethod.SubscriptionRequest)]
        protected void ReceiveSubscriptionEvent(SignedChatUser source, BadumnaId subject)
        {
            try
            {
                this.ValidateOwnership(source);
            }
            catch (SecurityException e)
            {
                Logger.TraceWarning(LogTag.Crypto | LogTag.Event, "SecurityException: {0}", e.Message);
                return;
            }

            if (null != this.presenceSubscriptionEventDelegate)
            {
                this.presenceSubscriptionEventDelegate(this, new PresenceSubscriptionEventArgs(source.User, subject));
            }
            else
            {
                Logger.TraceInformation(LogTag.Presence | LogTag.Event, "Dropping subscription event for user " + source + " interested in subject " + subject + " because PresenceSubscriptionEvent is null");
            }
        }

        /// <summary>
        /// Handles the presence event.
        /// </summary>
        /// <param name="presence">The subject's chat status.</param>
        [DhtProtocolMethod(DhtMethod.PresenceNotification)]
        protected void ReceivePresenceEvent(SignedUserPresenceStatus presence)
        {
            try
            {
                this.ValidateOwnership(presence);
            }
            catch (SecurityException e)
            {
                Logger.TraceWarning(LogTag.Crypto | LogTag.Event, "SecurityException: {0}", e.Message);
                return;
            }

            if (null != this.presenceEventDelegate)
            {
                this.presenceEventDelegate(this, new PresenceEventArgs(presence.Status));
            }
        }

        #endregion

        /// <summary>
        /// Send subscription request.
        /// </summary>
        /// <param name="requester">The requester.</param>
        /// <param name="userName">Name of the user.</param>
        private void SendRequestSubscriptionTo(SignedChatUser requester, string userName)
        {
            DhtEnvelope envelope = this.GetMessageFor(
                HashKey.Hash(userName),
                QualityOfService.Reliable,
                Parameters.ChatPresenceRedundancyFactor);

            this.RemoteCall(envelope, this.ForwardSubscriptionRequest, requester, userName);
            this.SendMessage(envelope);
        }

        /// <summary>
        /// Gets the presence replica.
        /// </summary>
        /// <param name="characterName">The character name.</param>
        /// <returns>The presence replica.</returns>
        private PresenceReplica GetReplica(string characterName)
        {
            IEnumerable<PresenceReplica> replicaList
                = this.facade.AccessReplicas<PresenceReplica>(this.CurrentEnvelope.DestinationKey);

            if (null != replicaList)
            {
                foreach (PresenceReplica replica in replicaList)
                {
                    if (replica.CharacterName == characterName)
                    {
                        return replica;
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Gets the presence replica.
        /// </summary>
        /// <param name="id">The replica id.</param>
        /// <returns>The presence replica.</returns>
        private PresenceReplica GetReplica(BadumnaId id)
        {
            return this.facade.AccessReplica<PresenceReplica>(this.CurrentEnvelope.DestinationKey, id);
        }

        /// <summary>
        /// The Presence replica renewal task.
        /// </summary>
        private void PresenceReplicaRenewalTask()
        {
            this.RenewPresenceReplicas(false);
        }

        /// <summary>
        /// Forces the renewal of presence replicas.
        /// </summary>
        private void ForceRenewPresenceReplica(NodeInfo node)
        {
            this.RenewPresenceReplicas(true);
        }

        /// <summary>
        /// Renews all active presence replicas.
        /// </summary>
        /// <param name="forced">if set to <c>true</c>, then force to renew.</param>
        private void RenewPresenceReplicas(bool forced)
        {
            if (this.connectivityReporter.Status == ConnectivityStatus.Online)
            {
                foreach(var userStatus in this.activeUsers.Values)
                {
                    Logger.TraceInformation(LogTag.Periodic | LogTag.Presence, "Renewing presence replica for user \"" + userStatus.Character + "\", status " + userStatus.ChatStatus);
                    this.UpdatePresenceReplica(userStatus);
                }

                TimeSpan now = this.timeKeeper.Now;
                foreach (KeyValuePair<Character, List<PresenceSubscriptionDetails>> item in this.activeRequests)
                {
                    foreach (PresenceSubscriptionDetails details in item.Value)
                    {
                        if (forced || now - details.LastRefreshTime >= Parameters.PresenceSubscriptionRenewalInterval)
                        {
                            Logger.TraceInformation(LogTag.Periodic | LogTag.Presence, "Renewing presence subscription for requester \"" + details.Requester + "\" interested in user \"" + details.Name + "\"");
                            this.SendRequestSubscriptionTo(details.Requester, details.Name);
                            details.LastRefreshTime = now;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Handles a user's certificate expiring.
        /// </summary>
        /// Updates the user's status after signing it with the new certificate.
        /// <param name="sender">The sender.</param>
        /// <param name="args">The <see cref="Badumna.Security.TokenExpirationEventArgs"/> instance containing the event data.</param>
        private void UserCertificateExpirationHandler(object sender, TokenExpirationEventArgs args)
        {
            CertificateToken newCertificate = args.NewToken as CertificateToken;

            if (newCertificate == null || newCertificate.Character == null)
            {
                Logger.TraceWarning(LogTag.Presence | LogTag.Event, "New user certificate ({0}) has no valid character. Not updating presence.", newCertificate);
                return;
            }

            SignedUserPresenceStatus status;
            if (this.activeUsers.TryGetValue(newCertificate.Character, out status))
            {
                this.eventQueue.Push(this.UpdatePresenceReplica, status.WithCertificate(newCertificate));
            }
            else
            {
                Logger.TraceInformation(LogTag.Periodic | LogTag.Presence, "Presence service ignored UserCertificate token for inactive character {0}", newCertificate.Character);
            }
        }

        /// <summary>
        /// Pushes the subscription request.
        /// </summary>
        /// <param name="requester">The user requesting subscription.</param>
        /// <param name="subject">The subject of the subscription.</param>
        private void PushSubscriptionRequest(SignedChatUser requester, BadumnaId subject)
        {
            this.eventQueue.Push(this.SendSubscriptionRequest, requester, subject);
        }

        /// <summary>
        /// Sends the subscription request.
        /// </summary>
        /// <param name="requester">The user requesting subscription.</param>
        /// <param name="subject">The subject of the subscription.</param>
        protected virtual void SendSubscriptionRequest(SignedChatUser requester, BadumnaId subject)
        {
            DhtEnvelope envelope = this.GetMessageFor(new PeerAddress(subject.Address), QualityOfService.Reliable);
            Logger.TraceInformation(LogTag.Presence | LogTag.Event, "Sending subscription request for requester " + requester + " interested in user ID " + subject);
            this.RemoteCall(envelope, this.ReceiveSubscriptionEvent, requester, subject);
            this.SendMessage(envelope);
        }

        /// <summary>
        /// Push a presence update onto the event queue.
        /// </summary>
        /// <param name="destination">The destination of the notification.</param>
        /// <param name="presence">The subject's presence status.</param>
        private void PushPresenceUpdate(BadumnaId destination, SignedUserPresenceStatus presence)
        {
            this.eventQueue.Push(this.SendPresenceUpdate, destination, presence);
        }

        /// <summary>
        /// Sends the presence update.
        /// </summary>
        /// <param name="destination">The destination of the notification.</param>
        /// <param name="presence">The subject's presence status.</param>
        protected virtual void SendPresenceUpdate(BadumnaId destination, SignedUserPresenceStatus presence)
        {
            DhtEnvelope envelope = this.GetMessageFor(destination.Address, QualityOfService.Reliable);
            this.RemoteCall(envelope, this.ReceivePresenceEvent, presence);
            this.SendMessage(envelope);
        }

    }

    internal delegate IPresenceService PresenceServiceFactory(BadumnaId SessionId, string displayname);
}









