//-----------------------------------------------------------------------
// <copyright file="ChatProtocolFacade.cs" company="NICTA">
//     Copyright (c) 2012 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
using Badumna.Core;
using Badumna.DataTypes;
using Badumna.Multicast;
using Badumna.Replication;
using Badumna.Transport;

namespace Badumna.Chat
{
    /// <summary>
    /// Factory that retusn a new ChatProtocolFacade.
    /// </summary>
    /// <param name="chatService">The ChatService associated with this facade.</param>
    /// <returns>A new ChatProtocolFacade</returns>
    internal delegate IChatProtocolFacade ChatProtocolFacadeFactory(ChatService chatService);

    /// <summary>
    /// Internal Chat Protocol Facade interface.
    /// </summary>
    internal interface IChatProtocolFacade
    {
        /// <summary>
        /// Publish the status of the user associated
        /// with a given chat session to the presence
        /// service and all channels they are active on.
        /// </summary>
        /// <param name="session">The chat session.</param>
        void PublishPresenceGlobally(IInternalChatSession session);

        /// <summary>
        /// Publish a user presence event to a given chat channel.
        /// This does not affect the presence service or other channels.
        /// </summary>
        /// <param name="channel">The channel to publish to.</param>
        /// <param name="userStatus">The user's status.</param>
        void PublishPresenceOnChannel(IInternalChatChannel channel, UserPresenceStatus userStatus);

        /// <summary>
        /// Publish a user presence event to a given chat service.
        /// This does not affect the presence service or other channels.
        /// </summary>
        /// <param name="channel">The channel to publish to.</param>
        /// <param name="userStatus">The user's status.</param>
        /// <param name="notificationType">The chat notification type</param>
        void PublishPresenceOnChannel(IInternalChatChannel channel, UserPresenceStatus userStatus, ChatNotificationType notificationType);
        
        /// <summary>
        /// Send a presence event directly to a specific peer
        /// on the given channel, if the channel type supports it.
        /// </summary>
        /// <param name="channelId">The channel ID.</param>
        /// <param name="remoteUserId">The remote user to send the presence event to.</param>
        /// <param name="userPresence">The current user's presence status.</param>
        void SendPresenceToSpecificPeer(ChatChannelId channelId, BadumnaId remoteUserId, UserPresenceStatus userPresence);
        
        /// <summary>
        /// Request a subscription to the current user with the given DisplayName.
        /// </summary>
        /// <param name="requestingUser">The user requesting the subscription.</param>
        /// <param name="displayName">The display name of the requested user.</param>
        void RequestPresenceSubscription(ChatUser requestingUser, string displayName);
        
        /// <summary>
        /// Join a channel.
        /// </summary>
        /// <param name="channelId">The Channel ID to join.</param>
        /// <param name="user">The user joining the channel.</param>
        void JoinChannel(ChatChannelId channelId, ChatUser user);
        
        /// <summary>
        /// Leave a channel.
        /// </summary>
        /// <param name="channel">The channel to leave.</param>
        /// <param name="user">The user leaving the channel.</param>
        void LeaveChannel(IInternalChatChannel channel, ChatUser user);
        
        /// <summary>
        /// Send a message to the given channel.
        /// </summary>
        /// <param name="channel">The channel to send a message on.</param>
        /// <param name="message">The message content.</param>
        void SendMessage(IInternalChatChannel channel, string message);
    }

    /// <summary>
    /// A wrapper class around:
    ///  - proximity chat protocol
    ///  - private protocol
    ///  - multicast protocol
    ///  - presence service
    /// For a given action, it'll call the corresponding method
    /// on the appropriate protocol(s).
    /// </summary>
    internal class ChatProtocolFacade : IChatProtocolFacade
    {
        /// <summary>
        /// Proximity protocol.
        /// </summary>
        private IProximityChatProtocol proximityProtocol;

        /// <summary>
        /// Multicast (group) protocol.
        /// </summary>
        private IMulticastChatProtocol multicastProtocol;

        /// <summary>
        /// Private protocol.
        /// </summary>
        private IPrivateChatProtocol privateProtocol;

        /// <summary>
        /// Presence service.
        /// </summary>
        private IPresenceService presenceService;

        /// <summary>
        /// Initializes a new instance of the ChatProtocolFacade class.
        /// </summary>
        /// <param name="proximityProtocol">The proximity chat protocol.</param>
        /// <param name="multicastProtocol">The multicast (group) chat protocol.</param>
        /// <param name="privateProtocol">The private chat protocol.</param>
        /// <param name="presenceService">The chat presence service.</param>
        internal ChatProtocolFacade(
            IProximityChatProtocol proximityProtocol,
            IMulticastChatProtocol multicastProtocol,
            IPrivateChatProtocol privateProtocol,
            IPresenceService presenceService)
        {
            this.proximityProtocol = proximityProtocol;
            this.privateProtocol = privateProtocol;
            this.multicastProtocol = multicastProtocol;
            this.presenceService = presenceService;
        }

        /// <inheritdoc/>
        public void PublishPresenceGlobally(IInternalChatSession session)
        {
            var userStatus = session.UserStatus;
            this.presenceService.SetStatus(userStatus);

            foreach (IInternalChatChannel channel in session.Channels)
            {
                this.PublishPresenceOnChannel(channel, userStatus);
            }
        }

        /// <inheritdoc/>
        public void PublishPresenceOnChannel(IInternalChatChannel channel, UserPresenceStatus userStatus)
        {
            this.PublishPresenceOnChannel(channel, userStatus, ChatNotificationType.Default);
        }

        /// <inheritdoc/>
        public void PublishPresenceOnChannel(IInternalChatChannel channel, UserPresenceStatus userStatus, ChatNotificationType notificationType)
        {
            if (channel.Id.Type == ChatChannelType.Group)
            {
                this.multicastProtocol.ChangePresence(channel.TransportId, userStatus, notificationType);
            }
            else if (channel.Id.Type == ChatChannelType.Private)
            {
                this.privateProtocol.ChangePresence(channel.TransportId, userStatus, notificationType);
            }
        }

        /// <inheritdoc/>
        public void SendPresenceToSpecificPeer(ChatChannelId channelId, BadumnaId remoteUserId, UserPresenceStatus userPresence)
        {
            switch (channelId.Type)
            {
                case ChatChannelType.Group:
                    this.multicastProtocol.SendDirectPresence(
                        channelId.TransportId,
                        remoteUserId.Address,
                        userPresence,
                        ChatNotificationType.DontReply);
                    break;

                case ChatChannelType.Private:
                    this.privateProtocol.ChangePresence(
                        channelId.TransportId,
                        userPresence,
                        ChatNotificationType.DontReply);
                    break;
            }
        }

        /// <inheritdoc/>
        public void RequestPresenceSubscription(ChatUser requestingUser, string displayName)
        {
            this.presenceService.RequestSubscriptionTo(requestingUser, displayName);
        }

        /// <inheritdoc/>
        public void JoinChannel(ChatChannelId channelId, ChatUser user)
        {
            switch (channelId.Type)
            {
                case ChatChannelType.Group:
                    this.multicastProtocol.JoinGroup(channelId.TransportId);
                    break;

                case ChatChannelType.Private:
                    this.privateProtocol.JoinChannel(channelId.TransportId, user);
                    break;
            }
        }

        /// <inheritdoc/>
        public void LeaveChannel(IInternalChatChannel channel, ChatUser user)
        {
            ChatChannelId channelId = channel.Id;
            switch (channelId.Type)
            {
                case ChatChannelType.Group:
                    this.multicastProtocol.LeaveGroup(channelId.TransportId);
                    break;
                case ChatChannelType.Private:
                    UserPresenceStatus offlineStatus = new UserPresenceStatus(user, ChatStatus.Offline);
                    this.PublishPresenceOnChannel(channel, offlineStatus);
                    break;
            }
        }

        /// <inheritdoc/>
        public void SendMessage(IInternalChatChannel channel, string message)
        {
            var channelId = channel.Id;
            var sessionId = channel.SessionId;
            switch (channelId.Type)
            {
                case ChatChannelType.Proximity:
                    this.proximityProtocol.SendProximityChatMessage(channelId.TransportId, message);
                    break;

                case ChatChannelType.Group:
                    this.multicastProtocol.SendChatMessage(channelId.TransportId, sessionId, message);
                    break;

                case ChatChannelType.Private:
                    this.privateProtocol.SendChatMessage(channelId.TransportId, sessionId, message);
                    break;

                    // TODO: is this ever used??
                case ChatChannelType.Local:
                    // this.ReceiveMessage(channel, this.mUserId, message);
                    break;
            }
        }

        /// <summary>
        /// Creates a default factory for constructing a ChatProtocolFacade.
        /// </summary>
        /// <param name="presenceService">A presence service.</param>
        /// <param name="entityManager">An entity manager.</param>
        /// <param name="multicastFacade">A multicast facade.</param>
        /// <param name="transportProtocol">A transport protocol.</param>
        /// <param name="addressProvider">A network address provider.</param>
        /// <returns>A ChatProtocolFacade</returns>
        internal static ChatProtocolFacadeFactory Factory(
            IPresenceService presenceService,
            EntityManager entityManager,
            MulticastFacade multicastFacade,
            TransportProtocol transportProtocol,
            INetworkAddressProvider addressProvider)
        {
            return delegate(ChatService chatService)
            {
                ProximityChatProtocol proximityProtocol = new ProximityChatProtocol(entityManager, addressProvider, chatService);
                MulticastChatProtocol multicastProtocol = new MulticastChatProtocol(multicastFacade, chatService);
                IPrivateChatProtocol privateProtocol = new PrivateChatProtocol(transportProtocol, presenceService, chatService);

                return new ChatProtocolFacade(proximityProtocol, multicastProtocol, privateProtocol, presenceService);
            };
        }
    }
}
