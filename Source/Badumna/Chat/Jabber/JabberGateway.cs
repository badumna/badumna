using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.Xml;
using System.Xml.XPath;
using System.IO;

using Badumna.Transport;
using Badumna.Core;
using Badumna.Utilities;

namespace Badumna.Chat.Jabber
{
    class JabberGateway
    {
        private TcpListener mListener;
        private XmlTextReader mReader;
        private StreamWriter mWriter;

        private ChatService mService;
        private String mServiceName;
        private bool mIsSaslAuthorized;
        private String mUserName = String.Empty;
        private String mResource = String.Empty;
        private String mLoginId = String.Empty;

        public JabberGateway(ChatService service, String serviceName)
        {
            this.mService = service;
            this.mServiceName = serviceName;
        }

        public void Initialize()
        {
            try
            {
                this.mListener = new TcpListener(PeerAddress.GetLoopback(ChatModule.Instance.Port));
                this.mListener.Start(1);

                this.BeginListen();
            }
            catch (SocketException e)
            {
                Logger.TraceWarning(LogTag.Chat, "Jabber gateway failed to initialize : {0}", e.Message);
            }
        }

        public void ShutDown()
        {
            this.Reset();
            this.mListener.Stop(); 
        }

        private void Reset()
        {
            this.mUserName = String.Empty;
            this.mResource = String.Empty;
            this.mLoginId = String.Empty;
            this.mIsSaslAuthorized = false;

            if (null != this.mReader)
            {
                this.mReader = null;
            }

            if (null != this.mWriter)
            {
                this.mWriter = null;
            }
        }

        private void BeginListen()
        {
            this.Reset();
            this.mListener.BeginAcceptTcpClient(JabberGateway.ReceiveCallback, this);
        }

        private void Start()
        {
            try
            {
                while (null != this.mReader && this.mLoginId == String.Empty && this.mReader.Read())
                {
                    System.Console.WriteLine("--- {0}", this.mReader.LocalName);

                    for (int i = 0; i < this.mReader.AttributeCount; i++)
                    {
                        this.mReader.MoveToAttribute(i);
                        System.Console.WriteLine("{0} = {1}", this.mReader.LocalName, this.mReader.Value);

                    }

                    this.mReader.MoveToElement();
                    if (this.mReader.LocalName == "stream")
                    {
                        if (this.mReader.IsStartElement())
                        {
                            this.StartStream();
                        }
                        else
                        {
                            this.EndStream();
                            return;
                        }
                    }
                    else if (this.mReader.NodeType == XmlNodeType.Element)
                    {
                        XPathDocument doc = new XPathDocument(this.mReader.ReadSubtree());
                        XPathNavigator nav = doc.CreateNavigator();

                        Console.WriteLine(nav.OuterXml);

                        switch (this.mReader.LocalName)
                        {
                            case "auth":
                                if (nav.MoveToFirstChild())
                                {
                                    this.SaslAuthorize(nav);
                                }
                                break;
                            case "iq":
                                if (nav.MoveToFirstChild())
                                {
                                    this.Iq(nav);
                                }
                                break;
                        }
                    }
                }
            }
            catch (IOException)
            {
            }
            catch (XmlException)
            {
            }
        }

        private void StartStream()
        {
            this.mReader.MoveToAttribute("to");
            this.mWriter.Write(
                String.Format("<?xml version='1.0'?><stream:stream from='{0}' id='c2s_123' xml:lang='en' " +
                "xmlns='jabber:client' xmlns:stream='http://etherx.jabber.org/streams' version='1.0'>",
                 this.mReader.Value)); this.mWriter.Flush();

            if (this.mIsSaslAuthorized)
            {
                // Set stream feature to indicate use of sasl
                this.mWriter.Write("<stream:features><mechanisms xmlns='urn:ietf:params:xml:ns:xmpp-sasl'><mechanism>PLAIN</mechanism></mechanisms></stream:features>");
            }
            else
            {
                this.mWriter.Write("<stream:features><session xmlns='urn:ietf:params:xml:ns:xmpp-session'/></stream:features>");
            }

            this.mWriter.Flush();
        }

        private void EndStream()
        {
            this.mWriter.Write("</stream>");
            this.mWriter.Flush();
        }

        private void Iq(XPathNavigator nav)
        {
            String id = nav.GetAttribute("id", "");
            String type = nav.GetAttribute("type", "");
            if (type == "get" || type == "set")
            {
                this.HandleQuery(nav, id);
            }
            else
            {
                this.mWriter.Write(String.Format("<iq type='error' id='{0}'><error type='modify'>Incorrect request</error></iq>", id));
                this.mWriter.Flush();
            }
        }

        private void HandleQuery(XPathNavigator nav, String id)
        {
            nav.MoveToFirstChild();
            switch (nav.LocalName)
            {
                case "query":
                    switch (nav.NamespaceURI)
                    {
                        case "jabber:iq:auth":
                            this.HandleAuthorizeRequest(nav, id);
                            break;

                        default:
                            this.mWriter.Write(String.Format("<iq type='error' id='{0}'><error type='modify'>Incorrect request</error></iq>", id));
                            this.mWriter.Flush();
                            break;
                    }
                    break;

                case "error":
                    nav.MoveToChild("error", "");
                    System.Console.WriteLine("Jabber error : ", nav.Value);
                    break;
            }
        }

        #region Authentication

        private void SaslAuthorize(XPathNavigator nav)
        {
            // Allways accept.
            this.mIsSaslAuthorized = true;
            this.mWriter.Write("<success xmlns='urn:ietf:params:xml:ns:xmpp-sasl'/>");
            this.mWriter.Flush();
        }

        private void HandleAuthorizeRequest(XPathNavigator nav, String id)
        {
            string username = null;
            string password = null;
            string resource = null;

            if (nav.MoveToChild("username", nav.NamespaceURI))
            {
                username = nav.Value;
                nav.MoveToParent();
            }

            if (nav.MoveToChild("password", nav.NamespaceURI))
            {
                password = nav.Value;
                nav.MoveToParent();
            }

            if (nav.MoveToChild("resource", nav.NamespaceURI))
            {
                resource = nav.Value;
                nav.MoveToParent();
            }

            if (null == username || null == password || null == resource)
            {
                this.mWriter.Write(String.Format("<iq type='result' id='{0}'><query xmlns='jabber:iq:auth'><username/><password/><resource/></query></iq>", id));
                this.mWriter.Flush();
            }
            else
            {
                this.mUserName = username;
                this.mResource = resource;
                this.mLoginId = id;

                ChatSession session = this.mService.CreateSession();

                if (null != session)
                {
                    this.mWriter.Write(String.Format("<iq type='result' id='{0}'/>", this.mLoginId));
                    this.mWriter.Flush();

                    new JabberSession(session, this.mServiceName, this.mUserName, this.mResource, this.mReader, this.mWriter);

                    // Start to listen again
                    this.BeginListen();
                }
                else
                {
                    this.mWriter.Write(String.Format("<iq type='error' id='{0}'><error type='modify'>Incorrect password</error></iq>", this.mLoginId));
                    this.mWriter.Flush();
                }
            }
        }


        #endregion



        static private void ReceiveCallback(IAsyncResult ar)
        {
            JabberGateway gateway = (JabberGateway)ar.AsyncState;
            TcpClient client = null;

            try
            {
                client = gateway.mListener.EndAcceptTcpClient(ar);
            }
            catch
            {
                return;
            }

            if (null != client && client.Connected)
            {
                try
                {
                    Stream clientStream = client.GetStream();
                    gateway.mReader = new XmlTextReader(clientStream);
                    gateway.mWriter = new StreamWriter(clientStream);

                    Thread thread = new Thread(gateway.Start);
                    thread.Start();
                }
                catch (Exception e)
                {
                    Logger.TraceException(System.Diagnostics.TraceEventType.Warning, e, "Exception caught in jabber gateway ReceiveCallback");
                }
            }
        }
    }
}
