using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;


namespace Badumna.Chat.Jabber
{
    [Serializable]
    class JabberContact
    {
        private String mJid;
        public String Jid { get { return this.mJid; } }

        private String mGroup = String.Empty;
        public String Group
        {
            get { return this.mGroup; }
            set { this.mGroup = value; }
        }

        private String mName = String.Empty;
        public String Name
        {
            get { return this.mName; }
            set { this.mName = value; }
        }
        
        [NonSerialized]
        private ChatChannelId mChannel;
        public ChatChannelId Channel 
        { 
            get { return this.mChannel; }
            set { this.mChannel = value; this.Subscription = "from"; }
        }
        
        private String mSubscription = "none";
        public String Subscription 
        { 
            get { return this.mSubscription; }
            set
            {
                if ((this.mSubscription == "to" && value == "from") ||
                    (this.mSubscription == "from" && value == "to"))
                {
                    this.mSubscription = "both";
                }
                else
                {
                    this.mSubscription = value;
                }
            }
        }

        public JabberContact(String jid)
        {
            this.mJid = jid;
        }


    }
}
