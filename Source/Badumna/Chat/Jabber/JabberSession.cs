using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.Xml;
using System.Xml.XPath;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

using Badumna;
using Badumna.Core;
using Badumna.Utilities;
using Badumna.Transport;
using Badumna.DataTypes;

namespace Badumna.Chat.Jabber
{
    internal enum JabberPresence
    {
        Available,
        Unavailable,
        Probe,
        Subscribe,
        Subscribed,
        Unsubscribe,
        Unsubscribed,
        Error,
        Unchanged
    }

    internal enum JabberMessageType
    {
        Chat,
        GroupChat,
        Normal,
        Error,
        Headline
    }

    enum JabberGroupAffiliation
    {
        Outcast,
        None,
        Member,
        Admin,
        Owner
    }

    enum JabberGroupRole
    {
        Owner,
        Moderator,
        Participant,
    }


    class JabberSession
    {
        private ChatSession mSession;
        private String mServiceName = "badumna";
        private String mUserJid;
        private String mResource;

        private XmlTextReader mReader;
        private StreamWriter mWriter;

        private List<String> mPendingClientMessages = new List<String>();
        private Dictionary<ChatChannelId, String> mChannelJidMap = new Dictionary<ChatChannelId, String>();
        private Dictionary<String, ChatChannelId> mJidChannelMap = new Dictionary<string, ChatChannelId>();
        private Dictionary<String, JabberContact> mContacts = new Dictionary<string, JabberContact>();

        private Dictionary<BadumnaId, String> mDisplayNames = new Dictionary<BadumnaId, string>();

        public JabberSession(ChatSession session, String serviceName, String username, String resource, XmlTextReader reader, StreamWriter writer)
        {
            this.mSession = session;
            this.mServiceName = serviceName;
            this.mResource = resource;
            this.mUserJid = username + "@127.0.0.1";

            this.mReader = reader;
            this.mWriter = writer;

            this.MapChannelToJid(ChatChannelId.Proximity, "proximity");
            this.MapChannelToJid(ChatChannelId.Scene, "scene");

            this.mSession.OpenPrivateChannels(this.HandleInvitation, username + "@" + this.mServiceName);

            this.mSession.ShuttingdownEvent += this.Shutdown;

            Thread thread = new Thread(this.Start);
            thread.Start();
        }

        public void MapChannelToJid(ChatChannelId channel, String jid)
        {
            this.UnmapChannelToJid(channel, jid);

            this.mChannelJidMap.Add(channel, jid);
            this.mJidChannelMap.Add(jid, channel);
        }

        public void UnmapChannelToJid(ChatChannelId channel, String jid)
        {
            if (this.mChannelJidMap.ContainsKey(channel))
            {
                this.mChannelJidMap.Remove(channel);
            }

            if (this.mJidChannelMap.ContainsKey(jid))
            {
                this.mJidChannelMap.Remove(jid);
            }
        }

        private void HandleInvitation(ChatChannelId channel, String username)
        {
            JabberPresence jabberPresence = JabberPresence.Subscribe;
            this.MapChannelToJid(channel, username);

            JabberContact contact = null;
            String contactsJid = this.TryGetJid(channel);

            // If the contact is known ...
            if (null != contactsJid && this.mContacts.TryGetValue(contactsJid, out contact))
            {
                // If the private channel is not subscribed to ...
                if (contact.Subscription != "none")
                {
                    this.mSession.AcceptInvitation(channel, this.HandleMessage, this.HandlePresence);
                    contact.Subscription = "both";
                    this.SendRosterItem(contactsJid, false);
                    contact.Channel = channel;
                    jabberPresence = JabberPresence.Subscribed;
                }
            }

            if (null != contactsJid)
            {
                // This will ask the user to accept an invitation to subscribe.
                this.ChangeContactsPresence(contactsJid, jabberPresence, null);
            }
        }

        private void HandlePresence(ChatChannelId channel, BadumnaId userId, String displayName, ChatStatus status)
        {
            String show = null;
            JabberPresence jabberPresence = JabberPresence.Unchanged;
            JabberContact contact = null;
            String contactsJid = null;

            if (this.mDisplayNames.ContainsKey(userId))
            {
                this.mDisplayNames.Remove(userId);
            }

            if (status != ChatStatus.Offline)
            {
                this.mDisplayNames.Add(userId, displayName);
            }

            switch (status)
            {
                case ChatStatus.Away:
                    show = "away";
                    break;

                case ChatStatus.Chat:
                    show = "chat";
                    break;

                case ChatStatus.DoNotDisturb:
                    show = "dnd";
                    break;

                case ChatStatus.ExtendedAway:
                    show = "xa";
                    break;

                case ChatStatus.Offline:
                    jabberPresence = JabberPresence.Unavailable;
                    break;

                case ChatStatus.Online:
                    jabberPresence = JabberPresence.Available;

                    // The first online presence for private channels is equivalent to a jabber subscribed stanza. 
                    if (channel.Type == ChatChannelType.Private)
                    {
                        contactsJid = this.TryGetJid(channel);

                        if (null != contactsJid && this.mContacts.TryGetValue(contactsJid, out contact))
                        {
                            // If not the first online presence on this channel ...
                            if (contact.Subscription != "both")
                            {
                                jabberPresence = JabberPresence.Subscribed;
                                contact.Subscription = "to";
                                if (contact.Group.Length > 0)
                                {
                                    this.SendRosterItem(contactsJid, false);
                                }
                            }
                        }
                    }

                    break;
            }


            bool isGroup = channel.Type == ChatChannelType.Group || channel.Type == ChatChannelType.Proximity;

            String jid = this.TryGetJid(channel);
            if (null != jid)
            {
                String groupJid = jid;
                if (isGroup)
                {
                    jid += "/" + displayName;
                }

                if (isGroup)
                {
                    this.ChangeGroupPresence(jid, jabberPresence, JabberGroupAffiliation.Member, JabberGroupRole.Participant);
                }
                else
                {
                    this.ChangeContactsPresence(jid, jabberPresence, show);
                }

                if (displayName == channel.ToString() && isGroup)
                {
                    this.SetGroupSubject(groupJid, this.mUserJid.Split('@')[0], channel.Name);
                }
            }
        }

        private void HandleMessage(ChatChannelId channel, BadumnaId userId, String message)
        {
            JabberMessageType type = JabberMessageType.Normal;
            String jid = this.TryGetJid(channel);
            String displayName = null;

            if (channel.Type == ChatChannelType.Proximity)
            {
                displayName = userId.ToString();
            }
            else
            {
                this.mDisplayNames.TryGetValue(userId, out displayName);
            }

            if (null == displayName)
            {
                return; // Unknown user
            }

            if (channel.Type != ChatChannelType.Private)
            {
                jid += displayName;
            }

            if (channel.Type == ChatChannelType.Group || channel.Type == ChatChannelType.Proximity)
            {
                type = JabberMessageType.GroupChat;
            }

            if (jid != null)
            {
                this.SendChatMessage(jid, type, message);
            }
        }

        private void SendCommand(String command)
        {
            lock (this.mPendingClientMessages)
            {
                this.mWriter.Write(command);

                try
                {
                    this.mWriter.Flush();
                }
                catch { }
            }
        }

        public void Shutdown(object sender, EventArgs e)
        {
            this.mReader = null;
            this.SendCommand("</stream>");
            this.mWriter.Close();
            this.mWriter = null;

            this.SaveContacts();
        }

        private void Start()
        {
            try
            {
                while (null != this.mReader && this.mReader.Read())
                {
                    if (this.mReader.NodeType == XmlNodeType.Element)
                    {
                        XPathDocument doc = new XPathDocument(this.mReader.ReadSubtree());
                        XPathNavigator nav = doc.CreateNavigator();

                        Console.WriteLine(nav.OuterXml);

                        switch (this.mReader.LocalName)
                        {
                            case "iq":
                                if (nav.MoveToFirstChild())
                                {
                                    this.Iq(nav);
                                }
                                break;
                            case "presence":
                                if (nav.MoveToFirstChild())
                                {
                                    this.Presence(nav);
                                }
                                break;
                            case "message":
                                if (nav.MoveToFirstChild())
                                {
                                    this.MessageFromJabberClient(nav);
                                }
                                break;
                        }
                    }
                }
            }
            catch (IOException)
            {
                return;
            }
            catch (XmlException)
            {
            }
        }

        private ChatChannelId TryGetChannel(String longJid, bool create)
        {
            String jid = longJid;//.Split('@')[0];
            ChatChannelId channel = null;
            this.mJidChannelMap.TryGetValue(jid, out channel);

            if (null == channel && create)
            {
                int hashCode = jid.GetHashCode();
                ushort port = (ushort)hashCode;
                ushort id = (ushort)(hashCode >> 16);

                BadumnaId channelId = new BadumnaId(BadumnaId.ConstructUniqueID(PeerAddress.GetLoopback(port), id));
                channel = new ChatChannelId(ChatChannelType.Group, channelId, jid);

                this.MapChannelToJid(channel, jid);
            }

            return channel;
        }

        private String TryGetJid(ChatChannelId channel)
        {
            String jid = null;
            this.mChannelJidMap.TryGetValue(channel, out jid);
            return jid;
        }

        private void MessageFromJabberClient(XPathNavigator nav)
        {
            String type = nav.GetAttribute("type", "");
            String to = nav.GetAttribute("to", "");
            String from = nav.GetAttribute("from", "");

            String groupJid = String.Empty;
            String userNickname = String.Empty;

            if (to.Contains("/"))
            {
                groupJid = to.Split(new char[] { '/' })[0];
                userNickname = to.Split(new char[] { '/' })[1];
            }

            if (nav.MoveToChild("body", "jabber:client"))
            {
                String messageBody = nav.InnerXml;
                ChatChannelId channel = this.TryGetChannel(to, groupJid.Length > 0);

                if (channel != null)
                {
                    this.mSession.SendChannelMessage(channel, messageBody);
                }
            }
        }

        private void Presence(XPathNavigator nav)
        {
            String type = nav.GetAttribute("type", "");
            String to = nav.GetAttribute("to", "");
            String from = nav.GetAttribute("from", "");

            String groupJid = String.Empty;
            String userNickname = String.Empty;
            bool isGroupRequest = false;

            if (to.Contains("/"))
            {
                groupJid = to.Split(new char[] { '/' })[0];
                userNickname = to.Split(new char[] { '/' })[1];
            }

            if (nav.MoveToChild("x", "http://jabber.org/protocol/muc") && to.Contains("/"))
            {
                if (groupJid.Length != 0)
                {
                    isGroupRequest = true;
                }
                else
                {
                    this.SendCommand(String.Format("<presence from='{0}' to='{1}' type='error'><error code='400' type='modify'>{2}", groupJid, this.mUserJid,
                        "<jid-malformed xmlns='urn:ietf:params:xml:ns:xmpp-stanzas'/></error></presence>"));
                }
            }

            JabberContact contact = null;
            ChatChannelId channel = null;
            if (to.Length > 0)
            {
                channel = this.TryGetChannel(to, isGroupRequest);
            }

            if (nav.MoveToChild("show", "jabber:client"))
            {
                ChatStatus status = ChatStatus.Online;

                switch (nav.InnerXml.ToLower())
                {
                    case "away":
                        status = ChatStatus.Away;
                        break;

                    case "dnd":
                        status = ChatStatus.DoNotDisturb;
                        break;

                    case "chat":
                        status = ChatStatus.Chat;
                        break;

                    case "xa":
                        status = ChatStatus.ExtendedAway;
                        break;
                }

                this.mSession.ChangePresence(status);
                return;
            }

            switch (type)
            {
                case "unavailable": // -- Signals that the entity is no longer available for communication.
                    if (null != channel)
                    {
                        this.mSession.UnsubscribeFromChatChannel(channel);
                    }
                    break;
                case "subscribe": // -- The sender wishes to subscribe to the recipient's presence.
                    if (!this.mContacts.TryGetValue(to, out contact))
                    {
                        contact = new JabberContact(to);
                        this.mContacts.Add(to, contact);
                    }

                    if (null == channel)
                    {
                        // The local user has initiated the request.
                        this.SendRosterItem(to, true);
                        contact.Subscription = "to";
                        this.mSession.InviteUserToPrivateChannel(to);
                    }
                    else
                    {
                        // The local user is responding to an invitation.
                        contact.Channel = channel;
                        this.SendRosterItem(to, true);
                        this.mSession.AcceptInvitation(channel, this.HandleMessage, this.HandlePresence);
                    }

                    break;
                case "subscribed": // -- The sender has allowed the recipient to receive their presence.
                    if (null != channel)
                    {
                        if (this.mContacts.TryGetValue(to, out contact))
                        {
                            contact.Channel = channel;
                        }
                        else
                        {
                            contact = new JabberContact(to);
                            contact.Channel = channel;
                            this.mContacts.Add(to, contact);
                        }
                    }

                    break;
                case "unsubscribe": // -- The sender is unsubscribing from another entity's presence.
                    break;
                case "unsubscribed": //  -- The subscription request has been denied or a previously-granted subscription has been cancelled.
                    break;
                case "probe": // -- A request for an entity's current presence; SHOULD be generated only by a server on behalf of a user.
                    break;
                case "error": // -- An error has occurred regarding processing or delivery of a previously-sent presence stanza.
                    break;
                default: // online and available
                    if (isGroupRequest)
                    {
                        if (null != channel)
                        {
                            this.mSession.SubscribeToChatChannel(channel, userNickname, this.HandleMessage, this.HandlePresence);
                        }
                    }
                    else
                    {
                        this.mSession.ChangePresence(ChatStatus.Online);
                    }

                    break;
            }
        }

        private void Iq(XPathNavigator nav)
        {
            String type = nav.GetAttribute("type", "");
            switch (type)
            {
                case "get":
                    this.GetIq(nav, nav.GetAttribute("id", ""));
                    break;
                case "set":
                    this.SetIq(nav, nav.GetAttribute("id", ""));
                    break;
            }
        }

        private void GetIq(XPathNavigator nav, String id)
        {
            nav.MoveToFirstChild();
            switch (nav.LocalName)
            {
                case "query":
                    switch (nav.NamespaceURI)
                    {
                        case "jabber:iq:register":
                            this.RegisterAccount(nav, id);
                            break;

                        case "jabber:iq:roster":
                            this.SendRoster(id);
                            break;

                        case "http://jabber.org/protocol/disco#items":
                            this.SendConferenceItems(id);
                            break;

                        case "http://jabber.org/protocol/disco#info":
                            this.SendInfo(id);
                            break;

                        //case "jabber:iq:private":
                        //  this.PrivateRequest(nav, id);
                        // break;
                        default:
                            this.mWriter.Write(String.Format("<iq type='result' id='{0}'></iq>", id));
                            this.mWriter.Flush();
                            break;
                    }
                    break;

                case "ping":
                    this.mWriter.Write("<iq from='{2}' to='{1}' id='{0}' type='result'/>", id, this.mUserJid, this.mServiceName);
                    this.mWriter.Flush();
                    break;

                case "vCard":

                    break;

                case "error":
                    break;

            }
        }

        private void GetInfoForSomeone(XPathNavigator nav, String id)
        {
            this.mWriter.Write(String.Format("<iq type='result' id='{0}'><query xmlns='jabber:iq:register'><username/><password/></query></iq>", id));
            this.mWriter.Flush();
        }

        private void SendConferenceItems(String id)
        {
            String command = String.Format("<iq from='{0}' id='{1}' to='{2}' type='result'>", this.mServiceName, id, this.mUserJid);
            command += "<query xmlns='http://jabber.org/protocol/disco#items'>";

            foreach (KeyValuePair<String, ChatChannelId> room in this.mJidChannelMap)
            {
                command += String.Format("<item jid='{0}' name='{1}'/>", room.Key, room.Value.Name);
            }
            command += "</query></iq>";

            this.SendCommand(command);
        }

        private void SendInfo(String id)
        {
            String command =
            String.Format("<iq type='result' from='{2}' to='{0}' id='{1}'><query xmlns='http://jabber.org/protocol/disco#info'><identity category='conference' ",
            this.mUserJid, id, this.mServiceName);
            command += "type='text' name='Proximity'/><identity category='directory' type='chatroom' name='Proximity'/>";
            command += "<feature var='http://jabber.org/protocol/disco#info'/><feature var='http://jabber.org/protocol/disco#items'/><feature var='http://jabber.org/protocol/muc'/>";
            command += "<feature var='jabber:iq:register'/><feature var='jabber:iq:search'/><feature var='jabber:iq:time'/><feature var='jabber:iq:version'/>";
            command += "</query></iq>";

            this.SendCommand(command);
        }

        private void SetGroupSubject(String groupJid, String userName, String subject)
        {
            String command = String.Format("<message from='{0}/{1}' to='{0}/{1}' type='groupchat'><subject>{2}</subject></message>", groupJid, userName, subject);
            this.SendCommand(command);
        }

        private void SendInviteToUser(String mucRoomJid, String subject)
        {
            String command = String.Format("<message from='{0}' to='{1}'><x xmlns='http://jabber.org/protocol/muc#user'><invite from='{0}'/></x></message>", mucRoomJid, this.mUserJid);
            this.SendCommand(command);
        }

        private void PrepareForSubscription(String jid, String displayName)
        {
            if (!this.mContacts.ContainsKey(jid))
            {
                String commandFormat = "<iq type='set'><query xmlns='jabber:iq:roster'><item jid='{0}' subscription='none' ask='subscribe' name='{0}'>"
                + "</item></query></iq>";

                this.SendCommand(String.Format(commandFormat, jid, displayName));
            }
        }

        private void PrivateRequest(XPathNavigator nav, String id)
        {
            this.mWriter.Write(String.Format("<iq type='result' id='{0}'></iq>", id));
            this.mWriter.Write("</iq>");
            this.mWriter.Flush();
        }

        private void SendRoster(String id)
        {
            this.LoadContacts();

            this.mWriter.Write(String.Format("<iq type='result' id='{0}' xmlns='jabber:client'>", id, this.mUserJid, this.mResource));
            this.mWriter.Write("<query xmlns='jabber:iq:roster'>");

            // .. Write user contact here.
            this.mWriter.Write(String.Format("<item jid='gateway@{0}' name='Gateway' subscription='both'><group>Badumna</group></item>", this.mServiceName));

            foreach (KeyValuePair<String, JabberContact> contactKVP in this.mContacts)
            {
                this.mWriter.Write(String.Format("<item jid='{0}' name='{1}' subscription='{2}'><group>{3}</group></item>",
                    contactKVP.Value.Jid, contactKVP.Value.Name, contactKVP.Value.Subscription, contactKVP.Value.Group));
            }

            this.mWriter.Write("</query></iq>");
            this.mWriter.Flush();

            this.ChangeContactsPresence("gateway@" + this.mServiceName, JabberPresence.Available, "available");
        }

        public void ChangeContactsPresence(String jid, JabberPresence type, String show)
        {
            String command = String.Empty;
            switch (type)
            {
                case JabberPresence.Unavailable:
                    command = String.Format("<presence from='{0}' to='{1}' type='unavailable'>", jid, this.mUserJid + "/" + this.mResource);
                    break;

                case JabberPresence.Available:
                    command = String.Format("<presence from='{0}' to='{1}'>", jid, this.mUserJid + "/" + this.mResource);
                    break;

                case JabberPresence.Subscribe:
                    command = String.Format("<presence from='{0}' to='{1}' type='subscribe'>", jid, this.mUserJid);
                    break;

                default: // Unchanged
                    command = String.Format("<presence from='{0}' to='{1}'>", jid, this.mUserJid + "/" + this.mResource);
                    break;
            }

            if (null != show)
            {
                command += String.Format("<show>{0}</show>", show);
            }

            command += "</presence>";
            this.SendCommand(command);
        }


        private void ChangeGroupPresence(String groupJid, JabberPresence type, JabberGroupAffiliation affliation, JabberGroupRole role)
        {
            String command = String.Empty;
            switch (type)
            {
                case JabberPresence.Unavailable:
                    command = String.Format("<presence from='{0}' to='{1}' type='{2}'>", groupJid, this.mUserJid + "/" + this.mResource, type.ToString().ToLower());
                    break;

                case JabberPresence.Available:
                    command = String.Format("<presence from='{0}' to='{1}'>", groupJid, this.mUserJid + "/" + this.mResource);
                    break;

                default:
                    command = String.Format("<presence from='{0}' to='{1}'>", groupJid, this.mUserJid + "/" + this.mResource);
                    break;
            }

            command += String.Format("<x xmlns='http://jabber.org/protocol/muc#user'><item affiliation='{0}' role='{1}'/></x></presence>", affliation, role);

            this.SendCommand(command);
        }

        public void SendChatMessage(String jid, JabberMessageType type, String message)
        {
            int l = message.Length;
            XmlDocument document = new XmlDocument();

            XmlNode messageNode = document.CreateElement("message");
            XmlAttribute fromAttrobute = document.CreateAttribute("from");
            XmlAttribute toAttribute = document.CreateAttribute("to");
            XmlAttribute typeAttribute = document.CreateAttribute("type");
            XmlNode bodyNode = document.CreateElement("body");

            fromAttrobute.Value = jid;
            toAttribute.Value = this.mUserJid + "/" + this.mResource;
            typeAttribute.Value = type.ToString().ToLower();

            bodyNode.InnerXml = System.Security.SecurityElement.Escape(message);

            messageNode.Attributes.Append(fromAttrobute);
            messageNode.Attributes.Append(toAttribute);
            messageNode.Attributes.Append(typeAttribute);
            messageNode.AppendChild(bodyNode);

            /*
                String command = String.Format("<message from='{0}' to='{1}' type='{2}'><body>{3}</body></message>",
                jid, this.mUserJid + "/" + this.mResource, type.ToString().ToLower(), message);
            */
            this.SendCommand(messageNode.OuterXml);
        }

        private void RegisterAccount(XPathNavigator nav, String id)
        {
            //this.mStreamWriter.Write(String.Format("<iq type='error' id='{0}'><error type='cancel'>Blah blah blah</error></iq>", id));
            this.mWriter.Write(String.Format("<iq type='result' id='{0}'><query xmlns='jabber:iq:register'><username/><password/></query></iq>", id));
            this.mWriter.Flush();
        }

        private void SetIq(XPathNavigator nav, String id)
        {
            nav.MoveToFirstChild();
            switch (nav.LocalName)
            {
                case "query":
                    switch (nav.NamespaceURI)
                    {
                        case "jabber:iq:register":
                            this.RegisterAccount(nav, id);
                            break;

                        case "jabber:iq:roster":
                            this.QueryUser(nav, id);
                            break;
                    }
                    break;

                case "bind":
                    this.mWriter.Write(String.Format("<iq from='{1}' type='result' id='{0}'/>", id, this.mServiceName));
                    this.mWriter.Flush();
                    break;

                case "session":
                    this.mWriter.Write(String.Format("<iq from='{1}' type='result' id='{0}'/>", id, this.mServiceName));
                    this.mWriter.Flush();
                    break;

                case "error":
                    break;
            }
        }

        private void QueryUser(XPathNavigator nav, String id)
        {
            if (nav.MoveToChild("item", "jabber:iq:roster"))
            {
                String jid = nav.GetAttribute("jid", "");
                String name = nav.GetAttribute("name", "");
                String subscription = nav.GetAttribute("subscription", ""); // possibly == to 'remove'
                String group = "";

                if (nav.MoveToChild("group", "jabber:iq:roster"))
                {
                    group = nav.InnerXml;
                }

                /*
                String command = String.Format("<iq type='result'><query xmlns='jabber:iq:roster'><item name=\"{1}\" jid=\"{2}\" subscription=\"none\">{3}</query></iq>",
                    id, name, jid, String.Format("<group>{0}</group>", group));
                */

                String command = String.Format("<iq type='result' id='{0}'/>", id);
                JabberContact contact = null;

                if (this.mContacts.TryGetValue(jid, out contact))
                {
                    if (subscription == "remove")
                    {
                        if (null != contact.Channel)
                        {
                            this.UnmapChannelToJid(contact.Channel, jid);
                            contact.Channel = null;
                        }
                        ChatChannelId channel = this.TryGetChannel(jid, false);
                        if (null != channel)
                        {
                            this.UnmapChannelToJid(channel, jid);
                            this.mSession.UnsubscribeFromChatChannel(channel);
                        }
                        this.mContacts.Remove(jid);
                        this.SendRosterItem(jid, false);
                        this.SendCommand(command);
                        return;
                    }
                }
                else if (subscription == "remove")
                {
                    this.SendCommand(command);
                    return;
                }
                else
                {
                    contact = new JabberContact(jid);
                    this.mContacts.Add(jid, contact);
                }

                if (name.Length == 0)
                {
                    try
                    {
                        name = jid.Split('@')[0];
                    }
                    catch { }
                }

                contact.Name = name;
                contact.Group = group;

                this.SendRosterItem(jid, false);
                this.SendCommand(command);
            }
        }

        private void SendRosterItem(String jid, bool isSubscribing)
        {
            JabberContact contact = null;

            if (this.mContacts.TryGetValue(jid, out contact))
            {
                String asking = "";
                String name = "";
                String group = "";

                if (isSubscribing)
                {
                    asking = " ask=\"subscribe\"";
                }
                if (contact.Name.Length > 0)
                {
                    name = " name=\"" + contact.Name + "\"";
                }
                if (contact.Group.Length > 0)
                {
                    group = String.Format("<group>{0}</group>", contact.Group);
                }

                String item = String.Format("<item {0} jid=\"{1}\" subscription=\"{2}\" {4}>{3}</item>", name, jid, contact.Subscription, group, asking);

                String command = String.Format("<iq type='set'><query xmlns='jabber:iq:roster'>{0}</query></iq>", item);

                this.SendCommand(command);
            }
            else
            {
                String item = String.Format("<item jid=\"{0}\" subscription=\"remove\"></item>", jid);
                String command = String.Format("<iq type='set'><query xmlns='jabber:iq:roster'>{0}</query></iq>", item);

                this.SendCommand(command);
            }

        }

        private void SaveContacts()
        {
            try
            {
                using (FileStream fileStream = new FileStream("Contacts.dat", FileMode.OpenOrCreate, FileAccess.Write))
                {
                    try
                    {
                        BinaryFormatter formatter = new BinaryFormatter();
                        formatter.Serialize(fileStream, this.mContacts);
                    }
                    finally
                    {
                        fileStream.Close();
                    }
                }
            }
            catch { }
        }

        private void LoadContacts()
        {
            try
            {
                using (FileStream fileStream = new FileStream("Contacts.dat", FileMode.Open, FileAccess.Read))
                {
                    try
                    {
                        BinaryFormatter binFormatter = new BinaryFormatter();
                        this.mContacts = (Dictionary<String, JabberContact>)binFormatter.Deserialize(fileStream);
                    }
                    finally
                    {
                        fileStream.Close();
                    }

                    foreach (KeyValuePair<String, JabberContact> contactKVP in this.mContacts)
                    {
                        if (contactKVP.Value.Subscription != "none")
                        {
                            this.mSession.InviteUserToPrivateChannel(contactKVP.Value.Jid);
                        }
                    }
                }
            }
            catch { }
        }
    }
}
