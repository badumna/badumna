﻿//-----------------------------------------------------------------------
// <copyright file="SignedUserPresenceStatus.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2012 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using Badumna.Core;
using Badumna.DataTypes;
using Badumna.Security;
using Badumna.Utilities;

namespace Badumna.Chat
{
    /// <summary>
    /// A signed container for a UserPresenceStatus
    /// </summary>
    internal class SignedUserPresenceStatus : SignedUserData<UserPresenceStatus>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SignedUserPresenceStatus"/> class.
        /// </summary>
        /// <param name="status">The status.</param>
        /// <param name="userCertificate">The user's certificate token.</param>
        public SignedUserPresenceStatus(UserPresenceStatus status, ICertificateToken userCertificate)
            : base(status, userCertificate)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SignedUserPresenceStatus"/> class.
        /// Should only be used by IParseable
        /// </summary>
        protected SignedUserPresenceStatus()
            : base()
        {
        }

        /// <summary>
        /// Gets the Chat user.
        /// </summary>
        internal ChatUser User
        {
            get { return this.Status.User; }
        }

        /// <summary>
        /// Gets the session id.
        /// </summary>
        internal BadumnaId SessionId
        {
            get { return this.User.SessionId; }
        }

        /// <summary>
        /// Gets the user presence status.
        /// </summary>
        internal Character Character
        {
            get { return this.Status.Character; }
        }

        /// <summary>
        /// Gets the (unsigned) UserPresenceStatus.
        /// </summary>
        internal UserPresenceStatus Status
        {
            get { return this.Value; }
        }

        /// <summary>
        /// Gets the status.
        /// </summary>
        internal ChatStatus ChatStatus
        {
            get { return this.Status.Status; }
        }

        /// <inheritdoc/>
        protected override Type ValueType
        {
            get { return typeof(UserPresenceStatus); }
        }

        /// <summary>
        /// Returns a new SignedUserPresenceStatus with the same status, but
        /// signed with the given UserCertificate.
        /// </summary>
        /// <param name="newCertificate">The new certificate token.</param>
        /// <returns>A new SignedUserPresenceStatus</returns>
        internal SignedUserPresenceStatus WithCertificate(CertificateToken newCertificate)
        {
            return new SignedUserPresenceStatus(this.Status, newCertificate);
        }
    }
}
