using System;
using System.Collections.Generic;
using Badumna.Core;
using Badumna.DataTypes;
using Badumna.Utilities;
using Badumna.Utilities.Logging;
using Badumna.Security;

namespace Badumna.Chat
{
    internal interface IChatService
    {
        IInternalChatSession CreateSession(ITokenCache tokenCache);
        void Initialize();
        void Shutdown();
        void Logout();

        void ReceiveMessage(ChatChannelId channelId, BadumnaId senderId, string message);
        void ReceivePresence(ChatChannelId channel, UserPresenceStatus status);
        void ReceivePresence(ChatChannelId channel, UserPresenceStatus status, ChatNotificationType notificationType);
        void ReceiveInvitation(ChatChannelId channel, ChatUser requester, BadumnaId sessionId);
    }

    internal interface IInternalChatService : IChatService
    {
        bool DoWhenOnline(GenericCallBack action);
        bool DoWhenOnlineReplacingExisting(BadumnaId key, GenericCallBack action);
    }

    internal delegate void UserPresenceTask(UserPresenceStatus userStatus);

    /// <summary>
    /// The badumna chat service. 
    /// </summary>
    internal class ChatService : IInternalChatService
    {
        /// <summary>
        /// All created sessions. 
        /// </summary>
        private ChatSessionCollection mSessions;

        /// <summary>
        /// The queue to use for scheduling events.
        /// </summary>
        private NetworkEventQueue eventQueue;

        /// <summary>
        /// Reports on network connectivity status.
        /// </summary>
        private INetworkConnectivityReporter connectivityReporter;

        /// <summary>
        /// Provides notification of peer connections and disconnections.
        /// </summary>
        private IPeerConnectionNotifier connectionNotifier;

        /// <summary>
        /// Holds the factory for constructing new instances of ChatSession
        /// </summary>
        private ChatSessionFactory chatSessionFactory;

        /// <summary>
        /// Holds *at most* one event per-session, used for
        /// sending the most recent status for each session when the
        /// network comes online.
        /// </summary>
        private Dictionary<BadumnaId, GenericCallBack> pendingSessionEvent;

        /// <summary>
        /// A lock object for `pendingSessionEvent`. Required since
        /// we use this object from public API-accessible code and
        /// from an event delegate (NetworkOnlineEvent).
        /// </summary>
        private object pendingSessionEventLock = new object();

        /// <summary>
        /// Holds all messages that are waiting to be sent (in order)
        /// when the network comes online.
        /// </summary>
        private List<GenericCallBack> orderedOnlineEvents;

        /// <summary>
        /// A lock object for `orderedOnlineEvents`. Required since
        /// we use this object from public API-accessible code and
        /// from an event delegate (NetworkOnlineEvent).
        /// </summary>
        private object orderedOnlineEventsLock = new object();
        
        /// <summary>
        /// The chat protocol facade instance, shared by all sessions.
        /// </summary>
        private IChatProtocolFacade chatProtocolFacade;

        /// <summary>
        /// Initializes a new instance of the <see cref="ChatService"/> class.
        /// </summary>
        /// <param name="chatProtocolFacadeFactory">A delegate that will generate a ChatProtocolFacade.</param>
        /// <param name="chatSessionFactory">A delegate that will generate a ChatSession given a ChatService and a display name.</param>
        /// <param name="eventQueue">The event queue to use for scheduling events.</param>
        /// <param name="connectivityReporter">Reports on network connectivity status.</param>
        /// <param name="connectionNotifier">Provides notification of peer connections and disconnections.</param>
        internal ChatService(
            ChatProtocolFacadeFactory chatProtocolFacadeFactory,
            ChatSessionFactory chatSessionFactory,
            NetworkEventQueue eventQueue,
            INetworkConnectivityReporter connectivityReporter,
            IPeerConnectionNotifier connectionNotifier)
        {
            this.chatProtocolFacade = chatProtocolFacadeFactory(this);
            this.chatSessionFactory = chatSessionFactory;
            this.eventQueue = eventQueue;
            this.connectivityReporter = connectivityReporter;
            this.connectionNotifier = connectionNotifier;

            this.mSessions = new ChatSessionCollection();
            this.orderedOnlineEvents = new List<GenericCallBack>();
            this.pendingSessionEvent = new Dictionary<BadumnaId, GenericCallBack>();
        }

        /// <summary>
        /// Initializes this instance.
        /// </summary>
        void IChatService.Initialize()
        {
            this.connectivityReporter.NetworkOnlineEvent += this.CurrentContext_NetworkOnlineEvent;
            this.connectivityReporter.NetworkOfflineEvent += this.CurrentContext_NetworkOfflineEvent;
            this.connectivityReporter.NetworkShuttingDownEvent += this.CurrentContext_NetworkShuttingDownEvent;

            this.connectionNotifier.ConnectionLostEvent += this.ConnectionLostEvent;

            if (this.connectivityReporter.Status == ConnectivityStatus.Online)
            {
                this.CurrentContext_NetworkOnlineEvent();
            }
        }

        void CurrentContext_NetworkOnlineEvent()
        {
            // Allow time for the dht and multicast groups to initialize.
            this.eventQueue.Schedule(5, this.SendPendingEvents);
        }

        private void CurrentContext_NetworkShuttingDownEvent()
        {
            // TODO: each session.Logout() triggers a status change to offline,
            // which is never sent (because the network connectivity status is offline!).
            ((IInternalChatService)this).Logout();
        }
        
        private void CurrentContext_NetworkOfflineEvent()
        {
            Logger.TraceInformation(LogTag.Event | LogTag.Chat | LogTag.Connection, "Network offline event triggered");
        }

        void IChatService.Shutdown()
        {
            this.connectivityReporter.NetworkOnlineEvent -= this.CurrentContext_NetworkOnlineEvent;
            this.connectivityReporter.NetworkOfflineEvent -= this.CurrentContext_NetworkOfflineEvent;
            this.connectivityReporter.NetworkShuttingDownEvent -= this.CurrentContext_NetworkShuttingDownEvent;
            this.connectionNotifier.ConnectionLostEvent -= this.ConnectionLostEvent;
            ((IInternalChatService)this).Logout();
        }

        void IChatService.Logout()
        {
            foreach (IInternalChatSession session in this.mSessions)
            {
                session.Shutdown();
            }
        }

        public IInternalChatSession CreateSession(ITokenCache tokenCache)
        {
            Character character = tokenCache.GetUserCertificateToken().Character;
            if (character == null)
            {
                throw new InvalidOperationException("Can't create a chat session without logging in as a character");
            }

            IInternalChatSession session = this.chatSessionFactory(tokenCache, this, this.chatProtocolFacade);
            Logger.TraceInformation(LogTag.Chat | LogTag.Event, "ChatManager created a new session for local user \"" + character + "\"");
            this.mSessions.Add(session);
            session.LoggingOutEvent += this.SessionLogoutHandler;
            return session;
        }

        private void SessionLogoutHandler(object sender, EventArgs e)
        {
            this.RemoveSession((IInternalChatSession)sender);
        }

        private void RemoveSession(IInternalChatSession session)
        {
            this.mSessions.Remove(session);
        }

        bool IInternalChatService.DoWhenOnline(GenericCallBack action)
        {
            if (this.connectivityReporter.Status == ConnectivityStatus.Online)
            {
                action();
                return true;
            }
            else
            {
                Logger.TraceInformation(LogTag.Event | LogTag.Chat | LogTag.Connection, "Delaying action until online event occurs");
                lock (this.orderedOnlineEventsLock)
                {
                    this.orderedOnlineEvents.Add(action);
                }
                return false;
            }
        }

        bool IInternalChatService.DoWhenOnlineReplacingExisting(BadumnaId sessionId, GenericCallBack action)
        {
            if (this.connectivityReporter.Status == ConnectivityStatus.Online)
            {
                action();
                return true;
            }
            else
            {
                Logger.TraceInformation(LogTag.Event | LogTag.Chat | LogTag.Connection, "Delaying online action for session " + sessionId + " until online event occurs");
                lock (this.pendingSessionEventLock)
                {
                    this.pendingSessionEvent[sessionId] = action;
                }
                return false;
            }
        }

        private void SendPendingEvents()
        {
            Logger.TraceInformation(LogTag.Event | LogTag.Chat | LogTag.Connection, "Sending pending events now that the connection status is " + this.connectivityReporter.Status);
            lock (this.pendingSessionEventLock)
            {
                Logger.TraceInformation(LogTag.Event | LogTag.Chat | LogTag.Connection, "Sending " + this.pendingSessionEvent.Count + " session events");
                foreach (GenericCallBack action in this.pendingSessionEvent.Values)
                {
                    action();
                }
                this.pendingSessionEvent.Clear();
            }

            lock (this.orderedOnlineEventsLock)
            {
                Logger.TraceInformation(LogTag.Event | LogTag.Chat | LogTag.Connection, "Sending " + this.pendingSessionEvent.Count + " ordered events");
                foreach (GenericCallBack action in this.orderedOnlineEvents)
                {
                    action();
                }
                this.orderedOnlineEvents.Clear();
            }
        }

        private void ConnectionLostEvent(PeerAddress address)
        {
            Logger.TraceInformation(LogTag.Connection | LogTag.Chat, "Connection lost to address: {0}", address);
            foreach (var session in this.mSessions)
            {
                foreach (var channel in session.Channels)
                {
                    var channelId = channel.Id;
                    foreach (ChatUser user in channel.Participants)
                    {
                        if (user.SessionId.Address.Equals(address))
                        {
                            this.eventQueue.Push(this.ReceivePresence, channelId, new UserPresenceStatus(user, ChatStatus.Offline), ChatNotificationType.Default);
                        }
                    }
                }
            }
        }

        void IChatService.ReceiveMessage(ChatChannelId channelId, BadumnaId senderId, string message)
        {
            this.mSessions.SessionPerform(channelId, s => s.ReceiveMessage(channelId, senderId, message));
        }

        public void ReceivePresence(ChatChannelId channel, UserPresenceStatus status)
        {
            this.ReceivePresence(channel, status, ChatNotificationType.Default);
        }

        public void ReceivePresence(ChatChannelId channel, UserPresenceStatus status, ChatNotificationType notificationType)
        {
            this.mSessions.SessionPerform(channel, s => s.ReceivePresence(channel, status, notificationType));
        }

        public void ReceiveInvitation(ChatChannelId channel, ChatUser requester, BadumnaId sessionId)
        {
            this.mSessions.SessionPerform(sessionId, s => s.ReceiveInvitation(channel, requester));
        }

        /// <summary>
        /// Internal, used for testing only.
        /// </summary>
        internal IEnumerable<IInternalChatSession> Sessions
        {
            get { return new List<IInternalChatSession>(this.mSessions); }
        }
    }
}
