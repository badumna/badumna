//---------------------------------------------------------------------------------
// <copyright file="PresenceReplica.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2011 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Diagnostics;
using Badumna.Core;
using Badumna.DataTypes;
using Badumna.DistributedHashTable;
using Badumna.Utilities;
using Badumna.Utilities.Logging;

namespace Badumna.Chat
{
    /// <summary>
    /// The object used to store presence information on DHT. 
    /// </summary>
    internal class PresenceReplica : IReplicatedObject
    {
        // TODO: this class is used to store two fairly different
        // kinds of presence - the two cases should be separated somehow:
        //  - name only: a placeholder for a user who is not currently online. Anyone can publish / modify such a replica
        //  - signedUserPresence: a user's known status - can only be stored on the DHT if the signature is valid

        /// <summary>
        /// The status, if known.
        /// </summary>
        private SignedUserPresenceStatus status;

        /// <summary>
        /// The character name (if status is unknown)
        /// </summary>
        private string characterName;

        /// <summary>
        /// The ID (if status is unknown)
        /// </summary>
        private BadumnaId guid;

        /// <summary>
        /// A list of pending requests - requests that occurred while the subject was offline.
        /// </summary>
        private Queue<RequestInfo> pendingRequests = new Queue<RequestInfo>();

        /// <summary>
        /// Initializes a new instance of the <see cref="PresenceReplica"/> class.
        /// </summary>
        /// <param name="characterName">The characterName.</param>
        /// <param name="id">The id for the replica</param>
        public PresenceReplica(string characterName, BadumnaId id)
        {
            this.Init(null, characterName, id);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PresenceReplica"/> class.
        /// </summary>
        /// <param name="status">The status.</param>
        public PresenceReplica(SignedUserPresenceStatus status)
        {
            this.Init(status, null, null);
        }

        /// <summary>
        /// Prevents a default instance of the <see cref="PresenceReplica"/> class from being created.
        /// Required by deserialization. 
        /// </summary>
        private PresenceReplica()
        {
        }

        /// <summary>
        /// The delegate task called on each pending request. 
        /// </summary>
        /// <param name="requester">The requesting user.</param>
        internal delegate void PendingRequestTask(SignedChatUser requester);

        /// <summary>
        /// Gets or sets the current status.
        /// </summary>
        /// <value>The current status, or `null` if status is unknown.</value>
        public SignedUserPresenceStatus PresenceStatus
        {
            get
            {
                return this.status;
            }

            set
            {
                this.Init(value, null, null);
            }
        }

        /// <inheritdoc/>
        public BadumnaId Guid
        {
            get
            {
                if (this.status != null)
                {
                    return this.status.SessionId;
                }

                return this.guid;
            }

            set
            {
                this.guid = value;
            }
        }

        /// <summary>
        /// Gets the name of the character.
        /// </summary>
        public string CharacterName
        {
            get
            {
                if (this.StatusKnown)
                {
                    return this.status.Character.Name;
                }

                return this.characterName;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the status of the user is known.
        /// </summary>
        public bool StatusKnown
        {
            get { return this.status != null; }
        }

        /// <summary>
        /// Gets a value indicating whether this user's status is both known and not offline.
        /// </summary>
        public bool IsOnline
        {
            get { return this.StatusKnown && this.PresenceStatus.ChatStatus != ChatStatus.Offline; }
        }

        /// <summary>
        /// Gets the number of replicas required.
        /// </summary>
        /// <value>The number of replicas.</value>
        public int NumberOfReplicas
        {
            get { return 5; }
        }

        /// <summary>
        /// Gets a value indicating whether to ignore modification number.
        /// </summary>
        /// <value>
        /// <c>true</c> if ignore modification number; otherwise, <c>false</c>.
        /// </value>
        public bool IgnoreModificationNumber
        {
            get { return false; }
        }

        /// <summary>
        /// Gets or sets the modification number.
        /// </summary>
        /// <value>The modification number.</value>
        public CyclicalID.UShortID ModificationNumber
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the key.
        /// </summary>
        /// <value>The key of this object.</value>
        public HashKey Key
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the time to live seconds.
        /// </summary>
        /// <value>The time to live seconds.</value>
        public uint TimeToLiveSeconds
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the expiration time.
        /// </summary>
        /// <value>The expiration time.</value>
        public TimeSpan ExpirationTime
        {
            get;
            set;
        }

        /// <summary>
        /// Adds a pending request.
        /// </summary>
        /// <param name="signedUser">The requesting user.</param>
        public void AddPendingRequest(SignedChatUser signedUser)
        {
            var existingRequest = CollectionUtils.Find(this.pendingRequests, req => req.User.Equals(signedUser));
            if (existingRequest != null)
            {
                return;
            }

            if (this.pendingRequests.Count >= Parameters.MaximumPendingPresenceEvents)
            {
                this.pendingRequests.Dequeue();
            }

            Logger.TraceInformation(
                LogTag.Event | LogTag.Presence,
                "Adding a new pending request ({0}) to {1} pending requests: {2}",
                signedUser.User,
                this.pendingRequests.Count,
                CollectionUtils.Join(CollectionUtils.Map(this.pendingRequests, r => r.User), ", "));

            RequestInfo request = new RequestInfo(signedUser);
            this.pendingRequests.Enqueue(request);
        }

        /// <summary>
        /// Invoke the specified task on each pending request. 
        /// </summary>
        /// <param name="task">The delegate task.</param>
        public void ForEachPendingRequest(PendingRequestTask task)
        {
            while (this.pendingRequests.Count > 0)
            {
                RequestInfo request = this.pendingRequests.Dequeue();
                task(request.User);
            }
        }

        #region IParseable Members

        /// <summary>
        /// Toes the message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="parameterType">Type of the parameter.</param>
        public void ToMessage(MessageBuffer message, Type parameterType)
        {
            bool hasStatus = this.StatusKnown;
            message.Write(hasStatus);
            if (hasStatus)
            {
                message.Write(this.status);
            }
            else
            {
                message.Write(this.characterName);
                message.Write(this.Guid);
            }

            message.Write((byte)this.pendingRequests.Count);
            foreach (RequestInfo request in this.pendingRequests)
            {
                message.Write(request.User);
            }
        }

        /// <summary>
        /// Froms the message.
        /// </summary>
        /// <param name="message">The message.</param>
        public void FromMessage(MessageBuffer message)
        {
            bool hasStatus = message.ReadBool();
            if (hasStatus)
            {
                this.Init(message.Read<SignedUserPresenceStatus>(), null, null);
            }
            else
            {
                this.Init(null, message.ReadString(), message.Read<BadumnaId>());
            }

            this.pendingRequests.Clear();
            byte numberOfPendingRequest = message.ReadByte();
            for (int i = 0; i < numberOfPendingRequest; i++)
            {
                this.pendingRequests.Enqueue(new RequestInfo(message.Read<SignedChatUser>()));
            }
        }

        #endregion

        /// <summary>
        /// Initializes all instance variables.
        /// </summary>
        /// <param name="status">The status.</param>
        /// <param name="characterName">Name of the character.</param>
        /// <param name="guid">The unique ID.</param>
        private void Init(SignedUserPresenceStatus status, string characterName, BadumnaId guid)
        {
            // A presence replica should have either (status), or (characterName and guid):
            if (status != null)
            {
                Debug.Assert(characterName == null && guid == null, "characterName or guid given");
            }
            else
            {
                Debug.Assert(characterName != null && guid != null, "characterName or guid missing");
            }

            this.status = status;
            this.characterName = characterName;
            this.guid = guid;
        }

        /// <summary>
        /// The request info. 
        /// </summary>
        private class RequestInfo
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="PresenceReplica.RequestInfo"/> class.
            /// </summary>
            /// <param name="user">The requesting user.</param>
            public RequestInfo(SignedChatUser user)
            {
                this.User = user;
            }

            /// <summary>
            /// Gets or sets the user.
            /// </summary>
            /// <value>The chat user.</value>
            public SignedChatUser User
            {
                get;
                set;
            }
        }
    }
}
