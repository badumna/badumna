//-----------------------------------------------------------------------
// <copyright file="ProximityChatProtocol.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using Badumna.Core;
using Badumna.DataTypes;
using Badumna.Replication;
using Badumna.Utilities;
using Badumna.Utilities.Logging;

namespace Badumna.Chat
{
    interface IProximityChatProtocol
    {
        void SendProximityChatMessage(BadumnaId sourceEntityId, string message);
    }

    /// <summary>
    /// The proximity chat protocol. 
    /// </summary>
    internal class ProximityChatProtocol : EntityProtocol, IProximityChatProtocol
    {
        /// <summary>
        /// The entity manager.
        /// </summary>
        private EntityManager entityManager;

        /// <summary>
        /// The chat service manager. 
        /// </summary>
        private IInternalChatService chatService;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProximityChatProtocol"/> class.
        /// </summary>
        /// <param name="entityManager">The entity manager.</param>
        /// <param name="addressProvider">Provides the current public address.</param>
        /// <param name="chatService">The chat session.</param>
        public ProximityChatProtocol(EntityManager entityManager, INetworkAddressProvider addressProvider, IInternalChatService chatService)
            : base(entityManager, addressProvider)
        {
            this.entityManager = entityManager;
            this.chatService = chatService;
        }

        /// <summary>
        /// Logs out this instance.
        /// </summary>
        public void Logout()
        {
        }

        /// <summary>
        /// Sends the proximity chat message.
        /// </summary>
        /// <param name="sourceEntityId">The source entity id.</param>
        /// <param name="message">The proximity message.</param>
        public void SendProximityChatMessage(BadumnaId sourceEntityId, string message)
        {
            if (null == sourceEntityId)
            {
                return;
            }

            EntityEnvelope envelope = this.GetMessageFrom(sourceEntityId, QualityOfService.Reliable);

            envelope.Qos.Priority = QosPriority.High;
            this.entityManager.RemoteCall(envelope, this.ReceiveProximityChatMessage, message);
            this.entityManager.SendProximityMessage(sourceEntityId, envelope);
        }

        /// <summary>
        /// Receives the proximity chat message.
        /// </summary>
        /// <param name="message">The message.</param>
        [EntityProtocolMethod(EntityMethod.ReceiveProximityChatMessage)]
        public void ReceiveProximityChatMessage(string message)
        {
            BadumnaId senderId = this.CurrentEnvelope.SourceEntity;
            BadumnaId receiverId = this.CurrentEnvelope.DestinationEntity;
            ChatChannelId channelId = new ChatChannelId(ChatChannelType.Proximity, receiverId);
            Logger.TraceInformation(LogTag.Chat | LogTag.Event, "Proximity chat message received for channel " + channelId + " from sender " + senderId + ", message: " + message);
            this.chatService.ReceiveMessage(channelId, senderId, message);
        }
    }
}
