//-----------------------------------------------------------------------
// <copyright file="PrivateChatProtocol.cs" company="NICTA">
//     Copyright (c) 2011 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;

using Badumna.Core;
using Badumna.Utilities;
using Badumna.Transport;
using Badumna;
using Badumna.DataTypes;
using Badumna.Security;
using System.Diagnostics;

namespace Badumna.Chat
{
    internal delegate IPrivateChatProtocol PrivateProtocolFactory(PresenceService presenceService, IInternalChatService chatService);

    interface IPrivateChatProtocol
    {
        void JoinChannel(BadumnaId channelId, ChatUser user);
        void ChangePresence(BadumnaId channelId, UserPresenceStatus presenceStatus, ChatNotificationType notificationType);
        void SendChatMessage(BadumnaId channelId, BadumnaId userId, string message);
    }

    class PrivateChatProtocol : IPrivateChatProtocol
    {
        internal static PrivateProtocolFactory Factory(ProtocolComponent<TransportEnvelope> protocolComponent)
        {
            return (presenceService, chatService) => new PrivateChatProtocol(protocolComponent, presenceService, chatService);
        }

        private IPresenceService mPresenceService;
        private IInternalChatService mService;
        private ProtocolComponent<TransportEnvelope> protocolComponent;

        public PrivateChatProtocol(ProtocolComponent<TransportEnvelope> protocolComponent, IPresenceService presenceService, IInternalChatService chatService)
        {
            this.protocolComponent = protocolComponent;
            protocolComponent.Parser.RegisterMethodsIn(this);
            this.mService = chatService;
            this.mPresenceService = presenceService;
            this.mPresenceService.PresenceEvent += this.mPresenceService_PrecenceEvent;
            this.mPresenceService.PresenceSubscriptionEvent += this.mPresenceService_PrecenceSubscriptionEvent;
        }

        protected virtual ITransportEnvelope CurrentEnvelope
        {
            get { return this.protocolComponent.CurrentEnvelope; }
        }

        private Character CurrentSourceCharacter
        {
            get
            {
                return this.CurrentEnvelope.Certificate.Character;
            }
        }

        private TransportEnvelope GetMessageFor(PeerAddress address, QualityOfService qos)
        {
            TransportEnvelope envelope = new TransportEnvelope(address, qos);
            this.protocolComponent.PrepareMessageForDeparture(ref envelope);
            return envelope;
        }

        private void mPresenceService_PrecenceEvent(object sender, PresenceEventArgs e)
        {
            ChatChannelId channel = new ChatChannelId(ChatChannelType.Private, new BadumnaId(e.Status.SessionId), e.Status.Character.Name);
            this.mService.ReceivePresence(channel, e.Status);
        }

        private void mPresenceService_PrecenceSubscriptionEvent(object sender, PresenceSubscriptionEventArgs e)
        {
            ChatChannelId channel = new ChatChannelId(ChatChannelType.Private, e.Requester.SessionId);
            this.mService.ReceiveInvitation(channel, e.Requester, e.Subject);
        }

        public void ChangePresence(BadumnaId channelId, UserPresenceStatus presenceStatus, ChatNotificationType notificationType)
        {
            // Send the presence directly to the given peer
            TransportEnvelope envelope = this.GetMessageFor(channelId.Address, QualityOfService.Reliable);
            var notification = new ChatStatusNotification(presenceStatus.Status, notificationType);
            this.protocolComponent.RemoteCall(envelope, this.PresenceEvent, presenceStatus.SessionId.LocalId, presenceStatus.Character, notification);
            this.protocolComponent.SendMessage(envelope);
        }

        public void JoinChannel(BadumnaId channelId, ChatUser user)
        {
            // Send invitation directly to the given peer
            TransportEnvelope envelope = this.GetMessageFor(channelId.Address, QualityOfService.Reliable);
            this.protocolComponent.RemoteCall(envelope, this.InvitationEvent, user.SessionId.LocalId, user.Character, channelId);
            this.protocolComponent.SendMessage(envelope);
        }

        public void SendChatMessage(BadumnaId channelId, BadumnaId userId, String message)
        {
            TransportEnvelope envelope = this.GetMessageFor(channelId.Address, QualityOfService.Reliable);

            this.protocolComponent.RemoteCall(envelope, this.ReceiveChatMessage, userId.LocalId, message);
            this.protocolComponent.SendMessage(envelope);
        }

        #region Protocol methods

        [ConnectionfulProtocolMethod(ConnectionfulMethod.ReceiveChatMessage)]
        private void ReceiveChatMessage(ushort localId, String message)
        {
            // NOTE: this doesn't verify the owner (character).
            // The ConnectionTable will ensure validity of other peer's certificate
            // (and we check the character on initiation), so it relies on the fact
            // that you can't change characters without
            // using a new Session ID
            ChatChannelId channel = this.CurrentSourceChannelId(localId);
            this.mService.ReceiveMessage(channel, channel.TransportId, message);
        }

        [ConnectionfulProtocolMethod(ConnectionfulMethod.PresenceEvent)]
        protected void PresenceEvent(ushort localId, Character character, ChatStatusNotification notification)
        {
            if (!this.SenderOwns(character))
            {
                return;
            }

            ChatChannelId channel = this.CurrentSourceChannelId(localId);
            var user = new ChatUser(channel.TransportId, character);
            this.mService.ReceivePresence(channel, new UserPresenceStatus(user, notification.Status), notification.Type);
        }

        [ConnectionfulProtocolMethod(ConnectionfulMethod.InvitationEvent)]
        protected void InvitationEvent(ushort localId, Character character, BadumnaId subject)
        {
            if (!this.SenderOwns(character))
            {
                return;
            }

            ChatChannelId channel = this.CurrentSourceChannelId(localId);
            var requester = new ChatUser(channel.TransportId, character);
            this.mService.ReceiveInvitation(channel, requester, subject);
        }

        #endregion

        private ChatChannelId CurrentSourceChannelId(ushort localId)
        {
            BadumnaId userId = new BadumnaId(this.CurrentEnvelope.Source, localId);
            return new ChatChannelId(ChatChannelType.Private, userId);
        }

        /// <summary>
        /// Validates that the sender owns the given character.
        /// </summary>
        /// Returns true if the given character matches that of
        /// the current sender's UserCertificate token.
        /// <param name="subject">The subject of the message.</param>
        private bool SenderOwns(Character subject)
        {
            Character sender = this.CurrentSourceCharacter;
            if (sender != subject)
            {
                Logger.TraceError(LogTag.Chat, "Message subject ({0}) does not match sender ({0})", subject, sender);
                return false;
            }

            return true;
        }
    }
}
