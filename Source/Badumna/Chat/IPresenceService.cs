﻿//-----------------------------------------------------------------------
// <copyright file="IPresenceService.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using Badumna.DataTypes;

namespace Badumna.Chat
{
    /// <summary>
    /// The presence service interface
    /// </summary>
    internal interface IPresenceService
    {
        /// <summary>
        /// Occurs when precence subscription event is received.
        /// </summary>
        event EventHandler<PresenceSubscriptionEventArgs> PresenceSubscriptionEvent;

        /// <summary>
        /// Occurs when precence event is received.
        /// </summary>
        event EventHandler<PresenceEventArgs> PresenceEvent;

        /// <summary>
        /// Shutdowns this instance.
        /// </summary>
        void Shutdown();

        /// <summary>
        /// Sets the presence status.
        /// </summary>
        /// <param name="userStatus">The presence status.</param>
        void SetStatus(UserPresenceStatus userStatus);

        /// <summary>
        /// Requests the subscription to.
        /// </summary>
        /// <param name="requestor">The requesting user.</param>
        /// <param name="userName">Name of the user.</param>
        void RequestSubscriptionTo(ChatUser requestor, string userName);
    }
}
