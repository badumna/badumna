﻿//-----------------------------------------------------------------------
// <copyright file="ChatChannel.cs" company="NICTA">
//     Copyright (c) 2012 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System.Collections.Generic;
using Badumna.Core;
using Badumna.DataTypes;
using Badumna.Security;
using Badumna.Utilities;
using Badumna.Utilities.Logging;

namespace Badumna.Chat
{
    /// <summary>
    /// The type of chat channel
    /// </summary>
    public enum ChatChannelType
    {
        /// <summary>
        /// Unknown. Used internaly.
        /// </summary>
        //// TODO: is this actually used anywhere?
        Unknown = 0,

        /// <summary>
        /// A chat channel between two users
        /// </summary>
        Private = 1,

        /// <summary>
        /// Chat within the area of interest for a given entity
        /// </summary>
        Proximity = 2,

        /// <summary>
        /// A chat channel with any number of people
        /// </summary>
        Group = 3,

        /// <summary>
        /// Messages that are not sent to other peers. Used internally.
        /// </summary>
        Local = 4
    }

    /// <summary>
    /// A Chat Channel.
    /// </summary>
    public interface IChatChannel
    {
        /// <summary>
        /// Gets this channel's ID. Note that this ID is only unique within the
        /// scope of a ChatSession.
        /// </summary>
        ChatChannelId Id { get; }

        /// <summary>
        /// Unsubscribe (remove the user) from this channel.
        /// After calling this method, you should not attempt to use this object
        /// for any other operation.
        /// </summary>
        void Unsubscribe();

        /// <summary>
        /// Send a message on this channel.
        /// </summary>
        /// <param name="message">The message content</param>
        void SendMessage(string message);
    }

    /// <summary>
    /// Internal ChatChannel interface.
    /// </summary>
    internal interface IInternalChatChannel : IChatChannel
    {
        /// <summary>
        /// Gets this session ID this channel belongs to.
        /// </summary>
        BadumnaId SessionId { get; }

        /// <summary>
        /// Gets the BadumnaId associated with this channel. This should only
        /// be used for internal message routing, where the type of the channel is known
        /// in advance (since it does not contain the type).
        /// </summary>
        BadumnaId TransportId { get; }

        /// <summary>
        /// Gets the participants in this channel.
        /// Does not include the current user.
        /// </summary>
        IEnumerable<ChatUser> Participants { get; }

        /// <summary>
        /// Handles a chat message.
        /// </summary>
        /// <param name="sessionId">The user's session id.</param>
        /// <param name="message">The chat message.</param>
        void HandleMessage(BadumnaId sessionId, string message);

        /// <summary>
        /// Called when a user changes presence.
        /// </summary>
        /// <param name="presence">The user's new presence status</param>
        void PresenceChanged(UserPresenceStatus presence);
    }

    /// <summary>
    /// A Chat Channel
    /// </summary>
    public class ChatChannel : IInternalChatChannel
    {
        /// <summary>
        /// This channel's ID
        /// </summary>
        private ChatChannelId mId;

        /// <summary>
        /// The network event queue
        /// </summary>
        private NetworkEventQueue eventQueue;

        /// <summary>
        /// Participants in this channel
        /// </summary>
        private Dictionary<Character, UserPresenceStatus> participants;

        /// <summary>
        /// A lock object for this.participants
        /// </summary>
        private object participantsLock = new object();

        /// <summary>
        /// The chat session this channel belongs to.
        /// </summary>
        private IInternalChatSession session;

        /// <summary>
        /// The chat service for this channel.
        /// </summary>
        private IInternalChatService service;

        /// <summary>
        /// The chat protocol facade for this channel
        /// </summary>
        private IChatProtocolFacade chatProtocolFacade;

        /// <summary>
        /// The message handler.
        /// </summary>
        private ChatMessageHandler messageHandler;

        /// <summary>
        /// The presence handler.
        /// </summary>
        private ChatPresenceHandler presenceHandler;

        /// <summary>
        /// Initializes a new instance of the ChatChannel class.
        /// </summary>
        /// <param name="channelId">This channel's ID</param>
        /// <param name="messageHandler">The message handler.</param>
        /// <param name="presenceHandler">The presence handler.</param>
        /// <param name="eventQueue">The event queue.</param>
        /// <param name="chatProtocolFacade">The chat protocol facade.</param>
        /// <param name="session">The underlying chat session.</param>
        /// <param name="service">The chat service.</param>
        internal ChatChannel(
            ChatChannelId channelId,
            ChatMessageHandler messageHandler,
            ChatPresenceHandler presenceHandler,
            NetworkEventQueue eventQueue,
            IChatProtocolFacade chatProtocolFacade,
            IInternalChatSession session,
            IInternalChatService service)
        {
            this.mId = channelId;
            this.session = session;
            this.chatProtocolFacade = chatProtocolFacade;
            this.eventQueue = eventQueue;
            this.service = service;
            this.messageHandler = messageHandler;
            this.presenceHandler = presenceHandler;
            this.participants = new Dictionary<Character, UserPresenceStatus>();
        }

        /// <inheritdoc/>
        public ChatChannelId Id
        {
            get { return this.mId; }
        }

        /// <inheritdoc/>
        BadumnaId IInternalChatChannel.SessionId
        {
            get { return this.session.Id;  }
        }

        /// <inheritdoc/>
        IEnumerable<ChatUser> IInternalChatChannel.Participants
        {
            get
            {
                lock (this.participantsLock)
                {
                    return CollectionUtils.Map(this.participants.Values, p => p.User);
                }
            }
        }

        /// <inheritdoc/>
        BadumnaId IInternalChatChannel.TransportId
        {
            get { return this.Id.TransportId; }
        }

        /// <inheritdoc/>
        public void Unsubscribe()
        {
            this.messageHandler = null;
            this.presenceHandler = null;
            Logger.TraceInformation(LogTag.Chat | LogTag.Event, "Unsubscribe() called on channel {0}", this);
            this.session.RemoveChannel(this);
        }

        /// <inheritdoc/>
        void IInternalChatChannel.HandleMessage(BadumnaId sessionId, string message)
        {
            Logger.TraceInformation(LogTag.Chat | LogTag.Event, "channel " + this.Id + " handling message: " + message);
            if (null != this.messageHandler)
            {
                this.messageHandler(this, sessionId, message);
            }
        }

        /// <inheritdoc/>
        public void SendMessage(string message)
        {
            if (message.Length > Parameters.MaximimumChatMessageLength && this.Id.Type != ChatChannelType.Local)
            {
                throw new ChatMessageLengthException();
            }

            GenericCallBack action = () => this.eventQueue.Push(this.chatProtocolFacade.SendMessage, this, message);
            if (!this.service.DoWhenOnline(action))
            {
                Logger.TraceInformation(LogTag.Chat, "Queued chat message.");
            }
        }

        /// <inheritdoc/>
        void IInternalChatChannel.PresenceChanged(UserPresenceStatus status)
        {
            if (!this.UpdateUserPresence(status))
            {
                return;
            }

            if (this.presenceHandler != null)
            {
                this.presenceHandler(this, status.User.SessionId, status.Character.Name, status.Status);
            }
        }

        /// <summary>
        /// Remove a user from this channel.
        /// </summary>
        /// <param name="character">The character that has left the channel.</param>
        private void RemoveUser(Character character)
        {
            lock (this.participantsLock)
            {
                this.participants.Remove(character);
            }

            if (this.mId.Type == ChatChannelType.Private)
            {
                Logger.TraceInformation(LogTag.Chat | LogTag.Event, "User {0} left private channel: {1}", character, this.Id);
                this.session.RemoveChannel(this);
            }
        }

        /// <summary>
        /// Updates the user presence record in `this.participants`.
        /// </summary>
        /// <param name="status">The status.</param>
        /// <returns>True if the status has changed, False if the status is the same as the existing status.</returns>
        private bool UpdateUserPresence(UserPresenceStatus status)
        {
            if (status.Status == ChatStatus.Offline)
            {
                this.RemoveUser(status.Character);
                return true;
            }

            lock (this.participantsLock)
            {
                UserPresenceStatus oldStatus;
                if (this.participants.TryGetValue(status.Character, out oldStatus))
                {
                    if (oldStatus == status)
                    {
                        return false;
                    }
                }

                this.participants[status.Character] = status;
                return true;
            }
        }
    }
}
