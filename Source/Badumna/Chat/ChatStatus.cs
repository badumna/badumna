﻿//-----------------------------------------------------------------------
// <copyright file="ChatStatus.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2012 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Diagnostics;
using Badumna.Core;

namespace Badumna.Chat
{
    /// <summary>
    /// The status of a particular user on a particular channel. 
    /// </summary>
    public enum ChatStatus
    {
        /// <summary>
        /// The user is online and available to chat.
        /// </summary>
        Online = 0x01,

        /// <summary>
        /// The user is unavailable. Please note, application should not call ChangePresence to set the presence status 
        /// to offline. 
        /// </summary>
        Offline = 0x02,

        /// <summary>
        /// The user is online but not present.
        /// </summary>
        Away = 0x03,

        /// <summary>
        /// The user is online and actively wants to chat.
        /// </summary>
        Chat = 0x04,

        /// <summary>
        /// The user is online but does not want to be disturbed.
        /// </summary>
        DoNotDisturb = 0x05,

        /// <summary>
        /// The user is online but in extended away status.
        /// </summary>
        ExtendedAway = 0x06
    }

    /// <summary>
    /// Flags to convey the reason for a chat status notification. Used internally.
    /// </summary>
    /// <remarks>
    /// NOTE: these values must NOT overlap with ChatStatus, as they are bitwise OR'ed together in update messages
    /// </remarks>
    internal enum ChatNotificationType
    {
        /// <summary>
        /// Normal notification (e.g status changed or periodic notification).
        /// </summary>
        Default = 0x0,

        /// <summary>
        /// Used internally.
        /// </summary>
        DontReply = 0x10,

        /// <summary>
        /// A request for a presence subscription (introduction).
        /// </summary>
        Ask = 0x20
    }

    /// <summary>
    /// Essentially tuple of (ChatStatus, ChatNotificationType)
    /// </summary>
    internal class ChatStatusNotification : IParseable
    {
        /// <summary>
        /// All ChatStatus values are within the ON bits of StatusMask, and all ChatNotificationTypes are within the OFF bits.
        /// </summary>
        private const byte StatusMask = 0x0f;

        /// <summary>
        /// The chat notification type.
        /// </summary>
        private ChatNotificationType type;

        /// <summary>
        /// The chat status.
        /// </summary>
        private ChatStatus status;

        /// <summary>
        /// Initializes a new instance of the ChatStatusNotification class
        /// </summary>
        /// <param name="status">The status</param>
        /// <param name="type">The notification type</param>
        public ChatStatusNotification(ChatStatus status, ChatNotificationType type)
        {
            this.type = type;
            this.status = status;
        }

        /// <summary>
        /// Prevents a default instance of the ChatStatusNotification class from being created.
        /// (Well, not really - It's required by IParseable but we don't actually want people to call it)
        /// </summary>
        private ChatStatusNotification() : this((ChatStatus)0, ChatNotificationType.Default)
        {
        }

        /// <summary>
        /// Gets the status.
        /// </summary>
        public ChatStatus Status
        {
            get { return this.status; }
        }

        /// <summary>
        /// Gets the type.
        /// </summary>
        public ChatNotificationType Type
        {
            get { return this.type; }
        }

        /// <summary>
        /// Serializes the ChatStatusNotification to a message buffer.
        /// </summary>
        /// <param name="message">The message buffer to write to.</param>
        /// <param name="parameterType">Unused parameter required by interface.</param>
        void IParseable.ToMessage(MessageBuffer message, Type parameterType)
        {
            message.Write((byte)((byte)this.type | (byte)this.status));
        }

        /// <summary>
        /// Initializes the ChatStatusNotification with state read from a message buffer.
        /// </summary>
        /// <param name="message">The message buffer to read from.</param>
        void IParseable.FromMessage(MessageBuffer message)
        {
            if (null != message)
            {
                byte statusAndType = message.ReadByte();
                int status = statusAndType & StatusMask;
                int type = statusAndType & (~StatusMask);

                Debug.Assert(Enum.IsDefined(typeof(ChatStatus), status), "Invalid status");
                Debug.Assert(Enum.IsDefined(typeof(ChatNotificationType), type), "Invalid type");

                this.status = (ChatStatus)status;
                this.type = (ChatNotificationType)type;
            }
        }
    }
}
