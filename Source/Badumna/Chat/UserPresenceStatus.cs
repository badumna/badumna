﻿//-----------------------------------------------------------------------
// <copyright file="UserPresenceStatus.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2012 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using Badumna.Core;
using Badumna.DataTypes;
using Badumna.Security;
using Badumna.Utilities;
using Badumna.Utilities.Logging;

namespace Badumna.Chat
{
    /// <summary>
    /// An immutable user presence status, essentially a tuple of (ChatUser, ChatStatus)
    /// </summary>
    internal class UserPresenceStatus : IParseable, IOwnedByCharacter, IEquatable<UserPresenceStatus>
    {
        /// <summary>
        /// The chat user.
        /// </summary>
        private ChatUser user;

        /// <summary>
        /// The user's chat status.
        /// </summary>
        private ChatStatus status;

        /// <summary>
        /// Initializes a new instance of the <see cref="UserPresenceStatus"/> class.
        /// </summary>
        /// <param name="user">The chat user.</param>
        /// <param name="status">The user's status.</param>
        internal UserPresenceStatus(ChatUser user, ChatStatus status)
        {
            this.user = user;
            this.status = status;
        }

        /// <summary>
        /// Prevents a default instance of the UserPresenceStatus class from being created.
        /// Used only for IParseable deserialisation
        /// </summary>
        private UserPresenceStatus()
            : this(null, (ChatStatus)0)
        {
        }

        /// <summary>
        /// Gets the chat status.
        /// </summary>
        public ChatStatus Status
        {
            get { return this.status; }
        }

        /// <summary>
        /// Gets the ChatUser.
        /// </summary>
        public ChatUser User
        {
            get { return this.user; }
        }

        /// <summary>
        /// Gets the user's session id.
        /// </summary>
        public BadumnaId SessionId
        {
            get { return this.user.SessionId; }
        }

        /// <summary>
        /// Gets the user's Character.
        /// </summary>
        public Character Character
        {
            get { return this.user.Character; }
        }

        /// <summary>
        /// Implements the operator ==.
        /// </summary>
        /// <param name="a">The first object.</param>
        /// <param name="b">The second object.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static bool operator ==(UserPresenceStatus a, UserPresenceStatus b)
        {
            return object.ReferenceEquals(a, null) ? object.ReferenceEquals(b, null) : a.Equals(b);
        }

        /// <summary>
        /// Implements the operator !=.
        /// </summary>
        /// <param name="a">The first object.</param>
        /// <param name="b">The second object.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static bool operator !=(UserPresenceStatus a, UserPresenceStatus b)
        {
            return !(a == b);
        }

        /// <inheritdoc/>
        public override bool Equals(object obj)
        {
            return this.Equals(obj as UserPresenceStatus);
        }

        /// <inheritdoc/>
        public bool Equals(UserPresenceStatus other)
        {
            return (!object.ReferenceEquals(other, null))
                && other.Status == this.Status
                && other.User == this.User;
        }

        /// <inheritdoc/>
        public override int GetHashCode()
        {
            return this.Status.GetHashCode()
                | this.User.GetHashCode();
        }

        /// <inheritdoc/>
        public override string ToString()
        {
            return String.Format("<#UserPresenceStatus: {0} : {1}>", this.User, this.Status);
        }

        /// <inheritdoc/>
        void IParseable.ToMessage(MessageBuffer message, Type ignored)
        {
            message.Write(this.user);
            message.Write((byte)this.status);
        }

        /// <inheritdoc/>
        void IParseable.FromMessage(MessageBuffer message)
        {
            this.user = message.Read<ChatUser>();
            this.status = (ChatStatus)message.ReadByte();

            if (!Enum.IsDefined(typeof(ChatStatus), this.status))
            {
                throw new InvalidCastException(String.Format("Invalid ChatStatus in message: {0}", (int)this.status));
            }
        }
    }
}
