﻿//-----------------------------------------------------------------------
// <copyright file="PresenceSubscriptionDetails.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using Badumna.Core;
using Badumna.DataTypes;

namespace Badumna.Chat
{
    /// <summary>
    /// The presence subscription details. 
    /// </summary>
    internal class PresenceSubscriptionDetails
    {
        /// <summary>
        /// The uesr id.
        /// </summary>
        private SignedChatUser requester;

        /// <summary>
        /// The user name.
        /// </summary>
        private string requestedUsername;

        /// <summary>
        /// When last refreshed.
        /// </summary>
        private TimeSpan lastRefreshTime;

        /// <summary>
        /// Initializes a new instance of the <see cref="PresenceSubscriptionDetails"/> class.
        /// </summary>
        /// <param name="requester">The user requesting the information.</param>
        /// <param name="requestedUsername">The user name.</param>
        /// <param name="initialRefreshTime">The initial refresh time.</param>
        public PresenceSubscriptionDetails(SignedChatUser requester, string requestedUsername, TimeSpan initialRefreshTime)
        {
            this.requester = requester;
            this.requestedUsername = requestedUsername;
            this.lastRefreshTime = initialRefreshTime;
        }

        /// <summary>
        /// Gets the user id.
        /// </summary>
        /// <value>The user id.</value>
        public BadumnaId Id
        {
            get { return this.requester.SessionId; }
        }

        /// <summary>
        /// Gets the requester.
        /// </summary>
        /// <value>The requester.</value>
        public SignedChatUser Requester
        {
            get { return this.requester; }
        }

        /// <summary>
        /// Gets the requested user name.
        /// </summary>
        /// <value>The user name.</value>
        public string Name
        {
            get { return this.requestedUsername; }
        }

        /// <summary>
        /// Gets or sets the last refresh time.
        /// </summary>
        /// <value>The last refresh time.</value>
        public TimeSpan LastRefreshTime
        {
            get { return this.lastRefreshTime; }
            set { this.lastRefreshTime = value; }
        }
    }
}
