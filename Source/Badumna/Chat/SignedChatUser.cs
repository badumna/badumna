﻿//-----------------------------------------------------------------------
// <copyright file="SignedChatUser.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2012 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using Badumna.Core;
using Badumna.DataTypes;
using Badumna.Security;
using Badumna.Utilities;

namespace Badumna.Chat
{
    /// <summary>
    /// A signed container for a ChatUser
    /// </summary>
    internal class SignedChatUser : SignedUserData<ChatUser>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SignedChatUser"/> class.
        /// </summary>
        /// <param name="user">The chat user.</param>
        /// <param name="userCertificate">The user's certificate token.</param>
        public SignedChatUser(ChatUser user, ICertificateToken userCertificate)
            : base(user, userCertificate)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SignedChatUser"/> class.
        /// Should only be used by IParseable
        /// </summary>
        protected SignedChatUser()
            : base()
        {
        }

        /// <summary>
        /// Gets the user.
        /// </summary>
        internal ChatUser User
        {
            get { return this.Value; }
        }

        /// <summary>
        /// Gets the session id.
        /// </summary>
        internal BadumnaId SessionId
        {
            get { return this.User.SessionId; }
        }

        /// <summary>
        /// Gets the user presence status.
        /// </summary>
        internal Character Character
        {
            get { return this.User.Character; }
        }

        /// <inheritdoc/>
        protected override Type ValueType
        {
            get { return typeof(ChatUser); }
        }
    }
}
