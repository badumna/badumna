﻿//-----------------------------------------------------------------------
// <copyright file="AllocatorTests.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

////namespace BadumnaTests.Validation
////{
////    using System;
////    using Badumna.Core;
////    using Badumna.DistributedHashTable;
////    using Badumna.Utilities;
////    using Badumna.Validation;
////    using NUnit.Framework;
////    using Rhino.Mocks;
////    using Constraints = Rhino.Mocks.Constraints;

////    [TestFixture]
////    [DontPerformCoverage]
////    public class AllocatorTests
////    {
////        private Allocator allocator;
////        private IDhtProtocol dhtProtocol;
////        private IMessageParser messageParser;
////        private DispatchMessageDelegate<DhtEnvelope> dispatcherMethod;
////        private INetworkEventScheduler eventScheduler;
////        private AllocationDecisionMethod decisionMethod;
////        private HashKey requestorKey;
////        private AllocationReplyHandler replyHandler;

////        [SetUp]
////        public void SetUp()
////        {
////            this.dhtProtocol = MockRepository.GenerateMock<IDhtProtocol>();
////            this.messageParser = MockRepository.GenerateMock<IMessageParser>();
////            this.dispatcherMethod = MockRepository.GenerateMock<DispatchMessageDelegate<DhtEnvelope>>();
////            this.dhtProtocol.Stub(p => p.Parser).Return(this.messageParser);
////            this.dhtProtocol.Stub(p => p.DispatcherMethod).Return(this.dispatcherMethod);
////            this.eventScheduler = MockRepository.GenerateMock<INetworkEventScheduler>();
////            this.decisionMethod = MockRepository.GenerateMock<AllocationDecisionMethod>();
////            this.allocator = new Allocator(this.dhtProtocol, this.eventScheduler);
////            this.requestorKey = HashKey.Random();
////            this.replyHandler = MockRepository.GenerateMock<AllocationReplyHandler>();
////        }

////        [Test]
////        [ExpectedException(typeof(ArgumentNullException))]
////        public void ConstructorThrowsForNullDhtProtocol()
////        {
////            this.allocator = new Allocator(null, this.eventScheduler);
////        }

////        [Test]
////        [ExpectedException(typeof(ArgumentNullException))]
////        public void ConstructorThrowsForNulleventScheduler()
////        {
////            this.allocator = new Allocator(this.dhtProtocol, null);
////        }

////        [Test]
////        public void ConstructorRegistersMethodsWithProtocolParser()
////        {
////            // Arrange
////            Allocator allocator = null;

////            // Expect
////            this.messageParser.Expect(p => p.RegisterMethodsIn(null))
////                .IgnoreArguments();

////            // Act
////            allocator = new Allocator(this.dhtProtocol, this.eventScheduler);

////            // Verify
////            this.messageParser.VerifyAllExpectations();
////        }

////        [Test]
////        public void AllocationDecisionMethodGetterReturnsDecisionMethod()
////        {
////            AllocationDecisionMethod method = MockRepository.GenerateMock<AllocationDecisionMethod>();
////            Assert.AreNotEqual(method, this.allocator.AllocationDecisionMethod);
////            this.allocator.AllocationDecisionMethod = method;
////            Assert.AreEqual(method, this.allocator.AllocationDecisionMethod);
////        }

////        [Test]
////        public void RequestValidatorGetsEnvelopeFromParentProtocol()
////        {
////            // Expect
////            DhtEnvelope envelope = null;
////            this.dhtProtocol.Expect(p => p.GetMessageFor(HashKey.Random(), QualityOfService.Reliable))
////                .IgnoreArguments()
////                .Return(envelope);

////            // Act
////            this.allocator.RequestValidator(this.requestorKey, null);

////            // Verify
////            this.dhtProtocol.VerifyAllExpectations();
////        }

////        [Test]
////        public void RequestValidatorPreparesValidatorAllocationRequestRemoteCall()
////        {
////            // Arrange
////            DhtEnvelope envelope = null;
////            this.dhtProtocol.Stub(p => p.GetMessageFor(this.requestorKey, QualityOfService.Reliable))
////                .Return(envelope);

////            // Expect
////            this.dhtProtocol.Expect(p => p.RemoteCall(envelope, null, this.requestorKey))
////                .IgnoreArguments()
////                .Constraints(
////                    Constraints.Is.Equal(envelope),
////                    Constraints.Is.Anything(),
////                    Constraints.Is.Equal(this.requestorKey));

////            // Act
////            this.allocator.RequestValidator(this.requestorKey, null);

////            // Verify
////            this.messageParser.VerifyAllExpectations();
////        }

////        [Test]
////        public void RequestValidatorMakesDHTQuery()
////        {
////            // Arrange
////            DhtEnvelope envelope = null;
////            this.dhtProtocol.Stub(p => p.GetMessageFor(this.requestorKey, QualityOfService.Reliable))
////                .Return(envelope);

////            // Expect
////            this.dhtProtocol.Expect(p => p.SendMessage(envelope));

////            // Act
////            this.allocator.RequestValidator(this.requestorKey, null);

////            // Verify
////            this.dhtProtocol.VerifyAllExpectations();
////        }

////        [Test]
////        public void RequestValidatorReturnsTrueWhenRequestorHasNoOutstandingRequests()
////        {
////            Assert.IsTrue(this.allocator.RequestValidator(this.requestorKey, null));
////        }

////        [Test]
////        public void RequestValidatorReturnsFalseWhenRequestorHasOutstandingRequests()
////        {
////            // Arrange
////            this.allocator.RequestValidator(this.requestorKey, null);

////            // Act
////            bool result = this.allocator.RequestValidator(this.requestorKey, null);

////            // Assert
////            Assert.IsFalse(result);
////        }

////        [Test]
////        public void RequestValidatorSchedulesTimeoutEvents()
////        {
////            // Expect two timeouts (query and allocation).
////            this.eventScheduler.Expect(q => q.Schedule(0.0, null))
////                .IgnoreArguments()
////                .Return(null)
////                .Repeat.Twice();

////            // Act
////            this.allocator.RequestValidator(this.requestorKey, null);

////            // Verify
////            this.eventScheduler.VerifyAllExpectations();
////        }

////        [Test]
////        public void OnRequestTimeoutMakesNewQuery()
////        {
////            // Arrange
////            // Capture first timeout (query timeout).
////            GenericCallBack queryTimeoutCallback = null;
////            this.eventScheduler.Stub(q => q.Schedule(0f, null))
////                .IgnoreArguments()
////                .WhenCalled(c => { queryTimeoutCallback = (GenericCallBack)c.Arguments[1]; })
////                .Return(null)
////                .Repeat.Once();
////            this.allocator.RequestValidator(this.requestorKey, this.replyHandler);

////            // Expect
////            this.dhtProtocol.Expect(p => p.SendMessage(null)).IgnoreArguments();

////            // Act
////            queryTimeoutCallback.Invoke();

////            // Verify
////            this.dhtProtocol.VerifyAllExpectations();
////        }

////        [Test]
////        public void RequestValidatorReplyHandlerInvokedOnAllocationTimeout()
////        {
////            // Arrange
////            // Capture last (second) timeout (allocation timeout).
////            GenericCallBack timeoutCallback = null;
////            this.eventScheduler.Stub(q => q.Schedule(0f, null))
////                .IgnoreArguments()
////                .WhenCalled(c => { timeoutCallback = (GenericCallBack)c.Arguments[1]; })
////                .Return(null);
////            this.allocator.RequestValidator(this.requestorKey, this.replyHandler);

////            // Expect
////            this.replyHandler.Expect(h => h.Invoke(AllocationRequestResult.Timeout, null));

////            // Act
////            timeoutCallback.Invoke();

////            // Verify
////            this.replyHandler.VerifyAllExpectations();
////        }

////        [Test]
////        public void ValidatorAllocationRequestInvokesDecisionMethod()
////        {
////            // Arrange
////            DhtEnvelope envelope = null;
////            this.dhtProtocol.Stub(p => p.GetMessageFor(this.requestorKey, QualityOfService.Reliable))
////                .Return(envelope);
////            GenericCallBack<HashKey> validatorAllocationRequestMethod = null;
////            this.dhtProtocol.Stub(p => p.RemoteCall(envelope, null, this.requestorKey))
////                .IgnoreArguments()
////                .WhenCalled(c => { validatorAllocationRequestMethod = (GenericCallBack<HashKey>)c.Arguments[1]; });
////            this.allocator.RequestValidator(this.requestorKey, this.replyHandler);

////            DhtEnvelope receivedEnvelope = new DhtEnvelope();
////            receivedEnvelope.Source = new PeerAddress(HashKey.Random());
////            this.dhtProtocol.Stub(p => p.CurrentEnvelope).Return(receivedEnvelope);

////            this.allocator.AllocationDecisionMethod = this.decisionMethod;

////            // Expect
////            this.decisionMethod.Expect(m => m.Invoke()).Return(true);

////            // Act
////            validatorAllocationRequestMethod.Invoke(this.requestorKey);

////            // Verify
////            this.decisionMethod.VerifyAllExpectations();
////        }

////        [Test]
////        public void ValidatorAllocationRequestGetsEnvelopeFromProtocol()
////        {
////            // Arrange
////            DhtEnvelope envelope = null;
////            this.dhtProtocol.Stub(p => p.GetMessageFor(this.requestorKey, QualityOfService.Reliable))
////                .Return(envelope);
////            GenericCallBack<HashKey> validatorAllocationRequestMethod = null;
////            this.dhtProtocol.Stub(p => p.RemoteCall(envelope, null, this.requestorKey))
////                .IgnoreArguments()
////                .WhenCalled(c => { validatorAllocationRequestMethod = (GenericCallBack<HashKey>)c.Arguments[1]; });
////            this.allocator.RequestValidator(this.requestorKey, this.replyHandler);

////            DhtEnvelope receivedEnvelope = new DhtEnvelope();
////            receivedEnvelope.Source = new PeerAddress(HashKey.Random());
////            this.dhtProtocol.Stub(p => p.CurrentEnvelope).Return(receivedEnvelope);

////            // Expect
////            this.dhtProtocol.Expect(p => p.GetMessageFor(receivedEnvelope.Source, QualityOfService.Reliable))
////                .Return(envelope);

////            // Act
////            validatorAllocationRequestMethod.Invoke(this.requestorKey);

////            // Verify
////            this.dhtProtocol.VerifyAllExpectations();
////        }

////        [Test]
////        public void ValidatorAllocationRequestPreparesRemoteCall()
////        {
////            // Arrange
////            DhtEnvelope envelope = null;
////            this.dhtProtocol.Stub(p => p.GetMessageFor(this.requestorKey, QualityOfService.Reliable))
////                .Return(envelope);
////            GenericCallBack<HashKey> validatorAllocationRequestMethod = null;
////            this.dhtProtocol.Expect(p => p.RemoteCall(envelope, null, this.requestorKey))
////                .IgnoreArguments()
////                .WhenCalled(c => { validatorAllocationRequestMethod = (GenericCallBack<HashKey>)c.Arguments[1]; })
////                .Repeat.Once();
////            this.allocator.RequestValidator(this.requestorKey, this.replyHandler);
////            DhtEnvelope receivedEnvelope = new DhtEnvelope();
////            receivedEnvelope.Source = new PeerAddress(HashKey.Random());
////            this.dhtProtocol.Stub(p => p.CurrentEnvelope).Return(receivedEnvelope);
////            bool decision = true;
////            this.decisionMethod.Stub(m => m.Invoke()).Return(decision);
////            this.allocator.AllocationDecisionMethod = this.decisionMethod;

////            // Expect
////            this.dhtProtocol.Expect(p => p.RemoteCall(envelope, null, this.requestorKey, decision))
////                .IgnoreArguments()
////                .Constraints(
////                    Constraints.Is.Anything(),
////                    Constraints.Is.Anything(),
////                    Constraints.Is.Equal(this.requestorKey),
////                    Constraints.Is.Equal(decision));

////            // Act
////            validatorAllocationRequestMethod.Invoke(this.requestorKey);

////            // Verify
////            this.dhtProtocol.VerifyAllExpectations();
////        }

////        [Test]
////        public void ValidatorAllocationRequestSendsReply()
////        {
////            // Arrange
////            DhtEnvelope envelope = null;
////            this.dhtProtocol.Stub(p => p.GetMessageFor(this.requestorKey, QualityOfService.Reliable))
////                .Return(envelope);
////            GenericCallBack<HashKey> validatorAllocationRequestMethod = null;
////            this.dhtProtocol.Stub(p => p.RemoteCall(envelope, null, this.requestorKey))
////                .IgnoreArguments()
////                .WhenCalled(c => { validatorAllocationRequestMethod = (GenericCallBack<HashKey>)c.Arguments[1]; });
////            this.allocator.RequestValidator(this.requestorKey, this.replyHandler);

////            DhtEnvelope receivedEnvelope = new DhtEnvelope();
////            receivedEnvelope.Source = new PeerAddress(HashKey.Random());
////            this.dhtProtocol.Stub(p => p.CurrentEnvelope).Return(receivedEnvelope);

////            // Expect
////            this.dhtProtocol.Expect(p => p.SendMessage(null)).IgnoreArguments();

////            // Act
////            validatorAllocationRequestMethod.Invoke(this.requestorKey);

////            // Verify
////            this.dhtProtocol.VerifyAllExpectations();
////        }

////        [Test]
////        public void ValidatorAllocationReplyWithAcceptanceCancelsQueryAndAllocationTimeouts()
////        {
////            // Arrange
////            DhtEnvelope envelope = null;
////            this.dhtProtocol.Stub(p => p.GetMessageFor(this.requestorKey, QualityOfService.Reliable))
////                .Return(envelope);
////            GenericCallBack<HashKey> validatorAllocationRequestMethod = null;
////            this.dhtProtocol.Stub(p => p.RemoteCall(envelope, null, this.requestorKey))
////                .IgnoreArguments()
////                .WhenCalled(c => { validatorAllocationRequestMethod = (GenericCallBack<HashKey>)c.Arguments[1]; })
////                .Repeat.Once();

////            this.allocator.RequestValidator(this.requestorKey, this.replyHandler);

////            DhtEnvelope receivedEnvelope = new DhtEnvelope();
////            receivedEnvelope.Source = new PeerAddress(HashKey.Random());
////            this.dhtProtocol.Stub(p => p.CurrentEnvelope).Return(receivedEnvelope);

////            GenericCallBack<HashKey, bool> validatorAllocationReplyMethod = null;
////            this.dhtProtocol.Stub(p => p.RemoteCall(envelope, null, this.requestorKey, true))
////                .IgnoreArguments()
////                .WhenCalled(c => { validatorAllocationReplyMethod = (GenericCallBack<HashKey, bool>)c.Arguments[1]; })
////                .Repeat.Once();

////            validatorAllocationRequestMethod.Invoke(this.requestorKey);

////            // Expect
////            this.eventScheduler.Expect(q => q.Remove(null))
////                .IgnoreArguments()
////                .Repeat.Twice();

////            // Act
////            validatorAllocationReplyMethod.Invoke(this.requestorKey, true);

////            // Verify
////            this.eventScheduler.VerifyAllExpectations();
////        }

////        [Test]
////        public void ValidatorAllocationReplyWithoutAcceptanceCancelsQueryTimeoutOnly()
////        {
////            // Arrange
////            DhtEnvelope envelope = null;
////            this.dhtProtocol.Stub(p => p.GetMessageFor(this.requestorKey, QualityOfService.Reliable))
////                .Return(envelope);
////            GenericCallBack<HashKey> validatorAllocationRequestMethod = null;
////            this.dhtProtocol.Stub(p => p.RemoteCall(envelope, null, this.requestorKey))
////                .IgnoreArguments()
////                .WhenCalled(c => { validatorAllocationRequestMethod = (GenericCallBack<HashKey>)c.Arguments[1]; })
////                .Repeat.Once();

////            this.allocator.RequestValidator(this.requestorKey, this.replyHandler);

////            DhtEnvelope receivedEnvelope = new DhtEnvelope();
////            receivedEnvelope.Source = new PeerAddress(HashKey.Random());
////            this.dhtProtocol.Stub(p => p.CurrentEnvelope).Return(receivedEnvelope);

////            GenericCallBack<HashKey, bool> validatorAllocationReplyMethod = null;
////            this.dhtProtocol.Stub(p => p.RemoteCall(envelope, null, this.requestorKey, true))
////                .IgnoreArguments()
////                .WhenCalled(c => { validatorAllocationReplyMethod = (GenericCallBack<HashKey, bool>)c.Arguments[1]; })
////                .Repeat.Once();

////            validatorAllocationRequestMethod.Invoke(this.requestorKey);

////            // Expect
////            this.eventScheduler.Expect(q => q.Remove(null))
////                .IgnoreArguments()
////                .Repeat.Once();
////            this.eventScheduler.Stub(q => q.Remove(null))
////                .IgnoreArguments()
////                .Throw(new InvalidOperationException("Only one timeout should be cancelled."));

////            // Act
////            validatorAllocationReplyMethod.Invoke(this.requestorKey, false);

////            // Verify
////            this.eventScheduler.VerifyAllExpectations();
////        }

////        [Test]
////        public void ValidatorAllocationReplyAcceptingAllocationInvokesReplyHandler()
////        {
////            // Arrange
////            DhtEnvelope envelope = null;
////            this.dhtProtocol.Stub(p => p.GetMessageFor(this.requestorKey, QualityOfService.Reliable))
////                .Return(envelope);
////            GenericCallBack<HashKey> validatorAllocationRequestMethod = null;
////            this.dhtProtocol.Stub(p => p.RemoteCall(envelope, null, this.requestorKey))
////                .IgnoreArguments()
////                .WhenCalled(c => { validatorAllocationRequestMethod = (GenericCallBack<HashKey>)c.Arguments[1]; })
////                .Repeat.Once();

////            this.allocator.RequestValidator(this.requestorKey, this.replyHandler);

////            DhtEnvelope receivedEnvelope = new DhtEnvelope();
////            receivedEnvelope.Source = new PeerAddress(HashKey.Random());
////            this.dhtProtocol.Stub(p => p.CurrentEnvelope).Return(receivedEnvelope);

////            GenericCallBack<HashKey, bool> validatorAllocationReplyMethod = null;
////            this.dhtProtocol.Stub(p => p.RemoteCall(envelope, null, this.requestorKey, true))
////                .IgnoreArguments()
////                .WhenCalled(c => { validatorAllocationReplyMethod = (GenericCallBack<HashKey, bool>)c.Arguments[1]; })
////                .Repeat.Once();

////            validatorAllocationRequestMethod.Invoke(this.requestorKey);

////            // Expect
////            this.replyHandler.Expect(h => h.Invoke(AllocationRequestResult.Success, receivedEnvelope.Source.ToString()));

////            // Act
////            validatorAllocationReplyMethod.Invoke(this.requestorKey, true);

////            // Verify
////            this.replyHandler.VerifyAllExpectations();
////        }

////        [TestCase(true)]
////        [TestCase(false)]
////        public void ValidatorAllocationReplyIgnoresRepliesForUnknownRequestors(bool acceptance)
////        {
////            // Arrange
////            DhtEnvelope envelope = null;
////            this.dhtProtocol.Stub(p => p.GetMessageFor(this.requestorKey, QualityOfService.Reliable))
////                .Return(envelope);
////            GenericCallBack<HashKey> validatorAllocationRequestMethod = null;
////            this.dhtProtocol.Stub(p => p.RemoteCall(envelope, null, this.requestorKey))
////                .IgnoreArguments()
////                .WhenCalled(c => { validatorAllocationRequestMethod = (GenericCallBack<HashKey>)c.Arguments[1]; })
////                .Repeat.Once();

////            this.allocator.RequestValidator(this.requestorKey, this.replyHandler);

////            DhtEnvelope receivedEnvelope = new DhtEnvelope();
////            receivedEnvelope.Source = new PeerAddress(HashKey.Random());
////            this.dhtProtocol.Stub(p => p.CurrentEnvelope).Return(receivedEnvelope);

////            GenericCallBack<HashKey, bool> validatorAllocationReplyMethod = null;
////            this.dhtProtocol.Stub(p => p.RemoteCall(envelope, null, this.requestorKey, acceptance))
////                .IgnoreArguments()
////                .WhenCalled(c => { validatorAllocationReplyMethod = (GenericCallBack<HashKey, bool>)c.Arguments[1]; })
////                .Repeat.Once();

////            validatorAllocationRequestMethod.Invoke(this.requestorKey);

////            // Expect
////            this.replyHandler.Expect(h => h.Invoke(AllocationRequestResult.Success, receivedEnvelope.Source.ToString()))
////                .IgnoreArguments()
////                .Repeat.Never();
////            this.eventScheduler.Expect(q => q.Remove(null))
////                .IgnoreArguments()
////                .Repeat.Never();
////            this.eventScheduler.Expect(q => q.Schedule(0.0, () => { }))
////                .IgnoreArguments()
////                .Repeat.Never();
////            this.dhtProtocol.Expect(p => p.SendMessage(null))
////                .IgnoreArguments()
////                .Repeat.Never();

////            // Act
////            validatorAllocationReplyMethod.Invoke(this.requestorKey.Opposite(), acceptance);

////            // Verify
////            ////this.replyHandler.VerifyAllExpectations();
////            ////this.eventScheduler.VerifyAllExpectations();
////            ////this.dhtProtocol.VerifyAllExpectations();
////        }
    
////        // TODO: Test to make sure duplicate messages are ignored?
////        // E.g. query reply is handled, the received again - we don't want to 
////        // cancel subsequent queries timeouts and send new ones.
////        // This would require query destinations to be matched.
////        // This is no tnecessary if it is guarenteed that duplicat messages are
////        // trapped by the protocol before reaching the allocator.
////    }
////}
