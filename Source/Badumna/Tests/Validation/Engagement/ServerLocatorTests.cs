﻿//-----------------------------------------------------------------------
// <copyright file="ServerLocatorTests.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BadumnaTests.Validation.EngagementTests
{
    using System;
    using System.Collections.Generic;
    using Badumna;
    using Badumna.Core;
    using Badumna.Validation.Engagement;
    using NUnit.Framework;
    using Rhino.Mocks;

    [TestFixture]
    [DontPerformCoverage]
    public class ServerLocatorTests
    {
        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ConstructorThrowsForNullValidationModule()
        {
            var serverLocator = new ServerLocator(null);
        }

        [Test]
        public void FindServerReturnsNullWhenNoServersSpecified()
        {
            var validationModule = new ValidationModule(null);
            var serverLocator = new ServerLocator(validationModule);
            Assert.IsNull(serverLocator.FindServer());
        }

        [Test]
        public void FindServerReturnsFirstServerFound()
        {
            string address = "123.123.123.123:123";
            var s1 = new ValidationServerDetails("Foo", address);
            var s2 = new ValidationServerDetails("Bar", "1.1.1.1:1");
            s1.Validate();
            s2.Validate();
            var servers = new List<ValidationServerDetails>() { s1, s2 };
            var validationModule = MockRepository.GenerateMock<IValidationModule>();
            validationModule.Stub(m => m.Servers).Return(servers);
            var serverLocator = new ServerLocator(validationModule);
            Assert.AreEqual(PeerAddress.GetAddress(address), serverLocator.FindServer());
        }
    }
}
