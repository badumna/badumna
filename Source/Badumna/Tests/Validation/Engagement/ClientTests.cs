﻿//-----------------------------------------------------------------------
// <copyright file="ClientTests.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BadumnaTests.Validation.EngagementTests
{
    using System;
    using Badumna.Core;
    using Badumna.Transport;
    using Badumna.Utilities;
    using Badumna.Validation.Allocation;
    using Badumna.Validation.Engagement;
    using NUnit.Framework;
    using Rhino.Mocks;
    using TestUtilities;

    [TestFixture]
    [DontPerformCoverage]
    public class ClientTests
    {
        private ITransportProtocol transportProtocol;
        private INetworkEventScheduler eventScheduler;
        private IServerProtocol serverProtocol;
        private IServerLocator serverLocator;
        private INetworkAddressProvider networkAddressProvider;
        private Client client;

        private EngagementReplyHandler replyHandler;

        [SetUp]
        public void SetUp()
        {
            this.transportProtocol = MockRepository.GenerateMock<ITransportProtocol>();
            this.eventScheduler = MockRepository.GenerateMock<INetworkEventScheduler>();
            this.serverProtocol = MockRepository.GenerateMock<IServerProtocol>();
            this.serverLocator = MockRepository.GenerateMock<IServerLocator>();
            this.networkAddressProvider = MockRepository.GenerateMock<INetworkAddressProvider>();

            this.replyHandler = MockRepository.GenerateMock<EngagementReplyHandler>();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ConstructorThrowsForNullTransportProtocol()
        {
            this.transportProtocol = null;
            this.ConstructClient();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ConstructorThrowsForNullEventScheduler()
        {
            this.eventScheduler = null;
            this.ConstructClient();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ConstructorThrowsForNullServerProtocol()
        {
            this.serverProtocol = null;
            this.ConstructClient();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ConstructorThrowsForNullAddressProvider()
        {
            this.networkAddressProvider = null;
            this.ConstructClient();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ConstructorThrowsForNullServerLocator()
        {
            this.serverLocator = null;
            this.ConstructClient();
        }

        [Test]
        public void RequestValidatorGetsServerAddressFromLocator()
        {
            // Arrange
            PeerAddress serverAddress = PeerAddress.GetRandomAddress();
            this.ConstructClient();

            // Expect
            this.serverLocator.Expect(l => l.FindServer()).Return(serverAddress);

            // Act
            this.client.RequestValidator(this.replyHandler);

            // Verify
            this.serverLocator.VerifyAllExpectations();
        }

        [Test]
        public void RequestValidatorThrowsWhenServerIsLocalMachine()
        {
            // Arrange
            PeerAddress serverAddress = PeerAddress.GetRandomAddress();
            this.ConstructClient();
            this.serverLocator.Stub(l => l.FindServer()).Return(serverAddress);
            this.networkAddressProvider.Stub(p => p.HasPrivateAddress(serverAddress))
                .Return(true);

            // Act + Assert
            Assert.Throws(
                typeof(InvalidOperationException),
                () => this.client.RequestValidator(this.replyHandler));
        }

        [Test]
        public void RequestValidatorThrowsIfNoServerAddress()
        {
            // Arrange
            this.ConstructClient();
            this.serverLocator.Stub(l => l.FindServer()).Return(null);

            // Act + Assert
            TestDelegate request = delegate { this.client.RequestValidator(this.replyHandler); };
            Assert.Throws(typeof(InvalidOperationException), request);
        }

        [Test]
        public void RequestValidatorSendsMessageViaTransportProtocol()
        {
            // Arrange
            this.ConstructClient();
            PeerAddress serverAddress = PeerAddress.GetRandomAddress();
            this.serverLocator.Stub(l => l.FindServer()).Return(serverAddress);
            TransportEnvelope envelope = new TransportEnvelope();

            // Expect
            this.transportProtocol.Expect(p => p.GetMessageFor(serverAddress, QualityOfService.Reliable))
                .Return(envelope);
            this.transportProtocol.Expect(p => p.RemoteCall(envelope, this.serverProtocol.ValidatorAssignmentRequest));
            this.transportProtocol.Expect(p => p.SendMessage(envelope));

            // Act
            this.client.RequestValidator(this.replyHandler);

            // Verify
            this.transportProtocol.VerifyAllExpectations();
        }

        [Test]
        public void RequestValidatorThrowsIfRequestPending()
        {
            PeerAddress serverAddress = PeerAddress.GetRandomAddress();
            this.serverLocator.Stub(l => l.FindServer()).Return(serverAddress);
            this.ConstructClient();
            TestDelegate request = delegate { this.client.RequestValidator(this.replyHandler); };
            request.Invoke();
            Assert.Throws(typeof(InvalidOperationException), request);
        }

        [Test]
        public void RequestValidatorSendsMessageIfPreviousRequestCompleted()
        {
            // Arrange
            PeerAddress serverAddress = PeerAddress.GetRandomAddress();
            this.serverLocator.Stub(l => l.FindServer()).Return(serverAddress);
            this.ConstructClient();
            this.client.RequestValidator(this.replyHandler);
            this.client.HandleAllocationReply(AllocationRequestResult.Timeout, string.Empty);

            // Expect
            this.transportProtocol.Expect(p => p.SendMessage(Arg<TransportEnvelope>.Is.Anything));
            
            // Act
            this.client.RequestValidator(this.replyHandler);

            // Verify
            this.transportProtocol.VerifyAllExpectations();
        }

        [Test]
        public void RequestValidatorSchedulesTimeout()
        {
            // Arrange
            this.ConstructClient();
            PeerAddress serverAddress = PeerAddress.GetRandomAddress();
            this.serverLocator.Stub(l => l.FindServer()).Return(serverAddress);

            // Expect
            this.eventScheduler.Expect(q => q.Schedule(
                Arg<float>.Is.Anything,
                Arg<GenericCallBack>.Is.Anything))
                .Return(null);

            // Act
            this.client.RequestValidator(this.replyHandler);

            // Verify
            this.eventScheduler.VerifyAllExpectations();
        }

        [Test]
        public void RequestValidatorReplyHandlerInvokedOnRequestTimeout()
        {
            // Arrange
            this.ConstructClient();
            PeerAddress serverAddress = PeerAddress.GetRandomAddress();
            this.serverLocator.Stub(l => l.FindServer()).Return(serverAddress);
            
            CapturingConstraint<GenericCallBack> cc = new CapturingConstraint<GenericCallBack>();
            this.eventScheduler.Stub(q => q.Schedule(
                Arg<float>.Is.Anything,
                Arg<GenericCallBack>.Matches(cc)))
                .Return(null);

            this.client.RequestValidator(this.replyHandler);

            // Expect
            this.replyHandler.Invoke(EngagementRequestResult.Timeout, null);

            // Act
            cc.Argument.Invoke();

            // Verify
            this.replyHandler.VerifyAllExpectations();
        }

        [Test]
        public void HandleAllocationReplyInvokesReplyHandler()
        {
            // Arrange
            PeerAddress serverAddress = PeerAddress.GetRandomAddress();
            this.serverLocator.Stub(l => l.FindServer()).Return(serverAddress);
            this.ConstructClient();
            this.client.RequestValidator(this.replyHandler);

            // Expect
            this.replyHandler.Expect(h => h.Invoke(EngagementRequestResult.Success, "foo"));

            // Act
            this.client.HandleAllocationReply(AllocationRequestResult.Success, "foo");

            // Verify
            this.replyHandler.VerifyAllExpectations();
        }

        [Test]
        public void HandleAllocationCancelsTimeout()
        {
            // Arrange
            PeerAddress serverAddress = PeerAddress.GetRandomAddress();
            this.serverLocator.Stub(l => l.FindServer()).Return(serverAddress);
            this.ConstructClient();
            CapturingConstraint<GenericCallBack> cc = new CapturingConstraint<GenericCallBack>();
            NetworkEvent timeout = new NetworkEvent(TimeSpan.Zero);
            this.eventScheduler.Stub(q => q.Schedule(Arg<double>.Is.Equal(15000.0), Arg<GenericCallBack>.Matches(cc)))
                .Return(timeout);
            this.client.RequestValidator(this.replyHandler);

            // Expect
            this.eventScheduler.Expect(q => q.Remove(timeout));

            // Act
            this.client.HandleAllocationReply(AllocationRequestResult.Success, "foo");

            // Verify
            this.eventScheduler.VerifyAllExpectations();
        }

        private void ConstructClient()
        {
            this.client = new Client(
                this.transportProtocol,
                this.eventScheduler,
                this.serverProtocol,
                this.serverLocator,
                this.networkAddressProvider);
        }
    }
}
