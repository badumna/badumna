﻿//-----------------------------------------------------------------------
// <copyright file="ServerTests.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BadumnaTests.Validation.EngagementTests
{
    using System;
    using Badumna.Core;
    using Badumna.Transport;
    using Badumna.Validation.Allocation;
    using Badumna.Validation.Engagement;
    using NUnit.Framework;
    using Rhino.Mocks;
    using TestUtilities;

    [TestFixture]
    [DontPerformCoverage]
    public class ServerTests
    {
        private ITransportProtocol transportProtocol;
        private IClientProtocol clientProtocol;
        private IAllocator allocator;
        private Server server;

        private AllocationReplyHandler replyHandler;

        [SetUp]
        public void SetUp()
        {
            this.transportProtocol = MockRepository.GenerateMock<ITransportProtocol>();
            this.clientProtocol = MockRepository.GenerateMock<IClientProtocol>();
            this.allocator = MockRepository.GenerateMock<IAllocator>();

            this.replyHandler = MockRepository.GenerateMock<AllocationReplyHandler>();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ConstructorThrowsForNullTransportProtocol()
        {
            this.transportProtocol = null;
            this.ConstructServer();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ConstructorThrowsForNullClientProtocol()
        {
            this.clientProtocol = null;
            this.ConstructServer();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ConstructorThrowsForNullAllocator()
        {
            this.allocator = null;
            this.ConstructServer();
        }

        [Test]
        public void HandleValidatorAssignmentRequestPassesRequestToAllocator()
        {
            // Arrange
            PeerAddress requestorAddress = PeerAddress.GetRandomAddress();
            this.ConstructServer();

            // Expect
            this.allocator.Expect(a => a.RequestValidator(
                Arg<PeerAddress>.Is.Equal(requestorAddress),
                Arg<AllocationReplyHandler>.Is.Anything))
                .Return(true);
            
            // Act
            this.server.HandleValidatorAssignmentRequest(requestorAddress);

            // Verify
            this.allocator.VerifyAllExpectations();
        }

        [Test]
        public void HandleAllocationReplySendsReplyToClient()
        {
            // Arrange
            PeerAddress requestorAddress = PeerAddress.GetRandomAddress();
            AllocationRequestResult result = AllocationRequestResult.Success;
            string validatorAddress = PeerAddress.GetRandomAddress().ToString();
            this.ConstructServer();
            var cc = new CapturingConstraint<AllocationReplyHandler>();
            this.allocator.Stub(a => a.RequestValidator(
                Arg<PeerAddress>.Is.Equal(requestorAddress),
                Arg<AllocationReplyHandler>.Matches(cc)))
                .Return(true);
            this.server.HandleValidatorAssignmentRequest(requestorAddress);

            // Expect
            this.transportProtocol.SendRemoteCall(
                requestorAddress,
                QualityOfService.Reliable,
                this.clientProtocol.AllocationReply,
                result,
                validatorAddress);

            // Act
            cc.Argument.Invoke(AllocationRequestResult.Success, validatorAddress);

            // Verify
            this.transportProtocol.VerifyAllExpectations();
        }

        ////[Test]
        ////public void RequestValidatorSendsMessageViaTransportProtocol()
        ////{
        ////    // Arrange
        ////    this.ConstructServer();
        ////    PeerAddress clientAddress = PeerAddress.GetRandomAddress();
        ////    this.clientLocator.Stub(l => l.FindClient()).Return(clientAddress);
        ////    TransportEnvelope envelope = new TransportEnvelope();

        ////    // Expect
        ////    this.transportProtocol.Expect(p => p.GetMessageFor(clientAddress, QualityOfService.Reliable))
        ////        .Return(envelope);
        ////    this.transportProtocol.Expect(p => p.RemoteCall(envelope, this.clientProtocol.ValidatorAssignmentRequest));
        ////    this.transportProtocol.Expect(p => p.SendMessage(envelope));

        ////    // Act
        ////    this.server.RequestValidator(this.replyHandler);

        ////    // Verify
        ////    this.transportProtocol.VerifyAllExpectations();
        ////}

        ////[Test]
        ////public void RequestValidatorThrowsIfRequestPending()
        ////{
        ////    PeerAddress clientAddress = PeerAddress.GetRandomAddress();
        ////    this.clientLocator.Stub(l => l.FindClient()).Return(clientAddress);
        ////    this.ConstructServer();
        ////    TestDelegate request = delegate { this.server.RequestValidator(this.replyHandler); };
        ////    request.Invoke();
        ////    Assert.Throws(typeof(InvalidOperationException), request);
        ////}

        ////[Test]
        ////public void RequestValidatorSendsMessageIfPreviousRequestCompleted()
        ////{
        ////    // Arrange
        ////    PeerAddress clientAddress = PeerAddress.GetRandomAddress();
        ////    this.clientLocator.Stub(l => l.FindClient()).Return(clientAddress);
        ////    this.ConstructServer();
        ////    this.server.RequestValidator(this.replyHandler);
        ////    this.server.HandleAllocationReply(AllocationRequestResult.Timeout, string.Empty);

        ////    // Expect
        ////    this.transportProtocol.Expect(p => p.SendMessage(Arg<TransportEnvelope>.Is.Anything));
            
        ////    // Act
        ////    this.server.RequestValidator(this.replyHandler);

        ////    // Verify
        ////    this.transportProtocol.VerifyAllExpectations();
        ////}

        ////[Test]
        ////public void HandleAllocationReplyInvokesReplyHandler()
        ////{
        ////    // Arrange
        ////    PeerAddress clientAddress = PeerAddress.GetRandomAddress();
        ////    this.clientLocator.Stub(l => l.FindClient()).Return(clientAddress);
        ////    this.ConstructServer();
        ////    this.server.RequestValidator(this.replyHandler);

        ////    // Expect
        ////    this.replyHandler.Expect(h => h.Invoke(AllocationRequestResult.Success, "foo"));

        ////    // Act
        ////    this.server.HandleAllocationReply(AllocationRequestResult.Success, "foo");

        ////    // Verify
        ////    this.replyHandler.VerifyAllExpectations();
        ////}

        private void ConstructServer()
        {
            this.server = new Server(this.transportProtocol, this.clientProtocol, this.allocator);
        }
    }
}
