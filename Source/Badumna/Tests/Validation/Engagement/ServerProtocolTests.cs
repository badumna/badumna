﻿//-----------------------------------------------------------------------
// <copyright file="ServerProtocolTests.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BadumnaTests.Validation.EngagementTests
{
    using System;
    using Badumna.Core;
    using Badumna.Transport;
    using Badumna.Validation.Allocation;
    using Badumna.Validation.Engagement;
    using NUnit.Framework;
    using Rhino.Mocks;

    [TestFixture]
    [DontPerformCoverage]
    public class ServerProtocolTests
    {
        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ConstructorThrowsForNullTransportProtocol()
        {
            var serverProtocol = new ServerProtocol(null);
        }

        [Test]
        public void ConstructorRegistersMethodsWithTransportProtocol()
        {
            // Arrange
            IServerProtocol passedAllocatorProtocol = null;
            var transportProtocol = MockRepository.GenerateMock<ITransportProtocol>();
            transportProtocol.Expect(p => p.RegisterMethodsIn(null))
                .IgnoreArguments()
                .WhenCalled(c =>
                {
                    passedAllocatorProtocol = (IServerProtocol)c.Arguments[0];
                });

            // Act
            var serverProtocol = new ServerProtocol(transportProtocol);

            // Assert
            transportProtocol.VerifyAllExpectations();
            Assert.AreEqual(serverProtocol, passedAllocatorProtocol);
        }

        [Test]
        public void AllocationReplyIsPassedToAllocator()
        {
            // Arrange
            var transportProtocol = MockRepository.GenerateMock<ITransportProtocol>();
            var serverProtocol = new ServerProtocol(transportProtocol);
            var server = MockRepository.GenerateMock<IServer>();
            serverProtocol.Server = server;
            PeerAddress source = PeerAddress.GetRandomAddress();
            TransportEnvelope envelope = new TransportEnvelope();
            envelope.Source = source;
            transportProtocol.Stub(p => p.CurrentEnvelope).Return(envelope);

            // Expect
            server.Expect(a => a.HandleValidatorAssignmentRequest(source));

            // Act
            serverProtocol.ValidatorAssignmentRequest();

            // Verify
            server.VerifyAllExpectations();
        }
    }
}
