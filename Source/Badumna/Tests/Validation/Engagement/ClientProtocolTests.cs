﻿//-----------------------------------------------------------------------
// <copyright file="ClientProtocolTests.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BadumnaTests.Validation.EngagementTests
{
    using System;
    using Badumna.Core;
    using Badumna.Transport;
    using Badumna.Validation.Allocation;
    using Badumna.Validation.Engagement;
    using NUnit.Framework;
    using Rhino.Mocks;

    [TestFixture]
    [DontPerformCoverage]
    public class ClientProtocolTests
    {
        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ConstructorThrowsForNullTransportProtocol()
        {
            var clientProtocol = new ClientProtocol(null);
        }

        [Test]
        public void ConstructorRegistersMethodsWithTransportProtocol()
        {
            // Arrange
            IClientProtocol passedAllocatorProtocol = null;
            var transportProtocol = MockRepository.GenerateMock<ITransportProtocol>();
            transportProtocol.Expect(p => p.RegisterMethodsIn(null))
                .IgnoreArguments()
                .WhenCalled(c =>
                {
                    passedAllocatorProtocol = (IClientProtocol)c.Arguments[0];
                });

            // Act
            var clientProtocol = new ClientProtocol(transportProtocol);

            // Assert
            transportProtocol.VerifyAllExpectations();
            Assert.AreEqual(clientProtocol, passedAllocatorProtocol);
        }

        [Test]
        public void AllocationReplyIsPassedToAllocator()
        {
            // Arrange
            var transportProtocol = MockRepository.GenerateMock<ITransportProtocol>();
            var clientProtocol = new ClientProtocol(transportProtocol);
            var client = MockRepository.GenerateMock<IClient>();
            clientProtocol.Client = client;
            var result = AllocationRequestResult.Success;
            var address = Guid.NewGuid().ToString();

            // Expect
            client.Expect(a => a.HandleAllocationReply(result, address));

            // Act
            clientProtocol.AllocationReply(result, address);

            // Verify
            client.VerifyAllExpectations();
        }
    }
}
