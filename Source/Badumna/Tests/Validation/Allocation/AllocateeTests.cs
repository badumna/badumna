﻿//-----------------------------------------------------------------------
// <copyright file="AllocateeTests.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BadumnaTests.Validation.Allocation
{
    using System;
    using Badumna.Core;
    using Badumna.DistributedHashTable;
    using Badumna.Utilities;
    using Badumna.Validation;
    using Badumna.Validation.Allocation;
    using BadumnaTests.Core;
    using NUnit.Framework;
    using Rhino.Mocks;
    using Constraints = Rhino.Mocks.Constraints;

    [TestFixture]
    [DontPerformCoverage]
    public class AllocateeTests
    {
        private Allocatee allocatee;
        private IDhtProtocol dhtProtocol;
        private MessageParser messageParser;
        private IAllocatorProtocol allocatorProtocol;
        private DispatchMessageDelegate<DhtEnvelope> dispatcherMethod;
        private INetworkEventScheduler eventScheduler;
        private AllocationDecisionMethod decisionMethod;
        private PeerAddress requestorKey;

        [SetUp]
        public void SetUp()
        {
            this.dhtProtocol = MockRepository.GenerateMock<IDhtProtocol>();
            this.messageParser = MockRepository.GenerateMock<MessageParser>(
                Guid.NewGuid().ToString(),
                typeof(DummyProtocolMethodAttribute),
                MockRepository.GenerateMock<GenericCallBackReturn<object, Type>>());
            this.allocatorProtocol = MockRepository.GenerateMock<IAllocatorProtocol>();
            this.dispatcherMethod = MockRepository.GenerateMock<DispatchMessageDelegate<DhtEnvelope>>();
            this.dhtProtocol.Stub(p => p.Parser).Return(this.messageParser);
            this.dhtProtocol.Stub(p => p.DispatcherMethod).Return(this.dispatcherMethod);
            this.eventScheduler = MockRepository.GenerateMock<INetworkEventScheduler>();
            this.decisionMethod = MockRepository.GenerateMock<AllocationDecisionMethod>();
            this.allocatee = new Allocatee(this.dhtProtocol, this.eventScheduler, this.allocatorProtocol);
            this.requestorKey = PeerAddress.GetRandomAddress();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ConstructorThrowsForNullDhtProtocol()
        {
            this.allocatee = new Allocatee(null, this.eventScheduler, this.allocatorProtocol);
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ConstructorThrowsForNullEventScheduler()
        {
            this.allocatee = new Allocatee(this.dhtProtocol, null, this.allocatorProtocol);
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ConstructorThrowsForNullAllocatorProtocol()
        {
            this.allocatee = new Allocatee(this.dhtProtocol, this.eventScheduler, null);
        }

        [Test]
        public void AllocationDecisionMethodGetterReturnsDecisionMethod()
        {
            AllocationDecisionMethod method = MockRepository.GenerateMock<AllocationDecisionMethod>();
            Assert.AreNotEqual(method, this.allocatee.AllocationDecisionMethod);
            this.allocatee.AllocationDecisionMethod = method;
            Assert.AreEqual(method, this.allocatee.AllocationDecisionMethod);
        }

        [Test]
        public void HandleAllocationRequestInvokesDecisionMethod()
        {
            // Arrange
            DhtEnvelope envelope = null;
            this.dhtProtocol.Stub(p => p.GetMessageFor(this.requestorKey, QualityOfService.Reliable))
                .Return(envelope);

            DhtEnvelope receivedEnvelope = new DhtEnvelope();
            receivedEnvelope.Source = new PeerAddress(HashKey.Random());
            this.dhtProtocol.Stub(p => p.CurrentEnvelope).Return(receivedEnvelope);

            this.allocatee.AllocationDecisionMethod = this.decisionMethod;

            // Expect
            this.decisionMethod.Expect(m => m.Invoke()).Return(true);

            // Act
            this.allocatee.HandleAllocationRequest(this.requestorKey);

            // Verify
            this.decisionMethod.VerifyAllExpectations();
        }

        [Test]
        public void HandlerAllocationRequestGetsEnvelopeFromProtocol()
        {
            // Arrange
            DhtEnvelope envelope = null;
            this.dhtProtocol.Stub(p => p.GetMessageFor(this.requestorKey, QualityOfService.Reliable))
                .Return(envelope);

            DhtEnvelope receivedEnvelope = new DhtEnvelope();
            receivedEnvelope.Source = new PeerAddress(HashKey.Random());
            this.dhtProtocol.Stub(p => p.CurrentEnvelope).Return(receivedEnvelope);

            // Expect
            this.dhtProtocol.Expect(p => p.GetMessageFor(receivedEnvelope.Source, QualityOfService.Reliable))
                .Return(envelope);

            // Act
            this.allocatee.HandleAllocationRequest(this.requestorKey);

            // Verify
            this.dhtProtocol.VerifyAllExpectations();
        }

        [Test]
        public void HandleAllocationRequestPreparesRemoteCall()
        {
            // Arrange
            DhtEnvelope envelope = null;
            this.dhtProtocol.Stub(p => p.GetMessageFor(this.requestorKey, QualityOfService.Reliable))
                .Return(envelope);

            DhtEnvelope receivedEnvelope = new DhtEnvelope();
            receivedEnvelope.Source = new PeerAddress(HashKey.Random());
            this.dhtProtocol.Stub(p => p.CurrentEnvelope).Return(receivedEnvelope);

            bool decision = true;
            this.decisionMethod.Stub(m => m.Invoke()).Return(decision);
            this.allocatee.AllocationDecisionMethod = this.decisionMethod;

            // Expect
            this.dhtProtocol.Expect(p => p.RemoteCall(envelope, null, this.requestorKey, decision))
                .IgnoreArguments()
                .Constraints(
                    Constraints.Is.Anything(),
                    Constraints.Is.Anything(),
                    Constraints.Is.Equal(this.requestorKey),
                    Constraints.Is.Equal(decision));

            // Act
            this.allocatee.HandleAllocationRequest(this.requestorKey);

            // Verify
            this.dhtProtocol.VerifyAllExpectations();
        }

        [Test]
        public void HandleAllocationRequestSendsReply()
        {
            // Arrange
            DhtEnvelope envelope = null;
            this.dhtProtocol.Stub(p => p.GetMessageFor(this.requestorKey, QualityOfService.Reliable))
                .Return(envelope);

            DhtEnvelope receivedEnvelope = new DhtEnvelope();
            receivedEnvelope.Source = new PeerAddress(HashKey.Random());
            this.dhtProtocol.Stub(p => p.CurrentEnvelope).Return(receivedEnvelope);

            // Expect
            this.dhtProtocol.Expect(p => p.SendMessage(null)).IgnoreArguments();

            // Act
            this.allocatee.HandleAllocationRequest(this.requestorKey);

            // Verify
            this.dhtProtocol.VerifyAllExpectations();
        }
    }
}
