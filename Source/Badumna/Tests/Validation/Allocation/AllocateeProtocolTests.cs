﻿//-----------------------------------------------------------------------
// <copyright file="AllocateeProtocolTests.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BadumnaTests.Validation.Allocation
{
    using System;
    using Badumna.Core;
    using Badumna.DistributedHashTable;
    using Badumna.Utilities;
    using Badumna.Validation;
    using Badumna.Validation.Allocation;
    using NUnit.Framework;
    using Rhino.Mocks;
    using Constraints = Rhino.Mocks.Constraints;

    [TestFixture]
    [DontPerformCoverage]
    public class AllocateeProtocolTests
    {
        private IAllocatee allocatee;
        private IDhtProtocol dhtProtocol;
        private AllocateeProtocol allocateeProtocol;
        
        private PeerAddress requestorKey;

        [SetUp]
        public void SetUp()
        {
            this.dhtProtocol = MockRepository.GenerateMock<IDhtProtocol>();
            this.allocatee = MockRepository.GenerateMock<IAllocatee>();
            
            this.requestorKey = PeerAddress.GetRandomAddress();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ConstructorThrowsForNullDhtProtocol()
        {
            this.allocateeProtocol = new AllocateeProtocol(null);
        }

        [Test]
        public void ConstructorRegistersMethodsWithDhtProtocol()
        {
            // Arrange
            IAllocateeProtocol passedAllocateeProtocol = null;
            this.dhtProtocol.Expect(p => p.RegisterMethodsIn(null))
                .IgnoreArguments()
                .WhenCalled(c =>
                    {
                        passedAllocateeProtocol = (IAllocateeProtocol)c.Arguments[0];
                    });

            // Act
            this.allocateeProtocol = new AllocateeProtocol(this.dhtProtocol);

            // Assert
            this.dhtProtocol.VerifyAllExpectations();
            Assert.AreEqual(this.allocateeProtocol, passedAllocateeProtocol);
        }

        [Test]
        public void AllocationRequestIsPassedToAllocatee()
        {
            // Arrange
            this.allocateeProtocol = new AllocateeProtocol(this.dhtProtocol);
            this.allocateeProtocol.Allocatee = this.allocatee;

            // Expect
            this.allocatee.Expect(a => a.HandleAllocationRequest(this.requestorKey));
            
            // Act
            this.allocateeProtocol.AllocationRequest(this.requestorKey);
            
            // Verify
            this.allocatee.VerifyAllExpectations();
        }
    }
}
