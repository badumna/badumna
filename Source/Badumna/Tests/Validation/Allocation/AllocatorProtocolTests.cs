﻿//-----------------------------------------------------------------------
// <copyright file="AllocatorProtocolTests.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BadumnaTests.Validation.Allocation
{
    using System;
    using Badumna.Core;
    using Badumna.DistributedHashTable;
    using Badumna.Utilities;
    using Badumna.Validation;
    using Badumna.Validation.Allocation;
    using NUnit.Framework;
    using Rhino.Mocks;
    using Constraints = Rhino.Mocks.Constraints;

    [TestFixture]
    [DontPerformCoverage]
    public class AllocatorProtocolTests
    {
        private IAllocator allocator;
        private IDhtProtocol dhtProtocol;
        private AllocatorProtocol allocatorProtocol;
                
        private PeerAddress requestorKey;
        private PeerAddress replierAddress;

        [SetUp]
        public void SetUp()
        {
            this.dhtProtocol = MockRepository.GenerateMock<IDhtProtocol>();
            this.allocator = MockRepository.GenerateMock<IAllocator>();

            this.requestorKey = PeerAddress.GetRandomAddress();
            this.replierAddress = PeerAddress.GetRandomAddress();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ConstructorThrowsForNullDhtProtocol()
        {
            this.allocatorProtocol = new AllocatorProtocol(null);
        }

        [Test]
        public void ConstructorRegistersMethodsWithDhtProtocol()
        {
            // Arrange
            IAllocatorProtocol passedAllocatorProtocol = null;
            this.dhtProtocol.Expect(p => p.RegisterMethodsIn(null))
                .IgnoreArguments()
                .WhenCalled(c =>
                    {
                        passedAllocatorProtocol = (IAllocatorProtocol)c.Arguments[0];
                    });

            // Act
            this.allocatorProtocol = new AllocatorProtocol(this.dhtProtocol);

            // Assert
            this.dhtProtocol.VerifyAllExpectations();
            Assert.AreEqual(this.allocatorProtocol, passedAllocatorProtocol);
        }

        [TestCase(true)]
        [TestCase(false)]
        public void AllocationReplyIsPassedToAllocator(bool result)
        {
            // Arrange
            this.allocatorProtocol = new AllocatorProtocol(this.dhtProtocol);
            this.allocatorProtocol.Allocator = this.allocator;
            DhtEnvelope envelope = new DhtEnvelope();
            envelope.Source = this.replierAddress;
            this.dhtProtocol.Stub(p => p.CurrentEnvelope).Return(envelope);

            // Expect
            this.allocator.Expect(a => a.ReceiveAllocationReply(this.requestorKey, this.replierAddress, result));
            
            // Act
            this.allocatorProtocol.ReceiveAllocationReply(this.requestorKey, result);
            
            // Verify
            this.allocator.VerifyAllExpectations();
        }
    }
}
