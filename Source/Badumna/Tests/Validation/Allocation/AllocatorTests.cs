﻿//-----------------------------------------------------------------------
// <copyright file="AllocatorTests.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BadumnaTests.Validation.Allocation
{
    using System;
    using Badumna.Core;
    using Badumna.DistributedHashTable;
    using Badumna.Utilities;
    using Badumna.Validation;
    using Badumna.Validation.Allocation;
    using BadumnaTests.Core;
    using NUnit.Framework;
    using Rhino.Mocks;
    using Constraints = Rhino.Mocks.Constraints;

    [TestFixture]
    [DontPerformCoverage]
    public class AllocatorTests
    {
        private Allocator allocator;
        private IDhtProtocol dhtProtocol;
        private INetworkEventScheduler eventScheduler;
        private IAllocateeProtocol allocateeProtocol;
        private MessageParser messageParser;
        private DispatchMessageDelegate<DhtEnvelope> dispatcherMethod;
        private AllocationDecisionMethod decisionMethod;
        private PeerAddress requestorKey;
        private PeerAddress nonRequestorAddress;
        private AllocationReplyHandler replyHandler;

        [SetUp]
        public void SetUp()
        {
            this.dhtProtocol = MockRepository.GenerateMock<IDhtProtocol>();
            this.eventScheduler = MockRepository.GenerateMock<INetworkEventScheduler>();
            this.allocateeProtocol = MockRepository.GenerateMock<IAllocateeProtocol>();
            this.messageParser = MockRepository.GenerateMock<MessageParser>(
                Guid.NewGuid().ToString(),
                typeof(DummyProtocolMethodAttribute),
                MockRepository.GenerateMock<GenericCallBackReturn<object, Type>>());
            this.dispatcherMethod = MockRepository.GenerateMock<DispatchMessageDelegate<DhtEnvelope>>();
            this.dhtProtocol.Stub(p => p.Parser).Return(this.messageParser);
            this.dhtProtocol.Stub(p => p.DispatcherMethod).Return(this.dispatcherMethod);
            this.decisionMethod = MockRepository.GenerateMock<AllocationDecisionMethod>();
            this.allocator = new Allocator(this.dhtProtocol, this.eventScheduler, this.allocateeProtocol);
            this.requestorKey = PeerAddress.GetRandomAddress();
            do
            {
                this.nonRequestorAddress = PeerAddress.GetRandomAddress();
            }
            while (this.requestorKey.Equals(this.nonRequestorAddress));
            this.replyHandler = MockRepository.GenerateMock<AllocationReplyHandler>();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ConstructorThrowsForNullDhtProtocol()
        {
            this.allocator = new Allocator(null, this.eventScheduler, this.allocateeProtocol);
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ConstructorThrowsForNullEventScheduler()
        {
            this.allocator = new Allocator(this.dhtProtocol, null, this.allocateeProtocol);
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ConstructorThrowsForNullAllocateeProtocol()
        {
            this.allocator = new Allocator(this.dhtProtocol, this.eventScheduler, null);
        }

        [Test]
        public void RequestValidatorGetsEnvelopeFromParentProtocol()
        {
            // Expect
            DhtEnvelope envelope = null;
            this.dhtProtocol.Expect(p => p.GetMessageFor(HashKey.Random(), QualityOfService.Reliable))
                .IgnoreArguments()
                .Return(envelope);

            // Act
            this.allocator.RequestValidator(this.requestorKey, null);

            // Verify
            this.dhtProtocol.VerifyAllExpectations();
        }

        [Test]
        public void RequestValidatorPreparesValidatorAllocationRequestRemoteCall()
        {
            // Arrange
            DhtEnvelope envelope = null;
            this.dhtProtocol.Stub(p => p.GetMessageFor(this.requestorKey, QualityOfService.Reliable))
                .Return(envelope);

            // Expect
            this.dhtProtocol.Expect(p => p.RemoteCall(envelope, null, this.requestorKey))
                .IgnoreArguments()
                .Constraints(
                    Constraints.Is.Equal(envelope),
                    Constraints.Is.Anything(),
                    Constraints.Is.Equal(this.requestorKey));

            // Act
            this.allocator.RequestValidator(this.requestorKey, null);

            // Verify
            this.messageParser.VerifyAllExpectations();
        }

        [Test]
        public void RequestValidatorMakesDHTQuery()
        {
            // Arrange
            DhtEnvelope envelope = null;
            this.dhtProtocol.Stub(p => p.GetMessageFor(this.requestorKey, QualityOfService.Reliable))
                .Return(envelope);

            // Expect
            this.dhtProtocol.Expect(p => p.SendMessage(envelope));

            // Act
            this.allocator.RequestValidator(this.requestorKey, null);

            // Verify
            this.dhtProtocol.VerifyAllExpectations();
        }

        [Test]
        public void RequestValidatorReturnsTrueWhenRequestorHasNoOutstandingRequests()
        {
            Assert.IsTrue(this.allocator.RequestValidator(this.requestorKey, null));
        }

        [Test]
        public void RequestValidatorReturnsFalseWhenRequestorHasOutstandingRequests()
        {
            // Arrange
            this.allocator.RequestValidator(this.requestorKey, null);

            // Act
            bool result = this.allocator.RequestValidator(this.requestorKey, null);

            // Assert
            Assert.IsFalse(result);
        }

        [Test]
        public void RequestValidatorSchedulesTimeoutEvents()
        {
            // Expect two timeouts (query and allocation).
            this.eventScheduler.Expect(q => q.Schedule(0.0, null))
                .IgnoreArguments()
                .Return(null)
                .Repeat.Twice();

            // Act
            this.allocator.RequestValidator(this.requestorKey, null);

            // Verify
            this.eventScheduler.VerifyAllExpectations();
        }

        [Test]
        public void OnRequestTimeoutMakesNewQuery()
        {
            // Arrange
            // Capture first timeout (query timeout).
            GenericCallBack queryTimeoutCallback = null;
            this.eventScheduler.Stub(q => q.Schedule(0f, null))
                .IgnoreArguments()
                .WhenCalled(c => { queryTimeoutCallback = (GenericCallBack)c.Arguments[1]; })
                .Return(null)
                .Repeat.Once();
            this.allocator.RequestValidator(this.requestorKey, this.replyHandler);

            // Expect
            this.dhtProtocol.Expect(p => p.SendMessage(null)).IgnoreArguments();

            // Act
            queryTimeoutCallback.Invoke();

            // Verify
            this.dhtProtocol.VerifyAllExpectations();
        }

        [Test]
        public void RequestValidatorReplyHandlerInvokedOnAllocationTimeout()
        {
            // Arrange
            // Capture last (second) timeout (allocation timeout).
            GenericCallBack timeoutCallback = null;
            this.eventScheduler.Stub(q => q.Schedule(0f, null))
                .IgnoreArguments()
                .WhenCalled(c => { timeoutCallback = (GenericCallBack)c.Arguments[1]; })
                .Return(null);
            this.allocator.RequestValidator(this.requestorKey, this.replyHandler);

            // Expect
            this.replyHandler.Expect(h => h.Invoke(AllocationRequestResult.Timeout, string.Empty));

            // Act
            timeoutCallback.Invoke();

            // Verify
            this.replyHandler.VerifyAllExpectations();
        }

        [Test]
        public void ValidatorAllocationReplyFromRequestorTriggersNewQuery()
        {
            // Arrange
            this.allocator.RequestValidator(this.requestorKey, this.replyHandler);

            DhtEnvelope receivedEnvelope = new DhtEnvelope();
            receivedEnvelope.Source = new PeerAddress(HashKey.Random());
            this.dhtProtocol.Stub(p => p.CurrentEnvelope).Return(receivedEnvelope);

            // Expect
            this.dhtProtocol.Expect(p => p.SendMessage(Arg<DhtEnvelope>.Is.Anything));

            // Act
            this.allocator.ReceiveAllocationReply(this.requestorKey, this.requestorKey, true);

            // Verify
            this.dhtProtocol.VerifyAllExpectations();
        }
        
        [Test]
        public void ValidatorAllocationReplyWithAcceptanceCancelsQueryAndAllocationTimeouts()
        {
            // Arrange
            this.allocator.RequestValidator(this.requestorKey, this.replyHandler);
            DhtEnvelope receivedEnvelope = new DhtEnvelope();
            receivedEnvelope.Source = new PeerAddress(HashKey.Random());
            this.dhtProtocol.Stub(p => p.CurrentEnvelope).Return(receivedEnvelope);

            // Expect
            this.eventScheduler.Expect(q => q.Remove(null))
                .IgnoreArguments()
                .Repeat.Twice();

            // Act
            this.allocator.ReceiveAllocationReply(this.requestorKey, this.nonRequestorAddress, true);

            // Verify
            this.eventScheduler.VerifyAllExpectations();
        }

        [Test]
        public void ValidatorAllocationReplyWithoutAcceptanceCancelsQueryTimeoutOnly()
        {
            // Arrange
            this.allocator.RequestValidator(this.requestorKey, this.replyHandler);

            DhtEnvelope receivedEnvelope = new DhtEnvelope();
            receivedEnvelope.Source = new PeerAddress(HashKey.Random());
            this.dhtProtocol.Stub(p => p.CurrentEnvelope).Return(receivedEnvelope);

            // Expect
            this.eventScheduler.Expect(q => q.Remove(null))
                .IgnoreArguments()
                .Repeat.Once();
            this.eventScheduler.Stub(q => q.Remove(null))
                .IgnoreArguments()
                .Throw(new InvalidOperationException("Only one timeout should be cancelled."));

            // Act
            this.allocator.ReceiveAllocationReply(this.requestorKey, this.nonRequestorAddress, false);

            // Verify
            this.eventScheduler.VerifyAllExpectations();
        }

        [Test]
        public void ValidatorAllocationReplyAcceptingAllocationInvokesReplyHandler()
        {
            // Arrange
            this.allocator.RequestValidator(this.requestorKey, this.replyHandler);

            // Expect
            this.replyHandler.Expect(h => h.Invoke(AllocationRequestResult.Success, this.nonRequestorAddress.ToString()));

            // Act
            this.allocator.ReceiveAllocationReply(this.requestorKey, this.nonRequestorAddress, true);

            // Verify
            this.replyHandler.VerifyAllExpectations();
        }

        [TestCase(true)]
        [TestCase(false)]
        public void ValidatorAllocationReplyIgnoresRepliesForUnknownRequestors(bool acceptance)
        {
            // Arrange
            this.allocator.RequestValidator(this.requestorKey, this.replyHandler);

            DhtEnvelope receivedEnvelope = new DhtEnvelope();
            receivedEnvelope.Source = new PeerAddress(HashKey.Random());
            this.dhtProtocol.Stub(p => p.CurrentEnvelope).Return(receivedEnvelope);

            // Expect
            this.replyHandler.Expect(h => h.Invoke(AllocationRequestResult.Success, receivedEnvelope.Source.ToString()))
                .IgnoreArguments()
                .Repeat.Never();
            this.eventScheduler.Expect(q => q.Remove(null))
                .IgnoreArguments()
                .Repeat.Never();
            this.eventScheduler.Expect(q => q.Schedule(0.0, () => { }))
                .IgnoreArguments()
                .Repeat.Never();
            this.dhtProtocol.Expect(p => p.SendMessage(null))
                .IgnoreArguments()
                .Repeat.Never();

            // Act
            this.allocator.ReceiveAllocationReply(this.nonRequestorAddress, this.nonRequestorAddress, acceptance);

            // Verify
            this.replyHandler.VerifyAllExpectations();
            this.eventScheduler.VerifyAllExpectations();
            this.dhtProtocol.VerifyAllExpectations();
        }
    
        // TODO: Test to make sure duplicate messages are ignored?
        // E.g. query reply is handled, the received again - we don't want to 
        // cancel subsequent queries timeouts and send new ones.
        // This would require query destinations to be matched.
        // This is not necessary if it is guarenteed that duplicate messages are
        // trapped by the protocol before reaching the allocator.
    }
}
