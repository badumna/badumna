﻿//-----------------------------------------------------------------------
// <copyright file="ValidatedEntityTests.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BadumnaTests.Validation
{
    using System;
    using System.IO;
    using Badumna;
    using Badumna.Core;
    using Badumna.DataTypes;
    using Badumna.DistributedHashTable;
    using Badumna.SpatialEntities;
    using Badumna.Transport;
    using Badumna.Utilities;
    using Badumna.Validation;
    using Badumna.Validation.Allocation;
    using NUnit.Framework;
    using Rhino.Mocks;
    using TestUtilities;
    using Engagement = Badumna.Validation.Engagement;
    using Hosting = Badumna.Validation.Hosting;

    [TestFixture]
    public class ValidatedEntityTests
    {
        private INetworkFacade networkFacade;
        private FakeValidatedEntity entity;
        private Vector3 testPosition;
        private byte testByte;
        private int testInt;
        private float testFloat;
        private bool testBool;
        private string testString;
        private Vector3 testVector;

        [SetUp]
        public void SetUp()
        {
            this.networkFacade = MockRepository.GenerateMock<INetworkFacade>();
            this.entity = new FakeValidatedEntity(this.networkFacade);
            this.GenerateRandomTestData();
        }

        [Test]
        public void ConstructorThrowsForUnsupportedReplicablePropertyType()
        {
            Assert.Throws(
                typeof(NotSupportedException),
                () => new ValidatedEntityWithBadReplicatedPropertyType(this.networkFacade));
        }

        [Test]
        public void SerializeDeserializePreservesValues()
        {
            this.ConstructEntityWithRandomPropertyValues();
            BooleanArray flags = new BooleanArray(true);
            MemoryStream stream = new MemoryStream();
            this.entity.Serialize(flags, stream);
            stream.Position = 0;
            FakeValidatedEntity replica = new FakeValidatedEntity(this.networkFacade);
            replica.Deserialize(flags, stream, 0);
            this.AssertEntityHasTestValuesExcludingPosition(replica);
        }

        [Test]
        public void SerializeDeserializeHandlesNullStrings()
        {
            this.ConstructEntityWithRandomPropertyValues();
            this.entity.MyString = null;
            BooleanArray flags = new BooleanArray(true);
            MemoryStream stream = new MemoryStream();
            this.entity.Serialize(flags, stream);
            stream.Position = 0;
            FakeValidatedEntity replica = new FakeValidatedEntity(this.networkFacade);
            replica.Deserialize(flags, stream, 0);
            Assert.AreEqual(this.testByte, replica.MyByte);
            Assert.AreEqual(this.testInt, replica.MyInt);
            Assert.AreEqual(this.testFloat, replica.MyFloat);
            Assert.AreEqual(this.testBool, replica.MyBool);
            Assert.AreEqual(this.testVector, replica.MyVector);
            Assert.IsNull(replica.MyString);
        }

        [Test]
        public void SaveStateRestoreStatePreservesValues()
        {
            this.ConstructEntityWithRandomPropertyValues();
            ISerializedEntityState state = this.entity.SaveState();
            FakeValidatedEntity replica = new FakeValidatedEntity(this.networkFacade);
            replica.RestoreState(state);
            this.AssertEntityHasTestValuesIncludingPosition(replica);
        }

        [Test]
        public void CreateUpdateCreatesFullUpdateOnFirstCall()
        {
            this.ConstructEntityWithRandomPropertyValues();
            ISerializedEntityState update = this.entity.CreateUpdate();
            FakeValidatedEntity replica = new FakeValidatedEntity(this.networkFacade);
            replica.RestoreState(update);
            this.AssertEntityHasTestValuesIncludingPosition(replica);
        }

        [Test]
        public void CreateUpdateCreatesDeltaUpdateSinceLastCallForChangedReplicableProperties()
        {
            this.ConstructEntityWithRandomPropertyValues();
            ISerializedEntityState update = this.entity.CreateUpdate();
            string newString = Guid.NewGuid().ToString();
            this.entity.MyString = newString;
            update = this.entity.CreateUpdate();
            FakeValidatedEntity replica = new FakeValidatedEntity(this.networkFacade);
            replica.RestoreState(update);
            Assert.AreEqual(newString, replica.MyString);
        }

        [Test]
        public void CreateUpdateCreatesDeltaUpdateSinceLastCallForChangedPosition()
        {
            this.ConstructEntityWithRandomPropertyValues();
            ISerializedEntityState update = this.entity.CreateUpdate();
            Vector3 newPosition = this.testPosition * -1;
            this.entity.Position = newPosition;
            update = this.entity.CreateUpdate();
            FakeValidatedEntity replica = new FakeValidatedEntity(this.networkFacade);
            replica.RestoreState(update);
            Assert.AreEqual(newPosition, replica.Position);
        }

        [Test]
        public void MergeStateUpdatesOverwritesPropertiesInUpdate()
        {
            this.ConstructEntityWithRandomPropertyValues();
            ISerializedEntityState baseState = this.entity.CreateUpdate();
            int newInt = -99;
            this.entity.MyInt = newInt;
            ISerializedEntityState partialUpdate = this.entity.CreateUpdate();
            var newState = this.entity.MergeStateUpdates(baseState, partialUpdate);
            FakeValidatedEntity replica = new FakeValidatedEntity(this.networkFacade);
            replica.RestoreState(newState);
            Assert.AreEqual(newInt, replica.MyInt);
        }

        private void ConstructEntityWithRandomPropertyValues()
        {
            this.entity = new FakeValidatedEntity(this.networkFacade);
            this.entity.Position = this.testPosition;
            this.entity.MyByte = this.testByte;
            this.entity.MyInt = this.testInt;
            this.entity.MyFloat = this.testFloat;
            this.entity.MyBool = this.testBool;
            this.entity.MyString = this.testString;
            this.entity.MyVector = this.testVector;
        }

        private void GenerateRandomTestData()
        {
            Random random = new Random();
            this.testPosition = new Vector3(
                (float)random.NextDouble(),
                (float)random.NextDouble(),
                (float)random.NextDouble());
            byte[] bytes = new byte[1];
            random.NextBytes(bytes);
            this.testByte = bytes[0];
            this.testInt = random.Next();
            this.testFloat = (float)random.NextDouble();
            this.testBool = this.testInt % 2 == 0;
            this.testString = Guid.NewGuid().ToString();
            this.testVector = new Vector3(
                (float)random.NextDouble(),
                (float)random.NextDouble(),
                (float)random.NextDouble());
        }

        private void AssertEntityHasTestValuesExcludingPosition(FakeValidatedEntity entity)
        {
            Assert.AreEqual(this.testByte, entity.MyByte);
            Assert.AreEqual(this.testInt, entity.MyInt);
            Assert.AreEqual(this.testFloat, entity.MyFloat);
            Assert.AreEqual(this.testBool, entity.MyBool);
            Assert.AreEqual(this.testString, entity.MyString);
            Assert.AreEqual(this.testVector, entity.MyVector);
        }

        private void AssertEntityHasTestValuesIncludingPosition(FakeValidatedEntity entity)
        {
            Assert.AreEqual(this.testPosition, entity.Position);
            this.AssertEntityHasTestValuesExcludingPosition(entity);
        }

        private class FakeValidatedEntity : ValidatedEntity
        {
            public FakeValidatedEntity(INetworkFacade networkFacade)
                : base(networkFacade)
            {
            }

            [Replicable]
            public byte MyByte { get; set; }

            [Replicable]
            public int MyInt { get; set; }

            [Replicable]
            public float MyFloat { get; set; }

            [Replicable]
            public bool MyBool { get; set; }

            [Replicable]
            public string MyString { get; set; }

            [Replicable]
            public Vector3 MyVector { get; set; }

            public override void Update(TimeSpan timeInterval, byte[] input, UpdateMode mode)
            {
                throw new NotImplementedException();
            }

            public override void HandleEvent(System.IO.Stream stream)
            {
                throw new NotImplementedException();
            }
        }

        private class ValidatedEntityWithBadReplicatedPropertyType : ValidatedEntity
        {
            public ValidatedEntityWithBadReplicatedPropertyType(INetworkFacade networkFacade)
                : base(networkFacade)
            {
            }

            [Replicable]
            private long MyLong { get; set; }

            public override void Update(TimeSpan timeInterval, byte[] input, UpdateMode mode)
            {
                throw new NotImplementedException();
            }

            public override void HandleEvent(System.IO.Stream stream)
            {
                throw new NotImplementedException();
            }
        }
    }
}
