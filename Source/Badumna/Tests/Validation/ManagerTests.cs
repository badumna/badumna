﻿//-----------------------------------------------------------------------
// <copyright file="ManagerTests.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

////namespace BadumnaTests.Validation
////{
////    using System;
////    using Badumna.Core;
////    using Badumna.DistributedHashTable;
////    using Badumna.Validation;
////    using NUnit.Framework;
////    using Rhino.Mocks;

////    [TestFixture]
////    public class ManagerTests
////    {
////        private Manager manager;
////        private IDhtProtocol dhtProtocol;
////        private INetworkEventScheduler eventScheduler;
////        private IAllocator allocator;
////        private AllocatorFactory allocatorFactory;

////        [SetUp]
////        public void SetUp()
////        {
////            this.dhtProtocol = MockRepository.GenerateMock<IDhtProtocol>();
////            this.eventScheduler = MockRepository.GenerateMock<INetworkEventScheduler>();
////            this.allocator = MockRepository.GenerateMock<IAllocator>();
////            this.allocatorFactory = MockRepository.GenerateMock<AllocatorFactory>();
////        }

////        [Test]
////        [ExpectedException(typeof(ArgumentNullException))]
////        public void ConstructorThrowsForNullProtocol()
////        {
////            this.manager = new Manager(
////                null,
////                this.eventScheduler,
////                this.allocatorFactory);
////        }

////        [Test]
////        [ExpectedException(typeof(ArgumentNullException))]
////        public void ConstructorThrowsForNullEventScheduler()
////        {
////            this.manager = new Manager(
////                this.dhtProtocol,
////                null,
////                this.allocatorFactory);
////        }

////        [Test]
////        [ExpectedException(typeof(ArgumentNullException))]
////        public void ConstructorThrowsForNullAllocatorFactory()
////        {
////            this.manager = new Manager(
////                this.dhtProtocol,
////                this.eventScheduler,
////                null);
////        }

////        [Test]
////        public void ConstructorCreatesDefaultAllocatorFactory()
////        {
////            // Not throwing exception indicates successful test.
////            this.dhtProtocol.Stub(p => p.Parser).Return(
////                MockRepository.GenerateMock<IMessageParser>());
////            this.manager = new Manager(this.dhtProtocol, this.eventScheduler);
////        }

////        [Test]
////        public void ConstructorCreatesAllocator()
////        {
////            this.allocatorFactory.Expect(f => f.Invoke(this.dhtProtocol, this.eventScheduler));
////            this.manager = new Manager(
////                this.dhtProtocol,
////                this.eventScheduler,
////                this.allocatorFactory);
////            this.allocatorFactory.VerifyAllExpectations();
////        }

////        [Test]
////        public void AllocationDecisionMethodSetsAllocatorDecisionMethod()
////        {
////            // Arrange
////            this.allocatorFactory.Stub(f => f.Invoke(null, null))
////                .IgnoreArguments()
////                .Return(this.allocator);
////            this.manager = new Manager(this.dhtProtocol, this.eventScheduler, this.allocatorFactory);
////            AllocationDecisionMethod method = MockRepository.GenerateMock<AllocationDecisionMethod>();

////            // Expect
////            this.allocator.Expect(a => a.AllocationDecisionMethod = method);

////            // Act
////            this.manager.AllocationDecisionMethod = method;

////            // Verify
////            this.allocator.VerifyAllExpectations();
////        }

////        [Test]
////        public void AllocationDecisionMethodGetsAllocatorDecisionMethod()
////        {
////            // Arrange
////            this.allocatorFactory.Stub(f => f.Invoke(null, null))
////                .IgnoreArguments()
////                .Return(this.allocator);
////            this.manager = new Manager(this.dhtProtocol, this.eventScheduler, this.allocatorFactory);

////            // Expect
////            this.allocator.Expect(a => a.AllocationDecisionMethod);

////            // Act
////            AllocationDecisionMethod method = this.manager.AllocationDecisionMethod;

////            // Verify
////            Assert.IsFalse(method.Invoke()); // Default method returns false.
////            this.allocator.VerifyAllExpectations();
////        }

////        [Test]
////        public void RequestValidatorPassesRequestToAllocator()
////        {
////            // Arrange
////            this.allocatorFactory.Stub(f => f.Invoke(null, null))
////                .IgnoreArguments()
////                .Return(this.allocator);
////            this.manager = new Manager(this.dhtProtocol, this.eventScheduler, this.allocatorFactory);
////            AllocationReplyHandler handler = MockRepository.GenerateMock<AllocationReplyHandler>();
////            HashKey requestorKey = HashKey.Random();
            
////            // Expect
////            this.allocator.Expect(a => a.RequestValidator(requestorKey, handler)).Return(true);

////            // Act
////            this.manager.RequestValidator(requestorKey.ToBytes(), handler);

////            // Verify
////            this.allocator.VerifyAllExpectations();
////        }
////    }
////}
