﻿//-----------------------------------------------------------------------
// <copyright file="FacadeTests.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BadumnaTests.Validation
{
    using System;
    using Badumna;
    using Badumna.Core;
    using Badumna.DataTypes;
    using Badumna.DistributedHashTable;
    using Badumna.SpatialEntities;
    using Badumna.Transport;
    using Badumna.Utilities;
    using Badumna.Validation;
    using Badumna.Validation.Allocation;
    using BadumnaTests.Core;
    using NUnit.Framework;
    using Rhino.Mocks;
    using TestUtilities;
    using Engagement = Badumna.Validation.Engagement;
    using Hosting = Badumna.Validation.Hosting;

    [TestFixture]
    public class FacadeTests
    {
        private Facade facade;
        private IValidationModule validationModule;
        private ITransportProtocol transportProtocol;
        private IDhtProtocol dhtProtocol;
        private INetworkEventScheduler eventScheduler;
        private IPeerConnectionNotifier peerConnectionNotifier;
        private INetworkAddressProvider networkAddressProvider;
        private ISpatialEntityManager spatialEntityManager;
        private INetworkFacade networkFacade;
        private ITime timeKeeper;
        private QueueInvokable queueApplicationEvent;
        private GenericCallBackReturn<BadumnaId> generateBadumnaId;
        private IAllocatorProtocol allocatorProtocol;
        private IAllocateeProtocol allocateeProtocol;
        private IAllocator allocator;
        private IAllocatee allocatee;
        private AllocatorFactory allocatorFactory;
        private AllocateeFactory allocateeFactory;
        private Engagement.IServerLocator serverLocator;
        private Engagement.IClientProtocol engagementClientProtocol;
        private Engagement.IServerProtocol engagementServerProtocol;
        private Engagement.ClientFactory engagementClientFactory;
        private Engagement.ServerFactory engagementServerFactory;
        private Engagement.IServer engagementServer;
        private Engagement.IClient engagementClient;
        private Hosting.IClientProtocol hostingClientProtocol;
        private Hosting.IServerProtocol hostingServerProtocol;
        private Hosting.ClientFactory hostingClientFactory;
        private Hosting.ServerFactory hostingServerFactory;
        private Hosting.IClient hostingClient;
        private Hosting.IServer hostingServer;

        private IValidatedEntity entity;
        private uint entityType;

        [SetUp]
        public void SetUp()
        {
            this.validationModule = MockRepository.GenerateMock<IValidationModule>();
            this.transportProtocol = MockRepository.GenerateMock<ITransportProtocol>();
            this.dhtProtocol = MockRepository.GenerateMock<IDhtProtocol>();
            this.eventScheduler = MockRepository.GenerateMock<INetworkEventScheduler>();
            this.peerConnectionNotifier = MockRepository.GenerateMock<IPeerConnectionNotifier>();
            this.networkAddressProvider = MockRepository.GenerateMock<INetworkAddressProvider>();
            this.spatialEntityManager = MockRepository.GenerateMock<ISpatialEntityManager>();
            this.networkFacade = MockRepository.GenerateMock<INetworkFacade>();
            this.timeKeeper = MockRepository.GenerateMock<ITime>();
            this.queueApplicationEvent = i => i.Invoke();
            this.generateBadumnaId = () => null;
            this.allocatorProtocol = MockRepository.GenerateMock<IAllocatorProtocol>();
            this.allocateeProtocol = MockRepository.GenerateMock<IAllocateeProtocol>();
            this.allocator = MockRepository.GenerateMock<IAllocator>();
            this.allocatee = MockRepository.GenerateMock<IAllocatee>();
            this.allocatorFactory = MockRepository.GenerateMock<AllocatorFactory>();
            this.allocateeFactory = MockRepository.GenerateMock<AllocateeFactory>();
            this.serverLocator = MockRepository.GenerateMock<Engagement.IServerLocator>();
            this.engagementClientProtocol = MockRepository.GenerateMock<Engagement.IClientProtocol>();
            this.engagementServerProtocol = MockRepository.GenerateMock<Engagement.IServerProtocol>();
            this.engagementClientFactory = MockRepository.GenerateMock<Engagement.ClientFactory>();
            this.engagementServerFactory = MockRepository.GenerateMock<Engagement.ServerFactory>();
            this.engagementServer = MockRepository.GenerateMock<Engagement.IServer>();
            this.engagementClient = MockRepository.GenerateMock<Engagement.IClient>();
            this.hostingClientProtocol = MockRepository.GenerateMock<Hosting.IClientProtocol>();
            this.hostingServerProtocol = MockRepository.GenerateMock<Hosting.IServerProtocol>();
            this.hostingClientFactory = MockRepository.GenerateMock<Hosting.ClientFactory>();
            this.hostingServerFactory = MockRepository.GenerateMock<Hosting.ServerFactory>();
            this.hostingClient = MockRepository.GenerateMock<Hosting.IClient>();
            this.hostingServer = MockRepository.GenerateMock<Hosting.IServer>();

            this.entity = MockRepository.GenerateMock<IValidatedEntity>();
            this.entityType = (uint)new Random().Next(0, int.MaxValue);
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ConstructorThrowsForNullTransportProtocol()
        {
            this.StubFactoryDelegates();
            this.transportProtocol = null;
            this.ConstructFacade();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ConstructorThrowsForNullDhtProtocol()
        {
            this.StubFactoryDelegates();
            this.dhtProtocol = null;
            this.ConstructFacade();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ConstructorThrowsForNullEventScheduler()
        {
            this.StubFactoryDelegates();
            this.eventScheduler = null;
            this.ConstructFacade();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ConstructorThrowsForNullNetworkFacade()
        {
            this.StubFactoryDelegates();
            this.networkFacade = null;
            this.ConstructFacade();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ConstructorThrowsForNullServerLocator()
        {
            this.StubFactoryDelegates();
            this.serverLocator = null;
            this.ConstructFacade();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ConstructorThrowsForNullNetworkAddressProvider()
        {
            this.StubFactoryDelegates();
            this.networkAddressProvider = null;
            this.ConstructFacade();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ConstructorThrowsForNullSpatialEntityManager()
        {
            this.StubFactoryDelegates();
            this.spatialEntityManager = null;
            this.ConstructFacade();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ConstructorThrowsForNullEngagementClientProtocol()
        {
            this.StubFactoryDelegates();
            this.engagementClientProtocol = null;
            this.ConstructFacade();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ConstructorThrowsForNullEngagementServerProtocol()
        {
            this.StubFactoryDelegates();
            this.engagementServerProtocol = null;
            this.ConstructFacade();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ConstructorThrowsForNullEngagementClientFactory()
        {
            this.StubFactoryDelegates();
            this.engagementClientFactory = null;
            this.ConstructFacade();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ConstructorThrowsForNullEngagementServerFactory()
        {
            this.engagementServerFactory = null;
            this.ConstructFacade();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ConstructorThrowsForNullPeerConnectionNotifier()
        {
            this.StubFactoryDelegates();
            this.peerConnectionNotifier = null;
            this.ConstructFacade();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ConstructorThrowsForNullAllocatorProtocol()
        {
            this.StubFactoryDelegates();
            this.allocatorProtocol = null;
            this.ConstructFacade();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ConstructorThrowsForNullAllocateeProtocol()
        {
            this.StubFactoryDelegates();
            this.allocateeProtocol = null;
            this.ConstructFacade();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ConstructorThrowsForNullAllocatorFactory()
        {
            this.StubFactoryDelegates();
            this.allocatorFactory = null;
            this.ConstructFacade();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ConstructorThrowsForNullAllocateeFactory()
        {
            this.StubFactoryDelegates();
            this.allocateeFactory = null;
            this.ConstructFacade();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ConstructorThrowsForNullHostingClientProtocol()
        {
            this.StubFactoryDelegates();
            this.hostingClientProtocol = null;
            this.ConstructFacade();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ConstructorThrowsForNullHostingServerProtocol()
        {
            this.StubFactoryDelegates();
            this.hostingServerProtocol = null;
            this.ConstructFacade();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ConstructorThrowsForNullHostingClientFactory()
        {
            this.StubFactoryDelegates();
            this.hostingClientFactory = null;
            this.ConstructFacade();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ConstructorThrowsForNullHostingServerFactory()
        {
            this.StubFactoryDelegates();
            this.hostingServerFactory = null;
            this.ConstructFacade();
        }

        [Test]
        public void ConstructorCreatesDefaultFactories()
        {
            // Not throwing exception indicates successful test.
            this.dhtProtocol.Stub(p => p.Parser).Return(
                MockRepository.GenerateMock<MessageParser>(
                    Guid.NewGuid().ToString(),
                    typeof(DummyProtocolMethodAttribute),
                    MockRepository.GenerateMock<GenericCallBackReturn<object, Type>>()));
            this.transportProtocol.Stub(p => p.Parser).Return(
                MockRepository.GenerateMock<MessageParser>(
                    Guid.NewGuid().ToString(),
                    typeof(DummyProtocolMethodAttribute),
                    MockRepository.GenerateMock<GenericCallBackReturn<object, Type>>()));
            this.facade = new Facade(
                this.validationModule,
                this.transportProtocol,
                this.dhtProtocol,
                this.eventScheduler,
                this.peerConnectionNotifier,
                this.networkAddressProvider,
                this.spatialEntityManager,
                this.networkFacade,
                this.timeKeeper,
                this.queueApplicationEvent,
                this.generateBadumnaId);
        }

        [Test]
        public void ConstructorCreatesAllocator()
        {
            this.StubFactoryDelegates();
            this.allocatorFactory = MockRepository.GenerateMock<AllocatorFactory>();
            this.allocatorFactory.Expect(f => f.Invoke(this.dhtProtocol, this.eventScheduler, this.allocateeProtocol));
            this.ConstructFacade();
            this.allocatorFactory.VerifyAllExpectations();
        }

        [Test]
        public void ConstructorCreatesAllocatee()
        {
            this.StubFactoryDelegates();
            this.allocateeFactory = MockRepository.GenerateMock<AllocateeFactory>();
            this.allocateeFactory.Expect(f => f.Invoke(this.dhtProtocol, this.eventScheduler, this.allocatorProtocol));
            this.ConstructFacade();
            this.allocateeFactory.VerifyAllExpectations();
        }

        [Test]
        public void ConstructorSubscribesToHostingClientFailureEvent()
        {
            // Expect
            this.hostingClient.Expect(c => c.ValidationFailure += null).IgnoreArguments();

            // Act
            this.FullyConstructFacade();

            // verify
            this.hostingClient.VerifyAllExpectations();
        }

        [Test]
        public void FailureEventTriggeredOnClientFailure()
        {
            // Arrange
            var cc = new CapturingConstraint<EventHandler<ValidationFailureEventArgs>>();
            this.hostingClient.Stub(c => c.ValidationFailure += Arg<EventHandler<ValidationFailureEventArgs>>.Matches(cc));
            this.FullyConstructFacade();
            var handler = MockRepository.GenerateMock<EventHandler<ValidationFailureEventArgs>>();
            this.facade.ValidationFailed += handler;
            
            // Expect
            handler.Expect(h => h.Invoke(Arg<object>.Is.Anything, Arg<ValidationFailureEventArgs>.Is.Anything));

            // Act
            cc.Argument.Invoke(null, null);

            // Verify
            handler.VerifyAllExpectations();
        }

        [Test]
        public void RegisterValidatedEntityPassesRequestToHostingClient()
        {
            // Arrange
            this.FullyConstructFacade();

            // Expect
            this.eventScheduler.Expect(q => q.Push(
                this.hostingClient.RegisterValidatedEntity,
                this.entity,
                this.entityType,
                (ISerializedEntityState)null))
                .Return(null);

            // Act
            this.facade.RegisterValidatedEntity(this.entity, this.entityType);

            // Verify
            this.eventScheduler.VerifyAllExpectations();
        }

        [Test]
        public void UpdateEntityPassesRequestToHostingClient()
        {
            // Arrange
            this.FullyConstructFacade();
            var interval = DateTime.Now - DateTime.Today;
            var userInput = new byte[0];

            // Expect
            this.eventScheduler.Expect(q => q.Push(
                this.hostingClient.UpdateEntity,
                this.entity,
                interval,
                userInput))
                .Return(null);

            // Act
            this.facade.UpdateEntity(this.entity, interval, userInput);

            // Verify
            this.eventScheduler.VerifyAllExpectations();
        }

        [Test]
        public void RegisterEntityWithScenePassesRequestToHostingClient()
        {
            // Arrange
            this.FullyConstructFacade();
            var id = Guid.NewGuid();
            var scene = MockRepository.GenerateMock<NetworkScene>(
                new QualifiedName("", Guid.NewGuid().ToString()),
                null,
                null,
                null,
                new NetworkEventQueue(),
                this.queueApplicationEvent,
                false);

            // Expect
            this.eventScheduler.Expect(q => q.Push(
                this.hostingClient.RegisterEntityWithScene,
                this.entity,
                scene))
                .Return(null);

            // Act
            this.facade.RegisterEntityWithScene(this.entity, scene);

            // Verify
            this.eventScheduler.VerifyAllExpectations();
        }

        [Test]
        public void UnregisterEntityWithScenePassesRequestToHostingClient()
        {
            // Arrange
            this.FullyConstructFacade();
            var id = Guid.NewGuid();

            // Expect
            this.eventScheduler.Expect(q => q.Push(
                this.hostingClient.UnregisterEntityFromScene,
                this.entity))
                .Return(null);

            // Act
            this.facade.UnregisterEntityFromScene(this.entity);

            // Verify
            this.eventScheduler.VerifyAllExpectations();
        }

        [Test]
        public void ShutdownShutsDownHostingServer()
        {
            // Arrange
            this.FullyConstructFacade();

            // Expect
            this.hostingServer.Expect(s => s.Shutdown());

            // Act
            this.facade.Shutdown();

            // Verify
            this.hostingServer.VerifyAllExpectations();
        }

        [Test]
        public void ShutdownShutsDownHostingClient()
        {
            // Arrange
            this.FullyConstructFacade();

            // Expect
            this.hostingClient.Expect(s => s.Shutdown());

            // Act
            this.facade.Shutdown();

            // Verify
            this.hostingClient.VerifyAllExpectations();
        }

        [Test]
        public void RegularProcessingTicksHostingClient()
        {
            // Arrange
            this.FullyConstructFacade();

            // Act
            this.facade.RegularProcessing();

            // Assert
            this.hostingClient.AssertWasCalled(c => c.RegularProcessing());
        }

        [Test]
        public void TimeoutCalculationMethodSetterPassesDelegateToHostingClient()
        {
            // Arrange
            this.FullyConstructFacade();
            ValidatorTimeoutCalculationMethod method =
                MockRepository.GenerateMock<ValidatorTimeoutCalculationMethod>();

            // Act
            this.facade.TimeoutCalculationMethod = method;

            // Assert
            this.hostingClient.AssertWasCalled(c => c.ValidationTimeoutCalculationFunction = method);
        }

        private void FullyConstructFacade()
        {
            this.StubFactoryDelegates();
            this.ConstructFacade();
        }

        private void StubFactoryDelegates()
        {
            this.engagementClientFactory.Stub(
                f => f.Invoke(
                    this.transportProtocol,
                    this.eventScheduler,
                    this.engagementServerProtocol,
                    this.serverLocator,
                    this.networkAddressProvider))
                .Return(this.engagementClient);
            this.engagementServerFactory.Stub(
                f => f.Invoke(this.transportProtocol, this.engagementClientProtocol, this.allocator))
                .Return(this.engagementServer);

            this.allocatorFactory.Stub(f => f.Invoke(this.dhtProtocol, this.eventScheduler, this.allocateeProtocol))
                .Return(this.allocator);
            this.allocateeFactory.Stub(f => f(this.dhtProtocol, this.eventScheduler, this.allocatorProtocol))
                .Return(this.allocatee);

            this.hostingClientFactory.Stub(f => f(
                this.transportProtocol,
                this.hostingServerProtocol,
                this.engagementClient,
                this.peerConnectionNotifier,
                this.timeKeeper,
                this.queueApplicationEvent,
                this.generateBadumnaId))
                .Return(this.hostingClient);
            this.hostingServerFactory.Stub(f => f(
                this.transportProtocol,
                this.hostingClientProtocol,
                this.spatialEntityManager,
                this.networkFacade,
                this.timeKeeper,
                this.networkAddressProvider,
                this.queueApplicationEvent))
                .Return(this.hostingServer);
        }

        private void ConstructFacade()
        {
            this.facade = new Facade(
                this.validationModule,
                this.transportProtocol,
                this.dhtProtocol,
                this.eventScheduler,
                this.peerConnectionNotifier,
                this.networkAddressProvider,
                this.spatialEntityManager,
                this.networkFacade,
                this.timeKeeper,
                this.queueApplicationEvent,
                this.generateBadumnaId,
                this.serverLocator,
                this.engagementClientProtocol,
                this.engagementServerProtocol,
                this.engagementClientFactory,
                this.engagementServerFactory,
                this.allocatorProtocol,
                this.allocateeProtocol,
                this.allocatorFactory,
                this.allocateeFactory,
                this.hostingClientProtocol,
                this.hostingServerProtocol,
                this.hostingClientFactory,
                this.hostingServerFactory);
        }
    }
}
