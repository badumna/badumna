﻿//-----------------------------------------------------------------------
// <copyright file="SignedEntityStateTests.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BadumnaTests.Validation.HostingTests
{
    using System;
    using Badumna.Core;
    using Badumna.Utilities;
    using Badumna.Validation;
    using Badumna.Validation.Hosting;
    using NUnit.Framework;
    using Rhino.Mocks;

    [TestFixture]
    [DontPerformCoverage]
    public class SignedEntityStateTests
    {
        private IOrdered<ISerializedEntityState> serialEntityState;
        private byte[] signature;
        private SignedEntityState signedEntityState;

        [SetUp]
        public void SetUp()
        {
            this.serialEntityState = MockRepository.GenerateMock<IOrdered<ISerializedEntityState>>();
            this.signature = new byte[0];
        }

        [Test]
        public void ConstructorThrowsForNullSerialEntityState()
        {
            this.serialEntityState = null;
            Assert.Throws(typeof(ArgumentNullException), this.ConstructSignedEntityState);
        }

        [Test]
        public void ConstructorThrowsForNullSignature()
        {
            this.signature = null;
            Assert.Throws(typeof(ArgumentNullException), this.ConstructSignedEntityState);
        }

        [Test]
        public void SerialEntityStateReturnsSerialEntityState()
        {
            this.ConstructSignedEntityState();
            Assert.AreEqual(this.serialEntityState, this.signedEntityState.SerialEntityState);
        }

        [Test]
        public void VerifyReturnsTrue()
        {
            this.ConstructSignedEntityState();
            Assert.IsTrue(this.signedEntityState.Verify(null));
        }

        [TestCase(0, 0)]
        [TestCase(0, 45)]
        [TestCase(23, 0)]
        [TestCase(23, 45)]
        public void ToMessageFromMessagePreservesData(int serialNumber, int dataSize)
        {
            ushort sequenceNumber = (ushort)serialNumber;
            var flags = new BooleanArray(new int[] { 1, 3, 4, 9 });
            var data = new byte[dataSize];
            var signature = new byte[0];
            var serialEntityState = new Ordered<ISerializedEntityState>(
                new CyclicalID.UShortID(sequenceNumber),
                new SerializedEntityState(flags, data));
            var signedEntityState = new SignedEntityState(serialEntityState, signature);
            var message = new MessageBuffer();
            signedEntityState.ToMessage(message, typeof(SignedEntityState));
            var deserializedSignedEntityState = new SignedEntityState();
            deserializedSignedEntityState.FromMessage(message);
            Assert.AreEqual(
                signedEntityState.SerialEntityState.SerialNumber.Value,
                deserializedSignedEntityState.SerialEntityState.SerialNumber.Value);
            Assert.AreEqual(
                ((SerializedEntityState)signedEntityState.SerialEntityState.Item).Flags,
                ((SerializedEntityState)deserializedSignedEntityState.SerialEntityState.Item).Flags);
            Assert.AreEqual(
                ((SerializedEntityState)signedEntityState.SerialEntityState.Item).Data,
                ((SerializedEntityState)deserializedSignedEntityState.SerialEntityState.Item).Data);
        }

        private void ConstructSignedEntityState()
        {
            this.signedEntityState = new SignedEntityState(this.serialEntityState, this.signature);
        }
    }
}
