﻿//-----------------------------------------------------------------------
// <copyright file="ClientProtocolTests.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BadumnaTests.Validation.HostingTests
{
    using System;
    using Badumna.Core;
    using Badumna.Transport;
    using Badumna.Utilities;
    using Badumna.Validation;
    using Badumna.Validation.Hosting;
    using NUnit.Framework;
    using Rhino.Mocks;
    using TestUtilities;

    [TestFixture]
    [DontPerformCoverage]
    public class ClientProtocolTests
    {
        private ITransportProtocol transportProtocol;
        private PeerAddress source;
        private ClientProtocol clientProtocol;
        private IClient client;

        [SetUp]
        public void SetUp()
        {
            this.transportProtocol = MockRepository.GenerateMock<ITransportProtocol>();
            this.source = PeerAddress.GetRandomAddress();
            TransportEnvelope envelope = new TransportEnvelope(new MessageBuffer(), this.source, false, TimeSpan.Zero);
            this.transportProtocol.Stub(p => p.CurrentEnvelope).Return(envelope);
            this.clientProtocol = new ClientProtocol(this.transportProtocol);
            this.client = MockRepository.GenerateMock<IClient>();
            this.clientProtocol.Client = this.client;
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ConstructorThrowsForNullTransportProtocol()
        {
            var clientProtocol = new ClientProtocol(null);
        }

        [Test]
        public void ConstructorRegistersMethodsWithTransportProtocol()
        {
            // Arrange
            var cc = new CapturingConstraint<ClientProtocol>();
            
            // Expect
            this.transportProtocol.Expect(p => p.RegisterMethodsIn(Arg<ClientProtocol>.Matches(cc)));

            // Act
            var clientProtocol = new ClientProtocol(this.transportProtocol);

            // Assert
            this.transportProtocol.VerifyAllExpectations();
            Assert.AreEqual(clientProtocol, cc.Argument);
        }

        [Test]
        public void ReceiveEntityStateUpdatePassesOnToClient()
        {
            // Arrange
            ushort localId = 99;
            var update = new SerializedEntityState();
            var serialNumber = new CyclicalID.UShortID(7);

            // Expect
            this.client.Expect(a => a.ReceiveEntityStateUpdate(
                Arg.Is(this.source),
                Arg.Is(localId),
                Arg<IOrdered<ISerializedEntityState>>.Is.Anything));

            ////Matches(o => o.SerialNumber.Equals(serialNumber))

            // Act
            this.clientProtocol.ReceiveEntityStateUpdate(localId, serialNumber, update);

            // Verify
            this.client.VerifyAllExpectations();
        }

        [Test]
        public void ReceiveEntityStateMilestonePassesOnToClient()
        {
            // Arrange
            ushort localId = (ushort)new Random().Next(ushort.MaxValue);
            var serialNumber = new CyclicalID.UShortID(8);
            var milestone = new SerializedEntityState();
            var signature = new byte[0];

            // Expect
            this.client.Expect(a => a.ReceiveEntityStateMilestone(
                Arg.Is(this.source),
                Arg.Is(localId),
                Arg<ISignedEntityState>.Is.Anything));

            // Act
            this.clientProtocol.ReceiveEntityStateMilestone(localId, serialNumber, milestone, signature);

            // Verify
            this.client.VerifyAllExpectations();
        }

        [Test]
        public void ReceiveTerminationNoticePassesOnToClient()
        {
            // Expect
            this.client.Expect(c => c.ReceiveTerminationNotice(this.source));

            // Act
            this.clientProtocol.ReceiveTerminationNotice();

            // Verify
            this.client.VerifyAllExpectations();
        }

        [TestCase(true)]
        [TestCase(false)]
        public void ReceivePingReplyPassesToClient(bool reply)
        {
            // Act
            this.clientProtocol.ReceivePingReply(reply);

            // Assert
            this.client.AssertWasCalled(c => c.ReceivePingReply(this.source, reply));
        }

        [Test]
        public void ReceiveServerErrorPassesToClient()
        {
            // Arrange
            ushort localId = (ushort)new Random().Next(ushort.MaxValue);

            // Act
            this.clientProtocol.ReceiveServerError(localId);

            // Assert
            this.client.AssertWasCalled(c => c.ReceiveServerError(this.source, localId));
        }
    }
}
