﻿//-----------------------------------------------------------------------
// <copyright file="OrderedTests.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BadumnaTests.Validation.HostingTests
{
    using System;
    using Badumna.Core;
    using Badumna.Utilities;
    using Badumna.Validation.Hosting;
    using NUnit.Framework;
    using Rhino.Mocks;

    [TestFixture]
    [DontPerformCoverage]
    public class OrderedTests
    {
        private CyclicalID.UShortID serialNumber;
        private object item;
        private Ordered<object> serialItem;

        [SetUp]
        public void SetUp()
        {
            this.serialNumber = new CyclicalID.UShortID(7);
            this.item = new object();
        }

        [Test]
        public void ConstructorThrowsForItem()
        {
            this.item = null;
            Assert.Throws(typeof(ArgumentNullException), this.ConstructSerialTimedInput);
        }

        [Test]
        public void SerialNumberGetsSerialNumber()
        {
            this.ConstructSerialTimedInput();
            Assert.AreEqual(this.serialNumber, this.serialItem.SerialNumber);
        }

        [Test]
        public void ItemGetsItem()
        {
            this.ConstructSerialTimedInput();
            Assert.AreEqual(this.item, this.serialItem.Item);
        }

        [Test]
        public void IsNextThrowsForNullArgument()
        {
            this.ConstructSerialTimedInput();
            Assert.Throws(
                typeof(ArgumentNullException),
                () => this.serialItem.IsNext(null));
        }

        [TestCase(0, 1)]
        [TestCase(1, 2)]
        [TestCase(255, 256)]
        [TestCase(ushort.MaxValue, ushort.MinValue)]
        public void IsNextReturnsTrueForNextItem(int current, int next)
        {
            var currentOrderedItem = new Ordered<object>(new CyclicalID.UShortID((ushort)current), new object());
            var nextOrderedItem = new Ordered<object>(new CyclicalID.UShortID((ushort)next), new object());
            Assert.IsTrue(currentOrderedItem.IsNext(nextOrderedItem));
        }

        [TestCase(0, 2)]
        [TestCase(1, 0)]
        [TestCase(255, 345)]
        [TestCase(ushort.MinValue, ushort.MaxValue)]
        public void IsNextReturnsFalseForNonNextItem(int current, int next)
        {
            var currentOrderedItem = new Ordered<object>(new CyclicalID.UShortID((ushort)current), new object());
            var nextOrderedItem = new Ordered<object>(new CyclicalID.UShortID((ushort)next), new object());
            Assert.IsFalse(currentOrderedItem.IsNext(nextOrderedItem));
        }

        private void ConstructSerialTimedInput()
        {
            this.serialItem = new Ordered<object>(this.serialNumber, this.item);
        }
    }
}
