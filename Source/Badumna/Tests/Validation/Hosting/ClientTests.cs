﻿//-----------------------------------------------------------------------
// <copyright file="ClientTests.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BadumnaTests.Validation.HostingTests
{
    using System;
    using Badumna.Core;
    using Badumna.DataTypes;
    using Badumna.SpatialEntities;
    using Badumna.Transport;
    using Badumna.Utilities;
    using Badumna.Validation;
    using Badumna.Validation.Allocation;
    using Badumna.Validation.Hosting;
    using NUnit.Framework;
    using Rhino.Mocks;
    using TestUtilities;
    using Constraints = Rhino.Mocks.Constraints;
    using EngagementReplyHandler = Badumna.Validation.Engagement.EngagementReplyHandler;
    using IEngagementClient = Badumna.Validation.Engagement.IClient;

    [TestFixture]
    [DontPerformCoverage]
    public class ClientTests
    {
        private ITransportProtocol transportProtocol;
        private IServerProtocol serverProtocol;
        private IEngagementClient engagementClient;
        private IPeerConnectionNotifier peerConnectionNotifier;
        private TimeFaker timeKeeper;
        private QueueInvokable queueApplicationEvent;
        private LocalEntityWrapperFactory wrapperFactory;
        private IntervalSeriesFactory intervalSeriesFactory;
        private GenericCallBackReturn<BadumnaId> idFactory;
        private ILocalEntityWrapper wrapper;
        private IIntervalSeries intervalSeries;
        private IValidatedEntity entity;
        private ushort localId;
        private uint entityType;
        private BadumnaId badumnaId;

        private Client client;
        private PeerAddress validatorAddress;

        private CapturingConstraint<EngagementReplyHandler> engagementReplyHandlerCapturer;

        [SetUp]
        public void SetUp()
        {
            this.transportProtocol = MockRepository.GenerateMock<ITransportProtocol>();
            this.serverProtocol = MockRepository.GenerateMock<IServerProtocol>();
            this.engagementClient = MockRepository.GenerateMock<IEngagementClient>();
            this.peerConnectionNotifier = MockRepository.GenerateMock<IPeerConnectionNotifier>();
            this.timeKeeper = new TimeFaker();
            this.queueApplicationEvent = MockRepository.GenerateMock<QueueInvokable>();
            this.wrapperFactory = MockRepository.GenerateMock<LocalEntityWrapperFactory>();
            this.intervalSeriesFactory = MockRepository.GenerateMock<IntervalSeriesFactory>();
            this.idFactory = MockRepository.GenerateMock<GenericCallBackReturn<BadumnaId>>();
            this.wrapper = MockRepository.GenerateMock<ILocalEntityWrapper>();
            this.validatorAddress = PeerAddress.GetRandomAddress();
            this.entity = MockRepository.GenerateMock<IValidatedEntity>();
            this.entityType = (uint)new Random().Next(int.MaxValue);
            this.localId = (ushort)new Random().Next(ushort.MaxValue);
            this.badumnaId = new BadumnaId(PeerAddress.GetRandomAddress(), this.localId);
            this.idFactory.Stub(f => f.Invoke()).Return(this.badumnaId).Repeat.Once();
            this.intervalSeries = MockRepository.GenerateMock<IIntervalSeries>();
            this.intervalSeriesFactory.Stub(f => f.Invoke(0, this.timeKeeper))
                .IgnoreArguments()
                .Return(this.intervalSeries);
            this.engagementReplyHandlerCapturer = new CapturingConstraint<EngagementReplyHandler>();
        }

        [Test]
        public void ConstructorThrowsForNullTransportProtocol()
        {
            this.transportProtocol = null;
            Assert.Throws(typeof(ArgumentNullException), this.ConstructClient);
        }

        [Test]
        public void ConstructorThrowsForNullEngagementClient()
        {
            this.serverProtocol = null;
            Assert.Throws(typeof(ArgumentNullException), this.ConstructClient);
        }

        [Test]
        public void ConstructorThrowsForNullSessionFactory()
        {
            this.engagementClient = null;
            Assert.Throws(typeof(ArgumentNullException), this.ConstructClient);
        }

        [Test]
        public void ConstructorThrowsForNullPeerConnectionNotifier()
        {
            this.peerConnectionNotifier = null;
            Assert.Throws(typeof(ArgumentNullException), this.ConstructClient);
        }

        [Test]
        public void ConstructorThrowsForNullTimeKeeper()
        {
            this.timeKeeper = null;
            Assert.Throws(typeof(ArgumentNullException), this.ConstructClient);
        }

        [Test]
        public void ConstructorThrowsForNullWrapperFactory()
        {
            this.wrapperFactory = null;
            Assert.Throws(typeof(ArgumentNullException), this.ConstructClient);
        }

        [Test]
        public void ConstructorCreatesDefaultWrapperFactory()
        {
            // Test passes if it does not throw.
            this.client = new Client(
                this.transportProtocol,
                this.serverProtocol,
                this.engagementClient,
                this.peerConnectionNotifier,
                this.timeKeeper,
                this.queueApplicationEvent,
                this.idFactory);

            // Trigger invocation of factory method.
            this.client.RegisterValidatedEntity(this.entity, this.entityType, null);
        }

        [Test]
        public void RegisterValidatedEntityRequestsValidatorIfNoneAssignedOrPending()
        {
            // Arrange
            this.ConstructClient();

            // Expect
            this.engagementClient.Expect(c => c.RequestValidator(Arg<EngagementReplyHandler>.Is.Anything));

            // Act
            this.client.RegisterValidatedEntity(this.entity, this.entityType, null);

            // Verify
            this.engagementClient.VerifyAllExpectations();
        }

        [Test]
        public void RegisterValidatedEntityDoesNotRequestValidatorIfAlreadyAssigned()
        {
            // Arrange
            this.ConstructClient();
            this.ConstructClientAndCompleteEntityRegistration();
            ushort localId2 = this.localId;
            if (localId2 > 0)
            {
                localId2--;
            }
            else
            {
                localId2++;
            }

            BadumnaId id2 = new BadumnaId(
                this.badumnaId.Address,
                localId2);
            this.idFactory.Stub(f => f.Invoke()).Return(id2);
            IValidatedEntity entity2 = MockRepository.GenerateMock<IValidatedEntity>();

            // Expect
            this.engagementClient.Expect(c => c.RequestValidator(Arg<EngagementReplyHandler>.Is.Anything))
                .Repeat.Never();

            // Act
            this.client.RegisterValidatedEntity(entity2, this.entityType, null);

            // Verify
            this.engagementClient.VerifyAllExpectations();
        }

        [Test]
        public void RegisterValidatedEntityThrowsForRegisteredId()
        {
            // Arrange
            this.ConstructClient();
            this.client.RegisterValidatedEntity(this.entity, this.entityType, null);

            // Act, Assert
            Assert.Throws(
                typeof(ArgumentException),
                () => this.client.RegisterValidatedEntity(this.entity, this.entityType, null));
        }

        [Test]
        public void RegisterValidatedEntityDoesNotRequestValidatorIfRequestPending()
        {
            // Arrange
            this.ConstructClient();
            this.client.RegisterValidatedEntity(this.entity, this.entityType, null);
            ushort localId2 = this.localId;
            if (localId2 > 0)
            {
                localId2--;
            }
            else
            {
                localId2++;
            }

            BadumnaId id2 = new BadumnaId(
                this.badumnaId.Address,
                localId2);
            this.idFactory.Stub(f => f.Invoke()).Return(id2);
            IValidatedEntity entity2 = MockRepository.GenerateMock<IValidatedEntity>();

            // Expect
            this.engagementClient.Expect(c => c.RequestValidator(Arg<EngagementReplyHandler>.Is.Anything))
                .Repeat.Never();

            // Act
            this.client.RegisterValidatedEntity(entity2, this.entityType, null);

            // Verify
            this.engagementClient.VerifyAllExpectations();
        }

        [Test]
        public void RegisterValidatedEntityCreatesWrapper()
        {
            // Arrange
            this.ConstructClient();

            // Expect
            this.wrapperFactory.Expect(f => f.Invoke(
                Arg.Is(this.transportProtocol),
                Arg.Is(this.serverProtocol),
                Arg.Is(this.entity),
                Arg.Is(this.entityType),
                Arg<BadumnaId>.Is.Anything,
                Arg<ISerializedEntityState>.Is.Anything,
                Arg<PeerAddress>.Is.Equal(null)));

            // Act
            this.client.RegisterValidatedEntity(this.entity, this.entityType, null);

            // Verify
            this.wrapperFactory.VerifyAllExpectations();
        }

        [Test]
        public void UnregisterValidatedEntityThrowsForUnknownId()
        {
            // Arrange
            this.ConstructClient();

            // Act + Assert
            Assert.Throws(
                typeof(ArgumentException),
                () => this.client.UnregisterValidatedEntity(this.entity));
        }

        [Test]
        public void UnregisterValidatedEntityRemovesWrapperForKnownId()
        {
            // Arrange
            this.ConstructClientAndCompleteEntityRegistration();

            // Act
            this.client.UnregisterValidatedEntity(this.entity);

            // Assert (by checking that repeat UnregsterValidatedEntity call throws).
            Assert.Throws(
                typeof(ArgumentException),
                () => this.client.UnregisterValidatedEntity(this.entity));
        }

        [Test]
        public void UnregisterValidatedEntitySendsRequestToValidatorWhenKnownIdAndValidatorAssigned()
        {
            // Arrange
            this.ConstructClientAndCompleteEntityRegistration();

            // Expect
            this.transportProtocol.Expect(p => p.SendRemoteCall(
                this.validatorAddress,
                QualityOfService.Reliable,
                this.serverProtocol.ReceiveEntityUnregistrationRequest,
                this.localId));

            // Act
            this.client.UnregisterValidatedEntity(this.entity);

            // Assert (by checking that repeat UnregsterValidatedEntity call throws).
            this.transportProtocol.VerifyAllExpectations();
        }

        [Test]
        public void ReceiveEntityStateMilestonePassesInitialStateFromAllocatedValidatorToEntityWrapper()
        {
            // Arrange
            this.ConstructClientAndCompleteEntityRegistration();
            var milestone = MockRepository.GenerateMock<ISignedEntityState>();

            // Expect
            ////this.eventScheduler.Expect(q => q.Push(wrapper.ReceiveMilestone, milestone))
            ////    .Return(null);
            this.queueApplicationEvent.Expect(q => q(null))
                .IgnoreArguments();

            // Act
            this.client.ReceiveEntityStateMilestone(this.validatorAddress, this.localId, milestone);

            // Verify
            this.queueApplicationEvent.VerifyAllExpectations();
        }

        [Test]
        public void ReceiveEntityStateMilestoneDoesNotPassInitialStateFromUnallocatedValidatorToEntityWrapper()
        {
            // Arrange
            this.ConstructClientAndCompleteEntityRegistration();
            var milestone = MockRepository.GenerateMock<ISignedEntityState>();

            // Expect
            ////this.eventScheduler.Expect(q => q.Push(wrapper.ReceiveMilestone, milestone))
            ////    .Repeat.Never();
            this.queueApplicationEvent.Expect(q => q(null))
                .IgnoreArguments()
                .Repeat.Never();

            // Act
            this.client.ReceiveEntityStateMilestone(PeerAddress.GetRandomAddress(), this.localId, milestone);

            // Verify
            this.queueApplicationEvent.VerifyAllExpectations();
        }

        [Test]
        public void ReceiveEntityStateUpdatePassesInitialStateFromAllocatedValidatorToEntityWrapper()
        {
            // Arrange
            this.ConstructClientAndCompleteEntityRegistration();
            var stateUpdate = MockRepository.GenerateMock<IOrdered<ISerializedEntityState>>();

            // Expect
            ////this.eventScheduler.Expect(q => q.Push(wrapper.ReceiveUpdate, stateUpdate))
            ////    .Return(null);
            this.queueApplicationEvent.Expect(q => q(null))
                .IgnoreArguments();

            // Act
            this.client.ReceiveEntityStateUpdate(this.validatorAddress, this.localId, stateUpdate);

            // Verify
            this.queueApplicationEvent.VerifyAllExpectations();
        }

        [Test]
        public void ReceiveEntityStateUpdateDoesNotPassInitialStateFromNonAllocatedValidatorToEntityWrapper()
        {
            // Arrange
            this.ConstructClientAndCompleteEntityRegistration();
            var stateUpdate = MockRepository.GenerateMock<IOrdered<ISerializedEntityState>>();

            // Expect
            ////this.eventScheduler.Expect(q => q.Push(wrapper.ReceiveUpdate, stateUpdate)).Repeat.Never();
            this.queueApplicationEvent.Expect(q => q(null))
                .IgnoreArguments()
                .Repeat.Never();

            // Act
            this.client.ReceiveEntityStateUpdate(PeerAddress.GetRandomAddress(), this.localId, stateUpdate);

            // Verify
            this.queueApplicationEvent.VerifyAllExpectations();
        }

        [Test]
        public void HandleAllocationReplyPositivePassesAllocatorAddressToWrappers()
        {
            var capturedApplicationEvent = new CapturingConstraint<IInvokable>();
            this.queueApplicationEvent.Stub(
                q => q(Arg<IInvokable>.Matches(capturedApplicationEvent)));
            this.ConstructClientAndBeginEntityRegistration();
            PeerAddress validatorAddress = PeerAddress.GetRandomAddress();

            // Expect
            this.wrapper.Expect(w => w.OnValidatorAllocation(validatorAddress));

            // Act
            this.engagementReplyHandlerCapturer.Argument.Invoke(
                EngagementRequestResult.Success,
                validatorAddress.ToString());
            capturedApplicationEvent.Argument.Invoke();

            // Verify
            this.wrapper.VerifyAllExpectations();
        }

        [Test]
        public void HandleAllocationReplyPositiveTriggersLiveEventIfNoValidatorAssigned()
        {
            this.ConstructClientAndBeginEntityRegistration();
            var stateUpdate = MockRepository.GenerateMock<IOrdered<ISerializedEntityState>>();
            PeerAddress validatorAddress = PeerAddress.GetRandomAddress();
            var onlineHandler = MockRepository.GenerateMock<EventHandler<ValidatorOnlineEventArgs>>();
            this.client.ValidatorOnline += onlineHandler;

            // Expect
            ////onlineHandler.Expect(h => h.Invoke(Arg.Is(this.client), Arg<ValidatorOnlineEventArgs>.Is.Anything));
            this.queueApplicationEvent.Expect(q => q(null))
                .IgnoreArguments();

            // Act
            this.engagementReplyHandlerCapturer.Argument.Invoke(
                EngagementRequestResult.Success,
                validatorAddress.ToString());

            // Verify
            this.queueApplicationEvent.VerifyAllExpectations();
        }

        [Test]
        public void HandleAllocationReplyPositiveDoesNotTriggerOnlineEventWhenValidatorAlreadyAssigned()
        {
            // Arrange
            this.ConstructClientAndCompleteEntityRegistration();
            var cc = new CapturingConstraint<EngagementReplyHandler>();
            this.engagementClient.Stub(c => c.RequestValidator(Arg<EngagementReplyHandler>.Matches(cc)))
                .Repeat.Once();
            this.client.RegularProcessing();
            PeerAddress backupAddress = PeerAddress.GetRandomAddress();
            this.timeKeeper.Now = TimeSpan.Zero;
            var onlineHandler = MockRepository.GenerateMock<EventHandler<ValidatorOnlineEventArgs>>();
            this.client.ValidatorOnline += onlineHandler;

            // Expect - test backup assignment by expecting future ping.
            onlineHandler.Expect(h => h.Invoke(Arg.Is(this.client), Arg<ValidatorOnlineEventArgs>.Is.Anything))
                .Repeat.Never();

            // Act
            cc.Argument.Invoke(EngagementRequestResult.Success, backupAddress.ToString());

            // Verify
            onlineHandler.VerifyAllExpectations();
        }

        [Test]
        public void HandleAllocationReplyNegativeNotifiesWrappersOfFailure()
        {
            this.ConstructClientAndBeginEntityRegistration();
            PeerAddress validatorAddress = PeerAddress.GetRandomAddress();

            // Expect
            ////this.eventScheduler.Expect(q => q.Push(wrapper.OnValidationFailure))
            ////    .Return(null);
            this.queueApplicationEvent.Expect(q => q(null))
                .IgnoreArguments();

            // Act
            this.engagementReplyHandlerCapturer.Argument.Invoke(
                EngagementRequestResult.Failure,
                validatorAddress.ToString());

            // Verify
            this.queueApplicationEvent.VerifyAllExpectations();
        }

        [TestCase(1, 1)] // Failure -> no validator available
        [TestCase(2, 0)] // Timeout -> server unreachable
        public void HandleAllocationReplyNegativeTriggersFailureEventIfEntitiesRegistered(
            int engagementResult,
            int failureReason)
        {
            EngagementRequestResult result = (EngagementRequestResult)engagementResult;
            ValidationFailureReason reason = (ValidationFailureReason)failureReason;
            this.ConstructClientAndBeginEntityRegistration();
            PeerAddress validatorAddress = PeerAddress.GetRandomAddress();
            var handler = MockRepository.GenerateMock<EventHandler<ValidationFailureEventArgs>>();
            this.client.ValidationFailure += handler;

            // Expect
            ////var constraint = new Constraints.PropertyConstraint(
            ////    "Reason",
            ////    Constraints.Is.Equal(reason));            
            ////handler.Expect(h => h.Invoke(
            ////    Arg<object>.Is.Equal(this.client),
            ////    Arg<ValidationFailureEventArgs>.Matches(constraint)));
            this.queueApplicationEvent.Expect(q => q(null))
                .IgnoreArguments();

            // Act
            this.engagementReplyHandlerCapturer.Argument.Invoke(
                result,
                validatorAddress.ToString());

            // Verify
            this.queueApplicationEvent.VerifyAllExpectations();
        }

        [TestCase(1, 1)] // Failure -> no validator available
        [TestCase(2, 0)] // Timeout -> server unreachable
        public void HandleAllocationReplyNegativeIgnoredIfNoEntitiesRegistered(
            int engagementResult,
            int failureReason)
        {
            EngagementRequestResult result = (EngagementRequestResult)engagementResult;
            ValidationFailureReason reason = (ValidationFailureReason)failureReason;
            this.ConstructClientAndBeginEntityRegistration();
            this.client.UnregisterValidatedEntity(this.entity);
            PeerAddress validatorAddress = PeerAddress.GetRandomAddress();
            var handler = MockRepository.GenerateMock<EventHandler<ValidationFailureEventArgs>>();
            this.client.ValidationFailure += handler;

            // Expect
            var constraint = new Constraints.PropertyConstraint(
                "Reason",
                Constraints.Is.Equal(reason));
            handler.Expect(h => h.Invoke(Arg<object>.Is.Anything, Arg<ValidationFailureEventArgs>.Is.Anything))
                .Repeat.Never();

            // Act
            this.engagementReplyHandlerCapturer.Argument.Invoke(
                result,
                validatorAddress.ToString());

            // Verify
            handler.VerifyAllExpectations();
        }

        [TestCase(1, 1)] // Failure -> no validator available
        [TestCase(2, 0)] // Timeout -> server unreachable
        public void HandleAllocationReplyNegativeTriggersValidatorRequestIfNoValidatorKnownButEntitiesRegistered(
            int engagementResult,
            int failureReason)
        {
            EngagementRequestResult result = (EngagementRequestResult)engagementResult;
            ValidationFailureReason reason = (ValidationFailureReason)failureReason;
            this.ConstructClientAndBeginEntityRegistration();
            var handler = MockRepository.GenerateMock<EventHandler<ValidationFailureEventArgs>>();
            this.client.ValidationFailure += handler;

            // Expect
            this.engagementClient.Expect(c => c.RequestValidator(this.engagementReplyHandlerCapturer.Argument));

            // Act
            this.engagementReplyHandlerCapturer.Argument.Invoke(result, string.Empty);

            // Verify
            this.engagementClient.VerifyAllExpectations();
        }

        [Test]
        public void OnConnectionLostNotifiesWrapperForCurrentValidator()
        {
            var capturedOnConnectionLost = new CapturingConstraint<ConnectionHandler>();
            this.peerConnectionNotifier.Stub(
                n => n.ConnectionLostEvent += Arg<ConnectionHandler>.Matches(capturedOnConnectionLost));
            this.ConstructClientAndCompleteEntityRegistration();

            // Expect
            ////this.eventScheduler.Expect(q => q.Push(wrapper.OnValidatorLost, validatorAddress))
            ////    .Return(null);
            this.queueApplicationEvent.Expect(q => q(null))
                .IgnoreArguments();

            // Act
            capturedOnConnectionLost.Argument.Invoke(this.validatorAddress);

            // Verify
            this.queueApplicationEvent.VerifyAllExpectations();
        }

        [Test]
        public void OnConnectionLostForCurrentValidatorTriggersAllocationRequest()
        {
            var capturedOnConnectionLost = new CapturingConstraint<ConnectionHandler>();
            this.peerConnectionNotifier.Stub(
                n => n.ConnectionLostEvent += Arg<ConnectionHandler>.Matches(capturedOnConnectionLost));
            this.ConstructClientAndCompleteEntityRegistration();

            // Expect
            this.engagementClient.Expect(c => c.RequestValidator(Arg<EngagementReplyHandler>.Is.Anything))
                .IgnoreArguments();

            // Act
            capturedOnConnectionLost.Argument.Invoke(this.validatorAddress);

            // Verify
            this.engagementClient.VerifyAllExpectations();
        }

        [Test]
        public void OnConnectionLostDoesNotNotifyWrapperForNonCurrentValidator()
        {
            var capturedOnConnectionLost = new CapturingConstraint<ConnectionHandler>();
            this.peerConnectionNotifier.Stub(
                n => n.ConnectionLostEvent += Arg<ConnectionHandler>.Matches(capturedOnConnectionLost));
            this.ConstructClientAndCompleteEntityRegistration();
            PeerAddress randomAddress = PeerAddress.GetRandomAddress();
            while (randomAddress.Equals(this.validatorAddress))
            {
                randomAddress = PeerAddress.GetRandomAddress();
            }

            // Expect
            ////this.eventScheduler.Expect(q => q.Push(wrapper.OnValidatorLost, randomAddress))
            ////    .IgnoreArguments()
            ////    .Repeat.Never();
            this.queueApplicationEvent.Expect(q => q(null))
                .IgnoreArguments()
                .Repeat.Never();

            // Act
            capturedOnConnectionLost.Argument.Invoke(randomAddress);

            // Verify
            this.queueApplicationEvent.VerifyAllExpectations();
        }

        [Test]
        public void OnConnectionLostForNonCurrentValidatorTriggersAllocationRequest()
        {
            // Arrange
            var capturedOnConnectionLost = new CapturingConstraint<ConnectionHandler>();
            this.peerConnectionNotifier.Stub(
                n => n.ConnectionLostEvent += Arg<ConnectionHandler>.Matches(capturedOnConnectionLost));
            this.ConstructClientAndCompleteEntityRegistration();
            PeerAddress randomAddress = PeerAddress.GetRandomAddress();
            while (randomAddress.Equals(this.validatorAddress))
            {
                randomAddress = PeerAddress.GetRandomAddress();
            }

            // Expect
            this.engagementClient.Expect(c => c.RequestValidator(Arg<EngagementReplyHandler>.Is.Anything))
                .Repeat.Never();

            // Act
            capturedOnConnectionLost.Argument.Invoke(randomAddress);

            // Verify
            this.engagementClient.VerifyAllExpectations();
        }

        [Test]
        public void UpdateEntityThrowsForUnknownId()
        {
            // Arrange
            this.ConstructClient();
            var interval = TimeSpan.FromTicks(1000);
            var input = new byte[0];

            // Act, Assert
            Assert.Throws(
                typeof(InvalidOperationException),
                () => this.client.UpdateEntity(this.entity, interval, input));
        }

        [Test]
        public void UpdateEntityPassesUpdateToWrapper()
        {
            // Arrange
            this.ConstructClientAndBeginEntityRegistration();
            var interval = TimeSpan.FromTicks(1000);
            var input = new byte[0];

            // Expect
            ////this.eventScheduler.Expect(q => q.Push(wrapper.UpdateEntity, interval, input))
            ////    .Return(null);
            this.queueApplicationEvent.Expect(q => q(null))
                .IgnoreArguments();

            // Act
            this.client.UpdateEntity(this.entity, interval, input);

            // Verify
            this.queueApplicationEvent.VerifyAllExpectations();
        }

        [Test]
        public void RegisterEntityWithSceneThrowsForUnknownEntityID()
        {
            // Arrange
            this.ConstructClient();
            var scene = MockRepository.GenerateMock<NetworkScene>(
                new QualifiedName("", Guid.NewGuid().ToString()),
                null,
                null,
                null,
                new NetworkEventQueue(),
                this.queueApplicationEvent,
                false);

            // Act + Assert
            Assert.Throws(
                typeof(InvalidOperationException),
                () => this.client.RegisterEntityWithScene(this.entity, scene));
        }

        [Test]
        public void RegisterEntityWithScenePassesRequestToWrapper()
        {
            // Arrange
            this.ConstructClientAndBeginEntityRegistration();
            var scene = MockRepository.GenerateMock<NetworkScene>(
                new QualifiedName("", Guid.NewGuid().ToString()),
                null,
                null,
                null,
                new NetworkEventQueue(),
                this.queueApplicationEvent,
                false);

            // Expect
            this.wrapper.Expect(w => w.RegisterEntityWithScene(scene));

            // Act
            this.client.RegisterEntityWithScene(this.entity, scene);

            // Verify
            this.wrapper.VerifyAllExpectations();
        }

        [Test]
        public void UnregisterEntityFromSceneThrowsForUnknownEntityID()
        {
            // Arrange
            this.ConstructClient();

            // Act + Assert
            Assert.Throws(
                typeof(InvalidOperationException),
                () => this.client.UnregisterEntityFromScene(this.entity));
        }

        [Test]
        public void UnregisterEntityFromScenePassesRequestToWrapper()
        {
            // Arrange
            this.ConstructClientAndBeginEntityRegistration();

            // Expect
            this.wrapper.Expect(w => w.UnregisterEntityFromScene());

            // Act
            this.client.UnregisterEntityFromScene(this.entity);

            // Verify
            this.wrapper.VerifyAllExpectations();
        }

        [Test]
        public void ShutdownSendsTerminationNoticeToServer()
        {
            // Arrange
            this.ConstructClientAndCompleteEntityRegistration();

            // Expect
            this.transportProtocol.Expect(p => p.SendRemoteCall(
                this.validatorAddress,
                QualityOfService.Reliable,
                this.serverProtocol.ReceiveTerminationNotice));

            // Act
            this.client.Shutdown();

            // Verify
            this.transportProtocol.VerifyAllExpectations();
        }

        [Test]
        public void ReceiveTerminationNoticeFromValidatorNotifiesWrapper()
        {
            // Arrange
            this.ConstructClientAndCompleteEntityRegistration();

            // Expect
            ////this.eventScheduler.Expect(q => q.Push(this.wrapper.OnValidatorLost, this.validatorAddress))
            ////    .Return(null);
            this.queueApplicationEvent.Expect(q => q(null))
                .IgnoreArguments();

            // Act
            this.client.ReceiveTerminationNotice(this.validatorAddress);

            // Verify
            this.queueApplicationEvent.VerifyAllExpectations();
        }

        [Test]
        public void ReceiveTerminationNoticeFromNonValidatorDoesNotNotifyWrapper()
        {
            // Arrange
            this.ConstructClientAndCompleteEntityRegistration();

            // Expect
            ////this.eventScheduler.Expect(q => q.Push(this.wrapper.OnValidatorLost, this.validatorAddress))
            ////    .Repeat.Never();
            this.queueApplicationEvent.Expect(q => q(null))
                .IgnoreArguments()
                .Repeat.Never();

            // Act
            this.client.ReceiveTerminationNotice(PeerAddress.GetRandomAddress());

            // Verify
            this.queueApplicationEvent.VerifyAllExpectations();
        }

        [Test]
        public void ReceiveServerErrorFromNonValidatorDoesNotNotifyWrapper()
        {
            // Arrange
            this.ConstructClientAndCompleteEntityRegistration();

            // Expect
            ////this.eventScheduler.Expect(q => q.Push(this.wrapper.OnValidatorLost, this.validatorAddress))
            ////    .Repeat.Never();
            this.queueApplicationEvent.Expect(q => q(null))
                .IgnoreArguments()
                .Repeat.Never();

            // Act
            this.client.ReceiveServerError(PeerAddress.GetRandomAddress(), this.localId);

            // Verify
            this.queueApplicationEvent.VerifyAllExpectations();
        }

        [Test]
        public void ReceiveServerErrorFromValidatorNotifiesWrapper()
        {
            // Arrange
            this.ConstructClientAndCompleteEntityRegistration();

            // Expect
            ////this.eventScheduler.Expect(q => q.Push(this.wrapper.OnValidatorLost, this.validatorAddress))
            ////    .Return(null);
            ////this.eventScheduler.Expect(q => q.Push(this.wrapper.OnValidatorAllocation, this.validatorAddress))
            ////    .Return(null);
            this.queueApplicationEvent.Expect(q => q(null))
                .IgnoreArguments()
                .Repeat.Twice();

            // Act
            this.client.ReceiveServerError(this.validatorAddress, this.localId);

            // Verify
            this.queueApplicationEvent.VerifyAllExpectations();
        }

        [Test]
        public void RegularProcessingDoesNotRequestBackupWhenNoValidatorAssigned()
        {
            // Arrange
            this.ConstructClient();

            // Expect
            this.engagementClient.Expect(c => c.RequestValidator(null))
                .IgnoreArguments()
                .Repeat.Never();

            // Act
            this.client.RegularProcessing();

            // Verify
            this.engagementClient.VerifyAllExpectations();
        }

        [Test]
        public void RegularProcessingRequestsBackupWhenValidatorAssignedButNoBackups()
        {
            // Arrange
            this.ConstructClientAndCompleteEntityRegistration();

            // Expect
            this.engagementClient.Expect(c => c.RequestValidator(null))
                .IgnoreArguments();

            // Act
            this.client.RegularProcessing();

            // Verify
            this.engagementClient.VerifyAllExpectations();
        }

        [Test]
        public void RegularProcessingDoesNotRequestBackupWhenBackupsFullyAssigned()
        {
            // Arrange
            this.ConstructClientAndCompleteEntityRegistration();
            var cc = new CapturingConstraint<EngagementReplyHandler>();
            this.engagementClient.Stub(c => c.RequestValidator(Arg<EngagementReplyHandler>.Matches(cc)))
                .Repeat.Once();
            this.client.RegularProcessing();
            PeerAddress backupAddress = PeerAddress.GetRandomAddress();
            cc.Argument.Invoke(EngagementRequestResult.Success, backupAddress.ToString());

            // Expect
            this.engagementClient.Expect(c => c.RequestValidator(null))
                .IgnoreArguments()
                .Repeat.Never();

            // Act
            this.client.RegularProcessing();

            // Verify
            this.engagementClient.VerifyAllExpectations();
        }

        [Test]
        public void RegularProcessingDoesNotRequestBackupWhenRequestInProgress()
        {
            // Arrange
            this.ConstructClientAndCompleteEntityRegistration();
            var cc = new CapturingConstraint<EngagementReplyHandler>();
            this.engagementClient.Stub(c => c.RequestValidator(null))
                .IgnoreArguments()
                .Repeat.Once();
            this.client.RegularProcessing();

            // Expect
            this.engagementClient.Expect(c => c.RequestValidator(null))
                .IgnoreArguments()
                .Repeat.Never();

            // Act
            this.client.RegularProcessing();

            // Verify
            this.engagementClient.VerifyAllExpectations();
        }

        [Test]
        public void RegularProcessingDoesNotPingBackupsBeforeSchedule()
        {
            // Arrange
            this.ConstructClientAndCompleteEntityRegistration();
            var cc = new CapturingConstraint<EngagementReplyHandler>();
            this.engagementClient.Stub(c => c.RequestValidator(Arg<EngagementReplyHandler>.Matches(cc)))
                .Repeat.Once();
            this.client.RegularProcessing();
            PeerAddress backupAddress = PeerAddress.GetRandomAddress();
            this.timeKeeper.Now = TimeSpan.Zero;
            cc.Argument.Invoke(EngagementRequestResult.Success, backupAddress.ToString());
            this.timeKeeper.Now = TimeSpan.FromSeconds(9.99);

            // Expect
            this.transportProtocol.Expect(p => p.SendRemoteCall(
                backupAddress,
                QualityOfService.Reliable,
                this.serverProtocol.ReceivePingRequest))
                .Repeat.Never();

            // Act
            this.client.RegularProcessing();

            // Verify
            this.transportProtocol.VerifyAllExpectations();
        }

        [Test]
        public void RegularProcessingPingsBackupOnSchedule()
        {
            // Arrange
            this.ConstructClientAndCompleteEntityRegistration();
            var cc = new CapturingConstraint<EngagementReplyHandler>();
            this.engagementClient.Stub(c => c.RequestValidator(Arg<EngagementReplyHandler>.Matches(cc)))
                .Repeat.Once();
            this.client.RegularProcessing();
            PeerAddress backupAddress = PeerAddress.GetRandomAddress();
            this.timeKeeper.Now = TimeSpan.Zero;
            cc.Argument.Invoke(EngagementRequestResult.Success, backupAddress.ToString());
            this.timeKeeper.Now = TimeSpan.FromSeconds(10.01);

            // Expect
            this.transportProtocol.Expect(p => p.SendRemoteCall(
                backupAddress,
                QualityOfService.Reliable,
                this.serverProtocol.ReceivePingRequest));

            // Act
            this.client.RegularProcessing();

            // Verify
            this.transportProtocol.VerifyAllExpectations();
        }

        [Test]
        public void RegularProcessingDoesNotPingBackupWithOutstandingPing()
        {
            // Arrange
            this.ConstructClientAndCompleteEntityRegistration();
            var cc = new CapturingConstraint<EngagementReplyHandler>();
            this.engagementClient.Stub(c => c.RequestValidator(Arg<EngagementReplyHandler>.Matches(cc)))
                .Repeat.Once();
            this.client.RegularProcessing();
            PeerAddress backupAddress = PeerAddress.GetRandomAddress();
            this.timeKeeper.Now = TimeSpan.Zero;
            cc.Argument.Invoke(EngagementRequestResult.Success, backupAddress.ToString());
            this.timeKeeper.Now = TimeSpan.FromSeconds(10.01);
            this.client.RegularProcessing();
            this.timeKeeper.Now = TimeSpan.FromSeconds(30);

            // Expect
            this.transportProtocol.Expect(p => p.SendRemoteCall(
                backupAddress,
                QualityOfService.Reliable,
                this.serverProtocol.ReceivePingRequest))
                .Repeat.Never();

            // Act
            this.client.RegularProcessing();

            // Verify
            this.transportProtocol.VerifyAllExpectations();
        }

        [Test]
        public void OnConnectionLostForCurrentBackupRemovesBackup()
        {
            // Arrange
            var capturedOnConnectionLost = new CapturingConstraint<ConnectionHandler>();
            this.peerConnectionNotifier.Stub(
                n => n.ConnectionLostEvent += Arg<ConnectionHandler>.Matches(capturedOnConnectionLost));
            this.ConstructClientAndCompleteEntityRegistration();
            var cc = new CapturingConstraint<EngagementReplyHandler>();
            this.engagementClient.Stub(c => c.RequestValidator(Arg<EngagementReplyHandler>.Matches(cc)))
                .Repeat.Once();
            this.client.RegularProcessing();
            PeerAddress backupAddress = PeerAddress.GetRandomAddress();
            cc.Argument.Invoke(EngagementRequestResult.Success, backupAddress.ToString());

            // Expect
            this.engagementClient.Expect(c => c.RequestValidator(null))
                .IgnoreArguments();

            // Act
            capturedOnConnectionLost.Argument.Invoke(backupAddress);
            this.client.RegularProcessing();

            // Verify
            this.engagementClient.VerifyAllExpectations();
        }

        [Test]
        public void ReceivePingReplyWithNegativeForKnownBackupRemovesBackup()
        {
            // Arrange
            var capturedOnConnectionLost = new CapturingConstraint<ConnectionHandler>();
            this.peerConnectionNotifier.Stub(
                n => n.ConnectionLostEvent += Arg<ConnectionHandler>.Matches(capturedOnConnectionLost));
            this.ConstructClientAndCompleteEntityRegistration();
            var cc = new CapturingConstraint<EngagementReplyHandler>();
            this.engagementClient.Stub(c => c.RequestValidator(Arg<EngagementReplyHandler>.Matches(cc)))
                .Repeat.Once();
            this.client.RegularProcessing();
            PeerAddress backupAddress = PeerAddress.GetRandomAddress();
            cc.Argument.Invoke(EngagementRequestResult.Success, backupAddress.ToString());

            // Expect (if backup removed, regular processing will trigger new request).
            this.engagementClient.Expect(c => c.RequestValidator(null))
                .IgnoreArguments();

            // Act
            this.client.ReceivePingReply(backupAddress, false);
            this.client.RegularProcessing();

            // Verify
            this.engagementClient.VerifyAllExpectations();
        }

        [Test]
        public void ReceivePingReplyWithNegativeForUnknownBackupIgnored()
        {
            // Arrange
            var capturedOnConnectionLost = new CapturingConstraint<ConnectionHandler>();
            this.peerConnectionNotifier.Stub(
                n => n.ConnectionLostEvent += Arg<ConnectionHandler>.Matches(capturedOnConnectionLost));
            this.ConstructClientAndCompleteEntityRegistration();
            var cc = new CapturingConstraint<EngagementReplyHandler>();
            this.engagementClient.Stub(c => c.RequestValidator(Arg<EngagementReplyHandler>.Matches(cc)))
                .Repeat.Once();
            this.client.RegularProcessing();
            PeerAddress backupAddress = PeerAddress.GetRandomAddress();
            cc.Argument.Invoke(EngagementRequestResult.Success, backupAddress.ToString());

            // Expect (if no backups removed, regular processing will NOT trigger a new request).
            this.engagementClient.Expect(c => c.RequestValidator(null))
                .IgnoreArguments()
                .Repeat.Never();

            // Act
            this.client.ReceivePingReply(PeerAddress.GetRandomAddress(), false);
            this.client.RegularProcessing();

            // Verify
            this.engagementClient.VerifyAllExpectations();
        }

        [Test]
        public void ReceivePingReplyWithPositiveForKnownBackupReschedulesNextPing()
        {
            // Arrange
            this.ConstructClientAndCompleteEntityRegistration();
            var cc = new CapturingConstraint<EngagementReplyHandler>();
            this.engagementClient.Stub(c => c.RequestValidator(Arg<EngagementReplyHandler>.Matches(cc)))
                .Repeat.Once();
            this.client.RegularProcessing();
            PeerAddress backupAddress = PeerAddress.GetRandomAddress();
            this.timeKeeper.Now = TimeSpan.Zero;
            cc.Argument.Invoke(EngagementRequestResult.Success, backupAddress.ToString());
            this.timeKeeper.Now = TimeSpan.FromSeconds(10.01);
            this.client.RegularProcessing();

            // Expect
            this.transportProtocol.Expect(p => p.SendRemoteCall(
                backupAddress,
                QualityOfService.Reliable,
                this.serverProtocol.ReceivePingRequest));

            // Act
            this.client.ReceivePingReply(backupAddress, true);
            this.timeKeeper.Now = TimeSpan.FromSeconds(20.02);
            this.client.RegularProcessing();

            // Verify
            this.transportProtocol.VerifyAllExpectations();
        }

        [Test]
        public void ReceivePingReplyWithPositiveForUnknownBackupIgnored()
        {
            // Arrange
            this.ConstructClientAndCompleteEntityRegistration();
            var cc = new CapturingConstraint<EngagementReplyHandler>();
            this.engagementClient.Stub(c => c.RequestValidator(Arg<EngagementReplyHandler>.Matches(cc)))
                .Repeat.Once();
            this.client.RegularProcessing();
            PeerAddress backupAddress = PeerAddress.GetRandomAddress();
            this.timeKeeper.Now = TimeSpan.Zero;
            cc.Argument.Invoke(EngagementRequestResult.Success, backupAddress.ToString());
            this.timeKeeper.Now = TimeSpan.FromSeconds(10.01);
            this.client.RegularProcessing();

            // Expect - no ping rescheduled, as reply not received.
            this.transportProtocol.Expect(p => p.SendRemoteCall(
                backupAddress,
                QualityOfService.Reliable,
                this.serverProtocol.ReceivePingRequest))
                .Repeat.Never();

            // Act
            this.client.ReceivePingReply(PeerAddress.GetRandomAddress(), true);
            this.timeKeeper.Now = TimeSpan.FromSeconds(20.02);
            this.client.RegularProcessing();

            // Verify
            this.transportProtocol.VerifyAllExpectations();
        }

        [Test]
        public void HandleAllocationReplyPositiveSavedAsBackupsWhenValidatorAlreadyAssigned()
        {
            // Arrange
            this.ConstructClientAndCompleteEntityRegistration();
            var cc = new CapturingConstraint<EngagementReplyHandler>();
            this.engagementClient.Stub(c => c.RequestValidator(Arg<EngagementReplyHandler>.Matches(cc)))
                .Repeat.Once();
            this.client.RegularProcessing();
            PeerAddress backupAddress = PeerAddress.GetRandomAddress();
            this.timeKeeper.Now = TimeSpan.Zero;

            // Expect - test backup assignment by expecting future ping.
            this.transportProtocol.Expect(p => p.SendRemoteCall(
                backupAddress,
                QualityOfService.Reliable,
                this.serverProtocol.ReceivePingRequest));

            // Act
            cc.Argument.Invoke(EngagementRequestResult.Success, backupAddress.ToString());
            this.timeKeeper.Now = TimeSpan.FromSeconds(10.01);
            this.client.RegularProcessing();

            // Verify
            this.transportProtocol.VerifyAllExpectations();
        }

        [Test]
        public void HandleAllocationReplyPositiveIgnoredIfValidatorAndBackupsAllAssigned()
        {
            // Arrange
            this.ConstructClientAndCompleteEntityRegistration();
            var cc = new CapturingConstraint<EngagementReplyHandler>();
            this.engagementClient.Stub(c => c.RequestValidator(Arg<EngagementReplyHandler>.Matches(cc)))
                .Repeat.Once();
            this.client.RegularProcessing();
            PeerAddress backupAddress = PeerAddress.GetRandomAddress();
            PeerAddress redundantBackupAddress = PeerAddress.GetRandomAddress();
            this.timeKeeper.Now = TimeSpan.Zero;
            cc.Argument.Invoke(EngagementRequestResult.Success, backupAddress.ToString());

            // Expect - test backup ignored by expecting no future ping.
            this.transportProtocol.Expect(p => p.SendRemoteCall(
                redundantBackupAddress,
                QualityOfService.Reliable,
                this.serverProtocol.ReceivePingRequest))
                .Repeat.Never();

            // Act
            cc.Argument.Invoke(EngagementRequestResult.Success, redundantBackupAddress.ToString());
            this.timeKeeper.Now = TimeSpan.FromSeconds(10.01);
            this.client.RegularProcessing();

            // Verify
            this.transportProtocol.VerifyAllExpectations();
        }

        ////[Test, Ignore("Delaying pings not currently working, but only an optimziation so not critical.")]
        ////public void HandleAllocationReplyPositiveForKnownBackupDelaysPing()
        ////{
        ////    // Arrange
        ////    this.ConstructClientAndCompleteEntityRegistration();
        ////    var cc = new CapturingConstraint<EngagementReplyHandler>();
        ////    this.engagementClient.Stub(c => c.RequestValidator(Arg<EngagementReplyHandler>.Matches(cc)))
        ////        .Repeat.Once();
        ////    this.client.RegularProcessing();
        ////    PeerAddress backupAddress = PeerAddress.GetRandomAddress();
        ////    this.timeKeeper.Now = TimeSpan.Zero;
        ////    cc.Argument.Invoke(EngagementRequestResult.Success, backupAddress.ToString());

        ////    // Expect
        ////    this.transportProtocol.Expect(p => p.SendRemoteCall(
        ////        backupAddress,
        ////        QualityOfService.Reliable,
        ////        this.serverProtocol.ReceivePingRequest))
        ////        .Repeat.Never();

        ////    // Act
        ////    this.timeKeeper.Now = TimeSpan.FromSeconds(5);
        ////    cc.Argument.Invoke(EngagementRequestResult.Success, backupAddress.ToString());
        ////    this.timeKeeper.Now = TimeSpan.FromSeconds(11);
        ////    this.client.RegularProcessing();

        ////    // Verify
        ////    this.transportProtocol.VerifyAllExpectations();
        ////}

        [Test]
        public void HandleAllocationReplyPositiveForKnownBackupSchedulesDelayedPing()
        {
            // Arrange
            this.ConstructClientAndCompleteEntityRegistration();
            var cc = new CapturingConstraint<EngagementReplyHandler>();
            this.engagementClient.Stub(c => c.RequestValidator(Arg<EngagementReplyHandler>.Matches(cc)))
                .Repeat.Once();
            this.client.RegularProcessing();
            PeerAddress backupAddress = PeerAddress.GetRandomAddress();
            this.timeKeeper.Now = TimeSpan.Zero;
            cc.Argument.Invoke(EngagementRequestResult.Success, backupAddress.ToString());

            // Expect
            this.transportProtocol.Expect(p => p.SendRemoteCall(
                backupAddress,
                QualityOfService.Reliable,
                this.serverProtocol.ReceivePingRequest));

            // Act
            this.timeKeeper.Now = TimeSpan.FromSeconds(5);
            cc.Argument.Invoke(EngagementRequestResult.Success, backupAddress.ToString());
            this.timeKeeper.Now = TimeSpan.FromSeconds(15.01);
            this.client.RegularProcessing();

            // Verify
            this.transportProtocol.VerifyAllExpectations();
        }

        [Test]
        public void HandleAllocationReplyNegativeIgnoredIfValidatorAssiged()
        {
            this.ConstructClientAndCompleteEntityRegistration();

            // Expect
            ////this.eventScheduler.Expect(q => q.Push(wrapper.OnValidationFailure)).Repeat.Never();
            this.queueApplicationEvent.Expect(q => q(null))
                .IgnoreArguments()
                .Repeat.Never();

            // Act
            this.engagementReplyHandlerCapturer.Argument.Invoke(
                EngagementRequestResult.Failure,
                PeerAddress.GetRandomAddress().ToString());

            // Verify
            this.queueApplicationEvent.VerifyAllExpectations();
        }

        [Test]
        public void RegularProcessingStartsIntervalSeriesIfNotStartedAndMessageReceived()
        {
            // Arrange
            this.ConstructClientAndCompleteEntityRegistration();
            var milestone = MockRepository.GenerateMock<ISignedEntityState>();
            this.timeKeeper.Now = TimeSpan.FromSeconds(0);
            this.client.ReceiveEntityStateMilestone(this.validatorAddress, this.localId, milestone);

            // Expect
            this.intervalSeries.Expect(s => s.Start());

            // Act
            this.client.RegularProcessing();

            // Verify
            this.intervalSeries.VerifyAllExpectations();
        }

        [Test]
        public void RegularProcessingAddsToIntervalSeriesIfStartedAndMessageReceived()
        {
            // Arrange
            this.ConstructClientAndCompleteEntityRegistration();
            var milestone = MockRepository.GenerateMock<ISignedEntityState>();
            this.timeKeeper.Now = TimeSpan.FromSeconds(0);
            this.client.ReceiveEntityStateMilestone(this.validatorAddress, this.localId, milestone);
            this.client.RegularProcessing();
            this.intervalSeries.Stub(s => s.IsStarted).Return(true);
            var update = MockRepository.GenerateMock<IOrdered<ISerializedEntityState>>();
            this.client.ReceiveEntityStateUpdate(this.validatorAddress, this.localId, update);

            // Expect
            this.intervalSeries.Expect(s => s.Add());

            // Act
            this.client.RegularProcessing();

            // Verify
            this.intervalSeries.VerifyAllExpectations();
        }

        [Test]
        public void RegularProcessingTimesOutSilentValidatorsAfterTimeoutInterval()
        {
            // Arrange
            this.intervalSeriesFactory = MockRepository.GenerateMock<IntervalSeriesFactory>();
            this.intervalSeriesFactory.Stub(f => f.Invoke(0, null))
                .IgnoreArguments()
                .Return(new IntervalSeries(3600, this.timeKeeper));
            this.ConstructClientAndCompleteEntityRegistration();
            var milestone = MockRepository.GenerateMock<ISignedEntityState>();
            this.timeKeeper.Now = TimeSpan.FromSeconds(0);
            this.client.ReceiveEntityStateMilestone(this.validatorAddress, this.localId, milestone);
            this.client.RegularProcessing();

            // Expect
            ////this.eventScheduler.Expect(q => q.Push(this.wrapper.OnValidatorLost, this.validatorAddress))
            ////    .Return(null);
            this.queueApplicationEvent.Expect(q => q(null))
                .IgnoreArguments();

            // Act
            this.timeKeeper.Now = TimeSpan.FromSeconds(2);
            this.client.RegularProcessing();

            // Verify
            this.queueApplicationEvent.VerifyAllExpectations();
        }

        [Test]
        public void RegularProcessingCalculatesTimeoutIntervalUsingDelegate()
        {
            // Arrange
            this.intervalSeriesFactory = MockRepository.GenerateMock<IntervalSeriesFactory>();
            this.intervalSeriesFactory.Stub(f => f.Invoke(0, null))
                .IgnoreArguments()
                .Return(new IntervalSeries(3600, this.timeKeeper));
            this.ConstructClientAndCompleteEntityRegistration();
            var milestone = MockRepository.GenerateMock<ISignedEntityState>();
            this.timeKeeper.Now = TimeSpan.FromSeconds(0);
            this.client.ReceiveEntityStateMilestone(this.validatorAddress, this.localId, milestone);
            this.client.RegularProcessing();
            var timeoutDelegate = MockRepository.GenerateMock<ValidatorTimeoutCalculationMethod>();
            this.client.ValidationTimeoutCalculationFunction = timeoutDelegate;

            // Expect
            timeoutDelegate.Expect(d => d.Invoke(0, 0, 0, 0))
                .IgnoreArguments()
                .Return(0);

            // Act
            this.client.RegularProcessing();

            // Verify
            timeoutDelegate.VerifyAllExpectations();
        }

        [Test]
        public void RegularProcessingUsesCorrectStatsWHenCalculatingTimeoutInterval()
        {
            // Arrange
            ////this.intervalSeriesFactory = MockRepository.GenerateMock<IntervalSeriesFactory>();
            ////this.intervalSeriesFactory.Stub(f => f.Invoke(0, null))
            ////    .IgnoreArguments()
            ////    .Return(new IntervalSeries(3600, this.timeKeeper));
            this.ConstructClientAndCompleteEntityRegistration();
            var milestone = MockRepository.GenerateMock<ISignedEntityState>();
            this.timeKeeper.Now = TimeSpan.FromSeconds(0);
            this.client.ReceiveEntityStateMilestone(this.validatorAddress, this.localId, milestone);
            this.client.RegularProcessing();
            var timeoutDelegate = MockRepository.GenerateMock<ValidatorTimeoutCalculationMethod>();
            this.client.ValidationTimeoutCalculationFunction = timeoutDelegate;
            double mean = 1.0;
            double stdDev = 2.0;
            double duration = 3.0;
            int messageCount = 4;
            this.intervalSeries.Stub(s => s.IsStarted).Return(true);
            this.intervalSeries.Stub(s => s.Mean).Return(mean);
            this.intervalSeries.Stub(s => s.StdDev).Return(stdDev);
            this.intervalSeries.Stub(s => s.GetTimeSinceStart()).Return(TimeSpan.FromMilliseconds(duration));
            this.intervalSeries.Stub(s => s.Count).Return(messageCount);

            // Expect
            timeoutDelegate.Expect(d => d.Invoke(mean, stdDev, duration, messageCount))
                .IgnoreArguments()
                .Return(0);

            // Act
            this.client.RegularProcessing();

            // Verify
            timeoutDelegate.VerifyAllExpectations();
        }

        [Test]
        public void OnConnectionLostForCurrentValidatorResetsIntervalSeries()
        {
            // Arrange
            var capturedOnConnectionLost = new CapturingConstraint<ConnectionHandler>();
            this.peerConnectionNotifier.Stub(
                n => n.ConnectionLostEvent += Arg<ConnectionHandler>.Matches(capturedOnConnectionLost));
            this.ConstructClientAndCompleteEntityRegistration();
            var milestone = MockRepository.GenerateMock<ISignedEntityState>();
            this.timeKeeper.Now = TimeSpan.FromSeconds(0);
            this.client.ReceiveEntityStateMilestone(this.validatorAddress, this.localId, milestone);
            this.client.RegularProcessing();

            // Expect
            this.intervalSeries.Expect(s => s.Reset());

            // Act
            capturedOnConnectionLost.Argument.Invoke(this.validatorAddress);

            // Verify
            this.intervalSeries.VerifyAllExpectations();
        }

        [Test]
        public void OnConnectionLostNotifiesWrappersOfNewValidatorIfBackupAvailable()
        {
            // Arrange
            var capturedApplicationEvent = new CapturingConstraint<IInvokable>();
            this.queueApplicationEvent.Stub(
                q => q(Arg<IInvokable>.Matches(capturedApplicationEvent)));
            var capturedOnConnectionLost = new CapturingConstraint<ConnectionHandler>();
            this.peerConnectionNotifier.Stub(
                n => n.ConnectionLostEvent += Arg<ConnectionHandler>.Matches(capturedOnConnectionLost));
            this.ConstructClientAndCompleteEntityRegistration();
            var cc = new CapturingConstraint<EngagementReplyHandler>();
            this.engagementClient.Stub(c => c.RequestValidator(Arg<EngagementReplyHandler>.Matches(cc)))
                .Repeat.Once();
            this.client.RegularProcessing();
            PeerAddress backupAddress = PeerAddress.GetRandomAddress();
            this.timeKeeper.Now = TimeSpan.Zero;
            cc.Argument.Invoke(EngagementRequestResult.Success, backupAddress.ToString());
            this.timeKeeper.Now = TimeSpan.FromSeconds(10.01);
            this.client.RegularProcessing();

            // Expect
            this.wrapper.Expect(w => w.OnValidatorAllocation(backupAddress));

            // Act
            capturedOnConnectionLost.Argument.Invoke(this.validatorAddress);
            capturedApplicationEvent.Argument.Invoke();

            // Verify
            this.wrapper.VerifyAllExpectations();
        }

        private void ConstructClient()
        {
            this.client = new Client(
                this.transportProtocol,
                this.serverProtocol,
                this.engagementClient,
                this.peerConnectionNotifier,
                this.timeKeeper,
                this.queueApplicationEvent,
                this.wrapperFactory,
                this.intervalSeriesFactory,
                this.idFactory);
        }

        private void ConstructClientAndCompleteEntityRegistration()
        {
            // Arrange
            this.ConstructClient();
            this.engagementClient.Stub(c => c.RequestValidator(
                Arg<EngagementReplyHandler>.Matches(this.engagementReplyHandlerCapturer)))
                .Repeat.Once();
            this.wrapperFactory.Stub(f => f.Invoke(
                this.transportProtocol,
                this.serverProtocol,
                this.entity,
                this.entityType,
                this.badumnaId,
                null,
                null))
                .IgnoreArguments()
                .Return(this.wrapper);
            this.client.RegisterValidatedEntity(this.entity, this.entityType, null);
            this.engagementReplyHandlerCapturer.Argument.Invoke(
                EngagementRequestResult.Success,
                this.validatorAddress.ToString());
        }

        private void ConstructClientAndBeginEntityRegistration()
        {
            // Arrange
            this.ConstructClient();
            this.engagementClient.Stub(c => c.RequestValidator(
                Arg<EngagementReplyHandler>.Matches(this.engagementReplyHandlerCapturer)))
                .Repeat.Once();
            this.wrapperFactory.Stub(f => f.Invoke(
                this.transportProtocol,
                this.serverProtocol,
                this.entity,
                this.entityType,
                this.badumnaId,
                null,
                null))
                .IgnoreArguments()
                .Return(this.wrapper);
            this.client.RegisterValidatedEntity(this.entity, this.entityType, null);
        }

        private class TimeFaker : ITime
        {
            public TimeSpan Now { get; set; }
        }
    }
}
