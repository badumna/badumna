﻿//-----------------------------------------------------------------------
// <copyright file="FlaggedTests.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BadumnaTests.Validation.HostingTests
{
    using System;
    using Badumna.Core;
    using Badumna.Transport;
    using Badumna.Validation;
    using Badumna.Validation.Hosting;
    using NUnit.Framework;
    using Rhino.Mocks;
    using TestUtilities;

    [TestFixture]
    [DontPerformCoverage]
    public class FlaggedTests
    {
        [SetUp]
        public void SetUp()
        {
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ConstructorThrowsForNullItem()
        {
            var f = new Flagged<int?>(true, null);
        }
    }
}
