﻿//-----------------------------------------------------------------------
// <copyright file="LocalEntityWrapperTests.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BadumnaTests.Validation.HostingTests
{
    using System;
    using System.Collections.Generic;
    using Badumna.Arbitration;
    using Badumna.Core;
    using Badumna.DataTypes;
    using Badumna.SpatialEntities;
    using Badumna.Transport;
    using Badumna.Utilities;
    using Badumna.Validation;
    using Badumna.Validation.Hosting;
    using NUnit.Framework;
    using Rhino.Mocks;

    [TestFixture]
    [DontPerformCoverage]
    public class LocalEntityWrapperTests
    {
        private ITransportProtocol transportProtocol;
        private IServerProtocol serverProtocol;
        private SequencerFactory<IOrdered<ISerializedEntityState>> sequencerFactory;
        private ISequencer<IOrdered<ISerializedEntityState>> sequencer;
        private ISequenceGenerator sequenceGenerator;
        private IValidatedEntity entity;
        private ushort localId;
        private BadumnaId badumnaId;
        private PeerAddress validatorAddress;
        private LocalEntityWrapper wrapper;

        private CyclicalID.UShortID milestoneNumber;
        private SerializedEntityState milestoneUpdate;
        private byte[] milestoneSignature;
        private SignedEntityState milestone;
        private IOrdered<ISerializedEntityState> serialStateUpdate;
        private ISerializedEntityState stateUpdate;
        private ISerializedEntityState mergedState;
        private List<byte[]> inputs;
        private List<TimeSpan> intervals;
        private int inputCount;
        private NetworkScene scene;
        private QualifiedName sceneName;
        private uint entityType;

        private NetworkEventQueue eventQueue = null;
        private QueueInvokable queueApplicationEvent;

        [SetUp]
        public void SetUp()
        {
            this.transportProtocol = MockRepository.GenerateMock<ITransportProtocol>();
            this.serverProtocol = MockRepository.GenerateMock<IServerProtocol>();
            this.sequencerFactory = MockRepository.GenerateMock<SequencerFactory<IOrdered<ISerializedEntityState>>>();
            this.sequencer = MockRepository.GenerateMock<ISequencer<IOrdered<ISerializedEntityState>>>();
            this.sequenceGenerator = MockRepository.GenerateMock<ISequenceGenerator>();            
            this.localId = (ushort)new Random().Next(ushort.MaxValue);
            this.badumnaId = new BadumnaId(PeerAddress.GetRandomAddress(), this.localId);
            this.validatorAddress = PeerAddress.GetRandomAddress();
            this.entity = MockRepository.GenerateMock<IValidatedEntity>();
            this.entity.Stub(e => e.Guid).Return(this.badumnaId);

            this.milestoneNumber = new CyclicalID.UShortID(999);
            this.milestoneUpdate = new SerializedEntityState();
            this.milestoneSignature = new byte[0];
            this.milestone = new SignedEntityState(
                new Ordered<ISerializedEntityState>(this.milestoneNumber, this.milestoneUpdate),
                this.milestoneSignature);
            this.serialStateUpdate = MockRepository.GenerateMock<IOrdered<ISerializedEntityState>>();
            this.stateUpdate = MockRepository.GenerateMock<ISerializedEntityState>();
            this.serialStateUpdate.Stub(u => u.Item).Return(this.stateUpdate);
            this.mergedState = MockRepository.GenerateMock<ISerializedEntityState>();
            this.entity.Stub(e => e.MergeStateUpdates(null, null))
                .IgnoreArguments()
                .Return(this.mergedState);

            this.inputCount = 5;
            this.inputs = new List<byte[]>(this.inputCount);
            this.intervals = new List<TimeSpan>(this.inputCount);
            for (int i = 0; i < this.inputCount; i++)
            {
                this.inputs.Add(new byte[0]);
                this.intervals.Add(TimeSpan.FromMilliseconds(i));
            }

            this.eventQueue = new NetworkEventQueue();
            this.queueApplicationEvent = i => i.Invoke();

            this.sceneName = new QualifiedName("", Guid.NewGuid().ToString());
            this.scene = MockRepository.GenerateMock<NetworkScene>(
                this.sceneName,
                null,
                null,
                null,
                this.eventQueue,
                this.queueApplicationEvent,
                false);
            this.entityType = (uint)new Random().Next(0, int.MaxValue);
        }

        [Test]
        public void ConstructorThrowsForNullTransportProtocol()
        {
            this.transportProtocol = null;
            Assert.Throws(typeof(ArgumentNullException), this.ConstructWrapper);
        }

        [Test]
        public void ConstructorThrowsForNullServerProtocol()
        {
            this.serverProtocol = null;
            Assert.Throws(typeof(ArgumentNullException), this.ConstructWrapper);
        }

        [Test]
        public void ConstructorThrowsForNullSequencerFactory()
        {
            this.sequencerFactory = null;
            Assert.Throws(typeof(ArgumentNullException), this.ConstructWrapper);
        }

        [Test]
        public void ConstructorThrowsForNullSequenceGenerator()
        {
            this.sequenceGenerator = null;
            Assert.Throws(typeof(ArgumentNullException), this.ConstructWrapper);
        }

        [Test]
        public void ConstructorThrowsForNullEntity()
        {
            this.entity = null;
            Assert.Throws(typeof(ArgumentNullException), this.ConstructWrapper);
        }

        [Test]
        public void ConstructorThrowsForNullBadumnaId()
        {
            this.badumnaId = null;
            Assert.Throws(typeof(ArgumentNullException), this.ConstructWrapper);
        }

        [Test]
        public void ConstructorCreatesDefaultSequencerFactory()
        {
            // Test passes if no exception thrown
            this.ConstructWrapperWithDefaultSequencerFactoryAndSequenceGenerator();

            // Constructor will also invoke factory, so this test will fail if the default factory does.
        }

        [Test]
        public void ConstructorInvokesSequencerFactory()
        {
            // Expect
            this.sequencerFactory.Expect(f => f.Invoke(Arg<GenericCallBack<IOrdered<ISerializedEntityState>>>.Is.Anything));

            // Act
            this.ConstructWrapper();

            // Verify
            this.sequencerFactory.VerifyAllExpectations();
        }

        [Test]
        public void ConstructorInitializesSequencer()
        {
            // Expect
            this.sequencer.Expect(s => s.Initialize(Arg<CyclicalID.UShortID>.Is.Anything));

            // Act
            this.ConstructWrapperWithStubbedFactories();

            // Verify
            this.sequencer.VerifyAllExpectations();
        }

        [Test]
        public void ConstructorSendsRegistrationRequestWhenValidatorAddressProvided()
        {
            this.sequencerFactory.Stub(f => f.Invoke(Arg<GenericCallBack<IOrdered<ISerializedEntityState>>>.Is.Anything))
                .Return(this.sequencer);
            var initialState = MockRepository.GenerateMock<ISerializedEntityState>();
            
                // Expect
            this.transportProtocol.Expect(p => p.SendRemoteCall(
                Arg.Is(this.validatorAddress),
                Arg.Is(QualityOfService.Reliable),
                Arg<GenericCallBack<uint, BadumnaId, SignedEntityState, QualifiedName>>.Is.Equal((GenericCallBack<uint, BadumnaId, SignedEntityState, QualifiedName>)this.serverProtocol.ReceiveEntityTransitionRequest),
                Arg.Is(this.entityType),
                Arg.Is(this.badumnaId),
                Arg<SignedEntityState>.Is.Anything,
                Arg.Is(default(QualifiedName))));

            // Act
            this.wrapper = new LocalEntityWrapper(
                this.transportProtocol,
                this.serverProtocol,
                this.sequencerFactory,
                this.sequenceGenerator,
                this.entity,
                this.entityType,
                this.badumnaId,
                initialState,
                this.validatorAddress);

            // Verify
            this.transportProtocol.VerifyAllExpectations();
        }

        [Test]
        public void ConstructorSendsTransitionRequestWhenValidatorAddressAndInitialStateProvided()
        {
            // Expect
            this.transportProtocol.Expect(p => p.SendRemoteCall(
                this.validatorAddress,
                QualityOfService.Reliable,
                this.serverProtocol.ReceiveEntityRegistrationRequest,
                this.entityType,
                this.badumnaId,
                default(QualifiedName)));

            // Act
            this.ConstructWrapperWithStubbedFactories();

            // Verify
            this.transportProtocol.VerifyAllExpectations();
        }

        [Test]
        public void ConstructorSendsNoRegistrationRequestWhenNoValidatorAddressProvided()
        {
            // Arrange
            this.validatorAddress = null;

            // Expect
            this.transportProtocol.Expect(p => p.SendRemoteCall(
                null,
                QualityOfService.Reliable,
                this.serverProtocol.ReceiveEntityRegistrationRequest,
                this.entityType,
                this.badumnaId,
                default(QualifiedName)))
                .IgnoreArguments()
                .Repeat.Never();

            // Act
            this.ConstructWrapperWithStubbedFactories();

            // Verify
            this.transportProtocol.VerifyAllExpectations();
        }

        [Test]
        public void OnValidatorAllocationThrowsForNullAddress()
        {
            // Arrange
            this.ConstructWrapperWithStubbedFactories();

            // Act, Assert
            Assert.Throws(
                typeof(ArgumentNullException),
                () => this.wrapper.OnValidatorAllocation(null));
        }

        [Test]
        public void OnValidatorAllocationTriggersRegistrationRequest()
        {
            // Arrange
            this.ConstructWrapperWithStubbedFactories();

            // Expect
            this.transportProtocol.Expect(p => p.SendRemoteCall(
                this.validatorAddress,
                QualityOfService.Reliable,
                this.serverProtocol.ReceiveEntityRegistrationRequest,
                this.entityType,
                this.badumnaId,
                default(QualifiedName)));

            // Act
            this.wrapper.OnValidatorAllocation(this.validatorAddress);

            // Verify
            this.transportProtocol.VerifyAllExpectations();
        }

        [Test]
        public void OnValidatorAllocationTriggersTransitionRequestWhenMilestoneExists()
        {
            // Arrange
            this.ConstructWrapperWithStubbedFactories();
            this.wrapper.OnValidatorAllocation(this.validatorAddress);
            this.wrapper.ReceiveMilestone(this.milestone);
            var newAllocator = PeerAddress.GetRandomAddress();

            // Expect
            this.transportProtocol.Expect(p => p.SendRemoteCall(
                newAllocator,
                QualityOfService.Reliable,
                this.serverProtocol.ReceiveEntityTransitionRequest,
                this.entityType,
                this.badumnaId,
                this.milestone,
                default(QualifiedName)));

            // Act
            this.wrapper.OnValidatorAllocation(newAllocator);

            // Verify
            this.transportProtocol.VerifyAllExpectations();
        }

        [Test]
        public void OnValidatorAllocationTriggersRegistrationRequestWithCurrentSceneWhenRegistered()
        {
            // Arrange
            this.ConstructWrapperWithStubbedFactories();
            this.wrapper.RegisterEntityWithScene(this.scene);

            // Expect
            this.transportProtocol.Expect(p => p.SendRemoteCall(
                this.validatorAddress,
                QualityOfService.Reliable,
                this.serverProtocol.ReceiveEntityRegistrationRequest,
                this.entityType,
                this.badumnaId,
                this.scene.QualifiedName));

            // Act
            this.wrapper.OnValidatorAllocation(this.validatorAddress);

            // Verify
            this.transportProtocol.VerifyAllExpectations();
        }

        [Test]
        public void OnValidatorAllocationTriggersTransitionRequestWhenMilestoneExistsWithCurrentScene()
        {
            // Arrange
            this.ConstructWrapperWithStubbedFactories();
            this.wrapper.RegisterEntityWithScene(this.scene);
            this.wrapper.OnValidatorAllocation(this.validatorAddress);
            this.wrapper.ReceiveMilestone(this.milestone);
            var newAllocator = PeerAddress.GetRandomAddress();

            // Expect
            this.transportProtocol.Expect(p => p.SendRemoteCall(
                newAllocator,
                QualityOfService.Reliable,
                this.serverProtocol.ReceiveEntityTransitionRequest,
                this.entityType,
                this.badumnaId,
                this.milestone,
                this.sceneName));

            // Act
            this.wrapper.OnValidatorAllocation(newAllocator);

            // Verify
            this.transportProtocol.VerifyAllExpectations();
        }

        [Test]
        public void OnValidatorAllocationTriggersRegistrationRequestWithNoSceneWhenUnregistered()
        {
            // Arrange
            this.ConstructWrapperWithStubbedFactories();
            this.wrapper.RegisterEntityWithScene(this.scene);
            this.wrapper.UnregisterEntityFromScene();

            // Expect
            this.transportProtocol.Expect(p => p.SendRemoteCall(
                this.validatorAddress,
                QualityOfService.Reliable,
                this.serverProtocol.ReceiveEntityRegistrationRequest,
                this.entityType,
                this.badumnaId,
                default(QualifiedName)));

            // Act
            this.wrapper.OnValidatorAllocation(this.validatorAddress);

            // Verify
            this.transportProtocol.VerifyAllExpectations();
        }

        [Test]
        public void OnValidatorAllocationTriggersTransitionRequestWhenMilestoneExistsWithNoSceneWhenUnregistered()
        {
            // Arrange
            this.ConstructWrapperWithStubbedFactories();
            this.wrapper.RegisterEntityWithScene(this.scene);
            this.wrapper.OnValidatorAllocation(this.validatorAddress);
            this.wrapper.ReceiveMilestone(this.milestone);
            var newAllocator = PeerAddress.GetRandomAddress();
            this.wrapper.UnregisterEntityFromScene();

            // Expect
            this.transportProtocol.Expect(p => p.SendRemoteCall(
                newAllocator,
                QualityOfService.Reliable,
                this.serverProtocol.ReceiveEntityTransitionRequest,
                this.entityType,
                this.badumnaId,
                this.milestone,
                default(QualifiedName)));

            // Act
            this.wrapper.OnValidatorAllocation(newAllocator);

            // Verify
            this.transportProtocol.VerifyAllExpectations();
        }

        [Test]
        public void OnValidationFailureChangesEntityStatusToPaused()
        {
            // Arrange
            this.ConstructWrapperWithStubbedFactories();

            // Expect
            this.entity.Expect(e => e.Status = EntityStatus.Paused);

            // Act
            this.wrapper.OnValidationFailure();

            // Verify
            this.entity.VerifyAllExpectations();
        }

        [Test]
        public void OnValidationFailureClearsBufferedInput()
        {
            // Arrange
            this.ConstructWrapperWithDefaultSequencerFactoryAndSequenceGenerator();
            this.wrapper.OnValidatorAllocation(this.validatorAddress);
            this.wrapper.ReceiveMilestone(this.milestone);
            for (int i = 0; i < this.inputCount; i++)
            {
                this.wrapper.UpdateEntity(this.intervals[i], this.inputs[i]);
            }

            this.wrapper.OnValidatorLost(this.validatorAddress);
            this.wrapper.OnValidationFailure();
            var newAllocator = PeerAddress.GetRandomAddress();
            this.wrapper.OnValidatorAllocation(newAllocator);

            // Expect
            this.transportProtocol.Expect(p => p.SendRemoteCall(
                Arg<PeerAddress>.Is.Equal(newAllocator),
                Arg<QualityOfService>.Is.Equal(QualityOfService.Reliable),
                Arg<GenericCallBack<byte[], CyclicalID.UShortID, long, byte[], bool>>.Is.Anything,
                Arg<byte[]>.Is.Equal(this.localId),
                Arg<CyclicalID.UShortID>.Is.Anything,
                Arg<long>.Is.Anything,
                Arg<byte[]>.Is.Anything,
                Arg<bool>.Is.Equal(false)))
                .IgnoreArguments()
                .Repeat.Never();

            // Act
            this.wrapper.ReceiveMilestone(this.milestone);

            // Verify
            this.transportProtocol.VerifyAllExpectations();
        }

        [Test]
        public void OnValidationFailureResetsSequenceGenerator()
        {
            // Arrange
            this.ConstructWrapperWithStubbedFactories();
            this.wrapper.OnValidatorAllocation(this.validatorAddress);
            this.wrapper.ReceiveMilestone(this.milestone);
            for (int i = 0; i < this.inputCount; i++)
            {
                this.wrapper.UpdateEntity(this.intervals[i], this.inputs[i]);
            }

            this.wrapper.OnValidatorLost(this.validatorAddress);

            // Expect
            this.sequenceGenerator.Expect(g => g.Reset(this.milestone.SerialEntityState.SerialNumber.Value));

            // Act
            this.wrapper.OnValidationFailure();

            // Verify
            this.sequenceGenerator.VerifyAllExpectations();
        }

        [Test]
        public void ReceiveMilestoneTriggersBufferedInputSendForNewValidator()
        {
            // Arrange
            this.ConstructWrapperWithDefaultSequencerFactoryAndSequenceGenerator();
            this.wrapper.OnValidatorAllocation(this.validatorAddress);
            this.wrapper.ReceiveMilestone(this.milestone);
            for (int i = 0; i < this.inputCount; i++)
            {
                this.wrapper.UpdateEntity(this.intervals[i], this.inputs[i]);
            }

            this.wrapper.OnValidatorLost(this.validatorAddress);
            var newAllocator = PeerAddress.GetRandomAddress();
            this.wrapper.OnValidatorAllocation(newAllocator);

            // Expect
            this.transportProtocol.Expect(p => p.SendRemoteCall(
                Arg<PeerAddress>.Is.Equal(newAllocator),
                Arg<QualityOfService>.Is.Equal(QualityOfService.Reliable),
                Arg<GenericCallBack<ushort, CyclicalID.UShortID, long, byte[], bool>>.Is.Anything,
                Arg.Is(this.localId),
                Arg<CyclicalID.UShortID>.Is.Anything,
                Arg<long>.Is.Anything,
                Arg<byte[]>.Is.Anything,
                Arg<bool>.Is.Equal(false)))
                .IgnoreArguments()
                .Repeat.Times(5);

            // Act
            this.wrapper.ReceiveMilestone(this.milestone);

            // Verify
            this.transportProtocol.VerifyAllExpectations();
        }

        [Test]
        public void ReceiveMilestoneClearsOldBufferedInput()
        {
            // Arrange
            this.ConstructWrapperWithDefaultSequencerFactoryAndSequenceGenerator();
            this.wrapper.OnValidatorAllocation(this.validatorAddress);
            this.wrapper.ReceiveMilestone(this.milestone);
            for (int i = 0; i < this.inputCount; i++)
            {
                this.wrapper.UpdateEntity(this.intervals[i], this.inputs[i]);
            }

            this.wrapper.OnValidatorLost(this.validatorAddress);
            var newAllocator = PeerAddress.GetRandomAddress();
            this.wrapper.OnValidatorAllocation(newAllocator);
            var serialStateUpdate = MockRepository.GenerateMock<IOrdered<ISerializedEntityState>>();
            var newMilestone = new SignedEntityState(serialStateUpdate, new byte[0]);
            serialStateUpdate.Stub(u => u.SerialNumber).Return(new CyclicalID.UShortID(4));

            // Expect
            this.transportProtocol.Expect(p => p.SendRemoteCall(
                newAllocator,
                QualityOfService.Reliable,
                this.serverProtocol.ReceiveEntityInput,
                this.localId,
                new CyclicalID.UShortID(4),
                this.intervals[4].Ticks,
                this.inputs[4],
                false));
            this.transportProtocol.Stub(p => p.SendRemoteCall(
                newAllocator,
                QualityOfService.Reliable,
                this.serverProtocol.ReceiveEntityInput,
                this.localId,
                new CyclicalID.UShortID(4),
                this.intervals[4].Ticks,
                this.inputs[4],
                false))
                .IgnoreArguments()
                .Throw(new InvalidOperationException("Test fails because input should only be sent once."));

            // Act
            this.wrapper.ReceiveMilestone(newMilestone);

            // Verify
            this.transportProtocol.VerifyAllExpectations();
        }

        [Test]
        public void ReceiveMilestonePassesUpdateToSequencer()
        {
            // Arrange
            this.ConstructWrapperWithStubbedFactories();
            this.wrapper.OnValidatorAllocation(this.validatorAddress);
            this.wrapper.ReceiveMilestone(this.milestone);

            // Expect
            this.sequencer.Expect(s => s.ReceiveEvent(
                this.milestone.SerialEntityState.SerialNumber,
                this.milestone.SerialEntityState));

            // Act
            this.wrapper.ReceiveMilestone(this.milestone);

            // Verify
            this.sequencer.VerifyAllExpectations();
        }

        [Test]
        public void ReceiveUpdatePassesUpdateToSequencer()
        {
            // Arrange
            this.ConstructWrapperWithStubbedFactories();
            ////this.wrapper.OnValidatorAllocation(this.validatorAddress);
            this.wrapper.ReceiveMilestone(this.milestone);

            // Expect
            this.sequencer.Expect(s => s.ReceiveEvent(
                this.serialStateUpdate.SerialNumber,
                this.serialStateUpdate));

            // Act
            this.wrapper.ReceiveUpdate(this.serialStateUpdate);

            // Verify
            this.sequencer.VerifyAllExpectations();
        }

        [Test]
        public void UdpateEntityDoesNotUpdateEntityIfNoVerifedStateKnown()
        {
            this.ConstructWrapperWithDefaultSequencerFactoryAndSequenceGenerator();
            ////this.wrapper.ReceiveMilestone(this.milestone);

            // Expect
            this.entity.Expect(e => e.Update(this.intervals[0], this.inputs[0], UpdateMode.Prediction))
                .IgnoreArguments()
                .Repeat.Never();

            // Act
            this.wrapper.UpdateEntity(this.intervals[0], this.inputs[0]);

            // Verify
            this.entity.VerifyAllExpectations();
        }

        [Test]
        public void UdpateEntityUpdatesEntityIfVerifedStateKnown()
        {
            this.ConstructWrapperWithDefaultSequencerFactoryAndSequenceGenerator();
            this.wrapper.ReceiveMilestone(this.milestone);

            // Expect
            this.entity.Expect(e => e.Update(this.intervals[0], this.inputs[0], UpdateMode.Prediction));

            // Act
            this.wrapper.UpdateEntity(this.intervals[0], this.inputs[0]);

            // Verify
            this.entity.VerifyAllExpectations();
        }

        [TestCase(0)]
        [TestCase(1)]
        [TestCase(2)]
        [TestCase(3)]
        [TestCase(4)]
        public void UpdateEntitySendsInputToValidator(int updateNumber)
        {
            this.entity.Stub(e => e.Status).PropertyBehavior();
            this.ConstructWrapperWithDefaultSequencerFactoryAndSequenceGenerator();
            this.wrapper.ReceiveMilestone(this.milestone);
            int i = 0;
            while (i < updateNumber)
            {
                this.wrapper.UpdateEntity(this.intervals[i], this.inputs[i]);
                i++;
            }

            // Expect
            this.transportProtocol.Expect(p => p.SendRemoteCall(
                this.validatorAddress,
                QualityOfService.Reliable,
                this.serverProtocol.ReceiveEntityInput,
                this.localId,
                new CyclicalID.UShortID((ushort)i),
                this.intervals[i].Ticks,
                this.inputs[i],
                true));

            // Act
            this.wrapper.UpdateEntity(this.intervals[i], this.inputs[i]);

            // Verify
            this.transportProtocol.VerifyAllExpectations();
        }

        [Test]
        public void UpdateEntityTriggersStateRestorationIfVerifiedStateChangedSinceLastUpdate()
        {
            this.ConstructWrapperWithDefaultSequencerFactoryAndSequenceGenerator();
            this.wrapper.ReceiveMilestone(this.milestone);

            // Expect
            this.entity.Expect(e => e.RestoreState(this.milestone.SerialEntityState.Item));

            // Act
            this.wrapper.UpdateEntity(this.intervals[0], this.inputs[0]);

            // Verify
            this.entity.VerifyAllExpectations();
        }

        [TestCase(1)]
        [TestCase(2)]
        [TestCase(3)]
        [TestCase(4)]

        public void UdpateEntityReappliesBufferedUpdatesIfVerifiedStateChangedSinceLastUpdate(int updateNumber)
        {
            this.ConstructWrapperWithDefaultSequencerFactoryAndSequenceGenerator();
            this.wrapper.ReceiveMilestone(this.milestone);
            int i = 0;
            while (i < updateNumber)
            {
                this.wrapper.UpdateEntity(this.intervals[i], this.inputs[i]);
                i++;
            }

            // Expect
            i = 1;
            while (i < updateNumber)
            {
                this.entity.Expect(e => e.Update(
                    this.intervals[i],
                    this.inputs[i],
                    UpdateMode.Synchronisation));
                i++;
            }

            // Act
            this.wrapper.ReceiveUpdate(
                new Ordered<ISerializedEntityState>(
                    new CyclicalID.UShortID(1),
                    MockRepository.GenerateMock<ISerializedEntityState>()));
            this.wrapper.UpdateEntity(this.intervals[i], this.inputs[i]);

            // Verify
            this.entity.VerifyAllExpectations();
        }

        [Test]
        public void RegisterEntityWithSceneRegistersEntityWithScene()
        {
            // Arrange
            this.ConstructWrapperWithStubbedFactories();
            var scene = this.ConstructScene();

            // Expect
            scene.Expect(s => s.RegisterEntity(this.entity, this.entityType));

            // Act
            this.wrapper.RegisterEntityWithScene(scene);

            // Verify
            scene.VerifyAllExpectations();
        }

        [Test]
        public void RegisterEntityWithSceneSendsRegistrationRequestToValidator()
        {
            // Arrange
            this.ConstructWrapperWithStubbedFactories();
            var scene = this.ConstructScene();
            uint entityType = 99;

            // Expect
            scene.Expect(s => s.RegisterEntity(this.entity, entityType));
            this.transportProtocol.Expect(p => p.SendRemoteCall(
                this.validatorAddress,
                QualityOfService.Reliable,
                this.serverProtocol.ReceiveSceneRegistrationRequest,
                this.localId,
                scene.QualifiedName));

            // Act
            this.wrapper.RegisterEntityWithScene(scene);

            // Verify
            this.transportProtocol.VerifyAllExpectations();
        }

        [Test]
        public void RegisterEntityWithSceneSendsNoRegistrationRequestWhenNoValidatorAssigned()
        {
            // Arrange
            this.ConstructWrapperWithStubbedFactories();
            this.wrapper.OnValidatorLost(this.validatorAddress);
            var scene = this.ConstructScene();
            uint entityType = 99;

            // Expect
            scene.Expect(s => s.RegisterEntity(this.entity, entityType));
            this.transportProtocol.Expect(p => p.SendRemoteCall(
                null,
                QualityOfService.Reliable,
                this.serverProtocol.ReceiveSceneRegistrationRequest,
                this.localId,
                scene.QualifiedName))
                .IgnoreArguments()
                .Repeat.Never();

            // Act
            this.wrapper.RegisterEntityWithScene(scene);

            // Verify
            this.transportProtocol.VerifyAllExpectations();
        }

        [Test]
        public void UnregisterEntityFromSceneUnregistersEntityFromScene()
        {
            // Arrange
            this.ConstructWrapperWithStubbedFactories();
            var scene = this.ConstructScene();
            this.wrapper.RegisterEntityWithScene(scene);

            // Expect
            scene.Expect(s => s.UnregisterEntity(this.entity));

            // Act
            this.wrapper.UnregisterEntityFromScene();

            // Verify
            scene.VerifyAllExpectations();
        }

        [Test]
        public void UnregisterEntityWithSceneSendsUnregistrationRequestToValidator()
        {
            // Arrange
            this.ConstructWrapperWithStubbedFactories();
            var scene = this.ConstructScene();
            this.wrapper.RegisterEntityWithScene(scene);

            // Expect
            this.transportProtocol.Expect(p => p.SendRemoteCall(
                this.validatorAddress,
                QualityOfService.Reliable,
                this.serverProtocol.ReceiveSceneUnregistrationRequest,
                this.localId,
                scene.QualifiedName));

            // Act
            this.wrapper.UnregisterEntityFromScene();

            // Verify
            this.transportProtocol.VerifyAllExpectations();
        }

        [Test]
        public void UnregisterEntityWithSceneSendsNoUnregistrationRequestWhenNoValidatorAssigned()
        {
            // Arrange
            this.ConstructWrapperWithStubbedFactories();
            var scene = this.ConstructScene();
            this.wrapper.RegisterEntityWithScene(scene);
            this.wrapper.OnValidatorLost(this.validatorAddress);

            // Expect
            this.transportProtocol.Expect(p => p.SendRemoteCall(
                null,
                QualityOfService.Reliable,
                this.serverProtocol.ReceiveSceneUnregistrationRequest,
                this.localId,
                scene.QualifiedName))
                .IgnoreArguments()
                .Repeat.Never();

            // Act
            this.wrapper.UnregisterEntityFromScene();

            // Verify
            this.transportProtocol.VerifyAllExpectations();
        }

        private void ConstructWrapperWithStubbedFactories()
        {
            this.sequencerFactory.Stub(f => f.Invoke(Arg<GenericCallBack<IOrdered<ISerializedEntityState>>>.Is.Anything))
                .Return(this.sequencer);
            this.ConstructWrapper();
        }

        private void ConstructWrapper()
        {
            this.wrapper = new LocalEntityWrapper(
                this.transportProtocol,
                this.serverProtocol,
                this.sequencerFactory,
                this.sequenceGenerator,
                this.entity,
                this.entityType,
                this.badumnaId,
                null,
                this.validatorAddress);
        }

        private NetworkScene ConstructScene()
        {
            return MockRepository.GenerateMock<NetworkScene>(
                this.sceneName,
                null,
                null,
                null,
                this.eventQueue,
                this.queueApplicationEvent,
                false);
        }

        private void ConstructWrapperWithDefaultSequencerFactoryAndSequenceGenerator()
        {
            this.wrapper = new LocalEntityWrapper(
                this.transportProtocol,
                this.serverProtocol,
                this.entity,
                this.entityType,
                this.badumnaId,
                null,
                this.validatorAddress);
        }
    }
}
