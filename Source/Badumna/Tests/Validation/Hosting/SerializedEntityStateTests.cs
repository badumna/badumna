﻿//-----------------------------------------------------------------------
// <copyright file="SerializedEntityStateTests.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BadumnaTests.Validation.HostingTests
{
    using System;
    using System.IO;
    using Badumna.Core;
    using Badumna.Transport;
    using Badumna.Utilities;
    using Badumna.Validation;
    using Badumna.Validation.Hosting;
    using NUnit.Framework;
    using Rhino.Mocks;
    using TestUtilities;

    [TestFixture]
    [DontPerformCoverage]
    public class SerializedEntityStateTests
    {
        [SetUp]
        public void SetUp()
        {
        }

        [Test]
        public void ToMessageFromMessagePreservesFlags()
        {
            BooleanArray flags = new BooleanArray(false);
            flags[2] = true;
            flags[5] = true;
            flags[10] = true;

            byte[] data = new byte[0];

            var originalState = new SerializedEntityState(flags, data);
            MessageBuffer message = new MessageBuffer();
            ((IParseable)originalState).ToMessage(message, typeof(SerializedEntityState));
            
            ////message.ReadOffset = 0;
            var deserializedState = new SerializedEntityState();
            ((IParseable)deserializedState).FromMessage(message);

            Assert.AreEqual(flags, deserializedState.Flags);
        }

        [TestCase(0)]
        [TestCase(1)]
        [TestCase(2)]
        [TestCase(100)]
        [TestCase(1000)]
        public void ToMessageFromMessagePreservesData(int length)
        {
            BooleanArray flags = new BooleanArray(false);
            byte[] data = new byte[length];
            new Random().NextBytes(data);

            var originalState = new SerializedEntityState(flags, data);
            MessageBuffer message = new MessageBuffer();
            ((IParseable)originalState).ToMessage(message, typeof(SerializedEntityState));

            ////message.ReadOffset = 0;
            var deserializedState = new SerializedEntityState();
            ((IParseable)deserializedState).FromMessage(message);

            Assert.AreEqual(data, deserializedState.Data);
        }
    }
}
