﻿//-----------------------------------------------------------------------
// <copyright file="TimedInputTests.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BadumnaTests.Validation.HostingTests
{
    using System;
    using Badumna.Core;
    using Badumna.Transport;
    using Badumna.Validation;
    using Badumna.Validation.Hosting;
    using NUnit.Framework;
    using Rhino.Mocks;

    [TestFixture]
    [DontPerformCoverage]
    public class TimedInputTests
    {
        [Test]
        public void ConstructorThrowsForNullUserInput()
        {
            Assert.Throws(typeof(ArgumentNullException), () => new TimedInput(TimeSpan.Zero, null));
        }

        [Test]
        public void TimePeriodReturnsTimePeriod()
        {
            TimeSpan interval = DateTime.Now - DateTime.Today;
            var ti = new TimedInput(interval, new byte[0]);
            Assert.AreEqual(interval, ti.TimePeriod);
        }

        [Test]
        public void UserInputGetsUserInput()
        {
            var ui = new byte[0];
            var ti = new TimedInput(TimeSpan.Zero, ui);
            Assert.AreEqual(ui, ti.UserInput);
        }
    }
}
