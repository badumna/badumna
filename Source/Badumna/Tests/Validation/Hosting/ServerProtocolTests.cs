﻿//-----------------------------------------------------------------------
// <copyright file="ServerProtocolTests.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BadumnaTests.Validation.HostingTests
{
    using System;
    using Badumna.Core;
    using Badumna.DataTypes;
    using Badumna.Transport;
    using Badumna.Utilities;
    using Badumna.Validation;
    using Badumna.Validation.Hosting;
    using NUnit.Framework;
    using Rhino.Mocks;
    using TestUtilities;

    [TestFixture]
    [DontPerformCoverage]
    public class ServerProtocolTests
    {
        private ITransportProtocol transportProtocol;
        private IServer server;
        private ServerProtocol serverProtocol;

        private PeerAddress clientAddress;
        private IValidatedEntity entity;
        private ushort localId;
        private BadumnaId badumnaId;
        private ISignedEntityState milestone;
        private QualifiedName sceneName;
        private uint entityType;

        [SetUp]
        public void SetUp()
        {
            this.transportProtocol = MockRepository.GenerateMock<ITransportProtocol>();
            this.server = MockRepository.GenerateMock<IServer>();
            this.clientAddress = PeerAddress.GetRandomAddress();
            this.entity = MockRepository.GenerateMock<IValidatedEntity>();
            this.localId = (ushort)new Random().Next(ushort.MaxValue);
            this.badumnaId = new BadumnaId(PeerAddress.GetRandomAddress(), this.localId);
            this.milestone = MockRepository.GenerateMock<ISignedEntityState>();
            this.sceneName = new QualifiedName("", Guid.NewGuid().ToString());
            this.entityType = (uint)new Random().Next(0, int.MaxValue);
        }

        [Test]
        public void ConstructorThrowsForNullTransportProtocol()
        {
            Assert.Throws(typeof(ArgumentNullException), () => new ServerProtocol(null));
        }

        [Test]
        public void ConstructorRegistersMethodsWithTransportProtocol()
        {
            // Arrange
            var cc = new CapturingConstraint<ServerProtocol>();

            // Exoect
            this.transportProtocol.Expect(p => p.RegisterMethodsIn(Arg<ServerProtocol>.Matches(cc)));

            // Act
            this.serverProtocol = new ServerProtocol(this.transportProtocol);

            // Assert
            this.transportProtocol.VerifyAllExpectations();
            Assert.AreEqual(this.serverProtocol, cc.Argument);
        }

        [Test]
        public void ReceiveEntityRegistrationRequestPassesRequestToServer()
        {
            // Arrange
            this.CreateServerProtocolAndStubEnvelope();

            // Expect
            this.server.Expect(a => a.ReceiveEntityRegistrationRequest(
                this.clientAddress,
                this.entityType,
                this.badumnaId,
                null,
                this.sceneName));

            // Act
            this.serverProtocol.ReceiveEntityRegistrationRequest(
                this.entityType,
                this.badumnaId,
                this.sceneName);

            // Verify
            this.server.VerifyAllExpectations();
        }

        [Test]
        public void ReceiveEntityTransitionRequestPassesRequestToServer()
        {
            // Arrange
            this.CreateServerProtocolAndStubEnvelope();
            var milestone = new SignedEntityState();

            // Expect
            this.server.Expect(a => a.ReceiveEntityRegistrationRequest(
                this.clientAddress,
                this.entityType,
                this.badumnaId,
                milestone,
                this.sceneName));

            // Act
            this.serverProtocol.ReceiveEntityTransitionRequest(
                this.entityType,
                this.badumnaId,
                milestone,
                this.sceneName);

            // Verify
            this.server.VerifyAllExpectations();
        }

        [Test]
        public void ReceiveEntityUnregistrationRequestPassesRequestToServer()
        {
            // Arrange
            this.CreateServerProtocolAndStubEnvelope();

            // Expect
            this.server.Expect(s => s.ReceiveEntityUnregistrationRequest(
                this.clientAddress,
                this.localId));

            // Act
            this.serverProtocol.ReceiveEntityUnregistrationRequest(this.localId);

            // Verify
            this.server.VerifyAllExpectations();
        }

        [Test]
        public void ReceiveEntityInputPassesInputToServer()
        {
            // Arrange
            this.CreateServerProtocolAndStubEnvelope();
            var serialNumber = new CyclicalID.UShortID(7);
            var period = TimeSpan.FromMilliseconds(56);
            var userInput = new byte[0];
            bool replyRequested = true;

            // Expect
            this.server.Expect(a => a.ReceiveEntityInput(
                this.clientAddress,
                this.localId,
                serialNumber,
                period,
                userInput,
                replyRequested));

            // Act
            this.serverProtocol.ReceiveEntityInput(this.localId, serialNumber, period.Ticks, userInput, replyRequested);

            // Verify
            this.server.VerifyAllExpectations();
        }

        [Test]
        public void ReceiveSceneRegistrationRequestPassesRequestToServer()
        {
            // Arrange
            this.CreateServerProtocolAndStubEnvelope();
            var sceneName = new QualifiedName("", Guid.NewGuid().ToString());

            // Expect
            this.server.Expect(
                s => s.ReceiveSceneRegistrationRequest(this.clientAddress, this.localId, sceneName));

            // Act
            this.serverProtocol.ReceiveSceneRegistrationRequest(this.localId, sceneName);

            // Verify
            this.server.VerifyAllExpectations();
        }

        [Test]
        public void ReceiveSceneUnregistrationRequestPassesRequestToServer()
        {
            // Arrange
            this.CreateServerProtocolAndStubEnvelope();
            var sceneName = new QualifiedName("", Guid.NewGuid().ToString());

            // Expect
            this.server.Expect(
                s => s.ReceiveSceneUnregistrationRequest(this.clientAddress, this.localId, sceneName));

            // Act
            this.serverProtocol.ReceiveSceneUnregistrationRequest(this.localId, sceneName);

            // Verify
            this.server.VerifyAllExpectations();
        }

        [Test]
        public void ReceiveTerminationNoticePassesNoticeToServer()
        {
            // Arrange
            this.CreateServerProtocolAndStubEnvelope();

            // Expect
            this.server.Expect(s => s.ReceiveTerminationNotice(this.clientAddress));

            // Act
            this.serverProtocol.ReceiveTerminationNotice();

            // Verify
            this.server.VerifyAllExpectations();
        }

        [Test]
        public void ReceivePingRequestPassesRequestToServer()
        {
            // Arrange
            this.CreateServerProtocolAndStubEnvelope();

            // Expect
            this.server.Expect(s => s.ReceivePingRequest(this.clientAddress));

            // Act
            this.serverProtocol.ReceivePingRequest();

            // Verify
            this.server.VerifyAllExpectations();
        }

        private void CreateServerProtocolAndStubEnvelope()
        {
            this.CreateServerProtocol();
            this.StubCurrentEnvelope();
        }
        
        private void CreateServerProtocol()
        {
            this.serverProtocol = new ServerProtocol(this.transportProtocol);
            this.serverProtocol.Server = this.server;
        }

        private void StubCurrentEnvelope()
        {
            TransportEnvelope envelope = new TransportEnvelope();
            envelope.Source = this.clientAddress;
            this.transportProtocol.Stub(p => p.CurrentEnvelope).Return(envelope);
        }
    }
}
