﻿//-----------------------------------------------------------------------
// <copyright file="SessionTests.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BadumnaTests.Validation.HostingTests
{
    using System;
    using Badumna;
    using Badumna.Core;
    using Badumna.DataTypes;
    using Badumna.SpatialEntities;
    using Badumna.Transport;
    using Badumna.Utilities;
    using Badumna.Validation;
    using Badumna.Validation.Hosting;
    using NUnit.Framework;
    using Rhino.Mocks;

    [TestFixture]
    [DontPerformCoverage]
    public class SessionTests
    {
        private PeerAddress clientAddress;
        private IClientProtocol clientProtocol;
        private ITransportProtocol transportProtocol;
        private ISpatialEntityManager spatialEntityManager;
        private INetworkFacade networkFacade;
        private INetworkAddressProvider addressProvider;
        private CreateValidatedEntity validatedEntityFactory;
        private RemoveValidatedEntity validatedEntityRemovalHandler;
        private HostedEntityWrapperFactory wrapperFactory;
        private Session session;
        private IValidatedEntity entity;
        private ushort localId;
        private BadumnaId badumnaId;
        private ISignedEntityState milestone;
        private IOrdered<ISerializedEntityState> serialStateUpdate;
        private ISerializedEntityState stateUpdate;
        private IHostedEntityWrapper wrapper;
        private CyclicalID.UShortID serialNumber;
        private uint entityType;
        private QualifiedName sceneName;

        [SetUp]
        public void SetUp()
        {
            this.clientAddress = PeerAddress.GetRandomAddress();
            this.clientProtocol = MockRepository.GenerateMock<IClientProtocol>();
            this.spatialEntityManager = MockRepository.GenerateMock<ISpatialEntityManager>();
            this.networkFacade = MockRepository.GenerateMock<INetworkFacade>();
            this.addressProvider = MockRepository.GenerateStub<INetworkAddressProvider>();
            this.transportProtocol = MockRepository.GenerateMock<ITransportProtocol>();
            this.validatedEntityFactory = MockRepository.GenerateMock<CreateValidatedEntity>();
            this.validatedEntityRemovalHandler = MockRepository.GenerateMock<RemoveValidatedEntity>();
            this.wrapperFactory = MockRepository.GenerateMock<HostedEntityWrapperFactory>();
            this.entity = MockRepository.GenerateMock<IValidatedEntity>();
            this.localId = (ushort)new Random().Next(ushort.MaxValue);
            this.badumnaId = new BadumnaId(PeerAddress.GetRandomAddress(), this.localId);
            this.milestone = MockRepository.GenerateMock<ISignedEntityState>();
            this.serialStateUpdate = MockRepository.GenerateMock<IOrdered<ISerializedEntityState>>();
            this.stateUpdate = MockRepository.GenerateMock<ISerializedEntityState>();
            this.wrapper = MockRepository.GenerateMock<IHostedEntityWrapper>();

            this.milestone.Stub(m => m.SerialEntityState).Return(this.serialStateUpdate);
            this.serialStateUpdate.Stub(u => u.Item).Return(this.stateUpdate);
            this.serialNumber = new CyclicalID.UShortID(0);
            this.entityType = (uint)new Random().Next(0, int.MaxValue);
            this.sceneName = new QualifiedName("", Guid.NewGuid().ToString());
        }

        [Test]
        public void ConstructorThrowsForNullClientAddress()
        {
            this.clientAddress = null;
            Assert.Throws(typeof(ArgumentNullException), this.ConstructServer);
        }

        [Test]
        public void ConstructorThrowsForNullClientProtocol()
        {
            this.clientProtocol = null;
            Assert.Throws(typeof(ArgumentNullException), this.ConstructServer);
        }

        [Test]
        public void ConstructorThrowsForNullTransportProtocol()
        {
            this.transportProtocol = null;
            Assert.Throws(typeof(ArgumentNullException), this.ConstructServer);
        }

        [Test]
        public void ConstructorThrowsForNullSpatialEntityManager()
        {
            this.spatialEntityManager = null;
            Assert.Throws(typeof(ArgumentNullException), this.ConstructServer);
        }

        [Test]
        public void ConstructorThrowsForNullValidatedEntityFactory()
        {
            this.validatedEntityFactory = null;
            Assert.Throws(typeof(ArgumentNullException), this.ConstructServer);
        }

        [Test]
        public void ConstructorThrowsForNullValidatedEntityRemovalHandler()
        {
            this.validatedEntityRemovalHandler = null;
            Assert.Throws(typeof(ArgumentNullException), this.ConstructServer);
        }

        [Test]
        public void ConstructorThrowsForNullWrapperFactory()
        {
            this.wrapperFactory = null;
            Assert.Throws(typeof(ArgumentNullException), this.ConstructServer);
        }

        [Test]
        public void ReceiveEntityRegistrationRequestInvokesEntityFactoryForNewEntity()
        {
            // Arrange
            this.ConstructServer();
            this.wrapperFactory.Stub(f => f.Invoke(
                this.transportProtocol,
                this.clientProtocol,
                this.entity,
                this.entityType,
                this.badumnaId,
                this.serialNumber,
                this.clientAddress,
                this.spatialEntityManager))
                .Return(this.wrapper);

            // Expect
            this.validatedEntityFactory.Expect(f => f.Invoke(this.entityType))
                .Return(this.entity);

            // Act
            this.session.ReceiveEntityRegistrationRequest(
                this.entityType,
                this.badumnaId,
                this.milestone,
                this.sceneName);

            // Verify
            this.validatedEntityFactory.VerifyAllExpectations();
        }

        [Test]
        public void ReceiveEntityRegistrationRequestCreatesHostedEntityWrapperForNewEntity()
        {
            // Arrange
            this.ConstructServer();
            this.validatedEntityFactory.Stub(f => f.Invoke(this.entityType))
                .Return(this.entity);

            // Expect
            this.wrapperFactory.Expect(f => f.Invoke(
                this.transportProtocol,
                this.clientProtocol,
                this.entity,
                this.entityType,
                this.badumnaId,
                this.serialNumber,
                this.clientAddress,
                this.spatialEntityManager))
                .Return(this.wrapper);

            // Act
            this.session.ReceiveEntityRegistrationRequest(
                this.entityType,
                this.badumnaId,
                this.milestone,
                this.sceneName);

            // Verify
            this.wrapperFactory.VerifyAllExpectations();
        }

        [Test]
        public void ReceiveEntityRegistrationRequestDoesNotCreatesHostedEntityWrapperForKnownEntity()
        {
            // Arrange
            this.ConstructServer();
            this.CompleteEntityRegistration();

            // Expect
            this.wrapperFactory.Expect(f => f.Invoke(
                null,
                null,
                null,
                (uint)0,
                this.badumnaId,
                this.serialNumber,
                null,
                null))
                .IgnoreArguments()
                .Repeat.Never();

            // Act
            this.session.ReceiveEntityRegistrationRequest(
                this.entityType,
                this.badumnaId,
                this.milestone,
                this.sceneName);

            // Verify
            this.wrapperFactory.VerifyAllExpectations();
        }

        [Test]
        public void ReceiveEntityUnregistrationRequestTerminatesWrapperForKnownEntity()
        {
            // Arrange
            this.ConstructServer();
            this.CompleteEntityRegistration();

            // Expect
            this.wrapper.Expect(w => w.Terminate());

            // Act
            this.session.ReceiveEntityUnregistrationRequest(this.localId);

            // Verify
            this.wrapper.VerifyAllExpectations();
        }

        [Test]
        public void ReceiveEntityUnregistrationRequestInvokesRemovalHandlerForKnownEntity()
        {
            // Arrange
            this.ConstructServer();
            this.CompleteEntityRegistration();
            this.wrapper.Stub(w => w.Entity).Return(this.entity);

            // Expect
            this.validatedEntityRemovalHandler.Expect(h => h.Invoke(this.entity));

            // Act
            this.session.ReceiveEntityUnregistrationRequest(this.localId);

            // Verify
            this.validatedEntityRemovalHandler.VerifyAllExpectations();
        }

        [Test]
        public void ReceiveEntityUnregistrationRequestDoesNotInvokeRemovalHandlerForUnknownEntity()
        {
            // Arrange
            this.ConstructServer();
            this.CompleteEntityRegistration();
            this.wrapper.Stub(w => w.Entity).Return(this.entity);
            ushort unknownId = 0;
            if (unknownId == this.localId)
            {
                unknownId++;
            }

            // Expect
            this.validatedEntityRemovalHandler.Expect(h => h.Invoke(Arg<IValidatedEntity>.Is.Anything))
                .Repeat.Never();

            // Act
            this.session.ReceiveEntityUnregistrationRequest(unknownId);

            // Verify
            this.validatedEntityRemovalHandler.VerifyAllExpectations();
        }

        [Test]
        public void ReceiveEntityInputForKnownEntityPassesInputToWrapper()
        {
            // Arrange
            this.ConstructServer();
            this.CompleteEntityRegistration();
            var serialNumber = new CyclicalID.UShortID(555);
            var timePeriod = TimeSpan.FromMilliseconds(234);
            var input = new byte[0];
            bool replyRequested = true;

            // Expect
            this.wrapper.Expect(w => w.ReceiveInput(serialNumber, timePeriod, input, replyRequested)).IgnoreArguments();

            // Act
            this.session.ReceiveEntityInput(this.localId, serialNumber, timePeriod, input, replyRequested);

            // Verify
            this.wrapper.VerifyAllExpectations();
        }

        [Test]
        public void ReceiveEntityInputForUnknownEntityTriggersErrorMessage()
        {
            // Arrange
            this.ConstructServer();
            var serialNumber = new CyclicalID.UShortID(555);
            var timePeriod = TimeSpan.FromMilliseconds(234);
            var input = new byte[0];
            bool replyRequested = true;

            // Expect
            this.transportProtocol.Expect(p => p.SendRemoteCall(
                this.clientAddress,
                QualityOfService.Reliable,
                this.clientProtocol.ReceiveServerError,
                this.localId));

            // Act
            this.session.ReceiveEntityInput(this.localId, serialNumber, timePeriod, input, replyRequested);

            // Verify
            this.transportProtocol.VerifyAllExpectations();
        }

        [Test]
        public void ReceiveSceneRegistrationRequestForKnownEntityPassesRequestToWrapper()
        {
            // Arrange
            this.ConstructServer();
            this.CompleteEntityRegistration();

            // Expect
            this.wrapper.Expect(w => w.RegisterWithScene(this.sceneName));

            // Act
            this.session.ReceiveSceneRegistrationRequest(this.localId, this.sceneName);

            // Verify
            this.wrapper.VerifyAllExpectations();
        }

        [Test]
        public void ReceiveSceneUnregistrationRequestForKnownEntityPassesRequestToWrapper()
        {
            // Arrange
            this.ConstructServer();
            this.CompleteEntityRegistration();

            // Expect
            this.wrapper.Expect(w => w.UnregisterFromScene(this.sceneName));

            // Act
            this.session.ReceiveSceneUnregistrationRequest(this.localId, this.sceneName);

            // Verify
            this.wrapper.VerifyAllExpectations();
        }

        [Test]
        public void ReceiveTerminationNoticeTerminatesAllWrappers()
        {
            // Arrange
            this.ConstructServer();
            this.CompleteEntityRegistration();

            // Expect
            this.wrapper.Expect(w => w.Terminate());

            // Act
            this.session.ReceiveTerminationNotice();

            // Verify
            this.wrapper.VerifyAllExpectations();
        }

        [Test]
        public void SendMilestonesTellsWrappersToSendMilestones()
        {
            // Arrange
            this.ConstructServer();
            this.CompleteEntityRegistration();

            // Expect
            this.wrapper.Expect(w => w.SendMilestone());

            // Act
            this.session.SendMilestones();

            // Verify
            this.wrapper.VerifyAllExpectations();
        }

        private void ConstructServer()
        {
            this.session = new Session(
                this.clientAddress,
                this.clientProtocol,
                this.transportProtocol,
                this.spatialEntityManager,
                this.networkFacade,
                this.addressProvider,
                this.validatedEntityFactory,
                this.validatedEntityRemovalHandler,
                this.wrapperFactory);
        }

        private void CompleteEntityRegistration()
        {
            this.validatedEntityFactory.Stub(f => f.Invoke(this.entityType))
                .IgnoreArguments()
                .Return(this.entity);
            this.wrapperFactory.Stub(f => f.Invoke(
                this.transportProtocol,
                this.clientProtocol,
                this.entity,
                this.entityType,
                this.badumnaId,
                new CyclicalID.UShortID(0),
                this.clientAddress,
                this.spatialEntityManager))
                .Return(this.wrapper)
                .Repeat.Once();
            this.session.ReceiveEntityRegistrationRequest(
                this.entityType,
                this.badumnaId,
                this.milestone,
                this.sceneName);
        }
    }
}
