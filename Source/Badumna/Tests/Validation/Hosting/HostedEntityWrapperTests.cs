﻿//////-----------------------------------------------------------------------
////// <copyright file="HostedEntityWrapperTests.cs" company="NICTA">
//////     Copyright (c) 2010 NICTA. All rights reserved.
////// </copyright>
//////-----------------------------------------------------------------------

////namespace BadumnaTests.Validation.HostingTests
////{
////    using System;
////    using Badumna.Arbitration;
////    using Badumna.Core;
////    using Badumna.DataTypes;
////    using Badumna.SpatialEntities;
////    using Badumna.Transport;
////    using Badumna.Utilities;
////    using Badumna.Validation;
////    using Badumna.Validation.Hosting;
////    using NUnit.Framework;
////    using Rhino.Mocks;
////    using TestUtilities;

////    [TestFixture]
////    [DontPerformCoverage]
////    public class HostedEntityWrapperTests
////    {
////        private ITransportProtocol transportProtocol;
////        private IClientProtocol clientProtocol;
////        private SequencerFactory<IFlagged<IOrdered<ITimedInput>>> sequencerFactory;
////        private IValidatedEntity entity;
////        private uint entityType;
////        private ushort localId;
////        private BadumnaId badumnaId;
////        private PeerAddress clientAddress;
////        private ISpatialEntityManager spatialEntityManager;
////        private HostedEntityWrapper wrapper;
////        private CyclicalID.UShortID serialNumber;
////        private ISerializedEntityState entityState;
////        private ISerializedEntityState milestone;
////        private NetworkScene scene;

////        [SetUp]
////        public void SetUp()
////        {
////            this.transportProtocol = MockRepository.GenerateMock<ITransportProtocol>();
////            this.clientProtocol = MockRepository.GenerateMock<IClientProtocol>();
////            this.sequencerFactory = MockRepository.GenerateMock<SequencerFactory<IFlagged<IOrdered<ITimedInput>>>>();
////            this.entity = MockRepository.GenerateMock<IValidatedEntity>();
////            this.entityType = (uint)new Random().Next(0, int.MaxValue);
////            this.localId = (ushort)new Random().Next(ushort.MaxValue);
////            this.badumnaId = new BadumnaId(PeerAddress.GetRandomAddress(), this.localId);
////            this.clientAddress = PeerAddress.GetRandomAddress();
////            this.spatialEntityManager = MockRepository.GenerateMock<ISpatialEntityManager>();
////            this.serialNumber = new CyclicalID.UShortID(0);
////            this.entityState = MockRepository.GenerateMock<ISerializedEntityState>();
////            this.milestone = MockRepository.GenerateMock<ISerializedEntityState>();
////            this.scene = MockRepository.GenerateMock<NetworkScene>();
////        }

////        [Test]
////        public void ConstructorThrowsForNullTransportProtocol()
////        {
////            this.transportProtocol = null;
////            Assert.Throws(typeof(ArgumentNullException), this.ConstructWrapperWithStubbedEntitySaveStateAndCreateUpdate);
////        }

////        [Test]
////        public void ConstructorThrowsForNullClientProtocol()
////        {
////            this.clientProtocol = null;
////            Assert.Throws(typeof(ArgumentNullException), this.ConstructWrapperWithStubbedEntitySaveStateAndCreateUpdate);
////        }

////        [Test]
////        public void ConstructorThrowsForNullSequencerFactory()
////        {
////            this.sequencerFactory = null;
////            Assert.Throws(typeof(ArgumentNullException), this.ConstructWrapperWithStubbedEntitySaveStateAndCreateUpdate);
////        }

////        [Test]
////        public void ConstructorThrowsForNullEntity()
////        {
////            this.entity = null;
////            Assert.Throws(typeof(ArgumentNullException), this.ConstructWrapper);
////        }

////        [Test]
////        public void ConstructorThrowsForNullBadumnaId()
////        {
////            this.badumnaId = null;
////            Assert.Throws(typeof(ArgumentNullException), this.ConstructWrapperWithStubbedEntitySaveStateAndCreateUpdate);
////        }

////        [Test]
////        public void ConstructorThrowsForNullClientAddress()
////        {
////            this.clientAddress = null;
////            Assert.Throws(typeof(ArgumentNullException), this.ConstructWrapperWithStubbedEntitySaveStateAndCreateUpdate);
////        }

////        [Test]
////        public void ConstructorThrowsForNullSpatialEntityManager()
////        {
////            this.spatialEntityManager = null;
////            Assert.Throws(typeof(ArgumentNullException), this.ConstructWrapperWithStubbedEntitySaveStateAndCreateUpdate);
////        }

////        [Test]
////        public void ConstructorCreatesDefaultSequencerFactory()
////        {
////            // Test passes if no exception thrown
////            this.entity.Stub(e => e.SaveState()).Return(this.milestone);
////            this.wrapper = new HostedEntityWrapper(
////                this.transportProtocol,
////                this.clientProtocol,
////                this.entity,
////                this.entityType,
////                this.badumnaId,
////                this.serialNumber,
////                this.clientAddress,
////                this.spatialEntityManager);

////            // Constructor will also invoke factory, so this test will fail if the default factory does.
////        }

////        [Test]
////        public void ConstructorInvokesSequencerFactory()
////        {
////            // Expect
////            this.sequencerFactory.Expect(f => f.Invoke(Arg<GenericCallBack<IFlagged<IOrdered<ITimedInput>>>>.Is.Anything));

////            // Act
////            this.ConstructWrapperWithStubbedEntitySaveStateAndCreateUpdate();

////            // Verify
////            this.sequencerFactory.VerifyAllExpectations();
////        }

////        [Test]
////        public void ConstructorSendsInitialMilestoneToClientForZeroSerialNumber()
////        {
////            // Expect
////            this.transportProtocol.Expect(p => p.SendRemoteCall(
////                Arg.Is(this.clientAddress),
////                Arg.Is(QualityOfService.Reliable),
////                Arg<GenericCallBack<ushort, CyclicalID.UShortID, SerializedEntityState, byte[]>>.Is.Anything,
////                Arg.Is(this.localId),
////                Arg.Is(this.serialNumber),
////                Arg<SerializedEntityState>.Is.Anything,
////                Arg.Is(new byte[0])));

////            // Act
////            this.ConstructWrapperWithDefaultSequencerFactoryAndStubbedEntitySaveStateAndCreateUpdate();

////            // Verify
////            this.transportProtocol.VerifyAllExpectations();
////        }

////        [Test]
////        public void ConstructorSendsInitialMilestoneToClientForNonMilestoneUpdates()
////        {
////            // Arrange
////            this.serialNumber.Increment();

////            // Expect
////            this.transportProtocol.Expect(p => p.SendRemoteCall(
////                Arg.Is(this.clientAddress),
////                Arg.Is(QualityOfService.Reliable),
////                Arg<GenericCallBack<ushort, CyclicalID.UShortID, SerializedEntityState, byte[]>>.Is.Anything,
////                Arg.Is(this.localId),
////                Arg.Is(this.serialNumber),
////                Arg<SerializedEntityState>.Is.Anything,
////                Arg.Is(new byte[0])));

////            // Act
////            this.ConstructWrapperWithDefaultSequencerFactoryAndStubbedEntitySaveStateAndCreateUpdate();

////            // Verify
////            this.transportProtocol.VerifyAllExpectations();
////        }

////        [Test]
////        public void ReceiveInputPassesInputToSequencer()
////        {
////            // Arrange
////            var sequencer = MockRepository.GenerateMock<ISequencer<IFlagged<IOrdered<ITimedInput>>>>();
////            this.sequencerFactory.Stub(f => f.Invoke(null))
////                .IgnoreArguments()
////                .Return(sequencer);
////            this.ConstructWrapperWithStubbedEntitySaveStateAndCreateUpdate();

////            var serialNumber = new CyclicalID.UShortID(879);
////            var interval = TimeSpan.FromMilliseconds(99999);
////            var input = new byte[0];
////            var cc = new CapturingConstraint<IFlagged<IOrdered<ITimedInput>>>();

////            // Expect
////            sequencer.Expect(s => s.ReceiveEvent(
////                Arg<CyclicalID.UShortID>.Is.Equal(serialNumber),
////                Arg<IFlagged<IOrdered<ITimedInput>>>.Matches(cc)));

////            // Act
////            this.wrapper.ReceiveInput(serialNumber, interval, input, true);

////            // Verify
////            sequencer.VerifyAllExpectations();
////            Assert.AreEqual(serialNumber, cc.Argument.Item.SerialNumber);
////            Assert.AreEqual(interval, cc.Argument.Item.Item.TimePeriod);
////            Assert.AreEqual(input, cc.Argument.Item.Item.UserInput);
////            Assert.IsTrue(cc.Argument.Flag);
////        }

////        [Test]
////        public void ReceiveInputTriggersUpdatesEntityForInOrderUpdatesWhenReplyRequested()
////        {
////            // Arrange
////            this.ConstructWrapperWithDefaultSequencerFactoryAndStubbedEntitySaveStateAndCreateUpdate();

////            var serialNumber = new CyclicalID.UShortID(0);
////            var interval = TimeSpan.FromMilliseconds(99999);
////            var input = new byte[0];
////            var cc = new CapturingConstraint<IOrdered<ITimedInput>>();
////            var entityState = MockRepository.GenerateMock<ISerializedEntityState>();
////            this.entity.Stub(e => e.CreateUpdate()).Return(entityState);

////            // Expect
////            this.entity.Expect(e => e.Update(interval, input, UpdateMode.Authoritative));

////            // Act
////            this.wrapper.ReceiveInput(serialNumber, interval, input, true);

////            // Verify
////            this.entity.VerifyAllExpectations();
////        }

////        [Test]
////        public void ReceiveInputTriggersUpdatesEntityForInOrderUpdatesWhenReplyNotRequested()
////        {
////            // Arrange
////            this.ConstructWrapperWithDefaultSequencerFactoryAndStubbedEntitySaveStateAndCreateUpdate();

////            var serialNumber = new CyclicalID.UShortID(0);
////            var interval = TimeSpan.FromMilliseconds(99999);
////            var input = new byte[0];
////            var cc = new CapturingConstraint<IOrdered<ITimedInput>>();
////            var entityState = MockRepository.GenerateMock<ISerializedEntityState>();
////            this.entity.Stub(e => e.CreateUpdate()).Return(entityState);

////            // Expect
////            this.entity.Expect(e => e.Update(interval, input, UpdateMode.Authoritative));

////            // Act
////            this.wrapper.ReceiveInput(serialNumber, interval, input, false);

////            // Verify
////            this.entity.VerifyAllExpectations();
////        }

////        [Test]
////        public void ReceiveInputDoesNotTriggerUpdateEntityForOutOfOrderUpdates()
////        {
////            // Arrange
////            this.ConstructWrapperWithDefaultSequencerFactoryAndStubbedEntitySaveStateAndCreateUpdate();

////            var serialNumber = new CyclicalID.UShortID(1);
////            var interval = TimeSpan.FromMilliseconds(99999);
////            var input = new byte[0];
////            var cc = new CapturingConstraint<IOrdered<ITimedInput>>();

////            // Expect
////            this.entity.Expect(e => e.Update(interval, input, UpdateMode.Authoritative))
////                .Repeat.Never();

////            // Act
////            this.wrapper.ReceiveInput(serialNumber, interval, input, true);

////            // Verify
////            this.entity.VerifyAllExpectations();
////        }

////        [Test]
////        public void ReceiveInputTriggersMilestoneToClientForMilestoneSerialNumbersWhenReplyRequested()
////        {
////            // Arrange
////            this.ConstructWrapperWithDefaultSequencerFactoryAndStubbedEntitySaveStateAndCreateUpdate();

////            var serialNumber = new CyclicalID.UShortID(0);
////            var interval = TimeSpan.FromMilliseconds(99999);
////            var input = new byte[0];
////            var cc = new CapturingConstraint<IOrdered<ITimedInput>>();
////            var entityState = MockRepository.GenerateMock<ISerializedEntityState>();

////            while (serialNumber < new CyclicalID.UShortID(49))
////            {
////                this.wrapper.ReceiveInput(serialNumber, interval, input, true);
////                serialNumber.Increment();
////            }

////            // Expect
////            var expectedUpdateNumber = new CyclicalID.UShortID(serialNumber.Value);
////            expectedUpdateNumber.Increment();
////            this.transportProtocol.Expect(p => p.SendRemoteCall(
////                Arg<PeerAddress>.Is.Equal(this.clientAddress),
////                Arg<QualityOfService>.Is.Equal(QualityOfService.Reliable),
////                Arg<GenericCallBack<ushort, CyclicalID.UShortID, SerializedEntityState, byte[]>>.Is.Equal((GenericCallBack<ushort, CyclicalID.UShortID, SerializedEntityState, byte[]>)this.clientProtocol.ReceiveEntityStateMilestone),
////                Arg.Is(this.localId),
////                Arg.Is(expectedUpdateNumber),
////                Arg<SerializedEntityState>.Is.Anything,
////                Arg.Is(new byte[0])));

////            // Act
////            this.wrapper.ReceiveInput(serialNumber, interval, input, true);

////            // Verify
////            this.transportProtocol.VerifyAllExpectations();
////        }

////        [Test]
////        public void ReceiveInputTriggersMilestoneIncludingFullUpdateForMilestoneSerialNumbersWhenReplyRequested()
////        {
////            // Arrange
////            this.entity.Stub(e => e.SaveState()).Return(this.milestone).Repeat.Once();
////            this.entity.Stub(e => e.CreateUpdate()).Return(this.entityState);
////            this.ConstructWrapperWithDefaultSequencerFactory();

////            var serialNumber = new CyclicalID.UShortID(0);
////            var interval = TimeSpan.FromMilliseconds(99999);
////            var input = new byte[0];
////            ////var cc = new CapturingConstraint<IOrdered<ITimedInput>>();
////            ////var entityState = MockRepository.GenerateMock<IEntityState>();

////            while (serialNumber < new CyclicalID.UShortID(49))
////            {
////                this.wrapper.ReceiveInput(serialNumber, interval, input, true);
////                serialNumber.Increment();
////            }

////            // Expect
////            this.entity.Expect(e => e.SaveState()).Return(this.milestone);

////            // Act
////            this.wrapper.ReceiveInput(serialNumber, interval, input, true);

////            // Verify
////            this.entity.VerifyAllExpectations();
////        }

////        [Test]
////        public void ReceiveInputTriggersNoMilestoneToClientForMilestoneSerialNumbersWhenReplyNotRequested()
////        {
////            // Arrange
////            this.ConstructWrapperWithDefaultSequencerFactoryAndStubbedEntitySaveStateAndCreateUpdate();

////            var serialNumber = new CyclicalID.UShortID(0);
////            var interval = TimeSpan.FromMilliseconds(99999);
////            var input = new byte[0];
////            var cc = new CapturingConstraint<IOrdered<ITimedInput>>();
////            var entityState = MockRepository.GenerateMock<ISerializedEntityState>();

////            while (serialNumber < new CyclicalID.UShortID(49))
////            {
////                this.wrapper.ReceiveInput(serialNumber, interval, input, false);
////                serialNumber.Increment();
////            }

////            // Expect
////            this.transportProtocol.Expect(p => p.SendRemoteCall(
////                Arg<PeerAddress>.Is.Equal(this.clientAddress),
////                Arg<QualityOfService>.Is.Equal(QualityOfService.Reliable),
////                Arg<GenericCallBack<ushort, CyclicalID.UShortID, SerializedEntityState, byte[]>>.Is.Equal((GenericCallBack<ushort, CyclicalID.UShortID, SerializedEntityState, byte[]>)this.clientProtocol.ReceiveEntityStateMilestone),
////                Arg.Is(this.localId),
////                Arg.Is(serialNumber),
////                Arg<SerializedEntityState>.Is.Anything,
////                Arg.Is(new byte[0])))
////                .Repeat.Never();

////            // Act
////            this.wrapper.ReceiveInput(serialNumber, interval, input, false);

////            // Verify
////            this.transportProtocol.VerifyAllExpectations();
////        }

////        [Test]
////        public void ReceiveInputTriggersUpdateToClientForNonMilestoneSerialNumbersWhenReplyRequired()
////        {
////            // Arrange
////            this.ConstructWrapperWithDefaultSequencerFactoryAndStubbedEntitySaveStateAndCreateUpdate();

////            var serialNumber = new CyclicalID.UShortID(0);
////            var interval = TimeSpan.FromMilliseconds(99999);
////            var input = new byte[0];
////            var cc = new CapturingConstraint<IOrdered<ITimedInput>>();
////            var entityState = MockRepository.GenerateMock<ISerializedEntityState>();

////            // Expect
////            var expectedUpdateNumber = new CyclicalID.UShortID(serialNumber.Value);
////            expectedUpdateNumber.Increment();
////            this.transportProtocol.Expect(p => p.SendRemoteCall(
////                Arg<PeerAddress>.Is.Equal(this.clientAddress),
////                Arg<QualityOfService>.Is.Equal(QualityOfService.Reliable),
////                Arg<GenericCallBack<ushort, CyclicalID.UShortID, SerializedEntityState>>.Is.Equal((GenericCallBack<ushort, CyclicalID.UShortID, SerializedEntityState>)this.clientProtocol.ReceiveEntityStateUpdate),
////                Arg.Is(this.localId),
////                Arg.Is(expectedUpdateNumber),
////                Arg<SerializedEntityState>.Is.Anything));

////            // Act
////            this.wrapper.ReceiveInput(serialNumber, interval, input, true);

////            // Verify
////            this.transportProtocol.VerifyAllExpectations();
////        }

////        [Test]
////        public void ReceiveInputTriggersUpdateWithDeltaUpdateForNonMilestoneSerialNumbersWhenReplyRequired()
////        {
////            // Arrange
////            this.entity.Stub(e => e.SaveState()).Return(this.milestone).Repeat.Once();

////            this.ConstructWrapperWithDefaultSequencerFactory();

////            var serialNumber = new CyclicalID.UShortID(0);
////            var interval = TimeSpan.FromMilliseconds(99999);
////            var input = new byte[0];
////            var cc = new CapturingConstraint<IOrdered<ITimedInput>>();
////            var entityState = MockRepository.GenerateMock<ISerializedEntityState>();

////            // Expect
////            this.entity.Expect(e => e.CreateUpdate()).Return(this.entityState);

////            // Act
////            this.wrapper.ReceiveInput(serialNumber, interval, input, true);

////            // Verify
////            this.entity.VerifyAllExpectations();
////        }

////        [Test]
////        public void ReceiveInputTriggersNoUpdateToClientForNonMilestoneSerialNumbersWhenReplyNotRequired()
////        {
////            // Arrange
////            this.ConstructWrapperWithDefaultSequencerFactoryAndStubbedEntitySaveStateAndCreateUpdate();

////            var serialNumber = new CyclicalID.UShortID(0);
////            var interval = TimeSpan.FromMilliseconds(99999);
////            var input = new byte[0];
////            var cc = new CapturingConstraint<IOrdered<ITimedInput>>();
////            var entityState = MockRepository.GenerateMock<ISerializedEntityState>();

////            // Expect
////            this.transportProtocol.Expect(p => p.SendRemoteCall(
////                Arg<PeerAddress>.Is.Equal(this.clientAddress),
////                Arg<QualityOfService>.Is.Equal(QualityOfService.Reliable),
////                Arg<GenericCallBack<ushort, CyclicalID.UShortID, SerializedEntityState>>.Is.Equal((GenericCallBack<ushort, CyclicalID.UShortID, SerializedEntityState>)this.clientProtocol.ReceiveEntityStateUpdate),
////                Arg.Is(this.localId),
////                Arg.Is(serialNumber),
////                Arg<SerializedEntityState>.Is.Anything))
////                .Repeat.Never();

////            // Act
////            this.wrapper.ReceiveInput(serialNumber, interval, input, false);

////            // Verify
////            this.transportProtocol.VerifyAllExpectations();
////        }

////        [Test]
////        public void RegisterWithSceneJoinsNewScene()
////        {
////            // Arrange
////            this.ConstructWrapperWithDefaultSequencerFactoryAndStubbedEntitySaveStateAndCreateUpdate();
////            string sceneName = Guid.NewGuid().ToString();
////            this.scene.Stub(s => s.Name).Return(sceneName);

////            // Expect
////            this.spatialEntityManager.Expect(m => m.JoinScene(
////                Arg.Is(sceneName),
////                Arg<CreateSpatialReplica>.Is.Anything,
////                Arg<RemoveSpatialReplica>.Is.Anything,
////                Arg<bool>.Is.Equal(false)))
////                .Return(this.scene);

////            // Act
////            this.wrapper.RegisterWithScene(sceneName);

////            // Verify
////            this.spatialEntityManager.VerifyAllExpectations();
////        }

////        [Test]
////        public void RegisterWithScenePassesDelegatesThatThrow()
////        {
////            // Arrange
////            this.ConstructWrapperWithDefaultSequencerFactoryAndStubbedEntitySaveStateAndCreateUpdate();
////            string sceneName = Guid.NewGuid().ToString();
////            this.scene.Stub(s => s.Name).Return(sceneName);
////            var creationDelegate = new CapturingConstraint<CreateSpatialReplica>();
////            var removalDelegate = new CapturingConstraint<RemoveSpatialReplica>(); 

////            // Expect
////            this.spatialEntityManager.Expect(m => m.JoinScene(
////                Arg.Is(sceneName),
////                Arg<CreateSpatialReplica>.Matches(creationDelegate),
////                Arg<RemoveSpatialReplica>.Matches(removalDelegate),
////                Arg<bool>.Is.Equal(false)))
////                .Return(this.scene);

////            // Act
////            this.wrapper.RegisterWithScene(sceneName);

////            // Verify
////            this.spatialEntityManager.VerifyAllExpectations();
////            Assert.Throws(
////                typeof(ValidationException),
////                () => creationDelegate.Argument(this.scene, this.badumnaId, 0));
////            Assert.Throws(
////                typeof(ValidationException),
////                () => removalDelegate.Argument(this.scene, this.entity));
////        }

////        [Test]
////        public void RegisterWithSceneDoesNotJoinCurrentScene()
////        {
////            // Arrange
////            this.ConstructWrapperWithDefaultSequencerFactoryAndStubbedEntitySaveStateAndCreateUpdate();
////            string sceneName = Guid.NewGuid().ToString();
////            this.spatialEntityManager.Stub(m => m.JoinScene(
////                Arg.Is(sceneName),
////                Arg<CreateSpatialReplica>.Is.Anything,
////                Arg<RemoveSpatialReplica>.Is.Anything,
////                Arg<bool>.Is.Equal(false)))
////                .Return(this.scene);
////            this.scene.Stub(s => s.Name).Return(sceneName);
////            this.wrapper.RegisterWithScene(sceneName);

////            // Expect
////            this.spatialEntityManager.Expect(m => m.JoinScene(
////                Arg.Is(sceneName),
////                Arg<CreateSpatialReplica>.Is.Anything,
////                Arg<RemoveSpatialReplica>.Is.Anything,
////                Arg<bool>.Is.Equal(false)))
////                .Repeat.Never();

////            // Act
////            this.wrapper.RegisterWithScene(sceneName);

////            // Verify
////            this.spatialEntityManager.VerifyAllExpectations();
////        }

////        [Test]
////        public void RegisterWithSceneRegistersWithNewScene()
////        {
////            // Arrange
////            this.ConstructWrapperWithDefaultSequencerFactoryAndStubbedEntitySaveStateAndCreateUpdate();
////            string sceneName = Guid.NewGuid().ToString();
////            this.scene.Stub(s => s.Name).Return(sceneName);
////            this.spatialEntityManager.Stub(m => m.JoinScene(
////                Arg.Is(sceneName),
////                Arg<CreateSpatialReplica>.Is.Anything,
////                Arg<RemoveSpatialReplica>.Is.Anything,
////                Arg<bool>.Is.Equal(false)))
////                .Return(this.scene);

////            // Expect
////            this.scene.Expect(s => s.RegisterEntity(this.entity, this.entityType));

////            // Act
////            this.wrapper.RegisterWithScene(sceneName);

////            // Verify
////            this.scene.VerifyAllExpectations();
////        }

////        [Test]
////        public void RegisterWithSceneDoesNotRegistersWithCurrentScene()
////        {
////            // Arrange
////            this.ConstructWrapperWithDefaultSequencerFactoryAndStubbedEntitySaveStateAndCreateUpdate();
////            string sceneName = Guid.NewGuid().ToString();
////            this.scene.Stub(s => s.Name).Return(sceneName);
////            this.spatialEntityManager.Stub(m => m.JoinScene(
////                Arg.Is(sceneName),
////                Arg<CreateSpatialReplica>.Is.Anything,
////                Arg<RemoveSpatialReplica>.Is.Anything,
////                Arg<bool>.Is.Equal(false)))
////                .Return(this.scene)
////                .Repeat.Once();
////            this.wrapper.RegisterWithScene(sceneName);

////            // Expect
////            this.spatialEntityManager.Expect(m => m.JoinScene(
////                Arg.Is(sceneName),
////                Arg<CreateSpatialReplica>.Is.Anything,
////                Arg<RemoveSpatialReplica>.Is.Anything,
////                Arg<bool>.Is.Equal(false)))
////                .Repeat.Never();

////            // Act
////            this.wrapper.RegisterWithScene(sceneName);

////            // Verify
////            this.spatialEntityManager.VerifyAllExpectations();
////        }

////        [Test]
////        public void RegisterWithSceneLeavesCurrentSceneIfDifferent()
////        {
////            // Arrange
////            this.ConstructWrapperWithDefaultSequencerFactoryAndStubbedEntitySaveStateAndCreateUpdate();
////            string sceneName = Guid.NewGuid().ToString();
////            this.scene.Stub(s => s.Name).Return(sceneName);
////            string newSceneName = Guid.NewGuid().ToString();
////            NetworkScene newScene = MockRepository.GenerateMock<NetworkScene>();
////            newScene.Stub(n => n.Name).Return(newSceneName);
////            this.spatialEntityManager.Stub(m => m.JoinScene(
////                Arg.Is(sceneName),
////                Arg<CreateSpatialReplica>.Is.Anything,
////                Arg<RemoveSpatialReplica>.Is.Anything,
////                Arg<bool>.Is.Equal(false)))
////                .Return(this.scene)
////                .Repeat.Once();
////            this.wrapper.RegisterWithScene(sceneName);
////            this.spatialEntityManager.Stub(m => m.JoinScene(
////                Arg.Is(newSceneName),
////                Arg<CreateSpatialReplica>.Is.Anything,
////                Arg<RemoveSpatialReplica>.Is.Anything,
////                Arg<bool>.Is.Equal(false)))
////                .Return(newScene)
////                .Repeat.Once();

////            // Expect
////            this.scene.Expect(s => s.Leave());

////            // Act
////            this.wrapper.RegisterWithScene(newSceneName);

////            // Verify
////            this.scene.VerifyAllExpectations();
////        }

////        [Test]
////        public void RegisterWithSceneDoesNotLeaveCurrentSceneIfSame()
////        {
////            // Arrange
////            this.ConstructWrapperWithDefaultSequencerFactoryAndStubbedEntitySaveStateAndCreateUpdate();
////            string sceneName = Guid.NewGuid().ToString();
////            this.scene.Stub(s => s.Name).Return(sceneName);
////            string newSceneName = Guid.NewGuid().ToString();
////            this.spatialEntityManager.Stub(m => m.JoinScene(
////                Arg.Is(sceneName),
////                Arg<CreateSpatialReplica>.Is.Anything,
////                Arg<RemoveSpatialReplica>.Is.Anything,
////                Arg<bool>.Is.Equal(false)))
////                .Return(this.scene);
////            this.wrapper.RegisterWithScene(sceneName);

////            // Expect
////            this.scene.Expect(s => s.Leave()).Repeat.Never();

////            // Act
////            this.wrapper.RegisterWithScene(sceneName);

////            // Verify
////            this.scene.VerifyAllExpectations();
////        }

////        [Test]
////        public void UnregisterFromSceneUnregistersFromSceneIfRegistered()
////        {
////            // Arrange
////            this.ConstructWrapperWithDefaultSequencerFactoryAndStubbedEntitySaveStateAndCreateUpdate();
////            string sceneName = Guid.NewGuid().ToString();
////            this.scene.Stub(s => s.Name).Return(sceneName);
////            string newSceneName = Guid.NewGuid().ToString();
////            this.spatialEntityManager.Stub(m => m.JoinScene(
////                Arg.Is(sceneName),
////                Arg<CreateSpatialReplica>.Is.Anything,
////                Arg<RemoveSpatialReplica>.Is.Anything,
////                Arg<bool>.Is.Equal(false)))
////                .Return(this.scene);
////            this.wrapper.RegisterWithScene(sceneName);

////            // Expect
////            this.scene.Expect(s => s.UnregisterEntity(this.entity));

////            // Act
////            this.wrapper.UnregisterFromScene(sceneName);

////            // Verify
////            this.scene.VerifyAllExpectations();
////        }

////        [Test]
////        public void UnregisterFromSceneLeavesCurrentSceneIfSame()
////        {
////            // Arrange
////            this.ConstructWrapperWithDefaultSequencerFactoryAndStubbedEntitySaveStateAndCreateUpdate();
////            string sceneName = Guid.NewGuid().ToString();
////            this.scene.Stub(s => s.Name).Return(sceneName);
////            string newSceneName = Guid.NewGuid().ToString();
////            this.spatialEntityManager.Stub(m => m.JoinScene(
////                Arg.Is(sceneName),
////                Arg<CreateSpatialReplica>.Is.Anything,
////                Arg<RemoveSpatialReplica>.Is.Anything,
////                Arg<bool>.Is.Equal(false)))
////                .Return(this.scene);
////            this.wrapper.RegisterWithScene(sceneName);

////            // Expect
////            this.scene.Expect(s => s.Leave());

////            // Act
////            this.wrapper.UnregisterFromScene(sceneName);

////            // Verify
////            this.scene.VerifyAllExpectations();
////        }

////        [Test]
////        public void UnregisterFromSceneDoesNotLeaveCurrentSceneIfDifferent()
////        {
////            // Arrange
////            this.ConstructWrapperWithDefaultSequencerFactoryAndStubbedEntitySaveStateAndCreateUpdate();
////            string sceneName = Guid.NewGuid().ToString();
////            this.scene.Stub(s => s.Name).Return(sceneName);
////            string newSceneName = Guid.NewGuid().ToString();
////            this.spatialEntityManager.Stub(m => m.JoinScene(
////                Arg.Is(sceneName),
////                Arg<CreateSpatialReplica>.Is.Anything,
////                Arg<RemoveSpatialReplica>.Is.Anything,
////                Arg<bool>.Is.Equal(false)))
////                .Return(this.scene);
////            this.wrapper.RegisterWithScene(sceneName);

////            // Expect
////            this.scene.Expect(s => s.Leave()).Repeat.Never();

////            // Act
////            this.wrapper.UnregisterFromScene(newSceneName);

////            // Verify
////            this.scene.VerifyAllExpectations();
////        }

////        [Test]
////        public void TerminateUnregsitersEntityFromCurrentScene()
////        {
////            // Arrange
////            this.ConstructWrapperWithDefaultSequencerFactoryAndStubbedEntitySaveStateAndCreateUpdate();
////            string sceneName = Guid.NewGuid().ToString();
////            this.scene.Stub(s => s.Name).Return(sceneName);
////            string newSceneName = Guid.NewGuid().ToString();
////            this.spatialEntityManager.Stub(m => m.JoinScene(
////                Arg.Is(sceneName),
////                Arg<CreateSpatialReplica>.Is.Anything,
////                Arg<RemoveSpatialReplica>.Is.Anything,
////                Arg<bool>.Is.Equal(false)))
////                .Return(this.scene);
////            this.wrapper.RegisterWithScene(sceneName);

////            // Expect
////            this.scene.Expect(s => s.UnregisterEntity(this.entity));

////            // Act
////            this.wrapper.Terminate();

////            // Verify
////            this.scene.VerifyAllExpectations();
////        }

////        [Test]
////        public void TerminateLeavesCurrentScene()
////        {
////            // Arrange
////            this.ConstructWrapperWithDefaultSequencerFactoryAndStubbedEntitySaveStateAndCreateUpdate();
////            string sceneName = Guid.NewGuid().ToString();
////            this.scene.Stub(s => s.Name).Return(sceneName);
////            string newSceneName = Guid.NewGuid().ToString();
////            this.spatialEntityManager.Stub(m => m.JoinScene(
////                Arg.Is(sceneName),
////                Arg<CreateSpatialReplica>.Is.Anything,
////                Arg<RemoveSpatialReplica>.Is.Anything,
////                Arg<bool>.Is.Equal(false)))
////                .Return(this.scene);
////            this.wrapper.RegisterWithScene(sceneName);

////            // Expect
////            this.scene.Expect(s => s.Leave());

////            // Act
////            this.wrapper.Terminate();

////            // Verify
////            this.scene.VerifyAllExpectations();
////        }

////        private void ConstructWrapper()
////        {
////            this.wrapper = new HostedEntityWrapper(
////                this.transportProtocol,
////                this.clientProtocol,
////                this.sequencerFactory,
////                this.entity,
////                this.entityType,
////                this.badumnaId,
////                this.serialNumber,
////                this.clientAddress,
////                this.spatialEntityManager);
////        }

////        private void ConstructWrapperWithStubbedEntitySaveStateAndCreateUpdate()
////        {
////            this.entity.Stub(e => e.CreateUpdate()).Return(this.entityState);
////            this.entity.Stub(e => e.SaveState()).Return(this.milestone);
////            this.ConstructWrapper();
////        }

////        private void ConstructWrapperWithDefaultSequencerFactory()
////        {
////            this.wrapper = new HostedEntityWrapper(
////                this.transportProtocol,
////                this.clientProtocol,
////                this.entity,
////                this.entityType,
////                this.badumnaId,
////                this.serialNumber,
////                this.clientAddress,
////                this.spatialEntityManager);
////        }

////        private void ConstructWrapperWithDefaultSequencerFactoryAndStubbedEntitySaveStateAndCreateUpdate()
////        {
////            this.entity.Stub(e => e.CreateUpdate()).Return(this.entityState);
////            this.entity.Stub(e => e.SaveState()).Return(this.milestone);
////            this.ConstructWrapperWithDefaultSequencerFactory();
////        }
////    }
////}
