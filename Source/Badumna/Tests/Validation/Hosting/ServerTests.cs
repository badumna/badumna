﻿//////-----------------------------------------------------------------------
////// <copyright file="ServerTests.cs" company="NICTA">
//////     Copyright (c) 2010 NICTA. All rights reserved.
////// </copyright>
//////-----------------------------------------------------------------------

////namespace BadumnaTests.Validation.HostingTests
////{
////    using System;
////    using Badumna;
////    using Badumna.Core;
////    using Badumna.DataTypes;
////    using Badumna.SpatialEntities;
////    using Badumna.Transport;
////    using Badumna.Utilities;
////    using Badumna.Validation;
////    using Badumna.Validation.Hosting;
////    using NUnit.Framework;
////    using Rhino.Mocks;
////    using TestUtilities;

////    [TestFixture]
////    [DontPerformCoverage]
////    public class ServerTests
////    {
////        private ITransportProtocol transportProtocol;
////        private IClientProtocol clientProtocol;
////        private ISpatialEntityManager spatialEntityManager;
////        private INetworkFacade networkFacade;
////        private TimeKeeper timeKeeper;
////        private CreateValidatedEntity validatedEntityFactory;
////        private RemoveValidatedEntity validatedEntityRemovalHandler;
////        private SessionFactory sessionFactory;
////        private INetworkAddressProvider addressProvider;
////        private QueueInvokable queueApplicationEvent;
////        private CapturingConstraint<IInvokable> capturedApplicationEvent;
////        private Server server;

////        private ISession session;
////        private PeerAddress clientAddress;
////        private IValidatedEntity entity;
////        private ushort localId;
////        private BadumnaId badumnaId;
////        private ISignedEntityState milestone;
////        private string sceneName;
////        private uint entityType;

////        [SetUp]
////        public void SetUp()
////        {
////            this.transportProtocol = MockRepository.GenerateMock<ITransportProtocol>();
////            this.clientProtocol = MockRepository.GenerateMock<IClientProtocol>();
////            this.spatialEntityManager = MockRepository.GenerateMock<ISpatialEntityManager>();
////            this.networkFacade = MockRepository.GenerateMock<INetworkFacade>();
////            this.timeKeeper = new TimeKeeper();
////            this.validatedEntityFactory = MockRepository.GenerateMock<CreateValidatedEntity>();
////            this.validatedEntityRemovalHandler = MockRepository.GenerateMock<RemoveValidatedEntity>();
////            this.queueApplicationEvent = MockRepository.GenerateMock<QueueInvokable>();
////            this.capturedApplicationEvent = new CapturingConstraint<IInvokable>();
////            this.queueApplicationEvent.Stub(q => q(Arg<IInvokable>.Matches(this.capturedApplicationEvent)));
////            this.addressProvider = MockRepository.GenerateStub<INetworkAddressProvider>();
////            this.sessionFactory = MockRepository.GenerateMock<SessionFactory>();
////            this.session = MockRepository.GenerateMock<ISession>();
////            this.clientAddress = PeerAddress.GetRandomAddress();
////            this.entity = MockRepository.GenerateMock<IValidatedEntity>();
////            this.localId = (ushort)new Random().Next(ushort.MaxValue);
////            this.badumnaId = new BadumnaId(PeerAddress.GetRandomAddress(), this.localId);
////            this.milestone = MockRepository.GenerateMock<ISignedEntityState>();
////            this.sceneName = Guid.NewGuid().ToString();
////            this.entityType = 4338;
////        }

////        [Test]
////        public void ConstructorThrowsForNullTransportProtocol()
////        {
////            this.transportProtocol = null;
////            Assert.Throws(typeof(ArgumentNullException), this.ConstructServer);
////        }

////        [Test]
////        public void ConstructorThrowsForNullClientProtocol()
////        {
////            this.clientProtocol = null;
////            Assert.Throws(typeof(ArgumentNullException), this.ConstructServer);
////        }

////        [Test]
////        public void ConstructorThrowsForNullSpatialEntityManager()
////        {
////            this.spatialEntityManager = null;
////            Assert.Throws(typeof(ArgumentNullException), this.ConstructServer);
////        }

////        [Test]
////        public void ConstructorThrowsForNullNetworkFacade()
////        {
////            this.networkFacade = null;
////            Assert.Throws(typeof(ArgumentNullException), this.ConstructServer);
////        }

////        [Test]
////        public void ConstructorThrowsForNullTimeKeeper()
////        {
////            this.timeKeeper = null;
////            Assert.Throws(typeof(ArgumentNullException), this.ConstructServer);
////        }

////        [Test]
////        public void ConstructorThrowsForNullSessionFactory()
////        {
////            this.sessionFactory = null;
////            Assert.Throws(typeof(ArgumentNullException), this.ConstructServer);
////        }

////        [Test]
////        public void ConstructorCreatesDefaultSessionFactory()
////        {
////            // Test passes if it does not throw.
////            this.server = new Server(
////                this.transportProtocol,
////                this.clientProtocol,
////                this.spatialEntityManager,
////                this.networkFacade,
////                this.timeKeeper,
////                this.addressProvider,
////                this.queueApplicationEvent);

////            this.server.Configure(this.validatedEntityFactory, this.validatedEntityRemovalHandler);
////            var serialStateUpdate = MockRepository.GenerateMock<IOrdered<ISerializedEntityState>>();
////            var stateUpdate = MockRepository.GenerateMock<ISerializedEntityState>();

////            this.milestone.Stub(m => m.SerialEntityState).Return(serialStateUpdate);
////            serialStateUpdate.Stub(u => u.Item).Return(stateUpdate);

////            this.spatialEntityManager.Stub(m => m.JoinScene(
////                Arg.Is(this.sceneName),
////                Arg<CreateSpatialReplica>.Is.Anything,
////                Arg<RemoveSpatialReplica>.Is.Anything,
////                Arg<bool>.Is.Equal(false)))
////                .Return(MockRepository.GenerateMock<NetworkScene>());

////            // Trigger invocation of factory method.
////            this.server.ReceiveEntityRegistrationRequest(
////                this.clientAddress,
////                this.entityType,
////                this.badumnaId,
////                this.milestone,
////                this.sceneName);
////        }

////        [Test]
////        public void ReceiveEntityRegistrationRequestCreatesSessionForNewRequestor()
////        {
////            // Arrange
////            this.ConstructServer();
////            this.server.Configure(this.validatedEntityFactory, this.validatedEntityRemovalHandler);

////            // Expect
////            this.sessionFactory.Expect(f => f(
////                this.clientAddress,
////                this.clientProtocol,
////                this.transportProtocol,
////                this.spatialEntityManager,
////                this.networkFacade,
////                this.addressProvider,
////                this.validatedEntityFactory,
////                this.validatedEntityRemovalHandler))
////                .Return(this.session);

////            // Act
////            this.server.ReceiveEntityRegistrationRequest(
////                this.clientAddress,
////                this.entityType,
////                this.badumnaId,
////                this.milestone,
////                this.sceneName);

////            // Verify
////            this.sessionFactory.VerifyAllExpectations();
////        }

////        [Test]
////        public void ReceiveEntityRegistrationRequestDoesNotCreatesSessionForKnownRequestor()
////        {
////            // Arrange
////            this.ConstructServer();
////            this.server.Configure(this.validatedEntityFactory, this.validatedEntityRemovalHandler);
////            this.sessionFactory.Stub(f => f(
////                this.clientAddress,
////                this.clientProtocol,
////                this.transportProtocol,
////                this.spatialEntityManager,
////                this.networkFacade,
////                this.addressProvider,
////                this.validatedEntityFactory,
////                this.validatedEntityRemovalHandler))
////                .Return(this.session)
////                .Repeat.Once();
////            this.server.ReceiveEntityRegistrationRequest(
////                this.clientAddress,
////                this.entityType,
////                this.badumnaId,
////                this.milestone,
////                this.sceneName);

////            // Expect
////            this.sessionFactory.Expect(f => f(
////                this.clientAddress,
////                this.clientProtocol,
////                this.transportProtocol,
////                this.spatialEntityManager,
////                this.networkFacade,
////                this.addressProvider,
////                this.validatedEntityFactory,
////                this.validatedEntityRemovalHandler))
////                .Repeat.Never();

////            // Act
////            this.server.ReceiveEntityRegistrationRequest(
////                this.clientAddress,
////                this.entityType,
////                this.badumnaId,
////                this.milestone,
////                this.sceneName);

////            // Verify
////            this.sessionFactory.VerifyAllExpectations();
////        }

////        [Test]
////        public void ReceiveEntityRegistrationRequestPassesRequestFromNewRequestorToSession()
////        {
////            // Arrange
////            this.ConstructServer();
////            this.server.Configure(this.validatedEntityFactory, this.validatedEntityRemovalHandler);
////            this.sessionFactory.Stub(f => f(
////                this.clientAddress,
////                this.clientProtocol,
////                this.transportProtocol,
////                this.spatialEntityManager,
////                this.networkFacade,
////                this.addressProvider,
////                this.validatedEntityFactory,
////                this.validatedEntityRemovalHandler))
////                .Return(this.session);

////            // Expect
////            this.session.Expect(s => s.ReceiveEntityRegistrationRequest(
////                this.entityType,
////                this.badumnaId,
////                this.milestone,
////                this.sceneName));
////            ////this.applicationEventQueue.Expect(q => q.QueueApplicationEvent(null))
////            ////    .IgnoreArguments();

////            // Act
////            this.server.ReceiveEntityRegistrationRequest(
////                this.clientAddress,
////                this.entityType,
////                this.badumnaId,
////                this.milestone,
////                this.sceneName);
////            this.capturedApplicationEvent.Argument.Invoke();

////            // Verify
////            this.session.VerifyAllExpectations();
////        }

////        [Test]
////        public void ReceiveEntityRegistrationRequestPassesRequestFromExistingRequestorToSession()
////        {
////            // Arrange
////            this.ConstructServerAndInitiateSession();

////            // Expect
////            this.session.Expect(s => s.ReceiveEntityRegistrationRequest(
////                this.entityType,
////                this.badumnaId,
////                this.milestone,
////                this.sceneName));
////            ////this.applicationEventQueue.Expect(q => q.QueueApplicationEvent(null))
////            ////    .IgnoreArguments();

////            // Act
////            this.server.ReceiveEntityRegistrationRequest(
////                this.clientAddress,
////                this.entityType,
////                this.badumnaId,
////                this.milestone,
////                this.sceneName);
////            this.capturedApplicationEvent.Argument.Invoke();

////            // Verify
////            this.session.VerifyAllExpectations();
////        }

////        [Test]
////        public void ReceiveEntityUnregistrationRequestPassesRequestFromExistingRequestorToSession()
////        {
////            // Arrange
////            this.ConstructServerAndInitiateSession();

////            // Expect
////            this.session.Expect(s => s.ReceiveEntityUnregistrationRequest(this.localId));
////            ////this.applicationEventQueue.Expect(q => q.QueueApplicationEvent(null))
////            ////    .IgnoreArguments();

////            // Act
////            this.server.ReceiveEntityUnregistrationRequest(
////                this.clientAddress,
////                this.localId);
////            this.capturedApplicationEvent.Argument.Invoke();

////            // Verify
////            this.session.VerifyAllExpectations();
////        }

////        [Test]
////        public void ReceiveEntityInputFromUnkownClientTriggersErrorNotification()
////        {
////            // Arrange
////            this.ConstructServer();
////            var serialNumber = new CyclicalID.UShortID(99);
////            var interval = TimeSpan.FromMilliseconds(789);
////            var userInput = new byte[0];
////            bool replyRequested = true;
////            ////var error = new ServerError(RequestType.Input, ErrorType.UnknownClient);

////            // Expect
////            this.transportProtocol.Expect(p => p.SendRemoteCall(
////                this.clientAddress,
////                QualityOfService.Reliable,
////                this.clientProtocol.ReceiveServerError,
////                this.localId));

////            // Act
////            this.server.ReceiveEntityInput(
////                this.clientAddress,
////                this.localId,
////                serialNumber,
////                interval,
////                userInput,
////                replyRequested);

////            // Verify
////            this.transportProtocol.VerifyAllExpectations();
////        }

////        [Test]
////        public void ReceiveEntityInputPassesInputToSession()
////        {
////            // Arrange
////            this.ConstructServerAndInitiateSession();
////            var serialNumber = new CyclicalID.UShortID(99);
////            var interval = TimeSpan.FromMilliseconds(789);
////            var userInput = new byte[0];
////            bool replyRequested = true;

////            // Expect
////            this.session.Expect(s => s.ReceiveEntityInput(
////                this.localId,
////                serialNumber,
////                interval,
////                userInput,
////                replyRequested));
////            ////this.applicationEventQueue.Expect(q => q.QueueApplicationEvent(null))
////            ////    .IgnoreArguments();

////            // Act
////            this.server.ReceiveEntityInput(
////                this.clientAddress,
////                this.localId,
////                serialNumber,
////                interval,
////                userInput,
////                replyRequested);
////            this.capturedApplicationEvent.Argument.Invoke();

////            // Verify
////            this.session.VerifyAllExpectations();
////        }

////        [Test]
////        public void ReceiveSceneRegistrationRequestPassesRequestToSession()
////        {
////            // Arrange
////            this.ConstructServerAndInitiateSession();
////            string sceneName = Guid.NewGuid().ToString();

////            // Expect
////            this.session.Expect(s => s.ReceiveSceneRegistrationRequest(
////                this.localId,
////                sceneName));

////            // Act
////            this.server.ReceiveSceneRegistrationRequest(
////                this.clientAddress,
////                this.localId,
////                sceneName);

////            // Verify
////            this.session.VerifyAllExpectations();
////        }

////        [Test]
////        public void ReceiveSceneUnregistrationRequestPassesRequestToSession()
////        {
////            // Arrange
////            this.ConstructServerAndInitiateSession();
////            string sceneName = Guid.NewGuid().ToString();

////            // Expect
////            this.session.Expect(s => s.ReceiveSceneUnregistrationRequest(
////                this.localId,
////                sceneName));

////            // Act
////            this.server.ReceiveSceneUnregistrationRequest(
////                this.clientAddress,
////                this.localId,
////                sceneName);

////            // Verify
////            this.session.VerifyAllExpectations();
////        }

////        [Test]
////        public void ReceiveTerminationNoticeTerminatesSessions()
////        {
////            // Arrange
////            this.ConstructServerAndInitiateSession();

////            // Expect
////            this.session.Expect(s => s.ReceiveTerminationNotice());
////            ////this.applicationEventQueue.Expect(q => q.QueueApplicationEvent(null))
////            ////    .IgnoreArguments();

////            // Act
////            this.server.ReceiveTerminationNotice(this.clientAddress);
////            this.capturedApplicationEvent.Argument.Invoke();

////            // Verify
////            this.session.VerifyAllExpectations();
////        }

////        [Test]
////        public void ShutdownSendsTerminationNotices()
////        {
////            // Arrange
////            this.ConstructServerAndInitiateSession();

////            // Expect
////            this.transportProtocol.Expect(p => p.SendRemoteCall(
////                this.clientAddress,
////                QualityOfService.Reliable,
////                this.clientProtocol.ReceiveTerminationNotice));

////            // Act
////            this.server.Shutdown();

////            // Verify
////            this.transportProtocol.VerifyAllExpectations();
////        }

////        [Test]
////        public void ReceivePingRequestSendsReply()
////        {
////            // Arrange
////            this.ConstructServer();

////            // Expect
////            this.transportProtocol.Expect(p => p.SendRemoteCall(
////                this.clientAddress,
////                QualityOfService.Reliable,
////                this.clientProtocol.ReceivePingReply,
////                true));

////            // Act
////            this.server.ReceivePingRequest(this.clientAddress);

////            // Verify
////            this.transportProtocol.VerifyAllExpectations();
////        }

////        [Test]
////        public void RegularProcessingTimesOutDormantSessions()
////        {
////            // Arrange
////            this.timeKeeper.Now = TimeSpan.Zero;
////            this.ConstructServerAndInitiateSession();
////            this.timeKeeper.Now = TimeSpan.FromSeconds(11);

////            // Expect
////            this.session.Expect(s => s.ReceiveTerminationNotice());
////            ////this.applicationEventQueue.Expect(q => q.QueueApplicationEvent(null))
////            ////    .IgnoreArguments();

////            // Act
////            this.server.RegularProcessing();
////            this.capturedApplicationEvent.Argument.Invoke();

////            // Verify
////            this.session.VerifyAllExpectations();
////        }

////        [Test]
////        public void RegularProcessingDoesNotTimeOutActiveSessions()
////        {
////            // Arrange
////            this.timeKeeper.Now = TimeSpan.Zero;
////            this.ConstructServerAndInitiateSession();
////            this.timeKeeper.Now = TimeSpan.FromSeconds(9);

////            // Expect
////            this.session.Expect(s => s.ReceiveTerminationNotice())
////                .Repeat.Never();

////            // Act
////            this.server.RegularProcessing();

////            // Verify
////            this.session.VerifyAllExpectations();
////        }

////        private void ConstructServer()
////        {
////            this.server = new Server(
////                this.transportProtocol,
////                this.clientProtocol,
////                this.spatialEntityManager,
////                this.networkFacade,
////                this.timeKeeper,
////                this.addressProvider,
////                this.queueApplicationEvent,
////                this.sessionFactory);
////        }

////        private void ConstructServerAndInitiateSession()
////        {
////            this.ConstructServer();
////            this.server.Configure(this.validatedEntityFactory, this.validatedEntityRemovalHandler);
////            this.sessionFactory.Stub(f => f(
////                this.clientAddress,
////                this.clientProtocol,
////                this.transportProtocol,
////                this.spatialEntityManager,
////                this.networkFacade,
////                this.addressProvider,
////                this.validatedEntityFactory,
////                this.validatedEntityRemovalHandler))
////                .Return(this.session);
////            this.server.ReceiveEntityRegistrationRequest(
////                this.clientAddress,
////                this.entityType,
////                this.badumnaId,
////                this.milestone,
////                this.sceneName);
////        }

////        /// <summary>
////        /// FakeValidatedEntity
////        /// </summary>
////        internal class FakeValidatedEntity : IValidatedEntity
////        {
////            public FakeValidatedEntity(INetworkFacade networkFacade)
////            {
////            }

////            public Vector3 Position
////            {
////                get
////                {
////                    throw new NotImplementedException();
////                }

////                set
////                {
////                    throw new NotImplementedException();
////                }
////            }

////            public float Radius { get; set; }

////            public float AreaOfInterestRadius { get; set; }

////            public BadumnaId Guid { get; set; }

////            public EntityStatus Status { get; set; }

////            public void RestoreState(ISerializedEntityState state)
////            {
////                // No operations.
////            }

////            public ISerializedEntityState SaveState()
////            {
////                // No operations.
////                return MockRepository.GenerateMock<ISerializedEntityState>();
////            }

////            public void Update(TimeSpan timeInterval, byte[] input, UpdateMode mode)
////            {
////                // No operations.
////            }

////            public ISerializedEntityState CreateUpdate()
////            {
////                // No operations.
////                return MockRepository.GenerateMock<ISerializedEntityState>();
////            }

////            public void Serialize(BooleanArray requiredParts, System.IO.Stream stream)
////            {
////                throw new NotImplementedException();
////            }

////            public void Deserialize(BooleanArray includedParts, System.IO.Stream stream, int estimatedMillisecondsSinceDeparture)
////            {
////                throw new NotImplementedException();
////            }

////            public void HandleEvent(System.IO.Stream stream)
////            {
////                throw new NotImplementedException();
////            }

////            public ISerializedEntityState MergeStateUpdates(ISerializedEntityState current, ISerializedEntityState update)
////            {
////                throw new NotImplementedException();
////            }
////        }

////        private class TimeKeeper : ITime
////        {
////            public TimeSpan Now
////            {
////                get;
////                set;
////            }
////        }
////    }
////}
