﻿//-----------------------------------------------------------------------
// <copyright file="CharacterTester.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2012 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System.Collections.Generic;
using Badumna.Security;
using NUnit.Framework;

namespace BadumnaTests.Protocol
{
    public class CharacterTester : BadumnaTests.Core.ParseableTestHelper
    {
        public CharacterTester()
            : base(new ParseableTester())
        {
        }

        [TestCase(1, "bob", Result = true)]
        [TestCase(1, "", Result = false)]
        [TestCase(1, null, Result = false)]
        [TestCase(-1, "bob", Result = false)]
        [TestCase(-1, "", Result = false)]
        [TestCase(-1, null, Result = false)]
        public bool TestIsValid(long id, string name)
        {
            var character = new Character(id, name);
            return character.IsValid;
        }

        internal class ParseableTester : BadumnaTests.Core.ParseableTester<Character>
        {
            public override ICollection<Character> CreateExpectedValues()
            {
                var expectedValues = new List<Character>();
                expectedValues.Add(new Character(-1, "bob"));
                expectedValues.Add(new Character(-1, ""));
                expectedValues.Add(new Character(999, "fred"));
                return expectedValues;
            }
        }
    }
}
