﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using Badumna.Core;
using Badumna.Security;
using NUnit.Framework;
using Rhino.Mocks;

namespace BadumnaTests.Security
{
    [TestFixture]
    public class CertificateTokenTester : TokenTester<CertificateToken>
    {
        private RSACryptoServiceProvider cryptoProvider;
        private ITokenCache tokenCache;
        protected CertificateToken MakePrivateKeyToken()
        {
            byte[] signatureBytes = new byte[PublicKey.SignatureLengthBytes];
            for (int i = 0; i < signatureBytes.Length; i++)
            {
                signatureBytes[i] = (byte)((i + 1) * 67 % 251);
            }

            return new CertificateToken(42, new Character(12, "character"), new DateTime(12345678), new DateTime(87654321), signatureBytes, new RSACryptoServiceProvider(CertificateToken.KeyLengthBits));
        }
        
        protected override CertificateToken MakeTestToken()
        {
            return this.MakePublicKeyToken();
        }

        private CertificateToken MakePublicKeyToken()
        {
            byte[] signatureBytes = new byte[PublicKey.SignatureLengthBytes];
            for (int i = 0; i < signatureBytes.Length; i++)
            {
                signatureBytes[i] = (byte)((i + 1) * 67 % 251);
            }

            return new CertificateToken(42, new Character(11, "bob"), new DateTime(12345678), new DateTime(87654321), PublicKey.GenerateKey(), new Signature(signatureBytes));
        }

        [SetUp]
        public void SetUp()
        {
            this.tokenCache = MockRepository.GenerateStub<ITokenCache>();
            this.cryptoProvider = new RSACryptoServiceProvider();
        }

        [Test]
        public void ToMessageShouldIncludePublicKeyButNotPrivateKey()
        {
            var token = this.MakePrivateKeyToken();
            var iToken = (ICertificateToken)token;
            var privateKeyBits = iToken.PrivateKey.RawData;
            var publicKeyBits = iToken.PublicKey.RawData;
            using (var message = new MessageBuffer())
            {
                iToken.ToMessage(message, null);
                Assert.IsTrue(this.bufferContains(publicKeyBits, message.GetBuffer()));
                Assert.IsFalse(this.bufferContains(privateKeyBits, message.GetBuffer()));
            }
        }

        [Test]
        public void ToMessageWithPrivateKeyShouldIncludePrivateKey()
        {
            var token = this.MakePrivateKeyToken();
            var iToken = (ICertificateToken)token;
            var privateKeyBits = iToken.PrivateKey.RawData;

            using (var message = new MessageBuffer())
            {
                token.ToMessageWithPrivateKey(message);
                Assert.IsTrue(this.bufferContains(privateKeyBits, message.GetBuffer()));
            }
        }

        [Test]
        public void ModifyingUserIdInvalidatesSignature()
        {
            var userId = 123;

            byte[] signature;
            var token = this.MakeValidToken(userId, new Character(5, "bob"), out signature);

            Assert.IsTrue(((ICertificateToken)token).Validate(tokenCache));
            token = new CertificateToken(userId + 1, ((ICertificateToken)token).Character, ((ICertificateToken)token).TimeStamp, token.ExpirationTime, signature, this.cryptoProvider);
            Assert.IsFalse(((ICertificateToken)token).Validate(tokenCache));
        }

        [Test]
        public void ModifyingCharacterNameOrIdInvalidatesSignature()
        {
            var userId = 123;
            var characterName = "fred";
            var characterId = 7;
            byte[] signature;
            var token = this.MakeValidToken(userId, new Character(characterId, characterName), out signature);

            Assert.IsTrue(((ICertificateToken)token).Validate(tokenCache));
            
            token = new CertificateToken(userId, new Character(characterId, "bob"), ((ICertificateToken)token).TimeStamp, token.ExpirationTime, signature, this.cryptoProvider);
            Assert.IsFalse(((ICertificateToken)token).Validate(tokenCache));

            token = new CertificateToken(userId, new Character(characterId+1, characterName), ((ICertificateToken)token).TimeStamp, token.ExpirationTime, signature, this.cryptoProvider);
            Assert.IsFalse(((ICertificateToken)token).Validate(tokenCache));
        }

        private CertificateToken MakeValidToken(long userId, Character character, out byte[] signature)
        {
            var now = new DateTime();
            var tomorrow = now.AddDays(1);

            var privateKey = new PrivateKey(this.cryptoProvider);
            var trustedPrivateKey = new PrivateKey(new RSACryptoServiceProvider());
            var trustedAuthority = new TrustedAuthorityToken(tomorrow, trustedPrivateKey.PublicKey.RawData);

            signature = CertificateToken.SignCertificate(userId, character, now, tomorrow, privateKey.PublicKey.RawData, trustedPrivateKey.RawData);
            var token = new CertificateToken(userId, character, now, tomorrow, signature, this.cryptoProvider);
            this.tokenCache.Stub(c => c.GetTrustedAuthorityToken()).Return(trustedAuthority);
            return token;
        }

        private bool bufferContains(byte[] needle, byte[] haystack)
        {
            //NOTE: should be able to replace much of this with Enumerable.SequenceEqual on a later .NET version
            for (var haystackIdx = 0; haystackIdx <= haystack.Length - needle.Length; haystackIdx++)
            {
                bool found = true;
                for(var needleIdx=0; needleIdx<needle.Length; needleIdx++)
                {
                    if(haystack[haystackIdx + needleIdx] != needle[needleIdx])
                    {
                        found = false;
                        break;
                    }
                }
                if(found)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
