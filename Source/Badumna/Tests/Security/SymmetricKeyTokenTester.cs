﻿using System;
using System.IO;
using Badumna.Security;
using NUnit.Framework;

namespace BadumnaTests.Security
{
    [TestFixture]
    public class SymmetricKeyTokenTester : TokenTester<SymmetricKeyToken>
    {
        protected override SymmetricKeyToken MakeTestToken()
        {
            return new SymmetricKeyToken(DateTime.Now + TimeSpan.FromHours(1),
                new byte[] { 8, 7, 6, 5, 4, 3, 2, 1 });
        }
    }
}
