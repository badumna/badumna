﻿using System;
using Badumna.Core;
using Badumna.Security;
using Badumna.Utilities;
using NUnit.Framework;
using Rhino.Mocks;

namespace BadumnaTests.Security
{
    [TestFixture]
    [DontPerformCoverage]
    public class TokenMonitorTester
    {
        private IIdentityProvider CreateIdentityProvider(DateTime expiryTime)
        {
            IIdentityProvider identityProvider = MockRepository.GenerateStub<IIdentityProvider>();
            identityProvider.Stub(x => x.GetNetworkParticipationToken()).Return(new SymmetricKeyToken(expiryTime, new byte[SymmetricKeyToken.KeyLengthBytes]));
            return identityProvider;
        }

        [Test]
        public void GetTest()
        {
            NetworkEventQueue eventQueue = new NetworkEventQueue();

            Clock clock = new Clock(eventQueue, delegate { return TimeSpan.Zero; });  // A clock that never changes time
            clock.Synchronize(DateTime.Now);

            IIdentityProvider identityProvider = this.CreateIdentityProvider(clock.Now + TimeSpan.FromSeconds(1));

            TokenMonitor monitor = new TokenMonitor(TokenType.NetworkParticipatation, eventQueue, clock);
            monitor.SetIdentityProvider(identityProvider);

            Assert.IsTrue(monitor.CacheAndMonitor());
            Assert.IsNotNull(monitor.GetToken());
            Assert.IsFalse(monitor.GetToken().IsExpiredAt(clock.Now));
            Assert.IsTrue(monitor.GetToken().IsExpiredAt(clock.Now + TimeSpan.FromSeconds(2)));
        }
    }
}
