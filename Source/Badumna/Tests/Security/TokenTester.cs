﻿using System;
using System.IO;
using Badumna.Security;
using NUnit.Framework;
using Badumna.Core;

namespace BadumnaTests.Security
{
    public abstract class TokenTester<T> where T : Token
    {
        protected abstract T MakeTestToken();

        internal T DeserializeToken(MessageBuffer message)
        {
            return message.Read<T>();
        }

        [Test]
        public virtual void SerializationTest()
        {
            T token = this.MakeTestToken();
            T newToken;

            using (MessageBuffer buffer = new MessageBuffer())
            {
                buffer.Write(token);
                buffer.ReadOffset = 0;
                newToken = this.DeserializeToken(buffer);
            }

            Assert.IsTrue(token.Equals(newToken), "Deserialized token is not equal to original token");
        }

        [Test]
        public virtual void ShortStreamTest()
        {
            using (MessageBuffer buffer = new MessageBuffer())
            {
                Assert.Throws(Is.InstanceOf<Exception>(),
                    () => this.DeserializeToken(buffer));
            }
        }

        [Test]
        public virtual void RandomStreamTest()
        {
            // First eight bytes are 0 so that the Token(BinaryReader) constructor succeeds, ensuring we
            // test the derived constructor.
            byte[] randomData = new byte[]
            {
                0, 0, 0, 0, 0, 0, 0, 0, 32, 58, 227, 208, 54, 95, 166, 100, 76, 13, 8, 89, 155, 166, 86, 219, 61, 49, 229,
                86, 253, 34, 214, 0, 167, 158, 164, 120, 189, 7, 219, 194, 161, 157, 152, 191, 34, 213, 189, 42, 135, 203, 199, 93, 123,
                48, 174, 43, 147, 175, 67, 106, 50, 214, 147, 40, 253, 8, 3, 56, 16, 17, 28, 227, 40, 95, 127, 104, 9, 250, 207, 248, 255,
                130, 163, 38, 255, 22, 214, 36, 23, 123, 230, 242, 173, 125, 225, 151, 226, 138, 201, 20, 42, 155, 89, 210, 178, 199, 66,
                34, 193, 113, 89, 150, 159, 174, 20, 250, 114, 19, 180, 202, 52, 131, 154, 38, 192, 165, 154, 180, 173, 166, 83, 87, 3, 92,
                121, 3, 12, 165, 125, 115, 240, 240, 18, 193, 33, 229, 115, 79, 204, 151, 36, 59, 251, 217, 180, 63, 23, 61, 156, 246, 147,
                253, 229, 187, 98, 136, 36, 210, 209, 57, 73, 43, 139, 96, 76, 99, 0, 45, 78, 112, 154, 96, 67, 70, 18, 237, 41, 167, 255,
                209, 111, 76, 64, 95, 41, 14, 48, 199, 129, 123, 138, 242, 23, 30, 38, 140, 138, 61, 132, 50, 27, 221, 216, 145, 220, 210,
                242, 111, 59, 26, 80, 244, 56, 162, 186, 72, 194, 105, 238, 193, 205, 147, 69, 148, 109, 136, 171, 167, 142, 85, 59, 67,
                143, 105, 96, 8, 115, 192, 1, 174, 54, 236, 104, 10, 201, 181, 156, 242, 81, 104, 169, 102, 31, 206, 171, 228, 170, 102,
                166, 177, 185, 18, 116, 186, 108, 40, 176, 220, 78, 96, 74, 97, 172, 201, 239, 188, 26, 200, 18, 254, 36, 150, 204, 214,
                175, 25, 165, 60, 27, 145
            };

            using (MessageBuffer buffer = new MessageBuffer(randomData))
            {
                Assert.Throws(Is.InstanceOf<Exception>(),
                    () => this.DeserializeToken(buffer));
            }
        }
    }
}
