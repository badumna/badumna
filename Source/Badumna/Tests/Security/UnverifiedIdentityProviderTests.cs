﻿//-----------------------------------------------------------------------
// <copyright file="UnverifiedIdentityProviderTests.cs" company="National ICT Australia Ltd">
//     Copyright (c) 2012 National ICT Australia Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BadumnaTests.Security
{
    using System;
    using Badumna.Security;
    using NUnit.Framework;
    using Rhino.Mocks;

    [TestFixture]
    public class UnverifiedIdentityProviderTests
    {
        [Test]
        public void IdentityProviderConstructorWithValidInputsWorks()
        {
            UnverifiedIdentityProvider deiIdentityProvider = new UnverifiedIdentityProvider("alice", UnverifiedIdentityProvider.GenerateKeyPair());
            Assert.IsNotNull(deiIdentityProvider);
        }

        [Test]
        [ExpectedException(typeof(ArgumentException))]
        public void UnverifiedIdentityProviderConstructorWithInvalidXmlStringShouldThrowArgumentException()
        {
            UnverifiedIdentityProvider deiIdentityProvider = new UnverifiedIdentityProvider("alice", string.Empty);
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void IdentityProviderConstructorWithNullXmlStringShouldThrowArgumentNullException()
        {
            UnverifiedIdentityProvider deiIdentityProvider = new UnverifiedIdentityProvider("alice", (string)null);
        }
    }
}
