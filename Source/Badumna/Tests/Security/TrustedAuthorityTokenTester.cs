﻿using System;
using System.IO;
using Badumna.Security;
using NUnit.Framework;

namespace BadumnaTests.Security
{
    [TestFixture]
    public class TrustedAuthorityTokenTester : TokenTester<TrustedAuthorityToken>
    {
        protected override TrustedAuthorityToken MakeTestToken()
        {
            return new TrustedAuthorityToken(DateTime.Now + TimeSpan.FromHours(1), PublicKey.GenerateKey().RawData);
        }
    }
}
