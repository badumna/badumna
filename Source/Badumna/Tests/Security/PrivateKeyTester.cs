﻿using System;
using System.Text;
using Badumna.Core;
using Badumna.Security;
using NUnit.Framework;

namespace BadumnaTests.Security
{
    [TestFixture]
    [DontPerformCoverage]
    public class PrivateKeyTester
    {
        [SetUp]
        public void Initialize()
        {
        }

        [TearDown]
        public void Shutdown()
        {
        }

        [Test]
        public void PublicEncryptTest()
        {
            PrivateKey privateKey = new PrivateKey();
            String testString = "Test String";

            byte[] encryptedString = privateKey.PublicKey.Encrypt(Encoding.UTF8.GetBytes(testString));
            string decryptedString = Encoding.UTF8.GetString(privateKey.Decrypt(encryptedString));

            Assert.AreEqual(testString, decryptedString, "Failed to encrypt and decrypte using private key.");
        }
    }
}
