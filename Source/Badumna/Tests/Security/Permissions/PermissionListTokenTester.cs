﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using Badumna.Security;
using System.IO;

namespace BadumnaTests.Security.Permissions
{
    [TestFixture]
    public class PermissionListTokenTester : TokenTester<PermissionListToken>
    {
        [Test, Ignore("Currently broken, but permissions aren't yet in use.  Need to add signatures to the serialized permissions?")]
        public override void SerializationTest()
        {
            base.SerializationTest();
        }

        protected override PermissionListToken MakeTestToken()
        {
            PermissionListToken token = new PermissionListToken(new PrivateKey());
            PublicKey key = PublicKey.GenerateKey();

            byte[] signatureBytes = new byte[PublicKey.SignatureLengthBytes];
            for (int i = 0; i < signatureBytes.Length; i++)
            {
                signatureBytes[i] = (byte)((i + 1) * 67 % 251);
            }

            Permission permission = new Permission(DateTime.Now + TimeSpan.FromHours(1), PermissionType.IniatiateDiagnostics, key);
            token.AddPermission(permission.Serialize(), signatureBytes);
            permission = new Permission(DateTime.Now + TimeSpan.FromHours(3.5), PermissionType.Administrator, key);
            token.AddPermission(permission.Serialize(), signatureBytes);

            return token;
        }
    }
}
