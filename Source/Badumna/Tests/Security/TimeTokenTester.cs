﻿using System;
using System.IO;
using Badumna.Security;
using NUnit.Framework;

namespace BadumnaTests.Security
{
    [TestFixture]
    public class TimeTokenTester : TokenTester<TimeToken>
    {
        protected override TimeToken MakeTestToken()
        {
            return new TimeToken(DateTime.Now, DateTime.Now + TimeSpan.FromHours(1));
        }

        [Test, Ignore("Sporadic failures due to assumption that deserialization time == serialization time")]
        public override void SerializationTest()
        {
            base.SerializationTest();
        }
    }
}
