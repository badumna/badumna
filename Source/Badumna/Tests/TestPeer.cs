﻿//-----------------------------------------------------------------------
// <copyright file="TestPeer.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2011 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System.Collections.Generic;
using System.Diagnostics;
using Badumna.Core;
using Badumna.Security;
using Badumna.Transport;
using BadumnaTests.Core;
using BadumnaTests.Transport.Connections;
using BadumnaTests.Utilities;
using Rhino.Mocks;

namespace BadumnaTests
{
    internal abstract class BarebonesTestPeer
    {
        protected NetworkEventQueue eventQueue;
        protected NetworkEventQueue encryptionQueue;
        protected ITime timeKeeper;
        protected TokenCache tokens;

        /// <summary>
        /// A unique ID for the peer (also used for port number).
        /// </summary>
        public int Id { get; private set; }

        public INetworkAddressProvider AddressProvider { get; private set; }

        public PeerAddress PublicAddress { get { return this.AddressProvider.PublicAddress; } }

        public MockTransportLayer Transport { get; private set; }

        public INetworkConnectivityReporter ConnectivityReporter { get; private set; }

        public BarebonesTestPeer(int id, NatType natType, NetworkEventQueue eventQueue, NetworkEventQueue encryptionQueue)
        {
            this.eventQueue = eventQueue;
            this.encryptionQueue = encryptionQueue;
            this.timeKeeper = this.eventQueue;

            this.Id = id;

            this.AddressProvider = MockRepository.GenerateStub<INetworkAddressProvider>();
            this.AddressProvider.Stub(x => x.PublicAddress).Return(PeerAddress.GetAddress("127.0.0.1", id, natType));

            this.ConnectivityReporter = new NetworkConnectivityReporter(this.eventQueue);

            this.tokens = new TokenCache(eventQueue, this.timeKeeper, this.ConnectivityReporter);
            this.tokens.SetIdentityProvider(new UnverifiedIdentityProvider());
            this.tokens.PreCache();

            this.ConnectivityReporter.SetStatus(ConnectivityStatus.Online);

            this.Transport = new MockTransportLayer(this.AddressProvider, this.eventQueue, this.timeKeeper);
        }
    }

    /// <summary>
    /// A simulated peer.
    /// </summary>
    internal abstract class TestPeer : BarebonesTestPeer
    {
        public TestPeer(int id, NatType natType, NetworkEventQueue eventQueue, NetworkEventQueue encryptionQueue)
            : base(id, natType, eventQueue, encryptionQueue)
        {
        }

        public TestableConnectionTable ConnectionTable { get; private set; }

        public ProtocolComponent<TransportEnvelope> Protocol { get; private set; }

        public TransportEnvelope LastFailedMessage { get; private set; }

        public PeerAddress LastFailedDestination { get; private set; }

        public void CreateConnection(TestPeer otherPeer)
        {
            this.CreateConnection(otherPeer.AddressProvider.PublicAddress, otherPeer.Id);
        }

        /// <summary>
        /// Creates a MockConnection table with an instance of Connection that references otherPeerAddress.  Note that
        /// a transport link needs to be created separately using MockTransportLayer.ModifyLink(...).
        /// </summary>
        /// <param name="otherPeer">The peer to connect to</param>
        /// <param name="destinationNatType"></param>
        public void CreateConnection(PeerAddress otherPeerAddress, int otherPeerId)
        {
            this.ConnectionTable = new TestableConnectionTable(otherPeerAddress, this.Transport, new SeedPeerFinder(new List<string>()), this.eventQueue, this.encryptionQueue, this.timeKeeper, this.AddressProvider, this.ConnectivityReporter, this.tokens, MessageParserTester.DefaultFactory);
            this.Protocol = this.CreateProtocol(otherPeerAddress, otherPeerId);

            this.ConnectionTable.Connection.MaximumRetriesReachedEvent +=
                delegate(TransportEnvelope messageEnvelope, PeerAddress peerName)
                {
                    ////Debug.WriteLine("Failed to send reliable message to " + peerName + ". Max retries reached.");
                    this.LastFailedMessage = messageEnvelope;
                    this.LastFailedDestination = peerName;
                };
        }

        public void Forge()
        {
            this.ConnectionTable.ForgeMethodList();
            this.ConnectionTable.ProtocolRoot.ForgeMethodList();
        }

        protected abstract ProtocolComponent<TransportEnvelope> CreateProtocol(PeerAddress otherPeerAddress, int otherPeerId);
    }

    internal abstract class TestPeer<T> : TestPeer where T : ProtocolComponent<TransportEnvelope>
    {
        public TestPeer(int id, NatType natType, NetworkEventQueue eventQueue, NetworkEventQueue encryptionQueue)
            : base(id, natType, eventQueue, encryptionQueue)
        {
        }

        public new T Protocol
        {
            get { return (T)base.Protocol; }
        }
    }
}
