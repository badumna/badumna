﻿//-----------------------------------------------------------------------
// <copyright file="SubscriptionDataTests.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2010 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BadumnaTests.InterestManagement
{
    using System;
    using Badumna.Core;
    using Badumna.DataTypes;
    using Badumna.InterestManagement;
    using Badumna.Utilities;
    using NUnit.Framework;
    using Rhino.Mocks;

    [TestFixture]
    public class SubscriptionDataTests
    {
        private SubscriptionData subscriptionData;
        private QualifiedName sceneName;
        private ImsRegion region;
        private uint timeToLiveSeconds = 7;
        private EventHandler<ImsEventArgs> enterNotificationHandler;
        private EventHandler<ImsStatusEventArgs> statusHandler;
        private ITime timeKeeper;
        private uint nowSeconds = 10;

        [SetUp]
        public void SetUp()
        {
            this.sceneName = new QualifiedName("", Guid.NewGuid().ToString());
            this.region = new ImsRegion(
                new Vector3(0f, 0f, 0f),
                1f,
                new BadumnaId(PeerAddress.GetLoopback(1), 1),
                RegionType.None,
                this.sceneName);
            this.enterNotificationHandler = MockRepository.GenerateMock<EventHandler<ImsEventArgs>>();
            this.statusHandler = MockRepository.GenerateMock<EventHandler<ImsStatusEventArgs>>();
            this.timeKeeper = MockRepository.GenerateMock<ITime>();
            this.timeKeeper.Stub(t => t.Now).Return(TimeSpan.FromSeconds(this.nowSeconds));
            this.subscriptionData = new SubscriptionData(
                this.sceneName,
                this.region,
                this.timeToLiveSeconds,
                this.enterNotificationHandler,
                this.statusHandler,
                this.timeKeeper);
        }

        [TearDown]
        public void TearDown()
        {
            this.region = null;
            this.sceneName = default(QualifiedName);
        }

        [Test]
        public void ConstructorCopiesRegion()
        {
            Assert.AreNotEqual(this.region, this.subscriptionData.Region);
            Assert.AreEqual(this.region.Guid, this.subscriptionData.Region.Guid);
        }

        [Test]
        public void ConstructorSetsSceneName()
        {
            Assert.AreEqual(this.sceneName, this.subscriptionData.SceneName);
        }    

        [Test]
        public void ConstructorEstimatesExpirationTime()
        {
        ////    uint ttlSeconds = 5;
        ////    TimeSpan t0 = NetworkEventQueue.Instance.Now;
        ////    SubscriptionData data = new SubscriptionData(null, this.region, ttlSeconds, null, null, null, null);
        ////    TimeSpan t1 = NetworkEventQueue.Instance.Now;
        ////    Assert.GreaterOrEqual(data.EstimatedExpirationTime, t0 + TimeSpan.FromSeconds(ttlSeconds));
        ////    Assert.LessOrEqual(data.EstimatedExpirationTime, t1 + TimeSpan.FromSeconds(ttlSeconds));
        ////
            Assert.AreEqual(
                TimeSpan.FromSeconds(this.nowSeconds + this.timeToLiveSeconds),
                this.subscriptionData.EstimatedExpirationTime);
        }

        [Test]
        public void EnterNotificationCallsHandler()
        {
            object sender = new object();
            ImsEventArgs args = new ImsEventArgs();
            this.enterNotificationHandler.Expect(h => h.Invoke(sender, args));
            this.subscriptionData.EnterNotification(sender, args);
            this.enterNotificationHandler.VerifyAllExpectations();
        }

        [Test]
        public void StatusCallsHandler()
        {
            object sender = new object();
            RequestStatus requestStatus = new RequestStatus();
            ImsStatusEventArgs args = new ImsStatusEventArgs(new BadumnaId(), requestStatus);
            this.statusHandler.Expect(h => h.Invoke(null, null))
                .IgnoreArguments()
                .WhenCalled(call =>
                    {
                        Assert.AreEqual(sender, call.Arguments[0]);
                        Assert.AreEqual(requestStatus, ((ImsStatusEventArgs)call.Arguments[1]).Status);
                    });
            this.subscriptionData.Status(sender, requestStatus);
            this.statusHandler.VerifyAllExpectations();
        }
    }
}