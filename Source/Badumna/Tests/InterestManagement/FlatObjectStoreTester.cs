﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using Badumna.InterestManagement;
using Badumna.Core;

namespace BadumnaTests.InterestManagement
{
    [TestFixture]
    public class FlatObjectStoreTester : SpatialObjectStoreInterfaceTester
    {
        override internal ISpatialObjectStore CreateNewDerivedInstance()
        {
            return new FlatObjectStore(new NetworkEventQueue());
        }
    }
}
