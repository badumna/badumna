﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using BadumnaTests.Core;
using Badumna.InterestManagement;
using Badumna.Core;
using Badumna.DataTypes;
using Badumna.Utilities;

namespace BadumnaTests.InterestManagement
{
    [TestFixture]
    public class EventRegionTester : BadumnaTests.Core.ParseableTestHelper
    {
        static readonly QualifiedName sceneName = new QualifiedName("", "EventRegionTester");
        class ParseableTester : BadumnaTests.Core.ParseableTester<EventRegion>
        {
            public override void AssertAreEqual(EventRegion expected, EventRegion actual)
            {
                Assert.AreEqual(expected.Centroid, actual.Centroid);
                Assert.AreEqual(expected.Guid, actual.Guid);
                Assert.AreEqual(expected.Radius, actual.Radius);
            }

            public override ICollection<EventRegion> CreateExpectedValues()
            {
                ICollection<EventRegion> expectedValues = new List<EventRegion>();

                expectedValues.Add(new EventRegion(new BadumnaId(PeerAddress.GetLoopback(9), 1),
                    new Vector3(0, 2, 6), 78.54f, sceneName));

                return expectedValues;
            }
        }

        public EventRegionTester()
            : base(new ParseableTester())
        { }

        [Test]
        public void InheritanceTest()
        {
            EventRegion region = new EventRegion(new BadumnaId(PeerAddress.GetLoopback(9), 1),
                new Vector3(99, 99, 99), 99, sceneName);
            ImsRegion result = new ImsRegion();
            MessageBuffer message = new MessageBuffer();

            region.ToMessage(message, typeof(ImsRegion));
            result.FromMessage(message);

            this.AssertAreEqual(region, new EventRegion(result));
        }

    }
}
