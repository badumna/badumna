﻿//-----------------------------------------------------------------------
// <copyright file="FlatObjectStoreTests.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2011 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using BadumnaTests.Utilities;

namespace BadumnaTests.InterestManagement
{
    using System;
    using System.Collections.Generic;
    using Badumna.Core;
    using Badumna.DataTypes;
    using Badumna.DistributedHashTable;
    using Badumna.InterestManagement;
    using Badumna.Utilities;
    using NUnit.Framework;
    using Rhino.Mocks;
    using Constraints = Rhino.Mocks.Constraints;

    [TestFixture]
    public class FlatObjectStoreTests
    {
        private INetworkEventScheduler eventQueue;
        private FlatObjectStore store;

        private EventHandler<IntersectionEventArgs> intersectionHandler;
        private EventHandler<IntersectionEventArgs> separationHandler;
        private QualifiedName sceneName;
        private ImsRegion nonZeroRegion;
        private ImsRegion zeroRegion;
        private uint ttl;
        private byte knownCollisions;

        private ImsRegion originalRegion;
        private ImsRegion newRegionNonIntersecting;
        private ImsRegion newRegionIntersecting;
        private ImsRegion otherRegionNonIntersecting;
        private ImsRegion otherRegionIntersecting;
        private ImsRegion extraRegionIntersecting;

        [SetUp]
        public void SetUp()
        {
            this.eventQueue = MockRepository.GenerateMock<INetworkEventScheduler>();
            this.store = new FlatObjectStore(this.eventQueue);

            this.intersectionHandler = MockRepository.GenerateMock<EventHandler<IntersectionEventArgs>>();
            this.separationHandler = MockRepository.GenerateMock<EventHandler<IntersectionEventArgs>>();
            this.store.IntersectionNotification += this.intersectionHandler;
            this.store.SeperationNotification += this.separationHandler;

            GenericCallBackReturn<BadumnaId, PeerAddress> generateBadumnaId = BadumnaIdGenerator.FromAddress();

            this.sceneName = new QualifiedName("", Guid.NewGuid().ToString());
            this.nonZeroRegion = new ImsRegion(
                new Vector3(0f, 0f, 0f),
                1f,
                generateBadumnaId(PeerAddress.GetLoopback(1)),
                RegionType.None,
                this.sceneName);
            this.zeroRegion = new ImsRegion(
                new Vector3(0f, 0f, 0f),
                0f,
                generateBadumnaId(PeerAddress.GetLoopback(1)),
                RegionType.None,
                this.sceneName);
            this.ttl = 7;
            this.knownCollisions = 0;

            // Test regions:
            // An original region, two new versions of it (one in same location, and one at a
            // second, non-intersecting location), and two two other regions (one in same location,
            // at the second non-intersecting location).
            this.originalRegion = new ImsRegion(
                new Vector3(0f, 0f, 0f),
                1f,
                generateBadumnaId(PeerAddress.GetLoopback(1)),
                RegionType.Source,
                this.sceneName);
            this.newRegionIntersecting = new ImsRegion(this.originalRegion);
            this.newRegionNonIntersecting = new ImsRegion(this.originalRegion);
            this.newRegionNonIntersecting.Centroid = new Vector3(9f, 9f, 9f);

            this.otherRegionIntersecting = new ImsRegion(this.originalRegion);
            this.otherRegionIntersecting.Guid = generateBadumnaId(new PeerAddress(HashKey.Random()));
            this.otherRegionNonIntersecting = new ImsRegion(this.newRegionNonIntersecting);
            this.otherRegionNonIntersecting.Guid = generateBadumnaId(new PeerAddress(HashKey.Random()));

            this.extraRegionIntersecting = new ImsRegion(this.originalRegion);
            this.extraRegionIntersecting.Guid = generateBadumnaId(new PeerAddress(HashKey.Random()));

            Assert.AreEqual(this.originalRegion.Guid, this.newRegionIntersecting.Guid);
            Assert.AreEqual(this.originalRegion.Guid, this.newRegionNonIntersecting.Guid);
            Assert.AreNotEqual(this.originalRegion.Guid, this.otherRegionIntersecting.Guid);
            Assert.AreNotEqual(this.originalRegion.Guid, this.otherRegionNonIntersecting.Guid);
            Assert.AreNotEqual(this.originalRegion.Guid, this.extraRegionIntersecting.Guid);

            Assert.IsTrue(this.originalRegion.IsIntersecting(this.newRegionIntersecting));
            Assert.IsFalse(this.originalRegion.IsIntersecting(this.newRegionNonIntersecting));
            Assert.IsTrue(this.originalRegion.IsIntersecting(this.otherRegionIntersecting));
            Assert.IsFalse(this.originalRegion.IsIntersecting(this.otherRegionNonIntersecting));
            Assert.IsTrue(this.originalRegion.IsIntersecting(this.extraRegionIntersecting));

            Assert.IsTrue(this.newRegionIntersecting.IsIntersecting(this.newRegionIntersecting));
            Assert.IsFalse(this.newRegionIntersecting.IsIntersecting(this.newRegionNonIntersecting));
            Assert.IsTrue(this.newRegionIntersecting.IsIntersecting(this.otherRegionIntersecting));
            Assert.IsFalse(this.newRegionIntersecting.IsIntersecting(this.otherRegionNonIntersecting));
            Assert.IsTrue(this.newRegionIntersecting.IsIntersecting(this.extraRegionIntersecting));

            Assert.IsFalse(this.newRegionNonIntersecting.IsIntersecting(this.newRegionIntersecting));
            Assert.IsTrue(this.newRegionNonIntersecting.IsIntersecting(this.newRegionNonIntersecting));
            Assert.IsFalse(this.newRegionNonIntersecting.IsIntersecting(this.otherRegionIntersecting));
            Assert.IsTrue(this.newRegionNonIntersecting.IsIntersecting(this.otherRegionNonIntersecting));
            Assert.IsFalse(this.newRegionNonIntersecting.IsIntersecting(this.extraRegionIntersecting));
        }

        [Test]
        public void ConstructorCreatesEmptyStore()
        {
            Assert.IsTrue(this.store.IsEmpty);
        }

        [Test]
        public void IsEmptyReturnsFalseForNonEmptyStore()
        {
            Assert.IsTrue(this.store.IsEmpty);
            this.store.AddOrModifyObject(this.nonZeroRegion, this.ttl, 0);
            Assert.IsFalse(this.store.IsEmpty);
        }

        [Test]
        public void ObjectCountReturnsNumberOfObjectsInStore()
        {
            Assert.AreEqual(0, this.store.ObjectCount);
            this.store.AddOrModifyObject(this.nonZeroRegion, this.ttl, 0);
            Assert.AreEqual(1, this.store.ObjectCount);
        }

        [Test]
        public void HashShutdownReturnsTrueAfterShutdown()
        {
            Assert.IsFalse(this.store.HasShutdown);
            this.store.Shutdown();
            Assert.IsTrue(this.store.HasShutdown);
        }

        [Test]
        [ExpectedException(typeof(InvalidOperationException))]
        public void AddOrModifyObjectThrowsWhenShutdown()
        {
            this.store.Shutdown();
            this.store.AddOrModifyObject(this.nonZeroRegion, this.ttl, this.knownCollisions);
        }

        [Test]
        [ExpectedException(typeof(ImsInvalidRegionException))]
        public void AddOrModifyObjectThrowsForZeroRadiusRegion()
        {
            this.store.AddOrModifyObject(this.zeroRegion, this.ttl, this.knownCollisions);
        }

        [Test]
        public void AddOrModifyObjectSchedulesExpiryEventForNewRegion()
        {
            this.eventQueue.Expect(q => q.Schedule(0, null, this.nonZeroRegion))
                .IgnoreArguments()
                .Constraints(
                    Constraints.Is.Equal((double)this.ttl * 1000.0f),
                    Constraints.Is.Anything(),
                    Constraints.Is.Equal(this.nonZeroRegion))
                .Return(null);
            this.store.AddOrModifyObject(this.nonZeroRegion, this.ttl, 0);
            this.eventQueue.VerifyAllExpectations();
        }

        [Test]
        public void AddOrModifyObjectRemovesExistingExpiryEventForRegion()
        {
            NetworkEvent fakeEvent = new NetworkEvent(TimeSpan.FromMinutes(1.0));
            this.eventQueue.Stub(q => q.Schedule(0, null, this.nonZeroRegion))
                .IgnoreArguments()
                .Constraints(
                    Constraints.Is.Equal((double)this.ttl * 1000.0f),
                    Constraints.Is.Anything(),
                    Constraints.Is.Equal(this.nonZeroRegion))
                .Return(fakeEvent);
            this.store.AddOrModifyObject(this.nonZeroRegion, this.ttl, 0);
            this.eventQueue.Expect(q => q.Remove(fakeEvent));
            this.store.AddOrModifyObject(this.nonZeroRegion, this.ttl, 0);
            this.eventQueue.VerifyAllExpectations();
        }

        [Test]
        public void AddOrModifyObjectSendsNoNotificationsWhenStoreIsEmpty()
        {
            this.intersectionHandler.Expect(h => h.Invoke(null, null)).IgnoreArguments().Repeat.Never();
            this.separationHandler.Expect(h => h.Invoke(null, null)).IgnoreArguments().Repeat.Never();
            this.store.AddOrModifyObject(this.nonZeroRegion, this.ttl, this.knownCollisions);
            this.intersectionHandler.VerifyAllExpectations();
            this.separationHandler.VerifyAllExpectations();
        }

        [Test]
        public void AddOrModifyObjectDoesNotNotifyCollisionsWithSelf()
        {
            this.intersectionHandler.Expect(h => h.Invoke(null, null)).IgnoreArguments().Repeat.Never();
            this.separationHandler.Expect(h => h.Invoke(null, null)).IgnoreArguments().Repeat.Never();
            this.store.AddOrModifyObject(this.originalRegion, this.ttl, this.knownCollisions);
            this.store.AddOrModifyObject(this.newRegionIntersecting, this.ttl, this.knownCollisions);
            this.intersectionHandler.VerifyAllExpectations();
            this.separationHandler.VerifyAllExpectations();
        }

        [Test]
        public void AddOrModifyObjectSendsNotificationsWhenIntersectingRegionExists()
        {
            bool intersectionNotificationCalled = false;
            bool reciprocalIntersectionNotificationCalled = false;

            this.intersectionHandler.Expect(h => h.Invoke(null, null))
                .IgnoreArguments()
                .WhenCalled(c =>
                    {
                        Assert.AreEqual(c.Arguments[0], this.store);
                        IntersectionEventArgs e = (IntersectionEventArgs)c.Arguments[1];
                        if (e.Region == this.originalRegion)
                        {
                            Assert.AreEqual(this.otherRegionIntersecting, e.ChangedRegions[0]);
                            intersectionNotificationCalled = true;
                        }
                        else
                        {
                            Assert.AreEqual(e.Region, this.otherRegionIntersecting);
                            Assert.AreEqual(this.originalRegion, e.ChangedRegions[0]);
                            reciprocalIntersectionNotificationCalled = true;
                        }
                    });
            this.separationHandler.Expect(h => h.Invoke(null, null)).IgnoreArguments().Repeat.Never();
            this.store.AddOrModifyObject(this.originalRegion, this.ttl, this.knownCollisions);
            this.store.AddOrModifyObject(this.otherRegionIntersecting, this.ttl, this.knownCollisions);
            this.intersectionHandler.VerifyAllExpectations();
            this.separationHandler.VerifyAllExpectations();
            Assert.IsTrue(intersectionNotificationCalled);
            Assert.IsTrue(reciprocalIntersectionNotificationCalled);
        }

        [Test]
        public void AddOrModifyObjectCombinesNotificationsWhenMultipleIntersectingRegionExists()
        {
            this.store.AddOrModifyObject(this.originalRegion, this.ttl, this.knownCollisions);
            this.store.AddOrModifyObject(this.otherRegionIntersecting, this.ttl, this.knownCollisions);
            bool firstIntersectionNotificationCalled = false;
            bool secondIntersectionNotificationCalled = false;
            bool reciprocalIntersectionsNotificationCalled = false;
            this.intersectionHandler.Expect(h => h.Invoke(null, null))
                .IgnoreArguments()
                .WhenCalled(c =>
                {
                    Assert.AreEqual(c.Arguments[0], this.store);
                    IntersectionEventArgs e = (IntersectionEventArgs)c.Arguments[1];
                    if (e.Region == this.originalRegion)
                    {
                        Assert.AreEqual(this.extraRegionIntersecting, e.ChangedRegions[0]);
                        firstIntersectionNotificationCalled = true;
                    }
                    else if (e.Region == this.otherRegionIntersecting)
                    {
                        Assert.AreEqual(this.extraRegionIntersecting, e.ChangedRegions[0]);
                        secondIntersectionNotificationCalled = true;
                    }
                    else
                    {
                        Assert.AreEqual(e.Region, this.extraRegionIntersecting);
                        Assert.IsTrue(e.ChangedRegions.Contains(this.originalRegion));
                        Assert.IsTrue(e.ChangedRegions.Contains(this.otherRegionIntersecting));
                        reciprocalIntersectionsNotificationCalled = true;
                    }
                });
            this.separationHandler.Expect(h => h.Invoke(null, null)).IgnoreArguments().Repeat.Never();
            this.store.AddOrModifyObject(this.extraRegionIntersecting, this.ttl, this.knownCollisions);
            this.intersectionHandler.VerifyAllExpectations();
            this.separationHandler.VerifyAllExpectations();
            Assert.IsTrue(firstIntersectionNotificationCalled);
            Assert.IsTrue(secondIntersectionNotificationCalled);
            Assert.IsTrue(reciprocalIntersectionsNotificationCalled);
        }

        [Test]
        public void AddOrModifyObjectSendsReciprocalSeparationNotificationsWhenAllCollisionsAreKnown()
        {
            this.store.AddOrModifyObject(this.originalRegion, this.ttl, this.knownCollisions);
            this.store.AddOrModifyObject(this.otherRegionIntersecting, this.ttl, this.knownCollisions);
            bool separationNotificationCalled = false;
            bool reciprocalSeparationNotificationCalled = false;
            this.separationHandler.Expect(h => h.Invoke(null, null))
                .IgnoreArguments()
                .WhenCalled(c =>
                {
                    Assert.AreEqual(c.Arguments[0], this.store);
                    IntersectionEventArgs e = (IntersectionEventArgs)c.Arguments[1];
                    if (e.Region == this.newRegionNonIntersecting)
                    {
                        Assert.AreEqual(this.otherRegionIntersecting, e.ChangedRegions[0]);
                        separationNotificationCalled = true;
                    }
                    else
                    {
                        Assert.AreEqual(e.Region, this.otherRegionIntersecting);
                        Assert.AreEqual(this.newRegionNonIntersecting, e.ChangedRegions[0]);
                        reciprocalSeparationNotificationCalled = true;
                    }
                });
            this.intersectionHandler.Expect(h => h.Invoke(null, null)).IgnoreArguments().Repeat.Never();
            
            // Known collisions matches number of collisions recorded in store.
            this.store.AddOrModifyObject(this.newRegionNonIntersecting, this.ttl, 1);
            this.intersectionHandler.VerifyAllExpectations();
            this.separationHandler.VerifyAllExpectations();
            Assert.IsTrue(separationNotificationCalled);
            Assert.IsTrue(reciprocalSeparationNotificationCalled);
        }

        // TO DO: is this behaviour correct? Shouldn't notifications be sent anyway?
        // Separation notifications no longer used.
        [Test, Ignore]
        public void AddOrModifyObjectSendsUnidirectionalSeparationNotificationsWhenUnkownCollisionsExist()
        {
            this.store.AddOrModifyObject(this.originalRegion, this.ttl, this.knownCollisions);
            this.store.AddOrModifyObject(this.otherRegionIntersecting, this.ttl, this.knownCollisions);
            this.separationHandler.Expect(h => h.Invoke(null, null))
                .IgnoreArguments()
                .WhenCalled(c =>
                {
                    Assert.AreEqual(c.Arguments[0], this.store);
                    IntersectionEventArgs e = (IntersectionEventArgs)c.Arguments[1];
                    Assert.AreEqual(this.otherRegionIntersecting, e.Region);
                    Assert.AreEqual(this.newRegionNonIntersecting, e.ChangedRegions[0]);
                });
            this.intersectionHandler.Expect(h => h.Invoke(null, null)).IgnoreArguments().Repeat.Never();

            // Known collisions less than number of collisions in store.
            this.store.AddOrModifyObject(this.newRegionNonIntersecting, this.ttl, 0);
            this.intersectionHandler.VerifyAllExpectations();
            this.separationHandler.VerifyAllExpectations();
        }

        [Test]
        public void AddOrModifyObjectCombinesNotificationsWhenMultipleSeparationsOccur()
        {
            this.store.AddOrModifyObject(this.originalRegion, this.ttl, this.knownCollisions);
            this.store.AddOrModifyObject(this.otherRegionIntersecting, this.ttl, this.knownCollisions);
            this.store.AddOrModifyObject(this.extraRegionIntersecting, this.ttl, this.knownCollisions);

            bool firstSeparationNotificationCalled = false;
            bool secondSeparationNotificationCalled = false;
            bool reciprocalSeparationsNotificationCalled = false;
            this.separationHandler.Expect(h => h.Invoke(null, null))
                .IgnoreArguments()
                .WhenCalled(c =>
                {
                    Assert.AreEqual(c.Arguments[0], this.store);
                    IntersectionEventArgs e = (IntersectionEventArgs)c.Arguments[1];
                    if (e.Region == this.otherRegionIntersecting)
                    {
                        Assert.AreEqual(this.newRegionNonIntersecting, e.ChangedRegions[0]);
                        firstSeparationNotificationCalled = true;
                    }
                    else if (e.Region == this.extraRegionIntersecting)
                    {
                        Assert.AreEqual(this.newRegionNonIntersecting, e.ChangedRegions[0]);
                        secondSeparationNotificationCalled = true;
                    }
                    else
                    {
                        Assert.AreEqual(e.Region, this.newRegionNonIntersecting);
                        Assert.IsTrue(e.ChangedRegions.Contains(this.otherRegionIntersecting));
                        Assert.IsTrue(e.ChangedRegions.Contains(this.extraRegionIntersecting));
                        reciprocalSeparationsNotificationCalled = true;
                    }
                });
            this.intersectionHandler.Expect(h => h.Invoke(null, null)).IgnoreArguments().Repeat.Never();
            
            // Known collisions matches number of collisions in store.
            this.store.AddOrModifyObject(this.newRegionNonIntersecting, this.ttl, 2);
            this.intersectionHandler.VerifyAllExpectations();
            this.separationHandler.VerifyAllExpectations();
            Assert.IsTrue(firstSeparationNotificationCalled);
            Assert.IsTrue(secondSeparationNotificationCalled);
            Assert.IsTrue(reciprocalSeparationsNotificationCalled);
        }

        [Test]
        [ExpectedException(typeof(InvalidOperationException))]
        public void KeepAliveThrowsWhenShutDown()
        {
            this.store.Shutdown();
            this.store.KeepAlive(this.nonZeroRegion.Guid, this.ttl);
        }

        [Test]
        public void KeepAliveDoesNotScheduleEventForUnknownObject()
        {
            this.eventQueue.Expect(q => q.Schedule(0, r => { }, this.nonZeroRegion))
                .IgnoreArguments()
                .Repeat.Never();
            this.store.KeepAlive(this.nonZeroRegion.Guid, this.ttl);
            this.eventQueue.VerifyAllExpectations();
        }

        [Test, Ignore]
        public void KeepAliveCancelsExistingExpirationEvent()
        {
            // TO DO: this test currently fails. It really needs to pass if KeepAlive is going to be used.
            NetworkEvent fakeEvent = new NetworkEvent(TimeSpan.FromMinutes(1));
            this.eventQueue.Stub(q => q.Schedule(0, null, this.nonZeroRegion))
                .IgnoreArguments()
                .Constraints(
                    Constraints.Is.Anything(),
                    Constraints.Is.Anything(),
                    Constraints.Is.Equal(this.nonZeroRegion))
                .Return(fakeEvent);
            this.store.AddOrModifyObject(this.nonZeroRegion, this.ttl, this.knownCollisions);
            this.eventQueue.Expect(q => q.Remove(fakeEvent)).IgnoreArguments();
            this.store.KeepAlive(this.nonZeroRegion.Guid, this.ttl);
            this.eventQueue.VerifyAllExpectations();
        }

        [Test]
        public void KeepAliveSchedulesNewExpirationEvent()
        {
            this.store.AddOrModifyObject(this.nonZeroRegion, this.ttl, this.knownCollisions);
            this.eventQueue.Expect(q => q.Schedule(0, r => { }, this.nonZeroRegion))
                .IgnoreArguments()
                .Constraints(
                    Constraints.Is.Equal((double)this.ttl * 1000.0),
                    Constraints.Is.Anything(),
                    Constraints.Is.Equal(this.nonZeroRegion))
                .Return(null);
            this.store.KeepAlive(this.nonZeroRegion.Guid, this.ttl);
            this.eventQueue.VerifyAllExpectations();
        }

        [Test]
        [ExpectedException(typeof(InvalidOperationException))]
        public void RemoveObjectThrowsWhenShutdown()
        {
            this.store.Shutdown();
            this.store.RemoveObject(this.nonZeroRegion);
        }

        [Test]
        public void RemoveObjectRemovesObjectFromStore()
        {
            this.store.AddOrModifyObject(this.nonZeroRegion, this.ttl, this.knownCollisions);
            Assert.IsFalse(this.store.IsEmpty);
            this.store.RemoveObject(this.nonZeroRegion);
            Assert.IsTrue(this.store.IsEmpty);
        }

        [Test]
        public void RemoveObjectRemovesOnlySingleObjectFromStore()
        {
            this.store.AddOrModifyObject(this.originalRegion);
            this.store.AddOrModifyObject(this.otherRegionNonIntersecting);
            Assert.AreEqual(2, this.store.ObjectCount);
            this.store.RemoveObject(this.newRegionIntersecting);
            Assert.AreEqual(1, this.store.ObjectCount);
        }

        [Test]
        public void RemoveObjectCancelsExpirationEvent()
        {
            NetworkEvent fakeEvent = new NetworkEvent(TimeSpan.FromMinutes(1));
            this.eventQueue.Expect(q => q.Schedule(0, null, this.originalRegion))
                .IgnoreArguments()
                .Return(fakeEvent);
            this.store.AddOrModifyObject(this.originalRegion);
            this.eventQueue.Expect(q => q.Remove(fakeEvent));
            this.store.RemoveObject(this.newRegionIntersecting);
            this.eventQueue.VerifyAllExpectations();
        }
        
        [Test]
        public void RemoveObjectSendsSeparationNotifications()
        {
            this.store.AddOrModifyObject(this.originalRegion);
            this.store.AddOrModifyObject(this.otherRegionIntersecting);
            bool separationNotificationCalled = false;
            bool reciprocalSeparationNotificationCalled = false;
            this.separationHandler.Expect(h => h.Invoke(null, null))
                .IgnoreArguments()
                .WhenCalled(c =>
                {
                    Assert.AreEqual(c.Arguments[0], this.store);
                    IntersectionEventArgs e = (IntersectionEventArgs)c.Arguments[1];
                    if (e.Region == this.newRegionIntersecting)
                    {
                        Assert.AreEqual(this.otherRegionIntersecting, e.ChangedRegions[0]);
                        separationNotificationCalled = true;
                    }
                    else
                    {
                        Assert.AreEqual(e.Region, this.otherRegionIntersecting);
                        Assert.AreEqual(this.newRegionIntersecting, e.ChangedRegions[0]);
                        reciprocalSeparationNotificationCalled = true;
                    }
                });
            this.store.RemoveObject(this.newRegionIntersecting);
            this.separationHandler.VerifyAllExpectations();
            Assert.IsTrue(separationNotificationCalled);
            Assert.IsTrue(reciprocalSeparationNotificationCalled);
        }

        [Test]
        [ExpectedException(typeof(InvalidOperationException))]
        public void RemoveWithoutNotificationThrowsWhenShutdown()
        {
            this.store.Shutdown();
            this.store.RemoveWithoutNotification(this.nonZeroRegion);
        }

        [Test]
        public void RemoveWithoutNotificationRemovesObjectFromStore()
        {
            this.store.AddOrModifyObject(this.nonZeroRegion, this.ttl, this.knownCollisions);
            Assert.IsFalse(this.store.IsEmpty);
            this.store.RemoveWithoutNotification(this.nonZeroRegion);
            Assert.IsTrue(this.store.IsEmpty);
        }

        [Test]
        public void RemoveWithoutNotificationRemovesOnlySingleObjectFromStore()
        {
            this.store.AddOrModifyObject(this.originalRegion);
            this.store.AddOrModifyObject(this.otherRegionNonIntersecting);
            Assert.AreEqual(2, this.store.ObjectCount);
            this.store.RemoveWithoutNotification(this.newRegionIntersecting);
            Assert.AreEqual(1, this.store.ObjectCount);
        }

        [Test]
        public void RemoveWithoutNotificationCancelsExpirationEvent()
        {
            NetworkEvent fakeEvent = new NetworkEvent(TimeSpan.FromMinutes(1));
            this.eventQueue.Expect(q => q.Schedule(0, null, this.originalRegion))
                .IgnoreArguments()
                .Return(fakeEvent);
            this.store.AddOrModifyObject(this.originalRegion);
            this.eventQueue.Expect(q => q.Remove(fakeEvent));
            this.store.RemoveWithoutNotification(this.newRegionIntersecting);
            this.eventQueue.VerifyAllExpectations();
        }

        [Test]
        public void RemoveWithoutNotificationSendsNoSeparationNotifications()
        {
            this.store.AddOrModifyObject(this.originalRegion);
            this.store.AddOrModifyObject(this.otherRegionIntersecting);
            this.separationHandler.Expect(h => h.Invoke(null, null))
                .IgnoreArguments()
                .Repeat.Never();
            this.store.RemoveWithoutNotification(this.newRegionIntersecting);
            this.separationHandler.VerifyAllExpectations();
        }

        [Test]
        [ExpectedException(typeof(InvalidOperationException))]
        public void RemoveAllObjectsThrowsWhenShutdown()
        {
            this.store.Shutdown();
            this.store.RemoveAllObjects();
        }

        [Test]
        public void RemoveAllObjectsRemovesAllObjectFromStore()
        {
            this.store.AddOrModifyObject(this.originalRegion);
            this.store.AddOrModifyObject(this.otherRegionNonIntersecting);
            Assert.AreEqual(2, this.store.ObjectCount);
            this.store.RemoveAllObjects();
            Assert.IsTrue(this.store.IsEmpty);
        }

        [Test, Ignore]
        public void RemoveAllObjectsCancelsExpirationEvents()
        {
            // TODO: It doesn't, but should it?
            // If so, add this code to FlatObjectStore.RemoveAllObjects():
            ////foreach (NetworkEvent expirationEvent in this.expirationEvents.Values)
            ////{
            ////    this.eventQueue.Remove(expirationEvent);
            ////}

            NetworkEvent fakeEvent1 = new NetworkEvent(TimeSpan.FromMinutes(1));
            NetworkEvent fakeEvent2 = new NetworkEvent(TimeSpan.FromMinutes(1));
            this.eventQueue.Stub(q => q.Schedule(0, null, this.originalRegion))
                .IgnoreArguments()
                .Constraints(
                    Constraints.Is.Anything(),
                    Constraints.Is.Anything(),
                    Constraints.Is.Equal(this.originalRegion))
                .Return(fakeEvent1);
            this.eventQueue.Stub(q => q.Schedule(0, null, this.otherRegionNonIntersecting))
                .IgnoreArguments()
                .Constraints(
                    Constraints.Is.Anything(),
                    Constraints.Is.Anything(),
                    Constraints.Is.Equal(this.otherRegionNonIntersecting))
                .Return(fakeEvent2);
            this.store.AddOrModifyObject(this.originalRegion);
            this.store.AddOrModifyObject(this.otherRegionNonIntersecting);
            this.eventQueue.Expect(q => q.Remove(fakeEvent1));
            this.eventQueue.Expect(q => q.Remove(fakeEvent2));
            this.store.RemoveAllObjects();
            this.eventQueue.VerifyAllExpectations();
        }

        [Test]
        public void RemoveAllObjectsSendsNoNotifications()
        {
            this.store.AddOrModifyObject(this.originalRegion);
            this.store.AddOrModifyObject(this.otherRegionIntersecting);
            Assert.AreEqual(2, this.store.ObjectCount);
            this.separationHandler.Expect(h => h.Invoke(null, null))
                .IgnoreArguments()
                .Repeat.Never();
            this.intersectionHandler.Expect(h => h.Invoke(null, null))
                .IgnoreArguments()
                .Repeat.Never();
            this.store.RemoveAllObjects();
            this.separationHandler.VerifyAllExpectations();
            this.intersectionHandler.VerifyAllExpectations();
        }

        [Test]
        [ExpectedException(typeof(ImsInvalidRegionException))]
        public void GetObjectsWithinRegionThrowsForZeroRadiusRegion()
        {
            this.store.GetObjectsWithinRegion(this.zeroRegion);
        }

        [Test]
        [ExpectedException(typeof(InvalidOperationException))]
        public void GetObjectsWithinRegionThrowsWhenShutdown()
        {
            this.store.Shutdown();
            this.store.GetObjectsWithinRegion(this.zeroRegion);
        }

        [Test]
        public void GetObjectsWithinRegionReturnsOnlyIntersectingRegions()
        {
            this.store.AddOrModifyObject(this.otherRegionIntersecting);
            this.store.AddOrModifyObject(this.otherRegionNonIntersecting);
            IList<ImsRegion> intersectingRegions = this.store.GetObjectsWithinRegion(this.originalRegion);
            Assert.IsTrue(intersectingRegions.Contains(this.otherRegionIntersecting));
            Assert.IsFalse(intersectingRegions.Contains(this.otherRegionNonIntersecting));
        }

        [Test]
        public void GetObjectsWithinRegionIncludesOriginalRegion()
        {
            // TODO: Is this intentional, or should it be excluded?
            this.store.AddOrModifyObject(this.newRegionIntersecting);
            IList<ImsRegion> intersectingRegions = this.store.GetObjectsWithinRegion(this.originalRegion);
            Assert.IsTrue(intersectingRegions.Contains(this.newRegionIntersecting));
        }
    }
}
