﻿//-----------------------------------------------------------------------
// <copyright file="InterestManagementClientProxyTests.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2010 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BadumnaTests.InterestManagement
{
    using System;
    using System.Collections.Generic;
    using Badumna.Core;
    using Badumna.DataTypes;
    using Badumna.InterestManagement;
    using Badumna.Utilities;
    using BadumnaTests.Core;
    using NUnit.Framework;
    using Rhino.Mocks;

    [TestFixture]
    public class InterestManagementClientProxyTests
    {
        private FakeInterestManagementClientProxy proxy;
        private MessageParser messageParser;
        private InterestManagementClientProtocol protocol;
        private IEnvelope envelope;
        private QualityOfService envelopeQos;
        private QualityOfService proxyQos;
        private PeerAddress destination;

        [SetUp]
        public void SetUp()
        {
            this.messageParser = MockRepository.GenerateMock<MessageParser>(
                Guid.NewGuid().ToString(),
                typeof(DummyProtocolMethodAttribute),
                MockRepository.GenerateMock<GenericCallBackReturn<object, Type>>());
            this.protocol = MockRepository.GenerateMock<InterestManagementClientProtocol>();
            this.envelope = MockRepository.GenerateMock<IEnvelope>();
            this.envelopeQos = new QualityOfService();
            this.proxyQos = new QualityOfService();
            this.envelope.Stub(e => e.Qos).Return(this.envelopeQos);
            this.destination = PeerAddress.Nowhere;
            this.proxy = new FakeInterestManagementClientProxy(
                this.messageParser,
                this.protocol,
                this.envelope);
            this.proxy.MessageDestination = this.destination;
            this.proxy.MessageQos = this.proxyQos;
        }

        [Test]
        public void StatusResponsePreparesEnvelope()
        {
            this.proxy.StatusResponse(null, null);
            Assert.AreEqual(1, this.proxy.PrepareReplyMessageCallCount);
        }

        [Test]
        public void StatusResponseSetsEnvelopeQosHigh()
        {
            this.proxy.StatusResponse(null, null);

            // TO DO: Make testable by making Qos interface
            Assert.AreEqual(
                (float)QosPriority.High / (float)byte.MaxValue,
                this.envelopeQos.PriorityProbability);
        }

        [Test]
        public void StatusResponseMakesRemoteCallViaMessageParser()
        {
            BadumnaId id = new BadumnaId(PeerAddress.Nowhere, 99);
            RequestStatus status = new RequestStatus();
            this.messageParser.Expect(
                p => p.RemoteCall(
                    this.envelope,
                    this.protocol.StatusResponse,
                    id,
                    status));
            this.proxy.StatusResponse(id, status);
            this.messageParser.VerifyAllExpectations();
        }

        [Test]
        public void StatusResponseDispatchesMessage()
        {
            this.proxy.StatusResponse(null, null);
            Assert.AreEqual(1, this.proxy.DispatchMessageCallCount);
        }

        [Test]
        public void IntersectionNotificationPreparesEnvelope()
        {
            this.proxy.IntersectionNotification(null, null);
            Assert.AreEqual(1, this.proxy.PrepareReplyMessageCallCount);
        }

        [Test]
        public void IntersectionNotificationSetsEnvelopeQosHigh()
        {
            this.proxy.IntersectionNotification(null, null);

            // TO DO: Make testable by making Qos interface
            Assert.AreEqual(
                (float)QosPriority.High / (float)byte.MaxValue,
                this.envelopeQos.PriorityProbability);
        }

        [Test]
        public void IntersectionNotificationMakesRemoteCallViaMessageParser()
        {
            BadumnaId id = new BadumnaId(PeerAddress.Nowhere, 99);
            IList<ImsEventArgs> enterList = new List<ImsEventArgs>();
            this.messageParser.Expect(
                p => p.RemoteCall(
                    this.envelope,
                    this.protocol.IntersectionNotification,
                    id,
                    enterList));
            this.proxy.IntersectionNotification(id, enterList);
            this.messageParser.VerifyAllExpectations();
        }

        [Test]
        public void IntersectionNotificationDispatchesMessage()
        {
            this.proxy.IntersectionNotification(null, null);
            Assert.AreEqual(1, this.proxy.DispatchMessageCallCount);
        }

        #region Helper methods

        #endregion // Helper methods

        #region Fakes

        /// <summary>
        /// Fake to permit testing of astract class.
        /// </summary>
        internal class FakeInterestManagementClientProxy : InterestManagementClientProxy
        {
            private IEnvelope envelope;

            public FakeInterestManagementClientProxy(
                MessageParser messageParser,
                InterestManagementClientProtocol clientProtocol,
                IEnvelope envelope)
                : base(messageParser, clientProtocol)
            {
                this.envelope = envelope;
            }

            public int PrepareReplyMessageCallCount { get; set; }

            public int DispatchMessageCallCount { get; set; }

            protected override IEnvelope PrepareReplyMessage()
            {
                this.PrepareReplyMessageCallCount++;
                return this.envelope;
            }

            protected override void DispatchMessage(IEnvelope envelope)
            {
                this.DispatchMessageCallCount++;
            }
        }

        #endregion // Fakes
    }
}