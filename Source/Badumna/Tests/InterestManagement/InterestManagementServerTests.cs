﻿//-----------------------------------------------------------------------
// <copyright file="InterestManagementServerTests.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2010 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BadumnaTests.InterestManagement
{
    using System;
    using Badumna.Core;
    using Badumna.DataTypes;
    using Badumna.DistributedHashTable;
    using Badumna.InterestManagement;
    using Badumna.Utilities;
    using BadumnaTests.Core;
    using NUnit.Framework;
    using Rhino.Mocks;

    [TestFixture]
    public class InterestManagementServerTests
    {
        private InterestManagementServer server;
        private MessageParser messageParser;
        private ObjectStoreFactory objectStoreFactory;
        private IInterestManagementClientProxy clientProxy;
        private QualifiedName sceneName;
        private ImsRegion region;
        private ImsRegion secondRegion;
        private ImsRegion zeroRadiusRegion;
        private ISpatialObjectStore store;
        private INetworkEventScheduler eventQueue;
        private INetworkConnectivityReporter connectivityReporter;

        [SetUp]
        public void SetUp()
        {
            this.sceneName = new QualifiedName("", Guid.NewGuid().ToString());
            this.region = new ImsRegion(
                new Vector3(0f, 0f, 0f),
                1f,
                new BadumnaId(PeerAddress.GetLoopback(1), 1),   // ensure that the source and sink regions have different addresses so that intersection notifications are generated.
                RegionType.Source,
                this.sceneName);
            this.secondRegion = new ImsRegion(
                new Vector3(0f, 0f, 0f),
                1f,
                new BadumnaId(PeerAddress.GetLoopback(2), 1),
                RegionType.None,
                this.sceneName);
            this.zeroRadiusRegion = new ImsRegion(
                new Vector3(0f, 0f, 0f),
                0f,
                new BadumnaId(PeerAddress.GetLoopback(2), 2),
                RegionType.None,
                this.sceneName);
            this.clientProxy = MockRepository.GenerateMock<IInterestManagementClientProxy>();
            this.store = MockRepository.GenerateMock<ISpatialObjectStore>();
            this.objectStoreFactory = MockRepository.GenerateMock<ObjectStoreFactory>();
            this.messageParser = MockRepository.GenerateMock<MessageParser>(
                Guid.NewGuid().ToString(),
                typeof(DummyProtocolMethodAttribute),
                MockRepository.GenerateMock<GenericCallBackReturn<object, Type>>());
            this.eventQueue = MockRepository.GenerateMock<INetworkEventScheduler>();
            this.connectivityReporter = MockRepository.GenerateMock<INetworkConnectivityReporter>();
            this.server = new FakeInterestManagementServer(
                this.messageParser,
                this.objectStoreFactory,
                this.clientProxy,
                this.eventQueue,
                this.connectivityReporter);
        }

        #region Tests

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ConstructorThrowsForNullObjectStoreFactory()
        {
            this.server = new FakeInterestManagementServer(this.messageParser, null, this.clientProxy, this.eventQueue, this.connectivityReporter);
        }

        [Test]
        public void ConstructorRegistersMethodsWithMessageParser()
        {
            InterestManagementServer passedServer = null;
            this.messageParser.Expect(p => p.RegisterMethodsIn(null))
                .IgnoreArguments()
                .WhenCalled(call =>
                    {
                        passedServer = (InterestManagementServer)call.Arguments[0];
                    });
            this.server = new FakeInterestManagementServer(
                this.messageParser,
                this.objectStoreFactory,
                this.clientProxy,
                this.eventQueue,
                this.connectivityReporter);
            this.messageParser.VerifyAllExpectations();
            Assert.AreEqual(passedServer, this.server);
        }

        // TO DO: InsertRequest with null region throws NullReferenceException. Should either:
        //  - throw null argument exception, or
        //  - just return invalid region response status.
        ////[Test]
        ////public void InsertRequestWithNullRegionResultsInInvalidRegionStatusResponse()
        ////{
        ////    this.clientProxy.Expect(p => p.StatusResponse(null, null))
        ////        .IgnoreArguments()
        ////        .WhenCalled(call =>
        ////            {
        ////                Assert.AreEqual(
        ////                    RequestStatus.Statuses.InvalidRegion,
        ////                    ((RequestStatus)call.Arguments[1]).Status);
        ////            });
        ////    this.server.InsertRequest(this.sceneName, null, 7);
        ////    this.clientProxy.VerifyAllExpectations();
        ////}

        [Test]
        public void InsertRequestWithZeroRadiusRegionResultsInInvalidRegionStatusResponse()
        {
            this.ExpectStatusResponseToClientProxy(RequestStatus.Statuses.InvalidRegion);
            this.server.InsertRequest(this.sceneName, this.zeroRadiusRegion, 7);
            this.clientProxy.VerifyAllExpectations();
        }

        [Test]
        public void InsertRequestForNewSceneNameCreatesNewObjectStore()
        {
            this.objectStoreFactory.Expect(f => f.Invoke(this.eventQueue));
            this.server.InsertRequest(this.sceneName, this.region, 7);
            this.objectStoreFactory.VerifyAllExpectations();
        }

        [Test]
        public void InsertRequestForKnownSceneNameDoesNotCreatesNewObjectStore()
        {
            this.objectStoreFactory.Stub(f => f.Invoke(this.eventQueue)).Return(this.store);
            this.server.InsertRequest(this.sceneName, this.region, 7);
            this.objectStoreFactory.Expect(f => f.Invoke(this.eventQueue)).Repeat.Never();
            this.server.InsertRequest(this.sceneName, this.secondRegion, 7);
            this.objectStoreFactory.VerifyAllExpectations();
        }

        [Test]
        public void InsertRequestRepliesWithNoSuchServiceStatusWhenStoreCreationThrowsImsException()
        {
            this.objectStoreFactory.Stub(f => f.Invoke(this.eventQueue)).Throw(new ImsException("dummy"));
            this.ExpectStatusResponseToClientProxy(RequestStatus.Statuses.NoSuchService);
            this.server.InsertRequest(this.sceneName, this.region, 7);
            this.clientProxy.VerifyAllExpectations();
        }

        [Test]
        public void InsertRequestRepliesWithExceptionStatusWhenStoreThrows()
        {
            this.store.Stub(s => s.AddOrModifyObject(null, 0, 0))
                .IgnoreArguments()
                .WhenCalled(call => { throw new ImsException("Fake"); });
            this.objectStoreFactory.Stub(f => f.Invoke(this.eventQueue)).Return(this.store);
            this.ExpectStatusResponseToClientProxy(RequestStatus.Statuses.ExceptionThrownByOperation);
            this.server.InsertRequest(this.sceneName, this.region, 7);
            this.clientProxy.VerifyAllExpectations();
        }

        [Test]
        public void InsertRequestsRepliedToReliably()
        {
            this.objectStoreFactory.Stub(f => f.Invoke(this.eventQueue)).Return(this.store);
            this.clientProxy.Expect(p => p.MessageQos = QualityOfService.Reliable);
            this.server.InsertRequest(this.sceneName, this.region, 7);
            this.clientProxy.VerifyAllExpectations();
        }

        [Test]
        public void InsertRequestRepliesSentToRegionAddress()
        {
            this.objectStoreFactory.Stub(f => f.Invoke(this.eventQueue)).Return(this.store);
            this.clientProxy.Expect(p => p.MessageDestination = this.region.Guid.Address);
            this.server.InsertRequest(this.sceneName, this.region, 7);
            this.clientProxy.VerifyAllExpectations();
        }

        // TO DO: Confirm intended design, since RemoveRequest for null region will actually throw.

        ////[Test]
        ////public void RemoveRequestForNullRegionRepliesWithInvalidRegion()
        ////{
        ////    this.ExpectStatusResponseToClientProxy(RequestStatus.Statuses.InvalidRegion);
        ////    this.server.RemoveRequest(this.sceneName, null);
        ////    this.clientProxy.VerifyAllExpectations();
        ////}

        [Test]
        public void RemoveRequestForNewSceneNameCreatesNewObjectStore()
        {
            this.objectStoreFactory.Expect(f => f.Invoke(this.eventQueue))
                .IgnoreArguments()
                .IgnoreArguments();
            this.server.RemoveRequest(this.sceneName, this.region);
            this.objectStoreFactory.VerifyAllExpectations();
        }

        [Test]
        public void RemoveRequestForknownSceneNameDoesNotCreatesNewObjectStore()
        {
            this.objectStoreFactory.Stub(f => f.Invoke(this.eventQueue))
                .Return(this.store);
            this.server.RemoveRequest(this.sceneName, this.region);
            this.objectStoreFactory.Expect(f => f.Invoke(this.eventQueue))
                .Repeat.Never();
            this.server.RemoveRequest(this.sceneName, this.secondRegion);
            this.objectStoreFactory.VerifyAllExpectations();
        }

        [Test]
        public void RemoveRequestRemovesRegionFromObjectStore()
        {
            this.store.Expect(s => s.RemoveObject(this.region));
            this.objectStoreFactory.Stub(f => f.Invoke(this.eventQueue)).Return(this.store);
            this.server.RemoveRequest(this.sceneName, this.region);
            this.store.VerifyAllExpectations();
        }

        [Test]
        public void RemoveRequestRepliesWithExceptionStatusWhenStoreThrows()
        {
            this.store.Stub(s => s.RemoveObject(this.region))
                .IgnoreArguments()
                .WhenCalled(call => { throw new ImsException("Fake"); });
            this.objectStoreFactory.Stub(f => f.Invoke(this.eventQueue)).Return(this.store);
            this.ExpectStatusResponseToClientProxy(RequestStatus.Statuses.ExceptionThrownByOperation);
            this.server.RemoveRequest(this.sceneName, this.region);
            this.clientProxy.VerifyAllExpectations();
        }

        [Test]
        public void RemoveRequestRepliesWithSuccessWhenRegionRemoved()
        {
            this.objectStoreFactory.Stub(f => f.Invoke(this.eventQueue)).Return(this.store);
            this.ExpectStatusResponseToClientProxy(RequestStatus.Statuses.Success);
            this.server.RemoveRequest(this.sceneName, this.region);
            this.clientProxy.VerifyAllExpectations();
        }

        [Test]
        public void RemoveRequestsRepliedToUnreliably()
        {
            this.objectStoreFactory.Stub(f => f.Invoke(this.eventQueue)).Return(this.store);
            this.clientProxy.Expect(p => p.MessageQos = QualityOfService.Unreliable);
            this.server.RemoveRequest(this.sceneName, this.region);
            this.clientProxy.VerifyAllExpectations();
        }

        [Test]
        public void RemoveRequestRepliesSentToRegionAddress()
        {
            this.objectStoreFactory.Stub(f => f.Invoke(this.eventQueue)).Return(this.store);
            this.clientProxy.Expect(p => p.MessageDestination = this.region.Guid.Address);
            this.server.RemoveRequest(this.sceneName, this.region);
            this.clientProxy.VerifyAllExpectations();
        }

        [Test]
        public void RemoveWithoutNotificationCreatesStoreIfNotPresent()
        {
            this.objectStoreFactory.Expect(f => f.Invoke(this.eventQueue));
            this.server.RemoveWithoutNotificationRequest(this.sceneName, this.region);
            this.objectStoreFactory.VerifyAllExpectations();
        }

        [Test]
        public void RemoveWithoutNotificationRemovesRegionFromStore()
        {
            this.objectStoreFactory.Stub(f => f.Invoke(this.eventQueue)).Return(this.store);
            this.store.Expect(s => s.RemoveWithoutNotification(this.region));
            this.server.RemoveWithoutNotificationRequest(this.sceneName, this.region);
            this.store.VerifyAllExpectations();
        }

        [Test]
        public void RemoveWithoutNotificationRepliesWithExceptionStatusIfStoreThrows()
        {
            this.objectStoreFactory.Stub(f => f.Invoke(this.eventQueue)).Return(this.store);
            this.store.Stub(s => s.RemoveWithoutNotification(this.region))
                .WhenCalled(call => { throw new ImsException("Fake"); });
            this.ExpectStatusResponseToClientProxy(RequestStatus.Statuses.ExceptionThrownByOperation);
            this.server.RemoveWithoutNotificationRequest(this.sceneName, this.region);
            this.clientProxy.VerifyAllExpectations();
        }

        [Test]
        public void RemoveWithoutNotificationRepliesWithSuccessIfStoreDoesNotThrow()
        {
            this.objectStoreFactory.Stub(f => f.Invoke(this.eventQueue)).Return(this.store);
            this.ExpectStatusResponseToClientProxy(RequestStatus.Statuses.Success);
            this.server.RemoveWithoutNotificationRequest(this.sceneName, this.region);
            this.clientProxy.VerifyAllExpectations();
        }

        [Test]
        public void RemoveWithoutNotificationRequestsRepliedToUnreliably()
        {
            this.objectStoreFactory.Stub(f => f.Invoke(this.eventQueue)).Return(this.store);
            this.clientProxy.Expect(p => p.MessageQos = QualityOfService.Unreliable);
            this.server.RemoveWithoutNotificationRequest(this.sceneName, this.region);
            this.clientProxy.VerifyAllExpectations();
        }

        [Test]
        public void RemoveWithoutNotificationRequestRepliesSentToRegionAddress()
        {
            this.objectStoreFactory.Stub(f => f.Invoke(this.eventQueue)).Return(this.store);
            this.clientProxy.Expect(p => p.MessageDestination = this.region.Guid.Address);
            this.server.RemoveWithoutNotificationRequest(this.sceneName, this.region);
            this.clientProxy.VerifyAllExpectations();
        }

        [Test]
        public void ModifyRequestWithZeroRadiusRegionResultsInInvalidRegion()
        {
            this.ExpectStatusResponseToClientProxy(RequestStatus.Statuses.InvalidRegion);
            this.server.ModifyRequest(this.sceneName, this.zeroRadiusRegion, 7, 0);
            this.clientProxy.VerifyAllExpectations();
        }

        [Test]
        public void ModifyRequestForNewSceneNameCreatesStore()
        {
            this.objectStoreFactory.Expect(f => f.Invoke(this.eventQueue));
            this.server.ModifyRequest(this.sceneName, this.region, 7, 0);
            this.objectStoreFactory.VerifyAllExpectations();
        }

        [Test]
        public void ModifyRequestForKnownSceneNameDoesNotCreateStore()
        {
            this.objectStoreFactory.Stub(f => f.Invoke(this.eventQueue)).Return(this.store);
            this.server.ModifyRequest(this.sceneName, this.region, 7, 0);
            this.objectStoreFactory.Expect(f => f.Invoke(null)).IgnoreArguments().Repeat.Never();
            this.server.ModifyRequest(this.sceneName, this.secondRegion, 7, 0);
            this.objectStoreFactory.VerifyAllExpectations();
        }

        [Test]
        public void ModifyRequestModifiesRegionInStore()
        {
            uint ttl = 7;
            byte collisions = 8;
            this.objectStoreFactory.Stub(f => f.Invoke(this.eventQueue)).Return(this.store);
            this.store.Expect(s => s.AddOrModifyObject(this.region, ttl, collisions));
            this.server.ModifyRequest(this.sceneName, this.region, ttl, collisions);
            this.store.VerifyAllExpectations();
        }

        [Test]
        public void ModifyRequestRepliesWithExceptionStatusIfStoreThrows()
        {
            uint ttl = 7;
            byte collisions = 8;
            this.objectStoreFactory.Stub(f => f.Invoke(this.eventQueue)).Return(this.store);
            this.store.Stub(s => s.AddOrModifyObject(this.region, ttl, collisions))
                .WhenCalled(call => { throw new ImsException("Fake"); });
            this.ExpectStatusResponseToClientProxy(RequestStatus.Statuses.ExceptionThrownByOperation);
            this.server.ModifyRequest(this.sceneName, this.region, ttl, collisions);
            this.clientProxy.VerifyAllExpectations();
        }

        [Test]
        public void ModifyRequestRepliedToReliably()
        {
            this.objectStoreFactory.Stub(f => f.Invoke(this.eventQueue)).Return(this.store);
            this.clientProxy.Expect(p => p.MessageQos = QualityOfService.Reliable);
            this.server.ModifyRequest(this.sceneName, this.region, 7, 8);
            this.clientProxy.VerifyAllExpectations();
        }

        [Test]
        public void ModifyRequestRepliesSentToRegionAddress()
        {
            this.objectStoreFactory.Stub(f => f.Invoke(this.eventQueue)).Return(this.store);
            this.clientProxy.Expect(p => p.MessageDestination = this.region.Guid.Address);
            this.server.ModifyRequest(this.sceneName, this.region, 7, 8);
            this.clientProxy.VerifyAllExpectations();
        }

        [Test]
        public void IntersectionHandlerDoesNotSetQoSForUninterestedRegions()
        {
            EventHandler<IntersectionEventArgs> intersectionHandler = null;
            this.store.Expect(s => s.IntersectionNotification += null)
                .IgnoreArguments()
                .WhenCalled(call =>
                {
                    intersectionHandler = (EventHandler<IntersectionEventArgs>)call.Arguments[0];
                });
            this.objectStoreFactory.Stub(f => f.Invoke(this.eventQueue)).Return(this.store);
            this.server.InsertRequest(this.sceneName, this.region, 7);
            this.clientProxy.Expect(p => p.MessageQos = QualityOfService.Reliable)
                .IgnoreArguments()
                .Repeat.Never();
            intersectionHandler.Invoke(this, new IntersectionEventArgs(this.region, this.secondRegion));
            this.clientProxy.VerifyAllExpectations();
        }

        [Test]
        public void IntersectionHandlerSendsNotificationsReliably()
        {
            // TO DO: use fake regions so as not to rely on regions' internals.
            ImsRegion intersectingRegion = new ImsRegion(
                new Vector3(0f, 0f, 0f),
                1f,
                new BadumnaId(PeerAddress.GetLoopback(2), 3),
                RegionType.Sink,
                this.sceneName);

            EventHandler<IntersectionEventArgs> intersectionHandler = null;
            this.store.Expect(s => s.IntersectionNotification += null)
                .IgnoreArguments()
                .WhenCalled(call =>
                {
                    intersectionHandler = (EventHandler<IntersectionEventArgs>)call.Arguments[0];
                });
            this.objectStoreFactory.Stub(f => f.Invoke(this.eventQueue)).Return(this.store);
            this.server.InsertRequest(this.sceneName, this.region, 7);
            this.clientProxy.Expect(p => p.MessageQos = QualityOfService.Reliable);
            intersectionHandler.Invoke(this, new IntersectionEventArgs(this.region, intersectingRegion));
            this.clientProxy.VerifyAllExpectations();
        }

        [Test]
        public void IntersectionHandlerDoesNotSendNotificationsForUninterestedRegions()
        {
            EventHandler<IntersectionEventArgs> intersectionHandler = null;
            this.store.Expect(s => s.IntersectionNotification += null)
                .IgnoreArguments()
                .WhenCalled(call =>
                {
                    intersectionHandler = (EventHandler<IntersectionEventArgs>)call.Arguments[0];
                });
            this.objectStoreFactory.Stub(f => f.Invoke(this.eventQueue)).Return(this.store);
            this.server.InsertRequest(this.sceneName, this.region, 7);
            this.clientProxy.Expect(p => p.IntersectionNotification(null, null))
                .IgnoreArguments()
                .Repeat.Never();
            intersectionHandler.Invoke(this, new IntersectionEventArgs(this.region, this.secondRegion));
            this.clientProxy.VerifyAllExpectations();
        }

        [Test]
        public void IntersectionHandlerSendsNotificationsForInterestedRegions()
        {
            // TO DO: use fake regions so as not to rely on regions' internals.
            ImsRegion intersectingRegion = new ImsRegion(
                new Vector3(0f, 0f, 0f),
                1f,
                new BadumnaId(PeerAddress.GetLoopback(2), 3),
                RegionType.Sink,
                this.sceneName);

            EventHandler<IntersectionEventArgs> intersectionHandler = null;
            this.store.Expect(s => s.IntersectionNotification += null)
                .IgnoreArguments()
                .WhenCalled(call =>
                {
                    intersectionHandler = (EventHandler<IntersectionEventArgs>)call.Arguments[0];
                });
            this.objectStoreFactory.Stub(f => f.Invoke(this.eventQueue)).Return(this.store);
            this.server.InsertRequest(this.sceneName, this.region, 7);
            this.clientProxy.Expect(p => p.IntersectionNotification(null, null))
                .IgnoreArguments();
            intersectionHandler.Invoke(this, new IntersectionEventArgs(this.region, intersectingRegion));
            this.clientProxy.VerifyAllExpectations();
        }

        [Test]
        public void ExpirationHandlerSendsClientExpiredStatus()
        {
            ImsRegion region = new ImsRegion(
                new Vector3(0f, 0f, 0f),
                1f,
                new BadumnaId(new PeerAddress(HashKey.Random()), 0),
                RegionType.None,
                this.sceneName);
            Assert.IsTrue(region.Guid.Address.IsValid);
            FlatObjectStore store = new FlatObjectStore(this.eventQueue);
            this.objectStoreFactory.Stub(f => f.Invoke(this.eventQueue)).Return(store);
            GenericCallBack<ImsRegion> storeExpirationHandler = null;
            ImsRegion expiredRegion = null;
            this.eventQueue.Expect(q => q.Schedule(0, null, this.region))
                .IgnoreArguments()
                .WhenCalled(c =>
                {
                    storeExpirationHandler = (GenericCallBack<ImsRegion>)c.Arguments[1];
                    expiredRegion = (ImsRegion)c.Arguments[2];
                })
                .Return(null);
            this.server.InsertRequest(this.sceneName, region, 7);
            this.ExpectStatusResponseToClientProxy(RequestStatus.Statuses.Expired);
            storeExpirationHandler.Invoke(expiredRegion);
            this.eventQueue.VerifyAllExpectations();
            this.clientProxy.VerifyAllExpectations();
        }

        [Test]
        public void ExpirationHandlerIgnoresExpirationOfRegionsWithInvalidAddress()
        {
            this.region.Guid = new BadumnaId(PeerAddress.Nowhere, 0);
            Assert.IsFalse(this.region.Guid.Address.IsValid);
            FlatObjectStore store = new FlatObjectStore(this.eventQueue);
            this.objectStoreFactory.Stub(f => f.Invoke(this.eventQueue)).Return(store);
            GenericCallBack<ImsRegion> storeExpirationHandler = null;
            ImsRegion expiredRegion = null;
            this.eventQueue.Expect(q => q.Schedule(0, null, this.region))
                .IgnoreArguments()
                .WhenCalled(c =>
                {
                    storeExpirationHandler = (GenericCallBack<ImsRegion>)c.Arguments[1];
                    expiredRegion = (ImsRegion)c.Arguments[2];
                })
                .Return(null);
            this.server.InsertRequest(this.sceneName, this.region, 7);
            this.clientProxy.Expect(c => c.StatusResponse(null, null))
                .IgnoreArguments()
                .Repeat.Never();
            storeExpirationHandler.Invoke(expiredRegion);
            this.eventQueue.VerifyAllExpectations();
            this.clientProxy.VerifyAllExpectations();
        }

        #endregion Tests

        #region Helper methods

        private void ExpectStatusResponseToClientProxy(RequestStatus.Statuses status)
        {
            this.clientProxy.Expect(p => p.StatusResponse(null, null))
                .IgnoreArguments()
                .WhenCalled(call =>
                {
                    Assert.AreEqual(
                        status,
                        ((RequestStatus)call.Arguments[1]).Status);
                });
        }

        #endregion // Helper methods

        #region Fakes

        /// <summary>
        /// Fake to permit testing of abstract base class.
        /// </summary>
        internal class FakeInterestManagementServer : InterestManagementServer
        {
            private IInterestManagementClientProxy clientProxy;

            public FakeInterestManagementServer(
                MessageParser messageParser,
                ObjectStoreFactory objectStoreFactory,
                IInterestManagementClientProxy clientProxy,
                INetworkEventScheduler eventQueue,
                INetworkConnectivityReporter connectivityReporter)
                : base(messageParser, eventQueue, connectivityReporter, objectStoreFactory)
            {
                this.clientProxy = clientProxy;
            }

            protected override IInterestManagementClientProxy ClientProxy
            {
                get { return this.clientProxy; }
            }
        }

        #endregion // Fakes
    }
}