﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using Badumna.InterestManagement;
using Badumna.Core;
using Badumna.DataTypes;
using Badumna.Utilities;

namespace BadumnaTests.InterestManagement
{
    public abstract class SpatialObjectStoreInterfaceTester
    {
        private ISpatialObjectStore mObjectStore;

        private ImsRegion mIntersectingObject1;
        private ImsRegion mIntersectingObject2;
        private int mNumIntersections;

        private ImsRegion mSeperatingObject1;
        private ImsRegion mSeperatingObject2;
        private int mNumSeperations;

        private PeerAddress localAddress;

        [SetUp]
        public void Initialize()
        {
            this.localAddress = PeerAddress.GetLoopback(1);

            this.mObjectStore = CreateNewDerivedInstance();
            this.mObjectStore.IntersectionNotification += this.IntersectionHandler;
            this.mObjectStore.SeperationNotification += this.SeperationHandler;
            this.mNumIntersections = 0;
            this.mNumSeperations = 0;
        }

        internal abstract ISpatialObjectStore CreateNewDerivedInstance();

        private void IntersectionHandler(Object sender, IntersectionEventArgs e)
        {
            if (e != null)
            {
                this.mNumIntersections++;

                //Assert.AreEqual(this.mObjectStore.Name, e.SceneName);
                Assert.AreEqual(1, e.ChangedRegions.Count);   // Added this assert after changing IntersectionEventArgs to hold a list of intersecting regions
                Assert.AreNotEqual(this.mIntersectingObject1, e.Region);
                Assert.AreNotEqual(this.mIntersectingObject2, e.ChangedRegions[0]);
                this.mIntersectingObject1 = e.Region;
                this.mIntersectingObject2 = e.ChangedRegions[0];
            }
        }

        private void SeperationHandler(Object sender, IntersectionEventArgs e)
        {
            if (null != e)
            {
                this.mNumSeperations++;

                //Assert.AreEqual(this.mObjectStore.Name, e.SceneName);

                Assert.AreEqual(1, e.ChangedRegions.Count);   // Added this assert after changing IntersectionEventArgs to hold a list of intersecting regions
                Assert.AreNotEqual(null, e.Region);
                Assert.AreNotEqual(null, e.ChangedRegions[0]);
                Assert.AreNotEqual(this.mSeperatingObject1, e.Region);
                Assert.AreNotEqual(this.mSeperatingObject2, e.ChangedRegions[0]);

                this.mSeperatingObject1 = e.Region;
                this.mSeperatingObject2 = e.ChangedRegions[0];
            }
        }

        static readonly QualifiedName sceneName = new QualifiedName("", "Testing scene");

        [Test]
        public void AddRemoveTest()
        {
            ImsRegion mockObject = new ImsRegion(new Vector3(0, 0, 0), 10, new BadumnaId(this.localAddress, 1), RegionType.Sink, sceneName);

            // Test that we can add objects.
            Assert.IsTrue(this.mObjectStore.IsEmpty);
            this.mObjectStore.AddOrModifyObject(mockObject, 10, byte.MaxValue);
            Assert.IsFalse(this.mObjectStore.IsEmpty);

            // Test that we can remove objects.
            this.mObjectStore.RemoveObject(mockObject);
            Assert.IsTrue(this.mObjectStore.IsEmpty);
        }

        [Test, ExpectedException(typeof(ImsInvalidRegionException))]
        public void AddObjectFailTest()
        {
            ImsRegion mockObject = new ImsRegion(new Vector3(0, 0, 0), -1, new BadumnaId(this.localAddress, 1), RegionType.Sink, sceneName);

            Assert.IsTrue(this.mObjectStore.IsEmpty);
            this.mObjectStore.AddOrModifyObject(mockObject, 10, byte.MaxValue);
        }

        [Test]
        public void QueryTest()
        {
            EventRegion mockRegion = new EventRegion(new BadumnaId(this.localAddress, 1), new Vector3(0, 0, 0), 10, sceneName);
            EventRegion queryRegion = new EventRegion(new BadumnaId(this.localAddress, 2), new Vector3(0, 0, 0), 5, sceneName);

            Assert.IsTrue(this.mObjectStore.IsEmpty);
            this.mObjectStore.AddOrModifyObject(mockRegion, 10, byte.MaxValue);
            Assert.IsFalse(this.mObjectStore.IsEmpty);

            // Test that query return an intersecting object.
            IList<ImsRegion> queryList = this.mObjectStore.GetObjectsWithinRegion(queryRegion);
            Assert.AreEqual(1, queryList.Count, "Incorrect number of objects returned in query.");
            Assert.AreEqual(mockRegion, queryList[0], "Incorrect object returned in query.");

            // Test that query of empty region returns no objects.
            queryRegion.Centroid = new Vector3(100, 100, 100);
            queryList = this.mObjectStore.GetObjectsWithinRegion(queryRegion);
            Assert.AreEqual(0, queryList.Count, "Incorrect number of objects returned in query.");

            // Test that a query of a region outside object store range return 0 objects, without exception.
            queryRegion.Centroid = new Vector3(10000, 10000, 10000);
            queryList = this.mObjectStore.GetObjectsWithinRegion(queryRegion);
            Assert.AreEqual(0, queryList.Count, "Incorrect number of objects returned in query.");
        }

        [Test, ExpectedException(typeof(ImsInvalidRegionException))]
        public void QueryFailTest()
        {
            EventRegion mockRegion = new EventRegion(new BadumnaId(this.localAddress, 1), new Vector3(0, 0, 0), -1, sceneName);
            this.mObjectStore.GetObjectsWithinRegion(mockRegion);
        }

        [Test]
        public void ModifyTest()
        {
            EventRegion mockRegion = new EventRegion(new BadumnaId(this.localAddress, 1), new Vector3(0, 0, 0), 10, sceneName);
            EventRegion queryRegion = new EventRegion(new BadumnaId(this.localAddress, 2), new Vector3(0, 0, 0), 5, sceneName);

            Assert.IsTrue(this.mObjectStore.IsEmpty);
            this.mObjectStore.AddOrModifyObject(mockRegion, 10, byte.MaxValue);
            Assert.IsFalse(this.mObjectStore.IsEmpty);

            IList<ImsRegion> queryList = this.mObjectStore.GetObjectsWithinRegion(queryRegion);
            Assert.AreEqual(1, queryList.Count, "Incorrect number of objects returned in query.");
            Assert.AreEqual(mockRegion, queryList[0], "Incorrect object returned in query.");

            // Test that we can modify objects.
            mockRegion.Centroid = new Vector3(5, 0, 0);
            this.mObjectStore.AddOrModifyObject(mockRegion, 10, byte.MaxValue);

            queryList = this.mObjectStore.GetObjectsWithinRegion(queryRegion);
            Assert.AreEqual(1, queryList.Count, "Incorrect number of objects returned in query.");
            Assert.AreEqual(mockRegion, queryList[0], "Incorrect object returned in query.");
            Assert.AreEqual(new Vector3(5, 0, 0), queryList[0].Centroid, "Incorrect object returned in query.");
            Assert.AreEqual(10, queryList[0].Radius, "Incorrect object returned in query.");
        }

        [Test]
        public void IntersectionTest()
        {

            EventRegion region1 = new EventRegion(new BadumnaId(this.localAddress, 1), new Vector3(4, 0, 0), 2, sceneName);
            EventRegion region2 = new EventRegion(new BadumnaId(this.localAddress, 2), new Vector3(0, 0, 0), 5, sceneName);

            Assert.IsTrue(this.mObjectStore.IsEmpty);
            this.mObjectStore.AddOrModifyObject(region1, 10, byte.MaxValue);

            Assert.IsFalse(this.mObjectStore.IsEmpty);
            this.mObjectStore.AddOrModifyObject(region2, 10, byte.MaxValue);

            // Test that collision of objects activates the IntersectionNotification event.
            if (this.mIntersectingObject1 == region1)
                Assert.AreEqual(this.mIntersectingObject2, region2);
            else
            {
                Assert.AreEqual(region1, this.mIntersectingObject2);
                Assert.AreEqual(region2, this.mIntersectingObject1);
            }

            Assert.AreEqual(2, this.mNumIntersections);

            this.mIntersectingObject1 = null;
            this.mIntersectingObject2 = null;

            // Test that the intersection notification occurs only upon initial intersection.
            region2.Radius = 10;
            this.mObjectStore.AddOrModifyObject(region2, 10, byte.MaxValue);

            Assert.AreEqual(null, this.mIntersectingObject1);
            Assert.AreEqual(null, this.mIntersectingObject2);


            // Test that when intersecting objects seperate the Seperate Notification is called.
            region2.Radius = 1;
            region2.Centroid = new Vector3(9, 0, 0);
            this.mObjectStore.AddOrModifyObject(region2, 10, byte.MaxValue);

            if (this.mSeperatingObject1 == region1)
                Assert.AreEqual(this.mSeperatingObject2, region2);
            else
            {
                Assert.AreEqual(region1, this.mSeperatingObject2);
                Assert.AreEqual(region2, this.mSeperatingObject1);
            }

            Assert.AreEqual(2, this.mNumSeperations);

            this.mSeperatingObject1 = null;
            this.mSeperatingObject2 = null;

            // Test that the seperation notification occurs only upon initial seperation.
            region2.Radius = 0.2f;
            this.mObjectStore.AddOrModifyObject(region2, 10, byte.MaxValue);

            Assert.AreEqual(null, this.mIntersectingObject1);
            Assert.AreEqual(null, this.mIntersectingObject2);
        }

        [Test]
        public void IntersectionTest2()
        {
            EventRegion region1 = new EventRegion(new BadumnaId(this.localAddress, 1), new Vector3(8, 0, 0), 2, sceneName);
            EventRegion region2 = new EventRegion(new BadumnaId(this.localAddress, 2), new Vector3(0, 0, 0), 2, sceneName);

            Assert.IsTrue(this.mObjectStore.IsEmpty);
            this.mObjectStore.AddOrModifyObject(region1, 10, byte.MaxValue);
            Assert.IsFalse(this.mObjectStore.IsEmpty);
            this.mObjectStore.AddOrModifyObject(region2, 10, byte.MaxValue);

            // Test that insertion of non interesecting objects does not cause an IntersectionNotification event.
            Assert.AreEqual(null, this.mIntersectingObject2);
            Assert.AreEqual(null, this.mIntersectingObject1);

            Assert.AreEqual(0, this.mNumIntersections);

            // Test that the intersection notification occurs when two objects intersect.
            region1.Centroid = new Vector3(0, 0, 0);
            this.mObjectStore.AddOrModifyObject(region1, 10, byte.MaxValue);

            if (this.mIntersectingObject1 == region1)
                Assert.AreEqual(this.mIntersectingObject2, region2);
            else
            {
                Assert.AreEqual(region1, this.mIntersectingObject2);
                Assert.AreEqual(region2, this.mIntersectingObject1);
            }

            // Test that correct up-to-date result is returned.
            if (this.mIntersectingObject1 == region1)
            {
                Assert.AreEqual(new Vector3(0, 0, 0), this.mIntersectingObject1.Centroid);
            }
            else
            {
                Assert.AreEqual(new Vector3(0, 0, 0), this.mIntersectingObject2.Centroid);
            }

            Assert.AreEqual(2, this.mNumIntersections);

            this.mIntersectingObject1 = null;
            this.mIntersectingObject2 = null;

            // Test that when intersecting objects seperate the Seperate Notification is called.
            region1.Radius = 1;
            region1.Centroid = new Vector3(9, 0, 0);
            this.mObjectStore.AddOrModifyObject(region1, 10, byte.MaxValue);

            if (this.mSeperatingObject1 == region1)
                Assert.AreEqual(this.mSeperatingObject2, region2);
            else
            {
                Assert.AreEqual(region1, this.mSeperatingObject2);
                Assert.AreEqual(region2, this.mSeperatingObject1);
            }

            Assert.AreEqual(2, this.mNumSeperations);

            this.mSeperatingObject1 = null;
            this.mSeperatingObject2 = null;

            // Test that the seperation notification occurs only upon initial seperation.
            region2.Radius = 0.2f;
            this.mObjectStore.AddOrModifyObject(region2, 10, byte.MaxValue);

            Assert.AreEqual(null, this.mIntersectingObject1);
            Assert.AreEqual(null, this.mIntersectingObject2);
        }

        [Test]
        public void RemoveSeperationTest()
        {
            EventRegion region1 = new EventRegion(new BadumnaId(this.localAddress, 1), new Vector3(1, 0, 0), 2, sceneName);
            EventRegion region2 = new EventRegion(new BadumnaId(this.localAddress, 2), new Vector3(0, 0, 0), 2, sceneName);

            Assert.IsTrue(this.mObjectStore.IsEmpty);
            this.mObjectStore.AddOrModifyObject(region1, 10, byte.MaxValue);

            Assert.IsFalse(this.mObjectStore.IsEmpty);
            this.mObjectStore.AddOrModifyObject(region2, 10, byte.MaxValue);

            this.mIntersectingObject1 = null;
            this.mIntersectingObject2 = null;

            // Test that when we remove the object the appropriate seperation notification event is thrown.
            this.mObjectStore.RemoveObject(region1);

            if (this.mSeperatingObject1 == region1)
                Assert.AreEqual(this.mSeperatingObject2, region2);
            else
            {
                Assert.AreEqual(region1, this.mSeperatingObject2);
                Assert.AreEqual(region2, this.mSeperatingObject1);
            }

            Assert.AreEqual(2, this.mNumSeperations);
        }

        [Test]
        public void InconsistencyTest()
        {
            EventRegion region1 = new EventRegion(new BadumnaId(this.localAddress, 1), new Vector3(1, 0, 0), 2, sceneName);
            EventRegion region2 = new EventRegion(new BadumnaId(this.localAddress, 2), new Vector3(0, 0, 0), 2, sceneName);

            Assert.IsTrue(this.mObjectStore.IsEmpty);
            this.mObjectStore.AddOrModifyObject(region1, 10, byte.MaxValue);

            Assert.IsFalse(this.mObjectStore.IsEmpty);
            this.mObjectStore.AddOrModifyObject(region2, 10, byte.MaxValue);

            // Test that the intersection notification is not raised upon another modify with the correct client perspective
            this.mIntersectingObject1 = null;
            this.mIntersectingObject2 = null;

            this.mObjectStore.AddOrModifyObject(region2, 10, 1);

            Assert.AreEqual(null, this.mIntersectingObject1);
            Assert.AreEqual(null, this.mIntersectingObject2);

            // Test that the same modification with incorrect client perspective initiates notification of the missing intersection
            this.mObjectStore.AddOrModifyObject(region2, 10, 0);

            Assert.AreEqual(region2.Guid, this.mIntersectingObject1.Guid);
            Assert.AreEqual(region1.Guid, this.mIntersectingObject2.Guid);

            this.mIntersectingObject1 = null;
            this.mIntersectingObject2 = null;
        }
    }
}
