﻿//-----------------------------------------------------------------------
// <copyright file="ImsExceptionTests.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BadumnaTests.InterestManagement
{
    using System;
    using Badumna.Core;
    using Badumna.InterestManagement;
    using NUnit.Framework;

    /// <summary>
    /// ImsExceptionTests
    /// </summary>
    [TestFixture]
    internal class ImsExceptionTests
    {
        [Test]
        public void ImsExceptionStatusIsExceptionThrownByOperation()
        {
            Assert.AreEqual(
                new ImsException(string.Empty).Status,
                RequestStatus.Statuses.ExceptionThrownByOperation);
        }

        [Test]
        public void ImsInvalidArgumentExceptionMessageIsSetCorrectly()
        {
            Assert.AreEqual(
                new ImsInvalidArgumentException(string.Empty).Message,
                Resources.ImsInvalidArgumentException_Message);
        }

        [Test]
        public void ImsInvalidArgumentExceptionArgumentNameIsReportedCorrectly()
        {
            string argument = Guid.NewGuid().ToString();
            Assert.AreEqual(
                argument,
                new ImsInvalidArgumentException(argument).ArgumentName);
        }

        [Test]
        public void ImsNoSuchServiceExceptionStatusIsNoSuchService()
        {
            Assert.AreEqual(
                new ImsNoSuchServiceException(string.Empty).Status,
                RequestStatus.Statuses.NoSuchService);
        }
    }
}
