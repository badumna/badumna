﻿using System;
using System.Collections.Generic;
using System.Text;
using Badumna.InterestManagement;
using Badumna.DataTypes;
using Badumna.Core;
using BadumnaTests.Utilities;
using NUnit.Framework;
using Badumna.Utilities;
using Rhino.Mocks;

namespace BadumnaTests.InterestManagement
{
    public abstract class InterestManagementServiceInterfaceTester
    {
        private class StubEventListener
        {
            public void EnterHandler(Object sender, ImsEventArgs e)
            {
            }
        }

        internal IInterestManagementService mService;

        private BadumnaId mEnterObjectId;
        private int mNumEntering;
        internal EventRegion mEventRegion;
        private InterestRegion mInterestRegion;
        private EventHandler<ImsEventArgs> mFirstEnterHandler;

        private BadumnaId mEnterObjectId2;
        private int mNumEntering2;
        private EventHandler<ImsEventArgs> mSecondEnterHandler;

        internal QualifiedName mSceneName = new QualifiedName("", "Test scene");

        private uint SubscriptionTimeToLiveSeconds = 10;
        protected int SubscriptionTimeToLiveMilliseconds = 10000;

        internal INetworkConnectivityReporter connectivityReporter;

        internal Simulator eventQueue;
        internal ITime timeKeeper;

        private GenericCallBackReturn<BadumnaId> generateBadumnaId;

        [SetUp]
        public void Initialize()
        {
            this.eventQueue = new Simulator();
            this.timeKeeper = this.eventQueue;

            this.mSceneName = new QualifiedName(this.mSceneName.Qualifier, this.mSceneName.Name + "*"); // Change scene name so that the tests do not interfere.

            this.connectivityReporter = new NetworkConnectivityReporter(this.eventQueue);
            this.connectivityReporter.SetStatus(ConnectivityStatus.Online);

            this.generateBadumnaId = BadumnaIdGenerator.Local();

            this.mService = this.CreateNewDerivedInstance();

            this.mEnterObjectId = null;
            this.mNumEntering = 0;
            this.mFirstEnterHandler = this.FirstEnterHandler;

            this.mEnterObjectId2 = null;
            this.mNumEntering2 = 0;
            this.mSecondEnterHandler = this.SecondEnterHandler;

            this.InitializeSubscription();

            this.mInterestRegion = this.MakeInterestRegion(new Vector3(0, 0, 0), 1.0f);
        }

        private ushort mUniquifier;
        private InterestRegion MakeInterestRegion(Vector3 position, float radius)
        {
            // Ensure the interest region belongs to a different machine so that notifications aren't suppressed
            this.mUniquifier++;
            return new InterestRegion(new BadumnaId(PeerAddress.GetLoopback(2), this.mUniquifier), position, radius, this.mSceneName);
        }

        private void DoEvents(TimeSpan timeSpan)
        {
            this.eventQueue.RunFor(timeSpan);
        }

        abstract protected int MaxGarbageCollectionDelayMilliseconds();
        abstract internal IInterestManagementService CreateNewDerivedInstance();

        private void FirstEnterHandler(Object sender, ImsEventArgs e)
        {
            Assert.AreNotEqual(null, e.OtherRegionId);

            //System.Console.WriteLine("enter object = " + e.OtherRegionId);

            this.mEnterObjectId = e.OtherRegionId;
            this.mNumEntering++;
        }

        private void SecondEnterHandler(Object sender, ImsEventArgs e)
        {
            Assert.AreNotEqual(null, e.OtherRegionId);

            //System.Console.WriteLine("enter object 2 = " + e.OtherRegionId);

            this.mEnterObjectId2 = e.OtherRegionId;
            this.mNumEntering2++;
        }

        public void InitializeSubscription()
        {
            EventRegion region = new EventRegion(this.generateBadumnaId(), new Vector3(0, 0, 0), 10, this.mSceneName);

            // Test that we can subscribe to a region.
            this.mService.InsertRegion(this.mSceneName, region, this.SubscriptionTimeToLiveSeconds,
                this.mFirstEnterHandler, null);
            this.mEventRegion = region;
            this.DoEvents(TimeSpan.FromMilliseconds(250));
        }

        [Test, ExpectedException(typeof(ImsInvalidOperationException))]
        public void SubscribeFailTest()
        {
            // Test that attempting to subscribe the same object twice throws an 
            this.mService.InsertRegion(new QualifiedName("", "twice registered"), this.mEventRegion, 30,
                this.mFirstEnterHandler, null);
        }

        [Test]
        public void AutoSubscribeTest()
        {
            this.mEventRegion = new EventRegion(new ImsRegion(new Vector3(0, 0, 0), 1, this.generateBadumnaId(), RegionType.Source, this.mSceneName));

            // Test that subscribing to a scene before registered automatically registers the scene.
            this.mService.InsertRegion(new QualifiedName("", "unregistered"), this.mEventRegion, 30,
                this.mFirstEnterHandler, null);
            this.DoEvents(TimeSpan.FromSeconds(1));

        }


        [Test]
        public void RegisterSceneTest2()
        {
            this.mInterestRegion = this.MakeInterestRegion(new Vector3(0, this.mEventRegion.Centroid.Y - 5, 0), 1);

            this.mService.InsertRegion(this.mSceneName, this.mInterestRegion, 99, null, null);
            this.DoEvents(TimeSpan.FromSeconds(1));

            Assert.AreEqual(this.mInterestRegion.Guid, this.mEnterObjectId);
            Assert.AreEqual(1, this.mNumEntering);

            this.mEnterObjectId = null;
            this.mNumEntering = 0;

            /*
            // Test that we can unregister a scene.
            this.mService.UnregisterScene(this.mSceneName);
            this.DoEvents(TimeSpan.FromSeconds(1));
            // Test that we don't get any notification of objects from the unregistered scene.
            Assert.AreEqual(null, this.mEnterObjectId); ;
            Assert.AreEqual(0, this.mNumEntering);
            Assert.AreEqual(null, this.mExitObjectId);
            Assert.AreEqual(0, this.mNumExiting);

            this.mEventRegion = new EventRegion(new Vector3(0, 0, 0), 1);

            // Test that any objects added to the previous scene are not in the new scene.
            this.mService.InsertRegion(this.mSceneName, this.mEventRegion, 30,
                this.mFirstEnterHandler, this.mFirstExitHandler, null);
            this.DoEvents(TimeSpan.FromSeconds(1));

            Assert.AreEqual(null, this.mEnterObjectId);
            Assert.AreEqual(0, this.mNumEntering);
            Assert.AreEqual(null, this.mExitObjectId);
            Assert.AreEqual(0, this.mNumExiting);*/
        }

        [Test]
        [Category("MonoFail")]
        public void EventHandlersRemovedTest()
        {
            StubEventListener listener = new StubEventListener();
            QualifiedName sceneName = new QualifiedName("", "EventHandlersRemovedTest");
            ImsRegion testRegion = new ImsRegion(new Vector3(-500.0f, 0.0f, 0.0f), 10.0f, this.generateBadumnaId(), RegionType.Sink, sceneName);
            this.mService.InsertRegion(this.mSceneName, testRegion, 60, listener.EnterHandler, null);
            this.DoEvents(TimeSpan.FromSeconds(1));

            WeakReference listenerRef = new WeakReference(listener);
            listener = null;
            GC.Collect();
            Assert.IsTrue(listenerRef.IsAlive, "Listener got collected although it should've been attached to the IMS's events");

            this.mService.RemoveRegion(this.mSceneName, testRegion);
            this.DoEvents(TimeSpan.FromSeconds(1));
            // Should no longer be any strong references to the listener, it should be collectable
            GC.Collect();
            Assert.IsFalse(listenerRef.IsAlive, "Listener wasn't collected even though it's zeroRadiusRegion has been removed from the IMS");


            // Make sure other event listeners weren't affected by removing testRegion
            this.AddObjectTest();
        }

        [Test]
        public void AddObjectTest()
        {
            this.mNumEntering = 0;
            this.mInterestRegion.Centroid = new Vector3(0, this.mEventRegion.Centroid.Y - 5, 0);
            Assert.IsTrue(this.mInterestRegion.IsIntersecting(this.mEventRegion));
            Assert.IsTrue(this.mEventRegion.IsIntersecting(this.mInterestRegion));

            // Test that objects added to a subscribed region are passed to the enterHandler of the subscription (and not exitHandler).
            this.mService.InsertRegion(this.mSceneName, this.mInterestRegion, 99, null, null);
            this.DoEvents(TimeSpan.FromSeconds(1));

            Assert.AreEqual(this.mInterestRegion.Guid, this.mEnterObjectId);
            Assert.AreEqual(1, this.mNumEntering);

            this.mEnterObjectId = null;
            this.mNumEntering = 0;
        }

        [Test]
        public void AddObjectTest2()
        {
            // Test that objects added to an area not subscribed to does not cause any Handlers to be called.
            this.mInterestRegion = this.MakeInterestRegion(new Vector3(0, this.mEventRegion.Centroid.Y + 11, 0), 1);

            this.mService.InsertRegion(this.mSceneName, this.mInterestRegion, 99, null, null);
            this.DoEvents(TimeSpan.FromSeconds(1));

            Assert.AreEqual(null, this.mEnterObjectId);
            Assert.AreEqual(0, this.mNumEntering);
        }

        [Test, ExpectedException(typeof(ImsInvalidRegionException))]
        public void AddObjectFailTest()
        {
            this.mInterestRegion = this.MakeInterestRegion(new Vector3(0, 0, 0), -1);

            // Test that attempting to add an invalid object throws an exception.
            this.mService.InsertRegion(this.mSceneName, this.mInterestRegion, 99, null, null);
        }


        [Test]
        public void MoveObjectTest()
        {
            this.AddObjectTest();

            // Test that an object moved out of a subscibed region is passed to exitHandler (and not enterHandler).
            this.mInterestRegion.Centroid = new Vector3(0, this.mEventRegion.Centroid.Y + 12, 0);
            Assert.IsTrue(!this.mInterestRegion.IsIntersecting(this.mEventRegion));
            this.mService.ModifyRegion(this.mSceneName, this.mInterestRegion, 99, byte.MaxValue);
            this.DoEvents(TimeSpan.FromSeconds(1));

            Assert.AreEqual(null, this.mEnterObjectId);
            Assert.AreEqual(0, this.mNumEntering);

            // Test that an object moved outside of a subscribed region doesn't get passed to handler.
            this.mInterestRegion.Centroid = new Vector3(0, this.mEventRegion.Centroid.Y + 11, 0);
            Assert.IsTrue(!this.mInterestRegion.IsIntersecting(this.mEventRegion));
            this.mService.ModifyRegion(this.mSceneName, this.mInterestRegion, 99, byte.MaxValue);
            this.DoEvents(TimeSpan.FromSeconds(1));

            Assert.AreEqual(null, this.mEnterObjectId);
            Assert.AreEqual(0, this.mNumEntering);

            // Test that an object moved into a subscribed region is passed to enterHandler.
            this.mInterestRegion.Centroid = new Vector3(0, this.mEventRegion.Centroid.Y + 10, 0);
            Assert.IsTrue(this.mInterestRegion.IsIntersecting(this.mEventRegion));
            this.mService.ModifyRegion(this.mSceneName, this.mInterestRegion, 99, byte.MaxValue);
            this.DoEvents(TimeSpan.FromSeconds(1));

            Assert.AreEqual(this.mInterestRegion.Guid, this.mEnterObjectId);
            Assert.AreEqual(1, this.mNumEntering);

            this.mEnterObjectId = null;
            this.mNumEntering = 0;

            // Test that an object moved within a subscribed region doesn't get passed to handler.
            this.mInterestRegion.Centroid = new Vector3(0, this.mEventRegion.Centroid.Y + 9, 0);
            Assert.IsTrue(this.mInterestRegion.IsIntersecting(this.mEventRegion));
            this.mService.ModifyRegion(this.mSceneName, this.mInterestRegion, 99, byte.MaxValue);
            this.DoEvents(TimeSpan.FromSeconds(1));

            Assert.AreEqual(null, this.mEnterObjectId);
            Assert.AreEqual(0, this.mNumEntering);
        }

        [Test, ExpectedException(typeof(ImsInvalidRegionException))]
        public void ModifyFailTest()
        {
            this.mInterestRegion = this.MakeInterestRegion(new Vector3(0, this.mEventRegion.Centroid.Y - 5, 0), 1);

            this.mService.InsertRegion(this.mSceneName, this.mInterestRegion, 99, null, null);
            this.DoEvents(TimeSpan.FromSeconds(1));

            // Test that attempting to modify an invalid object throws an exception.
            this.mInterestRegion.Radius = -1;
            this.mService.ModifyRegion(this.mSceneName, this.mInterestRegion, 99, byte.MaxValue);
        }

        /* Commecnted because UnregisterScene is no longer avilable
        [Test]
        public void ModifyTest2()
        {
            this.mInterestRegion = this.MakeInterestRegion(new Vector3(0, 0, 0), 1);

            this.mService.InsertRegion(this.mSceneName, this.mInterestRegion, 99, null, null, null);
            DoEvents(TimeSpan.FromSeconds(1));

            this.mService.UnregisterScene(this.mSceneName);
            DoEvents(TimeSpan.FromSeconds(1));

            // Test that attempting to modify an invalid scene initializes the scene and adds the object.
            this.mInterestRegion.Radius = 10;
            this.mService.ModifyRegion(this.mSceneName, this.mInterestRegion, 99, byte.MaxValue);
            DoEvents(TimeSpan.FromSeconds(1));
        }
        */

        [Test]
        public void RemoveObjectTest()
        {
            // Test that an object removed from a subscribed region does not get passed to enterHandler.
            this.AddObjectTest();
            this.mService.RemoveRegion(this.mSceneName, this.mInterestRegion);
            this.DoEvents(TimeSpan.FromSeconds(1));

            Assert.AreEqual(null, this.mEnterObjectId);
            Assert.AreEqual(0, this.mNumEntering);

            // Test that an object removed from without a subscribed region doesn't get passed to handler.
            this.AddObjectTest2();
            this.mService.RemoveRegion(this.mSceneName, this.mInterestRegion);
            this.DoEvents(TimeSpan.FromSeconds(1));

            Assert.AreEqual(null, this.mEnterObjectId);
            Assert.AreEqual(0, this.mNumEntering);
        }

        /* Commecnted because UnregisterScene is no longer avilable
        [Test]
        public void RemoveTest2()
        {
            this.mInterestRegion = this.MakeInterestRegion(new Vector3(0, 0, 0), 1);

            this.mService.InsertRegion(this.mSceneName, this.mInterestRegion, 99, null, null, null);
            this.DoEvents(TimeSpan.FromSeconds(1));

            this.mService.UnregisterScene(this.mSceneName);
            this.DoEvents(TimeSpan.FromSeconds(1));

            // Test that attempting to remove an object completes, even for unregistered scene.           
            this.mService.RemoveRegion(this.mSceneName, this.mInterestRegion);
            this.DoEvents(TimeSpan.FromSeconds(1));
        }
        */

        [Test]
        public void RemoveSubscriptionTest()
        {
            EventRegion region = new EventRegion(this.generateBadumnaId(), new Vector3(0, 5, 0), 10, this.mSceneName);

            this.mService.InsertRegion(this.mSceneName, region, 30,
                this.mSecondEnterHandler, null);
            this.DoEvents(TimeSpan.FromSeconds(1));

            this.mEnterObjectId = null;
            this.mNumEntering = 0;

            // Test that a removed subscription causes a complete status notification for the original request.
            this.mService.RemoveRegion(this.mSceneName, region);
            this.DoEvents(TimeSpan.FromSeconds(1));

            Assert.AreEqual(null, this.mEnterObjectId);
            Assert.AreEqual(0, this.mNumEntering);
        }


        [Test]
        public void ModifySubscriptionTest()
        {
            // Test that the move object test works for a modified region.
            this.mEventRegion.Centroid = new Vector3(0, 17, 0);
            this.mService.ModifyRegion(this.mSceneName, this.mEventRegion, 30, byte.MaxValue);
            this.DoEvents(TimeSpan.FromSeconds(1));

            this.MoveObjectTest();
            this.mService.RemoveRegion(this.mSceneName, this.mInterestRegion);
            this.DoEvents(TimeSpan.FromSeconds(1));
        }

        [Test]
        public void ModifySubscriptionTest2()
        {
            // Test that the remove test works for a modified region.
            this.mEventRegion.Centroid = new Vector3(0, 17, 0);
            this.mService.ModifyRegion(this.mSceneName, this.mEventRegion, 30, byte.MaxValue);
            this.DoEvents(TimeSpan.FromSeconds(1));

            this.RemoveObjectTest();
        }

        [Test]
        public void ModifySubscriptionTest3()
        {
            this.mEventRegion.Centroid = new Vector3(0, 17, 0);
            this.mService.ModifyRegion(this.mSceneName, this.mEventRegion, 30, byte.MaxValue);

            this.DoEvents(TimeSpan.FromSeconds(1));

            this.AddObjectTest(); // places an object at 0,12, 0

            // Test that a region that moves beyond an object does not causes that object to get passed to enterHandler.
            this.mEventRegion.Centroid = new Vector3(0, 0, 0);
            this.mService.ModifyRegion(this.mSceneName, this.mEventRegion, 30, byte.MaxValue);
            Assert.AreEqual(this.mEventRegion.Guid, this.mEventRegion.Guid);
            this.DoEvents(TimeSpan.FromSeconds(1));

            Assert.AreEqual(null, this.mEnterObjectId);
            Assert.AreEqual(0, this.mNumEntering);

            // Test that an region moving away from any objects does not result in handler being called.
            this.mEventRegion.Centroid = new Vector3(0, 1, 0);
            this.mService.ModifyRegion(this.mSceneName, this.mEventRegion, 30, byte.MaxValue);
            Assert.AreEqual(this.mEventRegion.Guid, this.mEventRegion.Guid);
            this.DoEvents(TimeSpan.FromSeconds(1));

            Assert.AreEqual(null, this.mEnterObjectId);
            Assert.AreEqual(0, this.mNumEntering);

            // Test that a region that moves over an object cause that object to get passed to enterHandler.
            this.mEventRegion.Centroid = new Vector3(0, 10, 0);
            this.mService.ModifyRegion(this.mSceneName, this.mEventRegion, 30, byte.MaxValue);
            Assert.AreEqual(this.mEventRegion.Guid, this.mEventRegion.Guid);
            this.DoEvents(TimeSpan.FromSeconds(1));

            Assert.AreEqual(this.mInterestRegion.Guid, this.mEnterObjectId);
            Assert.AreEqual(1, this.mNumEntering);

            this.mEnterObjectId = null;
            this.mNumEntering = 0;

            // Test that an region moving over, but not crossing an object does not result in handler being called.
            this.mEventRegion.Centroid = new Vector3(0, 12, 0);
            this.mService.ModifyRegion(this.mSceneName, this.mEventRegion, 30, byte.MaxValue);
            Assert.AreEqual(this.mEventRegion.Guid, this.mEventRegion.Guid);
            this.DoEvents(TimeSpan.FromSeconds(1));

            Assert.AreEqual(null, this.mEnterObjectId);
            Assert.AreEqual(0, this.mNumEntering);
        }


        [Test, ExpectedException(typeof(ImsInvalidOperationException))]
        public void ModifySubscriptionFailTest()
        {
            this.mService.RemoveRegion(this.mSceneName, this.mEventRegion);
            this.DoEvents(TimeSpan.FromSeconds(1));

            // Test that attempting to modify a non existing subscription fails.
            this.mService.ModifyRegion(this.mSceneName, this.mEventRegion, 30, byte.MaxValue);
        }


        /* Commecnted because UnregisterScene is no longer avilable
        [Test]
        public void ModifySubscriptionTest4()
        {
            this.mService.UnregisterScene(this.mSceneName);
            DoEvents(TimeSpan.FromSeconds(1));

            // Test that attempting to modify a subscription in a non existant scene succeeds
            this.mService.ModifyRegion(this.mSceneName, this.mEventRegion, 30, byte.MaxValue);
            this.DoEvents(TimeSpan.FromSeconds(1));
        }
        */

        [Test]
        public void RemoveRegionTest()
        {
            this.RemoveObjectTest();
            this.mEnterObjectId = null;
            this.mNumEntering = 0;

            this.mService.RemoveRegion(this.mSceneName, this.mEventRegion);
            this.DoEvents(TimeSpan.FromSeconds(1));

            // Test that a removed region doesn't have its handler called for added objects to its old region.
            this.mInterestRegion = this.MakeInterestRegion(new Vector3(0, this.mEventRegion.Centroid.Y - 5, 0), 1);
            this.mService.InsertRegion(this.mSceneName, this.mInterestRegion, 99, null, null);
            this.DoEvents(TimeSpan.FromSeconds(1));

            Assert.AreEqual(null, this.mEnterObjectId);
            Assert.AreEqual(0, this.mNumEntering);

            // Test that a removed region doesn't have its handlers called for moved objects in its old region.
            this.mInterestRegion.Centroid = new Vector3(0, this.mEventRegion.Centroid.Y, 0);
            this.mService.ModifyRegion(this.mSceneName, this.mInterestRegion, 99, byte.MaxValue);
            this.DoEvents(TimeSpan.FromSeconds(1));

            Assert.AreEqual(null, this.mEnterObjectId);
            Assert.AreEqual(0, this.mNumEntering);

            // Test that a removed region doesn't have its handler called for removed objects from its old region.
            this.mService.RemoveRegion(this.mSceneName, this.mInterestRegion);
            this.DoEvents(TimeSpan.FromSeconds(1));

            Assert.AreEqual(null, this.mEnterObjectId);
            Assert.AreEqual(0, this.mNumEntering);
        }

        ////[Test, ExpectedException(typeof(ImsInvalidArgumentException))]
        ////public void RemoveRegionFailTest()
        ////{
        ////    this.mService.RemoveRegion(this.mSceneName, this.mEventRegion);
        ////    this.DoEvents(TimeSpan.FromSeconds(1));

        ////    // Test that attempting to remove an unknown region fails.            
        ////    try
        ////    {
        ////        this.mService.RemoveRegion(this.mSceneName, this.mEventRegion);
        ////    }
        ////    catch (ImsInvalidArgumentException e)
        ////    {
        ////        Assert.AreEqual("subscription", e.ArgumentName);
        ////        throw;
        ////    }
        ////}

        [Test]
        public void RemoveRegionIgnoreTest()
        {
            this.mService.RemoveRegion(this.mSceneName, this.mEventRegion);
            this.DoEvents(TimeSpan.FromSeconds(1));

            // Test that attempting to remove an unknown region is ignored without excpetion.            
            this.mService.RemoveRegion(this.mSceneName, this.mEventRegion);
        }

        /* Commecnted because UnregisterScene is no longer avilable
        [Test]
        public void RemoveRegionTest2()
        {
            this.mService.UnregisterScene(this.mSceneName);
            DoEvents(TimeSpan.FromSeconds(1));

            // Test that attempting to modify a subscription in a n on existant scene succeeds.
            this.mService.RemoveRegion(this.mSceneName, this.mEventRegion);
            this.DoEvents(TimeSpan.FromSeconds(1));
        }
         * */

        [Test]
        public void MultipleRegionsTest()
        {
            EventRegion region2 = new EventRegion(this.generateBadumnaId(), new Vector3(0, this.mEventRegion.Radius, 0), this.mEventRegion.Radius, this.mSceneName);
            this.mService.InsertRegion(this.mSceneName, region2, 30, this.mFirstEnterHandler, null);
            DoEvents(TimeSpan.FromSeconds(1));

            this.mEnterObjectId = null;
            this.mNumEntering = 0;

            // Test that multiple overlapping regions are notified correctly for an object moving through their regions. 
            this.mInterestRegion = this.MakeInterestRegion(new Vector3(0, -11, 0), 1);

            this.mService.InsertRegion(this.mSceneName, this.mInterestRegion, 99, null, null);
            DoEvents(TimeSpan.FromSeconds(1));

            Assert.AreEqual(null, this.mEnterObjectId);
            Assert.AreEqual(0, this.mNumEntering);

            this.mInterestRegion.Centroid = new Vector3(0, -1, 0);

            this.mService.ModifyRegion(this.mSceneName, this.mInterestRegion, 99, byte.MaxValue);
            DoEvents(TimeSpan.FromSeconds(1));

            Assert.AreEqual(this.mInterestRegion.Guid, this.mEnterObjectId);
            Assert.AreEqual(1, this.mNumEntering);

            this.mEnterObjectId = null;
            this.mNumEntering = 0;


            this.mInterestRegion.Centroid = new Vector3(0, 0, 0);

            this.mService.ModifyRegion(this.mSceneName, this.mInterestRegion, 99, byte.MaxValue);
            DoEvents(TimeSpan.FromSeconds(1));

            Assert.AreEqual(this.mInterestRegion.Guid, this.mEnterObjectId);
            Assert.AreEqual(1, this.mNumEntering);

            this.mEnterObjectId = null;
            this.mNumEntering = 0;

            this.mInterestRegion.Centroid = new Vector3(0, 11, 0);

            this.mService.ModifyRegion(this.mSceneName, this.mInterestRegion, 99, byte.MaxValue);
            DoEvents(TimeSpan.FromSeconds(1));

            Assert.AreEqual(null, this.mEnterObjectId);
            Assert.AreEqual(0, this.mNumEntering);

            this.mEnterObjectId = null;
            this.mNumEntering = 0;

            this.mInterestRegion.Centroid = new Vector3(0, 21, 0);

            this.mService.ModifyRegion(this.mSceneName, this.mInterestRegion, 99, byte.MaxValue);
            DoEvents(TimeSpan.FromSeconds(1));

            Assert.AreEqual(null, this.mEnterObjectId);
            Assert.AreEqual(0, this.mNumEntering);

            this.mEnterObjectId = null;
            this.mNumEntering = 0;
        }

        [Test]
        public void InterestRegionTest()
        {
            EventRegion region = new EventRegion(this.generateBadumnaId(), new Vector3(0, -1, 0), 10, this.mSceneName);

            // Test that inserted interest regions DO NOT receive notification about each other
            this.mService.InsertRegion(this.mSceneName, region, 30, this.mSecondEnterHandler, null);
            PeerAddress address = region.Guid.Address;
            this.DoEvents(TimeSpan.FromSeconds(1));

            Assert.AreEqual(0, this.mNumEntering2);
            Assert.AreEqual(null, this.mEnterObjectId2);

            Assert.AreEqual(0, this.mNumEntering);
            Assert.AreEqual(null, this.mEnterObjectId);

            this.mNumEntering = this.mNumEntering2 = 0;

            this.mEnterObjectId = this.mEnterObjectId2 = null;

            // Test that a removed interest region returns the correct data.
            this.mService.RemoveRegion(this.mSceneName, region);
            this.DoEvents(TimeSpan.FromSeconds(1));

            Assert.AreEqual(0, this.mNumEntering2);
            Assert.AreEqual(null, this.mEnterObjectId2);

            Assert.AreEqual(0, this.mNumEntering);
            Assert.AreEqual(null, this.mEnterObjectId);
        }

        public void AddObjectHandler()
        {
            this.mInterestRegion = this.MakeInterestRegion(new Vector3(0, this.mEventRegion.Centroid.Y - 5, 0), 1);
            Assert.IsTrue(this.mInterestRegion.IsIntersecting(this.mEventRegion));
            Assert.IsTrue(this.mEventRegion.IsIntersecting(this.mInterestRegion));

            // Test that objects added to a subscribed region are passed to the enterHandler of the subscription.
            this.mService.InsertRegion(this.mSceneName, this.mInterestRegion, 99, null, null);
        }

        [Test]
        public void TimeToLiveExpireTest()
        {
            int delay = this.SubscriptionTimeToLiveMilliseconds + this.MaxGarbageCollectionDelayMilliseconds() + 1000;

            // Set a call back to enter an object in the subscribed to region after its time to live has expired.
            this.eventQueue.Schedule(delay, this.AddObjectHandler);

            this.DoEvents(TimeSpan.FromMilliseconds(delay - 500));

            this.DoEvents(TimeSpan.FromSeconds(3));

            // Test that a subscription is removed at the end of time to live.
            Assert.AreEqual(null, this.mEnterObjectId);
            Assert.AreEqual(0, this.mNumEntering);
        }

        public void ModifySubscriptionHandler()
        {
            // Test that all the above tests work for a modified region.
            this.mEventRegion.Centroid = new Vector3(0, 17, 0);
            this.mService.ModifyRegion(this.mSceneName, this.mEventRegion, 900, byte.MaxValue);
        }

        [Test]
        public void TimeToLiveResetTest()
        {
            int delay = this.SubscriptionTimeToLiveMilliseconds + this.MaxGarbageCollectionDelayMilliseconds() + 1000;
            this.eventQueue.Schedule(this.SubscriptionTimeToLiveMilliseconds - 500, this.ModifySubscriptionHandler);
            this.eventQueue.Schedule(delay, this.AddObjectHandler);
            this.DoEvents(TimeSpan.FromMilliseconds(delay + 1000));

            // Test that a modify subscription updates the time to live time too.
            Assert.AreEqual(this.mInterestRegion.Guid, this.mEnterObjectId);
            Assert.AreEqual(1, this.mNumEntering);

            this.mEnterObjectId = null;
            this.mNumEntering = 0;
        }

        [Test]
        public void SubscriptionExpiresTest()
        {
            int delay = this.SubscriptionTimeToLiveMilliseconds + this.MaxGarbageCollectionDelayMilliseconds();

            InterestRegion region = this.MakeInterestRegion(new Vector3(0, 0, 0), 10);
            //System.Console.WriteLine(this.mEventRegion.Guid);

            this.mService.InsertRegion(this.mSceneName, region, 5, null, null);

            this.DoEvents(TimeSpan.FromMilliseconds(1000));

            // Check that the source and sink intersect.
            Assert.AreEqual(1, this.mNumEntering);
            Assert.AreEqual(region.Guid, this.mEnterObjectId);
            Assert.AreEqual(0, this.mNumEntering2);
            Assert.AreEqual(null, this.mEnterObjectId2);

            this.mEnterObjectId2 = null;
            this.mEnterObjectId = null;
            this.mNumEntering2 = 0;
            this.mNumEntering = 0;

            this.DoEvents(TimeSpan.FromMilliseconds(delay * 2));

            // Test that when a subscription expires the correct notification is sent.
            Assert.AreEqual(0, this.mNumEntering);
            // Instead, test that a new region doesn't get notification of the expired region

            BadumnaId intersectId = null;
            EventHandler<ImsEventArgs> enterHandler = delegate(object sender, ImsEventArgs args)
            {
                intersectId = args.OtherRegionId;
            };
            EventRegion newEventRegion = new EventRegion(this.generateBadumnaId(), new Vector3(0, 0, 0), 10, this.mSceneName);
            this.mService.InsertRegion(this.mSceneName, newEventRegion, this.SubscriptionTimeToLiveSeconds,
                enterHandler, null);
            this.DoEvents(TimeSpan.FromMilliseconds(250));
            Assert.IsNull(intersectId);

            this.mEnterObjectId2 = null;
            this.mEnterObjectId = null;
            this.mNumEntering2 = 0;
            this.mNumEntering = 0;
        }

        // TODO : Test that when the clients view of intersections differs from for a modify request 
        //        the server the correct notifications are sent
    }
}
