﻿//-----------------------------------------------------------------------
// <copyright file="RequestStatusTester.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2010 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BadumnaTests.InterestManagement
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Badumna.Core;
    using Badumna.InterestManagement;
    using BadumnaTests.Core;
    using NUnit.Framework;

    [TestFixture]
    public class RequestStatusTester
    {
        [SetUp]
        public void SetUp()
        {
        }

        [TearDown]
        public void TearDown()
        {
        }

        [TestCase(0)]
        [TestCase(0x80)]
        [TestCase(0x81)]
        public void ToStringWritesStatusCorrectly(int statusCode)
        {
            RequestStatus.Statuses status = (RequestStatus.Statuses)(byte)statusCode;
            RequestStatus requestStatus = new RequestStatus(status);
            string output = requestStatus.ToString();
            Assert.IsTrue(output.Contains(status.ToString()));
        }

        [TestCase(0)]
        [TestCase(0x80)]
        public void HasFailedReturnsFalseForSuccessOrCompletedStatuses(int statusCode)
        {
            RequestStatus.Statuses status = (RequestStatus.Statuses)(byte)statusCode;
            RequestStatus requestStatus = new RequestStatus(status);
            Assert.IsFalse(requestStatus.HasFailed);
        }

        [TestCase(0x81)]
        [TestCase(0x82)]
        [TestCase(0x83)]
        [TestCase(0x84)]
        [TestCase(0x85)]
        [TestCase(0x86)]
        [TestCase(0x87)]
        public void HasFailedReturnsTrueForErrorStatuses(int statusCode)
        {
            RequestStatus.Statuses status = (RequestStatus.Statuses)(byte)statusCode;
            RequestStatus requestStatus = new RequestStatus(status);
            Assert.IsTrue(requestStatus.HasFailed);
        }

        [TestCase(0x80)]
        [TestCase(0x81)]
        [TestCase(0x82)]
        [TestCase(0x83)]
        [TestCase(0x84)]
        [TestCase(0x85)]
        [TestCase(0x86)]
        [TestCase(0x87)]
        public void IsCompleteReturnsTrueForAnyStatusExceptSuccess(int statusCode)
        {
            RequestStatus.Statuses status = (RequestStatus.Statuses)(byte)statusCode;
            RequestStatus requestStatus = new RequestStatus(status);
            Assert.IsTrue(requestStatus.IsComplete);
        }

        [Test]
        public void IsCompleteReturnsFalseForSuccess()
        {
            RequestStatus.Statuses status = RequestStatus.Statuses.Success;
            RequestStatus requestStatus = new RequestStatus(status);
            Assert.IsFalse(requestStatus.IsComplete);
        }

        [TestCase(0)]
        [TestCase(0x80)]
        [TestCase(0x81)]
        [TestCase(0x82)]
        [TestCase(0x83)]
        [TestCase(0x84)]
        [TestCase(0x85)]
        [TestCase(0x86)]
        [TestCase(0x87)]
        public void StatusReturnsCorrectStatus(int statusCode)
        {
            RequestStatus.Statuses status = (RequestStatus.Statuses)(byte)statusCode;
            RequestStatus requestStatus = new RequestStatus(status);
            Assert.AreEqual(status, requestStatus.Status);
        }

        [TestCase(0)]
        [TestCase(0x80)]
        [TestCase(0x81)]
        [TestCase(0x82)]
        [TestCase(0x83)]
        [TestCase(0x84)]
        [TestCase(0x85)]
        [TestCase(0x86)]
        [TestCase(0x87)]
        public void CopyConstructroCopiesStatus(int statusCode)
        {
            RequestStatus.Statuses status = (RequestStatus.Statuses)(byte)statusCode;
            RequestStatus requestStatus = new RequestStatus(status);
            RequestStatus copy = new RequestStatus(requestStatus);
            Assert.AreEqual(requestStatus.Status, copy.Status);
        }

        [TestCase(0)]
        [TestCase(0x80)]
        [TestCase(0x81)]
        [TestCase(0x82)]
        [TestCase(0x83)]
        [TestCase(0x84)]
        [TestCase(0x85)]
        [TestCase(0x86)]
        [TestCase(0x87)]
        public void ToMessageAndFromMessagePreserveStatus(int statusCode)
        {
            RequestStatus requestStatus = new RequestStatus((RequestStatus.Statuses)statusCode);
            MessageBuffer buffer = new MessageBuffer();
            requestStatus.ToMessage(buffer, null);
            RequestStatus parsedRequestStatus = new RequestStatus();
            parsedRequestStatus.FromMessage(buffer);
            Assert.AreEqual(requestStatus.Status, parsedRequestStatus.Status);
        }
    }
}
