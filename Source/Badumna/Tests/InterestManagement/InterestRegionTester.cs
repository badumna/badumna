﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using BadumnaTests.Core;
using Badumna.InterestManagement;
using Badumna.Core;
using Badumna.DataTypes;
using Badumna.Utilities;

namespace BadumnaTests.InterestManagement
{
    [TestFixture]
    [DontPerformCoverage]
    public class InterestRegionTester : BadumnaTests.Core.ParseableTestHelper
    {
        static readonly QualifiedName sceneName = new QualifiedName("", "InterestRegionTester");
        class ParseableTester : BadumnaTests.Core.ParseableTester<InterestRegion>
        {
            public override void AssertAreEqual(InterestRegion expected, InterestRegion actual)
            {
                Assert.AreEqual(expected.Centroid, actual.Centroid);
                Assert.AreEqual(expected.Guid, actual.Guid);
                Assert.AreEqual(expected.Radius, actual.Radius);
            }

            public override ICollection<InterestRegion> CreateExpectedValues()
            {
                ICollection<InterestRegion> expectedValues = new List<InterestRegion>();

                expectedValues.Add(new InterestRegion(new BadumnaId(PeerAddress.GetLoopback(99), 1),
                    new Vector3(0, 2, 6), 78.54f, sceneName));

                return expectedValues;
            }
        }

        public InterestRegionTester()
            : base(new ParseableTester())
        { }

        [Test]
        public void InheritanceTest()
        {
            InterestRegion region = new InterestRegion(new BadumnaId(PeerAddress.GetLoopback(99), 1),
                new Vector3(99, 99, 99), 99, sceneName);
            ImsRegion result = new ImsRegion();
            MessageBuffer message = new MessageBuffer();

            region.ToMessage(message, typeof(ImsRegion));
            result.FromMessage(message);

            this.AssertAreEqual(region, new InterestRegion(result));
        }
    }
}
