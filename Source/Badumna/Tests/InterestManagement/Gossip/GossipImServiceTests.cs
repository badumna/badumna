﻿//-----------------------------------------------------------------------
// <copyright file="GossipImServiceTests.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BadumnaTests.InterestManagement
{
    using System;
    using System.Collections.Generic;
    using Badumna.Core;
    using Badumna.DataTypes;
    using Badumna.DistributedHashTable;
    using Badumna.InterestManagement;
    using Badumna.Transport;
    using Badumna.Utilities;
    using BadumnaTests.Core;
    using NUnit.Framework;
    using Rhino.Mocks;
    using Constraints = Rhino.Mocks.Constraints;

    [TestFixture]
    internal class GossipImServiceTests
    {
        private TransportProtocol transportProtocol;
        private IInterestManagementClient client;
        private INetworkEventScheduler eventScheduler;
        private ITime timeKeeper;
        private INetworkConnectivityReporter connectivityReporter;
        private GossipRegionFactory gossipRegionFactory;
        private RegularTaskFactory regularTaskFactory;
        private SubscriptionDataFactory subscriptionDataFactory;
        private SmootherFactory smootherFactory;
        private IRegularTask regularTask;
        private ISubscriptionData subscriptionData;
        private GossipImService.IGossipRegion gossipRegion;
        private GossipImService service;
        private QualifiedName sceneName;
        private ImsRegion region;
        private ImsRegion intersectingRegion;
        private ImsRegion nonIntersectingRegion;
        private uint ttl;

        [SetUp]
        public void SetUp()
        {
            this.transportProtocol = new TransportProtocol("foo", typeof(ConnectionfulProtocolMethodAttribute), MessageParserTester.DefaultFactory);
            this.client = MockRepository.GenerateMock<IInterestManagementClient>();
            this.eventScheduler = MockRepository.GenerateMock<INetworkEventScheduler>();
            this.timeKeeper = MockRepository.GenerateMock<ITime>();
            this.connectivityReporter = MockRepository.GenerateMock<INetworkConnectivityReporter>();
            this.gossipRegionFactory = MockRepository.GenerateMock<GossipRegionFactory>();
            this.regularTaskFactory = MockRepository.GenerateMock<RegularTaskFactory>();
            this.subscriptionDataFactory = MockRepository.GenerateMock<SubscriptionDataFactory>();
            this.smootherFactory = MockRepository.GenerateMock<SmootherFactory>();
            this.gossipRegion = MockRepository.GenerateMock<GossipImService.IGossipRegion>();
            this.regularTask = MockRepository.GenerateMock<IRegularTask>();
            this.subscriptionData = MockRepository.GenerateMock<ISubscriptionData>();
            this.sceneName = new QualifiedName("", Guid.NewGuid().ToString());
            this.region = new ImsRegion(
                new Vector3(0f, 0f, 0f),
                1f,
                new BadumnaId(new PeerAddress(HashKey.Random()), 0),
                RegionType.Sink,
                default(QualifiedName));
            this.intersectingRegion = new ImsRegion(
                new Vector3(0f, 0f, 0f),
                1f,
                new BadumnaId(new PeerAddress(HashKey.Random()), 0),
                RegionType.Source,
                default(QualifiedName));
            this.nonIntersectingRegion = new ImsRegion(
                new Vector3(9f, 9f, 9f),
                1f,
                new BadumnaId(new PeerAddress(HashKey.Random()), 0),
                RegionType.Source,
                default(QualifiedName));
            Assert.IsTrue(this.region.IsIntersecting(this.intersectingRegion));
            Assert.IsFalse(this.region.IsIntersecting(this.nonIntersectingRegion));
            this.ttl = 99;
        }

        #region Tests

        [TearDown]
        public void TearDown()
        {
            this.service = null;
        }

        [Test, Ignore]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ConstructorThrowsForNullTransportProtocol()
        {
            // TO DO: Set up debug abort handling?
            GossipImService service = new GossipImService(
                null,
                this.client,
                this.eventScheduler,
                this.timeKeeper,
                this.connectivityReporter,
                this.gossipRegionFactory,
                this.regularTaskFactory,
                this.subscriptionDataFactory,
                this.smootherFactory);
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ConstructorThrowsForNullClient()
        {
            GossipImService service = new GossipImService(
                this.transportProtocol,
                null,
                this.eventScheduler,
                this.timeKeeper,
                this.connectivityReporter,
                this.gossipRegionFactory,
                this.regularTaskFactory,
                this.subscriptionDataFactory,
                this.smootherFactory);
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ConstructorThrowsForNullEventScheduler()
        {
            GossipImService service = new GossipImService(
                this.transportProtocol,
                this.client,
                null,
                this.timeKeeper,
                this.connectivityReporter,
                this.gossipRegionFactory,
                this.regularTaskFactory,
                this.subscriptionDataFactory,
                this.smootherFactory);
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ConstructorThrowsForNullTimeKeeper()
        {
            GossipImService service = new GossipImService(
                this.transportProtocol,
                this.client,
                this.eventScheduler,
                null,
                this.connectivityReporter,
                this.gossipRegionFactory,
                this.regularTaskFactory,
                this.subscriptionDataFactory,
                this.smootherFactory);
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ConstructorThrowsForNullNetworkConnectivityReporter()
        {
            GossipImService service = new GossipImService(
                this.transportProtocol,
                this.client,
                this.eventScheduler,
                this.timeKeeper,
                null,
                this.gossipRegionFactory,
                this.regularTaskFactory,
                this.subscriptionDataFactory,
                this.smootherFactory);
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ConstructorThrowsForNullGossipRegionFactory()
        {
            GossipImService service = new GossipImService(
                this.transportProtocol,
                this.client,
                this.eventScheduler,
                this.timeKeeper,
                this.connectivityReporter,
                null,
                this.regularTaskFactory,
                this.subscriptionDataFactory,
                this.smootherFactory);
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ConstructorThrowsForNullRegularTaskFactory()
        {
            GossipImService service = new GossipImService(
                this.transportProtocol,
                this.client,
                this.eventScheduler,
                this.timeKeeper,
                this.connectivityReporter,
                this.gossipRegionFactory,
                null,
                this.subscriptionDataFactory,
                this.smootherFactory);
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ConstructorThrowsForNullSubscriptionDataFactory()
        {
            GossipImService service = new GossipImService(
                this.transportProtocol,
                this.client,
                this.eventScheduler,
                this.timeKeeper,
                this.connectivityReporter,
                this.gossipRegionFactory,
                this.regularTaskFactory,
                null,
                this.smootherFactory);
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ConstructorThrowsForNullSmootherFactory()
        {
            GossipImService service = new GossipImService(
                this.transportProtocol,
                this.client,
                this.eventScheduler,
                this.timeKeeper,
                this.connectivityReporter,
                this.gossipRegionFactory,
                this.regularTaskFactory,
                this.subscriptionDataFactory,
                null);
        }

        [Test]
        public void ConstructorSubscribesToClientResultNotification()
        {
            this.client.Expect(c => c.ResultNotification += null).IgnoreArguments();
            GossipImService service = new GossipImService(
                this.transportProtocol,
                this.client,
                this.eventScheduler,
                this.timeKeeper,
                this.connectivityReporter);
            this.client.VerifyAllExpectations();
        }

        [Test]
        public void ConstructorSubscribesToNetworkOnlineEvent()
        {
            this.connectivityReporter.Expect(r => r.NetworkOnlineEvent += null).IgnoreArguments();
            this.SetupService();
            this.connectivityReporter.VerifyAllExpectations();
        }

        [Test]
        public void ConstructorCreatesRegionCountEstimationRegularTask()
        {
            this.regularTaskFactory.Expect(f => f.Invoke(null, TimeSpan.MaxValue, null, null, null))
                .IgnoreArguments()
                .Return(this.regularTask);
            GossipImService service = new GossipImService(
                this.transportProtocol,
                this.client,
                this.eventScheduler,
                this.timeKeeper,
                this.connectivityReporter,
                this.gossipRegionFactory,
                this.regularTaskFactory,
                this.subscriptionDataFactory,
                this.smootherFactory);
            this.regularTaskFactory.VerifyAllExpectations();
        }

        [Test]
        public void ConstructorStartsRegionCountEstimationRegularTask()
        {
            this.regularTask.Expect(t => t.Start());
            this.SetupService();
            this.regularTask.VerifyAllExpectations();
        }

        [Test]
        public void SimpleConstructorCreatesRequiredDependencies()
        {
            // Succeeds if no exception thrown.
            GossipImService service = new GossipImService(
                this.transportProtocol,
                this.client,
                this.eventScheduler,
                this.timeKeeper,
                this.connectivityReporter);
        }

        [Test]
        public void SimpleConstructorCreatesRequiredGossipRegionFactory()
        {
            // Succeeds if no exception thrown.
            GossipImService service = new GossipImService(
                this.transportProtocol,
                this.client,
                this.eventScheduler,
                this.timeKeeper,
                this.connectivityReporter);
            service.GossipAboutRegion(this.region, this.ttl);
        }

        [Test]
        public void SimpleConstructorCreatesRequiredSubscriptionDataFactory()
        {
            // Succeeds if no exception thrown.
            GossipImService service = new GossipImService(
                this.transportProtocol,
                this.client,
                this.eventScheduler,
                this.timeKeeper,
                this.connectivityReporter);
            service.InsertRegion(this.sceneName, this.region, this.ttl, null, null);
        }

        [Test]
        public void InitializeDoesNothing()
        {
            // TO DO: Add initialize checks or remove them from other IM services.
            this.SetupService();
        }

        [Test]
        public void InsertRegionCreatesNewGossipRegion()
        {
            this.SetupService();
            this.gossipRegionFactory.Expect(f => f(this.region, this.ttl, this.timeKeeper));
            this.service.InsertRegion(
                this.sceneName,
                this.region,
                this.ttl,
                null,
                null);
            this.gossipRegionFactory.VerifyAllExpectations();
        }

        [Test]
        public void InsertRegionCreatesNewSubscriptionData()
        {
            this.SetupService();
            this.subscriptionDataFactory.Expect(f => f(
                this.sceneName,
                this.region,
                this.ttl,
                null,
                null,
                this.timeKeeper));
            this.service.InsertRegion(this.sceneName, this.region, this.ttl, null, null);
            this.subscriptionDataFactory.VerifyAllExpectations();
        }

        [Test]
        public void ModifyRegionUpdatesSubscriptionDataForInsertedRegions()
        {
            // Arrange
            this.SetupService();
            this.SetupMocksForInsertRegion();
            this.service.InsertRegion(this.sceneName, this.region, this.ttl, null, null);

            // Expectations
            this.subscriptionData.Expect(d => d.Region).Return(this.region).Repeat.Twice();
            this.subscriptionData.Expect(d => d.ResetExpirationTime(this.ttl));
            
            // Act
            this.service.ModifyRegion(this.sceneName, this.region, this.ttl, 0);
            
            // Verify
            this.subscriptionData.VerifyAllExpectations();
        }

        [Test]
        public void ModifyRegionUpdatesGossipRegionForInsertedRegions()
        {
            // Arrange
            this.SetupService();
            this.SetupMocksForInsertRegion();
            this.service.InsertRegion(this.sceneName, this.region, this.ttl, null, null);
            this.subscriptionData.Stub(d => d.Region).Return(this.region).Repeat.Twice();
            
            // Expectations
            this.gossipRegion.Expect(r => r.Update(this.region, this.ttl));
            
            // Act
            this.service.ModifyRegion(this.sceneName, this.region, this.ttl, 0);
            
            // Verify
            this.gossipRegion.VerifyAllExpectations();
        }

        [Test]
        public void ModifyRegionUpdatesSubscriptionData()
        {
            // Arrange
            this.SetupService();
            this.SetupMocksForInsertRegion();
            this.service.InsertRegion(this.sceneName, this.region, this.ttl, null, null);
            this.subscriptionData.Stub(d => d.Region).Return(this.region);

            ImsRegion newRegion = new ImsRegion(this.region);
            newRegion.Centroid = new Vector3(1f, 1f, 1f);
            newRegion.Radius = 3f;
            uint newTtl = 9;
            
            // Preconditions
            Assert.AreNotEqual(newRegion.Centroid, this.subscriptionData.Region.Centroid);
            Assert.AreNotEqual(newRegion.Radius, this.subscriptionData.Region.Radius);
            Assert.AreNotEqual(this.ttl, newTtl);
            
            // Expectations
            this.subscriptionData.Expect(d => d.ResetExpirationTime(newTtl));
            
            // Act
            this.service.ModifyRegion(this.sceneName, newRegion, newTtl, 0);
            
            // Postconditions
            Assert.AreEqual(newRegion.Centroid, this.subscriptionData.Region.Centroid);
            Assert.AreEqual(newRegion.Radius, this.subscriptionData.Region.Radius);
            
            // Verifications
            this.subscriptionData.VerifyAllExpectations();
        }

        [Test]
        public void ModifyRegionUpdatesKnownGossipRegions()
        {
            // Arrange
            this.SetupService();
            this.SetupMocksForInsertRegion();
            this.service.InsertRegion(this.sceneName, this.region, this.ttl, null, null);
            this.subscriptionData.Stub(d => d.Region).Return(this.region);

            ImsRegion newRegion = new ImsRegion(this.region);
            newRegion.Centroid = new Vector3(1f, 1f, 1f);
            newRegion.Radius = 3f;
            uint newTtl = 9;
            
            // Expect
            this.gossipRegion.Expect(r => r.Update(newRegion, (ushort)newTtl));
            
            // Act
            this.service.ModifyRegion(this.sceneName, newRegion, newTtl, 0);
            
            // Verify
            this.gossipRegion.VerifyAllExpectations();
        }

        [Test]
        public void ModifyRegionDoesNotCreateGossipRegionsWhenNoSubscriptionFound()
        {
            // Arrange
            this.SetupService();

            // Expect
            this.gossipRegionFactory.Expect(f => f(this.region, this.ttl, this.timeKeeper))
                .IgnoreArguments()
                .Repeat.Never();
            
            // Act
            this.service.ModifyRegion(this.sceneName, this.region, this.ttl, 0);
            
            // Verify
            this.gossipRegionFactory.VerifyAllExpectations();
        }

        [Test]
        public void ModifyRegionRecreatesMissingSubscribedGossipRegions()
        {
            // Arrange
            // - Capturing network online event handler (DoGossip).
            GenericCallBack networkHandler = null;
            this.connectivityReporter.Stub(r => r.NetworkOnlineEvent += null)
                .IgnoreArguments()
                .WhenCalled(c =>
                    {
                        networkHandler = (GenericCallBack)c.Arguments[0];
                    });
            this.SetupService();

            // - Insert region
            this.SetupMocksForInsertRegion();
            this.service.InsertRegion(this.sceneName, this.region, this.ttl, null, null);
            
            // - Expire region
            this.gossipRegion.Stub(r => r.TimeToLiveSeconds).Return(0);
            this.gossipRegion.Stub(r => r.Region).Return(this.region);
            networkHandler.Invoke();

            ImsRegion newRegion = new ImsRegion(this.region);
            newRegion.Centroid = new Vector3(1f, 1f, 1f);
            newRegion.Radius = 3f;
            uint newTtl = 9;
            this.subscriptionData.Stub(d => d.Region).Return(this.region);
            
            // Expect
            this.gossipRegionFactory.Expect(f => f(newRegion, newTtl, this.timeKeeper))
                .Return(this.gossipRegion);
            
            // Act
            this.service.ModifyRegion(this.sceneName, newRegion, newTtl, 0);
            
            // Verify
            this.gossipRegionFactory.VerifyAllExpectations();
        }

        [Test]
        public void ModifyRegionDoesNotRecreateRemovedRegions()
        {
            // Arrange
            this.SetupService();
            this.SetupMocksForInsertRegion();
            this.service.InsertRegion(this.sceneName, this.region, this.ttl, null, null);
            this.service.RemoveRegion(this.sceneName, this.region);

            ImsRegion newRegion = new ImsRegion(this.region);
            newRegion.Centroid = new Vector3(1f, 1f, 1f);
            newRegion.Radius = 3f;
            uint newTtl = 9;
            this.subscriptionData.Stub(d => d.Region).Return(this.region);

            // Expect
            this.gossipRegionFactory.Expect(f => f(newRegion, newTtl, this.timeKeeper))
                .IgnoreArguments()
                .Repeat.Never();

            // Act
            this.service.ModifyRegion(this.sceneName, newRegion, newTtl, 0);

            // Verify
            this.gossipRegionFactory.VerifyAllExpectations();
        }

        [Test]
        public void RemoveRegionRemovesRegion()
        {
            // Arrange
            ISmoother smoother = MockRepository.GenerateMock<ISmoother>();
            this.smootherFactory.Stub(f => f.Invoke(5.0, 0.0)).Return(smoother);
            this.SetupService();
            this.SetupMocksForInsertRegion();
            this.service.InsertRegion(this.sceneName, this.region, this.ttl, null, null);
            smoother.Stub(s => s.EstimatedValue).Return(0.0);

            // Preconditions
            Assert.Greater(this.service.EstimatedNumberOfActualRegions, 0.0);

            // Act
            this.service.RemoveRegion(this.sceneName, this.region);

            // Postconditions
            Assert.AreEqual(0.0, this.service.EstimatedNumberOfActualRegions);
        }

        [Test]
        public void RemoveRegionWithoutNotificationRemovesRegion()
        {
            // Arrange
            ISmoother smoother = MockRepository.GenerateMock<ISmoother>();
            this.smootherFactory.Stub(f => f.Invoke(5.0, 0.0)).Return(smoother);
            this.SetupService();
            this.SetupMocksForInsertRegion();
            this.service.InsertRegion(this.sceneName, this.region, this.ttl, null, null);
            smoother.Stub(s => s.EstimatedValue).Return(0.0);

            // Preconditions
            Assert.Greater(this.service.EstimatedNumberOfActualRegions, 0.0);

            // Act
            this.service.RemoveRegionWithoutNotification(this.sceneName, this.region);

            // Postconditions
            Assert.AreEqual(0.0, this.service.EstimatedNumberOfActualRegions);
        }

        [Test]
        public void DetachSubscriptionRemovesRegion()
        {
            // Arrange
            ISmoother smoother = MockRepository.GenerateMock<ISmoother>();
            this.smootherFactory.Stub(f => f.Invoke(5.0, 0.0)).Return(smoother);
            this.SetupService();
            this.SetupMocksForInsertRegion();
            this.service.InsertRegion(this.sceneName, this.region, this.ttl, null, null);
            smoother.Stub(s => s.EstimatedValue).Return(0.0);

            // Preconditions
            Assert.Greater(this.service.EstimatedNumberOfActualRegions, 0.0);

            // Act
            this.service.DetachSubscription(this.sceneName, this.region);

            // Postconditions
            Assert.AreEqual(0.0, this.service.EstimatedNumberOfActualRegions);
        }

        [Test]
        public void GossipAboutRegionCreatesGossipRegionForUnknownRegion()
        {
            // Arrange
            this.SetupService();

            // Expect
            this.gossipRegionFactory.Expect(f => f.Invoke(this.region, this.ttl, this.timeKeeper));

            // Act
            this.service.GossipAboutRegion(this.region, this.ttl);

            // Verify
            this.gossipRegionFactory.VerifyAllExpectations();
        }

        [Test]
        public void GossipAboutRegionNotifiesExistingSubscriptionsOfNewIntersections()
        {
            // Arrange
            this.SetupService();
            this.SetupMocksForInsertRegion();
            this.service.InsertRegion(
                this.sceneName,
                this.intersectingRegion,
                this.ttl,
                null,
                null);
            ImsRegion copiedIntersectingRegion = new ImsRegion(this.intersectingRegion);

            // Subscription not expired
            this.subscriptionData.Stub(d => d.Region).Return(copiedIntersectingRegion);
            this.subscriptionData.Stub(d => d.EstimatedExpirationTime).Return(TimeSpan.FromSeconds(1));
            this.timeKeeper.Stub(t => t.Now).Return(TimeSpan.FromSeconds(0));

            // Expect
            this.subscriptionData.Expect(d => d.EnterNotification(
                Arg<object>.Is.Equal(this.service),
                Arg<ImsEventArgs>.Matches(Constraints.Property.Value("OtherRegionId", this.region.Guid))));

            // Act
            this.service.GossipAboutRegion(this.region, this.ttl);

            // Verify
            this.subscriptionData.VerifyAllExpectations();
        }

        [Test]
        public void GossipAboutRegionDoesNotNotifyExpiredSubscriptionsOfNewIntersections()
        {
            // Arrange
            this.SetupService();
            this.SetupMocksForInsertRegion();
            this.service.InsertRegion(
                this.sceneName,
                this.intersectingRegion,
                this.ttl,
                null,
                null);
            ImsRegion copiedIntersectingRegion = new ImsRegion(this.intersectingRegion);

            // Subscription not expired
            this.subscriptionData.Stub(d => d.Region).Return(copiedIntersectingRegion);
            this.subscriptionData.Stub(d => d.EstimatedExpirationTime).Return(TimeSpan.FromSeconds(0));
            this.timeKeeper.Stub(t => t.Now).Return(TimeSpan.FromSeconds(1));

            // Expect
            this.subscriptionData.Expect(d => d.EnterNotification(null, null))
                .IgnoreArguments()
                .Repeat.Never();
            this.eventScheduler.Expect(q => q.Push(
                Arg<GenericCallBack<ISubscriptionData>>.Is.Anything,
                Arg<ISubscriptionData>.Is.Equal(this.subscriptionData)))
                .Return(null);

            // Act
            this.service.GossipAboutRegion(this.region, this.ttl);

            // Verify
            this.subscriptionData.VerifyAllExpectations();
            this.eventScheduler.VerifyAllExpectations();
        }

        [Test, Ignore]
        public void GossipAboutRegionUpdatesExistingUnsubscribedRegions()
        {
            // Arrange
            // - Capturing network online event handler (DoGossip).
            GenericCallBack networkHandler = null;
            this.connectivityReporter.Stub(r => r.NetworkOnlineEvent += null)
                .IgnoreArguments()
                .WhenCalled(c =>
                {
                    networkHandler = (GenericCallBack)c.Arguments[0];
                });
            this.SetupService();

            ImsRegion newRegion = new ImsRegion(this.region);
            newRegion.Centroid = new Vector3(1f, 1f, 1f);
            newRegion.Radius = 3f;
            uint newTtl = 9;

            // TODO: Trigger intersection notification to insert gossip region.

            // Expect
            this.gossipRegion.Expect(r => r.Update(newRegion, newTtl));

            // Act
            this.service.GossipAboutRegion(newRegion, newTtl);

            // Verify
            this.gossipRegion.VerifyAllExpectations();
        }

        [Test]
        public void DoGossipReschedulesItselfIfOnline()
        {
            // Arrange

            // Capture network online event handler (DoGossip).
            GenericCallBack networkHandler = null;
            this.connectivityReporter.Stub(r => r.NetworkOnlineEvent += null)
                .IgnoreArguments()
                .WhenCalled(c =>
                {
                    networkHandler = (GenericCallBack)c.Arguments[0];
                });
            this.SetupService();

            // Expect
            this.eventScheduler.Expect(q => q.Schedule((double)0.0, null))
                .IgnoreArguments()
                .WhenCalled(c =>
                    {
                        Assert.AreEqual(networkHandler.Method, ((GenericCallBack)c.Arguments[1]).Method);
                    })
                .Return(null);

            // Act
            networkHandler.Invoke();

            // Verify
            this.eventScheduler.VerifyAllExpectations();
        }

        // Ignoring this test, as it actually triggers an assert that is silently swallowed by a nasty catch-all exception catch.
        // A better test should be written.
        [Test, Ignore]
        public void DoGossipSelectsRemoteRegion()
        {
            // Arrange

            // - Capture network online event handler (DoGossip).
            GenericCallBack networkHandler = null;
            this.connectivityReporter.Stub(r => r.NetworkOnlineEvent += null)
                .IgnoreArguments()
                .WhenCalled(c =>
                {
                    networkHandler = (GenericCallBack)c.Arguments[0];
                });
            this.SetupService();
            this.SetupMocksForInsertRegion();
            this.service.InsertRegion(this.sceneName, this.region, this.ttl, null, null);

            // - Create remote region
            GossipImService.IGossipRegion remoteGossipRegion =
                MockRepository.GenerateMock<GossipImService.IGossipRegion>();
            this.gossipRegionFactory.Stub(f => f(this.intersectingRegion, this.ttl, this.timeKeeper))
                .Return(remoteGossipRegion);
            this.service.GossipAboutRegion(this.intersectingRegion, this.ttl);

            this.gossipRegion.Stub(r => r.TimeToLiveSeconds).Return(9);
            this.gossipRegion.Stub(r => r.Region).Return(this.region);

            remoteGossipRegion.Stub(r => r.TimeToLiveSeconds).Return(9);
            remoteGossipRegion.Stub(r => r.Region).Return(this.intersectingRegion);

            // Expect - what?

            // Act
            this.transportProtocol.ForgeMethodList();
            networkHandler.Invoke();            
        }

        [Test]
        public void InterceptInteresectionCreatesGossipRegionForUnsubscribedIntersectedRegions()
        {
            // Arrange:
            // - Capture client result subscription (InterceptInteresection)
            EventHandler<ImsStatusEventArgs> intersectionHandler = null;
            this.client.Expect(c => c.ResultNotification += null)
                .IgnoreArguments()
                .WhenCalled(c => { intersectionHandler = (EventHandler<ImsStatusEventArgs>)c.Arguments[0]; });

            this.SetupService();

            // Construct args for intersection event.
            ImsStatusEventArgs args = new ImsStatusEventArgs(
                new BadumnaId(new PeerAddress(HashKey.Random()), 0),
                new RequestStatus(RequestStatus.Statuses.Success),
                new List<ImsEventArgs>() { new ImsEventArgs(this.region) });

            // Expect
            this.gossipRegionFactory.Expect(f => f.Invoke(
                Arg<ImsRegion>.Matches(Constraints.Property.Value("Guid", this.region.Guid)),
                Arg<uint>.Is.Anything,
                Arg<ITime>.Is.Equal(this.timeKeeper)));

            // Act
            intersectionHandler.Invoke(this.client, args);

            // Verify
            this.gossipRegionFactory.VerifyAllExpectations();
        }

        [Test]
        public void CalculateEstimateOfActualRegionsRecordsZeroValueEventWhenNoRegionsExist()
        {
            // Arrange
            ISmoother smoother = MockRepository.GenerateMock<ISmoother>();
            this.smootherFactory.Stub(f => f.Invoke(5.0, 0)).Return(smoother);
            
            GenericCallBack calculateEstimation = null;
            this.regularTaskFactory.Stub(f => f.Invoke(
                string.Empty,
                TimeSpan.FromDays(0),
                this.eventScheduler,
                this.connectivityReporter,
                null))
                .IgnoreArguments()
                .WhenCalled(c => { calculateEstimation = (GenericCallBack)c.Arguments[4]; });

            this.SetupService();

            // Expect
            smoother.Expect(s => s.ValueEvent(0.0));

            // Act
            calculateEstimation.Invoke();

            // Verify
            smoother.VerifyAllExpectations();
        }

        [Test]
        public void CalculateEstimateOfActualRegionsRecordsRatioOfZeroWhenNoUnknownIntroductions()
        {
            // Arrange
            ISmoother smoother = MockRepository.GenerateMock<ISmoother>();
            this.smootherFactory.Stub(f => f.Invoke(5.0, 0)).Return(smoother);

            GenericCallBack calculateEstimation = null;
            this.regularTaskFactory.Stub(f => f.Invoke(
                string.Empty,
                TimeSpan.FromDays(0),
                this.eventScheduler,
                this.connectivityReporter,
                null))
                .IgnoreArguments()
                .WhenCalled(c => { calculateEstimation = (GenericCallBack)c.Arguments[4]; });

            this.SetupService();
            this.service.GossipAboutRegion(this.intersectingRegion, 7);

            // Expect
            smoother.Expect(s => s.ValueEvent(0.0));

            // Act
            calculateEstimation.Invoke();

            // Verify
            smoother.VerifyAllExpectations();
        }

        [Test]
        public void CalculateEstimateOfActualRegionsRecordsRatioOfOneWhenAllIntroductionsKnown()
        {
            // Arrange
            ISmoother smoother = MockRepository.GenerateMock<ISmoother>();
            this.smootherFactory.Stub(f => f.Invoke(5.0, 0)).Return(smoother);

            GenericCallBack calculateEstimation = null;
            this.regularTaskFactory.Stub(f => f.Invoke(
                string.Empty,
                TimeSpan.FromDays(0),
                this.eventScheduler,
                this.connectivityReporter,
                null))
                .IgnoreArguments()
                .WhenCalled(c => { calculateEstimation = (GenericCallBack)c.Arguments[4]; });

            this.SetupService();
            this.SetupMocksForInsertRegion();
            this.service.InsertRegion(this.sceneName, this.region, this.ttl, null, null);
            this.subscriptionData.Stub(d => d.EstimatedExpirationTime).Return(TimeSpan.FromSeconds(9));
            this.subscriptionData.Stub(d => d.Region).Return(this.region);
            this.service.GossipAboutRegion(this.intersectingRegion, 7);

            // Expect
            smoother.Expect(s => s.ValueEvent(1.0));

            // Act
            calculateEstimation.Invoke();

            // Verify
            smoother.VerifyAllExpectations();
        }

        [Test]
        public void ExpireSubscriptionRemoveSubscription()
        {
            // Arrange
            this.SetupService();
            this.SetupMocksForInsertRegion();
            this.service.InsertRegion(this.sceneName, this.region, this.ttl, null, null);
            
            // - configure for expiration
            this.subscriptionData.Stub(d => d.Region).Return(this.region);
            this.subscriptionData.Stub(d => d.EstimatedExpirationTime).Return(TimeSpan.FromSeconds(0));
            this.timeKeeper.Stub(t => t.Now).Return(TimeSpan.FromSeconds(9.0));
            GenericCallBack<ISubscriptionData> expireSubscription = null;
            
            // - capture expiration callback
            this.eventScheduler.Stub(q => q.Push(null, this.subscriptionData))
                .IgnoreArguments()
                .WhenCalled(c => { expireSubscription = (GenericCallBack<ISubscriptionData>)c.Arguments[0]; })
                .Return(null);
            
            // - trigger expiration
            this.service.GossipAboutRegion(this.intersectingRegion, this.ttl);

            // act
            expireSubscription.Invoke(this.subscriptionData);

            // Postconditions: subscription no longer available for modification
            this.subscriptionData.Expect(d => d.Region).Repeat.Never();
            this.service.ModifyRegion(this.sceneName, this.region, this.ttl, 0);
            this.subscriptionData.VerifyAllExpectations();
        }

        #endregion // Tests

        #region Helper methods

        /// <summary>
        /// Instantiates a GossipImService using dependencies creates in setup,
        /// stubbing dependencies' behaviours required in constructor.
        /// </summary>
        private void SetupService()
        {
            this.regularTaskFactory.Stub(f => f.Invoke(null, TimeSpan.MaxValue, null, null, null))
                .IgnoreArguments()
                .Return(this.regularTask);
            this.service = new GossipImService(
                this.transportProtocol,
                this.client,
                this.eventScheduler,
                this.timeKeeper,
                this.connectivityReporter,
                this.gossipRegionFactory,
                this.regularTaskFactory,
                this.subscriptionDataFactory,
                this.smootherFactory);
        }

        /// <summary>
        /// Stubs dependencies' behaviours required for region insertion.
        /// </summary>
        private void SetupMocksForInsertRegion()
        {
            this.subscriptionDataFactory.Stub(f => f(default(QualifiedName), null, 0, null, null, null))
                .IgnoreArguments()
                .Return(this.subscriptionData);
            this.gossipRegionFactory.Stub(f => f(this.region, this.ttl, this.timeKeeper)).Return(this.gossipRegion);
        }

        #endregion // Helper methods
    }
}
