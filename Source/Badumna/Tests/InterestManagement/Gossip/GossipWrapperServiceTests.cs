﻿//-----------------------------------------------------------------------
// <copyright file="GossipWrapperServiceTests.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BadumnaTests.InterestManagement
{
    using System;
    using Badumna.Core;
    using Badumna.DataTypes;
    using Badumna.DistributedHashTable;
    using Badumna.InterestManagement;
    using Badumna.Transport;
    using Badumna.Utilities;
    using BadumnaTests.Core;
    using NUnit.Framework;
    using Rhino.Mocks;

    [TestFixture]
    internal class GossipWrapperServiceTests
    {
        private IGossipImService gossipImService;
        private IInterestManagementService distributedImService;
        private IRandomNumberGenerator randomNumberGenerator;
        private GossipWrapperService gossipWrapperService;
        private QualifiedName sceneName;
        private ImsRegion region;
        private uint ttl;
        private byte collisions;

        [SetUp]
        public void SetUp()
        {
            this.gossipImService = MockRepository.GenerateMock<IGossipImService>();
            this.distributedImService = MockRepository.GenerateMock<IInterestManagementService>();
            this.randomNumberGenerator = MockRepository.GenerateMock<IRandomNumberGenerator>();
            this.gossipWrapperService = new GossipWrapperService(
                this.distributedImService,
                this.gossipImService,
                this.randomNumberGenerator);
            this.sceneName = new QualifiedName("", Guid.NewGuid().ToString());
            this.region = new ImsRegion(
                new Vector3(0f, 0f, 0f),
                1f,
                new BadumnaId(new PeerAddress(HashKey.Random()), 0),
                RegionType.Sink,
                default(QualifiedName));
            this.ttl = 99;
            this.collisions = 88;
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ConstructorThrowsForNullDistributedImService()
        {
            GossipWrapperService service = new GossipWrapperService(
                null,
                this.gossipImService,
                this.randomNumberGenerator);
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ConstructorThrowsForNullGossipImService()
        {
            GossipWrapperService service = new GossipWrapperService(
                this.distributedImService,
                null,
                this.randomNumberGenerator);
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ConstructorThrowsForNullrandomNumberGenerator()
        {
            GossipWrapperService service = new GossipWrapperService(
                this.distributedImService,
                this.gossipImService,
                null);
        }

        [Test]
        public void ProductionConstructorDoesNotThrow()
        {
            // Test passes if constructor does not throw an exception.
            DhtFacade facade = new DhtFacade(
                    MockRepository.GenerateMock<IRouter>(),
                    new Simulator(),
                    MockRepository.GenerateMock<ITime>(),
                    MockRepository.GenerateMock<INetworkAddressProvider>(),
                    MockRepository.GenerateMock<INetworkConnectivityReporter>(),
                    MessageParserTester.DefaultFactory);
            DhtFacade allInclusiveFacade = new DhtFacade(
                    MockRepository.GenerateMock<IRouter>(),
                    new Simulator(),
                    MockRepository.GenerateMock<ITime>(),
                    MockRepository.GenerateMock<INetworkAddressProvider>(),
                    MockRepository.GenerateMock<INetworkConnectivityReporter>(),
                    MessageParserTester.DefaultFactory);
            GossipWrapperService service = new GossipWrapperService(
                facade,
                allInclusiveFacade,
                new TransportProtocol("foo", typeof(ConnectionlessProtocolMethodAttribute), MessageParserTester.DefaultFactory),
                new NetworkEventQueue(),
                MockRepository.GenerateMock<ITime>(),
                MockRepository.GenerateMock<INetworkConnectivityReporter>());
        }

        [Test]
        public void InsertRegionCallsInsertRegionOnWrappedServices()
        {
            // Expect
            this.distributedImService.Expect(s => s.InsertRegion(this.sceneName, this.region, this.ttl, null, null));
            this.gossipImService.Expect(s => s.InsertRegion(this.sceneName, this.region, this.ttl, null, null));
            
            // Act
            this.gossipWrapperService.InsertRegion(this.sceneName, this.region, this.ttl, null, null);
            
            // Verify
            this.distributedImService.VerifyAllExpectations();
            this.gossipImService.VerifyAllExpectations();
        }

        [Test]
        public void ModifyRegionCallsModifyRegionOnGossipImServices()
        {
            // Expect
            this.gossipImService.Expect(s => s.ModifyRegion(this.sceneName, this.region, this.ttl, this.collisions));

            // Act
            this.gossipWrapperService.ModifyRegion(this.sceneName, this.region, this.ttl, this.collisions);

            // Verify
            this.gossipImService.VerifyAllExpectations();
        }

        [TestCase(0.0, 0.0, true)]
        [TestCase(1.0, 0.0, true)]
        [TestCase(1.0, 1.0, true)]
        [TestCase(1.0, 24.0, true)]
        [TestCase(1.0, 25.0, false)]
        [TestCase(0.99, 25.0, true)]
        [TestCase(1.0, 26.0, false)]
        [TestCase(0.95, 26.0, true)]
        [TestCase(0.49, 50.0, true)]
        [TestCase(0.51, 50.0, false)]
        [TestCase(0.24, 100.0, true)]
        [TestCase(0.26, 100.0, false)]
        public void ModifyRegionCallsModifyRegionOnDistributedImServiceWhenFortuneDictates(
            double randomNumber,
            double estimatedNumberOfRegions,
            bool callExpected)
        {
            // Assumptions
            Assert.AreEqual(25, Parameters.GossipThreshold);

            // Arrange
            this.randomNumberGenerator.Stub(g => g.NextDouble()).Return(randomNumber);
            this.gossipImService.Stub(s => s.EstimatedNumberOfActualRegions).Return(estimatedNumberOfRegions);

            // Expect
            if (callExpected)
            {
                this.distributedImService.Expect(s => s.ModifyRegion(this.sceneName, this.region, this.ttl, this.collisions));
            }
            else
            {
                this.distributedImService.Expect(s => s.ModifyRegion(this.sceneName, this.region, this.ttl, this.collisions))
                    .Repeat.Never();
            }

            // Act
            this.gossipWrapperService.ModifyRegion(this.sceneName, this.region, this.ttl, this.collisions);

            // Verify
            this.distributedImService.VerifyAllExpectations();
        }

        [Test]
        public void RemoveRegionCallsRemoveRegionOnWrappedServices()
        {
            // Expect
            this.distributedImService.Expect(s => s.RemoveRegion(this.sceneName, this.region));
            this.gossipImService.Expect(s => s.RemoveRegion(this.sceneName, this.region));

            // Act
            this.gossipWrapperService.RemoveRegion(this.sceneName, this.region);

            // Verify
            this.distributedImService.VerifyAllExpectations();
            this.gossipImService.VerifyAllExpectations();
        }

        [Test]
        public void RemoveRegionWithoutNotificationCallsRemoveRegionOnWrappedServices()
        {
            // Expect
            this.distributedImService.Expect(s => s.RemoveRegionWithoutNotification(this.sceneName, this.region));
            this.gossipImService.Expect(s => s.RemoveRegionWithoutNotification(this.sceneName, this.region));

            // Act
            this.gossipWrapperService.RemoveRegionWithoutNotification(this.sceneName, this.region);

            // Verify
            this.distributedImService.VerifyAllExpectations();
            this.gossipImService.VerifyAllExpectations();
        }

        [Test]
        public void DetachSubscriptionCallsRemoveRegionOnWrappedServices()
        {
            // Expect
            this.distributedImService.Expect(s => s.DetachSubscription(this.sceneName, this.region));
            this.gossipImService.Expect(s => s.DetachSubscription(this.sceneName, this.region));

            // Act
            this.gossipWrapperService.DetachSubscription(this.sceneName, this.region);

            // Verify
            this.distributedImService.VerifyAllExpectations();
            this.gossipImService.VerifyAllExpectations();
        }
    }
}
