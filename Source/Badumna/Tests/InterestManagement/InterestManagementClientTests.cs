﻿//-----------------------------------------------------------------------
// <copyright file="InterestManagementClientTests.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2010 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BadumnaTests.InterestManagement
{
    using System;
    using System.Collections.Generic;
    using Badumna.Core;
    using Badumna.DataTypes;
    using Badumna.InterestManagement;
    using Badumna.Utilities;
    using BadumnaTests.Core;
    using NUnit.Framework;
    using Rhino.Mocks;

    [TestFixture]
    public class InterestManagementClientTests
    {
        private FakeInterestManagementClient client;
        private MessageParser messageParser;

        [SetUp]
        public void SetUp()
        {
            this.messageParser = MockRepository.GenerateMock<MessageParser>(
                Guid.NewGuid().ToString(),
                typeof(DummyProtocolMethodAttribute),
                MockRepository.GenerateMock<GenericCallBackReturn<object, Type>>());
            this.client = new FakeInterestManagementClient(this.messageParser);
        }

        [TearDown]
        public void TearDown()
        {
        }

        #region Tests

        [Test]
        public void ConstructorRegistersMethodsWithProtocolLayer()
        {
            InterestManagementClient passedClient = null;
            this.messageParser.Expect(p => p.RegisterMethodsIn(this.client))
                .IgnoreArguments()
                .WhenCalled(call => { passedClient = (InterestManagementClient)call.Arguments[0]; });
            FakeInterestManagementClient client = new FakeInterestManagementClient(this.messageParser);
            this.messageParser.VerifyAllExpectations();
            Assert.AreEqual(client, passedClient);
        }

        [Test]
        public void StatusResponseNotifiesSubscribersToStatusNotifications()
        {
            BadumnaId id = new BadumnaId(PeerAddress.Nowhere, 99);
            RequestStatus status = new RequestStatus();
            EventHandler<ImsStatusEventArgs> statusHandler = MockRepository.GenerateMock<EventHandler<ImsStatusEventArgs>>();
            this.client.StatusNotification += statusHandler;
            statusHandler.Expect(h => h.Invoke(null, null))
                .IgnoreArguments()
                .WhenCalled(call =>
                {
                    InterestManagementClient sender = (InterestManagementClient)call.Arguments[0];
                    ImsStatusEventArgs args = (ImsStatusEventArgs)call.Arguments[1];
                    Assert.AreEqual(this.client, sender);
                    Assert.AreEqual(id, args.Id);
                    Assert.AreEqual(status, args.Status);
                });
            this.client.StatusResponse(id, status);
            statusHandler.VerifyAllExpectations();
        }

        [Test]
        public void IntersectionNotificationNotifiesSubscribersToResultNotifications()
        {
            BadumnaId id = new BadumnaId(PeerAddress.Nowhere, 99);
            BadumnaId otherId = new BadumnaId(PeerAddress.Nowhere, 88);
            ImsEventArgs imsEventArgs = new ImsEventArgs(otherId, RegionType.None);
            EventHandler<ImsStatusEventArgs> resultHandler = MockRepository.GenerateMock<EventHandler<ImsStatusEventArgs>>();
            this.client.ResultNotification += resultHandler;
            resultHandler.Expect(h => h.Invoke(null, null))
                .IgnoreArguments()
                .WhenCalled(call =>
                {
                    InterestManagementClient sender = (InterestManagementClient)call.Arguments[0];
                    ImsStatusEventArgs args = (ImsStatusEventArgs)call.Arguments[1];
                    Assert.AreEqual(this.client, sender);
                    Assert.AreEqual(id, args.Id);
                    Assert.AreEqual(imsEventArgs, args.EnterList[0]);
                });
            this.client.IntersectionNotification(id, new List<ImsEventArgs>() { imsEventArgs });
            resultHandler.VerifyAllExpectations();
        }

        #endregion Tests

        #region Helper methods

        #endregion // Helper methods

        #region Fakes

        /// <summary>
        /// Fake to permit testing of astract class.
        /// </summary>
        internal class FakeInterestManagementClient : InterestManagementClient
        {
            public FakeInterestManagementClient(MessageParser messageParser)
                : base(messageParser)
            {
            }
        }

        #endregion // Fakes
    }
}