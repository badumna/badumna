﻿//-----------------------------------------------------------------------
// <copyright file="ImsRegionTests.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2011 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BadumnaTests.InterestManagement
{
    using System;
    using Badumna.Core;
    using Badumna.DataTypes;
    using Badumna.DistributedHashTable;
    using Badumna.InterestManagement;
    using Badumna.Utilities;
    using NUnit.Framework;

    [TestFixture]
    public class ImsRegionTests
    {
        private ImsRegion defaultRegion;

        [SetUp]
        public void SetUp()
        {
            this.defaultRegion = new ImsRegion(
                new Vector3(0f, 0f, 0f),
                1f,
                new BadumnaId(PeerAddress.GetLoopback(1), 1),
                RegionType.None,
                default(QualifiedName));
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void CopyConstructorThrowsForNullRegion()
        {
            ImsRegion region = new ImsRegion(null);
        }

        [Test]
        public void ConstructorCreatesIdWhenrequired()
        {
            Assert.IsNotNull(this.defaultRegion.Guid);
        }

        [Test]
        public void XUpdatesCentroidX()
        {
            Assert.AreEqual(0f, this.defaultRegion.Centroid.X);
            Assert.AreEqual(0f, this.defaultRegion.X);
            this.defaultRegion.X = 1f;
            Assert.AreEqual(1f, this.defaultRegion.X);
            Assert.AreEqual(1f, this.defaultRegion.Centroid.X);
        }

        [Test]
        public void YUpdatesCentroidY()
        {
            Assert.AreEqual(0f, this.defaultRegion.Centroid.Y);
            Assert.AreEqual(0f, this.defaultRegion.Y);
            this.defaultRegion.Y = 2f;
            Assert.AreEqual(2f, this.defaultRegion.Y);
            Assert.AreEqual(2f, this.defaultRegion.Centroid.Y);
        }

        [Test]
        public void ZUpdatesCentroidZ()
        {
            Assert.AreEqual(0f, this.defaultRegion.Centroid.Z);
            Assert.AreEqual(0f, this.defaultRegion.Z);
            this.defaultRegion.Z = 3f;
            Assert.AreEqual(3f, this.defaultRegion.Z);
            Assert.AreEqual(3f, this.defaultRegion.Centroid.Z);
        }

        [Test]
        public void IsIntersectingReturnsFalseForNullRegion()
        {
            Assert.IsFalse(this.defaultRegion.IsIntersecting(null));
        }

        [Test]
        public void IsIntersectingReturnsFalseForRegionInDifferentScene()
        {
            ImsRegion otherRegion = new ImsRegion(
                new Vector3(0f, 0f, 0f),
                1f,
                new BadumnaId(PeerAddress.GetLoopback(1), 2),
                RegionType.None,
                new QualifiedName("", Guid.NewGuid().ToString()));
            Assert.IsFalse(this.defaultRegion.IsIntersecting(otherRegion));
        }

        [Test]
        public void IsIntersectingReturnsTrueForTwoInverseSpheres()
        {
            ImsRegion otherRegion = new ImsRegion(this.defaultRegion);
            otherRegion.Centroid = new Vector3(99f, 99f, 99f);
            this.defaultRegion.Options = this.defaultRegion.Options ^ RegionOptions.Inverse;
            otherRegion.Options = otherRegion.Options ^ RegionOptions.Inverse;
            Assert.IsTrue(this.defaultRegion.IsIntersecting(otherRegion));
        }

        [Test]
        public void IsIntersectingReturnsFalseForSphereAndItsInverse()
        {
            ImsRegion otherRegion = new ImsRegion(this.defaultRegion);
            otherRegion.Options = otherRegion.Options ^ RegionOptions.Inverse;
            Assert.IsFalse(this.defaultRegion.IsIntersecting(otherRegion));
        }

        [Test]
        public void IsIntersectingReturnsFalseForInverseSphereAndItsInverse()
        {
            ImsRegion otherRegion = new ImsRegion(this.defaultRegion);
            this.defaultRegion.Options = this.defaultRegion.Options ^ RegionOptions.Inverse;
            Assert.IsFalse(this.defaultRegion.IsIntersecting(otherRegion));
        }

        [TestCase(0f, 0f, 0f, 0.866f, 1f, 1f, 1f, 0.866f)] // Not quite touching (r < sqrt(3)/2)
        [TestCase(0f, 0f, 0f, 1f, 2f, 0f, 0f, 1f)] // Just touching
        public void IsIntersectingReturnsFalseForNonIntersectingSpheres(
            float x1,
            float y1,
            float z1,
            float r1,
            float x2,
            float y2,
            float z2,
            float r2)
        {
            ImsRegion otherRegion = new ImsRegion(this.defaultRegion);
            this.defaultRegion.Centroid = new Vector3(x1, y1, z1);
            this.defaultRegion.Radius = r1;
            otherRegion.Centroid = new Vector3(x2, y2, z2);
            otherRegion.Radius = r2;
            Assert.IsFalse(this.defaultRegion.IsIntersecting(otherRegion));
            Assert.IsFalse(otherRegion.IsIntersecting(this.defaultRegion));
        }

        [TestCase(0f, 0f, 0f, 0.867f, 1f, 1f, 1f, 0.867f)] // Just overlapping (r > sqrt(3)/2)
        [TestCase(0f, 0f, 0f, 1.01f, 2f, 0f, 0f, 1f)] // Just overlapping
        public void IsIntersectingReturnsTrueForIntersectingSpheres(
            float x1,
            float y1,
            float z1,
            float r1,
            float x2,
            float y2,
            float z2,
            float r2)
        {
            ImsRegion otherRegion = new ImsRegion(this.defaultRegion);
            this.defaultRegion.Centroid = new Vector3(x1, y1, z1);
            this.defaultRegion.Radius = r1;
            otherRegion.Centroid = new Vector3(x2, y2, z2);
            otherRegion.Radius = r2;
            Assert.IsTrue(this.defaultRegion.IsIntersecting(otherRegion));
            Assert.IsTrue(otherRegion.IsIntersecting(this.defaultRegion));
        }

        [TestCase(0f, 0f, 0f, 1f, 0.5f, 0.5f, 0.5f, 0.133f)] // Not quite touching (r2 < 1 - sqrt(3)/2)
        [TestCase(0f, 0f, 0f, 1f, 0.5f, 0f, 0f, 0.5f)] // Just touching
        public void IsIntersectingReturnsFalseForSphereAndContainingInverse(
            float x1,
            float y1,
            float z1,
            float r1,
            float x2,
            float y2,
            float z2,
            float r2)
        {
            ImsRegion otherRegion = new ImsRegion(this.defaultRegion);
            this.defaultRegion.Centroid = new Vector3(x1, y1, z1);
            this.defaultRegion.Radius = r1;
            this.defaultRegion.Options = this.defaultRegion.Options ^ RegionOptions.Inverse;
            otherRegion.Centroid = new Vector3(x2, y2, z2);
            otherRegion.Radius = r2;

            float mag = (this.defaultRegion.Centroid - otherRegion.Centroid).Magnitude;
            ////Console.Write(mag);
            Assert.IsFalse(this.defaultRegion.IsIntersecting(otherRegion));
            Assert.IsFalse(otherRegion.IsIntersecting(this.defaultRegion));
        }

        [TestCase(0f, 0f, 0f, 1f, 0.5f, 0.5f, 0.5f, 0.134f)] // Not quite touching (r2 > 1 - sqrt(3)/2)
        [TestCase(0f, 0f, 0f, 1f, 0.5001f, 0f, 0f, 0.5f)] // Just touching
        public void IsIntersectingReturnsTrueForInverseAndUncontainedSphere(
            float x1,
            float y1,
            float z1,
            float r1,
            float x2,
            float y2,
            float z2,
            float r2)
        {
            ImsRegion otherRegion = new ImsRegion(this.defaultRegion);
            this.defaultRegion.Centroid = new Vector3(x1, y1, z1);
            this.defaultRegion.Radius = r1;
            this.defaultRegion.Options = this.defaultRegion.Options ^ RegionOptions.Inverse;
            otherRegion.Centroid = new Vector3(x2, y2, z2);
            otherRegion.Radius = r2;
            Assert.IsTrue(this.defaultRegion.IsIntersecting(otherRegion));
            Assert.IsTrue(otherRegion.IsIntersecting(this.defaultRegion));
        }

        [Test]
        public void RequiresIntersectionNotificationReturnsFalseWhenUninterested()
        {
            ImsRegion otherRegion = new ImsRegion(
                new Vector3(0f, 0f, 0f),
                1f,
                new BadumnaId(PeerAddress.GetLoopback(1), 2),
                RegionType.Sink,
                default(QualifiedName));
            this.defaultRegion.Options = this.defaultRegion.Options ^ RegionOptions.Uninterested;
            Assert.IsFalse(this.defaultRegion.RequiresIntersectionNotification(otherRegion));
        }

        [Test]
        public void RequiresIntersectionNotificationReturnsTrueWhenSourceForPassedSinkWithDifferentaAddress()
        {
            ImsRegion sourceRegion = new ImsRegion(
                new Vector3(0f, 0f, 0f),
                1f,
                new BadumnaId(new PeerAddress(HashKey.Random()), 0),
                RegionType.Source,
                default(QualifiedName));
            ImsRegion sinkRegion = new ImsRegion(
                new Vector3(0f, 0f, 0f),
                1f,
                new BadumnaId(new PeerAddress(HashKey.Random()), 0),
                RegionType.Sink,
                default(QualifiedName));
            Assert.IsTrue(sourceRegion.RequiresIntersectionNotification(sinkRegion));
        }

        [Test]
        public void RequiresIntersectionNotificationReturnsTrueWhenSourceEvenForPassedSinkWithSameAddress()
        {
            PeerAddress address = new PeerAddress(HashKey.Random());
            ImsRegion sourceRegion = new ImsRegion(
                new Vector3(0f, 0f, 0f),
                1f,
                new BadumnaId(address, 0),
                RegionType.Source,
                default(QualifiedName));
            ImsRegion sinkRegion = new ImsRegion(
                new Vector3(0f, 0f, 0f),
                1f,
                new BadumnaId(address, 1),
                RegionType.Sink,
                default(QualifiedName));
            Assert.IsTrue(sourceRegion.RequiresIntersectionNotification(sinkRegion));
        }

        [Test]
        public void RequiresIntersectionNotificationReturnsFalseWhenPassedSource()
        {
            ImsRegion sourceRegion = new ImsRegion(
                new Vector3(0f, 0f, 0f),
                1f,
                new BadumnaId(PeerAddress.GetLoopback(1), 2),
                RegionType.Source,
                default(QualifiedName));
            Assert.IsFalse(this.defaultRegion.RequiresIntersectionNotification(sourceRegion));
        }

        [Test]
        public void IParseableSerializationPreservesAllData()
        {
            BadumnaId id = new BadumnaId(new PeerAddress(HashKey.Random()), 0);
            ImsRegion sentRegion = new ImsRegion(
                new Vector3(1f, 2f, 3f),
                4f,
                id,
                RegionType.Source,
                new QualifiedName("", Guid.NewGuid().ToString()));
            MessageBuffer buffer = new MessageBuffer();
            sentRegion.ToMessage(buffer, null);
            ImsRegion receivedRegion = new ImsRegion();
            receivedRegion.FromMessage(buffer);
            Assert.AreEqual(sentRegion.Centroid, receivedRegion.Centroid);
            Assert.AreEqual(sentRegion.Radius, receivedRegion.Radius);
            Assert.AreEqual(sentRegion.Type, receivedRegion.Type);
            Assert.AreEqual(sentRegion.Options, receivedRegion.Options);
            Assert.AreEqual(sentRegion.SceneName, receivedRegion.SceneName);
        }

        [Test]
        public void ToStringWritesId()
        {
            Assert.IsTrue(this.defaultRegion.ToString().Contains(this.defaultRegion.Guid.ToString()));
        }

        [Test]
        public void ToStringWritesType()
        {
            Assert.IsTrue(this.defaultRegion.ToString().Contains(this.defaultRegion.Type.ToString()));
        }
    }
}
