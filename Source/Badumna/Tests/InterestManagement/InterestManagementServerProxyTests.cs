﻿//-----------------------------------------------------------------------
// <copyright file="InterestManagementServerProxyTests.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2010 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BadumnaTests.InterestManagement
{
    using System;
    using Badumna.Core;
    using Badumna.InterestManagement;
    using Badumna.Utilities;
    using BadumnaTests.Core;
    using NUnit.Framework;
    using Rhino.Mocks;

    [TestFixture]
    public class InterestManagementServerProxyTests
    {
        private FakeInterestManagementServerProxy proxy;
        private MessageParser messageParser;
        private InterestManagementServerProtocol protocol;
        private IEnvelope envelope;
        private QualityOfService qos;
        private QualifiedName sceneName;
        private uint ttl;
        private byte collisions;

        [SetUp]
        public void SetUp()
        {
            this.messageParser = MockRepository.GenerateMock<MessageParser>(
                Guid.NewGuid().ToString(),
                typeof(DummyProtocolMethodAttribute),
                MockRepository.GenerateMock<GenericCallBackReturn<object, Type>>());
            this.protocol = MockRepository.GenerateMock<InterestManagementServerProtocol>();
            this.envelope = MockRepository.GenerateMock<IEnvelope>();
            this.qos = new QualityOfService();
            this.envelope.Stub(e => e.Qos).Return(this.qos);
            this.proxy = new FakeInterestManagementServerProxy(
                this.messageParser,
                this.protocol,
                this.envelope);
            this.sceneName = new QualifiedName("", Guid.NewGuid().ToString());
            this.ttl = (uint)new Random().Next(1, int.MaxValue);
            this.collisions = (byte)new Random().Next(byte.MaxValue);
        }

        [TearDown]
        public void TearDown()
        {
        }

        #region Tests

        [Test]
        public void InsertRequestPreparesEnvelope()
        {
            this.proxy.InsertRequest(this.sceneName, null, this.ttl);
            Assert.AreEqual(1, this.proxy.PrepareRequestMessageCallCount);
        }

        [Test]
        public void InsertRequestSetsQosPriorityHigh()
        {
            this.proxy.InsertRequest(this.sceneName, null, this.ttl);

            // TO DO: Make testable by making Qos interface
            Assert.AreEqual(
                (float)QosPriority.High / (float)byte.MaxValue,
                this.qos.PriorityProbability);
        }

        [Test]
        public void InsertRequestMakesRemoteCallViaMessageParser()
        {
            ImsRegion region = new ImsRegion();
            this.messageParser.Expect(
                p => p.RemoteCall(
                    this.envelope,
                    this.protocol.InsertRequest,
                    this.sceneName,
                    region,
                    this.ttl));
            this.proxy.InsertRequest(this.sceneName, region, this.ttl);
            this.messageParser.VerifyAllExpectations();
        }

        [Test]
        public void InsertRequestDispatchesMessage()
        {
            ImsRegion region = new ImsRegion();
            this.proxy.InsertRequest(this.sceneName, region, this.ttl);
            Assert.AreEqual(1, this.proxy.DispatchMessageCallCount);
        }

        [Test]
        public void RemoveRequestPreparesEnvelope()
        {
            this.proxy.RemoveRequest(this.sceneName, null);
            Assert.AreEqual(1, this.proxy.PrepareRequestMessageCallCount);
        }

        [Test]
        public void RemoveRequestSetsQosPriorityHigh()
        {
            this.proxy.RemoveRequest(this.sceneName, null);

            // TO DO: Make testable by making Qos interface
            Assert.AreEqual(
                (float)QosPriority.High / (float)byte.MaxValue,
                this.qos.PriorityProbability);
        }

        [Test]
        public void RemoveRequestMakesRemoteCallViaMessageParser()
        {
            ImsRegion region = new ImsRegion();
            this.messageParser.Expect(
                p => p.RemoteCall(
                    this.envelope,
                    this.protocol.RemoveRequest,
                    this.sceneName,
                    region));
            this.proxy.RemoveRequest(this.sceneName, region);
            this.messageParser.VerifyAllExpectations();
        }

        [Test]
        public void RemoveRequestDispatchesMessage()
        {
            ImsRegion region = new ImsRegion();
            this.proxy.RemoveRequest(this.sceneName, region);
            Assert.AreEqual(1, this.proxy.DispatchMessageCallCount);
        }

        [Test]
        public void RemoveWithoutNotificationRequestPreparesEnvelope()
        {
            this.proxy.RemoveWithoutNotificationRequest(this.sceneName, null);
            Assert.AreEqual(1, this.proxy.PrepareRequestMessageCallCount);
        }

        [Test]
        public void RemoveWithoutNotificationRequestSetsQosPriorityHigh()
        {
            this.proxy.RemoveWithoutNotificationRequest(this.sceneName, null);

            // TO DO: Make testable by making Qos interface
            Assert.AreEqual(
                (float)QosPriority.High / (float)byte.MaxValue,
                this.qos.PriorityProbability);
        }

        [Test]
        public void RemoveWithoutNotificationRequestMakesRemoteCallViaMessageParser()
        {
            ImsRegion region = new ImsRegion();
            this.messageParser.Expect(
                p => p.RemoteCall(
                    this.envelope,
                    this.protocol.RemoveWithoutNotificationRequest,
                    this.sceneName,
                    region));
            this.proxy.RemoveWithoutNotificationRequest(this.sceneName, region);
            this.messageParser.VerifyAllExpectations();
        }

        [Test]
        public void RemoveWithoutNotificationRequestDispatchesMessage()
        {
            ImsRegion region = new ImsRegion();
            this.proxy.RemoveWithoutNotificationRequest(this.sceneName, region);
            Assert.AreEqual(1, this.proxy.DispatchMessageCallCount);
        }

        [Test]
        public void ModifyRequestPreparesEnvelope()
        {
            this.proxy.ModifyRequest(this.sceneName, null, this.ttl, this.collisions);
            Assert.AreEqual(1, this.proxy.PrepareRequestMessageCallCount);
        }

        [Test]
        public void ModifyRequestSetsQosPriorityHigh()
        {
            this.proxy.ModifyRequest(this.sceneName, null, this.ttl, this.collisions);

            // TO DO: Make testable by making Qos interface
            Assert.AreEqual(
                (float)QosPriority.High / (float)byte.MaxValue,
                this.qos.PriorityProbability);
        }

        [Test]
        public void ModifyRequestMakesRemoteCallViaMessageParser()
        {
            ImsRegion region = new ImsRegion();
            this.messageParser.Expect(
                p => p.RemoteCall(
                    this.envelope,
                    this.protocol.ModifyRequest,
                    this.sceneName,
                    region,
                    this.ttl,
                    this.collisions));
            this.proxy.ModifyRequest(this.sceneName, region, this.ttl, this.collisions);
            this.messageParser.VerifyAllExpectations();
        }

        [Test]
        public void ModifyRequestDispatchesMessage()
        {
            ImsRegion region = new ImsRegion();
            this.proxy.ModifyRequest(this.sceneName, region, this.ttl, this.collisions);
            Assert.AreEqual(1, this.proxy.DispatchMessageCallCount);
        }

        #endregion Tests

        #region Helper methods

        #endregion // Helper methods

        #region Fakes

        /// <summary>
        /// Fake to permit testing of abstract base class.
        /// </summary>
        internal class FakeInterestManagementServerProxy : InterestManagementServerProxy
        {
            private IEnvelope envelope;

            public FakeInterestManagementServerProxy(
                MessageParser parser,
                InterestManagementServerProtocol serverProtocol,
                IEnvelope envelope)
                : base(parser, serverProtocol)
            {
                this.envelope = envelope;
            }

            public int PrepareRequestMessageCallCount { get; set; }

            public int DispatchMessageCallCount { get; set; }

            protected override IEnvelope PrepareRequestMessage()
            {
                this.PrepareRequestMessageCallCount++;
                return this.envelope;
            }

            protected override void DispatchMessage(IEnvelope envelope)
            {
                this.DispatchMessageCallCount++;
            }
        }

        #endregion // Fakes
    }
}