﻿//-----------------------------------------------------------------------
// <copyright file="DistributedImServerProxyTests.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2010 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BadumnaTests.InterestManagement.DHT
{
    using System;
    using Badumna.Core;
    using Badumna.DataTypes;
    using Badumna.DistributedHashTable;
    using Badumna.InterestManagement;
    using Badumna.Utilities;
    using BadumnaTests.Core;
    using NUnit.Framework;
    using Rhino.Mocks;

    [TestFixture]
    public class DistributedImServerProxyTests
    {
        private DistributedImServerProxy proxy;
        private InterestManagementServerProtocol serverProtocol;
        private IDhtProtocol dhtProtocol;
        private MessageParser messageParser;
        private DhtEnvelope dhtEnvelope;

        [SetUp]
        public void SetUp()
        {
            this.serverProtocol = MockRepository.GenerateMock<InterestManagementServerProtocol>();
            this.dhtProtocol = MockRepository.GenerateMock<IDhtProtocol>();
            this.messageParser = MockRepository.GenerateMock<MessageParser>(
                Guid.NewGuid().ToString(),
                typeof(DummyProtocolMethodAttribute),
                MockRepository.GenerateMock<GenericCallBackReturn<object, Type>>());
            this.dhtProtocol.Stub(p => p.Parser).Return(this.messageParser);
            this.dhtEnvelope = new DhtEnvelope();
            this.proxy = new DistributedImServerProxy(this.dhtProtocol, this.serverProtocol);
        }

        [Test]
        public void DestinationKeyStringWorks()
        {
            string keyString = Guid.NewGuid().ToString();
            this.proxy.DestinationKeyString = keyString;
            Assert.AreEqual(keyString, this.proxy.DestinationKeyString);
        }

        [Test]
        public void InsertRequestGetsEnvelopeFromDhtProtocol()
        {
            BadumnaId id = new BadumnaId(PeerAddress.Nowhere, 99);
            RequestStatus status = new RequestStatus();
            string destinationKeyString = "foo";
            this.proxy.DestinationKeyString = destinationKeyString;
            this.dhtProtocol.Expect(p => p.GetMessageFor(null, null, 0))
                .IgnoreArguments()
                .Constraints(
                    Rhino.Mocks.Constraints.Is.Equal(HashKey.Hash(destinationKeyString)),
                    Rhino.Mocks.Constraints.Is.Equal(QualityOfService.Reliable),
                    Rhino.Mocks.Constraints.Is.Equal(Parameters.TotalNumberOfRedundantCellServers))
                    .Return(new DhtEnvelope());
            this.proxy.InsertRequest(new QualifiedName("", "sceneName"), new ImsRegion(), 99);
            this.dhtProtocol.VerifyAllExpectations();
        }

        [Test]
        public void StatusResponseSendsMessageViaDhtProtocol()
        {
            BadumnaId id = new BadumnaId(PeerAddress.Nowhere, 99);
            RequestStatus status = new RequestStatus();
            string destinationKeyString = "foo";
            this.proxy.DestinationKeyString = destinationKeyString;
            DhtEnvelope envelope = new DhtEnvelope();
            this.dhtProtocol.Stub(p => p.GetMessageFor(null, null, 0))
                .IgnoreArguments()
                .Return(envelope);
            this.dhtProtocol.Expect(p => p.SendMessage(envelope));
            this.proxy.InsertRequest(new QualifiedName("", "sceneName"), new ImsRegion(), 99);
            this.dhtProtocol.VerifyAllExpectations();
        }
    }
}
