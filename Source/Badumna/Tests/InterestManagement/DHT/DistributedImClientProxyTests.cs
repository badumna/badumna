﻿//-----------------------------------------------------------------------
// <copyright file="DistributedImClientProxyTests.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2010 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BadumnaTests.InterestManagement.DHT
{
    using System;
    using Badumna.Core;
    using Badumna.DataTypes;
    using Badumna.DistributedHashTable;
    using Badumna.InterestManagement;
    using Badumna.Utilities;
    using BadumnaTests.Core;
    using NUnit.Framework;
    using Rhino.Mocks;

    [TestFixture]
    public class DistributedImClientProxyTests
    {
        private DistributedImClientProxy proxy;
        private InterestManagementClientProtocol clientProtocol;
        private IDhtProtocol dhtProtocol;
        private MessageParser messageParser;
        private DhtEnvelope dhtEnvelope;

        [SetUp]
        public void SetUp()
        {
            this.clientProtocol = MockRepository.GenerateMock<InterestManagementClientProtocol>();
            this.dhtProtocol = MockRepository.GenerateMock<IDhtProtocol>();
            this.messageParser = MockRepository.GenerateMock<MessageParser>(
                Guid.NewGuid().ToString(),
                typeof(DummyProtocolMethodAttribute),
                MockRepository.GenerateMock<GenericCallBackReturn<object, Type>>());
            this.dhtProtocol.Stub(p => p.Parser).Return(this.messageParser);
            this.dhtEnvelope = new DhtEnvelope();
            this.proxy = new DistributedImClientProxy(this.dhtProtocol, this.clientProtocol);
        }

        [Test]
        public void StatusResponseGetsEnvelopeFromDhtProtocol()
        {
            PeerAddress destination = new PeerAddress(HashKey.Random());
            QualityOfService qos = new QualityOfService();
            BadumnaId id = new BadumnaId(PeerAddress.Nowhere, 99);
            RequestStatus status = new RequestStatus();
            this.proxy.MessageDestination = destination;
            this.proxy.MessageQos = qos;
            this.dhtProtocol.Expect(p => p.GetMessageFor(destination, qos))
                .Return(this.dhtEnvelope);

            this.proxy.StatusResponse(id, status);

            this.dhtProtocol.VerifyAllExpectations();
        }

        [Test]
        public void StatusResponseSendsMessageViaDhtProtocol()
        {
            PeerAddress destination = new PeerAddress(HashKey.Random());
            QualityOfService qos = new QualityOfService();
            BadumnaId id = new BadumnaId(PeerAddress.Nowhere, 99);
            RequestStatus status = new RequestStatus();
            this.proxy.MessageDestination = destination;
            this.proxy.MessageQos = qos;
            this.dhtProtocol.Stub(p => p.GetMessageFor(destination, qos))
                .Return(this.dhtEnvelope);
            this.dhtProtocol.Expect(p => p.SendMessage(this.dhtEnvelope));

            this.proxy.StatusResponse(id, status);

            this.dhtProtocol.VerifyAllExpectations();
        }
    }
}
