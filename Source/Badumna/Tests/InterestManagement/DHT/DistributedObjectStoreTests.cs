﻿//-----------------------------------------------------------------------
// <copyright file="DistributedObjectStoreTests.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2010 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using BadumnaTests.Utilities;

namespace BadumnaTests.InterestManagement.DHT
{
    using System;
    using System.Collections.Generic;
    using Badumna.Core;
    using Badumna.DataTypes;
    using Badumna.DistributedHashTable;
    using Badumna.InterestManagement;
    using Badumna.Utilities;
    using NUnit.Framework;
    using Rhino.Mocks;

    [TestFixture]
    public class DistributedObjectStoreTests
    {
        private QualifiedName sceneName;
        private IDhtFacade facade;
        private DistributedObjectStore store;
        private EventHandler<IntersectionEventArgs> separationNotificationHandler;
        private EventHandler<IntersectionEventArgs> intersectionNotificationHandler;

        private HashKey key;
        private DhtEnvelope envelope;

        private ImsRegion nonZeroRegion;
        private ImsRegion zeroRegion;
        private uint ttl;
        private byte knownCollisions;

        private ImsRegion originalRegion;
        private ImsRegion newRegionNonIntersecting;
        private ImsRegion newRegionIntersecting;
        private ImsRegion otherRegionNonIntersecting;
        private ImsRegion otherRegionIntersecting;
        private ImsRegion extraRegionIntersecting;

        [SetUp]
        public void SetUp()
        {
            this.sceneName = new QualifiedName("", Guid.NewGuid().ToString());
            this.facade = MockRepository.GenerateMock<IDhtFacade>();
            this.store = new DistributedObjectStore(this.sceneName, this.facade);

            this.separationNotificationHandler = MockRepository.GenerateMock<EventHandler<IntersectionEventArgs>>();
            this.intersectionNotificationHandler = MockRepository.GenerateMock<EventHandler<IntersectionEventArgs>>();

            this.store.SeperationNotification += this.separationNotificationHandler;
            this.store.IntersectionNotification += this.intersectionNotificationHandler;

            this.key = HashKey.Hash(Guid.NewGuid().ToString());
            this.envelope = new DhtEnvelope(this.key, QualityOfService.Reliable);

            GenericCallBackReturn<BadumnaId, PeerAddress> generateBadumnaId = BadumnaIdGenerator.FromAddress();

            this.nonZeroRegion = new ImsRegion(
                new Vector3(0f, 0f, 0f),
                1f,
                generateBadumnaId(PeerAddress.GetLoopback(1)),
                RegionType.None,
                this.sceneName);
            this.zeroRegion = new ImsRegion(
                new Vector3(0f, 0f, 0f),
                0f,
                generateBadumnaId(PeerAddress.GetLoopback(1)),
                RegionType.None,
                this.sceneName);
            this.ttl = 7;
            this.knownCollisions = 0;

            // Test regions:
            // An original region, two new versions of it (one in same location, and one at a
            // second, non-intersecting location), and two two other regions (one in same location,
            // at the second non-intersecting location).
            this.originalRegion = new ImsRegion(
                new Vector3(0f, 0f, 0f),
                1f,
                generateBadumnaId(PeerAddress.GetLoopback(1)),
                RegionType.Source,
                this.sceneName);
            this.newRegionIntersecting = new ImsRegion(this.originalRegion);
            this.newRegionNonIntersecting = new ImsRegion(this.originalRegion);
            this.newRegionNonIntersecting.Centroid = new Vector3(9f, 9f, 9f);

            this.otherRegionIntersecting = new ImsRegion(
                new Vector3(0f, 0f, 0f),
                1f,
                generateBadumnaId(PeerAddress.GetLoopback(1)),
                RegionType.Sink,
                this.sceneName);
            this.otherRegionIntersecting.Guid = generateBadumnaId(new PeerAddress(HashKey.Random()));
            this.otherRegionNonIntersecting = new ImsRegion(this.otherRegionIntersecting);
            this.otherRegionNonIntersecting.Centroid = new Vector3(9f, 9f, 9f);
            this.otherRegionNonIntersecting.Guid = generateBadumnaId(new PeerAddress(HashKey.Random()));

            this.extraRegionIntersecting = new ImsRegion(this.otherRegionIntersecting);
            this.extraRegionIntersecting.Guid = generateBadumnaId(new PeerAddress(HashKey.Random()));

            Assert.AreEqual(this.originalRegion.Guid, this.newRegionIntersecting.Guid);
            Assert.AreEqual(this.originalRegion.Guid, this.newRegionNonIntersecting.Guid);
            Assert.AreNotEqual(this.originalRegion.Guid, this.otherRegionIntersecting.Guid);
            Assert.AreNotEqual(this.originalRegion.Guid, this.otherRegionNonIntersecting.Guid);
            Assert.AreNotEqual(this.originalRegion.Guid, this.extraRegionIntersecting.Guid);

            Assert.IsTrue(this.originalRegion.IsIntersecting(this.newRegionIntersecting));
            Assert.IsFalse(this.originalRegion.IsIntersecting(this.newRegionNonIntersecting));
            Assert.IsTrue(this.originalRegion.IsIntersecting(this.otherRegionIntersecting));
            Assert.IsFalse(this.originalRegion.IsIntersecting(this.otherRegionNonIntersecting));
            Assert.IsTrue(this.originalRegion.IsIntersecting(this.extraRegionIntersecting));

            Assert.IsTrue(this.newRegionIntersecting.IsIntersecting(this.newRegionIntersecting));
            Assert.IsFalse(this.newRegionIntersecting.IsIntersecting(this.newRegionNonIntersecting));
            Assert.IsTrue(this.newRegionIntersecting.IsIntersecting(this.otherRegionIntersecting));
            Assert.IsFalse(this.newRegionIntersecting.IsIntersecting(this.otherRegionNonIntersecting));
            Assert.IsTrue(this.newRegionIntersecting.IsIntersecting(this.extraRegionIntersecting));

            Assert.IsFalse(this.newRegionNonIntersecting.IsIntersecting(this.newRegionIntersecting));
            Assert.IsTrue(this.newRegionNonIntersecting.IsIntersecting(this.newRegionNonIntersecting));
            Assert.IsFalse(this.newRegionNonIntersecting.IsIntersecting(this.otherRegionIntersecting));
            Assert.IsTrue(this.newRegionNonIntersecting.IsIntersecting(this.otherRegionNonIntersecting));
            Assert.IsFalse(this.newRegionNonIntersecting.IsIntersecting(this.extraRegionIntersecting));
        }

        [Test]
        public void IsEmptyReturnsTrueWhenFacadeHasNoReplicasForSceneKey()
        {
            this.facade.Stub(f => f.AccessReplicas<ImsRegion>(null))
                .IgnoreArguments()
                .Return(new List<ImsRegion>());
            Assert.IsTrue(this.store.IsEmpty);
        }

        [Test]
        public void IsEmptyReturnsFalseWhenFacadeHasSomeReplicasForSceneKey()
        {
            this.facade.Stub(f => f.AccessReplicas<ImsRegion>(null))
                .IgnoreArguments()
                .Return(new List<ImsRegion>() { new ImsRegion() });
            Assert.IsFalse(this.store.IsEmpty);
        }

        [TestCase(0)]
        [TestCase(1)]
        [TestCase(10)]
        public void ObjectCountReturnsCountOfReplicasFromFacade(int count)
        {
            List<ImsRegion> replicas = new List<ImsRegion>();
            for (int i = 0; i < count; i++)
            {
                replicas.Add(new ImsRegion());
            }

            this.facade.Stub(f => f.AccessReplicas<ImsRegion>(null))
                .IgnoreArguments()
                .Return(replicas);

            Assert.AreEqual(count, this.store.ObjectCount);
        }

        [Test]
        [ExpectedException(typeof(ImsInvalidRegionException))]
        public void AddOrModifyObjectThrowsForZeroRadiusRegion()
        {
            this.store.AddOrModifyObject(this.zeroRegion, this.ttl, this.knownCollisions);
        }

        [Test]
        public void AddOrModifyObjectResetsKeyFromFacadeCurrentEnvelope()
        {
            this.facade.Expect(f => f.CurrentEnvelope).Return(this.envelope);
            this.store.AddOrModifyObject(this.nonZeroRegion, this.ttl, this.knownCollisions);
            this.facade.VerifyAllExpectations();
        }

        [Test]
        public void AddOrModifyObjectFetchesReplicasWithNewSceneKey()
        {
            this.facade.Stub(f => f.CurrentEnvelope).Return(this.envelope);
            this.facade.Expect(f => f.AccessReplicas<ImsRegion>(this.key)).Return(null);
            this.store.AddOrModifyObject(this.nonZeroRegion, this.ttl, this.knownCollisions);
            this.facade.VerifyAllExpectations();
        }

        [Test]
        public void AddOrModifyObjectFetchesExistingRegionWithNewSceneKeyAndRegionId()
        {
            this.facade.Stub(f => f.CurrentEnvelope).Return(this.envelope);
            this.facade.Stub(f => f.AccessReplicas<ImsRegion>(this.key)).Return(null);
            this.facade.Expect(f => f.AccessReplica<ImsRegion>(this.key, this.nonZeroRegion.Guid))
                .Return(null);
            this.store.AddOrModifyObject(this.nonZeroRegion, this.ttl, this.knownCollisions);
            this.facade.VerifyAllExpectations();
        }

        [Test]
        public void AddOrModifyObjectNotifiesSeparatedRegions()
        {
            this.facade.Stub(f => f.CurrentEnvelope).Return(this.envelope);
            this.facade.Stub(f => f.AccessReplicas<ImsRegion>(this.key))
                .Return(new List<ImsRegion>() { this.otherRegionIntersecting });
            this.facade.Stub(f => f.AccessReplica<ImsRegion>(this.key, this.newRegionNonIntersecting.Guid))
                .Return(this.originalRegion);
            bool separationNotificationCalled = false;
            bool reciprocalSeparationNotificationCalled = false;
            this.separationNotificationHandler.Expect(h => h.Invoke(null, null))
                .IgnoreArguments()
                .Repeat.Twice()
                .WhenCalled(c =>
                    {
                        Assert.AreEqual(this.store, c.Arguments[0]);
                        IntersectionEventArgs e = (IntersectionEventArgs)c.Arguments[1];
                        if (e.Region == this.newRegionNonIntersecting)
                        {
                            Assert.AreEqual(this.otherRegionIntersecting, e.ChangedRegions[0]);
                            separationNotificationCalled = true;
                        }
                        else
                        {
                            Assert.AreEqual(e.Region, this.otherRegionIntersecting);
                            Assert.AreEqual(this.newRegionNonIntersecting, e.ChangedRegions[0]);
                            reciprocalSeparationNotificationCalled = true;
                        }
                    });
            this.store.AddOrModifyObject(this.newRegionNonIntersecting, this.ttl, 0);
            this.separationNotificationHandler.VerifyAllExpectations();
            Assert.IsTrue(separationNotificationCalled);
            Assert.IsTrue(reciprocalSeparationNotificationCalled);
        }

        [Test]
        public void AddOrModifyObjectNotifiesIntersectedRegions()
        {
            this.facade.Stub(f => f.CurrentEnvelope).Return(this.envelope);
            this.facade.Stub(f => f.AccessReplicas<ImsRegion>(this.key))
                .Return(new List<ImsRegion>() { this.otherRegionNonIntersecting });
            this.facade.Stub(f => f.AccessReplica<ImsRegion>(this.key, this.newRegionNonIntersecting.Guid))
                .Return(this.originalRegion);
            bool intersectionNotificationCalled = false;
            bool reciprocalIntersectionNotificationCalled = false;
            this.intersectionNotificationHandler.Expect(h => h.Invoke(null, null))
                .IgnoreArguments()
                .Repeat.Twice()
                .WhenCalled(c =>
                    {
                        Assert.AreEqual(this.store, c.Arguments[0]);
                        IntersectionEventArgs e = (IntersectionEventArgs)c.Arguments[1];
                        if (e.Region == this.newRegionNonIntersecting)
                        {
                            Assert.AreEqual(this.otherRegionNonIntersecting, e.ChangedRegions[0]);
                            intersectionNotificationCalled = true;
                        }
                        else
                        {
                            Assert.AreEqual(e.Region, this.otherRegionNonIntersecting);
                            Assert.AreEqual(this.newRegionNonIntersecting, e.ChangedRegions[0]);
                            reciprocalIntersectionNotificationCalled = true;
                        }
                    });
            this.store.AddOrModifyObject(this.newRegionNonIntersecting, this.ttl, 0);
            this.intersectionNotificationHandler.VerifyAllExpectations();
            Assert.IsTrue(intersectionNotificationCalled);
            Assert.IsTrue(reciprocalIntersectionNotificationCalled);
        }

        [Test]
        public void AddOrModifyObjectDoesNotNotifyOriginalRegionOfSeparations()
        {
            this.facade.Stub(f => f.CurrentEnvelope).Return(this.envelope);
            this.facade.Stub(f => f.AccessReplicas<ImsRegion>(this.key))
                .Return(new List<ImsRegion>() { this.originalRegion });
            this.facade.Stub(f => f.AccessReplica<ImsRegion>(this.key, this.newRegionNonIntersecting.Guid))
                .Return(this.originalRegion);
            this.separationNotificationHandler.Expect(h => h.Invoke(null, null))
                .IgnoreArguments()
                .Repeat.Never();
            this.store.AddOrModifyObject(this.newRegionNonIntersecting, this.ttl, 0);
            this.separationNotificationHandler.VerifyAllExpectations();
        }

        [Test]
        public void AddOrModifyObjectDoesNotNotifyOriginalRegionOfIntersections()
        {
            this.facade.Stub(f => f.CurrentEnvelope).Return(this.envelope);
            this.facade.Stub(f => f.AccessReplicas<ImsRegion>(this.key))
                .Return(new List<ImsRegion>() { this.originalRegion });
            this.facade.Stub(f => f.AccessReplica<ImsRegion>(this.key, this.newRegionNonIntersecting.Guid))
                .Return(this.originalRegion);
            this.separationNotificationHandler.Expect(h => h.Invoke(null, null))
                .IgnoreArguments()
                .Repeat.Never();
            this.store.AddOrModifyObject(this.newRegionIntersecting, this.ttl, 0);
            this.separationNotificationHandler.VerifyAllExpectations();
        }

        [Test]
        public void AddOrModifyObjectNotifiesAllExistingIntersectionsWhenUnknownIntersectionsFound()
        {
            this.facade.Stub(f => f.CurrentEnvelope).Return(this.envelope);
            this.facade.Stub(f => f.AccessReplicas<ImsRegion>(this.key))
                .Return(new List<ImsRegion>()
                    {
                        this.originalRegion,
                        this.otherRegionIntersecting,
                        this.extraRegionIntersecting
                    });
            this.facade.Stub(f => f.AccessReplica<ImsRegion>(this.key, this.newRegionNonIntersecting.Guid))
                .Return(this.originalRegion);
            this.intersectionNotificationHandler.Expect(h => h.Invoke(null, null))
                .IgnoreArguments()
                .WhenCalled(c =>
                {
                    Assert.AreEqual(this.store, c.Arguments[0]);
                    IntersectionEventArgs e = (IntersectionEventArgs)c.Arguments[1];
                    Assert.AreEqual(this.newRegionIntersecting, e.Region);
                    Assert.AreEqual(this.otherRegionIntersecting, e.ChangedRegions[0]);
                    Assert.AreEqual(this.extraRegionIntersecting, e.ChangedRegions[1]);
                });
            ////this.intersectionNotificationHandler.Expect(h => h.Invoke(null, null))
            ////    .IgnoreArguments()
            ////    .Throw(new InvalidOperationException("Only one notification should be sent."));
            this.store.AddOrModifyObject(this.newRegionIntersecting, this.ttl, 0);
            this.intersectionNotificationHandler.VerifyAllExpectations();
        }

        [Test]
        public void AddOrModifyObjectDoesNotNotifyAllExistingIntersectionsAllIntersectionsKnown()
        {
            this.facade.Stub(f => f.CurrentEnvelope).Return(this.envelope);
            this.facade.Stub(f => f.AccessReplicas<ImsRegion>(this.key))
                .Return(new List<ImsRegion>()
                    {
                        this.originalRegion,
                        this.otherRegionIntersecting,
                        this.extraRegionIntersecting
                    });
            this.facade.Stub(f => f.AccessReplica<ImsRegion>(this.key, this.newRegionNonIntersecting.Guid))
                .Return(this.originalRegion);
            this.intersectionNotificationHandler.Expect(h => h.Invoke(null, null))
                .IgnoreArguments()
                .Repeat.Never();
            this.store.AddOrModifyObject(this.newRegionIntersecting, this.ttl, 2);
            this.intersectionNotificationHandler.VerifyAllExpectations();
        }

        [Test]
        public void AddOrModifyObjectUpdatesReplicaViaFacade()
        {
            this.facade.Stub(f => f.CurrentEnvelope).Return(this.envelope);
            this.facade.Stub(f => f.AccessReplicas<ImsRegion>(this.key)).Return(null);
            this.facade.Stub(f => f.AccessReplica<ImsRegion>(this.key, this.newRegionIntersecting.Guid))
                .Return(this.originalRegion);
            this.facade.Expect(f => f.UpdateReplica(this.newRegionIntersecting, this.ttl));
            CyclicalID.UShortID modificationNumber = new CyclicalID.UShortID(77);
            this.originalRegion.ModificationNumber = modificationNumber;
            Assert.AreNotEqual(modificationNumber, this.newRegionIntersecting.ModificationNumber);
            Assert.AreNotEqual(this.key, this.newRegionIntersecting.Key);
            this.store.AddOrModifyObject(this.newRegionIntersecting, this.ttl, 0);
            this.facade.VerifyAllExpectations();
            Assert.AreEqual(modificationNumber, this.newRegionIntersecting.ModificationNumber);
            Assert.AreEqual(this.key, this.newRegionIntersecting.Key);
        }

        [Test, Ignore]
        public void AddOrModifyObjectsRespectsSceneNotificationRequirements()
        {
            // TODO: ImsRegion objects' notification requirements (i.e. as defined by 
            // ImsRegion.RequiresIntersectionNotification(ImsRegion) etc. only seem to be considered
            // for checking known collision count.
            // Shouldn't they be considered for all intersection and separation notifications?
        }

        [Test, Ignore]
        public void AddOrModifyObjectDoesNotDuplicateIntersectionNotifications()
        {
            // TODO: Test that new intersections aren't re-notified as part of existing intersection
            // notification.
        }

        [Test]
        public void KeepAliveDoesNotTryAndKeepUnknownObjectsAlive()
        {
            this.facade.Stub(f => f.AccessReplica<ImsRegion>(this.key, this.nonZeroRegion.Guid))
                .Return(null);
            this.facade.Expect(f => f.KeepReplicaAlive(null, 0)).IgnoreArguments().Repeat.Never();
            this.store.KeepAlive(this.nonZeroRegion.Guid, this.ttl);
            this.facade.VerifyAllExpectations();
        }

        [Test]
        public void KeepAliveTellsFacadeToKeepObjectAlive()
        {
            this.facade.Stub(f => f.AccessReplica<ImsRegion>(HashKey.Hash(this.sceneName.ToString()), this.nonZeroRegion.Guid))
                .Return(this.nonZeroRegion);
            this.facade.Expect(f => f.KeepReplicaAlive(this.nonZeroRegion, this.ttl));
            this.store.KeepAlive(this.nonZeroRegion.Guid, this.ttl);
            this.facade.VerifyAllExpectations();
        }

        [Test]
        public void RemoveObjectResetsSceneKeyWithKeyFromFacadesCurrentEnvelope()
        {
            this.facade.Expect(f => f.CurrentEnvelope).Return(this.envelope);
            this.store.RemoveObject(this.nonZeroRegion);
            this.facade.VerifyAllExpectations();
        }

        [Test]
        public void RemoveObjectFetchesRegionFromFacadeByKeyAndId()
        {
            this.facade.Stub(f => f.CurrentEnvelope).Return(this.envelope);
            this.facade.Expect(f => f.AccessReplica<ImsRegion>(this.key, this.nonZeroRegion.Guid))
                .Return(null);
            this.store.RemoveObject(this.nonZeroRegion);
            this.facade.VerifyAllExpectations();
        }

        [Test]
        public void RemoveObjectNotifiesSeparationsToIntersectingRegions()
        {
            this.facade.Stub(f => f.CurrentEnvelope).Return(this.envelope);
            this.facade.Stub(f => f.AccessReplicas<ImsRegion>(this.key))
                .Return(new List<ImsRegion>() { this.otherRegionIntersecting });
            this.facade.Stub(f => f.AccessReplica<ImsRegion>(this.key, this.originalRegion.Guid))
                .Return(this.originalRegion);
            bool separationNotificationCalled = false;
            bool reciprocalSeparationNotificationCalled = false;
            this.separationNotificationHandler.Expect(h => h.Invoke(null, null))
                .IgnoreArguments()
                .Repeat.Twice()
                .WhenCalled(c =>
                {
                    Assert.AreEqual(this.store, c.Arguments[0]);
                    IntersectionEventArgs e = (IntersectionEventArgs)c.Arguments[1];
                    if (e.Region == this.originalRegion)
                    {
                        Assert.AreEqual(this.otherRegionIntersecting, e.ChangedRegions[0]);
                        separationNotificationCalled = true;
                    }
                    else
                    {
                        Assert.AreEqual(e.Region, this.otherRegionIntersecting);
                        Assert.AreEqual(this.originalRegion, e.ChangedRegions[0]);
                        reciprocalSeparationNotificationCalled = true;
                    }
                });
            this.store.RemoveObject(this.originalRegion);
            this.separationNotificationHandler.VerifyAllExpectations();
            Assert.IsTrue(separationNotificationCalled);
            Assert.IsTrue(reciprocalSeparationNotificationCalled);
        }

        [Test]
        public void RemoveObjectDoesNotNotifySeparationsToNonIntersectingRegions()
        {
            this.facade.Stub(f => f.CurrentEnvelope).Return(this.envelope);
            this.facade.Stub(f => f.AccessReplicas<ImsRegion>(this.key))
                .Return(new List<ImsRegion>() { this.otherRegionNonIntersecting });
            this.facade.Stub(f => f.AccessReplica<ImsRegion>(this.key, this.originalRegion.Guid))
                .Return(this.originalRegion);
            this.separationNotificationHandler.Expect(h => h.Invoke(null, null))
                .IgnoreArguments()
                .Repeat.Never();
            this.store.RemoveObject(this.originalRegion);
            this.separationNotificationHandler.VerifyAllExpectations();
        }

        [Test]
        public void RemoveObjectDoesNotNotifySeparationsToOriginalRegion()
        {
            this.facade.Stub(f => f.CurrentEnvelope).Return(this.envelope);
            this.facade.Stub(f => f.AccessReplicas<ImsRegion>(this.key))
                .Return(new List<ImsRegion>() { this.newRegionIntersecting });
            this.facade.Stub(f => f.AccessReplica<ImsRegion>(this.key, this.originalRegion.Guid))
                .Return(this.originalRegion);
            this.separationNotificationHandler.Expect(h => h.Invoke(null, null))
                .IgnoreArguments()
                .Repeat.Never();
            this.store.RemoveObject(this.originalRegion);
            this.separationNotificationHandler.VerifyAllExpectations();
        }

        [Test, Ignore]
        public void RemoveObjectRespectsSceneNotificationRequirements()
        {
            // TODO: ImsRegion objects' notification requirements (i.e. as defined by 
            // ImsRegion.RequiresSeparationNotification(ImsRegion) is not considered.
            // Shouldn't they be considered for all separation notifications?
        }

        [Test]
        public void RemoveObjectRemovesObjectViaFacade()
        {
            this.facade.Stub(f => f.CurrentEnvelope).Return(this.envelope);
            this.facade.Stub(f => f.AccessReplicas<ImsRegion>(this.key))
                .Return(new List<ImsRegion>() { this.newRegionIntersecting });
            this.facade.Stub(f => f.AccessReplica<ImsRegion>(this.key, this.originalRegion.Guid))
                .Return(this.originalRegion);
            this.facade.Expect(f => f.RemoveReplica(this.originalRegion));
            this.store.RemoveObject(this.originalRegion);
            this.facade.VerifyAllExpectations();
        }

        [Test]
        public void RemoveWithoutNotificationResetsSceneKeyWithKeyFromFacadesCurrentEnvelope()
        {
            this.facade.Expect(f => f.CurrentEnvelope).Return(this.envelope);
            this.store.RemoveWithoutNotification(this.nonZeroRegion);
            this.facade.VerifyAllExpectations();
        }

        [Test]
        public void RemoveWithoutNotificationFetchesRegionFromFacadeByKeyAndId()
        {
            this.facade.Stub(f => f.CurrentEnvelope).Return(this.envelope);
            this.facade.Expect(f => f.AccessReplica<ImsRegion>(this.key, this.nonZeroRegion.Guid))
                .Return(null);
            this.store.RemoveWithoutNotification(this.nonZeroRegion);
            this.facade.VerifyAllExpectations();
        }

        [Test]
        public void RemoveObjectDoesNotNotifyAnySeparations()
        {
            this.facade.Stub(f => f.CurrentEnvelope).Return(this.envelope);
            this.facade.Stub(f => f.AccessReplicas<ImsRegion>(this.key))
                .Return(new List<ImsRegion>()
                    {
                        this.newRegionIntersecting,
                        this.newRegionNonIntersecting,
                        this.otherRegionNonIntersecting,
                        this.otherRegionIntersecting
                    });
            this.facade.Stub(f => f.AccessReplica<ImsRegion>(this.key, this.originalRegion.Guid))
                .Return(this.originalRegion);
            this.separationNotificationHandler.Expect(h => h.Invoke(null, null))
                .IgnoreArguments()
                .Repeat.Never();
            this.store.RemoveWithoutNotification(this.originalRegion);
            this.separationNotificationHandler.VerifyAllExpectations();
        }

        [Test]
        public void RemoveWithoutNotificationRemovesObjectViaFacade()
        {
            this.facade.Stub(f => f.CurrentEnvelope).Return(this.envelope);
            this.facade.Stub(f => f.AccessReplicas<ImsRegion>(this.key))
                .Return(new List<ImsRegion>() { this.newRegionIntersecting });
            this.facade.Stub(f => f.AccessReplica<ImsRegion>(this.key, this.originalRegion.Guid))
                .Return(this.originalRegion);
            this.facade.Expect(f => f.RemoveReplica(this.originalRegion));
            this.store.RemoveWithoutNotification(this.originalRegion);
            this.facade.VerifyAllExpectations();
        }

        [Test]
        [ExpectedException(typeof(ImsInvalidRegionException))]
        public void GetObjectsWithinRegionThrowsForZeroRadiusRegion()
        {
            this.store.GetObjectsWithinRegion(this.zeroRegion);
        }

        [Test]
        public void GetObjectsWithinRegionReturnsOnlyIntersectingRegions()
        {
            this.facade.Stub(f => f.AccessReplicas<ImsRegion>(null))
                .IgnoreArguments()
                .Return(new List<ImsRegion>()
                    {
                        this.otherRegionIntersecting,
                        this.otherRegionNonIntersecting
                    });
            IList<ImsRegion> intersectingRegions = this.store.GetObjectsWithinRegion(this.originalRegion);
            Assert.IsTrue(intersectingRegions.Contains(this.otherRegionIntersecting));
            Assert.IsFalse(intersectingRegions.Contains(this.otherRegionNonIntersecting));
        }

        [Test]
        public void GetObjectsWithinRegionIncludesOriginalRegion()
        {
            // TODO: Is this intentional, or should it be excluded?
            this.facade.Stub(f => f.AccessReplicas<ImsRegion>(null))
                .IgnoreArguments()
                .Return(new List<ImsRegion>()
                    {
                        this.newRegionIntersecting,
                        this.newRegionNonIntersecting
                    });
            IList<ImsRegion> intersectingRegions = this.store.GetObjectsWithinRegion(this.originalRegion);
            Assert.IsTrue(intersectingRegions.Contains(this.newRegionIntersecting));
            Assert.IsFalse(intersectingRegions.Contains(this.newRegionNonIntersecting));
        }
    }
}
