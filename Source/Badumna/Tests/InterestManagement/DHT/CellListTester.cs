﻿using System.Collections.Generic;

using Badumna.Core;
using Badumna.DataTypes;
using Badumna.InterestManagement;
using Badumna.Utilities;

using NUnit.Framework;
using BadumnaTests.Utilities;

namespace BadumnaTests.InterestManagement.DHT
{
    [TestFixture]
    public class CellListTester
    {
        private ImsRegion mQueryRegion;
        static readonly QualifiedName sceneName = new QualifiedName("", "CellListTester");
        private GenericCallBackReturn<BadumnaId> generateBadumnaId;

        public CellListTester()
        {
            this.generateBadumnaId = BadumnaIdGenerator.Local();
        }

        private void SetQueryRegion(float x, float y, float z, float radius)
        {
            this.mQueryRegion = new ImsRegion(new Vector3(x, y, z), radius, this.generateBadumnaId(), RegionType.Source, sceneName);
        }

        private void AssertEquivalent(ICollection<Vector3> expectedList, ICollection<Vector3> actualList)
        {
            Assert.AreEqual(expectedList.Count, actualList.Count);

            foreach (Vector3 expectedVector in expectedList)
            {
                Assert.IsTrue(actualList.Contains(expectedVector), "Failed to find {0} in actual list", expectedVector);
            }

            foreach (Vector3 actualVector in actualList)
            {
                Assert.IsTrue(expectedList.Contains(actualVector), "Failed to find {0} in expected list", actualVector);
            }
        }

        [Test]
        public void LessThanOneCellTest()
        {
            this.SetQueryRegion(0, 0, 0, 4);
            Vector3 cellSpan = new Vector3(10, 10, 10);

            ICollection<Vector3> expectedCellList = new List<Vector3>();
            ICollection<Vector3> actualList = CellList.IntersectingCells(this.mQueryRegion, cellSpan);

            foreach (Vector3 cellCenter in actualList)
            {
                //System.Console.WriteLine("{0}", cellCenter.ToString());
                CellList.IsIntersectingWithCell(cellCenter, this.mQueryRegion, cellSpan);
            }

            expectedCellList.Add(new Vector3(0, 0, 0));
            this.AssertEquivalent(expectedCellList, actualList);
        }

        [Test]
        public void EqualOneCellTest()
        {
            this.SetQueryRegion(0, 0, 0, 5);
            Vector3 cellSpan = new Vector3(10, 10, 10);

            ICollection<Vector3> expectedCellList = new List<Vector3>();
            ICollection<Vector3> actualList = CellList.IntersectingCells(this.mQueryRegion, cellSpan);

            foreach (Vector3 cellCenter in actualList)
            {
                //System.Console.WriteLine("{0}", cellCenter.ToString());
                Assert.IsTrue(CellList.IsIntersectingWithCell(cellCenter, this.mQueryRegion, cellSpan),
                    "Region {0}, {1} doesn't intersect with cell {2}", this.mQueryRegion.Centroid, this.mQueryRegion.Radius, cellCenter);
            }

            expectedCellList.Add(new Vector3(0, 0, 0));
            this.AssertEquivalent(expectedCellList, actualList);
        }

        [Test]
        public void GreaterThanOneCellTest()
        {
            this.SetQueryRegion(0, 0, 0, 5.001f);
            Vector3 cellSpan = new Vector3(10, 10, 10);

            ICollection<Vector3> expectedCellList = new List<Vector3>();
            ICollection<Vector3> actualList = CellList.IntersectingCells(this.mQueryRegion, cellSpan);

            foreach (Vector3 cellCenter in actualList)
            {
                //System.Console.WriteLine("{0}", cellCenter.ToString());
                Assert.IsTrue(CellList.IsIntersectingWithCell(cellCenter, this.mQueryRegion, cellSpan),
                    "Region {0}, {1} doesn't intersect with cell {2}", this.mQueryRegion.Centroid, this.mQueryRegion.Radius, cellCenter);
            }

            expectedCellList.Add(new Vector3(0, 0, 0));
            expectedCellList.Add(new Vector3(-10, 0, 0));
            expectedCellList.Add(new Vector3(10, 0, 0));
            expectedCellList.Add(new Vector3(0, -10, 0));
            expectedCellList.Add(new Vector3(0, 10, 0));
            expectedCellList.Add(new Vector3(0, 0, -10));
            expectedCellList.Add(new Vector3(0, 0, 10));

            this.AssertEquivalent(expectedCellList, actualList);
        }

        [Test]
        public void OriginTest()
        {
            this.SetQueryRegion(-15, 0, 0, 5.001f);
            Vector3 cellSpan = new Vector3(10, 10, 10);

            ICollection<Vector3> expectedCellList = new List<Vector3>();
            ICollection<Vector3> actualList = CellList.IntersectingCells(this.mQueryRegion, cellSpan);

            foreach (Vector3 cellCenter in actualList)
            {
                //System.Console.WriteLine("{0}", cellCenter.ToString());
                Assert.IsTrue(CellList.IsIntersectingWithCell(cellCenter, this.mQueryRegion, cellSpan),
                    "Region {0}, {1} doesn't intersect with cell {2}", this.mQueryRegion.Centroid, this.mQueryRegion.Radius, cellCenter);
            }

            expectedCellList.Add(new Vector3(-10, 0, 0));
            expectedCellList.Add(new Vector3(-20, 0, 0));
            expectedCellList.Add(new Vector3(-10, -10, 0));
            expectedCellList.Add(new Vector3(-10, 10, 0));
            expectedCellList.Add(new Vector3(-10, 0, -10));
            expectedCellList.Add(new Vector3(-10, 0, 10));
            expectedCellList.Add(new Vector3(-20, -10, 0));
            expectedCellList.Add(new Vector3(-20, 10, 0));
            expectedCellList.Add(new Vector3(-20, 0, -10));
            expectedCellList.Add(new Vector3(-20, 0, 10));

            this.AssertEquivalent(expectedCellList, actualList);
        }


        [Test]
        public void TwoDTest()
        {
            this.SetQueryRegion(-15, 0, 0, 5.001f);
            Vector3 cellSpan = new Vector3(10, 10, 1000);

            ICollection<Vector3> expectedCellList = new List<Vector3>();
            ICollection<Vector3> actualList = CellList.IntersectingCells(this.mQueryRegion, cellSpan);

            foreach (Vector3 cellCenter in actualList)
            {
                //System.Console.WriteLine("{0}", cellCenter.ToString());
                Assert.IsTrue(CellList.IsIntersectingWithCell(cellCenter, this.mQueryRegion, cellSpan),
                    "Region {0}, {1} doesn't intersect with cell {2}", this.mQueryRegion.Centroid, this.mQueryRegion.Radius, cellCenter);
            }

            expectedCellList.Add(new Vector3(-10, 0, 0));
            expectedCellList.Add(new Vector3(-20, 0, 0));
            expectedCellList.Add(new Vector3(-10, -10, 0));
            expectedCellList.Add(new Vector3(-10, 10, 0));
            expectedCellList.Add(new Vector3(-20, -10, 0));
            expectedCellList.Add(new Vector3(-20, 10, 0));

            this.AssertEquivalent(expectedCellList, actualList);
        }
    }
}
