﻿//-----------------------------------------------------------------------
// <copyright file="DistributedImServerTests.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2010 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BadumnaTests.InterestManagement.DHT
{
    using System;
    using Badumna.Core;
    using Badumna.DataTypes;
    using Badumna.DistributedHashTable;
    using Badumna.InterestManagement;
    using Badumna.Utilities;
    using BadumnaTests.Core;
    using NUnit.Framework;
    using Rhino.Mocks;

    [TestFixture]
    public class DistributedImServerTests
    {
        private IDhtFacade facade;
        private IDhtProtocol protocol;
        private IInterestManagementClientProxy clientProxy;
        private INetworkEventScheduler eventQueue;
        private MessageParser messageParser;
        private DistributedImServer server;
        private QualifiedName sceneName;
        private ImsRegion region;
        private uint ttl;
        private EventHandler<ExpireEventArgs> expireHandler;
        private INetworkConnectivityReporter connectivityReporter;

        [SetUp]
        public void SetUp()
        {
            this.facade = MockRepository.GenerateMock<IDhtFacade>();
            this.facade.Stub(f => f.CurrentEnvelope).Return(new DhtEnvelope());
            this.facade.Stub(f => f.RegisterTypeForReplication<ImsRegion>(null))
                .IgnoreArguments()
                .WhenCalled(call =>
                    {
                        this.expireHandler = (EventHandler<ExpireEventArgs>)call.Arguments[0];
                    });
            this.protocol = MockRepository.GenerateMock<IDhtProtocol>();
            this.clientProxy = MockRepository.GenerateMock<IInterestManagementClientProxy>();
            this.eventQueue = MockRepository.GenerateMock<INetworkEventScheduler>();
            this.messageParser = MockRepository.GenerateMock<MessageParser>(
                Guid.NewGuid().ToString(),
                typeof(DummyProtocolMethodAttribute),
                MockRepository.GenerateMock<GenericCallBackReturn<object, Type>>());
            this.protocol.Stub(p => p.Parser).Return(this.messageParser);
            this.connectivityReporter = MockRepository.GenerateMock<INetworkConnectivityReporter>();
            this.server = new DistributedImServer(this.facade, this.protocol, this.clientProxy, this.eventQueue, this.connectivityReporter);
            this.sceneName = new QualifiedName("", Guid.NewGuid().ToString());
            this.region = new ImsRegion(new Vector3(0f, 0f, 0f), 1f, new BadumnaId(PeerAddress.GetLoopback(1), 1), RegionType.None, this.sceneName);
            this.ttl = (uint)new Random().Next(1, int.MaxValue);
        }

        [TearDown]
        public void TearDown()
        {
        }

        [Test]
        public void ConstructorRegistersImsRegionForReplicationWithFacade()
        {
            // new facade required without stubbed RegisterTypeForReplication, to allow expectation.
            IDhtFacade facade = MockRepository.GenerateMock<IDhtFacade>();
            facade.Expect(f => f.RegisterTypeForReplication<ImsRegion>(null)).IgnoreArguments();

            DistributedImServer server = new DistributedImServer(
                facade,
                this.protocol,
                this.clientProxy,
                this.eventQueue,
                this.connectivityReporter);

            this.facade.VerifyAllExpectations();
        }

        [Test]
        public void InsertRequestRespondedTo()
        {
            this.clientProxy.Expect(p => p.StatusResponse(null, null))
                .IgnoreArguments()
                .WhenCalled(call =>
                    {
                        Assert.AreEqual(this.region.Guid, (BadumnaId)call.Arguments[0]);
                        Assert.AreEqual(RequestStatus.Statuses.Success, ((RequestStatus)call.Arguments[1]).Status);
                    });
            this.server.InsertRequest(this.sceneName, this.region, this.ttl);
        }

        [Test]
        public void RemoveRequestRespondedTo()
        {
            this.clientProxy.Expect(p => p.StatusResponse(null, null))
                .IgnoreArguments()
                .WhenCalled(call =>
                {
                    Assert.AreEqual(this.region.Guid, (BadumnaId)call.Arguments[0]);
                    Assert.AreEqual(RequestStatus.Statuses.Success, ((RequestStatus)call.Arguments[1]).Status);
                });
            this.server.RemoveRequest(this.sceneName, this.region);
        }

        [Test]
        public void RemoveWithoutNotificationRequestRespondedTo()
        {
            this.clientProxy.Expect(p => p.StatusResponse(null, null))
                .IgnoreArguments()
                .WhenCalled(call =>
                {
                    Assert.AreEqual(this.region.Guid, (BadumnaId)call.Arguments[0]);
                    Assert.AreEqual(RequestStatus.Statuses.Success, ((RequestStatus)call.Arguments[1]).Status);
                });
            this.server.RemoveWithoutNotificationRequest(this.sceneName, this.region);
        }

        [Test]
        public void ModifyRequestRespondedTo()
        {
            this.clientProxy.Expect(p => p.StatusResponse(null, null))
                .IgnoreArguments()
                .WhenCalled(call =>
                {
                    Assert.AreEqual(this.region.Guid, (BadumnaId)call.Arguments[0]);
                    Assert.AreEqual(RequestStatus.Statuses.Success, ((RequestStatus)call.Arguments[1]).Status);
                });
            this.server.ModifyRequest(this.sceneName, this.region, this.ttl, 0);
        }
    }
}
