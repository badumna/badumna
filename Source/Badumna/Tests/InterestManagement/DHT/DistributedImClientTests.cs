﻿//-----------------------------------------------------------------------
// <copyright file="DistributedImClientTests.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2010 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BadumnaTests.InterestManagement.DHT
{
    using System;
    using System.Collections.Generic;
    using Badumna.Core;
    using Badumna.DataTypes;
    using Badumna.DistributedHashTable;
    using Badumna.InterestManagement;
    using BadumnaTests.Core;
    using NUnit.Framework;
    using Rhino.Mocks;

    [TestFixture]
    public class DistributedImClientTests
    {
        private DistributedImClient client;
        private DhtProtocol protocol;

        [SetUp]
        public void SetUp()
        {
            this.protocol = new DhtProtocol("dummy", MessageParserTester.DefaultFactory);
            this.client = new DistributedImClient(this.protocol);
        }

        [TearDown]
        public void TearDown()
        {
        }

        #region Tests

        [Test]
        public void StatusResponseNotifiesSubscribersToStatusNotifications()
        {
            BadumnaId id = new BadumnaId(PeerAddress.Nowhere, 99);
            RequestStatus status = new RequestStatus();
            EventHandler<ImsStatusEventArgs> statusHandler = MockRepository.GenerateMock<EventHandler<ImsStatusEventArgs>>();
            this.client.StatusNotification += statusHandler;
            statusHandler.Expect(h => h.Invoke(null, null))
                .IgnoreArguments()
                .WhenCalled(call =>
                {
                    InterestManagementClient sender = (InterestManagementClient)call.Arguments[0];
                    ImsStatusEventArgs args = (ImsStatusEventArgs)call.Arguments[1];
                    Assert.AreEqual(this.client, sender);
                    Assert.AreEqual(id, args.Id);
                    Assert.AreEqual(status, args.Status);
                });
            this.client.StatusResponse(id, status);
            statusHandler.VerifyAllExpectations();
        }

        [Test]
        public void IntersectionNotificationNotifiesSubscribersToResultNotifications()
        {
            BadumnaId id = new BadumnaId(PeerAddress.Nowhere, 99);
            BadumnaId otherId = new BadumnaId(PeerAddress.Nowhere, 88);
            ImsEventArgs imsEventArgs = new ImsEventArgs(otherId, RegionType.None);
            EventHandler<ImsStatusEventArgs> resultHandler = MockRepository.GenerateMock<EventHandler<ImsStatusEventArgs>>();
            this.client.ResultNotification += resultHandler;
            resultHandler.Expect(h => h.Invoke(null, null))
                .IgnoreArguments()
                .WhenCalled(call =>
                {
                    InterestManagementClient sender = (InterestManagementClient)call.Arguments[0];
                    ImsStatusEventArgs args = (ImsStatusEventArgs)call.Arguments[1];
                    Assert.AreEqual(this.client, sender);
                    Assert.AreEqual(id, args.Id);
                    Assert.AreEqual(imsEventArgs, args.EnterList[0]);
                });
            this.client.IntersectionNotification(id, new List<ImsEventArgs>() { imsEventArgs });
            resultHandler.VerifyAllExpectations();
        }

        #endregion Tests
    }
}
