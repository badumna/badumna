﻿//-----------------------------------------------------------------------
// <copyright file="DistributedImServiceTests.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2010 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BadumnaTests.InterestManagement.DHT
{
    using System;
    using System.Collections.Generic;
    using Badumna.Core;
    using Badumna.DataTypes;
    using Badumna.InterestManagement;
    using Badumna.Utilities;
    using NUnit.Framework;
    using Rhino.Mocks;
    using Constraints = Rhino.Mocks.Constraints;

    [TestFixture]
    public class DistributedImServiceTests
    {
        private QualifiedName sceneName;
        private ImsRegion regionWithZeroRadius;
        private ImsRegion regionWithNonZeroRadius;
        private uint ttl = 7;
        private byte knownCollisionCount = 77;
        private Vector3 cellSpan = new Vector3(500f, 500f, 500f);
        private IInterestManagementClient client;
        private IDistributedImServerProxy serverProxy;
        private ITime timeKeeper;
        private SubscriptionDataFactory subscriptionDataFactory;
        private DistributedImService service;
        private EventHandler<ImsStatusEventArgs> statusHandler;
        private EventHandler<ImsStatusEventArgs> resultHandler;

        [SetUp]
        public void SetUp()
        {
            this.sceneName = new QualifiedName("", Guid.NewGuid().ToString());
            this.regionWithZeroRadius = new ImsRegion(
                new Vector3(0f, 0f, 0f),
                0f,
                new BadumnaId(PeerAddress.GetLoopback(1), 1),
                RegionType.None,
                this.sceneName);
            this.regionWithNonZeroRadius = new ImsRegion(
                new Vector3(0f, 0f, 0f),
                1f,
                new BadumnaId(PeerAddress.GetLoopback(1), 2),
                RegionType.None,
                this.sceneName);
            this.client = MockRepository.GenerateMock<IInterestManagementClient>();
            this.client.Stub(c => c.StatusNotification += null)
                .IgnoreArguments()
                .WhenCalled(call =>
                    {
                        this.statusHandler = (EventHandler<ImsStatusEventArgs>)call.Arguments[0];
                    });
            this.client.Stub(c => c.ResultNotification += null)
                .IgnoreArguments()
                .WhenCalled(call =>
                    {
                        this.resultHandler = (EventHandler<ImsStatusEventArgs>)call.Arguments[0];
                    });
            this.serverProxy = MockRepository.GenerateMock<IDistributedImServerProxy>();
            this.timeKeeper = MockRepository.GenerateMock<ITime>();
            this.subscriptionDataFactory = MockRepository.GenerateMock<SubscriptionDataFactory>();
            this.service = new DistributedImService(
                this.client,
                this.serverProxy,
                this.timeKeeper,
                this.subscriptionDataFactory);
        }

        [Test, Ignore]
        public void ConstructorSubscribesToClientEvents()
        {
            // TO DO. Need to refactor subscriptions into common code for each constructor
            // to enable meaningful tests.
        }

        [Test, Ignore]
        public void DisposeUnsubscribesFromClientEvents()
        {
            // TO DO. Should DistributedImService implement IDisposable to allow event unsubscription?
            // Low priority bug as it shouldn't ever manifest a problem.
        }

        [Test]
        public void ClientReturnsClient()
        {
            Assert.AreEqual(this.client, this.service.Client);
        }

        [Test]
        [ExpectedException(typeof(ImsInvalidRegionException))]
        public void InsertRegionThrowsForNullRegion()
        {
            this.service.InsertRegion(default(QualifiedName), null, 0, null, null);
        }

        [Test]
        [ExpectedException(typeof(ImsInvalidRegionException))]
        public void InsertRegionThrowsForRegionWithZeroRadius()
        {
            this.service.InsertRegion(this.sceneName, this.regionWithZeroRadius, 0, null, null);
        }

        [Test]
        [ExpectedException(typeof(ImsInvalidOperationException))]
        public void InsertRegionThrowsForAlreadyInsertedRegion()
        {
            this.service.InsertRegion(this.sceneName, this.regionWithNonZeroRadius, 0, null, null);
            this.service.InsertRegion(this.sceneName, this.regionWithNonZeroRadius, 0, null, null);
        }

        [Test]
        public void InsertRegionPassesCorrectArgumentsToSubscriptionDataFactory()
        {
            EventHandler<ImsEventArgs> enterNotificationHandler = new EventHandler<ImsEventArgs>((o, e) => { });
            EventHandler<ImsStatusEventArgs> statusHandler = new EventHandler<ImsStatusEventArgs>((o, e) => { });
            this.subscriptionDataFactory.Expect(s => s.Invoke(
                this.sceneName,
                this.regionWithNonZeroRadius,
                this.ttl,
                enterNotificationHandler,
                statusHandler,
                this.timeKeeper));
            this.service.InsertRegion(this.sceneName, this.regionWithNonZeroRadius, this.ttl, enterNotificationHandler, statusHandler);
            this.subscriptionDataFactory.VerifyAllExpectations();
        }

        [Test]
        public void InsertRegionSetsServerProxyDestinationKeyCorrectly()
        {
            this.regionWithNonZeroRadius.Radius = 600;
            string cellName = null;
            foreach (Vector3 cell in CellList.IntersectingCells(this.regionWithNonZeroRadius, this.cellSpan))
            {
                cellName = cell.ToString();
            }

            this.serverProxy.Expect(p => p.DestinationKeyString = this.sceneName + cellName);

            this.service.InsertRegion(
                this.sceneName,
                this.regionWithNonZeroRadius,
                this.ttl,
                null,
                null);
            this.serverProxy.VerifyAllExpectations();
        }

        [Test]
        public void InsertRegionMakesInsertRequestToServerProxy()
        {
            string cellName = null;
            foreach (Vector3 cell in CellList.IntersectingCells(this.regionWithNonZeroRadius, this.cellSpan))
            {
                cellName = cell.ToString();
            }

            this.serverProxy.Expect(
                p => p.InsertRequest(
                    new QualifiedName(this.sceneName.Qualifier, this.sceneName.Name + cellName),
                    this.regionWithNonZeroRadius,
                    this.ttl));
            this.service.InsertRegion(
                this.sceneName,
                this.regionWithNonZeroRadius,
                this.ttl,
                null,
                null);
            this.serverProxy.VerifyAllExpectations();
        }

        [Test]
        public void InsertRegionMakesInsertRequestForEachIntersectingCell()
        {
            int expectedIntersections = 27;
            this.regionWithNonZeroRadius.Radius = 600;
            Assert.AreEqual(
                expectedIntersections,
                CellList.IntersectingCells(this.regionWithNonZeroRadius, this.cellSpan).Count);
            this.serverProxy.Expect(p => p.DestinationKeyString = null)
                .IgnoreArguments()
                .Repeat.Times(expectedIntersections);
            this.serverProxy.Expect(p => p.InsertRequest(default(QualifiedName), null, 0))
                .IgnoreArguments()
                .Repeat.Times(expectedIntersections);
            this.service.InsertRegion(
                this.sceneName,
                this.regionWithNonZeroRadius,
                this.ttl,
                null,
                null);
            this.serverProxy.VerifyAllExpectations();
        }

        [Test]
        [ExpectedException(typeof(ImsInvalidRegionException))]
        public void ModifyRegionThrowsForNullRegion()
        {
            this.service.ModifyRegion(default(QualifiedName), null, 0, 0);
        }

        [Test]
        [ExpectedException(typeof(ImsInvalidRegionException))]
        public void ModifyRegionThrowsForRegionWithZeroRadius()
        {
            this.service.ModifyRegion(this.sceneName, this.regionWithZeroRadius, 0, 0);
        }

        [Test]
        [ExpectedException(typeof(ImsInvalidOperationException))]
        public void ModifyRegionThrowsForNonInsertedRegion()
        {
            this.service.ModifyRegion(this.sceneName, this.regionWithNonZeroRadius, 0, 0);
        }

        [Test, Ignore]
        public void ModifyRegionSetsServerProxyDestinationKeyCorrectly()
        {
            // TO DO: It does not. Should it?
        }

        [Test]
        public void ModifyRegionMakesModifyRequestsToCellServersRegionContinuesToIntersect()
        {
            this.regionWithNonZeroRadius.Radius = 600;
            this.subscriptionDataFactory.Stub(f => f.Invoke(
                this.sceneName, this.regionWithNonZeroRadius, this.ttl, null, null, this.timeKeeper))
                .Return(new SubscriptionData(
                    this.sceneName,
                    this.regionWithNonZeroRadius,
                    this.ttl,
                    null,
                    null,
                    this.timeKeeper));

            int expectedIntersections = 27;
            Assert.AreEqual(
                expectedIntersections,
                CellList.IntersectingCells(this.regionWithNonZeroRadius, this.cellSpan).Count);
            this.service.InsertRegion(this.sceneName, this.regionWithNonZeroRadius, this.ttl, null, null);
            this.serverProxy.Expect(sp => sp.ModifyRequest(default(QualifiedName), null, 0, 0))
                .IgnoreArguments()
                .Constraints(
                    Constraints.Is.Matching<QualifiedName>(x => x.Name.StartsWith(this.sceneName.Name)),
                    Constraints.Is.Equal(this.regionWithNonZeroRadius),
                    Constraints.Is.Equal(this.ttl),
                    Constraints.Is.Equal(this.knownCollisionCount))
                .Repeat.Times(expectedIntersections);
            this.service.ModifyRegion(this.sceneName, this.regionWithNonZeroRadius, this.ttl, this.knownCollisionCount);
            this.serverProxy.VerifyAllExpectations();
        }

        [Test]
        public void ModifyRegionMakesRemoveWithoutNotificationRequestsToCellServersRegionStopsIntersecting()
        {
            this.regionWithNonZeroRadius.Radius = 600;
            this.subscriptionDataFactory.Stub(f => f.Invoke(
                this.sceneName, this.regionWithNonZeroRadius, this.ttl, null, null, this.timeKeeper))
                .Return(new SubscriptionData(
                    this.sceneName,
                    this.regionWithNonZeroRadius,
                    this.ttl,
                    null,
                    null,
                    this.timeKeeper));
            int expectedIntersections = 27;
            ICollection<Vector3> oldIntersectingCells = CellList.IntersectingCells(this.regionWithNonZeroRadius, this.cellSpan);
            Assert.AreEqual(expectedIntersections, oldIntersectingCells.Count);
            this.service.InsertRegion(this.sceneName, this.regionWithNonZeroRadius, this.ttl, null, null);
            this.regionWithNonZeroRadius.Centroid = new Vector3(2000f, 2000f, 2000f);
            ICollection<Vector3> newIntersectingCells = CellList.IntersectingCells(this.regionWithNonZeroRadius, this.cellSpan);
            Assert.AreEqual(expectedIntersections, newIntersectingCells.Count);
            foreach (Vector3 cell in newIntersectingCells)
            {
                Assert.IsFalse(oldIntersectingCells.Contains(cell));
            }

            this.serverProxy.Expect(sp => sp.RemoveWithoutNotificationRequest(default(QualifiedName), null))
                .IgnoreArguments()
                .Constraints(
                    Constraints.Is.Matching<QualifiedName>(x => x.Name.StartsWith(this.sceneName.Name)),
                    Constraints.Is.Equal(this.regionWithNonZeroRadius))
                .Repeat.Times(expectedIntersections);
            this.service.ModifyRegion(this.sceneName, this.regionWithNonZeroRadius, this.ttl, this.knownCollisionCount);
            this.serverProxy.VerifyAllExpectations();
        }

        [Test]
        public void ModifyRegionMakesInsertRequestsToNewlyIntersectedCellServers()
        {
            this.regionWithNonZeroRadius.Radius = 600;
            this.subscriptionDataFactory.Stub(f => f.Invoke(
                this.sceneName, this.regionWithNonZeroRadius, this.ttl, null, null, this.timeKeeper))
                .Return(new SubscriptionData(
                    this.sceneName,
                    this.regionWithNonZeroRadius,
                    this.ttl,
                    null,
                    null,
                    this.timeKeeper));
            int expectedIntersections = 27;
            ICollection<Vector3> oldIntersectingCells = CellList.IntersectingCells(this.regionWithNonZeroRadius, this.cellSpan);
            Assert.AreEqual(expectedIntersections, oldIntersectingCells.Count);
            this.service.InsertRegion(this.sceneName, this.regionWithNonZeroRadius, this.ttl, null, null);
            this.regionWithNonZeroRadius.Centroid = new Vector3(1000f, 1000f, 1000f);
            ICollection<Vector3> newIntersectingCells = CellList.IntersectingCells(this.regionWithNonZeroRadius, this.cellSpan);
            Assert.AreEqual(expectedIntersections, newIntersectingCells.Count);
            foreach (Vector3 cell in oldIntersectingCells)
            {
                if (newIntersectingCells.Contains(cell))
                {
                    newIntersectingCells.Remove(cell);
                }
            }

            int newExpectedIntersections = newIntersectingCells.Count;
            Assert.Greater(newIntersectingCells.Count, 0);
            Assert.Less(newExpectedIntersections, expectedIntersections);

            this.serverProxy.Expect(sp => sp.InsertRequest(default(QualifiedName), null, 0))
                .IgnoreArguments()
                .Constraints(
                    Constraints.Is.Matching<QualifiedName>(x => x.Name.StartsWith(this.sceneName.Name)),
                    Constraints.Is.Equal(this.regionWithNonZeroRadius),
                    Constraints.Is.Equal(this.ttl))
                .Repeat.Times(newExpectedIntersections);
            this.service.ModifyRegion(this.sceneName, this.regionWithNonZeroRadius, this.ttl, this.knownCollisionCount);
            this.serverProxy.VerifyAllExpectations();
        }

        [Test]
        public void ModifyRegionResetsSubscriptionDataExpirationTime()
        {
            ISubscriptionData subscriptionData = MockRepository.GenerateMock<ISubscriptionData>();
            ImsRegion storedRegion = new ImsRegion(this.regionWithNonZeroRadius);
            subscriptionData.Stub(sd => sd.Region).Return(storedRegion)
                .WhenCalled(c =>
                    {
                        ////Console.WriteLine("papier hier");
                    });
            this.subscriptionDataFactory.Stub(f => f.Invoke(
                this.sceneName, this.regionWithNonZeroRadius, this.ttl, null, null, null))
                .IgnoreArguments()
                .Return(subscriptionData);

            Vector3 newCentroid = new Vector3(1f, 1f, 1f);
            float newRadius = 2f;
            ImsRegion newRegion = new ImsRegion(this.regionWithNonZeroRadius);
            newRegion.Centroid = newCentroid;
            newRegion.Radius = newRadius;
            uint newTtl = 8;

            this.service.InsertRegion(this.sceneName, this.regionWithNonZeroRadius, this.ttl, null, null);

            Assert.AreNotEqual(storedRegion.Centroid, newCentroid);
            Assert.AreNotEqual(storedRegion.Radius, newRadius);
            subscriptionData.Expect(sd => sd.ResetExpirationTime(newTtl));

            this.service.ModifyRegion(this.sceneName, newRegion, newTtl, this.knownCollisionCount);

            Assert.AreEqual(storedRegion.Centroid, newCentroid);
            Assert.AreEqual(storedRegion.Radius, newRadius);
            this.serverProxy.VerifyAllExpectations();
        }

        ////[Test]
        ////[ExpectedException(typeof(ImsInvalidArgumentException))]
        ////public void RemoveRegionThrowsForNonInsertedRegion()
        ////{
        ////    this.service.RemoveRegion(this.sceneName, this.regionWithNonZeroRadius);
        ////}

        [Test]
        public void RemoveRegionIgnoresNonInsertedRegion()
        {
            // Test passes if no exception thrown.
            this.service.RemoveRegion(this.sceneName, this.regionWithNonZeroRadius);
        }

        [Test]
        public void RemoveRegionSetsProxyServerDestinationKeyString()
        {
            this.subscriptionDataFactory.Stub(f => f.Invoke(
                this.sceneName, this.regionWithNonZeroRadius, this.ttl, null, null, this.timeKeeper))
                .Return(new SubscriptionData(
                    this.sceneName,
                    this.regionWithNonZeroRadius,
                    this.ttl,
                    null,
                    null,
                    this.timeKeeper));
            this.service.InsertRegion(this.sceneName, this.regionWithNonZeroRadius, this.ttl, null, null);
            string cellName = null;
            foreach (Vector3 cell in CellList.IntersectingCells(this.regionWithNonZeroRadius, this.cellSpan))
            {
                cellName = cell.ToString();
            }

            this.serverProxy.Expect(sp => sp.DestinationKeyString = this.sceneName + cellName);
            this.service.RemoveRegion(this.sceneName, this.regionWithNonZeroRadius);
            this.serverProxy.VerifyAllExpectations();
        }

        [Test]
        public void RemoveRegionMakesRemoveRequestsWithCorrectParameters()
        {
            this.subscriptionDataFactory.Stub(f => f.Invoke(
                this.sceneName, this.regionWithNonZeroRadius, this.ttl, null, null, this.timeKeeper))
                .Return(new SubscriptionData(
                    this.sceneName,
                    this.regionWithNonZeroRadius,
                    this.ttl,
                    null,
                    null,
                    this.timeKeeper));
            this.service.InsertRegion(this.sceneName, this.regionWithNonZeroRadius, this.ttl, null, null);
            string cellName = null;
            foreach (Vector3 cell in CellList.IntersectingCells(this.regionWithNonZeroRadius, this.cellSpan))
            {
                cellName = cell.ToString();
            }

            this.serverProxy.Expect(sp => sp.RemoveRequest(new QualifiedName(this.sceneName.Qualifier, this.sceneName.Name + cellName), this.regionWithNonZeroRadius));
            this.service.RemoveRegion(this.sceneName, this.regionWithNonZeroRadius);
            this.serverProxy.VerifyAllExpectations();
        }

        [Test]
        public void RemoveRegionSetsProxyServerDestinationKeyStringForEachIntersectingCell()
        {
            this.regionWithNonZeroRadius.Radius = 600;
            this.subscriptionDataFactory.Stub(f => f.Invoke(
                this.sceneName, this.regionWithNonZeroRadius, this.ttl, null, null, this.timeKeeper))
                .Return(new SubscriptionData(
                    this.sceneName,
                    this.regionWithNonZeroRadius,
                    this.ttl,
                    null,
                    null,
                    this.timeKeeper));
            int expectedIntersections = 27;
            ICollection<Vector3> intersectingCells = CellList.IntersectingCells(this.regionWithNonZeroRadius, this.cellSpan);
            Assert.AreEqual(expectedIntersections, intersectingCells.Count);
            this.service.InsertRegion(this.sceneName, this.regionWithNonZeroRadius, this.ttl, null, null);
            this.serverProxy.Expect(sp => sp.DestinationKeyString = null)
                .IgnoreArguments()
                .Constraints(Constraints.Text.StartsWith(this.sceneName.ToString()))
                .Repeat.Times(expectedIntersections);
            this.service.RemoveRegion(this.sceneName, this.regionWithNonZeroRadius);
            this.serverProxy.VerifyAllExpectations();
        }

        [Test]
        public void RemoveRegionMakesRemoveRequestToProxyServerForEachIntersectingCell()
        {
            this.regionWithNonZeroRadius.Radius = 600;
            this.subscriptionDataFactory.Stub(f => f.Invoke(
                this.sceneName, this.regionWithNonZeroRadius, this.ttl, null, null, this.timeKeeper))
                .Return(new SubscriptionData(
                    this.sceneName,
                    this.regionWithNonZeroRadius,
                    this.ttl,
                    null,
                    null,
                    this.timeKeeper));
            int expectedIntersections = 27;
            ICollection<Vector3> intersectingCells = CellList.IntersectingCells(this.regionWithNonZeroRadius, this.cellSpan);
            Assert.AreEqual(expectedIntersections, intersectingCells.Count);
            this.service.InsertRegion(this.sceneName, this.regionWithNonZeroRadius, this.ttl, null, null);
            this.serverProxy.Expect(sp => sp.RemoveRequest(default(QualifiedName), null))
                .IgnoreArguments()
                .Constraints(
                    Constraints.Is.Matching<QualifiedName>(x => x.Name.StartsWith(this.sceneName.Name)),
                    Constraints.Is.Equal(this.regionWithNonZeroRadius))
                .Repeat.Times(expectedIntersections);
            this.service.RemoveRegion(this.sceneName, this.regionWithNonZeroRadius);
            this.serverProxy.VerifyAllExpectations();
        }

        ////[Test]
        ////[ExpectedException(typeof(ImsInvalidArgumentException))]
        ////public void RemoveRegionWithoutNotificationThrowsForNonInsertedRegion()
        ////{
        ////    this.service.RemoveRegionWithoutNotification(this.sceneName, this.regionWithNonZeroRadius);
        ////}

        [Test]
        public void RemoveRegionWithoutNotificationTIgnoresNonInsertedRegion()
        {
            // Test passes if no exception thrown.
            this.service.RemoveRegionWithoutNotification(this.sceneName, this.regionWithNonZeroRadius);
        }

        [Test]
        public void RemoveRegionWithoutNotificationSetsProxyServerDestinationKeyString()
        {
            this.subscriptionDataFactory.Stub(f => f.Invoke(
                this.sceneName, this.regionWithNonZeroRadius, this.ttl, null, null, this.timeKeeper))
                .Return(new SubscriptionData(
                    this.sceneName,
                    this.regionWithNonZeroRadius,
                    this.ttl,
                    null,
                    null,
                    this.timeKeeper));
            this.service.InsertRegion(this.sceneName, this.regionWithNonZeroRadius, this.ttl, null, null);
            string cellName = null;
            foreach (Vector3 cell in CellList.IntersectingCells(this.regionWithNonZeroRadius, this.cellSpan))
            {
                cellName = cell.ToString();
            }

            this.serverProxy.Expect(sp => sp.DestinationKeyString = this.sceneName + cellName);
            this.service.RemoveRegionWithoutNotification(this.sceneName, this.regionWithNonZeroRadius);
            this.serverProxy.VerifyAllExpectations();
        }

        [Test]
        public void RemoveRegionWithoutNotificationMakesRemoveRequestsWithCorrectParameters()
        {
            this.subscriptionDataFactory.Stub(f => f.Invoke(
                this.sceneName, this.regionWithNonZeroRadius, this.ttl, null, null, this.timeKeeper))
                .Return(new SubscriptionData(
                    this.sceneName,
                    this.regionWithNonZeroRadius,
                    this.ttl,
                    null,
                    null,
                    this.timeKeeper));
            this.service.InsertRegion(this.sceneName, this.regionWithNonZeroRadius, this.ttl, null, null);
            string cellName = null;
            foreach (Vector3 cell in CellList.IntersectingCells(this.regionWithNonZeroRadius, this.cellSpan))
            {
                cellName = cell.ToString();
            }

            this.serverProxy.Expect(sp => sp.RemoveWithoutNotificationRequest(new QualifiedName(this.sceneName.Qualifier, this.sceneName.Name + cellName), this.regionWithNonZeroRadius));
            this.service.RemoveRegionWithoutNotification(this.sceneName, this.regionWithNonZeroRadius);
            this.serverProxy.VerifyAllExpectations();
        }

        [Test]
        public void RemoveRegionWithoutNotificationSetsProxyServerDestinationKeyStringForEachIntersectingCell()
        {
            this.regionWithNonZeroRadius.Radius = 600;
            this.subscriptionDataFactory.Stub(f => f.Invoke(
                this.sceneName, this.regionWithNonZeroRadius, this.ttl, null, null, this.timeKeeper))
                .Return(new SubscriptionData(
                    this.sceneName,
                    this.regionWithNonZeroRadius,
                    this.ttl,
                    null,
                    null,
                    this.timeKeeper));
            int expectedIntersections = 27;
            ICollection<Vector3> intersectingCells = CellList.IntersectingCells(this.regionWithNonZeroRadius, this.cellSpan);
            Assert.AreEqual(expectedIntersections, intersectingCells.Count);
            this.service.InsertRegion(this.sceneName, this.regionWithNonZeroRadius, this.ttl, null, null);
            this.serverProxy.Expect(sp => sp.DestinationKeyString = null)
                .IgnoreArguments()
                .Constraints(Constraints.Text.StartsWith(this.sceneName.ToString()))
                .Repeat.Times(expectedIntersections);
            this.service.RemoveRegionWithoutNotification(this.sceneName, this.regionWithNonZeroRadius);
            this.serverProxy.VerifyAllExpectations();
        }

        [Test]
        public void RemoveRegionWithoutNotificationMakesRemoveRequestToProxyServerForEachIntersectingCell()
        {
            this.regionWithNonZeroRadius.Radius = 600;
            this.subscriptionDataFactory.Stub(f => f.Invoke(
                this.sceneName, this.regionWithNonZeroRadius, this.ttl, null, null, this.timeKeeper))
                .Return(new SubscriptionData(
                    this.sceneName,
                    this.regionWithNonZeroRadius,
                    this.ttl,
                    null,
                    null,
                    this.timeKeeper));
            int expectedIntersections = 27;
            ICollection<Vector3> intersectingCells = CellList.IntersectingCells(this.regionWithNonZeroRadius, this.cellSpan);
            Assert.AreEqual(expectedIntersections, intersectingCells.Count);
            this.service.InsertRegion(this.sceneName, this.regionWithNonZeroRadius, this.ttl, null, null);
            this.serverProxy.Expect(sp => sp.RemoveWithoutNotificationRequest(default(QualifiedName), null))
                .IgnoreArguments()
                .Constraints(
                    Constraints.Is.Matching<QualifiedName>(x => x.Name.StartsWith(this.sceneName.Name)),
                    Constraints.Is.Equal(this.regionWithNonZeroRadius))
                .Repeat.Times(expectedIntersections);
            this.service.RemoveRegionWithoutNotification(this.sceneName, this.regionWithNonZeroRadius);
            this.serverProxy.VerifyAllExpectations();
        }

        [Test]
        public void DetachSubscriptionReenablesRegionInsertion()
        {
            this.service.InsertRegion(this.sceneName, this.regionWithNonZeroRadius, this.ttl, null, null);
            this.service.DetachSubscription(this.sceneName, this.regionWithNonZeroRadius);

            // Re-insertion will throw if detach subscription doesn't do its job.
            this.service.InsertRegion(this.sceneName, this.regionWithNonZeroRadius, this.ttl, null, null);
        }

        [Test]
        public void StatusNotificationHandlerPassesStatusNotificationsToSubscription()
        {
            ISubscriptionData subscriptionData = MockRepository.GenerateMock<ISubscriptionData>();
            this.subscriptionDataFactory.Stub(f => f.Invoke(default(QualifiedName), null, 0, null, null, null))
                .IgnoreArguments()
                .Return(subscriptionData);
            this.service.InsertRegion(this.sceneName, this.regionWithNonZeroRadius, this.ttl, null, null);
            RequestStatus status = new RequestStatus(RequestStatus.Statuses.Complete);
            ImsStatusEventArgs imsStatusEventArgs = new ImsStatusEventArgs(
                this.regionWithNonZeroRadius.Guid,
                status,
                null);
            subscriptionData.Expect(s => s.Status(this.service, status));
            this.statusHandler(this, imsStatusEventArgs);
            subscriptionData.VerifyAllExpectations();
        }

        [Test, Ignore]
        public void ShouldStatusNotificationHandlerRemoveSubscriptionOnReceiptOfNoSuchServiceExceptionStatus()
        {
            // TO DO: Should it? InterestManagementService does.
        }

        [Test]
        public void ResultNotificationHandlerPassesEnterNotificationsToSubscription()
        {
            ISubscriptionData subscriptionData = MockRepository.GenerateMock<ISubscriptionData>();
            this.subscriptionDataFactory.Stub(f => f.Invoke(default(QualifiedName), null, 0, null, null, null))
                .IgnoreArguments()
                .Return(subscriptionData);
            this.service.InsertRegion(this.sceneName, this.regionWithNonZeroRadius, this.ttl, null, null);
            RequestStatus status = new RequestStatus(RequestStatus.Statuses.Success);
            ImsEventArgs enterEventArgs = new ImsEventArgs(this.regionWithZeroRadius);
            ImsStatusEventArgs imsStatusEventArgs = new ImsStatusEventArgs(
                this.regionWithNonZeroRadius.Guid,
                status,
                new List<ImsEventArgs>() { enterEventArgs });
            subscriptionData.Expect(s => s.EnterNotification(this.service, enterEventArgs));
            this.resultHandler(this, imsStatusEventArgs);
            subscriptionData.VerifyAllExpectations();
        }

        [Test]
        [ExpectedException(typeof(ImsInvalidOperationException))]
        public void ResultNotificationHandlerRemovesCompleteSubscriptions()
        {
            ISubscriptionData subscriptionData = MockRepository.GenerateMock<ISubscriptionData>();
            this.subscriptionDataFactory.Stub(f => f.Invoke(default(QualifiedName), null, 0, null, null, null))
                .IgnoreArguments()
                .Return(subscriptionData);
            this.service.InsertRegion(this.sceneName, this.regionWithNonZeroRadius, this.ttl, null, null);
            RequestStatus status = new RequestStatus(RequestStatus.Statuses.Complete);
            ImsStatusEventArgs imsStatusEventArgs = new ImsStatusEventArgs(
                this.regionWithNonZeroRadius.Guid,
                status,
                null);
            this.resultHandler(this, imsStatusEventArgs);

            // ModifyRegion should fail as subscription should have been removed.
            this.service.ModifyRegion(this.sceneName, this.regionWithNonZeroRadius, this.ttl, this.knownCollisionCount);
        }

        [Test]
        public void ResultNotificationHandlerLogsResultsForUnknownRegions()
        {
            // TO DO: Make a proper test for this, instead of just fudging coverage!
            RequestStatus status = new RequestStatus(RequestStatus.Statuses.Complete);
            ImsStatusEventArgs imsStatusEventArgs = new ImsStatusEventArgs(
                this.regionWithNonZeroRadius.Guid,
                status,
                null);
            this.resultHandler(this, imsStatusEventArgs);
        }
    }
}
