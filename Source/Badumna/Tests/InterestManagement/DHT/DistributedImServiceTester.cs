﻿using Badumna.Core;
using Badumna.DistributedHashTable;
using Badumna.InterestManagement;
using BadumnaTests.Core;
using BadumnaTests.DistributedHashTable.Services;
using NUnit.Framework;
using Rhino.Mocks;

namespace BadumnaTests.InterestManagement.DHT
{
    [TestFixture]
    public class DistributedIMServiceTester : InterestManagementServiceInterfaceTester
    {
        internal override IInterestManagementService CreateNewDerivedInstance()
        {
            INetworkAddressProvider addressProvider = MockRepository.GenerateStub<INetworkAddressProvider>();
            addressProvider.Stub(x => x.PublicAddress).Return(PeerAddress.GetLoopback(1));

            MockLoopbackRouter router = new MockLoopbackRouter(this.eventQueue, addressProvider);
            DhtFacade dht = new DhtFacade(router, this.eventQueue, this.timeKeeper, addressProvider, this.connectivityReporter, MessageParserTester.DefaultFactory);
            router.DhtFacade = dht;

            // both request and reply are sent on the same dht facade.
            DistributedImService imService = new DistributedImService(dht, dht, this.eventQueue, this.timeKeeper, this.connectivityReporter);
            dht.ForgeMethodList();

            return imService;
        }

        override protected int MaxGarbageCollectionDelayMilliseconds()
        {
            return (InterestManagementServer.GarbageCollectionTimeoutSeconds + ReplicaManager.GarbageCollectionIntervalSeconds) * 1000;
        }

        [Test, Ignore]
        public void MultipleCellTest()
        {
        }
    }
}
