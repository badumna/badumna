﻿//-----------------------------------------------------------------------
// <copyright file="RegionManagerTests.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2010 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BadumnaTests.InterestManagement
{
    using System;
    using Badumna.Core;
    using Badumna.DataTypes;
    using Badumna.DistributedHashTable;
    using Badumna.InterestManagement;
    using Badumna.Utilities;
    using NUnit.Framework;
    using Rhino.Mocks;
    using Constraints = Rhino.Mocks.Constraints;

    [TestFixture]
    internal class RegionManagerTests
    {
        protected QualifiedName sceneName = new QualifiedName("", "theScene");
        protected IInterestManagementService interestManagementService;
        protected ImsRegion imsRegion;
        protected TimeSpan ttl = TimeSpan.FromSeconds(10.0);
        protected INetworkEventScheduler eventQueue;
        protected ITime timeKeeper;
        protected INetworkConnectivityReporter networkConnectivityReporter;
        protected RegularTaskFactory regularTaskFactory;
        protected IRegularTask regularTask;
        protected GenericCallBack task = null;
        protected RegionManager regionManager;

        /// <summary>
        /// Region Type to determine which RegionManager or subclass will be instantiated in set up.
        /// Returns Source for RegionManager. Override in TestFixture subclasses for RegionManager
        /// subclasses.
        /// </summary>
        protected virtual RegionType TestRegionType
        {
            get
            {
                return RegionType.Source;
            }
        }

        [SetUp]
        public void SetUp()
        {
            this.interestManagementService = MockRepository.GenerateMock<IInterestManagementService>();
            this.interestManagementService.Stub(ims => ims.SubscriptionInflation).Return(30.0f);
            ////this.imsFactory.Stub(f => f(null)).IgnoreArguments().Return(this.interestManagementService);
            BadumnaId regionId = new BadumnaId(new PeerAddress(HashKey.Random()), 0);
            this.imsRegion = new ImsRegion(
                new Vector3(0f, 0f, 0f),
                1f,
                regionId,
                this.TestRegionType, // Source region will ensure RegionManager created (not dervied class).
                new QualifiedName("", "foo"));
            this.eventQueue = MockRepository.GenerateMock<INetworkEventScheduler>();
            this.timeKeeper = MockRepository.GenerateMock<ITime>();
            this.networkConnectivityReporter = MockRepository.GenerateMock<INetworkConnectivityReporter>();
            this.regularTaskFactory = MockRepository.GenerateMock<RegularTaskFactory>();
            this.regularTask = MockRepository.GenerateMock<IRegularTask>();
            this.regularTaskFactory.Stub(f => f.Invoke(string.Empty, TimeSpan.MinValue, null, null, null))
                .IgnoreArguments()
                .WhenCalled(c => { this.task = (GenericCallBack)c.Arguments[4]; })
                .Return(this.regularTask);
            this.regionManager = new RegionManager(
                this.sceneName,
                this.interestManagementService,
                this.imsRegion,
                this.ttl,
                this.eventQueue,
                this.timeKeeper,
                this.networkConnectivityReporter,
                this.regularTaskFactory);
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void CreateThrowsForNullRegion()
        {
            RegionManager regionManager = new RegionManager(
                this.sceneName,
                this.interestManagementService,
                null,
                this.ttl,
                this.eventQueue,
                this.timeKeeper,
                this.networkConnectivityReporter);
        }

        [Test]
        [ExpectedException(typeof(ArgumentException))]
        public void CreateThrowsArgumentExceptionForRegionWithInvalidType()
        {
            BadumnaId regionId = new BadumnaId();
            ImsRegion region = new ImsRegion(
                new Vector3(),
                0f,
                regionId,
                RegionType.None,
                new QualifiedName("", "foo"));
            RegionManager regionManager = new RegionManager(
                this.sceneName,
                this.interestManagementService,
                region,
                this.ttl,
                this.eventQueue,
                this.timeKeeper,
                this.networkConnectivityReporter);
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ConstructorThrowsForNullIms()
        {
            this.regionManager = new RegionManager(
                this.sceneName,
                null,
                this.imsRegion,
                this.ttl,
                this.eventQueue,
                this.timeKeeper,
                this.networkConnectivityReporter,
                this.regularTaskFactory);
        }

        [TestCase(0.999)]
        [TestCase(0.5)]
        [TestCase(0)]
        [TestCase(-1)]
        [TestCase(-999999999)]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void ConstructorThrowsForNullTtlLessThanOneSecond(double seconds)
        {
            this.regionManager = new RegionManager(
                this.sceneName,
                this.interestManagementService,
                this.imsRegion,
                TimeSpan.FromSeconds(seconds),
                this.eventQueue,
                this.timeKeeper,
                this.networkConnectivityReporter,
                this.regularTaskFactory);
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ConstructorThrowsForNullTimeKeeper()
        {
            this.regionManager = new RegionManager(
                this.sceneName,
                this.interestManagementService,
                this.imsRegion,
                this.ttl,
                this.eventQueue,
                null,
                this.networkConnectivityReporter,
                this.regularTaskFactory);
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ConstructorThrowsForNullEventQueue()
        {
            this.regionManager = new RegionManager(
                this.sceneName,
                this.interestManagementService,
                this.imsRegion,
                this.ttl,
                null,
                this.timeKeeper,
                this.networkConnectivityReporter,
                this.regularTaskFactory);
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ConstructorThrowsForNullNetworkConnectivityReporter()
        {
            this.regionManager = new RegionManager(
                this.sceneName,
                this.interestManagementService,
                this.imsRegion,
                this.ttl,
                this.eventQueue,
                this.timeKeeper,
                null,
                this.regularTaskFactory);
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ConstructorThrowsForNullRegularTaskFactory()
        {
            this.regionManager = new RegionManager(
                this.sceneName,
                this.interestManagementService,
                this.imsRegion,
                this.ttl,
                this.eventQueue,
                this.timeKeeper,
                this.networkConnectivityReporter,
                null);
        }

        [Test]
        public void ConstructorCopiesRegion()
        {
            BadumnaId regionId = new BadumnaId();
            ImsRegion region = new ImsRegion(
                new Vector3(),
                0f,
                regionId,
                RegionType.Source,
                new QualifiedName("", "foo"));
            RegionManager regionManager = new RegionManager(
                this.sceneName,
                this.interestManagementService,
                region,
                this.ttl,
                this.eventQueue,
                this.timeKeeper,
                this.networkConnectivityReporter);
            Assert.AreNotSame(region, regionManager.Region);
            Assert.IsTrue(object.ReferenceEquals(region.Guid, regionManager.Region.Guid));
        }

        [Test]
        public void KeepAliveTaskQueriesConnectivity()
        {
            this.networkConnectivityReporter.Expect(r => r.Status).Return(ConnectivityStatus.Online);
            this.task.Invoke();
            this.networkConnectivityReporter.VerifyAllExpectations();
        }

        [Test]
        public void KeepAliveTaskTellsImServiceToModifyRegion()
        {
            this.networkConnectivityReporter.Stub(r => r.Status).Return(ConnectivityStatus.Online);
            this.regularTask.Stub(t => t.IsRunning).Return(false);
            this.regionManager.ModifyRegion(new Vector3(0f, 0f, 0f), 0f);
            this.interestManagementService.Expect(s => s.ModifyRegion(default(QualifiedName), null, 0, 0))
                .IgnoreArguments()
                .WhenCalled(c =>
                {
                    Assert.AreEqual(this.sceneName, (QualifiedName)c.Arguments[0]);
                    ImsRegion modifiedRegion = (ImsRegion)c.Arguments[1];
                    Assert.AreEqual(this.imsRegion.Guid, modifiedRegion.Guid);
                    Assert.AreEqual((uint)this.ttl.TotalSeconds, (uint)c.Arguments[2]);
                });
            this.task.Invoke();
            this.interestManagementService.VerifyAllExpectations();
        }

        [Test]
        public void KeepAliveTaskDoesNotTellImServiceToModifyRegionWhenNotOnline()
        {
            this.networkConnectivityReporter.Stub(r => r.Status).Return(ConnectivityStatus.Offline);
            this.regularTask.Stub(t => t.IsRunning).Return(false);
            this.regionManager.ModifyRegion(new Vector3(0f, 0f, 0f), 0f);
            this.interestManagementService.Expect(s => s.ModifyRegion(default(QualifiedName), null, 0, 0))
                .IgnoreArguments()
                .Repeat.Never();
            this.task.Invoke();
            this.interestManagementService.VerifyAllExpectations();
        }

        [Test]
        public void ShutdownUnsubscribesFromNetworkOnlineEvent()
        {
            this.networkConnectivityReporter.Expect(
                n => n.NetworkOnlineEvent -= null).IgnoreArguments();
            this.regionManager.ShutDown();
            this.networkConnectivityReporter.VerifyAllExpectations();
        }

        [Test]
        public void ShutdownWithNotificationRemovesRegionFromEachIMS()
        {
            this.regionManager.ModifyRegion(new Vector3(), 0f); // Triggers IMS creation.
            this.interestManagementService.Expect(
                ims => ims.RemoveRegion(this.sceneName, this.imsRegion)).IgnoreArguments();
            this.regionManager.ShutDown(true);
            this.interestManagementService.VerifyAllExpectations();
        }

        [Test]
        public void ShutdownWithoutNotificationDetachesSubscriptionsFromEachIMS()
        {
            this.regionManager.ModifyRegion(new Vector3(), 0f); // Triggers IMS creation.
            this.interestManagementService.Expect(
                ims => ims.DetachSubscription(this.sceneName, this.imsRegion)).IgnoreArguments();
            this.regionManager.ShutDown(false);
            this.interestManagementService.VerifyAllExpectations();
        }

        // TODO: Make this testable?

        ////[Test]
        ////public void ShutdownClearsIMSs()
        ////{
        ////    throw new NotImplementedException();
        ////}

        [Test]
        public void ShutdownStopsKeepAliveTask()
        {
            this.regularTask.Expect(t => t.Stop());
            this.regionManager.ShutDown();
            this.regularTask.VerifyAllExpectations();
        }

        [Test]
        public void ModifyRegionStartsUnstartedKeepAliveTasks()
        {
            this.regularTask.Stub(t => t.IsRunning).Return(false);
            this.regularTask.Expect(t => t.Start());
            this.regionManager.ModifyRegion(new Vector3(0f, 0f, 0f), 0f);
            this.regularTask.VerifyAllExpectations();
        }

        [Test]
        public void ModifyRegionDoesNotTryAndStartRunningKeepaliveTasks()
        {
            this.regularTask.Stub(t => t.IsRunning).Return(true);
            this.regularTask.Expect(t => t.Start()).Repeat.Never();
            this.regionManager.ModifyRegion(new Vector3(0f, 0f, 0f), 0f);
            this.regularTask.VerifyAllExpectations();
        }

        [Test]
        public void ModifyRegionMakesInsertionRequestOnFirstCall()
        {
            this.regularTask.Stub(t => t.IsRunning).Return(false);
            this.interestManagementService.Expect(s => s.InsertRegion(default(QualifiedName), null, 0, null, null))
                .IgnoreArguments()
                .WhenCalled(c =>
                    {
                        Assert.AreEqual(this.sceneName, (QualifiedName)c.Arguments[0]);
                        Assert.AreEqual(this.imsRegion.Guid, ((ImsRegion)c.Arguments[1]).Guid);
                        Assert.AreEqual((uint)this.ttl.TotalSeconds, (uint)c.Arguments[2]);
                    });
            this.regionManager.ModifyRegion(new Vector3(0f, 0f, 0f), 0f);
            this.interestManagementService.VerifyAllExpectations();
        }

        // Assumes inflation amount of 30.
        [TestCase(0f, 0f, 0f, 32f, 0f)] // Region grown.
        [TestCase(31f, 0f, 0f, 1f, 0f)] // Region moved, so no longer contained.
        [TestCase(0f, 0f, 0f, 0.5f, 11f)] // Changed region, not grown or moved, but sync interval passed.
        public virtual void ModifyRegionMakesModificationRequestWhenSubscriptionChanged(
            float x,
            float y,
            float z,
            float r,
            double timeInSeconds)
        {
            this.regularTask.Stub(t => t.IsRunning).Return(false);
            this.regionManager.ModifyRegion(new Vector3(0f, 0f, 0f), 1);
            this.timeKeeper.Stub(t => t.Now).Return(TimeSpan.FromSeconds(timeInSeconds));
            this.interestManagementService.Expect(s =>
                s.ModifyRegion(default(QualifiedName), null, 0, 0))
                .IgnoreArguments()
                .WhenCalled(c =>
                    {
                        Assert.AreEqual(this.sceneName, (QualifiedName)c.Arguments[0]);
                        Assert.AreEqual(this.imsRegion.Guid, ((ImsRegion)c.Arguments[1]).Guid);
                        Assert.AreEqual((uint)this.ttl.TotalSeconds, (uint)c.Arguments[2]);
                    });
            this.regularTask.Expect(t => t.Delay());
            this.regionManager.ModifyRegion(new Vector3(x, y, z), r);
            this.interestManagementService.VerifyAllExpectations();
            this.regularTask.VerifyAllExpectations();
        }

        // Assumes inflation amount of 30.
        [TestCase(0f, 0f, 0f, 1f, 0f)] // Region same.
        [TestCase(0f, 0f, 0f, 0.5f, 0f)] // Region shrunk.
        [TestCase(0f, 0f, 0f, 30f, 0f)] // Region grown, but not bigger than inflated region.
        [TestCase(30f, 0f, 0f, 1f, 0f)] // Region moved, but still contained in inflated region.
        [TestCase(0f, 0f, 0f, 1f, 11f)] // Sync interval passed, but region not changed.
        public virtual void ModifyRegionMakesNoModificationRequestWhenSubscriptionNotChanged(
            float x,
            float y,
            float z,
            float r,
            double timeInSeconds)
        {
            this.regularTask.Stub(t => t.IsRunning).Return(false);
            this.regionManager.ModifyRegion(new Vector3(0f, 0f, 0f), 1);
            this.timeKeeper.Stub(t => t.Now).Return(TimeSpan.FromSeconds(timeInSeconds));
            this.interestManagementService.Expect(s =>
                s.ModifyRegion(default(QualifiedName), null, 0, 0))
                .IgnoreArguments()
                .Repeat.Never();
            this.regularTask.Expect(t => t.Delay()).Repeat.Never();
            this.regionManager.ModifyRegion(new Vector3(x, y, z), r);
            this.interestManagementService.VerifyAllExpectations();
            this.regularTask.VerifyAllExpectations();
        }

        [Test]
        public void IntersectionHandlerRaisesIntersectionEventsForIntersectionsWithNonImServices()
        {
            this.regularTask.Stub(t => t.IsRunning).Return(false);
            EventHandler<ImsEventArgs> regionManagerIntersectionHandler = null;
            this.interestManagementService.Stub(s => s.InsertRegion(default(QualifiedName), null, 0, null, null))
                .IgnoreArguments()
                .WhenCalled(c => { regionManagerIntersectionHandler = (EventHandler<ImsEventArgs>)c.Arguments[3]; });
            this.regionManager.ModifyRegion(new Vector3(0f, 0f, 0f), 1);
            BadumnaId otherRegionId = new BadumnaId(new PeerAddress(HashKey.Random()), 0);
            RegionType otherRegionType = RegionType.None;
            EventHandler<ImsEventArgs> subscribedIntersectionHandler =
                MockRepository.GenerateMock<EventHandler<ImsEventArgs>>();
            this.regionManager.IntersectionEvent += subscribedIntersectionHandler;
            subscribedIntersectionHandler.Expect(h => h.Invoke(null, null))
                .IgnoreArguments()
                .WhenCalled(c =>
                    {
                        Assert.AreEqual(this.regionManager, (RegionManager)c.Arguments[0]);
                        ImsEventArgs ev = (ImsEventArgs)c.Arguments[1];
                        Assert.AreEqual(otherRegionId, ev.OtherRegionId);
                        Assert.AreEqual(otherRegionType, ev.OtherRegionType);
                    });
            regionManagerIntersectionHandler.Invoke(
                this.interestManagementService,
                new ImsEventArgs(otherRegionId, otherRegionType));
            subscribedIntersectionHandler.VerifyAllExpectations();
        }

        /// <summary>
        /// IntersectionHandler doesn't update RegionManager.numberOfActualIntersections, which is
        /// only updated in ModifyRegion. This seems incorrect.
        /// </summary>
        [Test, Ignore]
        public void IntersectionHandlerStoresIntersections()
        {
            this.regularTask.Stub(t => t.IsRunning).Return(false);
            EventHandler<ImsEventArgs> regionManagerIntersectionHandler = null;
            this.interestManagementService.Stub(s => s.InsertRegion(default(QualifiedName), null, 0, null, null))
                .IgnoreArguments()
                .WhenCalled(c => { regionManagerIntersectionHandler = (EventHandler<ImsEventArgs>)c.Arguments[3]; });
            this.regionManager.ModifyRegion(new Vector3(0f, 0f, 0f), 1);
            BadumnaId otherRegionId = new BadumnaId(new PeerAddress(HashKey.Random()), 0);
            RegionType otherRegionType = RegionType.None;
            regionManagerIntersectionHandler.Invoke(
                this.interestManagementService,
                new ImsEventArgs(otherRegionId, otherRegionType));
            this.networkConnectivityReporter.Stub(r => r.Status).Return(ConnectivityStatus.Online);
            this.interestManagementService.Expect(s => s.ModifyRegion(default(QualifiedName), null, 0, 0))
                .IgnoreArguments()
                .Constraints(
                    Constraints.Is.Anything(),
                    Constraints.Is.Anything(),
                    Constraints.Is.Anything(),
                    Constraints.Is.Equal((byte)1));
            this.task.Invoke();
            this.interestManagementService.VerifyAllExpectations();
        }
       
        /// <summary>
        /// IntersectionHandler doesn't update RegionManager.numberOfActualIntersections, which is
        /// only updated in ModifyRegion. This seems incorrect.
        /// Test will pass, but only because the number of intersections was not incremented
        /// in the first place.
        /// </summary>
        [Test]
        public void ForceRemovalOfRegionClearsIntersections()
        {
            this.regularTask.Stub(t => t.IsRunning).Return(false);
            EventHandler<ImsEventArgs> regionManagerIntersectionHandler = null;
            this.interestManagementService.Stub(s => s.InsertRegion(default(QualifiedName), null, 0, null, null))
                .IgnoreArguments()
                .WhenCalled(c => { regionManagerIntersectionHandler = (EventHandler<ImsEventArgs>)c.Arguments[3]; });
            this.regionManager.ModifyRegion(new Vector3(0f, 0f, 0f), 1);
            BadumnaId otherRegionId = new BadumnaId(new PeerAddress(HashKey.Random()), 0);
            RegionType otherRegionType = RegionType.None;
            regionManagerIntersectionHandler.Invoke(
                this.interestManagementService,
                new ImsEventArgs(otherRegionId, otherRegionType));
            this.regionManager.ForceRemovalOfRegion(otherRegionId);
            this.networkConnectivityReporter.Stub(r => r.Status).Return(ConnectivityStatus.Online);
            this.interestManagementService.Expect(s => s.ModifyRegion(default(QualifiedName), null, 0, 0))
                .IgnoreArguments()
                .Constraints(
                    Constraints.Is.Anything(),
                    Constraints.Is.Anything(),
                    Constraints.Is.Anything(),
                    Constraints.Is.Equal((byte)0));
            this.task.Invoke();
            this.interestManagementService.VerifyAllExpectations();
        }
    }
}