﻿using System.Collections.Generic;
using Badumna.Core;
using Badumna.DataTypes;
using Badumna.InterestManagement;
using NUnit.Framework;

namespace BadumnaTests.InterestManagement
{
    [TestFixture]
    [DontPerformCoverage]
    public class ImsRegionTester : BadumnaTests.Core.ParseableTestHelper
    {
        const string sceneName = "ImsRegionTester";
        class ParseableTester : BadumnaTests.Core.ParseableTester<ImsRegion>
        {
            public override void AssertAreEqual(ImsRegion expected, ImsRegion actual)
            {
                Assert.AreEqual(expected.Centroid, actual.Centroid);
                Assert.AreEqual(expected.Guid, actual.Guid);
                Assert.AreEqual(expected.Radius, actual.Radius);
            }

            public override ICollection<ImsRegion> CreateExpectedValues()
            {
                ICollection<ImsRegion> expectedValues = new List<ImsRegion>();

                expectedValues.Add(new ImsRegion(new Vector3(0, 2, 6), 78.54f, ImsRegion.RegionType.Sink, sceneName));

                return expectedValues;
            }
        }

        public ImsRegionTester()
            : base(new ParseableTester())
        {
            NetworkContext.CurrentContext.SetAddresses(PeerAddress.GetLoopback(99));
        }

        [Test]
        public void CopyTest()
        {
            foreach (ImsRegion expected in this.CreateExpectedValues<ImsRegion>())
            {
                ImsRegion actual = new ImsRegion(expected);

                this.AssertAreEqual(expected, actual);
            }
        }

        [Test]
        public void InverseIntersectionTest()
        {
            ImsRegion inverseOne = new ImsRegion(new Vector3(0, 0, 0), 2.0f, ImsRegion.RegionType.ImService, sceneName);
            inverseOne.Options = ImsRegion.RegionOptions.Inverse;
            ImsRegion sphere = new ImsRegion(new Vector3(0, 0, 0), 1.0f, ImsRegion.RegionType.ImService, sceneName);
            ImsRegion inverseTwo = new ImsRegion(new Vector3(100, 100, 100), 10.0f, ImsRegion.RegionType.ImService, sceneName);
            inverseTwo.Options = ImsRegion.RegionOptions.Inverse;

            // Test inside
            Assert.IsFalse(inverseOne.IsIntersecting(sphere));
            Assert.IsFalse(sphere.IsIntersecting(inverseOne));

            // Test outside
            Assert.IsTrue(inverseTwo.IsIntersecting(sphere));
            Assert.IsTrue(sphere.IsIntersecting(inverseTwo));

            // two inverse
            Assert.IsTrue(inverseTwo.IsIntersecting(inverseOne));
        }
    }
}
