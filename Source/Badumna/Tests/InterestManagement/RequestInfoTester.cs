﻿using System;
using System.Collections.Generic;
using System.Text;
using Badumna.Core;
using Badumna.InterestManagement;
using BadumnaTests.Core;
using NUnit.Framework;

namespace BadumnaTests.InterestManagement
{
    [TestFixture]
    [DontPerformCoverage]
    public class RequestInfoTester : ParseableTestHelper
    {
        class ParseableTester : ParseableTester<RequestStatus>
        {
            public override void AssertAreEqual(RequestStatus expected, RequestStatus actual)
            {
                Assert.AreEqual(expected.Status, actual.Status);
            }

            public override ICollection<RequestStatus> CreateExpectedValues()
            {
                ICollection<RequestStatus> expectedValues = new List<RequestStatus>();

                expectedValues.Add(new RequestStatus(RequestStatus.Statuses.InvalidRegion));

                return expectedValues;
            }
        }

        public RequestInfoTester()
            : base(new ParseableTester())
        { }
    }
}
