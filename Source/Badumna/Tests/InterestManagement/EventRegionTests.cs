﻿//-----------------------------------------------------------------------
// <copyright file="EventRegionTests.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2010 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BadumnaTests.InterestManagement
{
    using System;
    using Badumna.Core;
    using Badumna.DataTypes;
    using Badumna.InterestManagement;
    using Badumna.Utilities;
    using NUnit.Framework;

    [TestFixture]
    [DontPerformCoverage]
    public class EventRegionTests
    {
        [Test]
        public void ConstructorSetsRegionTypeToSource()
        {
            EventRegion eventRegion = new EventRegion(
                new BadumnaId(PeerAddress.GetLoopback(1), 1),
                new Vector3(0f, 0f, 0f),
                1f,
                new QualifiedName("", Guid.NewGuid().ToString()));
            Assert.AreEqual(RegionType.Source, eventRegion.Type);
        }
    }
}
