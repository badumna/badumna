﻿using System;
using System.Collections.Generic;
using System.Text;

using Badumna;
using Badumna.Core;
using Badumna.DistributedHashTable;
using Badumna.ServiceDiscovery;
using Badumna.Utilities;
using NUnit.Framework;
using BadumnaTests.DistributedHashTable.Services;
using Badumna.Security;
using Rhino.Mocks;

namespace BadumnaTests.ServiceDiscovery
{
    [TestFixture]
    [DontPerformCoverage]
    public class ServiceDiscoveryManagerTester
    {
        MockLoopbackRouter mRouter;
        DhtFacade mFacade;
        MockServiceDiscoveryManager mDiscoveryManager;
        PeerAddress mAddress;
        private Simulator eventQueue;
        private ITime timeKeeper;
        private TokenCache tokens;
        private INetworkConnectivityReporter connectivityReporter;
        private INetworkAddressProvider addressProvider;

        internal static GenericCallBackReturn<object, Type> MakeServiceDiscoveryFactory(ITime timeKeeper)
        {
            return delegate (Type type)
            {
                if (type == typeof(ServiceDetailsRecord))
                {
                    return new ServiceDetailsRecord(timeKeeper);
                }

                return Activator.CreateInstance(type);
            };
        }

        [SetUp]
        public void Initialize()
        {
            this.eventQueue = new Simulator();
            this.timeKeeper = this.eventQueue;

            this.connectivityReporter = MockRepository.GenerateStub<INetworkConnectivityReporter>();
            this.connectivityReporter.Stub(x => x.Status).Return(ConnectivityStatus.Online);

            this.addressProvider = MockRepository.GenerateStub<INetworkAddressProvider>();
            this.mAddress = PeerAddress.GetLoopback(1);
            this.addressProvider.Stub(x => x.PublicAddress).Return(this.mAddress);

            this.tokens = new TokenCache(this.eventQueue, this.timeKeeper, this.connectivityReporter);
            this.tokens.SetIdentityProvider(new UnverifiedIdentityProvider());
            this.tokens.PreCache();

            this.mRouter = new MockLoopbackRouter(this.eventQueue, this.addressProvider);
            this.mFacade = new DhtFacade(this.mRouter, this.eventQueue, this.timeKeeper, this.addressProvider, this.connectivityReporter, ServiceDiscoveryManagerTester.MakeServiceDiscoveryFactory(this.timeKeeper));
            this.mRouter.DhtFacade = this.mFacade;

            Assert.AreEqual(this.mFacade, this.mRouter.DhtFacade);
            Assert.AreEqual(HashKey.Min, this.mRouter.LocalHashKey);
        }

        [TearDown]
        public void Terminate()
        {
            if (this.mDiscoveryManager != null)
            {
                this.mDiscoveryManager.Shutdown();
            }
        }

        [Test]
        [Category("Slow")]
        public void TestRegularTask()
        {
            this.mDiscoveryManager = new MockServiceDiscoveryManager(this.mFacade, true, this.eventQueue, this.timeKeeper, this.addressProvider, this.connectivityReporter, this.tokens);
            this.mDiscoveryManager.Start();
            this.mFacade.ForgeMethodList();
            this.eventQueue.RunFor(TimeSpan.FromSeconds(601));

            Assert.AreEqual(60, this.mDiscoveryManager.MaintainceTaskCounter);
            Assert.IsFalse(this.mDiscoveryManager.QueryReplyContains(this.mAddress, new ServiceType(ServerType.Arbitration)));
            Assert.IsFalse(this.mDiscoveryManager.QueryReplyContains(this.mAddress, new ServiceType(ServerType.Overload)));

            this.mDiscoveryManager.AnnounceService(new ServiceType(ServerType.Arbitration));
            this.mDiscoveryManager.RegisterInterestedService(new ServiceType(ServerType.Arbitration));
            this.mDiscoveryManager.RegisterInterestedService(new ServiceType(ServerType.Overload));
            this.mDiscoveryManager.ForceCheckDht();

            this.eventQueue.RunFor(TimeSpan.FromSeconds(62));
            Assert.IsTrue(this.mDiscoveryManager.QueryReplyContains(this.mAddress, new ServiceType(ServerType.Arbitration)));
            Assert.IsFalse(this.mDiscoveryManager.QueryReplyContains(this.mAddress, new ServiceType(ServerType.Overload)));

            this.mDiscoveryManager.AnnounceService(new ServiceType(ServerType.Overload));
            this.eventQueue.RunFor(TimeSpan.FromSeconds(62));
            this.mDiscoveryManager.QueryAnnouncedService(new ServiceType(ServerType.Overload));
            this.eventQueue.RunFor(TimeSpan.FromSeconds(2));
            Assert.IsTrue(this.mDiscoveryManager.QueryReplyContains(this.mAddress, new ServiceType(ServerType.Arbitration)));
            Assert.IsTrue(this.mDiscoveryManager.QueryReplyContains(this.mAddress, new ServiceType(ServerType.Overload)));

            this.eventQueue.RunFor(TimeSpan.FromSeconds(62));
            this.eventQueue.RunFor(TimeSpan.FromSeconds(2));
            Assert.IsTrue(this.mDiscoveryManager.QueryReplyContains(this.mAddress, new ServiceType(ServerType.Arbitration)));
            Assert.IsTrue(this.mDiscoveryManager.QueryReplyContains(this.mAddress, new ServiceType(ServerType.Overload)));
        }

        [Test]
        public void TestDefaultSettings()
        {
            // total number of replicas
            Assert.GreaterOrEqual(MockServiceDiscoveryManager.DHTReplicas, 3);
            Assert.LessOrEqual(MockServiceDiscoveryManager.DHTReplicas, 128);
        }

        [Test]
        public void TestAnnounceAndQuery()
        {
            this.mDiscoveryManager = new MockServiceDiscoveryManager(this.mFacade, true, this.eventQueue, this.timeKeeper, this.addressProvider, this.connectivityReporter, this.tokens);
            this.mDiscoveryManager.Start();
            this.mFacade.ForgeMethodList();

            this.mDiscoveryManager.AnnounceService(new ServiceType(ServerType.Arbitration));
            this.mDiscoveryManager.RegisterInterestedService(new ServiceType(ServerType.Arbitration));
            this.eventQueue.RunFor(TimeSpan.FromSeconds(1));
            this.mDiscoveryManager.QueryAnnouncedService(new ServiceType(ServerType.Arbitration));
            this.eventQueue.RunFor(TimeSpan.FromSeconds(1));

            Assert.True(this.mDiscoveryManager.QueryReplyContains(this.mAddress, new ServiceType(ServerType.Arbitration)));
            Assert.IsFalse(this.mDiscoveryManager.QueryReplyContains(this.mAddress, new ServiceType(ServerType.Overload)));
        }

        [Test]
        public void TestRemove()
        {
            this.mDiscoveryManager = new MockServiceDiscoveryManager(this.mFacade, true, this.eventQueue, this.timeKeeper, this.addressProvider, this.connectivityReporter, this.tokens);
            this.mDiscoveryManager.Start();
            this.mFacade.ForgeMethodList();

            this.mDiscoveryManager.AnnounceService(new ServiceType(ServerType.Arbitration));
            this.eventQueue.RunFor(TimeSpan.FromSeconds(1));

            this.mDiscoveryManager.QueryAnnouncedService(new ServiceType(ServerType.Arbitration));
            this.eventQueue.RunFor(TimeSpan.FromSeconds(1));

            Assert.IsTrue(this.mDiscoveryManager.QueryReplyContains(this.mAddress, new ServiceType(ServerType.Arbitration)));
            this.mDiscoveryManager.ClearCachedQueryResult();
            Assert.IsFalse(this.mDiscoveryManager.QueryReplyContains(this.mAddress, new ServiceType(ServerType.Arbitration)));

            this.mDiscoveryManager.Shutdown();
            this.mDiscoveryManager.QueryAnnouncedService(new ServiceType(ServerType.Arbitration));
            this.eventQueue.RunFor(TimeSpan.FromSeconds(3));

            Assert.IsFalse(this.mDiscoveryManager.QueryReplyContains(this.mAddress, new ServiceType(ServerType.Arbitration)));
        }

        /*
        [Test]
        public void TestGarbageCollection()
        {
            this.mDiscoveryManager = new MockServiceDiscoveryManager(this.mFacade);
            this.mDiscoveryManager.Start();
            this.mFacade.ForgeMethodList();

            this.mDiscoveryManager.AnnounceService(new ServiceType(ServerType.Arbitration));
            this.mDiscoveryManager.RegisterInterestedService(new ServiceType(ServerType.Arbitration));
            
            Simulator.Instance.RunFor(TimeSpan.FromSeconds(1));

            this.mDiscoveryManager.QueryAnnouncedService(new ServiceType(ServerType.Arbitration));
            Simulator.Instance.RunFor(TimeSpan.FromSeconds(1));
            Assert.IsTrue(this.mDiscoveryManager.QueryReplyContains(this.mAddress, new ServiceType(ServerType.Arbitration)));

            Simulator.Instance.RunFor(Parameters.ServiceDiscoveryCachedRecordTTL);
            Simulator.Instance.RunFor(TimeSpan.FromMinutes(3));
            this.mDiscoveryManager.QueryAnnouncedService(new ServiceType(ServerType.Arbitration));
            Simulator.Instance.RunFor(TimeSpan.FromSeconds(1));
            Assert.IsTrue(this.mDiscoveryManager.QueryReplyContains(this.mAddress, new ServiceType(ServerType.Arbitration)));

            this.mDiscoveryManager.ShutdownService(new ServiceType(ServerType.Arbitration));
            Simulator.Instance.RunFor(Parameters.ServiceDiscoveryCachedRecordTTL);
            Simulator.Instance.RunFor(TimeSpan.FromMinutes(3));
            Assert.IsFalse(this.mDiscoveryManager.QueryReplyContains(this.mAddress, new ServiceType(ServerType.Arbitration)));
            this.mDiscoveryManager.QueryAnnouncedService(new ServiceType(ServerType.Arbitration));
            Simulator.Instance.RunFor(TimeSpan.FromSeconds(1));
            Assert.IsFalse(this.mDiscoveryManager.QueryReplyContains(this.mAddress, new ServiceType(ServerType.Arbitration)));
        }*/
    }
}
