﻿using System;
using System.Collections.Generic;
using Badumna.Core;
using Badumna.DistributedHashTable;
using Badumna.ServiceDiscovery;
using Badumna.Utilities;
using BadumnaTests.Core;
using BadumnaTests.DistributedHashTable.Services;
using NUnit.Framework;
using Rhino.Mocks;

namespace BadumnaTests.ServiceDiscovery
{
    [DontPerformCoverage]
    internal class MockDhtLoadEstimator : DhtLoadEstimator
    {
        public MockDhtLoadEstimator(DhtFacade dht, ITime timeKeeper)
            : base(dht, timeKeeper)
        {
        }

        public int GetObservedQueryNumber(ServiceType type)
        {
            if (this.ObservedDhtQueries.ContainsKey(type))
            {
                List<TimeSpan> list = this.ObservedDhtQueries[type];
                if (list == null || list.Count == 0)
                {
                    return 0;
                }

                return list.Count;
            }
            else
            {
                return 0;
            }
        }
    }

    [TestFixture]
    [DontPerformCoverage]
    public class DhtLoadEstimatorTester
    {
        private MockLoopbackRouter mRouter;
        private DhtFacade mFacade;

        private MockDhtLoadEstimator mDhtLoadEstimator;

        private Simulator eventQueue;
        private ITime timeKeeper;

        [SetUp]
        public void Initialize()
        {
            this.eventQueue = new Simulator();
            this.timeKeeper = this.eventQueue;
            
            INetworkConnectivityReporter connectivityReporter = MockRepository.GenerateStub<INetworkConnectivityReporter>();

            INetworkAddressProvider addressProvider = MockRepository.GenerateStub<INetworkAddressProvider>();
            addressProvider.Stub(x => x.PublicAddress).Return(PeerAddress.GetLoopback(1));

            this.mRouter = new MockLoopbackRouter(this.eventQueue, addressProvider);
            this.mFacade = new DhtFacade(this.mRouter, this.eventQueue, this.timeKeeper, addressProvider, connectivityReporter, MessageParserTester.DefaultFactory);
            this.mRouter.DhtFacade = this.mFacade;

            Assert.AreEqual(this.mFacade, this.mRouter.DhtFacade);
            Assert.AreEqual(HashKey.Min, this.mRouter.LocalHashKey);
        }

        [Test]
        public void TestGarbageCollection()
        {
            this.mDhtLoadEstimator = new MockDhtLoadEstimator(this.mFacade, this.timeKeeper);

            for (int i = 0; i < 10; i++)
            {
                this.mDhtLoadEstimator.RecordObservedDhtQuery(new ServiceType(ServerType.Arbitration));
                this.mDhtLoadEstimator.RecordObservedDhtQuery(new ServiceType(ServerType.Overload));
                this.eventQueue.RunFor(TimeSpan.FromSeconds(2));
            }

            this.mDhtLoadEstimator.RecordObservedDhtQuery(new ServiceType(ServerType.Overload));
            this.eventQueue.RunFor(TimeSpan.FromSeconds(2));
            this.mDhtLoadEstimator.RecordObservedDhtQuery(new ServiceType(ServerType.Overload));
            this.eventQueue.RunFor(TimeSpan.FromSeconds(2));

            Assert.AreEqual(10, this.mDhtLoadEstimator.GetObservedQueryNumber(new ServiceType(ServerType.Arbitration)));
            Assert.AreEqual(12, this.mDhtLoadEstimator.GetObservedQueryNumber(new ServiceType(ServerType.Overload)));

            // remove all unless the rec is observed in the last 3 seconds
            this.mDhtLoadEstimator.GarbageCollection(TimeSpan.FromSeconds(3));

            Assert.AreEqual(0, this.mDhtLoadEstimator.GetObservedQueryNumber(new ServiceType(ServerType.Arbitration)));
            Assert.AreEqual(1, this.mDhtLoadEstimator.GetObservedQueryNumber(new ServiceType(ServerType.Overload)));
        }
    }
}
