﻿
using Badumna.Core;
using Badumna.DistributedHashTable;
using Badumna.Security;
using Badumna.ServiceDiscovery;
using Badumna.Utilities;
using BadumnaTests.Core;
using BadumnaTests.DistributedHashTable.Services;
using NUnit.Framework;
using Rhino.Mocks;

namespace BadumnaTests.ServiceDiscovery
{
    [TestFixture]
    [DontPerformCoverage]
    public class ServiceExchangeTester
    {
        private MockLoopbackRouter mRouter;
        private DhtFacade mFacade;
        private ServiceExchangeManager mServiceExchangeManager;
        private ServiceDiscoveryManager mDiscoveryManager;
        private PeerAddress mFakeAddress = PeerAddress.GetAddress("1.2.3.4", 1, NatType.Open);
        private Simulator eventQueue;
        private ITime timeKeeper;
        private TokenCache tokens;
        private INetworkConnectivityReporter connectivityReporter;
        private INetworkAddressProvider addressProvider;

        [SetUp]
        public void Initialize()
        {
            this.eventQueue = new Simulator();
            this.timeKeeper = this.eventQueue;

            this.addressProvider = MockRepository.GenerateStub<INetworkAddressProvider>();
            this.addressProvider.Stub(x => x.PublicAddress).Return(PeerAddress.GetLoopback(1));

            this.connectivityReporter = MockRepository.GenerateStub<INetworkConnectivityReporter>();
            this.connectivityReporter.Stub(x => x.Status).Return(ConnectivityStatus.Online);

            this.tokens = new TokenCache(this.eventQueue, this.timeKeeper, this.connectivityReporter);
            this.tokens.SetIdentityProvider(new UnverifiedIdentityProvider());
            this.tokens.PreCache();

            this.mRouter = new MockLoopbackRouter(this.eventQueue, this.addressProvider);
            this.mFacade = new DhtFacade(this.mRouter, this.eventQueue, this.timeKeeper, this.addressProvider, this.connectivityReporter, MessageParserTester.DefaultFactory);
            this.mRouter.DhtFacade = this.mFacade;

            Assert.AreEqual(this.mFacade, this.mRouter.DhtFacade);
            Assert.AreEqual(HashKey.Min, this.mRouter.LocalHashKey);
        }

        [Test]
        public void TestInitialization()
        {
            bool isServiceDiscoveryEnabled = true;
            this.mDiscoveryManager = new ServiceDiscoveryManager(this.mFacade, isServiceDiscoveryEnabled, this.eventQueue, this.timeKeeper, this.addressProvider, this.connectivityReporter, this.tokens);
            this.mServiceExchangeManager = new ServiceExchangeManager(this.mFacade, this.mDiscoveryManager, this.addressProvider, isServiceDiscoveryEnabled, ServiceDiscoveryStatsCollector.GetInstance(this.timeKeeper));
            this.mFacade.ForgeMethodList();
        }
    }
}
