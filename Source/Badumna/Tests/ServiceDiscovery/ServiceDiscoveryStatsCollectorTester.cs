﻿using System;
using System.Collections.Generic;
using System.Text;

using Badumna;
using Badumna.Core;
using Badumna.DistributedHashTable;
using Badumna.ServiceDiscovery;
using Badumna.Utilities;
using NUnit.Framework;

namespace BadumnaTests.ServiceDiscovery
{
    [DontPerformCoverage]
    internal class ServiceDiscoveryStatsCollectorMock : ServiceDiscoveryStatsCollector
    {
        public ServiceDiscoveryStatsCollectorMock(ITime timeKeeper)
            : base(timeKeeper)
        {
        }

        public Dictionary<ServiceDetailsRecord, Stack<TimeSpan>> GetAnnouncement()
        {
            return this.mAnnouncement;
        }

        public Dictionary<ServiceQueryDetails, Stack<TimeSpan>> GetRecordedQuery()
        {
            return this.mQueryDetails;
        }
    }

    [TestFixture]
    [DontPerformCoverage]
    public class ServiceDiscoveryStatsCollectorTester
    {
        private ServiceDiscoveryStatsCollectorMock mStatsCollector;
        private Simulator eventQueue;
        private ITime timeKeeper;

        [SetUp]
        public void Initialize()
        {
            this.eventQueue = new Simulator();
            this.timeKeeper = this.eventQueue;
            this.mStatsCollector = new ServiceDiscoveryStatsCollectorMock(this.timeKeeper);
        }

        [Test]
        public void TestRecordAnnouncement()
        {
            ServiceDetailsRecord r1 = new ServiceDetailsRecord(
                PeerAddress.GetAddress("127.0.0.1", 1, NatType.Open),
                new ServiceType(ServerType.Arbitration),
                this.timeKeeper);
            this.eventQueue.RunFor(TimeSpan.FromSeconds(1));
            ServiceDetailsRecord r3 = new ServiceDetailsRecord(
                PeerAddress.GetAddress("127.0.0.1", 1, NatType.Open),
                new ServiceType(ServerType.Overload),
                this.timeKeeper);
            this.eventQueue.RunFor(TimeSpan.FromSeconds(1));
            ServiceDetailsRecord r4 = new ServiceDetailsRecord(
                PeerAddress.GetAddress("127.0.0.1", 1, NatType.Open),
                new ServiceType(ServerType.Overload),
                this.timeKeeper);

            this.mStatsCollector.RecordAnnouncement(r1);
            this.mStatsCollector.RecordAnnouncement(r3);
            this.mStatsCollector.RecordAnnouncement(r4);

            Assert.AreEqual(2, this.mStatsCollector.GetAnnouncement().Count);
            Assert.AreEqual(1, this.mStatsCollector.GetAnnouncement()[r1].Count);
            Assert.AreEqual(2, this.mStatsCollector.GetAnnouncement()[r3].Count);

            this.mStatsCollector.Clear();
        }

        [Test]
        public void TestRecordQuery()
        {
            this.mStatsCollector.RecordQuery(PeerAddress.GetAddress("127.0.0.1", 1, NatType.Open), new ServiceType(ServerType.Arbitration));
            this.mStatsCollector.RecordQuery(PeerAddress.GetAddress("127.0.0.2", 1, NatType.Open), new ServiceType(ServerType.Arbitration));
            this.mStatsCollector.RecordQuery(PeerAddress.GetAddress("127.0.0.3", 1, NatType.Open), new ServiceType(ServerType.Arbitration));
            this.mStatsCollector.RecordQuery(PeerAddress.GetAddress("127.0.0.3", 1, NatType.Open), new ServiceType(ServerType.Arbitration));

            Assert.AreEqual(3, this.mStatsCollector.GetRecordedQuery().Count);

            this.mStatsCollector.Clear();
        }

        [Test]
        public void TestTotalMethods()
        {
            ServiceDetailsRecord r1 = new ServiceDetailsRecord(
                PeerAddress.GetAddress("127.0.0.1", 1, NatType.Open),
                new ServiceType(ServerType.Arbitration),
                this.timeKeeper);
            ServiceDetailsRecord r3 = new ServiceDetailsRecord(
                PeerAddress.GetAddress("127.0.0.1", 1, NatType.Open),
                new ServiceType(ServerType.Overload),
                this.timeKeeper);
            ServiceDetailsRecord r4 = new ServiceDetailsRecord(
                PeerAddress.GetAddress("127.0.0.1", 1, NatType.Open),
                new ServiceType(ServerType.Overload),
                this.timeKeeper);

            this.mStatsCollector.RecordAnnouncement(r1);
            this.mStatsCollector.RecordAnnouncement(r3);
            this.mStatsCollector.RecordAnnouncement(r4);

            this.eventQueue.RunFor(TimeSpan.FromSeconds(1));
            Assert.AreEqual(2, this.mStatsCollector.GetTotalNumberOfServiceHost());

            this.mStatsCollector.RecordQuery(PeerAddress.GetAddress("127.0.0.1", 1, NatType.Open), new ServiceType(ServerType.Arbitration));
            this.mStatsCollector.RecordSuccessfulQuery(PeerAddress.GetAddress("127.0.0.1", 1, NatType.Open), new ServiceType(ServerType.Arbitration));
            this.mStatsCollector.RecordQuery(PeerAddress.GetAddress("127.0.0.2", 1, NatType.Open), new ServiceType(ServerType.Arbitration));
            this.mStatsCollector.RecordSuccessfulQuery(PeerAddress.GetAddress("127.0.0.2", 1, NatType.Open), new ServiceType(ServerType.Arbitration));
            this.mStatsCollector.RecordQuery(PeerAddress.GetAddress("127.0.0.3", 1, NatType.Open), new ServiceType(ServerType.Arbitration));
            this.mStatsCollector.RecordSuccessfulQuery(PeerAddress.GetAddress("127.0.0.3", 1, NatType.Open), new ServiceType(ServerType.Arbitration));
            this.eventQueue.RunFor(TimeSpan.FromSeconds(6));
            this.mStatsCollector.RecordQuery(PeerAddress.GetAddress("127.0.0.3", 1, NatType.Open), new ServiceType(ServerType.Arbitration));

            Assert.AreEqual(4, this.mStatsCollector.GetNumberOfQuery());
            Assert.AreEqual(3, this.mStatsCollector.GetNumberOfSuccessfulQuery());
        }
    }
}
