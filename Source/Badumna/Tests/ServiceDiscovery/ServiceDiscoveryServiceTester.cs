﻿using System;
using System.Collections.Generic;
using System.Text;

using Badumna;
using Badumna.Core;
using Badumna.DistributedHashTable;
using Badumna.ServiceDiscovery;
using Badumna.Utilities;
using NUnit.Framework;
using BadumnaTests.DistributedHashTable.Services;
using Badumna.Security;
using Rhino.Mocks;

namespace BadumnaTests.ServiceDiscovery
{
    [TestFixture]
    [DontPerformCoverage]
    public class ServiceDiscoveryServiceTester
    {
        MockLoopbackRouter mRouter;
        DhtFacade mFacade;
        PeerAddress mAddress;
        MockServiceDiscoveryService mDiscoveryService;
        PeerAddress mFakeAddress = PeerAddress.GetAddress("1.2.3.4", 1, NatType.Open);

        private Simulator eventQueue;
        private ITime timeKeeper;
        private TokenCache tokens;
        private INetworkConnectivityReporter connectivityReporter;
        private INetworkAddressProvider addressProvider;

        [SetUp]
        public void Initialize()
        {
            this.eventQueue = new Simulator();
            this.timeKeeper = this.eventQueue;

            this.connectivityReporter = MockRepository.GenerateStub<INetworkConnectivityReporter>();
            this.connectivityReporter.Stub(x => x.Status).Return(ConnectivityStatus.Online);

            this.tokens = new TokenCache(this.eventQueue, this.timeKeeper, this.connectivityReporter);
            this.tokens.SetIdentityProvider(new UnverifiedIdentityProvider());
            this.tokens.PreCache();

            this.mAddress = PeerAddress.GetLoopback(1);
            this.addressProvider = MockRepository.GenerateStub<INetworkAddressProvider>();
            this.addressProvider.Stub(x => x.PublicAddress).Return(this.mAddress);

            this.mRouter = new MockLoopbackRouter(this.eventQueue, this.addressProvider);
            this.mFacade = new DhtFacade(this.mRouter, this.eventQueue, this.timeKeeper, this.addressProvider, this.connectivityReporter, ServiceDiscoveryManagerTester.MakeServiceDiscoveryFactory(this.timeKeeper));
            this.mRouter.DhtFacade = this.mFacade;

            Assert.AreEqual(this.mFacade, this.mRouter.DhtFacade);
            Assert.AreEqual(HashKey.Min, this.mRouter.LocalHashKey);
        }

        [Test]
        [Category("Slow")]
        public void TestExcludes()
        {
            this.mDiscoveryService = new MockServiceDiscoveryService(this.mFacade, true, this.eventQueue, this.timeKeeper, this.addressProvider, this.connectivityReporter, this.tokens);
            this.mFacade.ForgeMethodList();
            this.mDiscoveryService.Start();

            // announce two services
            this.mDiscoveryService.AnnounceService(new ServiceDescription(ServerType.Arbitration));
            this.mDiscoveryService.AnnounceFakeService(this.mFakeAddress, new ServiceDescription(ServerType.Arbitration));
            this.mDiscoveryService.RegisterInterestedService(new ServiceDescription(ServerType.Arbitration));
            this.mDiscoveryService.ForceCheckDht();
            this.eventQueue.RunFor(TimeSpan.FromSeconds(70));

            // always try to return different services
            PeerAddress address1 = this.mDiscoveryService.QueryService(new ServiceDescription(ServerType.Arbitration));
            PeerAddress address2 = this.mDiscoveryService.QueryService(new ServiceDescription(ServerType.Arbitration));
            Assert.IsNotNull(address1);
            Assert.IsNotNull(address2);
            Assert.AreNotEqual(address1, address2);

            // but if we can't, just return some previously returned service
            PeerAddress address3 = this.mDiscoveryService.QueryService(new ServiceDescription(ServerType.Arbitration));
            Assert.IsNotNull(address3);
            Assert.IsTrue(address1.Equals(address3) || address2.Equals(address3));
        }

        [Test]
        [Category("Slow")]
        public void TestLastRefreshPolicy()
        {
            TestPolicy(ServiceSelectionPolicyType.LastRefreshed, this.mFakeAddress);
        }

        [Test]
        [Category("Slow")]
        public void TestFirstAvailablePolicy()
        {
            TestPolicy(ServiceSelectionPolicyType.FirstAvailable, this.mAddress);
        }

        /*[Test]
        public void TestRandomPolicy()
        {
            this.mDiscoveryService = new MockServiceDiscoveryService(this.mFacade);
            this.mFacade.ForgeMethodList();
            this.mDiscoveryService.Start();

            PeerAddress[] addresses = new PeerAddress[5000];
            
            // announce a service and wait for 10 seconds
            this.mDiscoveryService.AnnounceService(new ServiceDescription(ServerType.Arbitration));
            this.mDiscoveryService.RegisterInterestedService(new ServiceDescription(ServerType.Arbitration));
            this.mDiscoveryService.ForceCheckDht();
            Simulator.Instance.RunFor(TimeSpan.FromSeconds(70));

            PeerAddress address1 = this.mDiscoveryService.QueryService(new ServiceDescription(ServerType.Arbitration));
            Assert.IsNotNull(address1);

            // announce another service
            this.mDiscoveryService.AnnounceFakeService(this.mFakeAddress, new ServiceDescription(ServerType.Arbitration));
            Simulator.Instance.RunFor(TimeSpan.FromSeconds(2));
            // force populate
            this.mDiscoveryService.ForceCheckDht();
            this.mDiscoveryService.ForcePopulate();
            Simulator.Instance.RunFor(TimeSpan.FromSeconds(5));
            this.mDiscoveryService.ClearMemory();

            for (int i = 0; i < addresses.Length; i++)
            {
                PeerAddress address2 = this.mDiscoveryService.QueryService(new ServiceDescription(ServerType.Arbitration), ServiceSelectionPolicyType.Random);
                Assert.IsNotNull(address2);
                addresses[i] = address2;
            }

            int address1Count = 0;
            int address2Count = 0;

            for (int i = 0; i < addresses.Length; i++)
            {
                if (addresses[i].Equals(this.mAddress))
                {
                    address1Count++;
                }

                if (addresses[i].Equals(this.mFakeAddress))
                {
                    address2Count++;
                }
            }

            // are those results random enough? 
            Assert.AreEqual(addresses.Length, address1Count + address2Count);
            Assert.IsTrue(address1Count > (addresses.Length * 4 / 10));
            Assert.IsTrue(address2Count > (addresses.Length * 4 / 10));
        }*/

        private void TestPolicy(ServiceSelectionPolicyType policy, PeerAddress expectedAddress)
        {
            this.mDiscoveryService = new MockServiceDiscoveryService(this.mFacade, true, this.eventQueue, this.timeKeeper, this.addressProvider, this.connectivityReporter, this.tokens);
            this.mFacade.ForgeMethodList();
            this.mDiscoveryService.Start();
            // announce a service and wait for 10 seconds
            this.mDiscoveryService.AnnounceService(new ServiceDescription(ServerType.Arbitration));
            this.mDiscoveryService.RegisterInterestedService(new ServiceDescription(ServerType.Arbitration));
            this.mDiscoveryService.ForceCheckDht();
            this.eventQueue.RunFor(TimeSpan.FromSeconds(70));

            PeerAddress address1 = this.mDiscoveryService.QueryService(new ServiceDescription(ServerType.Arbitration));
            Assert.IsNotNull(address1);

            // announce another service
            this.mDiscoveryService.AnnounceFakeService(this.mFakeAddress, new ServiceDescription(ServerType.Arbitration));
            this.eventQueue.RunFor(TimeSpan.FromSeconds(2));
            // force populate
            this.mDiscoveryService.ForceCheckDht();
            this.mDiscoveryService.ForcePopulate();
            this.eventQueue.RunFor(TimeSpan.FromSeconds(5));
            this.mDiscoveryService.ClearMemory();
            PeerAddress address2 = this.mDiscoveryService.QueryService(new ServiceDescription(ServerType.Arbitration), policy);
            Assert.IsNotNull(address2);

            Assert.AreEqual(address1, this.mAddress);
            Assert.AreEqual(address2, expectedAddress);
            Assert.AreNotEqual(this.mAddress, this.mFakeAddress);
        }
    }
}
