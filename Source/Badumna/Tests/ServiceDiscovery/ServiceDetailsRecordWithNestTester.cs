﻿//------------------------------------------------------------------------------
// <copyright file="ServiceDetailsRecordWithNestTester.cs" company="National ICT Australia Limited">
//     Copyright (c) 2012 National ICT Australia Limited. All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using Badumna.Core;
using Badumna.ServiceDiscovery;
using Badumna.Utilities;
using BadumnaTests.Core;
using NUnit.Framework;

namespace BadumnaTests.ServiceDiscovery
{
    internal class ServiceDetailsRecordWithNestTester : ParseableTester<ServiceDetailsRecord>
    {
        private ITime timeKeeper;
        private Nest nest;

        [SetUp]
        public void SetUp()
        {
            this.timeKeeper = new NetworkEventQueue();
            this.nest = new Nest();
            this.nest.Set<ITime>(this.timeKeeper);
        }

        public override ICollection<ServiceDetailsRecord> CreateExpectedValues()
        {
            return new List<ServiceDetailsRecord>
                {
                     new ServiceDetailsRecord(PeerAddress.GetAddress("127.0.0.1", 1, NatType.Open), new ServiceType(ServerType.Arbitration), this.timeKeeper),
                     new ServiceDetailsRecord(PeerAddress.GetAddress("127.0.0.1", 1, NatType.Open), new ServiceType(ServerType.Overload), this.timeKeeper),
                     new ServiceDetailsRecord(PeerAddress.GetAddress("127.0.0.2", 42, NatType.FullCone), new ServiceType(ServerType.Overload), this.timeKeeper),
                };
        }

        protected override object ConstructForDeserialization(Type type)
        {
            return this.nest.Construct(type);
        }
    }
}
