﻿using System;
using System.Collections.Generic;
using Badumna.Core;
using Badumna.ServiceDiscovery;
using BadumnaTests.Core;
using NUnit.Framework;

namespace BadumnaTests.ServiceDiscovery
{
    [TestFixture]
    [DontPerformCoverage]
    internal class ServiceDetailsRecordTester : ParseableTester<ServiceDetailsRecord>
    {
        private ITime timeKeeper;

        [SetUp]
        public void SetUp()
        {
            NetworkEventQueue eventQueue = new NetworkEventQueue();
            this.timeKeeper = eventQueue;
        }

        public override ICollection<ServiceDetailsRecord> CreateExpectedValues()
        {
            return new List<ServiceDetailsRecord>
                {
                     new ServiceDetailsRecord(PeerAddress.GetAddress("127.0.0.1", 1, NatType.Open), new ServiceType(ServerType.Arbitration), this.timeKeeper),
                     new ServiceDetailsRecord(PeerAddress.GetAddress("127.0.0.1", 1, NatType.Open), new ServiceType(ServerType.Overload), this.timeKeeper),
                     new ServiceDetailsRecord(PeerAddress.GetAddress("127.0.0.2", 42, NatType.FullCone), new ServiceType(ServerType.Overload), this.timeKeeper),
                };
        }

        protected override object ConstructForDeserialization(Type type)
        {
            return new ServiceDetailsRecord(this.timeKeeper);
        }

        [Test]
        public void TestEquals()
        {
            ServiceDetailsRecord r1 = new ServiceDetailsRecord(
                PeerAddress.GetAddress("127.0.0.1", 1, NatType.Open),
                new ServiceType(ServerType.Arbitration),
                this.timeKeeper);
            ServiceDetailsRecord r2 = new ServiceDetailsRecord(
                PeerAddress.GetAddress("127.0.0.1", 1, NatType.Open),
                new ServiceType(ServerType.Overload),
                this.timeKeeper);
            ServiceDetailsRecord r3 = new ServiceDetailsRecord(
                PeerAddress.GetAddress("127.0.0.1", 1, NatType.Open),
                new ServiceType(ServerType.Arbitration),
                this.timeKeeper);

            Assert.AreNotEqual(r1, r2);
            Assert.AreNotEqual(r2, r1);

            Assert.AreNotEqual(r2, r3);
            Assert.AreNotEqual(r3, r2);

            Assert.AreEqual(r1, r3);
            Assert.AreEqual(r3, r1);

            r1 = new ServiceDetailsRecord(
                PeerAddress.GetAddress("127.0.0.1", 1, NatType.Open),
                new ServiceType(ServerType.Arbitration),
                this.timeKeeper);
            r2 = new ServiceDetailsRecord(
                PeerAddress.GetAddress("127.0.0.2", 1, NatType.Open),
                new ServiceType(ServerType.Arbitration),
                this.timeKeeper);

            Assert.AreNotEqual(r1, r2);

            r1 = new ServiceDetailsRecord(
                PeerAddress.GetAddress("127.0.0.1", 1, NatType.Open),
                new ServiceType(ServerType.Arbitration),
                this.timeKeeper);
            r2 = new ServiceDetailsRecord(
                PeerAddress.GetAddress("127.0.0.1", 2, NatType.Open),
                new ServiceType(ServerType.Arbitration),
                this.timeKeeper);

            Assert.AreNotEqual(r1, r2);
        }

        [Test]
        public void TestToFromMessage()
        {
            ServiceDetailsRecord r1 = new ServiceDetailsRecord(
                PeerAddress.GetAddress("127.0.0.1", 1, NatType.Open),
                new ServiceType(ServerType.Arbitration),
                this.timeKeeper);
            ServiceDetailsRecord r2 = new ServiceDetailsRecord(this.timeKeeper);

            Assert.AreNotEqual(r1, r2);

            MessageBuffer message = new MessageBuffer();
            r1.ToMessage(message, typeof(ServiceDetailsRecord));
            r2.FromMessage(message);

            Assert.AreEqual(r1, r2);
        }

        [Test]
        public void TestListOperation()
        {
            ServiceDetailsRecord r1 = new ServiceDetailsRecord(
                PeerAddress.GetAddress("127.0.0.1", 1, NatType.Open),
                new ServiceType(ServerType.Arbitration),
                this.timeKeeper);
            ServiceDetailsRecord r3 = new ServiceDetailsRecord(
                PeerAddress.GetAddress("127.0.0.1", 1, NatType.Open),
                new ServiceType(ServerType.Overload),
                this.timeKeeper);
            ServiceDetailsRecord r4 = new ServiceDetailsRecord(
                PeerAddress.GetAddress("127.0.0.2", 1, NatType.Open),
                new ServiceType(ServerType.Arbitration),
                this.timeKeeper);
            ServiceDetailsRecord r5 = new ServiceDetailsRecord(
                PeerAddress.GetAddress("127.0.0.3", 1, NatType.Open),
                new ServiceType(ServerType.Arbitration),
                this.timeKeeper);
            ServiceDetailsRecord r6 = new ServiceDetailsRecord(
                PeerAddress.GetAddress("127.0.0.4", 1, NatType.Open),
                new ServiceType(ServerType.Arbitration),
                this.timeKeeper);
            ServiceDetailsRecord r7 = new ServiceDetailsRecord(
                PeerAddress.GetAddress("127.0.0.4", 1, NatType.Open),
                new ServiceType(ServerType.Arbitration),
                this.timeKeeper);

            List<ServiceDetailsRecord> list = new List<ServiceDetailsRecord>();
            list.Add(r1);
            list.Add(r3);
            list.Add(r4);
            list.Add(r5);
            list.Add(r6);

            Assert.AreEqual(5, list.Count);
            Assert.IsTrue(list.Contains(r6));

            list.Remove(r7);
            Assert.AreEqual(4, list.Count);
            Assert.IsFalse(list.Contains(r6));
        }
    }
}
