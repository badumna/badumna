﻿using System;
using System.Collections.Generic;
using System.Text;
using Badumna;
using Badumna.ServiceDiscovery;
using NUnit.Framework;
using Badumna.Utilities;
using Badumna.Core;

namespace BadumnaTests.ServiceDiscovery
{
    [TestFixture]
    [DontPerformCoverage]
    public class ServiceRecordStoreTester
    {
        private ServiceRecordStore store;
        private Simulator eventQueue;
        private ITime timeKeeper;

        [SetUp]
        public void SetUp()
        {
            this.eventQueue = new Simulator();
            this.timeKeeper = this.eventQueue;
            store = new ServiceRecordStore(this.timeKeeper);
        }

        [Test]
        public void TestConstructor()
        {
            ServiceRecordStore store = new ServiceRecordStore(this.timeKeeper);
            Assert.AreEqual(0, store.Size);
        }

        [Test]
        public void TestAddRecords()
        {
            ServiceDetailsRecord r1 = new ServiceDetailsRecord(
                PeerAddress.GetAddress("127.0.0.1", 1, NatType.Open),
                new ServiceType(ServerType.Arbitration),
                this.timeKeeper);
            ServiceDetailsRecord r2 = new ServiceDetailsRecord(
                PeerAddress.GetAddress("127.0.0.3", 1, NatType.Open),
                new ServiceType(ServerType.Overload),
                this.timeKeeper);
            ServiceDetailsRecord r3 = new ServiceDetailsRecord(
                PeerAddress.GetAddress("127.0.0.2", 1, NatType.Open),
                new ServiceType(ServerType.Arbitration),
                this.timeKeeper);

            store.AddRecord(r1);
            store.AddRecord(r2);
            store.AddRecord(r3);

            Assert.AreEqual(2, store.GetServiceRecords(ServerType.Arbitration, "default").Count);
            Assert.AreEqual(1, store.GetServiceRecords(ServerType.Overload, "default").Count);

            ServiceDetailsRecord r4 = new ServiceDetailsRecord(
                PeerAddress.GetAddress("127.0.0.4", 1, NatType.Open),
                new ServiceType(ServerType.Arbitration, "attack"),
                this.timeKeeper);
            ServiceDetailsRecord r5 = new ServiceDetailsRecord(
                PeerAddress.GetAddress("127.0.0.5", 1, NatType.Open),
                new ServiceType(ServerType.Arbitration, "attack"),
                this.timeKeeper);
            ServiceDetailsRecord r6 = new ServiceDetailsRecord(
                PeerAddress.GetAddress("127.0.0.6", 1, NatType.Open),
                new ServiceType(ServerType.Arbitration, "attack"),
                this.timeKeeper);

            store.AddRecord(r4);
            store.AddRecord(r5);
            store.AddRecord(r6);

            Assert.AreEqual(2, store.GetServiceRecords(ServerType.Arbitration, "default").Count);
            Assert.AreEqual(3, store.GetServiceRecords(ServerType.Arbitration, "attack").Count);

            store.Clear();
        }

        /*
        [Test]
        public void TestRefresh()
        {
            ServiceDetailsRecord r1 = new ServiceDetailsRecord(PeerAddress.GetAddress("127.0.0.1", 1, NatType.Open), new ServiceType(ServerType.Arbitration));
            ServiceDetailsRecord r2 = new ServiceDetailsRecord(PeerAddress.GetAddress("127.0.0.3", 1, NatType.Open), new ServiceType(ServerType.Overload));
            ServiceDetailsRecord r3 = new ServiceDetailsRecord(PeerAddress.GetAddress("127.0.0.2", 1, NatType.Open), new ServiceType(ServerType.Arbitration));

            store.AddRecord(r1);
            store.AddRecord(r2);
            store.AddRecord(r3);

            List<ServiceDetailsRecord> list = store.GetServiceRecords(ServerType.Overload, "default");
            r1 = list[0];
            DateTime oldRrefreshTime = r1.LastRefreshTime;
            Simulator.Instance.RunFor(TimeSpan.FromSeconds(100));

            r2 = new ServiceDetailsRecord(PeerAddress.GetAddress("127.0.0.3", 1, NatType.Open), new ServiceType(ServerType.Overload));
            store.AddRecord(r2);

            list = store.GetServiceRecords(ServerType.Overload);
            Assert.AreEqual(1, list.Count);
            r3 = list[0];
            TimeSpan interval = r3.LastRefreshTime - oldRrefreshTime;

            Assert.IsTrue(interval.TotalSeconds >= 99 && interval.TotalSeconds <= 101);
        }*/

        [Test]
        public void TestRemove()
        {
            ServiceDetailsRecord r1 = new ServiceDetailsRecord(
                PeerAddress.GetAddress("127.0.0.1", 1, NatType.Open),
                new ServiceType(ServerType.Arbitration),
                this.timeKeeper);
            ServiceDetailsRecord r2 = new ServiceDetailsRecord(
                PeerAddress.GetAddress("127.0.0.3", 1, NatType.Open),
                new ServiceType(ServerType.Overload),
                this.timeKeeper);
            ServiceDetailsRecord r3 = new ServiceDetailsRecord(
                PeerAddress.GetAddress("127.0.0.2", 1, NatType.Open),
                new ServiceType(ServerType.Arbitration),
                this.timeKeeper);

            store.AddRecord(r1);
            store.AddRecord(r2);
            store.AddRecord(r3);

            ServiceDetailsRecord r4 = new ServiceDetailsRecord(
                PeerAddress.GetAddress("127.0.0.2", 1, NatType.Open),
                new ServiceType(ServerType.Arbitration),
                this.timeKeeper);
            List<ServiceDetailsRecord> list = store.GetServiceRecords(ServerType.Arbitration);
            Assert.AreEqual(2, list.Count);
            store.RemoveRecord(r4);
            Assert.AreEqual(2, list.Count);
            list = store.GetServiceRecords(ServerType.Arbitration);
            Assert.AreEqual(1, list.Count);
        }

        [Test]
        public void TestGarbageCollection()
        {
            ServiceDetailsRecord r1 = new ServiceDetailsRecord(
                PeerAddress.GetAddress("127.0.0.1", 1, NatType.Open),
                new ServiceType(ServerType.Arbitration),
                this.timeKeeper);
            ServiceDetailsRecord r2 = new ServiceDetailsRecord(
                PeerAddress.GetAddress("127.0.0.3", 1, NatType.Open),
                new ServiceType(ServerType.Overload),
                this.timeKeeper);
            ServiceDetailsRecord r3 = new ServiceDetailsRecord(
                PeerAddress.GetAddress("127.0.0.2", 1, NatType.Open),
                new ServiceType(ServerType.Arbitration),
                this.timeKeeper);

            store.AddRecord(r1);
            store.AddRecord(r2);
            store.AddRecord(r3);

            Assert.AreEqual(3, store.Count);

            this.eventQueue.RunFor(Parameters.ServiceDiscoveryDHTRecordTTL);
            this.eventQueue.RunFor(TimeSpan.FromSeconds(1));
            store.GarbageCollection();
            Assert.AreEqual(0, store.Count);
        }
    }
}
