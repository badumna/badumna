﻿//-----------------------------------------------------------------------
// <copyright file="SetupFixture.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

// Uncomment the line below to get Badumna log messages on the console when
// running nUnit tests.
////#define ENABLE_NUNIT_LOGGING

// Global setup fixtures are not run when running tests in the NUnit GUI runner
// using the /fixture option. For this reason, it is best to provide SetUpFixtures
// in each namespace.  Global setup is run if the /run option is used instead of
// the /fixture option.
// If there is a fix or satisfactory workaround for this problem, then global setup
// like that commented out below can be put here.
using System;
using System.Diagnostics;
using Badumna;
using Badumna.Core;
using Badumna.Utilities;
using NUnit.Framework;

/// <summary>
/// Global fixture setup for all tests in assembly.
/// Has to be declared in the global namespace.
/// </summary>
[SetUpFixture]
[DontPerformCoverage]
public class SetUpFixture
{
    // Field to hold exisiting trace listeners so they can be restored after test are run.
    private TraceListener[] originalListeners = null;

    // A trace listener to use during testing.
    private TraceListener nunitListener = new NUnitListener();

    [SetUp]
    public void SetUp()
    {
        // Replace existing listeners with listener for testing.
        this.originalListeners = new TraceListener[Trace.Listeners.Count];
        Trace.Listeners.CopyTo(this.originalListeners, 0);
        Trace.Listeners.Clear();
        Trace.Listeners.Add(this.nunitListener);

#if ENABLE_NUNIT_LOGGING
        LoggerModule logOptions = new LoggerModule(null);
        logOptions.LoggerType = LoggerType.Console;
        logOptions.LogLevel = LogLevel.Information;
        logOptions.LogTimestamp = true;
        Logger.Configure(logOptions);
#endif
    }

    [TearDown]
    public void TearDown()
    {
        // Restore original trace listeners.
        Trace.Listeners.Remove(this.nunitListener);
        Trace.Listeners.AddRange(this.originalListeners);
    }

    /// <summary>
    /// This listener ensures that failed System.Diagnostics assertions cause tests to fail.
    /// </summary>
    public class NUnitListener : DefaultTraceListener
    {
        public override void Fail(string message)
        {
            Assert.Fail("Debug.Assert() failed: " + message);
        }

        public override void Fail(string message, string detailMessage)
        {
            Assert.Fail("Debug.Assert() failed: " + message + " [" + detailMessage + "]");
        }

        public override void Write(string message)
        {
            Console.Write(message);
        }

        public override void WriteLine(string message)
        {
            Console.WriteLine(message);
        }
    }
}
