﻿
import System
from ScriptSim import Sim, MainWindow, RunSettings
from Visualizer import SourceConfig, GraphConfig
from NetworkSimulator import DiagnosticsHelper, NetworkInitializer, DhtPeer
from pysim import add_graph, push, push_repeat, push_n_shot, graph_parse_bytes, graph_call_counts


# A helper class used to create a simulated network.
class Initializer:
    # Constructor.
    # peer_factory : A function that takes a SimulatorFacade as an argument and resturns a new instance of 
    #                any type derived from BadumnaPeer
    # stack_type : A value of SimulatedStackType which indicates the type of protocol stack to use.
    # message_tracer : Whether to use the message tracer which displays the sequence diagram   
    def __init__(self, peer_factory, stack_type, message_tracer=False):
        self._stack_type = stack_type
        self._peer_factory = peer_factory
        self._peers = {}
        self._network_intializer = NetworkInitializer()
        self._network_intializer.SetDefaults()
        
        if (message_tracer):
            self._network_intializer.Topology.MessageTracer = Sim.Current.MessageTracer
            
        self._network_intializer.Topology.Load("mking-t.txt")  
        self._network_intializer.NumberOfInitialPeers = 1;
        self._network_intializer.UseSeedInitializer = True;
        self._next_node_number = 0
        self._random = System.Random()

    # Set the characteristics of the node lifetime in the simulation.
    # Nodes will start and stay in the online state for a given period of time before changing to the offline state
    # for a given amount of time. The actual value of uptime and downtime of each node is chosen so that the average 
    # and standard deviation matches the values given here. A standard deviation value of 0 will ensure that all nodes
    # have exatly the average uptime and downtime
    # average_uptime : The average time in seconds that any given node will be online for
    # average_downtime : The average time in seconds that any given node will be offline for
    # std_deviation_uptime : The amount of time in seconds of deviation from the average uptime
    # std_deviation_downtime : The amount of time in seconds of deviation from the average downtime
    def set_node_lifetime(self, average_uptime, average_downtime, std_deviation_uptime = 0, std_deviation_downtime = 0):
        self._network_intializer.AverageUpTimeMilliseconds = average_uptime * 1000
        self._network_intializer.AverageDownTimeMilliseconds = average_downtime * 1000 
        self._network_intializer.StdDevUpTimeMilliseconds = std_deviation_uptime * 1000
        self._network_intializer.StdDevDownTimeMilliseconds = std_deviation_downtime * 1000
        
    # Node initializer event handler. Called when a node has been initialized during a calls from NodeInitializer.InitializeNode*() methods.
    # sender : The instance of the calling NodeInitializer class
    # inti_args : the NodeInitializationEventArgs containing info relevent to the newly created node
    def _initialize_node(self, sender, init_args):
        facade = init_args.SimulatorFacade
        facade.CreateStack(self._stack_type)
        
        peer = self._peer_factory(facade)
        self._peers[init_args.NodeNumber] = peer
        
        if init_args.NodeNumber == 0:
            facade.LockOnline()         # Ensure that the seed peer is always online. 
        
        if self._helper is not None:
            self._helper.Add(peer)
            
        peer.Start() 

    # Initialize a simulation run using the given number of peers and start delay.
    # num_peers : The number of peers involved in the simulation.
    # peer_start_delay : The time delay between peers starting
    # has_full_connectivity : Indicates whether to ignore connectivity gaps in the topology. If false some peers may not be 
    #                         able to send to each other.                      
    def _initialize_run(self, num_peers, peer_start_delay, has_full_connectivity=True):
        self._network_intializer.Topology.HasFullConectivity = has_full_connectivity
        self._network_intializer.NumberOfNodes = num_peers
        self._network_intializer.Reset()  # here because InitializeNode*s* calls it, and want to make sure we have the same behaviour in both cases
        
        if peer_start_delay == 0:
            self._network_intializer.InitializeNodes(self._initialize_node)  # also resets sim
        else:
            def tmp():
                self._network_intializer.InitializeNode(self._initialize_node, self._next_node_number)
                self._next_node_number += 1
            push_n_shot(tmp, peer_start_delay, num_peers)

    # Initialize and perform a simulation run using the given parameters
    # duration : The amount of simulated time to run for.
    # run_settings : An instance of RunSettings given to the simulator
    # num_peers : The number of peers involved in the simulation.
    # peer_start_delay : The time delay between peers starting
    # has_full_connectivity : Indicates whether to ignore connectivity gaps in the topology. If false some peers may not be 
    #                         able to send to each other.                              
    # report_helper : A ReportRunHelper instance which handles the reporting of the run. 
    # runt_title : The title of the run used in the report    
    def initialize_run(self, run_settings, num_peers, peer_start_delay,  has_full_connectivity=True, report_helper = None, run_title = None):
        sim = Sim.Current
        
        if run_settings is None: 
	        sim.Reset()
        else:
            sim.Reset(run_settings)
            
        self._helper = sim.Helper
        self._next_node_number = 0
        self._initialize_run(num_peers, peer_start_delay, has_full_connectivity)

        if report_helper is not None:
            report_helper.StartRunReport(run_title, sim)
                
        return sim
        
    def get_random_peer(self):
        if len(self._peers) == 0:
            return None
	    
        random_peer = self._peers[self._random.Next(len(self._peers)-1)]
        if random_peer.NetworkFacade.IsOnline and random_peer.IsRouting:
            return random_peer
        return self._peers[0]
        	
    def get_first_peer(self):
        if len(self._peers) == 0:
            return None
	    
        return self._peers[0]
        
        
class ReportRunHelper:
    def __init__(self, report, sample_period_ms, show_parse_bytes=True, show_call_counts=True):
        self._report = report
        self._show_parse_bytes = show_parse_bytes
        self._show_call_counts = show_call_counts
        self._sample_period_ms = sample_period_ms
        self._graphs = {}
    
    def StartRunReport(self, run_title, sim):        
        self._report.Start(run_title)
        if self._show_parse_bytes:
            sources = {}
            def tmp(*args):
                graph_parse_bytes(run_title, self._report, self._graphs, sources)
            sim.Helper.NewStatistics += tmp
        
        if self._show_call_counts:
            sources = {}
            def tmp(*args):
                graph_call_counts(run_title, self._report, self._graphs, sources)
            sim.Helper.NewStatistics += tmp
                
        sim.RepeatGetStatistics(self._sample_period_ms)

    def add_graph(self, source, title):        
	    graph = GraphConfig(title, "Time", "Time")
	    graph.Sources.Add(source)
	    self._report.AddGraph(graph, False)


def make_dht_report(title):
    report = MainWindow.Current.AddReport(title)
    #add_graph(report, "Average Inbound Bandwidth", "kb/s", "AvgInbound")
    add_graph(report, "Average Outbound Bandwidth", "kb/s", "AvgOutbound")
    #add_graph(report, "Maximum Inbound Bandwidth", "kb/s", "MaxInbound")
    add_graph(report, "Maximum Outbound Bandwidth", "kb/s", "MaxOutbound")
    add_graph(report, "Peer Count", "peers", "PeerCount")
    return report



if __name__ == '__main__':
    run_length_ms = 120000000
    sample_period_ms = 10000
    peer_start_delay = 10000

    report = make_dht_report("Dht traffic")
    report_helper = ReportRunHelper(report, sample_period_ms, False, False)
                        
    for num_peers in [200]:
        run_settings = RunSettings("DhtTests", str(num_peers) + " peers", True) # output directory, testname, save statistics
        intitializer = Initializer(DhtPeer, SimulatedStackType.Dht)  
        
        #intitializer.set_node_lifetime(run_length_ms / 1000, 0) # Set the lifetime to the length of the simulation to ensure all peers remain online 
                                                                # for the entire time.
        intitializer.set_node_lifetime(3600, 3600, 1800, 1800)
         
        test_runner = TestRunner(intitializer, report_helper)
        sim = intitializer.initialize_run(run_settings, num_peers, peer_start_delay, True, report_helper, str(num_peers) + " peers")
        test_runner.schedule_test(30000, 5000)
        sim.RunFor(run_length_ms)