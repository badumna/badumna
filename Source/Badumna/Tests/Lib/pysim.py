﻿
import System

from ScriptSim import Sim, MainWindow
from Visualizer import SourceConfig, GraphConfig, Graph

from Microsoft.Research.DynamicDataDisplay import ChartPlotter
from Microsoft.Research.DynamicDataDisplay.DataSources import EnumerableDataSource, CompositeDataSource
from System.Windows.Media import Colors


def push(time, func, *args):
    Sim.Current.Push(time, System.Action[object](lambda a: func(*a)), args)
    
def push_script(script):
    """Pushes a list of a events in the form (offset_msec, function, [arg0, [arg1 ...]])"""
    for step in script:
	    push(step[0], step[1], *step[2:])

def push_repeat(f, period_ms):
    push(period_ms, repeat, f, period_ms)

def repeat(f, period_ms):
    push_repeat(f, period_ms)
    f()

def push_n_shot(f, period_ms, repeat_count):
    push(period_ms, n_shot, f, period_ms, repeat_count)

def n_shot(f, period_ms, repeat_count):
    repeat_count -= 1
    if (repeat_count > 0):
        push_n_shot(f, period_ms, repeat_count)
    f()



def add_graph(report, title, y_axis, source_id):
    graph = GraphConfig(title, "Time", y_axis)
    graph.Sources.Add(SourceConfig.DefaultSources[source_id])
    report.AddGraph(graph)


def graph_parse_bytes(run_title, report, graphs, sources):
    time = Sim.Current.TimeSeconds
    stats = Sim.Current.Helper.StatsAggregator.Table

    layer_table = Sim.Current.Filter(stats, "Time = '" + str(time) + "' AND Group LIKE 'ParseBytes.*'", 'Sum', 'Group')
    layers = []
    for row in layer_table.Rows:
        layer_name = row[0][len('ParseBytes.'):]
        layers.append(layer_name)
    Sim.Current.ClearDataSet(stats)
        
    for layer in layers:
        if layer not in sources:
            if layer not in graphs:
                graph = MainWindow.Current.Construct[Graph]()
                graphs[layer] = graph
                def tmp():
                    graph.Title = layer
                    graph.YAxisLabel = 'bytes'
                    report.AddGraph(graph)
                MainWindow.Current.DispatchSync(tmp)
            
            source = SourceConfig(run_title, 'Sum(Value)', "Time = '$now' AND Group = 'ParseBytes." + layer + "'")
            sources[layer] = source
            graphs[layer].AddSeries(source.Name, source.Source)
            
    for source in sources:
        sources[source].Append(time, stats)


def graph_call_counts(run_title, report, graphs, sources):
    time = Sim.Current.TimeSeconds
    stats = Sim.Current.Helper.StatsAggregator.Table

    call_table = Sim.Current.Filter(stats, "Time = '" + str(time) + "' AND Group LIKE 'ParseCount.*'", 'Sum', 'Group', 'Name')
    functions = []
    for row in call_table.Rows:
        function_name = row[0][len('ParseCount.'):] + '.' + row[1]
        functions.append(function_name)
    Sim.Current.ClearDataSet(stats)
        
    for function in functions:
        if function not in sources:
            if function not in graphs:
                graph = MainWindow.Current.Construct[Graph]()
                graphs[function] = graph
                def tmp():
                    graph.Title = function
                    graph.YAxisLabel = 'count'
                    report.AddGraph(graph)
                MainWindow.Current.DispatchSync(tmp)
            
            group, name = function.split('.')
            source = SourceConfig(run_title, 'Sum(Value)', "Time = '$now' AND Group = 'ParseCount." + group + "' AND Name = '" + name + "'")
            sources[function] = source
            graphs[function].AddSeries(source.Name, source.Source)
            
    for source in sources:
        sources[source].Append(time, stats)


def make_standard_report(title):
    report = MainWindow.Current.AddReport(title)
    add_graph(report, "Average Inbound Bandwidth", "kb/s", "AvgInbound")
    add_graph(report, "Average Outbound Bandwidth", "kb/s", "AvgOutbound")
    add_graph(report, "Maximum Inbound Bandwidth", "kb/s", "MaxInbound")
    add_graph(report, "Maximum Outbound Bandwidth", "kb/s", "MaxOutbound")
    add_graph(report, "Average Interest Management", "kb/s", "AvgImBytes")
    add_graph(report, "Maximum Interest Management", "kb/s", "MaxImBytes")
    add_graph(report, "Total Missing Entities", "Entity count", "TotalMissing")
    add_graph(report, "Total Hallucinated Entities", "Entity count", "TotalHallucinated")
    add_graph(report, "Average Distance Error", "units", "AvgDistErr")
    add_graph(report, "Maximum Distance Error", "units", "MaxDistErr")
    add_graph(report, "IM requests", "requests", "GossipIMCalls")    
    add_graph(report, "Peer Count", "peers", "PeerCount")
    return report

def make_stack_graph(components, sample_period_ms):
    stacks = [components[0:x+1] for x in range(len(components))]
    
    graph = GraphConfig("IM bandwidth", "Time", "kb / s")
    for stack in stacks:
        filter = ' OR '.join(map(lambda x: "Group = 'ParseBytes." + x + "'", stack))
        # Avg(Value) to average the bytes over the peers so we get peer bandwidth
        source = SourceConfig(stack[-1], 'Avg(Value) * 8 / ' + str(1024 * sample_period_ms / 1000), "Time = '$now' AND (" + filter + ")")
        graph.Sources.Add(source)
        
    return graph
    

def validate_stats(stats):
    results = []
    obj_manager_count = stats.Compute('Count(Value)', "Group = 'ObjectManager' AND Name = 'MaximumDistanceError'") # should be same count for all obj manager stats
    if obj_manager_count == 0:
        return results
        
    results.append(("% of samples with zero maximum distance error",
        100.0 * stats.Compute('Count(Value)', "Group = 'ObjectManager' AND Name = 'MaximumDistanceError' AND Value = 0.0") / obj_manager_count))
    results.append(("Maxiumum distance error",
        stats.Compute('Max(Value)', "Group = 'ObjectManager' AND Name = 'MaximumDistanceError'")))
    results.append(("Average distance error",
        stats.Compute('Avg(Value)', "Group = 'ObjectManager' AND Name = 'AverageDistanceError' AND Value > 0.0")))
    
    results.append(("% of samples with no missing objects",
        100.0 * stats.Compute('Count(Value)', "Group = 'ObjectManager' AND Name = 'MissingObjects' AND Value = 0.0") / obj_manager_count))
    results.append(("Maximum missing objects",
        stats.Compute('Max(Value)', "Group = 'ObjectManager' AND Name = 'MissingObjects'")))
    results.append(("Average missing objects",
        stats.Compute('Avg(Value)', "Group = 'ObjectManager' AND Name = 'MissingObjects' AND Value > 0.0")))
    
    results.append(("% of samples with no hallucinated objects",
        100.0 * stats.Compute('Count(Value)', "Group = 'ObjectManager' AND Name = 'HallucinatedObjects' AND Value = 0.0") / obj_manager_count))
    results.append(("Maximum hallucinated objects",
        stats.Compute('Max(Value)', "Group = 'ObjectManager' AND Name = 'HallucinatedObjects'")))
    results.append(("Average hallucinated objects",
        stats.Compute('Avg(Value)', "Group = 'ObjectManager' AND Name = 'HallucinatedObjects' AND Value > 0.0")))
  
#//processor.AddCollator("Maximum distance error for samples with no missing or hallucinated objects", 
#//    new ConstrainedCollator(new Maximum(),
#//        new );
#

    return results


def line_graph(xPoints, yPoints):
    graph = MainWindow.Current.Construct[ChartPlotter]()

    yDataSource = EnumerableDataSource[System.Double](yPoints);
    yDataSource.SetYMapping(lambda y: y);

    xDataSource = EnumerableDataSource[System.Double](xPoints);
    xDataSource.SetXMapping(lambda x: x);

    compositeDataSource = CompositeDataSource(xDataSource, yDataSource);

    def f():
        graph.AddLineGraph(compositeDataSource, Colors.Goldenrod, 3, "Cosine")
        graph.FitToView()
    MainWindow.Current.Dispatch(f)
    
    return graph
