﻿//-----------------------------------------------------------------------
// <copyright file="GroupConfigTester.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System.Collections.Generic;

using Badumna.Core;
using Badumna.DataTypes;
using Badumna.Overload;
using NUnit.Framework;

namespace BadumnaTests.Overload
{
    [TestFixture]
    [DontPerformCoverage]
    public class GroupConfigTester
    {
        private GroupConfig groupConfig;

        [Test]
        public void TestGroupConfigAddAndRemove()
        {
            ITime timeKeeper = new NetworkEventQueue();
            this.groupConfig = new GroupConfig(timeKeeper);
            List<BadumnaId> ids = new List<BadumnaId>();
            List<BadumnaId> idsToRemove = new List<BadumnaId>();

            for (ushort i = 0; i < 10; i++)
            {
                ids.Add(new BadumnaId(PeerAddress.Nowhere, i));
                ids.Add(new BadumnaId(PeerAddress.Nowhere, i));

                if (i < 5)
                {
                    idsToRemove.Add(new BadumnaId(PeerAddress.Nowhere, i));
                }
            }

            Assert.AreEqual(ids.Count, 20);

            this.groupConfig.AddEntities(ids);
            Assert.AreEqual(this.groupConfig.GroupEntities.Count, 10);

            this.groupConfig.RemoveEntities(idsToRemove);
            Assert.AreEqual(this.groupConfig.GroupEntities.Count, 5);

            foreach (BadumnaId id in this.groupConfig.GroupEntities)
            {
                Assert.GreaterOrEqual(id.LocalId, 5);
            }

            this.groupConfig.RemoveEntities(ids);
            Assert.AreEqual(0, this.groupConfig.GroupEntities.Count);
        }

        [Test]
        [Category("Slow")]
        public void TestGroupConfigHash()
        {
            ITime timeKeeper = new NetworkEventQueue();
            this.groupConfig = new GroupConfig(timeKeeper);
            int hash1 = this.groupConfig.GroupConfigHash;
            List<int> hashCollection = new List<int>();
            hashCollection.Add(hash1);

            for (ushort i = 0; i < 500; i++)
            {
                BadumnaId entityId = new BadumnaId(PeerAddress.Nowhere, i);
                this.groupConfig.AddEntity(entityId);
                int hash = this.groupConfig.GroupConfigHash;
                Assert.IsFalse(hashCollection.Contains(hash));
                hashCollection.Add(hash);
            }
        }
    }
}
