﻿//-----------------------------------------------------------------------
// <copyright file="ForwardingManagerTester.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Net;
using System.Xml;
using Badumna;
using Badumna.Core;
using Badumna.DataTypes;
using Badumna.Overload;
using Badumna.Security;
using Badumna.Transport;
using Badumna.Utilities;
using BadumnaTests.Core;
using NUnit.Framework;
using Rhino.Mocks;

namespace BadumnaTests.Overload
{
    [TestFixture]
    [DontPerformCoverage]
    internal class ForwardingManagerTester
    {
        private TransportProtocol transportProtocol;
        private ConnectionTable connectionTable;

        private IMessageDispatcher<TransportEnvelope> messageDispatcher;
        private IMessageProducer<TransportEnvelope> messageProducer;

        private EnvelopeForwarder forwarder;
        private IEnvelopeForwarderFactory envelopeForwarderFactory;
        private IForwardingGroupFactory forwardingGroupFactory;

        private IForwardingGroup noWhereGroup;
        private IForwardingGroup validGroup;
        private PeerAddress validPeerAddress;
        private BadumnaId groupId;

        private NetworkEventQueue eventQueue;
        private NetworkEventQueue encryptionQueue;
        private ITime timeKeeper;
        private INetworkConnectivityReporter connectivityReporter;

        private INetworkAddressProvider addressProvider;

        private OverloadModule overloadOptions;

        private TokenCache tokens;

        [SetUp]
        public void SetUp()
        {
            this.eventQueue = new NetworkEventQueue();
            this.encryptionQueue = new NetworkEventQueue();
            this.timeKeeper = this.eventQueue;

            this.overloadOptions = new OverloadModule(null);

            this.messageDispatcher = MockRepository.GenerateMock<IMessageDispatcher<TransportEnvelope>>();
            this.messageProducer = MockRepository.GenerateMock<IMessageProducer<TransportEnvelope>>();

            this.addressProvider = MockRepository.GenerateMock<INetworkAddressProvider>();

            this.connectivityReporter = MockRepository.GenerateStub<INetworkConnectivityReporter>();

            this.tokens = new TokenCache(this.eventQueue, this.timeKeeper, this.connectivityReporter);
            this.tokens.SetIdentityProvider(new UnverifiedIdentityProvider());
            this.tokens.PreCache();

            GenericCallBackReturn<object, Type> factory = MessageParserTester.DefaultFactory;
            this.transportProtocol = MockRepository.GenerateMock<TransportProtocol>("foo", typeof(DhtProtocolMethodAttribute), factory);
            this.connectionTable = MockRepository.GenerateMock<ConnectionTable>(this.messageDispatcher, this.messageProducer, new SeedPeerFinder(new List<string>()), this.eventQueue, this.encryptionQueue, this.timeKeeper, this.addressProvider, this.connectivityReporter, "testAppName", this.tokens, factory);
            this.forwarder = MockRepository.GenerateMock<EnvelopeForwarder>(this.transportProtocol, this.connectionTable, this.overloadOptions, this.eventQueue, this.timeKeeper, this.connectivityReporter, this.addressProvider);
            this.envelopeForwarderFactory = MockRepository.GenerateStub<IEnvelopeForwarderFactory>();
            this.envelopeForwarderFactory.Stub(f => f.Create(null, null, null, null, null, null, null)).IgnoreArguments().Return(this.forwarder);

            this.groupId = new BadumnaId(PeerAddress.Nowhere, 1);
            this.validPeerAddress = new PeerAddress(new IPEndPoint(new IPAddress(new byte[] { 222, 222, 222, 222 }), 12345), NatType.Unknown);

            this.validGroup = MockRepository.GenerateMock<IForwardingGroup>();
            this.noWhereGroup = MockRepository.GenerateMock<IForwardingGroup>();

            this.forwardingGroupFactory = MockRepository.GenerateStub<IForwardingGroupFactory>();
            this.forwardingGroupFactory.Stub(f => f.Create(Arg.Is(this.forwarder), Arg.Is(PeerAddress.Nowhere), Arg.Is(this.groupId), Arg<OverloadModule>.Is.Anything, Arg.Is(this.eventQueue), Arg.Is(this.timeKeeper), Arg<INetworkAddressProvider>.Is.Anything, Arg<IPeerConnectionNotifier>.Is.Anything)).Return(this.noWhereGroup);
            this.forwardingGroupFactory.Stub(f => f.Create(Arg.Is(this.forwarder), Arg.Is(this.validPeerAddress), Arg.Is(this.groupId), Arg<OverloadModule>.Is.Anything, Arg.Is(this.eventQueue), Arg.Is(this.timeKeeper), Arg<INetworkAddressProvider>.Is.Anything, Arg<IPeerConnectionNotifier>.Is.Anything)).Return(this.validGroup);
        }

        [Test]
        public void SimpleConstructorThrowsNoExceptions()
        {
            ForwardingManager manager = new ForwardingManager(
                this.overloadOptions,
                this.transportProtocol,
                this.connectionTable,
                this.envelopeForwarderFactory,
                this.forwardingGroupFactory,
                this.eventQueue,
                this.timeKeeper,
                this.addressProvider,
                this.connectivityReporter,
                this.connectionTable);
        }

        [Test]
        [ExpectedException("System.InvalidOperationException")]
        public void CreateForwardingGroupThrowsExceptionWhenForwardingIsNotEnabled()
        {
            XmlDocument document = new XmlDocument();
            string configString = "<Overload Enabled=\"true\"><EnableOverload>Disabled</EnableOverload></Overload>";
            document.LoadXml(configString);
            this.overloadOptions = new OverloadModule(document.FirstChild);

            ForwardingManager manager = new ForwardingManager(
                this.overloadOptions,
                this.transportProtocol,
                this.connectionTable,
                this.envelopeForwarderFactory,
                this.forwardingGroupFactory,
                this.eventQueue,
                this.timeKeeper,
                this.addressProvider,
                this.connectivityReporter,
                this.connectionTable);
            manager.CreateForwardingGroup(new BadumnaId(PeerAddress.Nowhere, 1));
        }

        [Test]
        public void CreateForwardingReturnsNoWhereGroupWhenDistributedLookupIsEnabled()
        {
            XmlDocument document = new XmlDocument();
            string configString = "<Module Name=\"Overload\" Enabled=\"true\"><EnableOverload>Enabled</EnableOverload><UseServiceDiscovery>Enabled</UseServiceDiscovery></Module>";
            document.LoadXml(configString);
            this.overloadOptions = new OverloadModule(document.FirstChild);

            // ForwardingGroup noWhereGroup = new ForwardingGroup(this.forwarder, PeerAddress.Nowhere, id);
            ForwardingManager manager = new ForwardingManager(
                this.overloadOptions,
                this.transportProtocol,
                this.connectionTable,
                this.envelopeForwarderFactory,
                this.forwardingGroupFactory,
                this.eventQueue,
                this.timeKeeper,
                this.addressProvider,
                this.connectivityReporter,
                this.connectionTable);
            IForwardingGroup returnedGroup = manager.CreateForwardingGroup(this.groupId);
            Assert.IsTrue(returnedGroup == this.noWhereGroup);
        }

        [Test]
        public void CreateForwardingReturnsValidGroupWhenPeerAddressIsSet()
        {
            XmlDocument document = new XmlDocument();
            string configString = "<Overload Enabled=\"true\"><EnableOverload>Enabled</EnableOverload><OverloadPeer>222.222.222.222:12345</OverloadPeer></Overload>";
            document.LoadXml(configString);
            this.overloadOptions = new OverloadModule(document.FirstChild);
            ((IOptionModule)this.overloadOptions).Validate();

            ForwardingManager manager = new ForwardingManager(
                this.overloadOptions,
                this.transportProtocol,
                this.connectionTable,
                this.envelopeForwarderFactory,
                this.forwardingGroupFactory,
                this.eventQueue,
                this.timeKeeper,
                this.addressProvider,
                this.connectivityReporter,
                this.connectionTable);
            IForwardingGroup returnedGroup = manager.CreateForwardingGroup(this.groupId);
            Assert.IsTrue(returnedGroup == this.validGroup);
        }

        /*
        [Test]
        public void TestSetEntityManager()
        {
            ForwardingManager manager = new ForwardingManager(this.transportProtocol, this.connectionTable, this.envelopeForwarderFactory, this.forwardingGroupFactory);
            this.forwarder.Expect(f => f.SetEntityManager(null));
            manager.SetEntityManager(null);
            this.forwarder.VerifyAllExpectations();
        }*/
    }
}
