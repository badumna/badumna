﻿//-----------------------------------------------------------------------
// <copyright file="EntityForwarderTester.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Net;
using Badumna.Core;
using Badumna.DataTypes;
using Badumna.Overload;
using Badumna.Replication;
using Badumna.Transport;
using Badumna.Utilities;
using NUnit.Framework;
using Rhino.Mocks;

namespace BadumnaTests.Overload
{
    /// <summary>
    /// Document this class.
    /// </summary>
    [TestFixture]
    [DontPerformCoverage]
    public class EntityForwarderTester
    {
        private IEntityRouter entityRouter;
        private TransportProtocol transportProtocol;
        private IEntityRouterFactory entityRouterFactory;
        private INetworkAddressProvider addressProvider;

        [SetUp]
        public void SetUp()
        {
            this.entityRouter = MockRepository.GenerateMock<IEntityRouter>();
            this.transportProtocol = MockRepository.GenerateMock<TransportProtocol>("foo", typeof(DhtProtocolMethodAttribute), new GenericCallBackReturn<object, Type>(Activator.CreateInstance));
            this.entityRouterFactory = MockRepository.GenerateStub<IEntityRouterFactory>();
            this.entityRouterFactory.Stub(f => f.Create(null, null)).IgnoreArguments().Return(this.entityRouter);
            this.addressProvider = MockRepository.GenerateStub<INetworkAddressProvider>();
        }

        [Test]
        public void SimpleConstructorThrowsNoException()
        {
            EntityForwarder entityForwarder = new EntityForwarder(this.transportProtocol, this.entityRouterFactory, this.addressProvider);
            Assert.IsNotNull(entityForwarder.Router);
        }

        [Test, Ignore]
        public void DirectSendBeCalledOnAllEntities()
        {
            EntityForwarder entityForwarder = new EntityForwarder(this.transportProtocol, this.entityRouterFactory, this.addressProvider);
            EntityEnvelope envelope = new EntityEnvelope(new BadumnaId(PeerAddress.Nowhere, 1), new QualityOfService());
            List<BadumnaId> entities = new List<BadumnaId>();

            for (ushort i = 100; i < 110; i++)
            {
                PeerAddress address = new PeerAddress(new IPEndPoint(new IPAddress(new byte[] { 222, 222, 222, 222 }), 12345), NatType.Unknown);
                BadumnaId id = new BadumnaId(address, i);
                entities.Add(id);

                envelope.DestinationEntity = id;
                this.entityRouter.Expect(f => f.DirectSend(Arg.Is(envelope)));
            }

            entityForwarder.ForwardToEntities(envelope, entities);
            this.entityRouter.VerifyAllExpectations();
        }

        [Test]
        public void ProcessMessageThrowsNoExceptionWhenEntityManagerIsUnknown()
        {
            EntityForwarder entityForwarder = new EntityForwarder(this.transportProtocol, this.entityRouterFactory, this.addressProvider);
            EntityEnvelope envelope = new EntityEnvelope(new BadumnaId(PeerAddress.Nowhere, 1), new QualityOfService());
            entityForwarder.EntityManager = null;
            entityForwarder.ProcessMessage(envelope);
        }
    }
}
