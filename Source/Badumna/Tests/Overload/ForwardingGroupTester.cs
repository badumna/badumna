﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Badumna;
using Badumna.Core;
using Badumna.DataTypes;
using Badumna.Overload;
using Badumna.Replication;
using Badumna.Security;
using Badumna.Transport;
using Badumna.Utilities;
using BadumnaTests.Core;
using BadumnaTests.Transport.Connections;
using BadumnaTests.Utilities;
using NUnit.Framework;
using Rhino.Mocks;
using System.Threading;

namespace BadumnaTests.Overload
{
    [TestFixture]
    [DontPerformCoverage]
    public class EnvelopeGroupTester
    {
        private MockTransportLayer mSourceTransport;
        private MockTransportLayer mDestinationTransport;

        private TransportProtocol mSourceLayer2;
        private TransportProtocol mDestinationLayer2;

        private TransportEnvelope mFailedMessage;
        private PeerAddress mFailedDestination;

        private TestableConnectionTable mSourceConnectionTable;
        private TestableConnectionTable mDestinationConnectionTable;

        private INetworkAddressProvider sourceAddressProvider;
        private INetworkAddressProvider destinationAddressProvider;

        private INetworkConnectivityReporter sourceConnectivityReporter;
        private INetworkConnectivityReporter destinationConnectivityReporter;

        private TokenCache sourceTokens;
        private TokenCache destinationTokens;

        private EnvelopeForwarder mForwarder;
        private EnvelopeForwarder mForwarderOnRemotePeer;

        private ForwardingGroup mForwardGroup;

        private BadumnaId mGroupId = new BadumnaId(PeerAddress.GetAddress("127.0.0.1", 1, NatType.Open), 0);

        private Simulator eventQueue;
        private Simulator encryptionQueue;
        private ITime timeKeeper;

        private OverloadModule defaultOptions;

        [SetUp]
        public void SetUp()
        {
            //Console.WriteLine("set up is called.");
            this.eventQueue = new Simulator();
            this.encryptionQueue = this.eventQueue;
            this.timeKeeper = this.eventQueue;

            // Test that we can construct a Connection.
            this.sourceAddressProvider = MockRepository.GenerateStub<INetworkAddressProvider>();
            this.sourceAddressProvider.Stub(x => x.PublicAddress).Return(PeerAddress.GetAddress("127.0.0.1", 1, NatType.Open));
            this.destinationAddressProvider = MockRepository.GenerateStub<INetworkAddressProvider>();
            this.destinationAddressProvider.Stub(x => x.PublicAddress).Return(PeerAddress.GetAddress("127.0.0.1", 2, NatType.FullCone));

            this.sourceConnectivityReporter = MockRepository.GenerateStub<INetworkConnectivityReporter>();
            this.sourceConnectivityReporter.Stub(x => x.Status).Return(ConnectivityStatus.Online);
            this.destinationConnectivityReporter = MockRepository.GenerateStub<INetworkConnectivityReporter>();
            this.destinationConnectivityReporter.Stub(x => x.Status).Return(ConnectivityStatus.Online);

            this.sourceTokens = new TokenCache(this.eventQueue, this.timeKeeper, this.sourceConnectivityReporter);
            this.sourceTokens.SetIdentityProvider(new UnverifiedIdentityProvider());
            this.sourceTokens.PreCache();
            this.destinationTokens = new TokenCache(this.eventQueue, this.timeKeeper, this.destinationConnectivityReporter);
            this.destinationTokens.SetIdentityProvider(new UnverifiedIdentityProvider());
            this.destinationTokens.PreCache();

            IPeerFinder peerFinder = new SeedPeerFinder(new List<string>());

            this.mSourceTransport = new MockTransportLayer(this.sourceAddressProvider, this.eventQueue, this.timeKeeper);
            this.mSourceConnectionTable = new TestableConnectionTable(this.destinationAddressProvider.PublicAddress, this.mSourceTransport, peerFinder, this.eventQueue, this.encryptionQueue, this.timeKeeper, this.sourceAddressProvider, this.sourceConnectivityReporter, this.sourceTokens, MessageParserTester.DefaultFactory);
            this.mSourceLayer2 = new TransportProtocol(this.mSourceConnectionTable.ProtocolRoot);

            this.mDestinationTransport = new MockTransportLayer(this.destinationAddressProvider, this.eventQueue, this.timeKeeper);
            this.mDestinationConnectionTable = new TestableConnectionTable(this.sourceAddressProvider.PublicAddress, this.mDestinationTransport, peerFinder, this.eventQueue, this.encryptionQueue, this.timeKeeper, this.destinationAddressProvider, this.destinationConnectivityReporter, this.destinationTokens, MessageParserTester.DefaultFactory);
            this.mDestinationLayer2 = new TransportProtocol(this.mDestinationConnectionTable.ProtocolRoot);

            // this.mSourceLayer2.SendText = "Test data.";
            // this.mDestinationLayer2.ExpectedText = this.mSourceLayer2.SendText;

            this.mSourceConnectionTable.Connection.MaximumRetriesReachedEvent += this.FailedRetryHandler;
            //Console.WriteLine("Set up is going to quit.");

            this.defaultOptions = new OverloadModule(null);
            ((IOptionModule)this.defaultOptions).Validate();
        }

        public void JoinWithRealiability(double reliability)
        {
            PeerAddress source = this.mSourceTransport.Address;
            PeerAddress destination = this.mDestinationTransport.Address;

            this.mSourceTransport.ModifyLink(this.mDestinationTransport, 10, reliability, 64);
            this.mDestinationTransport.ModifyLink(this.mSourceTransport, 10, reliability, 64);
        }

        internal void FailedRetryHandler(TransportEnvelope messageEnvelope, PeerAddress peerName)
        {
            ////Debug.WriteLine("Failed to send reliable message to " + peerName + ". Max retries reached.");
            this.mFailedMessage = messageEnvelope;
            this.mFailedDestination = peerName;
        }

        private void Forge()
        {
            this.mSourceConnectionTable.ForgeMethodList();
            this.mSourceConnectionTable.ProtocolRoot.ForgeMethodList();
            this.mDestinationConnectionTable.ForgeMethodList();
            this.mDestinationConnectionTable.ProtocolRoot.ForgeMethodList();
        }

        [TearDown]
        public void TearDown()
        {
            this.mSourceConnectionTable = null;
            this.mDestinationConnectionTable = null;
        }

        [TestCase(false)]
        public void InitializeTest(bool acceptOverloadOnRemote)
        {
            OverloadModule remoteOptions = new OverloadModule(null);
            remoteOptions.IsServer = acceptOverloadOnRemote;
            ((IOptionModule)remoteOptions).Validate();

            // create the forwarders
            this.mForwarder = new EnvelopeForwarder(this.mSourceLayer2, (GenericCallBack<TransportEnvelope, PeerAddress, PeerAddress>)null, this.defaultOptions, this.eventQueue, this.timeKeeper, this.sourceConnectivityReporter, this.sourceAddressProvider);
            this.mForwarderOnRemotePeer = new EnvelopeForwarder(this.mDestinationLayer2, (GenericCallBack<TransportEnvelope, PeerAddress, PeerAddress>)null, remoteOptions, this.eventQueue, this.timeKeeper, this.destinationConnectivityReporter, this.destinationAddressProvider);
            // the forwarding group
            this.Forge();
            this.mForwardGroup = new ForwardingGroup(this.mForwarder, this.mDestinationTransport.Address, this.mGroupId, this.defaultOptions, this.eventQueue, this.timeKeeper, this.sourceAddressProvider, this.mSourceConnectionTable);

            PeerAddress source = this.mSourceTransport.Address;
            PeerAddress destination = this.mDestinationTransport.Address;

            this.JoinWithRealiability(1.0);

            // Test that initialize() works.
            this.mSourceConnectionTable.Connection.BeginInitialization();
            this.mDestinationConnectionTable.Connection.BeginInitialization();

            Assert.IsFalse(this.mSourceConnectionTable.Connection.IsConnected, "Source connection claims to be connection prior to initializing");
            Assert.IsFalse(this.mDestinationConnectionTable.Connection.IsConnected, "Destination connection claims to be connection prior to initializing");
            //Console.WriteLine("going to run for 5 seconds");
            this.eventQueue.RunFor(TimeSpan.FromSeconds(5));
            //Console.WriteLine("5 seconds run done.");

            Assert.IsTrue(this.mSourceConnectionTable.Connection.IsConnected, "Source did not connect successfully");
            Assert.IsTrue(this.mDestinationConnectionTable.Connection.IsConnected, "Destination did not connect successfully");
        }

        [Test]
        public void TestForward()
        {
            this.InitializeTest(false);

            EntityEnvelope payload = new EntityEnvelope(this.mGroupId, QualityOfService.Reliable);
            this.mForwardGroup.ForwardEnvelope(payload);

            this.eventQueue.RunFor(TimeSpan.FromSeconds(1));

            // still be available after sending the payload.
            Assert.IsTrue(this.mForwardGroup.IsForwardingPeerAvailable);
        }

        [Test]
        public void TestForwardPeerConnectionFailure()
        {
            this.InitializeTest(false);
            EntityEnvelope payload = new EntityEnvelope(this.mGroupId, QualityOfService.Reliable);
            // set an invalid forwarding peer
            ForwardingGroup unavailableForwardGroup = new ForwardingGroup(this.mForwarder, PeerAddress.GetAddress("202.96.100.1", 1234, NatType.Open), new BadumnaId(this.sourceAddressProvider.PublicAddress, 1), this.defaultOptions, this.eventQueue, this.timeKeeper, this.sourceAddressProvider, this.mSourceConnectionTable);
            // always assume the forward group to be available on start up. 
            Assert.IsTrue(unavailableForwardGroup.IsForwardingPeerAvailable);

            // unavailableForwardGroup.ForwardEnvelope(payload);
            // unavailableForwardGroup.ForwardEnvelope(payload);
            unavailableForwardGroup.ForwardEnvelope(payload);
            this.eventQueue.RunFor(TimeSpan.FromSeconds(5));
            Assert.IsTrue(unavailableForwardGroup.IsForwardingPeerAvailable);
            this.eventQueue.RunFor(TimeSpan.FromSeconds(11));
            Assert.IsFalse(unavailableForwardGroup.IsForwardingPeerAvailable);
        }

        /*[Test]
        public void TestForwardPeerFailure()
        {
            // setup everything, forward a message and make sure it is successful.
            this.TestForward();

            NetworkContext.CurrentContext = this.mSourceContext;
            // set the reliability to zero, dropping all pkts
            this.JoinWithRealiability(0.0);
            this.eventQueue.RunFor(TimeSpan.FromSeconds(1));
            EntityEnvelope payload = new EntityEnvelope(this.groupId, QualityOfService.Reliable);
            NetworkContext.CurrentContext = this.mSourceContext;
            this.mForwardGroup.ForwardEnvelope(payload);

            // wait long enough to allow max retry to be reached. 
            this.eventQueue.RunFor(TimeSpan.FromSeconds(35));
            this.JoinWithRealiability(1.0);
            Assert.IsFalse(this.mForwardGroup.IsForwardingPeerAvailable);
        }*/

        private void TestAddedAddresses(List<BadumnaId> original, List<BadumnaId> newList)
        {
            Assert.AreEqual(original.Count, newList.Count);

            foreach (BadumnaId entity in original)
            {
                if (!newList.Contains(entity))
                {
                    Assert.Fail();
                }
            }

            foreach (BadumnaId entity in newList)
            {
                if (!original.Contains(entity))
                {
                    Assert.Fail();
                }
            }
        }

        [Test]
        public void TestGroupOperations()
        {
            // setup everything, forward a message and make sure it is successful.
            this.InitializeTest(true);

            this.mForwardGroup.Add(new BadumnaId(PeerAddress.GetAddress("192.168.0.1", 1, NatType.Open), 0));
            this.mForwardGroup.Add(new BadumnaId(PeerAddress.GetAddress("192.168.0.2", 1, NatType.Open), 0));
            this.mForwardGroup.Add(new BadumnaId(PeerAddress.GetAddress("192.168.0.3", 1, NatType.Open), 0));
            this.mForwardGroup.Add(new BadumnaId(PeerAddress.GetAddress("192.168.0.4", 1, NatType.Open), 0));
            this.mForwardGroup.Add(new BadumnaId(PeerAddress.GetAddress("192.168.0.5", 1, NatType.Open), 0));

            // this will register the added peers to the group. 
            EntityEnvelope payload = new EntityEnvelope(this.mGroupId, QualityOfService.Reliable);
            this.mForwardGroup.ForwardEnvelope(payload);

            this.eventQueue.RunFor(TimeSpan.FromSeconds(1));

            // add two more and remove one beforing committing
            this.mForwardGroup.Add(new BadumnaId(PeerAddress.GetAddress("192.168.0.6", 1, NatType.Open), 0));
            this.mForwardGroup.Add(new BadumnaId(PeerAddress.GetAddress("192.168.0.7", 1, NatType.Open), 0));
            this.mForwardGroup.Remove(new BadumnaId(PeerAddress.GetAddress("192.168.0.7", 1, NatType.Open), 0));
            payload = new EntityEnvelope(this.mGroupId, QualityOfService.Reliable);
            this.mForwardGroup.ForwardEnvelope(payload);

            this.eventQueue.RunFor(TimeSpan.FromSeconds(1));

            // remove two
            this.mForwardGroup.Remove(new BadumnaId(PeerAddress.GetAddress("192.168.0.3", 1, NatType.Open), 0));
            this.mForwardGroup.Remove(new BadumnaId(PeerAddress.GetAddress("192.168.0.4", 1, NatType.Open), 0));
            payload = new EntityEnvelope(this.mGroupId, QualityOfService.Reliable);
            this.mForwardGroup.ForwardEnvelope(payload);

            this.eventQueue.RunFor(TimeSpan.FromSeconds(1));

            List<BadumnaId> groupMembers = this.mForwarderOnRemotePeer.GetGroup();
            Assert.AreNotEqual(null, groupMembers);
            Assert.AreEqual(4, groupMembers.Count);

            // create another foreward group, import the group setting, they should be the same as the
            // the existing group setting
            ForwardingGroup newForwardGroup = new ForwardingGroup(this.mForwarder, this.mDestinationTransport.Address, new BadumnaId(this.sourceAddressProvider.PublicAddress, 1), this.defaultOptions, this.eventQueue, this.timeKeeper, this.sourceAddressProvider, this.mSourceConnectionTable);
            newForwardGroup.Import(this.mForwardGroup);
            Assert.AreNotEqual(null, newForwardGroup.GetAddedEntities());
            Assert.AreEqual(4, newForwardGroup.GetAddedEntities().Count);

            // test consistency
            TestAddedAddresses(newForwardGroup.GetAddedEntities(), this.mForwarderOnRemotePeer.GetGroup());

            this.mForwardGroup.RemoveAll();
            payload = new EntityEnvelope(this.mGroupId, QualityOfService.Reliable);
            this.mForwardGroup.ForwardEnvelope(payload);

            this.eventQueue.RunFor(TimeSpan.FromSeconds(1));

            groupMembers = this.mForwarderOnRemotePeer.GetGroup();
            Assert.AreNotEqual(null, groupMembers);
            Assert.AreEqual(0, groupMembers.Count);
        }
    }
}
