﻿//-----------------------------------------------------------------------
// <copyright file="EnvelopeForwarderTester.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Xml;
using Badumna;
using Badumna.Core;
using Badumna.DataTypes;
using Badumna.Overload;
using Badumna.Security;
using Badumna.Transport;
using Badumna.Utilities;
using NUnit.Framework;
using Rhino.Mocks;

namespace BadumnaTests.Overload
{
    [TestFixture]
    [DontPerformCoverage]
    public class EnvelopeForwarderTester
    {
        private TransportProtocol transportProtocol;
        private ConnectionTable connectionTable;

        private IMessageDispatcher<TransportEnvelope> messageDispatcher;
        private IMessageProducer<TransportEnvelope> messageProducer;

        private Simulator eventQueue;
        private Simulator encryptionQueue;
        private ITime timeKeeper;

        private INetworkAddressProvider addressProvider;
        private INetworkConnectivityReporter connectivityReporter;

        private TokenCache tokens;

        private OverloadModule overloadOptions;

        [SetUp]
        public void SetUp()
        {
            this.eventQueue = new Simulator();
            this.encryptionQueue = new Simulator();
            this.timeKeeper = this.eventQueue;

            this.messageDispatcher = MockRepository.GenerateMock<IMessageDispatcher<TransportEnvelope>>();
            this.messageProducer = MockRepository.GenerateMock<IMessageProducer<TransportEnvelope>>();

            GenericCallBackReturn<object, Type> factory = Activator.CreateInstance;
            this.transportProtocol = MockRepository.GenerateMock<TransportProtocol>("foo", typeof(DhtProtocolMethodAttribute), factory);

            this.addressProvider = MockRepository.GenerateMock<INetworkAddressProvider>();

            this.connectivityReporter = MockRepository.GenerateMock<INetworkConnectivityReporter>();
            this.connectivityReporter.Stub(x => x.Status).Return(ConnectivityStatus.Online);

            this.tokens = new TokenCache(this.eventQueue, this.timeKeeper, this.connectivityReporter);
            this.tokens.SetIdentityProvider(new UnverifiedIdentityProvider());
            this.tokens.PreCache();

            IPeerFinder peerFinder = MockRepository.GenerateMock<IPeerFinder>();
            this.connectionTable = MockRepository.GenerateMock<ConnectionTable>(this.messageDispatcher, this.messageProducer, peerFinder, this.eventQueue, this.encryptionQueue, this.timeKeeper, this.addressProvider, this.connectivityReporter, "testAppName", this.tokens, factory);

            this.overloadOptions = new OverloadModule(null);
        }

        [Test]
        public void SimpleConstructorThrowsNoException()
        {
            EnvelopeForwarder forwarder = this.CreateEnvelopeForwarder();
        }

        [Test]
        public void ShutdownThrowsNoException()
        {
            EnvelopeForwarder forwarder = this.CreateEnvelopeForwarder();
            forwarder.Shutdown();
        }

        [Test]
        public void OperationsIgnoredPeacefullyWhenAcceptOverloadIsNotEnabled()
        {
            this.ConfigAcceptOverload(false);
            EnvelopeForwarder forwarder = this.CreateEnvelopeForwarder();
            BadumnaId groupId = new BadumnaId(PeerAddress.Nowhere, 1);
            List<BadumnaId> entities = new List<BadumnaId>();
            BadumnaId garbageEntityId = new BadumnaId(PeerAddress.Nowhere, 2);
            entities.Add(garbageEntityId);

            forwarder.CreateForwardingGroup(groupId, entities);
            Dictionary<BadumnaId, GroupConfig> groups = forwarder.Groups;
            Assert.AreEqual(0, groups.Count);

            forwarder.AddToForwardingGroup(groupId, entities);
            groups = forwarder.Groups;
            Assert.AreEqual(0, groups.Count);

            forwarder.RemoveFromForwardingGroup(groupId, entities);
            groups = forwarder.Groups;
            Assert.AreEqual(0, groups.Count);

            forwarder.RemoveForwardingGroup(groupId);
            groups = forwarder.Groups;
            Assert.AreEqual(0, groups.Count);

            forwarder.ForwardToGroup(groupId, null, -1);

            forwarder.Shutdown();
        }

        [Test]
        public void TestCreateForwardingGroup()
        {
            this.ConfigAcceptOverload(true);
            EnvelopeForwarder forwarder = this.CreateEnvelopeForwarder();
            BadumnaId groupId = new BadumnaId(PeerAddress.Nowhere, 1);
            BadumnaId groupId2 = new BadumnaId(PeerAddress.Nowhere, 3);
            List<BadumnaId> entities = new List<BadumnaId>();
            BadumnaId garbageEntityId = new BadumnaId(PeerAddress.Nowhere, 2);
            entities.Add(garbageEntityId);

            forwarder.CreateForwardingGroup(groupId, entities);
            Dictionary<BadumnaId, GroupConfig> groups = forwarder.Groups;
            Assert.AreEqual(1, groups.Count);

            forwarder.CreateForwardingGroup(groupId, entities);
            groups = forwarder.Groups;
            Assert.AreEqual(1, groups.Count);

            forwarder.CreateForwardingGroup(groupId2, entities);
            groups = forwarder.Groups;
            Assert.AreEqual(2, groups.Count);

            forwarder.Shutdown();
        }

        [Test]
        public void TestRemoveForwardingGroup()
        {
            this.ConfigAcceptOverload(true);
            EnvelopeForwarder forwarder = this.CreateEnvelopeForwarder();
            BadumnaId groupId1 = new BadumnaId(PeerAddress.Nowhere, 1);
            BadumnaId groupId2 = new BadumnaId(PeerAddress.Nowhere, 2);
            BadumnaId groupId3 = new BadumnaId(PeerAddress.Nowhere, 3);
            List<BadumnaId> entities = new List<BadumnaId>();
            BadumnaId garbageEntityId = new BadumnaId(PeerAddress.Nowhere, 4);
            entities.Add(garbageEntityId);

            forwarder.CreateForwardingGroup(groupId1, entities);
            forwarder.CreateForwardingGroup(groupId2, entities);
            forwarder.CreateForwardingGroup(groupId3, entities);

            Dictionary<BadumnaId, GroupConfig> groups = forwarder.Groups;
            Assert.AreEqual(3, groups.Count);

            forwarder.RemoveForwardingGroup(groupId1);
            forwarder.RemoveForwardingGroup(groupId3);
            groups = forwarder.Groups;
            Assert.AreEqual(1, groups.Count);
            Assert.IsTrue(groups.Keys.Count == 1);
            Assert.IsTrue(groups.ContainsKey(groupId2));
            Assert.IsTrue(!groups.ContainsKey(groupId1));
            Assert.IsTrue(!groups.ContainsKey(groupId3));

            forwarder.Shutdown();
        }

        [Test]
        public void AddToForwardingGroupCanAlsoCreateGroups()
        {
            this.ConfigAcceptOverload(true);
            EnvelopeForwarder forwarder = this.CreateEnvelopeForwarder();
            BadumnaId groupId1 = new BadumnaId(PeerAddress.Nowhere, 1);
            List<BadumnaId> entities = new List<BadumnaId>();
            BadumnaId garbageEntityId = new BadumnaId(PeerAddress.Nowhere, 4);
            entities.Add(garbageEntityId);

            forwarder.AddToForwardingGroup(groupId1, entities);
            Dictionary<BadumnaId, GroupConfig> groups = forwarder.Groups;
            Assert.AreEqual(1, groups.Count);

            forwarder.Shutdown();
        }

        [Test]
        public void AddToForwardingGroupActuallyAddsEntitiesToGroup()
        {
            this.ConfigAcceptOverload(true);
            EnvelopeForwarder forwarder = this.CreateEnvelopeForwarder();
            BadumnaId groupId1 = new BadumnaId(PeerAddress.Nowhere, 1);
            List<BadumnaId> entities = new List<BadumnaId>();
            BadumnaId garbageEntityId = new BadumnaId(PeerAddress.Nowhere, 4);
            entities.Add(garbageEntityId);

            forwarder.AddToForwardingGroup(groupId1, entities);
            Dictionary<BadumnaId, GroupConfig> groups = forwarder.Groups;
            Assert.AreEqual(1, groups.Count);

            BadumnaId garbageEntityId2 = new BadumnaId(PeerAddress.Nowhere, 5);
            BadumnaId garbageEntityId3 = new BadumnaId(PeerAddress.Nowhere, 6);

            entities.Add(garbageEntityId2);
            entities.Add(garbageEntityId3);

            forwarder.AddToForwardingGroup(groupId1, entities);
            groups = forwarder.Groups;
            Assert.AreEqual(1, groups.Count);
            Assert.AreEqual(3, groups[groupId1].GroupEntities.Count);

            forwarder.Shutdown();
        }

        [Test]
        public void RemoveFromForwardingGroupActuallyRemovesEntitiesFromGroup()
        {
            this.ConfigAcceptOverload(true);
            EnvelopeForwarder forwarder = this.CreateEnvelopeForwarder();
            BadumnaId groupId1 = new BadumnaId(PeerAddress.Nowhere, 1);
            List<BadumnaId> entities = new List<BadumnaId>();
            BadumnaId garbageEntityId1 = new BadumnaId(PeerAddress.Nowhere, 4);
            BadumnaId garbageEntityId2 = new BadumnaId(PeerAddress.Nowhere, 5);
            BadumnaId garbageEntityId3 = new BadumnaId(PeerAddress.Nowhere, 6);
            entities.Add(garbageEntityId1);
            entities.Add(garbageEntityId2);
            entities.Add(garbageEntityId3);

            forwarder.AddToForwardingGroup(groupId1, entities);
            Dictionary<BadumnaId, GroupConfig> groups = forwarder.Groups;
            Assert.AreEqual(1, groups.Count);
            Assert.AreEqual(3, groups[groupId1].GroupEntities.Count);

            entities.Remove(garbageEntityId2);
            forwarder.RemoveFromForwardingGroup(groupId1, entities);
            groups = forwarder.Groups;
            Assert.AreEqual(1, groups.Count);
            Assert.AreEqual(1, groups[groupId1].GroupEntities.Count);
            Assert.IsTrue(groups[groupId1].GroupEntities.Contains(garbageEntityId2));
            Assert.IsFalse(groups[groupId1].GroupEntities.Contains(garbageEntityId1));
            Assert.IsFalse(groups[groupId1].GroupEntities.Contains(garbageEntityId3));

            entities.Clear();
            entities.Add(garbageEntityId2);
            forwarder.RemoveFromForwardingGroup(groupId1, entities);
            groups = forwarder.Groups;
            Assert.AreEqual(1, groups.Count);
            Assert.AreEqual(0, groups[groupId1].GroupEntities.Count);

            forwarder.Shutdown();
        }

        [Test]
        public void RemoveFromForwardingGroupDoesNothingWhenGroupIdDoesNotMatch()
        {
            this.ConfigAcceptOverload(true);
            EnvelopeForwarder forwarder = this.CreateEnvelopeForwarder();
            BadumnaId groupId1 = new BadumnaId(PeerAddress.Nowhere, 1);
            List<BadumnaId> entities = new List<BadumnaId>();
            BadumnaId garbageEntityId1 = new BadumnaId(PeerAddress.Nowhere, 4);
            BadumnaId garbageEntityId2 = new BadumnaId(PeerAddress.Nowhere, 5);
            BadumnaId garbageEntityId3 = new BadumnaId(PeerAddress.Nowhere, 6);
            entities.Add(garbageEntityId1);
            entities.Add(garbageEntityId2);
            entities.Add(garbageEntityId3);

            forwarder.AddToForwardingGroup(groupId1, entities);
            Dictionary<BadumnaId, GroupConfig> groups = forwarder.Groups;
            Assert.AreEqual(1, groups.Count);
            Assert.AreEqual(3, groups[groupId1].GroupEntities.Count);

            BadumnaId groupId2 = new BadumnaId(PeerAddress.Nowhere, 7);
            forwarder.RemoveFromForwardingGroup(groupId2, entities);
            groups = forwarder.Groups;
            Assert.AreEqual(1, groups.Count);
            Assert.AreEqual(3, groups[groupId1].GroupEntities.Count);

            forwarder.RemoveFromForwardingGroup(groupId1, entities);
            groups = forwarder.Groups;
            Assert.AreEqual(1, groups.Count);
            Assert.AreEqual(0, groups[groupId1].GroupEntities.Count);

            forwarder.Shutdown();
        }

        [Test]
        public void GarbageCollectionTaskDoesCollectGarbage()
        {
            this.ConfigAcceptOverload(true);
            EnvelopeForwarder forwarder = this.CreateEnvelopeForwarder();
            BadumnaId groupId1 = new BadumnaId(PeerAddress.Nowhere, 1);
            List<BadumnaId> entities = new List<BadumnaId>();
            BadumnaId garbageEntityId1 = new BadumnaId(PeerAddress.Nowhere, 4);
            entities.Add(garbageEntityId1);

            forwarder.AddToForwardingGroup(groupId1, entities);
            Dictionary<BadumnaId, GroupConfig> groups = forwarder.Groups;
            Assert.AreEqual(1, groups.Count);

            this.eventQueue.RunFor(TimeSpan.FromMinutes(10));
            groups = forwarder.Groups;
            Assert.AreEqual(0, groups.Count);

            forwarder.Shutdown();
        }

        [Test]
        public void GarbageCollectionTaskDoesNothingWhenTheGroupIsKeepBeingUsed()
        {
            this.ConfigAcceptOverload(true);
            EnvelopeForwarder forwarder = this.CreateEnvelopeForwarder();
            BadumnaId groupId1 = new BadumnaId(PeerAddress.Nowhere, 1);
            BadumnaId groupId2 = new BadumnaId(PeerAddress.Nowhere, 2);
            List<BadumnaId> entities = new List<BadumnaId>();
            BadumnaId garbageEntityId1 = new BadumnaId(PeerAddress.Nowhere, 4);
            entities.Add(garbageEntityId1);

            forwarder.AddToForwardingGroup(groupId1, entities);
            Dictionary<BadumnaId, GroupConfig> groups = forwarder.Groups;
            Assert.AreEqual(1, groups.Count);

            for (int i = 0; i < 10; i++)
            {
                this.eventQueue.RunFor(TimeSpan.FromMinutes(1));
                ((MockEnvelopeForwarder)forwarder).ForceUpdateLastForwardTime(groupId1);
                ((MockEnvelopeForwarder)forwarder).ForceUpdateLastForwardTime(groupId2);
            }

            groups = forwarder.Groups;
            Assert.AreEqual(1, groups.Count);

            forwarder.Shutdown();
        }

        private void ConfigAcceptOverload(bool accept)
        {
            XmlDocument document = new XmlDocument();
            string acceptOverload;

            if (accept)
            {
                acceptOverload = "Enabled";
            }
            else
            {
                acceptOverload = "Disabled";
            }

            string configString = "<Overload Enabled=\"true\"><AcceptOverload>" + acceptOverload + "</AcceptOverload></Overload>";
            document.LoadXml(configString);
            this.overloadOptions = new OverloadModule(document.FirstChild);
        }

        private EnvelopeForwarder CreateEnvelopeForwarder()
        {
            EnvelopeForwarder forwarder = new MockEnvelopeForwarder(this.transportProtocol, this.connectionTable, this.overloadOptions, this.eventQueue, this.timeKeeper, this.connectivityReporter, this.addressProvider);
            return forwarder;
        }

        [DontPerformCoverage]
        private class MockEnvelopeForwarder : EnvelopeForwarder
        {
            public MockEnvelopeForwarder(TransportProtocol protocol, ConnectionTable table, OverloadModule overloadOptions, NetworkEventQueue eventQueue, ITime timeKeeper, INetworkConnectivityReporter connectivityReporter, INetworkAddressProvider addressProvider)
                : base(protocol, table, overloadOptions, eventQueue, timeKeeper, connectivityReporter, addressProvider)
            {
            }

            public void ForceUpdateLastForwardTime(BadumnaId groupId)
            {
                this.UpdateLastForwardTime(groupId);
            }
        }
    }
}
