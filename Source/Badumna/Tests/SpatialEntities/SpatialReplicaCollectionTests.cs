﻿//-----------------------------------------------------------------------
// <copyright file="SpatialReplicaCollectionTests.cs" company="NICTA">
//     Copyright (c) 2009 NICTA. All right reserved.
// </copyright>
//-----------------------------------------------------------------------

using BadumnaTests.Utilities;

namespace BadumnaTests.SpatialEntities
{
    using System.Collections.Generic;
    using System.IO;
    using Badumna;
    using Badumna.Autoreplication;
    using Badumna.Core;
    using Badumna.DataTypes;
    using Badumna.InterestManagement;
    using Badumna.Replication;
    using Badumna.SpatialEntities;
    using Badumna.Utilities;
    using BadumnaTests.Core;
    using NUnit.Framework;
    using Rhino.Mocks;
    
    using SpatialAutoreplicationManager = Badumna.Autoreplication.SpatialAutoreplicationManager;

    [TestFixture]
    [DontPerformCoverage]
    public class SpatialReplicaCollectionTests
    {
        private string layerName = "Test layer name";

        private QualifiedName sceneName = new QualifiedName("", "Test scene name");

        private MessageParser parser = null;

        private AssociatedReplica association = null;

        private IEntityManager entityManager = null;

        private NetworkEventQueue eventQueue = null;

        private ITime timeKeeper = null;

        private INetworkConnectivityReporter connectivityReporter;

        private IPeerConnectionNotifier connectionNotifier;

        private SpatialEntityManager spatialEntityManager = null;

        private ISpatialReplica spatialReplica = null;

        private CreateSpatialReplica createReplica = null;

        private RemoveSpatialReplica removeReplica = null;

        private NetworkScene scene = null;

        private SpatialReplicaCollection collection = null;

        private IInterestManagementService interestManagementService = null;

        private SpatialAutoreplicationManager autoreplicationManager;

        private QueueInvokable queueApplicationEvent;

        private INetworkAddressProvider addressProvider;

        private Badumna.Autoreplication.Serialization.Manager serialization;

        [SetUp]
        public void SetUp()
        {
            this.parser = new MessageParser(
                this.layerName, typeof(UnitTestProtocolMethodAttribute), MessageParserTester.DefaultFactory);
            this.association = new AssociatedReplica();
            this.entityManager = MockRepository.GenerateMock<IEntityManager>();
            this.entityManager.Stub(f => f.Parser).Return(this.parser);
            this.entityManager.Stub(f => f.Association).Return(this.association);
            this.interestManagementService = MockRepository.GenerateMock<IInterestManagementService>();            
            this.eventQueue = new NetworkEventQueue();
            this.timeKeeper = this.eventQueue;
            
            this.serialization = new Badumna.Autoreplication.Serialization.Manager(this.timeKeeper);
            OriginalWrapperFactory<IReplicableEntity, IPositionalOriginalWrapper, KeyValuePair<float, float>> originalWrapperFactory = (o, d) => new PositionalOriginalWrapper(o, d.Key, d.Value, this.spatialEntityManager, this.serialization);
            ReplicaWrapperFactory<IReplicableEntity, IPositionalReplicaWrapper> replicaWrapperFactory = r => new PositionalReplicaWrapper(r, this.serialization, this.timeKeeper);
            this.autoreplicationManager = MockRepository.GenerateMock<SpatialAutoreplicationManager>(
                this.serialization,
                this.timeKeeper,
                originalWrapperFactory,
                replicaWrapperFactory);
            this.queueApplicationEvent = i => i.Invoke();
            this.connectivityReporter = MockRepository.GenerateMock<INetworkConnectivityReporter>();
            this.connectionNotifier = MockRepository.GenerateMock<IPeerConnectionNotifier>();

            this.addressProvider = MockRepository.GenerateStub<INetworkAddressProvider>();
            this.addressProvider.Stub(x => x.PublicAddress).Return(PeerAddress.GetLoopback(1));

            GenericCallBackReturn<BadumnaId, PeerAddress> generateBadumnaIdWithAddress =
                BadumnaIdGenerator.FromAddress();

            GenericCallBackReturn<BadumnaId> generateBadumnaId =
                () => generateBadumnaIdWithAddress(PeerAddress.GetLoopback(1));

            var options = new Options();

            this.spatialEntityManager = new SpatialEntityManager(
                this.entityManager,
                this.interestManagementService,
                this.autoreplicationManager,
                this.eventQueue,
                this.queueApplicationEvent,
                this.timeKeeper,
                this.connectivityReporter,
                this.connectionNotifier,
                this.addressProvider,
                generateBadumnaId,
                generateBadumnaIdWithAddress);
            this.spatialReplica = new TestSpatialReplica();
            this.createReplica +=
                delegate(NetworkScene scene, BadumnaId entityId, uint entityType) { return this.spatialReplica; };
            this.removeReplica += delegate(NetworkScene scene, IReplicableEntity replica) { };
            this.scene = new TestNetworkScene(
                this.sceneName, this.createReplica, this.removeReplica, this.eventQueue, this.queueApplicationEvent);
            this.collection = new SpatialReplicaCollection(
                this.spatialEntityManager, generateBadumnaId(), 0, this.eventQueue, this.queueApplicationEvent);
        }

        [TearDown]
        public void TearDown()
        {
            this.collection = null;
            this.scene = null;
            this.spatialEntityManager = null;
            this.timeKeeper = null;
            this.eventQueue = null;
            this.entityManager = null;
            this.association = null;
            this.parser = null;
        }

        [Test]
        public void AddToSceneShouldWorkWithNewScene()
        {
            Assert.AreEqual(0, this.collection.Count);
            this.collection.AddToScene(this.scene);
            Assert.AreEqual(1, this.collection.Count);
        }

        [Test]
        public void AddToSceneShouldNotWorkWithOldScene()
        {
            Assert.AreEqual(0, this.collection.Count);
            this.collection.AddToScene(this.scene);
            Assert.AreEqual(1, this.collection.Count);
            this.collection.AddToScene(this.scene);
            Assert.AreEqual(1, this.collection.Count);
        }

        [Test]
        public void RemoveFromSceneShouldWorkWithAddedScene()
        {
            Assert.AreEqual(0, this.collection.Count);
            this.collection.AddToScene(this.scene);
            Assert.AreEqual(1, this.collection.Count);
            this.collection.RemoveFromScene(this.scene);
            Assert.AreEqual(0, this.collection.Count);
        }

        [Test]
        public void RemoveFromSceneShouldNotWorkWithUnaddedScene()
        {
            Assert.AreEqual(0, this.collection.Count);
            this.collection.AddToScene(this.scene);
            Assert.AreEqual(1, this.collection.Count);

            NetworkScene newScene = new TestNetworkScene(
                new QualifiedName("", "New scene name"), this.createReplica, this.removeReplica, this.eventQueue, this.queueApplicationEvent);

            this.collection.RemoveFromScene(newScene);
            Assert.AreEqual(1, this.collection.Count);
        }

        [Test]
        public void RemoveFromAllScenesShouldWork()
        {
            Assert.AreEqual(0, this.collection.Count);
            this.collection.AddToScene(this.scene);
            Assert.AreEqual(1, this.collection.Count);

            NetworkScene newScene = new TestNetworkScene(
                new QualifiedName("", "New scene name"), this.createReplica, this.removeReplica, this.eventQueue, this.queueApplicationEvent);

            this.collection.AddToScene(newScene);
            Assert.AreEqual(2, this.collection.Count);

            this.collection.RemoveFromAllScenes();
            Assert.AreEqual(0, this.collection.Count);
        }

        [Test]
        public void DeserializeShouldWork()
        {
            Vector3 position = new Vector3(1.0f, 2.0f, 3.0f);
            float radius = 1.0f;

            Assert.AreNotEqual(position.X, this.collection.Position.X);
            Assert.AreNotEqual(position.Y, this.collection.Position.Y);
            Assert.AreNotEqual(position.Z, this.collection.Position.Z);
            Assert.AreNotEqual(radius, this.collection.Radius);

            MemoryStream outputStream = new MemoryStream();

            using (BinaryWriter writer = new BinaryWriter(outputStream))
            {
                writer.Write(this.sceneName.ToString());
                writer.Write(position.X);
                writer.Write(position.Y);
                writer.Write(position.Z);
                writer.Write(radius);
            }

            MemoryStream inputStream = new MemoryStream(outputStream.ToArray());
            BooleanArray includedParts = new BooleanArray();
            includedParts[(int)SpatialEntityStateSegment.Scene] = true;
            includedParts[(int)SpatialEntityStateSegment.Position] = true;
            includedParts[(int)SpatialEntityStateSegment.Radius] = true;

            this.collection.Deserialize(includedParts, inputStream, 0, this.collection.Guid);

            Assert.AreEqual(true, this.collection.SceneName.Equals(this.sceneName));
            Assert.AreEqual(position.X, this.collection.Position.X);
            Assert.AreEqual(position.Y, this.collection.Position.Y);
            Assert.AreEqual(position.Z, this.collection.Position.Z);
            Assert.AreEqual(radius, this.collection.Radius);
        }

        private class TestSpatialReplica : ISpatialReplica
        {
            public Vector3 Position { get; set; }

            public float Radius { get; set; }

            public float AreaOfInterestRadius { get; set; }

            public BadumnaId Guid { get; set; }

            public void Deserialize(BooleanArray includedParts, Stream stream, int estimatedMillisecondsSinceDeparture)
            {
            }

            public void HandleEvent(Stream stream)
            {
            }
        }

        private class TestNetworkScene : NetworkScene
        {
            internal TestNetworkScene(
                QualifiedName name,
                CreateSpatialReplica createReplica,
                RemoveSpatialReplica removeReplica,
                NetworkEventQueue eventQueue,
                QueueInvokable queueApplicationEvent)
                : base(
                    name,
                    createReplica,
                    removeReplica,
                    null,
                    eventQueue,
                    queueApplicationEvent,
                    false)
            {
            }

            protected override void LeaveImplimentation()
            {
            }

            protected override void RegisterEntityImplementation(ISpatialOriginal entity, uint entityType)
            {
            }

            protected override void UnregisterEntityImplementation(ISpatialOriginal entity)
            {
            }
        }
    }
}