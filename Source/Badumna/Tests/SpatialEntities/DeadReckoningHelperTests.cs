﻿//-----------------------------------------------------------------------
// <copyright file="DeadReckoningHelperTests.cs" company="NICTA">
//     Copyright (c) 2008 NICTA. All right reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BadumnaTests.SpatialEntities
{
    using System;
    using System.IO;

    using Badumna.Core;
    using Badumna.DataTypes;
    using Badumna.SpatialEntities;

    using NUnit.Framework;

    [TestFixture]
    [DontPerformCoverage]
    public class DeadReckoningHelperTests
    {
        private DeadReckoningHelper deadReckoningHelper = null;

        [SetUp]
        public void SetUp()
        {
            this.deadReckoningHelper = new DeadReckoningHelper();
        }

        [TearDown]
        public void TearDown()
        {
            this.deadReckoningHelper = null;
        }

        [Test]
        [ExpectedException(typeof(InvalidOperationException))]
        public void GetDestinationShouldThrowWithUnregisteredDeadReckonable()
        {
            IDeadReckonable deadReckonable = new TestDeadReckonable();
            deadReckonable.Guid = new BadumnaId(PeerAddress.GetLoopback(1), 1);

            this.deadReckoningHelper.GetDestination(deadReckonable);
        }

        [Test]
        [ExpectedException(typeof(InvalidOperationException))]
        public void SnapToDestinationShouldThrowWithUnregisteredDeadReckonable()
        {
            IDeadReckonable deadReckonable = new TestDeadReckonable();
            deadReckonable.Guid = new BadumnaId(PeerAddress.GetLoopback(1), 1);

            this.deadReckoningHelper.SnapToDestination(deadReckonable);
        }

        private class TestDeadReckonable : IDeadReckonable
        {
            public Vector3 Velocity { get; set; }

            public Vector3 Position { get; set; }

            public float Radius { get; set; }

            public float AreaOfInterestRadius { get; set; }

            public BadumnaId Guid { get; set; }

            public void AttemptMovement(Vector3 reckonedPosition)
            {
            }

            public void HandleEvent(Stream stream)
            {
            }
        }
    }
}