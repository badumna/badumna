﻿//-----------------------------------------------------------------------
// <copyright file="SpatialEntityManagerTests.cs" company="NICTA">
//     Copyright (c) 2009 NICTA. All right reserved.
// </copyright>
//-----------------------------------------------------------------------

using BadumnaTests.Utilities;

namespace BadumnaTests.SpatialEntities
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using Badumna.Autoreplication;
    using Badumna.Core;
    using Badumna.DataTypes;
    using Badumna.InterestManagement;
    using Badumna.Replication;
    using Badumna.SpatialEntities;
    using Badumna.Utilities;
    using BadumnaTests.Core;
    using NUnit.Framework;
    using Rhino.Mocks;

    using SpatialAutoreplicationManager = Badumna.Autoreplication.SpatialAutoreplicationManager;

    [TestFixture]
    [DontPerformCoverage]
    public class SpatialEntityManagerTests
    {
        private string layerName = "Test layer name";

        private QualifiedName sceneName = new QualifiedName("", "Test scene name");

        private MessageParser parser = null;

        private AssociatedReplica association = null;

        private IEntityManager entityManager = null;

        private Simulator eventQueue = null;

        private QueueInvokable queueApplicationEvent;

        private ITime timeKeeper = null;

        private INetworkConnectivityReporter connectivityReporter;

        private IPeerConnectionNotifier connectionNotifier;

        private IInterestManagementService interestManagementService = null;

        private SpatialAutoreplicationManager autoreplicationManager = null;

        private SpatialEntityManager spatialEntityManager = null;

        private CreateSpatialReplica createReplica = null;

        private RemoveSpatialReplica removeReplica = null;

        private INetworkAddressProvider addressProvider;

        private GenericCallBackReturn<BadumnaId, PeerAddress> generateBadumnaIdWithAddress;
        private GenericCallBackReturn<BadumnaId> generateBadumnaId;

        private Badumna.Autoreplication.Serialization.Manager serialization;

        [SetUp]
        public void SetUp()
        {
            this.parser = new MessageParser(this.layerName, typeof(UnitTestProtocolMethodAttribute), MessageParserTester.DefaultFactory);
            this.association = new AssociatedReplica();
            this.entityManager = MockRepository.GenerateMock<IEntityManager>();
            this.entityManager.Stub(f => f.Parser).Return(this.parser);
            this.entityManager.Stub(f => f.Association).Return(this.association);
            this.interestManagementService = MockRepository.GenerateMock<IInterestManagementService>();
            this.eventQueue = new Simulator();
            this.timeKeeper = this.eventQueue;
            this.serialization = new Badumna.Autoreplication.Serialization.Manager(this.timeKeeper);
            OriginalWrapperFactory<IReplicableEntity, IPositionalOriginalWrapper, KeyValuePair<float, float>> originalWrapperFactory = (o, d) => new PositionalOriginalWrapper(o, d.Key, d.Value, this.spatialEntityManager, this.serialization);
            ReplicaWrapperFactory<IReplicableEntity, IPositionalReplicaWrapper> replicaWrapperFactory = r => new PositionalReplicaWrapper(r, this.serialization, this.timeKeeper);
            this.autoreplicationManager = MockRepository.GenerateMock<SpatialAutoreplicationManager>(
                this.serialization,
                this.timeKeeper,
                originalWrapperFactory,
                replicaWrapperFactory);
            this.queueApplicationEvent = i => i.Invoke();
            this.connectivityReporter = MockRepository.GenerateMock<INetworkConnectivityReporter>();
            this.connectionNotifier = MockRepository.GenerateMock<IPeerConnectionNotifier>();

            this.addressProvider = MockRepository.GenerateStub<INetworkAddressProvider>();
            this.addressProvider.Stub(x => x.PublicAddress).Return(PeerAddress.GetLoopback(1));

            this.generateBadumnaIdWithAddress = BadumnaIdGenerator.FromAddress();

            this.generateBadumnaId = () => this.generateBadumnaIdWithAddress(PeerAddress.GetLoopback(1));

            this.spatialEntityManager = new SpatialEntityManager(
                this.entityManager,
                this.interestManagementService,
                this.autoreplicationManager,
                this.eventQueue,
                this.queueApplicationEvent,
                this.timeKeeper,
                this.connectivityReporter,
                this.connectionNotifier,
                this.addressProvider,
                this.generateBadumnaId,
                this.generateBadumnaIdWithAddress);
            this.createReplica += delegate(NetworkScene scene, BadumnaId entityId, uint entityType)
            {
                return null;
            };
            this.removeReplica += delegate(NetworkScene scene, IReplicableEntity replica)
            {
            };
        }

        [TearDown]
        public void TearDown()
        {
            this.spatialEntityManager = null;
            this.timeKeeper = null;
            this.eventQueue = null;
            this.entityManager = null;
            this.association = null;
            this.parser = null;
        }

        [Test]
        public void ConstructorShouldWork()
        {
            this.entityManager.Expect(m => m.Register(0, null, null)).IgnoreArguments();

            SpatialEntityManager spatialEntityManager = new SpatialEntityManager(
                this.entityManager,
                this.interestManagementService,
                this.autoreplicationManager,
                this.eventQueue,
                this.queueApplicationEvent,
                this.timeKeeper,
                this.connectivityReporter,
                this.connectionNotifier,
                this.addressProvider,
                this.generateBadumnaId,
                this.generateBadumnaIdWithAddress);
            this.entityManager.VerifyAllExpectations();
        }

        [Test]
        public void JoinSceneShouldWork()
        {
            Assert.IsNull(this.spatialEntityManager.TryGetScenes(this.sceneName));
            Assert.IsNotNull(this.spatialEntityManager.JoinScene(this.sceneName, this.createReplica, this.removeReplica, false));
            Assert.AreEqual(1, this.spatialEntityManager.TryGetScenes(this.sceneName).Count);
        }

        [Test]
        public void LeaveSceneShouldRemoveValidScene()
        {
            Assert.IsNull(this.spatialEntityManager.TryGetScenes(this.sceneName));

            NetworkScene newScene = this.spatialEntityManager.JoinScene(this.sceneName, this.createReplica, this.removeReplica, false);
            Assert.IsNotNull(newScene);
            Assert.AreEqual(1, this.spatialEntityManager.TryGetScenes(this.sceneName).Count);

            this.spatialEntityManager.LeaveScene(newScene);
            this.eventQueue.RunOne();
            Assert.IsNull(this.spatialEntityManager.TryGetScenes(this.sceneName));
        }

        [Test]
        public void LeaveSceneShouldNotRemoveInvalidScene()
        {
            Assert.IsNull(this.spatialEntityManager.TryGetScenes(this.sceneName));

            NetworkScene newScene = this.spatialEntityManager.JoinScene(this.sceneName, this.createReplica, this.removeReplica, false);
            Assert.IsNotNull(newScene);
            Assert.IsNotEmpty(this.spatialEntityManager.TryGetScenes(this.sceneName));

            NetworkScene invalidScene = new LocalNetworkScene(
                new QualifiedName("", "Invalid scene name"),
                this.spatialEntityManager,
                this.createReplica,
                this.removeReplica,
                null,
                this.interestManagementService,
                this.eventQueue,
                this.queueApplicationEvent,
                this.addressProvider,
                this.generateBadumnaId,
                false);
            this.spatialEntityManager.LeaveScene(invalidScene);
            Assert.IsNotEmpty(this.spatialEntityManager.TryGetScenes(this.sceneName));
        }

        [Test]
        public void AddOriginalShouldRegisterNewOriginal()
        {
            NetworkScene newScene = this.spatialEntityManager.JoinScene(this.sceneName, this.createReplica, this.removeReplica, false);
            Assert.IsNotNull(newScene);

            ISpatialOriginal spatialOriginal = new TestSpatialOriginal();
            spatialOriginal.Guid = this.generateBadumnaId();

            this.entityManager.Expect(m => m.RegisterEntity(null, new EntityTypeId(0, 0))).IgnoreArguments();
            this.spatialEntityManager.AddOriginal(spatialOriginal, 0, newScene);
            this.entityManager.VerifyAllExpectations();
        }

        [Test]
        public void AddOriginalShouldNotRegisterOldOriginal()
        {
            NetworkScene newScene = this.spatialEntityManager.JoinScene(this.sceneName, this.createReplica, this.removeReplica, false);
            Assert.IsNotNull(newScene);

            ISpatialOriginal spatialOriginal = new TestSpatialOriginal();
            spatialOriginal.Guid = this.generateBadumnaId();

            this.spatialEntityManager.AddOriginal(spatialOriginal, 0, newScene);

            this.entityManager.Expect(m => m.RegisterEntity(null, new EntityTypeId(0, 0))).Repeat.Never();
            this.spatialEntityManager.AddOriginal(spatialOriginal, 0, newScene);
            this.entityManager.VerifyAllExpectations();
        }

        [Test]
        public void RemoveOriginalShouldUnregisterValidOriginal()
        {
            NetworkScene newScene = this.spatialEntityManager.JoinScene(this.sceneName, this.createReplica, this.removeReplica, false);
            Assert.IsNotNull(newScene);

            ISpatialOriginal spatialOriginal = new TestSpatialOriginal();
            spatialOriginal.Guid = this.generateBadumnaId();

            this.spatialEntityManager.AddOriginal(spatialOriginal, 0, newScene);

            this.entityManager.Expect(m => m.UnregisterEntity(null, false)).IgnoreArguments();
            this.spatialEntityManager.RemoveOriginal(spatialOriginal, true);
            this.entityManager.VerifyAllExpectations();
        }

        [Test]
        public void RemoveOriginalShouldNotUnregisterInvalidOriginal()
        {
            NetworkScene newScene = this.spatialEntityManager.JoinScene(this.sceneName, this.createReplica, this.removeReplica, false);
            Assert.IsNotNull(newScene);

            ISpatialOriginal spatialOriginal = new TestSpatialOriginal();
            spatialOriginal.Guid = this.generateBadumnaId();

            this.spatialEntityManager.AddOriginal(spatialOriginal, 0, newScene);

            ISpatialOriginal invalidOriginal = new TestSpatialOriginal();
            invalidOriginal.Guid = this.generateBadumnaId();

            this.entityManager.Expect(m => m.UnregisterEntity(null, false)).Repeat.Never();
            this.spatialEntityManager.RemoveOriginal(invalidOriginal, true);
            this.entityManager.VerifyAllExpectations();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void MarkForUpdateShouldThrowWithNullSpatialOriginal()
        {
            this.spatialEntityManager.MarkForUpdate(null, new BooleanArray());
        }

        [Test, Ignore]
        public void MarkForUpdateShouldNotWorkForSpatialOriginalWithNullGuid()
        {
            // TODO: Fix to really test.
            this.entityManager.Expect(m => m.MarkForUpdate(null, null)).Repeat.Never();
            this.spatialEntityManager.MarkForUpdate(new TestSpatialOriginal(), new BooleanArray());
            this.entityManager.VerifyAllExpectations();
        }

        [Test, Ignore]
        public void MarkForUpdateShouldNotWorkForUnaddedSpatialOriginal()
        {
            ISpatialOriginal spatialOriginal = new TestSpatialOriginal();
            spatialOriginal.Guid = this.generateBadumnaId();

            this.entityManager.Expect(m => m.MarkForUpdate(null, null)).Repeat.Never();
            this.spatialEntityManager.MarkForUpdate(spatialOriginal, new BooleanArray());
            this.entityManager.VerifyAllExpectations();
        }

        [Test, Ignore]
        public void MarkForUpdateShouldWorkForAddedSpatialOriginal()
        {
            // TODO: Fix to really test.
            NetworkScene newScene = this.spatialEntityManager.JoinScene(this.sceneName, this.createReplica, this.removeReplica, false);
            Assert.IsNotNull(newScene);

            ISpatialOriginal spatialOriginal = new TestSpatialOriginal();
            spatialOriginal.Guid = this.generateBadumnaId();

            this.spatialEntityManager.AddOriginal(spatialOriginal, 0, newScene);

            this.entityManager.Expect(m => m.MarkForUpdate(null, null)).IgnoreArguments();
            this.spatialEntityManager.MarkForUpdate(spatialOriginal, new BooleanArray());
            this.entityManager.VerifyAllExpectations();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void MarkForUpdate2ShouldThrowWithNullSpatialOriginal()
        {
            this.spatialEntityManager.MarkForUpdate(null, 0);
        }

        [Test, Ignore]
        public void MarkForUpdate2ShouldNotWorkForSpatialOriginalWithNullGuid()
        {
            // TODO: Fix to really test.
            this.entityManager.Expect(m => m.MarkForUpdate(null, 0)).Repeat.Never();
            this.spatialEntityManager.MarkForUpdate(new TestSpatialOriginal(), 0);
            this.entityManager.VerifyAllExpectations();
        }

        [Test, Ignore]
        public void MarkForUpdate2ShouldNotScheduleOperationInEventQueueForUnaddedSpatialOriginal()
        {
            // TODO: Fix to use INetworkEventScheduler.

            ////// Arrange
            ////ISpatialOriginal spatialOriginal = new TestSpatialOriginal();
            ////spatialOriginal.Guid = BadumnaId.GetNextUniqueId();
            ////int changedPartIndex = 9;

            ////// Expect
            ////this.eventQueue.Expect(q => q.Push(
            ////    Arg<GenericCallBack<ISpatialOriginal, int>>.Is.Anything,
            ////    Arg<ISpatialOriginal>.Is.Equal(spatialOriginal),
            ////    Arg<int>.Is.Equal(changedPartIndex)))
            ////    .Repeat.Never();

            ////// Act
            ////this.spatialEntityManager.MarkForUpdate(spatialOriginal, changedPartIndex);

            ////// Verify
            ////this.eventQueue.VerifyAllExpectations();
        }

        [Test, Ignore]
        public void MarkForUpdate2ShouldScheduleOperationInEventQueueForAddedSpatialOriginal()
        {
            ////// Arrange
            ////NetworkScene newScene = this.spatialEntityManager.JoinScene(this.sceneName, this.createReplica, this.removeReplica);
            ////Assert.IsNotNull(newScene);
            ////ISpatialOriginal spatialOriginal = new TestSpatialOriginal();
            ////spatialOriginal.Guid = BadumnaId.GetNextUniqueId();
            ////this.spatialEntityManager.AddOriginal(spatialOriginal, 0, newScene);
            ////int changedPartIndex = 9;

            ////// Expect
            ////this.eventQueue.Expect(q => q.Push(
            ////    Arg<GenericCallBack<ISpatialOriginal, int>>.Is.Anything,
            ////    Arg<ISpatialOriginal>.Is.Equal(spatialOriginal),
            ////    Arg<int>.Is.Equal(changedPartIndex)))
            ////    .Return(null);

            ////// Act
            ////this.spatialEntityManager.MarkForUpdate(spatialOriginal, changedPartIndex);

            ////// Verify
            ////this.eventQueue.VerifyAllExpectations();
        }

        [Test, Ignore]
        public void MarkForUpdate2ScheduledOperationShouldPassRequestToEnityManagerForAddedSpatialOriginal()
        {
            ////// Arrange
            ////NetworkScene newScene = this.spatialEntityManager.JoinScene(this.sceneName, this.createReplica, this.removeReplica);
            ////Assert.IsNotNull(newScene);
            ////ISpatialOriginal spatialOriginal = new TestSpatialOriginal();
            ////spatialOriginal.Guid = BadumnaId.GetNextUniqueId();
            ////this.spatialEntityManager.AddOriginal(spatialOriginal, 0, newScene);
            ////int changedPartIndex = 9;
            ////var callback = new CapturingConstraint<GenericCallBack<ISpatialOriginal, int>>();
            ////var passedOriginal = new CapturingConstraint<ISpatialOriginal>();

            ////this.eventQueue.Stub(q => q.Push(
            ////    Arg<GenericCallBack<ISpatialOriginal, int>>.Matches(callback),
            ////    Arg<ISpatialOriginal>.Matches(passedOriginal),
            ////    Arg<int>.Is.Equal(changedPartIndex)))
            ////    .Return(null);
            ////this.spatialEntityManager.MarkForUpdate(spatialOriginal, changedPartIndex);

            ////// Expect
            ////this.entityManager.Expect(m => m.MarkForUpdate(
            ////    Arg<IOriginal>.Is.Anything,
            ////    changedPartIndex));

            ////// Act
            ////callback.Argument.Invoke(passedOriginal.Argument, changedPartIndex);

            ////// Verify
            ////this.entityManager.VerifyAllExpectations();
        }

        [Test]
        [ExpectedException(typeof(InvalidOperationException))]
        public void SendCustomMessageShouldThrowWithUnaddedSpatialOriginal()
        {
            ISpatialOriginal spatialOriginal = new TestSpatialOriginal();
            spatialOriginal.Guid = this.generateBadumnaId();

            this.spatialEntityManager.SendCustomMessageToReplicas(spatialOriginal, null);
        }

        [Test]
        public void SendCustomMessageShouldWorkWithAddedSpatialOriginal()
        {
            NetworkScene newScene = this.spatialEntityManager.JoinScene(this.sceneName, this.createReplica, this.removeReplica, false);
            Assert.IsNotNull(newScene);

            ISpatialOriginal spatialOriginal = new TestSpatialOriginal();
            spatialOriginal.Guid = this.generateBadumnaId();

            this.spatialEntityManager.AddOriginal(spatialOriginal, 0, newScene);

            this.entityManager.Expect(m => m.SendCustomMessageToReplicas(new TestOriginal(), null)).IgnoreArguments();
            this.spatialEntityManager.SendCustomMessageToReplicas(spatialOriginal, null);
            this.entityManager.VerifyAllExpectations();
        }

        [Test]
        [ExpectedException(typeof(InvalidOperationException))]
        public void SendCustomMessageShouldThrowWithUnaddedSpatialReplica()
        {
            ISpatialReplica spatialReplica = new TestSpatialReplica();
            spatialReplica.Guid = this.generateBadumnaId();

            this.spatialEntityManager.SendCustomMessageToOriginal(spatialReplica, null);
        }

        [Test]
        public void CalculateAttentionShouldWork()
        {
            NetworkScene newScene = this.spatialEntityManager.JoinScene(this.sceneName, this.createReplica, this.removeReplica, false);
            Assert.IsNotNull(newScene);

            MemoryStream outputStream = new MemoryStream();
            Vector3 position = new Vector3(0.0f, 0.0f, 0.0f);
            float radius = 1.0f;

            using (BinaryWriter writer = new BinaryWriter(outputStream))
            {
                writer.Write(this.sceneName.ToString());
                writer.Write(position.X);
                writer.Write(position.Y);
                writer.Write(position.Z);
                writer.Write(radius);
            }

            MemoryStream inputStream = new MemoryStream(outputStream.ToArray());
            BooleanArray includedParts = new BooleanArray();
            includedParts[(int)SpatialEntityStateSegment.Scene] = true;
            includedParts[(int)SpatialEntityStateSegment.Position] = true;
            includedParts[(int)SpatialEntityStateSegment.Radius] = true;

            SpatialReplicaCollection collection = new SpatialReplicaCollection(this.spatialEntityManager, this.generateBadumnaId(), 0, this.eventQueue, this.queueApplicationEvent);

            collection.Deserialize(includedParts, inputStream, 0, collection.Guid);
            Assert.AreEqual(0.0f, this.spatialEntityManager.CalculateAttention(collection));

            ISpatialOriginal spatialOriginal = new TestSpatialOriginal();
            spatialOriginal.Guid = this.generateBadumnaId();
            spatialOriginal.Position = new Vector3(1.0f, 1.0f, 1.0f);
            spatialOriginal.AreaOfInterestRadius = 10.0f;

            this.spatialEntityManager.AddOriginal(spatialOriginal, 0, newScene);
            Assert.AreEqual(1.0f, this.spatialEntityManager.CalculateAttention(collection));
        }

        private class TestSpatialOriginal : ISpatialOriginal
        {
            public Vector3 Position { get; set; }

            public float Radius { get; set; }

            public float AreaOfInterestRadius { get; set; }

            public BadumnaId Guid { get; set; }

            public void Serialize(BooleanArray requiredParts, Stream stream)
            {
            }

            public void HandleEvent(Stream stream)
            {
            }
        }

        private class TestSpatialReplica : ISpatialReplica
        {
            public Vector3 Position { get; set; }

            public float Radius { get; set; }

            public float AreaOfInterestRadius { get; set; }

            public BadumnaId Guid { get; set; }

            public void Deserialize(BooleanArray includedParts, Stream stream, int estimatedMillisecondsSinceDeparture)
            {
            }

            public void HandleEvent(Stream stream)
            {
            }
        }

        private class TestOriginal : IOriginal
        {
            public BadumnaId Guid { get; set; }

            public BooleanArray Serialize(BooleanArray requiredParts, Stream stream)
            {
                return null;
            }

            public void HandleEvent(Stream stream)
            {
            }
        }
    }
}
