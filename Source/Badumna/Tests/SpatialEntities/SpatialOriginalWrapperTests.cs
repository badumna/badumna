﻿//-----------------------------------------------------------------------
// <copyright file="SpatialOriginalWrapperTests.cs" company="NICTA">
//     Copyright (c) 2009 NICTA. All right reserved.
// </copyright>
//-----------------------------------------------------------------------

using BadumnaTests.Utilities;

namespace BadumnaTests.SpatialEntities
{
    using System.IO;

    using Badumna.Core;
    using Badumna.DataTypes;
    using Badumna.InterestManagement;
    using Badumna.Replication;
    using Badumna.SpatialEntities;
    using Badumna.Utilities;

    using NUnit.Framework;

    using Rhino.Mocks;

    [TestFixture]
    [DontPerformCoverage]
    public class SpatialOriginalWrapperTests
    {
        private QualifiedName sceneName = new QualifiedName("", "Test scene name");

        private float areaOfInterestRadius = 10.0f;

        private float radius = 1.0f;

        private float posX = 1.0f;

        private float posY = 2.0f;

        private float posZ = 3.0f;

        private IEntityManager entityManager = null;

        private NetworkEventQueue eventQueue = null;

        private ITime timeKeeper = null;

        private INetworkConnectivityReporter connectivityReporter;

        private CreateSpatialReplica createReplica = null;

        private RemoveSpatialReplica removeReplica = null;

        private NetworkScene scene = null;

        private ISpatialOriginal spatialOriginal = null;

        private SpatialOriginalWrapper wrapper = null;

        private IInterestManagementService interestManagementService = null;

        private GenericCallBackReturn<BadumnaId, PeerAddress> generateBadumnaIdWithAddress;

        [SetUp]
        public void SetUp()
        {
            this.interestManagementService = MockRepository.GenerateMock<IInterestManagementService>();
            this.entityManager = MockRepository.GenerateMock<IEntityManager>();
            this.eventQueue = new NetworkEventQueue();
            this.timeKeeper = this.eventQueue;
            this.connectivityReporter = MockRepository.GenerateMock<INetworkConnectivityReporter>();
            this.createReplica += delegate(NetworkScene scene, BadumnaId entityId, uint entityType)
            {
                return null;
            };
            this.removeReplica += delegate(NetworkScene scene, IReplicableEntity replica)
            {
            };
            this.scene = new TestNetworkScene(this.sceneName, this.createReplica, this.removeReplica, this.eventQueue, i => i.Invoke());

            this.generateBadumnaIdWithAddress = BadumnaIdGenerator.FromAddress();

            this.spatialOriginal = new TestSpatialOriginal();
            this.spatialOriginal.Guid = this.generateBadumnaIdWithAddress(PeerAddress.GetLoopback(1));
            this.spatialOriginal.AreaOfInterestRadius = this.areaOfInterestRadius;
            this.spatialOriginal.Radius = this.radius;
            this.spatialOriginal.Position = new Vector3(this.posX, this.posY, this.posZ);
            this.wrapper = new SpatialOriginalWrapper(this.spatialOriginal, 0, this.scene, this.interestManagementService, this.entityManager, this.eventQueue, this.timeKeeper, this.connectivityReporter, this.generateBadumnaIdWithAddress);
        }

        [TearDown]
        public void TearDown()
        {
            this.wrapper = null;
            this.spatialOriginal = null;
            this.scene = null;
            this.timeKeeper = null;
            this.eventQueue = null;
            this.entityManager = null;
        }

        [Test]
        public void ConstructorShouldWork()
        {
            SpatialOriginalWrapper wrapper = new SpatialOriginalWrapper(this.spatialOriginal, 0, this.scene, this.interestManagementService, this.entityManager, this.eventQueue, this.timeKeeper, this.connectivityReporter, this.generateBadumnaIdWithAddress);
            Assert.IsNotNull(wrapper.InterestRegion);
        }

        [Test]
        public void ShutdownShouldWork()
        {
            this.wrapper.Shutdown(true);
            Assert.IsNull(this.wrapper.InterestRegion);
        }

        [Test]
        public void MarkForUpdateShouldWork()
        {
            this.entityManager.Expect(m => m.MarkForUpdate(null, null)).IgnoreArguments();
            this.wrapper.MarkForUpdate(new BooleanArray());
            this.entityManager.VerifyAllExpectations();
        }

        [Test]
        public void MarkForUpdate2ShouldWork()
        {
            this.entityManager.Expect(m => m.MarkForUpdate(null, 0)).IgnoreArguments();
            this.wrapper.MarkForUpdate(0);
            this.entityManager.VerifyAllExpectations();
        }

        [Test]
        public void IsInterestedInShouldReturnFalseWithUninterestingEntity()
        {
            float inflatedInterestRadius = this.areaOfInterestRadius + (this.interestManagementService.SubscriptionInflation * 4.0f);
            Assert.AreEqual(false, this.wrapper.IsInterestedIn(this.sceneName, new Vector3(this.posX + inflatedInterestRadius + 1.0f, this.posY, this.posZ), this.radius));
        }

        [Test]
        public void IsInterestedInShouldReturnTrueWithInterestingEntity()
        {
            float inflatedInterestRadius = this.areaOfInterestRadius + (this.interestManagementService.SubscriptionInflation * 4.0f);
            Assert.AreEqual(true, this.wrapper.IsInterestedIn(this.sceneName, new Vector3(this.posX + inflatedInterestRadius, this.posY, this.posZ), this.radius));
        }

        [Test]
        public void PeerUninterestedFeedbackShouldWork()
        {
            this.entityManager.Expect(m => m.RemoveInterested(null, null)).IgnoreArguments();
            this.wrapper.PeerUninterestedFeedback(this.generateBadumnaIdWithAddress(PeerAddress.GetLoopback(1)));
            this.entityManager.VerifyAllExpectations();
        }

        [Test]
        public void SerializeShouldWork()
        {
            MemoryStream inputStream = new MemoryStream();
            BooleanArray requiredParts = new BooleanArray();
            requiredParts[(int)SpatialEntityStateSegment.Scene] = true;
            requiredParts[(int)SpatialEntityStateSegment.Position] = true;
            requiredParts[(int)SpatialEntityStateSegment.Radius] = true;
            requiredParts[(int)SpatialEntityStateSegment.InterestRadius] = true;

            this.wrapper.Serialize(requiredParts, inputStream);

            MemoryStream outputStream = new MemoryStream(inputStream.ToArray());
            BinaryReader reader = new BinaryReader(outputStream);

            var sceneName = new QualifiedName(reader.ReadString());
            Assert.AreEqual(true, sceneName.Equals(this.sceneName));

            float x = reader.ReadSingle();
            Assert.AreEqual(this.posX, x);

            float y = reader.ReadSingle();
            Assert.AreEqual(this.posY, y);

            float z = reader.ReadSingle();
            Assert.AreEqual(this.posZ, z);

            float radius = reader.ReadSingle();
            Assert.AreEqual(this.radius, radius);

            float interestRadius = reader.ReadSingle();
            Assert.AreEqual(this.areaOfInterestRadius, interestRadius);
        }

        [Test]
        public void MiniNetworkSceneShouldRegisterTheOriginalInTheOrigin()
        {
            this.scene = new TestMiniNetworkScene(this.sceneName, this.createReplica, this.removeReplica, this.eventQueue, i => i.Invoke(), true);
            this.wrapper = new SpatialOriginalWrapper(this.spatialOriginal, 0, this.scene, this.interestManagementService, this.entityManager, this.eventQueue, this.timeKeeper, this.connectivityReporter, this.generateBadumnaIdWithAddress);

            float expectedAreaOfInterestRadius = 1.0f;

            Assert.AreEqual(new Vector3(0, 0, 0), this.wrapper.InterestRegion.Centroid);
            Assert.AreEqual(expectedAreaOfInterestRadius, this.wrapper.InterestRegion.Radius);
        }

        [Test]
        public void MiniNetworkSceneShouldLimitRadii()
        {
            this.scene = new TestMiniNetworkScene(this.sceneName, this.createReplica, this.removeReplica, this.eventQueue, i => i.Invoke(), true);
            this.spatialOriginal.Radius = 1000;
            this.spatialOriginal.AreaOfInterestRadius = 2000;

            var inflation = 30.0f;
            this.interestManagementService.Stub(x => x.SubscriptionInflation).Return(inflation);

            // Should probably not inflate the regions for mini scenes, but that'd require a bigger change.
            this.interestManagementService.Expect(ims => ims.InsertRegion(new QualifiedName(), null, 0, null, null))
                .IgnoreArguments()
                .Repeat.Twice()  // Once for entity, once for interest region
                .WhenCalled(m => Assert.AreEqual(inflation + 1.0f, ((ImsRegion)m.Arguments[1]).Radius));

            this.interestManagementService.Expect(ims => ims.ModifyRegion(new QualifiedName(), null, 0, 0))
                .IgnoreArguments()
                .Repeat.Never();  // Once inserted, we shouldn't insert again (except for keep-alive).

            this.wrapper = new SpatialOriginalWrapper(this.spatialOriginal, 0, this.scene, this.interestManagementService, this.entityManager, this.eventQueue, this.timeKeeper, this.connectivityReporter, this.generateBadumnaIdWithAddress);

            this.wrapper.CheckImsRegions();
            this.wrapper.CheckImsRegions();

            this.interestManagementService.VerifyAllExpectations();
        }

        private class TestSpatialOriginal : ISpatialOriginal
        {
            public Vector3 Position { get; set; }

            public float Radius { get; set; }

            public float AreaOfInterestRadius { get; set; }

            public BadumnaId Guid { get; set; }

            public void Serialize(BooleanArray requiredParts, Stream stream)
            {
            }

            public void HandleEvent(Stream stream)
            {
            }
        }

        private class TestNetworkScene : NetworkScene
        {
            internal TestNetworkScene(QualifiedName name, CreateSpatialReplica createReplica, RemoveSpatialReplica removeReplica, NetworkEventQueue eventQueue, QueueInvokable queueApplicationEvent)
                : base(name, createReplica, removeReplica, null, eventQueue, queueApplicationEvent, false)
            {
            }

            protected override void LeaveImplimentation()
            {
            }

            protected override void RegisterEntityImplementation(ISpatialOriginal entity, uint entityType)
            {
            }

            protected override void UnregisterEntityImplementation(ISpatialOriginal entity)
            {
            }
        }

        private class TestMiniNetworkScene : NetworkScene
        {
            internal TestMiniNetworkScene(
                QualifiedName name,
                CreateSpatialReplica createReplica,
                RemoveSpatialReplica removeReplica,                
                NetworkEventQueue eventQueue,
                QueueInvokable queueApplicationEvent,
                bool miniScene)
                : base(name, createReplica, removeReplica, null, eventQueue, queueApplicationEvent, miniScene)
            {
            }

            protected override void LeaveImplimentation()
            {
            }

            protected override void RegisterEntityImplementation(ISpatialOriginal entity, uint entityType)
            {
            }

            protected override void UnregisterEntityImplementation(ISpatialOriginal entity)
            {
            }
        }
    }
}
