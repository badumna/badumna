﻿//-----------------------------------------------------------------------
// <copyright file="NetworkSceneTests.cs" company="NICTA">
//     Copyright (c) 2011 NICTA. All right reserved.
// </copyright>
//-----------------------------------------------------------------------

using BadumnaTests.Utilities;

namespace BadumnaTests.SpatialEntities
{
    using System;
    using System.IO;
    using Badumna.Autoreplication;
    using Badumna.Core;
    using Badumna.DataTypes;
    using Badumna.SpatialEntities;
    using Badumna.Utilities;
    using NUnit.Framework;

    [TestFixture]
    [DontPerformCoverage]
    public class NetworkSceneTests
    {
        private QualifiedName name = new QualifiedName("", "Test name");

        private CreateSpatialReplica createReplica = null;

        private RemoveSpatialReplica removeReplica = null;

        private NetworkEventQueue eventQueue = null;

        private NetworkScene scene = null;

        private QueueInvokable queueApplicationEvent;

        private GenericCallBackReturn<BadumnaId> generateBadumnaId;

        [SetUp]
        public void SetUp()
        {
            this.createReplica += delegate(NetworkScene scene, BadumnaId entityId, uint entityType)
            {
                return null;
            };
            this.removeReplica += delegate(NetworkScene scene, IReplicableEntity replica)
            {
            };
            this.eventQueue = new NetworkEventQueue();
            this.queueApplicationEvent = i => i.Invoke();
            this.scene = new TestNetworkScene(this.name, this.createReplica, this.removeReplica, this.eventQueue, this.queueApplicationEvent);

            this.generateBadumnaId = BadumnaIdGenerator.Local();
        }

        [TearDown]
        public void TearDown()
        {
            this.scene = null;
            this.eventQueue = null;
        }

        [Test]
        public void ConstructorShouldWork()
        {
            NetworkScene scene = new TestNetworkScene(this.name, this.createReplica, this.removeReplica, this.eventQueue, this.queueApplicationEvent);
        }

        [Test]
        public void ConstructingMiniSceneShouldWork()
        {
            NetworkScene scene = new TestMiniNetworkScene(this.name, this.createReplica, this.removeReplica, this.eventQueue, this.queueApplicationEvent, true);
            Assert.IsNotNull(scene);
            Assert.IsTrue(scene.MiniScene);
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void RegisterEntityThrowsWithNullSpatialOriginal()
        {
            this.scene.RegisterEntity((ISpatialOriginal)null, 0);
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void RegisterEntityThrowsWithNullReplicableEntity()
        {
            this.scene.RegisterEntity((ISpatialEntity)null, 0, 10f, 100f);
        }

        [Test]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void RegisterEntityThrowsWithNegativeAreaOfInterestRadius()
        {
            ISpatialOriginal original = new TestSpatialOriginal();
            original.AreaOfInterestRadius = -1.0f;

            this.scene.RegisterEntity(original, 0);
        }

        [Test]
        public void RegisterEntityShouldWorkWithValidSpatialOriginal()
        {
            Assert.IsEmpty(this.scene.Originals);

            ISpatialOriginal original = new TestSpatialOriginal();
            original.AreaOfInterestRadius = 1.0f;
            original.Guid = this.generateBadumnaId();

            this.scene.RegisterEntity(original, 0);
            Assert.IsNotEmpty(this.scene.Originals);
        }

        [Test]
        public void UnregisterEntityShouldNotWorkWithUnregisteredSpatialOriginal()
        {
            Assert.IsEmpty(this.scene.Originals);

            ISpatialOriginal spatialOriginal = new TestSpatialOriginal();
            spatialOriginal.AreaOfInterestRadius = 1.0f;
            spatialOriginal.Guid = this.generateBadumnaId();

            this.scene.RegisterEntity(spatialOriginal, 0);
            Assert.IsNotEmpty(this.scene.Originals);

            ISpatialOriginal invalidOriginal = new TestSpatialOriginal();
            invalidOriginal.Guid = this.generateBadumnaId();

            this.scene.UnregisterEntity(invalidOriginal);
            Assert.IsNotEmpty(this.scene.Originals);
        }

        [Test]
        public void UnregisterEntityShouldWorkWithRegisteredSpatialOriginal()
        {
            Assert.IsEmpty(this.scene.Originals);

            ISpatialOriginal spatialOriginal = new TestSpatialOriginal();
            spatialOriginal.AreaOfInterestRadius = 1.0f;
            spatialOriginal.Guid = this.generateBadumnaId();

            this.scene.RegisterEntity(spatialOriginal, 0);
            Assert.IsNotEmpty(this.scene.Originals);

            this.scene.UnregisterEntity(spatialOriginal);
            Assert.IsEmpty(this.scene.Originals);
        }

        private class TestNetworkScene : NetworkScene
        {
            internal TestNetworkScene(QualifiedName name, CreateSpatialReplica createReplica, RemoveSpatialReplica removeReplica, NetworkEventQueue eventQueue, QueueInvokable queueApplicationEvent)
                : base(name, createReplica, removeReplica, null, eventQueue, queueApplicationEvent, false)
            {
            }

            protected override void LeaveImplimentation()
            {
            }

            protected override void RegisterEntityImplementation(ISpatialOriginal entity, uint entityType)
            {
            }

            protected override void UnregisterEntityImplementation(ISpatialOriginal entity)
            {
            }
        }

        private class TestMiniNetworkScene : NetworkScene
        {
            internal TestMiniNetworkScene(QualifiedName name, CreateSpatialReplica createReplica, RemoveSpatialReplica removeReplica, NetworkEventQueue eventQueue, QueueInvokable queueApplicationEvent, bool miniScene)
                : base(name, createReplica, removeReplica, null, eventQueue, queueApplicationEvent, miniScene)
            {
            }

            protected override void LeaveImplimentation()
            {
            }

            protected override void RegisterEntityImplementation(ISpatialOriginal entity, uint entityType)
            {
            }

            protected override void UnregisterEntityImplementation(ISpatialOriginal entity)
            {
            }
        }

        private class TestSpatialOriginal : ISpatialOriginal
        {
            public Vector3 Position { get; set; }

            public float Radius { get; set; }

            public float AreaOfInterestRadius { get; set; }

            public BadumnaId Guid { get; set; }

            public void Serialize(BooleanArray requiredParts, Stream stream)
            {
            }

            public void HandleEvent(Stream stream)
            {
            }
        }
    }
}
