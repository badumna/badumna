﻿//-----------------------------------------------------------------------
// <copyright file="OriginalWrapperTests.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

[assembly: System.Runtime.CompilerServices.InternalsVisibleTo(Rhino.Mocks.RhinoMocks.StrongName)]

namespace BadumnaTests.Autoreplication
{
    using System;
    using Badumna;
    using Badumna.Autoreplication;
    using Badumna.Autoreplication.Serialization;
    using Badumna.Core;
    using Badumna.DataTypes;
    using Badumna.SpatialEntities;
    using Badumna.Utilities;

    using NUnit.Framework;
    using Rhino.Mocks;
    using Rhino.Mocks.Constraints;

    using Manager = Badumna.Autoreplication.Serialization.Manager;

    [TestFixture]
    [DontPerformCoverage]
    public class PositionalOriginalWrapperTests
    {
        private ISpatialEntityManager spatialEntityManager;

        private Manager serialization;

        private PositionalReplicableEntity entity;

        private PositionalOriginalWrapper wrapper;

        private ITime timeKeeper;

        [SetUp]
        public void SetUp()
        {
            this.spatialEntityManager = MockRepository.GenerateMock<ISpatialEntityManager>();
            this.timeKeeper = new FakeTimeKeeper();
            this.serialization = new Manager(this.timeKeeper);
            TypeRegistrar.RegisterTestTypes(this.serialization);
            this.entity = new PositionalReplicableEntity();
            this.wrapper = new PositionalOriginalWrapper(this.entity, 10f, 100f, this.spatialEntityManager, this.serialization);
        }

        [TearDown]
        public void TearDown()
        {
        }

        [Test]
        public void ConstructorThrowsForNullReplicableEntity()
        {
            Assert.Throws<ArgumentNullException>(
                () => new PositionalOriginalWrapper(null, 10f, 100f, this.spatialEntityManager, this.serialization));
        }

        [Test]
        public void ConstructorThrowsForNullNetworkFacade()
        {
            Assert.Throws<ArgumentNullException>(
                () => new PositionalOriginalWrapper(this.entity, 10f, 100f, null, this.serialization));
        }

        [Test]
        public void ConstructorThrowsForBadReplicableProperty()
        {
            Assert.Throws<TypeReplicationException>(
                () => new PositionalOriginalWrapper(new BadTestReplicableEntity(), 10f, 100f, this.spatialEntityManager, this.serialization));
        }

        [Test]
        public void TickFlagsChangedPropertyForUpdate()
        {
            this.entity.Bool = true;
            this.spatialEntityManager.Expect(n => n.MarkForUpdate(
                    Arg<ISpatialOriginal>.Is.Equal(this.wrapper),
                    Arg<BooleanArray>.Matches(x => ((BooleanArray)x)[(int)SpatialEntityStateSegment.FirstAvailableSegment])));
            this.wrapper.Tick(TimeSpan.FromHours(9));
            this.spatialEntityManager.VerifyAllExpectations();
        }

        [Test]
        public void TickDoesNotFlagUnchangedPropertyForUpdate()
        {
            this.entity.Position = new Vector3(99, 99, 99);
            this.spatialEntityManager.Expect(n => n.MarkForUpdate(
                    Arg<ISpatialOriginal>.Is.Equal(this.wrapper),
                    Arg<BooleanArray>.Matches(x => !((BooleanArray)x)[(int)SpatialEntityStateSegment.FirstAvailableSegment])));
            this.wrapper.Tick(TimeSpan.FromHours(18));
            this.spatialEntityManager.VerifyAllExpectations();
        }
    }
}
 