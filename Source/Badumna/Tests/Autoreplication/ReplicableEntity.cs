﻿// -----------------------------------------------------------------------
// <copyright file="ReplicableEntity.cs" company="National ICT Australia Limited">
//     Copyright (c) 2012 National ICT Australia Limited. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace BadumnaTests.Autoreplication
{
    using Badumna;
    using Badumna.DataTypes;

    /// <summary>
    /// Simple replicable entity for testing.
    /// </summary>
    public class ReplicableEntity
    {
        private MutableValueType mutableValueType;

        [Replicable]
        public bool Bool { get; set; }

        [Replicable]
        public byte Byte { get; set; }

        [Replicable]
        public char Char { get; set; }

        [Replicable]
        public decimal Decimal { get; set; }

        [Replicable]
        public double Double { get; set; }

        [Replicable]
        public short Short { get; set; }

        [Replicable]
        public int Int { get; set; }

        [Replicable]
        public long Long { get; set; }

        [Replicable]
        public sbyte Sbyte { get; set; }

        [Replicable]
        public float Float { get; set; }

        [Replicable]
        public ushort Ushort { get; set; }

        [Replicable]
        public uint Uint { get; set; }

        [Replicable]
        public ulong Ulong { get; set; }

        [Replicable]
        public string String { get; set; }

        [Replicable]
        public Vector3 Vector3 { get; set; }

        [Replicable]
        public MutableReferenceType MutableReferenceType { get; set; }

        [Replicable]
        public MutableValueType MutableValueType
        {
            get
            {
                return this.mutableValueType;
            }

            set
            {
                this.mutableValueType = value;
            }
        }

        [Replicable]
        public ImmutableReferenceType ImmutableReferenceType { get; set; }

        [Replicable]
        public ImmutableValueType ImmutableValueType { get; set; }

        public int RPCParam1 { get; set; }

        public int RPCParam2 { get; set; }

        [Replicable]
        public void RPC(int x, int y)
        {
            this.RPCParam1 = x;
            this.RPCParam2 = y;
        }

        public void MutateValueType(int number, string word)
        {
            this.mutableValueType.Number = number;
            this.mutableValueType.Word = word;
        }

        ////public void HandleEvent(Stream stream)
        ////{
        ////    // Not implemented.
        ////}
    }
}