﻿//-----------------------------------------------------------------------
// <copyright file="FakeNetworkFacade.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BadumnaTests.Autoreplication
{
    using System;
    using System.IO;
    using Badumna;
    using Badumna.DataTypes;
    using Badumna.Matchmaking;
    using Badumna.SpatialEntities;
    using Badumna.Utilities;

    internal abstract class FakeNetworkFacade : INetworkFacade
    {
#pragma warning disable 67

        public virtual event ConnectivityStatusDelegate OfflineEvent;

        public virtual event ConnectivityStatusDelegate OnlineEvent;

        public virtual event PublicAddressChangedDelegate AddressChangedEvent;

        public virtual event EventHandler RequestShutdown;

        public virtual event EventHandler RequestShutdownForUpdate;

#pragma warning restore 67

        public virtual double AveragePacketLossRate
        {
            get { throw new NotImplementedException(); }
        }

        public virtual double InboundBytesPerSecond
        {
            get { throw new NotImplementedException(); }
        }

        public virtual bool IsTunnelled
        {
            get { throw new NotImplementedException(); }
        }

        public virtual InitializationState InitializationProgress
        {
            get { throw new NotImplementedException(); }
        }

        public virtual bool IsLoggedIn
        {
            get { throw new NotImplementedException(); }
        }

        public virtual bool IsFullyConnected
        {
            get { throw new NotImplementedException(); }
        }

        public virtual bool IsOffline
        {
            get { throw new NotImplementedException(); }
        }

        public virtual bool IsOnline
        {
            get { throw new NotImplementedException(); }
        }

        public virtual double MaximumPacketLossRate
        {
            get { throw new NotImplementedException(); }
        }

        public virtual double MaximumSendLimitBytesPerSecond
        {
            get { throw new NotImplementedException(); }
        }

        public virtual double OutboundBytesPerSecond
        {
            get { throw new NotImplementedException(); }
        }

        public virtual TypeRegistry TypeRegistry
        {
            get { throw new NotImplementedException(); }
        }

        public virtual RPCManager RPCManager
        {
            get { throw new NotImplementedException(); }
        }

        public virtual Badumna.Validation.IFacade ValidationFacade
        {
            get { throw new NotImplementedException(); }
        }

        public virtual Badumna.Match.Facade Match
        {
            get { throw new NotImplementedException(); }
        }

        public virtual Badumna.Streaming.StreamingManager Streaming
        {
            get { throw new NotImplementedException(); }
        }

        public virtual double TotalSendLimitBytesPerSecond
        {
            get { throw new NotImplementedException(); }
        }

        public virtual Badumna.Chat.IChatSession ChatSession
        {
            get { throw new NotImplementedException(); }
        }

        public virtual Badumna.Security.Character Character
        {
            get { throw new NotImplementedException(); }
        }

        public virtual void AnnounceService(Badumna.ServiceDiscovery.ServerType type)
        {
            throw new NotImplementedException();
        }

        public virtual void FlagForUpdate(ISpatialOriginal localEntity, int changedPartIndex)
        {
            throw new NotImplementedException();
        }

        public virtual void FlagForUpdate(ISpatialOriginal localEntity, BooleanArray changedParts)
        {
            throw new NotImplementedException();
        }

        public virtual Badumna.Arbitration.IArbitrator GetArbitrator(string name)
        {
            throw new NotImplementedException();
        }

        public virtual Vector3 GetDestination(IDeadReckonable deadReckonable)
        {
            throw new NotImplementedException();
        }

        public virtual NetworkStatus GetNetworkStatus()
        {
            throw new NotImplementedException();
        }

        public virtual long GetUserIdForSession(int sessionId)
        {
            throw new NotImplementedException();
        }

        public virtual Badumna.Security.Character GetCharacterForArbitrationSession(int sessionId)
        {
            throw new NotImplementedException();
        }

        public virtual BadumnaId GetBadumnaIdForArbitrationSession(int sessionId)
        {
            throw new NotImplementedException();
        }

        public virtual Badumna.Matchmaking.MatchmakingRequest GetMatchmakingRequest()
        {
            throw new NotImplementedException();
        }

        public virtual NetworkScene JoinScene(string sceneName, CreateSpatialReplica createEntityDelegate, RemoveSpatialReplica removeEntityDelegate)
        {
            throw new NotImplementedException();
        }

        public virtual NetworkScene JoinMiniScene(string sceneName, CreateSpatialReplica createEntityDelegate, RemoveSpatialReplica removeEntityDelegate)
        {
            throw new NotImplementedException();
        }

        public virtual bool Login()
        {
            throw new NotImplementedException();
        }

        public virtual bool Login(string characterName)
        {
            throw new NotImplementedException();
        }

        public virtual bool Login(string characterName, string keyPairXml)
        {
            throw new NotImplementedException();
        }

        public virtual bool Login(Badumna.Security.IIdentityProvider identityProvider)
        {
            throw new NotImplementedException();
        }

        public virtual void ProcessNetworkState()
        {
            throw new NotImplementedException();
        }

        public virtual void RegisterArbitrationHandler(Badumna.Arbitration.HandleClientMessage handler, TimeSpan disconnectTimeout, Badumna.Arbitration.HandleClientDisconnect disconnect)
        {
            throw new NotImplementedException();
        }

        public virtual void RegisterEntityDetails(float areaOfInterestRadius, float maxEntitySpeed)
        {
            throw new NotImplementedException();
        }

        public virtual void SendCustomMessageToOriginal(ISpatialReplica remoteEntity, MemoryStream eventData)
        {
            throw new NotImplementedException();
        }

        public virtual void SendCustomMessageToRemoteCopies(ISpatialOriginal localEntity, MemoryStream eventData)
        {
            throw new NotImplementedException();
        }

        public virtual void SendServerArbitrationEvent(int destinationSessionId, byte[] message)
        {
            throw new NotImplementedException();
        }

        public virtual void Shutdown()
        {
            throw new NotImplementedException();
        }

        public virtual void Shutdown(bool blockUntilComplete)
        {
            throw new NotImplementedException();
        }

        public virtual void SnapToDestination(IDeadReckonable deadReckonable)
        {
            throw new NotImplementedException();
        }

        public virtual void StartController<T>(string controllerUniqueName) where T : Badumna.Controllers.DistributedController
        {
            throw new NotImplementedException();
        }

        public virtual string StartController<T>(string sceneName, string controllerName, ushort max) where T : Badumna.Controllers.DistributedController
        {
            throw new NotImplementedException();
        }

        public virtual void StopController<T>(string controllerUniqueName) where T : Badumna.Controllers.DistributedController
        {
            throw new NotImplementedException();
        }

        public virtual StatisticsTracker CreateTracker(string serverAddress, int serverPort, TimeSpan interval, string initialPayload)
        {
            throw new NotImplementedException();
        }

        public virtual StatisticsTracker CreateAndStartTracker(string serverAddress, int serverPort, TimeSpan interval, string initialPayload)
        {
            throw new NotImplementedException();
        }

        public MatchmakingAsyncResult BeginMatchmaking(
            HandleMatchmakingResult onCompletion,
            HandleMatchmakingProgress onProgress,
            MatchmakingOptions options)
        {
            throw new NotImplementedException();
        }

        public MatchmakingAsyncResult BeginMatchmaking(
            HandleMatchmakingResult onCompletion,
            MatchmakingOptions options)
        {
            throw new NotImplementedException();
        }

        public MatchmakingAsyncResult BeginMatchmaking(
            HandleMatchmakingProgress onProgress,
            MatchmakingOptions options)
        {
            throw new NotImplementedException();
        }

        public MatchmakingAsyncResult BeginMatchmaking(
            MatchmakingOptions options)
        {
            throw new NotImplementedException();
        }
    }
}
