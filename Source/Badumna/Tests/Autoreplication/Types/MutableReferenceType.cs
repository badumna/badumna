﻿// //------------------------------------------------------------------------------
// // <copyright file="MutableReferenceType.cs" company="National ICT Australia Limited">
// //     Copyright (c) 2012-2012 National ICT Australia Limited. All rights reserved.
// // </copyright>
// //------------------------------------------------------------------------------
namespace BadumnaTests.Autoreplication
{
    using Badumna.Autoreplication;

    public class MutableReferenceType
    {
        public MutableReferenceType()
        {
        }

        public int Number { get; set; }

        public string Word { get; set; }

        public bool Equals(MutableReferenceType obj)
        {
            if (object.ReferenceEquals(obj, null))
            {
                return false;
            }

            if (object.ReferenceEquals(this, obj))
            {
                return true;
            }

            return (this.Number == obj.Number) && (this.Word == obj.Word);
        }

        public override bool Equals(object obj)
        {
            return this.Equals(obj as MutableReferenceType);
        }

        public override int GetHashCode()
        {
            return this.Number.GetHashCode() ^ this.Word.GetHashCode();
        }

        ////public static bool operator ==(MutableReferenceType a, MutableReferenceType b)
        ////{
        ////    // If both are null, or both are same instance, return true.
        ////    if (System.Object.ReferenceEquals(a, b))
        ////    {
        ////        return true;
        ////    }

        ////    // If one is null, but not both, return false.
        ////    if (((object)a == null) || ((object)b == null))
        ////    {
        ////        return false;
        ////    }

        ////    // Return true if the properties match:
        ////    return a.Number == b.Number && a.Word == b.Word;
        ////}

        ////public static bool operator !=(MutableReferenceType a, MutableReferenceType b)
        ////{
        ////    return !(a == b);
        ////}

        public void Serialize(System.IO.BinaryWriter writer)
        {
            writer.Write(this.Number);
            writer.Write(this.Word != null);
            if (this.Word != null)
            {
                writer.Write(this.Word);
            }
        }

        public MutableReferenceType Deserialize(System.IO.BinaryReader reader)
        {
            this.Number = reader.ReadInt32();
            if (reader.ReadBoolean())
            {
                this.Word = reader.ReadString();
            }

            return this;
        }
    }
}