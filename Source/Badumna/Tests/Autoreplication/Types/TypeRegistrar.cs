﻿// ------------------------------------------------------------------------------
//  <copyright file="TypeRegistrar.cs" company="National ICT Australia Limited">
//      Copyright (c) 2012-2012 National ICT Australia Limited. All rights reserved.
//  </copyright>
// ------------------------------------------------------------------------------
namespace BadumnaTests.Autoreplication
{
    internal static class TypeRegistrar
    {
         public static void RegisterTestTypes(Badumna.Autoreplication.Serialization.Manager serialization)
         {
            serialization.RegisterMutableReferenceType<MutableReferenceType>(
                (o, w) => o.Serialize(w),
                r => new MutableReferenceType().Deserialize(r),
                o => new MutableReferenceType { Number = o.Number, Word = o.Word });
            serialization.RegisterValueType<MutableValueType>(
                (o, w) =>
                {
                    w.Write(o.Number);
                    w.Write(o.Word != null);
                    if (o.Word != null)
                    {
                        w.Write(o.Word);
                    }
                },
                r =>
                {
                    int i = r.ReadInt32();
                    string s = null;
                    if (r.ReadBoolean())
                    {
                        s = r.ReadString();
                    }

                    return new MutableValueType { Number = i, Word = s };
                });
            serialization.RegisterValueType<ImmutableValueType>(
                (o, w) =>
                {
                    w.Write(o.Number);
                    w.Write(o.Word != null);
                    if (o.Word != null)
                    {
                        w.Write(o.Word);
                    }
                },
                r =>
                {
                    int i = r.ReadInt32();
                    string s = null;
                    if (r.ReadBoolean())
                    {
                        s = r.ReadString();
                    }

                    return new ImmutableValueType(i, s);
                });
            serialization.RegisterImmutableReferenceType<ImmutableReferenceType>(
                (o, w) =>
                {
                    w.Write(o.Number);
                    w.Write(o.Word != null);
                    if (o.Word != null)
                    {
                        w.Write(o.Word);
                    }
                },
                r =>
                {
                    int i = r.ReadInt32();
                    string s = null;
                    if (r.ReadBoolean())
                    {
                        s = r.ReadString();
                    }

                    return new ImmutableReferenceType(i, s);
                });             
         }
    }
}