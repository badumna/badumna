﻿// //------------------------------------------------------------------------------
// // <copyright file="MutableValueType.cs" company="National ICT Australia Limited">
// //     Copyright (c) 2012-2012 National ICT Australia Limited. All rights reserved.
// // </copyright>
// //------------------------------------------------------------------------------
namespace BadumnaTests.Autoreplication
{
    using Badumna.Autoreplication;

    public struct MutableValueType
    {
        public int Number { get; set; }

        public string Word { get; set; }
    }
}