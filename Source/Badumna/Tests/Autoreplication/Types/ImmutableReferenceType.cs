﻿// //------------------------------------------------------------------------------
// // <copyright file="ImmutableReferenceType.cs" company="National ICT Australia Limited">
// //     Copyright (c) 2012-2012 National ICT Australia Limited. All rights reserved.
// // </copyright>
// //------------------------------------------------------------------------------
namespace BadumnaTests.Autoreplication
{
    using Badumna.Autoreplication;

    public class ImmutableReferenceType
    {
        private readonly int number;

        private readonly string word;

        public ImmutableReferenceType(int number, string word)
        {
            this.number = number;
            this.word = word;
        }

        public int Number
        {
            get
            {
                return this.number;
            }
        }

        public string Word
        {
            get
            {
                return this.word;
            }
        }

        public bool Equals(ImmutableReferenceType obj)
        {
            if (object.ReferenceEquals(obj, null))
            {
                return false;
            }

            if (object.ReferenceEquals(this, obj))
            {
                return true;
            }

            return (this.Number == obj.Number) && (this.Word == obj.Word);
        }

        public override bool Equals(object obj)
        {
            return this.Equals(obj as ImmutableReferenceType);
        }

        public override int GetHashCode()
        {
            return this.Number.GetHashCode() ^ this.Word.GetHashCode();
        }
    }
}