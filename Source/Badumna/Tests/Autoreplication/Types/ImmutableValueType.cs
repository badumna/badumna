﻿// //------------------------------------------------------------------------------
// // <copyright file="ImmutableValueType.cs" company="National ICT Australia Limited">
// //     Copyright (c) 2012-2012 National ICT Australia Limited. All rights reserved.
// // </copyright>
// //------------------------------------------------------------------------------
namespace BadumnaTests.Autoreplication
{
    public struct ImmutableValueType
    {
        private readonly int number;

        private readonly string word;

        public ImmutableValueType(int number, string word)
        {
            this.number = number;
            this.word = word;
        }

        public int Number
        {
            get
            {
                return this.number;
            }
        }

        public string Word
        {
            get
            {
                return this.word;
            }
        }
    }
}