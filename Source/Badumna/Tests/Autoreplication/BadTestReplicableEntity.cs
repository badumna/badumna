﻿// -----------------------------------------------------------------------
// <copyright file="BadTestReplicableEntity.cs" company="National ICT Australia Limited">
//     Copyright (c) 2012 National ICT Australia Limited. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace BadumnaTests.Autoreplication
{
    using System.IO;
    using Badumna;
    using Badumna.DataTypes;
    using Badumna.SpatialEntities;

    /// <summary>
    /// Replicable entity with unsupported property for testing.
    /// </summary>
    public class BadTestReplicableEntity : ISpatialEntity
    {
        public float AreaOfInterestRadius { get; set; }

        public BadumnaId Guid { get; set; }

        public Vector3 Position { get; set; }

        public float Radius { get; set; }

        [Replicable]
        public Foo BadProperty { get; set; }

        public void HandleEvent(Stream stream)
        {
            // Not implemented.
        }

        public class Foo
        {
        }
    }
}