﻿//-----------------------------------------------------------------------
// <copyright file="RPCIntegrationTests.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BadumnaTests.Autoreplication
{
    using System.Collections.Generic;
    using System.IO;
    using Badumna;
    using Badumna.Autoreplication;
    using Badumna.Core;
    using Badumna.SpatialEntities;
    using NUnit.Framework;
    using Rhino.Mocks;
    using TestUtilities;
    using Manager = Badumna.Autoreplication.Serialization.Manager;
    using SpatialAutoreplicationManager = Badumna.Autoreplication.SpatialAutoreplicationManager;

    [TestFixture]
    [DontPerformCoverage]
    public class RPCIntegrationTests
    {
        private ISpatialEntityManager spatialEntityManager;

        private SpatialAutoreplicationManager autoreplication;

        private Manager serialization;

        private RPCManager rpcManager;

        private PositionalReplicableEntity originalEntity;

        private PositionalReplicableEntity replicaEntity;

        private IPositionalOriginalWrapper originalWrapper;

        private IPositionalReplicaWrapper replicaWrapper;

        private ITime timeKeeper;

        private CapturingConstraint<ISpatialReplica> replicaSendingRPC = new CapturingConstraint<ISpatialReplica>();

        private CapturingConstraint<MemoryStream> streamToOriginal = new CapturingConstraint<MemoryStream>();

        private CapturingConstraint<ISpatialOriginal> originalSendingRPC = new CapturingConstraint<ISpatialOriginal>();

        private CapturingConstraint<MemoryStream> streamToReplica = new CapturingConstraint<MemoryStream>();

        [SetUp]
        public void SetUp()
        {
            this.spatialEntityManager = MockRepository.GenerateMock<ISpatialEntityManager>();
            this.timeKeeper = new FakeTimeKeeper();
            this.serialization = new Manager(this.timeKeeper);
            this.autoreplication = new SpatialAutoreplicationManager(
                this.serialization,
                new FakeTimeKeeper(),
                (e, d) => new PositionalOriginalWrapper(e, d.Key, d.Value, this.spatialEntityManager, this.serialization),
                e => new PositionalReplicaWrapper(e, this.serialization, this.timeKeeper));
            this.autoreplication.Initialize(this.spatialEntityManager);
            this.rpcManager = new RPCManager(this.serialization);
            TypeRegistrar.RegisterTestTypes(this.serialization);

            this.originalEntity = new PositionalReplicableEntity() { String = "original" };
            ////this.replicaWrapper = new ReplicaWrapper(this.replicaEntity, this.serialization, new FakeTimeKeeper());
            this.replicaEntity = new PositionalReplicableEntity() { String = "replica" };
            this.originalWrapper = this.autoreplication.RegisterOriginal(this.originalEntity, new KeyValuePair<float, float>(10f, 100f)) as IPositionalOriginalWrapper;
            this.replicaWrapper = this.autoreplication.RegisterReplica(this.replicaEntity);
            ////this.originalWrapper = new OriginalWrapper(this.originalEntity, this.network, this.serialization);

            this.replicaSendingRPC = new CapturingConstraint<ISpatialReplica>();
            this.streamToOriginal = new CapturingConstraint<MemoryStream>();
            this.originalSendingRPC = new CapturingConstraint<ISpatialOriginal>();
            this.streamToReplica = new CapturingConstraint<MemoryStream>();
            this.spatialEntityManager.Stub(m => m.SendCustomMessageToOriginal(
                Arg<ISpatialReplica>.Matches(this.replicaSendingRPC), Arg<MemoryStream>.Matches(this.streamToOriginal)));
            this.spatialEntityManager.Stub(m => m.SendCustomMessageToReplicas(
                Arg<ISpatialOriginal>.Matches(this.originalSendingRPC), Arg<MemoryStream>.Matches(this.streamToReplica)));
        }

        [Test]
        public void RPCToOriginalTest()
        {
            int p1 = 96;
            int p2 = 88888;
            this.autoreplication.CallMethodOnOriginal(this.replicaEntity.RPC, p1, p2);
            Assert.AreEqual(this.replicaWrapper, this.replicaSendingRPC.Argument);
            MemoryStream stream = new MemoryStream(this.streamToOriginal.Argument.GetBuffer());
            this.originalWrapper.HandleEvent(stream);
            Assert.AreEqual(p1, this.originalEntity.RPCParam1);
            Assert.AreEqual(p2, this.originalEntity.RPCParam2);
        }

        [Test]
        public void RPCToReplicaTest()
        {
            int p1 = 987;
            int p2 = -601;
            this.autoreplication.CallMethodOnReplicas(this.originalEntity.RPC, p1, p2);
            Assert.AreEqual(this.originalWrapper, this.originalSendingRPC.Argument);
            MemoryStream stream = new MemoryStream(this.streamToReplica.Argument.GetBuffer());
            this.replicaWrapper.HandleEvent(stream);
            Assert.AreEqual(p1, this.replicaEntity.RPCParam1);
            Assert.AreEqual(p2, this.replicaEntity.RPCParam2);
        }

        ////private class StubNetworkFacade : FakeNetworkFacade
        ////{
        ////    public ISpatialEntity ReplicaSendingRPC { get; set; }

        ////    public MemoryStream CustomMessageToOriginal { get; set; }
            
        ////    public ISpatialEntity OriginalSendingRPC { get; set; }
            
        ////    public MemoryStream CustomMessageToReplica { get; set; }

        ////    public override void SendCustomMessageToOriginal(ISpatialReplica remoteEntity, MemoryStream eventData)
        ////    {
        ////        this.ReplicaSendingRPC = remoteEntity;
        ////        this.CustomMessageToOriginal = eventData;
        ////    }

        ////    public override void SendCustomMessageToRemoteCopies(ISpatialOriginal localEntity, MemoryStream eventData)
        ////    {
        ////        this.OriginalSendingRPC = localEntity;
        ////        this.CustomMessageToReplica = eventData;
        ////    }
        ////}
    }
}
