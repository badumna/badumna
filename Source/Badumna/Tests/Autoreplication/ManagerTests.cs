﻿//-----------------------------------------------------------------------
// <copyright file="ManagerTests.cs" company="National ICT Australia Limited">
//     Copyright (c) 2012 National ICT Australia Limited. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

[assembly: System.Runtime.CompilerServices.InternalsVisibleTo(Rhino.Mocks.RhinoMocks.StrongName)]

namespace BadumnaTests.Autoreplication
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;
    using Badumna.Autoreplication;
    using Badumna.Core;
    using Badumna.SpatialEntities;
    using Badumna.Utilities;
    using NUnit.Framework;
    using Rhino.Mocks;
    using Manager = Badumna.Autoreplication.Serialization.Manager;

    [TestFixture]
    [DontPerformCoverage]
    public class ManagerTests
    {
        private float entityRadius = 10f;
        private float entityAOIRadius = 500f;
        private ISpatialEntityManager spatialEntityManager;
        private Manager serialization;
        private ITime clock;
        private IReplicableEntity original;
        private IPositionalOriginalWrapper originalWrapper;
        private IPositionalReplicaWrapper replicaWrapper;
        private OriginalWrapperFactory<IReplicableEntity, IPositionalOriginalWrapper, KeyValuePair<float, float>> fakeOriginalWrapperFactory;
        private ReplicaWrapperFactory<IReplicableEntity, IPositionalReplicaWrapper> fakeReplicaWrapperFactory;
        private Badumna.Autoreplication.Manager<IReplicableEntity, ISpatialOriginal, ISpatialReplica, IPositionalOriginalWrapper, IPositionalReplicaWrapper, KeyValuePair<float, float>, ISpatialEntityManager> manager;
        private Badumna.Autoreplication.Manager<IReplicableEntity, ISpatialOriginal, ISpatialReplica, IPositionalOriginalWrapper, IPositionalReplicaWrapper, KeyValuePair<float, float>, ISpatialEntityManager> managerWithFakeFactories;

        [SetUp]
        public void SetUp()
        {
            this.spatialEntityManager = MockRepository.GenerateMock<ISpatialEntityManager>();
            this.clock = MockRepository.GenerateMock<ITime>();
            this.serialization = MockRepository.GenerateMock<Manager>(this.clock);
            this.original = MockRepository.GenerateMock<IReplicableEntity>();
            this.originalWrapper = MockRepository.GenerateMock<IPositionalOriginalWrapper>();
            this.replicaWrapper = MockRepository.GenerateMock<IPositionalReplicaWrapper>();
            this.fakeOriginalWrapperFactory = (o, d) => this.originalWrapper;
            this.fakeReplicaWrapperFactory = r => this.replicaWrapper;
            this.manager = new Badumna.Autoreplication.Manager<IReplicableEntity, ISpatialOriginal, ISpatialReplica, IPositionalOriginalWrapper, IPositionalReplicaWrapper, KeyValuePair<float, float>, ISpatialEntityManager>(
                this.serialization,
                this.clock,
                (e, d) => new PositionalOriginalWrapper(e, d.Key, d.Value, this.spatialEntityManager, this.serialization),
                e => new PositionalReplicaWrapper(e, this.serialization, this.clock));
            this.manager.Initialize(this.spatialEntityManager);
            this.managerWithFakeFactories = new Badumna.Autoreplication.Manager<IReplicableEntity, ISpatialOriginal, ISpatialReplica, IPositionalOriginalWrapper, IPositionalReplicaWrapper, KeyValuePair<float, float>, ISpatialEntityManager>(
                this.serialization,
                this.clock,
                this.fakeOriginalWrapperFactory,
                this.fakeReplicaWrapperFactory);
            this.managerWithFakeFactories.Initialize(this.spatialEntityManager);
        }

        [TearDown]
        public void TearDown()
        {
        }

        [Test]
        public void RegisterOriginalThrowsForNullOriginal()
        {
            Assert.Throws<ArgumentNullException>(() => this.manager.RegisterOriginal(null, new KeyValuePair<float, float>(this.entityRadius, this.entityAOIRadius)));
        }

        [Test]
        public void RegisterOriginalThrowsForExistingOriginal()
        {
            this.manager.RegisterOriginal(this.original, new KeyValuePair<float, float>(this.entityRadius, this.entityAOIRadius));
            Assert.Throws<InvalidOperationException>(
                () => this.manager.RegisterOriginal(this.original, new KeyValuePair<float, float>(this.entityRadius, this.entityAOIRadius)));
        }

        [Test]
        public void UnregisterOriginalThrowsForUnknownOriginal()
        {
            Assert.Throws<InvalidOperationException>(() => this.manager.UnregisterOriginal(this.original));
        }

        [Test]
        public void TickTicksWrappersForRegisteredOriginals()
        {
            var now = TimeSpan.FromDays(27);
            this.managerWithFakeFactories.RegisterOriginal(this.original, new KeyValuePair<float, float>(this.entityRadius, this.entityAOIRadius));
            this.originalWrapper.Expect(w => w.Tick(now));
            this.managerWithFakeFactories.Tick(now);
            this.originalWrapper.VerifyAllExpectations();
        }

        [Test]
        public void TickDoesNotTickWrappersForUnregisteredOriginals()
        {
            var now = TimeSpan.FromDays(27);
            this.managerWithFakeFactories.RegisterOriginal(this.original, new KeyValuePair<float, float>(this.entityRadius, this.entityAOIRadius));
            this.managerWithFakeFactories.UnregisterOriginal(this.original);
            this.originalWrapper.Expect(w => w.Tick(now)).IgnoreArguments().Repeat.Never();
            this.managerWithFakeFactories.Tick(now);
            this.originalWrapper.VerifyAllExpectations();
        }

        [Test]
        public void DelegateCompatibilityTest()
        {
            Foo foo = new Foo();
            PropertyInfo property = typeof(Foo).GetProperty("Bar");
            GenericCallBackReturn<int> callback = (GenericCallBackReturn<int>)
            Delegate.CreateDelegate(typeof(GenericCallBackReturn<int>), foo, property.GetGetMethod(true));
            GenericCallBackReturn<object> callback2 = new GenericCallBackReturn<object>(
                () => ((GenericCallBackReturn<int>)
            Delegate.CreateDelegate(typeof(GenericCallBackReturn<int>), foo, property.GetGetMethod(true))).Invoke());

            Func<int> f1 = () => 7;
            Func<int> f2 = new Func<int>(f1);
            ////Func<object> f3 = new Func<object>(f1);
            Func<object> f4 = () => f1.Invoke();

            Func<Foo> g1 = () => new Foo();
            Func<object> g2 = new Func<object>(g1);
        }

        private class Foo
        {
            public int Bar { get; set; }
        }
    }
}
