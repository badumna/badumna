﻿//-----------------------------------------------------------------------
// <copyright file="FakeTimeKeeper.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BadumnaTests.Autoreplication
{
    using System;
    using Badumna.Core;

    internal class FakeTimeKeeper : ITime
    {
        public TimeSpan Now { get; set; }
    }
}
