﻿//-----------------------------------------------------------------------
// <copyright file="EstimatedDerivativeSmootherTests.cs" company="National ICT Australia Limited">
//     Copyright (c) 2012 National ICT Australia Limited. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BadumnaTests.Autoreplication
{
    using System;
    using Badumna.Autoreplication;
    using Badumna.Core;
    using Badumna.DataTypes;

    using NUnit.Framework;

    [TestFixture]
    [DontPerformCoverage]
    public class EstimatedDerivativeSmootherTests
    {
        [SetUp]
        public void SetUp()
        {
        }

        [Test]
        public void ConstructorThrowsForNullTimeKeeper()
        {
            Assert.Throws<ArgumentNullException>(
                () => new EstimatedDerivativeSmoother<Vector3>(
                    Vector3.Zero,
                    100,
                    0,
                    null,
                    new Vector3Arithmetic()));
        }

        [Test]
        public void ConstructorThrowsForNullArithmetic()
        {
            Assert.Throws<ArgumentNullException>(
                () => new EstimatedDerivativeSmoother<Vector3>(
                    Vector3.Zero,
                    100,
                    0,
                    new FakeTimeKeeper(),
                    null));
        }

        [Test]
        public void ConstructorThrowsForZeroInterpolation()
        {
            Assert.Throws<ArgumentOutOfRangeException>(
                () => new EstimatedDerivativeSmoother<Vector3>(
                    Vector3.Zero,
                    0,
                    0,
                    new FakeTimeKeeper(),
                    new Vector3Arithmetic()));
        }

        [Test]
        public void ConstructorThrowsFornegativeExtrapolation()
        {
            Assert.Throws<ArgumentOutOfRangeException>(
                () => new EstimatedDerivativeSmoother<Vector3>(
                    Vector3.Zero,
                    100,
                    -1,
                    new FakeTimeKeeper(),
                    new Vector3Arithmetic()));
        }

        [Test]
        public void InterpolationTest()
        {
            var clock = new FakeTimeKeeper();
            clock.Now = TimeSpan.FromMilliseconds(100);
            var smoother = new EstimatedDerivativeSmoother<Vector3>(Vector3.Zero, 200, 200, clock, new Vector3Arithmetic());
            clock.Now = TimeSpan.FromMilliseconds(200);
            smoother.Update(Vector3.UnitX);
            Assert.AreEqual(Vector3.Zero, smoother.GetCurrentValue(clock.Now));
            clock.Now = TimeSpan.FromMilliseconds(300);
            Assert.AreEqual(Vector3.Zero, smoother.GetCurrentValue(clock.Now));
            clock.Now = TimeSpan.FromMilliseconds(350);
            Assert.AreEqual(Vector3.UnitX * 0.5f, smoother.GetCurrentValue(clock.Now));
            clock.Now = TimeSpan.FromMilliseconds(400);
            Assert.AreEqual(Vector3.UnitX, smoother.GetCurrentValue(clock.Now));
        }

        [Test]
        public void ExtrapolationTest()
        {
            var clock = new FakeTimeKeeper();
            clock.Now = TimeSpan.FromMilliseconds(100);
            var smoother = new EstimatedDerivativeSmoother<Vector3>(Vector3.Zero, 200, 200, clock, new Vector3Arithmetic());
            clock.Now = TimeSpan.FromMilliseconds(200);
            smoother.Update(Vector3.UnitX);
            Assert.AreEqual(Vector3.Zero, smoother.GetCurrentValue(clock.Now));
            clock.Now = TimeSpan.FromMilliseconds(300);
            Assert.AreEqual(Vector3.Zero, smoother.GetCurrentValue(clock.Now));
            clock.Now = TimeSpan.FromMilliseconds(350);
            Assert.AreEqual(Vector3.UnitX * 0.5f, smoother.GetCurrentValue(clock.Now));
            clock.Now = TimeSpan.FromMilliseconds(400);
            Assert.AreEqual(Vector3.UnitX, smoother.GetCurrentValue(clock.Now));
            clock.Now = TimeSpan.FromMilliseconds(500);
            Assert.AreEqual(Vector3.UnitX * 2, smoother.GetCurrentValue(clock.Now));
            clock.Now = TimeSpan.FromMilliseconds(600);
            Assert.AreEqual(Vector3.UnitX * 3, smoother.GetCurrentValue(clock.Now));
            clock.Now = TimeSpan.FromMilliseconds(700);
            Assert.AreEqual(Vector3.UnitX * 3, smoother.GetCurrentValue(clock.Now));
        }

        [Test]
        public void ResumptionAfterExtrapolationTest()
        {
            var clock = new FakeTimeKeeper();
            clock.Now = TimeSpan.FromMilliseconds(100);
            var smoother = new EstimatedDerivativeSmoother<Vector3>(Vector3.Zero, 200, 200, clock, new Vector3Arithmetic());
            clock.Now = TimeSpan.FromMilliseconds(200);
            smoother.Update(Vector3.UnitX);
            Assert.AreEqual(Vector3.Zero, smoother.GetCurrentValue(clock.Now));
            clock.Now = TimeSpan.FromMilliseconds(300);
            Assert.AreEqual(Vector3.Zero, smoother.GetCurrentValue(clock.Now));
            clock.Now = TimeSpan.FromMilliseconds(350);
            Assert.AreEqual(Vector3.UnitX * 0.5f, smoother.GetCurrentValue(clock.Now));
            clock.Now = TimeSpan.FromMilliseconds(400);
            Assert.AreEqual(Vector3.UnitX, smoother.GetCurrentValue(clock.Now));
            clock.Now = TimeSpan.FromMilliseconds(500);
            Assert.AreEqual(Vector3.UnitX * 2, smoother.GetCurrentValue(clock.Now));
            clock.Now = TimeSpan.FromMilliseconds(600);
            Assert.AreEqual(Vector3.UnitX * 3, smoother.GetCurrentValue(clock.Now));
            clock.Now = TimeSpan.FromMilliseconds(700);
            smoother.Update(Vector3.UnitX * 6);
            Assert.AreEqual(Vector3.UnitX * 3, smoother.GetCurrentValue(clock.Now));
            clock.Now = TimeSpan.FromMilliseconds(800);
            Assert.AreEqual(Vector3.UnitX * 4.5f, smoother.GetCurrentValue(clock.Now));
            clock.Now = TimeSpan.FromMilliseconds(900);
            Assert.AreEqual(Vector3.UnitX * 6f, smoother.GetCurrentValue(clock.Now));
            clock.Now = TimeSpan.FromMilliseconds(1000);
            Assert.AreEqual(Vector3.UnitX * 7f, smoother.GetCurrentValue(clock.Now));
            clock.Now = TimeSpan.FromMilliseconds(1100);
            Assert.AreEqual(Vector3.UnitX * 8f, smoother.GetCurrentValue(clock.Now));
            clock.Now = TimeSpan.FromMilliseconds(1200);
            Assert.AreEqual(Vector3.UnitX * 8f, smoother.GetCurrentValue(clock.Now));
        }

        [Test]
        public void ResumptionDuringExtrapolationTest()
        {
            var clock = new FakeTimeKeeper();
            clock.Now = TimeSpan.FromMilliseconds(100);
            var smoother = new EstimatedDerivativeSmoother<Vector3>(Vector3.Zero, 200, 200, clock, new Vector3Arithmetic());
            clock.Now = TimeSpan.FromMilliseconds(200);
            smoother.Update(Vector3.UnitX);
            Assert.AreEqual(Vector3.Zero, smoother.GetCurrentValue(clock.Now));
            clock.Now = TimeSpan.FromMilliseconds(300);
            Assert.AreEqual(Vector3.Zero, smoother.GetCurrentValue(clock.Now));
            clock.Now = TimeSpan.FromMilliseconds(350);
            Assert.AreEqual(Vector3.UnitX * 0.5f, smoother.GetCurrentValue(clock.Now));
            clock.Now = TimeSpan.FromMilliseconds(400);
            Assert.AreEqual(Vector3.UnitX, smoother.GetCurrentValue(clock.Now));
            clock.Now = TimeSpan.FromMilliseconds(500);
            smoother.Update(Vector3.UnitX * 8);
            Assert.AreEqual(Vector3.UnitX * 2, smoother.GetCurrentValue(clock.Now));
            clock.Now = TimeSpan.FromMilliseconds(600);
            Assert.AreEqual(Vector3.UnitX * 5, smoother.GetCurrentValue(clock.Now));
            clock.Now = TimeSpan.FromMilliseconds(700);
            Assert.AreEqual(Vector3.UnitX * 8, smoother.GetCurrentValue(clock.Now));
            clock.Now = TimeSpan.FromMilliseconds(800);
            AreCloseEnough(Vector3.UnitX * (31f / 3), smoother.GetCurrentValue(clock.Now));
            clock.Now = TimeSpan.FromMilliseconds(900);
            AreCloseEnough(Vector3.UnitX * (38f / 3), smoother.GetCurrentValue(clock.Now));
            clock.Now = TimeSpan.FromMilliseconds(1000);
            AreCloseEnough(Vector3.UnitX * (38f / 3), smoother.GetCurrentValue(clock.Now));
        }

        [Test]
        public void ChangedReturnsTrueDuringInterpolation()
        {
            var clock = new FakeTimeKeeper();
            clock.Now = TimeSpan.Zero;
            var smoother = new EstimatedDerivativeSmoother<Vector3>(Vector3.Zero, 200, 200, clock, new Vector3Arithmetic());
            clock.Now = TimeSpan.FromMilliseconds(100);
            var val = smoother.GetCurrentValue(clock.Now);
            Assert.IsTrue(smoother.Changed);
        }

        [Test]
        public void ChangedReturnsTrueDuringExtrapolation()
        {
            var clock = new FakeTimeKeeper();
            clock.Now = TimeSpan.Zero;
            var smoother = new EstimatedDerivativeSmoother<Vector3>(Vector3.Zero, 200, 200, clock, new Vector3Arithmetic());
            clock.Now = TimeSpan.FromMilliseconds(300);
            var val = smoother.GetCurrentValue(clock.Now);
            Assert.IsTrue(smoother.Changed);
        }

        [Test]
        public void ChangedReturnsTrueAfterExtrapolationIfFinalValueNotYetFetched()
        {
            var clock = new FakeTimeKeeper();
            clock.Now = TimeSpan.Zero;
            var smoother = new EstimatedDerivativeSmoother<Vector3>(Vector3.Zero, 200, 200, clock, new Vector3Arithmetic());
            clock.Now = TimeSpan.FromMilliseconds(401);
            Assert.IsTrue(smoother.Changed);
        }

        [Test]
        public void ChangedReturnsFalseAfterExtrapolationIfFinalValueFetched()
        {
            var clock = new FakeTimeKeeper();
            clock.Now = TimeSpan.Zero;
            var smoother = new EstimatedDerivativeSmoother<Vector3>(Vector3.Zero, 200, 200, clock, new Vector3Arithmetic());
            clock.Now = TimeSpan.FromMilliseconds(401);
            smoother.GetCurrentValue(clock.Now);
            Assert.IsFalse(smoother.Changed);
        }

        [Test]
        public void ChangedReturnsTrueAfterUpdateWhenFinishedExtrapolating()
        {
            // Arrange
            var clock = new FakeTimeKeeper();
            clock.Now = TimeSpan.Zero;
            var smoother = new EstimatedDerivativeSmoother<Vector3>(Vector3.Zero, 200, 200, clock, new Vector3Arithmetic());
            clock.Now = TimeSpan.FromMilliseconds(500);
            smoother.GetCurrentValue(clock.Now); 

            // Act
            smoother.Update(Vector3.One);

            // Assert
            Assert.IsTrue(smoother.Changed);
        }

        private static void AreCloseEnough(Vector3 a, Vector3 b)
        {
            Assert.Less((a - b).Magnitude, 0.0001f);
        }

        private class FakeTimeKeeper : ITime
        {
            public TimeSpan Now { get; set; }
        }
    }
}
