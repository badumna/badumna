﻿//-----------------------------------------------------------------------
// <copyright file="EntityWrapperIntegrationTests.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BadumnaTests.Autoreplication
{
    using System;
    using System.IO;
    using Badumna.Autoreplication;
    using Badumna.Core;
    using Badumna.DataTypes;
    using Badumna.Replication;
    using Badumna.SpatialEntities;
    using Badumna.Utilities;
    using NUnit.Framework;
    using Rhino.Mocks;
    using Manager = Badumna.Autoreplication.Serialization.Manager;

    [TestFixture]
    [DontPerformCoverage]
    public class EntityWrapperIntegrationTests
    {
        private IReplicationService<IOriginal, IReplica> entityManager;

        private Manager serialization;

        private ReplicableEntity originalEntity;

        private ReplicableEntity replicaEntity;

        private OriginalEntityWrapper originalWrapper;

        private ReplicaEntityWrapper replicaWrapper;

        private ITime timeKeeper;

        [SetUp]
        public void SetUp()
        {
            this.entityManager = MockRepository.GenerateMock<IReplicationService<IOriginal, IReplica>>();
            this.timeKeeper = new FakeTimeKeeper();
            this.serialization = new Manager(this.timeKeeper);
            TypeRegistrar.RegisterTestTypes(this.serialization);
            this.originalEntity = new ReplicableEntity();
            this.replicaEntity = new ReplicableEntity();
            this.originalWrapper = new OriginalEntityWrapper(this.originalEntity, this.entityManager, this.serialization);
            this.replicaWrapper = new ReplicaEntityWrapper(this.replicaEntity, this.serialization, new FakeTimeKeeper());
        }

        [Test]
        public void TestBoolReplication()
        {
            this.originalEntity.Bool = true;
            this.ReplicateStatePart(0);
            Assert.AreEqual(this.originalEntity.Bool, this.replicaEntity.Bool);
        }

        [Test]
        public void TestByteReplication()
        {
            this.originalEntity.Byte = 1;
            this.ReplicateStatePart(1);
            Assert.AreEqual(this.originalEntity.Byte, this.replicaEntity.Byte);
        }

        [Test]
        public void TestCharReplication()
        {
            this.originalEntity.Char = '2';
            this.ReplicateStatePart(2);
            Assert.AreEqual(this.originalEntity.Char, this.replicaEntity.Char);
        }

        [Test]
        public void TestDecimalReplication()
        {
            this.originalEntity.Decimal = 3.33m;
            this.ReplicateStatePart(3);
            Assert.AreEqual(this.originalEntity.Decimal, this.replicaEntity.Decimal);
        }

        [Test]
        public void TestDoubleReplication()
        {
            this.originalEntity.Double = 4.4;
            this.ReplicateStatePart(4);
            Assert.AreEqual(this.originalEntity.Double, this.replicaEntity.Double);
        }

        [Test]
        public void TestShortReplication()
        {
            this.originalEntity.Short = 5;
            this.ReplicateStatePart(5);
            Assert.AreEqual(this.originalEntity.Short, this.replicaEntity.Short);
        }

        [Test]
        public void TestIntReplication()
        {
            this.originalEntity.Int = 6;
            this.ReplicateStatePart(6);
            Assert.AreEqual(this.originalEntity.Int, this.replicaEntity.Int);
        }

        [Test]
        public void TestLongReplication()
        {
            this.originalEntity.Long = 7;
            this.ReplicateStatePart(7);
            Assert.AreEqual(this.originalEntity.Long, this.replicaEntity.Long);
        }

        [Test]
        public void TestSbyteReplication()
        {
            this.originalEntity.Sbyte = 8;
            this.ReplicateStatePart(8);
            Assert.AreEqual(this.originalEntity.Sbyte, this.replicaEntity.Sbyte);
        }

        [Test]
        public void TestFloatReplication()
        {
            this.originalEntity.Float = 9f;
            this.ReplicateStatePart(9);
            Assert.AreEqual(this.originalEntity.Float, this.replicaEntity.Float);
        }

        [Test]
        public void TestUshortReplication()
        {
            this.originalEntity.Ushort = 10;
            this.ReplicateStatePart(10);
            Assert.AreEqual(this.originalEntity.Ushort, this.replicaEntity.Ushort);
        }

        [Test]
        public void TestUintReplication()
        {
            this.originalEntity.Uint = 11;
            this.ReplicateStatePart(11);
            Assert.AreEqual(this.originalEntity.Uint, this.replicaEntity.Uint);
        }

        [Test]
        public void TestUlongReplication()
        {
            this.originalEntity.Ulong = 12;
            this.ReplicateStatePart(12);
            Assert.AreEqual(this.originalEntity.Ulong, this.replicaEntity.Ulong);
        }

        [Test]
        public void TestStringReplication()
        {
            this.originalEntity.String = Guid.NewGuid().ToString();
            this.ReplicateStatePart(13);
            Assert.AreEqual(this.originalEntity.String, this.replicaEntity.String);
        }

        [Test]
        public void TestVector3Replication()
        {
            this.originalEntity.Vector3 = new Vector3(1, 2, 3);
            this.ReplicateStatePart(14);
            Assert.AreEqual(this.originalEntity.Vector3, this.replicaEntity.Vector3);
        }

        [Test]
        public void TestMutableReferenceTypeReplacementReplication()
        {
            this.originalEntity.MutableReferenceType = new MutableReferenceType { Number = 7, Word = "foo" };
            this.ReplicateStatePart(15);
            Assert.AreEqual(this.originalEntity.MutableReferenceType.Number, this.replicaEntity.MutableReferenceType.Number);
            Assert.AreEqual(this.originalEntity.MutableReferenceType.Word, this.replicaEntity.MutableReferenceType.Word);
        }

        [Test]
        public void TestMutableReferenceTypeMutationReplication()
        {
            this.originalEntity.MutableReferenceType = new MutableReferenceType { Number = 7, Word = "foo" };
            this.ReplicateStatePart(15);
            Assert.AreEqual(this.originalEntity.MutableReferenceType.Number, this.replicaEntity.MutableReferenceType.Number);
            Assert.AreEqual(this.originalEntity.MutableReferenceType.Word, this.replicaEntity.MutableReferenceType.Word);
            this.replicaEntity.MutableReferenceType.Number = 8;
            this.ReplicateStatePart(15);
            Assert.AreEqual(this.originalEntity.MutableReferenceType.Number, this.replicaEntity.MutableReferenceType.Number);
            Assert.AreEqual(this.originalEntity.MutableReferenceType.Word, this.replicaEntity.MutableReferenceType.Word);
        }

        [Test]
        public void TestMutableValueTypeReplacementReplication()
        {
            this.originalEntity.MutableValueType = new MutableValueType { Number = 7, Word = "foo" };
            this.ReplicateStatePart(16);
            Assert.AreEqual(this.originalEntity.MutableValueType.Number, this.replicaEntity.MutableValueType.Number);
            Assert.AreEqual(this.originalEntity.MutableValueType.Word, this.replicaEntity.MutableValueType.Word);
        }

        [Test]
        public void TestMutableValueTypeMutationReplication()
        {
            this.originalEntity.MutableValueType = new MutableValueType { Number = 7, Word = "foo" };
            this.ReplicateStatePart(16);
            Assert.AreEqual(this.originalEntity.MutableValueType.Number, this.replicaEntity.MutableValueType.Number);
            Assert.AreEqual(this.originalEntity.MutableValueType.Word, this.replicaEntity.MutableValueType.Word);
            this.originalEntity.MutateValueType(8, "bar");
            this.ReplicateStatePart(16);
            Assert.AreEqual(this.originalEntity.MutableValueType.Number, this.replicaEntity.MutableValueType.Number);
            Assert.AreEqual(this.originalEntity.MutableValueType.Word, this.replicaEntity.MutableValueType.Word);
        }

        [Test]
        public void TestImmutableReferenceTypeReplication()
        {
            this.originalEntity.ImmutableReferenceType = new ImmutableReferenceType(7, "foo");
            this.ReplicateStatePart(17);
            Assert.AreEqual(this.originalEntity.ImmutableReferenceType.Number, this.replicaEntity.ImmutableReferenceType.Number);
            Assert.AreEqual(this.originalEntity.ImmutableReferenceType.Word, this.replicaEntity.ImmutableReferenceType.Word);
        }

        [Test]
        public void TestImmutableValueTypeReplication()
        {
            this.originalEntity.ImmutableValueType = new ImmutableValueType(7, "foo");
            this.ReplicateStatePart(18);
            Assert.AreEqual(this.originalEntity.ImmutableValueType.Number, this.replicaEntity.ImmutableValueType.Number);
            Assert.AreEqual(this.originalEntity.ImmutableValueType.Word, this.replicaEntity.ImmutableValueType.Word);
        }

        private void ReplicateStatePart(int stateSegmentOffset)
        {
            var parts = new BooleanArray((int)SpatialEntityStateSegment.FirstAvailableSegment + stateSegmentOffset);
            var stream = new MemoryStream();
            this.originalWrapper.Serialize(parts, stream);
            stream.Seek(0, SeekOrigin.Begin);
            this.replicaWrapper.Deserialize(parts, stream, 1, BadumnaId.None);
        }

        private class StubNetworkFacade : FakeNetworkFacade
        {
            public override void FlagForUpdate(ISpatialOriginal localEntity, BooleanArray changedParts)
            {
                // Ignore.
            }
        }
    }
}
