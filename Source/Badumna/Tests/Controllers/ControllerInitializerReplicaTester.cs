﻿//-----------------------------------------------------------------------
// <copyright file="ControllerInitializerReplicaTester.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System.Collections.Generic;
using Badumna.Controllers;
using Badumna.Core;
using BadumnaTests.Core;
using NUnit.Framework;

namespace BadumnaTests.Controllers
{
    [TestFixture]
    [DontPerformCoverage]
    public class ControllerInitializerReplicaTester : ParseableTestHelper
    {
        public ControllerInitializerReplicaTester()
            : base(new ParseableTester())
        {
        }

        [Test]
        public void SimpleConstructorDoesntProduceError()
        {
            ControllerInitializerReplica replica = new ControllerInitializerReplica();
            ControllerInitializerReplica replica1 = new ControllerInitializerReplica("typename", "uniquename");
        }

        [Test]
        public void SimpleConstructorGeneratesValidIds()
        {
            ControllerInitializerReplica replica = new ControllerInitializerReplica("typename", "uniquename");
            Assert.AreNotEqual(replica.CheckpointDataId, replica.Guid);
            Assert.AreEqual(replica.CheckpointDataId.LocalId, replica.Guid.LocalId + 1);
        }

        [Test]
        public void ControllerInitializerReplicaIsAlwaysAddressedByKey()
        {
            ControllerInitializerReplica replica = new ControllerInitializerReplica("typename", "uniquename");
            Assert.NotNull(replica.Key);
            Assert.IsTrue(replica.Guid.Address.IsDhtAddress);
            Assert.IsTrue(replica.CheckpointDataId.Address.IsDhtAddress);
        }

        internal class ParseableTester : ParseableTester<ControllerInitializerReplica>
        {
            public override ICollection<ControllerInitializerReplica> CreateExpectedValues()
            {
                ICollection<ControllerInitializerReplica> expectedValues = new List<ControllerInitializerReplica>();
                ControllerInitializerReplica replica = new ControllerInitializerReplica("typename", "uniquename");
                expectedValues.Add(replica);

                return expectedValues;
            }

            public override void AssertAreEqual(ControllerInitializerReplica expected, ControllerInitializerReplica actual)
            {
                Assert.AreEqual(expected.CheckpointDataId, actual.CheckpointDataId);
                Assert.AreEqual(expected.Guid, actual.Guid);
                Assert.AreEqual(expected.UniqieName, actual.UniqieName);
                Assert.AreEqual(expected.ControllerTypeName, actual.ControllerTypeName);
            }
        }
    }
}
