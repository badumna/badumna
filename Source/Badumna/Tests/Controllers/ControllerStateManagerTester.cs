﻿//-----------------------------------------------------------------------
// <copyright file="ControllerStateManagerTester.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using Badumna.Controllers;
using Badumna.Core;
using Badumna.DataTypes;
using Badumna.DistributedHashTable;
using Badumna.Utilities;
using BadumnaTests.Core;
using BadumnaTests.DistributedHashTable.Services;
using BadumnaTests.Utilities;
using NUnit.Framework;
using Rhino.Mocks;

namespace BadumnaTests.Controllers
{
    [TestFixture]
    [DontPerformCoverage]
    public class ControllerStateManagerTester
    {
        private DistributedControllerTestHelper mockedDistributedController;
        private DistributedControllerTestHelper mockedDistributedController2;

        private QueueInvokable queueApplicationEvent;

        private MockLoopbackRouter router;
        private DhtFacade dhtFacade;

        private Simulator eventQueue;
        private ITime timeKeeper;
        private INetworkConnectivityReporter connectivityReporter;

        private GenericCallBackReturn<BadumnaId, HashKey> generateBadumnaId;

        [SetUp]
        public void Initialize()
        {
            this.eventQueue = new Simulator();
            this.timeKeeper = this.eventQueue;

            INetworkAddressProvider addressProvider = MockRepository.GenerateStub<INetworkAddressProvider>();
            addressProvider.Stub(x => x.PublicAddress).Return(PeerAddress.GetLoopback(1));

            this.connectivityReporter = new NetworkConnectivityReporter(this.eventQueue);
            this.connectivityReporter.SetStatus(ConnectivityStatus.Online);

            this.queueApplicationEvent = i => i.Invoke();

            this.generateBadumnaId = BadumnaIdGenerator.FromHash();

            this.router = new MockLoopbackRouter(this.eventQueue, addressProvider);
            this.dhtFacade = new DhtFacade(this.router, this.eventQueue, this.timeKeeper, addressProvider, this.connectivityReporter, MessageParserTester.DefaultFactory);
            this.router.DhtFacade = this.dhtFacade;

            Assert.AreEqual(this.dhtFacade, this.router.DhtFacade);
            Assert.AreEqual(HashKey.Min, this.router.LocalHashKey);

            this.ResetDistributedController();
        }

        [TearDown]
        public void Terminate()
        {
        }

        [Test]
        public void ConstructorThrowsNoException()
        {
            ControllerStateManager manager = new ControllerStateManager(this.dhtFacade, this.eventQueue, this.queueApplicationEvent);
            Assert.AreEqual(0, manager.Count);
        }

        [Test]
        public void RegisterControllerReturnsTrueWhenRegisteringNewController()
        {
            ControllerStateManager manager = new ControllerStateManager(this.dhtFacade, this.eventQueue, this.queueApplicationEvent);
            bool success = manager.RegisterController(this.mockedDistributedController);
            Assert.IsTrue(success);
            Assert.AreEqual(1, manager.Count);
        }

        [Test]
        public void RegisterControllerReturnsFalseWhenRegisteringDuplicatedController()
        {
            ControllerStateManager manager = new ControllerStateManager(this.dhtFacade, this.eventQueue, this.queueApplicationEvent);
            manager.RegisterController(this.mockedDistributedController);
            bool success = manager.RegisterController(this.mockedDistributedController);
            Assert.IsFalse(success);
            Assert.AreEqual(1, manager.Count);
        }

        [Test]
        public void LocallyControlledControllerWillBeProcessedWhenProcessControllerIsCalled()
        {
            this.ResetDistributedController();

            ControllerStateManager manager = new ControllerStateManager(this.dhtFacade, this.eventQueue, this.queueApplicationEvent);
            manager.RegisterController(this.mockedDistributedController);
            this.mockedDistributedController.Expect(f => f.WakePublicMethod()).Repeat.Once();
            this.mockedDistributedController.InvokeWake(null);
            this.mockedDistributedController.Expect(f => f.InvokeProcess(TimeSpan.FromMilliseconds(20))).Repeat.Once();
            manager.ProcessController(this.mockedDistributedController, TimeSpan.FromMilliseconds(20));
            this.mockedDistributedController.VerifyAllExpectations();
        }

        [Test]
        public void OnlyLocallyControlledControllerWillBeProcessedWhenProcessControllerIsCalled()
        {
            this.ResetDistributedController();

            ControllerStateManager manager = new ControllerStateManager(this.dhtFacade, this.eventQueue, this.queueApplicationEvent);
            manager.RegisterController(this.mockedDistributedController);

            this.mockedDistributedController.Expect(f => f.InvokeProcess(TimeSpan.FromMilliseconds(20))).Repeat.Never();
            manager.ProcessController(this.mockedDistributedController, TimeSpan.FromMilliseconds(20));
            this.mockedDistributedController.VerifyAllExpectations();
        }

        [Test]
        public void AllLocallyControlledControllerWillBeProcessedWhenProcessIsCalled()
        {
            this.ResetDistributedController();

            ControllerStateManager manager = new ControllerStateManager(this.dhtFacade, this.eventQueue, this.queueApplicationEvent);
            manager.RegisterController(this.mockedDistributedController);
            manager.RegisterController(this.mockedDistributedController2);

            this.mockedDistributedController.Expect(f => f.WakePublicMethod()).Repeat.Once();
            this.mockedDistributedController.InvokeWake(null);
            this.mockedDistributedController2.Expect(f => f.WakePublicMethod()).Repeat.Once();
            this.mockedDistributedController2.InvokeWake(null);

            this.mockedDistributedController.Expect(f => f.InvokeProcess(TimeSpan.FromMilliseconds(20))).IgnoreArguments().Repeat.Once();
            this.mockedDistributedController2.Expect(f => f.InvokeProcess(TimeSpan.FromMilliseconds(20))).IgnoreArguments().Repeat.Once();
            
            manager.Process();
            this.mockedDistributedController.VerifyAllExpectations();
            this.mockedDistributedController2.VerifyAllExpectations();
        }

        [Test]
        public void CallToSleepAllTriggerAllLocallyControlledControllersToCallWakeOnAppropriatePeer()
        {
            this.ResetDistributedController();

            DistributedControllerTestHelper mockedDistributedController3 = MockRepository.GeneratePartialMock<DistributedControllerTestHelper>("uniquename3");
            mockedDistributedController3.PostConstructor(this.eventQueue, this.queueApplicationEvent, delegate { });
            BadumnaId id1 = this.generateBadumnaId(HashKey.Hash("test13"));
            BadumnaId id2 = this.generateBadumnaId(HashKey.Hash("test23"));
            mockedDistributedController3.Initialize(null, id1, id2);

            ControllerStateManager manager = new ControllerStateManager(this.dhtFacade, this.eventQueue, this.queueApplicationEvent);
            manager.RegisterController(this.mockedDistributedController);
            manager.RegisterController(this.mockedDistributedController2);
            manager.RegisterController(mockedDistributedController3);

            mockedDistributedController3.Expect(f => f.WakePublicMethod()).Repeat.Once();
            mockedDistributedController3.InvokeWake(null);

            // controller 1 & 2 are not locally controller, 3 is locally controlled.
            this.mockedDistributedController.Expect(f => f.SleepPublicMethod()).Repeat.Never();
            this.mockedDistributedController.Expect(f => f.CheckpointPublicMethod(null)).IgnoreArguments().Repeat.Never();
            this.mockedDistributedController2.Expect(f => f.SleepPublicMethod()).Repeat.Never();
            this.mockedDistributedController2.Expect(f => f.CheckpointPublicMethod(null)).IgnoreArguments().Repeat.Never();
            mockedDistributedController3.Expect(f => f.SleepPublicMethod()).IgnoreArguments().Repeat.Once();
           
            manager.SleepAll();

            this.mockedDistributedController.VerifyAllExpectations();
            this.mockedDistributedController2.VerifyAllExpectations();
            mockedDistributedController3.VerifyAllExpectations();
        }

        [Test]
        public void ControllerReplicaExpireEventTriggerInvokeReplicate()
        {
            this.ResetDistributedController();

            ControllerStateManager manager = new ControllerStateManager(this.dhtFacade, this.eventQueue, this.queueApplicationEvent);
            manager.RegisterController(this.mockedDistributedController);

            this.mockedDistributedController.Expect(f => f.WakePublicMethod()).Repeat.Once();
            this.mockedDistributedController.InvokeWake(null);
            
            this.mockedDistributedController.Expect(f => f.InvokeReplicate());
            manager.ControllerReplicaExpiredHandler(this.mockedDistributedController.Guid);
            this.mockedDistributedController.VerifyAllExpectations();
        }

        [Test]
        public void ControllerReplicaExpireEventFromUnknownReplicaWillBeIgnored()
        {
            this.ResetDistributedController();

            ControllerStateManager manager = new ControllerStateManager(this.dhtFacade, this.eventQueue, this.queueApplicationEvent);
            manager.RegisterController(this.mockedDistributedController);

            this.mockedDistributedController.Expect(f => f.WakePublicMethod()).Repeat.Once();
            this.mockedDistributedController.InvokeWake(null);

            this.mockedDistributedController.Expect(f => f.InvokeReplicate()).Repeat.Never();
            manager.ControllerReplicaExpiredHandler(this.generateBadumnaId(HashKey.Hash("unknown_id_1234")));
            this.mockedDistributedController.VerifyAllExpectations();
        }

        [Test]
        public void WakeControllerTriggersInvokeWake()
        {
            this.ResetDistributedController();

            ControllerStateManager manager = new ControllerStateManager(this.dhtFacade, this.eventQueue, this.queueApplicationEvent);
            manager.RegisterController(this.mockedDistributedController);

            this.mockedDistributedController.Expect(f => f.InvokeWake(null)).Repeat.Once();
            manager.WakeController(this.mockedDistributedController, null);
            this.mockedDistributedController.VerifyAllExpectations();
        }

        [Test]
        public void CallToWakeAllControllersWhenNecessaryTriggerAllNonLocallyControllersToCallWakeController()
        {
            this.ResetDistributedController();

            DistributedControllerTestHelper mockedDistributedController3 = MockRepository.GeneratePartialMock<DistributedControllerTestHelper>("uniquename3");
            mockedDistributedController3.PostConstructor(this.eventQueue, this.queueApplicationEvent, delegate { });
            BadumnaId id1 = this.generateBadumnaId(HashKey.Hash("test13"));
            BadumnaId id2 = this.generateBadumnaId(HashKey.Hash("test23"));
            mockedDistributedController3.Initialize(null, id1, id2);

            ControllerStateManager manager = new ControllerStateManager(this.dhtFacade, this.eventQueue, this.queueApplicationEvent);
            manager.RegisterController(this.mockedDistributedController);
            manager.RegisterController(this.mockedDistributedController2);
            manager.RegisterController(mockedDistributedController3);

            mockedDistributedController3.Expect(f => f.WakePublicMethod()).Repeat.Once();
            mockedDistributedController3.InvokeWake(null);
            
            // controller 1 & 2 are not locally controller, 3 is locally controlled.
            this.mockedDistributedController.Expect(f => f.InvokeWake(null)).Repeat.Once();
            this.mockedDistributedController2.Expect(f => f.InvokeWake(null)).Repeat.Once();
            mockedDistributedController3.Expect(f => f.InvokeWake(null)).IgnoreArguments().Repeat.Never();

            manager.WakeAllControllersWhenNecessary();

            this.mockedDistributedController.VerifyAllExpectations();
            this.mockedDistributedController2.VerifyAllExpectations();
            mockedDistributedController3.VerifyAllExpectations();
        }

        [Test]
        public void CallToWakeOnAppropriatePeerTriggerInvokeReplicateAndInvokeSleep()
        {
            this.ResetDistributedController();
            
            ControllerStateManager manager = new ControllerStateManager(this.dhtFacade, this.eventQueue, this.queueApplicationEvent);
            manager.ForgeMethodList();
            manager.RegisterController(this.mockedDistributedController);

            this.mockedDistributedController.Expect(f => f.WakePublicMethod()).Repeat.Once();
            this.mockedDistributedController.InvokeWake(null);

            this.mockedDistributedController.Expect(f => f.InvokeReplicate()).Repeat.Once();
            this.mockedDistributedController.Expect(f => f.InvokeSleep()).Repeat.Once();

            manager.WakeOnAppropriatePeer(this.mockedDistributedController);
            this.eventQueue.RunFor(TimeSpan.FromSeconds(10));
            this.mockedDistributedController.VerifyAllExpectations();
        }

        [Test]
        public void NeighbourArrivalHandlerTriggerControllerMigrationIfNeighbourIsCloser()
        {
            this.ResetDistributedController();

            ControllerStateManager manager = new ControllerStateManager(this.dhtFacade, this.eventQueue, this.queueApplicationEvent);
            manager.RegisterController(this.mockedDistributedController);

            this.mockedDistributedController.Expect(f => f.WakePublicMethod()).Repeat.Once();
            this.mockedDistributedController.InvokeWake(null);

            this.mockedDistributedController.Expect(f => f.InvokeReplicate()).Repeat.Once();
            this.mockedDistributedController.Expect(f => f.InvokeSleep()).Repeat.Once();

            byte[] keyBytes = this.mockedDistributedController.HashKey.ToBytes();
            keyBytes[keyBytes.Length - 1] += 0x01;
            manager.NeighbourArrivalNotification(new HashKey(keyBytes));

            this.mockedDistributedController.VerifyAllExpectations();
        }

        [Test]
        public void NeighbourArrivalHandlerWillIgnoreNonLocallyControlledController()
        {
            this.ResetDistributedController();

            ControllerStateManager manager = new ControllerStateManager(this.dhtFacade, this.eventQueue, this.queueApplicationEvent);
            manager.RegisterController(this.mockedDistributedController);

            this.mockedDistributedController.Expect(f => f.InvokeReplicate()).Repeat.Never();
            this.mockedDistributedController.Expect(f => f.InvokeSleep()).Repeat.Never();

            byte[] keyBytes = this.mockedDistributedController.HashKey.ToBytes();
            keyBytes[keyBytes.Length - 1] += 0x01;
            manager.NeighbourArrivalNotification(new HashKey(keyBytes));

            this.mockedDistributedController.VerifyAllExpectations();
        }

        [Test]
        public void NeighbourArrivalHandlerWillIgnoreNeighboursNotCloser()
        {
            this.ResetDistributedController();

            ControllerStateManager manager = new ControllerStateManager(this.dhtFacade, this.eventQueue, this.queueApplicationEvent);
            manager.RegisterController(this.mockedDistributedController);

            this.mockedDistributedController.Expect(f => f.WakePublicMethod()).Repeat.Once();
            this.mockedDistributedController.InvokeWake(null);

            this.mockedDistributedController.Expect(f => f.InvokeReplicate()).Repeat.Never();
            this.mockedDistributedController.Expect(f => f.InvokeSleep()).Repeat.Never();

            byte[] keyBytes = this.mockedDistributedController.HashKey.ToBytes();
            keyBytes[0] += 0x80;
            manager.NeighbourArrivalNotification(new HashKey(keyBytes));

            this.mockedDistributedController.VerifyAllExpectations();
        }

        private void ResetDistributedController()
        {
            this.mockedDistributedController = MockRepository.GeneratePartialMock<DistributedControllerTestHelper>("uniquename");
            this.mockedDistributedController.PostConstructor(this.eventQueue, this.queueApplicationEvent, delegate { });
            BadumnaId id1 = this.generateBadumnaId(HashKey.Hash("test1"));
            BadumnaId id2 = this.generateBadumnaId(HashKey.Hash("test2"));
            this.mockedDistributedController.Initialize(null, id1, id2);

            this.mockedDistributedController2 = MockRepository.GeneratePartialMock<DistributedControllerTestHelper>("uniquename2");
            this.mockedDistributedController2.PostConstructor(this.eventQueue, this.queueApplicationEvent, delegate { });
            id1 = this.generateBadumnaId(HashKey.Hash("test12"));
            id2 = this.generateBadumnaId(HashKey.Hash("test22"));
            this.mockedDistributedController2.Initialize(null, id1, id2);
        }
    }
}
