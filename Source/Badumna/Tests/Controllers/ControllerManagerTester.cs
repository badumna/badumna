﻿//-----------------------------------------------------------------------
// <copyright file="ControllerManagerTester.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using Badumna.Controllers;
using Badumna.Core;
using Badumna.DataTypes;
using Badumna.DistributedHashTable;
using Badumna.Utilities;
using BadumnaTests.Core;
using BadumnaTests.DistributedHashTable.Services;
using BadumnaTests.Utilities;
using NUnit.Framework;
using Rhino.Mocks;

namespace BadumnaTests.Controllers
{
    [TestFixture]
    [DontPerformCoverage]
    public class ControllerManagerTester
    {
        private DistributedControllerTestHelper mockedDistributedController;
        private DistributedControllerTestHelper mockedDistributedController2;

        private QueueInvokable queueApplicationEvent;

        private MockLoopbackRouter router;
        private DhtFacade dhtFacade;

        private Simulator eventQueue;
        private ITime timeKeeper;
        private INetworkConnectivityReporter connectivityReporter;

        private GenericCallBackReturn<BadumnaId, HashKey> generateBadumnaId;

        [SetUp]
        public void Initialize()
        {
            this.eventQueue = new Simulator();
            this.timeKeeper = this.eventQueue;

            INetworkAddressProvider addressProvider = MockRepository.GenerateStub<INetworkAddressProvider>();
            addressProvider.Stub(x => x.PublicAddress).Return(PeerAddress.GetLoopback(1));

            this.connectivityReporter = new NetworkConnectivityReporter(this.eventQueue);
            this.connectivityReporter.SetStatus(ConnectivityStatus.Online);

            this.generateBadumnaId = BadumnaIdGenerator.FromHash();

            this.queueApplicationEvent = i => i.Invoke();

            this.router = new MockLoopbackRouter(this.eventQueue, addressProvider);
            this.dhtFacade = new DhtFacade(this.router, this.eventQueue, this.timeKeeper, addressProvider, this.connectivityReporter, MessageParserTester.DefaultFactory);
            this.router.DhtFacade = this.dhtFacade;

            Assert.AreEqual(this.dhtFacade, this.router.DhtFacade);
            Assert.AreEqual(HashKey.Min, this.router.LocalHashKey);

            this.ResetDistributedController();
        }

        [TearDown]
        public void Terminate()
        {
        }

        [Test]
        public void SimpleConstructorThrowsNoException()
        {
            ControllerManager manager = new ControllerManager(this.dhtFacade, this.eventQueue, this.queueApplicationEvent, this.connectivityReporter);
        }

        [Test]
        public void IsInitializedFlagIsFalseByDefault()
        {
            ControllerManager manager = new ControllerManager(this.dhtFacade, this.eventQueue, this.queueApplicationEvent, this.connectivityReporter);
            Assert.IsFalse(manager.IsInitialized);
        }

        [Test]
        public void NetworkShuttingDownEventTriggersSleepAll()
        {
            this.ResetDistributedController();

            DistributedControllerTestHelper mockedDistributedController3 = MockRepository.GeneratePartialMock<DistributedControllerTestHelper>("uniquename3");
            mockedDistributedController3.PostConstructor(this.eventQueue, this.queueApplicationEvent, delegate { });
            BadumnaId id1 = this.generateBadumnaId(HashKey.Hash("test13"));
            BadumnaId id2 = this.generateBadumnaId(HashKey.Hash("test23"));
            mockedDistributedController3.Initialize(null, id1, id2);

            ControllerStateManager manager = new ControllerStateManager(this.dhtFacade, this.eventQueue, this.queueApplicationEvent);
            manager.RegisterController(this.mockedDistributedController);
            manager.RegisterController(this.mockedDistributedController2);
            manager.RegisterController(mockedDistributedController3);

            mockedDistributedController3.Expect(f => f.WakePublicMethod()).Repeat.Once();
            mockedDistributedController3.InvokeWake(null);

            // controller 1 & 2 are not locally controller, 3 is locally controlled.
            this.mockedDistributedController.Expect(f => f.SleepPublicMethod()).Repeat.Never();
            this.mockedDistributedController.Expect(f => f.CheckpointPublicMethod(null)).IgnoreArguments().Repeat.Never();
            this.mockedDistributedController2.Expect(f => f.SleepPublicMethod()).Repeat.Never();
            this.mockedDistributedController2.Expect(f => f.CheckpointPublicMethod(null)).IgnoreArguments().Repeat.Never();
            mockedDistributedController3.Expect(f => f.SleepPublicMethod()).IgnoreArguments().Repeat.Once();

            ControllerManager controllerManager = new ControllerManager(this.dhtFacade, manager, this.eventQueue, this.connectivityReporter);
            this.connectivityReporter.SetStatus(ConnectivityStatus.ShuttingDown);

            this.mockedDistributedController.VerifyAllExpectations();
            this.mockedDistributedController2.VerifyAllExpectations();
            mockedDistributedController3.VerifyAllExpectations();
        }

        private void ResetDistributedController()
        {
            this.mockedDistributedController = MockRepository.GeneratePartialMock<DistributedControllerTestHelper>("uniquename");
            this.mockedDistributedController.PostConstructor(this.eventQueue, this.queueApplicationEvent, delegate { });
            BadumnaId id1 = this.generateBadumnaId(HashKey.Hash("test1"));
            BadumnaId id2 = this.generateBadumnaId(HashKey.Hash("test2"));
            this.mockedDistributedController.Initialize(null, id1, id2);

            this.mockedDistributedController2 = MockRepository.GeneratePartialMock<DistributedControllerTestHelper>("uniquename2");
            this.mockedDistributedController2.PostConstructor(this.eventQueue, this.queueApplicationEvent, delegate { });
            id1 = this.generateBadumnaId(HashKey.Hash("test12"));
            id2 = this.generateBadumnaId(HashKey.Hash("test22"));
            this.mockedDistributedController2.Initialize(null, id1, id2);
        }
    }
}
