﻿//-----------------------------------------------------------------------
// <copyright file="ConstructorInfoManagerTester.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System.Reflection;
using Badumna.Controllers;
using Badumna.Core;
using NUnit.Framework;

namespace BadumnaTests.Controllers
{
    [TestFixture]
    [DontPerformCoverage]
    public class ConstructorInfoManagerTester
    {
        [Test]
        public void GetConstructorInfoReturnsNullForUnknownTypeName()
        {
            ConstructorInfo info = ConstructorInfoManager.GetConstructorInfo("unknown_name");
            Assert.Null(info);
        }

        [Test]
        public void GetConstructorInfoReturnsObjectWhenUsingValidTypeName()
        {
            ConstructorInfo info = ConstructorInfoManager.GetConstructorInfo("BadumnaTests.Controllers.ConstructorInfoManagerTesterSample1");
            Assert.NotNull(info);
        }

        [Test]
        public void GetConstructorInfoReturnsNullWhenNoMatchingConstructorFound()
        {
            ConstructorInfo info = ConstructorInfoManager.GetConstructorInfo("BadumnaTests.Controllers.ConstructorInfoManagerTesterSample2");
            Assert.Null(info);
        }
    }

    [DontPerformCoverage]
    internal class ConstructorInfoManagerTesterSample1
    {
        public ConstructorInfoManagerTesterSample1(string whatever)
        {
        }
    }

    [DontPerformCoverage]
    internal class ConstructorInfoManagerTesterSample2
    {
        public ConstructorInfoManagerTesterSample2()
        {
        }
    }
}
