﻿//-----------------------------------------------------------------------
// <copyright file="DistributedControllerTester.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using Badumna.Core;
using Badumna.DataTypes;
using Badumna.DistributedHashTable;
using Badumna.Utilities;
using BadumnaTests.Core;
using BadumnaTests.DistributedHashTable.Services;
using NUnit.Framework;
using Rhino.Mocks;

namespace BadumnaTests.Controllers
{
    [TestFixture]
    [DontPerformCoverage]
    public class DistributedControllerTester
    {
        private DistributedControllerTestHelper mockedDistributedController;
        private QueueInvokable queueApplicationEvent;

        private MockLoopbackRouter router;
        private DhtFacade dhtFacade;

        private Simulator eventQueue;
        private ITime timeKeeper;
        private INetworkConnectivityReporter connectivityReporter;

        [SetUp]
        public void Initialize()
        {
            this.eventQueue = new Simulator();
            this.timeKeeper = this.eventQueue;

            INetworkAddressProvider addressProvider = MockRepository.GenerateStub<INetworkAddressProvider>();
            addressProvider.Stub(x => x.PublicAddress).Return(PeerAddress.GetLoopback(1));

            this.connectivityReporter = new NetworkConnectivityReporter(this.eventQueue);
            this.connectivityReporter.SetStatus(ConnectivityStatus.Online);

            this.router = new MockLoopbackRouter(this.eventQueue, addressProvider);
            this.dhtFacade = new DhtFacade(this.router, this.eventQueue, this.timeKeeper, addressProvider, this.connectivityReporter, MessageParserTester.DefaultFactory);
            this.router.DhtFacade = this.dhtFacade;
           
            this.queueApplicationEvent = i => i.Invoke();
            
            Assert.AreEqual(this.dhtFacade, this.router.DhtFacade);
            Assert.AreEqual(HashKey.Min, this.router.LocalHashKey);

            this.mockedDistributedController = MockRepository.GeneratePartialMock<DistributedControllerTestHelper>("uniquename");
            this.mockedDistributedController.PostConstructor(this.eventQueue, this.queueApplicationEvent, delegate { });
            BadumnaId id1 = new BadumnaId(new PeerAddress(HashKey.Hash("test1")), 1);
            BadumnaId id2 = new BadumnaId(new PeerAddress(HashKey.Hash("test2")), 2); 
            this.mockedDistributedController.Initialize(null, id1, id2);
        }

        [TearDown]
        public void Terminate()
        {
        }

        [Test]
        public void CallToInvokeProcessWillBePassedToProcess()
        {
            TimeSpan duration = TimeSpan.FromMilliseconds(100);
            this.mockedDistributedController.Expect(f => f.ProcessPublicMethod(duration)).Repeat.Once();
            this.mockedDistributedController.InvokeProcess(duration);
            this.mockedDistributedController.VerifyAllExpectations();
        }

        [Test]
        public void CallToInvokeRecoverWithCheckPointDataWillBePassedToRecoverWithCheckPointData()
        {
            byte[] checkpointData = new byte[16];
            this.mockedDistributedController.Expect(f => f.RecoverPublicMethod(null)).IgnoreArguments().Repeat.Once();
            this.mockedDistributedController.InvokeRecover(checkpointData);
            this.mockedDistributedController.VerifyAllExpectations();
        }

        [Test]
        public void CallToInvokeRecoverWithoutCheckPointDataWillBePassedToRecoverWithoutCheckPointData()
        {
            this.mockedDistributedController.Expect(f => f.RecoverPublicMethod()).Repeat.Once();
            this.mockedDistributedController.InvokeRecover(null);
            this.mockedDistributedController.VerifyAllExpectations();
        }

        [Test]
        public void CallToInvokeWakeWithCheckpointDataWillTriggerWakeAndRecover()
        {
            byte[] checkpointData = new byte[16];
            this.mockedDistributedController.Expect(f => f.RecoverPublicMethod(null)).IgnoreArguments().Repeat.Once();
            this.mockedDistributedController.Expect(f => f.WakePublicMethod()).Repeat.Once();
            this.mockedDistributedController.InvokeWake(checkpointData);
            this.mockedDistributedController.VerifyAllExpectations();
        }

        [Test]
        public void CallToInvokeWakeWithoutCheckpointDataWillTriggerWakeOnly()
        {
            this.mockedDistributedController.Expect(f => f.RecoverPublicMethod(null)).IgnoreArguments().Repeat.Never();
            this.mockedDistributedController.Expect(f => f.RecoverPublicMethod()).IgnoreArguments().Repeat.Never();
            this.mockedDistributedController.Expect(f => f.WakePublicMethod()).Repeat.Once();
            this.mockedDistributedController.InvokeWake(null);
            this.mockedDistributedController.VerifyAllExpectations();
        }

        [Test]
        public void CallToInvokeWakeWillHaveQueuedMessagesProcessed()
        {
            this.mockedDistributedController.HandleMessage(new MessageReadStream(new byte[16]), null, null);
            this.mockedDistributedController.HandleMessage(new MessageReadStream(new byte[16]), null, null);
            this.mockedDistributedController.Expect(f => f.RecoverPublicMethod(null)).IgnoreArguments().Repeat.Never();
            this.mockedDistributedController.Expect(f => f.RecoverPublicMethod()).IgnoreArguments().Repeat.Never();
            this.mockedDistributedController.Expect(f => f.WakePublicMethod()).Repeat.Once();
            this.mockedDistributedController.Expect(f => f.ReceiveMessagePublicMethod(null, null)).IgnoreArguments().Repeat.Twice();
            this.mockedDistributedController.InvokeWake(null);
            this.mockedDistributedController.VerifyAllExpectations();
        }

        [Test]
        public void CallToInvokeReplicateWillTriggerCheckpoint()
        {
            this.CheckpointTest(Parameters.DistributedControllerMaxCheckpointSize / 2);
        }

        [Test]
        [ExpectedException("Badumna.Controllers.CheckpointDataTooLongException")]
        public void LargeCheckpointDataSizeLeadsToCheckpointDataTooLongException()
        {
            this.CheckpointTest(Parameters.DistributedControllerMaxCheckpointSize + 1);
        }

        [Test]
        public void CallToInvokeSleepWillTriggerSleep()
        {
            this.mockedDistributedController.Expect(f => f.SleepPublicMethod()).Repeat.Once();
            this.mockedDistributedController.InvokeSleep();
            this.mockedDistributedController.VerifyAllExpectations();
        }

        [Test]
        public void CallToInvokeSleepWillMarkTheControllerToBeNotLocallyControlled()
        {
            this.mockedDistributedController.Expect(f => f.SleepPublicMethod()).Repeat.Once();
            this.mockedDistributedController.InvokeSleep();
            this.mockedDistributedController.VerifyAllExpectations();
            Assert.IsFalse(this.mockedDistributedController.IsControlledLocally);
        }

        private void CheckpointTest(int size)
        {
            this.mockedDistributedController.DhtFacade = this.dhtFacade;

            // call wake to make the controller believes that it is locally controlled. 
            this.mockedDistributedController.Expect(f => f.RecoverPublicMethod(null)).IgnoreArguments().Repeat.Never();
            this.mockedDistributedController.Expect(f => f.RecoverPublicMethod()).IgnoreArguments().Repeat.Never();
            this.mockedDistributedController.Expect(f => f.WakePublicMethod()).Repeat.Once();
            this.mockedDistributedController.InvokeWake(null);

            this.mockedDistributedController.CheckPointSize = size;
            this.mockedDistributedController.Expect(f => f.CheckpointPublicMethod(null)).IgnoreArguments().Repeat.Once();
            this.mockedDistributedController.InvokeReplicate();
            this.mockedDistributedController.VerifyAllExpectations();
        }
    }
}