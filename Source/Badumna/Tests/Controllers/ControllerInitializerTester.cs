﻿//-----------------------------------------------------------------------
// <copyright file="ControllerInitializerTester.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using Badumna;
using Badumna.Controllers;
using Badumna.Core;
using Badumna.DistributedHashTable;
using Badumna.Utilities;
using BadumnaTests.Core;
using BadumnaTests.DistributedHashTable.Services;
using NUnit.Framework;
using Rhino.Mocks;

namespace BadumnaTests.Controllers
{
    [TestFixture]
    [DontPerformCoverage]
    public class ControllerInitializerTester
    {
        private INetworkFacade networkFacade;
        private MockLoopbackRouter router;
        private DhtFacade dhtFacade;

        private INetworkConnectivityReporter connectivityReporter;

        private ControllerInitializer initializer;
        private IControllerManager controllerManagerMock;

        private QueueInvokable queueApplicationEvent;

        private Simulator eventQueue;
        private ITime timeKeeper;

        [SetUp]
        public void Initialize()
        {
            this.networkFacade = MockRepository.GenerateMock<INetworkFacade>();

            this.eventQueue = new Simulator();
            this.timeKeeper = this.eventQueue;

            INetworkAddressProvider addressProvider = MockRepository.GenerateStub<INetworkAddressProvider>();
            addressProvider.Stub(x => x.PublicAddress).Return(PeerAddress.GetLoopback(1));

            this.connectivityReporter = new NetworkConnectivityReporter(this.eventQueue);
            this.connectivityReporter.SetStatus(ConnectivityStatus.Online);

            this.router = new MockLoopbackRouter(this.eventQueue, addressProvider);
            this.dhtFacade = new DhtFacade(this.router, this.eventQueue, this.timeKeeper, addressProvider, this.connectivityReporter, MessageParserTester.DefaultFactory);
            this.router.DhtFacade = this.dhtFacade;

            Assert.AreEqual(this.dhtFacade, this.router.DhtFacade);
            Assert.AreEqual(HashKey.Min, this.router.LocalHashKey);

            this.queueApplicationEvent = i => i.Invoke();

            this.controllerManagerMock = MockRepository.GenerateMock<IControllerManager>();
            this.initializer = new ControllerInitializer(
                this.networkFacade,
                this.dhtFacade,
                this.controllerManagerMock,
                this.eventQueue,
                this.queueApplicationEvent,
                this.connectivityReporter,
                delegate { },
                false);

            Assert.NotNull(this.initializer);
        }

        [Test]
        [ExpectedException("Badumna.Controllers.MissingConstructorException")]
        public void CallToStartControllerWithInvalidTypeThrowsMissingConstructorException()
        {
            this.ResetInitializer();
            this.initializer.StartController<SimpleDistributedControllerInvalid>("scene_name^000");
        }

        [Test]
        public void CallToStartControllerWithValidTypeThrowsNoException()
        {
            this.ResetInitializer();
            this.initializer.StartController<SimpleDistributedController>("scene_name^000");

            Assert.AreEqual(1, this.initializer.GetNumberOfStartedController());
            Assert.AreEqual(0, this.initializer.GetNumberOfControllers());
        }

        [Test]
        [ExpectedException("Badumna.Controllers.TooManyDistributedControllerException")]
        public void TooManyControllersThrowsTooManyDistributedControllerException()
        {
            this.ResetInitializer();

            for (int i = 0; i <= Parameters.MaximumNumberOfDistributedController; i++)
            {
                string name = string.Format("scene_name^id{0}", i);
                this.initializer.StartController<SimpleDistributedController>(name);
                this.eventQueue.RunFor(TimeSpan.FromSeconds(1));
            }
        }

        [Test]
        public void CallToStartControllerMultipleTimesWithTheSameNameIsPeacefullyIgnored()
        {
            this.ResetInitializer();

            this.initializer.StartController<SimpleDistributedController>("scene_name^000");
            this.initializer.StartController<SimpleDistributedController>("scene_name^000");

            Assert.AreEqual(1, this.initializer.GetNumberOfStartedController());
        }

        [Test]
        public void CallToStartControllerWithValidTypeWillTriggerControllerBeCreated()
        {
            this.ResetInitializer();
            this.initializer.StartController<SimpleDistributedController>("scene_name^000");

            Assert.AreEqual(1, this.initializer.GetNumberOfStartedController());
            Assert.AreEqual(0, this.initializer.GetNumberOfControllers());

            this.eventQueue.RunFor(TimeSpan.FromSeconds(10));
            Assert.AreEqual(1, this.initializer.GetNumberOfControllers());
        }

        [Test]
        public void CallToProcessWillBePassedToTheControllerManager()
        {
            this.ResetInitializer();
            this.initializer.StartController<SimpleDistributedController>("scene_name^000");
            this.controllerManagerMock.Expect(f => f.Process()).Repeat.Once();
            this.initializer.Process();
            this.controllerManagerMock.VerifyAllExpectations();
        }

        [Test]
        public void StartedControllerWillKeepBeingRenewed()
        {
            this.ResetInitializer();
            this.initializer.StartController<SimpleDistributedController>("scene_name^000");

            Assert.AreEqual(0, this.initializer.GetNumberOfControllers());
            this.eventQueue.RunFor(TimeSpan.FromSeconds(5));
            Assert.AreEqual(1, this.initializer.GetNumberOfControllers());

            // created, one and only one controller is running.
            
            // wait...
            TimeSpan waitTime = TimeSpan.FromSeconds(5 * Parameters.DistributedControllerInitializerReplicaTTL.TotalSeconds);
            this.eventQueue.RunFor(waitTime);

            Assert.AreEqual(1, this.initializer.GetNumberOfControllers());
        }

        [Test]
        public void StartedControllerWillExpireAfterCallingStop()
        {
            this.ResetInitializer();
            this.dhtFacade.ForgeMethodList();
            this.initializer.StartController<SimpleDistributedController>("scene_name^000");

            Assert.AreEqual(0, this.initializer.GetNumberOfControllers());
            this.eventQueue.RunFor(TimeSpan.FromSeconds(5));
            Assert.AreEqual(1, this.initializer.GetNumberOfControllers());

            // created, one and only one controller is running.

            // wait...
            TimeSpan waitTime = TimeSpan.FromSeconds(5 * Parameters.DistributedControllerInitializerReplicaTTL.TotalSeconds);
            this.eventQueue.RunFor(waitTime);

            Assert.AreEqual(1, this.initializer.GetNumberOfControllers());

            this.initializer.StopController<SimpleDistributedController>("scene_name^000");
            
            this.eventQueue.RunFor(waitTime);

            Assert.AreEqual(0, this.initializer.GetNumberOfControllers());
        }

        private void ResetInitializer()
        {
            this.controllerManagerMock = MockRepository.GenerateMock<IControllerManager>();
            this.initializer = new ControllerInitializer(
                this.networkFacade,
                this.dhtFacade,
                this.controllerManagerMock,
                this.eventQueue,
                this.queueApplicationEvent,
                this.connectivityReporter,
                delegate { },
                false);
        }
    }
}