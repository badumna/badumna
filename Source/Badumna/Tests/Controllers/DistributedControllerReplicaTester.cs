﻿//-----------------------------------------------------------------------
// <copyright file="DistributedControllerReplicaTester.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System.Collections.Generic;
using Badumna.Controllers;
using Badumna.Core;
using Badumna.DataTypes;
using Badumna.DistributedHashTable;
using BadumnaTests.Core;
using NUnit.Framework;

namespace BadumnaTests.Controllers
{
    [DontPerformCoverage]
    [TestFixture]
    public class DistributedControllerReplicaTester : ParseableTestHelper
    {
        public DistributedControllerReplicaTester()
            : base(new ParseableTester())
        {
        }

        internal class ParseableTester : ParseableTester<DistributedControllerReplica>
        {
            public override ICollection<DistributedControllerReplica> CreateExpectedValues()
            {
                ICollection<DistributedControllerReplica> expectedValues = new List<DistributedControllerReplica>();
                BadumnaId id1 = new BadumnaId(new PeerAddress(HashKey.Hash("test1")), 1);
                BadumnaId id2 = new BadumnaId(new PeerAddress(HashKey.Hash("test2")), 2);
                DistributedControllerReplica replica = new DistributedControllerReplica(id1.Address.HashAddress, id2);
                replica.CheckpointData = new byte[16];
                expectedValues.Add(replica);

                return expectedValues;
            }

            public override void AssertAreEqual(DistributedControllerReplica expected, DistributedControllerReplica actual)
            {
                Assert.AreEqual(actual.CheckpointData.Length, expected.CheckpointData.Length);
                Assert.AreEqual(actual.CheckpointData, expected.CheckpointData);
            }
        }
    }
}
