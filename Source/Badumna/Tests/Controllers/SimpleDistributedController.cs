﻿//-----------------------------------------------------------------------
// <copyright file="SimpleDistributedController.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using Badumna.Controllers;
using Badumna.Core;

namespace BadumnaTests.Controllers
{
    [DontPerformCoverage]
    public class SimpleDistributedControllerInvalid : DistributedController
    {
        public SimpleDistributedControllerInvalid()
            : base("name")
        {
        }

        protected override void Checkpoint(System.IO.BinaryWriter writer)
        {
            return;
        }

        protected override void Recover(System.IO.BinaryReader reader)
        {
            return;
        }

        protected override void Recover()
        {
            return;
        }

        protected override void Process(TimeSpan duration)
        {
            return;
        }

        protected override void ReceiveMessage(Badumna.Core.MessageStream message, Badumna.DataTypes.BadumnaId source)
        {
            return;
        }
    }

    [DontPerformCoverage]
    public class SimpleDistributedController : DistributedController
    {
        public SimpleDistributedController(string name)
            : base(name)
        {
        }

        protected override void Checkpoint(System.IO.BinaryWriter writer)
        {
            return;
        }

        protected override void Recover(System.IO.BinaryReader reader)
        {
            return;
        }

        protected override void Recover()
        {
            return;
        }

        protected override void Process(TimeSpan duration)
        {
            return;
        }

        protected override void ReceiveMessage(Badumna.Core.MessageStream message, Badumna.DataTypes.BadumnaId source)
        {
            return;
        }
    }

    [DontPerformCoverage]
    public abstract class DistributedControllerTestHelper : DistributedController
    {
        private int checkpointSize;

        public DistributedControllerTestHelper(string name)
            : base(name)
        {
            this.checkpointSize = 0;
        }

        public int CheckPointSize
        {
            set { this.checkpointSize = value; }
        }

        internal abstract void CheckpointPublicMethod(System.IO.BinaryWriter writer);

        internal abstract void RecoverPublicMethod(System.IO.BinaryReader reader);

        internal abstract void RecoverPublicMethod();

        internal abstract void ProcessPublicMethod(TimeSpan duration);

        internal abstract void ReceiveMessagePublicMethod(Badumna.Core.MessageStream message, Badumna.DataTypes.BadumnaId source);

        internal abstract void WakePublicMethod();

        internal abstract void SleepPublicMethod();

        protected override void Checkpoint(System.IO.BinaryWriter writer)
        {
            if (this.checkpointSize > 0)
            {
                byte[] data = new byte[this.checkpointSize];
                writer.Write(data);
            }

            this.CheckpointPublicMethod(writer);
            return;
        }

        protected override void Recover(System.IO.BinaryReader reader)
        {
            this.RecoverPublicMethod(reader);
            return;
        }

        protected override void Recover()
        {
            this.RecoverPublicMethod();
            return;
        }

        protected override void Process(TimeSpan duration)
        {
            this.ProcessPublicMethod(duration);
            return;
        }

        protected override void ReceiveMessage(Badumna.Core.MessageStream message, Badumna.DataTypes.BadumnaId source)
        {
            this.ReceiveMessagePublicMethod(message, source);
            return;
        }

        protected override void Wake()
        {
            this.WakePublicMethod();
            return;
        }

        protected override void Sleep()
        {
            this.SleepPublicMethod();
            return;
        }
    }
}
