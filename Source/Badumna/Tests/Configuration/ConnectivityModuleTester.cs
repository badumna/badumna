﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.XPath;
using Badumna;
using Badumna.Core;
using Badumna.Transport;
using NUnit.Framework;

namespace BadumnaTests.Configuration
{
    internal class ConnectivityModuleTester : OptionModuleTests<ConnectivityModule>
    {
        private readonly string ApplicationName = "test";

        [Test]
        public void ConfigureForLanSetsCorrectValues()
        {
            ConnectivityModule module = new ConnectivityModule(null);
            module.IsBroadcastEnabled = false;
            module.StunServers.Add("server1.example.com");
            module.StunServers.Add("server2.example.com");
            module.IsPortForwardingEnabled = true;

            Assert.AreNotEqual(0, module.StunServers.Count);
            Assert.AreNotEqual(false, module.IsPortForwardingEnabled);
            Assert.AreNotEqual(true, module.IsBroadcastEnabled);

            module.ConfigureForLan();

            Assert.AreEqual(0, module.StunServers.Count);
            Assert.AreEqual(false, module.IsPortForwardingEnabled);
            Assert.AreEqual(true, module.IsBroadcastEnabled);

            // TODO: Also check that other properties have not been modified.
        }

        [Test]
        public void ConfigureForSpecificPortSetsCorrectValues()
        {
            ConnectivityModule module = new ConnectivityModule(null);
            module.StartPortRange = 0;
            module.EndPortRange = 0;
            module.MaxPortsToTry = 0;
            module.ConfigureForSpecificPort(42);
            Assert.AreEqual(42, module.StartPortRange);
            Assert.AreEqual(42, module.EndPortRange);
            Assert.AreEqual(1, module.MaxPortsToTry);
        }

        [Test]
        public void TestXmlDeserializationOfTwoStunServersAndAPort()
        {
            string xml =
                @"<Module Name=""Connectivity"" Enabled=""true"">
                     <Stun>
                       <Server>an.example.server</Server>
                       <Server>another.example.server</Server>
                     </Stun>
                     <Port>99</Port>
                  </Module>";

            ConnectivityModule module = this.MakeTea(xml);

            Assert.AreEqual(2, module.StunServers.Count);
            Assert.IsTrue(module.StunServers.Contains("an.example.server"));
            Assert.IsTrue(module.StunServers.Contains("another.example.server"));

            Assert.AreEqual(99, module.StartPortRange);
            Assert.AreEqual(99, module.EndPortRange);
            Assert.AreEqual(1, module.MaxPortsToTry);
        }

        [Test]
        public void NoStunServersImpliesPerformStunIsFalse()
        {
            ConnectivityModule module = new ConnectivityModule(null);
            module.StunServers.Clear();
            Assert.AreEqual(0, module.StunServers.Count);
            Assert.IsFalse(module.PerformStun);
        }

        [Test]
        public void SomeStunServersImpliesPerformStunIsTrue()
        {
            ConnectivityModule module = new ConnectivityModule(null);
            module.StunServers.Add("stun1.example.com");
            Assert.Greater(module.StunServers.Count, 0);
            Assert.IsTrue(module.PerformStun);
        }

        [TestCase(""), TestCase("not a number")]
        public void BadPortValueInXmlThrowsConfigurationException(string invalidAddress)
        {
            string xml = @"<Module Name=""Connectivity"" Enabled=""true""><Port>" + invalidAddress + "</Port></Module>";
            Assert.Throws(
                typeof(ConfigurationException),
                delegate
                {
                    this.MakeTea(xml);
                });
        }

        [TestCase(false, false, true)]
        [TestCase(false, true, false)]
        [TestCase(true, false, false)]
        [TestCase(true, true, false)]
        public void LackOfDiscoveryMechanismThrowsConfigurationException(bool isBroadcastEnabled, bool isSeedPeerDefined, bool isExceptionExpected)
        {
            ConnectivityModule module = new ConnectivityModule(null);
            module.ApplicationName = this.ApplicationName;
            module.IsBroadcastEnabled = isBroadcastEnabled;
            module.SeedPeers.Clear();
            if (isSeedPeerDefined)
            {
                module.SeedPeers.Add("seedpeer.example.com");
            }

            TestDelegate validate = delegate { ((IOptionModule)module).Validate(); };

            if (isExceptionExpected)
            {
                Assert.Throws(typeof(ConfigurationException), validate);
            }
            else
            {
                Assert.DoesNotThrow(validate);
            }
        }

        [Test]
        public void SpecificPortPassesValidation()
        {
            ConnectivityModule module = new ConnectivityModule(null);
            module.ApplicationName = this.ApplicationName;
            module.ConfigureForSpecificPort(42);
            Assert.DoesNotThrow(delegate { ((IOptionModule)module).Validate(); });
        }

        [TestCase(-1)]
        [TestCase(65536)]
        public void InvalidPortThrowsConfigurationException(int port)
        {
            ConnectivityModule module = new ConnectivityModule(null);
            module.ApplicationName = this.ApplicationName;
            module.StartPortRange = port;
            TestDelegate validate = delegate { ((IOptionModule)module).Validate(); };
            Assert.Throws(typeof(ConfigurationException), validate);

            module = new ConnectivityModule(null);
            module.ApplicationName = this.ApplicationName;
            module.EndPortRange = port;
            Assert.Throws(typeof(ConfigurationException), validate);

            module = new ConnectivityModule(null);
            module.ApplicationName = this.ApplicationName;
            module.IsBroadcastEnabled = true;
            module.BroadcastPort = port;
            Assert.Throws(typeof(ConfigurationException), validate);

            module = new ConnectivityModule(null);
            module.ApplicationName = this.ApplicationName;
            module.IsBroadcastEnabled = false;
            module.SeedPeers.Add("seedpeer.example.com:21252");  // Dummy seed peer to avoid "no discovery mechanism" error
            module.BroadcastPort = port;
            Assert.DoesNotThrow(validate);
        }

        [Test]
        public void BadPortRangeOrderThrowsConfigurationException()
        {
            ConnectivityModule module = new ConnectivityModule(null);
            module.ApplicationName = this.ApplicationName;
            module.StartPortRange = 10;
            module.EndPortRange = 5;
            Assert.Throws(typeof(ConfigurationException), delegate { ((IOptionModule)module).Validate(); });
        }

        [Test]
        public void ThrowConfigurationExceptionWhenApplicationNameIsInvalid()
        {
            ConnectivityModule module = new ConnectivityModule(null);
            Assert.Throws(typeof(ConfigurationException), delegate { ((IOptionModule)module).Validate(); });
        }

        public static void AssertEqual(ConnectivityModule left, ConnectivityModule right)
        {
            Assert.AreEqual(left.BandwidthLimit, right.BandwidthLimit);
            Assert.AreEqual(left.BroadcastPort, right.BroadcastPort);
            Assert.AreEqual(left.EndPortRange, right.EndPortRange);
            Assert.AreEqual(left.IsBroadcastEnabled, right.IsBroadcastEnabled);
            Assert.AreEqual(left.IsHostedService, right.IsHostedService);
            Assert.AreEqual(left.IsPortForwardingEnabled, right.IsPortForwardingEnabled);
            Assert.AreEqual(left.IsTransportLimiterEnabled, right.IsTransportLimiterEnabled);
            Assert.AreEqual(left.MaxPortsToTry, right.MaxPortsToTry);
            Assert.AreEqual(left.PerformStun, right.PerformStun);
            OptionsTests.AssertEqual(left.SeedPeers, right.SeedPeers);
            Assert.AreEqual(left.StartPortRange, right.StartPortRange);
            OptionsTests.AssertEqual(left.StunServers, right.StunServers);
            Assert.AreEqual(left.TunnelMode, right.TunnelMode);
            OptionsTests.AssertEqual(left.TunnelUris, right.TunnelUris);
            Assert.AreEqual(left.ApplicationName, right.ApplicationName);
        }

        protected override ConnectivityModule MakeTea(IXPathNavigable xml)
        {
            return new ConnectivityModule(xml);
        }

        protected override void AssertDefaults(ConnectivityModule module)
        {
            Assert.AreEqual("", module.ApplicationName);

            Assert.AreEqual(Parameters.DefaultStartPort, module.StartPortRange);
            Assert.AreEqual(Parameters.DefaultEndPort, module.EndPortRange);
            Assert.AreEqual(5, module.MaxPortsToTry);

            Assert.AreEqual(true, module.IsBroadcastEnabled);
            Assert.AreEqual(Parameters.DefaultBroadcastPort, module.BroadcastPort);

            Assert.AreEqual(true, module.IsPortForwardingEnabled);

            Assert.AreEqual(3, module.StunServers.Count);
            Assert.IsTrue(module.StunServers.Contains("stun1.noc.ams-ix.net"));
            Assert.IsTrue(module.StunServers.Contains("stun.voipbuster.com"));
            Assert.IsTrue(module.StunServers.Contains("stun.voxgratia.org"));

            Assert.AreEqual(0, module.SeedPeers.Count);

            Assert.AreEqual(TunnelMode.Off, module.TunnelMode);
            Assert.AreEqual(0, module.TunnelUris.Count);

            Assert.AreEqual(false, module.IsTransportLimiterEnabled);
            Assert.AreEqual(0, module.BandwidthLimit);

            Assert.AreEqual(false, module.IsHostedService);
        }

        public static void SetNonDefaults(ConnectivityModule module)
        {
            module.ApplicationName = "com.example.epic-elevator-conflict";
            module.StartPortRange = 42;
            module.EndPortRange = 65123;
            module.MaxPortsToTry = 71;
            module.IsBroadcastEnabled = false;
            module.BroadcastPort = 99;
            module.IsPortForwardingEnabled = false;
            module.StunServers.Clear();
            module.SeedPeers.Add("epic-elevator-conflict.example.com");
            module.TunnelMode = TunnelMode.Auto;
            module.TunnelUris.Add("http://epic-elevator-conflict.example.com/");
            module.EnableTransportLimiter("i_understand_this_is_for_testing_only");
            module.BandwidthLimit = 54321;
            module.IsHostedService = true;
        }
    }
}
