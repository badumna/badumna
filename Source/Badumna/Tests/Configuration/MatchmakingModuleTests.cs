﻿//------------------------------------------------------------------------------
// <copyright file="MatchmakingModuleTests.cs" company="National ICT Australia Limited">
//     Copyright (c) 2013 National ICT Australia Limited. All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

namespace BadumnaTests.Configuration
{
    using System.Xml.XPath;
    using Badumna;
    using NUnit.Framework;

    internal class MatchmakingModuleTests : OptionModuleTests<MatchmakingModule>
    {
        public static void AssertEqual(MatchmakingModule left, MatchmakingModule right)
        {
            Assert.AreEqual(left.ServerAddress, right.ServerAddress);
            Assert.AreEqual(left.ActiveMatchLimit, right.ActiveMatchLimit);
        }

        public static void SetNonDefaults(MatchmakingModule module)
        {
            module.ServerAddress = "matchmaking.example.com:1234";
            module.ActiveMatchLimit = 42;
        }

        protected override MatchmakingModule MakeTea(IXPathNavigable xml)
        {
            return new MatchmakingModule(xml);
        }

        protected override void AssertDefaults(MatchmakingModule module)
        {
            Assert.AreEqual("", module.ServerAddress);
            Assert.AreEqual(0, module.ActiveMatchLimit);
        }
    }
}
