﻿//-----------------------------------------------------------------------
// <copyright file="OptionModuleTests.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2011 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System.Xml;
using System.Xml.XPath;
using Badumna;
using Badumna.Core;
using NUnit.Framework;

namespace BadumnaTests.Configuration
{
    [TestFixture]
    [DontPerformCoverage]
    internal abstract class OptionModuleTests<T> where T : IOptionModule
    {
        [Test]
        public void VerifyDefaultValues()
        {
            this.AssertDefaults(this.MakeTea((IXPathNavigable)null));
        }

        [Test]
        public void EmptyXmlGivesDefaults()
        {
            this.AssertDefaults(this.MakeTea("<Module></Module>"));
        }

        protected abstract T MakeTea(IXPathNavigable xml);

        protected T MakeTea(string xml)
        {
            XmlDocument document = new XmlDocument();
            document.LoadXml(xml);
            return this.MakeTea(document.FirstChild);
        }

        protected abstract void AssertDefaults(T module);
    }
}
