﻿//-----------------------------------------------------------------------
// <copyright file="ArbitrationModuleTests.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BadumnaTests.Configuration
{
    using System.Text;
    using System.Xml;
    using System.Xml.XPath;
    using Badumna;
    using Badumna.Arbitration;
    using Badumna.Core;
    using NUnit.Framework;

    internal class ArbitrationModuleTests : OptionModuleTests<ArbitrationModule>
    {
        public static void AssertEqual(ArbitrationModule left, ArbitrationModule right)
        {
            Assert.AreEqual(left.Servers.Count, right.Servers.Count);
            for (int i = 0; i < left.Servers.Count; i++)
            {
                Assert.AreEqual(left.Servers[i].Name, right.Servers[i].Name);
                Assert.AreEqual(left.Servers[i].Address, right.Servers[i].Address);
            }
        }

        public static void SetNonDefaults(ArbitrationModule module)
        {
            module.Servers.Add(new ArbitrationServerDetails("conflict-a", "conflict-arbitration-a.example.com:12345"));
            module.Servers.Add(new ArbitrationServerDetails("conflict-b", "conflict-arbitration-b.example.com:12345"));
        }

        [Test]
        public void ServerInfoParsedCorrectly()
        {
            StringBuilder builder = new StringBuilder(@"<Module Name=""Arbitration"">");
            builder.AppendLine(@"<Server><Name>Movement</Name><UseServiceDiscovery>enabled</UseServiceDiscovery></Server>");
            builder.AppendLine(@"</Module>");
            IArbitrationModule module = this.MakeTea(builder.ToString());
            Assert.AreEqual(1, module.Servers.Count);
            Assert.AreEqual(true, module.RequiresDistributedLookup);
            Assert.AreEqual("movement", module.Servers[0].Name.ToLower());
        }

        [Test]
        public void MultipleServersParsedCorrectly()
        {
            StringBuilder builder = new StringBuilder(@"<Module Name=""Arbitration"">");
            builder.AppendLine(@"<Server><Name>foo</Name><ServerAddress>123.123.123.123:456</ServerAddress></Server>");
            builder.AppendLine(@"<Server><Name>bar</Name><ServerAddress>234.234.234.234:567</ServerAddress></Server>");
            builder.AppendLine(@"</Module>");
            ArbitrationModule module = this.MakeTea(builder.ToString());
            ((IOptionModule)module).Validate();

            Assert.AreEqual(2, module.Servers.Count);
            foreach (ArbitrationServerDetails details in module.Servers)
            {
                Assert.IsFalse(details.UseDistributedLookup);
            }

            Assert.IsFalse(((IArbitrationModule)module).RequiresDistributedLookup);

            Assert.AreEqual(module.Servers[0].Name, "foo");
            Assert.AreEqual(module.Servers[1].Name, "bar");
            Assert.AreEqual(module.Servers[0].PeerAddress, PeerAddress.FromString("Unknown|123.123.123.123:456"));
            Assert.AreEqual(module.Servers[1].PeerAddress, PeerAddress.FromString("Unknown|234.234.234.234:567"));
        }

        [Test]
        public void GetServerDetailsFindsDetailsByName()
        {
            ArbitrationModule module = new ArbitrationModule(null);
            module.Servers.Add(new ArbitrationServerDetails("bar", "234.234.234.234:567"));
            module.Servers.Add(new ArbitrationServerDetails("foo", "123.123.123.123:456"));
            ((IOptionModule)module).Validate();
            Assert.AreEqual(((IArbitrationModule)module).GetServerDetails("foo").PeerAddress, PeerAddress.FromString("Unknown|123.123.123.123:456"));
        }

        [Test]
        public void GetServerDetailsReturnsNullWhenNameIsNotPresent()
        {
            ArbitrationModule module = new ArbitrationModule(null);
            module.Servers.Add(new ArbitrationServerDetails("bar", "234.234.234.234:567"));
            module.Servers.Add(new ArbitrationServerDetails("foo", "123.123.123.123:456"));
            ((IOptionModule)module).Validate();
            Assert.AreEqual(((IArbitrationModule)module).GetServerDetails("bang"), null);
        }

        [Test]
        public void HasServerReturnsTrueWhenServerIsPresent()
        {
            ArbitrationModule module = new ArbitrationModule(null);
            module.Servers.Add(new ArbitrationServerDetails("bar", "234.234.234.234:567"));
            module.Servers.Add(new ArbitrationServerDetails("foo", "123.123.123.123:456"));
            Assert.IsTrue(((IArbitrationModule)module).HasServer("foo"));
        }

        [Test]
        public void HasServerReturnsFalseWhenServerIsNotPresent()
        {
            ArbitrationModule module = new ArbitrationModule(null);
            module.Servers.Add(new ArbitrationServerDetails("bar", "234.234.234.234:567"));
            module.Servers.Add(new ArbitrationServerDetails("foo", "123.123.123.123:456"));
            Assert.IsFalse(((IArbitrationModule)module).HasServer("bang"));
        }

        [Test]
        public void ServerNameSpecifiedProgrammaticallyIsTreatedCaseInsensitively()
        {
            ArbitrationModule module = new ArbitrationModule(null);
            module.Servers.Add(new ArbitrationServerDetails("FuNkY", "234.234.234.234:567"));
            Assert.IsTrue(((IArbitrationModule)module).HasServer("funky"));
        }

        [Test]
        public void ServerNameSpecifiedInXmlIsTreatedCaseInsensitively()
        {
            string xml = @"<Module Name=""Arbitration""><Server><Name>FuNkY</Name></Server></Module>";
            ArbitrationModule module = this.MakeTea(xml);
            Assert.IsTrue(((IArbitrationModule)module).HasServer("funky"));
        }

        protected override ArbitrationModule MakeTea(IXPathNavigable xml)
        {
            return new ArbitrationModule(xml);
        }

        protected override void AssertDefaults(ArbitrationModule module)
        {
            Assert.AreEqual(0, module.Servers.Count);
        }
    }
}
