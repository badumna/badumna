﻿//-----------------------------------------------------------------------
// <copyright file="LoggerModuleTests.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2011 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Text;
using System.Xml;
using System.Xml.XPath;
using Badumna;
using Badumna.Utilities.Logging;
using NUnit.Framework;

namespace BadumnaTests.Configuration
{
    internal class LoggerModuleTests : OptionModuleTests<LoggerModule>
    {
        public static void AssertEqual(LoggerModule left, LoggerModule right)
        {
            Assert.AreEqual(left.IsAssertUiSuppressed, right.IsAssertUiSuppressed);
            Assert.AreEqual(left.IsDotNetTraceForwardingEnabled, right.IsDotNetTraceForwardingEnabled);

            Assert.AreEqual(left.LogLevel, right.LogLevel);
            Assert.AreEqual(left.LogTimestamp, right.LogTimestamp);
            Assert.AreEqual(left.IncludeTags, right.IncludeTags);
            Assert.AreEqual(left.ExcludeTags, right.ExcludeTags);

            Assert.AreEqual(left.LoggerType, right.LoggerType);

            if (left.LoggerConfig == null)
            {
                Assert.AreEqual(left.LoggerConfig, right.LoggerConfig);
            }
            else
            {
                // NQR but close enough.  The XML serializer will pretty print the logger config,
                // so here we strip all whitespace before comparing.
                string leftConfig = left.LoggerConfig.Replace("\r", string.Empty).Replace("\n", string.Empty).Replace(" ", string.Empty);
                string rightConfig = right.LoggerConfig.Replace("\r", string.Empty).Replace("\n", string.Empty).Replace(" ", string.Empty);
                Assert.AreEqual(leftConfig, rightConfig);
            }
        }

        public static void SetNonDefaults(LoggerModule module)
        {
            module.LoggerType = LoggerType.Log4Net;
            module.LoggerConfig = "<Logger Type=\"Log4NetLogger\"><Log4Net>Cheesecake is delicious</Log4Net></Logger>";
            module.IsAssertUiSuppressed = true;
            module.IsDotNetTraceForwardingEnabled = true;
            module.IncludeTags = LogTag.Entity | LogTag.Event;
            module.ExcludeTags = LogTag.Periodic;
            module.LogLevel = LogLevel.Information;
            module.LogTimestamp = true;
        }

        [Test]
        public void AllLoggerTypesArePreserved()
        {
            foreach (LoggerType type in Enum.GetValues(typeof(LoggerType)))
            {
                var options = new Options();
                options.Logger.LoggerType = type;

                var xmlStringBuilder = new StringBuilder();
                options.Save(XmlWriter.Create(xmlStringBuilder));

                var document = new XmlDocument();
                document.LoadXml(xmlStringBuilder.ToString());
                var restoredOptions = new Options(document);

                LoggerModuleTests.AssertEqual(options.Logger, restoredOptions.Logger);
            }
        }
    
        protected override LoggerModule MakeTea(IXPathNavigable xml)
        {
            return new LoggerModule(xml);
        }

        protected override void AssertDefaults(LoggerModule module)
        {
            Assert.AreEqual(LoggerType.DotNetTrace, module.LoggerType);
            Assert.AreEqual(null, module.LoggerConfig);
            Assert.AreEqual(false, module.IsAssertUiSuppressed);
            Assert.AreEqual(false, module.IsDotNetTraceForwardingEnabled);
            Assert.AreEqual(LogTag.All, module.IncludeTags);
            Assert.AreEqual(LogTag.None, module.ExcludeTags);
            Assert.AreEqual(LogLevel.Warning, module.LogLevel);
            Assert.AreEqual(true, module.LogTimestamp);
        }
    }
}
