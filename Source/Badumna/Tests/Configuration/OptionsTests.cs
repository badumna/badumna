﻿//-----------------------------------------------------------------------
// <copyright file="OptionsTests.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2011 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

using Badumna;

using NUnit.Framework;

namespace BadumnaTests.Configuration
{
    /// <summary>
    /// Tests for the Badumna.Options class.
    /// </summary>
    [TestFixture]
    public class OptionsTests
    {
        public static void AssertEqual<T>(IList<T> left, IList<T> right)
        {
            if (left == right)
            {
                return;
            }

            Assert.NotNull(left);
            Assert.NotNull(right);

            Assert.AreEqual(left.Count, right.Count);
            for (int i = 0; i < left.Count; i++)
            {
                Assert.AreEqual(left[i], right[i]);
            }
        }

        public static void AssertEqual(Options left, Options right)
        {
            ArbitrationModuleTests.AssertEqual(left.Arbitration, right.Arbitration);
            ConnectivityModuleTester.AssertEqual(left.Connectivity, right.Connectivity);
            LoggerModuleTests.AssertEqual(left.Logger, right.Logger);
            OverloadModuleTester.AssertEqual(left.Overload, right.Overload);
            MatchmakingModuleTests.AssertEqual(left.Matchmaking, right.Matchmaking);
        }

        [Test]
        public void DefaultOptionsValidateSuccessfully()
        {
            Assert.DoesNotThrow(delegate
            {
                var option = new Options();
                option.Connectivity.ApplicationName = "test";
                option.Validate();
            });
        }

        [Test]
        public void DefaultOptionsSerializeDeserializeSuccessful()
        {
            this.AssertSerializeDeserializeSuccessful(new Options());
        }

        [Test]
        public void NonDefaultOptionsSerializeDeserializeSuccessful()
        {
            this.AssertSerializeDeserializeSuccessful(this.MakeNonDefaultOptions());
        }

        [Test]
        public void DefaultOptionsCopyCorrectly()
        {
            this.AssertCopySuccessful(new Options());
        }

        [Test]
        public void NonDefaultOptionsCopyCorrectly()
        {
            this.AssertCopySuccessful(this.MakeNonDefaultOptions());
        }

        [Test]
        public void LegacyNetworkConfigXmlLoadsCorrectly()
        {
            string xml = 
@"<?xml version=""1.0"" encoding=""us-ascii""?>
<Modules>
    <Module Name=""Connectivity"">
        <PortRange MaxPortsToTry=""3"">21300,21399</PortRange>
        <Broadcast Enabled=""true"">21250</Broadcast>
        <Initializer type=""SeedPeer"">seedpeer.example.com:21251</Initializer>
        <PortForwarding Enabled=""true"" />
        <Stun Enabled=""true"">
            <Server>stun1.noc.ams-ix.net</Server>
            <Server>stun.voipbuster.com</Server>
            <Server>stun.voxgratia.org</Server>
        </Stun>
    </Module>
  <Module Name=""Arbitration"" Enabled=""true"">
    <Server>
      <Name>combatzonea</Name>
      <UseServiceDiscovery>Enabled</UseServiceDiscovery>
    </Server>
    <Server>
      <Name>combatzoneb</Name>
      <UseServiceDiscovery>Enabled</UseServiceDiscovery>
    </Server>
  </Module>
</Modules>";

            XmlDocument document = new XmlDocument();
            document.LoadXml(xml);
            Options options = new Options(document);

            Assert.AreEqual(3, options.Connectivity.MaxPortsToTry);
            Assert.AreEqual(21300, options.Connectivity.StartPortRange);
            Assert.AreEqual(21399, options.Connectivity.EndPortRange);
            Assert.AreEqual(true, options.Connectivity.IsBroadcastEnabled);
            Assert.AreEqual(21250, options.Connectivity.BroadcastPort);
            Assert.AreEqual(1, options.Connectivity.SeedPeers.Count);
            Assert.AreEqual("seedpeer.example.com:21251", options.Connectivity.SeedPeers[0]);
            Assert.AreEqual(true, options.Connectivity.IsPortForwardingEnabled);
            Assert.AreEqual(3, options.Connectivity.StunServers.Count);
            Assert.AreEqual("stun1.noc.ams-ix.net", options.Connectivity.StunServers[0]);
            Assert.AreEqual("stun.voipbuster.com", options.Connectivity.StunServers[1]);
            Assert.AreEqual("stun.voxgratia.org", options.Connectivity.StunServers[2]);

            Assert.AreEqual(2, options.Arbitration.Servers.Count);
            Assert.AreEqual("combatzonea", options.Arbitration.Servers[0].Name);
            Assert.AreEqual(true, options.Arbitration.Servers[0].UseDistributedLookup);
            Assert.AreEqual("combatzoneb", options.Arbitration.Servers[1].Name);
            Assert.AreEqual(true, options.Arbitration.Servers[1].UseDistributedLookup);
        }

        private void AssertSerializeDeserializeSuccessful(Options options)
        {
            StringBuilder xmlStringBuilder = new StringBuilder();
            options.Save(XmlWriter.Create(xmlStringBuilder));

            XmlDocument document = new XmlDocument();
            document.LoadXml(xmlStringBuilder.ToString());
            Options restoredOptions = new Options(document);

            OptionsTests.AssertEqual(options, restoredOptions);
        }

        private Options MakeNonDefaultOptions()
        {
            Options options = new Options();
            ArbitrationModuleTests.SetNonDefaults(options.Arbitration);
            ConnectivityModuleTester.SetNonDefaults(options.Connectivity);
            LoggerModuleTests.SetNonDefaults(options.Logger);
            OverloadModuleTester.SetNonDefaults(options.Overload);
            MatchmakingModuleTests.SetNonDefaults(options.Matchmaking);
            options.CloudIdentifier = "cloud-id";
            return options;
        }

        private void AssertCopySuccessful(Options options)
        {
            var optionsCopy = new Options(options);
            OptionsTests.AssertEqual(options, optionsCopy);
            Assert.AreEqual(options.CloudIdentifier, optionsCopy.CloudIdentifier);  // Cloud id is not serialized/deserialized because it's internal; but should be copied.
        }
    }
}
