﻿//-----------------------------------------------------------------------
// <copyright file="OverloadModuleTester.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.XPath;
using Badumna;
using Badumna.Core;
using Badumna.Overload;
using NUnit.Framework;

namespace BadumnaTests.Configuration
{
    internal class OverloadModuleTester : OptionModuleTests<OverloadModule>
    {
        public static void AssertEqual(OverloadModule left, OverloadModule right)
        {
            Assert.AreEqual(left.IsDistributedLookupUsed, right.IsDistributedLookupUsed);
            Assert.AreEqual(left.IsServer, right.IsServer);
            Assert.AreEqual(left.IsForced, right.IsForced);
            Assert.AreEqual(left.IsClientEnabled, right.IsClientEnabled);
            Assert.AreEqual(left.ServerAddress, right.ServerAddress);
        }

        public static void SetNonDefaults(OverloadModule module)
        {
            module.ServerAddress = "epic-overload.example.com:42";
            module.IsServer = true;
            module.IsClientEnabled = true;
            module.EnableForcedOverload("i_understand_this_is_for_testing_only");
        }

        [Test]
        public void EnableOverloadDeserializesCorrectly()
        {
            // test enabled
            string configString = "<Module Name=\"Overload\" Enabled=\"true\"><EnableOverload>Enabled</EnableOverload></Module>";
            OverloadModule config = this.MakeTea(configString);
            Assert.IsTrue(config.IsClientEnabled);

            // test disabled
            configString = "<Module Name=\"Overload\" Enabled=\"true\"><EnableOverload>Disabled</EnableOverload></Module>";
            config = this.MakeTea(configString);
            Assert.IsFalse(config.IsClientEnabled);

            // test case insensitive
            configString = "<Module Name=\"Overload\" Enabled=\"true\"><EnableOverload>EnAbLeD</EnableOverload></Module>";
            config = this.MakeTea(configString);
            Assert.IsTrue(config.IsClientEnabled);
        }

        [Test]
        public void IsDistributedLookupUsedPropertySetCorrectlyAfterDeserialization()
        {
            // test enabled
            string configString = "<Module Name=\"Overload\" Enabled=\"true\"></Module>";
            OverloadModule config = this.MakeTea(configString);
            Assert.IsTrue(config.IsDistributedLookupUsed);

            // test disabled
            configString = "<Module Name=\"Overload\" Enabled=\"true\"><OverloadPeer>222.222.222.222:12345</OverloadPeer></Module>";
            config = this.MakeTea(configString);
            Assert.IsFalse(config.IsDistributedLookupUsed);
        }

        [TestCase("222.222.222.222:12345")]
        [TestCase("badumna.com:12345")]
        public void ValidAddressesAreDeserializedAndResolvedSuccessfully(string addressString)
        {
            string configString = "<Module Name=\"Overload\" Enabled=\"true\"><OverloadPeer>" + addressString + "</OverloadPeer></Module>";
            OverloadModule config = this.MakeTea(configString);
            ((IOptionModule)config).Validate();
            Assert.IsTrue(config.OverloadPeerAddress.IsValid);
        }

        [Test]
        public void AcceptOverloadDeserializesCorrectly()
        {
            // test enabled
            string configString = "<Module Name=\"Overload\" Enabled=\"true\"><AcceptOverload>Enabled</AcceptOverload></Module>";
            OverloadModule config = this.MakeTea(configString);
            Assert.IsTrue(config.IsServer);

            // test disabled
            configString = "<Module Name=\"Overload\" Enabled=\"true\"><AcceptOverload>Disabled</AcceptOverload></Module>";
            config = this.MakeTea(configString);
            Assert.IsFalse(config.IsServer);

            // test case insensitive
            configString = "<Module Name=\"Overload\" Enabled=\"true\"><AcceptOverload>EnAbLeD</AcceptOverload></Module>";
            config = this.MakeTea(configString);
            Assert.IsTrue(config.IsServer);
        }

        [Test]
        public void AcceptOverloadAndUseOverloadBothTrueThrowsExceptionOnValidate()
        {
            OverloadModule module = new OverloadModule(null);
            module.IsServer = true;
            module.IsClientEnabled = true;
            Assert.Throws(
                typeof(ConfigurationException),
                delegate
                {
                    ((IOptionModule)module).Validate();
                });
        }

        [Test]
        public void MaximumOutboundTrafficBytesSentWithNegativeValueThrowsExceptionOnValidate()
        {
            OverloadModule module = new OverloadModule(null);
            module.OutboundTrafficLimitBytesPerSecond = -10;
            Assert.Throws(
                typeof(ConfigurationException),
                delegate
                {
                    ((IOptionModule)module).Validate();
                });
        }

        [Test]
        public void MaximumOutboundTrafficDeserializedCorrectly()
        {
            // test enabled
            string configString = "<Module Name=\"Overload\" Enabled=\"true\"><MaximumOutboundTrafficBytesPerSecond>1000</MaximumOutboundTrafficBytesPerSecond></Module>";
            OverloadModule config = this.MakeTea(configString);
            Assert.AreEqual(config.OutboundTrafficLimitBytesPerSecond, 1000);
        }

        protected override OverloadModule MakeTea(IXPathNavigable xml)
        {
            return new OverloadModule(xml);
        }

        protected override void AssertDefaults(OverloadModule module)
        {
            Assert.IsNull(module.ServerAddress);
            Assert.IsFalse(module.IsServer);
            Assert.IsFalse(module.IsClientEnabled);
            Assert.IsFalse(module.IsForced);
        }
    }
}
