﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using BadumnaTests.Core;
using Badumna.Utilities;
using Badumna.Core;

namespace BadumnaTests.Utilities
{
    [TestFixture]
    [DontPerformCoverage]
    public class CyclicalIDTester : BadumnaTests.Core.ParseableTestHelper
    {
        class ParseableTester : BadumnaTests.Core.ParseableTester<CyclicalID.UShortID>
        {
            public override ICollection<CyclicalID.UShortID> CreateExpectedValues()
            {
                ICollection<CyclicalID.UShortID> expectedValues = new List<CyclicalID.UShortID>();

                CyclicalID.UShortID id = new CyclicalID.UShortID();

                expectedValues.Add(id);
                expectedValues.Add(new CyclicalID.UShortID(id.MaxValue));
                expectedValues.Add(new CyclicalID.UShortID((ushort)(id.MaxValue / 2)));

                return expectedValues;
            }
        }

        public CyclicalIDTester()
            : base(new ParseableTester())
        {
        }

        [Test]
        public void ConstructionTest()
        {
            CyclicalID.UShortID id = new CyclicalID.UShortID();

            Assert.AreEqual(ushort.MaxValue, id.MaxValue, "MaxValue is incorrect");
            Assert.AreEqual(0, id.Value, "Default constructor didn't initialise ID to 0");

            id = new CyclicalID.UShortID(42);
            Assert.AreEqual(42, id.Value, "Constructor with initial value failed");
        }


        [Test]
        public void OverflowTest()
        {
            CyclicalID.UShortID id = new CyclicalID.UShortID(ushort.MaxValue);
            id.Increment();
            Assert.AreEqual(0, id.Value, "Increment operation failed");
        }

        private void AssertConsistent(CyclicalID.UShortID left, CyclicalID.UShortID right)
        {
            int comp = left.CompareTo(right);

            Assert.IsTrue(comp == -right.CompareTo(left), "CompareTo inconsistent when parameters exchanged");

            if (comp == 0)
            {
                Assert.IsTrue(left.Equals(right), "CompareTo inconsistent with operators (Equals)");
                Assert.IsFalse(left < right, "CompareTo inconsistent with operators (<)");
                Assert.IsFalse(left > right, "CompareTo inconsistent with operators (>)");
                Assert.IsTrue(left <= right, "CompareTo inconsistent with operators (<=)");
                Assert.IsTrue(left >= right, "CompareTo inconsistent with operators (>=)");
            }
            else if (comp < 0)
            {
                Assert.IsFalse(left.Equals(right), "CompareTo inconsistent with operators (Equals)");
                Assert.IsTrue(left < right, "CompareTo inconsistent with operators (<)");
                Assert.IsFalse(left > right, "CompareTo inconsistent with operators (>)");
                Assert.IsTrue(left <= right, "CompareTo inconsistent with operators (<=)");
                Assert.IsFalse(left >= right, "CompareTo inconsistent with operators (>=)");
            }
            else
            {
                Assert.IsFalse(left.Equals(right), "CompareTo inconsistent with operators (Equals)");
                Assert.IsFalse(left < right, "CompareTo inconsistent with operators (<)");
                Assert.IsTrue(left > right, "CompareTo inconsistent with operators (>)");
                Assert.IsFalse(left <= right, "CompareTo inconsistent with operators (<=)");
                Assert.IsTrue(left >= right, "CompareTo inconsistent with operators (>=)");
            }

        }


        [Test]
        public void ConsistencyTest()
        {
            CyclicalID.UShortID id1 = new CyclicalID.UShortID();
            CyclicalID.UShortID id2 = new CyclicalID.UShortID();

            AssertConsistent(id1, id2);
            Assert.IsTrue(id1.Equals(id2), "id1 and id2 expected to be equal");
            id2.Increment();
            AssertConsistent(id1, id2);
            Assert.IsTrue(id2 > id1, "id2 expected to be greater than id1");

            uint half = ((uint)id1.MaxValue + 1) / 2;
            id2 = new CyclicalID.UShortID((ushort)(half - 1));
            AssertConsistent(id1, id2);
            AssertConsistent(id2, id1);
            Assert.IsTrue(id1 < id2, "id1 expected to be less than id2 (1)");

            id2 = new CyclicalID.UShortID((ushort)half);
            AssertConsistent(id1, id2);
            AssertConsistent(id2, id1);
            Assert.IsTrue(id1 < id2, "id1 expected to be less than id2 (2)");

            id2 = new CyclicalID.UShortID((ushort)(half + 1));
            AssertConsistent(id1, id2);
            AssertConsistent(id2, id1);
            Assert.IsTrue(id1 > id2, "id1 expected to be greater than id2 (1)");
        }

        [Test]
        public void OrderingTest()
        {
            CyclicalID.UShortID id1 = new CyclicalID.UShortID();
            CyclicalID.UShortID id2 = new CyclicalID.UShortID();
            ushort max = id1.MaxValue;
            ushort half = (ushort)(((uint)id1.MaxValue + 1) / 2);

            Assert.IsTrue(id1.Equals(id2), "Freshly constructed IDs are not equal");

            id2.Increment();
            Assert.IsTrue(id2 > id1, "Simple increment shows incorrect ordering");

            id2 = new CyclicalID.UShortID(max);
            Assert.IsTrue(id1 > id2, "Wrap around shows incorrect ordering");


            // Long increment starting from 0
            id1 = new CyclicalID.UShortID();
            id2 = new CyclicalID.UShortID();
            for (int i = 0; i < half; i++)
                id2.Increment();
            Assert.IsTrue(id2 > id1, "Long increment shows incorrect ordering (1)");
            id2.Increment();
            Assert.IsTrue(id1 > id2, "Long increment shows incorrect ordering (2)");

            // Long increment starting from 3/4ths
            id1 = new CyclicalID.UShortID((ushort)(3 * (max / 4)));
            id2 = new CyclicalID.UShortID((ushort)(3 * (max / 4)));
            for (int i = 0; i < half - 1; i++)  // Cutoff occurs one step earlier if the numerically higher ID is thought of as the first ID (see CyclicalID.Compare(...) where diff1 == diff2)
                id2.Increment();
            Assert.IsTrue(id2 > id1, "Long increment shows incorrect ordering (3)");
            id2.Increment();
            Assert.IsTrue(id1 > id2, "Long increment shows incorrect ordering (4)");
        }

        [Test]
        public void PreviousIDTest()
        {
            CyclicalID.UShortID zero = new CyclicalID.UShortID(0);
            CyclicalID.UShortID one = new CyclicalID.UShortID(1);
            CyclicalID.UShortID max = new CyclicalID.UShortID(zero.MaxValue);

            Assert.AreEqual(zero, one.Previous);
            Assert.AreEqual(max, zero.Previous);
        }
    }
}
