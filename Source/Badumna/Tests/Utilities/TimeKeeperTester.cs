﻿namespace BadumnaTests.Utilities
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Threading;
    using Badumna.Utilities;
    using NUnit.Framework;

    [TestFixture]
    public class TimeKeeperTester
    {
        [Test]
        public void RollOverTest()
        {
            // Test that TimeKeeper works when System.Environment.TickCount works when
            // it rolls-over from Int32.MaxValue to Int32.MinValue

            TestTimerKeeper keeper = new TestTimerKeeper();
            keeper.CurrentTicks = Int32.MaxValue - 5;
            keeper.Start();
            unchecked
            {
                keeper.CurrentTicks = keeper.CurrentTicks + 7;
            }
            keeper.Pause();
            Assert.AreEqual(TimeSpan.FromMilliseconds(7), keeper.Elapsed);
        }

        [Test]
        public void RunTest()
        {
            using (TimeKeeper keeper = new TimeKeeper())
            {
                keeper.Start();
                System.Threading.Thread.Sleep(1000);
                TimeSpan elapsed = keeper.Elapsed;
                Assert.GreaterOrEqual(elapsed.TotalMilliseconds, 980);
                Assert.LessOrEqual(elapsed.TotalMilliseconds, 1020);
                //Console.WriteLine(elapsed);
            }
        }

        [Test]
        [Category("Slow")]
        public void PauseTest()
        {
            using (TimeKeeper keeper = new TimeKeeper())
            {
                keeper.Start();
                System.Threading.Thread.Sleep(1000);
                keeper.Pause();
                TimeSpan e1 = keeper.Elapsed;
                System.Threading.Thread.Sleep(1000);
                Assert.AreEqual(e1, keeper.Elapsed);
                keeper.Start();
                System.Threading.Thread.Sleep(1000);
                TimeSpan e2 = keeper.Elapsed;
                Assert.GreaterOrEqual(e2.TotalMilliseconds, 2 * 980);
                Assert.LessOrEqual(e2.TotalMilliseconds, 2 * 1020);
            }
        }

        [Test]
        public void ResolutionTest()
        {
            using (TimeKeeper keeper = new TimeKeeper())
            {
                double estimatedResolution = keeper.EstimateResolution();
                //Console.WriteLine("Timer resolution ~= {0:0.0}ms", estimatedResolution);
                Assert.LessOrEqual(estimatedResolution, 2.0, "Timer resolution worse than 2.0ms");
            }
        }

        [Test, Ignore]
        public void WaitOneTimeoutTest()
        {
            ManualResetEvent mre = new ManualResetEvent(false);

            int iterations = 1000;

            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            for (int i = 0; i < iterations; i++)
            {
                mre.WaitOne(1);
                //Thread.Sleep(1);
            }
            stopwatch.Stop();

            double averageWait = (double)stopwatch.ElapsedMilliseconds / iterations;
            //Console.WriteLine("Average WaitOne time (expecting ~1ms) = {0:0.0}ms", averageWait);
        }

        /* Commented out because of its reference to VisualBasic 
        [Test, Ignore]  // Should only be run manually because it messes with the system clock
        public void IndependentOfClockTest()
        {
            using (TimeKeeper keeper = new TimeKeeper())
            {
                keeper.Start();

                DateTime now = DateTime.Now;
                Microsoft.VisualBasic.DateAndTime.TimeOfDay = now - TimeSpan.FromHours(1);
                TimeSpan elapsed = keeper.Elapsed;
                Microsoft.VisualBasic.DateAndTime.TimeOfDay = now;

                Assert.GreaterOrEqual(elapsed.TotalMilliseconds, 0);
                Assert.LessOrEqual(elapsed.TotalMilliseconds, 100);
            }
        }
         */

        private class TestTimerKeeper : TimeKeeper
        {
            public int CurrentTicks { get; set; }

            protected override int GetTickCount()
            {
                return this.CurrentTicks;
            }
        }
    }
}
