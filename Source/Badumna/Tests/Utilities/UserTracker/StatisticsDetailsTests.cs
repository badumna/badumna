﻿//-----------------------------------------------------------------------
// <copyright file="StatisticsDetailsTests.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2011 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using Badumna.Utilities.UserTracker;
using NUnit.Framework;

namespace BadumnaTests.Utilities.UserTracker
{
    [TestFixture]
    public class StatisticsDetailsTests
    {
        [Test]
        public void MinimumSizeTest()
        {
            StatisticsDetails details = new StatisticsDetails();
            details.Payload = string.Empty;
            Assert.AreEqual(StatisticsDetails.MinimumSize, details.ToArray().Length);
        }

        [Test]
        public void RoundTrippingTest()
        {
            string payload = "A sample payload.";
            long userId = -1;
            uint uniqueishId = uint.MaxValue;

            StatisticsDetails details = new StatisticsDetails(payload, uniqueishId);

            StatisticsDetails detailsCopy = new StatisticsDetails();
            detailsCopy.Deserialize(details.ToArray());

            Assert.AreEqual(payload, detailsCopy.Payload);
            Assert.AreEqual(userId, detailsCopy.UserID);
            Assert.AreEqual(uniqueishId, detailsCopy.UniqueishID);
        }
    }
}
