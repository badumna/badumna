﻿//-----------------------------------------------------------------------
// <copyright file="UserDetailsTests.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2011 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using Badumna;
using Badumna.Core;
using Badumna.Utilities.UserTracker;
using NUnit.Framework;

namespace BadumnaTests.Utilities.UserTracker
{
    [TestFixture]
    public class UserDetailsTests
    {
        /*
        [Test]
        public void MinimumSizeTest()
        {
            UserDetails details = new UserDetails();
            details.ApplicationName = string.Empty;
            details.PrivateAddress = string.Empty;
            details.PublicAddress = string.Empty;
            Calling serialize changes the details, so this isn't easy to test...
            Assert.AreEqual(UserDetails.MinimumSize, details.ToArray().Length);
        }
         */

        /*
        [Test]
        public void RoundTrippingTest()
        {
            string applicationName = "epic-elevator-conflict";
            string publicAddress = "Open|127.0.0.1:42";
            string privateAddress = "Symmetric|127.0.0.2:43";
            int revision = 71;

            ProtocolHash.SetApplicationSalt(applicationName);
            INetworkFacade facade = MockRepository.GenerateStub<INetworkFacade>();
            facade.Stub(f => f.GetNetworkStatus()).Return   arrgh, can't mock NetworkStatus easily :(

            UserDetails details = new UserDetails();
            details.ApplicationName = applicationName;
            details.PublicAddress = publicAddress;
            details.PrivateAddress = privateAddress;
            details.Revision = revision;

            UserDetails detailsCopy = new UserDetails();
            detailsCopy.Deserialize(details.ToArray());

            Assert.AreEqual(applicationName, detailsCopy.ApplicationName);
            Assert.AreEqual(publicAddress, detailsCopy.PublicAddress);
            Assert.AreEqual(privateAddress, detailsCopy.PrivateAddress);
            Assert.AreEqual(revision, detailsCopy.Revision);
        }
         */
    }
}
