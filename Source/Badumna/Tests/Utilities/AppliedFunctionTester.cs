﻿using System;
using Badumna.Utilities;
using NUnit.Framework;

namespace BadumnaTests.Utilities
{
    class ParamOrderCheck
    {
        private string mParams;
        public string Params { get { return this.mParams; } }

        public void Call(int a)
        {
            this.mParams = String.Format("{0}", a);
        }

        public void Call(int a, int b)
        {
            this.mParams = String.Format("{0}, {1}", a, b);
        }

        public void Call(int a, int b, int c)
        {
            this.mParams = String.Format("{0}, {1}, {2}", a, b, c);
        }

        public void Call(int a, int b, int c, int d)
        {
            this.mParams = String.Format("{0}, {1}, {2}, {3}", a, b, c, d);
        }

        public void Call(int a, int b, int c, int d, int e)
        {
            this.mParams = String.Format("{0}, {1}, {2}, {3}, {4}", a, b, c, d, e);
        }

        public void Call(int a, int b, int c, int d, int e, int f)
        {
            this.mParams = String.Format("{0}, {1}, {2}, {3}, {4}, {5}", a, b, c, d, e, f);
        }
    }

    [TestFixture]
    public class AppliedFunctionTester
    {
        [Test]
        public void ParamOrderTest()
        {
            ParamOrderCheck p = new ParamOrderCheck();

            new AppliedFunction<int>(p.Call, 1).Invoke();
            Assert.AreEqual("1", p.Params);

            new AppliedFunction<int, int>(p.Call, 1, 2).Invoke();
            Assert.AreEqual("1, 2", p.Params);

            new AppliedFunction<int, int, int>(p.Call, 1, 2, 3).Invoke();
            Assert.AreEqual("1, 2, 3", p.Params);

            new AppliedFunction<int, int, int, int>(p.Call, 1, 2, 3, 4).Invoke();
            Assert.AreEqual("1, 2, 3, 4", p.Params);

            new AppliedFunction<int, int, int, int, int>(p.Call, 1, 2, 3, 4, 5).Invoke();
            Assert.AreEqual("1, 2, 3, 4, 5", p.Params);

            new AppliedFunction<int, int, int, int, int, int>(p.Call, 1, 2, 3, 4, 5, 6).Invoke();
            Assert.AreEqual("1, 2, 3, 4, 5, 6", p.Params);
        }
    }
}
