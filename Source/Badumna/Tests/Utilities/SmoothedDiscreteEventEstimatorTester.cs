﻿using Badumna.Core;
using Badumna.Utilities;
using BadumnaTests.Core;
using NUnit.Framework;

namespace BadumnaTests.Utilities
{
    [TestFixture]
    [DontPerformCoverage]
    public class SmoothedDiscreteEventEstimatorTester
    {

        [SetUp]
        public void Initialize()
        {
        }

        [TearDown]
        public void Terminate()
        {
        }

        [Test]
        public void EstimatorTest()
        {
            Smoother estimator = new Smoother(5.0, 500.0);

            for (int i = 0; i < 5; i++)
            {
                estimator.ValueEvent(500.0);
            }

            Assert.AreEqual(500.0, estimator.EstimatedValue, "Incorrect estimated value");

            for (int i = 0; i < 500; i++)
            {
                estimator.ValueEvent(400.0);
            }

            Assert.Greater(400.1, estimator.EstimatedValue, "Incorrect estimated value");
            Assert.Less(399.9, estimator.EstimatedValue, "Incorrect estimated value");
        }
    }
}
