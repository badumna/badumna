﻿//---------------------------------------------------------------------------------
// <copyright file="BadumnaIdGenerator.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

using Badumna.Core;
using Badumna.DataTypes;
using Badumna.DistributedHashTable;
using Badumna.Utilities;

namespace BadumnaTests.Utilities
{
    public class BadumnaIdGenerator
    {
        /// <summary>
        /// Return a new BadumnaId generator for the loopback address on a meaningless port.
        /// </summary>
        /// <returns></returns>
        /// <remarks>
        /// Calling this method returns a generator that starts at ID 1, so that
        /// unchanged tests consistently use the same IDs. If you want multiple unique
        /// IDs, you should re-call the returned generator instead of getting a new one.
        /// </remarks>
        internal static GenericCallBackReturn<BadumnaId> Local()
        {
            return WithAddress(PeerAddress.GetLoopback(1));
        }

        /// <summary>
        /// Return a new BadumnaId generator for the given address.
        /// </summary>
        /// <returns></returns>
        internal static GenericCallBackReturn<BadumnaId> WithAddress(PeerAddress address)
        {
            ushort localId = 1;
            return () => new BadumnaId(address, localId++);
        }

        /// <summary>
        /// Return a new BadumnaId generator which takes an address and returns an
        /// ID with increasing sequence number (localId).
        /// </summary>
        /// <returns></returns>
        internal static GenericCallBackReturn<BadumnaId, PeerAddress> FromAddress()
        {
            ushort localId = 1;
            return (address) => new BadumnaId(address, localId++);
        }

        /// <summary>
        /// Return a new BadumnaId generator which takes a hash key and returns an
        /// ID with increasing sequence number (localId).
        /// </summary>
        /// <returns></returns>
        internal static GenericCallBackReturn<BadumnaId, HashKey> FromHash()
        {
            ushort localId = 1;
            return (hash) => new BadumnaId(new PeerAddress(hash), localId++);
        }
    }
}
