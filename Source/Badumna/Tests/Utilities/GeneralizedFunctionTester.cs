﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using Badumna.Utilities;
using System.Reflection;

namespace BadumnaTests.Utilities
{
    [TestFixture]
    public class GeneralizedFunctionTester
    {
        [TearDown]
        public void TearDown()
        {
            StaticVal = 0;
        }

        [Test]
        public void CallableTest()
        {
            ParamOrderCheck p = new ParamOrderCheck();

            GeneralizedFunction c = GeneralizedFunction.Make(p, p.GetType().GetMethod("Call", new Type[] { typeof(int) }), Activator.CreateInstance);
            c.Call(1);
            Assert.AreEqual("1", p.Params);

            c = GeneralizedFunction.Make(p, p.GetType().GetMethod("Call", new Type[] { typeof(int), typeof(int) }), Activator.CreateInstance);
            c.Call(1, 2);
            Assert.AreEqual("1, 2", p.Params);

            c = GeneralizedFunction.Make(p, p.GetType().GetMethod("Call", new Type[] { typeof(int), typeof(int), typeof(int) }), Activator.CreateInstance);
            c.Call(1, 2, 3);
            Assert.AreEqual("1, 2, 3", p.Params);

            c = GeneralizedFunction.Make(p, p.GetType().GetMethod("Call", new Type[] { typeof(int), typeof(int), typeof(int), typeof(int) }), Activator.CreateInstance);
            c.Call(1, 2, 3, 4);
            Assert.AreEqual("1, 2, 3, 4", p.Params);

            c = GeneralizedFunction.Make(p, p.GetType().GetMethod("Call", new Type[] { typeof(int), typeof(int), typeof(int), typeof(int), typeof(int) }), Activator.CreateInstance);
            c.Call(1, 2, 3, 4, 5);
            Assert.AreEqual("1, 2, 3, 4, 5", p.Params);

            c = GeneralizedFunction.Make(p, p.GetType().GetMethod("Call", new Type[] { typeof(int), typeof(int), typeof(int), typeof(int), typeof(int), typeof(int) }), Activator.CreateInstance);
            c.Call(1, 2, 3, 4, 5, 6);
            Assert.AreEqual("1, 2, 3, 4, 5, 6", p.Params);
        }

        static private int StaticVal = 0;
        static private void StaticFunc()
        {
            StaticVal = 42;
        }

        [Test]
        public void StaticTest()
        {
            Assert.AreEqual(0, StaticVal);
            GeneralizedFunction.Make(null, this.GetType().GetMethod("StaticFunc", BindingFlags.NonPublic | BindingFlags.Static), Activator.CreateInstance).Call();
            Assert.AreEqual(42, StaticVal);
        }
    }
}
