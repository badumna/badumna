﻿//---------------------------------------------------------------------------------
// <copyright file="DataScrambleSchemeTester.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Diagnostics;
using Badumna.Utilities;
using NUnit.Framework;

namespace BadumnaTests.Utilities
{
    public class DataScrambleSchemeTester
    {
        private Random random;

        [SetUp]
        public void Initialize()
        {
            this.random = new Random(Environment.TickCount);
        }

        [TearDown]
        public void Terminate()
        {
        }

        [Test]
        public void ScrambleAndThenUnscrambleLeadToSameData()
        {
            for (int i = 1; i < 512; i++)
            {
                byte[] data = this.GetRandomByteArray(i);
                byte[] scrambled = DataScrambleScheme.ScrambleUDPData(data);
                Assert.NotNull(scrambled);
                byte[] unscrambled = DataScrambleScheme.UnscrambleUDPData(scrambled);
                Assert.NotNull(unscrambled);
                Assert.IsTrue(this.AreEqual(data, unscrambled));
            }
        }

        [Test]
        [ExpectedException("System.ArgumentNullException")]
        public void NullReferenceInputCausesExceptionOnScramble()
        {
            DataScrambleScheme.ScrambleUDPData(null);
        }

        [Test]
        [ExpectedException("System.ArgumentNullException")]
        public void NullReferenceInputCausesExceptionOnUnscramble()
        {
            DataScrambleScheme.UnscrambleUDPData(null);
        }

        [Test]
        public void ScrambleWillNotChangeEmptyPacket()
        {
            byte[] data = new byte[0];
            byte[] scrambled = DataScrambleScheme.ScrambleUDPData(data);
            Assert.IsTrue(this.AreEqual(data, scrambled));
            Assert.IsTrue(data == scrambled);
        }

        [Test]
        public void UnscrambleWillNotChangeEmptyPacket()
        {
            byte[] data = new byte[0];
            byte[] unscrambled = DataScrambleScheme.UnscrambleUDPData(data);
            Assert.AreEqual(data, unscrambled);
            Assert.IsTrue(this.AreEqual(data, unscrambled));
        }

        [Test]
        public void IllegalSmallPacketsWillBeDetected()
        {
            for (int i = 1; i < 7; i++)
            {
                byte[] data = new byte[i];
                byte[] unscrambled = DataScrambleScheme.UnscrambleUDPData(data);
                Assert.IsNull(unscrambled);
            }
        }

        [Test]
        public void AlteredDataWillBeDetected()
        {
            for (int repeat = 0; repeat < 100; repeat++)
            {
                for (int i = 1; i < 512; i++)
                {
                    byte[] data = this.GetRandomByteArray(i);
                    byte[] scrambled = DataScrambleScheme.ScrambleUDPData(data);
                    Assert.NotNull(scrambled);

                    int randomIndex = (this.random.Next() % (scrambled.Length - 4)) + 4;
                    scrambled[randomIndex] = (byte)(scrambled[randomIndex] + 1);

                    byte[] unscrambled = DataScrambleScheme.UnscrambleUDPData(scrambled);
                    Assert.IsNull(unscrambled);
                }
            }
        }

        [Test]
        public void AlterTheFirstFourBytesJunkIsTolerated()
        {
            for (int repeat = 0; repeat < 10; repeat++)
            {
                for (int i = 1; i < 512; i++)
                {
                    byte[] data = this.GetRandomByteArray(i);
                    byte[] scrambled = DataScrambleScheme.ScrambleUDPData(data);
                    Assert.NotNull(scrambled);

                    int randomIndex = this.random.Next() % 4;
                    scrambled[randomIndex] = (byte)(scrambled[randomIndex] + 1);

                    byte[] unscrambled = DataScrambleScheme.UnscrambleUDPData(scrambled);
                    Assert.NotNull(unscrambled);
                    Assert.IsTrue(this.AreEqual(data, unscrambled));
                }
            }
        }

        [Test]
        public void AlteredDataLengthWillBeDetectedWithHighProbability()
        {
            int undetected = 0;

            for (int repeat = 0; repeat < 100; repeat++)
            {
                for (int i = 2; i < 512; i++)
                {
                    byte[] data = this.GetRandomByteArray(i);
                    byte[] scrambled = DataScrambleScheme.ScrambleUDPData(data);
                    Assert.NotNull(scrambled);

                    int randomIndex = this.random.Next() % scrambled.Length;
                    byte[] modified = new byte[scrambled.Length - 1];

                    int k = 0;
                    for (int j = 0; j < scrambled.Length; j++)
                    {
                        if (j != randomIndex)
                        {
                            modified[k++] = scrambled[j];
                        }
                    }

                    byte[] unscrambled = DataScrambleScheme.UnscrambleUDPData(modified);
                    if (unscrambled != null)
                    {
                        undetected++;
                    }
                }
            }

            Assert.IsTrue(undetected < 15);
        }

        [Test]
        public void RandomDataWillBeConsideredAsInvalidWithHighProbability()
        {
            int undetected = 0;

            for (int repeat = 0; repeat < 100; repeat++)
            {
                for (int i = 10; i < 512; i++)
                {
                    byte[] data = this.GetRandomByteArray(i);
                    byte[] unscrambled = DataScrambleScheme.UnscrambleUDPData(data);
                    if (unscrambled != null)
                    {
                        undetected++;
                    }
                }
            }

            Assert.IsTrue(undetected < 5);
        }

        [Test]
        public void VariedLengthDataWithValidChecksumsDoesNotCrash()
        {
            int blockSize = 64;
            int junkSize = 4;
            int checksumSize = 2;

            for (int i = 0; i <= 512; i++)
            {
                byte[] data = this.GetRandomByteArray(i);

                int blockCount = (i - junkSize + blockSize + checksumSize - 1) / (blockSize + checksumSize);
                int offset = junkSize;
                for (int block = 0; block < blockCount; block++)
                {
                    if (offset + blockSize + checksumSize > data.Length)
                    {
                        break;
                    }

                    ushort crc = CRC.CRC16(data, offset, blockSize);
                    offset += blockSize;
                    data[offset] = (byte)crc;
                    data[offset + 1] = (byte)(crc >> 8);
                    offset += 2;
                }

                DataScrambleScheme.UnscrambleUDPData(data);
            }
        }

        [Test]
        public void OverheadLengthIsFixedForFixedLengthInput()
        {
            int overhead;
            for (int i = 32; i < 512; i = i + 32)
            {
                overhead = -1;
                for (int repeat = 0; repeat < 100; repeat++)
                {
                    byte[] data = this.GetRandomByteArray(i);
                    if (overhead == -1)
                    {
                        overhead = data.Length - i;
                    }
                    else
                    {
                        Assert.AreEqual(overhead, data.Length - i);
                    }
                }
            }
        }

        [Test]
        public void ScramblingSchemeBandwidthIsReasonable()
        {
            List<byte[]> inputData = new List<byte[]>();
            List<byte[]> scrambledData = new List<byte[]>();
            List<byte[]> unscrambledData = new List<byte[]>();

            // generate 10mbytes input data. 
            for (int total = 0; total < 10 * 1024 * 1024; total += 512)
            {
                byte[] data = this.GetRandomByteArray(512);
                inputData.Add(data);
            }

            // scramble the data
            TimeSpan start = Process.GetCurrentProcess().UserProcessorTime;
            foreach (byte[] data in inputData)
            {
                byte[] scrambled = DataScrambleScheme.ScrambleUDPData(data);
                if (scrambled != null)
                {
                    scrambledData.Add(scrambled);
                }
            }

            TimeSpan end = Process.GetCurrentProcess().UserProcessorTime;
            
            // bandwidth in mbytes/sec
            double scrambleBadwidth = 10.0 / ((end - start).TotalMilliseconds / 1000.0);
            Assert.Greater(scrambleBadwidth, 20.0);

            // unscramble
            start = Process.GetCurrentProcess().UserProcessorTime;
            foreach (byte[] data in scrambledData)
            {
                byte[] unscrambled = DataScrambleScheme.UnscrambleUDPData(data);
                Assert.NotNull(unscrambled);
                unscrambledData.Add(unscrambled);
            }

            end = Process.GetCurrentProcess().UserProcessorTime;

            double unscrambleBadwidth = 10.0 / ((end - start).TotalMilliseconds / 1000.0);
            Assert.Greater(unscrambleBadwidth, 20.0);
        }

        private byte[] GetRandomByteArray(int length)
        {
            byte[] data = new byte[length];
            this.random.NextBytes(data);
            return data;
        }

        private bool AreEqual(byte[] one, byte[] two)
        {
            if (one == null && two == null)
            {
                return true;
            }
            else if (one == null && two != null)
            {
                return false;
            }
            else if (one != null && two == null)
            {
                return false;
            }

            if (one.Length != two.Length)
            {
                return false;
            }

            for (int i = 0; i < one.Length; i++)
            {
                if (one[i] != two[i])
                {
                    return false;
                }
            }

            return true;
        }
    }
}
