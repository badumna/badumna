﻿using Badumna.Utilities;
using NUnit.Framework;

namespace BadumnaTests.Utilities
{
    [TestFixture]
    public class CRCTester
    {
        // Check values calculated using http://zorc.breitbandkatze.de/crc.html

        [Test]
        public void CRC16Test()
        {
            Assert.AreEqual(0xFEE8, CRC.CRC16(new byte[] { 49, 50, 51, 52, 53, 54, 55, 56, 57 }));  // Test data is "123456789"
        }

        [Test]
        public void CRC32Test()
        {
            Assert.AreEqual(0x89A1897F, CRC.CRC32(new byte[] { 49, 50, 51, 52, 53, 54, 55, 56, 57 }));  // Test data is "123456789"
        }

        [Test]
        public void CRC32SplitTest()
        {
            uint result = CRC.CRC32(new byte[] { 49, 50, 51, 52 });
            Assert.AreEqual(0x89A1897F, CRC.CRC32(result, new byte[] { 53, 54, 55, 56, 57 }));  // Test data is "123456789"
        }

        [Test]
        public void ActualMessageTest()
        {
            uint result = CRC.CRC16(new byte[] { 57, 212, 239, 65, 150, 163, 84, 153, 0, 0, 4, 16, 65, 0, 0, 0, 2, 19, 32, 0, 0, 0, 21, 1, 5, 121, 162, 68, 201, 200, 88, 65, 239, 35, 163, 68, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 0, 0, 0, 0, 0, 2, 128, 0, 0, 10, 150, 198, 127, 65, 132, 123, 226, 183, 10, 38, 104, 213, 104, 241, 188, 90, 218, 149, 154, 255, 2, 0, 2, 128, 3, 128, 9, 126, 249, 149, 255, 243, 147, 32, 14, 51, 6, 58, 52, 9, 215, 84, 205, 115, 26, 167, 5, 128, 250, 77, 152, 226, 128, 15, 240, 128, 250, 76, 95 });
            Assert.AreEqual(result, CRC.CRC16(new byte[] { 57, 212, 239, 65, 150, 163, 84, 153, 0, 0, 4, 16, 65, 0, 0, 0, 2, 19, 32, 0, 0, 0, 21, 1, 5, 121, 162, 68, 201, 200, 88, 65, 239, 35, 163, 68, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 0, 0, 0, 0, 0, 2, 128, 0, 0, 10, 150, 198, 127, 65, 132, 123, 226, 183, 10, 38, 104, 213, 104, 241, 188, 90, 218, 149, 154, 255, 2, 0, 2, 128, 3, 128, 9, 126, 249, 149, 255, 243, 147, 32, 14, 51, 6, 58, 52, 9, 215, 84, 205, 115, 26, 167, 5, 128, 250, 77, 152, 226, 128, 15, 240, 128, 250, 76, 95 }));
        }
    }
}
