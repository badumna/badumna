﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using Badumna.Utilities;
using Badumna.Core;
using Rhino.Mocks;

namespace BadumnaTests.Utilities
{
    public class MockExpireable : IExpireable
    {
        private TimeSpan mExpirationTime;
        public TimeSpan ExpirationTime
        {
            get { return this.mExpirationTime; }
            set { this.mExpirationTime = value; }
        }
    }

    [TestFixture]
    public class GarbageCollectorTester
    {
        private GarbageCollector mCollector;
        private object mExpiredObject;

        private Simulator eventQueue;
        private ITime timeKeeper;
        private INetworkConnectivityReporter connectivityReporter;

        [SetUp]
        public void Initialize()
        {
            this.eventQueue = new Simulator();
            this.timeKeeper = this.eventQueue;
            this.connectivityReporter = MockRepository.GenerateStub<INetworkConnectivityReporter>();
            this.connectivityReporter.Stub(x => x.Status).Return(ConnectivityStatus.Online);
            this.mExpiredObject = null;
        }

        [Test]
        public void InitializeTest()
        {
            this.mCollector = new GarbageCollector("test", 0.05, this.eventQueue, this.timeKeeper, this.connectivityReporter);
        }


        private void ExpirationHandler(object sender, ExpireEventArgs e)
        {
            this.mExpiredObject = e.ExpiredObject;
        }

        [Test]
        public void CollectTest()
        {
            MockExpireable expireable = new MockExpireable();

            this.mCollector = new GarbageCollector("test", 3.0, this.eventQueue, this.timeKeeper, this.connectivityReporter);
            this.mCollector.ExpirationEvent += this.ExpirationHandler;

            this.mCollector.ExpireObjectSecondsFromNow(expireable, 5);

            this.eventQueue.RunFor(TimeSpan.FromMilliseconds(2000));
            Assert.AreEqual(null, this.mExpiredObject);

            this.eventQueue.RunFor(TimeSpan.FromMilliseconds(3500));
            Assert.AreEqual(null, this.mExpiredObject);

            this.eventQueue.RunFor(TimeSpan.FromMilliseconds(1000));
            Assert.AreEqual(expireable, this.mExpiredObject);
        }

        [Test]
        public void KeepAliveTest()
        {
            MockExpireable expireable = new MockExpireable();

            this.mCollector = new GarbageCollector("test", 3.0, this.eventQueue, this.timeKeeper, this.connectivityReporter);
            this.mCollector.ExpirationEvent += this.ExpirationHandler;

            this.mCollector.ExpireObjectSecondsFromNow(expireable, 5);

            this.eventQueue.RunFor(TimeSpan.FromMilliseconds(5500));
            Assert.AreEqual(null, this.mExpiredObject);
            this.mCollector.ExpireObjectSecondsFromNow(expireable, 2);

            this.eventQueue.RunFor(TimeSpan.FromMilliseconds(1000));
            Assert.AreEqual(null, this.mExpiredObject);

            this.eventQueue.RunFor(TimeSpan.FromMilliseconds(2500));
            Assert.AreEqual(expireable, this.mExpiredObject);
        }

        [Test]
        public void MultipleTest()
        {
            MockExpireable expireableA = new MockExpireable();
            MockExpireable expireableB = new MockExpireable();
            MockExpireable expireableC = new MockExpireable();
            MockExpireable expireableD = new MockExpireable();

            this.mCollector = new GarbageCollector("test", 0.2, this.eventQueue, this.timeKeeper, this.connectivityReporter);
            this.mCollector.ExpirationEvent += this.ExpirationHandler;

            this.mCollector.ExpireObjectSecondsFromNow(expireableC, 5);
            this.mCollector.ExpireObjectSecondsFromNow(expireableA, 2);
            this.mCollector.ExpireObjectSecondsFromNow(expireableD, 8);
            this.mCollector.ExpireObjectSecondsFromNow(expireableB, 5);

            this.eventQueue.RunFor(TimeSpan.FromMilliseconds(500));
            Assert.AreEqual(null, this.mExpiredObject);
            this.mCollector.ExpireObjectSecondsFromNow(expireableB, 6);

            this.eventQueue.RunFor(TimeSpan.FromMilliseconds(2000));
            Assert.AreEqual(expireableA, this.mExpiredObject);

            this.eventQueue.RunFor(TimeSpan.FromMilliseconds(3000));
            Assert.AreEqual(expireableC, this.mExpiredObject);

            this.eventQueue.RunFor(+TimeSpan.FromMilliseconds(1500));
            Assert.AreEqual(expireableB, this.mExpiredObject);

            this.eventQueue.RunFor(TimeSpan.FromMilliseconds(2000));
            Assert.AreEqual(expireableD, this.mExpiredObject);
        }
    }
}
