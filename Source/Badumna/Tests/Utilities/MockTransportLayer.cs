﻿//-----------------------------------------------------------------------
// <copyright file="MockTransportLayer.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2011 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using Badumna;
using Badumna.Core;
using Badumna.Transport;
using Badumna.Utilities;

namespace BadumnaTests.Utilities
{
    /// <summary>
    /// A transport layer for testing purposes.  This transport layer implements a
    /// message dispatcher that simply passes a copy of the envelope to the destination's
    /// message consumer after an appropriate delay.
    /// </summary>
    [DontPerformCoverage]
    internal class MockTransportLayer : IMessageProducer<TransportEnvelope>, IMessageDispatcher<TransportEnvelope>
    {
        /// <summary>
        /// The network links known by this peer.
        /// </summary>
        private Dictionary<PeerAddress, Link> links = new Dictionary<PeerAddress, Link>();

        /// <summary>
        /// The address provider.
        /// </summary>
        private INetworkAddressProvider addressProvider;

        /// <summary>
        /// The network event queue.
        /// </summary>
        private NetworkEventQueue eventQueue;

        /// <summary>
        /// The time keeper.
        /// </summary>
        private ITime timeKeeper;

        public MockTransportLayer(INetworkAddressProvider addressProvider, NetworkEventQueue eventQueue, ITime timeKeeper)
        {
            this.eventQueue = eventQueue;
            this.timeKeeper = timeKeeper;
            this.addressProvider = addressProvider;
            this.ResetCounters();
        }

        public PeerAddress Address
        {
            get { return this.addressProvider.PublicAddress; }
        }

        public long SentBytes { get; private set; }

        public long ReceivedBytes { get; private set; }

        public int NumSentMessages { get; private set; }

        public int NumReceivedMessages { get; private set; }

        public TimeSpan TimeOfLastArrival { get; private set; }

        public IMessageConsumer<TransportEnvelope> MessageConsumer { get; set; }

        public void ResetCounters()
        {
            this.NumSentMessages = 0;
            this.NumReceivedMessages = 0;
            this.SentBytes = 0;
            this.ReceivedBytes = 0;
            this.TimeOfLastArrival = TimeSpan.Zero;
        }

        /// <summary>
        /// Creates or modifies the link to the given destination with a custom drop filter.
        /// </summary>
        /// <param name="destination">The destination MockTransportLayer.</param>
        /// <param name="latencyMs">The latency of the link in milliseconds.</param>
        /// <param name="filter">A filter function that returns true if the envelope should be sent, or false if it should be dropped.</param>
        public void ModifyLink(
            MockTransportLayer destination,
            int latencyMs,
            GenericCallBackReturn<bool, MockTransportLayer, TransportEnvelope> filter)
        {
            this.links[destination.Address] = new Link(destination, latencyMs, filter);
        }

        /// <summary>
        /// Creates or modifies the link to the given destination with a specified reliability.
        /// </summary>
        /// <param name="destination">The destination MockTransportLayer.</param>
        /// <param name="latencyMs">The latency of the link in milliseconds.</param>
        /// <param name="reliability">The reliability of the link.</param>
        /// <param name="bandwidthKBpS">NOT IMPLEMENTED. The bandwidth of the link in kB/s.</param>
        /// <remarks>
        /// TODO: Use the bandwidth parameter to calculate packet travel time.
        /// </remarks>
        public void ModifyLink(MockTransportLayer destination, int latencyMs, double reliability, double bandwidthKBpS)
        {
            this.ModifyLink(destination, latencyMs, delegate { return RandomSource.Generator.NextDouble() < reliability; });
        }

        /// <summary>
        /// Creates or modifies the link to the given destination.
        /// </summary>
        /// <param name="destination">The destination MockTransportLayer.</param>
        /// <param name="latencyMs">The latency of the link in milliseconds.</param>
        public void ModifyLink(MockTransportLayer destination, int latencyMs)
        {
            this.ModifyLink(destination, latencyMs, delegate { return true; });
        }

        /// <summary>
        /// Removes the link to the given destination.
        /// </summary>
        /// <param name="destination">The destination MockTransportLayer.</param>
        public void RemoveLink(MockTransportLayer destination)
        {
            this.links.Remove(destination.Address);
        }

        public void SendMessage(TransportEnvelope envelope)
        {
            // Copy envelope (including message) so the receiver doesn't get the source's private data
            // (like HasBeenChecksummed, etc).
            TransportEnvelope envelopeCopy = new TransportEnvelope(envelope);
            envelopeCopy.Source = new PeerAddress(this.addressProvider.PublicAddress);

            Logger.TraceInformation(
                LogTag.Tests,
                "t = {0}ms  Sending {1} bytes to {2}: {3}",
                this.timeKeeper.Now.TotalMilliseconds,
                envelopeCopy.Length,
                envelopeCopy.Destination,
                envelopeCopy.Label);

            if (this.addressProvider.PublicAddress.Equals(envelopeCopy.Destination))
            {
                this.eventQueue.Push(this.ReceiveMessage, envelopeCopy);
                return;
            }

            Link link;
            if (!this.links.TryGetValue(envelopeCopy.Destination, out link))
            {
                Logger.TraceInformation(LogTag.Tests, "Peer " + this.addressProvider.PublicAddress + " cannot mock send to unknown destination : " + envelopeCopy.Destination);
                return;
            }

            // TODO: We should probably also count messages sent to ourself and to unknown destinations.
            //       However, changing this will entail updating existing test cases which have hard-coded message counts.
            this.SentBytes += envelopeCopy.Length;
            this.NumSentMessages++;

            if (link.Filter(this, envelopeCopy))
            {
                this.eventQueue.Schedule(link.Latency, link.Remote.ReceiveMessage, envelopeCopy);
            }
        }

        public void ReceiveMessage(TransportEnvelope messageEnvelope)
        {
            Logger.TraceInformation(
                LogTag.Tests,
                "t = {0}ms  Receiving {1} bytes from {2}: {3}",
                this.timeKeeper.Now.TotalMilliseconds,
                messageEnvelope.Length,
                messageEnvelope.Source,
                messageEnvelope.Label);

            this.ReceivedBytes += messageEnvelope.Length;
            this.NumReceivedMessages++;
            this.TimeOfLastArrival = this.timeKeeper.Now;

            messageEnvelope.ArrivalTime = this.timeKeeper.Now;
            messageEnvelope.ResetParseOffset();
            messageEnvelope.Source.NatType = NatType.Unknown;

            try
            {
                this.MessageConsumer.ProcessMessage(messageEnvelope);
            }
            catch (BadumnaException e)
            {
                e.TraceMessage();
                Logger.TraceInformation(LogTag.Tests, e.StackTrace);
            }
        }

        public void ReceiveRandomMessage(PeerAddress source)
        {
            int length = RandomSource.Generator.Next(800);
            TransportEnvelope envelope = new TransportEnvelope();
            byte[] bytes = new byte[length];

            RandomSource.Generator.NextBytes(bytes);
            envelope.Message.Write(bytes, length);

            this.MessageConsumer.ProcessMessage(envelope);
        }

        public void RandomMessageChain(PeerAddress address)
        {
            this.eventQueue.Schedule(100, this.RandomMessageChain, address);

            this.ReceiveRandomMessage(address);
        }

        /// <summary>
        /// Represents a network link to a remote peer.
        /// </summary>
        [DontPerformCoverage]
        private class Link
        {
            public MockTransportLayer Remote;
            public int Latency;
            public GenericCallBackReturn<bool, MockTransportLayer, TransportEnvelope> Filter;

            public Link(MockTransportLayer remote, int latencyMs, GenericCallBackReturn<bool, MockTransportLayer, TransportEnvelope> filter)
            {
                this.Remote = remote;
                this.Latency = latencyMs;
                this.Filter = filter;
            }
        }
    }

    /*
    [TestFixture]
    public class MockTransportLayerTester
    {

        // Test that we can create two MockTransportLayers.       
        private MockTransportLayer mMockProtocolLayerA;
        private MockTransportLayer mMockProtocolLayerB;

        private TestProtocolLayer2 mPeerALayer2;
        private TestProtocolLayer2 mPeerBLayer2;


        NetworkContext mPeerA = new NetworkContext();
        NetworkContext mPeerB = new NetworkContext();

        [SetUp]
        public void Initialize()
        {
            this.mPeerA.PublicAddress =  PeerAddress.GetAddress("127.0.0.1", 1);
            this.mPeerB.PublicAddress = PeerAddress.GetAddress("127.0.0.1", 2);

            this.mMockProtocolLayerA = new MockTransportLayer(mPeerA);
            this.mMockProtocolLayerB = new MockTransportLayer(mPeerB);

            this.mPeerALayer2 = new TestProtocolLayer2(this.mMockProtocolLayerA);
            this.mPeerBLayer2 = new TestProtocolLayer2(this.mMockProtocolLayerB);

            this.mPeerALayer2.SendText = "From peerA.";
            this.mPeerALayer2.ExpectedText = "From peerB.";
         //   this.mMockProtocolLayerA.AttachChildLayer(this.mPeerALayer2);

            this.mPeerBLayer2.SendText = "From peerB.";
            this.mPeerBLayer2.ExpectedText = "From peerA.";
          //  this.mMockProtocolLayerB.AttachChildLayer(this.mPeerBLayer2);           
        
        }

        [TearDown]
        public void Terminate()
        {
        }

        [Test]
        public void JoinTest()
        {

            // Test that we can join two MockTransportLayers together.
            // Test that we can specify the latency, bandwidth and reliability per connection direction.
            mMockProtocolLayerA.AddLink(this.mMockProtocolLayerB, 0, 1.0, 64);
            mMockProtocolLayerB.AddLink(this.mMockProtocolLayerA, 0, 1.0, 64);
        }

        [Test]
        public void SendMessage()
        {
            TransportEnvelope envelope;
            TimeSpan runTime = new TimeSpan(0, 0, 0, 0, 50);

            JoinTest();

            // Test that a messages can be sent from A to B.
            envelope = this.mPeerALayer2.GetMessageFor(mPeerB.PublicAddress, QualityOfService.Unreliable);
            this.mPeerALayer2.SendMessage(envelope);
            NetworkEventQueue.Instance.RunUntil(NetworkEventQueue.Instance.Now + runTime);

            Assert.IsTrue(this.mPeerBLayer2.mHasCallTestCB, "Failed to recieve message.");

            // Test that a messages can be sent from B to A.
            envelope = this.mPeerBLayer2.GetMessageFor(mPeerA.PublicAddress, QualityOfService.Unreliable);
            this.mPeerBLayer2.SendMessage(envelope);
            NetworkEventQueue.Instance.RunUntil(NetworkEventQueue.Instance.Now + runTime);

            Assert.IsTrue(this.mPeerALayer2.mHasCallTestCB, "Failed to recieve message.");              
        }

        [Test, ExpectedException(typeof(Exception))]
        public void FailToSendTest()
        {
            JoinTest();

            // Test that attempting to send to unknown destination throws an exception. 
            TransportEnvelope envelope = this.mPeerALayer2.GetMessageFor(PeerAddress.Nowhere, QualityOfService.Unreliable);
            this.mPeerALayer2.SendMessage(envelope);
        }


        [Test, ExpectedException(typeof(Exception))]
        public void RemovePeerTest()
        {
            JoinTest();

            // Test that we can remove a peer.
            mMockProtocolLayerA.RemoveLink(this.mMockProtocolLayerB);
            TransportEnvelope envelope = this.mPeerALayer2.GetMessageFor(mPeerB.PublicAddress, QualityOfService.Unreliable);

            // Expect a "Can't find mock peer" excpetion.
            this.mPeerALayer2.SendMessage(envelope);
        }

        [Test]
        public void LatencyTest()
        {
            NetworkEventQueue.Instance.Reset();
            TimeSpan runTime = new TimeSpan(0, 0, 0, 0, 1500);

            TimeSpan start;
            TimeSpan arrive;
            TimeSpan end;

            // Test that the latency argument for peers is correct.
            this.mMockProtocolLayerA.AddLink(this.mMockProtocolLayerB, 100, 1.0, 100);
            this.mMockProtocolLayerB.AddLink(this.mMockProtocolLayerA, 300, 1.0, 100);

            TransportEnvelope envelope = this.mPeerALayer2.GetMessageFor(this.mPeerB.PublicAddress, QualityOfService.Unreliable);
            this.mPeerALayer2.SendMessage(envelope);

            // This will instantiate objects so that they are not included in the timings later on.
            NetworkEventQueue.Instance.RunAll();

            this.mPeerALayer2.SendMessage(envelope);

            start = NetworkEventQueue.Instance.Now;
            NetworkEventQueue.Instance.RunUntil(start + runTime);
            arrive = this.mMockProtocolLayerB.TimeOfLastArrival;
            end = this.mPeerBLayer2.MessageReceiveTime;

            Assert.IsTrue(this.mPeerBLayer2.mHasCallTestCB, "Failed to recieve message.");

            TimeSpan measuredLatency = arrive - start;
            TimeSpan measuredProcessDelay = end - arrive;

            Console.WriteLine("Mock message took {0}ms to arrive.", measuredLatency.TotalMilliseconds);
            Console.WriteLine("Mock message took {0}ms to process.", measuredProcessDelay.TotalMilliseconds);

            Assert.IsTrue(measuredLatency.TotalMilliseconds > 85, "Mock message was too fast.");
            Assert.IsTrue(measuredLatency.TotalMilliseconds < 115, "Mock message was too slow.");

            Assert.IsTrue(measuredProcessDelay.TotalMilliseconds < 2, String.Format("Mock message took too long to process ({0}ms).", measuredProcessDelay.TotalMilliseconds));

            envelope = this.mPeerBLayer2.GetMessageFor(this.mPeerA.PublicAddress, QualityOfService.Unreliable);
            this.mPeerBLayer2.SendMessage(envelope);

            start = NetworkEventQueue.Instance.Now;
            NetworkEventQueue.Instance.RunUntil(start + runTime);
            arrive = this.mMockProtocolLayerA.TimeOfLastArrival;
            end = this.mPeerALayer2.MessageReceiveTime;

            Assert.IsTrue(this.mPeerALayer2.mHasCallTestCB, "Failed to recieve message.");

            measuredLatency = arrive - start;
            measuredProcessDelay = end - arrive;

            Console.WriteLine("Mock message took {0}ms to arrive.", measuredLatency.TotalMilliseconds);
            Console.WriteLine("Mock message took {0}ms to process.", measuredProcessDelay.TotalMilliseconds);

            Assert.IsTrue(measuredLatency.TotalMilliseconds > 285, "Mock message was too fast.");
            Assert.IsTrue(measuredLatency.TotalMilliseconds < 315, "Mock message was too slow.");

            Assert.IsTrue(measuredProcessDelay.TotalMilliseconds < 2, "Mock message took too long to process.");

        }

        [Test]
        public void ReliabilityTest()
        {
            TimeSpan runTime = new TimeSpan(0, 0, 0, 0, 1000);
            int numSent = 500;

            // Test that the latency argument for peers is correct.
            mMockProtocolLayerA.AddLink(this.mMockProtocolLayerB, 1, 0.3, 100);
            TransportEnvelope envelope = this.mPeerALayer2.GetMessageFor(this.mPeerB.PublicAddress, QualityOfService.Unreliable);

            for (int i = 0; i < numSent; i++)
            {
                this.mPeerALayer2.SendMessage(envelope);
            }

            NetworkEventQueue.Instance.RunUntil(NetworkEventQueue.Instance.Now + runTime);

            // Test that the reliability argument for peers is correct.
            double percentageArrived = ((double)this.mPeerBLayer2.MessagesArrivedCount / (double)numSent) * 100.0;

            Console.WriteLine("" + percentageArrived + "% messages arrived.");

            Assert.IsTrue(20 < percentageArrived, "Too few messages arrived.");
            Assert.IsTrue(40 > percentageArrived, "Too many messages arrived.");

            Assert.AreEqual(numSent, this.mMockProtocolLayerA.NumSentMessages);
        }

        [Test, Ignore]
        public void BandwidthTest()
        {
            // Test that the bandwidth argument for peers is correct.
            Assert.IsTrue(false, "Not implemented yet.");
        }
      
        [Test]
        public void StatisticsTest()
        {
            JoinTest();
            
            TimeSpan runTime = new TimeSpan(0, 0, 0, 0, 50);
            TransportEnvelope envelope;

            envelope = this.mPeerALayer2.GetMessageFor(this.mPeerB.PublicAddress, QualityOfService.Unreliable);
            this.mPeerALayer2.SendMessage(envelope);
            this.mPeerALayer2.SendMessage(envelope);
            NetworkEventQueue.Instance.RunUntil(NetworkEventQueue.Instance.Now + runTime);

            Assert.IsTrue(this.mPeerBLayer2.mHasCallTestCB, "Failed to recieve message.");

            // Test that the statistics are correct for  :
            // Outbound bandwidth usage.
            Assert.AreEqual(envelope.Length * 2, this.mMockProtocolLayerA.SentBytes, "Failed to gather send stats.");
      
            // Inbound bandwidth usage.
            Assert.AreEqual(envelope.Length * 2, this.mMockProtocolLayerB.ReceivedBytes, "Failed to gather send stats.");
        
            // Number of sent messages.
            Assert.AreEqual(2, this.mMockProtocolLayerA.NumSentMessages);
            Assert.AreEqual(0, this.mMockProtocolLayerB.NumSentMessages);

            // Number of received messages.
            Assert.AreEqual(0, this.mMockProtocolLayerA.NumReceivedMessages);
            Assert.AreEqual(2, this.mMockProtocolLayerB.NumReceivedMessages);
        }
    }
     */
}
