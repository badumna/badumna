﻿//-----------------------------------------------------------------------
// <copyright file="DataSeriesTests.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2011 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BadumnaTests.Utilities
{
    using System;
    using Badumna.Utilities;
    using NUnit.Framework;

    [TestFixture]
    internal class DataSeriesTests
    {
        private static Random random = new Random();
        private DataSeries ds;

        [Ignore]
        [TestCase(0, 1)]
        [TestCase(100, 100)]
        [TestCase(300, 600)]
        public void StdDevIsAccurateForRandomSamplesFromNormalDistribution(double mean, double stdDev)
        {
            this.CreateSeries(1000, mean, stdDev);
            Console.WriteLine("{0:0.000} -> {1:0.000}", this.ds.StdDev, stdDev);
            Assert.IsTrue(this.ds.StdDev > stdDev * 0.9);
            Assert.IsTrue(this.ds.StdDev < stdDev * 1.1);
        }

        [Ignore]
        [TestCase(0, 1)]
        [TestCase(100, 100)]
        [TestCase(300, 600)]
        public void MeanIsAccurateForRandomSamplesFromNormalDistribution(double mean, double stdDev)
        {
            this.CreateSeries(1000, mean, stdDev);
            Console.WriteLine("{0:0.000} -> {1:0.000}", this.ds.Mean, mean);
            Assert.IsTrue(this.ds.Mean > mean - (0.1 * stdDev));
            Assert.IsTrue(this.ds.Mean < mean + (0.1 * stdDev));
        }

        private static double RandomNormal(double mean, double stdDev)
        {
            return mean + (stdDev * (Math.Cos(2 * Math.PI * random.NextDouble()) * Math.Sqrt(-2 * Math.Log(random.NextDouble()))));
        }

        private void CreateSeries(int size, double mean, double stdDev)
        {
            this.ds = new DataSeries(size);
            for (int i = 0; i < size; i++)
            {
                this.ds.Add(RandomNormal(mean, stdDev));
            }
        }
    }
}
