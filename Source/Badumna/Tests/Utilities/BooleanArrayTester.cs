﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using Badumna.Utilities;
using BadumnaTests.Core;

namespace BadumnaTests.Utilities
{
    [TestFixture]
    public class BooleanArrayTester : ParseableTestHelper
    {
        class BooleanArrayParseableTester : ParseableTester<BooleanArray>
        {
            public override ICollection<BooleanArray> CreateExpectedValues()
            {
                List<BooleanArray> expected = new List<BooleanArray>();

                BooleanArray array = new BooleanArray();  // Just one byte
                array[0] = true;
                array[2] = true;
                expected.Add(array);

                array = new BooleanArray();  // Exactly 1 ulong
                array[5] = true;
                array[32] = true;
                array[63] = true;
                expected.Add(array);

                array = new BooleanArray();  // Exactly 2 ulongs
                array[5] = true;
                array[32] = true;
                array[126] = true;
                expected.Add(array);

                array = new BooleanArray();  // Ensure bytes of the last ulong don't get swapped
                array[0] = true;
                array[2] = true;
                array[9] = true;
                array[11] = true;
                expected.Add(array);

                array = new BooleanArray();  // Second ulong, accessed but set to same as default value
                array[64] = false;
                expected.Add(array);

                array = new BooleanArray();  // Second ulong, accessed but set to same as default value
                array[76] = false;
                expected.Add(array);

                return expected;
            }
        }

        public BooleanArrayTester()
            : base(new BooleanArrayParseableTester())
        {
        }

        [Test]
        public void AnyTest()
        {
            BooleanArray array = new BooleanArray();
            array.SetAll(true);
            Assert.IsTrue(array.Any());

            array[0] = false;
            Assert.IsTrue(array.Any());

            array.SetAll(false);
            Assert.IsFalse(array.Any());

            array = new BooleanArray();  // Less than one ulong
            array[0] = array[1] = array[2] = true;
            Assert.IsTrue(array.Any());

            array[1] = false;
            Assert.IsTrue(array.Any());

            array[0] = array[2] = false;
            Assert.IsFalse(array.Any());
        }

        [Test]
        public void OrTest()
        {
            BooleanArray all = new BooleanArray(true);
            BooleanArray none = new BooleanArray();

            BooleanArray test = new BooleanArray(all);
            Assert.IsTrue(test.Equals(all));
            Assert.IsFalse(test.Equals(none));
            test.Or(all);
            Assert.IsTrue(test.Equals(all));
            test.Or(none);
            Assert.IsTrue(test.Equals(all));


            test = new BooleanArray(none);
            test.Or(none);
            Assert.IsTrue(test.Equals(none));
            test.Or(all);
            Assert.IsTrue(test.Equals(all));


            test = new BooleanArray();
            test[0] = true;
            test[2] = true;
            BooleanArray test1 = new BooleanArray();
            test1[1] = true;
            test1[3] = true;
            test.Or(test1);
            for (int i = 0; i < 4; i++)
            {
                Assert.IsTrue(test[i]);
            }
            Assert.IsFalse(test1[0]);
            Assert.IsFalse(test[4]);
            Assert.IsFalse(test.Equals(all));
            test.Or(all);
            Assert.IsTrue(test.Equals(all));


            test = new BooleanArray();
            test[64] = true;
            test1 = new BooleanArray();
            test1[0] = true;
            test.Or(test1);
            Assert.IsTrue(test[0]);
            Assert.IsTrue(test[64]);
            test[0] = test[64] = false;
            Assert.IsTrue(test.Equals(none));
        }
    }
}
