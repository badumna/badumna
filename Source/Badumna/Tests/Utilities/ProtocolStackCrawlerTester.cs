﻿using System;
using System.IO;
using Badumna.Core;
using Badumna.Utilities;
using BadumnaTests.Core;
using NUnit.Framework;

namespace BadumnaTests.Utilities
{
    [TestFixture]
    public class ProtocolStackCrawlerTester
    {
        [SetUp]
        public void Initialize()
        {
        }

        [TearDown]
        public void Terminate()
        {
        }

        [Test]
        public void TraverseTest()
        {
            ITime timeKeeper = new NetworkEventQueue();

            TestProtocolComponent1<MockEnvelope> entryPoint = new TestProtocolComponent1<MockEnvelope>();
            new TestProtocolComponent2<MockEnvelope>(new TestProtocolComponent2<MockEnvelope>(entryPoint, timeKeeper), timeKeeper);

            TextCrawler crawler = new TextCrawler(new StreamWriter(new MemoryStream())); //Console.Out);

            entryPoint.Traverse(crawler);
        }
    }
}
