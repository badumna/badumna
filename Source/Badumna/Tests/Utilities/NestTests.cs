﻿//------------------------------------------------------------------------------
// <copyright file="NestTests.cs" company="National ICT Australia Limited">
//     Copyright (c) 2012 National ICT Australia Limited. All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Badumna.Utilities;
using NUnit.Framework;

namespace BadumnaTests.Utilities
{
    [TestFixture]
    public class NestTests
    {
        [Test]
        public void GetWithoutSetFails()
        {
            Nest nest = new Nest();
            Assert.Throws<KeyNotFoundException>(() => nest.Get<Base>());
        }

        [Test]
        public void ImplicitSetThenGetSucceeds()
        {
            Nest nest = new Nest();
            Base base_ = new Base();
            nest.Set(base_);  // Generic type parameter is implicitly ClassA
            Assert.AreSame(base_, nest.Get<Base>());
        }

        [Test]
        public void SetDerivedThenGetBaseFails()
        {
            Nest nest = new Nest();
            Derived derived = new Derived();
            nest.Set(derived);
            Assert.Throws<KeyNotFoundException>(() => nest.Get<Base>());
        }

        [Test]
        public void SetAsBaseSucceeds()
        {
            Nest nest = new Nest();
            Base base_ = new Derived();
            nest.Set(base_);
            Assert.AreSame(base_, nest.Get<Base>());
        }

        [Test]
        public void RepeatedSetFails()
        {
            Nest nest = new Nest();
            nest.Set(new Base());
            Assert.Throws<ArgumentException>(() => nest.Set(new Base()));
        }

        [Test]
        public void ConstructWithNoDependenciesSucceeds()
        {
            Nest nest = new Nest();
            Assert.IsNotNull(nest.Construct<Base>());
        }

        [Test]
        public void ConstructAndSetSucceeds()
        {
            Nest nest = new Nest();
            Base base_ = nest.ConstructAndSet<Base>();
            Assert.IsNotNull(base_);
            Assert.AreSame(base_, nest.Get<Base>());
        }

        [Test]
        public void ConstructWithMissingDependencyFails()
        {
            Nest nest = new Nest();
            Assert.Throws<InvalidOperationException>(() => nest.Construct<SingleConstructor>());
        }

        [Test]
        public void ConstructWithSingleDependencySucceeds()
        {
            Nest nest = new Nest();
            nest.Set("default string");
            Assert.IsNotNull(nest.Construct<SingleConstructor>());
        }

        [Test]
        public void ConstructChoosesSimplestKnownConstructor()
        {
            Nest nest = new Nest();
            nest.Set("default string");
            nest.Set<int>(42);
            nest.Set<float>(-1);
            
            /* We have four constructors, (string, int, float), (string, float), (string, double), and (string, string).
               (string, float) should always be chosen before (string, int, float); (string, string) should never
               be chosen (seeing as we only store one canonical instance of each type and presumably if the constructor
               needs two of the same type they should be different instances).  Then there are two
               constructors with the same number of parameters.  However, double is not known, so
               the (string, float) constructor should be chosen. */

            Assert.AreEqual("string_float", nest.Construct<MultipleConstructors>().Constructor);
        }

        [Test]
        public void ConstructWithMultipleSimplestKnownConstructorsFails()
        {
            Nest nest = new Nest();
            nest.Set("default string");
            nest.Set<float>(-1);
            nest.Set<double>(-2);

            // Can't distinguish between the (string, float) and the (string, double) constructors.
            Assert.Throws<InvalidOperationException>(() => nest.Construct<MultipleConstructors>());
        }

        [Test]
        public void ConstructorWithMoreParametersIsUsedIfShorterParametersConflict()
        {
            Nest nest = new Nest();
            nest.Set("default string");
            nest.Set<float>(0);
            nest.Set<double>(0);
            nest.Set<long>(42);
            nest.Set<bool>(true);

            // We know both (string, float) and (string, double) so can't distinguish between these.
            // Must choose long constructor.
            Assert.AreEqual("string_long_float_bool", nest.Construct<MultipleConstructors>().Constructor);
        }

        [Test]
        public void AddingANewCannonicalInstanceDoesNotChangeTheConstructor()
        {
            Nest nest = new Nest();
            nest.Set("default string");
            nest.Set<float>(-1);
            Assert.AreEqual("string_float", nest.Construct<MultipleConstructors>().Constructor);
            nest.Set<ushort>(1);
            Assert.AreEqual("string_float", nest.Construct<MultipleConstructors>().Constructor);
        }

        [Test]
        public void ConstructWithPrivateConstructorSucceeds()
        {
            Nest nest = new Nest();
            Assert.IsNotNull(nest.Construct<PrivateConstructor>());
        }

        [Test]
        public void ConstructWithInternalConstructorSucceeds()
        {
            Nest nest = new Nest();
            Assert.IsNotNull(nest.Construct<InternalConstructor>());
        }

        [Test]
        public void ConstructStructWithNonDefaultConstructorSucceeds()
        {
            Nest nest = new Nest();

            // All structs have a default constructor with no parameters.  
            // We haven't given the nest an int, so this must choose the
            // default constructor.
            Assert.IsNotNull(nest.Construct<StructWithNonDefaultConstructor>());
        }

        private struct StructWithNonDefaultConstructor
        {
            private int value;

            public StructWithNonDefaultConstructor(int value)
            {
                this.value = value;
            }
        }

        private class Base
        {
        }

        private class Derived : Base
        {
        }

        private class SingleConstructor
        {
            public SingleConstructor(string s)
            {
            }
        }

        private class MultipleConstructors
        {
            public MultipleConstructors(string s, int i, float f)
            {
                this.Constructor = "string_int_float";
            }

            public MultipleConstructors(string s, float f)
            {
                this.Constructor = "string_float";
            }

            public MultipleConstructors(string s, double f)
            {
                this.Constructor = "string_double";
            }

            public MultipleConstructors(string s1, string s2)
            {
                this.Constructor = "string_string";
            }

            public MultipleConstructors(string s, long l, float f, bool b)
            {
                this.Constructor = "string_long_float_bool";
            }

            public MultipleConstructors(ushort u)
            {
                this.Constructor = "ushort";
            }

            public string Constructor { get; private set; }
        }

        private class InternalConstructor
        {
            internal InternalConstructor()
            {
            }
        }

        private class PrivateConstructor
        {
            private PrivateConstructor()
            {
            }
        }
    }
}
