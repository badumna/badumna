﻿//-----------------------------------------------------------------------
// <copyright file="RegularTaskTests.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2011 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BadumnaTests.Utilities
{
    using System;
    using Badumna.Utilities;
    using NUnit.Framework;

    [TestFixture]
    internal class RegularTaskTests
    {
        [Test]
        public void ConstructorTakingGenericCallbackSetsPeriodCorrectlyForLargePeriods()
        {
            TimeSpan period = TimeSpan.MaxValue;
            RegularTask rt = new RegularTask(string.Empty, period, null, null, null);
            Assert.AreEqual(period.TotalMilliseconds, rt.PeriodMilliseconds);
        }

        [Test]
        public void ConstructorTakingDelegateSetsPeriodCorrectlyForLargePeriods()
        {
            TimeSpan period = TimeSpan.MaxValue;
            RegularTask rt = new RegularTask(string.Empty, period, null, null, null);
            Assert.AreEqual(period.TotalMilliseconds, rt.PeriodMilliseconds);
        }
    }
}
