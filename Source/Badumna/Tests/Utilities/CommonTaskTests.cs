﻿//------------------------------------------------------------------------------
// <copyright file="CommonTaskTests.cs" company="National ICT Australia Limited">
//     Copyright (c) 2012 National ICT Australia Limited. All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System;
using Badumna.Utilities;
using NUnit.Framework;

namespace BadumnaTests.Utilities
{
    [TestFixture]
    public class CommonTaskTests
    {
        [Test]
        public void SingleScheduleExecutesAfterSpecifiedDelay()
        {
            var eventQueue = new Simulator();
            bool called = false;
            var task = new CommonTask(eventQueue, eventQueue, () => called = true);

            var delay = TimeSpan.FromMilliseconds(10);
            task.Schedule(delay, this);
            eventQueue.RunUntil(
                () => called,
                delay,
                () => Assert.Fail("Action never called"));
            Assert.AreEqual(eventQueue.Now, delay, "Action called too early");
        }

        [TestCase(true), TestCase(false)]
        public void DoubleScheduleExecutesAfterEarliestDelay(bool earlyFirst)
        {
            var eventQueue = new Simulator();
            bool called = false;
            var task = new CommonTask(eventQueue, eventQueue, () => called = true);

            var delay1 = TimeSpan.FromMilliseconds(10);
            var delay2 = TimeSpan.FromMilliseconds(20);

            if (earlyFirst)
            {
                task.Schedule(delay1, this);
                task.Schedule(delay2, this);
            }
            else
            {
                task.Schedule(delay2, this);
                task.Schedule(delay1, this);
            }

            eventQueue.RunUntil(
                () => called,
                delay2,
                () => Assert.Fail("Action never called"));

            Assert.AreEqual(eventQueue.Now, delay1, "Action called at wrong time");
        }

        [Test]
        public void SingleScheduleCancelsCorrectly()
        {
            var eventQueue = new Simulator();
            bool called = false;
            var task = new CommonTask(eventQueue, eventQueue, () => called = true);

            var delay = TimeSpan.FromMilliseconds(10);
            task.Schedule(delay, this);
            eventQueue.RunFor(delay - TimeSpan.FromMilliseconds(1));
            Assert.IsFalse(called, "Action called too early");
            task.Cancel(this);
            eventQueue.RunFor(delay);
            Assert.IsFalse(called, "Action called when it should have been cancelled");
        }

        [TestCase(true), TestCase(false)]
        public void DoubleScheduleCancelsCorrectly(bool cancelBoth)
        {
            var eventQueue = new Simulator();
            bool called = false;
            var task = new CommonTask(eventQueue, eventQueue, () => called = true);

            var delay1 = TimeSpan.FromMilliseconds(10);
            var delay2 = TimeSpan.FromMilliseconds(20);

            object one = new object();
            object two = new object();
            
            task.Schedule(delay1, one);
            task.Schedule(delay2, two);
            
            eventQueue.RunFor(delay1 - TimeSpan.FromMilliseconds(1));
            Assert.IsFalse(called, "Action called too early");

            task.Cancel(one);
            if (cancelBoth)
            {
                task.Cancel(two);
            }

            eventQueue.RunFor(delay2);
            
            Assert.IsTrue(cancelBoth != called, "Action called when it should have been cancelled; or not called when it wasn't cancelled");
        }
    }
}
