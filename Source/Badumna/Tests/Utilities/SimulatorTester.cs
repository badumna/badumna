﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using Badumna.Utilities;
using Badumna.Core;

namespace BadumnaTests.Utilities
{
    [TestFixture]
    public class SimulatorTester
    {
        private bool mHasBeenCalled = false;

        [SetUp]
        public void Initialize()
        {
            this.mHasBeenCalled = false;
        }

        [Test]
        public void HarnessTest()
        {
        }

        public void EventCB()
        {
            this.mHasBeenCalled = true;
        }

        [Test]
        public void VirtualTimeTest()
        {
            Simulator eventQueue = new Simulator();

            Assert.AreEqual(0, eventQueue.Count);

            NetworkEvent ev = eventQueue.Schedule(1, this.EventCB);

            // Test that events put in the Simulator run on simulation time.            
            Assert.AreEqual(TimeSpan.FromMilliseconds(1), ev.Time);
            Assert.AreEqual(1, eventQueue.Count);

            eventQueue.RunFor(TimeSpan.FromMilliseconds(100));
            Assert.IsTrue(this.mHasBeenCalled, "Event never called.");
        }
    }
}
