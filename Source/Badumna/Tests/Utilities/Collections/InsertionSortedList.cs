﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using BadumnaTests.Core;
using Badumna.Utilities;
using Badumna.Core;

namespace BadumnaTests.Utilities.Collections
{
    [TestFixture]
    [DontPerformCoverage]
    public class InsertionSortedListTester
    {

        [Test]
        public void InsertInOrderTest()
        {
            InsertionSortedList<int> list = new InsertionSortedList<int>(3);

            list.Add(0);
            list.Add(1);
            list.Add(2);
            list.Add(3);
            list.Add(4);

            Assert.AreEqual(0, list[0]);
            Assert.AreEqual(1, list[1]);
            Assert.AreEqual(2, list[2]);
            Assert.AreEqual(3, list[3]);
            Assert.AreEqual(4, list[4]);
        }

        [Test]
        public void InsertInReverseOrderTest()
        {
            InsertionSortedList<int> list = new InsertionSortedList<int>(3);

            list.Add(4);
            list.Add(3);
            list.Add(2);
            list.Add(1);
            list.Add(0);

            Assert.AreEqual(0, list[0]);
            Assert.AreEqual(1, list[1]);
            Assert.AreEqual(2, list[2]);
            Assert.AreEqual(3, list[3]);
            Assert.AreEqual(4, list[4]);
        }

        [Test]
        public void InsertOutOfOrderTest()
        {
            InsertionSortedList<int> list = new InsertionSortedList<int>(3);

            list.Add(1);
            list.Add(2);
            list.Add(0);
            list.Add(4);
            list.Add(3);

            Assert.AreEqual(0, list[0]);
            Assert.AreEqual(1, list[1]);
            Assert.AreEqual(2, list[2]);
            Assert.AreEqual(3, list[3]);
            Assert.AreEqual(4, list[4]);
        }

        [Test]
        public void InsertDuplicateTest()
        {
            InsertionSortedList<int> list = new InsertionSortedList<int>(3);

            list.Add(1);
            list.Add(0);
            list.Add(2);
            list.Add(0);
            list.Add(4);
            list.Add(0);
            list.Add(3);

            Assert.AreEqual(0, list[0]);
            Assert.AreEqual(0, list[1]);
            Assert.AreEqual(0, list[2]);
            Assert.AreEqual(1, list[3]);
            Assert.AreEqual(2, list[4]);
            Assert.AreEqual(3, list[5]);
            Assert.AreEqual(4, list[6]);
        }

        [Test]
        public void InsertInRandomOrderTest()
        {
            Random generator = new Random((int)DateTime.Now.Ticks);
            InsertionSortedList<int> list = new InsertionSortedList<int>();

            for (int i = 0; i < 1000; i++)
            {
                list.Add(generator.Next());
            }

            int lastNum = 0;
            for (int i = 0; i < 100; i++)
            {
                Assert.LessOrEqual(lastNum, list[i]);
                lastNum = list[i];
            }
        }
    }
}
