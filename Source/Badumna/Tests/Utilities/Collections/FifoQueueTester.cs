﻿using System;
using System.Collections.Generic;
using System.Text;
using Badumna.Core;
using Badumna.Utilities;
using BadumnaTests.Core;
using NUnit.Framework;

namespace BadumnaTests.Utilities.Collections
{
    [TestFixture]
    [DontPerformCoverage]
    public class FifoQueueTester
    {
        [Test]
        public void AddRemoveTest()
        {
            FifoQueue<int> queue = new FifoQueue<int>(10);

            // Test that we can add and remove items
            for (int i = 0; i < 9; i++)
            {
                queue.Push(i);
                Assert.AreEqual(0, queue.Peek());
            }

            for (int i = 0; i < 9; i++)
            {
                Assert.AreEqual(9 - i, queue.Count, "Incorrect count");
                Assert.AreEqual(i, queue.Pop(), "Remove returned incorrect item");
            }
            Assert.AreEqual(0, queue.Count, "Incorrect count");

            // Test overflow
            for (int i = 0; i < 73; i++)
            {
                queue.Push(i);
                queue.Push(i);

                Assert.AreEqual(2, queue.Count, "Incorrect count");
                Assert.AreEqual(i, queue.Pop(), "Remove returned incorrect item");
                Assert.AreEqual(i, queue.Pop(), "Remove returned incorrect item");
                Assert.AreEqual(0, queue.Count, "Incorrect count");
            }

            for (int i = 0; i < 19; i++)
            {
                queue.Push(i);

                Assert.AreEqual(1, queue.Count, "Incorrect count");
                Assert.AreEqual(i, queue.Pop(), "Remove returned incorrect item");
                Assert.AreEqual(0, queue.Count, "Incorrect count");
            }

        }

        [Test]
        public void OverCapityTest()
        {
            FifoQueue<int> queue = new FifoQueue<int>(10);

            // Test that if we add more than capacity that the oldest are discarded
            for (int i = 0; i < 11; i++)
            {
                queue.Push(i);
            }

            Assert.AreEqual(10, queue.Count, "Incorrect count");

            for (int i = 1; i < 11; i++)
            {
                Assert.AreEqual(11 - i, queue.Count, "Incorrect count");
                Assert.AreEqual(i, queue.Pop(), "Remove returned incorrect item");
            }

            Assert.AreEqual(0, queue.Count, "Incorrect count");
        }

        [Test]
        public void EmptyRemoveTest()
        {
            FifoQueue<int> queue = new FifoQueue<int>(10);

            // Test empty remove
            for (int i = 0; i < 3; i++)
            {
                queue.Push(i);
            }

            for (int i = 0; i < 10; i++)
            {
                Assert.AreEqual(Math.Max(3 - i, 0), queue.Count, "Incorrect count");
                if (i < 3)
                {
                    Assert.AreEqual(i, queue.Pop(), "Remove returned incorrect item");
                }
                else
                {
                    Assert.AreEqual(0, queue.Pop(), "Remove returned incorrect item");
                }
            }

            Assert.AreEqual(0, queue.Count, "Incorrect count");
        }
    }
}
