﻿using System;
using System.Collections.Generic;
using Badumna.Core;
using Badumna.Utilities;
using BadumnaTests.Core;
using NUnit.Framework;

namespace BadumnaTests.Utilities.Collections
{
    [TestFixture]
    [DontPerformCoverage]
    public class ShortListTester : ParseableTestHelper
    {

        public ShortListTester()
            : base(new ParseableTesterClass())
        { }


        // TODO : Add more test cases

        class ParseableTesterClass : ParseableTester<ShortList<int>>
        {
            public override void AssertAreEqual(ShortList<int> expected, ShortList<int> actual)
            {
                Assert.AreEqual(expected.Count, actual.Count, "Count mismatch");

                int[] expectedAsArray = new int[expected.Count];
                int[] actualAsArray = new int[actual.Count];

                expected.CopyTo(expectedAsArray, 0);
                actual.CopyTo(actualAsArray, 0);

                for (int i = 0; i < expected.Count; i++)
                {
                    Assert.AreEqual(expectedAsArray[i], actualAsArray[i], String.Format("item {0} is incorrect", i));
                }
            }

            public override ICollection<ShortList<int>> CreateExpectedValues()
            {
                List<ShortList<int>> expectedValues = new List<ShortList<int>>();

                ShortList<int> example1 = new ShortList<int>();
                ShortList<int> example2 = new ShortList<int>();
                ShortList<int> example3 = new ShortList<int>();
                ShortList<int> example4 = new ShortList<int>();
                ShortList<int> example5 = new ShortList<int>();

                expectedValues.Add(new ShortList<int>());

                example1.Add(999);
                expectedValues.Add(example1);

                example2.Add(22);
                example2.Add(23);
                expectedValues.Add(example2);

                for (int i = 0; i < byte.MaxValue - 1; i++)
                {
                    example3.Add(i);
                }
                expectedValues.Add(example3);

                return expectedValues;
            }
        }
    }
}
