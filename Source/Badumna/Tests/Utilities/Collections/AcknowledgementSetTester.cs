﻿using System;
using System.Collections.Generic;
using System.Text;
using Badumna.Core;
using Badumna.Utilities;
using BadumnaTests.Core;
using NUnit.Framework;

namespace BadumnaTests.Utilities.Collections
{
    [TestFixture]
    [DontPerformCoverage]
    public class AcknowledgementSetTester : ParseableTestHelper
    {
        public AcknowledgementSetTester()
            : base(new ParseableTesterClass())
        { }

        [Test]
        public void AddTest()
        {
            AcknowledgementSet ackSet = new AcknowledgementSet();

            Assert.AreEqual(0, ackSet.Count);

            ackSet.Add(new CyclicalID.UShortID(3));
            Assert.IsTrue(ackSet.Contains(new CyclicalID.UShortID(3)));
            Assert.AreEqual(1, ackSet.Count);

            ackSet.Add(new CyclicalID.UShortID(5));
            Assert.IsTrue(ackSet.Contains(new CyclicalID.UShortID(5)));
            Assert.IsFalse(ackSet.Contains(new CyclicalID.UShortID(4)));
            Assert.AreEqual(2, ackSet.Count);

            ackSet.Add(new CyclicalID.UShortID(4));
            Assert.IsTrue(ackSet.Contains(new CyclicalID.UShortID(4)));
            Assert.AreEqual(3, ackSet.Count);

            ackSet.Add(new CyclicalID.UShortID(1));
            Assert.IsTrue(ackSet.Contains(new CyclicalID.UShortID(1)));
            Assert.IsFalse(ackSet.Contains(new CyclicalID.UShortID(2)));
            Assert.AreEqual(4, ackSet.Count);

            ackSet.Add(new CyclicalID.UShortID(2));
            Assert.IsTrue(ackSet.Contains(new CyclicalID.UShortID(2)));
            Assert.AreEqual(5, ackSet.Count);

            ackSet.Add(new CyclicalID.UShortID(6));
            Assert.IsTrue(ackSet.Contains(new CyclicalID.UShortID(6)));
            Assert.AreEqual(6, ackSet.Count);

            ackSet.Add(new CyclicalID.UShortID(0));
            Assert.IsTrue(ackSet.Contains(new CyclicalID.UShortID(0)));
            Assert.AreEqual(7, ackSet.Count);

            CyclicalID.UShortID nextId = new CyclicalID.UShortID(0);

            foreach (CyclicalID.UShortID ack in ackSet)
            {
                Assert.AreEqual(ack, nextId);
                nextId.Increment();
            }

            ackSet.Clear();
            Assert.AreEqual(0, ackSet.Count);

            ackSet.Add(new CyclicalID.UShortID(ushort.MaxValue - 1));
            ackSet.Add(new CyclicalID.UShortID(1));
            Assert.IsTrue(ackSet.Contains(new CyclicalID.UShortID(ushort.MaxValue - 1)));
            Assert.IsFalse(ackSet.Contains(new CyclicalID.UShortID(ushort.MaxValue)));
            Assert.IsFalse(ackSet.Contains(new CyclicalID.UShortID(0)));
            Assert.IsTrue(ackSet.Contains(new CyclicalID.UShortID(1)));
            Assert.AreEqual(2, ackSet.Count);

            ackSet.Add(new CyclicalID.UShortID(0));
            Assert.IsTrue(ackSet.Contains(new CyclicalID.UShortID(ushort.MaxValue - 1)));
            Assert.IsFalse(ackSet.Contains(new CyclicalID.UShortID(ushort.MaxValue)));
            Assert.IsTrue(ackSet.Contains(new CyclicalID.UShortID(0)));
            Assert.IsTrue(ackSet.Contains(new CyclicalID.UShortID(1)));
            Assert.AreEqual(3, ackSet.Count);

            ackSet.Add(new CyclicalID.UShortID(ushort.MaxValue));
            Assert.IsTrue(ackSet.Contains(new CyclicalID.UShortID(ushort.MaxValue - 1)));
            Assert.IsTrue(ackSet.Contains(new CyclicalID.UShortID(ushort.MaxValue)));
            Assert.IsTrue(ackSet.Contains(new CyclicalID.UShortID(0)));
            Assert.IsTrue(ackSet.Contains(new CyclicalID.UShortID(1)));
            Assert.AreEqual(4, ackSet.Count);

            nextId = new CyclicalID.UShortID(ushort.MaxValue - 1);
            foreach (CyclicalID.UShortID ack in ackSet)
            {
                Assert.AreEqual(ack, nextId);
                nextId.Increment();
            }
        }

        [Test]
        public void CountTest()
        {
            AcknowledgementSet ackSet = new AcknowledgementSet();

            Assert.AreEqual(0, ackSet.Count);

            ackSet.Add(new CyclicalID.UShortID(200));
            ackSet.Add(new CyclicalID.UShortID(207));

            Assert.AreEqual(2, ackSet.Count);
        }

        private void TestAreEqual(AcknowledgementSet ackSet, List<int> nums)
        {
            // TODO : 
            // it seems to me that there shouldn't be duplicated elements in the list
            nums = RemoveDuplicates<int>(nums);
            Assert.AreEqual(ackSet.Count, nums.Count);

            foreach (int num in nums)
            {
                Assert.IsTrue(ackSet.Contains(new CyclicalID.UShortID((ushort)num)));
            }

            foreach (CyclicalID.UShortID ack in ackSet)
            {
                Assert.IsTrue(nums.Contains((int)ack.Value));
            }
        }

        private List<T> RemoveDuplicates<T>(List<T> original)
        {
            Dictionary<string, T> uniqueStore = new Dictionary<string, T>();

            foreach (T element in original)
            {
                if (!uniqueStore.ContainsKey(element.ToString()))
                {
                    uniqueStore.Add(element.ToString(), element);
                }
            }

            return new List<T>(uniqueStore.Values);
        }

        [Test]
        public void RandomAddRemoveTest()
        {
            Random generator = new Random((int)DateTime.Now.Ticks);
            List<int> range = new List<int>();
            List<int> nums = new List<int>();
            AcknowledgementSet ackSet = new AcknowledgementSet();

            for (int i = 0; i < 100; i++)
            {
                range.Add(i);
            }

            for (int i = 0; i < 100; i++)
            {
                int r = generator.Next(0, range.Count);
                nums.Add(range[r]);
                nums.Sort();
                ackSet.Add(new CyclicalID.UShortID((ushort)range[r]));
                this.TestAreEqual(ackSet, nums);
                range.RemoveAt(r);
            }

            for (int i = 0; i < 100; i++)
            {
                int j = generator.Next(0, nums.Count);
                ackSet.Remove(new CyclicalID.UShortID((ushort)nums[j]));
                nums.RemoveAt(j);
                nums.Sort();
                this.TestAreEqual(ackSet, nums);
            }
        }

        [Test]
        public void AddRemoveTest()
        {
            Random generator = new Random((int)DateTime.Now.Ticks);
            List<int> nums = new List<int>();
            AcknowledgementSet ackSet = new AcknowledgementSet();

            nums.Add(ushort.MaxValue - 1);
            nums.Add(ushort.MaxValue);
            nums.Add(1);
            nums.Add(6);
            nums.Add(7);
            nums.Add(99);
            nums.Add(98);

            foreach (int num in nums)
            {
                ackSet.Add(new CyclicalID.UShortID((ushort)num));
            }

            nums.Remove(99);
            nums.Sort();
            ackSet.Remove(new CyclicalID.UShortID((ushort)99));
            this.TestAreEqual(ackSet, nums);

            nums.Remove(98);
            nums.Sort();
            ackSet.Remove(new CyclicalID.UShortID((ushort)98));
            this.TestAreEqual(ackSet, nums);

            nums.Remove(1);
            nums.Sort();
            ackSet.Remove(new CyclicalID.UShortID((ushort)1));
            this.TestAreEqual(ackSet, nums);

            nums.Remove(ushort.MaxValue - 1);
            nums.Sort();
            ackSet.Remove(new CyclicalID.UShortID((ushort)ushort.MaxValue - 1));
            this.TestAreEqual(ackSet, nums);

            nums.Remove(ushort.MaxValue);
            nums.Sort();
            ackSet.Remove(new CyclicalID.UShortID((ushort)ushort.MaxValue));
            this.TestAreEqual(ackSet, nums);

            nums.Remove(6);
            nums.Sort();
            ackSet.Remove(new CyclicalID.UShortID((ushort)6));
            this.TestAreEqual(ackSet, nums);

            nums.Remove(7);
            nums.Sort();
            ackSet.Remove(new CyclicalID.UShortID((ushort)7));
            this.TestAreEqual(ackSet, nums);
        }

        [Test]
        public void AnotherAddRemoveTest()
        {
            Random generator = new Random((int)DateTime.Now.Ticks);
            List<int> nums = new List<int>();
            AcknowledgementSet ackSet = new AcknowledgementSet();

            nums.Add(ushort.MaxValue - 1);
            nums.Add(ushort.MaxValue);
            nums.Add(99);
            nums.Add(98);

            foreach (int num in nums)
            {
                ackSet.Add(new CyclicalID.UShortID((ushort)num));
            }

            nums.Remove(99);
            nums.Sort();
            ackSet.Remove(new CyclicalID.UShortID((ushort)99));
            this.TestAreEqual(ackSet, nums);

            nums.Remove(98);
            nums.Sort();
            ackSet.Remove(new CyclicalID.UShortID((ushort)98));
            this.TestAreEqual(ackSet, nums);

            nums.Remove(ushort.MaxValue - 1);
            nums.Sort();
            ackSet.Remove(new CyclicalID.UShortID((ushort)ushort.MaxValue - 1));
            this.TestAreEqual(ackSet, nums);

            nums.Remove(ushort.MaxValue);
            nums.Sort();
            ackSet.Remove(new CyclicalID.UShortID((ushort)ushort.MaxValue));
            this.TestAreEqual(ackSet, nums);
        }

        [Test]
        public void ClipToFirstMissingTest()
        {
            List<int> nums = new List<int>();
            AcknowledgementSet ackSet = new AcknowledgementSet();

            nums.Add(ushort.MaxValue - 1);
            nums.Add(ushort.MaxValue);
            nums.Add(1);
            nums.Add(6);
            nums.Add(7);
            nums.Add(99);
            nums.Add(98);

            foreach (int num in nums)
            {
                ackSet.Add(new CyclicalID.UShortID((ushort)num));
            }

            nums.RemoveAt(0);
            ackSet.ClipToFirstMissing();
            this.TestAreEqual(ackSet, nums);

            nums.RemoveAt(0);
            nums.Add(1);
            nums.Sort();
            ackSet.Add(new CyclicalID.UShortID(0));
            ackSet.ClipToFirstMissing();
            this.TestAreEqual(ackSet, nums);
        }

        [Test]
        public void FullClipToMissingTest()
        {
            AcknowledgementSet ackSet = new AcknowledgementSet();

            for (int i = 0; i < 10; i++)
            {
                ackSet.Add(new CyclicalID.UShortID((ushort)i));
            }
            Assert.AreEqual(10, ackSet.Count);

            ackSet.ClipToFirstMissing();
            Assert.AreEqual(1, ackSet.Count);
        }


        class ParseableTesterClass : ParseableTester<AcknowledgementSet>
        {
            public override void AssertAreEqual(AcknowledgementSet expected, AcknowledgementSet actual)
            {
                Assert.AreEqual(expected.Count, actual.Count, "Count mismatch");

                CyclicalID.UShortID[] expectedAsArray = new CyclicalID.UShortID[expected.Count];
                CyclicalID.UShortID[] actualAsArray = new CyclicalID.UShortID[actual.Count];

                expected.CopyTo(expectedAsArray, 0);
                actual.CopyTo(actualAsArray, 0);

                for (int i = 0; i < expected.Count; i++)
                {
                    Assert.AreEqual(expectedAsArray[i], actualAsArray[i], String.Format("Acknowledgement {0} is incorrect", i));
                }
            }

            public override ICollection<AcknowledgementSet> CreateExpectedValues()
            {
                List<AcknowledgementSet> expectedValues = new List<AcknowledgementSet>();

                AcknowledgementSet example1 = new AcknowledgementSet();
                AcknowledgementSet example2 = new AcknowledgementSet();
                AcknowledgementSet example3 = new AcknowledgementSet();
                AcknowledgementSet example4 = new AcknowledgementSet();
                AcknowledgementSet example5 = new AcknowledgementSet();
                AcknowledgementSet example6 = new AcknowledgementSet();
                AcknowledgementSet example7 = new AcknowledgementSet();
                AcknowledgementSet example8 = new AcknowledgementSet();

                expectedValues.Add(new AcknowledgementSet());

                example1.Add(new CyclicalID.UShortID(99));
                expectedValues.Add(example1);

                example2.Add(new CyclicalID.UShortID(109));
                example2.Add(new CyclicalID.UShortID(110));
                expectedValues.Add(example2);

                example3.Add(new CyclicalID.UShortID(200));
                example3.Add(new CyclicalID.UShortID(207));
                expectedValues.Add(example3);

                example4.Add(new CyclicalID.UShortID(300));
                example4.Add(new CyclicalID.UShortID(307));
                example4.Add(new CyclicalID.UShortID(301));
                example4.Add(new CyclicalID.UShortID(306));
                example4.Add(new CyclicalID.UShortID(302));
                expectedValues.Add(example4);

                example5.Add(new CyclicalID.UShortID(400));
                example5.Add(new CyclicalID.UShortID(407));
                example5.Add(new CyclicalID.UShortID(401));
                example5.Add(new CyclicalID.UShortID(406));
                example5.Add(new CyclicalID.UShortID(402));
                example5.Add(new CyclicalID.UShortID(403));
                example5.Add(new CyclicalID.UShortID(404));
                example5.Add(new CyclicalID.UShortID(405));
                expectedValues.Add(example5);

                example6.Add(new CyclicalID.UShortID(ushort.MaxValue - 1));
                example6.Add(new CyclicalID.UShortID(1));
                expectedValues.Add(example6);

                example7.Add(new CyclicalID.UShortID(ushort.MaxValue - 1));
                example7.Add(new CyclicalID.UShortID(0));
                example7.Add(new CyclicalID.UShortID(1));
                expectedValues.Add(example7);

                example8.Add(new CyclicalID.UShortID(0));
                example8.Add(new CyclicalID.UShortID(ushort.MaxValue));
                example8.Add(new CyclicalID.UShortID(ushort.MaxValue - 1));
                example8.Add(new CyclicalID.UShortID(1));
                expectedValues.Add(example8);

                return expectedValues;
            }
        }
    }
}
