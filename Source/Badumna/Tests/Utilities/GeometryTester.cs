﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using Badumna.Utilities;
using Badumna.DataTypes;

namespace BadumnaTests.Utilities
{
    [TestFixture]
    public class GeometryTester
    {
        [Test]
        public void VolumeTest()
        {
            // Zero radius sphere volume is zero
            Assert.AreEqual(0.0F, Geometry.SphereVolume(0.0F), "Zero radius sphere volume calc is incorrect");

            // Unit sphere volume is right
            double unitVol = Geometry.SphereVolume(1.0F);
            Assert.AreEqual("4.189", String.Format("{0:F3}", unitVol), "Unit sphere volume calc is incorrect");

            // Sphere volume scales with r^3
            Assert.AreEqual(String.Format("{0:F3}", unitVol * 5.0 * 5.0 * 5.0), String.Format("{0:F3}", Geometry.SphereVolume(5.0F)), "Non-unit sphere volume calc is incorrect");
        }

        [Test, ExpectedException("System.ArgumentOutOfRangeException")]
        public void VolumeExceptionTest()
        {
            Geometry.SphereVolume(-1.0F);
        }

        [Test]
        public void IntersectTest()
        {
            // Non-intersecting spheres
            Assert.IsFalse(Geometry.SpheresIntersect(new Vector3(1.0F, 0.0F, 0.0F), 1.0F, new Vector3(10.0F, 0.0F, 0.0F), 2.0F),
                "SpheresIntersect returned true for non-intersecting spheres");

            // Intersecting spheres
            Assert.IsTrue(Geometry.SpheresIntersect(new Vector3(7.5F, 0.0F, 0.0F), 1.0F, new Vector3(10.0F, 0.0F, 0.0F), 2.0F),
                "SpheresIntersect returned false for intersecting spheres");
        }

        [Test, ExpectedException("System.ArgumentOutOfRangeException")]
        public void IntersectExceptionTest()
        {
            Geometry.SpheresIntersect(new Vector3(7.5F, 0.0F, 0.0F), -1.0F, new Vector3(10.0F, 0.0F, 0.0F), 2.0F);
        }

        [Test]
        public void IntersectVolumeTest()
        {
            // Non-intersecting spheres
            Assert.AreEqual(0.0F, Geometry.SphereIntersectVolume(new Vector3(1.0F, 0.0F, 0.0F), 1.0F, new Vector3(10.0F, 0.0F, 0.0F), 2.0F),
                "SphereIntersectVolume returned non-zero for non-intersecting spheres");

            // Fully intersecting spheres
            Assert.AreEqual("4.189", String.Format("{0:F3}",
                Geometry.SphereIntersectVolume(new Vector3(9.5F, 0.0F, 0.0F), 1.0F, new Vector3(10.0F, 0.0F, 0.0F), 2.0F)),
                "SphereIntersectVolume returned incorrect value for fully intersecting spheres");

            double previousVolume = -1.0F;
            for (int i = 0; i < 10; i++)
            {
                double vol = Geometry.SphereIntersectVolume(new Vector3(0.0F, 0.0F, 0.0F), 2.0F,
                    new Vector3(1.0F + i * 0.1F, 0.0F, 0.0F), 1.0F);

                Assert.GreaterOrEqual(vol, 0.0F, "Negative intersect volume");

                if (previousVolume >= 0.0F)
                {
                    Assert.Less(vol, previousVolume, "Spheres departing, but intersection volume grew");
                }

                previousVolume = vol;
            }
        }

        [Test, ExpectedException("System.ArgumentOutOfRangeException")]
        public void IntersectVolumeExceptionTest()
        {
            Geometry.SphereIntersectVolume(new Vector3(7.5F, 0.0F, 0.0F), -1.0F, new Vector3(10.0F, 0.0F, 0.0F), 2.0F);
        }

        [Test]
        public void SphereContainedTest()
        {
            // Non-intersecting spheres
            Assert.IsFalse(Geometry.SphereContained(new Vector3(10.0F, 0.0F, 0.0F), 2.0F, new Vector3(1.0F, 0.0F, 0.0F), 1.0F),
                "SphereContained returned true for non-intersecting spheres");

            // Fully contained sphere
            Assert.IsTrue(Geometry.SphereContained(new Vector3(10.0F, 0.0F, 0.0F), 2.0F, new Vector3(9.5F, 0.0F, 0.0F), 1.0F),
                "SphereContained returned false for fully contained sphere");

            // The contained sphere does not contain the containing sphere
            Assert.IsFalse(Geometry.SphereContained(new Vector3(9.5F, 0.0F, 0.0F), 1.0F, new Vector3(10.0F, 0.0F, 0.0F), 2.0F),
                "SphereContained returned true for inverted parameters");

            // Partially contained sphere
            Assert.IsFalse(Geometry.SphereContained(new Vector3(10.0F, 0.0F, 0.0F), 2.0F, new Vector3(7.5F, 0.0F, 0.0F), 1.0F),
                "SphereContained returned true for partially contained sphere");
        }

        [Test, ExpectedException("System.ArgumentOutOfRangeException")]
        public void SphereContainedExceptionTest()
        {
            Geometry.SphereContained(new Vector3(10.0F, 0.0F, 0.0F), -2.0F, new Vector3(7.5F, 0.0F, 0.0F), 1.0F);
        }
    }
}
