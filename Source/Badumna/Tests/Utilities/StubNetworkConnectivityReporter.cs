﻿//---------------------------------------------------------------------------------
// <copyright file="StubNetworkConnectivityReporter.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2012 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Badumna.Core;
using Badumna.Utilities;

namespace BadumnaTests.Utilities
{
    internal class StubNetworkConnectivityReporter : INetworkConnectivityReporter
    {
#pragma warning disable 67  // The event 'xxx' is never used
        public event GenericCallBack NetworkOnlineEvent;

        public event GenericCallBack NetworkOfflineEvent;

        public event GenericCallBack NetworkShuttingDownEvent;

        public event GenericCallBack NetworkInitializingEvent;
#pragma warning restore 67

        public ConnectivityStatus Status { get; set; }

        public void SetStatus(ConnectivityStatus status)
        {
            this.Status = status;

            switch (status)
            {
                case ConnectivityStatus.Online:
                    if (this.NetworkOnlineEvent != null)
                    {
                        this.NetworkOnlineEvent();
                    }

                    break;

                case ConnectivityStatus.Offline:
                    if (this.NetworkOfflineEvent != null)
                    {
                        this.NetworkOfflineEvent();
                    }

                    break;
            }
        }

        public void GoOnline()
        {
            this.SetStatus(ConnectivityStatus.Online);
        }

        public void GoOffline()
        {
            this.SetStatus(ConnectivityStatus.Offline);
        }

        public void Shutdown()
        {
            this.NetworkShuttingDownEvent();
        }
    }
}
