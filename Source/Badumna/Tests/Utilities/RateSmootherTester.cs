﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using Badumna.Core;
using Badumna.Utilities;
using System.Threading;
using System.Diagnostics;
using Rhino.Mocks;

namespace BadumnaTests.Utilities
{
    [TestFixture]
    public class SmoothedRateEstimatorTester
    {
        [Test]
        public void ConstantRateTest()
        {
            this.ConstantRateTestWithStubTimeKeeper(
                TimeSpan.FromSeconds(1),
                TimeSpan.FromSeconds(5),
                1000,
                1234.0,
                0.03);
        }

        // intermittent, need to be reviewed after 1.4 release. 
        [Test]
        public void MultipleValuesPerSampleTest()
        {
            this.ConstantRateTestWithStubTimeKeeper(
                TimeSpan.FromSeconds(3),
                TimeSpan.FromSeconds(15),
                500,
                1234.0,
                0.03);
        }

        public void ConstantRateTestWithStubTimeKeeper(
            TimeSpan sampleInterval,
            TimeSpan runTime,
            int eventIntervalMilliseconds,
            double rate,
            double tolerance)
        {
            StubTimeKeeper timeKeeper = new StubTimeKeeper();
            timeKeeper.Now = TimeSpan.FromSeconds(0);
            RateSmoother rs = new RateSmoother(sampleInterval, runTime, timeKeeper);
            for (int milliseconds = eventIntervalMilliseconds; milliseconds <= runTime.TotalMilliseconds; milliseconds += eventIntervalMilliseconds)
            {
                timeKeeper.Now = TimeSpan.FromMilliseconds(milliseconds);
                rs.ValueEvent((eventIntervalMilliseconds / 1000.0) * rate);
            }

            Assert.GreaterOrEqual(rs.EstimatedRate, rate * (1 / (1 + tolerance)));
            Assert.LessOrEqual(rs.EstimatedRate, rate * (1 + tolerance));
        }

        // No longer used, as it is slooooow. Kept for reference.
        private void ConstantRateTest(TimeSpan runTime, double rate, TimeSpan sampleRate, int tickRate)
        {
            ITime timeKeeper = new NetworkEventQueue();
            RateSmoother estimator = new RateSmoother(sampleRate, runTime, timeKeeper);

            ManualResetEvent hasTickedEnough = new ManualResetEvent(false);

            Stopwatch stopwatch = new Stopwatch();
            object tickLock = new object();
            int tickCount = 0;
            TimeSpan previousElapsed = TimeSpan.Zero;
            TimerCallback tick = delegate
            {
                if (stopwatch.Elapsed > runTime)
                {
                    hasTickedEnough.Set();
                    return;
                }
                lock (tickLock)
                {
                    //Console.WriteLine("{0}", stopwatch.ElapsedMilliseconds);
                    TimeSpan now = stopwatch.Elapsed;
                    estimator.ValueEvent(rate * (now - previousElapsed).TotalSeconds);
                    previousElapsed = now;
                }
                Interlocked.Increment(ref tickCount);
            };

            stopwatch.Start();
            Timer timer = new Timer(tick, null, 100, tickRate);

            hasTickedEnough.WaitOne();
            timer.Dispose();

            double estimatedRate = estimator.EstimatedRate;

            Assert.AreEqual((int)runTime.TotalMilliseconds / tickRate, tickCount, "Not enough ticks");
            Assert.GreaterOrEqual(estimatedRate, 0.97 * rate);  // TODO: Contract of SmoothedRateEstimator says we should get within +/- 1%
            Assert.LessOrEqual(estimatedRate, 1.03 * rate);
        }

        public class StubTimeKeeper : ITime
        {
            public TimeSpan Now
            {
                get;
                set;
            }
        }
    }
}
