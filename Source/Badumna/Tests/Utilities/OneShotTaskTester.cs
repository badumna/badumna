﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using Badumna.Utilities;
using Badumna.Core;

namespace BadumnaTests.Utilities
{
    [TestFixture]
    public class OneShotTaskTester
    {
        delegate void Action();

        private Simulator eventQueue;

        [SetUp]
        public void SetUp()
        {
            this.eventQueue = new Simulator();
        }

        [Test]
        public void OnceOnlyTest()
        {
            int count = 0;
            IInvokable countUp = Apply.Func(delegate { count++; });
            OneShotTask task = new OneShotTask(this.eventQueue);

            task.Trigger(TimeSpan.FromMilliseconds(100), countUp);
            task.Trigger(TimeSpan.FromMilliseconds(200), countUp);  // Second trigger should override the first
            this.eventQueue.RunAllIgnoringTime();
            Assert.AreEqual(1, count);
        }

        [Test]
        public void MultipleTest()
        {
            int count = 0;
            IInvokable countUp = Apply.Func(delegate { count++; });
            OneShotTask task = new OneShotTask(this.eventQueue);

            task.Trigger(TimeSpan.FromMilliseconds(100), countUp);
            this.eventQueue.RunAllIgnoringTime();
            task.Trigger(TimeSpan.FromMilliseconds(100), countUp);  // If no other trigger is on the queue, a second one should work independently
            this.eventQueue.RunAllIgnoringTime();
            Assert.AreEqual(2, count);
        }

        [Test]
        public void CancelTest()
        {
            int count = 0;
            IInvokable countUp = Apply.Func(delegate { count++; });
            OneShotTask task = new OneShotTask(this.eventQueue);

            task.Trigger(TimeSpan.FromMilliseconds(100), countUp);
            this.eventQueue.RunFor(TimeSpan.FromMilliseconds(50));
            task.Cancel();
            this.eventQueue.RunAllIgnoringTime();
            Assert.AreEqual(0, count);
        }
    }
}
