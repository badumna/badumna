﻿//-----------------------------------------------------------------------
// <copyright file="IntervalSeriesTests.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2011 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BadumnaTests.Utilities
{
    using System;
    using Badumna.Core;
    using Badumna.Utilities;
    using NUnit.Framework;
    using Rhino.Mocks;

    [TestFixture]
    internal class IntervalSeriesTests
    {
        private static Random random = new Random();
        private IntervalSeries ds;
        private int size;
        private FakeTime timeKeeper;

        [SetUp]
        public void SetUp()
        {
            this.size = 1000;
            this.timeKeeper = new FakeTime();
            this.ds = new IntervalSeries(1000, this.timeKeeper);
        }

        [Ignore]
        [TestCase(3, 1)]
        [TestCase(300, 100)]
        [TestCase(1300, 600)]
        public void StdDevIsAccurateForRandomSamplesFromNormalDistribution(double mean, double stdDev)
        {
            this.CreateSeries(mean, stdDev);
            Console.WriteLine("{0:0.000} -> {1:0.000}", this.ds.StdDev, stdDev);
            Assert.IsTrue(this.ds.StdDev > stdDev * 0.9);
            Assert.IsTrue(this.ds.StdDev < stdDev * 1.1);
        }

        [Ignore]
        [TestCase(3, 1)]
        [TestCase(300, 100)]
        [TestCase(1300, 600)]
        public void MeanIsAccurateForRandomSamplesFromNormalDistribution(double mean, double stdDev)
        {
            this.CreateSeries(mean, stdDev);
            Console.WriteLine("{0:0.000} -> {1:0.000}", this.ds.Mean, mean);
            Assert.IsTrue(this.ds.Mean > mean - (0.1 * stdDev));
            Assert.IsTrue(this.ds.Mean < mean + (0.1 * stdDev));
        }

        private static double RandomNormal(double mean, double stdDev)
        {
            return mean + (stdDev * (Math.Cos(2 * Math.PI * random.NextDouble()) * Math.Sqrt(-2 * Math.Log(random.NextDouble()))));
        }

        private void CreateSeries(double mean, double stdDev)
        {
            double milliseconds = 0f;
            this.timeKeeper.Now = TimeSpan.FromMilliseconds(milliseconds);
            this.ds.Start();
            for (int i = 0; i < this.size; i++)
            {
                double interval = Math.Max(0, RandomNormal(mean, stdDev));
                milliseconds += interval;
                this.timeKeeper.Now = TimeSpan.FromMilliseconds(milliseconds);
                this.ds.Add();
            }
        }

        public class FakeTime : ITime
        {
            public TimeSpan Now { get; set; }
        }
    }
}
