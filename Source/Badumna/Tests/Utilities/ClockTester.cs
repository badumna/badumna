﻿//------------------------------------------------------------------------------
// <copyright file="ClockTester.cs" company="National ICT Australia Limited">
//     Copyright (c) 2012 National ICT Australia Limited. All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System;

using Badumna.Core;
using Badumna.Utilities;

using NUnit.Framework;

namespace BadumnaTests.Utilities
{
    [TestFixture]
    public class ClockTester
    {
        private Clock clock;

        [Test]
        public void TestNetworkClockCompressedTime()
        {
            Simulator eventQueue = new Simulator();
            ITime timeKeeper = eventQueue;

            this.InitializeNetworkClock(true, eventQueue, timeKeeper);

            DateTime startTime = this.clock.Now;
            eventQueue.RunFor(TimeSpan.FromSeconds(3600));
            DateTime endTime = this.clock.Now;

            TimeSpan diff = endTime - startTime;
            Assert.IsTrue(diff > TimeSpan.FromSeconds(3590) && diff < TimeSpan.FromSeconds(3610));
        }

        [Test]
        [Category("Slow")]
        public void TestNetworkClockRealTime()
        {
            NetworkEventQueue eventQueue = new NetworkEventQueue();
            ITime timeKeeper = eventQueue;

            this.InitializeNetworkClock(true, eventQueue, timeKeeper);

            DateTime startTime = this.clock.Now;
            System.Threading.Thread.Sleep(3500);
            DateTime endTime = this.clock.Now;

            TimeSpan diff = endTime - startTime;
            Assert.IsTrue(diff > TimeSpan.FromMilliseconds(3400) && diff < TimeSpan.FromMilliseconds(3600));
        }

        private void InitializeNetworkClock(bool synchronize, NetworkEventQueue eventQueue, ITime timeKeeper)
        {
            this.clock = new Clock(eventQueue, delegate { return timeKeeper.Now; });

            if (synchronize)
            {
                this.clock.Synchronize(DateTime.Now);
            }
        }
    }
}
