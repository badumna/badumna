﻿using System;
using System.Collections.Generic;
using System.Text;
using Badumna.Core;
using Badumna.Utilities;
using BadumnaTests.Core;
using NUnit.Framework;

namespace BadumnaTests.Utilities
{
    [TestFixture]
    [DontPerformCoverage]
    public class ResourceAllocatorTester
    {

        [SetUp]
        public void Initialize()
        {
        }

        [TearDown]
        public void Terminate()
        {
        }

        [Test]
        public void SingleSliceTest()
        {
            // Test that a single stream gets all the bandwidth
            ResourceAllocator<int> allocator = new ResourceAllocator<int>(100, 0);

            allocator.AllocateSlice(0, 100, 1.0f);
            allocator.CalculateAllocation();

            Assert.AreEqual(100, allocator.GetAllocatedRate(0), "Rate of single stream is not maximum");
        }

        [Test]
        public void MultipleSliceTest()
        {
            // Test that streams get equal slice when rate is not maxed
            ResourceAllocator<int> allocator = new ResourceAllocator<int>(100, 0);

            allocator.AllocateSlice(0, 100, 1.0f);
            allocator.AllocateSlice(1, 100, 1.0f);
            allocator.AllocateSlice(2, 100, 1.0f);
            allocator.AllocateSlice(3, 100, 1.0f);
            allocator.CalculateAllocation();

            Assert.AreEqual(25, allocator.GetAllocatedRate(0), "Rate of stream is not equally divided");
            Assert.AreEqual(25, allocator.GetAllocatedRate(1), "Rate of stream is not equally divided");
            Assert.AreEqual(25, allocator.GetAllocatedRate(2), "Rate of stream is not equally divided");
            Assert.AreEqual(25, allocator.GetAllocatedRate(3), "Rate of stream is not equally divided");
        }


        [Test]
        public void RemoveTest()
        {
            // Test that a stream can be removed
            ResourceAllocator<int> allocator = new ResourceAllocator<int>(100, 0);

            allocator.AllocateSlice(0, 100, 1.0f);
            allocator.AllocateSlice(1, 100, 1.0f);
            allocator.CalculateAllocation();
            allocator.RemoveSlice(0);
            allocator.CalculateAllocation();

            Assert.AreEqual(0, allocator.GetAllocatedRate(0), "Rate of stream is not zero after removal");
            Assert.AreEqual(100, allocator.GetAllocatedRate(1), "Rate of single stream is not maximum after removal");
        }

        // Test that the sum of slices is not greater than the maximum rate

        [Test]
        public void WeightTest()
        {
            // Test that slices are allocated according to weight for equally desired streams
            ResourceAllocator<int> allocator = new ResourceAllocator<int>(100, 0);

            allocator.AllocateSlice(0, 100, 0.5f);
            allocator.AllocateSlice(1, 100, 0.3f);
            allocator.AllocateSlice(2, 100, 0.2f);

            allocator.CalculateAllocation();

            Assert.AreEqual(50, Math.Round(allocator.GetAllocatedRate(0), 0), "Rate of stream is not divided propoptially to weight");
            Assert.AreEqual(30, Math.Round(allocator.GetAllocatedRate(1), 0), "Rate of stream is not divided propoptially to weight");
            Assert.AreEqual(20, Math.Round(allocator.GetAllocatedRate(2), 0), "Rate of stream is not divided propoptially to weight");
            Assert.AreEqual(100, Math.Round(allocator.GetAllocatedRate(0) + allocator.GetAllocatedRate(1) + allocator.GetAllocatedRate(2), 0),
                "Resource is over or under utilised.");

            // Loop to allow enough time for the smoother to adjust
            for (int i = 0; i < 100; i++)
            {
                allocator.AllocateWeight(0, 0.4f);
                allocator.AllocateWeight(1, 0.4f);
                allocator.AllocateWeight(2, 0.2f);
            }
            Assert.AreEqual(0.2, Math.Round(allocator.GetWeight(2), 1), "Current weight is incorrect");

            allocator.CalculateAllocation();

            Assert.AreEqual(40, Math.Round(allocator.GetAllocatedRate(0), 0), "Rate of stream is not divided propoptially to weight");
            Assert.AreEqual(40, Math.Round(allocator.GetAllocatedRate(1), 0), "Rate of stream is not divided propoptially to weight");
            Assert.AreEqual(20, Math.Round(allocator.GetAllocatedRate(2), 0), "Rate of stream is not divided propoptially to weight");
            Assert.AreEqual(100, Math.Round(allocator.GetAllocatedRate(0) + allocator.GetAllocatedRate(1) + allocator.GetAllocatedRate(2), 0),
                "Resource is over or under utilised.");
        }

        [Test]
        public void DesiredVariationTest()
        {
            // Test that with equal weight variation of desired rate effects actual rate when under resourced
            ResourceAllocator<int> allocator = new ResourceAllocator<int>(90, 0);

            allocator.AllocateSlice(0, 100, 1.0f);
            allocator.AllocateSlice(1, 100, 1.0f);
            allocator.AllocateSlice(2, 100, 1.0f);

            allocator.CalculateAllocation();

            allocator.AllocateSlice(0, 20.0, 1.0f);
            allocator.AllocateSlice(1, 20.0, 1.0f);
            allocator.AllocateSlice(2, 50.0, 1.0f);

            allocator.CalculateAllocation();

            Assert.AreEqual(20, allocator.GetAllocatedRate(0), "Rate of stream is not divided propoptially to weight");
            Assert.AreEqual(20, allocator.GetAllocatedRate(1), "Rate of stream is not divided propoptially to weight");
            Assert.AreEqual(50, allocator.GetAllocatedRate(2), "Rate of stream is not divided propoptially to weight");
            Assert.AreEqual(90, Math.Round(allocator.GetAllocatedRate(0) + allocator.GetAllocatedRate(1) + allocator.GetAllocatedRate(2), 0),
             "Resource is over or under utilised.");
        }

        [Test]
        public void WeightAndDesireVariationTest()
        {
            // Test that variation in desire causes variation in slices according to weight
            ResourceAllocator<int> allocator = new ResourceAllocator<int>(90, 0);

            allocator.AllocateSlice(0, 70, 0.1f);
            allocator.AllocateSlice(1, 55, 0.8f);
            allocator.AllocateSlice(2, 75, 0.3f);

            allocator.CalculateAllocation();

            Assert.AreEqual(15, Math.Round(allocator.GetAllocatedRate(0), 0), "Rate of stream is not divided propoptially to weight");
            Assert.AreEqual(43, Math.Round(allocator.GetAllocatedRate(1), 0), "Rate of stream is not divided propoptially to weight");
            Assert.AreEqual(32, Math.Round(allocator.GetAllocatedRate(2), 0), "Rate of stream is not divided propoptially to weight");
            Assert.AreEqual(90, Math.Round(allocator.GetAllocatedRate(0) + allocator.GetAllocatedRate(1) + allocator.GetAllocatedRate(2), 0),
                "Resource is over or under utilised.");
        }

        [Test]
        public void NonNegativeTest()
        {
            // Test that no slice gets negative rate and that the maximum rate is always used (when total desired is greater)
            ResourceAllocator<int> allocator = new ResourceAllocator<int>(100, 0);

            allocator.AllocateSlice(0, 30, 0.01f);
            allocator.AllocateSlice(1, 90, 0.8f);
            allocator.AllocateSlice(2, 90, 0.8f);

            allocator.CalculateAllocation();

            Assert.AreEqual(0, Math.Round(allocator.GetAllocatedRate(0), 0), "Rate of stream is not divided propoptially to weight");
            Assert.AreEqual(50, Math.Round(allocator.GetAllocatedRate(1), 0), "Rate of stream is not divided propoptially to weight");
            Assert.AreEqual(50, Math.Round(allocator.GetAllocatedRate(2), 0), "Rate of stream is not divided propoptially to weight");
            Assert.AreEqual(100, Math.Round(allocator.GetAllocatedRate(0) + allocator.GetAllocatedRate(1) + allocator.GetAllocatedRate(2), 0),
                "Resource is over or under utilised.");
        }

        [Test]
        public void WeightVariationTest()
        {
            // Test that changes in weight approach the proportional variation in slices
            ResourceAllocator<int> allocator = new ResourceAllocator<int>(90, 0);

            allocator.AllocateSlice(0, 90, 0.5f);
            allocator.AllocateSlice(1, 90, 0.5f);
            allocator.AllocateSlice(2, 90, 0.5f);

            allocator.CalculateAllocation();

            Assert.AreEqual(30, Math.Round(allocator.GetAllocatedRate(0), 0), "Rate of stream is not divided propoptially to weight");
            Assert.AreEqual(30, Math.Round(allocator.GetAllocatedRate(1), 0), "Rate of stream is not divided propoptially to weight");
            Assert.AreEqual(30, Math.Round(allocator.GetAllocatedRate(2), 0), "Rate of stream is not divided propoptially to weight");
            Assert.AreEqual(90, Math.Round(allocator.GetAllocatedRate(0) + allocator.GetAllocatedRate(1) + allocator.GetAllocatedRate(2), 0),
                "Resource is over or under utilised.");

            for (int i = 0; i < 10; i++)
            {
                allocator.AllocateSlice(2, 90, 0.25f);
            }

            allocator.CalculateAllocation();

            Assert.AreEqual(38, Math.Round(allocator.GetAllocatedRate(0), 0), "Rate of stream is not divided propoptially to weight");
            Assert.AreEqual(38, Math.Round(allocator.GetAllocatedRate(1), 0), "Rate of stream is not divided propoptially to weight");
            Assert.AreEqual(14, Math.Round(allocator.GetAllocatedRate(2), 0), "Rate of stream is not divided propoptially to weight");
            Assert.AreEqual(90, Math.Round(allocator.GetAllocatedRate(0) + allocator.GetAllocatedRate(1) + allocator.GetAllocatedRate(2), 0),
                "Resource is over or under utilised.");

        }

        [Test, ExpectedException(typeof(InvalidOperationException))]
        public void InvalidWeightTest()
        {
            // Test that exception is thrown for invalid weight         
            ResourceAllocator<int> allocator = new ResourceAllocator<int>(90, 0);

            allocator.AllocateSlice(0, 90, 1.5f);
        }

    }
}
