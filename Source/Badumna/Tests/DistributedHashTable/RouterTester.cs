﻿using System;
using System.Collections.Generic;
using Badumna.Core;
using Badumna.DistributedHashTable;
using Badumna.Transport;
using Badumna.Utilities;
using BadumnaTests.Core;
using NUnit.Framework;

namespace BadumnaTests.DistributedHashTable
{
    internal class MockNode : RouterProtocol, IConnectionTable, IMessageConsumer<TransportEnvelope>
    {
        private PeerAddress mAddress;
        public PeerAddress Address { get { return this.mAddress; } }

        private MockLogicalTransportLayer mTransport;
        private List<PeerAddress> mConnectivityList = new List<PeerAddress>();

        private NetworkEventQueue eventQueue;

        public ConnectionNotifier ConnectionNotifier { get; private set; }
        public INetworkConnectivityReporter ConnectivityReporter { get; private set; }
        public INetworkAddressProvider AddressProvider { get; private set; }

        public MockNode(PeerAddress address, MockLogicalTransportLayer transport, NetworkEventQueue eventQueue, ITime timeKeeper)
            : base("MockNode", typeof(RouterProtocolMethodAttribute), MessageParserTester.DefaultFactory)
        {
            this.eventQueue = eventQueue;
            this.mAddress = address;
            this.AddressProvider = new AddressProvider(this.mAddress);
            this.mTransport = transport;
            this.DispatcherMethod = this.DispatchMessage;
            this.ConnectionNotifier = new ConnectionNotifier();
            this.ConnectivityReporter = new NetworkConnectivityReporter(eventQueue);
        }

        public void DispatchMessage(TransportEnvelope envelope)
        {
            envelope.Source = this.mAddress;
            this.mTransport.SendMessage(envelope, 0);
        }

        public override void PrepareMessageForDeparture(ref TransportEnvelope envelope)
        {
        }

        public bool IsKnown(PeerAddress address)
        {
            return this.mConnectivityList.Contains(address);
        }

        public IEnumerable<IConnection> AllConnections
        {
            get { throw new NotImplementedException(); }
        }

        public bool IsConnected(PeerAddress address)
        {
            return this.IsKnown(address);
        }

        public bool IsRelayed(PeerAddress address)
        {
            return false;
        }

        public void ConfirmConnection(PeerAddress address)
        {
            TransportEnvelope ping = this.GetMessageFor(address, QualityOfService.Reliable);
            ping.Qos.Priority = QosPriority.Medium;

            this.SendMessage(ping);
        }

        public void RemoveConnection(PeerAddress address)
        {
        }

        public void RemoveNeighbour(PeerAddress address)
        {
            if (this.mConnectivityList.Contains(address))
            {
                this.mConnectivityList.Remove(address);
                this.ConnectionNotifier.OnConnectionLost(address);
            }
        }

        #region IMessageConsumer<TransportEnvelope> Members

        public void ProcessMessage(TransportEnvelope envelope)
        {
            if (this.mConnectivityList.Contains(envelope.Source))
            {
                this.ParseMessage(envelope, 0);
            }
            else
            {
                this.ConfirmConnection(envelope.Source);
                this.mConnectivityList.Add(envelope.Source);
                this.ConnectionNotifier.OnConnectionEstablished(envelope.Source);
                this.eventQueue.Push(this.ParseMessage, envelope, 0);
            }
        }

        #endregion
    }

    internal class MockLogicalTransportLayer
    {
        private Dictionary<PeerAddress, MockNode> mNodes = new Dictionary<PeerAddress, MockNode>();

        private static int mSentMessagesCount;
        public static int SentMessagesCount
        {
            get { return MockLogicalTransportLayer.mSentMessagesCount; }
            set { MockLogicalTransportLayer.mSentMessagesCount = value; }
        }

        private NetworkEventQueue eventQueue;
        private ITime timeKeeper;

        public MockLogicalTransportLayer(NetworkEventQueue eventQueue, ITime timeKeeper)
        {
            this.eventQueue = eventQueue;
            this.timeKeeper = timeKeeper;
        }

        public MockNode NewNode(PeerAddress address)
        {
            MockNode node = new MockNode(address, this, this.eventQueue, this.timeKeeper);

            this.mNodes.Add(address, node);
            return node;
        }

        public void SendMessage(TransportEnvelope messageEnvelope, int nthAttemp)
        {
            if (this.mNodes.ContainsKey(messageEnvelope.Destination))
            {
                if (this.mNodes[messageEnvelope.Destination].ConnectivityReporter.Status != ConnectivityStatus.Online)
                {
                    messageEnvelope.Qos.HandleFailure(messageEnvelope.Destination, nthAttemp);

                    if (nthAttemp == 2)
                    {
                        this.mNodes[messageEnvelope.Source].RemoveNeighbour(messageEnvelope.Destination);
                    }

                    if (nthAttemp < 2)
                    {
                        this.eventQueue.Push(this.SendMessage, messageEnvelope, nthAttemp + 1);
                    }
                }
                else
                {
                    MockLogicalTransportLayer.mSentMessagesCount++;
                    this.eventQueue.Push(this.mNodes[messageEnvelope.Destination].ProcessMessage, messageEnvelope);
                }
            }
        }
    }

    class MockRouterChild : IMessageConsumer<DhtEnvelope>
    {
        private DhtEnvelope mReceivedMessage;
        public DhtEnvelope ReceivedMessage { get { return this.mReceivedMessage; } }

        private int mNumberMessagesReceived;
        public int NumberMessagesReceived { get { return this.mNumberMessagesReceived; } }

        private HashKey mDestinationKey;
        public HashKey DestinationKey { get { return this.mDestinationKey; } }

        private PeerAddress mMessageSource;
        public PeerAddress MessageSource { get { return this.mMessageSource; } }

        private static int mTotalReceivedCount;
        public static int TotalReceivedCount { get { return MockRouterChild.mTotalReceivedCount; } }

        public static void ResetReceivedCount()
        {
            MockRouterChild.mTotalReceivedCount = 0;
        }

        #region IMessageConsumer<DhtEnvelope> Members

        public void ProcessMessage(DhtEnvelope envelope)
        {
            MockRouterChild.mTotalReceivedCount++;
            this.mNumberMessagesReceived++;
            this.mDestinationKey = envelope.DestinationKey;
            this.mReceivedMessage = envelope;
            this.mMessageSource = envelope.Source;
        }

        #endregion
    }

    class ConnectionNotifier : IPeerConnectionNotifier
    {
        public event ConnectionHandler ConnectionEstablishedEvent;

        public event ConnectionHandler ConnectionLostEvent;

        public void OnConnectionEstablished(PeerAddress address)
        {
            ConnectionHandler handler = this.ConnectionEstablishedEvent;
            if (handler != null)
            {
                handler(address);
            }
        }

        public void OnConnectionLost(PeerAddress address)
        {
            ConnectionHandler handler = this.ConnectionLostEvent;
            if (handler != null)
            {
                handler(address);
            }
        }
    }

    class AddressProvider : INetworkAddressProvider
    {
        public AddressProvider(PeerAddress publicAddress)
        {
            this.PublicAddress = publicAddress;
        }

        public bool IsAddressKnownAndValid { get { return true; } }

        public PeerAddress PublicAddress { get; private set; }

        public int PrivatePort
        {
            get { throw new NotImplementedException(); }
        }

        public IEnumerable<PeerAddress> PrivateAddresses
        {
            get { throw new NotImplementedException(); }
        }

        public bool HasPrivateAddress(PeerAddress address)
        {
            throw new NotImplementedException();
        }

        public event GenericCallBack PublicAddressChanged
        {
            add { throw new NotImplementedException(); }
            remove { throw new NotImplementedException(); }
        }
    }



    [TestFixture]
    public class RouterTester
    {
        private MockLogicalTransportLayer mTransport;
        private int mNextPort;
        private List<PeerAddress> mInitialPeerAddressList = new List<PeerAddress>();
        private MockRouterChild mInitialChild;
        private MockNode mInitialNode;
        private Router mRouter;
        private Simulator eventQueue;

        public RouterTester()
        {
        }

        [SetUp]
        public void Initialize()
        {
            this.eventQueue = new Simulator();
            ITime timeKeeper = this.eventQueue;

            this.mTransport = new MockLogicalTransportLayer(this.eventQueue, timeKeeper);
            this.mNextPort = 1;
            this.mInitialPeerAddressList.Clear();

            MockRouterChild.ResetReceivedCount();
            MockLogicalTransportLayer.SentMessagesCount = 0;
        }

        public void DoEvents(TimeSpan timeSpan)
        {
            this.eventQueue.RunFor(timeSpan);
        }

        private void CreateTestRouter()
        {
            this.mInitialPeerAddressList.Add(PeerAddress.GetLoopback(this.mNextPort++));
            this.mInitialNode = this.mTransport.NewNode(this.mInitialPeerAddressList[0]);

            this.mInitialChild = new MockRouterChild();
            this.mRouter = new Router(this.mInitialNode, this.mInitialNode, this.mInitialChild, this.eventQueue, this.mInitialNode.AddressProvider, this.mInitialNode.ConnectivityReporter, this.mInitialNode.ConnectionNotifier);
            this.mInitialNode.ForgeMethodList();
        }

        private void Join(IConnectionTable node)
        {
            foreach (PeerAddress peer in this.mInitialPeerAddressList)
            {
                node.ConfirmConnection(peer);
            }
        }

        [Test]
        public void FirstNodeTest()
        {
            this.CreateTestRouter();
            this.mInitialNode.ConnectivityReporter.SetStatus(ConnectivityStatus.Online);

            // Test that if none of the given peers are responding that the node becomes the initial node.
            //this.router.Initialize();
            this.DoEvents(TimeSpan.FromSeconds(100));

            Assert.AreEqual(0, this.mRouter.LeafSet.UniqueNodeCount);
            Assert.AreEqual(this.mInitialPeerAddressList[0], this.mRouter.LeafSet.LocalNodeInfo.Address);
        }

        internal void OnlineContextEvent(MockNode node)
        {
            if (null != node)
            {
                node.ConnectivityReporter.SetStatus(ConnectivityStatus.Online);
            }
        }

        [Test]
        public void InitializeTest()
        {
            this.FirstNodeTest();

            PeerAddress address = PeerAddress.GetLoopback(this.mNextPort++);
            MockNode node = this.mTransport.NewNode(address);
            node.ConnectivityReporter.SetStatus(ConnectivityStatus.Initializing);
            Router router = new Router(node, node, new MockRouterChild(), this.eventQueue, node.AddressProvider, node.ConnectivityReporter, node.ConnectionNotifier);
            node.ForgeMethodList();


            // Test that a context change will result in the same.
            this.eventQueue.Schedule(1, this.OnlineContextEvent, node);
            this.DoEvents(TimeSpan.FromMilliseconds(2));

            // Test that if none of the given peers are responding that the node becomes the initial node.
            this.Join(node);

            this.DoEvents(TimeSpan.FromSeconds(100));

            // Test that initialize will intitiate a join request on the first available given peer.
            Assert.AreEqual(1, router.LeafSet.UniqueNodeCount);
            Assert.AreEqual(address, router.LeafSet.LocalNodeInfo.Address);
        }

        [Test]
        public void FullSetTest()
        {
            this.FirstNodeTest();

            List<Router> routers = new List<Router>();

            for (int i = 0; i < 4; i++)
            {
                PeerAddress address = PeerAddress.GetLoopback(this.mNextPort++);
                MockNode node = this.mTransport.NewNode(address);
                Router router = new Router(node, node, new MockRouterChild(), this.eventQueue, node.AddressProvider, node.ConnectivityReporter, node.ConnectionNotifier);
                node.ForgeMethodList();

                this.eventQueue.Schedule(1, this.OnlineContextEvent, node);
                this.DoEvents(TimeSpan.FromMilliseconds(2));

                this.Join(node);

                routers.Add(router);
                this.DoEvents(TimeSpan.FromSeconds(60));
            }

            this.DoEvents(TimeSpan.FromSeconds(100));

            // Test that when enough peers join all their leaf sets are synced correctly.
            foreach (Router router in routers)
            {
                Assert.AreEqual(4, router.LeafSet.UniqueNodeCount);
                foreach (Router neighbour in routers)
                {
                    //System.Console.WriteLine("--- {0}", neighbour.LeafSet[0].Address);
                    for (int i = -neighbour.LeafSet.PredecessorCount; i <= neighbour.LeafSet.SuccessorCount; i++)
                    {
                        if (!router.LeafSet.LocalNodeInfo.Address.Equals(neighbour.LeafSet[i].Address))
                        {
                            //System.Console.WriteLine("{0} -> {1}", router.LeafSet.LocalNodeInfo.Address, neighbour.LeafSet[i].Address);
                            Assert.IsTrue(router.LeafSet.Contains(neighbour.LeafSet[i]));
                        }
                    }
                }
            }
        }

        [Test]
        public void RouteTest()
        {
            this.FirstNodeTest();

            PeerAddress address = PeerAddress.GetLoopback(this.mNextPort++);
            MockNode node = this.mTransport.NewNode(address);
            node.ConnectivityReporter.SetStatus(ConnectivityStatus.Online);
            MockRouterChild child = new MockRouterChild();
            Router router = new Router(node, node, child, this.eventQueue, node.AddressProvider, node.ConnectivityReporter, node.ConnectionNotifier);
            node.ForgeMethodList();
            this.Join(node);

            this.DoEvents(TimeSpan.FromSeconds(100));

            // Test that a message can be routed.
            HashKey key = router.LeafSet[0].Key;
            DhtEnvelope envelope = new DhtEnvelope(key, QualityOfService.Reliable);
            int envelopeLength = envelope.Length;
            router.RouteMessage(envelope);

            this.DoEvents(TimeSpan.FromSeconds(100));

            Assert.AreEqual(envelopeLength, child.ReceivedMessage.Length);
            Assert.AreEqual(1, child.NumberMessagesReceived + this.mInitialChild.NumberMessagesReceived);

            if (1 == child.NumberMessagesReceived)
            {
                Assert.AreEqual(key, child.DestinationKey);
                Assert.AreEqual(address, child.MessageSource);
            }
            else
            {
                Assert.AreEqual(key, this.mInitialChild.DestinationKey);
                Assert.AreEqual(address, this.mInitialChild.MessageSource);
            }
        }

        [Test, Ignore] // The message count is not correct because it doesn't take into account the other dht messages
        public void MinimalRouteTest()
        {
            // Test that when 5 peers are in the network the number of hops is not greater than 1 for any source/destination pair.
            int trialsPerNode = 20;
            int nodeCount = 5;
            MockNode[] nodes = new MockNode[nodeCount];
            Router[] routers = new Router[nodeCount];

            this.FirstNodeTest();

            nodes[0] = this.mInitialNode;
            routers[0] = this.mRouter;

            for (int i = 1; i < nodeCount; i++)
            {
                PeerAddress address = PeerAddress.GetLoopback(this.mNextPort++);
                MockNode node = this.mTransport.NewNode(address);
                node.ConnectivityReporter.SetStatus(ConnectivityStatus.Online);
                MockRouterChild child = new MockRouterChild();
                Router router = new Router(node, node, child, this.eventQueue, node.AddressProvider, node.ConnectivityReporter, node.ConnectionNotifier);
                this.Join(node);

                nodes[i] = node;
                routers[i] = router;
                this.DoEvents(TimeSpan.FromSeconds(60));
            }

            this.DoEvents(TimeSpan.FromSeconds(100));

            int numInitialMessage = MockLogicalTransportLayer.SentMessagesCount;
            MockRouterChild.ResetReceivedCount();

            for (int i = 1; i <= nodeCount * trialsPerNode; i++)
            {
                int nodeIndex = (i - 1) / trialsPerNode;
                HashKey key = HashKey.Random();
                DhtEnvelope envelope = new DhtEnvelope(key, QualityOfService.Reliable);
                routers[nodeIndex].RouteMessage(envelope);

                this.DoEvents(TimeSpan.FromSeconds(100));

                //System.Console.WriteLine("{0} {1}", MockLogicalTransportLayer.SentMessagesCount, numInitialMessage + i);

                Assert.IsTrue(MockLogicalTransportLayer.SentMessagesCount <= numInitialMessage + i, "Too many messages sent.");
                Assert.IsTrue(i >= MockRouterChild.TotalReceivedCount);
            }

            //System.Console.WriteLine("Total message sent = {0}, initial messages = {1}", MockLogicalTransportLayer.SentMessagesCount, numInitialMessage);
        }



        [Test]
        public void InitializedTest()
        {
            this.CreateTestRouter();

            // Test that IsInitialized works.
            Assert.IsFalse(this.mRouter.IsInitialized);
            this.mInitialNode.ConnectivityReporter.SetStatus(ConnectivityStatus.Online);
            Assert.IsTrue(this.mRouter.IsInitialized);
            this.mRouter.Shutdown();
            Assert.IsFalse(this.mRouter.IsInitialized);
        }

        [Test, ExpectedException(typeof(InitializationException))]
        public void LocalHashKeyFailTest()
        {
            this.CreateTestRouter();

            // Test that attempting to get the local key prior to initialization fails.
            HashKey key = this.mRouter.LocalHashKey;
        }

        [Test]
        public void DepartAndRouteTest()
        {
            this.mInitialPeerAddressList.Add(PeerAddress.GetLoopback(this.mNextPort++));

            MockNode firstNode = this.mTransport.NewNode(this.mInitialPeerAddressList[0]);
            MockRouterChild firstChild = new MockRouterChild();
            Router firstRouter = new Router(firstNode, firstNode, firstChild, this.eventQueue, firstNode.AddressProvider, firstNode.ConnectivityReporter, firstNode.ConnectionNotifier);
            firstNode.ForgeMethodList();

            MockNode secondNode = this.mTransport.NewNode(PeerAddress.GetLoopback(this.mNextPort++));
            MockRouterChild secondChild = new MockRouterChild();
            Router secondRouter = new Router(secondNode, secondNode, secondChild, this.eventQueue, secondNode.AddressProvider, secondNode.ConnectivityReporter, secondNode.ConnectionNotifier);
            secondNode.ForgeMethodList();

            Assert.AreEqual(0, firstChild.NumberMessagesReceived);
            Assert.AreEqual(0, secondChild.NumberMessagesReceived);

            // Enter first node
            firstNode.ConnectivityReporter.SetStatus(ConnectivityStatus.Online);
            this.Join(firstNode);
            this.DoEvents(TimeSpan.FromSeconds(60));

            // Enter second node
            this.Join(secondNode);
            secondNode.ConnectivityReporter.SetStatus(ConnectivityStatus.Online);
            this.DoEvents(TimeSpan.FromSeconds(60));

            // Test that two nodes can enter and join
            Assert.IsTrue(firstNode.IsKnown(secondNode.AddressProvider.PublicAddress));
            Assert.IsTrue(secondNode.IsKnown(firstNode.AddressProvider.PublicAddress));
            Assert.IsTrue(firstRouter.LeafSet.Contains(secondRouter.LeafSet[0]));
            Assert.IsTrue(secondRouter.LeafSet.Contains(firstRouter.LeafSet[0]));

            HashKey firstKey = firstRouter.LocalHashKey;
            HashKey secondKey = secondRouter.LocalHashKey;

            // Test that each node can route to the other.
            DhtEnvelope envelope = new DhtEnvelope(secondKey, QualityOfService.Reliable);
            firstRouter.RouteMessage(envelope);

            this.DoEvents(TimeSpan.FromSeconds(60));
            Assert.AreEqual(1, secondChild.NumberMessagesReceived);
            Assert.AreEqual(envelope.Length, secondChild.ReceivedMessage.Length);
            Assert.AreEqual(firstNode.AddressProvider.PublicAddress, secondChild.MessageSource);
            Assert.AreEqual(secondKey, secondChild.DestinationKey);

            // Test that one node can leave.
            secondNode.ConnectivityReporter.SetStatus(ConnectivityStatus.Offline);
            this.DoEvents(TimeSpan.FromSeconds(60));

            // Test that the remaining node routes to itself when using departed nodes key
            envelope = new DhtEnvelope(secondKey, QualityOfService.Reliable);
            firstRouter.RouteMessage(envelope);

            this.DoEvents(TimeSpan.FromSeconds(60));
            Assert.AreEqual(1, firstChild.NumberMessagesReceived);
            Assert.AreEqual(envelope.Length, firstChild.ReceivedMessage.Length);
            Assert.AreEqual(firstNode.AddressProvider.PublicAddress, firstChild.MessageSource);
            Assert.AreEqual(secondKey, firstChild.DestinationKey);

        }

        [Test]
        public void DirectSendTest()
        {
            this.mInitialPeerAddressList.Add(PeerAddress.GetLoopback(this.mNextPort++));

            MockNode firstNode = this.mTransport.NewNode(this.mInitialPeerAddressList[0]);
            MockRouterChild firstChild = new MockRouterChild();
            Router firstRouter = new Router(firstNode, firstNode, firstChild, this.eventQueue, firstNode.AddressProvider, firstNode.ConnectivityReporter, firstNode.ConnectionNotifier);
            firstNode.ForgeMethodList();

            MockNode secondNode = this.mTransport.NewNode(PeerAddress.GetLoopback(this.mNextPort++));
            MockRouterChild secondChild = new MockRouterChild();
            Router secondRouter = new Router(secondNode, secondNode, secondChild, this.eventQueue, secondNode.AddressProvider, secondNode.ConnectivityReporter, secondNode.ConnectionNotifier);
            secondNode.ForgeMethodList();

            Assert.AreEqual(0, firstChild.NumberMessagesReceived);
            Assert.AreEqual(0, secondChild.NumberMessagesReceived);

            // Enter first node
            firstNode.ConnectivityReporter.SetStatus(ConnectivityStatus.Online);
            this.Join(firstNode);
            this.DoEvents(TimeSpan.FromSeconds(60));

            // Enter second node
            this.Join(secondNode);
            secondNode.ConnectivityReporter.SetStatus(ConnectivityStatus.Online);
            this.DoEvents(TimeSpan.FromSeconds(60));

            // Test that two nodes can enter and join
            Assert.IsTrue(firstNode.IsKnown(secondNode.AddressProvider.PublicAddress));
            Assert.IsTrue(secondNode.IsKnown(firstNode.AddressProvider.PublicAddress));
            Assert.IsTrue(firstRouter.LeafSet.Contains(secondRouter.LeafSet[0]));
            Assert.IsTrue(secondRouter.LeafSet.Contains(firstRouter.LeafSet[0]));

            HashKey firstKey = firstRouter.LocalHashKey;
            HashKey secondKey = secondRouter.LocalHashKey;

            // Test that a node can direct send to another.
            DhtEnvelope envelope = new DhtEnvelope(new TransportEnvelope(secondNode.AddressProvider.PublicAddress, QualityOfService.Reliable));
            firstRouter.DirectSend(envelope);

            this.DoEvents(TimeSpan.FromSeconds(10));
            Assert.AreEqual(1, secondChild.NumberMessagesReceived);
            Assert.AreEqual(envelope.Length, secondChild.ReceivedMessage.Length);
            Assert.AreEqual(firstNode.AddressProvider.PublicAddress, secondChild.MessageSource);
        }




        // Test that when excluding itself the route fails.
    }

}
