﻿using System;
using Badumna.Core;
using Badumna.DistributedHashTable;
using NUnit.Framework;

namespace BadumnaTests.DistributedHashTable
{
    [TestFixture]
    public class NodeInfoTester
    {
        private NodeInfo mNodeInfo;

        [Test]
        public void EqualsTest()
        {
            PeerAddress addressOne = PeerAddress.GetLoopback(1);
            PeerAddress addressTwo = PeerAddress.GetLoopback(2);

            this.mNodeInfo = new NodeInfo(addressOne, HashKey.Mid);

            Assert.AreEqual(this.mNodeInfo, this.mNodeInfo);
            Assert.AreNotEqual(this.mNodeInfo, new NodeInfo(addressTwo, HashKey.Mid));

            Assert.AreEqual(this.mNodeInfo, addressOne);
            Assert.AreNotEqual(this.mNodeInfo, addressTwo);

            Assert.AreEqual(this.mNodeInfo.GetHashCode(), this.mNodeInfo.GetHashCode());
        }

        [Test, ExpectedException(typeof(ArgumentNullException))]
        public void NullAddressTest()
        {
            this.mNodeInfo = new NodeInfo(null, HashKey.Random());
        }

        [Test, ExpectedException(typeof(ArgumentNullException))]
        public void NullKeyTest()
        {
            this.mNodeInfo = new NodeInfo(PeerAddress.GetLoopback(1), null);
        }
    }
}
