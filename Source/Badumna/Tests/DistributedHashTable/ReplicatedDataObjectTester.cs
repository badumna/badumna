﻿using System.Collections.Generic;
using Badumna.Core;
using Badumna.DistributedHashTable;
using NUnit.Framework;
using Badumna.DataTypes;

namespace BadumnaTests.DistributedHashTable
{
    [TestFixture]
    [DontPerformCoverage]
    public class ReplicatedDataObjectTester : BadumnaTests.Core.ParseableTestHelper
    {
        class ParseableTester : BadumnaTests.Core.ParseableTester<ReplicatedDataObject>
        {
            public override void AssertAreEqual(ReplicatedDataObject expected, ReplicatedDataObject actual)
            {
                Assert.AreEqual(expected.Data, actual.Data);
            }

            public override ICollection<ReplicatedDataObject> CreateExpectedValues()
            {
                ICollection<ReplicatedDataObject> expectedValues = new List<ReplicatedDataObject>();
                ReplicatedDataObject expected = new ReplicatedDataObject("Some key", new BadumnaId(PeerAddress.GetLoopback(1), 1));

                expected.Data = "The first example string";
                expectedValues.Add(expected);

                return expectedValues;
            }
        }

        public ReplicatedDataObjectTester()
            : base(new ParseableTester())
        { }
    }
}
