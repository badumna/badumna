﻿using System;
using System.Collections.Generic;
using System.Text;
using Badumna.DistributedHashTable;
using BadumnaTests.Core;
using Badumna.Core;
using Badumna.Transport;

namespace BadumnaTests.DistributedHashTable
{
    [DontPerformCoverage]
    class MockRouter : IRouter
    {
        private HashKey mKey;
        public HashKey LocalHashKey { get { return this.mKey; } }

        public int UniqueNeighbourCount { get { return 0; } }

        private BaseEnvelope mMessageEnvelope;
        public BaseEnvelope MessageEnvelope { get { return this.mMessageEnvelope; } }

        private PeerAddress mDestinationAddress;
        public PeerAddress DestinationAddress { get { return this.mDestinationAddress; } }

        private ICollection<PeerAddress> mExcludedAddresses;
        public ICollection<PeerAddress> ExcludedAddresses { get { return this.mExcludedAddresses; } }

        public bool IsInitialized { get { return true; } }
        public bool IsRouting { get { return true; } }

        private DhtFacade mFacade;
        public DhtFacade Facade
        {
            get { return this.mFacade; }
            set { this.mFacade = value; }
        }

        public HashKey NeighboursKey(int index)
        {
            return this.mKey;
        }

        public bool MapsToLocalPeer(HashKey key, ICollection<PeerAddress> excludedNodes)
        {
            return true;
        }

        public NodeInfo[] GetImmediateNeighbours()
        {
            return new NodeInfo[2];
        }

        public NodeInfo[] GetRoutingTableNodes()
        {
            throw new NotImplementedException("Not implemented.");
        }

        public void InitializeConnectionWith(PeerAddress peer)
        {
        }

        public void Initialize(ICollection<PeerAddress> initialPeers)
        {
            // No need to initialize, but required for interface.
        }

        public void RouteMessage(DhtEnvelope messageEnvelope)
        {
            this.mKey = messageEnvelope.DestinationKey;
            this.mMessageEnvelope = messageEnvelope;
        }

        public void RouteMessage(DhtEnvelope payloadMessage, IList<PeerAddress> excludedAddressList)
        {
        }

        public void Broadcast(TransportEnvelope messageEnvelope)
        {
        }

        private void ExcludedRouteEventHandler(object sender, ExcludedRouteEventArgs e)
        {
            this.mKey = e.MessageEnvelope.DestinationKey;
            this.mMessageEnvelope = e.MessageEnvelope;
            this.mExcludedAddresses = e.ExcludedNeighbours;
        }

        public void DirectSend(Badumna.Transport.TransportEnvelope messageEnvelope)
        {
            this.mDestinationAddress = messageEnvelope.Destination;
            this.mMessageEnvelope = messageEnvelope;
        }
    }
}
