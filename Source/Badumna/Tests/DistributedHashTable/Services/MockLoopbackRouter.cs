﻿using System;
using System.Collections.Generic;
using System.Text;
using Badumna.DistributedHashTable;
using Badumna.Core;
using Badumna.Transport;

namespace BadumnaTests.DistributedHashTable.Services
{
    class MockLoopbackRouter : IRouter
    {
        private DhtFacade mDhtFacade;
        public DhtFacade DhtFacade
        {
            get { return this.mDhtFacade; }
            set { this.mDhtFacade = value; }
        }
        public int UniqueNeighbourCount { get { return 0; } }

        public bool IsInitialized { get { return true; } }
        public bool IsRouting { get { return true; } }

        private HashKey mKey = HashKey.Min;
        public HashKey LocalHashKey { get { return this.mKey; } }

        private NetworkEventQueue eventQueue;
        private INetworkAddressProvider addressProvider;

        public MockLoopbackRouter(NetworkEventQueue eventQueue, INetworkAddressProvider addressProvider)
        {
            this.eventQueue = eventQueue;
            this.addressProvider = addressProvider;
        }

        public HashKey NeighboursKey(int index)
        {
            return this.mKey;
        }

        public bool MapsToLocalPeer(HashKey key, ICollection<PeerAddress> excludedNodes)
        {
            return true;
        }

        public NodeInfo[] GetRoutingTableNodes()
        {
            return this.GetImmediateNeighbours();
        }

        
        public IList<NodeInfo> GetReplicatingNodesList(HashKey key, int maximumHopDistance)
        {
            return new List<NodeInfo>();
        }
        
    
        public NodeInfo[] GetImmediateNeighbours()
        {
            return new NodeInfo[2];
        }

        public void InitializeConnectionWith(PeerAddress peer)
        { }

        public void Initialize(ICollection<PeerAddress> initialPeers)
        {
            // No need to initialize, but required for interface.
        }

        public void RouteMessage(DhtEnvelope messageEnvelope)
        {
            this.eventQueue.Push(this.HandleMessage, messageEnvelope);
        }

        public void RouteMessage(DhtEnvelope payloadMessage, IList<PeerAddress> excludedAddresses)
        {
        }


        public void DirectSend(TransportEnvelope messageEnvelope)
        {
            this.eventQueue.Push(this.HandleMessage, new DhtEnvelope(messageEnvelope));
        }

        public void Broadcast(TransportEnvelope messageEnvelope)
        {
        }

        public void HandleMessage(DhtEnvelope messageEnvelope)
        {
            messageEnvelope.Source = this.addressProvider.PublicAddress;
            this.mDhtFacade.ProcessMessage(messageEnvelope);
        }
    }
}
