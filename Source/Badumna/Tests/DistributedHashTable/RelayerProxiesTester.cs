﻿//-----------------------------------------------------------------------
// <copyright file="RelayerProxiesTester.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using Badumna.Core;
using Badumna.DistributedHashTable;
using Badumna.Utilities;
using NUnit.Framework;

namespace BadumnaTests.DistributedHashTable
{   
    [TestFixture]
    [DontPerformCoverage]
    public class RelayerProxiesTester
    {
        private RelayerProxies proxy;
        private Simulator eventQueue;
        private ITime timeKeeper;

        [SetUp]
        public void SetUp()
        {
            this.eventQueue = new Simulator();
            this.timeKeeper = this.eventQueue;
        }

        [Test]
        public void GarbageCollectionWorksFineOnEmptyMessageQueue()
        {
            this.proxy = new RelayerProxies(PeerAddress.Nowhere, null, this.timeKeeper);
            Assert.AreEqual(0, this.proxy.Count);
            this.proxy.GarbageCollection();
            Assert.AreEqual(0, this.proxy.Count);
        }

        [Test]
        public void GarbageCollectionSuccessfullyRemoveOldMessages()
        {
            this.proxy = new RelayerProxies(PeerAddress.Nowhere, null, this.timeKeeper);
            Assert.AreEqual(0, this.proxy.Count);

            for (int i = 0; i < 10; i++)
            {
                this.proxy.QueueMessage(new Badumna.Transport.TransportEnvelope());
                this.eventQueue.RunFor(TimeSpan.FromSeconds(Parameters.MaxTimeToKeepMessageInRelayProxyMessageQueue.TotalSeconds + 1));
            }

            Assert.AreEqual(10, this.proxy.MessageCount);

            this.proxy.QueueMessage(new Badumna.Transport.TransportEnvelope());
            this.proxy.QueueMessage(new Badumna.Transport.TransportEnvelope());

            Assert.AreEqual(12, this.proxy.MessageCount);

            this.proxy.GarbageCollection();

            Assert.AreEqual(2, this.proxy.MessageCount);
        }
    }
}
