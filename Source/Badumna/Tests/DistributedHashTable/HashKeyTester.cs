﻿using System;
using System.Collections.Generic;
using System.Text;
using Badumna.Core;
using Badumna.DistributedHashTable;
using BadumnaTests.Core;
using NUnit.Framework;

namespace BadumnaTests.DistributedHashTable
{
    [TestFixture]
    [DontPerformCoverage]
    public class HashKeyTester : BadumnaTests.Core.ParseableTestHelper
    {
        class ParseableTester : BadumnaTests.Core.ParseableTester<HashKey>
        {
            public override ICollection<HashKey> CreateExpectedValues()
            {
                ICollection<HashKey> hashKey = new List<HashKey>();

                hashKey.Add(HashKey.Hash("Expected hash key"));

                return hashKey;
            }
        }

        private HashKey mKey;

        public HashKeyTester()
            : base(new ParseableTester())
        {
        }

        [SetUp]
        public new void Initialize()
        {
            this.mKey = new HashKey();
            base.Initialize();
        }

        [Test]
        public void DefaultConstructorTest()
        {
            // Test that the default constructor sets inizializes the key to 0.
            for (int i = 0; i < HashKey.Length; i++)
            {
                Assert.AreEqual(0, this.mKey[0]);
            }
        }

        [Test, ExpectedException(typeof(ArgumentException))]
        public void ConstructorFailTest()
        {
            byte[] bytes = new byte[HashKey.Length + 1];

            // Test that if an array of incorrect length is given to the constructor an exception is thrown.
            this.mKey = new HashKey(bytes);
        }

        [Test]
        public void OverflowConstructorTest()
        {
            byte[] bytes = new byte[HashKey.Length];

            for (int i = 0; i < HashKey.Length; i++)
            {
                bytes[i] = 0xff;
            }

            // Test that a HashKey.Max value is set to HashKey.Min in the constructor.
            this.mKey = new HashKey(bytes);
            Assert.AreEqual(HashKey.Min, this.mKey);
        }

        [Test]
        public void OperatorTest()
        {
            // Test get/set [] operator.
            Assert.AreEqual(0, this.mKey[0]);
            this.mKey[0] = 0xff;
            Assert.AreEqual(0xff, this.mKey[0]);
        }

        [Test, ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void OutOfRangeTest()
        {
            // Test that an out of range parameter to [] throws an exception
            this.mKey[-1] = 0xff;
        }

        [Test, ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void OutOfRangeTest2()
        {
            // Test that an out of range parameter to [] throws an exception
            byte b = this.mKey[HashKey.Length];
        }

        [Test]
        public void AsKeyTest()
        {
            this.mKey = HashKey.Random();
            MessageBuffer message = new MessageBuffer();
            Dictionary<HashKey, string> store = new Dictionary<HashKey, string>();

            store.Add(this.mKey, "some string");

            this.mKey.ToMessage(message, typeof(HashKey));
            HashKey r = new HashKey();
            r.FromMessage(message);

            //System.Console.WriteLine("{0} -- {1}", this.mKey.GetHashCode(), r.GetHashCode());
            //System.Console.WriteLine("{0} - {1}", this.mKey, r);
            Assert.AreEqual(this.mKey, r);
            Assert.IsTrue(store.ContainsKey(r));
        }

        [Test]
        public void StringTest()
        {
            Assert.AreEqual(this.mKey.ToString(), this.mKey.ToString());
        }

        [Test]
        public void OrderTest()
        {
            HashKey key = new HashKey();

            // Test ordering is correct.
            this.mKey[0] = 0x01;
            Assert.Less(key, this.mKey);

            // Test that byte significance is correct (left to right).
            key[HashKey.Length - 1] = 0x01;
            Assert.Less(key, this.mKey);
            key[0] = 0x01;
            Assert.Less(this.mKey, key);

            // Test Equality.
            this.mKey[HashKey.Length - 1] = 0x01;
            Assert.AreEqual(this.mKey, key);
        }

        [Test]
        public void AbsoluteDistanceTest()
        {
            HashKey zero = new HashKey();
            HashKey one = new HashKey();

            // Set hash key values.
            one[HashKey.Length - 1] = 0x01;

            // Test that distance is correct.
            Assert.AreEqual(one, one.AbsoluteDistanceFrom(zero));
            Assert.AreEqual(one, zero.AbsoluteDistanceFrom(one));

            // TODO: HashKey.Max has some unusual properties... (see HashKey.CheckOverflow)
            Assert.AreEqual(one, zero.AbsoluteDistanceFrom(HashKey.Max).AbsoluteDistanceFrom(zero));
            Assert.AreEqual(one, zero.AbsoluteDistanceFrom(HashKey.Max));
            Assert.AreEqual(one, HashKey.Max.AbsoluteDistanceFrom(zero));

            Assert.AreEqual(zero, one.AbsoluteDistanceFrom(one));
            Assert.AreEqual(zero, HashKey.Max.AbsoluteDistanceFrom(HashKey.Max));
            Assert.AreEqual(zero, HashKey.Mid.AbsoluteDistanceFrom(HashKey.Mid));
            Assert.AreEqual(HashKey.Mid, zero.AbsoluteDistanceFrom(HashKey.Mid));
            Assert.AreEqual(HashKey.Mid, HashKey.Mid.AbsoluteDistanceFrom(zero));
            Assert.AreEqual(one, one.AbsoluteDistanceFrom(HashKey.Mid).AbsoluteDistanceFrom(HashKey.Mid));
            Assert.AreEqual(zero, HashKey.Mid.AbsoluteDistanceFrom(zero).AbsoluteDistanceFrom(HashKey.Mid));
            Assert.AreEqual(one, HashKey.Mid.AbsoluteDistanceFrom(HashKey.Max).AbsoluteDistanceFrom(HashKey.Mid));
            Assert.AreEqual(HashKey.Mid.AbsoluteDistanceFrom(one), HashKey.Mid.AbsoluteDistanceFrom(HashKey.Max));
            Assert.AreEqual(one.AbsoluteDistanceFrom(HashKey.Mid), HashKey.Max.AbsoluteDistanceFrom(HashKey.Mid));
            Assert.AreEqual(zero, HashKey.Hash("Test string").AbsoluteDistanceFrom(HashKey.Hash("Test string")));
            Assert.AreEqual(null, this.mKey.AbsoluteDistanceFrom(null));
            Assert.AreEqual(null, this.mKey.DistanceFrom(null));

            // Test that no pair of keys will resturn a value greater than HashKey.Mid.
            this.mKey = HashKey.Random();
            for (int i = 0; i < 200; i++)
            {
                Assert.IsTrue(this.mKey.AbsoluteDistanceFrom(HashKey.Random()) <= HashKey.Mid);
            }
        }

        [Test]
        public void MinusTest()
        {
            HashKey zero = this.mKey;
            HashKey one = new HashKey();

            // Set hash key values.
            one[HashKey.Length - 1] = 0x01;

            // Test that distance is correct.
            Assert.AreEqual(one, one.DistanceFrom(zero));
            Assert.AreEqual(HashKey.Max, zero.Subtract(one));
            Assert.AreEqual(one, zero.DistanceFrom(HashKey.Max)); // overflow
            Assert.AreEqual(HashKey.Max, HashKey.Max.DistanceFrom(zero));
            Assert.AreEqual(zero, one.DistanceFrom(one));
            Assert.AreEqual(zero, HashKey.Max.DistanceFrom(HashKey.Max));
            Assert.AreEqual(zero, HashKey.Mid.DistanceFrom(HashKey.Mid));
            Assert.AreEqual(HashKey.Mid, zero.DistanceFrom(HashKey.Mid));
            Assert.AreEqual(HashKey.Mid, HashKey.Mid.DistanceFrom(zero));
            Assert.AreEqual(one, one.DistanceFrom(HashKey.Mid).DistanceFrom(HashKey.Mid));
            Assert.AreEqual(zero, HashKey.Mid.DistanceFrom(zero).DistanceFrom(HashKey.Mid));
            Assert.AreEqual(zero, HashKey.Hash("Test string").DistanceFrom(HashKey.Hash("Test string")));

        }

        [Test]
        public void FirstDifferentDigitTest()
        {
            byte remainder;
            this.mKey = HashKey.Random();
            this.mKey[HashKey.Length - 1] = 0x87;  // So we can predict the remainder
            HashKey copyKey = this.mKey.Copy();

            // Test that an identical copy will return -1 from the FirstDifferentDigit() method.
            Assert.AreEqual(-1, this.mKey.FirstDifferentDigit(copyKey, 8, out remainder));
            Assert.AreEqual(0, remainder);
            Assert.AreEqual(-1, copyKey.FirstDifferentDigit(this.mKey, 8, out remainder));
            Assert.AreEqual(0, remainder);

            // Test that we can get the first different digit.
            copyKey[HashKey.Length - 1] = 0x86;
            Assert.AreEqual(HashKey.Length - 1, this.mKey.FirstDifferentDigit(copyKey, 8, out remainder));
            Assert.AreEqual(1, remainder);
            Assert.AreEqual(HashKey.Length - 1, copyKey.FirstDifferentDigit(this.mKey, 8, out remainder));
            Assert.AreEqual(255, remainder);

            // Test the result is correct for various digit sizes.
            Assert.AreEqual(HashKey.Length * 2 - 1, this.mKey.FirstDifferentDigit(copyKey, 4, out remainder));
            Assert.AreEqual(1, remainder);
            Assert.AreEqual(HashKey.Length * 2 - 1, copyKey.FirstDifferentDigit(this.mKey, 4, out remainder));
            Assert.AreEqual(15, remainder);

            Assert.AreEqual(HashKey.Length * 4 - 1, this.mKey.FirstDifferentDigit(copyKey, 2, out remainder));
            Assert.AreEqual(1, remainder);
            Assert.AreEqual(HashKey.Length * 4 - 1, copyKey.FirstDifferentDigit(this.mKey, 2, out remainder));
            Assert.AreEqual(3, remainder);

            Assert.AreEqual(HashKey.Length * 8 - 1, this.mKey.FirstDifferentDigit(copyKey, 1, out remainder));
            Assert.AreEqual(1, remainder);
            Assert.AreEqual(HashKey.Length * 8 - 1, copyKey.FirstDifferentDigit(this.mKey, 1, out remainder));
            Assert.AreEqual(1, remainder);

            // Test again for a different location in the byte.
            copyKey[HashKey.Length - 1] = 0x80;
            Assert.AreEqual(HashKey.Length * 2 - 1, this.mKey.FirstDifferentDigit(copyKey, 4, out remainder));
            Assert.AreEqual(7, remainder);
            Assert.AreEqual(HashKey.Length * 2 - 1, copyKey.FirstDifferentDigit(this.mKey, 4, out remainder));
            Assert.AreEqual(9, remainder);

            Assert.AreEqual(HashKey.Length * 4 - 2, this.mKey.FirstDifferentDigit(copyKey, 2, out remainder));
            Assert.AreEqual(1, remainder);
            Assert.AreEqual(HashKey.Length * 4 - 2, copyKey.FirstDifferentDigit(this.mKey, 2, out remainder));
            Assert.AreEqual(3, remainder);

            Assert.AreEqual(HashKey.Length * 8 - 3, this.mKey.FirstDifferentDigit(copyKey, 1, out remainder));
            Assert.AreEqual(1, remainder);
            Assert.AreEqual(HashKey.Length * 8 - 3, copyKey.FirstDifferentDigit(this.mKey, 1, out remainder));
            Assert.AreEqual(1, remainder);

            // Test again for a different byte and remainder.
            this.mKey[5] = 0xDC;
            copyKey[5] = 0xD8;
            Assert.AreEqual(5, this.mKey.FirstDifferentDigit(copyKey, 8, out remainder));
            Assert.AreEqual(4, remainder);
            Assert.AreEqual(5, copyKey.FirstDifferentDigit(this.mKey, 8, out remainder));
            Assert.AreEqual(252, remainder);

            Assert.AreEqual(6 * 2 - 1, this.mKey.FirstDifferentDigit(copyKey, 4, out remainder));
            Assert.AreEqual(4, remainder);
            Assert.AreEqual(6 * 2 - 1, copyKey.FirstDifferentDigit(this.mKey, 4, out remainder));
            Assert.AreEqual(12, remainder);

            Assert.AreEqual(6 * 4 - 2, this.mKey.FirstDifferentDigit(copyKey, 2, out remainder));
            Assert.AreEqual(1, remainder);
            Assert.AreEqual(6 * 4 - 2, copyKey.FirstDifferentDigit(this.mKey, 2, out remainder));
            Assert.AreEqual(3, remainder);

            Assert.AreEqual(6 * 8 - 3, this.mKey.FirstDifferentDigit(copyKey, 1, out remainder));
            Assert.AreEqual(1, remainder);
            Assert.AreEqual(6 * 8 - 3, copyKey.FirstDifferentDigit(this.mKey, 1, out remainder));
            Assert.AreEqual(1, remainder);

            // Test for the first byte.
            this.mKey[0] = 0x00;
            copyKey[0] = 0xFF;
            Assert.AreEqual(0, this.mKey.FirstDifferentDigit(copyKey, 8, out remainder));
            Assert.AreEqual(1, remainder);
            Assert.AreEqual(0, copyKey.FirstDifferentDigit(this.mKey, 8, out remainder));
            Assert.AreEqual(255, remainder);

            Assert.AreEqual(0, this.mKey.FirstDifferentDigit(copyKey, 4, out remainder));
            Assert.AreEqual(1, remainder);
            Assert.AreEqual(0, copyKey.FirstDifferentDigit(this.mKey, 4, out remainder));
            Assert.AreEqual(15, remainder);

            Assert.AreEqual(0, this.mKey.FirstDifferentDigit(copyKey, 2, out remainder));
            Assert.AreEqual(1, remainder);
            Assert.AreEqual(0, copyKey.FirstDifferentDigit(this.mKey, 2, out remainder));
            Assert.AreEqual(3, remainder);

            Assert.AreEqual(0, this.mKey.FirstDifferentDigit(copyKey, 1, out remainder));
            Assert.AreEqual(1, remainder);
            Assert.AreEqual(0, copyKey.FirstDifferentDigit(this.mKey, 1, out remainder));
            Assert.AreEqual(1, remainder);
        }

        [Test]
        public void OppositeTest()
        {
            HashKey oppositeOne = new HashKey();

            // Set hash key values.
            this.mKey[HashKey.Length - 1] = 0x01;
            oppositeOne[HashKey.Length - 1] = 0x01;
            oppositeOne[0] = 0x80;

            // Test that the value return by Opposite() is correct.
            Assert.AreEqual(HashKey.Mid, this.mKey.Opposite().DistanceFrom(this.mKey));
            Assert.AreEqual(oppositeOne, this.mKey.Opposite());

            Assert.AreEqual(HashKey.Mid, HashKey.Max.Opposite());
            Assert.AreEqual(HashKey.Mid, HashKey.Min.Opposite());
            Assert.AreEqual(HashKey.Min, HashKey.Mid.Opposite());

            for (int i = 0; i < 200; i++)
            {
                HashKey randomKey = HashKey.Random();

                Assert.AreEqual(HashKey.Mid, randomKey.Opposite().DistanceFrom(randomKey));
            }
        }


        private int HashKeyComparer(HashKey firstComparand, HashKey secondComparand)
        {
            if (null == firstComparand)
            {
                throw new ArgumentNullException("firstComparand");
            }

            if (null == secondComparand)
            {
                throw new ArgumentNullException("secondComparand");
            }

            if (firstComparand.Precedes(secondComparand))
            {
                Assert.IsTrue(secondComparand.Succeeds(firstComparand));
                return -1;
            }

            if (secondComparand.Precedes(firstComparand))
            {
                Assert.IsTrue(firstComparand.Succeeds(secondComparand));
                return 1;
            }

            Assert.AreEqual(firstComparand, secondComparand);
            return 0;
        }


        [Test]
        public void PrecedenceTest()
        {
            HashKey zero = this.mKey;
            HashKey one = new HashKey();

            // Set hash key values.
            one[HashKey.Length - 1] = 0x01;

            // Test that precedence is correct.
            Assert.IsTrue(zero.Precedes(one));
            Assert.IsTrue(one.Succeeds(zero));
            Assert.IsTrue(HashKey.Max.Precedes(zero));
            Assert.IsTrue(HashKey.Max.Precedes(one));

            // Test opposites.
            Assert.IsTrue(zero.Precedes(HashKey.Mid));
            Assert.IsTrue(HashKey.Mid.Succeeds(zero));
            Assert.IsTrue(HashKey.Max.Precedes(HashKey.Mid));
            Assert.IsTrue(HashKey.Mid.Succeeds(HashKey.Max));
            HashKey random = HashKey.Random();
            Assert.IsTrue(random.Opposite().Precedes(random));
            Assert.IsTrue(random.Succeeds(random.Opposite()));
        }


        [Test]
        public void NullExceptionTests()
        {
            int exceptionsCaught = 0;
            bool result;

            // <=
            try
            {
                result = null <= this.mKey;
            }
            catch (ArgumentNullException)
            {
                exceptionsCaught++;
            }
            try
            {
                result = this.mKey <= null;
            }
            catch (ArgumentNullException)
            {
                exceptionsCaught++;
            }

            // <
            try
            {
                result = null < this.mKey;
            }
            catch (ArgumentNullException)
            {
                exceptionsCaught++;
            }
            try
            {
                result = this.mKey < null;
            }
            catch (ArgumentNullException)
            {
                exceptionsCaught++;
            }

            // >=
            try
            {
                result = null >= this.mKey;
            }
            catch (ArgumentNullException)
            {
                exceptionsCaught++;
            }
            try
            {
                result = this.mKey >= null;
            }
            catch (ArgumentNullException)
            {
                exceptionsCaught++;
            }

            // >
            try
            {
                result = null > this.mKey;
            }
            catch (ArgumentNullException)
            {
                exceptionsCaught++;
            }
            try
            {
                result = this.mKey > null;
            }
            catch (ArgumentNullException)
            {
                exceptionsCaught++;
            }

            Assert.AreEqual(8, exceptionsCaught);
        }


        [Test]
        public void CompareTest()
        {
            HashKey anotherKey = this.mKey;

            Assert.IsTrue(this.mKey == anotherKey);
            Assert.IsFalse(this.mKey != anotherKey);

            Assert.IsTrue(null != this.mKey);
            Assert.IsFalse(null == this.mKey);

            Assert.IsTrue(this.mKey != null);
            Assert.IsFalse(this.mKey == null);

            Assert.IsTrue(this.mKey.Equals(this.mKey));
            Assert.IsFalse(this.mKey.Equals(null));
        }

        [Test]
        public void NullArgumentTest()
        {
            int exceptionsCaught = 0;

            try
            {
                new HashKey(null);
            }
            catch (ArgumentNullException)
            {
                exceptionsCaught++;
            }
            try
            {
                byte tmp;
                this.mKey.FirstDifferentDigit(null, 0, out tmp);
            }
            catch (ArgumentNullException)
            {
                exceptionsCaught++;
            }
            try
            {
                this.mKey.CompareTo(exceptionsCaught);
                this.mKey.CompareTo(null);
            }
            catch (ArgumentNullException)
            {
                exceptionsCaught++;
            }
            try
            {
                this.mKey.CompareTo((object)null);
            }
            catch (ArgumentNullException)
            {
                exceptionsCaught++;
            }

            Assert.AreEqual(4, exceptionsCaught);
        }

        [Test]
        public void DivideTest()
        {
            ulong value = 0xff00000000000000;
            HashKey key = new HashKey(value);

            for (int i = 0; i < 20 * 8; i++)
            {
                //System.Console.WriteLine("{0} {1}", key.ToString(), value);

                Assert.AreEqual(new HashKey(value), key);
                key = key.DivideByPowerOfTwo(1);
                value /= 2;
            }
        }

        [Test]
        public void ToBytesOutputCanBeUsedToConstructDuplicateHashKey()
        {
            HashKey key = HashKey.Random();
            HashKey copy = new HashKey(key.ToBytes());
            Assert.AreEqual(key, copy);
        }
    }
}
