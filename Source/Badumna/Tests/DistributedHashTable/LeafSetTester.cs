﻿using System;
using System.Collections.Generic;
using System.Text;
using Badumna.Core;
using Badumna.DistributedHashTable;
using BadumnaTests.Core;
using NUnit.Framework;
using Badumna.Utilities;

namespace BadumnaTests.DistributedHashTable
{
    [TestFixture]
    [DontPerformCoverage]
    public class LeafSetTester : BadumnaTests.Core.ParseableTestHelper
    {
        class ParseableTester : BadumnaTests.Core.ParseableTester<LeafSet>
        {
            internal LeafSet mExpectedValue;

            public override ICollection<LeafSet> CreateExpectedValues()
            {
                ICollection<LeafSet> expectedValues = new List<LeafSet>();

                if (null == this.mExpectedValue)
                {
                    this.mExpectedValue = new LeafSet();
                    this.mExpectedValue.Initialize(LeafSetTester.GetLocalInfo().Address);
                    for (int i = 0; i < LeafSet.MaximumNumberOfNeighbours; i++)
                    {
                        this.mExpectedValue.AddNeighbour(PeerAddress.GetLoopback(i + 66));
                    }
                    LeafSetTester.AssertValidity(this.mExpectedValue);

                    Assert.AreEqual(this.mExpectedValue.PredecessorCount, this.mExpectedValue.SuccessorCount);
                    Assert.AreEqual(LeafSet.MaximumNumberOfNeighbours, this.mExpectedValue.UniqueNodeCount);
                }

                expectedValues.Add(this.mExpectedValue);

                return expectedValues;
            }

            public override void AssertAreEqual(LeafSet expected, LeafSet actual)
            {
                Assert.AreEqual(expected.LocalNodeInfo, actual.LocalNodeInfo);
                Assert.AreEqual(expected.UniqueNodeCount, actual.UniqueNodeCount);
                Assert.AreEqual(expected.PredecessorCount, actual.PredecessorCount);
                Assert.AreEqual(expected.SuccessorCount, actual.SuccessorCount);
                Assert.AreEqual(expected.ImmediatePredecessor, actual.ImmediatePredecessor);
                Assert.AreEqual(expected.ImmediateSuccessor, actual.ImmediateSuccessor);

                for (int i = -expected.PredecessorCount; i <= expected.SuccessorCount; i++)
                {
                    Assert.AreEqual(expected[i], actual[i]);
                    Assert.AreEqual(expected[i].Key, actual[i].Key);
                }

                LeafSetTester.AssertValidity(expected);
            }
        }

        private NodeInfo mAddedNodeInfo;
        private NodeInfo mRemovedNodeInfo;

        private LeafSet mLeafSet;

        public LeafSetTester()
            : base(new ParseableTester())
        { }

        [SetUp]
        public new void Initialize()
        {
            this.mLeafSet = new LeafSet();
            this.mAddedNodeInfo = null;
            this.mRemovedNodeInfo = null;
            base.Initialize();
        }

        private static NodeInfo GetLocalInfo()
        {
            PeerAddress localAddress = PeerAddress.GetLoopback(0);
            return new NodeInfo(localAddress, HashKey.Hash(localAddress));
        }

        private NodeInfo GetNewPredecessorInfo()
        {
            NodeInfo localNodeInfo = LeafSetTester.GetLocalInfo();
            NodeInfo predecessor = new NodeInfo(PeerAddress.GetLoopback(RandomSource.Generator.Next(65000)), localNodeInfo.Key);

            while (!predecessor.Key.Precedes(localNodeInfo.Key))
            {
                predecessor = new NodeInfo(PeerAddress.GetLoopback(RandomSource.Generator.Next(65000)), localNodeInfo.Key);
            }

            return predecessor;
        }

        private NodeInfo GetNewSuccessorInfo()
        {
            NodeInfo localNodeInfo = LeafSetTester.GetLocalInfo();
            NodeInfo successor = new NodeInfo(PeerAddress.GetLoopback(RandomSource.Generator.Next(65000)), localNodeInfo.Key);

            while (!successor.Key.Succeeds(localNodeInfo.Key))
            {
                successor = new NodeInfo(PeerAddress.GetLoopback(RandomSource.Generator.Next(65000)), localNodeInfo.Key);
            }

            return successor;
        }

        private NodeInfo GetNewRandomInfo()
        {
            NodeInfo localNodeInfo = LeafSetTester.GetLocalInfo();
            return new NodeInfo(PeerAddress.GetLoopback(RandomSource.Generator.Next(65000)), localNodeInfo.Key);
        }

        private void AddedNodeHandler(object sender, EventArgs e)
        {
            LeafSetChangeEventArgs arg = e as LeafSetChangeEventArgs;

            if (null != arg)
            {
                this.mAddedNodeInfo = arg.NodeInfo;
            }
        }

        private void RemovedNodeHandler(object sender, EventArgs e)
        {
            LeafSetChangeEventArgs arg = e as LeafSetChangeEventArgs;

            if (null != arg)
            {
                this.mRemovedNodeInfo = arg.NodeInfo;
            }
        }

        [Test, ExpectedException(typeof(InvalidOperationException))]
        public void InvalidOperationTest()
        {
            // Test that attempting to add a neighbour prior to initialization fails.
            this.mLeafSet.AddNeighbour(PeerAddress.GetLoopback(0));
        }

        private static void AssertValidity(LeafSet leafSet)
        {
            List<PeerAddress> addressList = new List<PeerAddress>();
            for (int j = -leafSet.PredecessorCount; j <= leafSet.SuccessorCount; j++)
            {
                if (!addressList.Contains(leafSet[j].Address))
                {
                    addressList.Add(leafSet[j].Address);
                }
            }

            Assert.AreEqual(addressList.Count - 1, leafSet.UniqueNodeCount);
            Assert.AreEqual(leafSet.SuccessorCount, leafSet.PredecessorCount);

            NodeInfo lastNode = leafSet[0];
            for (int i = -1; i >= -leafSet.PredecessorCount; i--)
            {
                Assert.IsTrue(lastNode.Distance(false) < leafSet[i].Distance(false),
                    "Incorrect order of nodes in the leafset.");
                lastNode = leafSet[i];
            }

            lastNode = leafSet[0];
            for (int i = 1; i <= leafSet.SuccessorCount; i++)
            {
                Assert.IsTrue(lastNode.Key.DistanceFrom(leafSet[0].Key) < leafSet[i].Distance(true),
                    "Incorrect order of nodes in the leafset.");
                lastNode = leafSet[i];
            }

        }


        [Test]
        public void AddLocalTest()
        {
            LeafSet leafSet = new LeafSet();

            leafSet.Initialize(LeafSetTester.GetLocalInfo().Address);
            LeafSetTester.AssertValidity(leafSet);

            // Test that null is returned from GetImmediate*() methods.
            Assert.AreEqual(null, leafSet.ImmediatePredecessor);
            Assert.AreEqual(null, leafSet.ImmediateSuccessor);
            Assert.AreEqual(leafSet[0], leafSet.LocalNodeInfo);

            // Test that adding the local node does nothing.
            Assert.IsFalse(leafSet.IsCandidateForAddition(leafSet[0].Address));
            leafSet.AddNeighbour(LeafSetTester.GetLocalInfo().Address);
            Assert.AreEqual(0, leafSet.UniqueNodeCount);
            LeafSetTester.AssertValidity(leafSet);

            // Test that null is still returned from GetImmediate*() methods.
            Assert.AreEqual(null, leafSet.ImmediatePredecessor);
            Assert.AreEqual(null, leafSet.ImmediateSuccessor);

            // Test that duplicates are not re added.
            NodeInfo successor = this.GetNewSuccessorInfo();
            NodeInfo predecessor = this.GetNewPredecessorInfo();

            Assert.IsTrue(leafSet.IsCandidateForAddition(successor.Address));
            Assert.IsTrue(leafSet.IsCandidateForAddition(predecessor.Address));

            leafSet.AddNeighbour(successor.Address);
            leafSet.AddNeighbour(successor.Address);
            Assert.AreEqual(1, leafSet.UniqueNodeCount);
            Assert.AreEqual(1, leafSet.SuccessorCount);
            Assert.AreEqual(successor, leafSet.ImmediateSuccessor);
            Assert.AreEqual(successor, leafSet[1]);
            Assert.AreEqual(1, leafSet.PredecessorCount);
            Assert.AreEqual(successor, leafSet.ImmediatePredecessor);
            Assert.AreEqual(successor, leafSet[-1]);
            Assert.IsTrue(leafSet.Contains(successor.Address));
            Assert.IsTrue(leafSet.Contains(successor));
            Assert.IsFalse(leafSet.IsCandidateForAddition(successor.Address));
            Assert.IsTrue(leafSet.IsCandidateForAddition(predecessor.Address));
            LeafSetTester.AssertValidity(leafSet);

            // Test duplicate predecessors are not re added.
            leafSet.AddNeighbour(predecessor.Address);
            leafSet.AddNeighbour(predecessor.Address);
            Assert.AreEqual(2, leafSet.UniqueNodeCount);
            Assert.AreEqual(2, leafSet.SuccessorCount);
            Assert.AreEqual(successor, leafSet.ImmediateSuccessor);
            Assert.AreEqual(2, leafSet.PredecessorCount);
            Assert.AreEqual(predecessor, leafSet.ImmediatePredecessor);
            Assert.IsTrue(leafSet.Contains(predecessor.Address));
            Assert.IsTrue(leafSet.Contains(predecessor));
            Assert.IsFalse(leafSet.IsCandidateForAddition(successor.Address));
            Assert.IsFalse(leafSet.IsCandidateForAddition(predecessor.Address));
            LeafSetTester.AssertValidity(leafSet);

            Assert.AreEqual(successor, leafSet[1]);
            Assert.AreEqual(predecessor, leafSet[-1]);
            Assert.AreEqual(predecessor, leafSet[2]);
            Assert.AreEqual(successor, leafSet[-2]);

            LeafSetTester.AssertValidity(leafSet);
        }

        [Test]
        public void AddSuccessorTest()
        {
            LeafSet leafSet = new LeafSet();

            leafSet.NeighbourAdditionEvent += this.AddedNodeHandler;
            leafSet.NeighbourRemovalEvent += this.RemovedNodeHandler;
            leafSet.Initialize(LeafSetTester.GetLocalInfo().Address);
            LeafSetTester.AssertValidity(leafSet);

            // Test that we can add a successor.
            NodeInfo successor = this.GetNewSuccessorInfo();
            leafSet.AddNeighbour(successor.Address);
            Assert.AreEqual(1, leafSet.UniqueNodeCount);
            Assert.AreEqual(1, leafSet.SuccessorCount);
            Assert.AreEqual(successor, leafSet.ImmediateSuccessor);
            Assert.IsTrue(leafSet.Contains(successor.Address));
            Assert.IsTrue(leafSet.Contains(successor));
            LeafSetTester.AssertValidity(leafSet);

            // Test that the node is also added to the predecessor list.
            Assert.AreEqual(1, leafSet.PredecessorCount);
            Assert.AreEqual(successor, leafSet.ImmediatePredecessor);

            // Test that the addition of only successors fills the predecessor list as well
            for (int i = 1; i < LeafSet.MaximumNumberOfNeighbours; i++)
            {
                successor = this.GetNewSuccessorInfo();
                leafSet.AddNeighbour(successor.Address);
                LeafSetTester.AssertValidity(leafSet);

                Assert.AreEqual(i + 1, leafSet.UniqueNodeCount);
                Assert.IsTrue(leafSet.Contains(successor.Address));
                Assert.IsTrue(leafSet.Contains(successor));
            }
            Assert.AreEqual(LeafSet.MaximumNumberOfNeighbours / 2, leafSet.SuccessorCount);
            Assert.AreEqual(LeafSet.MaximumNumberOfNeighbours / 2, leafSet.PredecessorCount);

            // Test that the addition of a closer successors displaces other nodes.
            successor = this.GetNewSuccessorInfo();
            while (successor.ForwardDistance >= leafSet.ImmediateSuccessor.ForwardDistance)
            {
                successor = this.GetNewSuccessorInfo();
            }
            leafSet.AddNeighbour(successor.Address);
            LeafSetTester.AssertValidity(leafSet);

            Assert.IsTrue(leafSet.Contains(successor.Address));
            Assert.IsTrue(leafSet.Contains(successor));
            Assert.AreEqual(successor, leafSet.ImmediateSuccessor);

            // Test that the addition event is called when a node is entered.
            Assert.AreEqual(successor, this.mAddedNodeInfo);

            // Test that the removal event is called when a node is displaced from either list.
            Assert.AreNotEqual(null, this.mRemovedNodeInfo);

            // Test that the maximum nuber of leaf nodes is not exceeded.
            Assert.AreEqual(LeafSet.MaximumNumberOfNeighbours, leafSet.UniqueNodeCount);
            Assert.AreEqual(LeafSet.MaximumNumberOfNeighbours / 2, leafSet.SuccessorCount);
            Assert.AreEqual(LeafSet.MaximumNumberOfNeighbours / 2, leafSet.PredecessorCount);
        }

        [Test]
        public void AddPredecessorTest()
        {
            LeafSet leafSet = new LeafSet();

            leafSet.NeighbourAdditionEvent += this.AddedNodeHandler;
            leafSet.NeighbourRemovalEvent += this.RemovedNodeHandler;
            leafSet.Initialize(LeafSetTester.GetLocalInfo().Address);

            // Test that we can add a predecessor.
            NodeInfo predecessor = this.GetNewPredecessorInfo();
            leafSet.AddNeighbour(predecessor.Address);
            Assert.AreEqual(1, leafSet.UniqueNodeCount);
            Assert.AreEqual(1, leafSet.SuccessorCount);
            Assert.AreEqual(predecessor, leafSet.ImmediatePredecessor);
            Assert.IsTrue(leafSet.Contains(predecessor.Address));
            Assert.IsTrue(leafSet.Contains(predecessor));

            // Test that the node is also added to the successor list.
            Assert.AreEqual(1, leafSet.SuccessorCount);
            Assert.AreEqual(predecessor, leafSet.ImmediateSuccessor);

            // Test that the addition of only predecessor fills the successors list as well once full.
            for (int i = 1; i < LeafSet.MaximumNumberOfNeighbours; i++)
            {
                predecessor = this.GetNewPredecessorInfo();
                leafSet.AddNeighbour(predecessor.Address);
                LeafSetTester.AssertValidity(leafSet);

                Assert.AreEqual(i + 1, leafSet.UniqueNodeCount);
                Assert.IsTrue(leafSet.Contains(predecessor.Address));
                Assert.IsTrue(leafSet.Contains(predecessor));
            }
            Assert.AreEqual(LeafSet.MaximumNumberOfNeighbours / 2, leafSet.PredecessorCount);
            Assert.AreEqual(LeafSet.MaximumNumberOfNeighbours / 2, leafSet.SuccessorCount);

            // Test that the addition of a closer predecessors displaces other nodes.
            predecessor = this.GetNewPredecessorInfo();
            while (predecessor.BackwardDistance >= leafSet.ImmediatePredecessor.BackwardDistance)
            {
                predecessor = this.GetNewPredecessorInfo();
            }
            leafSet.AddNeighbour(predecessor.Address);
            LeafSetTester.AssertValidity(leafSet);

            Assert.IsTrue(leafSet.Contains(predecessor.Address));
            Assert.IsTrue(leafSet.Contains(predecessor));
            Assert.AreEqual(predecessor, leafSet.ImmediatePredecessor);

            // Test that the addition event is called when a node is entered.
            Assert.AreEqual(predecessor, this.mAddedNodeInfo);

            // Test that the removal event is called when a node is displaced from either list.
            Assert.AreNotEqual(null, this.mRemovedNodeInfo);

            // Test that the maximum nuber of leaf nodes is not exceeded.
            Assert.AreEqual(LeafSet.MaximumNumberOfNeighbours, leafSet.UniqueNodeCount);
            Assert.AreEqual(LeafSet.MaximumNumberOfNeighbours / 2, leafSet.SuccessorCount);
            Assert.AreEqual(LeafSet.MaximumNumberOfNeighbours / 2, leafSet.PredecessorCount);
        }


        private HashKey mCompareKey;
        private int CompareKeys(HashKey firstComparand, HashKey secondComparand)
        {
            return this.mCompareKey.AbsoluteDistanceFrom(firstComparand).CompareTo(this.mCompareKey.AbsoluteDistanceFrom(secondComparand));
        }

        [Test]
        public void ClosestToTest()
        {
            LeafSet leafSet = new LeafSet();
            List<HashKey> nodes = new List<HashKey>();
            List<HashKey> succs = new List<HashKey>();
            List<HashKey> preds = new List<HashKey>();

            leafSet.Initialize(LeafSetTester.GetLocalInfo().Address);

            Assert.AreEqual(LeafSetTester.GetLocalInfo(), leafSet.GetNodeClosestTo(HashKey.Random(), null));

            // Construct a list of the closest nodes, including the local node.
            this.mCompareKey = LeafSetTester.GetLocalInfo().Key;
            nodes.Add(LeafSetTester.GetLocalInfo().Key);
            for (int i = 0; i < 20; i++)
            {
                NodeInfo random = this.GetNewRandomInfo();

                leafSet.AddNeighbour(random.Address);
                LeafSetTester.AssertValidity(leafSet);

                if (LeafSetTester.GetLocalInfo().Key.Precedes(random.Key))
                {
                    // Only the LeafSet.MaxNumNeighbours/2 closest are kept.
                    succs.Add(random.Key);
                    succs.Sort(this.CompareKeys);
                    if (succs.Count > LeafSet.MaximumNumberOfNeighbours / 2)
                    {
                        succs.RemoveAt(LeafSet.MaximumNumberOfNeighbours / 2);
                    }
                }
                else
                {
                    // Only the LeafSet.MaxNumNeighbours/2 closest are kept.
                    preds.Add(random.Key);
                    preds.Sort(this.CompareKeys);
                    if (preds.Count > LeafSet.MaximumNumberOfNeighbours / 2)
                    {
                        preds.RemoveAt(LeafSet.MaximumNumberOfNeighbours / 2);
                    }
                }
            }

            nodes.AddRange(succs);
            nodes.AddRange(preds);

            // Test that the closest leaf node is returned by GetNodeClosestTo()
            int j;
            for (j = 0; j < 250; j++)
            {
                HashKey randomKey = HashKey.Random();
                this.mCompareKey = randomKey;
                nodes.Sort(this.CompareKeys);

                Assert.AreEqual(nodes[0], leafSet.GetNodeClosestTo(randomKey, null).Key);
            }
        }

        [Test]
        public void RemoveTest()
        {
            LeafSet leafSet = new LeafSet();

            leafSet.NeighbourRemovalEvent += this.RemovedNodeHandler;
            leafSet.Initialize(LeafSetTester.GetLocalInfo().Address);

            // Test that removing an unknown node doesn't call the call back.
            leafSet.RemoveNeighbour(PeerAddress.GetLoopback(88));
            Assert.AreEqual(null, this.mRemovedNodeInfo);

            for (int i = 0; i < 4; i++)
            {
                NodeInfo random = this.GetNewRandomInfo();

                leafSet.AddNeighbour(random.Address);
                LeafSetTester.AssertValidity(leafSet);
            }

            // Test removal of nodes.
            NodeInfo removedNode = leafSet.ImmediatePredecessor;
            leafSet.RemoveNeighbour(removedNode.Address);

            Assert.AreEqual(removedNode, this.mRemovedNodeInfo);
            Assert.IsFalse(leafSet.Contains(removedNode));

            removedNode = leafSet.ImmediateSuccessor;
            leafSet.RemoveNeighbour(removedNode.Address);

            Assert.AreEqual(removedNode, this.mRemovedNodeInfo);
            Assert.IsFalse(leafSet.Contains(removedNode));

            Assert.AreEqual(leafSet.PredecessorCount, leafSet.SuccessorCount);
            LeafSetTester.AssertValidity(leafSet);
        }

        [Test]
        public void AddRemoveTest()
        {
            LeafSet leafSet = new LeafSet();

            leafSet.Initialize(LeafSetTester.GetLocalInfo().Address);

            for (int i = 0; i < 4; i++)
            {
                NodeInfo random = this.GetNewRandomInfo();

                leafSet.AddNeighbour(random.Address);
                LeafSetTester.AssertValidity(leafSet);
            }

            for (int i = 0; i < 100; i++)
            {
                int addOrRemove = RandomSource.Generator.Next(0, 1000);

                if (addOrRemove % 2 == 0 && leafSet.UniqueNodeCount != 0)
                {
                    int randomIndex = 0;
                    while (randomIndex == 0)
                    {
                        randomIndex = RandomSource.Generator.Next(-leafSet.PredecessorCount, leafSet.SuccessorCount);
                    }

                    NodeInfo remove = leafSet[randomIndex];
                    leafSet.RemoveNode(remove);
                    LeafSetTester.AssertValidity(leafSet);
                }
                else
                {
                    NodeInfo random = this.GetNewRandomInfo();

                    leafSet.AddNeighbour(random.Address);
                    LeafSetTester.AssertValidity(leafSet);
                }
            }
        }

        [Test]
        public void AlternateParseTests()
        {
            LeafSet expectedValue = new LeafSet();
            (this.mParseableTester as ParseableTester).mExpectedValue = expectedValue;
            expectedValue.Initialize(LeafSetTester.GetLocalInfo().Address);

            // Test that an empty leafset parses correctly.
            base.ToFromMessageTest();
            base.AsArgumentTest();

            // Test that for each state of the leaf set it parses correctly.
            for (int i = 0; i < LeafSet.MaximumNumberOfNeighbours; i++)
            {
                expectedValue.AddNeighbour(this.GetNewRandomInfo().Address);
                LeafSetTester.AssertValidity(expectedValue);
                base.Initialize();
                base.ToFromMessageTest();
                base.AsArgumentTest();
            }

            (this.mParseableTester as ParseableTester).mExpectedValue = null;
        }

        [Test]
        public void CandidateTest()
        {
            this.mLeafSet = new LeafSet();
            this.mLeafSet.Initialize(LeafSetTester.GetLocalInfo().Address);

            for (int i = 0; i < LeafSet.MaximumNumberOfNeighbours / 2; i++)
            {
                NodeInfo nodeInfo = this.GetNewSuccessorInfo();
                Assert.IsTrue(this.mLeafSet.IsCandidateForAddition(nodeInfo.Address));
                this.mLeafSet.AddNeighbour(nodeInfo.Address);
                Assert.IsFalse(this.mLeafSet.IsCandidateForAddition(nodeInfo.Address));
            }

            for (int i = 0; i < LeafSet.MaximumNumberOfNeighbours / 2; i++)
            {
                NodeInfo nodeInfo = this.GetNewPredecessorInfo();
                Assert.IsTrue(this.mLeafSet.IsCandidateForAddition(nodeInfo.Address));
                this.mLeafSet.AddNeighbour(nodeInfo.Address);
                Assert.IsFalse(this.mLeafSet.IsCandidateForAddition(nodeInfo.Address));
            }

            HashKey opposite = this.mLeafSet.LocalNodeInfo.Key.Opposite();
            NodeInfo randomNode = this.GetNewSuccessorInfo();

            // Find a node that is a candidate.
            while (randomNode.Key.AbsoluteDistanceFrom(this.mLeafSet.LocalNodeInfo.Key) >
                this.mLeafSet.LocalNodeInfo.Key.AbsoluteDistanceFrom(this.mLeafSet[2].Key))
            {
                Assert.IsFalse(this.mLeafSet.IsCandidateForAddition(randomNode.Address));
                randomNode = this.GetNewSuccessorInfo();
            }
            Assert.IsTrue(this.mLeafSet.IsCandidateForAddition(randomNode.Address));

            randomNode = this.GetNewPredecessorInfo();
            while (randomNode.Key.AbsoluteDistanceFrom(this.mLeafSet.LocalNodeInfo.Key) >
                this.mLeafSet.LocalNodeInfo.Key.AbsoluteDistanceFrom(this.mLeafSet[-2].Key))
            {
                Assert.IsFalse(this.mLeafSet.IsCandidateForAddition(randomNode.Address));
                randomNode = this.GetNewPredecessorInfo();
            }
            Assert.IsTrue(this.mLeafSet.IsCandidateForAddition(randomNode.Address));
        }

        [Test]
        public void SpecificScenarioTest()
        {
            byte[] addressBytes = new byte[4];

            this.mLeafSet = new LeafSet();

            addressBytes = new byte[] { 0, 183, 0, 0 };
            this.mLeafSet.Initialize(new PeerAddress(new System.Net.IPAddress(addressBytes), 0, NatType.Open));


            addressBytes = new byte[] { 1, 80, 0, 0 };
            this.mLeafSet.AddNeighbour(new PeerAddress(new System.Net.IPAddress(addressBytes), 0, NatType.Open));
            addressBytes = new byte[] { 0, 147, 0, 0 };
            this.mLeafSet.AddNeighbour(new PeerAddress(new System.Net.IPAddress(addressBytes), 0, NatType.Open));
            addressBytes = new byte[] { 3, 50, 0, 0 };
            this.mLeafSet.AddNeighbour(new PeerAddress(new System.Net.IPAddress(addressBytes), 0, NatType.Open));

            //for (int i = -this.mLeafSet.PredecessorCount; i <= this.mLeafSet.SuccessorCount; i++)
            //{
            //    Console.WriteLine("**z {0} \t {1}", this.mLeafSet[i].Address, this.mLeafSet[i].Key);
            //}

            addressBytes = new byte[] { 0, 223, 0, 0 };
            PeerAddress newAddress = new PeerAddress(new System.Net.IPAddress(addressBytes), 0, NatType.Open);

            Assert.IsTrue(this.mLeafSet.IsCandidateForAddition(newAddress));

            this.mLeafSet.AddNeighbour(newAddress);

            //Console.WriteLine("----");
            //for (int i = -this.mLeafSet.PredecessorCount; i <= this.mLeafSet.SuccessorCount; i++)
            //{
            //    Console.WriteLine("**z {0} \t {1}", this.mLeafSet[i].Address, this.mLeafSet[i].Key);
            //}

            Assert.IsFalse(this.mLeafSet.IsCandidateForAddition(newAddress));
        }

        [Test]
        public void EncompassTestA()
        {
            // Test that when the leafest encompasses MORE than half the space the correct result is returned.

            byte[] addressBytes = new byte[4];

            this.mLeafSet = new LeafSet();

            addressBytes = new byte[] { 2, 143, 0, 0 };
            this.mLeafSet.Initialize(new PeerAddress(new System.Net.IPAddress(addressBytes), 0, NatType.Open));


            addressBytes = new byte[] { 3, 169, 0, 0 };
            this.mLeafSet.AddNeighbour(new PeerAddress(new System.Net.IPAddress(addressBytes), 0, NatType.Open));
            addressBytes = new byte[] { 6, 91, 0, 0 };
            this.mLeafSet.AddNeighbour(new PeerAddress(new System.Net.IPAddress(addressBytes), 0, NatType.Open));
            addressBytes = new byte[] { 3, 229, 0, 0 };
            this.mLeafSet.AddNeighbour(new PeerAddress(new System.Net.IPAddress(addressBytes), 0, NatType.Open));
            addressBytes = new byte[] { 6, 116, 0, 0 };
            this.mLeafSet.AddNeighbour(new PeerAddress(new System.Net.IPAddress(addressBytes), 0, NatType.Open));

            //for (int i = -this.mLeafSet.PredecessorCount; i <= this.mLeafSet.SuccessorCount; i++)
            //{
            //    Console.WriteLine("**z {0} \t {1}", this.mLeafSet[i].Address, this.mLeafSet[i].Key);
            //}

            HashKey one = new HashKey();
            one[HashKey.Length - 1] = 0x01;

            HashKey outsideKey = this.mLeafSet[-2].Key.Subtract(one);
            //System.Console.WriteLine("{0}", outsideKey);

            addressBytes = new byte[] { 5, 158, 0, 0 };
            PeerAddress newAddress = new PeerAddress(new System.Net.IPAddress(addressBytes), 0, NatType.Open);
            NodeInfo newNodeInfo = new NodeInfo(newAddress, this.mLeafSet[0].Key);

            Assert.IsTrue(this.mLeafSet.Encompases(newNodeInfo.Key));
            Assert.IsFalse(this.mLeafSet.Encompases(outsideKey));

            Assert.IsTrue(this.mLeafSet.Encompases(this.mLeafSet[-2].Key));
            Assert.IsTrue(this.mLeafSet.Encompases(this.mLeafSet[2].Key));
        }


        [Test]
        public void EncompassTestB()
        {
            // Test that when the leafest encompasses LESS than half the space the correct result is returned.

            byte[] addressBytes = new byte[4];

            this.mLeafSet = new LeafSet();

            addressBytes = new byte[] { 2, 143, 0, 0 };
            this.mLeafSet.Initialize(new PeerAddress(new System.Net.IPAddress(addressBytes), 0, NatType.Open));


            addressBytes = new byte[] { 3, 0, 0, 1 };
            this.mLeafSet.AddNeighbour(new PeerAddress(new System.Net.IPAddress(addressBytes), 0, NatType.Open));
            addressBytes = new byte[] { 6, 91, 0, 0 };
            this.mLeafSet.AddNeighbour(new PeerAddress(new System.Net.IPAddress(addressBytes), 0, NatType.Open));
            addressBytes = new byte[] { 3, 0, 0, 2 };
            this.mLeafSet.AddNeighbour(new PeerAddress(new System.Net.IPAddress(addressBytes), 0, NatType.Open));
            addressBytes = new byte[] { 3, 0, 0, 3 };
            this.mLeafSet.AddNeighbour(new PeerAddress(new System.Net.IPAddress(addressBytes), 0, NatType.Open));

            //for (int i = -this.mLeafSet.PredecessorCount; i <= this.mLeafSet.SuccessorCount; i++)
            //{
            //    Console.WriteLine("**z {0} \t {1}", this.mLeafSet[i].Address, this.mLeafSet[i].Key);
            //}

            addressBytes = new byte[] { 5, 159, 0, 0 };
            PeerAddress newAddress = new PeerAddress(new System.Net.IPAddress(addressBytes), 0, NatType.Open);
            NodeInfo newNodeInfo = new NodeInfo(newAddress, this.mLeafSet[0].Key);

            //System.Console.WriteLine("{0}", newNodeInfo.Key);

            Assert.IsTrue(this.mLeafSet.Encompases(newNodeInfo.Key));
            Assert.IsFalse(this.mLeafSet.Encompases(this.mLeafSet[0].Key.Opposite()));

            Assert.IsTrue(this.mLeafSet.Encompases(this.mLeafSet[-2].Key));
            Assert.IsTrue(this.mLeafSet.Encompases(this.mLeafSet[2].Key));
        }

        [Test]
        public void EncompassTestC()
        {
            // Test that when the leafest encompasses the ENTIRE space the correct result is returned.

            byte[] addressBytes = new byte[4];

            this.mLeafSet = new LeafSet();

            addressBytes = new byte[] { 2, 143, 0, 0 };
            this.mLeafSet.Initialize(new PeerAddress(new System.Net.IPAddress(addressBytes), 0, NatType.Open));


            addressBytes = new byte[] { 3, 169, 0, 0 };
            this.mLeafSet.AddNeighbour(new PeerAddress(new System.Net.IPAddress(addressBytes), 0, NatType.Open));
            addressBytes = new byte[] { 6, 116, 0, 0 };
            this.mLeafSet.AddNeighbour(new PeerAddress(new System.Net.IPAddress(addressBytes), 0, NatType.Open));

            //for (int i = -this.mLeafSet.PredecessorCount; i <= this.mLeafSet.SuccessorCount; i++)
            //{
            //    Console.WriteLine("**z {0} \t {1}", this.mLeafSet[i].Address, this.mLeafSet[i].Key);
            //}

            addressBytes = new byte[] { 5, 158, 0, 0 };
            PeerAddress newAddress = new PeerAddress(new System.Net.IPAddress(addressBytes), 0, NatType.Open);
            NodeInfo newNodeInfo = new NodeInfo(newAddress, this.mLeafSet[0].Key);

            Assert.IsTrue(this.mLeafSet.Encompases(newNodeInfo.Key));
        }

    }

}
