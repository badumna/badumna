﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using Badumna.DistributedHashTable;
using BadumnaTests.Core;
using Badumna.Core;

namespace BadumnaTests.DistributedHashTable
{
    [TestFixture]
    [DontPerformCoverage]
    class InitializationExceptionTester : NetworkExceptionTester<InitializationException>
    {
    }
}
