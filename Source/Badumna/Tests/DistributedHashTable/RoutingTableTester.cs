﻿using System;
using System.Collections.Generic;
using Badumna.Core;
using Badumna.DistributedHashTable;
using NUnit.Framework;

namespace BadumnaTests.DistributedHashTable
{
    [TestFixture]
    public class RoutingTableTester
    {
        private RoutingTable mRoutingTable;

        [Test]
        public void InsertTest()
        {
            PeerAddress localAddress = PeerAddress.GetAddress("1.2.1.99", 0);
            NodeInfo local = new NodeInfo(localAddress, HashKey.Hash(localAddress));
            NodeInfo node = new NodeInfo(PeerAddress.GetAddress("128.250.1.1", 0), local.Key);

            this.mRoutingTable = new RoutingTable(8, local);

            // Test that inserting the local node does nothing.
            this.mRoutingTable.InsertNodeInfo(local);
            Assert.AreEqual(null, this.mRoutingTable.BestCandidateForRouteTo(local.Key));

            // Test that the closest node for the nodes key is correct.
            this.mRoutingTable.InsertNodeInfo(node);
            Assert.AreEqual(node, this.mRoutingTable.BestCandidateForRouteTo(node.Key));

            byte remainder;
            int firstDifferenceIndex = node.Key.FirstDifferentDigit(local.Key, 8, out remainder);

            // Test that an address with a different remainder does not return the the entered node info.
            HashKey differentKey = node.Key.Copy();

            differentKey[firstDifferenceIndex] = (byte)(remainder ^ 0xff);
            Assert.AreNotEqual(differentKey, node.Key);
            Assert.AreEqual(null, this.mRoutingTable.BestCandidateForRouteTo(differentKey));

            // Test that a key with a difference with the same first difference returns the entered node.
            differentKey = node.Key.Copy();

            if (firstDifferenceIndex - 1 < HashKey.Length)
            {
                differentKey[firstDifferenceIndex + 1] = (byte)(remainder ^ 0xff);
                Assert.AreNotEqual(differentKey, node.Key);
            }

            Assert.AreEqual(node, this.mRoutingTable.BestCandidateForRouteTo(differentKey));
        }

        [Test]
        public void RemoveTest()
        {
            PeerAddress localAddress = PeerAddress.GetAddress("1.31.13.3", 0);
            NodeInfo local = new NodeInfo(localAddress, HashKey.Hash(localAddress));
            NodeInfo node = new NodeInfo(PeerAddress.GetAddress("128.20.1.1", 0), local.Key);

            this.mRoutingTable = new RoutingTable(8, local);

            this.mRoutingTable.InsertNodeInfo(node);
            Assert.AreEqual(node, this.mRoutingTable.BestCandidateForRouteTo(node.Key));

            // Test that the removal of the node means future queries don't give it as a candidate for routing.
            this.mRoutingTable.RemoveNodeInfo(node);
            Assert.AreEqual(null, this.mRoutingTable.BestCandidateForRouteTo(node.Key));
        }

        [Test]
        public void ExcludedListTest()
        {
            PeerAddress localAddress = PeerAddress.GetAddress("1.2.1.99", 0);
            NodeInfo local = new NodeInfo(localAddress, HashKey.Hash(localAddress));
            NodeInfo node = new NodeInfo(PeerAddress.GetAddress("128.250.1.1", 0), local.Key);

            this.mRoutingTable = new RoutingTable(8, local);

            // Test that the closest node for the nodes key is correct.
            this.mRoutingTable.InsertNodeInfo(node);
            Assert.AreEqual(node, this.mRoutingTable.BestCandidateForRouteTo(node.Key));

            List<PeerAddress> excludedList = new List<PeerAddress>();

            excludedList.Add(node.Address);

            Assert.AreEqual(null, this.mRoutingTable.BestCandidateForRouteTo(node.Key, excludedList));
            Assert.AreEqual(node, this.mRoutingTable.BestCandidateForRouteTo(node.Key, null));
        }

        [Test, ExpectedException(typeof(ArgumentNullException))]
        public void ConstructorFailTest()
        {
            this.mRoutingTable = new RoutingTable(2, null);
        }

        [Test]
        public void NoEntryTest()
        {
            PeerAddress localAddress = PeerAddress.GetAddress("1.2.1.99", 0);
            NodeInfo local = new NodeInfo(localAddress, HashKey.Hash(localAddress));

            this.mRoutingTable = new RoutingTable(8, local);

            Assert.AreEqual(null, this.mRoutingTable.BestCandidateForRouteTo(HashKey.Random(), null));
        }

    }
}
