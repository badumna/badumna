﻿using Badumna.Core;
using Badumna.DistributedHashTable;
using NUnit.Framework;
using BadumnaTests.Core;

namespace BadumnaTests.DistributedHashTable
{
    [TestFixture]
    [DontPerformCoverage]
    class DhtExceptionTester : NetworkExceptionTester<DhtException>
    {
    }

    [TestFixture]
    [DontPerformCoverage]
    class InvalidReplicaTypeExceptionTester : NetworkExceptionTester<InvalidReplicaTypeException>
    {
    }
}
