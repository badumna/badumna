﻿// -----------------------------------------------------------------------
// <copyright file="MatchReplicaWrapperTests.cs" company="Scalify">
//     Copyright (c) 2013 Scalify. All right reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace BadumnaTests.Match
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using Badumna.Autoreplication;
    using Badumna.Core;
    using Badumna.DataTypes;
    using Badumna.Match;
    using Badumna.SpatialEntities;
    using Badumna.Utilities;
    using BadumnaTests.Utilities;
    using NUnit.Framework;
    using Rhino.Mocks;

    using MathAutoreplicationManager = Badumna.Autoreplication.MatchAutoreplicationManager;

    [TestFixture]
    [DontPerformCoverage]
    public class MatchReplicaWrapperTests
    {
        private MatchAutoreplicationManager autoreplicationManager;

        private Badumna.Autoreplication.Serialization.Manager serialization;

        private Badumna.Replication.IEntityManager replicationEntityManager = null;

        private INetworkConnectivityReporter connectivityReporter;

        private NetworkEventQueue eventQueue = null;

        private QueueInvokable queueApplicationEvent;

        private ITime timeKeeper;

        private IMembershipReporter membershipReporter;

        private GenericCallBackReturn<BadumnaId, PeerAddress> generateBadumnaIdWithAddress;

        private MatchReplicaWrapper wrapper;

        private EntityManager entityManager;

        private IBadumnaIdAllocator badumnaIDAllocator;

        [SetUp]
        public void SetUp()
        {
            this.replicationEntityManager = MockRepository.GenerateMock<Badumna.Replication.IEntityManager>();
            this.connectivityReporter = MockRepository.GenerateMock<INetworkConnectivityReporter>();
            this.eventQueue = new NetworkEventQueue();
            this.timeKeeper = this.eventQueue;
            this.queueApplicationEvent = i => i.Invoke();

            this.serialization = new Badumna.Autoreplication.Serialization.Manager(this.timeKeeper);
            this.autoreplicationManager = new MatchAutoreplicationManager(
                this.serialization,
                this.timeKeeper,
                (o, d) => { return MockRepository.GenerateMock<IOriginalEntityWrapper>(); },
                r => { return MockRepository.GenerateMock<IReplicaEntityWrapper>(); });
            this.badumnaIDAllocator = MockRepository.GenerateMock<IBadumnaIdAllocator>();
            this.entityManager = new EntityManager(
                this.replicationEntityManager,
                this.autoreplicationManager,
                this.connectivityReporter,
                this.eventQueue,
                this.queueApplicationEvent,
                this.badumnaIDAllocator);

            this.membershipReporter = MockRepository.GenerateMock<IMembershipReporter>();
            this.membershipReporter.Stub(m => m.Members).Return(new List<MemberIdentity>());

            this.generateBadumnaIdWithAddress = BadumnaIdGenerator.FromAddress();

            this.wrapper = new MatchReplicaWrapper(this.generateBadumnaIdWithAddress(PeerAddress.GetLoopback(1)), 0, this.eventQueue, this.entityManager, this.queueApplicationEvent);
        }

        [TearDown]
        public void TearDown()
        {
            this.replicationEntityManager = null;
            this.membershipReporter = null;
            this.connectivityReporter = null;
            this.eventQueue = null;
            this.queueApplicationEvent = null;
            this.serialization = null;
            this.autoreplicationManager = null;
            this.entityManager = null;
        }

        [Test]
        public void ConstructorShouldWork()
        {
            var wrapper = new MatchReplicaWrapper(this.generateBadumnaIdWithAddress(PeerAddress.GetLoopback(1)), 0, this.eventQueue, this.entityManager, this.queueApplicationEvent);
            Assert.IsNotNull(wrapper);
        }       
    }
}
