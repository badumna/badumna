﻿//-----------------------------------------------------------------------
// <copyright file="MatchOriginalWrapperTests.cs" company="Scalify">
//     Copyright (c) 2013 Scalify. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BadumnaTests.Match
{
    using System;
    using System.Collections.Generic;
    using Badumna.Autoreplication;
    using Badumna.Core;
    using Badumna.DataTypes;
    using Badumna.Match;
    using Badumna.Replication;
    using Badumna.Utilities;
    using BadumnaTests.Utilities;
    using NUnit.Framework;
    using Rhino.Mocks;

    [TestFixture]
    [DontPerformCoverage]
    public class MatchOriginalWrapperTests
    {
        private object entity;

        private IOriginal original;

        private BadumnaId entityID;

        private uint entityType;

        private IEntityManager replicationEntityManager;

        private IReplicationService<IOriginal, IReplica> matchEntityManager;

        private ITime timeKeeper;

        private OriginalEntityWrapper originalEntityWrapper;

        private MatchOriginalWrapper wrapper;

        private IMembershipReporter membershipReporter;

        private GenericCallBackReturn<BadumnaId, PeerAddress> generateBadumnaIdWithAddress;

        private MatchAutoreplicationManager autoreplicationManager;

        private EntityTypeId entityTypeId;

        [SetUp]
        public void SetUp()
        {
            this.timeKeeper = MockRepository.GenerateMock<ITime>();
            this.replicationEntityManager = MockRepository.GenerateMock<IEntityManager>();
            this.matchEntityManager = MockRepository.GenerateMock<IReplicationService<IOriginal, IReplica>>();

            this.membershipReporter = MockRepository.GenerateMock<IMembershipReporter>();
            this.membershipReporter.Stub(m => m.Members).Return(new List<MemberIdentity>());
            
            this.generateBadumnaIdWithAddress = BadumnaIdGenerator.FromAddress();

            this.entity = new object();
            this.entityID = this.generateBadumnaIdWithAddress(PeerAddress.GetLoopback(1));

            Badumna.Autoreplication.Serialization.Manager serializationManager =
                MockRepository.GenerateMock<Badumna.Autoreplication.Serialization.Manager>(this.timeKeeper);
            
            this.originalEntityWrapper = MockRepository.GenerateMock<OriginalEntityWrapper>(
                this.entity,
                this.matchEntityManager,
                serializationManager);
                
            ////this.original.Stub(o => o.Guid).Return(this.entityID);
            this.originalEntityWrapper.Guid = this.entityID;
            
            OriginalWrapperFactory<object, IOriginalEntityWrapper, KeyValuePair<float, float>> originalWrapperFactory =
                (e, d) => this.originalEntityWrapper;
            ReplicaWrapperFactory<object, IReplicaEntityWrapper> replicaWrapperFactory = e => null;
            this.autoreplicationManager = MockRepository.GenerateMock<MatchAutoreplicationManager>(
                serializationManager,
                this.timeKeeper,
                originalWrapperFactory,
                replicaWrapperFactory);
            ////this.autoreplicationManager.Stub(m => m.RegisterOriginal(this.entity, new KeyValuePair<float, float>(0, 0)))
            ////    .Return(this.original);

            this.entityType = 0;
            this.entityTypeId = new EntityTypeId((byte)EntityGroup.Match, 0);

            this.original = this.autoreplicationManager.RegisterOriginal(this.entity, new KeyValuePair<float, float>());
        }

        [TearDown]
        public void TearDown()
        {
            this.replicationEntityManager = null;
            this.originalEntityWrapper = null;
            this.wrapper = null;
        }

        [Test]
        public void ConstructorShouldWork()
        {
            this.wrapper = new MatchOriginalWrapper(this.original, this.entityID, this.entityType, this.membershipReporter, this.replicationEntityManager);
            Assert.IsNotNull(this.wrapper);
        }       

        [Test]
        public void NewMemberShouldBeAddedToEntityManager()
        {
            this.wrapper = new MatchOriginalWrapper(this.original, this.entityID, this.entityType, this.membershipReporter, this.replicationEntityManager);
            this.replicationEntityManager.Expect(em => em.AddInterested(null, null)).IgnoreArguments().Repeat.Once();
            var identity = new MemberIdentity(this.generateBadumnaIdWithAddress(PeerAddress.GetLoopback(2)), "foo");
            var eventArgs = new MatchMembershipEventArgs(identity);
            this.membershipReporter.Raise(m => m.MemberAdded += null, new object[] { this.membershipReporter, eventArgs });
            this.replicationEntityManager.VerifyAllExpectations();
        }

        [Test]
        public void OnMemberLeavedRemoveInterestedShouldBeCalled()
        {
            this.wrapper = new MatchOriginalWrapper(this.original, this.entityID, this.entityType, this.membershipReporter, this.replicationEntityManager);
            this.replicationEntityManager.Expect(em => em.RemoveInterested(null, null)).IgnoreArguments().Repeat.Once();
            var identity = new MemberIdentity(this.generateBadumnaIdWithAddress(PeerAddress.GetLoopback(2)), "bar");
            var eventArgs = new MatchMembershipEventArgs(identity);
            this.membershipReporter.Raise(m => m.MemberRemoved += null, new object[] { this.membershipReporter, eventArgs });
            this.replicationEntityManager.VerifyAllExpectations();
        }
    }
}
