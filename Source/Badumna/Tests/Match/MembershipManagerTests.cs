﻿//-----------------------------------------------------------------------
// <copyright file="MembershipManagerTests.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2013 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BadumnaTests.Match
{
    using System;
    using Badumna.Core;
    using Badumna.DataTypes;
    using Badumna.Match;
    using Badumna.Transport;
    using BadumnaTests.Core;
    using NUnit.Framework;
    using Rhino.Mocks;
    using Constraints = Rhino.Mocks.Constraints;

    [TestFixture]
    [DontPerformCoverage]
    public class MembershipManagerTests
    {
        private BadumnaId localMember;
        private PeerAddress serverAddress;
        private ITime timeKeeper;
        private IMatchmaker matchmaker;

        [SetUp]
        public void SetUp()
        {
            var localAddress = PeerAddress.GetRandomAddress();
            this.localMember = new BadumnaId(localAddress, 0);
            do
            {
                this.serverAddress = PeerAddress.GetRandomAddress();
            }
            while (this.serverAddress == localAddress);
            this.timeKeeper = MockRepository.GenerateMock<ITime>();
            this.matchmaker = MockRepository.GenerateMock<IMatchmaker>();
        }

        ////[Test]
        ////[ExpectedException(typeof(ArgumentNullException))]
        ////public void ConstructorThrowsForTransportProtocol()
        ////{
        ////}
    }
}
