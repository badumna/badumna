﻿//-----------------------------------------------------------------------
// <copyright file="FacadeIntegrationTests.cs" company="NICTA">
//     Copyright (c) 2013 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BadumnaTests.Match
{
    using System;
    using System.Collections.Generic;
    using Badumna;
    using Badumna.Autoreplication;
    using Badumna.Core;
    using Badumna.DataTypes;
    using Badumna.Match;
    using Badumna.Transport;
    using Badumna.Utilities;
    using BadumnaTests.Autoreplication;
    using NUnit.Framework;
    using Rhino.Mocks;
    using TestUtilities;

    using MathAutoreplicationManager = Badumna.Autoreplication.MatchAutoreplicationManager;

    [TestFixture]
    [DontPerformCoverage]
    public class FacadeIntegrationTests
    {
        private INetworkConnectivityReporter networkConnectivityReporter;
        private IPeerConnectionNotifier peerConnectionNotifier;
        private ITransportProtocol transportProtocol;
        private INetworkAddressProvider addressProvider;
        private INetworkConnectivityReporter connectivityReporter;
        private QueueInvokable applicationQueue;
        private MatchmakingModule optionsModule;

        private IBadumnaIdAllocator badumnaIdAllocator;
        private IMatchProtocol messagingService;

        private PeerAddress serverAddress;
        private FakeTimeKeeper timeKeeper;
        private IMatchmaker matchmaker;
        private Facade facade;
        private INetworkEventScheduler eventQueue;
        private Badumna.Replication.IEntityManager replicationEntityManager;
        private EntityManager entityManager;

        private MathAutoreplicationManager autoreplicationManager;
        private Badumna.Autoreplication.Serialization.Manager serialization;

        private CreateReplica createReplicaDelegate;
        private RemoveReplica removeReplicaDelegate;

        [SetUp]
        public void SetUp()
        {
            this.networkConnectivityReporter = MockRepository.GenerateMock<INetworkConnectivityReporter>();
            this.peerConnectionNotifier = MockRepository.GenerateMock<IPeerConnectionNotifier>();
            this.matchmaker = MockRepository.GenerateMock<IMatchmaker>();
            this.badumnaIdAllocator = MockRepository.GenerateMock<IBadumnaIdAllocator>();
            this.messagingService = MockRepository.GenerateMock<IMatchProtocol>();
            this.transportProtocol = MockRepository.GenerateMock<ITransportProtocol>();
            this.addressProvider = MockRepository.GenerateMock<INetworkAddressProvider>();
            this.connectivityReporter = MockRepository.GenerateMock<INetworkConnectivityReporter>();
            this.applicationQueue = MockRepository.GenerateMock<QueueInvokable>();
            this.serverAddress = PeerAddress.GetAddress("10.0.0.1:5000");
            this.optionsModule = new MatchmakingModule(this.serverAddress.EndPoint.ToString(), 999);
            this.timeKeeper = new FakeTimeKeeper();
            this.eventQueue = MockRepository.GenerateMock<INetworkEventScheduler>();
            this.replicationEntityManager = MockRepository.GenerateMock<Badumna.Replication.IEntityManager>();

            this.serialization = new Badumna.Autoreplication.Serialization.Manager(this.timeKeeper);
            this.autoreplicationManager = new MathAutoreplicationManager(
                this.serialization,
                this.timeKeeper,
                (o, d) => { return MockRepository.GenerateMock<IOriginalEntityWrapper>(); },
                r => { return MockRepository.GenerateMock<IReplicaEntityWrapper>(); });

            this.entityManager = new EntityManager(
                this.replicationEntityManager,
                this.autoreplicationManager,
                this.connectivityReporter,
                this.eventQueue,
                this.applicationQueue,
                this.badumnaIdAllocator);

            this.facade = new Facade(
                this.networkConnectivityReporter,
                this.peerConnectionNotifier,
                this.matchmaker,
                this.messagingService,
                this.badumnaIdAllocator,
                this.applicationQueue,
                this.eventQueue,
                this.optionsModule,
                this.timeKeeper,
                this.entityManager,
                this.autoreplicationManager);

            this.createReplicaDelegate = (m, s, type) => { return null; };
            this.removeReplicaDelegate = (m, s, o) => { };
        }

        [Test]
        public void CreateMatchPingsMatchmakingServer()
        {
            // Arrange
            BadumnaId id = new BadumnaId(PeerAddress.GetRandomAddress(), 0);
            this.badumnaIdAllocator
                .Expect(a => a.GetNextUniqueId())
                .Return(id);
            var criteria = new MatchmakingCriteria();
            var capacity = new MatchCapacity();

            // (Execute events pushed to network thread immediately.)
            this.eventQueue.Stub(q => q.Push(null))
                .IgnoreArguments()
                .WhenCalled(c => ((GenericCallBack)c.Arguments[0]).Invoke())
                .Return(null);

            // Expect
            this.matchmaker.Expect(m => m.HostingRequest(null, null, criteria, capacity, int.MaxValue))
                .IgnoreArguments();

            // Act
            this.facade.CreateMatch(criteria, 4, "foo", this.createReplicaDelegate, this.removeReplicaDelegate);

            // Assert
            this.matchmaker.VerifyAllExpectations();
        }

        [Test]
        public void CreateMatchResultsInRegularPingingOfMatchmakingServer()
        {
            // Arrange
            CapturingConstraint<GenericCallBack> scheduledEvent = new CapturingConstraint<GenericCallBack>();
            BadumnaId id = new BadumnaId(PeerAddress.GetRandomAddress(), 0);
            this.badumnaIdAllocator
                .Expect(a => a.GetNextUniqueId())
                .Return(id);
            var criteria = new MatchmakingCriteria();
            var capacity = new MatchCapacity();
            this.timeKeeper.Now = TimeSpan.FromSeconds(1);

            // (Execute events pushed to network thread immediately.)
            this.eventQueue.Stub(q => q.Push(null))
                .IgnoreArguments()
                .WhenCalled(c => ((GenericCallBack)c.Arguments[0]).Invoke())
                .Return(null);
            this.eventQueue.Expect(q => q.Schedule(Arg<double>.Is.Anything, Arg<GenericCallBack>.Matches(scheduledEvent)))
                .Return(null);
            this.facade.CreateMatch(criteria, 4, "foo", this.createReplicaDelegate, this.removeReplicaDelegate);
            this.timeKeeper.Now = TimeSpan.FromSeconds(4.0001);

            // Expect
            this.matchmaker.Expect(m => m.HostingRequest(null, null, criteria, capacity, int.MaxValue))
                .IgnoreArguments();

            // Act
            scheduledEvent.Argument.Invoke();

            // Assert
            this.matchmaker.VerifyAllExpectations();
        }

        [Test]
        public void JoinMatchPingsMatchHost()
        {
            // Arrange
            var playerName = Guid.NewGuid().ToString();
            var localID = new BadumnaId(PeerAddress.GetRandomAddress(), 0);
            var identity = new MemberIdentity(localID, playerName);
            this.badumnaIdAllocator
                .Expect(a => a.GetNextUniqueId())
                .Return(localID);
            var criteria = new MatchmakingCriteria();
            var capacity = new MatchCapacity();
            var hostID = new BadumnaId(PeerAddress.GetRandomAddress(), 0);
            var certificate = new HostAuthorizationCertificate(hostID, hostID, TimeSpan.Zero);
            var result = new MatchmakingResult(certificate, capacity, criteria);

            // (Execute events pushed to network thread immediately.)
            this.eventQueue.Stub(q => q.Push(null))
                .IgnoreArguments()
                .WhenCalled(c => ((GenericCallBack)c.Arguments[0]).Invoke())
                .Return(null);

            // Expect
            this.messagingService.Expect(m => m.PingFromClientToHost(hostID, identity, hostID));

            // Act
            this.facade.JoinMatch(
                result,
                playerName,
                this.createReplicaDelegate,
                this.removeReplicaDelegate);

            // Assert
            this.messagingService.VerifyAllExpectations();
        }    
    }
}
