﻿//------------------------------------------------------------------------------
// <copyright file="MatchmakerTests.cs" company="National ICT Australia Limited">
//     Copyright (c) 2013 National ICT Australia Limited. All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

namespace BadumnaTests.Match
{
    using System;
    using System.Collections.Generic;
    using System.Net;
    using Badumna;
    using Badumna.Core;
    using Badumna.DataTypes;
    using Badumna.Match;
    using Badumna.Transport;
    using Badumna.Utilities;
    using NUnit.Framework;
    using Rhino.Mocks;

    /// <summary>
    /// Tests for Match.Matchmaker.
    /// </summary>
    public class MatchmakerTests
    {
        private const string IpAddress = "1.1.1.1";

        [Test]
        public void MustRegisterReplyHandler()
        {
            var matchmaker = this.ConstructMatchmakers(1, 2, new Simulator())[0];
            var requestorId = new BadumnaId(PeerAddress.GetLoopback(1), 1);
            var matchId = new BadumnaId(PeerAddress.GetLoopback(1), 2);
            var criteria = new MatchmakingCriteria();
            var capacity = new MatchCapacity();
            var takeoverCode = 1234;

            Assert.Throws<InvalidOperationException>(
                () => matchmaker.HostingRequest(requestorId, matchId, criteria, capacity, takeoverCode));
        }

        [Test]
        public void HostingRequestReceivesResponse()
        {
            // Arrange
            var matchmakers = this.ConstructMatchmakers(2, 1, new Simulator());
            var server = matchmakers[0];
            var client = matchmakers[1];
            var clientAddress = PeerAddress.GetAddress(MatchmakerTests.IpAddress, 2);  // port is index + 1.

            var requestorId = new BadumnaId(clientAddress, 1);
            var matchId = new BadumnaId(clientAddress, 2);
            var criteria = new MatchmakingCriteria();
            var capacity = new MatchCapacity();
            var takeoverCode = 1234;

            var matchmakingClient = MockRepository.GenerateMock<IMatchmakingClient>();
            client.RegisterMatchmakingClient(requestorId, matchmakingClient);

            // Expect
            matchmakingClient.Expect(c => c.HandleHostingReply(null, 0)).IgnoreArguments();

            // Act
            client.HostingRequest(requestorId, matchId, criteria, capacity, takeoverCode);
            
            // Assert
            matchmakingClient.VerifyAllExpectations();
        }

        [Test]
        public void MatchQueryWithNoServerGetsFailureResponse()
        {
            var simulator = new Simulator();
            var matchmaker = this.ConstructMatchmakers(1, 2, simulator)[0];
            var gotFailure = false;
            matchmaker.MatchQuery(
                new MatchmakingCriteria(),
                delegate { Assert.Fail("Got success result, but expected failure."); },
                delegate { gotFailure = true; });
            simulator.RunFor(TimeSpan.FromSeconds(60));
            Assert.IsTrue(gotFailure, "Failure notification not made.");
        }

        private TransportProtocol ConstructTransport(int port, Func<int, TransportProtocol> lookupTransport)
        {
            var nest = new Nest();
            var transport = new TransportProtocol("Test", typeof(ConnectionfulProtocolMethodAttribute), nest.Construct);
            transport.DispatcherMethod =
                delegate(TransportEnvelope envelope)
                {
                    envelope.Source = PeerAddress.GetAddress(MatchmakerTests.IpAddress, port);
                    Assert.AreEqual(IPAddress.Parse(MatchmakerTests.IpAddress), envelope.Destination.Address);
                    var destination = lookupTransport(envelope.Destination.EndPoint.Port);

                    if (destination != null)
                    {
                        destination.ParseMessage(envelope, 0);
                    }
                };

            return transport;
        }

        private Matchmaker ConstructMatchmaker(int port, int serverPort, Simulator simulator, TransportProtocol transport)
        {
            var options = new Options();
            options.Connectivity.ApplicationName = "test";

            if (port == serverPort)
            {
                options.Matchmaking.ActiveMatchLimit = 100;
            }
            else
            {
                options.Matchmaking.ServerAddress = MatchmakerTests.IpAddress + ":" + serverPort.ToString();
            }

            options.Validate();

            var matchmaker = new Matchmaker(simulator, simulator, transport, options);
            transport.ForgeMethodList();
            return matchmaker;
        }

        private List<Matchmaker> ConstructMatchmakers(int count, int serverPort, Simulator simulator)
        {
            var transports = new Dictionary<int, TransportProtocol>();

            var matchmakers = new List<Matchmaker>(count);
            for (int i = 1; i <= count; ++i)
            {
                transports[i] = this.ConstructTransport(i, x => transports.ContainsKey(x) ? transports[x] : null);
                matchmakers.Add(this.ConstructMatchmaker(i, serverPort, simulator, transports[i]));
            }

            return matchmakers;
        }
    }
}
