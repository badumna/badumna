﻿// -----------------------------------------------------------------------
// <copyright file="EntityManagerTests.cs" company="Scalify">
//     Copyright (c) 2013 Scalify. All right reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace BadumnaTests.Match
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Badumna.Autoreplication;
    using Badumna.Core;
    using Badumna.DataTypes;
    using Badumna.Match;
    using Badumna.Utilities;
    using BadumnaTests.Utilities;
    using NUnit.Framework;
    using Rhino.Mocks;

    using MathAutoreplicationManager = Badumna.Autoreplication.MatchAutoreplicationManager;

    [TestFixture]
    [DontPerformCoverage]
    public class EntityManagerTests
    {
        private MatchAutoreplicationManager autoreplicationManager;

        private Badumna.Autoreplication.Serialization.Manager serialization;

        private Badumna.Replication.IEntityManager replicationEntityManager;

        private INetworkConnectivityReporter connectivityReporter;

        private NetworkEventQueue eventQueue;

        private QueueInvokable applicationQueue;

        private ITime timeKeeper;

        private EntityManager entityManager;

        private Badumna.Replication.IOriginal original;

        private GenericCallBackReturn<BadumnaId, PeerAddress> generateBadumnaIdWithAddress;

        private IBadumnaIdAllocator identifierAllocator;

        [SetUp]
        public void SetUp()
        {
            this.generateBadumnaIdWithAddress = BadumnaIdGenerator.FromAddress();
            this.replicationEntityManager = MockRepository.GenerateMock<Badumna.Replication.IEntityManager>();
            this.connectivityReporter = MockRepository.GenerateMock<INetworkConnectivityReporter>();
            this.eventQueue = new NetworkEventQueue();
            this.timeKeeper = this.eventQueue;
            this.applicationQueue = MockRepository.GenerateMock<QueueInvokable>();

            this.serialization = new Badumna.Autoreplication.Serialization.Manager(this.timeKeeper);
            this.autoreplicationManager = new MatchAutoreplicationManager(
                this.serialization,
                this.timeKeeper,
                (o, d) => { return MockRepository.GenerateMock<IOriginalEntityWrapper>(); },
                r => { return MockRepository.GenerateMock<IReplicaEntityWrapper>(); });
            this.identifierAllocator = MockRepository.GenerateMock<IBadumnaIdAllocator>();
            this.entityManager = new EntityManager(
                this.replicationEntityManager,
                this.autoreplicationManager,
                this.connectivityReporter,
                this.eventQueue,
                this.applicationQueue,
                this.identifierAllocator);
            this.original = MockRepository.GenerateMock<Badumna.Replication.IOriginal>();
            this.original.Stub(o => o.Guid).Return(this.generateBadumnaIdWithAddress(PeerAddress.GetLoopback(1)));
        }

        [TearDown]
        public void TearDown()
        {
            this.eventQueue = null;
            this.timeKeeper = null;
            this.applicationQueue = null;
            this.replicationEntityManager = null;
            this.connectivityReporter = null;
            this.entityManager = null;
            this.autoreplicationManager = null;
        }

        [Test]
        public void ConstructorShouldWorks()
        {
            var entityManager = new EntityManager(
                this.replicationEntityManager,
                this.autoreplicationManager,
                this.connectivityReporter,
                this.eventQueue,
                this.applicationQueue,
                this.identifierAllocator);
            Assert.IsNotNull(entityManager);
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ConstructorShouldThrowArgumentNullException()
        {
            var entityManager = new EntityManager(
                null,
                this.autoreplicationManager,
                this.connectivityReporter,
                this.eventQueue,
                this.applicationQueue,
                this.identifierAllocator);
        }

        ////[Test]
        ////public void MarkForUpdateShouldWorks()
        ////{
        ////    this.replicationEntityManager.Expect(em => em.MarkForUpdate(null, new BooleanArray())).IgnoreArguments();
        ////    this.entityManager.MarkForUpdate(this.original, new BooleanArray());
        ////    this.eventQueue.RunUntil(TimeSpan.FromMilliseconds(200));
        ////    this.replicationEntityManager.VerifyAllExpectations();
        ////}
    }
}
