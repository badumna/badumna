﻿//-----------------------------------------------------------------------
// <copyright file="ChatProtocolFacadeTester.cs" company="NICTA">
//     Copyright (c) 2012 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
using Badumna.Chat;
using Badumna.Core;
using Badumna.DataTypes;
using Badumna.Utilities;
using BadumnaTests.Utilities;
using NUnit.Framework;
using Rhino.Mocks;

namespace BadumnaTests.Chat
{
    public class ChatProtocolFacadeTester
    {
        private IPrivateChatProtocol privateProtocol;
        private ChatProtocolFacade chatProtocolFacade;
        private IMulticastChatProtocol multicastProtocol;
        private IProximityChatProtocol proximityProtocol;
        private GenericCallBackReturn<BadumnaId> generatebadumnaId;
        private IPresenceService presenceService;
        
        [SetUp]
        public void SetUp()
        {
            this.privateProtocol = MockRepository.GenerateStub<IPrivateChatProtocol>();
            this.multicastProtocol = MockRepository.GenerateStub<IMulticastChatProtocol>();
            this.proximityProtocol = MockRepository.GenerateStub<IProximityChatProtocol>();
            this.presenceService = MockRepository.GenerateStub<IPresenceService>();
            this.chatProtocolFacade = new ChatProtocolFacade(this.proximityProtocol, this.multicastProtocol, this.privateProtocol, this.presenceService);
            this.generatebadumnaId = BadumnaIdGenerator.Local();
        }

        [Test]
        public void ShouldPublishPresenceToPresenceServiceAndEachChannelsAppropriateProtocols()
        {
            IInternalChatSession session = MockRepository.GenerateStub<IInternalChatSession>();
            var channel1 = this.StubChannel(ChatChannelType.Private);
            var channel2 = this.StubChannel(ChatChannelType.Private);
            var channel3 = this.StubChannel(ChatChannelType.Group);
            var userStatus = UserFixture.StatusFor("user", ChatStatus.Away);
            session.Stub(s => s.UserStatus).Return(userStatus);
            session.Stub(c => session.Channels).Return(new IInternalChatChannel[] { channel1, channel2, channel3 });

            this.chatProtocolFacade.PublishPresenceGlobally(session);

            this.presenceService.AssertWasCalled(p => p.SetStatus(userStatus));
            this.privateProtocol.AssertWasCalled(p => p.ChangePresence(channel1.TransportId, userStatus, ChatNotificationType.Default));
            this.privateProtocol.AssertWasCalled(p => p.ChangePresence(channel2.TransportId, userStatus, ChatNotificationType.Default));
            this.multicastProtocol.AssertWasCalled(p => p.ChangePresence(channel3.TransportId, userStatus, ChatNotificationType.Default));
        }

        [Test]
        public void ShouldSendPresenceToSpecificPeerOnAPrivateChannel()
        {
            var userPresence = new UserPresenceStatus(UserFixture.UserNamed("user"), ChatStatus.Away);
            var channelId = new ChatChannelId(ChatChannelType.Private, this.generatebadumnaId(), "private chat");

            this.chatProtocolFacade.SendPresenceToSpecificPeer(channelId, null, userPresence);

            this.privateProtocol.AssertWasCalled(p => p.ChangePresence(channelId.TransportId, userPresence, ChatNotificationType.DontReply));
        }

        [Test]
        public void ShouldSendPresenceToSpecificPeerOnAGroupChannel()
        {
            var userPresence = new UserPresenceStatus(UserFixture.UserNamed("user"), ChatStatus.Away);
            var remoteUserId = new BadumnaId(PeerAddress.GetLoopback(1234), 77);
            var channelId = new ChatChannelId(ChatChannelType.Group, this.generatebadumnaId(), "private chat");

            this.chatProtocolFacade.SendPresenceToSpecificPeer(channelId, remoteUserId, userPresence);

            this.multicastProtocol.AssertWasCalled(p => p.SendDirectPresence(channelId.TransportId, remoteUserId.Address, userPresence, ChatNotificationType.DontReply));
        }

        [Test]
        public void ShouldSendAPrivateMessage()
        {
            var channel = this.StubChannel(ChatChannelType.Private);
            var msg = "Hello, world!";

            this.chatProtocolFacade.SendMessage(channel, msg);

            this.privateProtocol.AssertWasCalled(p => p.SendChatMessage(channel.TransportId, channel.SessionId, msg));
        }

        [Test]
        public void ShouldSendAGroupMessage()
        {
            var channel = this.StubChannel(ChatChannelType.Group);
            var msg = "Hello, world!";

            this.chatProtocolFacade.SendMessage(channel, msg);

            this.multicastProtocol.AssertWasCalled(p => p.SendChatMessage(channel.TransportId, channel.SessionId, msg));
        }

        [Test]
        public void ShouldSendAProximityMessage()
        {
            var channel = this.StubChannel(ChatChannelType.Proximity);
            var msg = "Hello, world!";

            this.chatProtocolFacade.SendMessage(channel, msg);

            this.proximityProtocol.AssertWasCalled(p => p.SendProximityChatMessage(channel.TransportId, msg));
        }

        [Test]
        public void ShouldSendOfflineStatusWhenLeavingPrivateChannel()
        {
            var privateChannel = this.StubChannel(ChatChannelType.Private);
            var proximityChannel = this.StubChannel(ChatChannelType.Proximity);

            var user = UserFixture.UserNamed("bob");

            this.chatProtocolFacade.LeaveChannel(privateChannel, user);
            this.chatProtocolFacade.LeaveChannel(proximityChannel, user);

            this.privateProtocol.AssertWasCalled(p => p.ChangePresence(
                privateChannel.TransportId,
                new UserPresenceStatus(user, ChatStatus.Offline),
                ChatNotificationType.Default));
        }

        private IInternalChatChannel StubChannel(ChatChannelType channelType)
        {
            var channel = MockRepository.GenerateStub<IInternalChatChannel>();
            var channelId = new ChatChannelId(channelType, this.generatebadumnaId(), "proximity");
            var sessionId = this.generatebadumnaId();
            channel.Stub(c => c.Id).Return(channelId);
            channel.Stub(c => c.TransportId).Return(channelId.TransportId);
            channel.Stub(c => c.SessionId).Return(sessionId);
            return channel;
        }
    }
}
