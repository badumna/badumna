﻿//-----------------------------------------------------------------------
// <copyright file="ChatStatusNotificationTester.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2012 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BadumnaTests.Chat
{
    using Badumna.Chat;
    using Badumna.Core;
    using Badumna.DataTypes;
    using Badumna.Utilities;
    using NUnit.Framework;
    using Rhino.Mocks;

    public class ChatStatusNotificationTester : BadumnaTests.Core.ParseableTestHelper
    {
        public ChatStatusNotificationTester()
            : base(new ParseableTester())
        {
        }

        internal class ParseableTester : BadumnaTests.Core.ParseableTester<ChatStatusNotification>
        {
            public override void AssertAreEqual(ChatStatusNotification expected, ChatStatusNotification actual)
            {
                Assert.IsTrue(expected.Status == actual.Status && expected.Type == actual.Type, actual + " != " + expected);
            }

            public override ICollection<ChatStatusNotification> CreateExpectedValues()
            {
                ICollection<ChatStatusNotification> expectedValues = new List<ChatStatusNotification>();

                expectedValues.Add(new ChatStatusNotification(ChatStatus.Away, ChatNotificationType.Default));
                expectedValues.Add(new ChatStatusNotification(ChatStatus.Online, ChatNotificationType.Ask));

                return expectedValues;
            }
        }
    }
}
