﻿//-----------------------------------------------------------------------
// <copyright file="UserFixture.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2012 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using Badumna.Chat;
using Badumna.DataTypes;
using Badumna.Security;
using Badumna.Utilities;

namespace BadumnaTests.Chat
{
    /// <summary>
    /// Helper methods for generating users with reasonable defaults.
    /// 
    /// Using a method that specifies only as much information as your test requires
    /// means we can confine changes in constructors etc mostly to this class.
    /// </summary>
    public class UserFixture
    {
        internal static GenericCallBackReturn<BadumnaId> BadumnaIdGenerator = BadumnaTests.Utilities.BadumnaIdGenerator.Local();
        
        internal static GenericCallBackReturn<int> CharacterIdGenerator = delegate
        {
            return ++lastCharacterId;
        };

        internal static GenericCallBackReturn<int> UserIdGenerator = delegate
        {
            return ++lastUserId;
        };

        private static int lastCharacterId = 0;
        private static int lastUserId = 0;

        internal static ChatUser UserNamed(string name)
        {
            return UserNamed(name, BadumnaIdGenerator());
        }

        internal static ChatUser User(Character character)
        {
            return User(character, BadumnaIdGenerator());
        }

        internal static ChatUser UserNamed(string name, BadumnaId id)
        {
            return User(UserFixture.CharacterNamed(name), id);
        }

        internal static ChatUser User(Character character, BadumnaId id)
        {
            return new ChatUser(id, character);
        }

        internal static Character CharacterNamed(string name)
        {
            return new Character(CharacterIdGenerator(), name);
        }

        internal static CertificateToken CertificateFor(string name)
        {
            return CertificateFor(CharacterNamed(name));
        }

        internal static CertificateToken CertificateFor(Character character)
        {
            return CertificateFor(UserIdGenerator(), character);
        }

        internal static CertificateToken CertificateFor(long userId)
        {
            return CertificateFor(userId, CharacterNamed("character"));
        }

        internal static CertificateToken CertificateFor(long userId, Character character)
        {
            return DefaultUserCertificate.Create(userId, character, DateTime.Now, new PrivateKey());
        }

        internal static UserPresenceStatus StatusFor(string name, ChatStatus status)
        {
            return StatusFor(UserNamed(name), status);
        }

        internal static UserPresenceStatus StatusFor(ChatUser user, ChatStatus status)
        {
            return new UserPresenceStatus(user, status);
        }

        internal static SignedUserPresenceStatus SignedStatusFor(string name, ChatStatus status)
        {
            return new SignedUserPresenceStatus(StatusFor(name, status), CertificateFor(name));
        }

        internal static SignedUserPresenceStatus SignedStatusFor(string name, BadumnaId id, ChatStatus status)
        {
            return new SignedUserPresenceStatus(StatusFor(UserNamed(name, id), status), CertificateFor(name));
        }

        internal static SignedUserPresenceStatus SignedStatusFor(ChatUser user, ChatStatus status)
        {
            return new SignedUserPresenceStatus(StatusFor(user, status), CertificateFor(user.Character));
        }

        internal static SignedChatUser SignedUser(ChatUser user)
        {
            return new SignedChatUser(user, CertificateFor(user.Character));
        }

        internal static SignedChatUser SignedUser(Character character)
        {
            return SignedUser(User(character));
        }
    }
}
