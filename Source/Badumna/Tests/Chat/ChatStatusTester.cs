﻿//-----------------------------------------------------------------------
// <copyright file="ChatStatusTester.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2012 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BadumnaTests.Chat
{
    using Badumna.Chat;
    using Badumna.Core;
    using Badumna.DataTypes;
    using Badumna.Utilities;
    using NUnit.Framework;
    using Rhino.Mocks;

    public class ChatStatusTester
    {
        [Test]
        public void NotificationTypeShouldNotOverlapChatStatus()
        {
            var statuses = Enum.GetValues(typeof(ChatStatus)).Cast<int>();
            var notificationTypes = Enum.GetValues(typeof(ChatNotificationType)).Cast<int>();

            Func<int, int, int> combineBits = (accum, status) => accum | status;
            int allStatuses = Enumerable.Aggregate(statuses, combineBits);
            int allNotificationTypes = Enumerable.Aggregate(notificationTypes, combineBits);

            int overlap = allStatuses & allNotificationTypes;

            Assert.AreEqual(overlap, 0);
        }
    }
}
