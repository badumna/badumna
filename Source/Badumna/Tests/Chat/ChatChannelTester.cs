﻿//-----------------------------------------------------------------------
// <copyright file="ChatChannelTester.cs" company="NICTA">
//     Copyright (c) 2012 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using Badumna.Chat;
using Badumna.DataTypes;
using Badumna.Utilities;
using BadumnaTests.Utilities;
using NUnit.Framework;
using Rhino.Mocks;

namespace BadumnaTests.Chat
{
    public class ChatChannelTester
    {
        private GenericCallBackReturn<BadumnaId> idGenerator;
        private Simulator eventQueue;
        private IInternalChatService chatService;
        private ChatPresenceHandler presenceHandler;
        private IInternalChatSession chatSession;
        private IChatProtocolFacade chatProtocolFacade;
        private UserPresenceStatus userStatus;

        [SetUp]
        public void SetUp()
        {
            this.idGenerator = BadumnaIdGenerator.Local();
            this.eventQueue = new Simulator();
            this.presenceHandler = MockRepository.GenerateMock<ChatPresenceHandler>();
            this.chatSession = MockRepository.GenerateMock<IInternalChatSession>();
            this.userStatus = UserFixture.StatusFor(UserFixture.UserNamed("username", this.idGenerator()), ChatStatus.Online);
            this.chatSession.Stub(s => s.UserStatus).Return(this.userStatus);
            this.chatService = MockRepository.GenerateMock<IInternalChatService>();
            this.chatProtocolFacade = MockRepository.GenerateStub<IChatProtocolFacade>();
        }

        [Test]
        public void ShouldFirePresenceHandlerWhenAUserInTheChannelChangesStatus()
        {
            var channel = this.CreateChannel();
            BadumnaId userId = this.idGenerator();
            ChatUser user = UserFixture.UserNamed("bob");
            channel.PresenceChanged(new UserPresenceStatus(user, ChatStatus.Away));
            this.presenceHandler.AssertWasCalled(p => p.Invoke(channel, user.SessionId, "bob", ChatStatus.Away));
        }

        [Test]
        public void ShouldNotFirePresenceHandlerWhenUserStatusIsTheSame()
        {
            var channel = this.CreateChannel();
            BadumnaId userId = this.idGenerator();
            ChatUser user = UserFixture.UserNamed("bob");
            channel.PresenceChanged(new UserPresenceStatus(user, ChatStatus.Away));
            channel.PresenceChanged(new UserPresenceStatus(user, ChatStatus.Away));
            channel.PresenceChanged(new UserPresenceStatus(user, ChatStatus.Online));
            channel.PresenceChanged(new UserPresenceStatus(user, ChatStatus.Online));
            channel.PresenceChanged(new UserPresenceStatus(user, ChatStatus.Online));
            this.presenceHandler.AssertWasCalled(p => p.Invoke(channel, user.SessionId, "bob", ChatStatus.Away), o => o.Repeat.Once());
            this.presenceHandler.AssertWasCalled(p => p.Invoke(channel, user.SessionId, "bob", ChatStatus.Online), o => o.Repeat.Once());
        }

        [Test]
        public void ShouldNotErrorWhenThereIsNoPresenceHandler()
        {
            ChatChannelId channelId = new ChatChannelId(ChatChannelType.Private, this.idGenerator(), "channel name");
            IInternalChatChannel channel = new ChatChannel(
                channelId,
                null,
                null,
                this.eventQueue,
                this.chatProtocolFacade,
                this.chatSession,
                this.chatService);

            ChatUser user = UserFixture.UserNamed("bob", this.idGenerator());
            channel.PresenceChanged(new UserPresenceStatus(user, ChatStatus.Away));
        }

        [Test]
        public void ShouldRemoveUserFromChannelWhenUnsubscribingWithoutChangingSessionStatus()
        {
            var channel = this.CreateChannel();
            ChatChannelId id = channel.Id;
            channel.Unsubscribe();

            this.eventQueue.RunAllIgnoringTime();

            this.chatSession.AssertWasCalled(s => s.RemoveChannel(channel));
            Assert.AreEqual(ChatStatus.Online, this.chatSession.UserStatus.Status);
        }

        [Test]
        public void ShouldRemovePrivateChannelWhenPeerGoesOffline()
        {
            ChatUser otherUser = UserFixture.UserNamed("bob", this.idGenerator());
            IInternalChatChannel channel = this.CreateChannel(new ChatChannelId(ChatChannelType.Private, otherUser.SessionId, "chat"));
            channel.PresenceChanged(new UserPresenceStatus(otherUser, ChatStatus.Offline));
            this.chatSession.AssertWasCalled(s => s.RemoveChannel(channel));
        }

        [Test]
        public void ShouldNotRemoveProximityChannelWhenPeerGoesOffline()
        {
            ChatUser otherUser = UserFixture.UserNamed("bob", this.idGenerator());
            IInternalChatChannel channel = this.CreateChannel(ChatChannelType.Proximity);
            channel.PresenceChanged(new UserPresenceStatus(otherUser, ChatStatus.Offline));
            this.chatSession.AssertWasNotCalled(s => s.RemoveChannel(channel));
        }

        private IInternalChatChannel CreateChannel()
        {
            return this.CreateChannel(ChatChannelType.Private);
        }

        private IInternalChatChannel CreateChannel(ChatChannelType channelType)
        {
            return this.CreateChannel(new ChatChannelId(channelType, this.idGenerator(), "channel name"));
        }

        private IInternalChatChannel CreateChannel(ChatChannelId channelId)
        {
            return new ChatChannel(
                channelId,
                null,
                this.presenceHandler,
                this.eventQueue,
                this.chatProtocolFacade,
                this.chatSession,
                this.chatService);
        }
    }
}
