﻿//-----------------------------------------------------------------------
// <copyright file="UserPresenceStatusTester.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2012 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BadumnaTests.Chat
{
    using System;
    using System.Collections.Generic;
    using System.Security.Cryptography;
    using Badumna.Chat;
    using Badumna.Core;
    using Badumna.DataTypes;
    using Badumna.Security;
    using NUnit.Framework;
    using Rhino.Mocks;

    [TestFixture]
    [DontPerformCoverage]
    public class UserPresenceStatusTester : BadumnaTests.Core.ParseableTestHelper
    {
        public UserPresenceStatusTester()
            : base(new ParseableTester())
        {
        }

        internal class ParseableTester : BadumnaTests.Core.ParseableTester<UserPresenceStatus>
        {
            public override ICollection<UserPresenceStatus> CreateExpectedValues()
            {
                ICollection<UserPresenceStatus> expectedValues = new List<UserPresenceStatus>();

                expectedValues.Add(new UserPresenceStatus(UserFixture.UserNamed("user1"), ChatStatus.Away));
                expectedValues.Add(new UserPresenceStatus(UserFixture.UserNamed("user2"), ChatStatus.Online));
                return expectedValues;
            }
        }
    }
}
