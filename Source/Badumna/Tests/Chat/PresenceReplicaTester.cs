﻿//---------------------------------------------------------------------------------
// <copyright file="PresenceReplicaTester.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

namespace BadumnaTests.Chat
{
    using System;
    using System.Collections.Generic;
    using Badumna.Chat;
    using Badumna.Core;
    using Badumna.DataTypes;
    using Badumna.DistributedHashTable;
    using Badumna.Security;
    using BadumnaTests.Core;
    using BadumnaTests.Utilities;
    using NUnit.Framework;

    [TestFixture]
    [DontPerformCoverage]
    public class PresenceReplicaTester : ParseableTestHelper
    {
        private Badumna.Utilities.GenericCallBackReturn<BadumnaId> idGenerator;

        public PresenceReplicaTester()
            : base(new ParseableTester())
        {
            this.idGenerator = BadumnaIdGenerator.Local();
        }

        [Test]
        public void TestPropertiesForUserWithPresence()
        {
            var status = UserFixture.SignedStatusFor("characterName", ChatStatus.Away);
            var replica = new PresenceReplica(status);
            
            Assert.AreEqual(replica.CharacterName, "characterName");
            Assert.AreEqual(replica.StatusKnown, true);
            Assert.AreEqual(replica.PresenceStatus, status);
            Assert.AreEqual(replica.IsOnline, true);
            Assert.AreEqual(replica.Guid, status.SessionId);
        }

        [Test]
        public void TestPropertiesForUserWithoutPresence()
        {
            var guid = new BadumnaId(new PeerAddress(HashKey.Hash("source")), 4);
            var replica = new PresenceReplica("characterName", guid);

            Assert.AreEqual(replica.CharacterName, "characterName");
            Assert.AreEqual(replica.StatusKnown, false);
            Assert.AreEqual(replica.PresenceStatus, null);
            Assert.AreEqual(replica.IsOnline, false);
            Assert.AreEqual(replica.Guid, guid);
        }

        [Test]
        public void PendingRequestsIgnoresDuplicateUserss()
        {
            Character bob = UserFixture.CharacterNamed("Bob");
            Character fred = UserFixture.CharacterNamed("Fred");

            ChatUser bobUser1 = UserFixture.User(bob);
            ChatUser bobUser2 = UserFixture.User(bob);
            Assert.AreNotEqual(bobUser1, bobUser2);

            SignedChatUser signedBob1 = UserFixture.SignedUser(bobUser1);
            SignedChatUser signedBob2 = UserFixture.SignedUser(bobUser2);
            SignedChatUser signedFred = UserFixture.SignedUser(fred);

            var replica = new PresenceReplica("characterName", this.idGenerator());
            replica.AddPendingRequest(signedBob1);
            replica.AddPendingRequest(signedBob2);
            replica.AddPendingRequest(signedBob1);
            replica.AddPendingRequest(signedFred);
            replica.AddPendingRequest(signedBob2);

            var requestors = new List<ChatUser>();
            replica.ForEachPendingRequest(r => requestors.Add(r.User));

            Assert.AreEqual(new List<ChatUser> { bobUser1, bobUser2, signedFred.User }, requestors);
        }

        internal class ParseableTester : ParseableTester<PresenceReplica>
        {
            public override ICollection<PresenceReplica> CreateExpectedValues()
            {
                ICollection<PresenceReplica> expectedValues = new List<PresenceReplica>();
                PresenceReplica replica = new PresenceReplica("john", new BadumnaId(PeerAddress.GetLoopback(1), 1));
                PresenceReplica replica2 = new PresenceReplica(UserFixture.SignedStatusFor("mary", ChatStatus.Online));

                expectedValues.Add(replica);
                expectedValues.Add(replica2);

                return expectedValues;
            }

            public override void AssertAreEqual(PresenceReplica expected, PresenceReplica actual)
            {
                Assert.IsTrue(expected.PresenceStatus == actual.PresenceStatus || expected.PresenceStatus.Equals(actual.PresenceStatus));
                Assert.AreEqual(expected.CharacterName, actual.CharacterName);
                Assert.AreEqual(expected.Guid, actual.Guid);
            }
        }
    }
}