﻿//---------------------------------------------------------------------------------
// <copyright file="ChatServiceTester.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2012 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

namespace BadumnaTests.Chat
{
    using Badumna.Chat;
    using Badumna.Core;
    using Badumna.DataTypes;
    using Badumna.Security;
    using Badumna.Utilities;
    using NUnit.Framework;
    using Rhino.Mocks;
    using Utilities;
    using Collections = System.Collections.Generic;

    [DontPerformCoverage]
    public class ChatServiceTester
    {
        private Simulator eventQueue;
        private ChatService chatService;
        private IInternalChatService internalChatService;
        private IPeerConnectionNotifier connectionNotifier;
        private StubNetworkConnectivityReporter connectivityReporter;
        private GenericCallBackReturn<BadumnaId> generateBadumnaId;
        private ChatSessionFactory chatSessionFactory;
        private ChatProtocolFacadeFactory chatProtocolFacadeFactory;
        private IChatProtocolFacade chatProtocolFacade;

        [SetUp]
        public void Init()
        {
            this.generateBadumnaId = BadumnaIdGenerator.Local();

            this.chatSessionFactory = delegate(ITokenCache tokenCache, IInternalChatService chatService, IChatProtocolFacade protocolFacade)
            {
                var mock = MockRepository.GenerateStub<IInternalChatSession>();
                mock.Stub(m => m.Id).Return(this.generateBadumnaId());
                mock.Stub(m => m.Character).Return(tokenCache.GetUserCertificateToken().Character);
                return mock;
            };
            this.chatProtocolFacade = null;
            this.chatProtocolFacadeFactory = (service) => this.chatProtocolFacade;
            this.connectivityReporter = new StubNetworkConnectivityReporter();
            this.connectionNotifier = MockRepository.GenerateStub<IPeerConnectionNotifier>();

            this.eventQueue = new Simulator();
            this.chatService = new ChatService(
                this.chatProtocolFacadeFactory,
                this.chatSessionFactory,
                this.eventQueue,
                this.connectivityReporter,
                this.connectionNotifier);
            this.internalChatService = this.chatService;
            this.internalChatService.Initialize();
        }

        public void InitChannel(ChatChannelType channelType)
        {
            this.connectivityReporter.SetStatus(ConnectivityStatus.Online);
            this.internalChatService.Initialize();
            this.eventQueue.RunOne(); // runs the queued ChatService.SendPendingMessage(), which will do nothing yet.
        }

        [Test]
        public void ShouldShutdownEachSessionWhenNetworkShuttingDown()
        {
            var session1 = this.CreateSession(UserFixture.CertificateFor("user1"));
            var session2 = this.CreateSession(UserFixture.CertificateFor("user2"));

            this.connectivityReporter.Shutdown();

            session1.AssertWasCalled(s => s.Shutdown());
            session2.AssertWasCalled(s => s.Shutdown());
        }

        [Test]
        public void ShouldRemoveSessionOnLogout()
        {
            var certificate = UserFixture.CertificateFor("user1");
            var session = this.CreateSession(certificate);

            Assert.AreEqual(1, CollectionUtils.Length(this.chatService.Sessions));

            session.Raise(s => s.LoggingOutEvent += null, session, null);
            Assert.AreEqual(0, CollectionUtils.Length(this.chatService.Sessions));
        }

        [Test]
        public void ShouldStoreEventsWhenofflineAndReplayThemInOrderWhenOnline()
        {
            this.connectivityReporter.GoOffline();
            this.eventQueue.RunAllIgnoringTime();
            var calls = new Collections.List<string>();
            GenericCallBack action1 = () => calls.Add("one");
            GenericCallBack action2 = () => calls.Add("two");

            Assert.IsFalse(this.internalChatService.DoWhenOnline(action1));
            Assert.IsFalse(this.internalChatService.DoWhenOnline(action2));
            this.eventQueue.RunAllIgnoringTime();

            Assert.IsEmpty(calls);

            this.connectivityReporter.GoOnline();
            this.eventQueue.RunAllIgnoringTime();

            Assert.AreEqual(new Collections.List<string> { "one", "two" }, calls);
        }

        [Test]
        public void ShouldStoreSessionEventsWhenofflineAndReplayOnlyTheLatestEventForEachSessionWhenOnline()
        {
            this.connectivityReporter.GoOffline();
            this.eventQueue.RunAllIgnoringTime();
            BadumnaId sessionOneId = this.generateBadumnaId();
            BadumnaId sessionTwoId = this.generateBadumnaId();
            var calls = new Collections.List<string>();
            GenericCallBack sessionOneActionOne = () => calls.Add("session one, action one");
            GenericCallBack sessionOneActionTwo = () => calls.Add("session one, action two");
            GenericCallBack sessionTwoActionOne = () => calls.Add("session two, action one");

            Assert.IsFalse(this.internalChatService.DoWhenOnlineReplacingExisting(sessionOneId, sessionOneActionOne));
            Assert.IsFalse(this.internalChatService.DoWhenOnlineReplacingExisting(sessionOneId, sessionOneActionTwo));
            Assert.IsFalse(this.internalChatService.DoWhenOnlineReplacingExisting(sessionTwoId, sessionTwoActionOne));
            this.eventQueue.RunAllIgnoringTime();

            Assert.IsEmpty(calls);

            this.connectivityReporter.GoOnline();
            this.eventQueue.RunAllIgnoringTime();

            Assert.AreEqual(new Collections.List<string> { "session one, action two", "session two, action one" }, calls);
        }

        [Test]
        public void ShouldPerformEventsImmediatelyWhenOnline()
        {
            this.connectivityReporter.GoOnline();
            this.eventQueue.RunAllIgnoringTime();
            var calls = new Collections.List<string>();
            GenericCallBack action1 = () => calls.Add("one");
            GenericCallBack action2 = () => calls.Add("two");

            Assert.IsTrue(this.internalChatService.DoWhenOnline(action1));
            Assert.IsTrue(this.internalChatService.DoWhenOnline(action2));

            Assert.AreEqual(new Collections.List<string> { "one", "two" }, calls);
        }

        [Test]
        public void ShouldMarkAllUsersOnAGivenPeerAsOfflineWhenAPeerConnectionIsLost()
        {
            var remoteAddress = PeerAddress.GetAddress("127.0.0.17", 77);
            var disconnectedUser1 = new ChatUser(new BadumnaId(remoteAddress, 12), UserFixture.CharacterNamed("disconnected user 1"));
            var disconnectedUser2 = new ChatUser(new BadumnaId(remoteAddress, 13), UserFixture.CharacterNamed("disconnected user 2"));
            var unchangedUser = new ChatUser(this.generateBadumnaId(), UserFixture.CharacterNamed("unchanged user"));

            var channelId = new ChatChannelId(ChatChannelType.Private, this.generateBadumnaId());
            var channel = MockRepository.GenerateStub<IInternalChatChannel>();
            channel.Stub(c => c.Id).Return(channelId);
            channel.Stub(c => c.Participants).Return(new Collections.List<ChatUser> { disconnectedUser1, disconnectedUser2, unchangedUser });

            var tokenCache = MockRepository.GenerateStub<ITokenCache>();
            tokenCache.Stub(c => c.GetUserCertificateToken()).Return(UserFixture.CertificateFor("my username"));

            var session = this.internalChatService.CreateSession(tokenCache);
            session.Stub(s => s.Channels).Return(new Collections.List<IInternalChatChannel> { channel });
            session.Stub(s => s.HasChannel(channelId)).Return(true);

            this.connectionNotifier.Raise(n => n.ConnectionLostEvent += null, remoteAddress);
            this.eventQueue.RunAllIgnoringTime();

            session.AssertWasCalled(s => s.ReceivePresence(channelId, new UserPresenceStatus(disconnectedUser1, ChatStatus.Offline), ChatNotificationType.Default));
            session.AssertWasCalled(s => s.ReceivePresence(channelId, new UserPresenceStatus(disconnectedUser2, ChatStatus.Offline), ChatNotificationType.Default));
            session.AssertWasNotCalled(s => s.ReceivePresence(channelId, new UserPresenceStatus(unchangedUser,  ChatStatus.Offline), ChatNotificationType.Default));
        }

        private IInternalChatSession CreateSession(CertificateToken token)
        {
            var tokenCache = MockRepository.GenerateStub<ITokenCache>();
            tokenCache.Stub(c => c.GetUserCertificateToken()).Return(token);
            var session = this.internalChatService.CreateSession(tokenCache);
            return session;
        }
    }
}
