﻿//---------------------------------------------------------------------------------
// <copyright file="PresenceServiceTester.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

using Badumna.Chat;

namespace BadumnaTests.Chat
{
    using System;
    using System.Collections.Generic;
    using Badumna.Core;
    using Badumna.DataTypes;
    using Badumna.DistributedHashTable;
    using Badumna.Security;
    using Badumna.Utilities;
    using BadumnaTests.Core;
    using BadumnaTests.DistributedHashTable.Services;
    using Rhino.Mocks;

    // don't import all of NUnit, as it shadows the rather useful `List` class.
    using Assert = NUnit.Framework.Assert;
    using SetUp = NUnit.Framework.SetUpAttribute;
    using Test = NUnit.Framework.TestAttribute;

    [DontPerformCoverage]
    public class PresenceServiceTester
    {
        private MockLoopbackRouter router;
        private DhtFacade facade;
        private PresenceServiceStub presenceService;
        private BadumnaId myId;
        private Simulator eventQueue;
        private INetworkConnectivityReporter connectivityReporter;
        private SignedUserPresenceStatus myOnlineStatus;
        private ChatUser myUser;
        private string myUserName;
        private HashKey myUserHash;
        private BadumnaId anotherId;
        private ChatUser anotherUser;
        private ITokenCache tokenCache;
        private ICertificateToken myUserToken;

        [SetUp]
        public void SetUp()
        {
            this.eventQueue = new Simulator();
            ITime timeKeeper = this.eventQueue;

            INetworkAddressProvider addressProvider = MockRepository.GenerateStub<INetworkAddressProvider>();
            addressProvider.Stub(x => x.PublicAddress).Return(PeerAddress.GetLoopback(1));

            this.myId = new BadumnaId(addressProvider.PublicAddress, 1);
            this.anotherId = new BadumnaId(addressProvider.PublicAddress, 2);
            this.myUserName = "username";
            this.myUserHash = HashKey.Hash(this.myUserName);

            this.anotherUser = new ChatUser(this.anotherId, new Character("another username"));
            this.myUser = new ChatUser(this.myId, new Character(this.myUserName));

            this.myOnlineStatus = UserFixture.SignedStatusFor(this.myUser, ChatStatus.Online);
            this.myUserToken = this.myOnlineStatus.UserCertificate;

            this.tokenCache = MockRepository.GenerateStub<ITokenCache>();
            this.tokenCache.Stub(c => c.GetTrustedAuthorityToken()).Return(new UnverifiedIdentityProvider().GetTrustedAuthorityToken());
            this.tokenCache.Stub(c => c.GetUserCertificateToken()).Return(this.myUserToken);

            this.router = new MockLoopbackRouter(this.eventQueue, addressProvider);

            this.connectivityReporter = new NetworkConnectivityReporter(this.eventQueue);
            this.connectivityReporter.SetStatus(ConnectivityStatus.Online);

            this.facade = new DhtFacade(this.router, this.eventQueue, timeKeeper, addressProvider, this.connectivityReporter, MessageParserTester.DefaultFactory);
            this.router.DhtFacade = this.facade;

            Assert.AreEqual(this.facade, this.router.DhtFacade);
            Assert.AreEqual(HashKey.Min, this.router.LocalHashKey);

            this.presenceService = new PresenceServiceStub(this.facade, this.eventQueue, timeKeeper, this.connectivityReporter, this.tokenCache);
            this.facade.ForgeMethodList();
        }

        [Test]
        public void FirstSetStatusTriggersPresenceReplicaToBeUploaded()
        {
            Assert.AreEqual(0, this.presenceService.HandleChangePresenceCount);
            this.presenceService.SetStatus(this.myOnlineStatus);
            this.eventQueue.RunFor(TimeSpan.FromSeconds(1));
            Assert.AreEqual(1, this.presenceService.HandleChangePresenceCount);

            PresenceReplica replica = this.facade.AccessReplica<PresenceReplica>(this.myUserHash, this.myId);
            Assert.IsNotNull(replica);
        }

        [Test]
        public void PresenceReplicaIsRenewedRegularly()
        {
            Assert.AreEqual(0, this.presenceService.HandleChangePresenceCount);
            this.presenceService.SetStatus(this.myOnlineStatus);
            this.eventQueue.RunFor(TimeSpan.FromSeconds(1));
            Assert.AreEqual(1, this.presenceService.HandleChangePresenceCount);
            int totalRuntime = (int)Parameters.PresenceRenewalTaskInterval.TotalSeconds * 10;
            this.eventQueue.RunFor(TimeSpan.FromSeconds(totalRuntime));
            Assert.AreEqual(11, this.presenceService.HandleChangePresenceCount);
        }

        [Test]
        public void PresenceReplicaIsRenewedRegularlyForMultipleUsers()
        {
            Assert.AreEqual(0, this.presenceService.HandleChangePresenceCount);
            this.presenceService.SetStatus(UserFixture.SignedStatusFor(this.myUser, ChatStatus.Online));
            this.presenceService.SetStatus(UserFixture.SignedStatusFor(this.anotherUser, ChatStatus.Online));
            this.eventQueue.RunFor(TimeSpan.FromSeconds(1));
            Assert.AreEqual(2, this.presenceService.HandleChangePresenceCount);
            int totalRuntime = (int)Parameters.PresenceRenewalTaskInterval.TotalSeconds * 5;
            this.eventQueue.RunFor(TimeSpan.FromSeconds(totalRuntime));
            Assert.AreEqual(12, this.presenceService.HandleChangePresenceCount);
        }

        [Test]
        public void PresenceReplicaStopsRenewingOfflineUser()
        {
            // online
            Assert.AreEqual(0, this.presenceService.HandleChangePresenceCount);
            this.presenceService.SetStatus(this.myOnlineStatus);
            this.eventQueue.RunFor(TimeSpan.FromSeconds(1));
            Assert.AreEqual(1, this.presenceService.HandleChangePresenceCount);

            // go offline
            this.presenceService.SetStatus(UserFixture.SignedStatusFor(this.myUser, ChatStatus.Offline));
            this.eventQueue.RunFor(TimeSpan.FromSeconds(1));
            Assert.AreEqual(2, this.presenceService.HandleChangePresenceCount);

            // run for a while, no updates should occur
            int runTime = (int)Parameters.PresenceRenewalTaskInterval.TotalSeconds * 5;
            this.eventQueue.RunFor(TimeSpan.FromSeconds(runTime));
            Assert.AreEqual(2, this.presenceService.HandleChangePresenceCount);
        }

        [Test]
        public void ShutdownStopsTheReplicaRenewalTask()
        {
            this.PresenceReplicaIsRenewedRegularly();
            int count = this.presenceService.HandleChangePresenceCount;
            this.presenceService.Shutdown();
            int totalRuntime = (int)Parameters.PresenceRenewalTaskInterval.TotalSeconds * 10;
            this.eventQueue.RunFor(TimeSpan.FromSeconds(totalRuntime));
            Assert.AreEqual(count, this.presenceService.HandleChangePresenceCount);
        }

        [Test]
        public void UpdateReplicaDeniedForInvalidCertificate()
        {
            var identityProvider = new UnverifiedIdentityProvider("Tom", UnverifiedIdentityProvider.GenerateKeyPair());
            var user = UserFixture.UserNamed("Bob");
            var status = new UserPresenceStatus(user, ChatStatus.Away);
            var signedStatus = new SignedUserPresenceStatus(status, identityProvider.GetUserCertificateToken());
            Assert.Throws<Badumna.Security.SecurityException>(() => this.presenceService.TriggerStatusUpdate(signedStatus), "Invalid user certificate");
        }

        [Test]
        public void ReceivePresenceIgnoredForInvalidCertificate()
        {
            var user = UserFixture.UserNamed("Bob");
            
            var awayStatus = new UserPresenceStatus(user, ChatStatus.Away);
            var invalidSignedStatus = new SignedUserPresenceStatus(
                awayStatus,
                UserFixture.CertificateFor("not bob"));

            var onlineStatus = new UserPresenceStatus(user, ChatStatus.Online);
            var validUserCertificate = UserFixture.CertificateFor(user.Character);
            var validSignedStatus = new SignedUserPresenceStatus(
                onlineStatus,
                validUserCertificate);

            List<UserPresenceStatus> statuses = new List<UserPresenceStatus>();
            this.presenceService.PresenceEvent += (sender, evt) => statuses.Add(evt.Status);

            this.presenceService.TriggerReceivePresence(invalidSignedStatus);
            this.presenceService.TriggerReceivePresence(validSignedStatus);

            Assert.AreEqual(new List<UserPresenceStatus> { onlineStatus }, statuses);
        }

        [Test]
        public void ChangePresenceTriggerPresenceReplicaToBeUpdated()
        {
            Assert.AreEqual(0, this.presenceService.HandleChangePresenceCount);
            this.presenceService.SetStatus(this.myOnlineStatus);
            this.eventQueue.RunFor(TimeSpan.FromSeconds(1));
            Assert.AreEqual(1, this.presenceService.HandleChangePresenceCount);
            this.presenceService.SetStatus(UserFixture.SignedStatusFor(this.myUser, ChatStatus.Away));
            this.eventQueue.RunFor(TimeSpan.FromSeconds(1));
            Assert.AreEqual(2, this.presenceService.HandleChangePresenceCount);
        }

        [Test]
        public void RequestSubscriptionActuallySendsTheRequest()
        {
            Assert.AreEqual(0, this.presenceService.HandleForwarderSubscriptionRequestCount);
            this.presenceService.SetStatus(this.myOnlineStatus);
            this.eventQueue.RunFor(TimeSpan.FromSeconds(1));
            Assert.AreEqual(0, this.presenceService.HandleForwarderSubscriptionRequestCount);

            int total = 10;
            for (int i = 0; i < total; i++)
            {
                this.presenceService.RequestSubscriptionTo(this.myUser, "user_name_" + i);
            }

            this.eventQueue.RunFor(TimeSpan.FromSeconds(1));
            Assert.AreEqual(total, this.presenceService.HandleForwarderSubscriptionRequestCount);
        }

        [Test]
        public void SubscriptionIsRenewedRegularly()
        {
            Assert.AreEqual(0, this.presenceService.HandleForwarderSubscriptionRequestCount);
            this.eventQueue.RunFor(TimeSpan.FromSeconds(1));

            int total = 10;
            for (int i = 0; i < total; i++)
            {
                this.presenceService.RequestSubscriptionTo(this.myUser, "user_name_" + i);
            }

            this.eventQueue.RunFor(TimeSpan.FromSeconds(1));

            int totalRuntime = (int)Parameters.PresenceSubscriptionRenewalInterval.TotalSeconds * 3;
            this.eventQueue.RunFor(TimeSpan.FromSeconds(totalRuntime));
            Assert.AreEqual(30, this.presenceService.HandleForwarderSubscriptionRequestCount);
        }

        [Test]
        public void NoErrorsWhenUserTokenReplacementHasNoCharacter()
        {
            var eventSubscriptionCalls = this.tokenCache.GetArgumentsForCallsMadeOn(
                c => c.SubscribeToExpireEvent(Arg.Is(TokenType.UserCertificate), Arg<EventHandler<TokenExpirationEventArgs>>.Is.Anything));
            Assert.AreEqual(1, eventSubscriptionCalls.Count);
            EventHandler<TokenExpirationEventArgs> expirationHandler = (EventHandler<TokenExpirationEventArgs>)eventSubscriptionCalls[0][1];
            Assert.IsNotNull(expirationHandler);

            expirationHandler(null, new TokenExpirationEventArgs(TokenType.UserCertificate, null, UserFixture.CertificateFor((Character)null)));
            expirationHandler(null, new TokenExpirationEventArgs(TokenType.UserCertificate, null, null));
        }

        [Test]
        public void PresenceIsRenewedWhenUserTokenExpires()
        {
            var eventSubscriptionCalls = this.tokenCache.GetArgumentsForCallsMadeOn(
                c => c.SubscribeToExpireEvent(Arg.Is(TokenType.UserCertificate), Arg<EventHandler<TokenExpirationEventArgs>>.Is.Anything));
            Assert.AreEqual(1, eventSubscriptionCalls.Count);
            EventHandler<TokenExpirationEventArgs> expirationHandler = (EventHandler<TokenExpirationEventArgs>)eventSubscriptionCalls[0][1];
            Assert.IsNotNull(expirationHandler);

            Assert.AreEqual(0, this.presenceService.HandleChangePresenceCount);
            this.presenceService.SetStatus(this.myOnlineStatus);
            this.eventQueue.RunFor(TimeSpan.FromSeconds(1));
            Assert.AreEqual(1, this.presenceService.HandleChangePresenceCount);

            PresenceReplica replica = this.facade.AccessReplica<PresenceReplica>(this.myUserHash, this.myId);
            ICertificateToken oldCertificate = replica.PresenceStatus.UserCertificate;
            CertificateToken newCertificate = UserFixture.CertificateFor(this.myUser.Character);
            this.tokenCache.Stub(c => c.GetUserCertificateToken()).Return(newCertificate).Repeat.Any();
            
            // sanity-check: make sure the token has changed
            Assert.AreNotEqual(this.tokenCache.GetUserCertificateToken(), oldCertificate);

            expirationHandler(null, new TokenExpirationEventArgs(TokenType.UserCertificate, null, newCertificate));
            this.eventQueue.RunFor(TimeSpan.FromSeconds(1));
            Assert.AreEqual(2, this.presenceService.HandleChangePresenceCount);

            replica = this.facade.AccessReplica<PresenceReplica>(this.myUserHash, this.myId);
            Assert.IsNotNull(replica);

            Assert.AreEqual(replica.PresenceStatus.UserCertificate, newCertificate);
        }

        [Test]
        public void ShouldDropInvitationIfItDoesntMatchUserCertificate()
        {
            BadumnaId subjectId = this.myId;
            Character character = UserFixture.CharacterNamed("joe");
            SignedChatUser validUser = UserFixture.SignedUser(character);
            SignedChatUser invalidUser = new SignedChatUser(UserFixture.UserNamed("bob"), validUser.UserCertificate);

            List<ChatUser> requests = new List<ChatUser>();
            this.presenceService.PresenceSubscriptionEvent += (sender, evt) => requests.Add(evt.Requester);

            this.presenceService.TriggerSubscriptionEvent(invalidUser, subjectId);
            Assert.AreEqual(0, requests.Count);

            this.presenceService.TriggerSubscriptionEvent(validUser, subjectId);
            Assert.AreEqual(new List<ChatUser> { validUser.User }, requests);
        }
    }
}
