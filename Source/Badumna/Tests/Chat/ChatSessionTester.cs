﻿//---------------------------------------------------------------------------------
// <copyright file="ChatSessionTester.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2012 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

using System;
using BadumnaTests.Utilities;

namespace BadumnaTests.Chat
{
    using Badumna.Chat;
    using Badumna.Configuration;
    using Badumna.Core;
    using Badumna.DataTypes;
    using Badumna.Security;
    using Badumna.Utilities;
    using NUnit.Framework;
    using Rhino.Mocks;

    [DontPerformCoverage]
    public class ChatSessionTester
    {
        private Simulator eventQueue;
        private IInternalChatService chatService;
        private ChatSession chatSession;
        private ITokenCache tokenCache;
        private IInternalChatSession internalChatSession;
        private QueueInvokable queueApplicationEvent;
        private ChatInvitationHandler invitationHandler;
        private CertificateToken userCertificate;
        private ChatUser otherUser;
        private ChatMessageHandler messageHandler;
        private StubNetworkConnectivityReporter connectivityReporter;
        private GenericCallBackReturn<BadumnaId> generateBadumnaId;
        private IPresenceService presenceService;
        private string displayName;
        private IChatProtocolFacade chatProtocolFacade;
        private ChatProtocolFacadeFactory chatProtocolFacadeFactory;
        private IFeaturePolicy featurePolicy;

        [SetUp]
        public void Init()
        {
            this.generateBadumnaId = BadumnaIdGenerator.Local();

            this.eventQueue = new Simulator();
            this.queueApplicationEvent = this.queueApplicationEvent = i => i.Invoke();
            this.invitationHandler = MockRepository.GenerateStub<ChatInvitationHandler>();
            this.connectivityReporter = new StubNetworkConnectivityReporter();
            this.connectivityReporter.Status = ConnectivityStatus.Online;
            this.chatProtocolFacade = MockRepository.GenerateStub<IChatProtocolFacade>();
            this.chatProtocolFacadeFactory = (session) => this.chatProtocolFacade;
            this.featurePolicy = MockRepository.GenerateStub<IFeaturePolicy>();

            var chatSessionFactory = ChatSession.Factory(
                this.eventQueue,
                this.featurePolicy,
                this.queueApplicationEvent,
                this.generateBadumnaId,
                x =>
                    {
                        throw new NotImplementedException(
                            "Need to modify this test fixture's setup before IReplicableEntities can be used.");
                    });

            // Use an actual chat service, otherwise the mocking gets too complex when dealing with network online/offline events.
            this.chatService = new ChatService(this.chatProtocolFacadeFactory, chatSessionFactory, this.eventQueue, this.connectivityReporter, MockRepository.GenerateStub<IPeerConnectionNotifier>());
            this.chatService.Initialize();
            this.eventQueue.RunOne(); // run queued ChatService.SendPendingEvents(), which will do nothing at this point.
            
            this.presenceService = MockRepository.GenerateStub<IPresenceService>();

            this.displayName = "username";

            this.tokenCache = MockRepository.GenerateStub<ITokenCache>();
            this.userCertificate = UserFixture.CertificateFor(this.displayName);
            this.tokenCache.Stub(c => c.GetUserCertificateToken()).Return(this.userCertificate);

            this.chatSession = (ChatSession)this.chatService.CreateSession(this.tokenCache);
            this.internalChatSession = (IInternalChatSession)this.chatSession; // a convenient shortcut for accessing IInternalChatSession-only methods

            this.otherUser = UserFixture.UserNamed("other user", this.generateBadumnaId());
            this.messageHandler = MockRepository.GenerateStub<ChatMessageHandler>();
        }

        [Test]
        public void ChangePresenceUpdatesPresenceStatusAndSendsNotification()
        {
            this.chatSession.ChangePresence(ChatStatus.Away);

            this.eventQueue.RunOne();

            this.chatProtocolFacade.AssertWasCalled(p => p.PublishPresenceGlobally(this.chatSession));
            Assert.AreEqual(ChatStatus.Away, this.internalChatSession.UserStatus.Status);
        }

        [Test]
        public void OpenPrivateChannelsChecksFeaturePolicy()
        {
            this.chatSession.OpenPrivateChannels(this.invitationHandler);
            this.featurePolicy.AssertWasCalled(p => p.AccessPrivateChat());
        }

        [Test]
        public void InvitationForANewChannelCallsOurHandlerEachTime()
        {
            ChatChannelId channelId = new ChatChannelId(ChatChannelType.Private, this.otherUser.SessionId, "anything");
            ChatChannelId expectedChannel = new ChatChannelId(ChatChannelType.Private, this.otherUser.SessionId, "other username");

            this.chatSession.OpenPrivateChannels(this.invitationHandler);

            this.internalChatSession.ReceiveInvitation(channelId, this.otherUser);
            this.internalChatSession.ReceiveInvitation(channelId, this.otherUser);
            this.eventQueue.RunAllIgnoringTime();

            this.chatProtocolFacade.AssertWasNotCalled(
                p => p.PublishPresenceOnChannel(Arg<IInternalChatChannel>.Is.Anything, Arg<UserPresenceStatus>.Is.Anything));
            this.invitationHandler.AssertWasCalled(i => i.Invoke(Arg.Is(expectedChannel), Arg.Is(this.otherUser.Character.Name)), o => o.Repeat.Twice());
        }

        [Test]
        public void HandleAnInvitationForAChannelWeAlreadyHave()
        {
            ((IChatSession)this.chatSession).ChangePresence(ChatStatus.Online);
            ChatChannelId channelId = new ChatChannelId(ChatChannelType.Private, this.otherUser.SessionId, "anything");
            this.chatSession.OpenPrivateChannels(this.invitationHandler);

            IInternalChatChannel channel = this.internalChatSession.AcceptInvitation(channelId, null, null) as IInternalChatChannel;
            Assert.IsNotNull(channel);
            this.eventQueue.RunAllIgnoringTime();

            // initial subscription sends a broadcast, but does not ask the other recipient for their status
            Action<IChatProtocolFacade> broadcastPresence = p => p.PublishPresenceOnChannel(Arg.Is(channel), Arg.Is(this.internalChatSession.UserStatus), Arg.Is(ChatNotificationType.DontReply));
            Action<IChatProtocolFacade> askForPresence = p => p.PublishPresenceOnChannel(Arg.Is(channel), Arg.Is(this.internalChatSession.UserStatus), Arg.Is(ChatNotificationType.Ask));
            this.chatProtocolFacade.AssertWasCalled(broadcastPresence, o => o.Repeat.Once());
            this.chatProtocolFacade.AssertWasNotCalled(askForPresence);

            // having already added this channel, a new invitation should *not* call our invitation handler:
            this.internalChatSession.ReceiveInvitation(channelId, this.otherUser);
            this.eventQueue.RunAllIgnoringTime();

            this.chatProtocolFacade.AssertWasCalled(broadcastPresence, o => o.Repeat.Once());
            this.chatProtocolFacade.AssertWasCalled(askForPresence, o => o.Repeat.Once());
            this.invitationHandler.AssertWasNotCalled(i => i.Invoke(Arg<ChatChannelId>.Is.Anything, Arg<string>.Is.Anything));
        }

        [Test]
        public void ShouldPublishPresenceToNewlySubscribedChannel()
        {
            var channelId = new ChatChannelId(ChatChannelType.Private, this.otherUser.SessionId, "anything");
            this.chatSession.ChangePresence(ChatStatus.Online);
            var userPresenceStatus = this.internalChatSession.UserStatus;

            IChatChannel channel = this.SubscribeToChannel(channelId, null, null);

            this.chatProtocolFacade.AssertWasCalled(p => p.PublishPresenceOnChannel(
                Arg.Is(channel as IInternalChatChannel),
                Arg.Is(userPresenceStatus),
                Arg.Is(ChatNotificationType.DontReply)));
        }

        [Test]
        public void ReceiveInvitationDoesNothingWhenThereIsNoInvitationHandler()
        {
            ChatChannelId channelId = new ChatChannelId(ChatChannelType.Private, this.generateBadumnaId(), "new private chat");

            this.internalChatSession.ReceiveInvitation(channelId, this.otherUser);

            this.invitationHandler.AssertWasNotCalled(i => i.Invoke(Arg<ChatChannelId>.Is.Anything, Arg<string>.Is.Anything));
        }

        [Test]
        public void ReceivePresenceShouldSendStatusToUserWhenItReceivesAnAskStatus()
        {
            ChatChannelId channelId = new ChatChannelId(ChatChannelType.Private, this.generateBadumnaId(), "new private chat");
            this.chatSession.ChangePresence(ChatStatus.Away);
            ChatPresenceHandler presenceHandler = MockRepository.GenerateStub<ChatPresenceHandler>();
            var channel = this.SubscribeToChannel(channelId, null, presenceHandler);

            var otherUsersPresence = new UserPresenceStatus(this.otherUser, ChatStatus.Chat);
            var myUserPresence = new UserPresenceStatus(this.internalChatSession.User, ChatStatus.Away);

            this.internalChatSession.ReceivePresence(channelId, otherUsersPresence, ChatNotificationType.Ask);

            this.chatProtocolFacade.AssertWasCalled(p => p.SendPresenceToSpecificPeer(channelId, this.otherUser.SessionId, myUserPresence));
            presenceHandler.AssertWasCalled(c => c.Invoke(channel, this.otherUser.SessionId, this.otherUser.Character.Name, ChatStatus.Chat));
        }

        [Test]
        public void ReceiveMessageShouldHandleMessageWhenUserHasASubscriptionToTheGivenChannel()
        {
            ChatChannelId channelId = new ChatChannelId(ChatChannelType.Private, this.generateBadumnaId(), "new private chat");
            var channel = this.SubscribeToChannel(channelId, this.messageHandler, null);

            this.internalChatSession.ReceiveMessage(channelId, this.otherUser.SessionId, "hello");

            this.messageHandler.AssertWasCalled(h => h.Invoke(channel, this.otherUser.SessionId, "hello"));
        }

        [Test]
        public void ReceiveMessageShouldDoNothingWhenUserHasNotSubscribedToTheGivenChannel()
        {
            ChatChannelId channelId = new ChatChannelId(ChatChannelType.Private, this.generateBadumnaId(), "new private chat");

            this.internalChatSession.ReceiveMessage(channelId, this.otherUser.SessionId, "hello");

            this.messageHandler.AssertWasNotCalled(
                h => h.Invoke(Arg<IChatChannel>.Is.Anything, Arg<BadumnaId>.Is.Anything, Arg<string>.Is.Anything));
        }

        [Test]
        public void ShouldInviteUserToPrivateChatImmediatelyWhenTheNetworkIsOnline()
        {
            this.chatSession.OpenPrivateChannels(this.invitationHandler);
            this.chatSession.InviteUserToPrivateChannel("joe");
            this.eventQueue.RunAllIgnoringTime();

            this.chatProtocolFacade.AssertWasCalled(p => p.RequestPresenceSubscription(this.internalChatSession.User, "joe"));
        }

        [Test]
        public void ShouldInviteUserToPrivateChatTheNextTimeTheNetworkIsOnline()
        {
            this.connectivityReporter.SetStatus(ConnectivityStatus.Offline);

            this.chatSession.OpenPrivateChannels(this.invitationHandler);
            this.eventQueue.RunAllIgnoringTime();

            this.chatSession.InviteUserToPrivateChannel("joe");
            Assert.AreEqual(0, this.eventQueue.Count, this.eventQueue.DescribeQueuedEvents());

            // go toggle network connectivity a few times to make sure we only send the invite once in total
            this.connectivityReporter.GoOnline();
            this.connectivityReporter.GoOffline();
            this.connectivityReporter.GoOnline();

            this.eventQueue.RunAllIgnoringTime();

            this.chatProtocolFacade.AssertWasCalled(p => p.RequestPresenceSubscription(Arg.Is(this.internalChatSession.User), Arg.Is("joe")), o => o.Repeat.Once());
        }

        [Test]
        public void TestShutdownShouldTriggerLoggingOutEventIfSet()
        {
            var evt = MockRepository.GenerateStub<EventHandler>();
            this.chatSession.LoggingOutEvent += evt;

            this.internalChatSession.Shutdown();

            evt.AssertWasCalled(e => e.Invoke(this.chatSession, null));
        }

        [Test]
        public void LogoutShouldUnsubscribeEachChannelAndRemoveSessionFromChatService()
        {
            var channelId = new ChatChannelId(ChatChannelType.Private, this.otherUser.SessionId, "anything");
            this.chatSession.ChangePresence(ChatStatus.Online);
            IInternalChatChannel channel = (IInternalChatChannel)this.SubscribeToChannel(channelId, null, null);

            Assert.IsTrue(this.HasSession(this.displayName));

            this.internalChatSession.Shutdown();
            this.eventQueue.RunAllIgnoringTime();

            Assert.IsFalse(this.HasSession(this.displayName));
            Assert.AreEqual(ChatStatus.Offline, this.internalChatSession.UserStatus.Status);
            this.chatProtocolFacade.AssertWasCalled(p => p.PublishPresenceGlobally(this.internalChatSession));
            this.chatProtocolFacade.AssertWasCalled(p => p.LeaveChannel(channel, this.internalChatSession.User));
        }

        [Test]
        public void RemoveChannelShouldRemoveKeyImmediatelyAndScheduleChannelLeave()
        {
            var channelId = new ChatChannelId(ChatChannelType.Private, this.otherUser.SessionId, "anything");
            this.chatSession.ChangePresence(ChatStatus.Online);
            IInternalChatChannel channel = (IInternalChatChannel)this.SubscribeToChannel(channelId, null, null);
            this.eventQueue.RunAllIgnoringTime();

            Assert.IsTrue(this.internalChatSession.HasChannel(channelId));
            this.internalChatSession.RemoveChannel(channel);
            Assert.IsFalse(this.internalChatSession.HasChannel(channelId));

            this.eventQueue.RunOne();
            this.chatProtocolFacade.AssertWasCalled(p => p.LeaveChannel(channel, this.internalChatSession.User));
        }

        private bool HasSession(string name)
        {
            return CollectionUtils.Find(((ChatService)this.chatService).Sessions, s => s.Character.Name == name) != null;
        }

        private IChatChannel SubscribeToChannel(ChatChannelId id, ChatMessageHandler messageHandler, ChatPresenceHandler presenceHandler)
        {
            var channel = ((IInternalChatSession)this.chatSession).SubscribeToChatChannel(id, messageHandler, presenceHandler);
            this.eventQueue.RunAllIgnoringTime();
            return channel;
        }
    }
}
