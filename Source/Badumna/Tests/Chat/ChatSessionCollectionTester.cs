﻿//-----------------------------------------------------------------------
// <copyright file="ChatSessionCollectionTester.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2012 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;

namespace BadumnaTests.Chat
{
    using Badumna.Chat;
    using Badumna.Core;
    using Badumna.DataTypes;
    using Badumna.Security;
    using Badumna.Utilities;
    using NUnit.Framework;
    using Rhino.Mocks;
    using Utilities;

    [DontPerformCoverage]
    public class ChatSessionCollectionTester
    {
        private ChatSessionCollection sessions;
        private string name;
        private GenericCallBackReturn<BadumnaId> generateBadumnaId;

        [SetUp]
        public void SetUp()
        {
            this.name = "name";
            this.sessions = new ChatSessionCollection();
            this.generateBadumnaId = BadumnaIdGenerator.Local();
        }

        [Test]
        public void ShouldRefuseToAddASessionWithTheSameNameAsAnExistingSession()
        {
            var id = UserFixture.CharacterNamed(this.name);
            this.sessions.Add(this.SessionFor(id));
            Assert.Throws<ArgumentException>(() => this.sessions.Add(this.SessionFor(id)));
        }

        [Test]
        public void ShouldDeleteASession()
        {
            var session = this.SessionNamed(this.name);

            this.sessions.Add(session);

            Assert.DoesNotThrow(() => this.sessions.Remove(session));
        }

        [Test]
        public void ShouldNotDeleteASessionWithTheSameNameIfItIsNotTheSameSession()
        {
            var session1 = this.SessionNamed(this.name);
            var session2 = this.SessionNamed(this.name);

            this.sessions.Add(session1);

            Assert.Throws<KeyNotFoundException>(() => this.sessions.Remove(session2));
        }

        [Test]
        public void SessionPerformWithSessionIdShouldReturnFalseIfNoSuchSession()
        {
            var called = new List<string>();
            this.sessions.Add(this.SessionNamed("one"));
            this.sessions.Add(this.SessionNamed("two"));
            var missingSessionId = this.generateBadumnaId();
            
            Assert.IsFalse(this.sessions.SessionPerform(missingSessionId, s => called.Add(s.Character.Name)));

            Assert.IsEmpty(called);
        }

        [Test]
        public void SessionPerformWithSessionIdShouldPerformTheAction()
        {
            var called = new List<string>();
            var session1 = this.SessionNamed("one");
            var session2 = this.SessionNamed("two");
            this.sessions.Add(session1);
            this.sessions.Add(session2);

            Assert.IsTrue(this.sessions.SessionPerform(session1.Id, s => called.Add(s.Character.Name)));

            Assert.AreEqual(new List<string> { "one" }, called);
        }

        [Test]
        public void SessionPerformWithChannelIdShouldReturnFalseIfNoSuchChannel()
        {
            var called = new List<string>();
            var channelId = this.GenerateChannelId();
            var session1 = this.SessionNamed("one");
            this.sessions.Add(session1);

            Assert.IsFalse(this.sessions.SessionPerform(channelId, s => called.Add(s.Character.Name)));

            Assert.IsEmpty(called);
        }

        [Test]
        public void SessionPerformWithChannelIdShouldPerformTheActionOnAllMatchingSessions()
        {
            var called = new List<string>();
            var channelId = this.GenerateChannelId();
            var session1 = this.SessionNamed("one");
            var session2 = this.SessionNamed("two");
            var session3 = this.SessionNamed("three");
            this.sessions.Add(session1);
            this.sessions.Add(session2);
            this.sessions.Add(session3);
            session1.Stub(s => s.HasChannel(channelId)).Return(true);
            session3.Stub(s => s.HasChannel(channelId)).Return(true);

            Assert.IsTrue(this.sessions.SessionPerform(channelId, s => called.Add(s.Character.Name)));

            called.Sort();
            Assert.AreEqual(new List<string> { "one", "three" }, called);
        }

        private IInternalChatSession SessionNamed(string name)
        {
            return this.SessionFor(UserFixture.CharacterNamed(name));
        }

        private IInternalChatSession SessionFor(Character character)
        {
            var cert = UserFixture.CertificateFor(character);
            var session = MockRepository.GenerateStub<IInternalChatSession>();
            session.Stub(s => s.Character).Return(character);
            session.Stub(s => s.Id).Return(this.generateBadumnaId());
            return session;
        }

        private ChatChannelId GenerateChannelId()
        {
            return new ChatChannelId(ChatChannelType.Private, this.generateBadumnaId());
        }
    }
}

