﻿//---------------------------------------------------------------------------------
// <copyright file="ChatChannelIdTester.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

namespace BadumnaTests.Chat
{
    using System.Collections.Generic;
    using Badumna.Chat;
    using Badumna.Core;
    using Badumna.DataTypes;
    using NUnit.Framework;

    [TestFixture]
    [DontPerformCoverage]
    public class ChatChannelIdTester
    {
        [Test]
        public void NameAndTypeAreRecorded()
        {
            ChatChannelId id1 = new ChatChannelId(ChatChannelType.Private, new BadumnaId(PeerAddress.GetLoopback(1), 1), "tom");
            Assert.AreEqual("tom", id1.Name);
            Assert.AreEqual(ChatChannelType.Private, id1.Type);

            ChatChannelId id2 = new ChatChannelId(ChatChannelType.Private, id1.TransportId, "john");
            Assert.AreEqual("john", id2.Name);
            Assert.AreEqual(ChatChannelType.Private, id2.Type);
        }

        [Test]
        public void CopyconstructorActuallyCopiesMembers()
        {
            ChatChannelId id1 = new ChatChannelId(ChatChannelType.Private, new BadumnaId(PeerAddress.GetLoopback(1), 1), "tom");
            ChatChannelId id2 = new ChatChannelId(id1);

            Assert.AreEqual("tom", id2.Name);
            Assert.AreEqual(ChatChannelType.Private, id2.Type);

            id1.Name = "tom2";
            Assert.AreEqual("tom", id2.Name);
        }

        [Test]
        public void OrderingIsBasedOnTypeThenAddress()
        {
            BadumnaId id_1 = new BadumnaId(PeerAddress.GetLoopback(1), 1);
            BadumnaId id_2 = new BadumnaId(PeerAddress.GetLoopback(1), 2);
            ChatChannelId private_1 = new ChatChannelId(ChatChannelType.Private, new BadumnaId(id_1), "tom");
            ChatChannelId another_private_1 = new ChatChannelId(ChatChannelType.Private, new BadumnaId(id_1), "bob");
            ChatChannelId private_2 = new ChatChannelId(ChatChannelType.Private, new BadumnaId(id_2), "bob");
            ChatChannelId group_1 = new ChatChannelId(ChatChannelType.Group, new BadumnaId(id_1), "bob");

            Assert.AreEqual(private_1.CompareTo(another_private_1), 0);
            Assert.IsTrue(private_1.CompareTo(private_2) < 0);
            Assert.IsTrue(private_1.CompareTo(group_1) < 0);

            List<ChatChannelId> sorted = new List<ChatChannelId> { group_1, private_2, another_private_1, private_1 };
            sorted.Sort();

            List<ChatChannelId> expected = new List<ChatChannelId> { private_1, another_private_1, private_2, group_1 };
            Assert.AreEqual(sorted, expected);
        }

        [Test]
        public void ChatChannelIdsAreHashable()
        {
            BadumnaId id_1 = new BadumnaId(PeerAddress.GetLoopback(1), 1);
            BadumnaId id_2 = new BadumnaId(PeerAddress.GetLoopback(1), 2);
            ChatChannelId private_1 = new ChatChannelId(ChatChannelType.Private, new BadumnaId(id_1), "tom");
            ChatChannelId another_private_1 = new ChatChannelId(ChatChannelType.Private, new BadumnaId(id_1), "bob");
            ChatChannelId private_2 = new ChatChannelId(ChatChannelType.Private, new BadumnaId(id_2), "bob");

            var dict = new Dictionary<ChatChannelId, ChatChannelId>();

            Assert.AreEqual(private_1, another_private_1);

            dict.Add(private_1, private_1);
            dict.Add(private_2, private_2);

            Assert.AreEqual(dict[private_1], private_1);
            Assert.AreEqual(dict[another_private_1], private_1);
            Assert.AreEqual(dict[private_2], private_2);
        }
    }
}