﻿//-----------------------------------------------------------------------
// <copyright file="PrivateChatProtocolTester.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2012 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using Badumna.Chat;
using Badumna.Core;
using Badumna.DataTypes;
using Badumna.Security;
using Badumna.Transport;
using Badumna.Utilities;
using BadumnaTests.Utilities;
using NUnit.Framework;
using Rhino.Mocks;

namespace BadumnaTests.Chat
{
    public class PrivateChatProtocolTester
    {
        private IInternalChatService chatService;
        private PrivateChatProtocolStub protocol;
        private ProtocolComponent<TransportEnvelope> protocolComponent;
        private GenericCallBackReturn<Badumna.DataTypes.BadumnaId> idGenerator;
        
        [SetUp]
        public void SetUp()
        {
            this.chatService = MockRepository.GenerateStub<IInternalChatService>();
            var objectFactory = MockRepository.GenerateMock<GenericCallBackReturn<object, Type>>();
            var parser = MockRepository.GenerateStub<MessageParser>(
                "StubbedParser",
                typeof(ConnectionfulProtocolMethodAttribute),
                objectFactory);
            this.protocolComponent = MockRepository.GenerateStub<ProtocolComponent<TransportEnvelope>>(parser);
            var presenceService = MockRepository.GenerateStub<IPresenceService>();

            this.idGenerator = BadumnaIdGenerator.Local();
            
            this.protocol = new PrivateChatProtocolStub(this.protocolComponent, presenceService, this.chatService);
        }

        [TestCase(true)]
        [TestCase(false)]
        public void ShouldDropDhtPresenceEventIfItDoesntMatchUserCertificate(bool spoof)
        {
            Character subject = UserFixture.CharacterNamed("player 1");
            Character sender = spoof ? UserFixture.CharacterNamed("nefarious ned") : subject;

            var envelope = MockRepository.GenerateStub<ITransportEnvelope>();
            envelope.Stub(e => e.Source).Return(PeerAddress.GetLoopback(123));
            envelope.Stub(e => e.Certificate).Return(DefaultUserCertificate.Create(1, sender, DateTime.Now, new PrivateKey()));
            this.protocol.SetEnvelope(envelope);

            ChatStatusNotification notification = new ChatStatusNotification(ChatStatus.Away, ChatNotificationType.Default);

            this.protocol.TriggerPresenceEvent(123, subject, notification);

            int expectedCalls = spoof ? 0 : 1;
            this.chatService.AssertWasCalled(
                (s) => s.ReceivePresence(null, null, ChatNotificationType.Default),
                (opts) => opts.IgnoreArguments().Repeat.Times(expectedCalls));
        }

        [TestCase(true)]
        [TestCase(false)]
        public void ShouldDropConnectionInvitationEventIfItDoesntMatchUserCertificate(bool spoof)
        {
            Character character = UserFixture.CharacterNamed("player 1");
            Character sender = spoof ? UserFixture.CharacterNamed("nefarious ned") : character;
            BadumnaId sessionId = this.idGenerator();

            var envelope = MockRepository.GenerateStub<ITransportEnvelope>();
            envelope.Stub(e => e.Source).Return(PeerAddress.GetLoopback(123));
            envelope.Stub(e => e.Certificate).Return(DefaultUserCertificate.Create(1, sender, DateTime.Now, new PrivateKey()));
            this.protocol.SetEnvelope(envelope);

            this.protocol.TriggerInvitationEvent(123, character, sessionId);

            int expectedCalls = spoof ? 0 : 1;
            this.chatService.AssertWasCalled(
                (s) => s.ReceiveInvitation(null, null, null),
                (opts) => opts.IgnoreArguments().Repeat.Times(expectedCalls));
        }

        private class PrivateChatProtocolStub : PrivateChatProtocol
        {
            private ITransportEnvelope currentEnvelope;

            public PrivateChatProtocolStub(ProtocolComponent<TransportEnvelope> protocolComponent, IPresenceService presenceService, IInternalChatService chatService)
                : base(protocolComponent, presenceService, chatService)
            {
            }

            protected override ITransportEnvelope CurrentEnvelope
            {
                get { return this.currentEnvelope; }
            }

            public void SetEnvelope(ITransportEnvelope value)
            {
                this.currentEnvelope = value;
            }

            public void TriggerPresenceEvent(ushort localId, Character character, ChatStatusNotification notification)
            {
                this.PresenceEvent(localId, character, notification);
            }

            public void TriggerInvitationEvent(ushort localId, Character character, BadumnaId subject)
            {
                this.InvitationEvent(localId, character, subject);
            }
        }
    }
}
