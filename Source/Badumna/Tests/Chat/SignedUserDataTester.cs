﻿//-----------------------------------------------------------------------
// <copyright file="SignedUserDataTester.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2012 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BadumnaTests.Chat
{
    using System;
    using System.Collections.Generic;
    using System.Security.Cryptography;
    using Badumna.Chat;
    using Badumna.Core;
    using Badumna.DataTypes;
    using Badumna.Security;
    using NUnit.Framework;
    using Rhino.Mocks;

    [TestFixture]
    [DontPerformCoverage]
    public class SignedUserDataTester : BadumnaTests.Core.ParseableTestHelper
    {
        private DateTime now;
        private ICertificateToken token;
        private ITokenCache tokenCache;
        private TestValue data;
        private TestSignedData signedData;
        private long userId;

        private RSACryptoServiceProvider cryptoProvider;
        private PrivateKey privateKey;
        private PublicKey publicKey;
        private Character character;

        public SignedUserDataTester()
            : base(new ParseableTester())
        {
        }

        [SetUp]
        public void SetUp()
        {
            this.userId = 123;
            this.character = new Character(11, "player1");
            this.data = new TestValue(this.character);
            this.cryptoProvider = new RSACryptoServiceProvider();
            this.privateKey = new PrivateKey(this.cryptoProvider);
            this.publicKey = this.privateKey.PublicKey;

            this.now = new DateTime();
            this.tokenCache = MockRepository.GenerateStub<ITokenCache>();
            this.token = MockRepository.GenerateStub<ICertificateToken>();
            this.token.Stub(t => t.PrivateKey).Return(this.privateKey);
            this.token.Stub(t => t.PublicKey).Return(this.publicKey);
        }

        [Test]
        public void PrivateKeyIsNotSerialized()
        {
            var cryptoProvider = new RSACryptoServiceProvider();
            var token = new CertificateToken(this.userId, this.character, this.now, this.now.AddDays(1), new byte[PublicKey.SignatureLengthBytes], cryptoProvider);

            var status = new TestSignedData(this.data, token);
            var buffer = new MessageBuffer();
            buffer.Write(status);

            buffer.ReadOffset = 0;
            var serializedStatus = buffer.Read<TestSignedData>();
            Assert.AreEqual(status, serializedStatus);

            // original status should have a private key, but not the deserialised one:
            Assert.IsNotNull(status.UserCertificate.PrivateKey);
            Assert.IsNull(serializedStatus.UserCertificate.PrivateKey);
        }

        [Test]
        public void ValidateReturnsTrueWhenSignatureIsValidAndSigned()
        {
            var trustedPrivateKey = new PrivateKey(new RSACryptoServiceProvider());
            var trustedAuthority = new TrustedAuthorityToken(this.now.AddDays(1), trustedPrivateKey.PublicKey.RawData);

            var tomorrow = this.now.AddDays(1);
            var certificateSignature = CertificateToken.SignCertificate(this.userId, this.character, this.now, tomorrow, this.publicKey.RawData, trustedPrivateKey.RawData);
            var token = new CertificateToken(this.userId, this.character, this.now, tomorrow, certificateSignature, this.cryptoProvider);
            this.tokenCache.Stub(c => c.GetTrustedAuthorityToken()).Return(trustedAuthority);

            var status = new TestSignedData(this.data, token);

            Assert.IsTrue(status.Validate(this.tokenCache));
        }

        [Test]
        public void EnsureStubValidScenarioWorks()
        {
            // If this test fails, it means the later tests in this class are
            // invalid because this.StubValidScenario() is not doing what it's supposed to
            this.StubValidScenario();
            Assert.IsTrue(this.signedData.Validate(this.tokenCache));
        }

        [Test]
        public void ValidateReturnsFalseWhenTokenIsInvalid()
        {
            this.StubValidScenario();
            this.token.Stub(t => t.Validate(this.tokenCache)).Return(false).Repeat.Any();
            Assert.IsFalse(this.signedData.Validate(this.tokenCache));
        }

        [Test]
        public void ValidateReturnsFalseWhenSignatureDoesNotMatch()
        {
            this.StubValidScenario();
            this.token.Stub(t => t.PublicKey).Return(PublicKey.GenerateKey()).Repeat.Any();
            Assert.IsFalse(this.signedData.Validate(this.tokenCache));
        }

        [Test]
        public void ValidateReturnsFalseWhenSignatureIsNotFromTheClaimedCharacter()
        {
            this.StubValidScenario();
            this.token.Stub(t => t.Character).Return(new Character(this.character.Id + 1, this.character.Name)).Repeat.Any();
            Assert.IsFalse(this.signedData.Validate(this.tokenCache));
        }

        // for most of these tests, we want to start with a known-good scenario and modify it
        // minimally to trigger each failure case
        //
        // Note that you need to use `obj.Stub( ... ).Repeat.Any()` to override an existing stubbed behaviour
        private void StubValidScenario()
        {
            this.signedData = new TestSignedData(this.data, this.token);
            this.token.Stub(t => t.Validate(this.tokenCache)).Return(true);
            this.token.Stub(t => t.Character).Return(this.character);
        }

        internal class ParseableTester : BadumnaTests.Core.ParseableTester<TestSignedData>
        {
            public override ICollection<TestSignedData> CreateExpectedValues()
            {
                DateTime now = new DateTime();

                ICollection<TestSignedData> expectedValues = new List<TestSignedData>();

                var status = new TestValue(UserFixture.CharacterNamed("player1"));
                expectedValues.Add(new TestSignedData(status, DefaultUserCertificate.Create(111, "player1", now, new PrivateKey())));
                return expectedValues;
            }
        }

        // ----------------------------------------------------------
        // Concrete subclasses of the generic class under test:
        // ----------------------------------------------------------
        internal class TestValue : IOwnedByCharacter, IEquatable<TestValue>, IParseable
        {
            public TestValue(Character character)
            {
                this.Character = character;
            }

            public TestValue()
            {
            }

            public Character Character { get; set; }

            public bool Equals(TestValue other)
            {
                return other.Character == this.Character;
            }

            void IParseable.ToMessage(MessageBuffer message, Type parameterType)
            {
                message.Write(Character);
            }

            void IParseable.FromMessage(MessageBuffer message)
            {
                this.Character = message.Read<Character>();
            }
        }

        internal class TestSignedData : SignedUserData<TestValue>
        {
            public TestSignedData(TestValue value, ICertificateToken userCertificate)
                : base(value, userCertificate)
            {
            }

            public TestSignedData()
                : base()
            {
            }

            protected override Type ValueType
            {
                get { return typeof(TestValue); }
            }
        }
    }
}
