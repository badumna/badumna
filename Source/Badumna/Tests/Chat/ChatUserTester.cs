﻿//-----------------------------------------------------------------------
// <copyright file="ChatUserTester.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2012 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System.Collections.Generic;
using Badumna.Chat;
using Badumna.Core;
using Badumna.DataTypes;

namespace BadumnaTests.Chat
{
    public class ChatUserTester : BadumnaTests.Core.ParseableTestHelper
    {
        public ChatUserTester()
            : base(new ParseableTester())
        {
        }

        internal class ParseableTester : BadumnaTests.Core.ParseableTester<ChatUser>
        {
            public override ICollection<ChatUser> CreateExpectedValues()
            {
                ICollection<ChatUser> expectedValues = new List<ChatUser>();

                expectedValues.Add(new ChatUser(
                    new BadumnaId(PeerAddress.GetLoopback(1), 1),
                    UserFixture.CharacterNamed("bob")));

                expectedValues.Add(new ChatUser(
                    new BadumnaId(PeerAddress.GetLoopback(4), 256),
                    UserFixture.CharacterNamed("joe")));

                return expectedValues;
            }
        }
    }
}
