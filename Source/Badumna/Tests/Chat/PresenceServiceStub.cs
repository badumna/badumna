﻿//---------------------------------------------------------------------------------
// <copyright file="PresenceServiceStub.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

using System.Collections.Generic;
using Badumna.Chat;
using Badumna.Core;
using Badumna.DataTypes;
using Badumna.DistributedHashTable;
using Badumna.Security;
using Badumna.Utilities;
using BadumnaTests.Utilities;

namespace BadumnaTests.Chat
{
    [DontPerformCoverage]
    internal class PresenceServiceStub : PresenceService
    {
        private int handleChangePresenceCount;
        private int handleForwarderSubscriptionRequestCount;
        private List<Pair<SignedUserPresenceStatus, BadumnaId>> sentPresenceEvents;

        public PresenceServiceStub(DhtFacade facade, NetworkEventQueue eventQueue, ITime timeKeeper, INetworkConnectivityReporter connectivityReporter, ITokenCache tokenCache)
            : base(facade, eventQueue, timeKeeper, connectivityReporter, tokenCache, BadumnaIdGenerator.Local())
        {
            this.sentPresenceEvents = new List<Pair<SignedUserPresenceStatus, BadumnaId>>();
        }

        public int HandleChangePresenceCount
        {
            get { return this.handleChangePresenceCount; }
        }

        public int HandleForwarderSubscriptionRequestCount
        {
            get { return this.handleForwarderSubscriptionRequestCount; }
        }

        public List<Pair<SignedUserPresenceStatus, BadumnaId>> SentPresenceEvents
        {
            get { return this.sentPresenceEvents; }
        }

        public void TriggerStatusUpdate(SignedUserPresenceStatus status)
        {
            this.HandleChangePresence(status);
        }

        public void TriggerReceivePresence(SignedUserPresenceStatus status)
        {
            this.ReceivePresenceEvent(status);
        }

        public void TriggerSubscriptionEvent(SignedChatUser source, BadumnaId subject)
        {
            this.ReceiveSubscriptionEvent(source, subject);
        }

        protected override void SendPresenceUpdate(BadumnaId destination, SignedUserPresenceStatus status)
        {
            var pair = new Pair<SignedUserPresenceStatus, BadumnaId>(status, destination);
            this.sentPresenceEvents.Add(pair);
            base.SendPresenceUpdate(destination, status);
        }

        protected override void HandleChangePresence(SignedUserPresenceStatus status)
        {
            this.handleChangePresenceCount++;
            base.HandleChangePresence(status);
        }

        protected override void HandleForwarderSubscriptionRequest(
            SignedChatUser sourceUser, 
            string destinationUsername)
        {
            this.handleForwarderSubscriptionRequestCount++;
            base.HandleForwarderSubscriptionRequest(sourceUser, destinationUsername);
        }
    }
}
