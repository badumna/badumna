﻿//-----------------------------------------------------------------------
// <copyright file="InterestedEntityTester.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using Badumna.Core;
using Badumna.DataTypes;
using Badumna.DistributedHashTable;
using Badumna.Replication;
using NUnit.Framework;

namespace BadumnaTests.Replication
{
    [TestFixture]
    [DontPerformCoverage]
    public class InterestedEntityTester
    {
        private InterestedEntity entity1;
        private InterestedEntity entity2;
        private BadumnaId nextId;

        private ITime timeKeeper;

        [SetUp]
        public void Initialize()
        {
            this.timeKeeper = new NetworkEventQueue();
            this.entity1 = new InterestedEntity(new BadumnaId(new PeerAddress(HashKey.Hash("test1")), 1), this.timeKeeper);
            this.entity2 = new InterestedEntity(new BadumnaId(new PeerAddress(HashKey.Hash("test2")), 2), this.timeKeeper);
            this.nextId = new BadumnaId(new PeerAddress(HashKey.Hash("test1")), 3);
        }

        [TearDown]
        public void Terminate()
        {
        }

        [Test]
        public void DefaultUpdateMethodIsDirect()
        {
            InterestedEntity entity = new InterestedEntity(this.nextId, this.timeKeeper);
            Assert.AreEqual(UpdateDeliveryMethod.Direct, entity.UpdateMethod);
        }

        [Test]
        public void CorrectSwitchingOrderThrowsNoException()
        {
            InterestedEntity entity = new InterestedEntity(this.nextId, this.timeKeeper);
            Assert.AreEqual(UpdateDeliveryMethod.Direct, entity.UpdateMethod);

            entity.SwitchUpdateMethod(UpdateDeliveryMethod.Overload);
            entity.SwitchUpdateMethod(UpdateDeliveryMethod.SwitchingBack);
            entity.SwitchUpdateMethod(UpdateDeliveryMethod.Direct);
        }

        [Test]
        [ExpectedException("System.InvalidOperationException")]
        public void IncorrectSwitchingOrderThrowInvaldOperationExceptionTest()
        {
            InterestedEntity entity = new InterestedEntity(this.nextId, this.timeKeeper);
            Assert.AreEqual(UpdateDeliveryMethod.Direct, entity.UpdateMethod);

            entity.SwitchUpdateMethod(UpdateDeliveryMethod.SwitchingBack);
        }

        [Test]
        public void EqualTestsReturnCorrectResults()
        {
            Assert.IsFalse(this.entity1.Equals(this.entity2));
            Assert.IsFalse(this.entity2.Equals(this.entity1));
            Assert.IsTrue(this.entity1.Equals(this.entity1));
            Assert.IsFalse(this.entity1.Equals(new object()));
        }
    }
}
