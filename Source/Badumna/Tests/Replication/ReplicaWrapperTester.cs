﻿//-----------------------------------------------------------------------
// <copyright file="ReplicaWrapperTester.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.IO;

using Badumna.Core;
using Badumna.DataTypes;
using Badumna.Replication;
using Badumna.Utilities;
using NUnit.Framework;
using Rhino.Mocks;

namespace BadumnaTests.Replication
{
    [TestFixture]
    [DontPerformCoverage]
    public class ReplicaWrapperTester
    {
        private IUpdateAckEventHandler mockedAckHandler;
        private IEntityManager mockedEntityManager;
        private IReplica mockedReplica;
        private RemoveReplica mockedRemoveReplicaDelegate;
        private EntityTypeId typeId;

        private Simulator eventQueue;
        private ITime timeKeeper;
        private INetworkConnectivityReporter connectivityReporter;

        [SetUp]
        public void Initialize()
        {
            this.eventQueue = new Simulator();
            this.timeKeeper = this.eventQueue;
            this.Reset();
        }

        [Test]
        public void ConstructorThrowsNoException()
        {
            ReplicaWrapper wrapper = this.GetReplicaWrapper();
            ReplicaWrapper wrapper2 = new ReplicaWrapper(
                this.mockedEntityManager,
                this.mockedReplica,
                this.typeId,
                this.mockedRemoveReplicaDelegate,
                this.eventQueue,
                this.timeKeeper,
                this.connectivityReporter);
        }

        [Test]
        public void WrapperIsNotExpiredByDefault()
        {
            ReplicaWrapper wrapper = this.GetReplicaWrapper();
            Assert.IsFalse(wrapper.IsExpired);
        }

        [Test]
        public void WrapperWillBecomeExpiredWithoutUpdate()
        {
            ReplicaWrapper wrapper = this.GetReplicaWrapper();
            Assert.IsFalse(wrapper.IsExpired);
            this.eventQueue.RunFor(Parameters.ReplicaTimeToLive);
            Assert.IsFalse(wrapper.IsExpired);
            this.eventQueue.RunFor(TimeSpan.FromSeconds(1));
            Assert.IsTrue(wrapper.IsExpired);
        }

        [Test]
        public void ApplyUpdateUpdatesTheExpireTime()
        {
            ReplicaWrapper wrapper = this.GetReplicaWrapper();
            this.TestApplyUpdateUpdatesTheExpireTime(wrapper);
            this.mockedAckHandler.VerifyAllExpectations();
            this.mockedReplica.VerifyAllExpectations();
        }

        [Test]
        public void ReplicaKeepGettingUpdatedWillNotBecomeExpired()
        {
            ReplicaWrapper wrapper = this.GetReplicaWrapper();
            this.TestApplyUpdateUpdatesTheExpireTime(wrapper);

            for (int i = 0; i < 200; i++)
            {
                Assert.IsFalse(wrapper.IsExpired);
                this.TestApplyUpdateUpdatesTheExpireTime(wrapper);
                this.eventQueue.RunFor(TimeSpan.FromSeconds(3));
            }

            this.mockedAckHandler.VerifyAllExpectations();
            this.mockedReplica.VerifyAllExpectations();
        }

        [Test]
        public void AllQueuedMessagesWillBeProcessed()
        {
            ReplicaWrapper wrapper = this.GetReplicaWrapper();

            for (int i = 0; i < 17; i++)
            {
                CyclicalID.UShortID id = new CyclicalID.UShortID((ushort)i);
                byte[] data = new byte[10];
                wrapper.QueueCustomMessage(id, new MemoryStream(data));
            }

            this.mockedReplica.Expect(f => f.HandleEvent(null)).IgnoreArguments().Repeat.Times(17);
            wrapper.ProcessCustomMessages();
            this.mockedReplica.VerifyAllExpectations();
        }

        [Test]
        public void CallToSynchronizeWillBePassedToReplica()
        {
            ReplicaWrapper wrapper = this.GetReplicaWrapper();
            ((ISynchronizable)this.mockedReplica).Expect(f => f.Synchronize(null)).IgnoreArguments().Repeat.Once();
            wrapper.Synchronize(null);
            this.mockedReplica.VerifyAllExpectations();
        }

        [Test]
        public void ShutdownWillTriggerRemoveReplicaDelegate()
        {
            ReplicaWrapper wrapper = this.GetReplicaWrapper();
            this.mockedRemoveReplicaDelegate.Expect(f => f.Invoke(this.mockedReplica)).Repeat.Once();
            wrapper.Shutdown();
            this.mockedRemoveReplicaDelegate.VerifyAllExpectations();
        }

        [Test]
        public void GuidReturnsReplicasGuid()
        {
            ReplicaWrapper wrapper = this.GetReplicaWrapper();
            this.mockedReplica.Expect(f => f.Guid).Repeat.Once();
            BadumnaId id = wrapper.Guid;
            this.mockedReplica.VerifyAllExpectations();
        }

        private void TestApplyUpdateUpdatesTheExpireTime(ReplicaWrapper wrapper)
        {
            TimeSpan expireTime1 = wrapper.ExpireTime;
            this.eventQueue.RunFor(TimeSpan.FromSeconds(1));
            TimeSpan expireTime2 = wrapper.ExpireTime;

            Assert.AreEqual(expireTime1.TotalMilliseconds, expireTime2.TotalMilliseconds);

            CyclicalID.UShortID id = new CyclicalID.UShortID(1);
            this.mockedReplica.Expect(f => f.Deserialize(null, null, 1, null)).IgnoreArguments().Repeat.Once().Return(1);
            this.mockedAckHandler.Expect(f => f.UpdateDelayedAckTimes(id, null)).IgnoreArguments().Repeat.Once();

            wrapper.ApplyUpdate(id, null, new byte[1], 1, null);

            TimeSpan expireTime3 = wrapper.ExpireTime;
            Assert.AreNotEqual(expireTime1.TotalMilliseconds, expireTime3.TotalMilliseconds);
            Assert.IsTrue(expireTime3.TotalMilliseconds > expireTime1.TotalMilliseconds);
        }

        private ReplicaWrapper GetReplicaWrapper()
        {
            this.Reset();
            ReplicaWrapper wrapper = new ReplicaWrapper(
                this.mockedEntityManager,
                this.mockedReplica,
                this.mockedAckHandler,
                this.typeId,
                this.mockedRemoveReplicaDelegate,
                this.eventQueue,
                this.timeKeeper,
                this.connectivityReporter);
            
            return wrapper;
        }

        private void Reset()
        {
            this.typeId = new EntityTypeId((byte)EntityGroup.Spatial, 0);
            this.mockedAckHandler = MockRepository.GenerateMock<IUpdateAckEventHandler>();
            this.mockedEntityManager = MockRepository.GenerateMock<IEntityManager>();
            this.mockedReplica = MockRepository.GenerateMock<IReplica, ISynchronizable>();
            this.mockedRemoveReplicaDelegate = MockRepository.GenerateMock<RemoveReplica>();
            this.connectivityReporter = MockRepository.GenerateMock<INetworkConnectivityReporter>();
        }
    }
}
