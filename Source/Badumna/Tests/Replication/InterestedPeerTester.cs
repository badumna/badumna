﻿//-----------------------------------------------------------------------
// <copyright file="InterestedPeerTester.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using Badumna.Core;
using Badumna.DataTypes;
using Badumna.DistributedHashTable;
using Badumna.Replication;
using Badumna.Utilities;
using NUnit.Framework;

namespace BadumnaTests.Replication
{
    [TestFixture]
    [DontPerformCoverage]
    public class InterestedPeerTester
    {
        private InterestedPeer interestedPeer;
    
        private BadumnaId entityId1 = new BadumnaId(new PeerAddress(HashKey.Hash("test1")), 1);
        private BadumnaId entityId2 = new BadumnaId(new PeerAddress(HashKey.Hash("test2")), 2);
        private BadumnaId entityId3 = new BadumnaId(new PeerAddress(HashKey.Hash("test3")), 3);

        private Simulator eventQueue;
        private ITime timeKeeper;

        [SetUp]
        public void Initialize()
        {
            this.eventQueue = new Simulator();
            this.timeKeeper = this.eventQueue;
        }

        [Test]
        public void SimpleConstructorThrowsNoException()
        {
            this.interestedPeer = new InterestedPeer(this.timeKeeper);
            Assert.AreEqual(0, this.interestedPeer.Count);
        }

        [Test]
        public void AddCanSuccessfullyAddEntities()
        {
            this.interestedPeer = new InterestedPeer(this.timeKeeper);
            Assert.AreEqual(0, this.interestedPeer.Count);
            this.interestedPeer.Add(this.entityId1);
            Assert.AreEqual(1, this.interestedPeer.Count);
            this.interestedPeer.Add(this.entityId1);
            Assert.AreEqual(1, this.interestedPeer.Count);
        }

        [Test]
        public void RemoveCanSuccessfullyRemoveEntities()
        {
            this.interestedPeer = new InterestedPeer(this.timeKeeper);
            Assert.AreEqual(0, this.interestedPeer.Count);
            this.interestedPeer.Add(this.entityId1);
            Assert.AreEqual(1, this.interestedPeer.Count);
            this.interestedPeer.Add(this.entityId2);
            Assert.AreEqual(2, this.interestedPeer.Count);
            this.interestedPeer.Remove(this.entityId2);
            Assert.AreEqual(1, this.interestedPeer.Count);
            this.interestedPeer.Remove(this.entityId2);
            Assert.AreEqual(1, this.interestedPeer.Count);
            this.interestedPeer.Remove(this.entityId1);
            Assert.AreEqual(0, this.interestedPeer.Count);
        }

        [Test]
        public void ContainsReturnCorrectResults()
        {
            this.interestedPeer = new InterestedPeer(this.timeKeeper);
            Assert.IsFalse(this.interestedPeer.Contains(this.entityId1));
            Assert.IsFalse(this.interestedPeer.Contains(this.entityId2));

            this.interestedPeer.Add(this.entityId1);
            this.interestedPeer.Add(this.entityId2);

            Assert.IsTrue(this.interestedPeer.Contains(this.entityId1));
            Assert.IsTrue(this.interestedPeer.Contains(this.entityId2));
            Assert.IsFalse(this.interestedPeer.Contains(this.entityId3));

            this.interestedPeer.Remove(this.entityId2);
            Assert.IsFalse(this.interestedPeer.Contains(this.entityId2));
        }

        [Test]
        public void GetEnumeratorReturnsAllEntities()
        {
            Dictionary<string, BadumnaId> entities = new Dictionary<string, BadumnaId>();
            this.interestedPeer = new InterestedPeer(this.timeKeeper);
            this.interestedPeer.Add(this.entityId1);
            this.interestedPeer.Add(this.entityId2);
            this.interestedPeer.Add(this.entityId3);

            Assert.AreEqual(3, this.interestedPeer.Count);

            foreach (InterestedEntity entity in this.interestedPeer)
            {
                if (!entities.ContainsKey(entity.EntityId.ToString()))
                {
                    entities.Add(entity.EntityId.ToString(), entity.EntityId);
                }
            }

            Assert.AreEqual(3, entities.Count);
        }

        [Test]
        public void PeerThatDoesNotSendFlowControlMsgWillBeConsideredAsTimeout()
        {
            this.interestedPeer = new InterestedPeer(this.timeKeeper);
            this.interestedPeer.ApplyFlowControlFeedback(128, 0.5f);

            Assert.IsFalse(this.interestedPeer.TimedOut);
            this.eventQueue.RunFor(Parameters.FlowControlTimeout + TimeSpan.FromSeconds(2));
            Assert.IsTrue(this.interestedPeer.TimedOut);
        }

        [Test]
        public void PeerThatSendsFlowControlMsgsWillNeverBeConsideredAsTimeout()
        {
            this.interestedPeer = new InterestedPeer(this.timeKeeper);
            this.interestedPeer.ApplyFlowControlFeedback(128, 0.5f);

            for (int i = 0; i < 10; ++i)
            {
                Assert.IsFalse(this.interestedPeer.TimedOut);
                this.eventQueue.RunFor(Parameters.FlowControlTimeout - TimeSpan.FromSeconds(2));
                this.interestedPeer.ApplyFlowControlFeedback(128, 0.5f);
                Assert.IsFalse(this.interestedPeer.TimedOut);
            }
        }
    }
}
