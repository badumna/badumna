﻿//-----------------------------------------------------------------------
// <copyright file="EntityTypeIdTester.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Badumna.Core;
using Badumna.Replication;
using BadumnaTests.Core;
using NUnit.Framework;

namespace BadumnaTests.Replication
{
    [TestFixture]
    [DontPerformCoverage]
    public class EntityTypeIdTester : ParseableTestHelper
    {
        public EntityTypeIdTester()
            : base(new ParseableTester())
        {
        }

        internal class ParseableTester : ParseableTester<EntityTypeId>
        {
            public override ICollection<EntityTypeId> CreateExpectedValues()
            {
                ICollection<EntityTypeId> expectedValues = new List<EntityTypeId>();
                EntityTypeId id1 = new EntityTypeId((byte)EntityGroup.Spatial, 0x01);
                expectedValues.Add(id1);
                return expectedValues;
            }

            public override void AssertAreEqual(EntityTypeId expected, EntityTypeId actual)
            {
                Assert.AreEqual(expected.Group, actual.Group);
                Assert.AreEqual(expected.Id, actual.Id);
            }
        }
    }
}
