﻿//-----------------------------------------------------------------------
// <copyright file="InterestedPeerListTester.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Xml;
using Badumna;
using Badumna.Core;
using Badumna.DataTypes;
using Badumna.DistributedHashTable;
using Badumna.Replication;
using Badumna.Transport;
using Badumna.Utilities;
using NUnit.Framework;
using Rhino.Mocks;

namespace BadumnaTests.Replication
{
    [TestFixture]
    [DontPerformCoverage]
    public class InterestedPeerListTester
    {
        private IEntityManager entityManager;
        private InterestedPeerList interestedPeerList;
        private BadumnaId sourceEntityId;
        private Simulator eventQueue;
        private OverloadModule overloadOptions;

        [SetUp]
        public void SetUp()
        {
            this.eventQueue = new Simulator();
            this.ResetPeerList();
        }

        [Test]
        public void ConstructorThrowsNoException()
        {
            this.DisableOverload();
            this.ResetPeerList();
            this.EnableOverload();
            this.ResetPeerList();
        }

        [Test]
        public void ShutdownThrowsNoException()
        {
            this.DisableOverload();
            this.ResetPeerList();
            this.interestedPeerList.Shutdown();
        }

        [Test]
        public void DefaultInterestCountIsZero()
        {
            this.ResetPeerList();
            Assert.AreEqual(0, this.interestedPeerList.InterestedCount);
        }

        [Test]
        public void AddInterestedEntityTest()
        {
            BadumnaId entityId1 = new BadumnaId(new PeerAddress(HashKey.Hash("id1")), 1);
            BadumnaId entityId2 = new BadumnaId(new PeerAddress(HashKey.Hash("id2")), 2);

            this.ResetPeerList();
            this.interestedPeerList.AddInterestedEntity(entityId1);
            Assert.AreEqual(1, this.interestedPeerList.InterestedCount);

            this.interestedPeerList.AddInterestedEntity(entityId2);
            Assert.AreEqual(2, this.interestedPeerList.InterestedCount);

            this.interestedPeerList.AddInterestedEntity(entityId2);
            Assert.AreEqual(2, this.interestedPeerList.InterestedCount);
        }

        [Test]
        public void RemoveInterestedEntityTest()
        {
            BadumnaId entityId1 = new BadumnaId(new PeerAddress(HashKey.Hash("id1")), 1);
            BadumnaId entityId2 = new BadumnaId(new PeerAddress(HashKey.Hash("id2")), 2);

            this.ResetPeerList();
            this.interestedPeerList.AddInterestedEntity(entityId1);
            Assert.AreEqual(1, this.interestedPeerList.InterestedCount);

            this.interestedPeerList.AddInterestedEntity(entityId2);
            Assert.AreEqual(2, this.interestedPeerList.InterestedCount);

            this.interestedPeerList.RemoveInterestedEntity(entityId2);
            Assert.AreEqual(1, this.interestedPeerList.InterestedCount);
            this.interestedPeerList.RemoveInterestedEntity(entityId2);
            Assert.AreEqual(1, this.interestedPeerList.InterestedCount);

            this.interestedPeerList.AddInterestedEntity(entityId2);
            Assert.AreEqual(2, this.interestedPeerList.InterestedCount);
        }

        [Test]
        public void IsInterestedInTest()
        {
            BadumnaId entityId1 = new BadumnaId(new PeerAddress(HashKey.Hash("id1")), 1);
            BadumnaId entityId2 = new BadumnaId(new PeerAddress(HashKey.Hash("id2")), 2);

            this.ResetPeerList();
            this.interestedPeerList.AddInterestedEntity(entityId1);
            Assert.AreEqual(1, this.interestedPeerList.InterestedCount);
            Assert.IsTrue(this.interestedPeerList.IsInterestedIn(entityId1));

            this.interestedPeerList.AddInterestedEntity(entityId2);
            Assert.AreEqual(2, this.interestedPeerList.InterestedCount);
            Assert.IsTrue(this.interestedPeerList.IsInterestedIn(entityId2));

            this.interestedPeerList.RemoveInterestedEntity(entityId2);
            Assert.AreEqual(1, this.interestedPeerList.InterestedCount);
            Assert.IsFalse(this.interestedPeerList.IsInterestedIn(entityId2));
            this.interestedPeerList.RemoveInterestedEntity(entityId2);
            Assert.AreEqual(1, this.interestedPeerList.InterestedCount);
            Assert.IsFalse(this.interestedPeerList.IsInterestedIn(entityId2));

            this.interestedPeerList.AddInterestedEntity(entityId2);
            Assert.AreEqual(2, this.interestedPeerList.InterestedCount);
            Assert.IsTrue(this.interestedPeerList.IsInterestedIn(entityId2));
        }

        [Test]
        public void InterestedRegionsReturnsCorrectResults()
        {
            BadumnaId entityId1 = new BadumnaId(new PeerAddress(HashKey.Hash("id1")), 1);
            BadumnaId entityId2 = new BadumnaId(new PeerAddress(HashKey.Hash("id2")), 2);
            BadumnaId entityId3 = new BadumnaId(new PeerAddress(HashKey.Hash("id3")), 3);

            this.ResetPeerList();
            this.interestedPeerList.AddInterestedEntity(entityId1);
            this.interestedPeerList.AddInterestedEntity(entityId2);
            this.interestedPeerList.AddInterestedEntity(entityId3);

            int count = 0;
            foreach (BadumnaId id in this.interestedPeerList.InterestedRegions)
            {
                count++;
            }

            Assert.AreEqual(3, count);
        }

        [Test]
        public void GetTimeOutPeersReturnsCorrectResults()
        {
            BadumnaId entityId1 = new BadumnaId(new PeerAddress(HashKey.Hash("id1")), 1);
            BadumnaId entityId2 = new BadumnaId(new PeerAddress(HashKey.Hash("id2")), 2);
            BadumnaId entityId3 = new BadumnaId(new PeerAddress(HashKey.Hash("id3")), 3);

            this.ResetPeerList();
            this.interestedPeerList.AddInterestedEntity(entityId1);
            this.interestedPeerList.AddInterestedEntity(entityId2);
            this.interestedPeerList.AddInterestedEntity(entityId3);

            List<Pair<BadumnaId, BadumnaId>> peers = this.interestedPeerList.GetTimeOutPeers();
            Assert.AreEqual(0, peers.Count);

            this.eventQueue.RunFor(Parameters.FlowControlTimeout);
            this.eventQueue.RunFor(TimeSpan.FromSeconds(1));
            peers = this.interestedPeerList.GetTimeOutPeers();
            Assert.AreEqual(3, peers.Count);

            this.ResetPeerList();
            this.interestedPeerList.AddInterestedEntity(entityId1);
            this.interestedPeerList.AddInterestedEntity(entityId2);
            this.interestedPeerList.AddInterestedEntity(entityId3);

            peers = this.interestedPeerList.GetTimeOutPeers();
            Assert.AreEqual(0, peers.Count);

            this.eventQueue.RunFor(Parameters.FlowControlTimeout);
            this.interestedPeerList.ApplyFlowControlFeedback(entityId1.Address, 128, 1.0f);
            this.eventQueue.RunFor(TimeSpan.FromSeconds(1));
            peers = this.interestedPeerList.GetTimeOutPeers();
            Assert.AreEqual(2, peers.Count);
        }

        private void ResetPeerList()
        {
            this.entityManager = MockRepository.GenerateMock<IEntityManager>();
            this.sourceEntityId = new BadumnaId(new PeerAddress(HashKey.Hash("test1")), 1);
            ITime timeKeeper = this.eventQueue;
            this.overloadOptions = new OverloadModule(null);
            IChannelFunnel<TransportEnvelope> connectionTable = MockRepository.GenerateMock<IChannelFunnel<TransportEnvelope>>();
            INetworkConnectivityReporter connectivityReporter = MockRepository.GenerateMock<INetworkConnectivityReporter>();
            this.interestedPeerList = new InterestedPeerList(this.entityManager, this.sourceEntityId, this.overloadOptions, this.eventQueue, timeKeeper, connectivityReporter, connectionTable);
        }

        private void EnableOverload()
        {
            XmlDocument document = new XmlDocument();
            string configString = "<Overload Enabled=\"true\"><EnableOverload>Enabled</EnableOverload></Overload>";
            document.LoadXml(configString);
            this.overloadOptions = new OverloadModule(document.FirstChild);
        }

        private void DisableOverload()
        {
            XmlDocument document = new XmlDocument();
            string configString = "<Overload Enabled=\"true\"><EnableOverload>Disabled</EnableOverload></Overload>";
            document.LoadXml(configString);
            this.overloadOptions = new OverloadModule(document.FirstChild);
        }
    }
}
