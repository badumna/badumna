﻿//-----------------------------------------------------------------------
// <copyright file="UpdateChannelTester.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using Badumna.Core;
using Badumna.DataTypes;
using Badumna.DistributedHashTable;
using Badumna.Replication;
using Badumna.Transport;
using Badumna.Utilities;
using NUnit.Framework;
using Rhino.Mocks;

namespace BadumnaTests.Replication
{
    [TestFixture]
    [DontPerformCoverage]
    public class UpdateChannelTester
    {
        private UpdateChannel channel;
        private IEntityRouter router;
        private BadumnaId destinationId;
        private Simulator eventQueue;
        private ITime timeKeeper;

        [SetUp]
        public void Initialize()
        {
            this.destinationId = new BadumnaId(new PeerAddress(HashKey.Hash("test1")), 1);
            this.router = MockRepository.GenerateMock<IEntityRouter>();
            this.eventQueue = new Simulator();
            this.timeKeeper = this.eventQueue;
        }

        [Test]
        public void ConstructorThrowsNoException()
        {
            this.channel = new UpdateChannel(this.destinationId.Address, this.router, this.eventQueue, this.timeKeeper);
        }

        [Test]
        public void DefaultDropRateIsZero()
        {
            this.channel = new UpdateChannel(this.destinationId.Address, this.router, this.eventQueue, this.timeKeeper);
            Assert.AreEqual(0.0, this.channel.DropRate);
        }

        [Test]
        public void PeekAndGetReturnsNullWhenEmpty()
        {
            this.channel = new UpdateChannel(this.destinationId.Address, this.router, this.eventQueue, this.timeKeeper);
            Assert.IsTrue(this.channel.IsEmpty);
            Assert.Null(this.channel.Get());
            Assert.Null(this.channel.Peek());
        }

        [Test]
        public void IsEmptyReturnsTrueWhenEmpty()
        {
            this.channel = new UpdateChannel(this.destinationId.Address, this.router, this.eventQueue, this.timeKeeper);
            Assert.IsTrue(this.channel.IsEmpty);
        }

        [Test]
        public void IsEmptyReturnsFalseWhenUpdateIsAvailable()
        {
            this.channel = new UpdateChannel(this.destinationId.Address, this.router, this.eventQueue, this.timeKeeper);
            this.channel.SetUnemptiedCallback(s => { });
            this.router.Stub(f => f.ToTransportEnvelope(null)).IgnoreArguments().Return(new TransportEnvelope());
            this.channel.NewUpdate(new EntityEnvelope(this.destinationId, QualityOfService.Unreliable));
            Assert.IsFalse(this.channel.IsEmpty);
        }

        [Test]
        public void IsEmptyReturnsTrueWhenUpdateIsAvailableNutTooEarlyToSend()
        {
            this.channel = new UpdateChannel(this.destinationId.Address, this.router, this.eventQueue, this.timeKeeper);
            this.channel.SetUnemptiedCallback(s => { });
            this.channel.ApplyFlowControlFeedback(32, 1.0f);
            this.eventQueue.RunFor(TimeSpan.FromMilliseconds(200));
            
            TransportEnvelope envelope = new TransportEnvelope();
            envelope.Message = new MessageBuffer(new byte[128]);
            this.router.Stub(f => f.ToTransportEnvelope(null)).IgnoreArguments().Return(envelope).Repeat.Twice();
            this.channel.NewUpdate(new EntityEnvelope(this.destinationId, QualityOfService.Unreliable));
            this.eventQueue.RunFor(TimeSpan.FromMilliseconds(200));
            this.channel.Get();

            this.channel.NewUpdate(new EntityEnvelope(this.destinationId, QualityOfService.Unreliable));
            Assert.IsTrue(this.channel.IsEmpty);
        }

        [Test]
        public void DropRateIsCalculatedCorrectlyWhenAllDropped()
        {
            this.channel = new UpdateChannel(this.destinationId.Address, this.router, this.eventQueue, this.timeKeeper);
            this.channel.SetUnemptiedCallback(s => { });
            Assert.AreEqual(0.0, this.channel.DropRate);

            for (int i = 0; i < 100; ++i)
            {
                this.router.Stub(f => f.ToTransportEnvelope(null)).IgnoreArguments().Return(new TransportEnvelope());
                this.channel.NewUpdate(new EntityEnvelope(this.destinationId, QualityOfService.Unreliable));
                this.eventQueue.RunFor(TimeSpan.FromMilliseconds(200));
            }

            Assert.AreEqual(1.0, this.channel.DropRate);
        }

        [Test]
        public void DropRateIsCalculatedCorrectlyWhenMostDropped()
        {
            this.channel = new UpdateChannel(this.destinationId.Address, this.router, this.eventQueue, this.timeKeeper);
            this.channel.SetUnemptiedCallback(s => { });
            Assert.AreEqual(0.0, this.channel.DropRate);

            for (int i = 0; i < 100; ++i)
            {
                this.router.Stub(f => f.ToTransportEnvelope(null)).IgnoreArguments().Return(new TransportEnvelope());
                this.channel.NewUpdate(new EntityEnvelope(this.destinationId, QualityOfService.Unreliable));
                this.eventQueue.RunFor(TimeSpan.FromMilliseconds(200));

                if (i % 20 == 0 && i != 0)
                {
                    this.channel.Get();
                }
            }

            double rate = this.channel.DropRate;
            Assert.IsTrue(rate > 0.945 && rate < 0.965);
        }

        [Test]
        public void DropRateIsCalculatedCorrectlyWhenHalfDropped()
        {
            this.channel = new UpdateChannel(this.destinationId.Address, this.router, this.eventQueue, this.timeKeeper);
            this.channel.SetUnemptiedCallback(s => { });
            Assert.AreEqual(0.0, this.channel.DropRate);

            for (int i = 0; i < 100; ++i)
            {
                this.router.Stub(f => f.ToTransportEnvelope(null)).IgnoreArguments().Return(new TransportEnvelope());
                this.channel.NewUpdate(new EntityEnvelope(this.destinationId, QualityOfService.Unreliable));
                this.eventQueue.RunFor(TimeSpan.FromMilliseconds(200));

                if (i % 2 == 0 && i != 0)
                {
                    this.channel.Get();
                }
            }

            double rate = this.channel.DropRate;
            Assert.IsTrue(rate > 0.48 && rate < 0.52);
        }

        [Test]
        public void DropRateIsCalculatedCorrectlyWhenFewDropped()
        {
            this.channel = new UpdateChannel(this.destinationId.Address, this.router, this.eventQueue, this.timeKeeper);
            this.channel.SetUnemptiedCallback(s => { });
            Assert.AreEqual(0.0, this.channel.DropRate);

            for (int i = 0; i < 100; ++i)
            {
                this.router.Stub(f => f.ToTransportEnvelope(null)).IgnoreArguments().Return(new TransportEnvelope());
                this.channel.NewUpdate(new EntityEnvelope(this.destinationId, QualityOfService.Unreliable));
                this.eventQueue.RunFor(TimeSpan.FromMilliseconds(200));

                if (i % 20 != 0)
                {
                    this.channel.Get();
                }
            }

            double rate = this.channel.DropRate;
            Assert.IsTrue(rate > 0.04 && rate < 0.06);
        }
    }
}
