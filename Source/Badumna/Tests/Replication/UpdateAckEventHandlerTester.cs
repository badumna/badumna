﻿//-----------------------------------------------------------------------
// <copyright file="UpdateAckEventHandlerTester.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using Badumna.Core;
using Badumna.DataTypes;
using Badumna.DistributedHashTable;
using Badumna.Replication;
using Badumna.Utilities;
using NUnit.Framework;
using Rhino.Mocks;

namespace BadumnaTests.Replication
{
    [TestFixture]
    [DontPerformCoverage]
    public class UpdateAckEventHandlerTester
    {
        private IEntityManager mockedEntityManager;
        private IEntityRouter mockedEntityRouter;
        private AssociatedReplica associatedReplica;

        private EntityEnvelope envelope;

        private BadumnaId replicaId;
        private BadumnaId originalId1;
        private BadumnaId originalId2;

        private Simulator eventQueue;
        private ITime timeKeeper;

        [SetUp]
        public void Initialize()
        {
            this.eventQueue = new Simulator();
            this.timeKeeper = this.eventQueue;
            this.Reset();
        }

        [Test]
        public void ConstructorThrowsNoException()
        {
            UpdateAckEventHandler handler = new UpdateAckEventHandler(this.mockedEntityManager, this.replicaId, this.eventQueue, this.timeKeeper);
        }

        [Test]
        public void AckIsDelayed()
        {
            UpdateAckEventHandler handler = new UpdateAckEventHandler(this.mockedEntityManager, this.replicaId, this.eventQueue, this.timeKeeper);
            this.mockedEntityManager.Stub(f => f.Router).Return(this.mockedEntityRouter);

            this.associatedReplica.AddAssociationRelationship(this.originalId1, this.replicaId);
            this.mockedEntityManager.Stub(f => f.Association).Return(this.associatedReplica);
            this.mockedEntityManager.Stub(f => f.GetMessageFor(null, QualityOfService.Unreliable))
                .IgnoreArguments()
                .Return(this.envelope);
            this.mockedEntityManager.Expect(f => f.AcknowledgeSegments(null, null, null))
                .IgnoreArguments()
                .Repeat.Never();
            this.mockedEntityRouter.Expect(f => f.DirectSend(null))
                .IgnoreArguments()
                .Repeat.Never();

            CyclicalID.UShortID seq = new CyclicalID.UShortID(100);
            handler.UpdateDelayedAckTimes(seq, new BooleanArray(true));
            this.eventQueue.RunFor(Parameters.SegmentAckDelayTime - TimeSpan.FromMilliseconds(1));
            this.mockedEntityRouter.VerifyAllExpectations();
            this.mockedEntityManager.VerifyAllExpectations();
        }

        [Test]
        public void DelayedAckWillBeSentOnTime()
        {
            this.AckTest(Parameters.SegmentAckDelayTime + TimeSpan.FromSeconds(1));
        }

        [Test]
        public void DelayedAckWillOnlyBeSentOnce()
        {
            TimeSpan timeToWait = TimeSpan.FromSeconds(Parameters.SegmentAckDelayTime.TotalSeconds * 5.0);
            this.AckTest(timeToWait);
        }

        [Test]
        public void MultipleSegmentsAreAckedInSingleRemoteCall()
        {
            UpdateAckEventHandler handler = new UpdateAckEventHandler(this.mockedEntityManager, this.replicaId, this.eventQueue, this.timeKeeper);
            this.mockedEntityManager.Stub(f => f.Router).Return(this.mockedEntityRouter);

            this.associatedReplica.AddAssociationRelationship(this.originalId1, this.replicaId);
            this.mockedEntityManager.Stub(f => f.Association).Return(this.associatedReplica);
            this.mockedEntityManager.Stub(f => f.GetMessageFor(null, QualityOfService.Unreliable)).IgnoreArguments().Return(this.envelope);
            this.mockedEntityManager.Expect(f => f.AcknowledgeSegments(null, null, null)).IgnoreArguments().Repeat.Any();
            this.mockedEntityRouter.Expect(f => f.DirectSend(null)).IgnoreArguments().Repeat.Once();

            BooleanArray array1 = new BooleanArray();
            BooleanArray array2 = new BooleanArray();

            array1[0] = true;
            array2[1] = true;

            CyclicalID.UShortID seq1 = new CyclicalID.UShortID(100);
            CyclicalID.UShortID seq2 = new CyclicalID.UShortID(101);

            handler.UpdateDelayedAckTimes(seq1, array1);
            handler.UpdateDelayedAckTimes(seq2, array2);
            this.eventQueue.RunFor(Parameters.SegmentAckDelayTime + TimeSpan.FromSeconds(1));
            this.mockedEntityRouter.VerifyAllExpectations();
            this.mockedEntityManager.VerifyAllExpectations();
        }

        private void AckTest(TimeSpan timeToWait)
        {
            UpdateAckEventHandler handler = new UpdateAckEventHandler(this.mockedEntityManager, this.replicaId, this.eventQueue, this.timeKeeper);
            this.mockedEntityManager.Stub(f => f.Router).Return(this.mockedEntityRouter);

            this.associatedReplica.AddAssociationRelationship(this.originalId1, this.replicaId);
            this.mockedEntityManager.Stub(f => f.Association).Return(this.associatedReplica);
            this.mockedEntityManager.Stub(f => f.GetMessageFor(null, QualityOfService.Unreliable)).IgnoreArguments().Return(this.envelope);
            this.mockedEntityManager.Expect(f => f.AcknowledgeSegments(null, null, null)).IgnoreArguments().Repeat.Any();
            this.mockedEntityRouter.Expect(f => f.DirectSend(null)).IgnoreArguments().Repeat.Once();

            CyclicalID.UShortID seq = new CyclicalID.UShortID(100);
            handler.UpdateDelayedAckTimes(seq, new BooleanArray(true));
            this.eventQueue.RunFor(timeToWait);
            this.mockedEntityRouter.VerifyAllExpectations();
            this.mockedEntityManager.VerifyAllExpectations();
        }

        private void Reset()
        {
            this.replicaId = new BadumnaId(new PeerAddress(HashKey.Hash("replica_id")), 1);
            this.originalId1 = new BadumnaId(new PeerAddress(HashKey.Hash("original_id1")), 2);
            this.originalId2 = new BadumnaId(new PeerAddress(HashKey.Hash("original_id2")), 3);

            this.mockedEntityRouter = MockRepository.GenerateMock<IEntityRouter>();
            this.mockedEntityManager = MockRepository.GenerateMock<IEntityManager>();
            this.associatedReplica = new AssociatedReplica();

            this.envelope = new EntityEnvelope();
        }
    }
}
