﻿//---------------------------------------------------------------------------------
// <copyright file="OverloadSwitchingManagerTester.cs" company="National ICT Australia Pty Ltd">
//     Copyright (c) 2010 National ICT Australia Pty Ltd. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using Badumna;
using Badumna.Core;
using Badumna.DataTypes;
using Badumna.DistributedHashTable;
using Badumna.Overload;
using Badumna.Replication;
using Badumna.Transport;
using Badumna.Utilities;
using BadumnaTests.Utilities;
using NUnit.Framework;
using Rhino.Mocks;

namespace BadumnaTests.Replication
{
    [TestFixture]
    [DontPerformCoverage]
    public class OverloadSwitchingManagerTester
    {
        private Dictionary<PeerAddress, InterestedPeer> interestedPeers;

        private UpdateChannelManager updateChannels;

        private List<InterestedEntity> overloadEntities;

        private IForwardingGroup mockedForwardingGroup;

        private IEntityManager mockedEntityManager;

        private IEntityRouter mockedEntityRouter1;
        private IEntityRouter mockedEntityRouter2;
        private IEntityRouter mockedEntityRouter3;

        private BadumnaId id1;
        private BadumnaId id2;
        private BadumnaId id3;
        private BadumnaId idNeverUsed;

        private PeerAddress peerAddress1;
        private PeerAddress peerAddress2;
        private PeerAddress peerAddress3;

        private UpdateChannel updateChannel1;
        private UpdateChannel updateChannel2;
        private UpdateChannel updateChannel3;

        private OverloadSwitchingManager switchingManager;
        private UpdateMethodManager updateMethodManager;

        private Simulator eventQueue;

        private GenericCallBackReturn<BadumnaId, PeerAddress> generateBadumnaId;

        [SetUp]
        public void Initialize()
        {
            this.eventQueue = new Simulator();
            ITime timeKeeper = this.eventQueue;

            this.overloadEntities = new List<InterestedEntity>();
            this.interestedPeers = new Dictionary<PeerAddress, InterestedPeer>();

            this.generateBadumnaId = BadumnaIdGenerator.FromAddress();

            this.id1 = this.generateBadumnaId(new PeerAddress(HashKey.Hash("test1")));
            this.id2 = this.generateBadumnaId(new PeerAddress(HashKey.Hash("test2")));
            this.id3 = this.generateBadumnaId(new PeerAddress(HashKey.Hash("test3")));
            this.idNeverUsed = this.generateBadumnaId(new PeerAddress(HashKey.Hash("test4")));

            this.peerAddress1 = PeerAddress.GetAddress("127.0.0.1", 1, NatType.Open);
            this.peerAddress2 = PeerAddress.GetAddress("127.0.0.1", 2, NatType.Open);
            this.peerAddress3 = PeerAddress.GetAddress("127.0.0.1", 3, NatType.Open);

            InterestedPeer peer1 = new InterestedPeer(timeKeeper);
            peer1.Add(this.generateBadumnaId(this.peerAddress1));
            peer1.Add(this.generateBadumnaId(this.peerAddress1));
            peer1.Add(this.generateBadumnaId(this.peerAddress1));
            this.interestedPeers.Add(this.peerAddress1, peer1);

            InterestedPeer peer2 = new InterestedPeer(timeKeeper);
            peer2.Add(this.generateBadumnaId(this.peerAddress2));
            peer2.Add(this.generateBadumnaId(this.peerAddress2));
            peer2.Add(this.generateBadumnaId(this.peerAddress2));
            this.interestedPeers.Add(this.peerAddress2, peer2);

            InterestedPeer peer3 = new InterestedPeer(timeKeeper);
            peer3.Add(this.generateBadumnaId(this.peerAddress3));
            peer3.Add(this.generateBadumnaId(this.peerAddress3));
            peer3.Add(this.generateBadumnaId(this.peerAddress3));
            this.interestedPeers.Add(this.peerAddress3, peer3);

            this.mockedEntityManager = MockRepository.GenerateMock<IEntityManager>();
            this.mockedForwardingGroup = MockRepository.GenerateMock<IForwardingGroup>();
            this.mockedEntityRouter1 = MockRepository.GenerateMock<IEntityRouter>();
            this.mockedEntityRouter2 = MockRepository.GenerateMock<IEntityRouter>();
            this.mockedEntityRouter3 = MockRepository.GenerateMock<IEntityRouter>();

            this.updateChannels = new UpdateChannelManager(this.mockedEntityManager, this.eventQueue, timeKeeper);
            this.updateChannel1 = new UpdateChannel(this.peerAddress1, this.mockedEntityRouter1, this.eventQueue, timeKeeper);
            this.updateChannel2 = new UpdateChannel(this.peerAddress2, this.mockedEntityRouter2, this.eventQueue, timeKeeper);
            this.updateChannel3 = new UpdateChannel(this.peerAddress3, this.mockedEntityRouter3, this.eventQueue, timeKeeper);

            this.updateChannels[this.peerAddress1] = this.updateChannel1;
            this.updateChannels[this.peerAddress2] = this.updateChannel2;
            this.updateChannels[this.peerAddress3] = this.updateChannel3;

            OverloadModule overloadOptions = new OverloadModule(null);

            IChannelFunnel<TransportEnvelope> connectionTable = MockRepository.GenerateMock<IChannelFunnel<TransportEnvelope>>();

            this.switchingManager = new OverloadSwitchingManager(this.interestedPeers, this.updateChannels, this.overloadEntities, overloadOptions, timeKeeper, connectionTable);
            this.updateMethodManager = new UpdateMethodManager(this.interestedPeers, this.updateChannels, this.overloadEntities, this.mockedForwardingGroup, overloadOptions);
        
            // Set unemptied callbacks to prevent assertions on updates
            this.updateChannel1.SetUnemptiedCallback(s => { });
            this.updateChannel2.SetUnemptiedCallback(s => { });
            this.updateChannel3.SetUnemptiedCallback(s => { });
        }

        [Test]
        public void UpdateChannelsAreNotOverloadedByDefault()
        {
            List<UpdateChannel> overloaded = this.switchingManager.GetOverloadedChannel();
            Assert.AreEqual(0, overloaded.Count);
        }

        [Test]
        public void OverloadedChannelsWillBeCorrectlyReported()
        {
            // 20% drop rate, not overloaded
            this.OverloadUpdateChannel(this.updateChannel1, this.mockedEntityRouter1, 2);
            this.OverloadUpdateChannel(this.updateChannel2, this.mockedEntityRouter2, 2);
            this.OverloadUpdateChannel(this.updateChannel3, this.mockedEntityRouter3, 7);
            
            List<UpdateChannel> overloaded = this.switchingManager.GetOverloadedChannel();
            Assert.AreEqual(2, overloaded.Count);
            Assert.IsTrue(overloaded.Contains(this.updateChannel1));
            Assert.IsTrue(overloaded.Contains(this.updateChannel2));
            Assert.IsFalse(overloaded.Contains(this.updateChannel3));
        }

        [Test]
        public void LoadOnOverloadedChannelsWillDropEventually()
        {
            this.OverloadedChannelsWillBeCorrectlyReported();

            this.OverloadUpdateChannel(this.updateChannel1, this.mockedEntityRouter1, 10);
            this.OverloadUpdateChannel(this.updateChannel2, this.mockedEntityRouter2, 10);
            this.OverloadUpdateChannel(this.updateChannel3, this.mockedEntityRouter3, 10);
            List<UpdateChannel> overloaded = this.switchingManager.GetOverloadedChannel();
            Assert.AreEqual(0, overloaded.Count);
        }

        [Test]
        public void UnderloadedChannelsWillBeCorrectlyReported()
        {
            // overload two channels (1 and 2)
            this.OverloadedChannelsWillBeCorrectlyReported();

            // switch to overloaded mode
            List<UpdateChannel> overloaded = this.switchingManager.GetOverloadedChannel();
            Assert.AreEqual(2, overloaded.Count);
            this.updateMethodManager.SwitchToOverloadMode(overloaded);

            // none of overloaded entities can change state
            List<InterestedEntity> changeToSwitchingBackMethod = null;
            List<InterestedEntity> changeToDirectMethod = null;
            this.switchingManager.GetUnderloaded(out changeToSwitchingBackMethod, out changeToDirectMethod);
            Assert.AreEqual(0, changeToSwitchingBackMethod.Count);
            Assert.AreEqual(0, changeToDirectMethod.Count);

            // can correctly switch to the switching back mode
            this.eventQueue.RunFor(TimeSpan.FromSeconds(1) + Parameters.OverloadModeMinDuration);
            this.switchingManager.GetUnderloaded(out changeToSwitchingBackMethod, out changeToDirectMethod);
            Assert.AreEqual(6, changeToSwitchingBackMethod.Count);
            Assert.AreEqual(0, changeToDirectMethod.Count);
            //// 6 wants to be set to switching back mode, only 3 will be allowed. 
            //// 3 will be kept as overloaded. 
            this.updateMethodManager.SwitchToSwitchingBackMethod(changeToSwitchingBackMethod);

            // can correctly switch to the direct mode
            this.eventQueue.RunFor(Parameters.SwitchingBackModeMaxDuration + TimeSpan.FromSeconds(1));
            this.switchingManager.GetUnderloaded(out changeToSwitchingBackMethod, out changeToDirectMethod);
            Assert.AreEqual(3, changeToSwitchingBackMethod.Count);
            Assert.AreEqual(3, changeToDirectMethod.Count);
        }

        [Test]
        public void OverOutboundTrafficLimitPeerWillGetOverloadedChannelsCorrectly()
        {
            ITime timeKeeper = this.eventQueue;

            OverloadModule overloadOptions = new OverloadModule(null);
            overloadOptions.OutboundTrafficLimitBytesPerSecond = 1000;

            IChannelFunnel<TransportEnvelope> connectionTable = MockRepository.GenerateMock<IChannelFunnel<TransportEnvelope>>();           

            this.switchingManager = new OverloadSwitchingManager(this.interestedPeers, this.updateChannels, this.overloadEntities, overloadOptions, timeKeeper, connectionTable);
            this.updateMethodManager = new UpdateMethodManager(this.interestedPeers, this.updateChannels, this.overloadEntities, this.mockedForwardingGroup, overloadOptions);
            
            this.GeneratingChannelTraffic(
               new UpdateChannel[] { this.updateChannel1, this.updateChannel2, this.updateChannel3 },
               new IEntityRouter[] { this.mockedEntityRouter1, this.mockedEntityRouter2, this.mockedEntityRouter3 });
            connectionTable.Stub(f => f.GetTotalOutboundTraffic()).Return(
                this.updateChannel1.GeneratingBytesPerSecond + this.updateChannel2.GeneratingBytesPerSecond + this.updateChannel3.GeneratingBytesPerSecond);
            
            List<UpdateChannel> overloaded = this.switchingManager.GetOverloadedChannel();
            Assert.AreEqual(2, overloaded.Count);
            Assert.IsTrue(overloaded.Contains(this.updateChannel1));
            Assert.IsTrue(overloaded.Contains(this.updateChannel2));
            Assert.IsFalse(overloaded.Contains(this.updateChannel3));
        }

        private void OverloadUpdateChannel(UpdateChannel channel, IEntityRouter mockedEntityRouter, int rate)
        {
            BadumnaId destinationId = this.generateBadumnaId(new PeerAddress(HashKey.Hash("test1")));
            
            for (int i = 0; i < 100; ++i)
            {
                mockedEntityRouter.Stub(f => f.ToTransportEnvelope(null)).IgnoreArguments().Return(new TransportEnvelope());
                channel.NewUpdate(new EntityEnvelope(destinationId, QualityOfService.Unreliable));
                this.eventQueue.RunFor(TimeSpan.FromMilliseconds(80));

                if (i % rate != 0)
                {
                    channel.Get();
                }
            }
        }

        private void GeneratingChannelTraffic(UpdateChannel[] channels, IEntityRouter[] mockedEntityRouters)
        {
            this.eventQueue.RunFor(TimeSpan.FromMilliseconds(6000));

            BadumnaId destinationId = this.generateBadumnaId(new PeerAddress(HashKey.Hash("test1")));

            for (int i = 0; i < 50; i++)
            {
                TransportEnvelope envelope = new TransportEnvelope();
                envelope.Message.Write(new byte[100], 100);

                for (int j = 0; j < channels.Length; j++)
                {
                    mockedEntityRouters[j].Stub(f => f.ToTransportEnvelope(null)).IgnoreArguments().Return(envelope);
                    channels[j].NewUpdate(new EntityEnvelope(destinationId, QualityOfService.Unreliable));
                }

                this.eventQueue.RunFor(TimeSpan.FromMilliseconds(80));

                foreach (UpdateChannel channel in channels)
                {
                    channel.Get();
                }
            }
        }
    }
}
