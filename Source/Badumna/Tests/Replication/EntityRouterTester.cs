﻿//-----------------------------------------------------------------------
// <copyright file="EntityRouterTester.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using Badumna.Core;
using Badumna.DataTypes;
using Badumna.DistributedHashTable;
using Badumna.Replication;
using Badumna.Transport;
using Badumna.Utilities;
using BadumnaTests.Core;
using NUnit.Framework;
using Rhino.Mocks;

namespace BadumnaTests.Replication
{
    internal delegate void DispatchMessageDelegate<EnvelopeType>(EnvelopeType messageEnvelope) where EnvelopeType : IEnvelope;

    [TestFixture]
    [DontPerformCoverage]
    public class EntityRouterTester
    {
        private IMessageConsumer<EntityEnvelope> consumer = null;
        private TransportProtocol transportProtocol;
        private EntityRouter router;

        private BadumnaId sourceEntityId;
        private BadumnaId destinationEntityId;

        [SetUp]
        public void SetUp()
        {
            this.sourceEntityId = new BadumnaId(new PeerAddress(HashKey.Hash("source")), 1);
            this.destinationEntityId = new BadumnaId(new PeerAddress(HashKey.Hash("destination")), 2);
            this.transportProtocol = MockRepository.GenerateMock<TransportProtocol>("foo", typeof(ConnectionfulProtocolMethodAttribute), new GenericCallBackReturn<object, Type>(MessageParserTester.DefaultFactory));
        }

        [Test]
        public void ConstructorThrowsNoException()
        {
            this.router = new EntityRouter(this.transportProtocol, this.consumer);
        }

        [Test]
        public void ToTransportEnvelopeThrowsNoExceptionWithValidInput()
        {
            this.router = new EntityRouter(this.transportProtocol, this.consumer);
            this.router.ForgeMethodList();
            EntityEnvelope envelope = new EntityEnvelope();
            envelope.SourceEntity = this.sourceEntityId;
            envelope.DestinationEntity = this.destinationEntityId;

            Assert.NotNull(this.router.ToTransportEnvelope(envelope));
        }

        [Test]
        [ExpectedException("System.ArgumentException")]
        public void ToTransportEnvelopeThrowsExceptionWithInvalidDestinationEntityInput()
        {
            this.router = new EntityRouter(this.transportProtocol, this.consumer);
            this.router.ForgeMethodList();
            EntityEnvelope envelope = new EntityEnvelope();
            envelope.SourceEntity = this.sourceEntityId;

            this.router.ToTransportEnvelope(envelope);
        }

        [Test]
        [ExpectedException("System.ArgumentException")]
        public void ToTransportEnvelopeThrowsExceptionWithInvalidSourceEntityInput()
        {
            this.router = new EntityRouter(this.transportProtocol, this.consumer);
            this.router.ForgeMethodList();
            EntityEnvelope envelope = new EntityEnvelope();
            envelope.DestinationEntity = this.destinationEntityId;

            this.router.ToTransportEnvelope(envelope);
        }
    }
}
