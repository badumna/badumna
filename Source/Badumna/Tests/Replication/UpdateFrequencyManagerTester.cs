﻿//-----------------------------------------------------------------------
// <copyright file="UpdateFrequencyManagerTester.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using Badumna.Core;
using Badumna.DistributedHashTable;
using Badumna.Replication;
using Badumna.SpatialEntities;
using Badumna.Utilities;
using NUnit.Framework;

namespace BadumnaTests.Replication
{
    [TestFixture]
    [DontPerformCoverage]
    public class UpdateFrequencyManagerTester
    {
        private UpdateFrequencyManager manager;

        private PeerAddress regularAddress;
        private PeerAddress dhtAddress;

        private Simulator eventQueue;

        [SetUp]
        public void SetUp()
        {
            this.eventQueue = new Simulator();
            this.regularAddress = new PeerAddress(new System.Net.IPAddress(new byte[] { 127, 0, 0, 1 }), 1, NatType.Unknown);
            this.dhtAddress = new PeerAddress(HashKey.Hash("dht_address"));
        }

        [Test]
        public void RegularRemoteEntityShouldAlwaysBeUpdated()
        {
            this.Reset();

            Assert.IsTrue(this.manager.ShouldUpdateReplica(this.regularAddress, null));
            Assert.IsTrue(this.manager.ShouldUpdateReplica(this.regularAddress, new Badumna.Utilities.BooleanArray()));
            Assert.IsTrue(this.manager.ShouldUpdateReplica(this.regularAddress, new Badumna.Utilities.BooleanArray(true)));
            Assert.IsTrue(this.manager.ShouldUpdateReplica(this.regularAddress, new Badumna.Utilities.BooleanArray(false)));
        }

        [Test]
        public void FirstUpdateIsAlwaysAllowed()
        {
            this.Reset();
            BooleanArray array = new BooleanArray();
            array[(int)SpatialEntityStateSegment.Position] = true;
            array[(int)SpatialEntityStateSegment.Velocity] = true;

            Assert.IsTrue(this.manager.ShouldUpdateReplica(this.dhtAddress, array));
        }

        [Test]
        public void ContinousUpdateIsNotRequiredForDhtEntity()
        {
            this.Reset();
            BooleanArray array = new BooleanArray();
            array[(int)SpatialEntityStateSegment.Position] = true;
            array[(int)SpatialEntityStateSegment.Velocity] = true;

            Assert.IsTrue(this.manager.ShouldUpdateReplica(this.dhtAddress, array));
            Assert.IsFalse(this.manager.ShouldUpdateReplica(this.dhtAddress, array));            
        }

        [Test]
        public void UpdateIntervalIsEnforcedForDhtEntity()
        {
            this.Reset();
            BooleanArray array = new BooleanArray();
            array[(int)SpatialEntityStateSegment.Position] = true;
            array[(int)SpatialEntityStateSegment.Velocity] = true;

            Assert.IsTrue(this.manager.ShouldUpdateReplica(this.dhtAddress, array));
            Assert.IsFalse(this.manager.ShouldUpdateReplica(this.dhtAddress, array));
            Assert.IsFalse(this.manager.ShouldUpdateReplica(this.dhtAddress, array));
            this.eventQueue.RunFor(TimeSpan.FromMilliseconds(2001));
            Assert.IsTrue(this.manager.ShouldUpdateReplica(this.dhtAddress, array));
            Assert.IsFalse(this.manager.ShouldUpdateReplica(this.dhtAddress, array));
            this.eventQueue.RunFor(TimeSpan.FromMilliseconds(2001));
            Assert.IsTrue(this.manager.ShouldUpdateReplica(this.dhtAddress, array));
            this.eventQueue.RunFor(TimeSpan.FromMilliseconds(1001));
            Assert.IsFalse(this.manager.ShouldUpdateReplica(this.dhtAddress, array));
        }

        private void Reset()
        {
            this.manager = new UpdateFrequencyManager(this.eventQueue);
        }
    }
}
