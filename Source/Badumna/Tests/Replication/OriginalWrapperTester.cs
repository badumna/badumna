﻿//-----------------------------------------------------------------------
// <copyright file="OriginalWrapperTester.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.IO;
using Badumna;
using Badumna.Core;
using Badumna.DataTypes;
using Badumna.DistributedHashTable;
using Badumna.Replication;
using Badumna.Transport;
using Badumna.Utilities;
using NUnit.Framework;
using Rhino.Mocks;

namespace BadumnaTests.Replication
{
    [TestFixture]
    [DontPerformCoverage]
    public class OriginalWrapperTester
    {
        private MockedOriginal original;
        private IEntityManager entityManager;
        private EntityTypeId entityTypeId;

        private Simulator eventQueue;
        private ITime timeKeeper;

        private IChannelFunnel<TransportEnvelope> connectionTable;
        private INetworkAddressProvider addressProvider;
        private INetworkConnectivityReporter connectivityReporter;

        private OverloadModule overloadOptions;

        [SetUp]
        public void Initialize()
        {
            this.original = new MockedOriginal(new BadumnaId(new PeerAddress(HashKey.Hash("mockedoriginalid")), 1));
            this.entityManager = MockRepository.GenerateMock<IEntityManager>();
            this.entityTypeId = new EntityTypeId((byte)EntityGroup.Spatial, 1);

            this.eventQueue = new Simulator();
            this.timeKeeper = this.eventQueue;

            this.overloadOptions = new OverloadModule(null);

            this.connectionTable = MockRepository.GenerateMock<IChannelFunnel<TransportEnvelope>>();

            this.addressProvider = MockRepository.GenerateMock<INetworkAddressProvider>();
            this.connectivityReporter = MockRepository.GenerateMock<INetworkConnectivityReporter>();
            this.connectivityReporter.Stub(x => x.Status).Return(ConnectivityStatus.Online);
        }

        [Test]
        public void ConstructorThrowsNoException()
        {
            OriginalWrapper wrapper = new OriginalWrapper(this.entityManager, this.original, this.entityTypeId, this.overloadOptions, this.eventQueue, this.timeKeeper, this.connectionTable, this.addressProvider, this.connectivityReporter);
        }

        [Test]
        public void AddInterestedEntityWillForceImmediateUpdate()
        {
            OriginalWrapper wrapper = new OriginalWrapper(this.entityManager, this.original, this.entityTypeId, this.overloadOptions, this.eventQueue, this.timeKeeper, this.connectionTable, this.addressProvider, this.connectivityReporter);
            Assert.IsFalse(wrapper.IsUpdateRequired);
            wrapper.AddInterested(new BadumnaId(PeerAddress.GetLoopback(1), 1));
            Assert.IsTrue(wrapper.IsUpdateRequired);
        }

        [Test]
        public void InterestedCountReportCorrectResult()
        {
            OriginalWrapper wrapper = new OriginalWrapper(this.entityManager, this.original, this.entityTypeId, this.overloadOptions, this.eventQueue, this.timeKeeper, this.connectionTable, this.addressProvider, this.connectivityReporter);
            Assert.AreEqual(0, wrapper.InterestedCount());
            wrapper.AddInterested(new BadumnaId(new PeerAddress(HashKey.Hash("test1")), 1));
            Assert.AreEqual(1, wrapper.InterestedCount());
            wrapper.AddInterested(new BadumnaId(new PeerAddress(HashKey.Hash("test2")), 2));
            Assert.AreEqual(2, wrapper.InterestedCount());
        }

        [Test]
        public void RemovedInterestedWorksCorrectly()
        {
            BadumnaId entityId = new BadumnaId(new PeerAddress(HashKey.Hash("test1")), 1);
            OriginalWrapper wrapper = new OriginalWrapper(this.entityManager, this.original, this.entityTypeId, this.overloadOptions, this.eventQueue, this.timeKeeper, this.connectionTable, this.addressProvider, this.connectivityReporter);
            Assert.AreEqual(0, wrapper.InterestedCount());
            wrapper.AddInterested(entityId);
            Assert.AreEqual(1, wrapper.InterestedCount());
            wrapper.RemoveInterested(new BadumnaId(entityId));
            Assert.AreEqual(0, wrapper.InterestedCount());
        }

        [Test]
        public void MarkForUpdateWillForceImmediateUpdate()
        {
            OriginalWrapper wrapper = new OriginalWrapper(this.entityManager, this.original, this.entityTypeId, this.overloadOptions, this.eventQueue, this.timeKeeper, this.connectionTable, this.addressProvider, this.connectivityReporter);
            Assert.IsFalse(wrapper.IsUpdateRequired);
            wrapper.MarkForUpdate(0);
            Assert.IsTrue(wrapper.IsUpdateRequired);

            wrapper = new OriginalWrapper(this.entityManager, this.original, this.entityTypeId, this.overloadOptions, this.eventQueue, this.timeKeeper, this.connectionTable, this.addressProvider, this.connectivityReporter);
            Assert.IsFalse(wrapper.IsUpdateRequired);
            wrapper.MarkForUpdate(new BooleanArray(true));
            Assert.IsTrue(wrapper.IsUpdateRequired);
        }

        [Test]
        public void ClearChangedStateWorksCorrectly()
        {
            OriginalWrapper wrapper = new OriginalWrapper(this.entityManager, this.original, this.entityTypeId, this.overloadOptions, this.eventQueue, this.timeKeeper, this.connectionTable, this.addressProvider, this.connectivityReporter);
            Assert.IsFalse(wrapper.IsUpdateRequired);
            wrapper.MarkForUpdate(0);
            Assert.IsTrue(wrapper.IsUpdateRequired);
            wrapper.ClearChangedState();
            Assert.IsFalse(wrapper.IsUpdateRequired);
        }

        [Test]
        public void MinUpdateRateIsEnforced()
        {
            OriginalWrapper wrapper = new OriginalWrapper(this.entityManager, this.original, this.entityTypeId, this.overloadOptions, this.eventQueue, this.timeKeeper, this.connectionTable, this.addressProvider, this.connectivityReporter);
            Assert.IsFalse(wrapper.IsUpdateRequired);
            this.eventQueue.RunFor(Parameters.MinimumUpdateRate);
            this.eventQueue.RunFor(TimeSpan.FromMilliseconds(100));
            Assert.IsTrue(wrapper.IsUpdateRequired);
        }

        [Test]
        public void CustomMessageNumberWillIncrease()
        {
            OriginalWrapper wrapper = new OriginalWrapper(this.entityManager, this.original, this.entityTypeId, this.overloadOptions, this.eventQueue, this.timeKeeper, this.connectionTable, this.addressProvider, this.connectivityReporter);
            CyclicalID.UShortID id1 = wrapper.GetNextCustomMessageNumber();
            CyclicalID.UShortID id2 = wrapper.GetNextCustomMessageNumber();
            CyclicalID.UShortID id3 = wrapper.GetNextCustomMessageNumber();

            Assert.IsTrue(id2 > id1);
            Assert.IsTrue(id3 > id2);
            Assert.IsTrue(id3 > id1);
        }

        [Test]
        public void CustomMessagesAreHandledCorrectly()
        {
            OriginalWrapper wrapper = new OriginalWrapper(this.entityManager, this.original, this.entityTypeId, this.overloadOptions, this.eventQueue, this.timeKeeper, this.connectionTable, this.addressProvider, this.connectivityReporter);
            Assert.AreEqual(0, this.original.HandledEvent);

            for (int i = 0; i < 10; i++)
            {
                wrapper.QueueCustomMessageRequest(new MemoryStream());
                Assert.AreEqual(0, this.original.HandledEvent);
            }

            wrapper.ProcessCustomMessageRequests();
            Assert.AreEqual(10, this.original.HandledEvent);
        }

        [Test]
        public void GetCompleteUpdateActuallyGetsCompleteUpdate()
        {
            OriginalWrapper wrapper = new OriginalWrapper(this.entityManager, this.original, this.entityTypeId, this.overloadOptions, this.eventQueue, this.timeKeeper, this.connectionTable, this.addressProvider, this.connectivityReporter);
            BooleanArray includedParts;
            BooleanArray remainingParts;
            int field1;
            int field2;

            byte[] data = wrapper.GetCompleteUpdate(out includedParts, out remainingParts);
            using (MemoryStream stream = new MemoryStream(data))
            {
                using (BinaryReader reader = new BinaryReader(stream))
                {
                    field1 = reader.ReadInt32();
                    field2 = reader.ReadInt32();
                }
            }

            Assert.AreEqual(this.original.Field1, field1);
            Assert.AreEqual(this.original.Field2, field2);
        }

        [Test]
        public void GetUpdateGetsCorrectUpdate()
        {
            OriginalWrapper wrapper = new OriginalWrapper(this.entityManager, this.original, this.entityTypeId, this.overloadOptions, this.eventQueue, this.timeKeeper, this.connectionTable, this.addressProvider, this.connectivityReporter);
            BooleanArray includedParts = new BooleanArray(false);
            BooleanArray remainingParts;
            includedParts[1] = true;
            int field2;

            byte[] data = wrapper.GetUpdate(ref includedParts, out remainingParts);
            using (MemoryStream stream = new MemoryStream(data))
            {
                using (BinaryReader reader = new BinaryReader(stream))
                {
                    field2 = reader.ReadInt32();
                }
            }

            Assert.AreEqual(this.original.Field2, field2);
        }
    }
}
