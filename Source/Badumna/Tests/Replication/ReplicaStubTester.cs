﻿//-----------------------------------------------------------------------
// <copyright file="ReplicaStubTester.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using Badumna.Core;
using Badumna.DataTypes;
using Badumna.DistributedHashTable;
using Badumna.Replication;

using NUnit.Framework;

namespace BadumnaTests.Replication
{
    [TestFixture]
    [DontPerformCoverage]
    public class ReplicaStubTester
    {
        [Test]
        public void SimpleConstructorDoesNotThrowException()
        {
            ReplicaStub stub = new ReplicaStub(new BadumnaId(new PeerAddress(HashKey.Hash("test1")), 1));
        }
    }
}
