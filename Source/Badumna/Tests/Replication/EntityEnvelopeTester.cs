﻿//-----------------------------------------------------------------------
// <copyright file="EntityEnvelopeTester.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using Badumna.Core;
using Badumna.Replication;
using BadumnaTests.Core;
using NUnit.Framework;

namespace BadumnaTests.Replication
{
    [TestFixture]
    [DontPerformCoverage]
    public class EntityEnvelopeTester : ParseableTestHelper
    {
        public EntityEnvelopeTester()
            : base(new ParseableTester())
        {
        }

        internal class ParseableTester : ParseableTester<EntityEnvelope>
        {
            public override ICollection<EntityEnvelope> CreateExpectedValues()
            {
                ICollection<EntityEnvelope> expectedValues = new List<EntityEnvelope>();
                EntityEnvelope emptyEnvelope = new EntityEnvelope();
                expectedValues.Add(emptyEnvelope);

                return expectedValues;
            }

            public override void AssertAreEqual(EntityEnvelope expected, EntityEnvelope actual)
            {
                Assert.IsTrue(expected.SourceQueuingDelay.TotalMilliseconds - actual.SourceQueuingDelay.TotalMilliseconds < 1.0);  // Value is serialized as number of milliseconds, truncating any fractional part.
                if (expected.Message != null)
                {
                    Assert.AreEqual(expected.Message.Length, actual.Message.Length);
                    Assert.AreEqual(expected.Message.GetBuffer(), actual.Message.GetBuffer());
                }
                else
                {
                    Assert.Null(actual.Message);
                }
            }
        }
    }
}
