﻿using System;
using System.Collections.Generic;
using System.Text;
using Badumna.Core;
using Badumna.Replication;
using Badumna.DataTypes;
using Badumna.Utilities;
using NUnit.Framework;
using Rhino.Mocks;

namespace BadumnaTests.Replication
{
    [TestFixture]
    [DontPerformCoverage]
    class AssociatedReplicaTestHelper : AssociatedReplica
    {
        public Dictionary<BadumnaId, Set<BadumnaId>> Association { get { return this.mAssociation; } }
    }

    [TestFixture]
    [DontPerformCoverage]
    public class AssociatedReplicaTester
    {
        private AssociatedReplicaTestHelper mAssociation = new AssociatedReplicaTestHelper();

        private BadumnaId original1;
        private BadumnaId original2;
        private BadumnaId replica1;
        private BadumnaId replica2;

        private BadumnaId replica3;
        private BadumnaId replica4;
        private BadumnaId replica5;
        private BadumnaId replica6;

        private BadumnaIdAllocator idAllocator;

        public AssociatedReplicaTester()
        {
            INetworkAddressProvider addressProvider = MockRepository.GenerateStub<INetworkAddressProvider>();
            addressProvider.Stub(x => x.PublicAddress).Return(PeerAddress.GetLoopback(1));
            INetworkConnectivityReporter connectivityReporter = MockRepository.GenerateStub<INetworkConnectivityReporter>();
            this.idAllocator = new BadumnaIdAllocator(addressProvider, connectivityReporter, new RandomNumberGenerator());

            this.original1 = idAllocator.GetNextUniqueId();
            this.original2 = idAllocator.GetNextUniqueId();
            
            this.replica1 = idAllocator.GetNextUniqueId();
            this.replica2 = idAllocator.GetNextUniqueId();
            this.replica3 = idAllocator.GetNextUniqueId();
            this.replica4 = idAllocator.GetNextUniqueId();
            this.replica5 = idAllocator.GetNextUniqueId();
            this.replica6 = idAllocator.GetNextUniqueId();
        }

        private void TestAddAssociation()
        {
            this.mAssociation.AddAssociationRelationship(original1, replica3);
            this.mAssociation.AddAssociationRelationship(original1, replica4);
            this.mAssociation.AddAssociationRelationship(original1, replica5);

            this.mAssociation.AddAssociationRelationship(original1, replica1);
            this.mAssociation.AddAssociationRelationship(original1, replica1);
            this.mAssociation.AddAssociationRelationship(original1, replica2);

            this.mAssociation.AddAssociationRelationship(original2, this.idAllocator.GetNextUniqueId());
            this.mAssociation.AddAssociationRelationship(original2, this.idAllocator.GetNextUniqueId());
            this.mAssociation.AddAssociationRelationship(original2, this.idAllocator.GetNextUniqueId());
            this.mAssociation.AddAssociationRelationship(original2, replica2);


            Assert.AreEqual(2, this.mAssociation.Association.Keys.Count);
            Assert.AreEqual(5, this.mAssociation.Association[original1].Count);
            Assert.AreEqual(4, this.mAssociation.Association[original2].Count);
        }

        [Test]
        public void TestAdd()
        {
            this.mAssociation.Clear();
            this.TestAddAssociation();
        }

        [Test]
        public void TestGetAssociatedReplicas()
        {
            this.mAssociation.Clear();
            this.TestAddAssociation();

            List<BadumnaId> list = this.mAssociation.GetAssociatedReplicas(this.idAllocator.GetNextUniqueId());
            Assert.IsNull(list);

            list = this.mAssociation.GetAssociatedReplicas(original1);
            Assert.AreEqual(5, list.Count);

            list = this.mAssociation.GetAssociatedReplicas(original2);
            Assert.AreEqual(4, list.Count);
        }

        [Test]
        public void TestGetAssociatedOriginals()
        {
            this.mAssociation.Clear();
            this.TestAddAssociation();

            List<BadumnaId> list = this.mAssociation.GetAssociatedOriginals(replica2);
            Assert.NotNull(list);
            Assert.AreEqual(2, list.Count);

            Assert.IsTrue(list.Contains(original1));
            Assert.IsTrue(list.Contains(original2));
        }

        [Test]
        public void TestRemoveFromAllAssociatedOriginals()
        {
            this.mAssociation.Clear();
            this.TestAddAssociation();

            this.mAssociation.RemoveFromAllAssociatedOriginals(replica2);
            List<BadumnaId> list = this.mAssociation.GetAssociatedOriginals(replica2);
            Assert.IsNull(list);

            Assert.AreEqual(2, this.mAssociation.Association.Keys.Count);
            Assert.AreEqual(4, this.mAssociation.Association[original1].Count);
            Assert.AreEqual(3, this.mAssociation.Association[original2].Count);
        }

        [Test]
        public void TestRemoveAssociatedReplicas()
        {
            this.mAssociation.Clear();
            this.TestAddAssociation();

            this.mAssociation.RemoveAssociatedReplicas(original1);
            Assert.AreEqual(1, this.mAssociation.Association.Keys.Count);
            Assert.IsFalse(this.mAssociation.Association.ContainsKey(original1));
            Assert.AreEqual(4, this.mAssociation.Association[original2].Count);
        }

        [Test]
        public void TestRemoveAssociatedReplicas2()
        {
            this.mAssociation.Clear();
            this.TestAddAssociation();

            this.mAssociation.RemoveAssociatedReplicas(original1, this.idAllocator.GetNextUniqueId());
            this.mAssociation.RemoveAssociatedReplicas(original2, this.idAllocator.GetNextUniqueId());

            // nothing should happen
            Assert.AreEqual(2, this.mAssociation.Association.Keys.Count);
            Assert.AreEqual(5, this.mAssociation.Association[original1].Count);
            Assert.AreEqual(4, this.mAssociation.Association[original2].Count);

            this.mAssociation.RemoveAssociatedReplicas(original1, replica1);
            this.mAssociation.RemoveAssociatedReplicas(original1, replica2);

            Assert.AreEqual(2, this.mAssociation.Association.Keys.Count);
            Assert.AreEqual(3, this.mAssociation.Association[original1].Count);
            Assert.AreEqual(4, this.mAssociation.Association[original2].Count);

            this.mAssociation.RemoveAssociatedReplicas(original1, replica3);
            this.mAssociation.RemoveAssociatedReplicas(original1, replica4);
            this.mAssociation.RemoveAssociatedReplicas(original1, replica5);

            Assert.AreEqual(1, this.mAssociation.Association.Keys.Count);
            Assert.IsFalse(this.mAssociation.Association.ContainsKey(original1));
            Assert.AreEqual(4, this.mAssociation.Association[original2].Count);
        }

        [Test]
        public void TestHasAssociatedOriginal()
        {
            this.mAssociation.Clear();
            this.TestAddAssociation();

            Assert.IsTrue(this.mAssociation.HasAssociatedOriginal(replica1));
            Assert.IsTrue(this.mAssociation.HasAssociatedOriginal(replica2));
            Assert.IsTrue(this.mAssociation.HasAssociatedOriginal(replica3));
            Assert.IsTrue(this.mAssociation.HasAssociatedOriginal(replica4));
            Assert.IsTrue(this.mAssociation.HasAssociatedOriginal(replica5));
            Assert.IsFalse(this.mAssociation.HasAssociatedOriginal(replica6));
            Assert.IsFalse(this.mAssociation.HasAssociatedOriginal(this.idAllocator.GetNextUniqueId()));

            this.mAssociation.RemoveAssociatedReplicas(original1);

            Assert.IsFalse(this.mAssociation.HasAssociatedOriginal(replica1));
            Assert.IsTrue(this.mAssociation.HasAssociatedOriginal(replica2));
            Assert.IsFalse(this.mAssociation.HasAssociatedOriginal(replica3));
            Assert.IsFalse(this.mAssociation.HasAssociatedOriginal(replica4));
            Assert.IsFalse(this.mAssociation.HasAssociatedOriginal(replica5));
            Assert.IsFalse(this.mAssociation.HasAssociatedOriginal(replica6));
            Assert.IsFalse(this.mAssociation.HasAssociatedOriginal(this.idAllocator.GetNextUniqueId()));
        }
    }
}
