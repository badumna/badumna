﻿//-----------------------------------------------------------------------
// <copyright file="UpdateMethodManagerTester.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System.Collections.Generic;
using Badumna;
using Badumna.Core;
using Badumna.DataTypes;
using Badumna.DistributedHashTable;
using Badumna.Overload;
using Badumna.Replication;
using Badumna.Utilities;
using BadumnaTests.Utilities;
using NUnit.Framework;
using Rhino.Mocks;

namespace BadumnaTests.Replication
{
    [TestFixture]
    [DontPerformCoverage]
    public class UpdateMethodManagerTester
    {
        private Dictionary<PeerAddress, InterestedPeer> interestedPeers;

        private UpdateChannelManager updateChannels;

        private List<InterestedEntity> overloadEntities;

        private IForwardingGroup mockedForwardingGroup;

        private IEntityManager mockedEntityManager;

        private IEntityRouter mockedEntityRouter1;
        private IEntityRouter mockedEntityRouter2;
        private IEntityRouter mockedEntityRouter3;
    
        private UpdateMethodManager updateMethodManager;

        private BadumnaId id1;
        private BadumnaId id2;
        private BadumnaId id3;
        private BadumnaId idNeverUsed;

        private PeerAddress peerAddress1;
        private PeerAddress peerAddress2;
        private PeerAddress peerAddress3;

        private UpdateChannel updateChannel1;
        private UpdateChannel updateChannel2;
        private UpdateChannel updateChannel3;

        private ITime timeKeeper;

        [SetUp]
        public void Initialize()
        {
            NetworkEventQueue eventQueue = new NetworkEventQueue();
            this.timeKeeper = eventQueue;

            this.overloadEntities = new List<InterestedEntity>();
            this.interestedPeers = new Dictionary<PeerAddress, InterestedPeer>();

            GenericCallBackReturn<BadumnaId, PeerAddress> generateBadumnaId = BadumnaIdGenerator.FromAddress();

            this.id1 = generateBadumnaId(new PeerAddress(HashKey.Hash("test1")));
            this.id2 = generateBadumnaId(new PeerAddress(HashKey.Hash("test2")));
            this.id3 = generateBadumnaId(new PeerAddress(HashKey.Hash("test3")));
            this.idNeverUsed = generateBadumnaId(new PeerAddress(HashKey.Hash("test4")));

            this.peerAddress1 = PeerAddress.GetAddress("127.0.0.1", 1, NatType.Open);
            this.peerAddress2 = PeerAddress.GetAddress("127.0.0.1", 2, NatType.Open);
            this.peerAddress3 = PeerAddress.GetAddress("127.0.0.1", 3, NatType.Open);

            InterestedPeer peer1 = new InterestedPeer(this.timeKeeper);
            peer1.Add(generateBadumnaId(this.peerAddress1));
            peer1.Add(generateBadumnaId(this.peerAddress1));
            peer1.Add(generateBadumnaId(this.peerAddress1));
            this.interestedPeers.Add(this.peerAddress1, peer1);

            InterestedPeer peer2 = new InterestedPeer(this.timeKeeper);
            peer2.Add(generateBadumnaId(this.peerAddress2));
            peer2.Add(generateBadumnaId(this.peerAddress2));
            peer2.Add(generateBadumnaId(this.peerAddress2));
            this.interestedPeers.Add(this.peerAddress2, peer2);

            InterestedPeer peer3 = new InterestedPeer(this.timeKeeper);
            peer3.Add(generateBadumnaId(this.peerAddress3));
            peer3.Add(generateBadumnaId(this.peerAddress3));
            peer3.Add(generateBadumnaId(this.peerAddress3));
            this.interestedPeers.Add(this.peerAddress3, peer3);

            this.mockedEntityManager = MockRepository.GenerateMock<IEntityManager>();
            this.mockedForwardingGroup = MockRepository.GenerateMock<IForwardingGroup>();
            this.mockedEntityRouter1 = MockRepository.GenerateMock<IEntityRouter>();
            this.mockedEntityRouter2 = MockRepository.GenerateMock<IEntityRouter>();
            this.mockedEntityRouter3 = MockRepository.GenerateMock<IEntityRouter>();

            this.updateChannels = new UpdateChannelManager(this.mockedEntityManager, eventQueue, this.timeKeeper);
            this.updateChannel1 = new UpdateChannel(this.peerAddress1, this.mockedEntityRouter1, eventQueue, this.timeKeeper);
            this.updateChannel2 = new UpdateChannel(this.peerAddress2, this.mockedEntityRouter2, eventQueue, this.timeKeeper);
            this.updateChannel3 = new UpdateChannel(this.peerAddress3, this.mockedEntityRouter3, eventQueue, this.timeKeeper);

            this.updateChannels[this.peerAddress1] = this.updateChannel1;
            this.updateChannels[this.peerAddress2] = this.updateChannel2;
            this.updateChannels[this.peerAddress3] = this.updateChannel3;

            OverloadModule overloadOptions = new OverloadModule(null);

            this.updateMethodManager = new UpdateMethodManager(this.interestedPeers, this.updateChannels, this.overloadEntities, this.mockedForwardingGroup, overloadOptions);
        }

        [TearDown]
        public void Terminate()
        {
        }

        [Test]
        public void DefaultCurrentEntitiesInSwitchingBackModeValueIsZero()
        {
            Assert.AreEqual(0, this.updateMethodManager.CurrentEntitiesInSwitchingBackMode);
        }

        [Test]
        public void HasOverloadedReturnsCorrectResults()
        {
            Assert.IsFalse(this.updateMethodManager.HasOverloaded(this.idNeverUsed));

            InterestedEntity entity = new InterestedEntity(this.id1, this.timeKeeper);
            this.updateMethodManager.AddOverloadDestination(entity);
            Assert.IsTrue(this.updateMethodManager.HasOverloaded(this.id1));
        }

        [Test]
        public void AddAndRemoveOverloadDestinationWorksCorrectly()
        {
            Assert.IsFalse(this.updateMethodManager.HasOverloaded(this.idNeverUsed));

            InterestedEntity entity1 = new InterestedEntity(this.id1, this.timeKeeper);
            InterestedEntity entity2 = new InterestedEntity(this.id2, this.timeKeeper);
            InterestedEntity entity3 = new InterestedEntity(this.id3, this.timeKeeper);

            this.updateMethodManager.AddOverloadDestination(entity1);
            this.updateMethodManager.AddOverloadDestination(entity2);
            this.updateMethodManager.AddOverloadDestination(entity3);

            Assert.IsTrue(this.updateMethodManager.HasOverloaded(this.id1));
            Assert.IsTrue(this.updateMethodManager.HasOverloaded(this.id2));
            Assert.IsTrue(this.updateMethodManager.HasOverloaded(this.id3));

            this.updateMethodManager.RemoveOverloadDestination(entity1);
            Assert.IsFalse(this.updateMethodManager.HasOverloaded(this.id1));
            Assert.IsTrue(this.updateMethodManager.HasOverloaded(this.id2));
            Assert.IsTrue(this.updateMethodManager.HasOverloaded(this.id3));

            this.updateMethodManager.RemoveOverloadDestination(this.idNeverUsed);
            Assert.IsFalse(this.updateMethodManager.HasOverloaded(this.id1));
            Assert.IsTrue(this.updateMethodManager.HasOverloaded(this.id2));
            Assert.IsTrue(this.updateMethodManager.HasOverloaded(this.id3));
        }

        [Test]
        public void ClearOverloadDestinationsWorksCorrectly()
        {
            Assert.IsFalse(this.updateMethodManager.HasOverloaded(this.idNeverUsed));

            InterestedEntity entity1 = new InterestedEntity(this.id1, this.timeKeeper);
            InterestedEntity entity2 = new InterestedEntity(this.id2, this.timeKeeper);
            InterestedEntity entity3 = new InterestedEntity(this.id3, this.timeKeeper);

            this.updateMethodManager.AddOverloadDestination(entity1);
            this.updateMethodManager.AddOverloadDestination(entity2);
            this.updateMethodManager.AddOverloadDestination(entity3);

            Assert.IsTrue(this.updateMethodManager.HasOverloaded(this.id1));
            Assert.IsTrue(this.updateMethodManager.HasOverloaded(this.id2));
            Assert.IsTrue(this.updateMethodManager.HasOverloaded(this.id3));

            this.updateMethodManager.ClearOverloadDestinations();
            Assert.IsFalse(this.updateMethodManager.HasOverloaded(this.id1));
            Assert.IsFalse(this.updateMethodManager.HasOverloaded(this.id2));
            Assert.IsFalse(this.updateMethodManager.HasOverloaded(this.id3));
        }

        [Test]
        public void OnlySwitchToOverloadWhenForwardingGroupHasBeenConnected()
        {
            this.mockedForwardingGroup.Stub(f => f.ConfirmConnected).IgnoreArguments().Repeat.Once().Return(false);
            Assert.IsFalse(this.updateMethodManager.ShouldSwitchToOverload(2));

            this.mockedForwardingGroup.Stub(f => f.ConfirmConnected).IgnoreArguments().Repeat.Once().Return(true);
            Assert.IsTrue(this.updateMethodManager.ShouldSwitchToOverload(2));
        }

        [Test]
        public void WillNotEngageOverloadWhenOnlyOneOverloaded()
        {
            this.mockedForwardingGroup.Stub(f => f.ConfirmConnected).IgnoreArguments().Repeat.Once().Return(true);
            Assert.IsFalse(this.updateMethodManager.ShouldSwitchToOverload(1));

            this.mockedForwardingGroup.Stub(f => f.ConfirmConnected).IgnoreArguments().Repeat.Once().Return(true);
            Assert.IsTrue(this.updateMethodManager.ShouldSwitchToOverload(2));
        }

        [Test]
        public void SwitchToOverloadModeWorksCorrectly()
        {
            List<UpdateChannel> channels = new List<UpdateChannel>();
            channels.Add(this.updateChannel2);
            channels.Add(this.updateChannel3);

            this.updateMethodManager.SwitchToOverloadMode(channels);

            Assert.AreEqual(6, this.overloadEntities.Count);
            Assert.IsFalse(this.updateChannels.ContainsKey(this.peerAddress2));
            Assert.IsFalse(this.updateChannels.ContainsKey(this.peerAddress3));
            Assert.IsTrue(this.updateChannels.ContainsKey(this.peerAddress1));

            this.AssertDeliveryMethod(this.peerAddress1, UpdateDeliveryMethod.Direct);
            this.AssertDeliveryMethod(this.peerAddress2, UpdateDeliveryMethod.Overload);
            this.AssertDeliveryMethod(this.peerAddress3, UpdateDeliveryMethod.Overload);
        }

        [Test]
        public void SwitchToSwitchingBackWorksCorrectly()
        {
            this.SwitchToOverloadModeWorksCorrectly();

            List<InterestedEntity> changeToSwitchingBackMethod = new List<InterestedEntity>();
            InterestedPeer currentPeer = this.interestedPeers[this.peerAddress2];
            foreach (InterestedEntity entity in currentPeer)
            {
                changeToSwitchingBackMethod.Add(entity);
            }

            Assert.AreEqual(3, changeToSwitchingBackMethod.Count);
            this.updateMethodManager.SwitchToSwitchingBackMethod(changeToSwitchingBackMethod);

            this.AssertDeliveryMethod(this.peerAddress1, UpdateDeliveryMethod.Direct);
            this.AssertDeliveryMethod(this.peerAddress2, UpdateDeliveryMethod.SwitchingBack);
            this.AssertDeliveryMethod(this.peerAddress3, UpdateDeliveryMethod.Overload);

            Assert.IsTrue(this.updateChannels.ContainsKey(this.peerAddress2));
            Assert.IsFalse(this.updateChannels.ContainsKey(this.peerAddress3));
            Assert.IsTrue(this.updateChannels.ContainsKey(this.peerAddress1));

            Assert.AreEqual(3, this.updateMethodManager.CurrentEntitiesInSwitchingBackMode);
        }

        [Test]
        public void SwitchToDirectModeWorksCorrectly()
        {
            this.SwitchToSwitchingBackWorksCorrectly();

            List<InterestedEntity> changeToDirect = new List<InterestedEntity>();
            InterestedPeer currentPeer = this.interestedPeers[this.peerAddress2];
            foreach (InterestedEntity entity in currentPeer)
            {
                changeToDirect.Add(entity);
            }

            Assert.AreEqual(3, changeToDirect.Count);
            this.updateMethodManager.SwitchToDirectMode(changeToDirect);

            this.AssertDeliveryMethod(this.peerAddress1, UpdateDeliveryMethod.Direct);
            this.AssertDeliveryMethod(this.peerAddress2, UpdateDeliveryMethod.Direct);
            this.AssertDeliveryMethod(this.peerAddress3, UpdateDeliveryMethod.Overload);

            Assert.IsTrue(this.updateChannels.ContainsKey(this.peerAddress2));
            Assert.IsFalse(this.updateChannels.ContainsKey(this.peerAddress3));
            Assert.IsTrue(this.updateChannels.ContainsKey(this.peerAddress1));
        }

        [Test]
        public void ShutdownCorrectlyResetCurrentEntitiesInSwitchingBackMode()
        {
            this.SwitchToSwitchingBackWorksCorrectly();
            this.updateMethodManager.Shutdown();
            Assert.AreEqual(0, this.updateMethodManager.CurrentEntitiesInSwitchingBackMode);
        }

        private void AssertDeliveryMethod(PeerAddress address, UpdateDeliveryMethod method)
        {
            InterestedPeer currentPeer = this.interestedPeers[address];
            foreach (InterestedEntity entity in currentPeer)
            {
                Assert.AreEqual(method, entity.UpdateMethod);
            }
        }
    }
}
