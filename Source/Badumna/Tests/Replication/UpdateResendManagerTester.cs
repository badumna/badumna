﻿//-----------------------------------------------------------------------
// <copyright file="UpdateResendManagerTester.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using Badumna.Core;
using Badumna.DataTypes;
using Badumna.DistributedHashTable;
using Badumna.Replication;
using Badumna.Utilities;
using NUnit.Framework;
using Rhino.Mocks;

namespace BadumnaTests.Replication
{
    [TestFixture]
    [DontPerformCoverage]
    public class UpdateResendManagerTester
    {
        private UpdateResendManager manager;
        private IEntityManager mockedEntityManager;
        private IOriginalWrapper mockedOriginalWrapper;
        private IEntityRouter mockedEntityRouter;

        private BadumnaId sourceId;
        private BadumnaId id1;
        private BadumnaId id2;
        private BadumnaId id3;

        private IOriginal original;

        private EntityEnvelope envelope = new EntityEnvelope();
        private CyclicalID.UShortID seq = new CyclicalID.UShortID(1234);

        private Simulator eventQueue;

        [SetUp]
        public void Initialize()
        {
            this.eventQueue = new Simulator();
            this.ResetManager();
        }

        [Test]
        public void ConstructorThrowsNoException()
        {
            this.ResetManager();
        }

        [Test]
        public void UpdateResnedWillBeCorrectlySentToAllTargets()
        {
            this.UpdateResendEvent();
            this.eventQueue.RunFor(Parameters.SegmentResendDelayTime + TimeSpan.FromSeconds(1));

            BooleanArray remainingParts;
            this.mockedOriginalWrapper.Stub(f => f.SerializeParts(null, out remainingParts)).IgnoreArguments().Return(new byte[1]);
            this.mockedEntityManager.Stub(f => f.Router).Return(this.mockedEntityRouter);
            this.mockedEntityManager.Stub(f => f.GetMessageFor(null, null, null)).IgnoreArguments().Return(this.envelope);
            this.mockedOriginalWrapper.Stub(f => f.LocalEntity).Return(this.original);
            this.original.Stub(f => f.Guid).Return(this.sourceId);
            this.mockedEntityManager.Expect(f => f.UpdateEntity(null, this.seq, null, new byte[1])).IgnoreArguments().Repeat.Times(6);
            this.mockedEntityRouter.Expect(f => f.DirectSend(null)).IgnoreArguments().Repeat.Times(3);

            this.manager.ResendUpdates();
            
            this.mockedEntityManager.VerifyAllExpectations();
            this.mockedEntityRouter.VerifyAllExpectations();
        }

        [Test]
        public void UpdateResendWillNotBeSentToUninterestedPeer()
        {
            this.UpdateResendEvent();
            this.eventQueue.RunFor(Parameters.SegmentResendDelayTime);
            this.manager.RemoveUnacknowledgedPeer(this.id2);
            this.eventQueue.RunFor(TimeSpan.FromSeconds(1));

            BooleanArray remainingParts;
            this.mockedOriginalWrapper.Stub(f => f.SerializeParts(null, out remainingParts)).IgnoreArguments().Return(new byte[1]);
            this.mockedEntityManager.Stub(f => f.Router).Return(this.mockedEntityRouter);
            this.mockedEntityManager.Stub(f => f.GetMessageFor(null, null, null)).IgnoreArguments().Return(this.envelope);
            this.mockedOriginalWrapper.Stub(f => f.LocalEntity).Return(this.original);
            this.original.Stub(f => f.Guid).Return(this.sourceId);
            this.mockedEntityManager.Expect(f => f.UpdateEntity(null, this.seq, null, new byte[1])).IgnoreArguments().Repeat.Times(4);
            this.mockedEntityRouter.Expect(f => f.DirectSend(null)).IgnoreArguments().Repeat.Times(2);

            this.manager.ResendUpdates();

            this.mockedEntityManager.VerifyAllExpectations();
            this.mockedEntityRouter.VerifyAllExpectations();
        }

        [Test]
        public void UpdateResendWillNotBeSentToAcknowledgedPeer()
        {
            this.UpdateResendEvent();
            this.eventQueue.RunFor(TimeSpan.FromSeconds(1));
            
            BooleanArray array = new BooleanArray();
            array.SetAll(false);
            array[0] = true;
            array[1] = true;

            List<CyclicalID.UShortID> seqs = new List<CyclicalID.UShortID>();
            seqs.Add(this.seq);
            seqs.Add(this.seq);
            this.manager.ProcessAcknowledgements(this.id2, array, seqs);

            this.eventQueue.RunFor(Parameters.SegmentResendDelayTime);
            BooleanArray remainingParts;
            this.mockedOriginalWrapper.Stub(f => f.SerializeParts(null, out remainingParts)).IgnoreArguments().Return(new byte[1]);
            this.mockedEntityManager.Stub(f => f.Router).Return(this.mockedEntityRouter);
            this.mockedEntityManager.Stub(f => f.GetMessageFor(null, null, null)).IgnoreArguments().Return(this.envelope);
            this.mockedOriginalWrapper.Stub(f => f.LocalEntity).Return(this.original);
            this.original.Stub(f => f.Guid).Return(this.sourceId);
            this.mockedEntityManager.Expect(f => f.UpdateEntity(null, this.seq, null, new byte[1])).IgnoreArguments().Repeat.Times(4);
            this.mockedEntityRouter.Expect(f => f.DirectSend(null)).IgnoreArguments().Repeat.Times(2);

            this.manager.ResendUpdates();

            this.mockedEntityManager.VerifyAllExpectations();
            this.mockedEntityRouter.VerifyAllExpectations();
        }

        [Test]
        public void AckWithUnmatchedSeqWillBeIgnored()
        {
            this.UpdateResendEvent();
            this.eventQueue.RunFor(TimeSpan.FromSeconds(1));
            
            BooleanArray array = new BooleanArray();
            array.SetAll(false);
            array[0] = true;
            array[1] = true;

            List<CyclicalID.UShortID> seqs = new List<CyclicalID.UShortID>();
            seqs.Add(new CyclicalID.UShortID(1));
            seqs.Add(new CyclicalID.UShortID(2));
            this.manager.ProcessAcknowledgements(this.id2, array, seqs);

            this.eventQueue.RunFor(Parameters.SegmentResendDelayTime);
            BooleanArray remainingParts;
            this.mockedOriginalWrapper.Stub(f => f.SerializeParts(null, out remainingParts)).IgnoreArguments().Return(new byte[1]);
            this.mockedEntityManager.Stub(f => f.Router).Return(this.mockedEntityRouter);
            this.mockedEntityManager.Stub(f => f.GetMessageFor(null, null, null)).IgnoreArguments().Return(this.envelope);
            this.mockedOriginalWrapper.Stub(f => f.LocalEntity).Return(this.original);
            this.original.Stub(f => f.Guid).Return(this.sourceId);
            this.mockedEntityManager.Expect(f => f.UpdateEntity(null, this.seq, null, new byte[1])).IgnoreArguments().Repeat.Times(6);
            this.mockedEntityRouter.Expect(f => f.DirectSend(null)).IgnoreArguments().Repeat.Times(3);

            this.manager.ResendUpdates();

            this.mockedEntityManager.VerifyAllExpectations();
            this.mockedEntityRouter.VerifyAllExpectations();
        }

        private void UpdateResendEvent()
        {
            BooleanArray array = new BooleanArray();
            array.SetAll(false);
            array[0] = true;
            array[1] = true;

            List<BadumnaId> target = new List<BadumnaId>();
            target.Add(this.id1);
            target.Add(this.id2);
            target.Add(this.id3);

            this.manager.UpdateResendEvents(array, this.seq, target);
        }

        private void ResetManager()
        {
            this.mockedEntityManager = MockRepository.GenerateMock<IEntityManager>();
            this.mockedOriginalWrapper = MockRepository.GenerateMock<IOriginalWrapper>();
            this.mockedEntityRouter = MockRepository.GenerateMock<IEntityRouter>();

            this.manager = new UpdateResendManager(this.mockedEntityManager, this.mockedOriginalWrapper, this.eventQueue);

            this.id1 = new BadumnaId(new PeerAddress(HashKey.Hash("test1")), 1);
            this.id2 = new BadumnaId(new PeerAddress(HashKey.Hash("test2")), 2);
            this.id3 = new BadumnaId(new PeerAddress(HashKey.Hash("test3")), 3);

            this.sourceId = new BadumnaId(new PeerAddress(HashKey.Hash("sourceId")), 4);

            this.original = MockRepository.GenerateMock<IOriginal>();
        }
    }
}
