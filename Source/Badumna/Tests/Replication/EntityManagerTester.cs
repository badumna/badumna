﻿//-----------------------------------------------------------------------
// <copyright file="EntityManagerTester.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using Badumna;
using Badumna.Core;
using Badumna.DataTypes;
using Badumna.DistributedHashTable;
using Badumna.Replication;
using Badumna.Transport;
using BadumnaTests.Core;
using NUnit.Framework;
using Rhino.Mocks;

namespace BadumnaTests.Replication
{
    [TestFixture]
    [DontPerformCoverage]
    public class EntityManagerTester
    {
        private TransportProtocol mockedTransportProtocol;
        private IChannelFunnel<TransportEnvelope> mockedConnectionTable;
        private EntityManager entityManager;
        private MockedOriginal localEntity;
        private BadumnaId entityId;

        [SetUp]
        public void Initialize()
        {
            this.entityId = new BadumnaId(new PeerAddress(HashKey.Hash("entityid")), 1);
            this.localEntity = new MockedOriginal(new BadumnaId(new PeerAddress(HashKey.Hash("mockedoriginalid")), 1));
            this.mockedTransportProtocol = new TransportProtocol("foo", typeof(ConnectionfulProtocolMethodAttribute), MessageParserTester.DefaultFactory);
            this.mockedConnectionTable = MockRepository.GenerateMock<IChannelFunnel<TransportEnvelope>>();
            NetworkEventQueue eventQueue = new NetworkEventQueue();
            QueueInvokable queueApplicationEvent = i => i.Invoke();
            ITime timeKeeper = eventQueue;
            OverloadModule overloadOptions = new OverloadModule(null);
            INetworkConnectivityReporter connectivityReporter = MockRepository.GenerateStub<INetworkConnectivityReporter>();
            INetworkAddressProvider addressProvider = MockRepository.GenerateStub<INetworkAddressProvider>();
            addressProvider.Stub(x => x.PublicAddress).Return(PeerAddress.GetLoopback(1));
            this.entityManager = new EntityManager(this.mockedTransportProtocol, this.mockedConnectionTable, null, overloadOptions, eventQueue, queueApplicationEvent, timeKeeper, addressProvider, connectivityReporter, MessageParserTester.DefaultFactory);
            this.mockedTransportProtocol.ForgeMethodList();
            this.entityManager.ForgeMethodList();
        }

        [Test]
        public void RegisterEntityAcceptsNewOriginalEntity()
        {
            EntityTypeId id = new EntityTypeId((byte)EntityGroup.Spatial, 0);
            Assert.AreEqual(0, this.entityManager.OriginalEntityCount);
            this.entityManager.RegisterEntity(this.localEntity, id);
            Assert.AreEqual(1, this.entityManager.OriginalEntityCount);
        }

        [Test]
        public void RegisterEntityIgnoresExistingOriginalEntity()
        {
            EntityTypeId id = new EntityTypeId((byte)EntityGroup.Spatial, 0);
            Assert.AreEqual(0, this.entityManager.OriginalEntityCount);
            this.entityManager.RegisterEntity(this.localEntity, id);
            this.entityManager.RegisterEntity(this.localEntity, id);
            this.entityManager.RegisterEntity(this.localEntity, id);
            Assert.AreEqual(1, this.entityManager.OriginalEntityCount);
        }

        [Test]
        [ExpectedException("System.ArgumentException")]
        public void RegisterEntityThrowsExceptionWhenTheOriginalDoesNotHaveGUID()
        {
            MockedOriginal entity = new MockedOriginal(null);
            EntityTypeId id = new EntityTypeId((byte)EntityGroup.Spatial, 0);
            this.entityManager.RegisterEntity(entity, id);
        }

        [Test]
        public void UnregisterEntityRemovesRegisteredEntity()
        {
            this.RegisterEntityAcceptsNewOriginalEntity();
            this.entityManager.UnregisterEntity(this.localEntity, false);
            Assert.AreEqual(0, this.entityManager.OriginalEntityCount);
        }

        [Test]
        public void UnregisterEntityIgnoresEntityThatIsNotRegistered()
        {
            this.RegisterEntityAcceptsNewOriginalEntity();
            MockedOriginal localEntity2 = new MockedOriginal(new BadumnaId(new PeerAddress(HashKey.Hash("mockedoriginalid")), 2));
            this.entityManager.UnregisterEntity(localEntity2, false);
            Assert.AreEqual(1, this.entityManager.OriginalEntityCount);
        }

        [Test]
        public void AddInterestedActuallyAddsInterested()
        {
            this.RegisterEntityAcceptsNewOriginalEntity();
            
            Assert.AreEqual(0, this.entityManager.InterestedCount(this.localEntity));
            this.entityManager.AddInterested(this.localEntity, this.entityId);
            Assert.AreEqual(1, this.entityManager.InterestedCount(this.localEntity));
        }

        /*[Test]
        public void RemoveInterestedActuallyRemovesInterested()
        {
            this.AddInterestedActuallyAddsInterested();
            Assert.AreEqual(1, this.entityManager.InterestedCount(this.localEntity));
            this.entityManager.RemoveInterested(this.localEntity, this.entityId);
            Assert.AreEqual(0, this.entityManager.InterestedCount(this.localEntity));
        }*/

        [Test]
        public void CustomMessagesAreCachedUntilReplicaInstantiation()
        {
            // Hmmm... need to refactor a bit to make this easily testable.

            ////// Arrange
            ////var seqNum = new Badumna.Utilities.CyclicalID.UShortID(0);
            ////byte[] data = new byte[4]; 
            ////new Random().NextBytes(data);
            ////var destination = new BadumnaId(PeerAddress.GetRandomAddress(), 7);
            ////var source = new BadumnaId(PeerAddress.GetRandomAddress(), 6);
            ////var envelope = new EntityEnvelope(destination, source, QualityOfService.Reliable);
            ////envelope.
            ////this.entityManager.ProcessMessage();

            ////this.entityManager.ProcessCustomMessage(seqNum, data);
        }
    }
}
