﻿//-----------------------------------------------------------------------
// <copyright file="EntityProtocolTester.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using Badumna.Core;
using Badumna.DataTypes;
using Badumna.DistributedHashTable;
using Badumna.Replication;
using BadumnaTests.Core;
using NUnit.Framework;
using Rhino.Mocks;

namespace BadumnaTests.Replication
{
    [TestFixture]
    [DontPerformCoverage]
    public class EntityProtocolTester
    {
        private BadumnaId sourceEntityId;
        private BadumnaId destinationEntityId;

        private EntityProtocol protocol;

        private INetworkAddressProvider addressProvider;

        [SetUp]
        public void Initialize()
        {
            this.addressProvider = MockRepository.GenerateStub<INetworkAddressProvider>();
            this.addressProvider.Stub(x => x.PublicAddress).Return(PeerAddress.GetLoopback(1));
            this.protocol = new EntityProtocol(this.addressProvider, MessageParserTester.DefaultFactory);
            this.sourceEntityId = new BadumnaId(new PeerAddress(HashKey.Hash("test1")), 1);
            this.destinationEntityId = new BadumnaId(new PeerAddress(HashKey.Hash("test2")), 2);
        }

        [TearDown]
        public void Terminate()
        {
        }

        [Test]
        public void SimpleConstructorDoesNotThrowException()
        {
            this.protocol = new EntityProtocol(this.addressProvider, MessageParserTester.DefaultFactory);
            EntityProtocol protocol2 = new EntityProtocol(this.protocol, this.addressProvider);
        }

        [Test]
        public void GetMessageForWithOnlyDestinationTest()
        {
            EntityEnvelope envelope = this.protocol.GetMessageFor(this.destinationEntityId, QualityOfService.Unreliable);
            Assert.AreEqual(envelope.DestinationEntity, this.destinationEntityId);
            Assert.IsFalse(envelope.Qos.IsReliable);
        }

        [Test]
        public void GetMessageForWithSourceAndDestinationTest()
        {
            EntityEnvelope envelope = this.protocol.GetMessageFor(this.sourceEntityId, this.destinationEntityId, QualityOfService.Unreliable);
            Assert.AreEqual(envelope.SourceEntity, this.sourceEntityId);
            Assert.AreEqual(envelope.DestinationEntity, this.destinationEntityId);
            Assert.IsFalse(envelope.Qos.IsReliable);
        }

        [Test]
        public void GetMessageFromWithOnlySourceTest()
        {
            EntityEnvelope envelope = this.protocol.GetMessageFrom(this.sourceEntityId, QualityOfService.Unreliable);
            Assert.AreEqual(envelope.SourceEntity, this.sourceEntityId);
            Assert.IsFalse(envelope.Qos.IsReliable);
        }
    }
}
