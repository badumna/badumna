﻿//-----------------------------------------------------------------------
// <copyright file="MockedOriginal.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System.IO;

using Badumna.Core;
using Badumna.DataTypes;
using Badumna.DistributedHashTable;
using Badumna.Replication;
using Badumna.Utilities;

namespace BadumnaTests.Replication
{
    [DontPerformCoverage]
    internal class MockedOriginal : IOriginal 
    {
        private int handledEvent;

        private int field1;
        private int field2;

        private BadumnaId id;

        #region IOriginal Members

        public MockedOriginal(BadumnaId id)
        {
            this.field2 = 0x1234;
            this.field2 = 0xabcd;
            this.id = id;
            this.handledEvent = 0;
        }

        public int Field1
        {
            get { return this.field1; }
            set { this.field1 = value; }
        }

        public int Field2
        {
            get { return this.field2; }
            set { this.field2 = value; }
        }

        public int HandledEvent
        {
            get { return this.handledEvent; }
        }

        public BadumnaId Guid
        {
            get
            {
                return this.id;
            }

            set
            {
                this.id = value;
            }
        }

        public BooleanArray Serialize(BooleanArray requiredParts, Stream stream)
        {
            using (BinaryWriter writer = new BinaryWriter(stream))
            {
                if (requiredParts[0])
                {
                    writer.Write(this.field1);
                }

                if (requiredParts[1])
                {
                    writer.Write(this.field2);
                }
            }

            return new BooleanArray(false);
        }

        #endregion

        #region IEntity Members

        public void HandleEvent(System.IO.Stream stream)
        {
            this.handledEvent++;
        }

        #endregion
    }
}
