﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using Badumna.Core;
using Badumna.Utilities;
using Badumna.Replication;

namespace BadumnaTests.Replication
{
    [TestFixture]
    [DontPerformCoverage]
    public class CustomMessageQueueTester
    {
        private CustomMessageQueue mQueue;

        private Simulator eventQueue;

        [SetUp]
        public void Initialize()
        {
            this.eventQueue = new Simulator();
            ITime timeKeeper = eventQueue;
            INetworkConnectivityReporter connectivityReporter = new NetworkConnectivityReporter(eventQueue);
            connectivityReporter.SetStatus(ConnectivityStatus.Online);
            this.mQueue = new CustomMessageQueue(this.eventQueue, timeKeeper, connectivityReporter);
        }

        private CyclicalID.UShortID PopAvailableFrom(ushort initialValue)
        {
            CyclicalID.UShortID expectedNumber = new CyclicalID.UShortID(initialValue);
            CustomMessageQueue.CustomMessage customMessage = this.mQueue.InOrderPop();
            while (null != customMessage)
            {
                Assert.AreEqual(expectedNumber.Value, customMessage.MessageNumber.Value, "Invalid Order");
                expectedNumber.Increment();
                customMessage = this.mQueue.InOrderPop();
            }

            return expectedNumber;
        }

        [Test]
        public void OrderTest()
        {
            // Test that custom messages are removed in the correct order.
            this.mQueue.Add(new CyclicalID.UShortID(0), null);
            this.mQueue.Add(new CyclicalID.UShortID(2), null);
            this.mQueue.Add(new CyclicalID.UShortID(1), null);
            this.mQueue.Add(new CyclicalID.UShortID(3), null);

            Assert.AreEqual(0, this.mQueue.Pop().MessageNumber.Value, "Incorrect order");
            Assert.AreEqual(1, this.mQueue.Pop().MessageNumber.Value, "Incorrect order");
            Assert.AreEqual(2, this.mQueue.Pop().MessageNumber.Value, "Incorrect order");
            Assert.AreEqual(3, this.mQueue.Pop().MessageNumber.Value, "Incorrect order");
        }

        [Test]
        public void DelayTest()
        {
            // Test that custom messages aren't returned until there are in order.
            this.mQueue.Add(new CyclicalID.UShortID(0), null);
            this.mQueue.Add(new CyclicalID.UShortID(1), null);
            this.mQueue.Add(new CyclicalID.UShortID(3), null);

            CyclicalID.UShortID nextExpectedNumber = this.PopAvailableFrom(0);

            Assert.AreEqual(2, nextExpectedNumber.Value, "Incorrect returned message.");

            // Test that the remaining messages are popped if the missing one is added.
            this.mQueue.Add(new CyclicalID.UShortID(2), null);
            nextExpectedNumber = this.PopAvailableFrom(nextExpectedNumber.Value);

            Assert.AreEqual(4, nextExpectedNumber.Value, "Incorrect returned message.");
        }

        [Test]
        public void DelayTimeoutTest()
        {
            // Test that a missing custom message is ignored after the timeout and that all subsequent messages are handled.
            this.mQueue.Add(new CyclicalID.UShortID(0), null);

            // Run the simulator to give a time difference between first and second messages arriving.
            this.eventQueue.RunFor(TimeSpan.FromSeconds(3));

            this.mQueue.Add(new CyclicalID.UShortID(1), null);
            this.mQueue.Add(new CyclicalID.UShortID(3), null);

            CyclicalID.UShortID nextExpectedNumber = this.PopAvailableFrom(0);
            Assert.AreEqual(2, nextExpectedNumber.Value, "Failed to wait on correct message");

            this.eventQueue.RunFor(Parameters.CustomMessageCleanupRate);
            nextExpectedNumber = this.PopAvailableFrom(3); // Skipped 2
            Assert.AreEqual(4, nextExpectedNumber.Value, "Failed to continue past missing message.");

            // Test that if a old discarded message arrives it is ignored, without interruption
            this.mQueue.Add(new CyclicalID.UShortID(2), null);
            this.mQueue.Add(new CyclicalID.UShortID(4), null);
            this.mQueue.Add(new CyclicalID.UShortID(5), null);

            nextExpectedNumber = this.PopAvailableFrom(nextExpectedNumber.Value);
            Assert.AreEqual(6, nextExpectedNumber.Value, "Failed to continue past late message.");
        }
    }
}
