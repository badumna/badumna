﻿// -----------------------------------------------------------------------
// <copyright file="CustomMessageCacheTests.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2013 Scalify Pty Ltd. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace BadumnaTests.Replication
{
    using System.Collections.Generic;
    using Badumna.Core;
    using Badumna.DataTypes;
    using Badumna.Replication;
    using Badumna.Utilities;
    using NUnit.Framework;

    [TestFixture]
    [DontPerformCoverage]
    public class CustomMessageCacheTests
    {
        private CustomMessageCache cache;
        private List<BadumnaId> testIDs;
        private List<byte[]> testData;
        private List<CyclicalID.UShortID> testNumbers;

        [SetUp]
        public void Initialize()
        {
            this.cache = new CustomMessageCache();
            this.testIDs = new List<BadumnaId>
            {
                new BadumnaId(PeerAddress.GetRandomAddress(), 0),
                new BadumnaId(PeerAddress.GetRandomAddress(), 1),
                new BadumnaId(PeerAddress.GetRandomAddress(), 2)
            };
            this.testData = new List<byte[]>
            {
                new byte[] { 0x01, 0x02, 0x03, 0x04 },
                new byte[] { 0x11, 0x12, 0x13, 0x14 },
                new byte[] { 0x21, 0x22, 0x23, 0x24 }
            };
            this.testNumbers = new List<CyclicalID.UShortID>
            {
                new CyclicalID.UShortID(0),
                new CyclicalID.UShortID(1),
                new CyclicalID.UShortID(2)
            };
        }

        [Test]
        public void GetEntitiesWithCachedMessagesReturnsAllEntities()
        {
            // Arrange
            this.cache.Cache(this.testIDs[0], this.testNumbers[0], this.testData[0]);

            // Act
            var entities = new List<BadumnaId>(this.cache.GetEntitiesWithCachedMessages());

            // Assert
            Assert.AreEqual(1, entities.Count);
            Assert.IsTrue(entities.Contains(this.testIDs[0]));

            // Arrange
            this.cache.Cache(this.testIDs[1], this.testNumbers[1], this.testData[1]);

            // Act
            entities = new List<BadumnaId>(this.cache.GetEntitiesWithCachedMessages());

            // Assert
            Assert.AreEqual(2, entities.Count);
            Assert.IsTrue(entities.Contains(this.testIDs[0]));
            Assert.IsTrue(entities.Contains(this.testIDs[1]));

            // Arrange
            this.cache.Cache(this.testIDs[2], this.testNumbers[2], this.testData[2]);

            // Act
            entities = new List<BadumnaId>(this.cache.GetEntitiesWithCachedMessages());

            // Assert
            Assert.AreEqual(3, entities.Count);
            Assert.IsTrue(entities.Contains(this.testIDs[0]));
            Assert.IsTrue(entities.Contains(this.testIDs[1]));
            Assert.IsTrue(entities.Contains(this.testIDs[2]));
        }

        [Test]
        public void PopRemovesCachedMessagesForOnlyPoppedEntity()
        {
            // Arrange
            this.cache.Cache(this.testIDs[0], this.testNumbers[0], this.testData[0]);
            this.cache.Cache(this.testIDs[1], this.testNumbers[1], this.testData[1]);
            this.cache.Cache(this.testIDs[2], this.testNumbers[2], this.testData[2]);

            // Act
            var messages = this.cache.PopCachedMessages(this.testIDs[1]);

            // Assert
            var entities = new List<BadumnaId>(this.cache.GetEntitiesWithCachedMessages());
            Assert.AreEqual(2, entities.Count);
            Assert.IsTrue(entities.Contains(this.testIDs[0]));
            Assert.IsFalse(entities.Contains(this.testIDs[1]));
            Assert.IsTrue(entities.Contains(this.testIDs[2]));
        }

        [Test]
        public void PopReturnsAllCachedMessagesForEntity()
        {
            // Arrange
            this.cache.Cache(this.testIDs[0], this.testNumbers[0], this.testData[0]);
            this.cache.Cache(this.testIDs[0], this.testNumbers[1], this.testData[1]);
            this.cache.Cache(this.testIDs[1], this.testNumbers[0], this.testData[2]);

            // Act
            var messages = new List<CustomMessageCache.CachedCustomMessage>(
                this.cache.PopCachedMessages(this.testIDs[0]));

            // Assert
            Assert.AreEqual(2, messages.Count);
            Assert.AreEqual(this.testNumbers[0], messages[0].UpdateNumber);
            Assert.AreEqual(this.testData[0], messages[0].EventData);
            Assert.AreEqual(this.testNumbers[1], messages[1].UpdateNumber);
            Assert.AreEqual(this.testData[1], messages[1].EventData);
        }

        [Test]
        public void CanPopDuringIteration()
        {
            // Arrange
            this.cache.Cache(this.testIDs[0], this.testNumbers[0], this.testData[0]);
            this.cache.Cache(this.testIDs[1], this.testNumbers[1], this.testData[1]);
            this.cache.Cache(this.testIDs[2], this.testNumbers[2], this.testData[2]);

            // Act
            int popCount = 0;
            foreach (var id in this.cache.GetEntitiesWithCachedMessages())
            {
                var messages = this.cache.PopCachedMessages(id);
                popCount++;
            }

            // Assert
            Assert.AreEqual(3, popCount);
        }

        [Test]
        public void CollectGarbageDoesNotCollectMessagesForNewEntities()
        {
            // Arrange
            this.cache.Cache(this.testIDs[0], this.testNumbers[0], this.testData[0]);
            this.cache.Cache(this.testIDs[1], this.testNumbers[1], this.testData[1]);
            this.cache.Cache(this.testIDs[2], this.testNumbers[2], this.testData[2]);

            // Act
            this.cache.CollectGarbage();

            // Assert
            var entities = new List<BadumnaId>(this.cache.GetEntitiesWithCachedMessages());
            Assert.AreEqual(3, entities.Count);
        }

        [Test]
        public void CollectGarbageCollectsMessagesForOldEntities()
        {
            // Arrange
            this.cache.Cache(this.testIDs[0], this.testNumbers[0], this.testData[0]);
            this.cache.Cache(this.testIDs[1], this.testNumbers[1], this.testData[1]);
            this.cache.Cache(this.testIDs[2], this.testNumbers[2], this.testData[2]);
            this.cache.CollectGarbage();

            // Act
            this.cache.CollectGarbage();

            // Assert
            var entities = new List<BadumnaId>(this.cache.GetEntitiesWithCachedMessages());
            Assert.AreEqual(0, entities.Count);
        }
    }
}
