﻿//-----------------------------------------------------------------------
// <copyright file="ArbitrationManagerTests.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BadumnaTests.Arbitration
{
    using System;
    using System.Text;
    using Badumna;
    using Badumna.Arbitration;
    using Badumna.Core;
    using Badumna.Transport;
    using Badumna.Utilities;
    using BadumnaTests.Core;
    using NUnit.Framework;
    using Rhino.Mocks;

    [TestFixture]
    [DontPerformCoverage]
    public class ArbitrationManagerTests
    {
        private ArbitrationManager manager = null;
        private IArbitrationModule module = null;
        private IArbitrationServerFactory serverFactory = null;
        private IArbitrationServer server = null;
        private TransportProtocol transportProtocol = null;
        private NetworkEventQueue eventQueue;
        private QueueInvokable queueApplicationEvent;
        private ITime timeKeeper;
        private INetworkConnectivityReporter connectivityReporter;
        private IPeerConnectionNotifier connectionNotifier;

        [SetUp]
        public void SetUp()
        {
            this.eventQueue = new NetworkEventQueue();
            this.timeKeeper = this.eventQueue;

            this.transportProtocol = MockRepository.GenerateMock<TransportProtocol>(
                "foo",
                typeof(ConnectionlessProtocolMethodAttribute),
                new GenericCallBackReturn<object, Type>(MessageParserTester.DefaultFactory));
            this.module = MockRepository.GenerateMock<IArbitrationModule>();
            this.server = MockRepository.GenerateMock<IArbitrationServer>();
            this.serverFactory = MockRepository.GenerateMock<IArbitrationServerFactory>();
            this.serverFactory.Stub(f => f.Create(null, null, null)).IgnoreArguments().Return(this.server);
            this.connectivityReporter = MockRepository.GenerateMock<INetworkConnectivityReporter>();
            this.connectionNotifier = MockRepository.GenerateMock<IPeerConnectionNotifier>();
            this.queueApplicationEvent = i => i.Invoke();
            this.manager = new ArbitrationManager(
                this.transportProtocol,
                this.module,
                this.serverFactory,
                this.eventQueue,
                this.queueApplicationEvent,
                this.timeKeeper,
                this.connectivityReporter,
                this.connectionNotifier);
        }

        [TearDown]
        public void TearDown()
        {
            this.manager = null;
            this.module = null;
            this.server = null;
            this.serverFactory = null;
            this.transportProtocol = null;
        }

        //// Constructor

        [Test]
        [ExpectedException("System.ArgumentNullException")]
        public void ConstructorThrowsExceptionForNullTransportProtocol()
        {
            ArbitrationManager manager = new ArbitrationManager(null, this.module, this.eventQueue, this.queueApplicationEvent, this.timeKeeper, this.connectivityReporter, this.connectionNotifier);
        }

        [Test]
        [ExpectedException("System.ArgumentNullException")]
        public void ConstructorThrowsExceptionForNullArbitrationModule()
        {
            ArbitrationManager manager = new ArbitrationManager(this.transportProtocol, null, this.serverFactory, this.eventQueue, this.queueApplicationEvent, this.timeKeeper, this.connectivityReporter, this.connectionNotifier);
        }

        [Test]
        [ExpectedException("System.ArgumentNullException")]
        public void ConstructorThrowsExceptionForNullServerFactory()
        {
            ArbitrationManager manager = new ArbitrationManager(this.transportProtocol, this.module, null, this.eventQueue, this.queueApplicationEvent, this.timeKeeper, this.connectivityReporter, this.connectionNotifier);
        }

        [Test]
        public void TransportProtocolComponentRegistersMethodsOnConstruction()
        {
            // TODO: Test this behaviour?
        }

        //// TimeSpan ConnectionTimeout { get; set; }

        //// OnServiceBecomeOffline OnArbitrationServerBecomeOffline { get; set; }

        //// IArbitrator GetArbitrator(string name);

        [Test]
        [ExpectedException("System.InvalidOperationException")]
        public void GetArbitratorThrowsExceptionIfArbitrationModuleNotEnabled()
        {
            this.module.Stub(mod => mod.IsEnabled).Return(false);
            this.manager.GetArbitrator("foo");
        }

        [Test]
        [ExpectedException("System.InvalidOperationException")]
        public void GetArbitratorThrowsExceptionIfArbitrationModuleDoesNotSecifyNamedServer()
        {
            this.module.Stub(mod => mod.IsEnabled).Return(true);
            this.manager.GetArbitrator("foo");
        }

        [Test]
        public void GetArbitratorReturnsAribtrationClientWithNoAddressIfDistributedLookupIsEnabled()
        {
            ArbitrationServerDetails details = new ArbitrationServerDetails("foo");
            this.module.Stub(mod => mod.IsEnabled).Return(true);
            this.module.Stub(mod => mod.GetServerDetails("foo")).Return(details);
            ArbitrationClient arbitrator = (ArbitrationClient)this.manager.GetArbitrator("foo");
            Assert.AreEqual(arbitrator.ServerAddress, PeerAddress.Nowhere);
        }

        [Test]
        public void GetArbitratorReturnsAribtrationClientWithSpecifiedAddressIfDistributedLookupIsDisabled()
        {
            PeerAddress address = new PeerAddress(new System.Net.IPAddress(new byte[] { 10, 0, 0, 1 }), 42, NatType.Unknown);
            ArbitrationServerDetails details = new ArbitrationServerDetails("foo", "10.0.0.1:42");
            details.Validate();
            this.module.Stub(mod => mod.IsEnabled).Return(true);
            this.module.Stub(mod => mod.GetServerDetails("foo")).Return(details);
            ArbitrationClient arbitrator = (ArbitrationClient)this.manager.GetArbitrator("foo");
            Assert.AreEqual(arbitrator.ServerAddress, address);
        }

        //// void RegisterArbitrationHandler(HandleClientMessage clientEvent, TimeSpan disconnectTimeout, HandleClientDisconnect disconnect);

        [Test]
        public void RegisterArbitrationHandlerConfiguresServerClientEventHandler()
        {
            HandleClientMessage messageHandler = MockRepository.GenerateMock<HandleClientMessage>();
            this.server.Stub(s => s.ClientEvent).PropertyBehavior();
            this.manager.RegisterArbitrationHandler(messageHandler, new TimeSpan(), null);
            Assert.AreEqual(this.server.ClientEvent, messageHandler);
        }

        [Test]
        public void RegisterArbitrationHandlerConfiguresServerClientClientDisconnectHandler()
        {
            HandleClientDisconnect disconnectHandler = MockRepository.GenerateMock<HandleClientDisconnect>();
            this.server.Stub(s => s.Disconnect).PropertyBehavior();
            this.manager.RegisterArbitrationHandler(null, new System.TimeSpan(), disconnectHandler);
            Assert.AreEqual(this.server.Disconnect, disconnectHandler);
        }

        [Test]
        public void RegisterArbitrationHandlerConfiguresConnectionTimeout()
        {
            TimeSpan timeout = new TimeSpan(45, 34, 67);
            this.manager.RegisterArbitrationHandler(null, timeout, null);
            Assert.AreEqual(this.manager.ConnectionTimeout, timeout);
        }

        //// SendServerEvent

        [Test]
        public void SendServerEventPassesMessagesToServer()
        {
            int sessionId = 77;
            byte[] message = Encoding.UTF8.GetBytes("abc");
            this.server.Expect(s => s.SendEvent(sessionId, message));
            this.manager.SendServerEvent(sessionId, message);
        }

        //// SendServerRejectionEvent

        //// TODO SendServerRejectionEvent tests

        //// TODO: Need to create interface for Transport protocol to allow
        //// mocking for propert testing of following methods:
        //// SendClientInitialSequence
        //// SendClientEvent
        //// SendServerInitialSequence
        //// SendServerEvent
        
        //// GetUserIdForSession

        [Test]
        public void GetUserIdForSessionRequestPassedOnToServer()
        {
            int sessionId = 82;
            long userId = 567;
            this.server.Stub(s => s.GetUserIdForSession(sessionId)).Return(userId);
            Assert.AreEqual(userId, this.manager.GetUserIdForSession(sessionId));
        }
    }
}
