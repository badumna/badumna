﻿//-----------------------------------------------------------------------
// <copyright file="ArbitrationServerTests.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BadumnaTests.Arbitration
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Badumna.Arbitration;
    using Badumna.Core;
    using Badumna.DataTypes;
    using Badumna.Security;
    using Badumna.Utilities;
    using BadumnaTests.Chat;
    using NUnit.Framework;
    using Rhino.Mocks;

    [TestFixture]
    [DontPerformCoverage]
    public class ArbitrationServerTests
    {
        private IArbitrationManager manager = null;
        private ArbitrationServer server = null;
        private List<IArbitrationSession> sessions = new List<IArbitrationSession>();
        private IArbitrationSessionFactory factory = null;
        private List<PeerAddress> addresses = new List<PeerAddress>();
        private QueueInvokable queueApplicationEvent;
        private Simulator eventQueue;
        private ITime timeKeeper;

        [SetUp]
        public void SetUp()
        {
            this.eventQueue = new Simulator();
            this.timeKeeper = this.eventQueue;

            this.manager = MockRepository.GenerateMock<IArbitrationManager>();
            this.factory = MockRepository.GenerateStub<IArbitrationSessionFactory>();
            for (int i = 0; i < 2; ++i)
            {
                this.addresses.Add(new PeerAddress(
                    new System.Net.IPAddress(i),
                    1,
                    NatType.Open));
                IArbitrationSession session = MockRepository.GenerateMock<IArbitrationSession>();
                session.Stub(s => s.PeerAddress).Return(this.addresses[i]);
                this.sessions.Add(session);
                this.factory.Stub(f => f.Create(i, null, null, null, null, null)).IgnoreArguments().Return(this.sessions[i]).Repeat.Once();
            }

            this.queueApplicationEvent = i => i.Invoke();

            this.server = new ArbitrationServer(this.manager, this.factory, this.queueApplicationEvent, this.timeKeeper);
        }

        [TearDown]
        public void TearDown()
        {
            this.server = null;
            this.manager = null;
            this.factory = null;
            this.sessions.Clear();
            this.addresses.Clear();
        }

        [Test]
        public void SimpleConstructorThrowsNoExceptions()
        {
            ArbitrationServer server = new ArbitrationServer(this.manager, this.queueApplicationEvent, this.timeKeeper);
        }

        [Test]
        [ExpectedException("System.ArgumentNullException")]
        public void ArbitrationServerThrowsExceptionWhenConstructedWithNullManager()
        {
            ArbitrationServer server = new ArbitrationServer(null, this.factory, this.queueApplicationEvent, this.timeKeeper);
        }

        [Test]
        [ExpectedException("System.ArgumentNullException")]
        public void ArbitrationServerThrowsExceptionWhenConstructedWithNullSessionFactory()
        {
            ArbitrationServer server = new ArbitrationServer(this.manager, this.queueApplicationEvent, null);
        }

        [Test]
        public void CollectGarbageRemovesTimedOutSessions()
        {
            // NB testing session removal indirectly via userId request

            // Arrange: Session that has timed out.
            int userId = 99;
            CyclicalID.UShortID sequenceNumber = new CyclicalID.UShortID(0);
            this.server.ReceiveClientInitialSequence(this.addresses[0], UserFixture.CertificateFor(userId), sequenceNumber);
            this.manager.Stub(m => m.ConnectionTimeout).Return(TimeSpan.FromMilliseconds(10));
            this.eventQueue.RunFor(TimeSpan.FromMilliseconds(25));

            // Act
            this.server.CollectGarbage();

            // Assert
            Assert.IsNull(this.server.SessionForId(0));
        }

        [Test]
        public void RemovingTimedOutSessionsTriggersDisconnectHandlerCall()
        {
            // Arrange: Session that has timed out.
            int userId = 99;
            CyclicalID.UShortID sequenceNumber = new CyclicalID.UShortID(0);
            HandleClientDisconnect handler = MockRepository.GenerateMock<HandleClientDisconnect>();
            this.server.Disconnect = handler;
            this.server.ReceiveClientInitialSequence(this.addresses[0], UserFixture.CertificateFor(userId), sequenceNumber);
            this.manager.Stub(m => m.ConnectionTimeout).Return(TimeSpan.FromMilliseconds(10));
            handler.Expect(h => h.Invoke(0));
            this.eventQueue.RunFor(TimeSpan.FromMilliseconds(25));

            // Act
            this.server.CollectGarbage();

            // Assert
            handler.VerifyAllExpectations();
        }

        [Test]
        public void CollectGarbageDoesNotRemoveSessionsThatHaveNotTimedOut()
        {
            // NB testing session removal indirectly via userId request

            // Arrange: Session that has not timed out (unless your machine is crazily slow)
            int userId = 99;
            TimeSpan oneDayTimeout = new TimeSpan(0, 0, 1, 0, 0);
            CyclicalID.UShortID sequenceNumber = new CyclicalID.UShortID(0);
            this.server.ReceiveClientInitialSequence(this.addresses[0], UserFixture.CertificateFor(userId), sequenceNumber);
            this.manager.Stub(m => m.ConnectionTimeout).Return(oneDayTimeout);
            System.Threading.Thread.Sleep(20);

            // Act
            this.server.CollectGarbage();

            // Assert
            Assert.AreEqual(this.sessions[0], this.server.SessionForId(0));
        }

        [TestCase(13)]
        [TestCase(5)]
        [TestCase(8790)]
        public void GetUserIdForSessionReturnsMinusOneForUnknownSession(int sessionId)
        {
            CyclicalID.UShortID sequenceNumber = new CyclicalID.UShortID(0);
            this.server.ReceiveClientInitialSequence(this.addresses[0], UserFixture.CertificateFor(0), sequenceNumber);
            this.server.ReceiveClientInitialSequence(this.addresses[1], UserFixture.CertificateFor(1), sequenceNumber);
            Assert.AreEqual(-1, this.server.GetUserIdForSession(sessionId));
        }

        [Test]
        public void SessionIsRetrievedById()
        {
            CyclicalID.UShortID sequenceNumber = new CyclicalID.UShortID(0);
            this.server.ReceiveClientInitialSequence(this.addresses[0], UserFixture.CertificateFor(0), sequenceNumber);
            this.server.ReceiveClientInitialSequence(this.addresses[1], UserFixture.CertificateFor(1), sequenceNumber);
            Assert.AreEqual(this.sessions[0], this.server.SessionForId(0));
            Assert.AreEqual(this.sessions[1], this.server.SessionForId(1));
        }

        [Test]
        public void GetUserIdForSessionReturnsUserIdForKnownSession()
        {
            long userId = 123;
            this.sessions[0].Stub(s => s.UserId).Return(userId);

            CyclicalID.UShortID sequenceNumber = new CyclicalID.UShortID(0);
            this.server.ReceiveClientInitialSequence(this.addresses[0], UserFixture.CertificateFor(userId), sequenceNumber);
            Assert.AreEqual(userId, this.server.GetUserIdForSession(0));
        }

        [TestCase(13)]
        [TestCase(5)]
        [TestCase(8790)]
        public void GetCharacterForSessionReturnsNullForUnknownSession(int sessionId)
        {
            CyclicalID.UShortID sequenceNumber = new CyclicalID.UShortID(0);
            this.server.ReceiveClientInitialSequence(this.addresses[0], UserFixture.CertificateFor(0), sequenceNumber);
            this.server.ReceiveClientInitialSequence(this.addresses[1], UserFixture.CertificateFor(1), sequenceNumber);
            Assert.IsNull(this.server.GetCharacterForSession(sessionId));
        }

        [Test]
        public void GetCharacterForSessionReturnsCharacterForKnownSession()
        {
            Character character = new Character(1, "player1");
            this.sessions[0].Stub(s => s.Character).Return(character);

            CyclicalID.UShortID sequenceNumber = new CyclicalID.UShortID(0);
            var certificate = UserFixture.CertificateFor(character);
            this.server.ReceiveClientInitialSequence(this.addresses[0], certificate, sequenceNumber);
            Assert.AreEqual(character, this.server.GetCharacterForSession(0));
        }

        [TestCase(13)]
        [TestCase(5)]
        [TestCase(8790)]
        public void GetBadumnaIdForSessionReturnsNoneForUnknownSession(int sessionId)
        {
            CyclicalID.UShortID sequenceNumber = new CyclicalID.UShortID(0);
            this.server.ReceiveClientInitialSequence(this.addresses[0], UserFixture.CertificateFor(0), sequenceNumber);
            this.server.ReceiveClientInitialSequence(this.addresses[1], UserFixture.CertificateFor(1), sequenceNumber);
            Assert.AreEqual(BadumnaId.None, this.server.GetBadumnaIdForSession(sessionId));
        }

        [Test]
        public void GetBadumnaIdForSessionReturnsBadumnaIdForKnownSession()
        {
            CyclicalID.UShortID sequenceNumber = new CyclicalID.UShortID(0);
            this.server.ReceiveClientInitialSequence(this.addresses[0], UserFixture.CertificateFor(0), sequenceNumber);
            this.server.ReceiveClientInitialSequence(this.addresses[1], UserFixture.CertificateFor(1), sequenceNumber);
            Assert.AreEqual(this.addresses[0], this.server.GetBadumnaIdForSession(0).Address);
            Assert.AreEqual(this.addresses[1], this.server.GetBadumnaIdForSession(1).Address);
        }

        [Test]
        [ExpectedException("System.ArgumentNullException")]
        public void SendEventThrowsExceptionForNullMessage()
        {
            this.server.SendEvent(0, null);
        }

        [TestCase(0)]
        [TestCase(1)]
        public void SendEventSendsMessageToSessionIfItExists(int sessionId)
        {
            // Arrange: Two existing session.
            CyclicalID.UShortID sequenceNumber = new CyclicalID.UShortID(0);
            byte[] message = Encoding.UTF8.GetBytes("Hello");
            this.server.ReceiveClientInitialSequence(this.addresses[0], UserFixture.CertificateFor(0), sequenceNumber);
            this.server.ReceiveClientInitialSequence(this.addresses[1], UserFixture.CertificateFor(1), sequenceNumber);

            // Expect: Session told to send message.
            this.sessions[sessionId].Expect(s => s.SendMessage(message));

            // Act: Receive a message from connected client.
            this.server.SendEvent(sessionId, message);

            // Assert
            this.sessions[sessionId].VerifyAllExpectations();
        }

        [Test]
        [ExpectedException("System.InvalidOperationException")]
        public void SendEventThrowsExceptionIfSessionDoesNotExist()
        {
            // Arrange: Two existing session.
            CyclicalID.UShortID sequenceNumber = new CyclicalID.UShortID(0);
            byte[] message = Encoding.UTF8.GetBytes("Hello");
            this.server.ReceiveClientInitialSequence(this.addresses[0], UserFixture.CertificateFor(0), sequenceNumber);
            this.server.ReceiveClientInitialSequence(this.addresses[1], UserFixture.CertificateFor(1), sequenceNumber);

            // Expect: Exception
            // Act: Receive a message from connected client.
            this.server.SendEvent(2, message);
        }

        [Test]
        public void ReceiveClientInitialSequenceInitializesNewSessionForNewClients()
        {
            // Arrange: No sessions.
            // Expect: Expect two sessions to be initialized.
            CyclicalID.UShortID sequenceNumber = new CyclicalID.UShortID(0);
            this.sessions[0].Expect(s => s.Initialize(sequenceNumber));
            this.sessions[1].Expect(s => s.Initialize(sequenceNumber));

            // Act: Receive initial sequences from two different peers.
            this.server.ReceiveClientInitialSequence(this.addresses[0], UserFixture.CertificateFor(0), sequenceNumber);
            this.server.ReceiveClientInitialSequence(this.addresses[1], UserFixture.CertificateFor(0), sequenceNumber);

            // Assert
            this.sessions[0].VerifyAllExpectations();
            this.sessions[1].VerifyAllExpectations();
        }

        [Test]
        public void ReceiveClientInitialSequenceInitializesExistingSessionForExistingClient()
        {
            // Arrange: Start a session.
            CyclicalID.UShortID sequenceNumber = new CyclicalID.UShortID(0);
            this.server.ReceiveClientInitialSequence(this.addresses[0], UserFixture.CertificateFor(0), sequenceNumber);

            // Expect: session to be re-initialized
            this.sessions[0].Expect(s => s.Initialize(sequenceNumber));

            // Act: Receive initial sequence from existing client.
            this.server.ReceiveClientInitialSequence(this.addresses[0], UserFixture.CertificateFor(0), sequenceNumber);

            // Assert
            this.sessions[0].VerifyAllExpectations();
        }

        [Test]
        public void ReceiveClientArbitrationEventSendsMessageToSessionIfItExists()
        {
            // Arrange: Start a session.
            CyclicalID.UShortID sequenceNumber = new CyclicalID.UShortID(0);
            byte[] message = Encoding.UTF8.GetBytes("Hello");
            this.server.ReceiveClientInitialSequence(this.addresses[0], UserFixture.CertificateFor(0), sequenceNumber);

            // Expect: Message to be passed to session.
            this.sessions[0].Expect(s => s.ReceiveMessage(sequenceNumber, message));

            // Act: Receive message from existing client.
            this.server.ReceiveClientArbitrationEvent(this.addresses[0], sequenceNumber, message);

            // Assert
            this.sessions[0].VerifyAllExpectations();
        }

        [Test]
        public void ReceiveClientArbitrationEventWarnsClientIfNoSessionExists()
        {
            // Arrange: No sessions.
            CyclicalID.UShortID sequenceNumber = new CyclicalID.UShortID(0);
            byte[] message = Encoding.UTF8.GetBytes("Hello");

            // Expect: Rejection message to be sent via manager.
            this.manager.Expect(
                m => m.SendServerRejectionEvent(this.addresses[0], sequenceNumber, message));

            // Act: Receive message from client with no session.
            this.server.ReceiveClientArbitrationEvent(this.addresses[0], sequenceNumber, message);

            // Assert
            this.manager.VerifyAllExpectations();
        }

        //// Integration Tests

        [Test]
        public void ReceivedMessagesPassedToApplicationHandler()
        {
            byte[] message = Encoding.UTF8.GetBytes("Hello");
            CyclicalID.UShortID seqNum = new CyclicalID.UShortID(0);
            HandleClientMessage handler = MockRepository.GenerateMock<HandleClientMessage>();
            ArbitrationServer server = new ArbitrationServer(this.manager, this.queueApplicationEvent, this.timeKeeper);
            server.ClientEvent = handler;
            handler.Expect(h => h.Invoke(0, message));
            server.ReceiveClientInitialSequence(this.addresses[0], UserFixture.CertificateFor(0), seqNum);
            server.ReceiveClientArbitrationEvent(this.addresses[0], seqNum, message);
            handler.VerifyAllExpectations();
        }

        [Test]
        public void SentMessagesSentViaManager()
        {
            byte[] message = Encoding.UTF8.GetBytes("Hello");
            CyclicalID.UShortID seqNum = new CyclicalID.UShortID(0);
            ArbitrationServer server = new ArbitrationServer(this.manager, this.queueApplicationEvent, this.timeKeeper);
            this.manager.Expect(
                m => m.SendServerEvent(this.addresses[0], seqNum, message));
            server.ReceiveClientInitialSequence(this.addresses[0], UserFixture.CertificateFor(0), seqNum);
            server.SendEvent(0, message);
            this.manager.VerifyAllExpectations();
        }
    }
}
