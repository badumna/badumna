﻿//-----------------------------------------------------------------------
// <copyright file="ArbitrationEventSetTests.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BadumnaTests.Arbitration
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using Badumna.Arbitration;
    using Badumna.Core;
    using NUnit.Framework;

    [TestFixture]
    [DontPerformCoverage]
    public class ArbitrationEventSetTests
    {
        [SetUp]
        public void SetUp()
        {
        }

        [TearDown]
        public void TearDown()
        {
        }

        [Test]
        public void RegisteredEventsSerializeAndDeserializeCorrectly()
        {
            ArbitrationEventSet set = new ArbitrationEventSet();
            set.Register(new Type[] { typeof(TestArbitrationEvent1), typeof(TestArbitrationEvent2) });
            byte[] data = set.Serialize(new TestArbitrationEvent1(89));
            TestArbitrationEvent1 event1 = set.Deserialize(data) as TestArbitrationEvent1;
            Assert.AreEqual(event1.Datum, 89);
            data = set.Serialize(new TestArbitrationEvent2(34));
            TestArbitrationEvent2 event2 = set.Deserialize(data) as TestArbitrationEvent2;
            Assert.AreEqual(event2.Datum, 34);
        }

        [Test]
        public void VariableLengthEventsSerializeAndDeserializeCorrectly()
        {
            ArbitrationEventSet set = new ArbitrationEventSet();
            set.Register(new Type[] { typeof(ArrayArbitrationEvent) });

            byte[] data = set.Serialize(new ArrayArbitrationEvent(new int[] { 1, 2, 3 }));
            ArrayArbitrationEvent event1 = set.Deserialize(data) as ArrayArbitrationEvent;
            Assert.AreEqual(new List<int>(new int[] { 1, 2, 3 }), event1.Numbers);

            data = set.Serialize(new ArrayArbitrationEvent(new int[] { 111, 222, 333, 444, 555, 666 }));
            ArrayArbitrationEvent event2 = set.Deserialize(data) as ArrayArbitrationEvent;
            Assert.AreEqual(new List<int>(new int[] { 111, 222, 333, 444, 555, 666 }), event2.Numbers);
        }

        [Test]
        public void MoreVariableLengthEventsSerializeAndDeserializeCorrectly()
        {
            ArbitrationEventSet set = new ArbitrationEventSet();
            set.Register(new Type[] { typeof(VariableLengthArbitrationEvent) });

            string myString = Guid.NewGuid().ToString();
            DateTime myDateTime = DateTime.Today;

            byte[] data = set.Serialize(new VariableLengthArbitrationEvent(myString, myDateTime));
            VariableLengthArbitrationEvent event1 = set.Deserialize(data) as VariableLengthArbitrationEvent;
            Assert.AreEqual(myString, event1.MyString);
            Assert.AreEqual(myDateTime, event1.MyDateTime);
        }

        [Test]
        public void EventsSerializeCorrectlyWhenMoreThan256AreRegistered()
        {
            ArbitrationEventSet set = new ArbitrationEventSet();
            set.Register(new Type[] { typeof(TestArbitrationEvent1), typeof(TestArbitrationEvent2) });
            for (int i = 0; i < 257; i++)
            {
                set.Register(new Type[] { typeof(TestArbitrationEvent1) });
            }

            byte[] data = set.Serialize(new TestArbitrationEvent1(345));
            TestArbitrationEvent1 event1 = set.Deserialize(data) as TestArbitrationEvent1;
            Assert.AreEqual(event1.Datum, 345);
        }

        [Test]
        [ExpectedException("System.InvalidOperationException")]
        public void CannotRegisterEventTypesAfterSetUse()
        {
            ArbitrationEventSet set = new ArbitrationEventSet();
            set.Register(new Type[] { typeof(TestArbitrationEvent1) });
            byte[] data = set.Serialize(new TestArbitrationEvent1(987));
            set.Register(new Type[] { typeof(TestArbitrationEvent2) });
        }

        [Test]
        [ExpectedException("System.ArgumentException")]
        public void CannotRegisterEventTypesThatDoNotDeriveFromArbitrationEvent()
        {
            ArbitrationEventSet set = new ArbitrationEventSet();
            set.Register(new Type[] { typeof(int) });
        }

        [Test]
        [ExpectedException("System.ArgumentException")]
        public void CannotRegisterEventTypesThatDoNotImplementCorrectConstructor()
        {
            ArbitrationEventSet set = new ArbitrationEventSet();
            set.Register(new Type[] { typeof(BadArbitrationEvent) });
        }

        [Test]
        public void EventShorterThanClaimedSerializedLengthSerializesAndDeserializesCorrectly()
        {
            ArbitrationEventSet set = new ArbitrationEventSet();
            set.Register(new Type[] { typeof(ArbitrationEventWithBadLength) });
            ArbitrationEventWithBadLength event1 = new ArbitrationEventWithBadLength("1");
            byte[] message = set.Serialize(event1);
            ArbitrationEventWithBadLength event2 = (ArbitrationEventWithBadLength)set.Deserialize(message);
            Assert.AreEqual(event1.Contents, event2.Contents);
        }

        [Test]
        [ExpectedException(typeof(InvalidOperationException))]
        public void EventLongerThanClaimedSerializedLengthSerializesAndDeserializesCorrectly()
        {
            ArbitrationEventSet set = new ArbitrationEventSet();
            set.Register(new Type[] { typeof(ArbitrationEventWithBadLength) });
            ArbitrationEventWithBadLength event1 = new ArbitrationEventWithBadLength("12345678");
            byte[] message = set.Serialize(event1);
        }

        private class TestArbitrationEvent1 : ArbitrationEvent
        {
            private ulong datum = 0;

            public TestArbitrationEvent1(ulong datum)
            {
                this.datum = datum;
            }

            public TestArbitrationEvent1(BinaryReader reader)
            {
                this.datum = reader.ReadUInt64();
            }

            public ulong Datum
            {
                get { return this.datum; }
            }

            public override int SerializedLength
            {
                get
                {
                    return (int)sizeof(ulong);
                }
            }

            public override void Serialize(BinaryWriter writer)
            {
                writer.Write(this.datum);
            }
        }

        private class TestArbitrationEvent2 : ArbitrationEvent
        {
            private ushort datum = 0;

            public TestArbitrationEvent2(ushort datum)
            {
                this.datum = datum;
            }

            public TestArbitrationEvent2(BinaryReader reader)
            {
                this.datum = reader.ReadUInt16();
            }

            public ushort Datum
            {
                get { return this.datum; }
            }

            public override int SerializedLength
            {
                get
                {
                    return (int)sizeof(ushort);
                }
            }

            public override void Serialize(BinaryWriter writer)
            {
                writer.Write(this.datum);
            }
        }

        private class ArrayArbitrationEvent : ArbitrationEvent
        {
            private List<int> numbers = new List<int>();

            public ArrayArbitrationEvent(int[] numbers)
            {
                this.numbers.AddRange(numbers);
            }

            public ArrayArbitrationEvent(BinaryReader reader)
            {
                ushort length = reader.ReadUInt16();
                for (ushort i = 0; i < length; i++)
                {
                    this.numbers.Add(reader.ReadInt32());
                }
            }

            public List<int> Numbers
            {
                get
                {
                    return this.numbers;
                }
            }

            public override void Serialize(BinaryWriter writer)
            {
                writer.Write((ushort)this.numbers.Count);
                foreach (int val in this.numbers)
                {
                    writer.Write(val);
                }
            }
        }

        private class VariableLengthArbitrationEvent : ArbitrationEvent
        {
            private string myString;
            private DateTime myDateTime;

            public VariableLengthArbitrationEvent(string myString, DateTime myDateTime)
            {
                this.myString = myString;
                this.myDateTime = myDateTime;
            }

            public VariableLengthArbitrationEvent(BinaryReader reader)
            {
                this.myString = reader.ReadString();
                this.myDateTime = DateTime.Parse(
                    reader.ReadString(),
                    System.Globalization.CultureInfo.InvariantCulture,
                    System.Globalization.DateTimeStyles.RoundtripKind);
            }

            public string MyString
            {
                get
                {
                    return this.myString;
                }
            }

            public DateTime MyDateTime
            {
                get
                {
                    return this.myDateTime;
                }
            }

            public override void Serialize(BinaryWriter writer)
            {
                writer.Write(this.myString);
                writer.Write(this.myDateTime.ToString(
                    "o",
                    System.Globalization.CultureInfo.InvariantCulture));
            }
        }

        private class BadArbitrationEvent : ArbitrationEvent
        {
            public override void Serialize(BinaryWriter writer)
            {
            }
        }

        private class ArbitrationEventWithBadLength : ArbitrationEvent
        {
            private string contents;

            public ArbitrationEventWithBadLength(string contents)
            {
                this.contents = contents;
            }

            public ArbitrationEventWithBadLength(BinaryReader reader)
            {
                this.contents = reader.ReadString();
            }

            public string Contents
            {
                get
                {
                    return this.contents;
                }
            }

            public override int SerializedLength
            {
                get
                {
                    return (int)sizeof(long);
                }
            }

            public override void Serialize(BinaryWriter writer)
            {
                writer.Write(this.contents);
            }
        }
    }
}
