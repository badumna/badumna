﻿//-----------------------------------------------------------------------
// <copyright file="ArbitrationSessionTests.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BadumnaTests.Arbitration
{
    using System.Text;
    using Badumna.Arbitration;
    using Badumna.Core;
    using Badumna.Security;
    using Badumna.Utilities;
    using BadumnaTests.Chat;
    using NUnit.Framework;
    using Rhino.Mocks;

    [TestFixture]
    [DontPerformCoverage]
    public class ArbitrationSessionTests
    {
        private ArbitrationSession session;
        private ISequencer<byte[]> mockSequencer;
        private PeerAddress dummyAddress = new PeerAddress(
            new System.Net.IPAddress(1),
            1,
            NatType.Open);

        private ITime timeKeeper;

        [SetUp]
        public void SetUp()
        {
            this.timeKeeper = new NetworkEventQueue();

            this.mockSequencer = MockRepository.GenerateMock<ISequencer<byte[]>>();
            GenericCallBack<byte[]> handler = MockRepository.GenerateMock<GenericCallBack<byte[]>>();
            GenericCallBack<CyclicalID.UShortID, byte[]> sender =
                MockRepository.GenerateMock<GenericCallBack<CyclicalID.UShortID, byte[]>>();
            SequencerFactory<byte[]> sequencerFactory =
                MockRepository.GenerateStub<SequencerFactory<byte[]>>();
            sequencerFactory.Stub(f => f.Invoke(null)).IgnoreArguments().Return(this.mockSequencer);
            this.session = new ArbitrationSession(
                0,
                new PeerAddress(),
                UserFixture.CertificateFor("user123"),
                sequencerFactory,
                handler,
                sender,
                this.timeKeeper);
        }

        [TearDown]
        public void TearDown()
        {
            this.session = null;
        }

        [Test]
        [ExpectedException("System.ArgumentNullException")]
        public void ConstructorThrowsExceptionForNullPeerAddress()
        {
            ArbitrationSession session = new ArbitrationSession(
                0,
                null,
                MockRepository.GenerateMock<ICertificateToken>(),
                MockRepository.GenerateMock<SequencerFactory<byte[]>>(),
                MockRepository.GenerateMock<GenericCallBack<byte[]>>(),
                MockRepository.GenerateMock<GenericCallBack<CyclicalID.UShortID, byte[]>>(),
                this.timeKeeper);
        }

        [Test]
        [ExpectedException("System.ArgumentNullException")]
        public void ConstructorThrowsExceptionForNullSequencerFactory()
        {
            ArbitrationSession session = new ArbitrationSession(
                0,
                new PeerAddress(),
                MockRepository.GenerateMock<ICertificateToken>(),
                null,
                MockRepository.GenerateMock<GenericCallBack<byte[]>>(),
                MockRepository.GenerateMock<GenericCallBack<CyclicalID.UShortID, byte[]>>(),
                this.timeKeeper);
        }

        [Test]
        [ExpectedException("System.ArgumentNullException")]
        public void ConstructorThrowsExceptionForNullHandler()
        {
            ArbitrationSession session = new ArbitrationSession(
                0,
                new PeerAddress(),
                MockRepository.GenerateMock<ICertificateToken>(),
                MockRepository.GenerateMock<SequencerFactory<byte[]>>(),
                null,
                MockRepository.GenerateMock<GenericCallBack<CyclicalID.UShortID, byte[]>>(),
                this.timeKeeper);
        }

        [Test]
        [ExpectedException("System.ArgumentNullException")]
        public void ConstructorThrowsExceptionForNullSender()
        {
            ArbitrationSession session = new ArbitrationSession(
                0,
                new PeerAddress(),
                MockRepository.GenerateMock<ICertificateToken>(),
                MockRepository.GenerateMock<SequencerFactory<byte[]>>(),
                MockRepository.GenerateMock<GenericCallBack<byte[]>>(),
                null,
                this.timeKeeper);
        }

        [Test]
        public void SequencerIsInitializedOnInitialization()
        {
            // Arrange
            CyclicalID.UShortID sequenceNumber = new CyclicalID.UShortID();
            this.mockSequencer.Expect(
                seq => seq.Initialize(sequenceNumber))
                .Repeat.Once();

            // Act
            this.session.Initialize(sequenceNumber);
            
            // Assert
            this.mockSequencer.VerifyAllExpectations();
        }

        [Test]
        public void UserPropertiesAreExtractedFromCertificate()
        {
            var userId = 123;
            var character = UserFixture.CharacterNamed("char1");
            ArbitrationSession session = new ArbitrationSession(
                0,
                new PeerAddress(),
                UserFixture.CertificateFor(userId, character),
                MockRepository.GenerateMock<SequencerFactory<byte[]>>(),
                MockRepository.GenerateMock<GenericCallBack<byte[]>>(),
                MockRepository.GenerateMock<GenericCallBack<CyclicalID.UShortID, byte[]>>(),
                this.timeKeeper);

            Assert.AreEqual(userId, session.UserId);
            Assert.AreEqual(character, session.Character);
        }

        [Test]
        public void MessagesArePassedToSequencer()
        {
            // Arrange
            CyclicalID.UShortID id = new CyclicalID.UShortID(0);
            byte[] message = Encoding.UTF8.GetBytes("Hello");
            this.session.Initialize(id);
            this.mockSequencer.Expect(
                seq => seq.ReceiveEvent(id, message));
            
            // Act
            this.session.ReceiveMessage(id, message);

            // Assert
            this.mockSequencer.VerifyAllExpectations();
        }
    }
}
