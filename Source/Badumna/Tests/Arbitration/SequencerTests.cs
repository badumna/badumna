﻿//-----------------------------------------------------------------------
// <copyright file="SequencerTests.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BadumnaTests.Arbitration
{
    using System.Diagnostics;
    using Badumna.Arbitration;
    using Badumna.Core;
    using Badumna.Utilities;
    using NUnit.Framework;
    using Rhino.Mocks;

    [TestFixture]
    [DontPerformCoverage]
    public class SequencerTests
    {
        private GenericCallBack<int> handler;
        private Sequencer<int> sequencer;
        private ITime timeKeeper;

        [SetUp]
        public void SetUp()
        {
            this.handler = MockRepository.GenerateMock<GenericCallBack<int>>();
            this.timeKeeper = new NetworkEventQueue();
            this.sequencer = new Sequencer<int>(this.handler);
        }

        [TearDown]
        public void TearDown()
        {
            this.handler = null;
            this.sequencer = null;
        }

        [TestCase(0)]
        [TestCase(1)]
        [TestCase(2)]
        public void ReceivedEventIsHandled(int message)
        {
            this.handler.Expect(h => h(message)).Repeat.Once();
            CyclicalID.UShortID messageId = new CyclicalID.UShortID(0);
            this.sequencer.Initialize(messageId);
            this.sequencer.ReceiveEvent(messageId, message);
            this.handler.VerifyAllExpectations();
        }

        [Test]
        public void OutOfOrderEventIsNotHandledImmediately()
        {
            CyclicalID.UShortID messageId = new CyclicalID.UShortID(0);
            this.sequencer.Initialize(messageId);
            messageId.Increment();
            this.handler.Expect(h => h(0)).Repeat.Never();
            this.sequencer.ReceiveEvent(messageId, 0);
            this.handler.VerifyAllExpectations();
        }

        [Test]
        public void OutOfOrderEventIsHandledInOrder()
        {
            // NB Must use old Rhino Mocks syntax to enforce order
            var mocks = new MockRepository();
            var handler = mocks.DynamicMock<GenericCallBack<int>>();
            using (mocks.Ordered())
            {
                handler.Expect(h => h(1));
                handler.Expect(h => h(2));
            }
            
            handler.Replay();

            CyclicalID.UShortID messageId = new CyclicalID.UShortID(0);
            Sequencer<int> sequencer = new Sequencer<int>(handler);
            sequencer.Initialize(messageId);
            messageId.Increment();
            sequencer.ReceiveEvent(messageId, 2);
            sequencer.ReceiveEvent(messageId.Previous, 1);
            handler.VerifyAllExpectations();
        }

        [Test]
        public void StaleEventsAreDiscarded()
        {
            CyclicalID.UShortID messageId = new CyclicalID.UShortID(0);
            this.handler.Expect(h => h(1)).Repeat.Never();
            this.handler.Expect(h => h(2)).Repeat.Never();
            this.handler.Expect(h => h(3)).Repeat.Once();

            this.sequencer.Initialize(messageId); // Expect seq 0
            messageId.Increment();
            this.sequencer.ReceiveEvent(messageId, 1); // seq 1
            messageId.Increment();
            this.sequencer.ReceiveEvent(messageId, 2); // seq 2
            messageId.Increment();
            this.sequencer.Initialize(messageId); // Expect seq 3
            this.sequencer.ReceiveEvent(messageId, 3); // seq 3
            this.handler.VerifyAllExpectations();
        }

        [Test]
        public void OldMessagesAreIgnored()
        {
            CyclicalID.UShortID messageId = new CyclicalID.UShortID(2);
            this.handler.Expect(h => h(0)).Repeat.Never();
            this.handler.Expect(h => h(1)).Repeat.Never();
            this.handler.Expect(h => h(2)).Repeat.Once();

            this.sequencer.Initialize(messageId); // Expect seq 2
            // Send old messages
            messageId = new CyclicalID.UShortID(0);
            this.sequencer.ReceiveEvent(messageId, 0); // seq 0
            messageId.Increment();
            this.sequencer.ReceiveEvent(messageId, 1); // seq 1
            messageId.Increment();
            this.sequencer.ReceiveEvent(messageId, 2); // seq 2
            this.handler.VerifyAllExpectations();
        }
    }
}
