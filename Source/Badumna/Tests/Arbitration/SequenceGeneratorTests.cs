﻿//-----------------------------------------------------------------------
// <copyright file="SequenceGeneratorTests.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BadumnaTests.Arbitration
{
    using System.Collections.Generic;
    using System.Timers;
    using Badumna.Arbitration;
    using Badumna.Core;
    using Badumna.Utilities;
    using NUnit.Framework;

    [TestFixture]
    [DontPerformCoverage]
    public class SequenceGeneratorTests
    {
        private SequenceGenerator sequenceGenerator;
        private Dictionary<CyclicalID.UShortID, int> sequenceNumberCount;
        private Timer scheduler;

        [SetUp]
        public void SetUp()
        {
            this.sequenceGenerator = new SequenceGenerator();
            this.sequenceNumberCount = new Dictionary<CyclicalID.UShortID, int>();
        }

        [TearDown]
        public void TearDown()
        {
            this.sequenceGenerator = null;
            this.sequenceNumberCount = null;
        }

        [Test]
        public void ContiguouslyIncrementingNumbersGenerated()
        {
            CyclicalID.UShortID number1 = this.sequenceGenerator.ConsumeSequenceNumber();
            CyclicalID.UShortID number2 = this.sequenceGenerator.ConsumeSequenceNumber();
            number1.Increment();
            Assert.AreEqual(number1, number2);
        }

        [Test]
        public void FirstNumberServedIsNextNumber()
        {
            CyclicalID.UShortID initialNumber = this.sequenceGenerator.NextSequenceNumber;
            CyclicalID.UShortID number1 = this.sequenceGenerator.ConsumeSequenceNumber();
            Assert.AreEqual(number1, initialNumber);
        }

        [Test]
        public void ResetResetsSequence()
        {
            ushort newSequenceNumber = 99;
            this.sequenceGenerator.Reset(newSequenceNumber);
            Assert.AreEqual(newSequenceNumber, this.sequenceGenerator.NextSequenceNumber.Value);
            Assert.AreEqual(newSequenceNumber, this.sequenceGenerator.ConsumeSequenceNumber().Value);
        }

        [Test]
        [Category("Slow")]
        public void ConcurrentAccessYieldsNoDuplicates()
        {
            try
            {
                this.scheduler = new Timer(1);
                this.scheduler.Elapsed += new System.Timers.ElapsedEventHandler(this.GetSequenceNumber);
                this.scheduler.Enabled = true;
                System.Threading.Thread.Sleep(5000);
                this.scheduler.Enabled = false;
                this.scheduler.Elapsed -= new System.Timers.ElapsedEventHandler(this.GetSequenceNumber);
                ////var multiples =
                ////    from entry in sequenceNumberCount
                ////    where (entry.Value > 1)
                ////    select entry.Key;
                ////Assert.AreEqual(0,multiples.Count());
            }
            finally
            {
                if (this.scheduler != null)
                {
                    this.scheduler.Enabled = false;
                    this.scheduler = null;
                }
            }
        }

        private void GetSequenceNumber(object sender, System.Timers.ElapsedEventArgs e)
        {
            CyclicalID.UShortID number = this.sequenceGenerator.ConsumeSequenceNumber();
            if (this.sequenceNumberCount != null)
            {
                if (this.sequenceNumberCount.ContainsKey(number))
                {
                    this.sequenceNumberCount[number]++;
                }
                else
                {
                    this.sequenceNumberCount[number] = 1;
                }
            }
        }
    }
}
