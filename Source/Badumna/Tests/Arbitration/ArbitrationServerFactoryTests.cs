﻿//-----------------------------------------------------------------------
// <copyright file="ArbitrationServerFactoryTests.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BadumnaTests.Arbitration
{
    using Badumna.Arbitration;
    using Badumna.Core;
    using Badumna.Utilities;
    using NUnit.Framework;
    using Rhino.Mocks;

    [TestFixture]
    [DontPerformCoverage]
    public class ArbitrationServerFactoryTests
    {
        [Test]
        public void ArbitrationServerFactoryReturnsValidArbitrationServer()
        {
            ArbitrationServerFactory factory = new ArbitrationServerFactory();
            IArbitrationServer server = factory.Create(
                MockRepository.GenerateMock<IArbitrationManager>(),
                i => i.Invoke(),
                new NetworkEventQueue());
            Assert.IsTrue(server is ArbitrationServer);
        }
    }
}
