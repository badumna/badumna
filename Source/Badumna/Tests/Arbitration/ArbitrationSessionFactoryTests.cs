﻿//-----------------------------------------------------------------------
// <copyright file="ArbitrationSessionFactoryTests.cs" company="Scalify Pty Ltd">
//     Copyright (c) 2010 Scalify Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BadumnaTests.Arbitration
{
    using Badumna.Arbitration;
    using Badumna.Core;
    using Badumna.Utilities;
    using NUnit.Framework;
    using Rhino.Mocks;

    [TestFixture]
    [DontPerformCoverage]
    public class ArbitrationSessionFactoryTests
    {
        [SetUp]
        public void SetUp()
        {
        }

        [TearDown]
        public void TearDown()
        {
        }

        [Test]
        [ExpectedException("System.ArgumentNullException")]
        public void ConstructorThrowsExceptionForNullSequenceFactory()
        {
            ArbitrationSessionFactory asf = new ArbitrationSessionFactory(null);
        }
    }
}
