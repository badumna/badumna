﻿//-----------------------------------------------------------------------
// <copyright file="ArbitrationClientTests.cs" company="NICTA">
//     Copyright (c) 2010 NICTA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

[assembly: System.Runtime.CompilerServices.InternalsVisibleTo(Rhino.Mocks.RhinoMocks.StrongName)]

namespace BadumnaTests.Arbitration
{
    using System.Text;
    using Badumna;
    using Badumna.Arbitration;
    using Badumna.Core;
    using Badumna.ServiceDiscovery;
    using Badumna.Utilities;
    using NUnit.Framework;
    using Rhino.Mocks;

    [TestFixture]
    [DontPerformCoverage]
    public class ArbitrationClientTests
    {
        private PeerAddress dummyAddress = new PeerAddress(
            new System.Net.IPAddress(1),
            1,
            NatType.Open);

        private ServiceDescription description = new ServiceDescription(
            ServerType.Arbitration,
            "Test service type",
            "Test attribute");

        private ArbitrationClient clientWithNoServer = null;

        private ArbitrationClient clientWithServer = null;

        private IArbitrationManager manager = null;

        private SequencerFactory<byte[]> sequencerFactory = null;

        private ISequencer<byte[]> sequencer = null;

        private ArbitrationConnectionResultHandler connectionResultHandler = null;
        
        private HandleConnectionFailure connectionFailedHandler = null;
        
        private HandleServerMessage serverEventHandler = null;

        private QueueInvokable queueApplicationEvent;

        private Simulator eventQueue;

        private ITime timeKeeper;

        private IArbitrationModule module;

        [SetUp]
        public void SetUp()
        {
            this.eventQueue = new Simulator();
            this.timeKeeper = this.eventQueue;

            this.queueApplicationEvent = i => i.Invoke();

            this.sequencer = MockRepository.GenerateMock<ISequencer<byte[]>>();
            this.sequencerFactory =
                MockRepository.GenerateStub<SequencerFactory<byte[]>>();
            this.sequencerFactory.Stub(f => f.Invoke(null)).IgnoreArguments().Return(this.sequencer);
            this.manager = MockRepository.GenerateMock<IArbitrationManager>();
            this.module = MockRepository.GenerateMock<IArbitrationModule>();
            this.clientWithNoServer = new ArbitrationClient(
                this.manager,
                PeerAddress.Nowhere,
                this.description,
                this.sequencerFactory,
                this.module,
                this.eventQueue,
                this.queueApplicationEvent,
                this.timeKeeper);

            this.clientWithServer = new ArbitrationClient(
                this.manager,
                this.dummyAddress,
                this.description,
                this.sequencerFactory,
                this.module,
                this.eventQueue,
                this.queueApplicationEvent,
                this.timeKeeper);

            this.connectionResultHandler = MockRepository.GenerateMock<ArbitrationConnectionResultHandler>();
            this.connectionFailedHandler = MockRepository.GenerateMock<HandleConnectionFailure>();
            this.serverEventHandler = MockRepository.GenerateMock<HandleServerMessage>();
        }

        [TearDown]
        public void TearDown()
        {
            this.manager = null;
            this.clientWithServer = null;
            this.clientWithNoServer = null;
            this.sequencerFactory = null;
            this.sequencer = null;
            this.connectionResultHandler = null;
            this.connectionFailedHandler = null;
            this.serverEventHandler = null;
        }

        //// Constructor

        [Test]
        public void SimpleConstructorThrowsNoExceptions()
        {
            ArbitrationClient client = new ArbitrationClient(
                this.manager,
                this.dummyAddress,
                this.description,
                this.module,
                this.eventQueue,
                this.queueApplicationEvent,
                this.timeKeeper);
        }

        [Test]
        [ExpectedException("System.ArgumentNullException")]
        public void ConstructorThrowsWhenPassedNullManager()
        {
            ArbitrationClient client = new ArbitrationClient(
                null,
                this.dummyAddress,
                this.description,
                this.sequencerFactory,
                this.module,
                this.eventQueue,
                this.queueApplicationEvent,
                this.timeKeeper);
        }

        [Test]
        [ExpectedException("System.ArgumentNullException")]
        public void ConstructorThrowsWhenPassedNullSequencer()
        {
            ArbitrationClient client = new ArbitrationClient(
                this.manager,
                this.dummyAddress,
                this.description,
                null,
                this.module,
                this.eventQueue,
                this.queueApplicationEvent,
                this.timeKeeper);
        }

        //// Connect

        [Test]
        [ExpectedException("System.ArgumentNullException")]
        public void ConnectThrowWhenPassedNullConnectionResultHandler()
        {
            this.clientWithNoServer.Connect(
                null,
                this.connectionFailedHandler,
                this.serverEventHandler);
        }

        [Test]
        [ExpectedException("System.ArgumentNullException")]
        public void ConnectThrowWhenPassedNullConnectionFailedHandler()
        {
            this.clientWithNoServer.Connect(
                this.connectionResultHandler,
                null,
                this.serverEventHandler);
        }

        [Test]
        [ExpectedException("System.ArgumentNullException")]
        public void ConnectThrowWhenPassedNullServerEventHandler()
        {
            this.clientWithNoServer.Connect(
                this.connectionResultHandler,
                this.connectionFailedHandler,
                null);
        }

        [Test]
        public void DoNotConnectToNowhere()
        {
            this.manager.Expect(
                m => m.SendClientInitialSequence(
                    PeerAddress.Nowhere,
                    new CyclicalID.UShortID(0),
                    this.clientWithNoServer.OnConnectionFailure)).Repeat.Never();
            this.clientWithNoServer.Connect(
                this.connectionResultHandler,
                this.connectionFailedHandler,
                this.serverEventHandler);
            this.manager.VerifyAllExpectations();
        }

        [Test]
        public void SendInitialSequenceOnConnectionToKnownAddress()
        {
            this.manager.Expect(
                m => m.SendClientInitialSequence(
                    this.dummyAddress,
                    new CyclicalID.UShortID(0),
                    this.clientWithServer.OnConnectionFailure));
            this.clientWithServer.Connect(
                this.connectionResultHandler,
                this.connectionFailedHandler,
                this.serverEventHandler);
            this.eventQueue.RunOne();
            this.manager.VerifyAllExpectations();
        }

        [Test]
        public void ConnectOnServerBecomingAvailableIfPreviouslyRequested()
        {
            this.clientWithNoServer.Connect(
                this.connectionResultHandler,
                this.connectionFailedHandler,
                this.serverEventHandler);
            this.eventQueue.RunOne();
            this.manager.Expect(
                m => m.SendClientInitialSequence(
                    this.dummyAddress,
                    new CyclicalID.UShortID(0),
                    this.clientWithNoServer.OnConnectionFailure));
            this.clientWithNoServer.SwitchToNewServer(this.dummyAddress);
            this.eventQueue.RunAllIgnoringTime();
            this.manager.VerifyAllExpectations();
        }

        [Test]
        public void DoNotConnectOnServerBecomingAvailableIfNotPreviouslyRequested()
        {
            this.manager.Expect(
                m => m.SendClientInitialSequence(
                    this.dummyAddress,
                    new CyclicalID.UShortID(0),
                    this.clientWithNoServer.OnConnectionFailure)).Repeat.Never();
            this.clientWithNoServer.SwitchToNewServer(this.dummyAddress);
            this.manager.VerifyAllExpectations();
        }

        [Test]
        public void DirectConnectionSuccesNotifiedToClient()
        {            
            this.connectionResultHandler.Expect(h => h.Invoke(ServiceConnectionResultType.Success));
            this.clientWithServer.Connect(
                this.connectionResultHandler,
                this.connectionFailedHandler,
                this.serverEventHandler);
            this.eventQueue.RunOne();
            this.clientWithServer.ReceiveServerInitialSequence(new CyclicalID.UShortID(0));
            this.connectionResultHandler.VerifyAllExpectations();
        }

        [Test]
        public void ConnectionViaLookupSuccessNotifiedToClient()
        {
            this.connectionResultHandler.Expect(h => h.Invoke(ServiceConnectionResultType.Success));
            this.clientWithNoServer.Connect(
                this.connectionResultHandler,
                this.connectionFailedHandler,
                this.serverEventHandler);
            this.eventQueue.RunOne();
            this.clientWithNoServer.SwitchToNewServer(this.dummyAddress);
            this.clientWithNoServer.ReceiveServerInitialSequence(new CyclicalID.UShortID(0));
            this.connectionResultHandler.VerifyAllExpectations();
        }

        [Test]
        public void SuccessfulDirectConnectionDoesNotTriggerTimeout()
        {
            this.connectionResultHandler.Expect(h => h.Invoke(ServiceConnectionResultType.ConnectionTimeout)).Repeat.Never();
            this.clientWithServer.Connect(
                this.connectionResultHandler,
                this.connectionFailedHandler,
                this.serverEventHandler);
            this.eventQueue.RunOne();
            this.clientWithServer.ReceiveServerInitialSequence(new CyclicalID.UShortID(0));
            this.eventQueue.RunAllIgnoringTime();
            this.connectionResultHandler.VerifyAllExpectations();
        }

        [Test]
        public void SuccessfulConnectionViaLookupDoesNotTriggerTimeout()
        {
            this.connectionResultHandler.Expect(h => h.Invoke(ServiceConnectionResultType.ConnectionTimeout)).Repeat.Never();
            this.clientWithNoServer.Connect(
                this.connectionResultHandler,
                this.connectionFailedHandler,
                this.serverEventHandler);
            this.eventQueue.RunOne();
            this.clientWithNoServer.SwitchToNewServer(this.dummyAddress);
            this.clientWithNoServer.ReceiveServerInitialSequence(new CyclicalID.UShortID(0));
            this.eventQueue.RunAllIgnoringTime();
            this.connectionResultHandler.VerifyAllExpectations();
        }

        [Test]
        public void DirectConnectionAttemptTimeoutNotifiedToClient()
        {
            this.connectionResultHandler.Expect(h => h.Invoke(ServiceConnectionResultType.ConnectionTimeout));
            this.clientWithServer.Connect(
                this.connectionResultHandler,
                this.connectionFailedHandler,
                this.serverEventHandler);

            // Trigger ealy firing of any pending timeouts.
            this.eventQueue.RunAllIgnoringTime();
            this.connectionResultHandler.VerifyAllExpectations();
        }

        [Test]
        public void ConnectionViaLookupAttemptTimeoutNotifiedToClient()
        {
            this.connectionResultHandler.Expect(h => h.Invoke(ServiceConnectionResultType.ConnectionTimeout));
            this.clientWithNoServer.Connect(
                this.connectionResultHandler,
                this.connectionFailedHandler,
                this.serverEventHandler);
            this.eventQueue.RunOne();
            this.clientWithNoServer.SwitchToNewServer(this.dummyAddress);

            // Trigger ealy firing of any pending timeouts.
            this.eventQueue.RunAllIgnoringTime();
            this.connectionResultHandler.VerifyAllExpectations();
        }

        [Test]
        public void ConnectionViaLookupWithNoServerEverFoundTimeoutNotifiedToClient()
        {
            this.connectionResultHandler.Expect(h => h.Invoke(ServiceConnectionResultType.ServiceNotAvailable));
            this.clientWithNoServer.Connect(
                this.connectionResultHandler,
                this.connectionFailedHandler,
                this.serverEventHandler);

            // Trigger ealy firing of any pending timeouts.
            this.eventQueue.RunAllIgnoringTime();
            this.connectionResultHandler.VerifyAllExpectations();
        }

        //// IsArbitrationServerAvailable

        [Test]
        public void ServersNotAvailableWhenAddressIsNowhere()
        {
            Assert.IsFalse(this.clientWithNoServer.IsArbitrationServerAvailable());
        }

        [Test]
        public void ServerIsAvailableWhenTryingToConnect()
        {
            this.clientWithServer.Connect(
                this.connectionResultHandler,
                this.connectionFailedHandler,
                this.serverEventHandler);
            this.eventQueue.RunOne();
            Assert.IsTrue(this.clientWithServer.IsArbitrationServerAvailable());
        }

        [Test]
        public void ServerIsNotAvailableIfConnectionAttemptTimesOut()
        {
            this.clientWithServer.Connect(
                this.connectionResultHandler,
                this.connectionFailedHandler,
                this.serverEventHandler);

            // Timeout connection attempt.
            this.eventQueue.RunAllIgnoringTime();
            Assert.IsFalse(this.clientWithServer.IsArbitrationServerAvailable());
        }

        [Test]
        public void ServerIsAvailableWhenConnected()
        {
            this.clientWithServer.Connect(
                this.connectionResultHandler,
                this.connectionFailedHandler,
                this.serverEventHandler);
            this.eventQueue.RunOne();
            this.clientWithServer.ReceiveServerInitialSequence(new CyclicalID.UShortID(0));
            Assert.IsTrue(this.clientWithServer.IsArbitrationServerAvailable());
        }

        [Test]
        public void ServerIsNotAvailableWhenConnectionFails()
        {
            this.clientWithServer.Connect(
                this.connectionResultHandler,
                this.connectionFailedHandler,
                this.serverEventHandler);
            this.clientWithServer.ReceiveServerInitialSequence(new CyclicalID.UShortID(0));
            this.clientWithServer.OnConnectionFailure();
            Assert.IsFalse(this.clientWithServer.IsArbitrationServerAvailable());
        }

        //// OnConnnectionFailure

        [Test]
        public void ConnectionFailedhandlerCalledOnConnectionFailureForConnectedClient()
        {
            this.connectionFailedHandler.Expect(h => h.Invoke());
            this.clientWithServer.Connect(
                this.connectionResultHandler,
                this.connectionFailedHandler,
                this.serverEventHandler);
            this.eventQueue.RunOne();
            this.clientWithServer.ReceiveServerInitialSequence(new CyclicalID.UShortID(0));
            this.clientWithServer.OnConnectionFailure();
            this.connectionFailedHandler.VerifyAllExpectations();
        }

        //// ReceiveServerArbitrationEvent

        [Test]
        public void ServerArbitrationEventsArePassedToSequencer()
        {
            CyclicalID.UShortID seqNum = new CyclicalID.UShortID(0);
            byte[] message = Encoding.UTF8.GetBytes("Hello");
            this.sequencer.Expect(
                sequencer => sequencer.ReceiveEvent(seqNum, message));
            this.clientWithServer.ReceiveServerArbitrationEvent(seqNum, message);
            this.sequencer.VerifyAllExpectations();
        }

        //// ReceiveServerInitialSequence

        [Test]
        public void ClientIsNotConnectedUnlessInitialSequenceReceived()
        {
            this.clientWithServer.Connect(
                this.connectionResultHandler,
                this.connectionFailedHandler,
                this.serverEventHandler);
            Assert.IsFalse(this.clientWithServer.IsServerConnected);
        }

        [Test]
        public void ClientIsConnectedAfterInitialSequenceReceivedWhenConnecting()
        {
            CyclicalID.UShortID seqNum = new CyclicalID.UShortID(0);
            this.clientWithServer.Connect(
                this.connectionResultHandler,
                this.connectionFailedHandler,
                this.serverEventHandler);
            this.eventQueue.RunOne();
            this.clientWithServer.ReceiveServerInitialSequence(seqNum);
            Assert.IsTrue(this.clientWithServer.IsServerConnected);
        }

        [Test]
        public void ClientIsNotConnectedAfterInitialSequenceReceivedWhenNotConnecting()
        {
            CyclicalID.UShortID seqNum = new CyclicalID.UShortID(0);
            this.clientWithServer.ReceiveServerInitialSequence(seqNum);
            Assert.IsFalse(this.clientWithServer.IsServerConnected);
        }

        [Test]
        public void InitialSequenceIsPassedToSequencerWhenConnecting()
        {
            CyclicalID.UShortID seqNum = new CyclicalID.UShortID(0);
            this.sequencer.Expect(s => s.Initialize(seqNum));
            this.clientWithServer.Connect(
                this.connectionResultHandler,
                this.connectionFailedHandler,
                this.serverEventHandler);
            this.eventQueue.RunOne();
            this.clientWithServer.ReceiveServerInitialSequence(seqNum);
            this.sequencer.VerifyAllExpectations();
        }

        [Test]
        public void InitialSequenceIsNotPassedToSequencerWhenNotConnecting()
        {
            CyclicalID.UShortID seqNum = new CyclicalID.UShortID(0);
            this.sequencer.Expect(s => s.Initialize(seqNum)).Repeat.Never();
            this.clientWithServer.ReceiveServerInitialSequence(seqNum);
            this.sequencer.VerifyAllExpectations();
        }

        //// SendEvent

        [Test]
        public void SendEventSendsViaManagerWhenDirectlyConnected()
        {
            this.clientWithServer.Connect(
                this.connectionResultHandler,
                this.connectionFailedHandler,
                this.serverEventHandler);
            this.eventQueue.RunOne();
            this.clientWithServer.ReceiveServerInitialSequence(new CyclicalID.UShortID(0));
            byte[] message = Encoding.UTF8.GetBytes("Hello");
            this.manager.Expect(
                m => m.SendClientEvent(
                    this.dummyAddress,
                    new CyclicalID.UShortID(0),
                    message,
                    this.clientWithServer.OnConnectionFailure));
            this.clientWithServer.SendEvent(message);
            this.eventQueue.RunOne();
            this.manager.VerifyAllExpectations();
        }

        [Test]
        public void SendEventSendsViaManagerWhenConnectedViaLookup()
        {
            this.clientWithNoServer.Connect(
                this.connectionResultHandler,
                this.connectionFailedHandler,
                this.serverEventHandler);
            this.eventQueue.RunOne();
            this.clientWithNoServer.SwitchToNewServer(this.dummyAddress);
            this.clientWithNoServer.ReceiveServerInitialSequence(new CyclicalID.UShortID(0));
            byte[] message = Encoding.UTF8.GetBytes("Hello");
            this.manager.Expect(
                m => m.SendClientEvent(
                    this.dummyAddress,
                    new CyclicalID.UShortID(0),
                    message,
                    this.clientWithNoServer.OnConnectionFailure));
            this.clientWithNoServer.SendEvent(message);
            this.eventQueue.RunOne();
            this.manager.VerifyAllExpectations();
        }

        [Test]
        [ExpectedException("System.ArgumentNullException")]
        public void NullMessagesThrowException()
        {
            this.clientWithServer.SendEvent(null);
        }

        //// SwitchToNewServer

        [Test]
        public void SwitchingToNewServerChangesConectionStatusToNotConnected()
        {
            PeerAddress newAddress = new PeerAddress(new System.Net.IPAddress(2), 2, NatType.Open);
            this.clientWithServer.Connect(
                this.connectionResultHandler,
                this.connectionFailedHandler,
                this.serverEventHandler);
            this.eventQueue.RunOne();
            this.clientWithServer.ReceiveServerInitialSequence(new CyclicalID.UShortID(0));
            Assert.IsTrue(this.clientWithServer.IsServerConnected);
            this.clientWithServer.SwitchToNewServer(newAddress);
            Assert.IsFalse(this.clientWithServer.IsServerConnected);
        }

        [Test]
        public void SwitchingToNewServerSetsServerAddress()
        {
            PeerAddress newAddress = new PeerAddress(new System.Net.IPAddress(2), 2, NatType.Open);
            this.clientWithServer.SwitchToNewServer(newAddress);
            Assert.AreEqual(newAddress, this.clientWithServer.ServerAddress);
        }

        //// Integration Tests

        [Test]
        public void ReceivedMessagesPassedToApplicationHandler()
        {
            byte[] message = Encoding.UTF8.GetBytes("Hello");
            CyclicalID.UShortID seqNum = new CyclicalID.UShortID(0);
            ArbitrationClient client = new ArbitrationClient(
                this.manager,
                this.dummyAddress,
                this.description,
                this.module,
                this.eventQueue,
                this.queueApplicationEvent,
                this.timeKeeper);
            this.serverEventHandler.Expect(h => h.Invoke(message));
            client.Connect(
                this.connectionResultHandler,
                this.connectionFailedHandler,
                this.serverEventHandler);
            this.eventQueue.RunOne();
            client.ReceiveServerInitialSequence(seqNum);
            client.ReceiveServerArbitrationEvent(seqNum, message);
            this.serverEventHandler.VerifyAllExpectations();
        }
    }
}
