﻿//-----------------------------------------------------------------------
// <copyright file="DummyProtocolMethodAttribute.cs" company="NICTA Pty Ltd">
//     Copyright (c) 2012 NICTA Pty Ltd. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BadumnaTests.Core
{
    using Badumna.Core;

    /// <summary>
    /// Dummy protocol method attribute for use in tests.
    /// </summary>
    internal class DummyProtocolMethodAttribute : ProtocolMethodAttribute
    {
        public DummyProtocolMethodAttribute()
            : base(0)
        {
        }
    }
}
