﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.XPath;
using Badumna.Core;
using Badumna.Utilities;
using NUnit.Framework;

namespace BadumnaTests.Core
{
    [DontPerformCoverage]
    internal class MockModule : ConfigModule
    {
        private String mValue = String.Empty;
        public String Value { get { return this.mValue; } }

        public String mAttribute = String.Empty;
        public String Attribute { get { return this.mAttribute; } }

        private bool mConfigureHasBeenCalled;
        public bool ConfigureHasBeenCalled { get { return this.mConfigureHasBeenCalled; } }

        private bool mConfigureDefaultsCalled;
        public bool ConfigureDefaultsCalled { get { return this.mConfigureDefaultsCalled; } }

        public MockModule() : base("MockModule") { }

        internal override void Configure(IXPathNavigable configModuleNode)
        {
            XPathNavigator navigator = configModuleNode.CreateNavigator();

            XPathNavigator elementNode = navigator.SelectSingleNode("child::Element");
            this.mAttribute = elementNode.GetAttribute("Attribute", "");
            this.mValue = elementNode.InnerXml;
            this.mConfigureHasBeenCalled = true;
        }

        internal override void ConfigureDefaults()
        {
            this.mConfigureDefaultsCalled = true;
        }
    }

    [DontPerformCoverage]
    internal class MockExceptionModule : ConfigModule
    {
        public MockExceptionModule() : base("MockExceptionModule") { }

        internal override void Configure(IXPathNavigable configModuleNode)
        {
            throw new XPathException("an exception thrown by a broken config module");
        }
    }

    [DontPerformCoverage]
    internal class NoNameModule : ConfigModule
    {
        private bool mConfigureHasBeenCalled;
        public bool ConfigureHasBeenCalled { get { return this.mConfigureHasBeenCalled; } }

        public NoNameModule() : base("") { }

        internal override void Configure(IXPathNavigable navigable)
        {
        }

        internal override void ConfigureDefaults()
        {
            this.mConfigureHasBeenCalled = true;
        }
    }

    [TestFixture]
    [DontPerformCoverage]
    public class ConfigReaderTester
    {
        private ConfigReader mConfigReader;
        private static String mTestFilePath = "./test_config.xml";
        private static String mTestNotXmlFilePath = "./test_notxml_config.xml";
        private static String mTestInvalidFilePath = "./test_invalid_config.xml";

        public void UseReader(String fileName)
        {
            ConfigReader.Instance = null;
            this.mConfigReader = new ConfigReader(fileName);
            ConfigReader.Instance = this.mConfigReader;
        }

        [SetUp]
        public void Initialize()
        {
            if (File.Exists(ConfigReaderTester.mTestFilePath))
            {
                return;
            }

            XmlTextWriter writer = new XmlTextWriter(ConfigReaderTester.mTestFilePath, Encoding.ASCII);

            writer.Formatting = Formatting.Indented;
            writer.Indentation = 4;
            writer.Namespaces = false;

            writer.WriteStartDocument();

            writer.WriteStartElement("Modules");

            writer.WriteStartElement("Module");
            writer.WriteAttributeString("Name", "MockModule");

            writer.WriteStartElement("Element");
            writer.WriteAttributeString("Attribute", "TestAttribute");
            writer.WriteString("TestValue");
            writer.WriteEndElement();

            writer.WriteEndElement();

            writer.WriteStartElement("Module");
            writer.WriteAttributeString("Name", "MockExceptionModule");

            writer.WriteStartElement("Element");
            writer.WriteString("TestValue");
            writer.WriteEndElement();

            writer.WriteEndElement();

            writer.WriteStartElement("Module");
            writer.WriteAttributeString("Name", "NonExistantModule");
            writer.WriteEndElement();

            writer.WriteStartElement("Module");
            writer.WriteAttributeString("Name", "MockModule");

            writer.WriteStartElement("Element");
            writer.WriteString("WrongValue");
            writer.WriteEndElement();

            writer.WriteEndElement();

            writer.WriteEndElement();
            writer.WriteEndDocument();
            writer.Close();

            // Write an invalid config file
            writer = new XmlTextWriter(ConfigReaderTester.mTestInvalidFilePath, Encoding.ASCII);

            writer.Formatting = Formatting.Indented;
            writer.Indentation = 4;
            writer.Namespaces = false;

            writer.WriteStartDocument();

            writer.WriteStartElement("Modules");
            writer.WriteStartElement("UnModule");
            writer.WriteAttributeString("Noname", "NonExistantModule");
            writer.WriteEndElement();

            writer.WriteEndElement();
            writer.WriteEndDocument();
            writer.Close();

            // Write a non-xml file
            StreamWriter streamWriter = new StreamWriter(ConfigReaderTester.mTestNotXmlFilePath);

            streamWriter.Write("some random stuff that is not xml");
            streamWriter.Close();
        }

        [TearDown]
        public void Shutdown()
        {
            if (File.Exists(ConfigReaderTester.mTestFilePath))
            {
                File.Delete(ConfigReaderTester.mTestFilePath);
            }
            if (File.Exists(ConfigReaderTester.mTestNotXmlFilePath))
            {
                File.Delete(ConfigReaderTester.mTestNotXmlFilePath);
            }
            if (File.Exists(ConfigReaderTester.mTestInvalidFilePath))
            {
                File.Delete(ConfigReaderTester.mTestInvalidFilePath);
            }
        }

        [Test]
        public void LoadTest()
        {
            this.UseReader(ConfigReaderTester.mTestFilePath);

            // Test load of generated template.
            MockModule mockModule = new MockModule();

            // Test modules are given xml nodes.
            Assert.AreEqual("TestAttribute", mockModule.Attribute);
            Assert.AreEqual("TestValue", mockModule.Value);
            Assert.IsTrue(mockModule.ConfigureHasBeenCalled);

            // Test modules configuration XPath exceptions are caught.
            MockExceptionModule mockExceptionModule = new MockExceptionModule();
        }

        [Test]
        public void InvalidFileNameTest()
        {
            bool gotException = false;
            try
            {
                this.UseReader("non_existant_config_file.xml");
            }
            catch (FileNotFoundException)
            {
                gotException = true;
            }
            Assert.IsTrue(gotException);

            // Test that defaults have been used
            MockModule mockModule = new MockModule();

            Assert.IsFalse(mockModule.ConfigureHasBeenCalled);
            Assert.IsTrue(mockModule.ConfigureDefaultsCalled);
        }

        [Test]
        public void FileOpenTest()
        {
            // open to read
            using (FileStream readLocker = new FileStream(ConfigReaderTester.mTestFilePath, FileMode.Open, FileAccess.Read))
            {
                this.UseReader(ConfigReaderTester.mTestFilePath);

                // Test config still works.
                MockModule mockModule = new MockModule();

                Assert.AreEqual("TestAttribute", mockModule.Attribute);
                Assert.AreEqual("TestValue", mockModule.Value);
                Assert.IsTrue(mockModule.ConfigureHasBeenCalled);
            }

            // open to write
            using (FileStream writeLocker = new FileStream(ConfigReaderTester.mTestFilePath, FileMode.Open, FileAccess.Write))
            {
                bool gotException = false;
                try
                {
                    this.UseReader(ConfigReaderTester.mTestFilePath);
                }
                catch (IOException)
                {
                    gotException = true;
                }
                Assert.IsTrue(gotException);

                // Test config module uses defaults
                MockModule mockModule = new MockModule();

                Assert.IsFalse(mockModule.ConfigureHasBeenCalled);
                Assert.IsTrue(mockModule.ConfigureDefaultsCalled);
            }
        }

        [Test]
        public void NonXmlTest()
        {
            bool gotException = false;
            try
            {
                this.UseReader(ConfigReaderTester.mTestNotXmlFilePath);
            }
            catch (XmlException)
            {
                gotException = true;
            }
            Assert.IsTrue(gotException);

            // Test that a non xml document is caught and defualts are used.
            MockModule mockModule = new MockModule();

            Assert.IsFalse(mockModule.ConfigureHasBeenCalled);
            Assert.IsTrue(mockModule.ConfigureDefaultsCalled);
        }

        [Test]
        public void InvalidNameElementTest()
        {
            this.UseReader(ConfigReaderTester.mTestInvalidFilePath);

            // Test that unexpected xml is caught and defualts are used.
            MockModule mockModule = new MockModule();

            Assert.IsFalse(mockModule.ConfigureHasBeenCalled);
            Assert.IsTrue(mockModule.ConfigureDefaultsCalled);
        }

        [Test]
        public void NoNameTest()
        {
            this.UseReader(ConfigReaderTester.mTestInvalidFilePath);

            // Test that an invalid name cause defaults to be used.
            NoNameModule module = new NoNameModule();

            Assert.IsTrue(module.ConfigureHasBeenCalled);
        }
    }

}
