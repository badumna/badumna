﻿using System;
using System.Collections.Generic;
using System.Text;

using Badumna.Utilities;
using NUnit.Framework;
using Badumna.Core;

namespace BadumnaTests.Core
{
    [TestFixture]
    public class ChecksumSaltTester
    {
        // Check values calculated using http://zorc.breitbandkatze.de/crc.html

        [Test]
        public void ActualMessageTest()
        {
            MessageBuffer message = new MessageBuffer();
            PeerAddress address = new PeerAddress(new System.Net.IPAddress(new byte[] { 128, 250, 77, 95 }), 41251, NatType.FullCone);
            byte[] extraSalt = new byte[] { 0, 1, 2, 3, 4, 5, 6, 7 };

            byte[] data = new byte[] { 41, 240, 54, 136, 55, 237, 73, 149, 0 };
            message.Write(data, data.Length);

            message.Checksum(new ChecksumSalter("testAppName").GetSalt(address), extraSalt);

            MessageBuffer copy = new MessageBuffer(message);
            Assert.IsTrue(copy.ValidateChecksum(new ChecksumSalter("testAppName").GetSalt(address), extraSalt), "Failed to checksum message");
        }
    }
}
