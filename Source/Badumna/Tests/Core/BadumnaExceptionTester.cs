﻿using System;
using System.Collections.Generic;
using System.Text;

using Badumna.Utilities;
using NUnit.Framework;
using Badumna.Core;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace BadumnaTests.Core
{
    [DontPerformCoverage]
    abstract class NetworkExceptionTester<T> where T : BadumnaException
    {
        [Test]
        public void DefaultTest()
        {
            try
            {
                throw (T)Activator.CreateInstance(typeof(T));
            }
            catch (BadumnaException e)
            {
                e.TraceMessage();
            }
        }

        [Test]
        public void MessageTest()
        {
            try
            {
                throw (T)Activator.CreateInstance(typeof(T), "Test exception");
            }
            catch (BadumnaException e)
            {
                e.TraceMessage();
            }
        }

        [Test]
        public void InnerTest()
        {
            try
            {
                throw (BadumnaException)Activator.CreateInstance(typeof(T), "Test exception", new Exception());
            }
            catch (BadumnaException e)
            {
                e.TraceMessage();
            }
        }

        [Test]
        public void TraceTest()
        {
            try
            {
                throw (BadumnaException)Activator.CreateInstance(typeof(T), "Test exception");
            }
            catch (BadumnaException e)
            {
                e.TraceMessage();
            }
        }

        [Test]
        public void SerializableTest()
        {
            T exception = this.CreateExceptionForSeralization();
            T deserializedException;

            using (MemoryStream stream = new MemoryStream())
            {
                BinaryFormatter formatter = new BinaryFormatter();
                formatter.Serialize(stream, exception);
                stream.Position = 0;
                deserializedException = (T)formatter.Deserialize(stream);
            }

            Assert.IsTrue(this.CheckEquality(exception, deserializedException));
        }

        protected virtual bool CheckEquality(T left, T right)
        {
            return left.Message == right.Message;
        }

        protected virtual T CreateExceptionForSeralization()
        {
            return (T)Activator.CreateInstance(typeof(T));
        }
    }


    [TestFixture]
    [DontPerformCoverage]
    class CoreExceptionTester : NetworkExceptionTester<CoreException>
    {
    }

    [TestFixture]
    [DontPerformCoverage]
    class ConfigurationExceptionTester : NetworkExceptionTester<ConfigurationException>
    {
        protected override ConfigurationException CreateExceptionForSeralization()
        {
            return new ConfigurationException(ConfigurationException.ErrorCode.Inconsistent, "Inconsistency error");
        }

        protected override bool CheckEquality(ConfigurationException left, ConfigurationException right)
        {
            return base.CheckEquality(left, right) && left.Error == right.Error;
        }
    }

    [TestFixture]
    [DontPerformCoverage]
    class EndOfMessageExceptionTester : NetworkExceptionTester<EndOfMessageException>
    {
    }

    [TestFixture]
    [DontPerformCoverage]
    class ParseableMethodExceptionTester : NetworkExceptionTester<ParseableMethodException>
    {
    }

    [TestFixture]
    [DontPerformCoverage]
    class ParseExceptionTester : NetworkExceptionTester<ParseException>
    {
    }

    [TestFixture]
    [DontPerformCoverage]
    class UnknownMethodExceptionTester : NetworkExceptionTester<UnknownMethodException>
    {
    }

    [TestFixture]
    [DontPerformCoverage]
    class ForgeMessageExceptionTester : NetworkExceptionTester<ForgeMessageException>
    {
    }
}
