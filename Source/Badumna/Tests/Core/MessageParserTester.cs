﻿using System;
using System.Collections;
using System.Collections.Generic;
using Badumna.Core;
using Badumna.Utilities;
using NUnit.Framework;

namespace BadumnaTests.Core
{
    /// <summary>
    /// A test class for checking the appropriate protection levels on parsed protocol methods work 
    /// as expected.
    /// </summary>
    [DontPerformCoverage]
    abstract class AbstractAutoParseTest : MessageParser
    {
        private String mReceivedString;
        public String ReceivedString { get { return this.mReceivedString; } }

        private int mRecievedInt;
        public int ReceivedInt { get { return this.mRecievedInt; } }

        private bool mProtectedFlag;
        public bool ProtectedFlag { get { return this.mProtectedFlag; } }

        private bool mAbstractFlag;
        public bool AbstractFlag
        {
            get { return this.mAbstractFlag; }
            set { this.mAbstractFlag = value; }
        }

        private int mAbstractInt;
        public int AbstractInt
        {
            get { return this.mAbstractInt; }
            set { this.mAbstractInt = value; }
        }

        private int mProtectedInt;
        public int ProtectedInt { get { return this.mProtectedInt; } }

        protected AbstractAutoParseTest()
            : base("AbstractAutoParse", typeof(UnitTestProtocolMethodAttribute), MessageParserTester.DefaultFactory)
        {
            this.mReceivedString = String.Empty;
            this.RegisterMethodsIn(this);
        }

        [UnitTestProtocolMethod(0)]
        public void ParsingExampleMethod(String firstParameter, int secondParameter)
        {
            this.mReceivedString = firstParameter;
            this.mRecievedInt = secondParameter;
        }

        [UnitTestProtocolMethod(1)]
        protected void ProtectedTest()
        {
            this.mProtectedFlag = true;
        }

        [UnitTestProtocolMethod(2)]
        public abstract void AbstractTest();


        [UnitTestProtocolMethod(3)]
        protected void ProtectedTest(int arg)
        {
            this.mProtectedInt = arg;
        }

        [UnitTestProtocolMethod(4)]
        public abstract void AbstractTest(int arg);

        public BaseEnvelope WriteTestMessage(String testArgument1, int testArgument2)
        {
            BaseEnvelope envelope = new BaseEnvelope();

            this.RemoteCall(envelope, this.ParsingExampleMethod, testArgument1, testArgument2);

            return envelope;
        }
    }

    [DontPerformCoverage]
    class AutoParseTest : AbstractAutoParseTest
    {
        public override void AbstractTest()
        {
            this.AbstractFlag = true;
        }

        public override void AbstractTest(int arg)
        {
            this.AbstractInt = arg;
        }
    }


    [TestFixture]
    [DontPerformCoverage]
    public class MessageParserTester
    {
        internal static object DefaultFactory(Type type)
        {
            return Activator.CreateInstance(type, true);
        }

        private MessageParser mMessageParser;

        private int mNextDelegateNum = 0;

        private bool mPublicParsed = false;
        static bool mStaticParsed = false;

        [UnitTestProtocolMethod(0)]
        public void TestPublicParse()
        {
            this.mPublicParsed = true;
        }

        [UnitTestProtocolMethod(1)]
        static public void TestStaticParse()
        {
            MessageParserTester.mStaticParsed = true;
        }

        [UnitTestProtocolMethod(2)]
        public void TestDelegate0()
        {
            CheckOrder(0);
            //Console.WriteLine("Delegate 0");
        }

        [UnitTestProtocolMethod(3)]
        public void TestDelegate1(String s)
        {
            CheckOrder(1);
            //Console.WriteLine("Delegate 1 : {0}", s);
        }

        [UnitTestProtocolMethod(4)]
        public void TestDelegate2(String s, int i)
        {
            CheckOrder(2);
            //Console.WriteLine("Delegate 2 : {0}, {1}", s, i);
        }

        [UnitTestProtocolMethod(5)]
        public void TestDelegate3(String s, float f, int i)
        {
            CheckOrder(3);
            //Console.WriteLine("Delegate 3 : {0}, {1}, {2}", s, f, i);
        }

        [UnitTestProtocolMethod(6)]
        public void TestDelegate4(String s, double d, byte[] c, int i)
        {
            CheckOrder(4);
            //Console.WriteLine("Delegate 4 : {0}, {1}, {2}, {3}", s, d, c, i);
        }

        [UnitTestProtocolMethod(7)]
        public void TestDelegate5(String s, double d, char c, int i, float f)
        {
            CheckOrder(5);
            //Console.WriteLine("Delegate 5 : {0}, {1}, {2}, {3}, {4}", s, d, c, i, f);
        }

        [UnitTestProtocolMethod(8)]
        public void SameNameDelegate(String s)
        {
            CheckOrder(0);
            //Console.WriteLine("Same name delegate : {0}", s);
        }

        [UnitTestProtocolMethod(9)]
        public void SameNameDelegate(int i)
        {
            CheckOrder(1);
            //Console.WriteLine("Same name delegate : {0}", i);
        }

        [UnitTestProtocolMethod(10)]
        public void TestDelegate6()
        {
            // Not called in test cases.
        }

        private IList<String> mListOne;
        private IList<int> mListTwo;

        [UnitTestProtocolMethod(11)]
        public void ListTestDelegate(IList<String> listOne, List<int> listTwo)
        {
            this.mListOne = listOne;
            this.mListTwo = listTwo;
        }

        GenericCallBack mDel0;
        GenericCallBack<String> mDel1;
        GenericCallBack<String, int> mDel2;
        GenericCallBack<String, float, int> mDel3;
        GenericCallBack<String, double, byte[], int> mDel4;
        GenericCallBack<String, double, char, int, float> mDel5;
        GenericCallBack<String> mSameNameDel0;
        GenericCallBack<int> mSameNameDel1;


        [SetUp]
        public void Initialize()
        {
            this.mNextDelegateNum = 0;
            this.mMessageParser = new MessageParser("MessageParserTester", typeof(UnitTestProtocolMethodAttribute), MessageParserTester.DefaultFactory);

            this.mDel0 = new GenericCallBack(this.TestDelegate0);
            this.mDel1 = new GenericCallBack<String>(this.TestDelegate1);
            this.mDel2 = new GenericCallBack<String, int>(this.TestDelegate2);
            this.mDel3 = new GenericCallBack<String, float, int>(this.TestDelegate3);
            this.mDel4 = new GenericCallBack<String, double, byte[], int>(this.TestDelegate4);
            this.mDel5 = new GenericCallBack<String, double, char, int, float>(this.TestDelegate5);

            this.mSameNameDel0 = new GenericCallBack<String>(this.SameNameDelegate);
            this.mSameNameDel1 = new GenericCallBack<int>(this.SameNameDelegate);
        }

        [TearDown]
        public void Terminate()
        {
        }

        [Test]
        public void AddDelegateTest()
        {
            bool caughtException = false;

            // Test that we can add a delegate.
            this.mMessageParser.AddDelegate(mDel1, 1);

            // Test that we can forge the method list.
            this.mMessageParser.ForgeMethodList();

            // Test that attempting to add a delegate after forgeing fails with an exception.
            try
            {
                this.mMessageParser.AddDelegate(mDel2, 2);
            }
            catch (ForgeMessageException) // e)
            {
                caughtException = true;
                //Console.WriteLine("Expected exception caught : " + e.Message);
            }

            Assert.IsTrue(caughtException, "Failed to catch expected exception.");
        }

        [Test]
        public void MultipleAddTest()
        {
            // Test that we can add 6 delegates with up to 5 arguments.
            this.mMessageParser.AddDelegate(mDel0, 0);
            this.mMessageParser.AddDelegate(mDel1, 1);
            this.mMessageParser.AddDelegate(mDel2, 2);
            this.mMessageParser.AddDelegate(mDel3, 3);
            this.mMessageParser.AddDelegate(mDel4, 4);
            this.mMessageParser.AddDelegate(mDel5, 5);
        }

        [Test]
        public void IndexTest()
        {
            this.MultipleAddTest();

            // Test that forgeing creates the minimum sized index for the delegates.
            this.mMessageParser.ForgeMethodList();

            // 6 delegates, 2 < log_2(6) < 3, so need at least 3 bits.
            Assert.AreEqual(3, this.mMessageParser.BitsInDelegateId);
        }

        [Test]
        public void WriteToTest()
        {
            BaseEnvelope envelope = new BaseEnvelope();

            this.MultipleAddTest();
            this.mMessageParser.ForgeMethodList();

            // Test that we can WriteToMessage for each delegate.
            this.mMessageParser.RemoteCall(envelope, mDel0);
            this.mMessageParser.RemoteCall(envelope, mDel1, "Example string.");
            this.mMessageParser.RemoteCall(envelope, mDel2, "Example string.", 3);
            this.mMessageParser.RemoteCall(envelope, mDel3, "Example string.", 5.0f, 2);
            this.mMessageParser.RemoteCall(envelope, mDel4, "Example string.", 3.0, new byte[2], 2);
            this.mMessageParser.RemoteCall(envelope, mDel5, "Example string.", 3.0, 'a', 2, 5.0f);

            // Test that attempting to write an unregistered delegate fails and throws an exception.
            GenericCallBack unregistedDel = new GenericCallBack(this.TestDelegate6);
            bool caughtException = false;

            try
            {
                this.mMessageParser.RemoteCall(envelope, unregistedDel);
            }
            catch (UnknownMethodException) // e)
            {
                //Console.WriteLine("Caught expected exception : " + e.Message);
                caughtException = true;
            }

            Assert.IsTrue(caughtException, "Failed to catch expected exception.");
        }

        [Test]
        public void GenericWriteToTest()
        {
            BaseEnvelope envelope = new BaseEnvelope();

            this.MultipleAddTest();
            this.mMessageParser.ForgeMethodList();

            // Test that we can WriteToMessage for each delegate.
            // this.mMessageParser.RemoteCall(envelope, this, mDel0.Method);
            this.mMessageParser.RemoteCall(envelope, this.TestDelegate0);
            this.mMessageParser.RemoteCall(envelope, this.TestDelegate1, "Example string.");
            this.mMessageParser.RemoteCall(envelope, this.TestDelegate2, "Example string.", 3);
            this.mMessageParser.RemoteCall(envelope, this.TestDelegate3, "Example string.", 5.0f, 2);
            this.mMessageParser.RemoteCall(envelope, this.TestDelegate4, "Example string.", 3.0, new byte[2], 2);
            this.mMessageParser.RemoteCall(envelope, this.TestDelegate5, "Example string.", 3.0, 'a', 2, 5.0f);
        }

        [Test]
        public void ParseTest()
        {
            BaseEnvelope envelope = new BaseEnvelope();

            this.MultipleAddTest();
            this.mMessageParser.ForgeMethodList();

            // Test that we can WriteToMessage for each delegate.
            this.mMessageParser.RemoteCall(envelope, mDel0);
            this.mMessageParser.RemoteCall(envelope, mDel1, "Example string.");
            this.mMessageParser.RemoteCall(envelope, mDel2, "Example string.", 3);
            this.mMessageParser.RemoteCall(envelope, mDel3, "Example string.", 5.0f, 2);
            this.mMessageParser.RemoteCall(envelope, mDel4, "Example string.", 3.0, new byte[2], 2);
            this.mMessageParser.RemoteCall(envelope, mDel5, "Example string.", 3.0, 'a', 2, 5.0f);

            // Test that the Parsed message calls each of the 5 delegates in order.
            this.mMessageParser.Parse(envelope);

            Assert.AreEqual(6, this.mNextDelegateNum, "Failed to call delegates in order.");
        }

        [Test, ExpectedException(typeof(ParseException))] // Fail to parse exception.
        public void SerializationFailTest()
        {
            String testData = "Some random stuff: 42097202lk20- %$%@#$K%246 9826 ^Vl;gvliws83gvp54@Cc5ouv06vb$^fc67";
            BaseEnvelope envelope = new BaseEnvelope();

            envelope.Message.Write(testData);

            // Test that an unparsable message fails with exception.            
            this.MultipleAddTest();
            this.mMessageParser.ForgeMethodList();
            this.mMessageParser.Parse(envelope);
        }

        private void CheckOrder(int expectedDelegateNum)
        {
            if (this.mNextDelegateNum != expectedDelegateNum)
                throw new ApplicationException("Delegates called out of order.");

            this.mNextDelegateNum++;
        }

        [Test]
        public void MethodNameDisambiguateTest()
        {
            BaseEnvelope envelope = new BaseEnvelope();

            this.mMessageParser.AddDelegate(mSameNameDel0, 0);
            this.mMessageParser.AddDelegate(mSameNameDel1, 1);
            this.mMessageParser.ForgeMethodList();

            this.mMessageParser.RemoteCall(envelope, mSameNameDel0, "a string");
            this.mMessageParser.RemoteCall(envelope, mSameNameDel1, 99);

            // Test that delegates with the same name can be disambiguated.
            this.mMessageParser.Parse(envelope);

            Assert.AreEqual(2, this.mNextDelegateNum, "Failed to call delegates in order.");
        }

        [Test]
        public void ZeroDelegateTest()
        {
            BaseEnvelope envelope = new BaseEnvelope();
            MessageParser layer1 = new MessageParser("Layer1", typeof(UnitTestProtocolMethodAttribute), MessageParserTester.DefaultFactory);
            Assert.Throws<InvalidOperationException>(() => layer1.ForgeMethodList(), "Forge succeeded even though no methods were registered");
        }

        [Test]
        public void AutoParseTest()
        {
            AutoParseTest protocol = new AutoParseTest();

            protocol.RegisterMethodsIn(this);
            protocol.ForgeMethodList();

            BaseEnvelope envelope = protocol.WriteTestMessage("testString", 99);

            protocol.RemoteCall(envelope, this.TestPublicParse);
            protocol.RemoteCall(envelope, TestStaticParse);
            protocol.RemoteCall(envelope, protocol.AbstractTest);

            AutoParseTest protocol2 = new AutoParseTest();

            protocol2.RegisterMethodsIn(this);
            protocol2.ForgeMethodList();

            protocol2.Parse(envelope);

            Assert.AreEqual("testString", protocol2.ReceivedString);
            Assert.AreEqual(99, protocol2.ReceivedInt);
            Assert.IsTrue(this.mPublicParsed);
            Assert.IsTrue(MessageParserTester.mStaticParsed);

            Assert.IsTrue(protocol2.AbstractFlag);
        }

        [Test]
        public void ListTest()
        {
            BaseEnvelope envelope = new BaseEnvelope();

            this.mMessageParser.RegisterMethodsIn(this);
            this.mMessageParser.ForgeMethodList();

            IList<String> listOne = new List<String>();
            List<int> listTwo = new List<int>();

            listOne.Add("one");
            listOne.Add("two");

            for (int i = 0; i < 100; i++)
            {
                listTwo.Add(i);
            }

            this.mMessageParser.RemoteCall(envelope, this.ListTestDelegate, listOne, listTwo);

            // Test that the parsed delegate is called correctly.
            this.mMessageParser.Parse(envelope);

            Assert.AreEqual(2, this.mListOne.Count);
            Assert.AreEqual("one", this.mListOne[0]);
            Assert.AreEqual("two", this.mListOne[1]);

            Assert.AreEqual(100, this.mListTwo.Count);
            for (int i = 0; i < this.mListTwo.Count; i++)
            {
                Assert.AreEqual(i, this.mListTwo[i]);
            }
        }

        [Test]
        public void EndOfMessageTest()
        {
            AutoParseTest protocol = new AutoParseTest();

            protocol.RegisterMethodsIn(this);
            protocol.ForgeMethodList();

            BaseEnvelope envelope = protocol.WriteTestMessage("testString", 99);

            protocol.RemoteCall(envelope, this.TestPublicParse);

            AutoParseTest protocol2 = new AutoParseTest();

            protocol2.RegisterMethodsIn(this);
            protocol2.ForgeMethodList();

            protocol2.Parse(envelope);

            Assert.AreEqual("testString", protocol2.ReceivedString);
        }


        // TODO : Test that indexes that don't fit 1 byte are caught.
    }
}
