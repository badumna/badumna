﻿using System;
using Badumna.Core;
using NUnit.Framework;

namespace BadumnaTests.Core
{
    [DontPerformCoverage]
    class MockEnvelope : BaseEnvelope
    {
    }

    [DontPerformCoverage]
    class TestProtocolComponent1<T> : ProtocolComponent<T> where T : IEnvelope
    {
        public bool mHasCallTestCB = false;
        public String SendText = "Layer1 test argument.";
        private String ExpectedText = "Layer1 test argument.";

        private T mNextMessage;
        public T NextMessage { set { this.mNextMessage = value; } }

        public TestProtocolComponent1()
            : base("TestProtocolComponent", typeof(ConnectionfulProtocolMethodAttribute), MessageParserTester.DefaultFactory)
        {
        }

        public void Initialize()
        {
            this.ForgeMethodList();
        }

        public T GetMessageFor(T envelope)
        {
            this.PrepareMessageForDeparture(ref envelope);

            return envelope;
        }

        [ConnectionfulProtocolMethod(0)]
        public void TestCB(String text)
        {
            Assert.AreEqual(this.ExpectedText, text);
            this.mHasCallTestCB = true;
        }

        public override void PrepareMessageForDeparture(ref T envelope)
        {
            if (this.mNextMessage != null)
            {
                envelope = this.mNextMessage;
            }

            this.RemoteCall(envelope, this.TestCB, this.SendText);
        }
    }

    [DontPerformCoverage]
    class TestProtocolComponent2<T> : ProtocolComponent<T> where T : IEnvelope
    {
        public bool mHasCallTestCB = false;

        private int mMessagesArrivedCount;
        public int MessagesArrivedCount { get { return this.mMessagesArrivedCount; } }

        public String SendText = "Layer2 test argument.";
        public String ExpectedText = "Layer2 test argument.";

        public TimeSpan MessageReceiveTime;

        private ITime timeKeeper;

        public TestProtocolComponent2(ProtocolComponent<T> parent, ITime timeKeeper)
            : base(parent)
        {
            this.timeKeeper = timeKeeper;
        }

        public void Initialize()
        {
            this.ForgeMethodList();
        }

        public T GetMessageFor(T envelope)
        {
            this.PrepareMessageForDeparture(ref envelope);

            return envelope;
        }

        [ConnectionfulProtocolMethod(0)]
        public void TestCB(String text)
        {
            Assert.AreEqual(this.ExpectedText, text);
            this.MessageReceiveTime = this.timeKeeper.Now;
            this.mHasCallTestCB = true;
            this.mMessagesArrivedCount++;
        }

        public void MakeCall(T envelope)
        {
            this.RemoteCall(envelope, this.TestCB, this.SendText);
        }
    }

    /*
    [DontPerformCoverage]
    internal class TestDepartureFailLayer<T> : ProtocolComponent<T> where T : BaseEnvelope
    {
        public TestDepartureFailLayer()
            : base()
        { }

        protected override void PrepareMessageForDeparture(ref T envelope)
        {
            throw new Exception("Test MessageDeparture() exception.");
        }
    }

    [DontPerformCoverage]
    internal class NoForgeTestLayer<T> : ProtocolComponent<T> where T : BaseEnvelope
    {
        public NoForgeTestLayer()
        {
            // Meant to be empty.
        }
    }

    */

    [TestFixture]
    [DontPerformCoverage]
    public class ProtocolComponentTester
    {

        [SetUp]
        public void Initialize()
        {
        }

        [TearDown]
        public void Terminate()
        {
        }

        [Test]
        public void MessagePreperationTest()
        {
            ITime timeKeeper = new NetworkEventQueue();

            TestProtocolComponent1<MockEnvelope> root = new TestProtocolComponent1<MockEnvelope>();
            TestProtocolComponent2<MockEnvelope> child2 = new TestProtocolComponent2<MockEnvelope>(root, timeKeeper);
            TestProtocolComponent2<MockEnvelope> child3 = new TestProtocolComponent2<MockEnvelope>(root, timeKeeper);
            root.Initialize();
            child2.Initialize();
            child3.Initialize();

            // Test that messages gotten from layer 3 have layer 1 message written
            MockEnvelope envelope = child3.GetMessageFor(new MockEnvelope());

            root.ParseMessage(envelope, 0);
            Assert.IsTrue(root.mHasCallTestCB);
        }

        [Test]
        public void MessageParseTest()
        {
            ITime timeKeeper = new NetworkEventQueue();

            TestProtocolComponent1<MockEnvelope> root = new TestProtocolComponent1<MockEnvelope>();
            TestProtocolComponent2<MockEnvelope> child2 = new TestProtocolComponent2<MockEnvelope>(root, timeKeeper);
            TestProtocolComponent2<MockEnvelope> child3 = new TestProtocolComponent2<MockEnvelope>(root, timeKeeper);
            root.Initialize();
            child2.Initialize();
            child3.Initialize();

            MockEnvelope envelope = child3.GetMessageFor(new MockEnvelope());

            child2.MakeCall(envelope);
            child3.MakeCall(envelope);

            // Test that all the methods written to a message are called.
            root.ParseMessage(envelope, 0);

            // Test that all the intermediate methods are called in order.
            // Note. The asserts for order are in the methods themselves.
            Assert.IsTrue(root.mHasCallTestCB);
            Assert.IsTrue(child2.mHasCallTestCB);
            Assert.IsTrue(child3.mHasCallTestCB);
        }

        [Test]
        public void MultipleChildrenTest()
        {
            ITime timeKeeper = new NetworkEventQueue();

            // Add some other children
            TestProtocolComponent1<MockEnvelope> root = new TestProtocolComponent1<MockEnvelope>();
            TestProtocolComponent2<MockEnvelope> child2 = new TestProtocolComponent2<MockEnvelope>(root, timeKeeper);
            TestProtocolComponent2<MockEnvelope> child3 = new TestProtocolComponent2<MockEnvelope>(root, timeKeeper);
            TestProtocolComponent2<MockEnvelope> child4 = new TestProtocolComponent2<MockEnvelope>(root, timeKeeper);

            root.ForgeMethodList();

            MockEnvelope envelope = child2.GetMessageFor(new MockEnvelope());
            child2.MakeCall(envelope);
            root.ParseMessage(envelope, 0);
            Assert.IsTrue(child2.mHasCallTestCB);

            envelope = child3.GetMessageFor(new MockEnvelope());
            child3.MakeCall(envelope);
            root.ParseMessage(envelope, 0);
            Assert.IsTrue(child3.mHasCallTestCB);

            envelope = child4.GetMessageFor(new MockEnvelope());
            child4.MakeCall(envelope);
            root.ParseMessage(envelope, 0);
            Assert.IsTrue(child4.mHasCallTestCB);

        }

        [Test]
        public void LotsOfMethodsTest()
        {
            ITime timeKeeper = new NetworkEventQueue();

            TestProtocolComponent1<MockEnvelope> root = new TestProtocolComponent1<MockEnvelope>();
            TestProtocolComponent2<MockEnvelope>[] protocolComponents = new TestProtocolComponent2<MockEnvelope>[1000];

            for (int i = 0; i < protocolComponents.Length; i++) // max braching factor
            {
                protocolComponents[i] = new TestProtocolComponent2<MockEnvelope>(root, timeKeeper);
            }

            root.ForgeMethodList();

            for (int i = 0; i < protocolComponents.Length; i++) // max braching factor
            {
                MockEnvelope envelope = root.GetMessageFor(new MockEnvelope());
                protocolComponents[i].MakeCall(envelope);
                root.ParseMessage(envelope, 0);
                Assert.IsTrue(protocolComponents[i].mHasCallTestCB, "Failed to call method on protocol {0}", i);
            }
        }

        [Test]
        public void MultipleParseTest()
        {
            ITime timeKeeper = new NetworkEventQueue();

            // Add some other children
            TestProtocolComponent1<MockEnvelope> root = new TestProtocolComponent1<MockEnvelope>();
            TestProtocolComponent2<MockEnvelope> child2 = new TestProtocolComponent2<MockEnvelope>(root, timeKeeper);
            TestProtocolComponent2<MockEnvelope> child3 = new TestProtocolComponent2<MockEnvelope>(root, timeKeeper);
            TestProtocolComponent2<MockEnvelope> child4 = new TestProtocolComponent2<MockEnvelope>(root, timeKeeper);
            root.Initialize();
            child2.Initialize();
            child3.Initialize();
            child4.Initialize();

            // Test that we can use the same message for multiple parses.
            MockEnvelope envelope = root.GetMessageFor(new MockEnvelope());

            root.NextMessage = envelope;
            MockEnvelope envelope2 = child3.GetMessageFor(envelope);
            child3.MakeCall(envelope2);

            root.NextMessage = envelope2;
            MockEnvelope envelope3 = child4.GetMessageFor(envelope2);
            child4.MakeCall(envelope3);

            while (envelope.Available > 0)
            {
                root.ParseMessage(envelope, envelope.Message.ReadOffset);
            }

            Assert.IsTrue(root.mHasCallTestCB);
            Assert.IsTrue(child3.mHasCallTestCB);
            Assert.IsTrue(child4.mHasCallTestCB);
        }
    }

    [TestFixture]
    [DontPerformCoverage]
    class ProtocolExceptionTester : NetworkExceptionTester<ProtocolException>
    {
    }
}
